/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* This program tests the clhy_find_list_item routine in ../main/util.c.
 *
 * The defines in this sample compile line are specific to Roy's system.
 * They should match whatever was used to compile cLHy first.
 *
     gcc -g -O2 -I../platforms/unix -I../include -o test_find \
            -DSOLARIS2=250 -Wall -DALLOC_DEBUG -DPOOL_DEBUG \
            ../main/alloc.o ../main/buff.o ../main/util.o \
            ../clhy/libap.a -lsocket -lnsl test_find.c
 *
 * Roy Fielding, 1999
 */
#include <stdio.h>
#include <stdlib.h>
#include "wwhy.h"
#include "kuda_general.h"

/*
 * Dummy a bunch of stuff just to get a compile
 */
uid_t clhy_user_id;
gid_t clhy_group_id;
void *clhy_dummy_mutex = &clhy_dummy_mutex;
char *clhy_server_argv0;

CLHY_DECLARE(void) clhy_block_alarms(void)
{
    ;
}

CLHY_DECLARE(void) clhy_unblock_alarms(void)
{
    ;
}

CLHY_DECLARE(void) clhy_log_error(const char *file, int line, int level,
                              const request_rec *r, const char *fmt, ...)
{
    ;
}

int main (void)
{
    kuda_pool_t *p;
    char line[512];
    char tok[512];

    p = kuda_pool_alloc_init();

    printf("Enter field value to find items within:\n");
    if (!gets(line))
        exit(0);

    printf("Enter search item:\n");
    while (gets(tok)) {
        printf("  [%s] == %s\n", tok, clhy_find_list_item(p, line, tok)
                                  ? "Yes" : "No");
        printf("Enter search item:\n");
    }

    exit(0);
}
