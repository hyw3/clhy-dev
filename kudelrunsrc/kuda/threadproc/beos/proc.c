/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_threadproc.h"
#include "kuda_strings.h"

/* Heavy on no'ops, here's what we want to pass if there is KUDA_NO_FILE
 * requested for a specific child handle;
 */
static kuda_file_t no_file = { NULL, -1, };

struct send_pipe {
	int in;
	int out;
	int err;
};

KUDA_DECLARE(kuda_status_t) kuda_procattr_create(kuda_procattr_t **new, kuda_pool_t *pool)
{
    (*new) = (kuda_procattr_t *)kuda_palloc(pool, 
              sizeof(kuda_procattr_t));

    if ((*new) == NULL) {
        return KUDA_ENOMEM;
    }
    (*new)->pool = pool;
    (*new)->parent_in = NULL;
    (*new)->child_in = NULL;
    (*new)->parent_out = NULL;
    (*new)->child_out = NULL;
    (*new)->parent_err = NULL;
    (*new)->child_err = NULL;
    (*new)->currdir = NULL; 
    (*new)->cmdtype = KUDA_PROGRAM;
    (*new)->detached = 0;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_io_set(kuda_procattr_t *attr,
                                              kuda_int32_t in,
                                              kuda_int32_t out,
                                              kuda_int32_t err)
{
    kuda_status_t rv;

    if ((in != KUDA_NO_PIPE) && (in != KUDA_NO_FILE)) {
        /* KUDA_CHILD_BLOCK maps to KUDA_WRITE_BLOCK, while
         * KUDA_PARENT_BLOCK maps to KUDA_READ_BLOCK, so transpose 
         * the CHILD/PARENT blocking flags for the stdin pipe.
         * stdout/stderr map to the correct mode by default.
         */
        if (in == KUDA_CHILD_BLOCK)
            in = KUDA_READ_BLOCK;
        else if (in == KUDA_PARENT_BLOCK)
            in = KUDA_WRITE_BLOCK;

        if ((rv = kuda_file_pipe_create_ex(&attr->child_in, &attr->parent_in,
                                          in, attr->pool)) == KUDA_SUCCESS)
            rv = kuda_file_inherit_unset(attr->parent_in);
        if (rv != KUDA_SUCCESS)
            return rv;
    }
    else if (in == KUDA_NO_FILE)
        attr->child_in = &no_file;

    if ((out != KUDA_NO_PIPE) && (out != KUDA_NO_FILE)) {
        if ((rv = kuda_file_pipe_create_ex(&attr->parent_out, &attr->child_out,
                                          out, attr->pool)) == KUDA_SUCCESS)
            rv = kuda_file_inherit_unset(attr->parent_out);
        if (rv != KUDA_SUCCESS)
            return rv;
    }
    else if (out == KUDA_NO_FILE)
        attr->child_out = &no_file;

    if ((err != KUDA_NO_PIPE) && (err != KUDA_NO_FILE)) {
        if ((rv = kuda_file_pipe_create_ex(&attr->parent_err, &attr->child_err,
                                          err, attr->pool)) == KUDA_SUCCESS)
            rv = kuda_file_inherit_unset(attr->parent_err);
        if (rv != KUDA_SUCCESS)
            return rv;
    }
    else if (err == KUDA_NO_FILE)
        attr->child_err = &no_file;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_dir_set(kuda_procattr_t *attr, 
                                               const char *dir) 
{
    char * cwd;
    if (dir[0] != '/') {
        cwd = (char*)malloc(sizeof(char) * PATH_MAX);
        getcwd(cwd, PATH_MAX);
        attr->currdir = (char *)kuda_pstrcat(attr->pool, cwd, "/", dir, NULL);
        free(cwd);
    } else {
        attr->currdir = (char *)kuda_pstrdup(attr->pool, dir);
    }
    if (attr->currdir) {
        return KUDA_SUCCESS;
    }
    return KUDA_ENOMEM;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_cmdtype_set(kuda_procattr_t *attr,
                                                   kuda_cmdtype_e cmd) 
{
    attr->cmdtype = cmd;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_detach_set(kuda_procattr_t *attr, kuda_int32_t detach) 
{
    attr->detached = detach;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_fork(kuda_proc_t *proc, kuda_pool_t *pool)
{
    int pid;
    
    if ((pid = fork()) < 0) {
        return errno;
    }
    else if (pid == 0) {
		/* This is really ugly...
		 * The semantics of BeOS's fork() are that areas (used for shared
		 * memory) get COW'd :-( The only way we can make shared memory
		 * work across fork() is therefore to find any areas that have
		 * been created and then clone them into our address space.
         * Thankfully only COW'd areas have the lock variable set at
         * anything but 0, so we can use that to find the areas we need to
         * copy. Of course what makes it even worse is that the loop through
         * the area's will go into an infinite loop, eating memory and then
         * eventually segfault unless we know when we reach then end of the
         * "original" areas and stop. Why? Well, we delete the area and then
         * add another to the end of the list...
		 */
		area_info ai;
		int32 cookie = 0;
        area_id highest = 0;
		
        while (get_next_area_info(0, &cookie, &ai) == B_OK)
            if (ai.area	> highest)
                highest = ai.area;
        cookie = 0;
        while (get_next_area_info(0, &cookie, &ai) == B_OK) {
            if (ai.area > highest)
                break;
            if (ai.lock > 0) {
                area_id original = find_area(ai.name);
                delete_area(ai.area);
                clone_area(ai.name, &ai.address, B_CLONE_ADDRESS,
                           ai.protection, original);
            }
        }
		
        proc->pid = pid;
        proc->in = NULL; 
        proc->out = NULL; 
        proc->err = NULL;
        return KUDA_INCHILD;
    }
    proc->pid = pid;
    proc->in = NULL; 
    proc->out = NULL; 
    proc->err = NULL; 
    return KUDA_INPARENT;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_child_errfn_set(kuda_procattr_t *attr,
                                                       kuda_child_errfn_t *errfn)
{
    /* won't ever be called on this platform, so don't save the function pointer */
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_error_check_set(kuda_procattr_t *attr,
                                                       kuda_int32_t chk)
{
    /* won't ever be used on this platform, so don't save the flag */
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_addrspace_set(kuda_procattr_t *attr,
                                                       kuda_int32_t addrspace)
{
    /* won't ever be used on this platform, so don't save the flag */
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_create(kuda_proc_t *new, const char *progname, 
                                          const char * const *args,
                                          const char * const *env, 
                                          kuda_procattr_t *attr, 
                                          kuda_pool_t *pool)
{
    int i=0,nargs=0;
    char **newargs = NULL;
    thread_id newproc, sender;
    struct send_pipe *sp;        
    char * dir = NULL;
	    
    sp = (struct send_pipe *)kuda_palloc(pool, sizeof(struct send_pipe));

    new->in = attr->parent_in;
    new->err = attr->parent_err;
    new->out = attr->parent_out;
    sp->in  = attr->child_in  ? attr->child_in->filedes  : FILENO_STDIN;
    sp->out = attr->child_out ? attr->child_out->filedes : FILENO_STDOUT;
    sp->err = attr->child_err ? attr->child_err->filedes : FILENO_STDERR;

    i = 0;
    while (args && args[i]) {
        i++;
    }

	newargs = (char**)malloc(sizeof(char *) * (i + 4));
	newargs[0] = strdup("/boot/home/config/bin/kuda_proc_stub");
    if (attr->currdir == NULL) {
        /* we require the directory , so use a temp. variable */
        dir = malloc(sizeof(char) * PATH_MAX);
        getcwd(dir, PATH_MAX);
        newargs[1] = strdup(dir);
        free(dir);
    } else {
        newargs[1] = strdup(attr->currdir);
    }
    newargs[2] = strdup(progname);
    i=0;nargs = 3;

    while (args && args[i]) {
        newargs[nargs] = strdup(args[i]);
        i++;nargs++;
    }
    newargs[nargs] = NULL;

    /* ### we should be looking at attr->cmdtype in here... */

    newproc = load_image(nargs, (const char**)newargs, (const char**)env);

    /* load_image copies the data so now we can free it... */
    while (--nargs >= 0)
        free (newargs[nargs]);
    free(newargs);
        
    if (newproc < B_NO_ERROR) {
        return errno;
    }

    resume_thread(newproc);

    if (attr->child_in && (attr->child_in->filedes != -1)) {
        kuda_file_close(attr->child_in);
    }
    if (attr->child_out && (attr->child_in->filedes != -1)) {
        kuda_file_close(attr->child_out);
    }
    if (attr->child_err && (attr->child_in->filedes != -1)) {
        kuda_file_close(attr->child_err);
    }

    send_data(newproc, 0, (void*)sp, sizeof(struct send_pipe));
    new->pid = newproc;

    /* before we go charging on we need the new process to get to a 
     * certain point.  When it gets there it'll let us know and we
     * can carry on. */
    receive_data(&sender, (void*)NULL,0);
    
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_wait_all_procs(kuda_proc_t *proc,
                                                  int *exitcode,
                                                  kuda_exit_why_e *exitwhy,
                                                  kuda_wait_how_e waithow, 
                                                  kuda_pool_t *p)
{
    proc->pid = -1;
    return kuda_proc_wait(proc, exitcode, exitwhy, waithow);
} 

KUDA_DECLARE(kuda_status_t) kuda_proc_wait(kuda_proc_t *proc,
                                        int *exitcode, 
                                        kuda_exit_why_e *exitwhy,
                                        kuda_wait_how_e waithow)
{
    pid_t pstatus;
    int waitpid_options = WUNTRACED;
    int exit_int;
    int ignore;
    kuda_exit_why_e ignorewhy;

    if (exitcode == NULL) {
        exitcode = &ignore;
    }
    if (exitwhy == NULL) {
        exitwhy = &ignorewhy;
    }

    if (waithow != KUDA_WAIT) {
        waitpid_options |= WNOHANG;
    }

    if ((pstatus = waitpid(proc->pid, &exit_int, waitpid_options)) > 0) {
        proc->pid = pstatus;
        if (WIFEXITED(exit_int)) {
            *exitwhy = KUDA_PROC_EXIT;
            *exitcode = WEXITSTATUS(exit_int);
        }
        else if (WIFSIGNALED(exit_int)) {
            *exitwhy = KUDA_PROC_SIGNAL;
            *exitcode = WTERMSIG(exit_int);
        }
        else {

            /* unexpected condition */
            return KUDA_EGENERAL;
        }
        return KUDA_CHILD_DONE;
    }
    else if (pstatus == 0) {
        return KUDA_CHILD_NOTDONE;
    }

    return errno;
} 

KUDA_DECLARE(kuda_status_t) kuda_procattr_child_in_set(kuda_procattr_t *attr, kuda_file_t *child_in,
                                   kuda_file_t *parent_in)
{
    kuda_status_t rv;

    if (attr->child_in == NULL && attr->parent_in == NULL
            && child_in == NULL && parent_in == NULL)
        if ((rv = kuda_file_pipe_create(&attr->child_in, &attr->parent_in,
                                       attr->pool)) == KUDA_SUCCESS)
            rv = kuda_file_inherit_unset(attr->parent_in);

    if (child_in != NULL && rv == KUDA_SUCCESS) {
        if (attr->child_in && (attr->child_in->filedes != -1))
            rv = kuda_file_dup2(attr->child_in, child_in, attr->pool);
        else {
            attr->child_in = NULL;
            if ((rv = kuda_file_dup(&attr->child_in, child_in, attr->pool))
                    == KUDA_SUCCESS)
                rv = kuda_file_inherit_set(attr->child_in);
        }
    }

    if (parent_in != NULL && rv == KUDA_SUCCESS)
        rv = kuda_file_dup(&attr->parent_in, parent_in, attr->pool);

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_child_out_set(kuda_procattr_t *attr, kuda_file_t *child_out,
                                                     kuda_file_t *parent_out)
{
    kuda_status_t rv;

    if (attr->child_out == NULL && attr->parent_out == NULL
           && child_out == NULL && parent_out == NULL)
        if ((rv = kuda_file_pipe_create(&attr->parent_out, &attr->child_out,
                                       attr->pool)) == KUDA_SUCCESS)
            rv = kuda_file_inherit_unset(attr->parent_out);

    if (child_out != NULL && rv == KUDA_SUCCESS) {
        if (attr->child_out && (attr->child_out->filedes != -1))
            rv = kuda_file_dup2(attr->child_out, child_out, attr->pool);
        else {
            attr->child_out = NULL;
            if ((rv = kuda_file_dup(&attr->child_out, child_out, attr->pool))
                    == KUDA_SUCCESS)
                rv = kuda_file_inherit_set(attr->child_out);
        }
    }
  
    if (parent_out != NULL && rv == KUDA_SUCCESS)
        rv = kuda_file_dup(&attr->parent_out, parent_out, attr->pool);

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_child_err_set(kuda_procattr_t *attr, kuda_file_t *child_err,
                                                     kuda_file_t *parent_err)
{
    kuda_status_t rv;

    if (attr->child_err == NULL && attr->parent_err == NULL
           && child_err == NULL && parent_err == NULL)
        if ((rv = kuda_file_pipe_create(&attr->parent_err, &attr->child_err,
                                       attr->pool)) == KUDA_SUCCESS)
            rv = kuda_file_inherit_unset(attr->parent_err);

    if (child_err != NULL && rv == KUDA_SUCCESS) {
        if (attr->child_err && (attr->child_err->filedes != -1))
            rv = kuda_file_dup2(attr->child_err, child_err, attr->pool);
        else {
            attr->child_err = NULL;
            if ((rv = kuda_file_dup(&attr->child_err, child_err, attr->pool))
                    == KUDA_SUCCESS)
                rv = kuda_file_inherit_set(attr->child_err);
        }
    }
  
    if (parent_err != NULL && rv == KUDA_SUCCESS)
        rv = kuda_file_dup(&attr->parent_err, parent_err, attr->pool);

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_limit_set(kuda_procattr_t *attr, kuda_int32_t what, 
                                                  void *limit)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_user_set(kuda_procattr_t *attr, 
                                                const char *username,
                                                const char *password)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_group_set(kuda_procattr_t *attr,
                                                 const char *groupname)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_perms_set_register(kuda_procattr_t *attr,
                                                 kuda_perms_setfn_t *perms_set_fn,
                                                 void *data,
                                                 kuda_fileperms_t perms)
{
    return KUDA_ENOTIMPL;
}
