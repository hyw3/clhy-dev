/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_threadproc.h"

static struct beos_key key_table[BEOS_MAX_DATAKEYS];
static struct beos_private_data *beos_data[BEOS_MAX_DATAKEYS];
static sem_id lock;

KUDA_DECLARE(kuda_status_t) kuda_threadkey_private_create(kuda_threadkey_t **key,
                                       void (*dest)(void *), kuda_pool_t *pool)
{
    (*key) = (kuda_threadkey_t *)kuda_palloc(pool, sizeof(kuda_threadkey_t));
    if ((*key) == NULL) {
        return KUDA_ENOMEM;
    }

    (*key)->pool = pool;
    	
	acquire_sem(lock);
	for ((*key)->key=0; (*key)->key < BEOS_MAX_DATAKEYS; (*key)->key++){
		if (key_table[(*key)->key].assigned == 0){
			key_table[(*key)->key].assigned = 1;
			key_table[(*key)->key].destructor = dest;
			release_sem(lock);
			return KUDA_SUCCESS;
		}				

	}
	release_sem(lock);
    return KUDA_ENOMEM;
}

KUDA_DECLARE(kuda_status_t) kuda_threadkey_private_get(void **new, kuda_threadkey_t *key)
{
	thread_id tid;
	int i, index=0;
	tid = find_thread(NULL);
	for (i=0;i<BEOS_MAX_DATAKEYS;i++){
		if (beos_data[i]->data){
			/* it's been used */
			if (beos_data[i]->td == tid){
				index = i;
			}
		}
	}
	if (index == 0){
		/* no storage for thread so we can't get anything... */
		return KUDA_ENOMEM;
	}

	if ((key->key < BEOS_MAX_DATAKEYS) && (key_table)){
		acquire_sem(key_table[key->key].lock);
		if (key_table[key->key].count){
			(*new) = (void*)beos_data[index]->data[key->key];
		} else {
			(*new) = NULL;
		}
		release_sem(key_table[key->key].lock);
	} else {
		(*new) = NULL;
	}
	return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_threadkey_private_set(void *priv, kuda_threadkey_t *key)
{
	thread_id tid;
	int i,index = 0, ret = 0;

	tid = find_thread(NULL);	
	for (i=0; i < BEOS_MAX_DATAKEYS; i++){
		if (beos_data[i]->data){
			if (beos_data[i]->td == tid){index = i;}
		}
	}
	if (index==0){
		/* not yet been allocated */
		for (i=0; i< BEOS_MAX_DATAKEYS; i++){
			if (! beos_data[i]->data){
				/* we'll take this one... */
				index = i;
				beos_data[i]->data = (const void **)malloc(sizeof(void *) * BEOS_MAX_DATAKEYS);
				memset((void *)beos_data[i]->data, 0, sizeof(void *) * BEOS_MAX_DATAKEYS);
				beos_data[i]->count = (int)malloc(sizeof(int));
				beos_data[i]->td = (thread_id)malloc(sizeof(thread_id));
				beos_data[i]->td = tid;
			}
		}
	}
	if (index == 0){
		/* we're out of luck.. */
		return KUDA_ENOMEM;
	}
	if ((key->key < BEOS_MAX_DATAKEYS) && (key_table)){
		acquire_sem(key_table[key->key].lock);
		if (key_table[key->key].count){
			if (beos_data[index]->data[key->key] == NULL){
				if (priv != NULL){
					beos_data[index]->count++;
					key_table[key->key].count++;
				}
			} else {
				if (priv == NULL){
					beos_data[index]->count--;
					key_table[key->key].count--;
				}
			}
			beos_data[index]->data[key->key] = priv;
			ret = 1;
		} else {
			ret = 0;
		}
		release_sem(key_table[key->key].lock);
	}
	if (ret)
    	return KUDA_SUCCESS;
	return KUDA_ENOMEM;
}

KUDA_DECLARE(kuda_status_t) kuda_threadkey_private_delete(kuda_threadkey_t *key)
{
	if (key->key < BEOS_MAX_DATAKEYS){
		acquire_sem(key_table[key->key].lock);
		if (key_table[key->key].count == 1){
			key_table[key->key].destructor = NULL;
			key_table[key->key].count = 0;
		}
		release_sem(key_table[key->key].lock);
	} else {
		return KUDA_ENOMEM;
	}
	return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_threadkey_data_get(void **data, const char *key,
                                                 kuda_threadkey_t *threadkey)
{
    return kuda_pool_userdata_get(data, key, threadkey->pool);
}

KUDA_DECLARE(kuda_status_t) kuda_threadkey_data_set(void *data, const char *key,
                                                 kuda_status_t (*cleanup) (void *),
                                                 kuda_threadkey_t *threadkey)
{
    return kuda_pool_userdata_set(data, key, cleanup, threadkey->pool);
}

KUDA_DECLARE(kuda_status_t) kuda_platform_threadkey_get(kuda_platform_threadkey_t *thekey, kuda_threadkey_t *key)
{
    *thekey = key->key;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_threadkey_put(kuda_threadkey_t **key, 
                                               kuda_platform_threadkey_t *thekey, kuda_pool_t *pool)
{
    if (pool == NULL) {
        return KUDA_ENOPOOL;
    }
    if ((*key) == NULL) {
        (*key) = (kuda_threadkey_t *)kuda_pcalloc(pool, sizeof(kuda_threadkey_t));
        (*key)->pool = pool;
    }
    (*key)->key = *thekey;
    return KUDA_SUCCESS;
}
