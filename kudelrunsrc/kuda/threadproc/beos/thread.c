/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_threadproc.h"
#include "kuda_portable.h"

KUDA_DECLARE(kuda_status_t) kuda_threadattr_create(kuda_threadattr_t **new, kuda_pool_t *pool)
{
    (*new) = (kuda_threadattr_t *)kuda_palloc(pool, 
              sizeof(kuda_threadattr_t));

    if ((*new) == NULL) {
        return KUDA_ENOMEM;
    }

    (*new)->pool = pool;
	(*new)->attr = (int32)B_NORMAL_PRIORITY;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_detach_set(kuda_threadattr_t *attr, kuda_int32_t on)
{
	if (on == 1){
		attr->detached = 1;
	} else {
		attr->detached = 0;
	}    
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_detach_get(kuda_threadattr_t *attr)
{
	if (attr->detached == 1){
		return KUDA_DETACH;
	}
	return KUDA_NOTDETACH;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_stacksize_set(kuda_threadattr_t *attr,
                                                       kuda_size_t stacksize)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_guardsize_set(kuda_threadattr_t *attr,
                                                       kuda_size_t size)
{
    return KUDA_ENOTIMPL;
}

static void *dummy_worker(void *opaque)
{
    kuda_thread_t *thd = (kuda_thread_t*)opaque;
    return thd->func(thd, thd->data);
}

KUDA_DECLARE(kuda_status_t) kuda_thread_create(kuda_thread_t **new, kuda_threadattr_t *attr,
                                            kuda_thread_start_t func, void *data,
                                            kuda_pool_t *pool)
{
    int32 temp;
    kuda_status_t stat;
    
    (*new) = (kuda_thread_t *)kuda_palloc(pool, sizeof(kuda_thread_t));
    if ((*new) == NULL) {
        return KUDA_ENOMEM;
    }

    (*new)->data = data;
    (*new)->func = func;
    (*new)->exitval = -1;

    /* First we create the new thread...*/
	if (attr)
	    temp = attr->attr;
	else
	    temp = B_NORMAL_PRIORITY;

    stat = kuda_pool_create(&(*new)->pool, pool);
    if (stat != KUDA_SUCCESS) {
        return stat;
    }

    (*new)->td = spawn_thread((thread_func)dummy_worker, 
                              "kuda thread", 
                              temp, 
                              (*new));

    /* Now we try to run it...*/
    if (resume_thread((*new)->td) == B_NO_ERROR) {
        return KUDA_SUCCESS;
    }
    else {
        return errno;
    } 
}

KUDA_DECLARE(kuda_platform_thread_t) kuda_platform_thread_current(void)
{
    return find_thread(NULL);
}

int kuda_platform_thread_equal(kuda_platform_thread_t tid1, kuda_platform_thread_t tid2)
{
    return tid1 == tid2;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_exit(kuda_thread_t *thd, kuda_status_t retval)
{
    kuda_pool_destroy(thd->pool);
    thd->exitval = retval;
    exit_thread ((status_t)(retval));
    /* This will never be reached... */
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_join(kuda_status_t *retval, kuda_thread_t *thd)
{
    status_t rv = 0, ret;
    ret = wait_for_thread(thd->td, &rv);
    if (ret == B_NO_ERROR) {
        *retval = rv;
        return KUDA_SUCCESS;
    }
    else {
        /* if we've missed the thread's death, did we set an exit value prior
         * to it's demise?  If we did return that.
         */
        if (thd->exitval != -1) {
            *retval = thd->exitval;
            return KUDA_SUCCESS;
        } else 
            return ret;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_thread_detach(kuda_thread_t *thd)
{
	if (suspend_thread(thd->td) == B_NO_ERROR){
        return KUDA_SUCCESS;
    }
    else {
        return errno;
    }
}

void kuda_thread_yield()
{
}

KUDA_DECLARE(kuda_status_t) kuda_thread_data_get(void **data, const char *key, kuda_thread_t *thread)
{
    return kuda_pool_userdata_get(data, key, thread->pool);
}

KUDA_DECLARE(kuda_status_t) kuda_thread_data_set(void *data, const char *key,
                                              kuda_status_t (*cleanup) (void *),
                                              kuda_thread_t *thread)
{
    return kuda_pool_userdata_set(data, key, cleanup, thread->pool);
}

KUDA_DECLARE(kuda_status_t) kuda_platform_thread_get(kuda_platform_thread_t **thethd, kuda_thread_t *thd)
{
    *thethd = &thd->td;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_thread_put(kuda_thread_t **thd, kuda_platform_thread_t *thethd, 
                                            kuda_pool_t *pool)
{
    if (pool == NULL) {
        return KUDA_ENOPOOL;
    }
    if ((*thd) == NULL) {
        (*thd) = (kuda_thread_t *)kuda_pcalloc(pool, sizeof(kuda_thread_t));
        (*thd)->pool = pool;
    }
    (*thd)->td = *thethd;
    return KUDA_SUCCESS;
}

static kuda_status_t thread_once_cleanup(void *vcontrol)
{
    kuda_thread_once_t *control = (kuda_thread_once_t *)vcontrol;

    if (control->sem) {
        release_sem(control->sem);
        delete_sem(control->sem);
    }

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_once_init(kuda_thread_once_t **control,
                                               kuda_pool_t *p)
{
    int rc;
    *control = (kuda_thread_once_t *)kuda_pcalloc(p, sizeof(kuda_thread_once_t));
    (*control)->hit = 0; /* we haven't done it yet... */
    rc = ((*control)->sem = create_sem(1, "thread_once"));
    if (rc < 0)
        return rc;

    kuda_pool_cleanup_register(p, control, thread_once_cleanup, kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_thread_once(kuda_thread_once_t *control, 
                                          void (*func)(void))
{
    if (!control->hit) {
        if (acquire_sem(control->sem) == B_OK) {
            control->hit = 1;
            func();
        }
    }
    return KUDA_SUCCESS;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread)
