/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_portable.h"
#include "kuda_arch_threadproc.h"

#if KUDA_HAS_THREADS

#if KUDA_HAVE_PTHREAD_H
KUDA_DECLARE(kuda_status_t) kuda_threadkey_private_create(kuda_threadkey_t **key,
                                                       void (*dest)(void *),
                                                       kuda_pool_t *pool)
{
    (*key) = (kuda_threadkey_t *)kuda_pcalloc(pool, sizeof(kuda_threadkey_t));

    if ((*key) == NULL) {
        return KUDA_ENOMEM;
    }

    (*key)->pool = pool;

    return pthread_key_create(&(*key)->key, dest);

}

KUDA_DECLARE(kuda_status_t) kuda_threadkey_private_get(void **new,
                                                    kuda_threadkey_t *key)
{
#ifdef PTHREAD_GETSPECIFIC_TAKES_TWO_ARGS
    if (pthread_getspecific(key->key,new))
       *new = NULL;
#else
    (*new) = pthread_getspecific(key->key);
#endif
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_threadkey_private_set(void *priv,
                                                    kuda_threadkey_t *key)
{
    kuda_status_t stat;

    if ((stat = pthread_setspecific(key->key, priv)) == 0) {
        return KUDA_SUCCESS;
    }
    else {
        return stat;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_threadkey_private_delete(kuda_threadkey_t *key)
{
#ifdef HAVE_PTHREAD_KEY_DELETE
    kuda_status_t stat;

    if ((stat = pthread_key_delete(key->key)) == 0) {
        return KUDA_SUCCESS;
    }

    return stat;
#else
    return KUDA_ENOTIMPL;
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_threadkey_data_get(void **data, const char *key,
                                                 kuda_threadkey_t *threadkey)
{
    return kuda_pool_userdata_get(data, key, threadkey->pool);
}

KUDA_DECLARE(kuda_status_t) kuda_threadkey_data_set(void *data, const char *key,
                              kuda_status_t (*cleanup)(void *),
                              kuda_threadkey_t *threadkey)
{
    return kuda_pool_userdata_set(data, key, cleanup, threadkey->pool);
}

KUDA_DECLARE(kuda_status_t) kuda_platform_threadkey_get(kuda_platform_threadkey_t *thekey,
                                               kuda_threadkey_t *key)
{
    *thekey = key->key;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_threadkey_put(kuda_threadkey_t **key,
                                               kuda_platform_threadkey_t *thekey,
                                               kuda_pool_t *pool)
{
    if (pool == NULL) {
        return KUDA_ENOPOOL;
    }

    if ((*key) == NULL) {
        (*key) = (kuda_threadkey_t *)kuda_pcalloc(pool, sizeof(kuda_threadkey_t));
        (*key)->pool = pool;
    }

    (*key)->key = *thekey;
    return KUDA_SUCCESS;
}
#endif /* KUDA_HAVE_PTHREAD_H */
#endif /* KUDA_HAS_THREADS */

#if !KUDA_HAS_THREADS

/* avoid warning for no prototype */
KUDA_DECLARE(kuda_status_t) kuda_platform_threadkey_get(void);

KUDA_DECLARE(kuda_status_t) kuda_platform_threadkey_get(void)
{
    return KUDA_ENOTIMPL;
}

#endif
