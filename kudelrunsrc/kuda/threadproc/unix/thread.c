/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_portable.h"
#include "kuda_arch_threadproc.h"

#if KUDA_HAS_THREADS

#if KUDA_HAVE_PTHREAD_H

/* Destroy the threadattr object */
static kuda_status_t threadattr_cleanup(void *data)
{
    kuda_threadattr_t *attr = data;
    kuda_status_t rv;

    rv = pthread_attr_destroy(&attr->attr);
#ifdef HAVE_ZOS_PTHREADS
    if (rv) {
        rv = errno;
    }
#endif
    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_create(kuda_threadattr_t **new,
                                                kuda_pool_t *pool)
{
    kuda_status_t stat;

    (*new) = kuda_palloc(pool, sizeof(kuda_threadattr_t));
    (*new)->pool = pool;
    stat = pthread_attr_init(&(*new)->attr);

    if (stat == 0) {
        kuda_pool_cleanup_register(pool, *new, threadattr_cleanup,
                                  kuda_pool_cleanup_null);
        return KUDA_SUCCESS;
    }
#ifdef HAVE_ZOS_PTHREADS
    stat = errno;
#endif

    return stat;
}

#if defined(PTHREAD_CREATE_DETACHED)
#define DETACH_ARG(v) ((v) ? PTHREAD_CREATE_DETACHED : PTHREAD_CREATE_JOINABLE)
#else
#define DETACH_ARG(v) ((v) ? 1 : 0)
#endif

KUDA_DECLARE(kuda_status_t) kuda_threadattr_detach_set(kuda_threadattr_t *attr,
                                                    kuda_int32_t on)
{
    kuda_status_t stat;
#ifdef HAVE_ZOS_PTHREADS
    int arg = DETACH_ARG(on);

    if ((stat = pthread_attr_setdetachstate(&attr->attr, &arg)) == 0) {
#else
    if ((stat = pthread_attr_setdetachstate(&attr->attr, 
                                            DETACH_ARG(on))) == 0) {
#endif
        return KUDA_SUCCESS;
    }
    else {
#ifdef HAVE_ZOS_PTHREADS
        stat = errno;
#endif

        return stat;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_detach_get(kuda_threadattr_t *attr)
{
    int state;

#ifdef PTHREAD_ATTR_GETDETACHSTATE_TAKES_ONE_ARG
    state = pthread_attr_getdetachstate(&attr->attr);
#else
    pthread_attr_getdetachstate(&attr->attr, &state);
#endif
    if (state == DETACH_ARG(1))
        return KUDA_DETACH;
    return KUDA_NOTDETACH;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_stacksize_set(kuda_threadattr_t *attr,
                                                       kuda_size_t stacksize)
{
    int stat;

    stat = pthread_attr_setstacksize(&attr->attr, stacksize);
    if (stat == 0) {
        return KUDA_SUCCESS;
    }
#ifdef HAVE_ZOS_PTHREADS
    stat = errno;
#endif

    return stat;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_guardsize_set(kuda_threadattr_t *attr,
                                                       kuda_size_t size)
{
#ifdef HAVE_PTHREAD_ATTR_SETGUARDSIZE
    kuda_status_t rv;

    rv = pthread_attr_setguardsize(&attr->attr, size);
    if (rv == 0) {
        return KUDA_SUCCESS;
    }
#ifdef HAVE_ZOS_PTHREADS
    rv = errno;
#endif
    return rv;
#else
    return KUDA_ENOTIMPL;
#endif
}

static void *dummy_worker(void *opaque)
{
    kuda_thread_t *thread = (kuda_thread_t*)opaque;
    return thread->func(thread, thread->data);
}

KUDA_DECLARE(kuda_status_t) kuda_thread_create(kuda_thread_t **new,
                                            kuda_threadattr_t *attr,
                                            kuda_thread_start_t func,
                                            void *data,
                                            kuda_pool_t *pool)
{
    kuda_status_t stat;
    pthread_attr_t *temp;

    (*new) = (kuda_thread_t *)kuda_pcalloc(pool, sizeof(kuda_thread_t));

    if ((*new) == NULL) {
        return KUDA_ENOMEM;
    }

    (*new)->td = (pthread_t *)kuda_pcalloc(pool, sizeof(pthread_t));

    if ((*new)->td == NULL) {
        return KUDA_ENOMEM;
    }

    (*new)->data = data;
    (*new)->func = func;

    if (attr)
        temp = &attr->attr;
    else
        temp = NULL;

    stat = kuda_pool_create(&(*new)->pool, pool);
    if (stat != KUDA_SUCCESS) {
        return stat;
    }

    if ((stat = pthread_create((*new)->td, temp, dummy_worker, (*new))) == 0) {
        return KUDA_SUCCESS;
    }
    else {
#ifdef HAVE_ZOS_PTHREADS
        stat = errno;
#endif

        return stat;
    }
}

KUDA_DECLARE(kuda_platform_thread_t) kuda_platform_thread_current(void)
{
    return pthread_self();
}

KUDA_DECLARE(int) kuda_platform_thread_equal(kuda_platform_thread_t tid1,
                                     kuda_platform_thread_t tid2)
{
    return pthread_equal(tid1, tid2);
}

KUDA_DECLARE(kuda_status_t) kuda_thread_exit(kuda_thread_t *thd,
                                          kuda_status_t retval)
{
    thd->exitval = retval;
    kuda_pool_destroy(thd->pool);
    pthread_exit(NULL);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_join(kuda_status_t *retval,
                                          kuda_thread_t *thd)
{
    kuda_status_t stat;
    kuda_status_t *thread_stat;

    if ((stat = pthread_join(*thd->td,(void *)&thread_stat)) == 0) {
        *retval = thd->exitval;
        return KUDA_SUCCESS;
    }
    else {
#ifdef HAVE_ZOS_PTHREADS
        stat = errno;
#endif

        return stat;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_thread_detach(kuda_thread_t *thd)
{
    kuda_status_t stat;

#ifdef HAVE_ZOS_PTHREADS
    if ((stat = pthread_detach(thd->td)) == 0) {
#else
    if ((stat = pthread_detach(*thd->td)) == 0) {
#endif

        return KUDA_SUCCESS;
    }
    else {
#ifdef HAVE_ZOS_PTHREADS
        stat = errno;
#endif

        return stat;
    }
}

KUDA_DECLARE(void) kuda_thread_yield(void)
{
#ifdef HAVE_PTHREAD_YIELD
#ifdef HAVE_ZOS_PTHREADS
    pthread_yield(NULL);
#else
    pthread_yield();
#endif /* HAVE_ZOS_PTHREADS */
#else
#ifdef HAVE_SCHED_YIELD
    sched_yield();
#endif
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_thread_data_get(void **data, const char *key,
                                              kuda_thread_t *thread)
{
    return kuda_pool_userdata_get(data, key, thread->pool);
}

KUDA_DECLARE(kuda_status_t) kuda_thread_data_set(void *data, const char *key,
                              kuda_status_t (*cleanup)(void *),
                              kuda_thread_t *thread)
{
    return kuda_pool_userdata_set(data, key, cleanup, thread->pool);
}

KUDA_DECLARE(kuda_status_t) kuda_platform_thread_get(kuda_platform_thread_t **thethd,
                                            kuda_thread_t *thd)
{
    *thethd = thd->td;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_thread_put(kuda_thread_t **thd,
                                            kuda_platform_thread_t *thethd,
                                            kuda_pool_t *pool)
{
    if (pool == NULL) {
        return KUDA_ENOPOOL;
    }

    if ((*thd) == NULL) {
        (*thd) = (kuda_thread_t *)kuda_pcalloc(pool, sizeof(kuda_thread_t));
        (*thd)->pool = pool;
    }

    (*thd)->td = thethd;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_once_init(kuda_thread_once_t **control,
                                               kuda_pool_t *p)
{
    static const pthread_once_t once_init = PTHREAD_ONCE_INIT;

    *control = kuda_palloc(p, sizeof(**control));
    (*control)->once = once_init;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_once(kuda_thread_once_t *control,
                                          void (*func)(void))
{
    return pthread_once(&control->once, func);
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread)

#endif  /* HAVE_PTHREAD_H */
#endif  /* KUDA_HAS_THREADS */

#if !KUDA_HAS_THREADS

/* avoid warning for no prototype */
KUDA_DECLARE(kuda_status_t) kuda_platform_thread_get(void);

KUDA_DECLARE(kuda_status_t) kuda_platform_thread_get(void)
{
    return KUDA_ENOTIMPL;
}

#endif
