/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_portable.h"
#include "kuda_strings.h"
#include "kuda_arch_threadproc.h"

static int thread_count = 0;

kuda_status_t kuda_threadattr_create(kuda_threadattr_t **new,
                                                kuda_pool_t *pool)
{
    (*new) = (kuda_threadattr_t *)kuda_palloc(pool, 
              sizeof(kuda_threadattr_t));

    if ((*new) == NULL) {
        return KUDA_ENOMEM;
    }

    (*new)->pool = pool;
    (*new)->stack_size = KUDA_DEFAULT_STACK_SIZE;
    (*new)->detach = 0;
    (*new)->thread_name = NULL;
    return KUDA_SUCCESS;
}

kuda_status_t kuda_threadattr_detach_set(kuda_threadattr_t *attr,kuda_int32_t on)
{
    attr->detach = on;
    return KUDA_SUCCESS;   
}

kuda_status_t kuda_threadattr_detach_get(kuda_threadattr_t *attr)
{
    if (attr->detach == 1)
        return KUDA_DETACH;
    return KUDA_NOTDETACH;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_stacksize_set(kuda_threadattr_t *attr,
                                                       kuda_size_t stacksize)
{
    attr->stack_size = stacksize;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_guardsize_set(kuda_threadattr_t *attr,
                                                       kuda_size_t size)
{
    return KUDA_ENOTIMPL;
}

static void *dummy_worker(void *opaque)
{
    kuda_thread_t *thd = (kuda_thread_t *)opaque;
    return thd->func(thd, thd->data);
}

kuda_status_t kuda_thread_create(kuda_thread_t **new,
                               kuda_threadattr_t *attr, 
                               kuda_thread_start_t func,
                               void *data,
                               kuda_pool_t *pool)
{
    kuda_status_t stat;
    unsigned long flags = NX_THR_BIND_CONTEXT;
    char threadName[NX_MAX_OBJECT_NAME_LEN+1];
    size_t stack_size = KUDA_DEFAULT_STACK_SIZE;

    if (attr && attr->thread_name) {
        strncpy (threadName, attr->thread_name, NX_MAX_OBJECT_NAME_LEN);
    }
    else {
        sprintf(threadName, "KUDA_thread %04ld", ++thread_count);
    }

    /* An original stack size of 0 will allow NXCreateThread() to
    *   assign a default system stack size.  An original stack
    *   size of less than 0 will assign the KUDA default stack size.
    *   anything else will be taken as is.
    */
    if (attr && (attr->stack_size >= 0)) {
        stack_size = attr->stack_size;
    }
    
    (*new) = (kuda_thread_t *)kuda_palloc(pool, sizeof(kuda_thread_t));

    if ((*new) == NULL) {
        return KUDA_ENOMEM;
    }
    
    (*new)->data = data;
    (*new)->func = func;
    (*new)->thread_name = (char*)kuda_pstrdup(pool, threadName);
    
    stat = kuda_pool_create(&(*new)->pool, pool);
    if (stat != KUDA_SUCCESS) {
        return stat;
    }
    
    if (attr && attr->detach) {
        flags |= NX_THR_DETACHED;
    }
    
    (*new)->ctx = NXContextAlloc(
        /* void(*start_routine)(void *arg) */ (void (*)(void *)) dummy_worker,
        /* void *arg */                       (*new),
        /* int priority */                    NX_PRIO_MED,
        /* size_t stackSize */                stack_size,
        /* unsigned long flags */             NX_CTX_NORMAL,
        /* int *error */                      &stat);

    stat = NXContextSetName(
        /* NXContext_t ctx */  (*new)->ctx,
        /* const char *name */ threadName);

    stat = NXThreadCreate(
        /* NXContext_t context */     (*new)->ctx,
        /* unsigned long flags */     flags,
        /* NXThreadId_t *thread_id */ &(*new)->td);

    if (stat == 0)
        return KUDA_SUCCESS;
        
    return(stat); /* if error */    
}

kuda_platform_thread_t kuda_platform_thread_current()
{
    return NXThreadGetId();
}

int kuda_platform_thread_equal(kuda_platform_thread_t tid1, kuda_platform_thread_t tid2)
{
    return (tid1 == tid2);
}

void kuda_thread_yield()
{
    NXThreadYield();
}

kuda_status_t kuda_thread_exit(kuda_thread_t *thd,
                             kuda_status_t retval)
{
    thd->exitval = retval;
    kuda_pool_destroy(thd->pool);
    NXThreadExit(NULL);
    return KUDA_SUCCESS;
}

kuda_status_t kuda_thread_join(kuda_status_t *retval,
                                          kuda_thread_t *thd)
{
    kuda_status_t  stat;    
    NXThreadId_t dthr;

    if ((stat = NXThreadJoin(thd->td, &dthr, NULL)) == 0) {
        *retval = thd->exitval;
        return KUDA_SUCCESS;
    }
    else {
        return stat;
    }
}

kuda_status_t kuda_thread_detach(kuda_thread_t *thd)
{
    return KUDA_SUCCESS;
}

kuda_status_t kuda_thread_data_get(void **data, const char *key,
                                             kuda_thread_t *thread)
{
    if (thread != NULL) {
            return kuda_pool_userdata_get(data, key, thread->pool);
    }
    else {
        data = NULL;
        return KUDA_ENOTHREAD;
    }
}

kuda_status_t kuda_thread_data_set(void *data, const char *key,
                              kuda_status_t (*cleanup) (void *),
                              kuda_thread_t *thread)
{
    if (thread != NULL) {
       return kuda_pool_userdata_set(data, key, cleanup, thread->pool);
    }
    else {
        data = NULL;
        return KUDA_ENOTHREAD;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_platform_thread_get(kuda_platform_thread_t **thethd,
                                            kuda_thread_t *thd)
{
    if (thd == NULL) {
        return KUDA_ENOTHREAD;
    }
    *thethd = &(thd->td);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_thread_put(kuda_thread_t **thd,
                                            kuda_platform_thread_t *thethd,
                                            kuda_pool_t *pool)
{
    if (pool == NULL) {
        return KUDA_ENOPOOL;
    }
    if ((*thd) == NULL) {
        (*thd) = (kuda_thread_t *)kuda_palloc(pool, sizeof(kuda_thread_t));
        (*thd)->pool = pool;
    }
    (*thd)->td = *thethd;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_once_init(kuda_thread_once_t **control,
                                               kuda_pool_t *p)
{
    (*control) = kuda_pcalloc(p, sizeof(**control));
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_once(kuda_thread_once_t *control,
                                          void (*func)(void))
{
    if (!atomic_xchg(&control->value, 1)) {
        func();
    }
    return KUDA_SUCCESS;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread)


