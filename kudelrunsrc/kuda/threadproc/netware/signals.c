/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_threadproc.h"
#include "kuda_private.h"
#include "kuda_pools.h"
#include "kuda_signal.h"
#include "kuda_strings.h"

#include <assert.h>
#if KUDA_HAS_THREADS && KUDA_HAVE_PTHREAD_H
#include <pthread.h>
#endif

KUDA_DECLARE(kuda_status_t) kuda_proc_kill(kuda_proc_t *proc, int signum)
{
    return KUDA_ENOTIMPL;
}


void kuda_signal_init(kuda_pool_t *pglobal)
{
}

const char *kuda_signal_description_get(int signum)
{
    switch (signum)
    {
    case SIGABRT:
        return "Abort";
    case SIGFPE:
        return "Arithmetic exception";
    case SIGILL:
        return "Illegal instruction";
    case SIGINT:
        return "Interrupt";
    case SIGSEGV:
        return "Segmentation fault";
    case SIGTERM:
        return "Terminated";
    case SIGPOLL:
        return "Pollable event occurred";
    default:
        return "unknown signal (not supported)";
    }
}

static void *signal_thread_func(void *signal_handler)
{
    return NULL;
}

#if (KUDA_HAVE_SIGWAIT || KUDA_HAVE_SIGSUSPEND)
KUDA_DECLARE(kuda_status_t) kuda_setup_signal_thread(void)
{
    return 0;
}
#endif /* (KUDA_HAVE_SIGWAIT || KUDA_HAVE_SIGSUSPEND) */

KUDA_DECLARE(kuda_status_t) kuda_signal_block(int signum)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_signal_unblock(int signum)
{
    return KUDA_ENOTIMPL;
}
