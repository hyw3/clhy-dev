/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_portable.h"
#include "kuda_arch_threadproc.h"

kuda_status_t kuda_threadkey_private_create(kuda_threadkey_t **key, 
                                        void (*dest)(void *), kuda_pool_t *pool) 
{
    kuda_status_t stat;
    
    (*key) = (kuda_threadkey_t *)kuda_palloc(pool, sizeof(kuda_threadkey_t));
	if ((*key) == NULL) {
        return KUDA_ENOMEM;
    }

    (*key)->pool = pool;

    if ((stat = NXKeyCreate(NULL, dest, &(*key)->key)) == 0) {
        return stat;
    }
    return stat;
}

kuda_status_t kuda_threadkey_private_get(void **new, kuda_threadkey_t *key)
{
    kuda_status_t stat;
    
    if ((stat = NXKeyGetValue(key->key, new)) == 0) {
        return KUDA_SUCCESS;
    }
    else {
        return stat;    
    }
}

kuda_status_t kuda_threadkey_private_set(void *priv, kuda_threadkey_t *key)
{
    kuda_status_t stat;
    if ((stat = NXKeySetValue(key->key, priv)) == 0) {
        return KUDA_SUCCESS;
    }
    else {
        return stat;
    }
}

kuda_status_t kuda_threadkey_private_delete(kuda_threadkey_t *key)
{
    kuda_status_t stat;
    if ((stat = NXKeyDelete(key->key)) == 0) {
        return KUDA_SUCCESS; 
    }
    return stat;
}

kuda_status_t kuda_threadkey_data_get(void **data, const char *key, kuda_threadkey_t *threadkey)
{
    return kuda_pool_userdata_get(data, key, threadkey->pool);
}

kuda_status_t kuda_threadkey_data_set(void *data,
                                 const char *key, kuda_status_t (*cleanup) (void *),
                                 kuda_threadkey_t *threadkey)
{
    return kuda_pool_userdata_set(data, key, cleanup, threadkey->pool);
}

kuda_status_t kuda_platform_threadkey_get(kuda_platform_threadkey_t *thekey,
                                               kuda_threadkey_t *key)
{
    thekey = &(key->key);
    return KUDA_SUCCESS;
}

kuda_status_t kuda_platform_threadkey_put(kuda_threadkey_t **key, 
                                kuda_platform_threadkey_t *thekey, kuda_pool_t *pool)
{
    if (pool == NULL) {
        return KUDA_ENOPOOL;
    }
    if ((*key) == NULL) {
        (*key) = (kuda_threadkey_t *)kuda_palloc(pool, sizeof(kuda_threadkey_t));
        (*key)->pool = pool;
    }
    (*key)->key = *thekey;
    return KUDA_SUCCESS;
}           

