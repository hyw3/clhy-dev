/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_threadproc.h"
#include "kuda_arch_file_io.h"
#include "kuda_thread_proc.h"
#include "kuda_signal.h"
#include "kuda_file_io.h"
#include "kuda_general.h"
#if KUDA_HAVE_SIGNAL_H
#include <signal.h>
#endif
#include <string.h>
#if KUDA_HAVE_SYS_WAIT
#include <sys/wait.h>
#endif

/* Windows only really support killing process, but that will do for now. 
 *
 * ### Actually, closing the input handle to the proc should also do fine 
 * for most console apps.  This definitely needs improvement...
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_kill(kuda_proc_t *proc, int signal)
{
    if (proc->hproc != NULL) {
        if (TerminateProcess(proc->hproc, signal) == 0) {
            return kuda_get_platform_error();
        }
        /* On unix, SIGKILL leaves a kuda_proc_wait()able pid lying around, 
         * so we will leave hproc alone until the app calls kuda_proc_wait().
         */
        return KUDA_SUCCESS;
    }
    return KUDA_EPROC_UNKNOWN;
}

void kuda_signal_init(kuda_pool_t *pglobal)
{
}

KUDA_DECLARE(const char *) kuda_signal_description_get(int signum)
{
    return "unknown signal (not supported)";
}

KUDA_DECLARE(kuda_status_t) kuda_signal_block(int signum)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_signal_unblock(int signum)
{
    return KUDA_ENOTIMPL;
}
