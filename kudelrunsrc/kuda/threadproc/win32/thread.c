/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_private.h"
#include "kuda_arch_threadproc.h"
#include "kuda_thread_proc.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_portable.h"
#if KUDA_HAVE_PROCESS_H
#include <process.h>
#endif
#include "kuda_arch_misc.h"   

/* Chosen for us by kuda_initialize */
DWORD tls_kuda_thread = 0;

KUDA_DECLARE(kuda_status_t) kuda_threadattr_create(kuda_threadattr_t **new,
                                                kuda_pool_t *pool)
{
    (*new) = (kuda_threadattr_t *)kuda_palloc(pool, 
              sizeof(kuda_threadattr_t));

    if ((*new) == NULL) {
        return KUDA_ENOMEM;
    }

    (*new)->pool = pool;
    (*new)->detach = 0;
    (*new)->stacksize = 0;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_detach_set(kuda_threadattr_t *attr,
                                                   kuda_int32_t on)
{
    attr->detach = on;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_detach_get(kuda_threadattr_t *attr)
{
    if (attr->detach == 1)
        return KUDA_DETACH;
    return KUDA_NOTDETACH;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_stacksize_set(kuda_threadattr_t *attr,
                                                       kuda_size_t stacksize)
{
    attr->stacksize = stacksize;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_guardsize_set(kuda_threadattr_t *attr,
                                                       kuda_size_t size)
{
    return KUDA_ENOTIMPL;
}

static void *dummy_worker(void *opaque)
{
    kuda_thread_t *thd = (kuda_thread_t *)opaque;
    TlsSetValue(tls_kuda_thread, thd->td);
    return thd->func(thd, thd->data);
}

KUDA_DECLARE(kuda_status_t) kuda_thread_create(kuda_thread_t **new,
                                            kuda_threadattr_t *attr,
                                            kuda_thread_start_t func,
                                            void *data, kuda_pool_t *pool)
{
    kuda_status_t stat;
	unsigned temp;
    HANDLE handle;

    (*new) = (kuda_thread_t *)kuda_palloc(pool, sizeof(kuda_thread_t));

    if ((*new) == NULL) {
        return KUDA_ENOMEM;
    }

    (*new)->data = data;
    (*new)->func = func;
    (*new)->td   = NULL;
    stat = kuda_pool_create(&(*new)->pool, pool);
    if (stat != KUDA_SUCCESS) {
        return stat;
    }

    /* Use 0 for default Thread Stack Size, because that will
     * default the stack to the same size as the calling thread.
     */
#ifndef _WIN32_WCE
    if ((handle = (HANDLE)_beginthreadex(NULL,
                        (DWORD) (attr ? attr->stacksize : 0),
                        (unsigned int (KUDA_THREAD_FUNC *)(void *))dummy_worker,
                        (*new), 0, &temp)) == 0) {
        return KUDA_FROM_PLATFORM_ERROR(_doserrno);
    }
#else
   if ((handle = CreateThread(NULL,
                        attr && attr->stacksize > 0 ? attr->stacksize : 0,
                        (unsigned int (KUDA_THREAD_FUNC *)(void *))dummy_worker,
                        (*new), 0, &temp)) == 0) {
        return kuda_get_platform_error();
    }
#endif
    if (attr && attr->detach) {
        CloseHandle(handle);
    }
    else
        (*new)->td = handle;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_exit(kuda_thread_t *thd,
                                          kuda_status_t retval)
{
    thd->exitval = retval;
    kuda_pool_destroy(thd->pool);
    thd->pool = NULL;
#ifndef _WIN32_WCE
    _endthreadex(0);
#else
    ExitThread(0);
#endif
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_join(kuda_status_t *retval,
                                          kuda_thread_t *thd)
{
    kuda_status_t rv = KUDA_SUCCESS;
    
    if (!thd->td) {
        /* Can not join on detached threads */
        return KUDA_DETACH;
    }
    rv = WaitForSingleObject(thd->td, INFINITE);
    if ( rv == WAIT_OBJECT_0 || rv == WAIT_ABANDONED) {
        /* If the thread_exit has been called */
        if (!thd->pool)
            *retval = thd->exitval;
        else
            rv = KUDA_INCOMPLETE;
    }
    else
        rv = kuda_get_platform_error();
    CloseHandle(thd->td);
    thd->td = NULL;

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_detach(kuda_thread_t *thd)
{
    if (thd->td && CloseHandle(thd->td)) {
        thd->td = NULL;
        return KUDA_SUCCESS;
    }
    else {
        return kuda_get_platform_error();
    }
}

KUDA_DECLARE(void) kuda_thread_yield()
{
    /* SwitchToThread is not supported on Win9x, but since it's
     * primarily a noop (entering time consuming code, therefore
     * providing more critical threads a bit larger timeslice)
     * we won't worry too much if it's not available.
     */
#ifndef _WIN32_WCE
    if (kuda_platform_level >= KUDA_WIN_NT) {
        SwitchToThread();
    }
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_thread_data_get(void **data, const char *key,
                                             kuda_thread_t *thread)
{
    return kuda_pool_userdata_get(data, key, thread->pool);
}

KUDA_DECLARE(kuda_status_t) kuda_thread_data_set(void *data, const char *key,
                                             kuda_status_t (*cleanup) (void *),
                                             kuda_thread_t *thread)
{
    return kuda_pool_userdata_set(data, key, cleanup, thread->pool);
}


KUDA_DECLARE(kuda_platform_thread_t) kuda_platform_thread_current(void)
{
    HANDLE hthread = (HANDLE)TlsGetValue(tls_kuda_thread);
    HANDLE hproc;

    if (hthread) {
        return hthread;
    }
    
    hproc = GetCurrentProcess();
    hthread = GetCurrentThread();
    if (!DuplicateHandle(hproc, hthread, 
                         hproc, &hthread, 0, FALSE, 
                         DUPLICATE_SAME_ACCESS)) {
        return NULL;
    }
    TlsSetValue(tls_kuda_thread, hthread);
    return hthread;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_thread_get(kuda_platform_thread_t **thethd,
                                            kuda_thread_t *thd)
{
    if (thd == NULL) {
        return KUDA_ENOTHREAD;
    }
    *thethd = thd->td;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_thread_put(kuda_thread_t **thd,
                                            kuda_platform_thread_t *thethd,
                                            kuda_pool_t *pool)
{
    if (pool == NULL) {
        return KUDA_ENOPOOL;
    }
    if ((*thd) == NULL) {
        (*thd) = (kuda_thread_t *)kuda_palloc(pool, sizeof(kuda_thread_t));
        (*thd)->pool = pool;
    }
    (*thd)->td = thethd;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_once_init(kuda_thread_once_t **control,
                                               kuda_pool_t *p)
{
    (*control) = kuda_pcalloc(p, sizeof(**control));
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_once(kuda_thread_once_t *control,
                                          void (*func)(void))
{
    if (!InterlockedExchange(&control->value, 1)) {
        func();
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(int) kuda_platform_thread_equal(kuda_platform_thread_t tid1,
                                     kuda_platform_thread_t tid2)
{
    /* Since the only tid's we support our are own, and
     * kuda_platform_thread_current returns the identical handle
     * to the one we created initially, the test is simple.
     */
    return (tid1 == tid2);
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread)
