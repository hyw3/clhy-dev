/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_threadproc.h"
#include "kuda_arch_file_io.h"

#include "kuda_thread_proc.h"
#include "kuda_file_io.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_lib.h"
#include <stdlib.h>
#if KUDA_HAVE_SIGNAL_H
#include <signal.h>
#endif
#include <string.h>
#if KUDA_HAVE_PROCESS_H
#include <process.h>
#endif

/* Heavy on no'ops, here's what we want to pass if there is KUDA_NO_FILE
 * requested for a specific child handle;
 */
static kuda_file_t no_file = { NULL, INVALID_HANDLE_VALUE, };

/* We have very carefully excluded volumes of definitions from the
 * Microsoft Platform SDK, which kill the build time performance.
 * These the sole constants we borrow from WinBase.h and WinUser.h
 */
#ifndef LOGON32_LOGON_NETWORK
#define LOGON32_LOGON_NETWORK 3
#endif

#ifdef _WIN32_WCE
#ifndef DETACHED_PROCESS
#define DETACHED_PROCESS 0
#endif
#ifndef CREATE_UNICODE_ENVIRONMENT
#define CREATE_UNICODE_ENVIRONMENT 0
#endif
#ifndef STARTF_USESHOWWINDOW
#define STARTF_USESHOWWINDOW 0
#endif
#ifndef SW_HIDE
#define SW_HIDE 0
#endif
#endif

/* 
 * some of the ideas expressed herein are based off of Microsoft
 * Knowledge Base article: Q190351
 *
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_create(kuda_procattr_t **new,
                                                  kuda_pool_t *pool)
{
    (*new) = (kuda_procattr_t *)kuda_pcalloc(pool, sizeof(kuda_procattr_t));
    (*new)->pool = pool;
    (*new)->cmdtype = KUDA_PROGRAM;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_io_set(kuda_procattr_t *attr,
                                              kuda_int32_t in, 
                                              kuda_int32_t out,
                                              kuda_int32_t err)
{
    kuda_status_t stat = KUDA_SUCCESS;

    if (in) {
        /* KUDA_CHILD_BLOCK maps to KUDA_WRITE_BLOCK, while
         * KUDA_PARENT_BLOCK maps to KUDA_READ_BLOCK, so transpose 
         * the CHILD/PARENT blocking flags for the stdin pipe.
         * stdout/stderr map to the correct mode by default.
         */
        if (in == KUDA_CHILD_BLOCK)
            in = KUDA_READ_BLOCK;
        else if (in == KUDA_PARENT_BLOCK)
            in = KUDA_WRITE_BLOCK;

        if (in == KUDA_NO_FILE)
            attr->child_in = &no_file;
        else { 
            stat = kuda_file_pipe_create_ex(&attr->child_in, &attr->parent_in,
                                           in, attr->pool);
        }
        if (stat == KUDA_SUCCESS)
            stat = kuda_file_inherit_unset(attr->parent_in);
    }
    if (out && stat == KUDA_SUCCESS) {
        if (out == KUDA_NO_FILE)
            attr->child_out = &no_file;
        else { 
            stat = kuda_file_pipe_create_ex(&attr->parent_out, &attr->child_out,
                                           out, attr->pool);
        }
        if (stat == KUDA_SUCCESS)
            stat = kuda_file_inherit_unset(attr->parent_out);
    }
    if (err && stat == KUDA_SUCCESS) {
        if (err == KUDA_NO_FILE)
            attr->child_err = &no_file;
        else { 
            stat = kuda_file_pipe_create_ex(&attr->parent_err, &attr->child_err,
                                           err, attr->pool);
        }
        if (stat == KUDA_SUCCESS)
            stat = kuda_file_inherit_unset(attr->parent_err);
    }
    return stat;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_child_in_set(kuda_procattr_t *attr, 
                                                  kuda_file_t *child_in, 
                                                  kuda_file_t *parent_in)
{
    kuda_status_t rv = KUDA_SUCCESS;

    if (child_in) {
        if ((attr->child_in == NULL) || (attr->child_in == &no_file))
            rv = kuda_file_dup(&attr->child_in, child_in, attr->pool);
        else
            rv = kuda_file_dup2(attr->child_in, child_in, attr->pool);

        if (rv == KUDA_SUCCESS)
            rv = kuda_file_inherit_set(attr->child_in);
    }

    if (parent_in && rv == KUDA_SUCCESS) {
        if (attr->parent_in == NULL)
            rv = kuda_file_dup(&attr->parent_in, parent_in, attr->pool);
        else
            rv = kuda_file_dup2(attr->parent_in, parent_in, attr->pool);
    }

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_child_out_set(kuda_procattr_t *attr,
                                                   kuda_file_t *child_out,
                                                   kuda_file_t *parent_out)
{
    kuda_status_t rv = KUDA_SUCCESS;

    if (child_out) {
        if ((attr->child_out == NULL) || (attr->child_out == &no_file))
            rv = kuda_file_dup(&attr->child_out, child_out, attr->pool);
        else
            rv = kuda_file_dup2(attr->child_out, child_out, attr->pool);

        if (rv == KUDA_SUCCESS)
            rv = kuda_file_inherit_set(attr->child_out);
    }

    if (parent_out && rv == KUDA_SUCCESS) {
        if (attr->parent_out == NULL)
            rv = kuda_file_dup(&attr->parent_out, parent_out, attr->pool);
        else
            rv = kuda_file_dup2(attr->parent_out, parent_out, attr->pool);
    }

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_child_err_set(kuda_procattr_t *attr,
                                                   kuda_file_t *child_err,
                                                   kuda_file_t *parent_err)
{
    kuda_status_t rv = KUDA_SUCCESS;

    if (child_err) {
        if ((attr->child_err == NULL) || (attr->child_err == &no_file))
            rv = kuda_file_dup(&attr->child_err, child_err, attr->pool);
        else
            rv = kuda_file_dup2(attr->child_err, child_err, attr->pool);

        if (rv == KUDA_SUCCESS)
            rv = kuda_file_inherit_set(attr->child_err);
    }

    if (parent_err && rv == KUDA_SUCCESS) {
        if (attr->parent_err == NULL)
            rv = kuda_file_dup(&attr->parent_err, parent_err, attr->pool);
        else
            rv = kuda_file_dup2(attr->parent_err, parent_err, attr->pool);
    }

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_dir_set(kuda_procattr_t *attr,
                                              const char *dir) 
{
    /* curr dir must be in native format, there are all sorts of bugs in
     * the NT library loading code that flunk the '/' parsing test.
     */
    return kuda_filepath_merge(&attr->currdir, NULL, dir, 
                              KUDA_FILEPATH_NATIVE, attr->pool);
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_cmdtype_set(kuda_procattr_t *attr,
                                                  kuda_cmdtype_e cmd) 
{
    attr->cmdtype = cmd;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_detach_set(kuda_procattr_t *attr,
                                                 kuda_int32_t det) 
{
    attr->detached = det;
    return KUDA_SUCCESS;
}

#ifndef _WIN32_WCE
static kuda_status_t attr_cleanup(void *theattr)
{
    kuda_procattr_t *attr = (kuda_procattr_t *)theattr;    
    if (attr->user_token)
        CloseHandle(attr->user_token);
    attr->user_token = NULL;
    return KUDA_SUCCESS;
}
#endif

KUDA_DECLARE(kuda_status_t) kuda_procattr_user_set(kuda_procattr_t *attr, 
                                                const char *username,
                                                const char *password)
{
#ifdef _WIN32_WCE
    return KUDA_ENOTIMPL;
#else
    HANDLE user;
    kuda_wchar_t *wusername = NULL;
    kuda_wchar_t *wpassword = NULL;
    kuda_status_t rv;
    kuda_size_t len, wlen;

    if (kuda_platform_level >= KUDA_WIN_NT_4) 
    {
        if (attr->user_token) {
            /* Cannot set that twice */
            if (attr->errfn) {
                attr->errfn(attr->pool, 0, 
                            kuda_pstrcat(attr->pool, 
                                        "function called twice" 
                                         " on username: ", username, NULL));
            }
            return KUDA_EINVAL;
        }
        len = strlen(username) + 1;
        wlen = len;
        wusername = kuda_palloc(attr->pool, wlen * sizeof(kuda_wchar_t));
        if ((rv = kuda_conv_utf8_to_ucs2(username, &len, wusername, &wlen))
                   != KUDA_SUCCESS) {
            if (attr->errfn) {
                attr->errfn(attr->pool, rv, 
                            kuda_pstrcat(attr->pool, 
                                        "utf8 to ucs2 conversion failed" 
                                         " on username: ", username, NULL));
            }
            return rv;
        }
        if (password) {
            len = strlen(password) + 1;
            wlen = len;
            wpassword = kuda_palloc(attr->pool, wlen * sizeof(kuda_wchar_t));
            if ((rv = kuda_conv_utf8_to_ucs2(password, &len, wpassword, &wlen))
                       != KUDA_SUCCESS) {
                if (attr->errfn) {
                    attr->errfn(attr->pool, rv, 
                                kuda_pstrcat(attr->pool, 
                                        "utf8 to ucs2 conversion failed" 
                                         " on password: ", password, NULL));
                }
                return rv;
            }
        }
        if (!LogonUserW(wusername, 
                        NULL, 
                        wpassword ? wpassword : L"",
                        LOGON32_LOGON_NETWORK,
                        LOGON32_PROVIDER_DEFAULT,
                        &user)) {
            /* Logon Failed */            
            return kuda_get_platform_error();
        }
        if (wpassword)
            memset(wpassword, 0, wlen * sizeof(kuda_wchar_t));
        /* Get the primary token for user */
        if (!DuplicateTokenEx(user, 
                              TOKEN_QUERY | TOKEN_DUPLICATE | TOKEN_ASSIGN_PRIMARY, 
                              NULL,
                              SecurityImpersonation,
                              TokenPrimary,
                              &(attr->user_token))) {
            /* Failed to duplicate the user token */
            rv = kuda_get_platform_error();
            CloseHandle(user);
            return rv;
        }
        CloseHandle(user);

        attr->sd = kuda_pcalloc(attr->pool, SECURITY_DESCRIPTOR_MIN_LENGTH);
        InitializeSecurityDescriptor(attr->sd, SECURITY_DESCRIPTOR_REVISION);
        SetSecurityDescriptorDacl(attr->sd, -1, 0, 0);
        attr->sa = kuda_palloc(attr->pool, sizeof(SECURITY_ATTRIBUTES));
        attr->sa->nLength = sizeof (SECURITY_ATTRIBUTES);
        attr->sa->lpSecurityDescriptor = attr->sd;
        attr->sa->bInheritHandle = FALSE;

        /* register the cleanup */
        kuda_pool_cleanup_register(attr->pool, (void *)attr,
                                  attr_cleanup,
                                  kuda_pool_cleanup_null);
        return KUDA_SUCCESS;
    }
    else
        return KUDA_ENOTIMPL;
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_group_set(kuda_procattr_t *attr,
                                                 const char *groupname)
{
    /* Always return SUCCESS cause groups are irrelevant */
    return KUDA_SUCCESS;
}

static const char* has_space(const char *str)
{
    const char *ch;
    for (ch = str; *ch; ++ch) {
        if (kuda_isspace(*ch)) {
            return ch;
        }
    }
    return NULL;
}

static char *kuda_caret_escape_args(kuda_pool_t *p, const char *str)
{
    char *cmd;
    unsigned char *d;
    const unsigned char *s;

    cmd = kuda_palloc(p, 2 * strlen(str) + 1);	/* Be safe */
    d = (unsigned char *)cmd;
    s = (const unsigned char *)str;
    for (; *s; ++s) {

        /* 
         * Newlines to Win32/OS2 CreateProcess() are ill advised.
         * Convert them to spaces since they are effectively white
         * space to most applications
         */
	if (*s == '\r' || *s == '\n') {
	    *d++ = ' ';
            continue;
	}

	if (IS_SHCHAR(*s)) {
	    *d++ = '^';
	}
	*d++ = *s;
    }
    *d = '\0';

    return cmd;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_child_errfn_set(kuda_procattr_t *attr,
                                                       kuda_child_errfn_t *errfn)
{
    attr->errfn = errfn;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_error_check_set(kuda_procattr_t *attr,
                                                       kuda_int32_t chk)
{
    attr->errchk = chk;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_addrspace_set(kuda_procattr_t *attr,
                                                       kuda_int32_t addrspace)
{
    /* won't ever be used on this platform, so don't save the flag */
    return KUDA_SUCCESS;
}

#if KUDA_HAS_UNICODE_FS && !defined(_WIN32_WCE)

/* Used only for the NT code path, a critical section is the fastest
 * implementation available.
 */
static CRITICAL_SECTION proc_lock;

static kuda_status_t threadproc_global_cleanup(void *ignored)
{
    DeleteCriticalSection(&proc_lock);
    return KUDA_SUCCESS;
}

/* Called from kuda_initialize, we need a critical section to handle
 * the pipe inheritance on win32.  This will mutex any process create
 * so as we change our inherited pipes, we prevent another process from
 * also inheriting those alternate handles, and prevent the other process
 * from failing to inherit our standard handles.
 */
kuda_status_t kuda_threadproc_init(kuda_pool_t *pool)
{
    IF_WIN_PLATFORM_IS_UNICODE
    {
        InitializeCriticalSection(&proc_lock);
        /* register the cleanup */
        kuda_pool_cleanup_register(pool, &proc_lock,
                                  threadproc_global_cleanup,
                                  kuda_pool_cleanup_null);
    }
    return KUDA_SUCCESS;
}

#else /* !KUDA_HAS_UNICODE_FS || defined(_WIN32_WCE) */

kuda_status_t kuda_threadproc_init(kuda_pool_t *pool)
{
    return KUDA_SUCCESS;
}

#endif

KUDA_DECLARE(kuda_status_t) kuda_proc_create(kuda_proc_t *new,
                                          const char *progname,
                                          const char * const *args,
                                          const char * const *env,
                                          kuda_procattr_t *attr,
                                          kuda_pool_t *pool)
{
    kuda_status_t rv;
    kuda_size_t i;
    const char *argv0;
    char *cmdline;
    char *pEnvBlock;
    PROCESS_INFORMATION pi;
    DWORD dwCreationFlags = 0;

    new->in = attr->parent_in;
    new->out = attr->parent_out;
    new->err = attr->parent_err;

    if (attr->detached) {
        /* If we are creating ourselves detached, then we should hide the
         * window we are starting in.  And we had better redefine our
         * handles for STDIN, STDOUT, and STDERR. Do not set the
         * detached attribute for Win9x. We have found that Win9x does
         * not manage the stdio handles properly when running old 16
         * bit executables if the detached attribute is set.
         */
        if (kuda_platform_level >= KUDA_WIN_NT) {
            /* 
             * XXX DETACHED_PROCESS won't on Win9x at all; on NT/W2K 
             * 16 bit executables fail (MS KB: Q150956)
             */
            dwCreationFlags |= DETACHED_PROCESS;
        }
    }

    /* progname must be unquoted, in native format, as there are all sorts 
     * of bugs in the NT library loader code that fault when parsing '/'.
     * XXX progname must be NULL if this is a 16 bit app running in WOW
     */
    if (progname[0] == '\"') {
        progname = kuda_pstrmemdup(pool, progname + 1, strlen(progname) - 2);
    }

    if (attr->cmdtype == KUDA_PROGRAM || attr->cmdtype == KUDA_PROGRAM_ENV) {
        char *fullpath = NULL;
        if ((rv = kuda_filepath_merge(&fullpath, attr->currdir, progname, 
                                     KUDA_FILEPATH_NATIVE, pool)) != KUDA_SUCCESS) {
            if (attr->errfn) {
                attr->errfn(pool, rv, 
                            kuda_pstrcat(pool, "filepath_merge failed.", 
                                        " currdir: ", attr->currdir, 
                                        " progname: ", progname, NULL));
            }
            return rv;
        }
        progname = fullpath;
    } 
    else {
        /* Do not fail if the path isn't parseable for KUDA_PROGRAM_PATH
         * or KUDA_SHELLCMD.  We only invoke kuda_filepath_merge (with no
         * left hand side expression) in order to correct the path slash
         * delimiters.  But the filename doesn't need to be in the CWD,
         * nor does it need to be a filename at all (it could be a
         * built-in shell command.)
         */
        char *fullpath = NULL;
        if ((rv = kuda_filepath_merge(&fullpath, "", progname, 
                                     KUDA_FILEPATH_NATIVE, pool)) == KUDA_SUCCESS) {
            progname = fullpath;
        }        
    }

    if (has_space(progname)) {
        argv0 = kuda_pstrcat(pool, "\"", progname, "\"", NULL);
    }
    else {
        argv0 = progname;
    }

    /* Handle the args, seperate from argv0 */
    cmdline = "";
    for (i = 1; args && args[i]; ++i) {
        if (has_space(args[i]) || !args[i][0]) {
            cmdline = kuda_pstrcat(pool, cmdline, " \"", args[i], "\"", NULL);
        }
        else {
            cmdline = kuda_pstrcat(pool, cmdline, " ", args[i], NULL);
        }
    }

#ifndef _WIN32_WCE
    if (attr->cmdtype == KUDA_SHELLCMD || attr->cmdtype == KUDA_SHELLCMD_ENV) {
        char *shellcmd = getenv("COMSPEC");
        if (!shellcmd) {
            if (attr->errfn) {
                attr->errfn(pool, KUDA_EINVAL, "COMSPEC envar is not set");
            }
            return KUDA_EINVAL;
        }
        if (shellcmd[0] == '"') {
            progname = kuda_pstrmemdup(pool, shellcmd + 1, strlen(shellcmd) - 2);
        }
        else {
            progname = shellcmd;
            if (has_space(shellcmd)) {
                shellcmd = kuda_pstrcat(pool, "\"", shellcmd, "\"", NULL);
            }
        }
        /* Command.com does not support a quoted command, while cmd.exe demands one.
         */
        i = strlen(progname);
        if (i >= 11 && strcasecmp(progname + i - 11, "command.com") == 0) {
            cmdline = kuda_pstrcat(pool, shellcmd, " /C ", argv0, cmdline, NULL);
        }
        else {
            cmdline = kuda_pstrcat(pool, shellcmd, " /C \"", argv0, cmdline, "\"", NULL);
        }
    } 
    else 
#endif
    {
#if defined(_WIN32_WCE)
        {
#else
        /* Win32 is _different_ than unix.  While unix will find the given
         * program since it's already chdir'ed, Win32 cannot since the parent
         * attempts to open the program with it's own path.
         * ###: This solution isn't much better - it may defeat path searching
         * when the path search was desired.  Open to further discussion.
         */
        i = strlen(progname);
        if (i >= 4 && (strcasecmp(progname + i - 4, ".bat") == 0
                    || strcasecmp(progname + i - 4, ".cmd") == 0))
        {
            char *shellcmd = getenv("COMSPEC");
            if (!shellcmd) {
                if (attr->errfn) {
                    attr->errfn(pool, KUDA_EINVAL, "COMSPEC envar is not set");
                }
                return KUDA_EINVAL;
            }
            if (shellcmd[0] == '"') {
                progname = kuda_pstrmemdup(pool, shellcmd + 1, strlen(shellcmd) - 2);
            }
            else {
                progname = shellcmd;
                if (has_space(shellcmd)) {
                    shellcmd = kuda_pstrcat(pool, "\"", shellcmd, "\"", NULL);
                }
            }
            i = strlen(progname);
            if (i >= 11 && strcasecmp(progname + i - 11, "command.com") == 0) {
                /* XXX: Still insecure - need doubled-quotes on each individual
                 * arg of cmdline.  Suspect we need to postpone cmdline parsing
                 * until this moment in all four code paths, with some flags
                 * to toggle 'which flavor' is needed.
                 */
                cmdline = kuda_pstrcat(pool, shellcmd, " /C ", argv0, cmdline, NULL);
            }
            else {
                /* We must protect the cmdline args from any interpolation - this
                 * is not a shellcmd, and the source of argv[] is untrusted.
                 * Notice we escape ALL the cmdline args, including the quotes
                 * around the individual args themselves.  No sense in allowing
                 * the shift-state to be toggled, and the application will 
                 * not see the caret escapes.
                 */
                cmdline = kuda_caret_escape_args(pool, cmdline);
                /*
                 * Our app name must always be quoted so the quotes surrounding
                 * the entire /c "command args" are unambigious.
                 */
                if (*argv0 != '"') {
                    cmdline = kuda_pstrcat(pool, shellcmd, " /C \"\"", argv0, "\"", cmdline, "\"", NULL);
                }
                else {
                    cmdline = kuda_pstrcat(pool, shellcmd, " /C \"", argv0, cmdline, "\"", NULL);
                }
            }
        }
        else {
#endif
            /* A simple command we are directly invoking.  Do not pass
             * the first arg to CreateProc() for KUDA_PROGRAM_PATH
             * invocation, since it would need to be a literal and
             * complete file path.  That is; "c:\bin\kudatest.exe"
             * would succeed, but "c:\bin\kudatest" or "kudatest.exe"
             * can fail.
             */
            cmdline = kuda_pstrcat(pool, argv0, cmdline, NULL);

            if (attr->cmdtype == KUDA_PROGRAM_PATH) {
                progname = NULL;
            }
        }
    }

    if (!env || attr->cmdtype == KUDA_PROGRAM_ENV ||
        attr->cmdtype == KUDA_SHELLCMD_ENV) {
        pEnvBlock = NULL;
    }
    else {
        kuda_size_t iEnvBlockLen;
        /*
         * Win32's CreateProcess call requires that the environment
         * be passed in an environment block, a null terminated block of
         * null terminated strings.
         */  
        i = 0;
        iEnvBlockLen = 1;
        while (env[i]) {
            iEnvBlockLen += strlen(env[i]) + 1;
            i++;
        }
        if (!i) 
            ++iEnvBlockLen;

#if KUDA_HAS_UNICODE_FS
        IF_WIN_PLATFORM_IS_UNICODE
        {
            kuda_wchar_t *pNext;
            pEnvBlock = (char *)kuda_palloc(pool, iEnvBlockLen * 2);
            dwCreationFlags |= CREATE_UNICODE_ENVIRONMENT;

            i = 0;
            pNext = (kuda_wchar_t*)pEnvBlock;
            while (env[i]) {
                kuda_size_t in = strlen(env[i]) + 1;
                if ((rv = kuda_conv_utf8_to_ucs2(env[i], &in, 
                                                pNext, &iEnvBlockLen)) 
                        != KUDA_SUCCESS) {
                    if (attr->errfn) {
                        attr->errfn(pool, rv, 
                                    kuda_pstrcat(pool, 
                                                "utf8 to ucs2 conversion failed" 
                                                " on this string: ", env[i], NULL));
                    }
                    return rv;
                }
                pNext = wcschr(pNext, L'\0') + 1;
                i++;
            }
	    if (!i)
                *(pNext++) = L'\0';
	    *pNext = L'\0';
        }
#endif /* KUDA_HAS_UNICODE_FS */
#if KUDA_HAS_ANSI_FS
        ELSE_WIN_PLATFORM_IS_ANSI
        {
            char *pNext;
            pEnvBlock = (char *)kuda_palloc(pool, iEnvBlockLen);
    
            i = 0;
            pNext = pEnvBlock;
            while (env[i]) {
                strcpy(pNext, env[i]);
                pNext = strchr(pNext, '\0') + 1;
                i++;
            }
	    if (!i)
                *(pNext++) = '\0';
	    *pNext = '\0';
        }
#endif /* KUDA_HAS_ANSI_FS */
    } 

    new->invoked = cmdline;

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        STARTUPINFOW si;
        DWORD stdin_reset = 0;
        DWORD stdout_reset = 0;
        DWORD stderr_reset = 0;
        kuda_wchar_t *wprg = NULL;
        kuda_wchar_t *wcmd = NULL;
        kuda_wchar_t *wcwd = NULL;

        if (progname) {
            kuda_size_t nprg = strlen(progname) + 1;
            kuda_size_t nwprg = nprg + 6;
            wprg = kuda_palloc(pool, nwprg * sizeof(wprg[0]));
            if ((rv = kuda_conv_utf8_to_ucs2(progname, &nprg, wprg, &nwprg))
                   != KUDA_SUCCESS) {
                if (attr->errfn) {
                    attr->errfn(pool, rv, 
                                kuda_pstrcat(pool, 
                                            "utf8 to ucs2 conversion failed" 
                                            " on progname: ", progname, NULL));
                }
                return rv;
            }
        }

        if (cmdline) {
            kuda_size_t ncmd = strlen(cmdline) + 1;
            kuda_size_t nwcmd = ncmd;
            wcmd = kuda_palloc(pool, nwcmd * sizeof(wcmd[0]));
            if ((rv = kuda_conv_utf8_to_ucs2(cmdline, &ncmd, wcmd, &nwcmd))
                    != KUDA_SUCCESS) {
                if (attr->errfn) {
                    attr->errfn(pool, rv, 
                                kuda_pstrcat(pool, 
                                            "utf8 to ucs2 conversion failed" 
                                            " on cmdline: ", cmdline, NULL));
                }
                return rv;
            }
        }

        if (attr->currdir)
        {
            kuda_size_t ncwd = strlen(attr->currdir) + 1;
            kuda_size_t nwcwd = ncwd;
            wcwd = kuda_palloc(pool, ncwd * sizeof(wcwd[0]));
            if ((rv = kuda_conv_utf8_to_ucs2(attr->currdir, &ncwd, 
                                            wcwd, &nwcwd))
                    != KUDA_SUCCESS) {
                if (attr->errfn) {
                    attr->errfn(pool, rv, 
                                kuda_pstrcat(pool, 
                                            "utf8 to ucs2 conversion failed" 
                                            " on currdir: ", attr->currdir, NULL));
                }
                return rv;
            }
        }

        memset(&si, 0, sizeof(si));
        si.cb = sizeof(si);

        if (attr->detached) {
            si.dwFlags |= STARTF_USESHOWWINDOW;
            si.wShowWindow = SW_HIDE;
        }

#ifndef _WIN32_WCE
        /* LOCK CRITICAL SECTION 
         * before we begin to manipulate the inherited handles
         */
        EnterCriticalSection(&proc_lock);

        if ((attr->child_in && attr->child_in->filehand)
            || (attr->child_out && attr->child_out->filehand)
            || (attr->child_err && attr->child_err->filehand))
        {
            si.dwFlags |= STARTF_USESTDHANDLES;

            si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
            if (attr->child_in && attr->child_in->filehand)
            {
                if (GetHandleInformation(si.hStdInput,
                                         &stdin_reset)
                        && (stdin_reset &= HANDLE_FLAG_INHERIT))
                    SetHandleInformation(si.hStdInput,
                                         HANDLE_FLAG_INHERIT, 0);

                if ( (si.hStdInput = attr->child_in->filehand) 
                                   != INVALID_HANDLE_VALUE )
                    SetHandleInformation(si.hStdInput, HANDLE_FLAG_INHERIT,
                                                       HANDLE_FLAG_INHERIT);
            }
            
            si.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
            if (attr->child_out && attr->child_out->filehand)
            {
                if (GetHandleInformation(si.hStdOutput,
                                         &stdout_reset)
                        && (stdout_reset &= HANDLE_FLAG_INHERIT))
                    SetHandleInformation(si.hStdOutput,
                                         HANDLE_FLAG_INHERIT, 0);

                if ( (si.hStdOutput = attr->child_out->filehand) 
                                   != INVALID_HANDLE_VALUE )
                    SetHandleInformation(si.hStdOutput, HANDLE_FLAG_INHERIT,
                                                        HANDLE_FLAG_INHERIT);
            }

            si.hStdError = GetStdHandle(STD_ERROR_HANDLE);
            if (attr->child_err && attr->child_err->filehand)
            {
                if (GetHandleInformation(si.hStdError,
                                         &stderr_reset)
                        && (stderr_reset &= HANDLE_FLAG_INHERIT))
                    SetHandleInformation(si.hStdError,
                                         HANDLE_FLAG_INHERIT, 0);

                if ( (si.hStdError = attr->child_err->filehand) 
                                   != INVALID_HANDLE_VALUE )
                    SetHandleInformation(si.hStdError, HANDLE_FLAG_INHERIT,
                                                       HANDLE_FLAG_INHERIT);
            }
        }
        if (attr->user_token) {
            /* XXX: for terminal services, handles can't be cannot be
             * inherited across sessions.  This process must be created 
             * in our existing session.  lpDesktop assignment appears
             * to be wrong according to these rules.
             */
            si.lpDesktop = L"Winsta0\\Default";
            if (!ImpersonateLoggedOnUser(attr->user_token)) {
            /* failed to impersonate the logged user */
                rv = kuda_get_platform_error();
                CloseHandle(attr->user_token);
                attr->user_token = NULL;
                LeaveCriticalSection(&proc_lock);
                return rv;
            }
            rv = CreateProcessAsUserW(attr->user_token,
                                      wprg, wcmd,
                                      attr->sa,
                                      NULL,
                                      TRUE,
                                      dwCreationFlags,
                                      pEnvBlock,
                                      wcwd,
                                      &si, &pi);

            RevertToSelf();
        }
        else {
            rv = CreateProcessW(wprg, wcmd,        /* Executable & Command line */
                                NULL, NULL,        /* Proc & thread security attributes */
                                TRUE,              /* Inherit handles */
                                dwCreationFlags,   /* Creation flags */
                                pEnvBlock,         /* Environment block */
                                wcwd,              /* Current directory name */
                                &si, &pi);
        }

        if ((attr->child_in && attr->child_in->filehand)
            || (attr->child_out && attr->child_out->filehand)
            || (attr->child_err && attr->child_err->filehand))
        {
            if (stdin_reset)
                SetHandleInformation(GetStdHandle(STD_INPUT_HANDLE),
                                     stdin_reset, stdin_reset);

            if (stdout_reset)
                SetHandleInformation(GetStdHandle(STD_OUTPUT_HANDLE),
                                     stdout_reset, stdout_reset);

            if (stderr_reset)
                SetHandleInformation(GetStdHandle(STD_ERROR_HANDLE),
                                     stderr_reset, stderr_reset);
        }
        /* RELEASE CRITICAL SECTION 
         * The state of the inherited handles has been restored.
         */
        LeaveCriticalSection(&proc_lock);

#else /* defined(_WIN32_WCE) */
        rv = CreateProcessW(wprg, wcmd,        /* Executable & Command line */
                            NULL, NULL,        /* Proc & thread security attributes */
                            FALSE,             /* must be 0 */
                            dwCreationFlags,   /* Creation flags */
                            NULL,              /* Environment block must be NULL */
                            NULL,              /* Current directory name must be NULL*/
                            NULL,              /* STARTUPINFO not supported */
                            &pi);
#endif
    }
#endif /* KUDA_HAS_UNICODE_FS */
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        STARTUPINFOA si;
        memset(&si, 0, sizeof(si));
        si.cb = sizeof(si);

        if (attr->detached) {
            si.dwFlags |= STARTF_USESHOWWINDOW;
            si.wShowWindow = SW_HIDE;
        }

        if ((attr->child_in && attr->child_in->filehand)
            || (attr->child_out && attr->child_out->filehand)
            || (attr->child_err && attr->child_err->filehand))
        {
            si.dwFlags |= STARTF_USESTDHANDLES;

            si.hStdInput = (attr->child_in) 
                              ? attr->child_in->filehand
                              : GetStdHandle(STD_INPUT_HANDLE);

            si.hStdOutput = (attr->child_out)
                              ? attr->child_out->filehand
                              : GetStdHandle(STD_OUTPUT_HANDLE);

            si.hStdError = (attr->child_err)
                              ? attr->child_err->filehand
                              : GetStdHandle(STD_ERROR_HANDLE);
        }

        rv = CreateProcessA(progname, cmdline, /* Command line */
                            NULL, NULL,        /* Proc & thread security attributes */
                            TRUE,              /* Inherit handles */
                            dwCreationFlags,   /* Creation flags */
                            pEnvBlock,         /* Environment block */
                            attr->currdir,     /* Current directory name */
                            &si, &pi);
    }
#endif /* KUDA_HAS_ANSI_FS */

    /* Check CreateProcess result 
     */
    if (!rv)
        return kuda_get_platform_error();

    /* XXX Orphaned handle warning - no fix due to broken kuda_proc_t api.
     */
    new->hproc = pi.hProcess;
    new->pid = pi.dwProcessId;

    if ((attr->child_in) && (attr->child_in != &no_file)) {
        kuda_file_close(attr->child_in);
    }
    if ((attr->child_out) && (attr->child_out != &no_file)) {
        kuda_file_close(attr->child_out);
    }
    if ((attr->child_err) && (attr->child_err != &no_file)) {
        kuda_file_close(attr->child_err);
    }
    CloseHandle(pi.hThread);

    return KUDA_SUCCESS;
}

static kuda_exit_why_e why_from_exit_code(DWORD exit) {
    /* See WinNT.h STATUS_ACCESS_VIOLATION and family for how
     * this class of failures was determined
     */
    if (((exit & 0xC0000000) == 0xC0000000) 
                    && !(exit & 0x3FFF0000))
        return KUDA_PROC_SIGNAL;
    else
        return KUDA_PROC_EXIT;

    /* ### No way to tell if Dr Watson grabbed a core, AFAICT. */
}

KUDA_DECLARE(kuda_status_t) kuda_proc_wait_all_procs(kuda_proc_t *proc,
                                                  int *exitcode,
                                                  kuda_exit_why_e *exitwhy,
                                                  kuda_wait_how_e waithow,
                                                  kuda_pool_t *p)
{
#if KUDA_HAS_UNICODE_FS
#ifndef _WIN32_WCE
    IF_WIN_PLATFORM_IS_UNICODE
    {
        DWORD  dwId    = GetCurrentProcessId();
        DWORD  i;
        DWORD  nChilds = 0;
        DWORD  nActive = 0;
        HANDLE ps32;
        PROCESSENTRY32W pe32;
        BOOL   bHasMore = FALSE;
        DWORD  dwFlags  = PROCESS_QUERY_INFORMATION;
        kuda_status_t rv = KUDA_EGENERAL;

        if (waithow == KUDA_WAIT)
            dwFlags |= SYNCHRONIZE;
        if (!(ps32 = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0))) {
            return kuda_get_platform_error();
        }
        pe32.dwSize = sizeof(PROCESSENTRY32W);
        if (!Process32FirstW(ps32, &pe32)) {
            if (GetLastError() == ERROR_NO_MORE_FILES)
                return KUDA_EOF;
            else
                return kuda_get_platform_error();
        }
        do {
            DWORD  dwRetval = 0;
            DWORD  nHandles = 0;
            HANDLE hProcess = NULL;
            HANDLE pHandles[MAXIMUM_WAIT_OBJECTS];
            do {
                if (pe32.th32ParentProcessID == dwId) {
                    nChilds++;
                    if ((hProcess = OpenProcess(dwFlags, FALSE,
                                                pe32.th32ProcessID)) != NULL) {
                        if (GetExitCodeProcess(hProcess, &dwRetval)) {
                            if (dwRetval == STILL_ACTIVE) {
                                nActive++;
                                if (waithow == KUDA_WAIT)
                                    pHandles[nHandles++] = hProcess;
                                else
                                    CloseHandle(hProcess);
                            }
                            else {                                
                                /* Process has exited.
                                 * No need to wait for its termination.
                                 */
                                CloseHandle(hProcess);
                                if (exitcode)
                                    *exitcode = dwRetval;
                                if (exitwhy)
                                    *exitwhy  = why_from_exit_code(dwRetval);
                                proc->pid = pe32.th32ProcessID;
                            }
                        }
                        else {
                            /* Unexpected error code.
                             * Cleanup and return;
                             */
                            rv = kuda_get_platform_error();
                            CloseHandle(hProcess);
                            for (i = 0; i < nHandles; i++)
                                CloseHandle(pHandles[i]);
                            return rv;
                        }
                    }
                    else {
                        /* This is our child, so it shouldn't happen
                         * that we cannot open our child's process handle.
                         * However if the child process increased the
                         * security token it might fail.
                         */
                    }
                }
            } while ((bHasMore = Process32NextW(ps32, &pe32)) &&
                     nHandles < MAXIMUM_WAIT_OBJECTS);
            if (nHandles) {
                /* Wait for all collected processes to finish */
                DWORD waitStatus = WaitForMultipleObjects(nHandles, pHandles,
                                                          TRUE, INFINITE);
                for (i = 0; i < nHandles; i++)
                    CloseHandle(pHandles[i]);
                if (waitStatus == WAIT_OBJECT_0) {
                    /* Decrease active count by the number of awaited
                     * processes.
                     */
                    nActive -= nHandles;
                }
                else {
                    /* Broken from the infinite loop */
                    break;
                }
            }
        } while (bHasMore);
        CloseHandle(ps32);
        if (waithow != KUDA_WAIT) {
            if (nChilds && nChilds == nActive) {
                /* All child processes are running */
                rv = KUDA_CHILD_NOTDONE;
                proc->pid = -1;
            }
            else {
                /* proc->pid contains the pid of the
                 * exited processes
                 */
                rv = KUDA_CHILD_DONE;
            }
        }
        if (nActive == 0) {
            rv = KUDA_CHILD_DONE;
            proc->pid = -1;
        }
        return rv;
    }
#endif /* _WIN32_WCE */
#endif /* KUDA_HAS_UNICODE_FS */
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_wait(kuda_proc_t *proc,
                                        int *exitcode, kuda_exit_why_e *exitwhy,
                                        kuda_wait_how_e waithow)
{
    DWORD stat;
    DWORD time;

    if (waithow == KUDA_WAIT)
        time = INFINITE;
    else
        time = 0;

    if ((stat = WaitForSingleObject(proc->hproc, time)) == WAIT_OBJECT_0) {
        if (GetExitCodeProcess(proc->hproc, &stat)) {
            if (exitcode)
                *exitcode = stat;
            if (exitwhy)
                *exitwhy = why_from_exit_code(stat);
            CloseHandle(proc->hproc);
            proc->hproc = NULL;
            return KUDA_CHILD_DONE;
        }
    }
    else if (stat == WAIT_TIMEOUT) {
        return KUDA_CHILD_NOTDONE;
    }
    return kuda_get_platform_error();
}

KUDA_DECLARE(kuda_status_t) kuda_proc_detach(int daemonize)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_perms_set_register(kuda_procattr_t *attr,
                                                 kuda_perms_setfn_t *perms_set_fn,
                                                 void *data,
                                                 kuda_fileperms_t perms)
{
    return KUDA_ENOTIMPL;
}
