/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define INCL_DOSERRORS
#define INCL_DOS
#include "kuda_arch_threadproc.h"
#include "kuda_thread_proc.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_portable.h"
#include "kuda_arch_file_io.h"
#include <stdlib.h>

KUDA_DECLARE(kuda_status_t) kuda_threadattr_create(kuda_threadattr_t **new, kuda_pool_t *pool)
{
    (*new) = (kuda_threadattr_t *)kuda_palloc(pool, sizeof(kuda_threadattr_t));

    if ((*new) == NULL) {
        return KUDA_ENOMEM;
    }

    (*new)->pool = pool;
    (*new)->attr = 0;
    (*new)->stacksize = 0;
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_threadattr_detach_set(kuda_threadattr_t *attr, kuda_int32_t on)
{
    attr->attr |= KUDA_THREADATTR_DETACHED;
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_threadattr_detach_get(kuda_threadattr_t *attr)
{
    return (attr->attr & KUDA_THREADATTR_DETACHED) ? KUDA_DETACH : KUDA_NOTDETACH;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_stacksize_set(kuda_threadattr_t *attr,
                                                       kuda_size_t stacksize)
{
    attr->stacksize = stacksize;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_threadattr_guardsize_set(kuda_threadattr_t *attr,
                                                       kuda_size_t size)
{
    return KUDA_ENOTIMPL;
}

static void kuda_thread_begin(void *arg)
{
  kuda_thread_t *thread = (kuda_thread_t *)arg;
  thread->exitval = thread->func(thread, thread->data);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_create(kuda_thread_t **new, kuda_threadattr_t *attr, 
                                            kuda_thread_start_t func, void *data, 
                                            kuda_pool_t *pool)
{
    kuda_status_t stat;
    kuda_thread_t *thread;
 
    thread = (kuda_thread_t *)kuda_palloc(pool, sizeof(kuda_thread_t));
    *new = thread;

    if (thread == NULL) {
        return KUDA_ENOMEM;
    }

    thread->attr = attr;
    thread->func = func;
    thread->data = data;
    stat = kuda_pool_create(&thread->pool, pool);
    
    if (stat != KUDA_SUCCESS) {
        return stat;
    }

    if (attr == NULL) {
        stat = kuda_threadattr_create(&thread->attr, thread->pool);
        
        if (stat != KUDA_SUCCESS) {
            return stat;
        }
    }

    thread->tid = _beginthread(kuda_thread_begin, NULL, 
                               thread->attr->stacksize > 0 ?
                               thread->attr->stacksize : KUDA_THREAD_STACKSIZE,
                               thread);
        
    if (thread->tid < 0) {
        return errno;
    }

    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_platform_thread_t) kuda_platform_thread_current()
{
    PIB *ppib;
    TIB *ptib;
    DosGetInfoBlocks(&ptib, &ppib);
    return ptib->tib_ptib2->tib2_ultid;
}



KUDA_DECLARE(kuda_status_t) kuda_thread_exit(kuda_thread_t *thd, kuda_status_t retval)
{
    thd->exitval = retval;
    _endthread();
    return -1; /* If we get here something's wrong */
}



KUDA_DECLARE(kuda_status_t) kuda_thread_join(kuda_status_t *retval, kuda_thread_t *thd)
{
    ULONG rc;
    TID waittid = thd->tid;

    if (thd->attr->attr & KUDA_THREADATTR_DETACHED)
        return KUDA_EINVAL;

    rc = DosWaitThread(&waittid, DCWW_WAIT);

    if (rc == ERROR_INVALID_THREADID)
        rc = 0; /* Thread had already terminated */

    *retval = thd->exitval;
    return KUDA_OS2_STATUS(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_detach(kuda_thread_t *thd)
{
    thd->attr->attr |= KUDA_THREADATTR_DETACHED;
    return KUDA_SUCCESS;
}



void kuda_thread_yield()
{
    DosSleep(0);
}



KUDA_DECLARE(kuda_status_t) kuda_platform_thread_get(kuda_platform_thread_t **thethd, kuda_thread_t *thd)
{
    *thethd = &thd->tid;
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_platform_thread_put(kuda_thread_t **thd, kuda_platform_thread_t *thethd, 
                                            kuda_pool_t *pool)
{
    if ((*thd) == NULL) {
        (*thd) = (kuda_thread_t *)kuda_pcalloc(pool, sizeof(kuda_thread_t));
        (*thd)->pool = pool;
    }
    (*thd)->tid = *thethd;
    return KUDA_SUCCESS;
}



int kuda_platform_thread_equal(kuda_platform_thread_t tid1, kuda_platform_thread_t tid2)
{
    return tid1 == tid2;
}



KUDA_DECLARE(kuda_status_t) kuda_thread_data_get(void **data, const char *key, kuda_thread_t *thread)
{
    return kuda_pool_userdata_get(data, key, thread->pool);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_data_set(void *data, const char *key,
                                              kuda_status_t (*cleanup) (void *),
                                              kuda_thread_t *thread)
{
    return kuda_pool_userdata_set(data, key, cleanup, thread->pool);
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread)



static kuda_status_t thread_once_cleanup(void *vcontrol)
{
    kuda_thread_once_t *control = (kuda_thread_once_t *)vcontrol;

    if (control->sem) {
        DosCloseEventSem(control->sem);
    }

    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_thread_once_init(kuda_thread_once_t **control,
                                               kuda_pool_t *p)
{
    ULONG rc;
    *control = (kuda_thread_once_t *)kuda_pcalloc(p, sizeof(kuda_thread_once_t));
    rc = DosCreateEventSem(NULL, &(*control)->sem, 0, TRUE);
    kuda_pool_cleanup_register(p, control, thread_once_cleanup, kuda_pool_cleanup_null);
    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_once(kuda_thread_once_t *control, 
                                          void (*func)(void))
{
    if (!control->hit) {
        ULONG count, rc;
        rc = DosResetEventSem(control->sem, &count);

        if (rc == 0 && count) {
            control->hit = 1;
            func();
        }
    }

    return KUDA_SUCCESS;
}
