/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define INCL_DOS
#define INCL_DOSERRORS

#include "kuda_arch_threadproc.h"
#include "kuda_arch_file_io.h"
#include "kuda_private.h"
#include "kuda_thread_proc.h"
#include "kuda_file_io.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_portable.h"
#include "kuda_strings.h"
#include "kuda_signal.h"
#include <signal.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <process.h>
#include <stdlib.h>

/* Heavy on no'ops, here's what we want to pass if there is KUDA_NO_FILE
 * requested for a specific child handle;
 */
static kuda_file_t no_file = { NULL, -1, };

KUDA_DECLARE(kuda_status_t) kuda_procattr_create(kuda_procattr_t **new, kuda_pool_t *pool)
{
    (*new) = (kuda_procattr_t *)kuda_palloc(pool, 
              sizeof(kuda_procattr_t));

    if ((*new) == NULL) {
        return KUDA_ENOMEM;
    }
    (*new)->pool = pool;
    (*new)->parent_in = NULL;
    (*new)->child_in = NULL;
    (*new)->parent_out = NULL;
    (*new)->child_out = NULL;
    (*new)->parent_err = NULL;
    (*new)->child_err = NULL;
    (*new)->currdir = NULL; 
    (*new)->cmdtype = KUDA_PROGRAM;
    (*new)->detached = FALSE;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_io_set(kuda_procattr_t *attr,
                                              kuda_int32_t in,
                                              kuda_int32_t out,
                                              kuda_int32_t err)
{
    kuda_status_t rv;

    if ((in != KUDA_NO_PIPE) && (in != KUDA_NO_FILE)) {
        /* KUDA_CHILD_BLOCK maps to KUDA_WRITE_BLOCK, while
         * KUDA_PARENT_BLOCK maps to KUDA_READ_BLOCK, so transpose 
         * the CHILD/PARENT blocking flags for the stdin pipe.
         * stdout/stderr map to the correct mode by default.
         */
        if (in == KUDA_CHILD_BLOCK)
            in = KUDA_READ_BLOCK;
        else if (in == KUDA_PARENT_BLOCK)
            in = KUDA_WRITE_BLOCK;

        if ((rv = kuda_file_pipe_create_ex(&attr->child_in, &attr->parent_in,
                                          in, attr->pool)) == KUDA_SUCCESS)
            rv = kuda_file_inherit_unset(attr->parent_in);
        if (rv != KUDA_SUCCESS)
            return rv;
    }
    else if (in == KUDA_NO_FILE)
        attr->child_in = &no_file;

    if ((out != KUDA_NO_PIPE) && (out != KUDA_NO_FILE)) {
        if ((rv = kuda_file_pipe_create_ex(&attr->parent_out, &attr->child_out,
                                          out, attr->pool)) == KUDA_SUCCESS)
            rv = kuda_file_inherit_unset(attr->parent_out);
        if (rv != KUDA_SUCCESS)
            return rv;
    }
    else if (out == KUDA_NO_FILE)
        attr->child_out = &no_file;

    if ((err != KUDA_NO_PIPE) && (err != KUDA_NO_FILE)) {
        if ((rv = kuda_file_pipe_create_ex(&attr->parent_err, &attr->child_err,
                                          err, attr->pool)) == KUDA_SUCCESS)
            rv = kuda_file_inherit_unset(attr->parent_err);
        if (rv != KUDA_SUCCESS)
            return rv;
    }
    else if (err == KUDA_NO_FILE)
        attr->child_err = &no_file;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_child_in_set(kuda_procattr_t *attr, kuda_file_t *child_in,
                                   kuda_file_t *parent_in)
{
    kuda_status_t rv;

    if (attr->child_in == NULL && attr->parent_in == NULL
            && child_in == NULL && parent_in == NULL)
        if ((rv = kuda_file_pipe_create(&attr->child_in, &attr->parent_in,
                                       attr->pool)) == KUDA_SUCCESS)
            rv = kuda_file_inherit_unset(attr->parent_in);

    if (child_in != NULL && rv == KUDA_SUCCESS) {
        if (attr->child_in && (attr->child_in->filedes != -1))
            rv = kuda_file_dup2(attr->child_in, child_in, attr->pool);
        else {
            attr->child_in = NULL;
            if ((rv = kuda_file_dup(&attr->child_in, child_in, attr->pool))
                    == KUDA_SUCCESS)
                rv = kuda_file_inherit_set(attr->child_in);
        }
    }

    if (parent_in != NULL && rv == KUDA_SUCCESS) {
        rv = kuda_file_dup(&attr->parent_in, parent_in, attr->pool);
    }

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_child_out_set(kuda_procattr_t *attr, kuda_file_t *child_out,
                                                     kuda_file_t *parent_out)
{
    kuda_status_t rv;

    if (attr->child_out == NULL && attr->parent_out == NULL
           && child_out == NULL && parent_out == NULL)
        if ((rv = kuda_file_pipe_create(&attr->parent_out, &attr->child_out,
                                       attr->pool)) == KUDA_SUCCESS)
            rv = kuda_file_inherit_unset(attr->parent_out);

    if (child_out != NULL && rv == KUDA_SUCCESS) {
        if (attr->child_out && (attr->child_out->filedes != -1))
            rv = kuda_file_dup2(attr->child_out, child_out, attr->pool);
        else {
            attr->child_out = NULL;
            if ((rv = kuda_file_dup(&attr->child_out, child_out, attr->pool))
                    == KUDA_SUCCESS)
                rv = kuda_file_inherit_set(attr->child_out);
        }
    }
  
    if (parent_out != NULL && rv == KUDA_SUCCESS) {
        rv = kuda_file_dup(&attr->parent_out, parent_out, attr->pool);
    }

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_child_err_set(kuda_procattr_t *attr, kuda_file_t *child_err,
                                                     kuda_file_t *parent_err)
{
    kuda_status_t rv;

    if (attr->child_err == NULL && attr->parent_err == NULL
           && child_err == NULL && parent_err == NULL)
        if ((rv = kuda_file_pipe_create(&attr->parent_err, &attr->child_err,
                                       attr->pool)) == KUDA_SUCCESS)
            rv = kuda_file_inherit_unset(attr->parent_err);

    if (child_err != NULL && rv == KUDA_SUCCESS) {
        if (attr->child_err && (attr->child_err->filedes != -1))
            rv = kuda_file_dup2(attr->child_err, child_err, attr->pool);
        else {
            attr->child_err = NULL;
            if ((rv = kuda_file_dup(&attr->child_err, child_err, attr->pool))
                    == KUDA_SUCCESS)
                rv = kuda_file_inherit_set(attr->child_err);
        }
    }
  
    if (parent_err != NULL && rv == KUDA_SUCCESS) {
        rv = kuda_file_dup(&attr->parent_err, parent_err, attr->pool);
    }

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_dir_set(kuda_procattr_t *attr, const char *dir)
{
    attr->currdir = kuda_pstrdup(attr->pool, dir);
    if (attr->currdir) {
        return KUDA_SUCCESS;
    }
    return KUDA_ENOMEM;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_cmdtype_set(kuda_procattr_t *attr,
                                                   kuda_cmdtype_e cmd) 
{
    attr->cmdtype = cmd;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_detach_set(kuda_procattr_t *attr, kuda_int32_t detach) 
{
    attr->detached = detach;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_fork(kuda_proc_t *proc, kuda_pool_t *pool)
{
    int pid;
    
    if ((pid = fork()) < 0) {
        return errno;
    }
    else if (pid == 0) {
        proc->pid = pid;
        proc->in = NULL; 
        proc->out = NULL; 
        proc->err = NULL; 
        return KUDA_INCHILD;
    }
    proc->pid = pid;
    proc->in = NULL; 
    proc->out = NULL; 
    proc->err = NULL; 
    return KUDA_INPARENT;
}



/* quotes in the string are doubled up.
 * Used to escape quotes in args passed to OS2's cmd.exe
 */
static char *double_quotes(kuda_pool_t *pool, const char *str)
{
    int num_quotes = 0;
    int len = 0;
    char *quote_doubled_str, *dest;
    
    while (str[len]) {
        num_quotes += str[len++] == '\"';
    }
    
    quote_doubled_str = kuda_palloc(pool, len + num_quotes + 1);
    dest = quote_doubled_str;
    
    while (*str) {
        if (*str == '\"')
            *(dest++) = '\"';
        *(dest++) = *(str++);
    }
    
    *dest = 0;
    return quote_doubled_str;
}



KUDA_DECLARE(kuda_status_t) kuda_procattr_child_errfn_set(kuda_procattr_t *attr,
                                                       kuda_child_errfn_t *errfn)
{
    /* won't ever be called on this platform, so don't save the function pointer */
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_procattr_error_check_set(kuda_procattr_t *attr,
                                                       kuda_int32_t chk)
{
    /* won't ever be used on this platform, so don't save the flag */
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_addrspace_set(kuda_procattr_t *attr,
                                                       kuda_int32_t addrspace)
{
    /* won't ever be used on this platform, so don't save the flag */
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_proc_create(kuda_proc_t *proc, const char *progname,
                                          const char * const *args,
                                          const char * const *env,
                                          kuda_procattr_t *attr, kuda_pool_t *pool)
{
    int i, arg, numargs, cmdlen;
    kuda_status_t status;
    const char **newargs;
    char savedir[300];
    HFILE save_in, save_out, save_err, dup;
    int criticalsection = FALSE;
    char *extension, *newprogname, *extra_arg = NULL, *cmdline, *cmdline_pos;
    char interpreter[1024];
    char error_object[260];
    kuda_file_t *progfile;
    int env_len, e;
    char *env_block, *env_block_pos;
    RESULTCODES rescodes;

    proc->in = attr->parent_in;
    proc->err = attr->parent_err;
    proc->out = attr->parent_out;

    /* Prevent other threads from running while these process-wide resources are modified */
    if (attr->child_in || attr->child_out || attr->child_err || attr->currdir) {
        criticalsection = TRUE;
        DosEnterCritSec();
    }

    if (attr->child_in) {
        save_in = -1;
        DosDupHandle(STDIN_FILENO, &save_in);
        dup = STDIN_FILENO;
        if (attr->child_in->filedes == -1)
            DosClose(dup);
        else
            DosDupHandle(attr->child_in->filedes, &dup);
    }
    
    if (attr->child_out) {
        save_out = -1;
        DosDupHandle(STDOUT_FILENO, &save_out);
        dup = STDOUT_FILENO;
        if (attr->child_out->filedes == -1)
            DosClose(dup);
        else
            DosDupHandle(attr->child_out->filedes, &dup);
    }
    
    if (attr->child_err) {
        save_err = -1;
        DosDupHandle(STDERR_FILENO, &save_err);
        dup = STDERR_FILENO;
        if (attr->child_err->filedes == -1)
            DosClose(dup);
        else
            DosDupHandle(attr->child_err->filedes, &dup);
    }

    kuda_signal(SIGCHLD, SIG_DFL); /*not sure if this is needed or not */

    if (attr->currdir != NULL) {
        _getcwd2(savedir, sizeof(savedir));
        
        if (_chdir2(attr->currdir) < 0) {
            if (criticalsection)
                DosExitCritSec();
            return errno;
        }
    }

    interpreter[0] = 0;
    extension = strrchr(progname, '.');

    if (extension == NULL || strchr(extension, '/') || strchr(extension, '\\'))
        extension = "";

    /* ### how to handle KUDA_PROGRAM_ENV and KUDA_PROGRAM_PATH? */

    if (attr->cmdtype == KUDA_SHELLCMD ||
        attr->cmdtype == KUDA_SHELLCMD_ENV ||
        strcasecmp(extension, ".cmd") == 0) {
        strcpy(interpreter, "#!" SHELL_PATH);
        extra_arg = "/C";
    } else if (stricmp(extension, ".exe") != 0) {
        status = kuda_file_open(&progfile, progname, KUDA_READ|KUDA_BUFFERED, 0, pool);

        if (status != KUDA_SUCCESS && KUDA_STATUS_IS_ENOENT(status)) {
            progname = kuda_pstrcat(pool, progname, ".exe", NULL);
        }

        if (status == KUDA_SUCCESS) {
            status = kuda_file_gets(interpreter, sizeof(interpreter), progfile);

            if (status == KUDA_SUCCESS) {
                if (interpreter[0] == '#' && interpreter[1] == '!') {
                    /* delete CR/LF & any other whitespace off the end */
                    int end = strlen(interpreter) - 1;

                    while (end >= 0 && kuda_isspace(interpreter[end])) {
                        interpreter[end] = '\0';
                        end--;
                    }

                    if (interpreter[2] != '/' && interpreter[2] != '\\' && interpreter[3] != ':') {
                        char buffer[300];

                        if (DosSearchPath(SEARCH_ENVIRONMENT, "PATH", interpreter+2, buffer, sizeof(buffer)) == 0) {
                            strcpy(interpreter+2, buffer);
                        } else {
                            strcat(interpreter, ".exe");
                            if (DosSearchPath(SEARCH_ENVIRONMENT, "PATH", interpreter+2, buffer, sizeof(buffer)) == 0) {
                                strcpy(interpreter+2, buffer);
                            }
                        }
                    }
                } else {
                    interpreter[0] = 0;
                }
            }

            kuda_file_close(progfile);
        }
    }

    i = 0;

    while (args && args[i]) {
        i++;
    }

    newargs = (const char **)kuda_palloc(pool, sizeof (char *) * (i + 4));
    numargs = 0;

    if (interpreter[0])
        newargs[numargs++] = interpreter + 2;
    if (extra_arg)
        newargs[numargs++] = "/c";

    newargs[numargs++] = newprogname = kuda_pstrdup(pool, progname);
    arg = 1;

    while (args && args[arg]) {
        newargs[numargs++] = args[arg++];
    }

    newargs[numargs] = NULL;

    for (i=0; newprogname[i]; i++)
        if (newprogname[i] == '/')
            newprogname[i] = '\\';

    cmdlen = 0;

    for (i=0; i<numargs; i++)
        cmdlen += strlen(newargs[i]) + 3;

    cmdline = kuda_palloc(pool, cmdlen + 2);
    cmdline_pos = cmdline;

    for (i=0; i<numargs; i++) {
        const char *a = newargs[i];

        if (strpbrk(a, "&|<>\" "))
            a = kuda_pstrcat(pool, "\"", double_quotes(pool, a), "\"", NULL);

        if (i)
            *(cmdline_pos++) = ' ';

        strcpy(cmdline_pos, a);
        cmdline_pos += strlen(cmdline_pos);
    }

    *(++cmdline_pos) = 0; /* Add required second terminator */
    cmdline_pos = strchr(cmdline, ' ');

    if (cmdline_pos) {
        *cmdline_pos = 0;
        cmdline_pos++;
    }

    /* Create environment block from list of envariables */
    if (env) {
        for (env_len=1, e=0; env[e]; e++)
            env_len += strlen(env[e]) + 1;

        env_block = kuda_palloc(pool, env_len);
        env_block_pos = env_block;

        for (e=0; env[e]; e++) {
            strcpy(env_block_pos, env[e]);
            env_block_pos += strlen(env_block_pos) + 1;
        }

        *env_block_pos = 0; /* environment block is terminated by a double null */
    } else
        env_block = NULL;

    status = DosExecPgm(error_object, sizeof(error_object),
                        attr->detached ? EXEC_BACKGROUND : EXEC_ASYNCRESULT,
                        cmdline, env_block, &rescodes, cmdline);

    proc->pid = rescodes.codeTerminate;

    if (attr->currdir != NULL) {
        chdir(savedir);
    }

    if (attr->child_in) {
        if (attr->child_in->filedes != -1) {
            kuda_file_close(attr->child_in);
        }

        dup = STDIN_FILENO;
        DosDupHandle(save_in, &dup);
        DosClose(save_in);
    }
    
    if (attr->child_out) {
        if  (attr->child_out->filedes != -1) {
            kuda_file_close(attr->child_out);
        }

        dup = STDOUT_FILENO;
        DosDupHandle(save_out, &dup);
        DosClose(save_out);
    }
    
    if (attr->child_err) {
        if (attr->child_err->filedes != -1) {
            kuda_file_close(attr->child_err);
        }

        dup = STDERR_FILENO;
        DosDupHandle(save_err, &dup);
        DosClose(save_err);
    }

    if (criticalsection)
        DosExitCritSec();

    return status;
}



static void proces_result_codes(RESULTCODES codes, 
                                int *exitcode, 
                                kuda_exit_why_e *exitwhy)
{
    int result = 0;
    kuda_exit_why_e why = KUDA_PROC_EXIT;

    switch (codes.codeTerminate) {
    case TC_EXIT:        /* Normal exit */
        why = KUDA_PROC_EXIT;
        result = codes.codeResult;
        break;

    case TC_HARDERROR:   /* Hard error halt */
        why = KUDA_PROC_SIGNAL;
        result = SIGSYS;
        break;

    case TC_KILLPROCESS: /* Was killed by a DosKillProcess() */
        why = KUDA_PROC_SIGNAL;
        result = SIGKILL;
        break;

    case TC_TRAP:        /* TRAP in 16 bit code */
    case TC_EXCEPTION:   /* Threw an exception (32 bit code) */
        why = KUDA_PROC_SIGNAL;

        switch (codes.codeResult | XCPT_FATAL_EXCEPTION) {
        case XCPT_ACCESS_VIOLATION:
            result = SIGSEGV;
            break;

        case XCPT_ILLEGAL_INSTRUCTION:
            result = SIGILL;
            break;

        case XCPT_FLOAT_DIVIDE_BY_ZERO:
        case XCPT_INTEGER_DIVIDE_BY_ZERO:
            result = SIGFPE;
            break;

        default:
            result = codes.codeResult;
            break;
        }
    }

    if (exitcode) {
        *exitcode = result;
    }

    if (exitwhy) {
        *exitwhy = why;
    }
}



KUDA_DECLARE(kuda_status_t) kuda_proc_wait_all_procs(kuda_proc_t *proc,
                                                  int *exitcode,
                                                  kuda_exit_why_e *exitwhy,
                                                  kuda_wait_how_e waithow,
                                                  kuda_pool_t *p)
{
    RESULTCODES codes;
    ULONG rc;
    PID pid;

    rc = DosWaitChild(DCWA_PROCESSTREE, waithow == KUDA_WAIT ? DCWW_WAIT : DCWW_NOWAIT, &codes, &pid, 0);

    if (rc == 0) {
        proc->pid = pid;
        proces_result_codes(codes, exitcode, exitwhy);
        return KUDA_CHILD_DONE;
    } else if (rc == ERROR_CHILD_NOT_COMPLETE) {
        return KUDA_CHILD_NOTDONE;
    }

    return KUDA_OS2_STATUS(rc);
} 



KUDA_DECLARE(kuda_status_t) kuda_proc_wait(kuda_proc_t *proc,
                                        int *exitcode, kuda_exit_why_e *exitwhy,
                                        kuda_wait_how_e waithow)
{
    RESULTCODES codes;
    ULONG rc;
    PID pid;
    rc = DosWaitChild(DCWA_PROCESS, waithow == KUDA_WAIT ? DCWW_WAIT : DCWW_NOWAIT, &codes, &pid, proc->pid);

    if (rc == 0) {
        proces_result_codes(codes, exitcode, exitwhy);
        return KUDA_CHILD_DONE;
    } else if (rc == ERROR_CHILD_NOT_COMPLETE) {
        return KUDA_CHILD_NOTDONE;
    }

    return KUDA_OS2_STATUS(rc);
} 



KUDA_DECLARE(kuda_status_t) kuda_proc_detach(int daemonize)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_user_set(kuda_procattr_t *attr, 
                                                const char *username,
                                                const char *password)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_group_set(kuda_procattr_t *attr,
                                                 const char *groupname)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_procattr_perms_set_register(kuda_procattr_t *attr,
                                                 kuda_perms_setfn_t *perms_set_fn,
                                                 void *data,
                                                 kuda_fileperms_t perms)
{
    return KUDA_ENOTIMPL;
}
