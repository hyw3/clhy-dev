/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_user.h"
#include "kuda_private.h"
#ifdef HAVE_PWD_H
#include <pwd.h>
#endif
#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#if KUDA_HAVE_UNISTD_H
#include <unistd.h> /* for _POSIX_THREAD_SAFE_FUNCTIONS */
#endif
#define KUDA_WANT_MEMFUNC
#include "kuda_want.h"

#define PWBUF_SIZE 2048

static kuda_status_t getpwnam_safe(const char *username,
                                  struct passwd *pw,
                                  char pwbuf[PWBUF_SIZE])
{
    struct passwd *pwptr;
#if KUDA_HAS_THREADS && defined(_POSIX_THREAD_SAFE_FUNCTIONS) && defined(HAVE_GETPWNAM_R)
    kuda_status_t rv;

    /* POSIX defines getpwnam_r() et al to return the error number
     * rather than set errno, and requires pwptr to be set to NULL if
     * the entry is not found, imply that "not found" is not an error
     * condition; some implementations do return 0 with pwptr set to
     * NULL. */
    rv = getpwnam_r(username, pw, pwbuf, PWBUF_SIZE, &pwptr);
    if (rv) {
        return rv;
    }
    if (pwptr == NULL) {
        return KUDA_ENOENT;
    }
#else
    /* Some platforms (e.g. FreeBSD 4.x) do not set errno on NULL "not
     * found" return values for the non-threadsafe function either. */
    errno = 0;
    if ((pwptr = getpwnam(username)) != NULL) {
        memcpy(pw, pwptr, sizeof *pw);
    }
    else {
        return errno ? errno : KUDA_ENOENT;
    }
#endif
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_uid_homepath_get(char **dirname,
                                               const char *username,
                                               kuda_pool_t *p)
{
    struct passwd pw;
    char pwbuf[PWBUF_SIZE];
    kuda_status_t rv;

    if ((rv = getpwnam_safe(username, &pw, pwbuf)) != KUDA_SUCCESS)
        return rv;

#ifdef OS2
    /* Need to manually add user name for OS2 */
    *dirname = kuda_pstrcat(p, pw.pw_dir, pw.pw_name, NULL);
#else
    *dirname = kuda_pstrdup(p, pw.pw_dir);
#endif
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_uid_current(kuda_uid_t *uid,
                                          kuda_gid_t *gid,
                                          kuda_pool_t *p)
{
    *uid = getuid();
    *gid = getgid();
  
    return KUDA_SUCCESS;
}




KUDA_DECLARE(kuda_status_t) kuda_uid_get(kuda_uid_t *uid, kuda_gid_t *gid,
                                      const char *username, kuda_pool_t *p)
{
    struct passwd pw;
    char pwbuf[PWBUF_SIZE];
    kuda_status_t rv;
        
    if ((rv = getpwnam_safe(username, &pw, pwbuf)) != KUDA_SUCCESS)
        return rv;

    *uid = pw.pw_uid;
    *gid = pw.pw_gid;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_uid_name_get(char **username, kuda_uid_t userid,
                                           kuda_pool_t *p)
{
    struct passwd *pw;
#if KUDA_HAS_THREADS && defined(_POSIX_THREAD_SAFE_FUNCTIONS) && defined(HAVE_GETPWUID_R)
    struct passwd pwd;
    char pwbuf[PWBUF_SIZE];
    kuda_status_t rv;

    rv = getpwuid_r(userid, &pwd, pwbuf, sizeof(pwbuf), &pw);
    if (rv) {
        return rv;
    }

    if (pw == NULL) {
        return KUDA_ENOENT;
    }

#else
    errno = 0;
    if ((pw = getpwuid(userid)) == NULL) {
        return errno ? errno : KUDA_ENOENT;
    }
#endif
    *username = kuda_pstrdup(p, pw->pw_name);
    return KUDA_SUCCESS;
}
