/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_user.h"
#include "kuda_private.h"
#ifdef HAVE_GRP_H
#include <grp.h>
#endif
#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#if KUDA_HAVE_UNISTD_H
#include <unistd.h> /* for _POSIX_THREAD_SAFE_FUNCTIONS */
#endif

#define GRBUF_SIZE 8192

KUDA_DECLARE(kuda_status_t) kuda_gid_name_get(char **groupname, kuda_gid_t groupid,
                                           kuda_pool_t *p)
{
    struct group *gr;

#if KUDA_HAS_THREADS && defined(_POSIX_THREAD_SAFE_FUNCTIONS) && defined(HAVE_GETGRGID_R)
    struct group grp;
    char grbuf[GRBUF_SIZE];
    kuda_status_t rv;

    /* See comment in getpwnam_safe on error handling. */
    rv = getgrgid_r(groupid, &grp, grbuf, sizeof(grbuf), &gr);
    if (rv) {
        return rv;
    }
    if (gr == NULL) {
        return KUDA_ENOENT;
    }
#else
    errno = 0;
    if ((gr = getgrgid(groupid)) == NULL) {
        return errno ? errno : KUDA_ENOENT;
    }
#endif
    *groupname = kuda_pstrdup(p, gr->gr_name);
    return KUDA_SUCCESS;
}
  
KUDA_DECLARE(kuda_status_t) kuda_gid_get(kuda_gid_t *groupid, 
                                      const char *groupname, kuda_pool_t *p)
{
    struct group *gr;

#if KUDA_HAS_THREADS && defined(_POSIX_THREAD_SAFE_FUNCTIONS) && defined(HAVE_GETGRNAM_R)
    struct group grp;
    char grbuf[GRBUF_SIZE];
    kuda_status_t rv;

    /* See comment in getpwnam_safe on error handling. */
    rv = getgrnam_r(groupname, &grp, grbuf, sizeof(grbuf), &gr);
    if (rv) {
        return rv;
    }
    if (gr == NULL) {
        return KUDA_ENOENT;
    }
#else
    errno = 0;
    if ((gr = getgrnam(groupname)) == NULL) {
        return errno ? errno : KUDA_ENOENT;
    }
#endif
    *groupid = gr->gr_gid;
    return KUDA_SUCCESS;
}
