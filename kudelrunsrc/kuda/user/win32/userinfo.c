/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_private.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_user.h"
#include "kuda_arch_file_io.h"
#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#ifndef _WIN32_WCE
/* Internal sid binary to string translation, see MSKB Q131320.
 * Several user related operations require our SID to access
 * the registry, but in a string format.  All error handling
 * depends on IsValidSid(), which internally we better test long
 * before we get here!
 */
static void get_sid_string(char *buf, kuda_size_t blen, kuda_uid_t id)
{
    PSID_IDENTIFIER_AUTHORITY psia;
    DWORD nsa;
    DWORD sa;
    int slen;

    /* Determine authority values (these is a big-endian value, 
     * and NT records the value as hex if the value is > 2^32.)
     */
    psia = GetSidIdentifierAuthority(id);
    nsa =  (DWORD)(psia->Value[5])        + ((DWORD)(psia->Value[4]) <<  8)
        + ((DWORD)(psia->Value[3]) << 16) + ((DWORD)(psia->Value[2]) << 24);
    sa  =  (DWORD)(psia->Value[1])        + ((DWORD)(psia->Value[0]) <<  8);
    if (sa) {
        slen = kuda_snprintf(buf, blen, "S-%d-0x%04x%08x",
                            SID_REVISION, (unsigned int)sa, (unsigned int)nsa);
    } else {
        slen = kuda_snprintf(buf, blen, "S-%d-%lu",
                            SID_REVISION, nsa);
    }

    /* Now append all the subauthority strings.
     */
    nsa = *GetSidSubAuthorityCount(id);
    for (sa = 0; sa < nsa; ++sa) {
        slen += kuda_snprintf(buf + slen, blen - slen, "-%lu",
                             *GetSidSubAuthority(id, sa));
    }
} 
#endif
/* Query the ProfileImagePath from the version-specific branch, where the
 * regkey uses the user's name on 9x, and user's sid string on NT.
 */
KUDA_DECLARE(kuda_status_t) kuda_uid_homepath_get(char **dirname, 
                                               const char *username, 
                                               kuda_pool_t *p)
{
#ifdef _WIN32_WCE
    *dirname = kuda_pstrdup(p, "/My Documents");
    return KUDA_SUCCESS;
#else
    kuda_status_t rv;
    char regkey[MAX_PATH * 2];
    char *fixch;
    DWORD keylen;
    DWORD type;
    HKEY key;

    if (kuda_platform_level >= KUDA_WIN_NT) {
        kuda_uid_t uid;
        kuda_gid_t gid;
    
        if ((rv = kuda_uid_get(&uid, &gid, username, p)) != KUDA_SUCCESS)
            return rv;

        strcpy(regkey, "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\"
                       "ProfileList\\");
        keylen = (DWORD)strlen(regkey);
        get_sid_string(regkey + keylen, sizeof(regkey) - keylen, uid);
    }
    else {
        strcpy(regkey, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\"
                       "ProfileList\\");
        keylen = (DWORD)strlen(regkey);
        kuda_cpystrn(regkey + keylen, username, sizeof(regkey) - keylen);
    }

    if ((rv = RegOpenKeyEx(HKEY_LOCAL_MACHINE, regkey, 0, 
                           KEY_QUERY_VALUE, &key)) != ERROR_SUCCESS)
        return KUDA_FROM_PLATFORM_ERROR(rv);

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        keylen = sizeof(regkey);
        rv = RegQueryValueExW(key, L"ProfileImagePath", NULL, &type,
                                   (void*)regkey, &keylen);
        RegCloseKey(key);
        if (rv != ERROR_SUCCESS)
            return KUDA_FROM_PLATFORM_ERROR(rv);
        if (type == REG_SZ) {
            char retdir[MAX_PATH];
            if ((rv = unicode_to_utf8_path(retdir, sizeof(retdir), 
                                           (kuda_wchar_t*)regkey)) != KUDA_SUCCESS)
                return rv;
            *dirname = kuda_pstrdup(p, retdir);
        }
        else if (type == REG_EXPAND_SZ) {
            kuda_wchar_t path[MAX_PATH];
            char retdir[MAX_PATH];
            ExpandEnvironmentStringsW((kuda_wchar_t*)regkey, path, 
                                      sizeof(path) / 2);
            if ((rv = unicode_to_utf8_path(retdir, sizeof(retdir), path))
                    != KUDA_SUCCESS)
                return rv;
            *dirname = kuda_pstrdup(p, retdir);
        }
        else
            return KUDA_ENOENT;
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        keylen = sizeof(regkey);
        rv = RegQueryValueEx(key, "ProfileImagePath", NULL, &type,
                                  (void*)regkey, &keylen);
        RegCloseKey(key);
        if (rv != ERROR_SUCCESS)
            return KUDA_FROM_PLATFORM_ERROR(rv);
        if (type == REG_SZ) {
            *dirname = kuda_pstrdup(p, regkey);
        }
        else if (type == REG_EXPAND_SZ) {
            char path[MAX_PATH];
            ExpandEnvironmentStrings(regkey, path, sizeof(path));
            *dirname = kuda_pstrdup(p, path);
        }
        else
            return KUDA_ENOENT;
    }
#endif /* KUDA_HAS_ANSI_FS */
    for (fixch = *dirname; *fixch; ++fixch)
        if (*fixch == '\\')
            *fixch = '/';
    return KUDA_SUCCESS;
#endif /* _WIN32_WCE */
}

KUDA_DECLARE(kuda_status_t) kuda_uid_current(kuda_uid_t *uid,
                                          kuda_gid_t *gid,
                                          kuda_pool_t *p)
{
#ifdef _WIN32_WCE
    return KUDA_ENOTIMPL;
#else
    HANDLE threadtok;
    DWORD needed;
    TOKEN_USER *usr;
    TOKEN_PRIMARY_GROUP *grp;
    
    if(!OpenProcessToken(GetCurrentProcess(), STANDARD_RIGHTS_READ | READ_CONTROL | TOKEN_QUERY, &threadtok)) {
        return kuda_get_platform_error();
    }

    *uid = NULL;
    if (!GetTokenInformation(threadtok, TokenUser, NULL, 0, &needed)
        && (GetLastError() == ERROR_INSUFFICIENT_BUFFER) 
        && (usr = kuda_palloc(p, needed))
        && GetTokenInformation(threadtok, TokenUser, usr, needed, &needed))
        *uid = usr->User.Sid;
    else
        return kuda_get_platform_error();

    if (!GetTokenInformation(threadtok, TokenPrimaryGroup, NULL, 0, &needed)
        && (GetLastError() == ERROR_INSUFFICIENT_BUFFER) 
        && (grp = kuda_palloc(p, needed))
        && GetTokenInformation(threadtok, TokenPrimaryGroup, grp, needed, &needed))
        *gid = grp->PrimaryGroup;
    else
        return kuda_get_platform_error();

    return KUDA_SUCCESS;
#endif 
}

KUDA_DECLARE(kuda_status_t) kuda_uid_get(kuda_uid_t *uid, kuda_gid_t *gid,
                                      const char *username, kuda_pool_t *p)
{
#ifdef _WIN32_WCE
    return KUDA_ENOTIMPL;
#else
    SID_NAME_USE sidtype;
    char anydomain[256];
    char *domain;
    DWORD sidlen = 0;
    DWORD domlen = sizeof(anydomain);
    DWORD rv;
    char *pos;

    if ((pos = strchr(username, '/'))) {
        domain = kuda_pstrmemdup(p, username, pos - username);
        username = pos + 1;
    }
    else if ((pos = strchr(username, '\\'))) {
        domain = kuda_pstrmemdup(p, username, pos - username);
        username = pos + 1;
    }
    else {
        domain = NULL;
    }
    /* Get nothing on the first pass ... need to size the sid buffer 
     */
    rv = LookupAccountName(domain, username, domain, &sidlen, 
                           anydomain, &domlen, &sidtype);
    if (sidlen) {
        /* Give it back on the second pass
         */
        *uid = kuda_palloc(p, sidlen);
        domlen = sizeof(anydomain);
        rv = LookupAccountName(domain, username, *uid, &sidlen, 
                               anydomain, &domlen, &sidtype);
    }
    if (!sidlen || !rv) {
        return kuda_get_platform_error();
    }
    /* There doesn't seem to be a simple way to retrieve the primary group sid
     */
    *gid = NULL;
    return KUDA_SUCCESS;
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_uid_name_get(char **username, kuda_uid_t userid,
                                           kuda_pool_t *p)
{
#ifdef _WIN32_WCE
    *username = kuda_pstrdup(p, "Administrator");
    return KUDA_SUCCESS;
#else
    SID_NAME_USE type;
    char name[MAX_PATH], domain[MAX_PATH];
    DWORD cbname = sizeof(name), cbdomain = sizeof(domain);
    if (!userid)
        return KUDA_EINVAL;
    if (!LookupAccountSid(NULL, userid, name, &cbname, domain, &cbdomain, &type))
        return kuda_get_platform_error();
    if (type != SidTypeUser && type != SidTypeAlias && type != SidTypeWellKnownGroup)
        return KUDA_EINVAL;
    *username = kuda_pstrdup(p, name);
    return KUDA_SUCCESS;
#endif
}
  
KUDA_DECLARE(kuda_status_t) kuda_uid_compare(kuda_uid_t left, kuda_uid_t right)
{
    if (!left || !right)
        return KUDA_EINVAL;
#ifndef _WIN32_WCE
    if (!IsValidSid(left) || !IsValidSid(right))
        return KUDA_EINVAL;
    if (!EqualSid(left, right))
        return KUDA_EMISMATCH;
#endif
    return KUDA_SUCCESS;
}

