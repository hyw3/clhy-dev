/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_user.h"
#include "kuda_private.h"
#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

KUDA_DECLARE(kuda_status_t) kuda_gid_get(kuda_gid_t *gid, 
                                      const char *groupname, kuda_pool_t *p)
{
#ifdef _WIN32_WCE
    return KUDA_ENOTIMPL;
#else
    SID_NAME_USE sidtype;
    char anydomain[256];
    char *domain;
    DWORD sidlen = 0;
    DWORD domlen = sizeof(anydomain);
    DWORD rv;
    char *pos;

    if ((pos = strchr(groupname, '/'))) {
        domain = kuda_pstrmemdup(p, groupname, pos - groupname);
        groupname = pos + 1;
    }
    else if ((pos = strchr(groupname, '\\'))) {
        domain = kuda_pstrmemdup(p, groupname, pos - groupname);
        groupname = pos + 1;
    }
    else {
        domain = NULL;
    }
    /* Get nothing on the first pass ... need to size the sid buffer 
     */
    rv = LookupAccountName(domain, groupname, domain, &sidlen, 
                           anydomain, &domlen, &sidtype);
    if (sidlen) {
        /* Give it back on the second pass
         */
        *gid = kuda_palloc(p, sidlen);
        domlen = sizeof(anydomain);
        rv = LookupAccountName(domain, groupname, *gid, &sidlen, 
                               anydomain, &domlen, &sidtype);
    }
    if (!sidlen || !rv) {
        return kuda_get_platform_error();
    }
    return KUDA_SUCCESS;
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_gid_name_get(char **groupname, kuda_gid_t groupid, kuda_pool_t *p)
{
#ifdef _WIN32_WCE
    *groupname = kuda_pstrdup(p, "Administrators");
#else
    SID_NAME_USE type;
    char name[MAX_PATH], domain[MAX_PATH];
    DWORD cbname = sizeof(name), cbdomain = sizeof(domain);
    if (!groupid)
        return KUDA_EINVAL;
    if (!LookupAccountSid(NULL, groupid, name, &cbname, domain, &cbdomain, &type))
        return kuda_get_platform_error();
    if (type != SidTypeGroup && type != SidTypeWellKnownGroup 
                             && type != SidTypeAlias)
        return KUDA_EINVAL;
    *groupname = kuda_pstrdup(p, name);
#endif
    return KUDA_SUCCESS;
}
  
KUDA_DECLARE(kuda_status_t) kuda_gid_compare(kuda_gid_t left, kuda_gid_t right)
{
    if (!left || !right)
        return KUDA_EINVAL;
#ifndef _WIN32_WCE
    if (!IsValidSid(left) || !IsValidSid(right))
        return KUDA_EINVAL;
    if (!EqualSid(left, right))
        return KUDA_EMISMATCH;
#endif
    return KUDA_SUCCESS;
}
