/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_private.h"

#include "kuda_general.h"
#include "kuda_pools.h"
#include "kuda_time.h"

#include "kuda_hash.h"

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif
#if KUDA_HAVE_STRING_H
#include <string.h>
#endif

#if KUDA_POOL_DEBUG && KUDA_HAVE_STDIO_H
#include <stdio.h>
#endif

/*
 * The internal form of a hash table.
 *
 * The table is an array indexed by the hash of the key; collisions
 * are resolved by hanging a linked list of hash entries off each
 * element of the array. Although this is a really simple design it
 * isn't too bad given that pools have a low allocation overhead.
 */

typedef struct kuda_hash_entry_t kuda_hash_entry_t;

struct kuda_hash_entry_t {
    kuda_hash_entry_t *next;
    unsigned int      hash;
    const void       *key;
    kuda_ssize_t       klen;
    const void       *val;
};

/*
 * Data structure for iterating through a hash table.
 *
 * We keep a pointer to the next hash entry here to allow the current
 * hash entry to be freed or otherwise mangled between calls to
 * kuda_hash_next().
 */
struct kuda_hash_index_t {
    kuda_hash_t         *ht;
    kuda_hash_entry_t   *this, *next;
    unsigned int        index;
};

/*
 * The size of the array is always a power of two. We use the maximum
 * index rather than the size so that we can use bitwise-AND for
 * modular arithmetic.
 * The count of hash entries may be greater depending on the chosen
 * collision rate.
 */
struct kuda_hash_t {
    kuda_pool_t          *pool;
    kuda_hash_entry_t   **array;
    kuda_hash_index_t     iterator;  /* For kuda_hash_first(NULL, ...) */
    unsigned int         count, max, seed;
    kuda_hashfunc_t       hash_func;
    kuda_hash_entry_t    *free;  /* List of recycled entries */
};

#define INITIAL_MAX 15 /* tunable == 2^n - 1 */


/*
 * Hash creation functions.
 */

static kuda_hash_entry_t **alloc_array(kuda_hash_t *ht, unsigned int max)
{
   return kuda_pcalloc(ht->pool, sizeof(*ht->array) * (max + 1));
}

KUDA_DECLARE(kuda_hash_t *) kuda_hash_make(kuda_pool_t *pool)
{
    kuda_hash_t *ht;
    kuda_time_t now = kuda_time_now();

    ht = kuda_palloc(pool, sizeof(kuda_hash_t));
    ht->pool = pool;
    ht->free = NULL;
    ht->count = 0;
    ht->max = INITIAL_MAX;
    ht->seed = (unsigned int)((now >> 32) ^ now ^ (kuda_uintptr_t)pool ^
                              (kuda_uintptr_t)ht ^ (kuda_uintptr_t)&now) - 1;
    ht->array = alloc_array(ht, ht->max);
    ht->hash_func = NULL;

    return ht;
}

KUDA_DECLARE(kuda_hash_t *) kuda_hash_make_custom(kuda_pool_t *pool,
                                               kuda_hashfunc_t hash_func)
{
    kuda_hash_t *ht = kuda_hash_make(pool);
    ht->hash_func = hash_func;
    return ht;
}


/*
 * Hash iteration functions.
 */

KUDA_DECLARE(kuda_hash_index_t *) kuda_hash_next(kuda_hash_index_t *hi)
{
    hi->this = hi->next;
    while (!hi->this) {
        if (hi->index > hi->ht->max)
            return NULL;

        hi->this = hi->ht->array[hi->index++];
    }
    hi->next = hi->this->next;
    return hi;
}

KUDA_DECLARE(kuda_hash_index_t *) kuda_hash_first(kuda_pool_t *p, kuda_hash_t *ht)
{
    kuda_hash_index_t *hi;
    if (p)
        hi = kuda_palloc(p, sizeof(*hi));
    else
        hi = &ht->iterator;

    hi->ht = ht;
    hi->index = 0;
    hi->this = NULL;
    hi->next = NULL;
    return kuda_hash_next(hi);
}

KUDA_DECLARE(void) kuda_hash_this(kuda_hash_index_t *hi,
                                const void **key,
                                kuda_ssize_t *klen,
                                void **val)
{
    if (key)  *key  = hi->this->key;
    if (klen) *klen = hi->this->klen;
    if (val)  *val  = (void *)hi->this->val;
}

KUDA_DECLARE(const void *) kuda_hash_this_key(kuda_hash_index_t *hi)
{
    const void *key;

    kuda_hash_this(hi, &key, NULL, NULL);
    return key;
}

KUDA_DECLARE(kuda_ssize_t) kuda_hash_this_key_len(kuda_hash_index_t *hi)
{
    kuda_ssize_t klen;

    kuda_hash_this(hi, NULL, &klen, NULL);
    return klen;
}

KUDA_DECLARE(void *) kuda_hash_this_val(kuda_hash_index_t *hi)
{
    void *val;

    kuda_hash_this(hi, NULL, NULL, &val);
    return val;
}

/*
 * Expanding a hash table
 */

static void expand_array(kuda_hash_t *ht)
{
    kuda_hash_index_t *hi;
    kuda_hash_entry_t **new_array;
    unsigned int new_max;

    new_max = ht->max * 2 + 1;
    new_array = alloc_array(ht, new_max);
    for (hi = kuda_hash_first(NULL, ht); hi; hi = kuda_hash_next(hi)) {
        unsigned int i = hi->this->hash & new_max;
        hi->this->next = new_array[i];
        new_array[i] = hi->this;
    }
    ht->array = new_array;
    ht->max = new_max;
}

static unsigned int hashfunc_default(const char *char_key, kuda_ssize_t *klen,
                                     unsigned int hash)
{
    const unsigned char *key = (const unsigned char *)char_key;
    const unsigned char *p;
    kuda_ssize_t i;
    
    /*
     * This is the popular `times 33' hash algorithm which is used by
     * perl and also appears in Berkeley DB. This is one of the best
     * known hash functions for strings because it is both computed
     * very fast and distributes very well.
     *
     * The originator may be Dan Bernstein but the code in Berkeley DB
     * cites Chris Torek as the source. The best citation I have found
     * is "Chris Torek, Hash function for text in C, Usenet message
     * <27038@mimsy.umd.edu> in comp.lang.c , October, 1990." in Rich
     * Salz's USENIX 1992 paper about INN which can be found at
     * <http://citeseer.nj.nec.com/salz92internetnews.html>.
     *
     * The magic of number 33, i.e. why it works better than many other
     * constants, prime or not, has never been adequately explained by
     * anyone. So I try an explanation: if one experimentally tests all
     * multipliers between 1 and 256 (as I did while writing a low-level
     * data structure library some time ago) one detects that even
     * numbers are not useable at all. The remaining 128 odd numbers
     * (except for the number 1) work more or less all equally well.
     * They all distribute in an acceptable way and this way fill a hash
     * table with an average percent of approx. 86%.
     *
     * If one compares the chi^2 values of the variants (see
     * Bob Jenkins ``Hashing Frequently Asked Questions'' at
     * http://burtleburtle.net/bob/hash/hashfaq.html for a description
     * of chi^2), the number 33 not even has the best value. But the
     * number 33 and a few other equally good numbers like 17, 31, 63,
     * 127 and 129 have nevertheless a great advantage to the remaining
     * numbers in the large set of possible multipliers: their multiply
     * operation can be replaced by a faster operation based on just one
     * shift plus either a single addition or subtraction operation. And
     * because a hash function has to both distribute good _and_ has to
     * be very fast to compute, those few numbers should be preferred.
     *
     *                  -- Ralf S. Engelschall <rse@engelschall.com>
     */

    if (*klen == KUDA_HASH_KEY_STRING) {
        for (p = key; *p; p++) {
            hash = hash * 33 + *p;
        }
        *klen = p - key;
    }
    else {
        for (p = key, i = *klen; i; i--, p++) {
            hash = hash * 33 + *p;
        }
    }

    return hash;
}

KUDA_DECLARE_NONSTD(unsigned int) kuda_hashfunc_default(const char *char_key,
                                                      kuda_ssize_t *klen)
{
    return hashfunc_default(char_key, klen, 0);
}

/*
 * This is where we keep the details of the hash function and control
 * the maximum collision rate.
 *
 * If val is non-NULL it creates and initializes a new hash entry if
 * there isn't already one there; it returns an updatable pointer so
 * that hash entries can be removed.
 */

static kuda_hash_entry_t **find_entry(kuda_hash_t *ht,
                                     const void *key,
                                     kuda_ssize_t klen,
                                     const void *val)
{
    kuda_hash_entry_t **hep, *he;
    unsigned int hash;

    if (ht->hash_func)
        hash = ht->hash_func(key, &klen);
    else
        hash = hashfunc_default(key, &klen, ht->seed);

    /* scan linked list */
    for (hep = &ht->array[hash & ht->max], he = *hep;
         he; hep = &he->next, he = *hep) {
        if (he->hash == hash
            && he->klen == klen
            && memcmp(he->key, key, klen) == 0)
            break;
    }
    if (he || !val)
        return hep;

    /* add a new entry for non-NULL values */
    if ((he = ht->free) != NULL)
        ht->free = he->next;
    else
        he = kuda_palloc(ht->pool, sizeof(*he));
    he->next = NULL;
    he->hash = hash;
    he->key  = key;
    he->klen = klen;
    he->val  = val;
    *hep = he;
    ht->count++;
    return hep;
}

KUDA_DECLARE(kuda_hash_t *) kuda_hash_copy(kuda_pool_t *pool,
                                        const kuda_hash_t *orig)
{
    kuda_hash_t *ht;
    kuda_hash_entry_t *new_vals;
    unsigned int i, j;

    ht = kuda_palloc(pool, sizeof(kuda_hash_t) +
                    sizeof(*ht->array) * (orig->max + 1) +
                    sizeof(kuda_hash_entry_t) * orig->count);
    ht->pool = pool;
    ht->free = NULL;
    ht->count = orig->count;
    ht->max = orig->max;
    ht->seed = orig->seed;
    ht->hash_func = orig->hash_func;
    ht->array = (kuda_hash_entry_t **)((char *)ht + sizeof(kuda_hash_t));

    new_vals = (kuda_hash_entry_t *)((char *)(ht) + sizeof(kuda_hash_t) +
                                    sizeof(*ht->array) * (orig->max + 1));
    j = 0;
    for (i = 0; i <= ht->max; i++) {
        kuda_hash_entry_t **new_entry = &(ht->array[i]);
        kuda_hash_entry_t *orig_entry = orig->array[i];
        while (orig_entry) {
            *new_entry = &new_vals[j++];
            (*new_entry)->hash = orig_entry->hash;
            (*new_entry)->key = orig_entry->key;
            (*new_entry)->klen = orig_entry->klen;
            (*new_entry)->val = orig_entry->val;
            new_entry = &((*new_entry)->next);
            orig_entry = orig_entry->next;
        }
        *new_entry = NULL;
    }
    return ht;
}

KUDA_DECLARE(void *) kuda_hash_get(kuda_hash_t *ht,
                                 const void *key,
                                 kuda_ssize_t klen)
{
    kuda_hash_entry_t *he;
    he = *find_entry(ht, key, klen, NULL);
    if (he)
        return (void *)he->val;
    else
        return NULL;
}

KUDA_DECLARE(void) kuda_hash_set(kuda_hash_t *ht,
                               const void *key,
                               kuda_ssize_t klen,
                               const void *val)
{
    kuda_hash_entry_t **hep;
    hep = find_entry(ht, key, klen, val);
    if (*hep) {
        if (!val) {
            /* delete entry */
            kuda_hash_entry_t *old = *hep;
            *hep = (*hep)->next;
            old->next = ht->free;
            ht->free = old;
            --ht->count;
        }
        else {
            /* replace entry */
            (*hep)->val = val;
            /* check that the collision rate isn't too high */
            if (ht->count > ht->max) {
                expand_array(ht);
            }
        }
    }
    /* else key not present and val==NULL */
}

KUDA_DECLARE(unsigned int) kuda_hash_count(kuda_hash_t *ht)
{
    return ht->count;
}

KUDA_DECLARE(void) kuda_hash_clear(kuda_hash_t *ht)
{
    kuda_hash_index_t *hi;
    for (hi = kuda_hash_first(NULL, ht); hi; hi = kuda_hash_next(hi))
        kuda_hash_set(ht, hi->this->key, hi->this->klen, NULL);
}

KUDA_DECLARE(kuda_hash_t*) kuda_hash_overlay(kuda_pool_t *p,
                                          const kuda_hash_t *overlay,
                                          const kuda_hash_t *base)
{
    return kuda_hash_merge(p, overlay, base, NULL, NULL);
}

KUDA_DECLARE(kuda_hash_t *) kuda_hash_merge(kuda_pool_t *p,
                                         const kuda_hash_t *overlay,
                                         const kuda_hash_t *base,
                                         void * (*merger)(kuda_pool_t *p,
                                                     const void *key,
                                                     kuda_ssize_t klen,
                                                     const void *h1_val,
                                                     const void *h2_val,
                                                     const void *data),
                                         const void *data)
{
    kuda_hash_t *res;
    kuda_hash_entry_t *new_vals = NULL;
    kuda_hash_entry_t *iter;
    kuda_hash_entry_t *ent;
    unsigned int i, j, k, hash;

#if KUDA_POOL_DEBUG
    /* we don't copy keys and values, so it's necessary that
     * overlay->a.pool and base->a.pool have a life span at least
     * as long as p
     */
    if (!kuda_pool_is_ancestor(overlay->pool, p)) {
        fprintf(stderr,
                "kuda_hash_merge: overlay's pool is not an ancestor of p\n");
        abort();
    }
    if (!kuda_pool_is_ancestor(base->pool, p)) {
        fprintf(stderr,
                "kuda_hash_merge: base's pool is not an ancestor of p\n");
        abort();
    }
#endif

    res = kuda_palloc(p, sizeof(kuda_hash_t));
    res->pool = p;
    res->free = NULL;
    res->hash_func = base->hash_func;
    res->count = base->count;
    res->max = (overlay->max > base->max) ? overlay->max : base->max;
    if (base->count + overlay->count > res->max) {
        res->max = res->max * 2 + 1;
    }
    res->seed = base->seed;
    res->array = alloc_array(res, res->max);
    if (base->count + overlay->count) {
        new_vals = kuda_palloc(p, sizeof(kuda_hash_entry_t) *
                              (base->count + overlay->count));
    }
    j = 0;
    for (k = 0; k <= base->max; k++) {
        for (iter = base->array[k]; iter; iter = iter->next) {
            i = iter->hash & res->max;
            new_vals[j].klen = iter->klen;
            new_vals[j].key = iter->key;
            new_vals[j].val = iter->val;
            new_vals[j].hash = iter->hash;
            new_vals[j].next = res->array[i];
            res->array[i] = &new_vals[j];
            j++;
        }
    }

    for (k = 0; k <= overlay->max; k++) {
        for (iter = overlay->array[k]; iter; iter = iter->next) {
            if (res->hash_func)
                hash = res->hash_func(iter->key, &iter->klen);
            else
                hash = hashfunc_default(iter->key, &iter->klen, res->seed);
            i = hash & res->max;
            for (ent = res->array[i]; ent; ent = ent->next) {
                if ((ent->klen == iter->klen) &&
                    (memcmp(ent->key, iter->key, iter->klen) == 0)) {
                    if (merger) {
                        ent->val = (*merger)(p, iter->key, iter->klen,
                                             iter->val, ent->val, data);
                    }
                    else {
                        ent->val = iter->val;
                    }
                    break;
                }
            }
            if (!ent) {
                new_vals[j].klen = iter->klen;
                new_vals[j].key = iter->key;
                new_vals[j].val = iter->val;
                new_vals[j].hash = hash;
                new_vals[j].next = res->array[i];
                res->array[i] = &new_vals[j];
                res->count++;
                j++;
            }
        }
    }
    return res;
}

/* This is basically the following...
 * for every element in hash table {
 *    comp elemeny.key, element.value
 * }
 *
 * Like with kuda_table_do, the comp callback is called for each and every
 * element of the hash table.
 */
KUDA_DECLARE(int) kuda_hash_do(kuda_hash_do_callback_fn_t *comp,
                             void *rec, const kuda_hash_t *ht)
{
    kuda_hash_index_t  hix;
    kuda_hash_index_t *hi;
    int rv, dorv  = 1;

    hix.ht    = (kuda_hash_t *)ht;
    hix.index = 0;
    hix.this  = NULL;
    hix.next  = NULL;

    if ((hi = kuda_hash_next(&hix))) {
        /* Scan the entire table */
        do {
            rv = (*comp)(rec, hi->this->key, hi->this->klen, hi->this->val);
        } while (rv && (hi = kuda_hash_next(hi)));

        if (rv == 0) {
            dorv = 0;
        }
    }
    return dorv;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(hash)
