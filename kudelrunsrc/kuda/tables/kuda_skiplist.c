/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Modified to use KUDA and KUDA pools.
 *  TODO: Is malloc() better? Will long running skiplists grow too much?
 *  Keep the skiplist_alloc() and skiplist_free() until we know
 *  Yeah, if using pools it means some bogus cycles for checks
 *  (and an useless function call for skiplist_free) which we
 *  can removed if/when needed.
 */

#include "kuda_skiplist.h"

typedef struct {
    kuda_skiplistnode **data;
    size_t size, pos;
    kuda_pool_t *p;
} kuda_skiplist_q; 

struct kuda_skiplist {
    kuda_skiplist_compare compare;
    kuda_skiplist_compare comparek;
    int height;
    int preheight;
    size_t size;
    kuda_skiplistnode *top;
    kuda_skiplistnode *bottom;
    /* These two are needed for appending */
    kuda_skiplistnode *topend;
    kuda_skiplistnode *bottomend;
    kuda_skiplist *index;
    kuda_array_header_t *memlist;
    kuda_skiplist_q nodes_q,
                   stack_q;
    kuda_pool_t *pool;
};

struct kuda_skiplistnode {
    void *data;
    kuda_skiplistnode *next;
    kuda_skiplistnode *prev;
    kuda_skiplistnode *down;
    kuda_skiplistnode *up;
    kuda_skiplistnode *previndex;
    kuda_skiplistnode *nextindex;
    kuda_skiplist *sl;
};

static int get_b_rand(void)
{
    static int ph = 32;         /* More bits than we will ever use */
    static int randseq;
    if (ph > 31) {              /* Num bits in return of rand() */
        ph = 0;
        randseq = rand();
    }
    return randseq & (1 << ph++);
}

typedef struct {
    size_t size;
    kuda_array_header_t *list;
} memlist_t;

typedef struct {
    void *ptr;
    char inuse;
} chunk_t;

KUDA_DECLARE(void *) kuda_skiplist_alloc(kuda_skiplist *sl, size_t size)
{
    if (sl->pool) {
        void *ptr;
        int found_size = 0;
        int i;
        chunk_t *newchunk;
        memlist_t *memlist = (memlist_t *)sl->memlist->elts;
        for (i = 0; i < sl->memlist->nelts; i++) {
            if (memlist->size == size) {
                int j;
                chunk_t *chunk = (chunk_t *)memlist->list->elts;
                found_size = 1;
                for (j = 0; j < memlist->list->nelts; j++) {
                    if (!chunk->inuse) {
                        chunk->inuse = 1;
                        return chunk->ptr;
                    }
                    chunk++;
                }
                break; /* no free of this size; punt */
            }
            memlist++;
        }
        /* no free chunks */
        ptr = kuda_palloc(sl->pool, size);
        if (!ptr) {
            return ptr;
        }
        /*
         * is this a new sized chunk? If so, we need to create a new
         * array of them. Otherwise, re-use what we already have.
         */
        if (!found_size) {
            memlist = kuda_array_push(sl->memlist);
            memlist->size = size;
            memlist->list = kuda_array_make(sl->pool, 20, sizeof(chunk_t));
        }
        newchunk = kuda_array_push(memlist->list);
        newchunk->ptr = ptr;
        newchunk->inuse = 1;
        return ptr;
    }
    else {
        return malloc(size);
    }
}

KUDA_DECLARE(void) kuda_skiplist_free(kuda_skiplist *sl, void *mem)
{
    if (!sl->pool) {
        free(mem);
    }
    else {
        int i;
        memlist_t *memlist = (memlist_t *)sl->memlist->elts;
        for (i = 0; i < sl->memlist->nelts; i++) {
            int j;
            chunk_t *chunk = (chunk_t *)memlist->list->elts;
            for (j = 0; j < memlist->list->nelts; j++) {
                if (chunk->ptr == mem) {
                    chunk->inuse = 0;
                    return;
                }
                chunk++;
            }
            memlist++;
        }
    }
}

static kuda_status_t skiplist_qpush(kuda_skiplist_q *q, kuda_skiplistnode *m)
{
    if (q->pos >= q->size) {
        kuda_skiplistnode **data;
        size_t size = (q->pos) ? q->pos * 2 : 32;
        if (q->p) {
            data = kuda_palloc(q->p, size * sizeof(*data));
            if (data) {
                memcpy(data, q->data, q->pos * sizeof(*data));
            }
        }
        else {
            data = realloc(q->data, size * sizeof(*data));
        }
        if (!data) {
            return KUDA_ENOMEM;
        }
        q->data = data;
        q->size = size;
    }
    q->data[q->pos++] = m;
    return KUDA_SUCCESS;
}

static KUDA_INLINE kuda_skiplistnode *skiplist_qpop(kuda_skiplist_q *q)
{
    return (q->pos > 0) ? q->data[--q->pos] : NULL;
}

static KUDA_INLINE void skiplist_qclear(kuda_skiplist_q *q)
{
    q->pos = 0;
}

static kuda_skiplistnode *skiplist_new_node(kuda_skiplist *sl)
{
    kuda_skiplistnode *m = skiplist_qpop(&sl->nodes_q);
    if (!m) {
        if (sl->pool) {
            m = kuda_palloc(sl->pool, sizeof *m);
        }
        else {
            m = malloc(sizeof *m);
        }
    }
    return m;
}

static kuda_status_t skiplist_put_node(kuda_skiplist *sl, kuda_skiplistnode *m)
{
    return skiplist_qpush(&sl->nodes_q, m);
}

static kuda_status_t skiplisti_init(kuda_skiplist **s, kuda_pool_t *p)
{
    kuda_skiplist *sl;
    if (p) {
        sl = kuda_pcalloc(p, sizeof(kuda_skiplist));
        sl->memlist = kuda_array_make(p, 20, sizeof(memlist_t));
        sl->pool = sl->nodes_q.p = sl->stack_q.p = p;
    }
    else {
        sl = calloc(1, sizeof(kuda_skiplist));
        if (!sl) {
            return KUDA_ENOMEM;
        }
    }
    *s = sl;
    return KUDA_SUCCESS;
}

static int indexing_comp(void *a, void *b)
{
    void *ac = (void *) (((kuda_skiplist *) a)->compare);
    void *bc = (void *) (((kuda_skiplist *) b)->compare);
    return ((ac < bc) ? -1 : ((ac > bc) ? 1 : 0));
}

static int indexing_compk(void *ac, void *b)
{
    void *bc = (void *) (((kuda_skiplist *) b)->compare);
    return ((ac < bc) ? -1 : ((ac > bc) ? 1 : 0));
}

KUDA_DECLARE(kuda_status_t) kuda_skiplist_init(kuda_skiplist **s, kuda_pool_t *p)
{
    kuda_skiplist *sl;
    skiplisti_init(s, p);
    sl = *s;
    skiplisti_init(&(sl->index), p);
    kuda_skiplist_set_compare(sl->index, indexing_comp, indexing_compk);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(void) kuda_skiplist_set_compare(kuda_skiplist *sl,
                          kuda_skiplist_compare comp,
                          kuda_skiplist_compare compk)
{
    if (sl->compare && sl->comparek) {
        kuda_skiplist_add_index(sl, comp, compk);
    }
    else {
        sl->compare = comp;
        sl->comparek = compk;
    }
}

KUDA_DECLARE(void) kuda_skiplist_add_index(kuda_skiplist *sl,
                        kuda_skiplist_compare comp,
                        kuda_skiplist_compare compk)
{
    kuda_skiplistnode *m;
    kuda_skiplist *ni;
    int icount = 0;
    kuda_skiplist_find(sl->index, (void *)comp, &m);
    if (m) {
        return;                 /* Index already there! */
    }
    skiplisti_init(&ni, sl->pool);
    kuda_skiplist_set_compare(ni, comp, compk);
    /* Build the new index... This can be expensive! */
    m = kuda_skiplist_insert(sl->index, ni);
    while (m->prev) {
        m = m->prev;
        icount++;
    }
    for (m = kuda_skiplist_getlist(sl); m; kuda_skiplist_next(sl, &m)) {
        int j = icount - 1;
        kuda_skiplistnode *nsln;
        nsln = kuda_skiplist_insert(ni, m->data);
        /* skip from main index down list */
        while (j > 0) {
            m = m->nextindex;
            j--;
        }
        /* insert this node in the indexlist after m */
        nsln->nextindex = m->nextindex;
        if (m->nextindex) {
            m->nextindex->previndex = nsln;
        }
        nsln->previndex = m;
        m->nextindex = nsln;
    }
}

static int skiplisti_find_compare(kuda_skiplist *sl, void *data,
                                  kuda_skiplistnode **ret,
                                  kuda_skiplist_compare comp,
                                  int last)
{
    int count = 0;
    kuda_skiplistnode *m, *found = NULL;
    for (m = sl->top; m; count++) {
        if (m->next) {
            int compared = comp(data, m->next->data);
            if (compared == 0) {
                found = m = m->next;
                if (!last) {
                    break;
                }
                continue;
            }
            if (compared > 0) {
                m = m->next;
                continue;
            }
        }
        m = m->down;
    }
    if (found) {
        while (found->down) {
            found = found->down;
        }
        *ret = found;
    }
    else {
        *ret = NULL;
    }
    return count;
}

static void *find_compare(kuda_skiplist *sli, void *data,
                          kuda_skiplistnode **iter,
                          kuda_skiplist_compare comp,
                          int last)
{
    kuda_skiplistnode *m;
    kuda_skiplist *sl;
    if (!comp) {
        if (iter) {
            *iter = NULL;
        }
        return NULL;
    }
    if (comp == sli->compare || !sli->index) {
        sl = sli;
    }
    else {
        kuda_skiplist_find(sli->index, (void *)comp, &m);
        if (!m) {
            if (iter) {
                *iter = NULL;
            }
            return NULL;
        }
        sl = (kuda_skiplist *) m->data;
    }
    skiplisti_find_compare(sl, data, &m, sl->comparek, last);
    if (iter) {
        *iter = m;
    }
    return (m) ? m->data : NULL;
}

KUDA_DECLARE(void *) kuda_skiplist_find_compare(kuda_skiplist *sl, void *data,
                                              kuda_skiplistnode **iter,
                                              kuda_skiplist_compare comp)
{
    return find_compare(sl, data, iter, comp, 0);
}

KUDA_DECLARE(void *) kuda_skiplist_find(kuda_skiplist *sl, void *data, kuda_skiplistnode **iter)
{
    return find_compare(sl, data, iter, sl->compare, 0);
}

KUDA_DECLARE(void *) kuda_skiplist_last_compare(kuda_skiplist *sl, void *data,
                                              kuda_skiplistnode **iter,
                                              kuda_skiplist_compare comp)
{
    return find_compare(sl, data, iter, comp, 1);
}

KUDA_DECLARE(void *) kuda_skiplist_last(kuda_skiplist *sl, void *data,
                                      kuda_skiplistnode **iter)
{
    return find_compare(sl, data, iter, sl->compare, 1);
}


KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_getlist(kuda_skiplist *sl)
{
    if (!sl->bottom) {
        return NULL;
    }
    return sl->bottom->next;
}

KUDA_DECLARE(void *) kuda_skiplist_next(kuda_skiplist *sl, kuda_skiplistnode **iter)
{
    if (!*iter) {
        return NULL;
    }
    *iter = (*iter)->next;
    return (*iter) ? ((*iter)->data) : NULL;
}

KUDA_DECLARE(void *) kuda_skiplist_previous(kuda_skiplist *sl, kuda_skiplistnode **iter)
{
    if (!*iter) {
        return NULL;
    }
    *iter = (*iter)->prev;
    return (*iter) ? ((*iter)->data) : NULL;
}

KUDA_DECLARE(void *) kuda_skiplist_element(kuda_skiplistnode *iter)
{
    return (iter) ? iter->data : NULL;
}

/* forward declared */
static int skiplisti_remove(kuda_skiplist *sl, kuda_skiplistnode *m,
                            kuda_skiplist_freefunc myfree);

static KUDA_INLINE int skiplist_height(const kuda_skiplist *sl)
{
    /* Skiplists (even empty) always have a top node, although this
     * implementation defers its creation until the first insert, or
     * deletes it with the last remove. We want the real height here.
     */
    return sl->height ? sl->height : 1;
}

static kuda_skiplistnode *insert_compare(kuda_skiplist *sl, void *data,
                                        kuda_skiplist_compare comp, int add,
                                        kuda_skiplist_freefunc myfree)
{
    kuda_skiplistnode *m, *p, *tmp, *ret = NULL;
    int ch, top_nh, nh = 1;

    ch = skiplist_height(sl);
    if (sl->preheight) {
        while (nh < sl->preheight && get_b_rand()) {
            nh++;
        }
    }
    else {
        while (nh <= ch && get_b_rand()) {
            nh++;
        }
    }
    top_nh = nh;

    /* Now we have in nh the height at which we wish to insert our new node,
     * and in ch the current height: don't create skip paths to the inserted
     * element until the walk down through the tree (which decrements ch)
     * reaches nh. From there, any walk down pushes the current node on a
     * stack (the node(s) after which we would insert) to pop back through
     * for insertion later.
     */
    m = sl->top;
    while (m) {
        /*
         * To maintain stability, dups (compared == 0) must be added
         * AFTER each other.
         */
        if (m->next) {
            int compared = comp(data, m->next->data);
            if (compared == 0) {
                if (!add) {
                    /* Keep the existing element(s) */
                    skiplist_qclear(&sl->stack_q);
                    return NULL;
                }
                if (add < 0) {
                    /* Remove this element and continue with the next node
                     * or the new top if the current one is also removed.
                     */
                    kuda_skiplistnode *top = sl->top;
                    skiplisti_remove(sl, m->next, myfree);
                    if (top != sl->top) {
                        m = sl->top;
                        skiplist_qclear(&sl->stack_q);
                        ch = skiplist_height(sl);
                        nh = top_nh;
                    }
                    continue;
                }
            }
            if (compared >= 0) {
                m = m->next;
                continue;
            }
        }
        if (ch <= nh) {
            /* push on stack */
            skiplist_qpush(&sl->stack_q, m);
        }
        m = m->down;
        ch--;
    }
    /* Pop the stack and insert nodes */
    p = NULL;
    while ((m = skiplist_qpop(&sl->stack_q))) {
        tmp = skiplist_new_node(sl);
        tmp->next = m->next;
        if (m->next) {
            m->next->prev = tmp;
        }
        m->next = tmp;
        tmp->prev = m;
        tmp->up = NULL;
        tmp->nextindex = tmp->previndex = NULL;
        tmp->down = p;
        if (p) {
            p->up = tmp;
        }
        else {
            /* This sets ret to the bottom-most node we are inserting */
            ret = tmp;
        }
        tmp->data = data;
        tmp->sl = sl;
        p = tmp;
    }

    /* Now we are sure the node is inserted, grow our tree to 'nh' tall */
    for (; sl->height < nh; sl->height++) {
        m = skiplist_new_node(sl);
        tmp = skiplist_new_node(sl);
        m->up = m->prev = m->nextindex = m->previndex = NULL;
        m->next = tmp;
        m->down = sl->top;
        m->data = NULL;
        m->sl = sl;
        if (sl->top) {
            sl->top->up = m;
        }
        else {
            sl->bottom = sl->bottomend = m;
        }
        sl->top = sl->topend = tmp->prev = m;
        tmp->up = tmp->next = tmp->nextindex = tmp->previndex = NULL;
        tmp->down = p;
        tmp->data = data;
        tmp->sl = sl;
        if (p) {
            p->up = tmp;
        }
        else {
            /* This sets ret to the bottom-most node we are inserting */
            ret = tmp;
        }
        p = tmp;
    }
    if (sl->index != NULL) {
        /*
         * this is a external insertion, we must insert into each index as
         * well
         */
        kuda_skiplistnode *ni, *li;
        li = ret;
        for (p = kuda_skiplist_getlist(sl->index); p; kuda_skiplist_next(sl->index, &p)) {
            kuda_skiplist *sli = (kuda_skiplist *)p->data;
            ni = insert_compare(sli, ret->data, sli->compare, 1, NULL);
            li->nextindex = ni;
            ni->previndex = li;
            li = ni;
        }
    }
    sl->size++;
    return ret;
}

KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_insert_compare(kuda_skiplist *sl, void *data,
                                      kuda_skiplist_compare comp)
{
    if (!comp) {
        return NULL;
    }
    return insert_compare(sl, data, comp, 0, NULL);
}

KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_insert(kuda_skiplist *sl, void *data)
{
    return kuda_skiplist_insert_compare(sl, data, sl->compare);
}

KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_add_compare(kuda_skiplist *sl, void *data,
                                      kuda_skiplist_compare comp)
{
    if (!comp) {
        return NULL;
    }
    return insert_compare(sl, data, comp, 1, NULL);
}

KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_add(kuda_skiplist *sl, void *data)
{
    return kuda_skiplist_add_compare(sl, data, sl->compare);
}

KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_replace_compare(kuda_skiplist *sl,
                                    void *data, kuda_skiplist_freefunc myfree,
                                    kuda_skiplist_compare comp)
{
    if (!comp) {
        return NULL;
    }
    return insert_compare(sl, data, comp, -1, myfree);
}

KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_replace(kuda_skiplist *sl,
                                    void *data, kuda_skiplist_freefunc myfree)
{
    return kuda_skiplist_replace_compare(sl, data, myfree, sl->compare);
}

#if 0
void skiplist_print_struct(kuda_skiplist * sl, char *prefix)
{
    kuda_skiplistnode *p, *q;
    fprintf(stderr, "Skiplist Structure (height: %d)\n", sl->height);
    p = sl->bottom;
    while (p) {
        q = p;
        fprintf(stderr, prefix);
        while (q) {
            fprintf(stderr, "%p ", q->data);
            q = q->up;
        }
        fprintf(stderr, "\n");
        p = p->next;
    }
}
#endif

static int skiplisti_remove(kuda_skiplist *sl, kuda_skiplistnode *m,
                            kuda_skiplist_freefunc myfree)
{
    kuda_skiplistnode *p;
    if (!m) {
        return 0;
    }
    if (m->nextindex) {
        skiplisti_remove(m->nextindex->sl, m->nextindex, NULL);
    }
    while (m->up) {
        m = m->up;
    }
    do {
        p = m;
        /* take me out of the list */
        p->prev->next = p->next;
        if (p->next) {
            p->next->prev = p->prev;
        }
        m = m->down;
        /* This only frees the actual data in the bottom one */
        if (!m && myfree && p->data) {
            myfree(p->data);
        }
        skiplist_put_node(sl, p);
    } while (m);
    sl->size--;
    while (sl->top && sl->top->next == NULL) {
        /* While the row is empty and we are not on the bottom row */
        p = sl->top;
        sl->top = sl->top->down;/* Move top down one */
        if (sl->top) {
            sl->top->up = NULL; /* Make it think its the top */
        }
        skiplist_put_node(sl, p);
        sl->height--;
    }
    if (!sl->top) {
        sl->bottom = sl->bottomend = NULL;
        sl->topend = NULL;
    }
    return skiplist_height(sl);
}

KUDA_DECLARE(int) kuda_skiplist_remove_node(kuda_skiplist *sl,
                                          kuda_skiplistnode *iter,
                                          kuda_skiplist_freefunc myfree)
{
    kuda_skiplistnode *m = iter;
    if (!m) {
        return 0;
    }
    while (m->down) {
        m = m->down;
    }
    while (m->previndex) {
        m = m->previndex;
    }
    return skiplisti_remove(sl, m, myfree);
}

KUDA_DECLARE(int) kuda_skiplist_remove_compare(kuda_skiplist *sli,
                            void *data,
                            kuda_skiplist_freefunc myfree, kuda_skiplist_compare comp)
{
    kuda_skiplistnode *m;
    kuda_skiplist *sl;
    if (!comp) {
        return 0;
    }
    if (comp == sli->comparek || !sli->index) {
        sl = sli;
    }
    else {
        kuda_skiplist_find(sli->index, (void *)comp, &m);
        if (!m) {
            return 0;
        }
        sl = (kuda_skiplist *) m->data;
    }
    skiplisti_find_compare(sl, data, &m, comp, 0);
    if (!m) {
        return 0;
    }
    while (m->previndex) {
        m = m->previndex;
    }
    return skiplisti_remove(sl, m, myfree);
}

KUDA_DECLARE(int) kuda_skiplist_remove(kuda_skiplist *sl, void *data, kuda_skiplist_freefunc myfree)
{
    return kuda_skiplist_remove_compare(sl, data, myfree, sl->comparek);
}

KUDA_DECLARE(void) kuda_skiplist_remove_all(kuda_skiplist *sl, kuda_skiplist_freefunc myfree)
{
    /*
     * This must remove even the place holder nodes (bottom though top)
     * because we specify in the API that one can free the Skiplist after
     * making this call without memory leaks
     */
    kuda_skiplistnode *m, *p, *u;
    m = sl->bottom;
    while (m) {
        p = m->next;
        if (myfree && p && p->data) {
            myfree(p->data);
        }
        do {
            u = m->up;
            skiplist_put_node(sl, m);
            m = u;
        } while (m);
        m = p;
    }
    sl->top = sl->bottom = NULL;
    sl->topend = sl->bottomend = NULL;
    sl->height = 0;
    sl->size = 0;
}

KUDA_DECLARE(void *) kuda_skiplist_pop(kuda_skiplist *a, kuda_skiplist_freefunc myfree)
{
    kuda_skiplistnode *sln;
    void *data = NULL;
    sln = kuda_skiplist_getlist(a);
    if (sln) {
        data = sln->data;
        skiplisti_remove(a, sln, myfree);
    }
    return data;
}

KUDA_DECLARE(void *) kuda_skiplist_peek(kuda_skiplist *a)
{
    kuda_skiplistnode *sln;
    sln = kuda_skiplist_getlist(a);
    if (sln) {
        return sln->data;
    }
    return NULL;
}

KUDA_DECLARE(size_t) kuda_skiplist_size(const kuda_skiplist *sl)
{
    return sl->size;
}

KUDA_DECLARE(int) kuda_skiplist_height(const kuda_skiplist *sl)
{
    return skiplist_height(sl);
}

KUDA_DECLARE(int) kuda_skiplist_preheight(const kuda_skiplist *sl)
{
    return sl->preheight;
}

KUDA_DECLARE(void) kuda_skiplist_set_preheight(kuda_skiplist *sl, int to)
{
    sl->preheight = (to > 0) ? to : 0;
}

static void skiplisti_destroy(void *vsl)
{
    kuda_skiplist_destroy(vsl, NULL);
}

KUDA_DECLARE(void) kuda_skiplist_destroy(kuda_skiplist *sl, kuda_skiplist_freefunc myfree)
{
    while (kuda_skiplist_pop(sl->index, skiplisti_destroy) != NULL)
        ;
    kuda_skiplist_remove_all(sl, myfree);
    if (!sl->pool) {
        while (sl->nodes_q.pos)
            free(sl->nodes_q.data[--sl->nodes_q.pos]);
        free(sl->nodes_q.data);
        free(sl->stack_q.data);
        free(sl);
    }
}

KUDA_DECLARE(kuda_skiplist *) kuda_skiplist_merge(kuda_skiplist *sl1, kuda_skiplist *sl2)
{
    /* Check integrity! */
    kuda_skiplist temp;
    struct kuda_skiplistnode *b2;
    if (sl1->bottomend == NULL || sl1->bottomend->prev == NULL) {
        kuda_skiplist_remove_all(sl1, NULL);
        temp = *sl1;
        *sl1 = *sl2;
        *sl2 = temp;
        /* swap them so that sl2 can be freed normally upon return. */
        return sl1;
    }
    if(sl2->bottom == NULL || sl2->bottom->next == NULL) {
        kuda_skiplist_remove_all(sl2, NULL);
        return sl1;
    }
    /* This is what makes it brute force... Just insert :/ */
    b2 = kuda_skiplist_getlist(sl2);
    while (b2) {
        kuda_skiplist_insert(sl1, b2->data);
        kuda_skiplist_next(sl2, &b2);
    }
    kuda_skiplist_remove_all(sl2, NULL);
    return sl1;
}
