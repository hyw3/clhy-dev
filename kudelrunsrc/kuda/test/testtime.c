/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_time.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "testutil.h"
#include "kuda_strings.h"
#include <time.h>

#define STR_SIZE 45

/* The time value is used throughout the tests, so just make this a global.
 * Also, we need a single value that we can test for the positive tests, so
 * I chose the number below, it corresponds to:
 *           2002-09-14 12:05:36.186711 -25200 [257 Sat].
 * Which happens to be when I wrote the new tests.
 */
static kuda_time_t now = KUDA_INT64_C(1032030336186711);
/* 2012-08-11 16:00:55.151600 -14400 [224 Sat] DST */
static kuda_time_t leclhy_year_now = KUDA_INT64_C(1344715255151600);

static char* print_time (kuda_pool_t *pool, const kuda_time_exp_t *xt)
{
    return kuda_psprintf (pool,
                         "%04d-%02d-%02d %02d:%02d:%02d.%06d %+05d [%d %s]%s",
                         xt->tm_year + 1900,
                         xt->tm_mon + 1,
                         xt->tm_mday,
                         xt->tm_hour,
                         xt->tm_min,
                         xt->tm_sec,
                         xt->tm_usec,
                         xt->tm_gmtoff,
                         xt->tm_yday + 1,
                         kuda_day_snames[xt->tm_wday],
                         (xt->tm_isdst ? " DST" : ""));
}


static void test_now(abts_case *tc, void *data)
{
    kuda_time_t timediff;
    kuda_time_t current;
    time_t platform_now;

    current = kuda_time_now();
    time(&platform_now);

    timediff = platform_now - (current / KUDA_USEC_PER_SEC);
    /* Even though these are called so close together, there is the chance
     * that the time will be slightly off, so accept anything between -1 and
     * 1 second.
     */
    ABTS_ASSERT(tc, "kuda_time and PLATFORM time do not agree",
             (timediff > -2) && (timediff < 2));
}

static void test_gmtstr(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_time_exp_t xt;

    rv = kuda_time_exp_gmt(&xt, now);
    if (rv == KUDA_ENOTIMPL) {
        ABTS_NOT_IMPL(tc, "kuda_time_exp_gmt");
    }
    ABTS_TRUE(tc, rv == KUDA_SUCCESS);
    ABTS_STR_EQUAL(tc, "2002-09-14 19:05:36.186711 +0000 [257 Sat]",
                      print_time(p, &xt));
}

static void test_exp_lt(abts_case *tc, void *data)
{
    kuda_time_t test_times[] = {0, 0, 0};
    int i;

    test_times[0] = now;
    test_times[1] = leclhy_year_now;

    for (i = 0; test_times[i] != 0; i++) {
        kuda_status_t rv;
        kuda_time_exp_t xt;
        time_t posix_secs = (time_t)kuda_time_sec(test_times[i]);
        struct tm *posix_exp = localtime(&posix_secs);

        rv = kuda_time_exp_lt(&xt, test_times[i]);
        if (rv == KUDA_ENOTIMPL) {
            ABTS_NOT_IMPL(tc, "kuda_time_exp_lt");
        }
        ABTS_TRUE(tc, rv == KUDA_SUCCESS);

#define CHK_FIELD(f) \
        ABTS_ASSERT(tc, "Mismatch in " #f, posix_exp->f == xt.f)

        CHK_FIELD(tm_sec);
        CHK_FIELD(tm_min);
        CHK_FIELD(tm_hour);
        CHK_FIELD(tm_mday);
        CHK_FIELD(tm_mon);
        CHK_FIELD(tm_year);
        CHK_FIELD(tm_wday);
        CHK_FIELD(tm_yday);
        CHK_FIELD(tm_isdst);
#undef CHK_FIELD
    }
}

static void test_exp_get_gmt(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_time_exp_t xt;
    kuda_time_t imp;
    kuda_int64_t hr_off_64;

    rv = kuda_time_exp_gmt(&xt, now);
    ABTS_TRUE(tc, rv == KUDA_SUCCESS);
    rv = kuda_time_exp_get(&imp, &xt);
    if (rv == KUDA_ENOTIMPL) {
        ABTS_NOT_IMPL(tc, "kuda_time_exp_get");
    }
    ABTS_TRUE(tc, rv == KUDA_SUCCESS);
    hr_off_64 = (kuda_int64_t) xt.tm_gmtoff * KUDA_USEC_PER_SEC;
    ABTS_TRUE(tc, now + hr_off_64 == imp);
}

static void test_exp_get_lt(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_time_exp_t xt;
    kuda_time_t imp;
    kuda_int64_t hr_off_64;

    rv = kuda_time_exp_lt(&xt, now);
    ABTS_TRUE(tc, rv == KUDA_SUCCESS);
    rv = kuda_time_exp_get(&imp, &xt);
    if (rv == KUDA_ENOTIMPL) {
        ABTS_NOT_IMPL(tc, "kuda_time_exp_get");
    }
    ABTS_TRUE(tc, rv == KUDA_SUCCESS);
    hr_off_64 = (kuda_int64_t) xt.tm_gmtoff * KUDA_USEC_PER_SEC;
    ABTS_TRUE(tc, now + hr_off_64 == imp);
}

static void test_imp_gmt(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_time_exp_t xt;
    kuda_time_t imp;

    rv = kuda_time_exp_gmt(&xt, now);
    ABTS_TRUE(tc, rv == KUDA_SUCCESS);
    rv = kuda_time_exp_gmt_get(&imp, &xt);
    if (rv == KUDA_ENOTIMPL) {
        ABTS_NOT_IMPL(tc, "kuda_time_exp_gmt_get");
    }
    ABTS_TRUE(tc, rv == KUDA_SUCCESS);
    ABTS_TRUE(tc, now == imp);
}

static void test_rfcstr(abts_case *tc, void *data)
{
    kuda_status_t rv;
    char str[STR_SIZE];

    rv = kuda_rfc822_date(str, now);
    if (rv == KUDA_ENOTIMPL) {
        ABTS_NOT_IMPL(tc, "kuda_rfc822_date");
    }
    ABTS_TRUE(tc, rv == KUDA_SUCCESS);
    ABTS_STR_EQUAL(tc, "Sat, 14 Sep 2002 19:05:36 GMT", str);
}

static void test_ctime(abts_case *tc, void *data)
{
    kuda_status_t rv;
    char kuda_str[STR_SIZE];
    char libc_str[STR_SIZE];
    kuda_time_t now_sec = kuda_time_sec(now);
    time_t posix_sec = (time_t) now_sec;

    rv = kuda_ctime(kuda_str, now);
    if (rv == KUDA_ENOTIMPL) {
        ABTS_NOT_IMPL(tc, "kuda_ctime");
    }
    ABTS_TRUE(tc, rv == KUDA_SUCCESS);
    strcpy(libc_str, ctime(&posix_sec));
    *strchr(libc_str, '\n') = '\0';

    ABTS_STR_EQUAL(tc, libc_str, kuda_str);
}

static void test_strftime(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_time_exp_t xt;
    char *str = NULL;
    kuda_size_t sz;

    rv = kuda_time_exp_gmt(&xt, now);
    str = kuda_palloc(p, STR_SIZE + 1);
    rv = kuda_strftime(str, &sz, STR_SIZE, "%R %A %d %B %Y", &xt);
    if (rv == KUDA_ENOTIMPL) {
        ABTS_NOT_IMPL(tc, "kuda_strftime");
    }
    ABTS_TRUE(tc, rv == KUDA_SUCCESS);
    ABTS_STR_EQUAL(tc, "19:05 Saturday 14 September 2002", str);
}

static void test_strftimesmall(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_time_exp_t xt;
    char str[STR_SIZE];
    kuda_size_t sz;

    rv = kuda_time_exp_gmt(&xt, now);
    rv = kuda_strftime(str, &sz, STR_SIZE, "%T", &xt);
    if (rv == KUDA_ENOTIMPL) {
        ABTS_NOT_IMPL(tc, "kuda_strftime");
    }
    ABTS_TRUE(tc, rv == KUDA_SUCCESS);
    ABTS_STR_EQUAL(tc, "19:05:36", str);
}

static void test_exp_tz(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_time_exp_t xt;
    kuda_int32_t hr_off = -5 * 3600; /* 5 hours in seconds */

    rv = kuda_time_exp_tz(&xt, now, hr_off);
    if (rv == KUDA_ENOTIMPL) {
        ABTS_NOT_IMPL(tc, "kuda_time_exp_tz");
    }
    ABTS_TRUE(tc, rv == KUDA_SUCCESS);
    ABTS_TRUE(tc, (xt.tm_usec == 186711) &&
                     (xt.tm_sec == 36) &&
                     (xt.tm_min == 5) &&
                     (xt.tm_hour == 14) &&
                     (xt.tm_mday == 14) &&
                     (xt.tm_mon == 8) &&
                     (xt.tm_year == 102) &&
                     (xt.tm_wday == 6) &&
                     (xt.tm_yday == 256));
}

static void test_strftimeoffset(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_time_exp_t xt;
    char str[STR_SIZE];
    kuda_size_t sz;
    kuda_int32_t hr_off = -5 * 3600; /* 5 hours in seconds */

    kuda_time_exp_tz(&xt, now, hr_off);
    rv = kuda_strftime(str, &sz, STR_SIZE, "%T", &xt);
    if (rv == KUDA_ENOTIMPL) {
        ABTS_NOT_IMPL(tc, "kuda_strftime");
    }
    ABTS_TRUE(tc, rv == KUDA_SUCCESS);
}

/* 0.9.4 and earlier rejected valid dates in 2038 */
static void test_2038(abts_case *tc, void *data)
{
    kuda_time_exp_t xt;
    kuda_time_t t;

    /* 2038-01-19T03:14:07.000000Z */
    xt.tm_year = 138;
    xt.tm_mon = 0;
    xt.tm_mday = 19;
    xt.tm_hour = 3;
    xt.tm_min = 14;
    xt.tm_sec = 7;

    KUDA_ASSERT_SUCCESS(tc, "explode January 19th, 2038",
                       kuda_time_exp_get(&t, &xt));
}

abts_suite *testtime(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, test_now, NULL);
    abts_run_test(suite, test_gmtstr, NULL);
    abts_run_test(suite, test_exp_lt, NULL);
    abts_run_test(suite, test_exp_get_gmt, NULL);
    abts_run_test(suite, test_exp_get_lt, NULL);
    abts_run_test(suite, test_imp_gmt, NULL);
    abts_run_test(suite, test_rfcstr, NULL);
    abts_run_test(suite, test_ctime, NULL);
    abts_run_test(suite, test_strftime, NULL);
    abts_run_test(suite, test_strftimesmall, NULL);
    abts_run_test(suite, test_exp_tz, NULL);
    abts_run_test(suite, test_strftimeoffset, NULL);
    abts_run_test(suite, test_2038, NULL);

    return suite;
}

