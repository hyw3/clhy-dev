/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "kuda.h"
#include "testutil.h"
#include "kuda_general.h"
#include "kuda_pools.h"
#include "kuda_errno.h"
#include "kuda_dso.h"
#include "kuda_strings.h"
#include "kuda_file_info.h"
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif

#if KUDA_HAS_DSO

#ifdef NETWARE
# define CAPI_NAME "capi_test.nlm"
#elif defined(BEOS) || defined(__MVS__)
# define CAPI_NAME "capi_test.so"
#elif defined(WIN32)
# define CAPI_NAME TESTBINPATH "capi_test.dll"
#elif defined(DARWIN)
# define CAPI_NAME ".libs/capi_test.so" 
# define LIB_NAME ".libs/libcapi_test.dylib" 
#elif (defined(__hpux__) || defined(__hpux)) && !defined(__ia64)
# define CAPI_NAME ".libs/capi_test.sl"
# define LIB_NAME ".libs/libcapi_test.sl"
#elif defined(_AIX) || defined(__bsdi__)
# define CAPI_NAME ".libs/libcapi_test.so"
# define LIB_NAME ".libs/libcapi_test.so"
#else /* Every other Unix */
# define CAPI_NAME ".libs/capi_test.so"
# define LIB_NAME ".libs/libcapi_test.so"
#endif

static char *capiname;

static void test_load_capi(abts_case *tc, void *data)
{
    kuda_dso_handle_t *h = NULL;
    kuda_status_t status;
    char errstr[256];

    status = kuda_dso_load(&h, capiname, p);
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);
    ABTS_PTR_NOTNULL(tc, h);

    kuda_dso_unload(h);
}

static void test_dso_sym(abts_case *tc, void *data)
{
    kuda_dso_handle_t *h = NULL;
    kuda_dso_handle_sym_t func1 = NULL;
    kuda_status_t status;
    void (*function)(char str[256]);
    char teststr[256];
    char errstr[256];

    status = kuda_dso_load(&h, capiname, p);
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);
    ABTS_PTR_NOTNULL(tc, h);

    status = kuda_dso_sym(&func1, h, "print_hello");
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);
    ABTS_PTR_NOTNULL(tc, func1);

    if (!tc->failed) {
        function = (void (*)(char *))func1;
        (*function)(teststr);
        ABTS_STR_EQUAL(tc, "Hello - I'm a DSO!\n", teststr);
    }

    kuda_dso_unload(h);
}

static void test_dso_sym_return_value(abts_case *tc, void *data)
{
    kuda_dso_handle_t *h = NULL;
    kuda_dso_handle_sym_t func1 = NULL;
    kuda_status_t status;
    int (*function)(int);
    char errstr[256];

    status = kuda_dso_load(&h, capiname, p);
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);
    ABTS_PTR_NOTNULL(tc, h);

    status = kuda_dso_sym(&func1, h, "count_reps");
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);
    ABTS_PTR_NOTNULL(tc, func1);

    if (!tc->failed) {
        function = (int (*)(int))func1;
        status = (*function)(5);
        ABTS_INT_EQUAL(tc, 5, status);
    }

    kuda_dso_unload(h);
}

static void test_unload_capi(abts_case *tc, void *data)
{
    kuda_dso_handle_t *h = NULL;
    kuda_status_t status;
    char errstr[256];
    kuda_dso_handle_sym_t func1 = NULL;

    status = kuda_dso_load(&h, capiname, p);
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);
    ABTS_PTR_NOTNULL(tc, h);

    status = kuda_dso_unload(h);
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);

    status = kuda_dso_sym(&func1, h, "print_hello");
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_ESYMNOTFOUND(status));
}


#ifdef LIB_NAME
static char *libname;

static void test_load_library(abts_case *tc, void *data)
{
    kuda_dso_handle_t *h = NULL;
    kuda_status_t status;
    char errstr[256];

    status = kuda_dso_load(&h, libname, p);
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);
    ABTS_PTR_NOTNULL(tc, h);

    kuda_dso_unload(h);
}

static void test_dso_sym_library(abts_case *tc, void *data)
{
    kuda_dso_handle_t *h = NULL;
    kuda_dso_handle_sym_t func1 = NULL;
    kuda_status_t status;
    void (*function)(char str[256]);
    char teststr[256];
    char errstr[256];

    status = kuda_dso_load(&h, libname, p);
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);
    ABTS_PTR_NOTNULL(tc, h);

    status = kuda_dso_sym(&func1, h, "print_hello");
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);
    ABTS_PTR_NOTNULL(tc, func1);

    if (!tc->failed) {
        function = (void (*)(char *))func1;
        (*function)(teststr);
        ABTS_STR_EQUAL(tc, "Hello - I'm a DSO!\n", teststr);
    }

    kuda_dso_unload(h);
}

static void test_dso_sym_return_value_library(abts_case *tc, void *data)
{
    kuda_dso_handle_t *h = NULL;
    kuda_dso_handle_sym_t func1 = NULL;
    kuda_status_t status;
    int (*function)(int);
    char errstr[256];

    status = kuda_dso_load(&h, libname, p);
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);
    ABTS_PTR_NOTNULL(tc, h);

    status = kuda_dso_sym(&func1, h, "count_reps");
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);
    ABTS_PTR_NOTNULL(tc, func1);

    if (!tc->failed) {
        function = (int (*)(int))func1;
        status = (*function)(5);
        ABTS_INT_EQUAL(tc, 5, status);
    }

    kuda_dso_unload(h);
}

static void test_unload_library(abts_case *tc, void *data)
{
    kuda_dso_handle_t *h = NULL;
    kuda_status_t status;
    char errstr[256];
    kuda_dso_handle_sym_t func1 = NULL;

    status = kuda_dso_load(&h, libname, p);
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);
    ABTS_PTR_NOTNULL(tc, h);

    status = kuda_dso_unload(h);
    ABTS_ASSERT(tc, kuda_dso_error(h, errstr, 256), KUDA_SUCCESS == status);

    status = kuda_dso_sym(&func1, h, "print_hello");
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_ESYMNOTFOUND(status));
}

#endif /* def(LIB_NAME) */

static void test_load_notthere(abts_case *tc, void *data)
{
    kuda_dso_handle_t *h = NULL;
    kuda_status_t status;

    status = kuda_dso_load(&h, "No_File.so", p);

    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_EDSOOPEN(status));
    ABTS_PTR_NOTNULL(tc, h);
}    

#endif /* KUDA_HAS_DSO */

abts_suite *testdso(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

#if KUDA_HAS_DSO
    kuda_filepath_merge(&capiname, NULL, CAPI_NAME, 0, p);
    
    abts_run_test(suite, test_load_capi, NULL);
    abts_run_test(suite, test_dso_sym, NULL);
    abts_run_test(suite, test_dso_sym_return_value, NULL);
    abts_run_test(suite, test_unload_capi, NULL);

#ifdef LIB_NAME
    kuda_filepath_merge(&libname, NULL, LIB_NAME, 0, p);
    
    abts_run_test(suite, test_load_library, NULL);
    abts_run_test(suite, test_dso_sym_library, NULL);
    abts_run_test(suite, test_dso_sym_return_value_library, NULL);
    abts_run_test(suite, test_unload_library, NULL);
#endif

    abts_run_test(suite, test_load_notthere, NULL);
#endif /* KUDA_HAS_DSO */

    return suite;
}

