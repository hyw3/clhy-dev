/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_network_io.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "testutil.h"

#define STRLEN 21

static void tcp_socket(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_socket_t *sock = NULL;
    int type;

    rv = kuda_socket_create(&sock, KUDA_INET, SOCK_STREAM, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, sock);

    rv = kuda_socket_type_get(sock, &type);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_INT_EQUAL(tc, SOCK_STREAM, type);

    kuda_socket_close(sock);
}

static void udp_socket(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_socket_t *sock = NULL;
    int type;

    rv = kuda_socket_create(&sock, KUDA_INET, SOCK_DGRAM, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, sock);

    rv = kuda_socket_type_get(sock, &type);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_INT_EQUAL(tc, SOCK_DGRAM, type);

    kuda_socket_close(sock);
}

#if KUDA_HAVE_IPV6
static void tcp6_socket(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_socket_t *sock = NULL;

    rv = kuda_socket_create(&sock, KUDA_INET6, SOCK_STREAM, 0, p);
    if (KUDA_STATUS_IS_EAFNOSUPPORT(rv)) {
        ABTS_NOT_IMPL(tc, "IPv6 not enabled");
        return;
    }
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, sock);
    kuda_socket_close(sock);
}

static void udp6_socket(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_socket_t *sock = NULL;

    rv = kuda_socket_create(&sock, KUDA_INET6, SOCK_DGRAM, 0, p);
    if (KUDA_STATUS_IS_EAFNOSUPPORT(rv)) {
        ABTS_NOT_IMPL(tc, "IPv6 not enabled");
        return;
    }
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, sock);
    kuda_socket_close(sock);
}
#endif

static void sendto_receivefrom_helper(abts_case *tc, const char *addr,
                                      int family)
{
    kuda_status_t rv;
    kuda_socket_t *sock = NULL;
    kuda_socket_t *sock2 = NULL;
    char sendbuf[STRLEN] = "KUDA_INET, SOCK_DGRAM";
    char recvbuf[80];
    char *ip_addr;
    kuda_port_t fromport;
    kuda_sockaddr_t *from;
    kuda_sockaddr_t *to;
    kuda_size_t len = 30;

    rv = kuda_socket_create(&sock, family, SOCK_DGRAM, 0, p);
#if KUDA_HAVE_IPV6
    if ((family == KUDA_INET6) && KUDA_STATUS_IS_EAFNOSUPPORT(rv)) {
        ABTS_NOT_IMPL(tc, "IPv6 not enabled");
        return;
    }
#endif
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    if (rv != KUDA_SUCCESS)
        return;
    rv = kuda_socket_create(&sock2, family, SOCK_DGRAM, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    if (rv != KUDA_SUCCESS)
        return;

    rv = kuda_sockaddr_info_get(&to, addr, family, 7772, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_sockaddr_info_get(&from, addr, family, 7771, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_socket_opt_set(sock, KUDA_SO_REUSEADDR, 1);
    KUDA_ASSERT_SUCCESS(tc, "Could not set REUSEADDR on socket", rv);
    rv = kuda_socket_opt_set(sock2, KUDA_SO_REUSEADDR, 1);
    KUDA_ASSERT_SUCCESS(tc, "Could not set REUSEADDR on socket2", rv);

    rv = kuda_socket_bind(sock, to);
    KUDA_ASSERT_SUCCESS(tc, "Could not bind socket", rv);
    if (rv != KUDA_SUCCESS)
        return;
    rv = kuda_mcast_hops(sock, 10);
    KUDA_ASSERT_SUCCESS(tc, "Could not set multicast hops", rv);
    if (rv != KUDA_SUCCESS)
        return;

    rv = kuda_socket_bind(sock2, from);
    KUDA_ASSERT_SUCCESS(tc, "Could not bind second socket", rv);
    if (rv != KUDA_SUCCESS)
        return;

    len = STRLEN;
    rv = kuda_socket_sendto(sock2, to, 0, sendbuf, &len);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, STRLEN, len);

    /* fill the "from" sockaddr with a random address from another
     * family to ensure that recvfrom sets it up properly. */
#if KUDA_HAVE_IPV6
    if (family == KUDA_INET)
        rv = kuda_sockaddr_info_get(&from, "3ffE:816e:abcd:1234::1",
                                   KUDA_INET6, 4242, 0, p);
    else
#endif
        rv = kuda_sockaddr_info_get(&from, "127.1.2.3", KUDA_INET, 4242, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    len = 80;
    rv = kuda_socket_recvfrom(from, sock, 0, recvbuf, &len);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, STRLEN, len);
    ABTS_STR_EQUAL(tc, "KUDA_INET, SOCK_DGRAM", recvbuf);

    kuda_sockaddr_ip_get(&ip_addr, from);
    fromport = from->port;
    ABTS_STR_EQUAL(tc, addr, ip_addr);
    ABTS_INT_EQUAL(tc, 7771, fromport);

    kuda_socket_close(sock);
    kuda_socket_close(sock2);
}

static void sendto_receivefrom(abts_case *tc, void *data)
{
    int failed;
    sendto_receivefrom_helper(tc, "127.0.0.1", KUDA_INET);
    failed = tc->failed; tc->failed = 0;
    ABTS_TRUE(tc, !failed);
}

#if KUDA_HAVE_IPV6
static void sendto_receivefrom6(abts_case *tc, void *data)
{
    int failed;
    sendto_receivefrom_helper(tc, "::1", KUDA_INET6);
    failed = tc->failed; tc->failed = 0;
    ABTS_TRUE(tc, !failed);
}
#endif

static void socket_userdata(abts_case *tc, void *data)
{
    kuda_socket_t *sock1, *sock2;
    kuda_status_t rv;
    void *user;
    const char *key = "GENERICKEY";

    rv = kuda_socket_create(&sock1, AF_INET, SOCK_STREAM, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_socket_create(&sock2, AF_INET, SOCK_STREAM, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_socket_data_set(sock1, "SOCK1", key, NULL);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_socket_data_set(sock2, "SOCK2", key, NULL);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_socket_data_get(&user, key, sock1);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_STR_EQUAL(tc, "SOCK1", user);
    rv = kuda_socket_data_get(&user, key, sock2);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_STR_EQUAL(tc, "SOCK2", user);
}

abts_suite *testsockets(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, tcp_socket, NULL);
    abts_run_test(suite, udp_socket, NULL);

    abts_run_test(suite, sendto_receivefrom, NULL);

#if KUDA_HAVE_IPV6
    abts_run_test(suite, tcp6_socket, NULL);
    abts_run_test(suite, udp6_socket, NULL);

    abts_run_test(suite, sendto_receivefrom6, NULL);
#endif

    abts_run_test(suite, socket_userdata, NULL);
    
    return suite;
}

