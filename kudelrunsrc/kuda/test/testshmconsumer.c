/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_shm.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_time.h"
#include "testshm.h"
#include "kuda.h"

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif


#if KUDA_HAS_SHARED_MEMORY

static int msgwait(int sleep_sec, int first_box, int last_box)
{
    int i;
    int recvd = 0;
    kuda_time_t start = kuda_time_now();
    kuda_interval_time_t sleep_duration = kuda_time_from_sec(sleep_sec);
    while (kuda_time_now() - start < sleep_duration) {
        for (i = first_box; i < last_box; i++) {
            if (boxes[i].msgavail && !strcmp(boxes[i].msg, MSG)) {
                recvd++;
                boxes[i].msgavail = 0; /* reset back to 0 */
                memset(boxes[i].msg, 0, 1024);
            }
        }
        kuda_sleep(kuda_time_from_sec(1));
    }
    return recvd;
}

int main(void)
{
    kuda_status_t rv;
    kuda_pool_t *pool;
    kuda_shm_t *shm;
    int recvd;

    kuda_initialize();
    
    if (kuda_pool_create(&pool, NULL) != KUDA_SUCCESS) {
        exit(-1);
    }

    rv = kuda_shm_attach(&shm, SHARED_FILENAME, pool);
    if (rv != KUDA_SUCCESS) {
        exit(-2);
    }

    boxes = kuda_shm_baseaddr_get(shm);

    /* consume messages on all of the boxes */
    recvd = msgwait(30, 0, N_BOXES); /* wait for 30 seconds for messages */

    rv = kuda_shm_detach(shm);
    if (rv != KUDA_SUCCESS) {
        exit(-3);
    }

    return recvd;
}

#else /* KUDA_HAS_SHARED_MEMORY */

int main(void)
{
    /* Just return, this program will never be called, so we don't need
     * to print a message 
     */
    return 0;
}

#endif /* KUDA_HAS_SHARED_MEMORY */

