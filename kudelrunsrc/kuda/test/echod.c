/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Simple echo daemon, designed to be used for network throughput
 * benchmarks. The aim is to allow us to monitor changes in performance
 * of KUDA networking code, nothing more.
 */

#include <stdio.h>
#include <stdlib.h>  /* for atexit() */

#include "kuda.h"
#include "kuda_network_io.h"
#include "kuda_strings.h"

#define BUF_SIZE 4096

static void reportError(const char *msg, kuda_status_t rv, 
                        kuda_pool_t *pool)
{
    fprintf(stderr, "%s\nError: %d\n'%s'\n", msg, rv,
            kuda_psprintf(pool, "%pm", &rv));
}

static kuda_status_t talkTalk(kuda_socket_t *socket, kuda_pool_t *parent)
{
    kuda_pool_t *pool;
    kuda_size_t len;
    char *buf;
    kuda_status_t rv;

    if (kuda_pool_create(&pool, parent) != KUDA_SUCCESS)
        return KUDA_ENOPOOL;


    buf = kuda_palloc(pool, BUF_SIZE);
    if (!buf)
        return ENOMEM;

    do {
        len = BUF_SIZE;
        rv = kuda_socket_recv(socket, buf, &len);
        if (KUDA_STATUS_IS_EOF(rv) || len == 0 || rv != KUDA_SUCCESS)
            break;
        rv = kuda_socket_send(socket, buf, &len);
        if (len == 0 || rv != KUDA_SUCCESS)
            break;
    } while (rv == KUDA_SUCCESS);

    kuda_pool_clear(pool);
    return KUDA_SUCCESS;
}

static kuda_status_t glassToWall(kuda_port_t port, kuda_pool_t *parent)
{
    kuda_sockaddr_t *sockAddr;
    kuda_socket_t *listener, *accepted;
    kuda_status_t rv;

    rv = kuda_socket_create(&listener, KUDA_INET, SOCK_STREAM, KUDA_PROTO_TCP,
                           parent);
    if (rv != KUDA_SUCCESS) {
        reportError("Unable to create socket", rv, parent);
        return rv;
    }

    rv = kuda_sockaddr_info_get(&sockAddr, "127.0.0.1", KUDA_UNSPEC,
                               port, 0, parent);
    if (rv != KUDA_SUCCESS) {
        reportError("Unable to get socket info", rv, parent);
        kuda_socket_close(listener);
        return rv;
    }

    if ((rv = kuda_socket_bind(listener, sockAddr)) != KUDA_SUCCESS ||
        (rv = kuda_socket_listen(listener, 5)) != KUDA_SUCCESS) {
        reportError("Unable to bind or listen to socket", rv, parent);
        kuda_socket_close(listener);
        return rv;
    }

    for (;;) {
        rv = kuda_socket_accept(&accepted, listener, parent);
        if (rv != KUDA_SUCCESS) {
            reportError("Error accepting on socket", rv, parent);
            break;
        }
        printf("\tAnswering connection\n");
        rv = talkTalk(accepted, parent);
        kuda_socket_close(accepted);
        printf("\tConnection closed\n");
        if (rv != KUDA_SUCCESS)
            break;
    }

    kuda_socket_close(listener);
    return KUDA_SUCCESS;
}

int main(int argc, char **argv)
{
    kuda_pool_t *pool;
    kuda_port_t theport = 4747;

    printf("KUDA Test Application: echod\n");

    kuda_initialize();
    atexit(kuda_terminate);

    kuda_pool_create(&pool, NULL);

    if (argc >= 2) {
        printf("argc = %d, port = '%s'\n", argc, argv[1]);
        theport = atoi(argv[1]);
    }

    fprintf(stdout, "Starting to listen on port %d\n", theport);
    glassToWall(theport, pool);

    return 0;
}
