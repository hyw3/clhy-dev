/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include "kuda_file_io.h"

int main(int argc, char *argv[])
{
    kuda_file_t *in, *out;
    kuda_size_t nbytes, total_bytes;
    kuda_pool_t *p;
    char buf[128];
    kuda_status_t rv;
    
    kuda_initialize();
    atexit(kuda_terminate);
    kuda_pool_create(&p, NULL);

    kuda_file_open_stdin(&in, p);
    kuda_file_open_stdout(&out, p);

    total_bytes = 0;
    nbytes = sizeof(buf);
    while ((rv = kuda_file_read(in, buf, &nbytes)) == KUDA_SUCCESS) {
        total_bytes += nbytes;
        nbytes = sizeof(buf);
    }

    kuda_file_printf(out, "%" KUDA_SIZE_T_FMT " bytes were read\n",
                    total_bytes);
    return 0;
}
