/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_thread_proc.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_time.h"
#include "testutil.h"

#if KUDA_HAS_THREADS

static kuda_thread_mutex_t *thread_lock;
static kuda_thread_once_t *control = NULL;
static int x = 0;
static int value = 0;

static kuda_thread_t *t1;
static kuda_thread_t *t2;
static kuda_thread_t *t3;
static kuda_thread_t *t4;

/* just some made up number to check on later */
static kuda_status_t exit_ret_val = 123;

static void init_func(void)
{
    value++;
}

static void * KUDA_THREAD_FUNC thread_func1(kuda_thread_t *thd, void *data)
{
    int i;

    kuda_thread_once(control, init_func);

    for (i = 0; i < 10000; i++) {
        kuda_thread_mutex_lock(thread_lock);
        x++;
        kuda_thread_mutex_unlock(thread_lock);
    }
    kuda_thread_exit(thd, exit_ret_val);
    return NULL;
} 

static void thread_init(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_thread_once_init(&control, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_thread_mutex_create(&thread_lock, KUDA_THREAD_MUTEX_DEFAULT, p); 
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

static void create_threads(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_thread_create(&t1, NULL, thread_func1, NULL, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_thread_create(&t2, NULL, thread_func1, NULL, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_thread_create(&t3, NULL, thread_func1, NULL, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_thread_create(&t4, NULL, thread_func1, NULL, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

static void join_threads(abts_case *tc, void *data)
{
    kuda_status_t s;

    kuda_thread_join(&s, t1);
    ABTS_INT_EQUAL(tc, exit_ret_val, s);
    kuda_thread_join(&s, t2);
    ABTS_INT_EQUAL(tc, exit_ret_val, s);
    kuda_thread_join(&s, t3);
    ABTS_INT_EQUAL(tc, exit_ret_val, s);
    kuda_thread_join(&s, t4);
    ABTS_INT_EQUAL(tc, exit_ret_val, s);
}

static void check_locks(abts_case *tc, void *data)
{
    ABTS_INT_EQUAL(tc, 40000, x);
}

static void check_thread_once(abts_case *tc, void *data)
{
    ABTS_INT_EQUAL(tc, 1, value);
}

#else

static void threads_not_impl(abts_case *tc, void *data)
{
    ABTS_NOT_IMPL(tc, "Threads not implemented on this platform");
}

#endif

abts_suite *testthread(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

#if !KUDA_HAS_THREADS
    abts_run_test(suite, threads_not_impl, NULL);
#else
    abts_run_test(suite, thread_init, NULL);
    abts_run_test(suite, create_threads, NULL);
    abts_run_test(suite, join_threads, NULL);
    abts_run_test(suite, check_locks, NULL);
    abts_run_test(suite, check_thread_once, NULL);
#endif

    return suite;
}

