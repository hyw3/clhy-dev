/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testflock.h"
#include "testutil.h"
#include "kuda_pools.h"
#include "kuda_thread_proc.h"
#include "kuda_file_io.h"
#include "kuda_file_info.h"
#include "kuda_general.h"
#include "kuda_strings.h"

static int launch_reader(abts_case *tc)
{
    kuda_proc_t proc = {0};
    kuda_procattr_t *procattr;
    const char *args[2];
    kuda_status_t rv;
    kuda_exit_why_e why;
    int exitcode;

    rv = kuda_procattr_create(&procattr, p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't create procattr", rv);

    rv = kuda_procattr_io_set(procattr, KUDA_NO_PIPE, KUDA_NO_PIPE,
            KUDA_NO_PIPE);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't set io in procattr", rv);

    rv = kuda_procattr_cmdtype_set(procattr, KUDA_PROGRAM_ENV);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't set copy environment", rv);

    rv = kuda_procattr_error_check_set(procattr, 1);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't set error check in procattr", rv);

    args[0] = "tryread" EXTENSION;
    args[1] = NULL;
    rv = kuda_proc_create(&proc, TESTBINPATH "tryread" EXTENSION, args, NULL, procattr, p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't launch program", rv);

    ABTS_ASSERT(tc, "wait for child process",
            kuda_proc_wait(&proc, &exitcode, &why, KUDA_WAIT) == KUDA_CHILD_DONE);

    ABTS_ASSERT(tc, "child terminated normally", why == KUDA_PROC_EXIT);
    return exitcode;
}

static void test_withlock(abts_case *tc, void *data)
{
    kuda_file_t *file;
    kuda_status_t rv;
    int code;
    
    rv = kuda_file_open(&file, TESTFILE, KUDA_FOPEN_WRITE|KUDA_FOPEN_CREATE,
                       KUDA_PLATFORM_DEFAULT, p);
    KUDA_ASSERT_SUCCESS(tc, "Could not create file.", rv);
    ABTS_PTR_NOTNULL(tc, file);

    rv = kuda_file_lock(file, KUDA_FLOCK_EXCLUSIVE);
    KUDA_ASSERT_SUCCESS(tc, "Could not lock the file.", rv);
    ABTS_PTR_NOTNULL(tc, file);

    code = launch_reader(tc);
    ABTS_INT_EQUAL(tc, FAILED_READ, code);

    (void) kuda_file_close(file);
}

static void test_withoutlock(abts_case *tc, void *data)
{
    int code;
    
    code = launch_reader(tc);
    ABTS_INT_EQUAL(tc, SUCCESSFUL_READ, code);
}

static void remove_lockfile(abts_case *tc, void *data)
{
    KUDA_ASSERT_SUCCESS(tc, "Couldn't remove lock file.",
                       kuda_file_remove(TESTFILE, p));
}
    
abts_suite *testflock(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, test_withlock, NULL);
    abts_run_test(suite, test_withoutlock, NULL);
    abts_run_test(suite, remove_lockfile, NULL);

    return suite;
}
