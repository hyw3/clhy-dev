/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testutil.h"
#include "kuda.h"
#include "kuda_portable.h"
#include "kuda_strings.h"

static void ssize_t_fmt(abts_case *tc, void *data)
{
    char buf[100];
    kuda_ssize_t var = 0;

    sprintf(buf, "%" KUDA_SSIZE_T_FMT, var);
    ABTS_STR_EQUAL(tc, "0", buf);
    kuda_snprintf(buf, sizeof(buf), "%" KUDA_SSIZE_T_FMT, var);
    ABTS_STR_EQUAL(tc, "0", buf);
}

static void size_t_fmt(abts_case *tc, void *data)
{
    char buf[100];
    kuda_size_t var = 0;

    sprintf(buf, "%" KUDA_SIZE_T_FMT, var);
    ABTS_STR_EQUAL(tc, "0", buf);
    kuda_snprintf(buf, sizeof(buf), "%" KUDA_SIZE_T_FMT, var);
    ABTS_STR_EQUAL(tc, "0", buf);
}

static void time_t_fmt(abts_case *tc, void *data)
{
    char buf[100];
    kuda_time_t var = 1;

    sprintf(buf, "%" KUDA_TIME_T_FMT, var);
    ABTS_STR_EQUAL(tc, "1", buf);
    kuda_snprintf(buf, sizeof(buf), "%" KUDA_TIME_T_FMT, var);
    ABTS_STR_EQUAL(tc, "1", buf);
}

static void off_t_fmt(abts_case *tc, void *data)
{
    char buf[100];
    kuda_off_t var = 0;

    sprintf(buf, "%" KUDA_OFF_T_FMT, var);
    ABTS_STR_EQUAL(tc, "0", buf);
    kuda_snprintf(buf, sizeof(buf), "%" KUDA_OFF_T_FMT, var);
    ABTS_STR_EQUAL(tc, "0", buf);
}

static void pid_t_fmt(abts_case *tc, void *data)
{
    char buf[100];
    pid_t var = 0;

    sprintf(buf, "%" KUDA_PID_T_FMT, var);
    ABTS_STR_EQUAL(tc, "0", buf);
    kuda_snprintf(buf, sizeof(buf), "%" KUDA_PID_T_FMT, var);
    ABTS_STR_EQUAL(tc, "0", buf);
}

static void int64_t_fmt(abts_case *tc, void *data)
{
    char buf[100];
    kuda_int64_t var = 0;

    sprintf(buf, "%" KUDA_INT64_T_FMT, var);
    ABTS_STR_EQUAL(tc, "0", buf);
    kuda_snprintf(buf, sizeof(buf), "%" KUDA_INT64_T_FMT, var);
    ABTS_STR_EQUAL(tc, "0", buf);
}

static void uint64_t_fmt(abts_case *tc, void *data)
{
    char buf[100];
    kuda_uint64_t var = KUDA_UINT64_C(14000000);

    sprintf(buf, "%" KUDA_UINT64_T_FMT, var);
    ABTS_STR_EQUAL(tc, "14000000", buf);
    kuda_snprintf(buf, sizeof(buf), "%" KUDA_UINT64_T_FMT, var);
    ABTS_STR_EQUAL(tc, "14000000", buf);
}

static void uint64_t_hex_fmt(abts_case *tc, void *data)
{
    char buf[100];
    kuda_uint64_t var = KUDA_UINT64_C(14000000);

    sprintf(buf, "%" KUDA_UINT64_T_HEX_FMT, var);
    ABTS_STR_EQUAL(tc, "d59f80", buf);
    kuda_snprintf(buf, sizeof(buf), "%" KUDA_UINT64_T_HEX_FMT, var);
    ABTS_STR_EQUAL(tc, "d59f80", buf);
}

static void more_int64_fmts(abts_case *tc, void *data)
{
    char buf[100];
    kuda_int64_t i = KUDA_INT64_C(-42);
    kuda_int64_t ibig = KUDA_INT64_C(-314159265358979323);
    kuda_uint64_t ui = KUDA_UINT64_C(42);
    kuda_uint64_t big = KUDA_UINT64_C(10267677267010969076);

    kuda_snprintf(buf, sizeof buf, "%" KUDA_INT64_T_FMT, i);
    ABTS_STR_EQUAL(tc, "-42", buf);

    kuda_snprintf(buf, sizeof buf, "%" KUDA_UINT64_T_FMT, ui);
    ABTS_STR_EQUAL(tc, "42", buf);

    kuda_snprintf(buf, sizeof buf, "%" KUDA_UINT64_T_FMT, big);
    ABTS_STR_EQUAL(tc, "10267677267010969076", buf);

    kuda_snprintf(buf, sizeof buf, "%" KUDA_INT64_T_FMT, ibig);
    ABTS_STR_EQUAL(tc, "-314159265358979323", buf);
}

static void error_fmt(abts_case *tc, void *data)
{
    char ebuf[150], sbuf[150], *s;
    kuda_status_t rv;

    rv = KUDA_SUCCESS;
    kuda_strerror(rv, ebuf, sizeof ebuf);
    kuda_snprintf(sbuf, sizeof sbuf, "%pm", &rv);
    ABTS_STR_EQUAL(tc, sbuf, ebuf);

    rv = KUDA_ENOTIMPL;
    s = kuda_pstrcat(p, "foo-",
                    kuda_strerror(rv, ebuf, sizeof ebuf),
                    "-bar", NULL);
    kuda_snprintf(sbuf, sizeof sbuf, "foo-%pm-bar", &rv);
    ABTS_STR_EQUAL(tc, sbuf, s);
}

abts_suite *testfmt(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, ssize_t_fmt, NULL);
    abts_run_test(suite, size_t_fmt, NULL);
    abts_run_test(suite, time_t_fmt, NULL);
    abts_run_test(suite, off_t_fmt, NULL);
    abts_run_test(suite, pid_t_fmt, NULL);
    abts_run_test(suite, int64_t_fmt, NULL);
    abts_run_test(suite, uint64_t_fmt, NULL);
    abts_run_test(suite, uint64_t_hex_fmt, NULL);
    abts_run_test(suite, more_int64_fmts, NULL);
    abts_run_test(suite, error_fmt, NULL);

    return suite;
}

