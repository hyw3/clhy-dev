/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* sockperf.c
 * This simple network client tries to connect to an echo daemon (echod)
 * listening on a port it supplies, then time how long it takes to
 * reply with packets of varying sizes.
 * It prints results once completed.
 *
 * To run,
 *
 *   ./echod &
 *   ./sockperf
 */

#include <stdio.h>
#include <stdlib.h>  /* for atexit() */

#include "kuda.h"
#include "kuda_network_io.h"
#include "kuda_strings.h"

#define MAX_ITERS    10
#define TEST_SIZE  1024

struct testSet {
    char c;
    kuda_size_t size;
    int iters;
} testRuns[] = {
    { 'a', 1, 3 },
    { 'b', 4, 3 },
    { 'c', 16, 5 },
    { 'd', 64, 5 },
    { 'e', 256, 10 },
};

struct testResult {
    int size;
    int iters;
    kuda_time_t msecs[MAX_ITERS];
    kuda_time_t avg;
};

static kuda_int16_t testPort = 4747;
static kuda_sockaddr_t *sockAddr = NULL;

static void reportError(const char *msg, kuda_status_t rv, 
                        kuda_pool_t *pool)
{
    fprintf(stderr, "%s\n", msg);
    if (rv != KUDA_SUCCESS)
        fprintf(stderr, "Error: %d\n'%s'\n", rv,
                kuda_psprintf(pool, "%pm", &rv));
    
}

static void closeConnection(kuda_socket_t *sock)
{
    kuda_size_t len = 0;
    kuda_socket_send(sock, NULL, &len);
}

static kuda_status_t sendRecvBuffer(kuda_time_t *t, const char *buf, 
                                   kuda_size_t size, kuda_pool_t *pool)
{
    kuda_socket_t *sock;
    kuda_status_t rv;
    kuda_size_t len = size, thistime = size;
    char *recvBuf;
    kuda_time_t testStart = kuda_time_now(), testEnd;
    int i;

    if (! sockAddr) {
        rv = kuda_sockaddr_info_get(&sockAddr, "127.0.0.1", KUDA_UNSPEC,
                                   testPort, 0, pool);
        if (rv != KUDA_SUCCESS) {
            reportError("Unable to get socket info", rv, pool);
            return rv;
        }

        /* make sure we can connect to daemon before we try tests */

        rv = kuda_socket_create(&sock, KUDA_INET, SOCK_STREAM, KUDA_PROTO_TCP,
                           pool);
        if (rv != KUDA_SUCCESS) {
            reportError("Unable to create IPv4 stream socket", rv, pool);
            return rv;
        }

        rv = kuda_socket_connect(sock, sockAddr);
        if (rv != KUDA_SUCCESS) {
            reportError("Unable to connect to echod!", rv, pool);
            kuda_socket_close(sock);
            return rv;
        }
        kuda_socket_close(sock);

    }

    recvBuf = kuda_palloc(pool, size);
    if (! recvBuf) {
        reportError("Unable to allocate buffer", ENOMEM, pool);
        return ENOMEM;
    }

    *t = 0;

    /* START! */
    testStart = kuda_time_now();
    rv = kuda_socket_create(&sock, KUDA_INET, SOCK_STREAM, KUDA_PROTO_TCP,
                           pool);
    if (rv != KUDA_SUCCESS) {
        reportError("Unable to create IPv4 stream socket", rv, pool);
        return rv;
    }

    rv = kuda_socket_connect(sock, sockAddr);
    if (rv != KUDA_SUCCESS) {
        reportError("Unable to connect to echod!", rv, pool);
        kuda_socket_close(sock);
        return rv;
    }

    for (i = 0; i < 3; i++) {

        len = size;
        thistime = size;

        rv = kuda_socket_send(sock, buf, &len);
        if (rv != KUDA_SUCCESS || len != size) {
            reportError(kuda_psprintf(pool, 
                         "Unable to send data correctly (iteration %d of 3)",
                         i) , rv, pool);
            closeConnection(sock);
            kuda_socket_close(sock);
            return rv;
        }
    
        do {
            len = thistime;
            rv = kuda_socket_recv(sock, &recvBuf[size - thistime], &len);
            if (rv != KUDA_SUCCESS) {
                reportError("Error receiving from socket", rv, pool);
                break;
            }
            thistime -= len;
        } while (thistime);
    }

    closeConnection(sock);
    kuda_socket_close(sock);
    testEnd = kuda_time_now();
    /* STOP! */

    if (thistime) {
        reportError("Received less than we sent :-(", rv, pool);
        return rv;
    }        
    if (strncmp(recvBuf, buf, size) != 0) {
        reportError("Received corrupt data :-(", 0, pool);
        printf("We sent:\n%s\nWe received:\n%s\n", buf, recvBuf);
        return EINVAL;
    }
    *t = testEnd - testStart;
    return KUDA_SUCCESS;
}

static kuda_status_t runTest(struct testSet *ts, struct testResult *res,
                            kuda_pool_t *pool)
{
    char *buffer;
    kuda_status_t rv = KUDA_SUCCESS;
    int i;
    kuda_size_t sz = ts->size * TEST_SIZE;
    
    buffer = kuda_palloc(pool, sz);
    if (!buffer) {
        reportError("Unable to allocate buffer", ENOMEM, pool);
        return ENOMEM;
    }
    memset(buffer, ts->c, sz);

    res->iters = ts->iters > MAX_ITERS ? MAX_ITERS : ts->iters;

    for (i = 0; i < res->iters; i++) {
        kuda_time_t iterTime;
        rv = sendRecvBuffer(&iterTime, buffer, sz, pool);
        if (rv != KUDA_SUCCESS) {
            res->iters = i;
            break;
        }
        res->msecs[i] = iterTime;
    }

    return rv;
}

int main(int argc, char **argv)
{
    kuda_pool_t *pool;
    kuda_status_t rv;
    int i;
    int nTests = sizeof(testRuns) / sizeof(testRuns[0]);
    struct testResult *results;

    printf("KUDA Test Application: sockperf\n");

    kuda_initialize();
    atexit(kuda_terminate);

    kuda_pool_create(&pool, NULL);

    results = (struct testResult *)kuda_pcalloc(pool, 
                                        sizeof(*results) * nTests);

    for (i = 0; i < nTests; i++) {
        printf("Test -> %c\n", testRuns[i].c);
        results[i].size = testRuns[i].size * (kuda_size_t)TEST_SIZE;
        rv = runTest(&testRuns[i], &results[i], pool);
        if (rv != KUDA_SUCCESS) {
            /* error already reported */
            exit(1);
        }
    }

    printf("Tests Complete!\n");
    for (i = 0; i < nTests; i++) {
        int j;
        kuda_time_t totTime = 0;
        printf("%10d byte block:\n", results[i].size);
        printf("\t%2d iterations : ", results[i].iters);
        for (j = 0; j < results[i].iters; j++) {
            printf("%6" KUDA_TIME_T_FMT, results[i].msecs[j]);
            totTime += results[i].msecs[j];
        }
        printf("<\n");
        printf("\t  Average: %6" KUDA_TIME_T_FMT "\n",
               totTime / results[i].iters);
    }

    return 0;
}
