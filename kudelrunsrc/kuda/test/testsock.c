/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testutil.h"
#include "testsock.h"
#include "kuda_thread_proc.h"
#include "kuda_network_io.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_poll.h"
#define KUDA_WANT_BYTEFUNC
#include "kuda_want.h"

#define UNIX_SOCKET_NAME    "/tmp/kuda-socket"
#define IPV4_SOCKET_NAME    "127.0.0.1"
static char *socket_name = NULL;
static int   socket_type = KUDA_INET;

static void launch_child(abts_case *tc, kuda_proc_t *proc, const char *arg1, kuda_pool_t *p)
{
    kuda_procattr_t *procattr;
    const char *args[4];
    kuda_status_t rv;

    rv = kuda_procattr_create(&procattr, p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't create procattr", rv);

    rv = kuda_procattr_io_set(procattr, KUDA_NO_PIPE, KUDA_NO_PIPE,
            KUDA_NO_PIPE);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't set io in procattr", rv);

    rv = kuda_procattr_error_check_set(procattr, 1);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't set error check in procattr", rv);

    rv = kuda_procattr_cmdtype_set(procattr, KUDA_PROGRAM_ENV);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't set copy environment", rv);

    args[0] = "sockchild" EXTENSION;
    args[1] = arg1;
    args[2] = socket_name;
    args[3] = NULL;
    rv = kuda_proc_create(proc, TESTBINPATH "sockchild" EXTENSION, args, NULL,
                         procattr, p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't launch program", rv);
}

static int wait_child(abts_case *tc, kuda_proc_t *proc) 
{
    int exitcode;
    kuda_exit_why_e why;

    ABTS_ASSERT(tc, "Error waiting for child process",
            kuda_proc_wait(proc, &exitcode, &why, KUDA_WAIT) == KUDA_CHILD_DONE);

    ABTS_ASSERT(tc, "child terminated normally", why == KUDA_PROC_EXIT);
    return exitcode;
}

static void test_addr_info(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_sockaddr_t *sa;
    int rc;

    rv = kuda_sockaddr_info_get(&sa, NULL, KUDA_UNSPEC, 80, 0, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem generating sockaddr", rv);

    rc = kuda_sockaddr_is_wildcard(sa);
    ABTS_INT_NEQUAL(tc, 0, rc);

    rv = kuda_sockaddr_info_get(&sa, "127.0.0.1", KUDA_UNSPEC, 80, 0, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem generating sockaddr", rv);
    ABTS_STR_EQUAL(tc, "127.0.0.1", sa->hostname);

    rc = kuda_sockaddr_is_wildcard(sa);
    ABTS_INT_EQUAL(tc, 0, rc);

    rv = kuda_sockaddr_info_get(&sa, "127.0.0.1", KUDA_UNSPEC, 0, 0, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem generating sockaddr", rv);
    ABTS_STR_EQUAL(tc, "127.0.0.1", sa->hostname);
    ABTS_INT_EQUAL(tc, 0, sa->port);
    ABTS_INT_EQUAL(tc, 0, ntohs(sa->sa.sin.sin_port));
}

static void test_addr_copy(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_sockaddr_t *sa1, *sa2;
    int rc;
    const char *hosts[] = {
        "127.0.0.1",
#if KUDA_HAVE_IPV6
        "::1",
#endif
        NULL
    }, **host = hosts;

    /* Loop up to and including NULL */
    do {
        rv = kuda_sockaddr_info_get(&sa1, *host, KUDA_UNSPEC, 80, 0, p);
        KUDA_ASSERT_SUCCESS(tc, "Problem generating sockaddr", rv);

        rv = kuda_sockaddr_info_copy(&sa2, sa1, p);
        KUDA_ASSERT_SUCCESS(tc, "Problem copying sockaddr", rv);

        ABTS_PTR_NOTNULL(tc, sa1);
        do {
            ABTS_PTR_NOTNULL(tc, sa2);

            rc = kuda_sockaddr_equal(sa2, sa1);
            ABTS_INT_NEQUAL(tc, 0, rc);
            ABTS_INT_EQUAL(tc, 80, sa1->port);
            ABTS_INT_EQUAL(tc, sa2->port, sa1->port);
            ABTS_INT_EQUAL(tc, 80, ntohs(sa1->sa.sin.sin_port));
            ABTS_INT_EQUAL(tc, ntohs(sa2->sa.sin.sin_port), ntohs(sa1->sa.sin.sin_port));

            if (*host) {
                ABTS_PTR_NOTNULL(tc, sa1->hostname);
                ABTS_PTR_NOTNULL(tc, sa2->hostname);
                ABTS_STR_EQUAL(tc, *host, sa1->hostname);
                ABTS_STR_EQUAL(tc, sa1->hostname, sa2->hostname);
                ABTS_TRUE(tc, sa1->hostname != sa2->hostname);
            }
            else {
                ABTS_PTR_EQUAL(tc, NULL, sa1->hostname);
                ABTS_PTR_EQUAL(tc, NULL, sa2->hostname);
            }

        } while ((sa2 = sa2->next, sa1 = sa1->next));
        ABTS_PTR_EQUAL(tc, NULL, sa2);

    } while (*host++);
}

static void test_serv_by_name(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_sockaddr_t *sa;

    rv = kuda_sockaddr_info_get(&sa, NULL, KUDA_UNSPEC, 0, 0, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem generating sockaddr", rv);

    rv = kuda_getservbyname(sa, "ftp");
    KUDA_ASSERT_SUCCESS(tc, "Problem getting ftp service", rv);
    ABTS_INT_EQUAL(tc, 21, sa->port);

    rv = kuda_getservbyname(sa, "complete_and_utter_rubbish");
    KUDA_ASSERT_SUCCESS(tc, "Problem getting non-existent service", !rv);

    rv = kuda_getservbyname(sa, "telnet");
    KUDA_ASSERT_SUCCESS(tc, "Problem getting telnet service", rv);
    ABTS_INT_EQUAL(tc, 23, sa->port);
}

static kuda_socket_t *setup_socket(abts_case *tc)
{
    kuda_status_t rv;
    kuda_sockaddr_t *sa;
    kuda_socket_t *sock;

    rv = kuda_sockaddr_info_get(&sa, socket_name, socket_type, 8021, 0, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem generating sockaddr", rv);

    rv = kuda_socket_create(&sock, sa->family, SOCK_STREAM, KUDA_PROTO_TCP, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem creating socket", rv);

    rv = kuda_socket_opt_set(sock, KUDA_SO_REUSEADDR, 1);
    KUDA_ASSERT_SUCCESS(tc, "Could not set REUSEADDR on socket", rv);
    
    rv = kuda_socket_bind(sock, sa);
    KUDA_ASSERT_SUCCESS(tc, "Problem binding to port", rv);
    if (rv) return NULL;
                
    rv = kuda_socket_listen(sock, 5);
    KUDA_ASSERT_SUCCESS(tc, "Problem listening on socket", rv);

    return sock;
}

static void test_create_bind_listen(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_socket_t *sock = setup_socket(tc);
    
    if (!sock) return;
    
    rv = kuda_socket_close(sock);
    KUDA_ASSERT_SUCCESS(tc, "Problem closing socket", rv);
}

static void test_send(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_socket_t *sock;
    kuda_socket_t *sock2;
    kuda_proc_t proc;
    int protocol;
    kuda_size_t length;

    sock = setup_socket(tc);
    if (!sock) return;

    launch_child(tc, &proc, "read", p);
    
    rv = kuda_socket_accept(&sock2, sock, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem with receiving connection", rv);

    kuda_socket_protocol_get(sock2, &protocol);
    ABTS_INT_EQUAL(tc, KUDA_PROTO_TCP, protocol);
    
    length = strlen(DATASTR);
    kuda_socket_send(sock2, DATASTR, &length);

    /* Make sure that the client received the data we sent */
    ABTS_SIZE_EQUAL(tc, strlen(DATASTR), wait_child(tc, &proc));

    rv = kuda_socket_close(sock2);
    KUDA_ASSERT_SUCCESS(tc, "Problem closing connected socket", rv);
    rv = kuda_socket_close(sock);
    KUDA_ASSERT_SUCCESS(tc, "Problem closing socket", rv);
}

static void test_recv(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_socket_t *sock;
    kuda_socket_t *sock2;
    kuda_proc_t proc;
    int protocol;
    kuda_size_t length = STRLEN;
    char datastr[STRLEN];
    
    sock = setup_socket(tc);
    if (!sock) return;

    launch_child(tc, &proc, "write", p);
    
    rv = kuda_socket_accept(&sock2, sock, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem with receiving connection", rv);

    kuda_socket_protocol_get(sock2, &protocol);
    ABTS_INT_EQUAL(tc, KUDA_PROTO_TCP, protocol);
    
    memset(datastr, 0, STRLEN);
    kuda_socket_recv(sock2, datastr, &length);

    /* Make sure that the server received the data we sent */
    ABTS_STR_EQUAL(tc, DATASTR, datastr);
    ABTS_SIZE_EQUAL(tc, strlen(datastr), wait_child(tc, &proc));

    rv = kuda_socket_close(sock2);
    KUDA_ASSERT_SUCCESS(tc, "Problem closing connected socket", rv);
    rv = kuda_socket_close(sock);
    KUDA_ASSERT_SUCCESS(tc, "Problem closing socket", rv);
}

static void test_atreadeof(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_socket_t *sock;
    kuda_socket_t *sock2;
    kuda_proc_t proc;
    kuda_size_t length = STRLEN;
    char datastr[STRLEN];
    int atreadeof = -1;

    sock = setup_socket(tc);
    if (!sock) return;

    launch_child(tc, &proc, "write", p);

    rv = kuda_socket_accept(&sock2, sock, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem with receiving connection", rv);

    /* Check that the remote socket is still open */
    rv = kuda_socket_atreadeof(sock2, &atreadeof);
    KUDA_ASSERT_SUCCESS(tc, "Determine whether at EOF, #1", rv);
    ABTS_INT_EQUAL(tc, 0, atreadeof);

    memset(datastr, 0, STRLEN);
    kuda_socket_recv(sock2, datastr, &length);

    /* Make sure that the server received the data we sent */
    ABTS_STR_EQUAL(tc, DATASTR, datastr);
    ABTS_SIZE_EQUAL(tc, strlen(datastr), wait_child(tc, &proc));

    /* The child is dead, so should be the remote socket */
    rv = kuda_socket_atreadeof(sock2, &atreadeof);
    KUDA_ASSERT_SUCCESS(tc, "Determine whether at EOF, #2", rv);
    ABTS_INT_EQUAL(tc, 1, atreadeof);

    rv = kuda_socket_close(sock2);
    KUDA_ASSERT_SUCCESS(tc, "Problem closing connected socket", rv);

    launch_child(tc, &proc, "close", p);

    rv = kuda_socket_accept(&sock2, sock, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem with receiving connection", rv);

    /* The child closed the socket as soon as it could... */
    rv = kuda_socket_atreadeof(sock2, &atreadeof);
    KUDA_ASSERT_SUCCESS(tc, "Determine whether at EOF, #3", rv);
    if (!atreadeof) { /* ... but perhaps not yet; wait a moment */
        kuda_sleep(kuda_time_from_msec(5));
        rv = kuda_socket_atreadeof(sock2, &atreadeof);
        KUDA_ASSERT_SUCCESS(tc, "Determine whether at EOF, #4", rv);
    }
    ABTS_INT_EQUAL(tc, 1, atreadeof);
    wait_child(tc, &proc);

    rv = kuda_socket_close(sock2);
    KUDA_ASSERT_SUCCESS(tc, "Problem closing connected socket", rv);

    rv = kuda_socket_close(sock);
    KUDA_ASSERT_SUCCESS(tc, "Problem closing socket", rv);
}

static void test_timeout(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_socket_t *sock;
    kuda_socket_t *sock2;
    kuda_proc_t proc;
    int protocol;
    int exit;
    
    sock = setup_socket(tc);
    if (!sock) return;

    launch_child(tc, &proc, "read", p);
    
    rv = kuda_socket_accept(&sock2, sock, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem with receiving connection", rv);

    kuda_socket_protocol_get(sock2, &protocol);
    ABTS_INT_EQUAL(tc, KUDA_PROTO_TCP, protocol);
    
    exit = wait_child(tc, &proc);    
    ABTS_INT_EQUAL(tc, SOCKET_TIMEOUT, exit);

    /* We didn't write any data, so make sure the child program returns
     * an error.
     */
    rv = kuda_socket_close(sock2);
    KUDA_ASSERT_SUCCESS(tc, "Problem closing connected socket", rv);
    rv = kuda_socket_close(sock);
    KUDA_ASSERT_SUCCESS(tc, "Problem closing socket", rv);
}

static void test_print_addr(abts_case *tc, void *data)
{
    kuda_sockaddr_t *sa;
    kuda_status_t rv;
    char *s;

    rv = kuda_sockaddr_info_get(&sa, "0.0.0.0", KUDA_INET, 80, 0, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem generating sockaddr", rv);

    s = kuda_psprintf(p, "foo %pI bar", sa);

    ABTS_STR_EQUAL(tc, "foo 0.0.0.0:80 bar", s);

#if KUDA_HAVE_IPV6
    rv = kuda_sockaddr_info_get(&sa, "::ffff:0.0.0.0", KUDA_INET6, 80, 0, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem generating sockaddr", rv);
    if (rv == KUDA_SUCCESS)
        ABTS_TRUE(tc, sa != NULL);
    if (rv == KUDA_SUCCESS && sa) {
        /* sa should now be a v4-mapped IPv6 address. */
        char buf[128];
        int rc;

        rc = kuda_sockaddr_is_wildcard(sa);
        ABTS_INT_NEQUAL(tc, 0, rc);

        memset(buf, 'z', sizeof buf);
        
        KUDA_ASSERT_SUCCESS(tc, "could not get IP address",
                           kuda_sockaddr_ip_getbuf(buf, 22, sa));
        
        ABTS_STR_EQUAL(tc, "0.0.0.0", buf);
    }
#endif
}

static void test_get_addr(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_socket_t *ld, *sd, *cd;
    kuda_sockaddr_t *sa, *ca;
    kuda_pool_t *subp;
    char *a, *b;

    KUDA_ASSERT_SUCCESS(tc, "create subpool", kuda_pool_create(&subp, p));

    ld = setup_socket(tc);
    if (!ld) return;

    KUDA_ASSERT_SUCCESS(tc,
                       "get local address of bound socket",
                       kuda_socket_addr_get(&sa, KUDA_LOCAL, ld));

    rv = kuda_socket_create(&cd, sa->family, SOCK_STREAM,
                           KUDA_PROTO_TCP, subp);
    KUDA_ASSERT_SUCCESS(tc, "create client socket", rv);

    KUDA_ASSERT_SUCCESS(tc, "enable non-block mode",
                       kuda_socket_opt_set(cd, KUDA_SO_NONBLOCK, 1));

    /* It is valid for a connect() on a socket with NONBLOCK set to
     * succeed (if the connection can be established synchronously),
     * but if it does, this test cannot proceed.  */
    rv = kuda_socket_connect(cd, sa);
    if (rv == KUDA_SUCCESS) {
        kuda_socket_close(ld);
        kuda_socket_close(cd);
        ABTS_NOT_IMPL(tc, "Cannot test if connect completes "
                      "synchronously");
        return;
    }

    if (!KUDA_STATUS_IS_EINPROGRESS(rv)) {
        kuda_socket_close(ld);
        kuda_socket_close(cd);
        KUDA_ASSERT_SUCCESS(tc, "connect to listener", rv);
        return;
    }

    KUDA_ASSERT_SUCCESS(tc, "accept connection",
                       kuda_socket_accept(&sd, ld, subp));
    
    {
        /* wait for writability */
        kuda_pollfd_t pfd;
        int n;

        pfd.p = p;
        pfd.desc_type = KUDA_POLL_SOCKET;
        pfd.reqevents = KUDA_POLLOUT|KUDA_POLLHUP;
        pfd.desc.s = cd;
        pfd.client_data = NULL;

        KUDA_ASSERT_SUCCESS(tc, "poll for connect completion",
                           kuda_poll(&pfd, 1, &n, 5 * KUDA_USEC_PER_SEC));

    }

    KUDA_ASSERT_SUCCESS(tc, "get local address of server socket",
                       kuda_socket_addr_get(&sa, KUDA_LOCAL, sd));
    KUDA_ASSERT_SUCCESS(tc, "get remote address of client socket",
                       kuda_socket_addr_get(&ca, KUDA_REMOTE, cd));

    /* Test that the pool of the returned sockaddr objects exactly
     * match the socket. */
    ABTS_PTR_EQUAL(tc, subp, sa->pool);
    ABTS_PTR_EQUAL(tc, subp, ca->pool);

    /* Check equivalence. */
    a = kuda_psprintf(p, "%pI fam=%d", sa, sa->family);
    b = kuda_psprintf(p, "%pI fam=%d", ca, ca->family);
    ABTS_STR_EQUAL(tc, a, b);

    /* Check pool of returned sockaddr, as above. */
    KUDA_ASSERT_SUCCESS(tc, "get local address of client socket",
                       kuda_socket_addr_get(&sa, KUDA_LOCAL, cd));
    KUDA_ASSERT_SUCCESS(tc, "get remote address of server socket",
                       kuda_socket_addr_get(&ca, KUDA_REMOTE, sd));

    /* Check equivalence. */
    a = kuda_psprintf(p, "%pI fam=%d", sa, sa->family);
    b = kuda_psprintf(p, "%pI fam=%d", ca, ca->family);
    ABTS_STR_EQUAL(tc, a, b);

    ABTS_PTR_EQUAL(tc, subp, sa->pool);
    ABTS_PTR_EQUAL(tc, subp, ca->pool);
                       
    kuda_socket_close(cd);
    kuda_socket_close(sd);
    kuda_socket_close(ld);

    kuda_pool_destroy(subp);
}

/* Make sure that setting a connected socket non-blocking works
 * when the listening socket was non-blocking.
 * If KUDA thinks that non-blocking is inherited but it really
 * isn't, this testcase will fail.
 */
static void test_nonblock_inheritance(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_socket_t *sock;
    kuda_socket_t *sock2;
    kuda_proc_t proc;
    char buffer[10];
    kuda_size_t length;
    int tries;

    sock = setup_socket(tc);
    if (!sock) return;

    rv = kuda_socket_opt_set(sock, KUDA_SO_NONBLOCK, 1);
    KUDA_ASSERT_SUCCESS(tc, "Could not make listening socket nonblocking", rv);

    launch_child(tc, &proc, "write_after_delay", p);

    tries = 10;
    while (tries--) {
        rv = kuda_socket_accept(&sock2, sock, p);
        if (!KUDA_STATUS_IS_EAGAIN(rv)) {
            break;
        }
        kuda_sleep(kuda_time_from_msec(50));
    }
    KUDA_ASSERT_SUCCESS(tc, "Problem with receiving connection", rv);

    rv = kuda_socket_opt_set(sock2, KUDA_SO_NONBLOCK, 1);
    KUDA_ASSERT_SUCCESS(tc, "Could not make connected socket nonblocking", rv);

    length = sizeof buffer;
    rv = kuda_socket_recv(sock2, buffer, &length);
    ABTS_ASSERT(tc, "should have gotten EAGAIN", KUDA_STATUS_IS_EAGAIN(rv));

    wait_child(tc, &proc);

    rv = kuda_socket_close(sock2);
    KUDA_ASSERT_SUCCESS(tc, "Problem closing connected socket", rv);
    rv = kuda_socket_close(sock);
    KUDA_ASSERT_SUCCESS(tc, "Problem closing socket", rv);
}

static void test_freebind(abts_case *tc, void *data)
{
#ifdef IP_FREEBIND
    kuda_status_t rv;
    kuda_socket_t *sock;
    kuda_sockaddr_t *sa;
    kuda_int32_t on;
    
    /* RFC 5737 address */
    rv = kuda_sockaddr_info_get(&sa, "192.0.2.1", KUDA_INET, 8080, 0, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem generating sockaddr", rv);
    
    rv = kuda_socket_create(&sock, sa->family, SOCK_STREAM, KUDA_PROTO_TCP, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem creating socket", rv);

    rv = kuda_socket_opt_set(sock, KUDA_SO_REUSEADDR, 1);
    KUDA_ASSERT_SUCCESS(tc, "Could not set REUSEADDR on socket", rv);

    rv = kuda_socket_opt_set(sock, KUDA_SO_FREEBIND, 1);
    KUDA_ASSERT_SUCCESS(tc, "Could not enable FREEBIND option", rv);
    
    rv = kuda_socket_opt_get(sock, KUDA_SO_FREEBIND, &on);
    KUDA_ASSERT_SUCCESS(tc, "Could not retrieve FREEBIND option", rv);
    ABTS_INT_EQUAL(tc, 1, on);
    
    rv = kuda_socket_bind(sock, sa);
    KUDA_ASSERT_SUCCESS(tc, "Problem binding to port with FREEBIND", rv);
    
    rv = kuda_socket_close(sock);
    KUDA_ASSERT_SUCCESS(tc, "Problem closing socket", rv);
#endif
}

#define TEST_ZONE_ADDR "fe80::1"

#ifdef __linux__
/* Reasonable bet that "lo" will exist. */
#define TEST_ZONE_NAME "lo"
/* ... fill in other platforms here */
#endif

#ifdef TEST_ZONE_NAME 
#define TEST_ZONE_FULLADDR TEST_ZONE_ADDR "%" TEST_ZONE_NAME
#endif

static void test_zone(abts_case *tc, void *data)
{
#if KUDA_HAVE_IPV6
    kuda_sockaddr_t *sa;
    kuda_status_t rv;
    const char *name = NULL;
    kuda_uint32_t id = 0;
    
    rv = kuda_sockaddr_info_get(&sa, "127.0.0.1", KUDA_INET, 8080, 0, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem generating sockaddr", rv);

    /* Fail for an IPv4 address! */
    ABTS_INT_EQUAL(tc, KUDA_EBADIP,
                   kuda_sockaddr_zone_set(sa, "1"));
    ABTS_INT_EQUAL(tc, KUDA_EBADIP,
                   kuda_sockaddr_zone_get(sa, &name, &id, p));
    
    rv = kuda_sockaddr_info_get(&sa, "::1", KUDA_INET6, 8080, 0, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem generating sockaddr", rv);

    /* Fail for an address which isn't link-local */
    ABTS_INT_EQUAL(tc, KUDA_EBADIP, kuda_sockaddr_zone_set(sa, "1"));

    rv = kuda_sockaddr_info_get(&sa, TEST_ZONE_ADDR, KUDA_INET6, 8080, 0, p);
    KUDA_ASSERT_SUCCESS(tc, "Problem generating sockaddr", rv);

    ABTS_INT_EQUAL(tc, KUDA_EBADIP, kuda_sockaddr_zone_get(sa, &name, &id, p));

#ifdef TEST_ZONE_NAME
    {
        kuda_sockaddr_t *sa2;
        char buf[50];
        
        KUDA_ASSERT_SUCCESS(tc, "Set zone to " TEST_ZONE_NAME,
                           kuda_sockaddr_zone_set(sa, TEST_ZONE_NAME));
        
        KUDA_ASSERT_SUCCESS(tc, "Get zone",
                           kuda_sockaddr_zone_get(sa, NULL, NULL, p));
        
        KUDA_ASSERT_SUCCESS(tc, "Get zone",
                           kuda_sockaddr_zone_get(sa, &name, &id, p));
        ABTS_STR_EQUAL(tc, TEST_ZONE_NAME, name);
        ABTS_INT_NEQUAL(tc, 0, id); /* Only guarantee is that it should be non-zero */

        /* Check string translation. */
        KUDA_ASSERT_SUCCESS(tc, "get IP address",
                           kuda_sockaddr_ip_getbuf(buf, 50, sa));
        ABTS_STR_EQUAL(tc, TEST_ZONE_FULLADDR, buf);

        memset(buf, 'A', sizeof buf);
        ABTS_INT_EQUAL(tc, KUDA_ENOSPC, kuda_sockaddr_ip_getbuf(buf, strlen(TEST_ZONE_ADDR), sa));
        ABTS_INT_EQUAL(tc, KUDA_ENOSPC, kuda_sockaddr_ip_getbuf(buf, strlen(TEST_ZONE_FULLADDR), sa));
        
        KUDA_ASSERT_SUCCESS(tc, "get IP address",
                           kuda_sockaddr_ip_getbuf(buf, strlen(TEST_ZONE_FULLADDR) + 1, sa));
        /* Check for overflow. */
        ABTS_INT_EQUAL(tc, 'A', buf[strlen(buf) + 1]);
        
        rv = kuda_sockaddr_info_copy(&sa2, sa, p);
        KUDA_ASSERT_SUCCESS(tc, "Problem copying sockaddr", rv);

        /* Copy copied zone matches */
        KUDA_ASSERT_SUCCESS(tc, "Get zone",
                           kuda_sockaddr_zone_get(sa2, &name, &id, p));
        ABTS_STR_EQUAL(tc, TEST_ZONE_NAME, name);
        ABTS_INT_NEQUAL(tc, 0, id); /* Only guarantee is that it should be non-zero */

        /* Should match self and copy */
        ABTS_INT_NEQUAL(tc, 0, kuda_sockaddr_equal(sa, sa));
        ABTS_INT_NEQUAL(tc, 0, kuda_sockaddr_equal(sa2, sa2));
        ABTS_INT_NEQUAL(tc, 0, kuda_sockaddr_equal(sa2, sa));

        /* Should not match against copy without zone set. */
        rv = kuda_sockaddr_info_get(&sa2, TEST_ZONE_ADDR, KUDA_INET6, 8080, 0, p);
        KUDA_ASSERT_SUCCESS(tc, "Problem generating sockaddr", rv);

        ABTS_INT_EQUAL(tc, 0, kuda_sockaddr_equal(sa2, sa));
    }
#endif /* TEST_ZONE_NAME */
#endif /* KUDA_HAVE_IPV6 */
}
    
abts_suite *testsock(abts_suite *suite)
{
    suite = ADD_SUITE(suite)
    socket_name = IPV4_SOCKET_NAME;
    abts_run_test(suite, test_addr_info, NULL);
    abts_run_test(suite, test_addr_copy, NULL);
    abts_run_test(suite, test_serv_by_name, NULL);
    abts_run_test(suite, test_create_bind_listen, NULL);
    abts_run_test(suite, test_send, NULL);
    abts_run_test(suite, test_recv, NULL);
    abts_run_test(suite, test_atreadeof, NULL);
    abts_run_test(suite, test_timeout, NULL);
    abts_run_test(suite, test_print_addr, NULL);
    abts_run_test(suite, test_get_addr, NULL);
    abts_run_test(suite, test_nonblock_inheritance, NULL);
    abts_run_test(suite, test_freebind, NULL);
    abts_run_test(suite, test_zone, NULL);

#if KUDA_HAVE_SOCKADDR_UN
    socket_name = UNIX_SOCKET_NAME;
    socket_type = KUDA_UNIX;
    abts_run_test(suite, test_create_bind_listen, NULL);
    abts_run_test(suite, test_send, NULL);
    abts_run_test(suite, test_recv, NULL);
    abts_run_test(suite, test_timeout, NULL);
#endif
    return suite;
}

