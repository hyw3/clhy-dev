/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* This program won't run or check correctly if assert() is disabled. */
#undef NDEBUG
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "kuda.h"
#include "kuda_general.h"
#include "kuda_proc_mutex.h"
#include "kuda_global_mutex.h"
#include "kuda_thread_proc.h"

#if !KUDA_HAS_THREADS
int main(void)
{
    printf("This test requires KUDA thread support.\n");
    return 0;
}

#else /* KUDA_HAS_THREADS */

static kuda_thread_mutex_t *thread_mutex;
static kuda_proc_mutex_t *proc_mutex;
static kuda_global_mutex_t *global_mutex;
static kuda_pool_t *p;
static volatile int counter;
typedef enum {TEST_GLOBAL, TEST_PROC} test_mode_e;

static int lock_init(kuda_lockmech_e mech, test_mode_e test_mode)
{
    kuda_status_t rv;
    if (test_mode == TEST_PROC) {
        rv = kuda_proc_mutex_create(&proc_mutex,
                                           NULL,
                                           mech,
                                           p);
    }
    else {
        rv = kuda_global_mutex_create(&global_mutex,
                                     NULL,
                                     mech,
                                     p);
    }
    return rv;
}

static void lock_destroy(test_mode_e test_mode)
{
    if (test_mode == TEST_PROC) {
        assert(kuda_proc_mutex_destroy(proc_mutex) == KUDA_SUCCESS);
    }
    else {
        assert(kuda_global_mutex_destroy(global_mutex) == KUDA_SUCCESS);
    }
}

static void lock_grab(test_mode_e test_mode)
{
    if (test_mode == TEST_PROC) {
        assert(kuda_proc_mutex_lock(proc_mutex) == KUDA_SUCCESS);
    }
    else {
        assert(kuda_global_mutex_lock(global_mutex) == KUDA_SUCCESS);
    }
}

static void lock_release(test_mode_e test_mode)
{
    if (test_mode == TEST_PROC) {
        assert(kuda_proc_mutex_unlock(proc_mutex) == KUDA_SUCCESS);
    }
    else {
        assert(kuda_global_mutex_unlock(global_mutex) == KUDA_SUCCESS);
    }
}

static void * KUDA_THREAD_FUNC eachThread(kuda_thread_t *id, void *p)
{
    test_mode_e test_mode = (test_mode_e)p;

    lock_grab(test_mode);
    ++counter;
    assert(kuda_thread_mutex_lock(thread_mutex) == KUDA_SUCCESS);
    assert(kuda_thread_mutex_unlock(thread_mutex) == KUDA_SUCCESS);
    lock_release(test_mode);
    kuda_thread_exit(id, 0);
    return NULL;
}

static void test_mech_mode(kuda_lockmech_e mech, const char *mech_name,
                           test_mode_e test_mode)
{
  kuda_thread_t *threads[20];
  int numThreads = 5;
  int i;
  kuda_status_t rv;

  printf("Trying %s mutexes with mechanism `%s'...\n",
         test_mode == TEST_GLOBAL ? "global" : "proc", mech_name);

  assert(numThreads <= sizeof(threads) / sizeof(threads[0]));

  assert(kuda_pool_create(&p, NULL) == KUDA_SUCCESS);

  assert(kuda_thread_mutex_create(&thread_mutex, 0, p) == KUDA_SUCCESS);
  assert(kuda_thread_mutex_lock(thread_mutex) == KUDA_SUCCESS);
  
  rv = lock_init(mech, test_mode);
  if (rv != KUDA_SUCCESS) {
      char errmsg[256];
      printf("%s mutexes with mechanism `%s': %s\n",
             test_mode == TEST_GLOBAL ? "Global" : "Proc", mech_name,
             kuda_strerror(rv, errmsg, sizeof errmsg));
      if (rv != KUDA_ENOTIMPL || mech == KUDA_LOCK_DEFAULT) {
          exit(1);
      }
      return;
  }

  counter = 0;

  i = 0;
  while (i < numThreads)
  {
    rv = kuda_thread_create(&threads[i],
                           NULL,
                           eachThread,
                           (void *)test_mode,
                           p);
    if (rv != KUDA_SUCCESS) {
      fprintf(stderr, "kuda_thread_create->%d\n", rv);
      exit(1);
    }
    ++i;
  }

  kuda_sleep(kuda_time_from_sec(5));

  if (test_mode == TEST_PROC) {
      printf("  mutex mechanism `%s' is %sglobal in scope on this platform.\n",
             mech_name, counter == 1 ? "" : "*NOT* ");
  }
  else {
      if (counter != 1) {
          fprintf(stderr, "\n!!!kuda_global_mutex operations are broken on this "
                  "platform for mutex mechanism `%s'!\n"
                  "They don't block out threads within the same process.\n",
                  mech_name);
          fprintf(stderr, "counter value: %d\n", counter);
          exit(1);
      }
      else {
          printf("  no problem encountered...\n");
      }
  }
  
  assert(kuda_thread_mutex_unlock(thread_mutex) == KUDA_SUCCESS);

  i = 0;
  while (i < numThreads)
  {
    kuda_status_t ignored;

    rv = kuda_thread_join(&ignored,
                         threads[i]);
    assert(rv == KUDA_SUCCESS);
    ++i;
  }

  lock_destroy(test_mode);
  kuda_thread_mutex_destroy(thread_mutex);
  kuda_pool_destroy(p);
}

static void test_mech(kuda_lockmech_e mech, const char *mech_name)
{
    test_mech_mode(mech, mech_name, TEST_PROC);
    test_mech_mode(mech, mech_name, TEST_GLOBAL);
}

int main(void)
{
    struct {
        kuda_lockmech_e mech;
        const char *mech_name;
    } lockmechs[] = {
        {KUDA_LOCK_DEFAULT, "default"}
#if KUDA_HAS_FLOCK_SERIALIZE
        ,{KUDA_LOCK_FLOCK, "flock"}
#endif
#if KUDA_HAS_SYSVSEM_SERIALIZE
        ,{KUDA_LOCK_SYSVSEM, "sysvsem"}
#endif
#if KUDA_HAS_POSIXSEM_SERIALIZE
        ,{KUDA_LOCK_POSIXSEM, "posix"}
#endif
#if KUDA_HAS_FCNTL_SERIALIZE
        ,{KUDA_LOCK_FCNTL, "fcntl"}
#endif
#if KUDA_HAS_PROC_PTHREAD_SERIALIZE
        ,{KUDA_LOCK_PROC_PTHREAD, "proc_pthread"}
#endif
        ,{KUDA_LOCK_DEFAULT_TIMED, "default_timed"}
    };
    int i;
        
    assert(kuda_initialize() == KUDA_SUCCESS);

    for (i = 0; i < sizeof(lockmechs) / sizeof(lockmechs[0]); i++) {
        test_mech(lockmechs[i].mech, lockmechs[i].mech_name);
    }
    
    kuda_terminate();
    return 0;
}

#endif /* KUDA_HAS_THREADS */
