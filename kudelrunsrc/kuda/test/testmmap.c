/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testutil.h"
#include "kuda_mmap.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_file_io.h"
#include "kuda_strings.h"

/* hmmm, what is a truly portable define for the max path
 * length on a platform?
 */
#define PATH_LEN 255

#if !KUDA_HAS_MMAP
static void not_implemented(abts_case *tc, void *data)
{
    ABTS_NOT_IMPL(tc, "MMAP functions");
}

#else

static char test_string[256]; /* read from the datafile */
static kuda_mmap_t *themmap = NULL;
static kuda_file_t *thefile = NULL;
static char *file1;
static kuda_finfo_t thisfinfo;
static kuda_size_t thisfsize;

static void create_filename(abts_case *tc, void *data)
{
    char *oldfileptr;

    kuda_filepath_get(&file1, 0, p);
#ifndef NETWARE
#ifdef WIN32
    ABTS_TRUE(tc, file1[1] == ':');
#else
    ABTS_TRUE(tc, file1[0] == '/');
#endif
#endif
    ABTS_TRUE(tc, file1[strlen(file1) - 1] != '/');

    oldfileptr = file1;
    file1 = kuda_pstrcat(p, file1,"/data/mmap_datafile.txt" ,NULL);
    ABTS_TRUE(tc, oldfileptr != file1);
}

static void test_file_close(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_file_close(thefile);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}
   
static void read_expected_contents(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_size_t nbytes = sizeof(test_string) - 1;

    rv = kuda_file_read(thefile, test_string, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    test_string[nbytes] = '\0';
    thisfsize = strlen(test_string);
}

static void test_file_open(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_file_open(&thefile, file1, KUDA_FOPEN_READ, KUDA_UREAD | KUDA_GREAD, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, thefile);
}
   
static void test_get_filesize(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_file_info_get(&thisfinfo, KUDA_FINFO_NORM, thefile);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_ASSERT(tc, "File size mismatch", thisfsize == thisfinfo.size);
}

static void test_mmap_create(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_mmap_create(&themmap, thefile, 0, (kuda_size_t) thisfinfo.size, 
		                 KUDA_MMAP_READ, p);
    ABTS_PTR_NOTNULL(tc, themmap);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

static void test_mmap_contents(abts_case *tc, void *data)
{
    
    ABTS_PTR_NOTNULL(tc, themmap);
    ABTS_PTR_NOTNULL(tc, themmap->mm);
    ABTS_SIZE_EQUAL(tc, thisfsize, themmap->size);

    /* Must use nEquals since the string is not guaranteed to be NULL terminated */
    ABTS_STR_NEQUAL(tc, themmap->mm, test_string, thisfsize);
}

static void test_mmap_delete(abts_case *tc, void *data)
{
    kuda_status_t rv;

    ABTS_PTR_NOTNULL(tc, themmap);
    rv = kuda_mmap_delete(themmap);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

static void test_mmap_offset(abts_case *tc, void *data)
{
    kuda_status_t rv;
    void *addr;

    ABTS_PTR_NOTNULL(tc, themmap);
    rv = kuda_mmap_offset(&addr, themmap, 5);

    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    /* Must use nEquals since the string is not guaranteed to be NULL terminated */
    ABTS_STR_NEQUAL(tc, addr, test_string + 5, thisfsize-5);
}
#endif

abts_suite *testmmap(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

#if KUDA_HAS_MMAP    
    abts_run_test(suite, create_filename, NULL);
    abts_run_test(suite, test_file_open, NULL);
    abts_run_test(suite, read_expected_contents, NULL);
    abts_run_test(suite, test_get_filesize, NULL);
    abts_run_test(suite, test_mmap_create, NULL);
    abts_run_test(suite, test_mmap_contents, NULL);
    abts_run_test(suite, test_mmap_offset, NULL);
    abts_run_test(suite, test_mmap_delete, NULL);
    abts_run_test(suite, test_file_close, NULL);
#else
    abts_run_test(suite, not_implemented, NULL);
#endif

    return suite;
}

