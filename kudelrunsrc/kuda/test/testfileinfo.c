/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_file_io.h"
#include "kuda_file_info.h"
#include "kuda_strings.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_poll.h"
#include "kuda_lib.h"
#include "testutil.h"

#define FILENAME "data/file_datafile.txt"
#define NEWFILENAME "data/new_datafile.txt"
#define NEWFILEDATA "This is new text in a new file."

static const struct view_fileinfo
{
    kuda_int32_t bits;
    char *description;
} vfi[] = {
    {KUDA_FINFO_MTIME,  "MTIME"},
    {KUDA_FINFO_CTIME,  "CTIME"},
    {KUDA_FINFO_ATIME,  "ATIME"},
    {KUDA_FINFO_SIZE,   "SIZE"},
    {KUDA_FINFO_DEV,    "DEV"},
    {KUDA_FINFO_INODE,  "INODE"},
    {KUDA_FINFO_NLINK,  "NLINK"},
    {KUDA_FINFO_TYPE,   "TYPE"},
    {KUDA_FINFO_USER,   "USER"}, 
    {KUDA_FINFO_GROUP,  "GROUP"}, 
    {KUDA_FINFO_UPROT,  "UPROT"}, 
    {KUDA_FINFO_GPROT,  "GPROT"},
    {KUDA_FINFO_WPROT,  "WPROT"},
    {0,                NULL}
}; 

static void finfo_equal(abts_case *tc, kuda_finfo_t *f1, kuda_finfo_t *f2)
{
    /* Minimum supported flags across all platforms (KUDA_FINFO_MIN) */
    ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo must return KUDA_FINFO_TYPE",
             (f1->valid & f2->valid & KUDA_FINFO_TYPE));
    ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in filetype",
             f1->filetype == f2->filetype);
    ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo must return KUDA_FINFO_SIZE",
             (f1->valid & f2->valid & KUDA_FINFO_SIZE));
    ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in size",
             f1->size == f2->size);
    ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo must return KUDA_FINFO_ATIME",
             (f1->valid & f2->valid & KUDA_FINFO_ATIME));
    ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in atime",
             f1->atime == f2->atime);
    ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo must return KUDA_FINFO_MTIME",
             (f1->valid & f2->valid & KUDA_FINFO_MTIME));
    ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in mtime",
             f1->mtime == f2->mtime);
    ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo must return KUDA_FINFO_CTIME",
             (f1->valid & f2->valid & KUDA_FINFO_CTIME));
    ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in ctime",
             f1->ctime == f2->ctime);

    if (f1->valid & f2->valid & KUDA_FINFO_NAME)
        ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in name",
                 !strcmp(f1->name, f2->name));
    if (f1->fname && f2->fname)
        ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in fname",
                 !strcmp(f1->fname, f2->fname));

    /* Additional supported flags not supported on all platforms */
    if (f1->valid & f2->valid & KUDA_FINFO_USER)
        ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in user",
                 !kuda_uid_compare(f1->user, f2->user));
    if (f1->valid & f2->valid & KUDA_FINFO_GROUP)
        ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in group",
                 !kuda_gid_compare(f1->group, f2->group));
    if (f1->valid & f2->valid & KUDA_FINFO_INODE)
        ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in inode",
                 f1->inode == f2->inode);
    if (f1->valid & f2->valid & KUDA_FINFO_DEV)
        ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in device",
                 f1->device == f2->device);
    if (f1->valid & f2->valid & KUDA_FINFO_NLINK)
        ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in nlink",
                 f1->nlink == f2->nlink);
    if (f1->valid & f2->valid & KUDA_FINFO_CSIZE)
        ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in csize",
                 f1->csize == f2->csize);
    if (f1->valid & f2->valid & KUDA_FINFO_PROT)
        ABTS_ASSERT(tc, "kuda_stat and kuda_getfileinfo differ in protection",
                 f1->protection == f2->protection);
}

static void test_info_get(abts_case *tc, void *data)
{
    kuda_file_t *thefile;
    kuda_finfo_t finfo;
    kuda_status_t rv;

    rv = kuda_file_open(&thefile, FILENAME, KUDA_FOPEN_READ, KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_info_get(&finfo, KUDA_FINFO_NORM, thefile);
    if (KUDA_STATUS_IS_INCOMPLETE(rv)) {
        char *str;
	int i;
        str = kuda_pstrdup(p, "KUDA_INCOMPLETE:  Missing ");
        for (i = 0; vfi[i].bits; ++i) {
            if (vfi[i].bits & ~finfo.valid) {
                str = kuda_pstrcat(p, str, vfi[i].description, " ", NULL);
            }
        }
        ABTS_FAIL(tc, str);
    }
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    kuda_file_close(thefile);
}

static void test_stat(abts_case *tc, void *data)
{
    kuda_finfo_t finfo;
    kuda_status_t rv;

    rv = kuda_stat(&finfo, FILENAME, KUDA_FINFO_NORM, p);
    if (KUDA_STATUS_IS_INCOMPLETE(rv)) {
        char *str;
	int i;
        str = kuda_pstrdup(p, "KUDA_INCOMPLETE:  Missing ");
        for (i = 0; vfi[i].bits; ++i) {
            if (vfi[i].bits & ~finfo.valid) {
                str = kuda_pstrcat(p, str, vfi[i].description, " ", NULL);
            }
        }
        ABTS_FAIL(tc, str);
    }
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

static void test_stat_eq_finfo(abts_case *tc, void *data)
{
    kuda_file_t *thefile;
    kuda_finfo_t finfo;
    kuda_finfo_t stat_finfo;
    kuda_status_t rv;

    rv = kuda_file_open(&thefile, FILENAME, KUDA_FOPEN_READ, KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_file_info_get(&finfo, KUDA_FINFO_NORM, thefile);

    /* Opening the file may have toggled the atime member (time last
     * accessed), so fetch our kuda_stat() after getting the fileinfo 
     * of the open file...
     */
    rv = kuda_stat(&stat_finfo, FILENAME, KUDA_FINFO_NORM, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    kuda_file_close(thefile);

    finfo_equal(tc, &stat_finfo, &finfo);
}

static void test_buffered_write_size(abts_case *tc, void *data)
{
    const kuda_size_t data_len = strlen(NEWFILEDATA);
    kuda_file_t *thefile;
    kuda_finfo_t finfo;
    kuda_status_t rv;
    kuda_size_t bytes;

    rv = kuda_file_open(&thefile, NEWFILENAME,
                       KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE | KUDA_FOPEN_TRUNCATE
                       | KUDA_FOPEN_BUFFERED | KUDA_FOPEN_DELONCLOSE,
                       KUDA_PLATFORM_DEFAULT, p);
    KUDA_ASSERT_SUCCESS(tc, "open file", rv);

    /* A funny thing happened to me the other day: I wrote something
     * into a buffered file, then asked for its size using
     * kuda_file_info_get; and guess what? The size was 0! That's not a
     * nice way to behave.
     */
    bytes = data_len;
    rv = kuda_file_write(thefile, NEWFILEDATA, &bytes);
    KUDA_ASSERT_SUCCESS(tc, "write file contents", rv);
    ABTS_TRUE(tc, data_len == bytes);

    rv = kuda_file_info_get(&finfo, KUDA_FINFO_SIZE, thefile);
    KUDA_ASSERT_SUCCESS(tc, "get file size", rv);
    ABTS_TRUE(tc, bytes == (kuda_size_t) finfo.size);
    kuda_file_close(thefile);
}

static void test_mtime_set(abts_case *tc, void *data)
{
    kuda_file_t *thefile;
    kuda_finfo_t finfo;
    kuda_time_t epoch = 0;
    kuda_status_t rv;

    /* This test sort of depends on the system clock being at least
     * marginally ccorrect; We'll be setting the modification time to
     * the epoch.
     */
    rv = kuda_file_open(&thefile, NEWFILENAME,
                       KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE | KUDA_FOPEN_TRUNCATE
                       | KUDA_FOPEN_BUFFERED | KUDA_FOPEN_DELONCLOSE,
                       KUDA_PLATFORM_DEFAULT, p);
    KUDA_ASSERT_SUCCESS(tc, "open file", rv);

    /* Check that the current mtime is not the epoch */
    rv = kuda_stat(&finfo, NEWFILENAME, KUDA_FINFO_MTIME, p);
    if (KUDA_STATUS_IS_INCOMPLETE(rv)) {
        char *str;
	int i;
        str = kuda_pstrdup(p, "KUDA_INCOMPLETE:  Missing ");
        for (i = 0; vfi[i].bits; ++i) {
            if (vfi[i].bits & ~finfo.valid) {
                str = kuda_pstrcat(p, str, vfi[i].description, " ", NULL);
            }
        }
        ABTS_FAIL(tc, str);
    }
    KUDA_ASSERT_SUCCESS(tc, "get initial mtime", rv);
    ABTS_TRUE(tc, finfo.mtime != epoch);

    /* Reset the mtime to the epoch and verify the result.
     * Note: we blindly assume that if the first kuda_stat succeeded,
     * the second one will, too.
     */
    rv = kuda_file_mtime_set(NEWFILENAME, epoch, p);
    KUDA_ASSERT_SUCCESS(tc, "set mtime", rv);

    rv = kuda_stat(&finfo, NEWFILENAME, KUDA_FINFO_MTIME, p);
    KUDA_ASSERT_SUCCESS(tc, "get modified mtime", rv);
    ABTS_TRUE(tc, finfo.mtime == epoch);

    kuda_file_close(thefile);
}

abts_suite *testfileinfo(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, test_info_get, NULL);
    abts_run_test(suite, test_stat, NULL);
    abts_run_test(suite, test_stat_eq_finfo, NULL);
    abts_run_test(suite, test_buffered_write_size, NULL);
    abts_run_test(suite, test_mtime_set, NULL);

    return suite;
}

