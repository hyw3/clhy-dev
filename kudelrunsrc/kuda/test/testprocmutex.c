/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_shm.h"
#include "kuda_thread_proc.h"
#include "kuda_file_io.h"
#include "kuda_proc_mutex.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_getopt.h"
#include <stdio.h>
#include <stdlib.h>
#include "testutil.h"

#if KUDA_HAS_FORK

#define MAX_ITER 200
#define CHILDREN 6
#define MAX_COUNTER (MAX_ITER * CHILDREN)
#define MAX_WAIT_USEC (1000*1000)

static kuda_proc_mutex_t *proc_lock;
static volatile int *x;

typedef struct lockmech {
    kuda_lockmech_e num;
    const char *name;
} lockmech_t;

/* a slower more racy way to implement (*x)++ */
static int increment(int n)
{
    kuda_sleep(1);
    return n+1;
}

static void make_child(abts_case *tc, int trylock, kuda_proc_t **proc, kuda_pool_t *p)
{
    kuda_status_t rv;

    *proc = kuda_pcalloc(p, sizeof(**proc));

    /* slight delay to allow things to settle */
    kuda_sleep (1);

    rv = kuda_proc_fork(*proc, p);
    if (rv == KUDA_INCHILD) {
        int i = 0;
        /* The parent process has setup all processes to call kuda_terminate
         * at exit.  But, that means that all processes must also call
         * kuda_initialize at startup.  You cannot have an unequal number
         * of kuda_terminate and kuda_initialize calls.  If you do, bad things
         * will happen.  In this case, the bad thing is that if the mutex
         * is a semaphore, it will be destroyed before all of the processes
         * die.  That means that the test will most likely fail.
         */
        kuda_initialize();

        if (kuda_proc_mutex_child_init(&proc_lock, NULL, p))
            exit(1);

        do {
            if (trylock > 0) {
                int wait_usec = 0;

                while ((rv = kuda_proc_mutex_trylock(proc_lock))) {
                    if (!KUDA_STATUS_IS_EBUSY(rv))
                        exit(1);
                    if (++wait_usec >= MAX_WAIT_USEC)
                        exit(1);
                    kuda_sleep(1);
                }
            }
            else if (trylock < 0) {
                int wait_usec = 0;

                while ((rv = kuda_proc_mutex_timedlock(proc_lock, 1))) {
                    if (!KUDA_STATUS_IS_TIMEUP(rv))
                        exit(1);
                    if (++wait_usec >= MAX_WAIT_USEC)
                        exit(1);
                }
            }
            else {
                if (kuda_proc_mutex_lock(proc_lock))
                    exit(1);
            }

            i++;
            *x = increment(*x);
            if (kuda_proc_mutex_unlock(proc_lock))
                exit(1);
        } while (i < MAX_ITER);
        exit(0);
    } 

    ABTS_ASSERT(tc, "fork failed", rv == KUDA_INPARENT);
}

/* Wait for a child process and check it terminated with success. */
static void await_child(abts_case *tc, kuda_proc_t *proc)
{
    int code;
    kuda_exit_why_e why;
    kuda_status_t rv;

    rv = kuda_proc_wait(proc, &code, &why, KUDA_WAIT);
    ABTS_ASSERT(tc, "child did not terminate with success",
             rv == KUDA_CHILD_DONE && why == KUDA_PROC_EXIT && code == 0);
}

static void test_exclusive(abts_case *tc, const char *lockname, 
                           lockmech_t *mech)
{
    kuda_proc_t *child[CHILDREN];
    kuda_status_t rv;
    int n;
 
    rv = kuda_proc_mutex_create(&proc_lock, lockname, mech->num, p);
    if (rv == KUDA_ENOTIMPL) {
        /* MacOS lacks TIMED implementation, so don't fail for ENOTIMPL */
        fprintf(stderr, "method %s not implemented, ", mech->name);
        return;
    }
    KUDA_ASSERT_SUCCESS(tc, "create the mutex", rv);
 
    for (n = 0; n < CHILDREN; n++)
        make_child(tc, 0, &child[n], p);

    for (n = 0; n < CHILDREN; n++)
        await_child(tc, child[n]);
    
    ABTS_ASSERT(tc, "Locks don't appear to work", *x == MAX_COUNTER);

    rv = kuda_proc_mutex_trylock(proc_lock);
    if (rv == KUDA_ENOTIMPL) {
        fprintf(stderr, "%s_trylock() not implemented, ", mech->name);
        ABTS_ASSERT(tc, "Default timed trylock not implemented",
                    mech->num != KUDA_LOCK_DEFAULT &&
                    mech->num != KUDA_LOCK_DEFAULT_TIMED);
    }
    else {
        KUDA_ASSERT_SUCCESS(tc, "check for trylock", rv);

        for (n = 0; n < 2; n++) {
            rv = kuda_proc_mutex_trylock(proc_lock);
            /* Some mech (eg. flock or fcntl) may succeed when the
             * lock is re-acquired in the same process.
             */
            if (rv != KUDA_SUCCESS) {
                ABTS_ASSERT(tc,
                            kuda_psprintf(p, "%s_trylock() should be busy => %pm",
                                         mech->name, &rv),
                            KUDA_STATUS_IS_EBUSY(rv));
            }
        }

        rv = kuda_proc_mutex_unlock(proc_lock);
        KUDA_ASSERT_SUCCESS(tc, "unlock after trylock check", rv);

        *x = 0;

        for (n = 0; n < CHILDREN; n++)
            make_child(tc, 1, &child[n], p);

        for (n = 0; n < CHILDREN; n++)
            await_child(tc, child[n]);
        
        ABTS_ASSERT(tc, "Locks don't appear to work with trylock",
                    *x == MAX_COUNTER);
    }

#if KUDA_HAS_TIMEDLOCKS
    rv = kuda_proc_mutex_timedlock(proc_lock, 1);
    if (rv == KUDA_ENOTIMPL) {
        fprintf(stderr, "%s_timedlock() not implemented, ", mech->name);
        ABTS_ASSERT(tc, "Default timed timedlock not implemented",
                    mech->num != KUDA_LOCK_DEFAULT_TIMED);
    }
    else {
        KUDA_ASSERT_SUCCESS(tc, "check for timedlock", rv);

        for (n = 0; n < 2; n++) {
            rv = kuda_proc_mutex_timedlock(proc_lock, 1);
            /* Some mech (eg. flock or fcntl) may succeed when the
             * lock is re-acquired in the same process.
             */
            if (rv != KUDA_SUCCESS) {
                ABTS_ASSERT(tc,
                            kuda_psprintf(p, "%s_timedlock() should time out => %pm",
                                         mech->name, &rv),
                            KUDA_STATUS_IS_TIMEUP(rv));
            }
        }

        rv = kuda_proc_mutex_unlock(proc_lock);
        KUDA_ASSERT_SUCCESS(tc, "unlock after timedlock check", rv);

        *x = 0;

        for (n = 0; n < CHILDREN; n++)
            make_child(tc, -1, &child[n], p);

        for (n = 0; n < CHILDREN; n++)
            await_child(tc, child[n]);
        
        ABTS_ASSERT(tc, "Locks don't appear to work with timedlock",
                    *x == MAX_COUNTER);
    }
#endif  /* KUDA_HAS_TIMEDLOCKS */
}

static void proc_mutex(abts_case *tc, void *data)
{
    kuda_status_t rv;
    const char *shmname = "tpm.shm";
    kuda_shm_t *shm;

    /* Use anonymous shm if available. */
    rv = kuda_shm_create(&shm, sizeof(int), NULL, p);
    if (rv == KUDA_ENOTIMPL) {
        kuda_file_remove(shmname, p);
        rv = kuda_shm_create(&shm, sizeof(int), shmname, p);
    }

    KUDA_ASSERT_SUCCESS(tc, "create shm segment", rv);
    if (rv != KUDA_SUCCESS)
        return;

    x = kuda_shm_baseaddr_get(shm);
    test_exclusive(tc, NULL, data);
    rv = kuda_shm_destroy(shm);
    KUDA_ASSERT_SUCCESS(tc, "Error destroying shared memory block", rv);
}


abts_suite *testprocmutex(abts_suite *suite)
{
    lockmech_t lockmechs[] = {
        {KUDA_LOCK_DEFAULT, "default"}
#if KUDA_HAS_FLOCK_SERIALIZE
        ,{KUDA_LOCK_FLOCK, "flock"}
#endif
#if KUDA_HAS_SYSVSEM_SERIALIZE
        ,{KUDA_LOCK_SYSVSEM, "sysvsem"}
#endif
#if KUDA_HAS_POSIXSEM_SERIALIZE
        ,{KUDA_LOCK_POSIXSEM, "posix"}
#endif
#if KUDA_HAS_FCNTL_SERIALIZE
        ,{KUDA_LOCK_FCNTL, "fcntl"}
#endif
#if KUDA_HAS_PROC_PTHREAD_SERIALIZE
        ,{KUDA_LOCK_PROC_PTHREAD, "proc_pthread"}
#endif
        ,{KUDA_LOCK_DEFAULT_TIMED, "default_timed"}
    };
    int i;

    suite = ADD_SUITE(suite)
    for (i = 0; i < sizeof(lockmechs) / sizeof(lockmechs[0]); i++) {
        abts_run_test(suite, proc_mutex, &lockmechs[i]);
    }
    return suite;
}

#else /* KUDA_HAS_FORK */

static void proc_mutex(abts_case *tc, void *data)
{
    ABTS_NOT_IMPL(tc, "KUDA lacks fork() support");
}

abts_suite *testprocmutex(abts_suite *suite)
{
    suite = ADD_SUITE(suite);
    abts_run_test(suite, proc_mutex, NULL);
    return suite;
}
#endif /* KUDA_HAS_FORK */
