/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "kuda_general.h"
#include "kuda_pools.h"
#include "kuda_errno.h"
#include "kuda_file_io.h"
#include "testutil.h"

#define TEST "Testing\n"
#define TEST2 "Testing again\n"
#define FILEPATH "data/"

static void test_file_dup(abts_case *tc, void *data)
{
    kuda_file_t *file1 = NULL;
    kuda_file_t *file3 = NULL;
    kuda_status_t rv;
    kuda_finfo_t finfo;

    /* First, create a new file, empty... */
    rv = kuda_file_open(&file1, FILEPATH "testdup.file", 
                       KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE |
                       KUDA_FOPEN_DELONCLOSE, KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, file1);

    rv = kuda_file_dup(&file3, file1, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, file3);

    rv = kuda_file_close(file1);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    /* cleanup after ourselves */
    rv = kuda_file_close(file3);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_stat(&finfo, FILEPATH "testdup.file", KUDA_FINFO_NORM, p);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_ENOENT(rv));
}  

static void test_file_readwrite(abts_case *tc, void *data)
{
    kuda_file_t *file1 = NULL;
    kuda_file_t *file3 = NULL;
    kuda_status_t rv;
    kuda_finfo_t finfo;
    kuda_size_t txtlen = sizeof(TEST);
    char buff[50];
    kuda_off_t fpos;

    /* First, create a new file, empty... */
    rv = kuda_file_open(&file1, FILEPATH "testdup.readwrite.file", 
                       KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE |
                       KUDA_FOPEN_DELONCLOSE, KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, file1);

    rv = kuda_file_dup(&file3, file1, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, file3);

    rv = kuda_file_write(file3, TEST, &txtlen);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, sizeof(TEST), txtlen);

    fpos = 0;
    rv = kuda_file_seek(file1, KUDA_SET, &fpos);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_ASSERT(tc, "File position mismatch, expected 0", fpos == 0);

    txtlen = 50;
    rv = kuda_file_read(file1, buff, &txtlen);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_STR_EQUAL(tc, TEST, buff);

    /* cleanup after ourselves */
    rv = kuda_file_close(file1);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_close(file3);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_stat(&finfo, FILEPATH "testdup.readwrite.file", KUDA_FINFO_NORM, p);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_ENOENT(rv));
}  

static void test_dup2(abts_case *tc, void *data)
{
    kuda_file_t *testfile = NULL;
    kuda_file_t *errfile = NULL;
    kuda_file_t *saveerr = NULL;
    kuda_status_t rv;

    rv = kuda_file_open(&testfile, FILEPATH "testdup2.file", 
                       KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE |
                       KUDA_FOPEN_DELONCLOSE, KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, testfile);

    rv = kuda_file_open_stderr(&errfile, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    /* Set aside the real errfile */
    rv = kuda_file_dup(&saveerr, errfile, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, saveerr);

    rv = kuda_file_dup2(errfile, testfile, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, errfile);

    kuda_file_close(testfile);

    rv = kuda_file_dup2(errfile, saveerr, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, errfile);

    kuda_file_close(saveerr);
}

static void test_dup2_readwrite(abts_case *tc, void *data)
{
    kuda_file_t *errfile = NULL;
    kuda_file_t *testfile = NULL;
    kuda_file_t *saveerr = NULL;
    kuda_status_t rv;
    kuda_size_t txtlen = sizeof(TEST);
    char buff[50];
    kuda_off_t fpos;

    rv = kuda_file_open(&testfile, FILEPATH "testdup2.readwrite.file", 
                       KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE |
                       KUDA_FOPEN_DELONCLOSE, KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, testfile);

    rv = kuda_file_open_stderr(&errfile, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    /* Set aside the real errfile */
    rv = kuda_file_dup(&saveerr, errfile, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, saveerr);

    rv = kuda_file_dup2(errfile, testfile, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, errfile);

    txtlen = sizeof(TEST2);
    rv = kuda_file_write(errfile, TEST2, &txtlen);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, sizeof(TEST2), txtlen);

    fpos = 0;
    rv = kuda_file_seek(testfile, KUDA_SET, &fpos);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_ASSERT(tc, "File position mismatch, expected 0", fpos == 0);

    txtlen = 50;
    rv = kuda_file_read(testfile, buff, &txtlen);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_STR_EQUAL(tc, TEST2, buff);
      
    kuda_file_close(testfile);

    rv = kuda_file_dup2(errfile, saveerr, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, errfile);

    kuda_file_close(saveerr);
}

abts_suite *testdup(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, test_file_dup, NULL);
    abts_run_test(suite, test_file_readwrite, NULL);
    abts_run_test(suite, test_dup2, NULL);
    abts_run_test(suite, test_dup2_readwrite, NULL);

    return suite;
}

