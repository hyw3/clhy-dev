/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>

#include "testutil.h"
#include "kuda_version.h"
#include "kuda_general.h"


static void test_strings(abts_case *tc, void *data)
{
    ABTS_STR_EQUAL(tc, KUDA_VERSION_STRING, kuda_version_string());
}

#ifdef KUDA_IS_DEV_VERSION
# define IS_DEV 1
#else
# define IS_DEV 0
#endif

static void test_ints(abts_case *tc, void *data)
{
    kuda_version_t vsn;

    kuda_version(&vsn);

    ABTS_INT_EQUAL(tc, KUDA_MAJOR_VERSION, vsn.major);
    ABTS_INT_EQUAL(tc, KUDA_MINOR_VERSION, vsn.minor);
    ABTS_INT_EQUAL(tc, KUDA_PATCH_VERSION, vsn.patch);
    ABTS_INT_EQUAL(tc, IS_DEV,            vsn.is_dev);
}

abts_suite *testvsn(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, test_strings, NULL);
    abts_run_test(suite, test_ints, NULL);

    return suite;
}

