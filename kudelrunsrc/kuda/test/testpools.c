/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "kuda_general.h"
#include "kuda_pools.h"
#include "kuda_errno.h"
#include "kuda_file_io.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#include "testutil.h"

#define ALLOC_BYTES 1024

static kuda_pool_t *pmain = NULL;
static kuda_pool_t *pchild = NULL;

static void alloc_bytes(abts_case *tc, void *data)
{
    int i;
    char *alloc;
    
    alloc = kuda_palloc(pmain, ALLOC_BYTES);
    ABTS_PTR_NOTNULL(tc, alloc);

    for (i=0;i<ALLOC_BYTES;i++) {
        char *ptr = alloc + i;
        *ptr = 0xa;
    }
    /* This is just added to get the positive.  If this test fails, the
     * suite will seg fault.
     */
    ABTS_TRUE(tc, 1);
}

static void calloc_bytes(abts_case *tc, void *data)
{
    int i;
    char *alloc;
    
    alloc = kuda_pcalloc(pmain, ALLOC_BYTES);
    ABTS_PTR_NOTNULL(tc, alloc);

    for (i=0;i<ALLOC_BYTES;i++) {
        char *ptr = alloc + i;
        ABTS_TRUE(tc, *ptr == '\0');
    }
}

static void parent_pool(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_pool_create(&pmain, NULL);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, pmain);
}

static void child_pool(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_pool_create(&pchild, pmain);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, pchild);
}

static void test_ancestor(abts_case *tc, void *data)
{
    ABTS_INT_EQUAL(tc, 1, kuda_pool_is_ancestor(pmain, pchild));
}

static void test_notancestor(abts_case *tc, void *data)
{
    ABTS_INT_EQUAL(tc, 0, kuda_pool_is_ancestor(pchild, pmain));
}

static kuda_status_t success_cleanup(void *data)
{
    return KUDA_SUCCESS;
}

static char *checker_data = "Hello, world.";

static kuda_status_t checker_cleanup(void *data)
{
    return data == checker_data ? KUDA_SUCCESS : KUDA_EGENERAL;
}

static void test_cleanups(abts_case *tc, void *data)
{
    kuda_status_t rv;
    int n;

    /* do this several times to test the cleanup freelist handling. */
    for (n = 0; n < 5; n++) {
        kuda_pool_cleanup_register(pchild, NULL, success_cleanup,
                                  success_cleanup);
        kuda_pool_cleanup_register(pchild, checker_data, checker_cleanup,
                                  success_cleanup);
        kuda_pool_cleanup_register(pchild, NULL, checker_cleanup, 
                                  success_cleanup);

        rv = kuda_pool_cleanup_run(p, NULL, success_cleanup);
        ABTS_ASSERT(tc, "nullop cleanup run OK", rv == KUDA_SUCCESS);
        rv = kuda_pool_cleanup_run(p, checker_data, checker_cleanup);
        ABTS_ASSERT(tc, "cleanup passed correct data", rv == KUDA_SUCCESS);
        rv = kuda_pool_cleanup_run(p, NULL, checker_cleanup);
        ABTS_ASSERT(tc, "cleanup passed correct data", rv == KUDA_EGENERAL);

        if (n == 2) {
            /* clear the pool to check that works */
            kuda_pool_clear(pchild);
        }

        if (n % 2 == 0) {
            /* throw another random cleanup into the mix */
            kuda_pool_cleanup_register(pchild, NULL,
                                      kuda_pool_cleanup_null,
                                      kuda_pool_cleanup_null);
        }
    }
}

abts_suite *testpool(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, parent_pool, NULL);
    abts_run_test(suite, child_pool, NULL);
    abts_run_test(suite, test_ancestor, NULL);
    abts_run_test(suite, test_notancestor, NULL);
    abts_run_test(suite, alloc_bytes, NULL);
    abts_run_test(suite, calloc_bytes, NULL);
    abts_run_test(suite, test_cleanups, NULL);

    return suite;
}

