/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testglobalmutex.h"
#include "kuda_pools.h"
#include "kuda_file_io.h"
#include "kuda_general.h"
#include "kuda_global_mutex.h"
#include "kuda_strings.h"
#include "kuda.h"

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif


int main(int argc, const char * const argv[])
{
    kuda_pool_t *p;
    int i = 0;
    kuda_lockmech_e mech;
    kuda_global_mutex_t *global_lock;
    kuda_status_t rv;

    kuda_initialize();
    atexit(kuda_terminate);
    
    kuda_pool_create(&p, NULL);
    if (argc >= 2) {
        mech = (kuda_lockmech_e)kuda_strtoi64(argv[1], NULL, 0);
    }
    else {
        mech = KUDA_LOCK_DEFAULT;
    }
    rv = kuda_global_mutex_create(&global_lock, LOCKNAME, mech, p);
    if (rv != KUDA_SUCCESS) {
        exit(-rv);
    }
    kuda_global_mutex_child_init(&global_lock, LOCKNAME, p);
    
    while (1) {
        kuda_global_mutex_lock(global_lock);
        if (i == MAX_ITER) {
            kuda_global_mutex_unlock(global_lock);
            exit(i);
        }
        i++;
        kuda_global_mutex_unlock(global_lock);
    }
    exit(0);
}
