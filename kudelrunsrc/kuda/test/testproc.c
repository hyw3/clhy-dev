/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_thread_proc.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "testutil.h"

#define TESTSTR "This is a test"

#define PROC_CHILD_NAME TESTBINPATH "proc_child" EXTENSION

static char *proc_child;

static kuda_proc_t newproc;

static void test_create_proc(abts_case *tc, void *data)
{
    const char *args[2];
    kuda_procattr_t *attr;
    kuda_file_t *testfile = NULL;
    kuda_status_t rv;
    kuda_size_t length;
    char *buf;

    rv = kuda_procattr_create(&attr, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_procattr_io_set(attr, KUDA_FULL_BLOCK, KUDA_FULL_BLOCK, 
                             KUDA_NO_PIPE);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_procattr_dir_set(attr, "data");
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_procattr_cmdtype_set(attr, KUDA_PROGRAM_ENV);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    args[0] = "proc_child" EXTENSION;
    args[1] = NULL;
    
    rv = kuda_proc_create(&newproc, proc_child, args, NULL, 
                         attr, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    testfile = newproc.in;

    length = strlen(TESTSTR);
    rv = kuda_file_write(testfile, TESTSTR, &length);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, strlen(TESTSTR), length);

    testfile = newproc.out;
    length = 256;
    buf = kuda_pcalloc(p, length);
    rv = kuda_file_read(testfile, buf, &length);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_STR_EQUAL(tc, TESTSTR, buf);
}

static void test_proc_wait(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_proc_wait(&newproc, NULL, NULL, KUDA_WAIT);
    ABTS_INT_EQUAL(tc, KUDA_CHILD_DONE, rv);
}

static void test_file_redir(abts_case *tc, void *data)
{
    kuda_file_t *testout = NULL;
    kuda_file_t *testerr = NULL;
    kuda_off_t offset;
    kuda_status_t rv;
    const char *args[2];
    kuda_procattr_t *attr;
    kuda_file_t *testfile = NULL;
    kuda_size_t length;
    char *buf;

    testfile = NULL;
    rv = kuda_file_open(&testfile, "data/stdin",
                       KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE | KUDA_FOPEN_EXCL,
                       KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_file_open(&testout, "data/stdout",
                       KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE | KUDA_FOPEN_EXCL,
                       KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_file_open(&testerr, "data/stderr",
                       KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE | KUDA_FOPEN_EXCL,
                       KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    length = strlen(TESTSTR);
    kuda_file_write(testfile, TESTSTR, &length);
    offset = 0;
    rv = kuda_file_seek(testfile, KUDA_SET, &offset);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_ASSERT(tc, "File position mismatch, expected 0", offset == 0);

    rv = kuda_procattr_create(&attr, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_procattr_child_in_set(attr, testfile, NULL);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_procattr_child_out_set(attr, testout, NULL);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_procattr_child_err_set(attr, testerr, NULL);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_procattr_dir_set(attr, "data");
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_procattr_cmdtype_set(attr, KUDA_PROGRAM_ENV);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    args[0] = "proc_child";
    args[1] = NULL;

    rv = kuda_proc_create(&newproc, proc_child, args, NULL, 
                         attr, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_proc_wait(&newproc, NULL, NULL, KUDA_WAIT);
    ABTS_INT_EQUAL(tc, KUDA_CHILD_DONE, rv);

    offset = 0;
    rv = kuda_file_seek(testout, KUDA_SET, &offset);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    length = 256;
    buf = kuda_pcalloc(p, length);
    rv = kuda_file_read(testout, buf, &length);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_STR_EQUAL(tc, TESTSTR, buf);


    kuda_file_close(testfile);
    kuda_file_close(testout);
    kuda_file_close(testerr);

    rv = kuda_file_remove("data/stdin", p);;
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_file_remove("data/stdout", p);;
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_file_remove("data/stderr", p);;
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

abts_suite *testproc(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    kuda_filepath_merge(&proc_child, NULL, PROC_CHILD_NAME, 0, p);
    abts_run_test(suite, test_create_proc, NULL);
    abts_run_test(suite, test_proc_wait, NULL);
    abts_run_test(suite, test_file_redir, NULL);

    return suite;
}

