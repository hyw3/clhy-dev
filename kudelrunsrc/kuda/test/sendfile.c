/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include "kuda_network_io.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_poll.h"
#include "kuda_thread_proc.h"

#include "testutil.h"

#if !KUDA_HAS_SENDFILE
int main(void)
{
    fprintf(stderr, 
            "This program won't work on this platform because there is no "
            "support for sendfile().\n");
    return 0;
}
#else /* !KUDA_HAS_SENDFILE */

#define FILE_LENGTH    200000

#define FILE_DATA_CHAR '0'

#define HDR1           "1234567890ABCD\n"
#define HDR2           "EFGH\n"
#define HDR3_LEN       80000
#define HDR3_CHAR      '^'
#define TRL1           "IJKLMNOPQRSTUVWXYZ\n"
#define TRL2           "!@#$%&*()\n"
#define TRL3_LEN       90000
#define TRL3_CHAR      '@'

#define TESTSF_PORT    8021

#define TESTFILE       "testsf.dat"

typedef enum {BLK, NONBLK, TIMEOUT} client_socket_mode_t;

static void kudaerr(const char *fn, kuda_status_t rv)
{
    char buf[120];

    fprintf(stderr, "%s->%d/%s\n",
            fn, rv, kuda_strerror(rv, buf, sizeof buf));
    exit(1);
}

static void kuda_setup(kuda_pool_t *p, kuda_socket_t **sock, int *family)
{
    kuda_status_t rv;

    *sock = NULL;
    rv = kuda_socket_create(sock, *family, SOCK_STREAM, 0, p);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_socket_create()", rv);
    }

    if (*family == KUDA_UNSPEC) {
        kuda_sockaddr_t *localsa;

        rv = kuda_socket_addr_get(&localsa, KUDA_LOCAL, *sock);
        if (rv != KUDA_SUCCESS) {
            kudaerr("kuda_socket_addr_get()", rv);
        }
        *family = localsa->family;
    }
}

static void create_testfile(kuda_pool_t *p, const char *fname)
{
    kuda_file_t *f = NULL;
    kuda_status_t rv;
    char buf[120];
    int i;
    kuda_finfo_t finfo;

    printf("Creating a test file...\n");
    rv = kuda_file_open(&f, fname, 
                 KUDA_FOPEN_CREATE | KUDA_FOPEN_WRITE | KUDA_FOPEN_TRUNCATE | KUDA_FOPEN_BUFFERED,
                 KUDA_UREAD | KUDA_UWRITE, p);
    if (rv) {
        kudaerr("kuda_file_open()", rv);
    }
    
    buf[0] = FILE_DATA_CHAR;
    buf[1] = '\0';
    for (i = 0; i < FILE_LENGTH; i++) {
        /* exercise kuda_file_putc() and kuda_file_puts() on buffered files */
        if ((i % 2) == 0) {
            rv = kuda_file_putc(buf[0], f);
            if (rv) {
                kudaerr("kuda_file_putc()", rv);
            }
        }
        else {
            rv = kuda_file_puts(buf, f);
            if (rv) {
                kudaerr("kuda_file_puts()", rv);
            }
        }
    }

    rv = kuda_file_close(f);
    if (rv) {
        kudaerr("kuda_file_close()", rv);
    }

    rv = kuda_stat(&finfo, fname, KUDA_FINFO_NORM, p);
    if (rv != KUDA_SUCCESS && ! KUDA_STATUS_IS_INCOMPLETE(rv)) {
        kudaerr("kuda_stat()", rv);
    }

    if (finfo.size != FILE_LENGTH) {
        fprintf(stderr, 
                "test file %s should be %ld-bytes long\n"
                "instead it is %ld-bytes long\n",
                fname,
                (long int)FILE_LENGTH,
                (long int)finfo.size);
        exit(1);
    }
}

static void spawn_server(kuda_pool_t *p, kuda_proc_t *out_proc)
{
    kuda_proc_t proc = {0};
    kuda_procattr_t *procattr;
    kuda_status_t rv;
    const char *args[3];

    rv = kuda_procattr_create(&procattr, p);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_procattr_create()", rv);
    }

    rv = kuda_procattr_io_set(procattr, KUDA_CHILD_BLOCK, KUDA_CHILD_BLOCK,
                             KUDA_CHILD_BLOCK);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_procattr_io_set()", rv);
    }

    rv = kuda_procattr_cmdtype_set(procattr, KUDA_PROGRAM_ENV);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_procattr_cmdtype_set()", rv);
    }

    rv = kuda_procattr_error_check_set(procattr, 1);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_procattr_error_check_set()", rv);
    }

    args[0] = "sendfile" EXTENSION;
    args[1] = "server";
    args[2] = NULL;
    rv = kuda_proc_create(&proc, TESTBINPATH "sendfile" EXTENSION, args, NULL, procattr, p);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_proc_create()", rv);
    }

    *out_proc = proc;
}

static int client(kuda_pool_t *p, client_socket_mode_t socket_mode,
                  const char *host, int start_server)
{
    kuda_status_t rv, tmprv;
    kuda_socket_t *sock;
    char buf[120];
    kuda_file_t *f = NULL;
    kuda_size_t len;
    kuda_size_t expected_len;
    kuda_off_t current_file_offset;
    kuda_hdtr_t hdtr;
    struct iovec headers[3];
    struct iovec trailers[3];
    kuda_size_t bytes_read;
    kuda_pollset_t *pset;
    kuda_int32_t nsocks;
    int connect_tries = 1;
    int i;
    int family;
    kuda_sockaddr_t *destsa;
    kuda_proc_t server;
    kuda_interval_time_t connect_retry_interval = kuda_time_from_msec(50);

    if (start_server) {
        spawn_server(p, &server);
        connect_tries = 5; /* give it a chance to start up */
    }

    create_testfile(p, TESTFILE);

    rv = kuda_file_open(&f, TESTFILE, KUDA_FOPEN_READ, 0, p);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_file_open()", rv);
    }

    if (!host) {
        host = "127.0.0.1";
    }
    family = KUDA_INET;
    rv = kuda_sockaddr_info_get(&destsa, host, family, TESTSF_PORT, 0, p);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_sockaddr_info_get()", rv);
    }

    while (connect_tries--) {
        kuda_setup(p, &sock, &family);
        rv = kuda_socket_connect(sock, destsa);
        if (connect_tries && KUDA_STATUS_IS_ECONNREFUSED(rv)) {
            kuda_status_t tmprv = kuda_socket_close(sock);
            if (tmprv != KUDA_SUCCESS) {
                kudaerr("kuda_socket_close()", tmprv);
            }
            kuda_sleep(connect_retry_interval);
            connect_retry_interval *= 2;
        }
        else {
            break;
        }
    }
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_socket_connect()", rv);
    }

    switch(socket_mode) {
    case BLK:
        /* leave it blocking */
        break;
    case NONBLK:
        /* set it non-blocking */
        rv = kuda_socket_opt_set(sock, KUDA_SO_NONBLOCK, 1);
        if (rv != KUDA_SUCCESS) {
            kudaerr("kuda_socket_opt_set(KUDA_SO_NONBLOCK)", rv);
        }
        break;
    case TIMEOUT:
        /* set a timeout */
        rv = kuda_socket_timeout_set(sock, 100 * KUDA_USEC_PER_SEC);
        if (rv != KUDA_SUCCESS) {
            kudaerr("kuda_socket_opt_set(KUDA_SO_NONBLOCK)", rv);
            exit(1);
        }
        break;
    default:
        assert(1 != 1);
    }

    printf("Sending the file...\n");

    hdtr.headers = headers;
    hdtr.numheaders = 3;
    hdtr.headers[0].iov_base = HDR1;
    hdtr.headers[0].iov_len  = strlen(hdtr.headers[0].iov_base);
    hdtr.headers[1].iov_base = HDR2;
    hdtr.headers[1].iov_len  = strlen(hdtr.headers[1].iov_base);
    hdtr.headers[2].iov_base = malloc(HDR3_LEN);
    assert(hdtr.headers[2].iov_base);
    memset(hdtr.headers[2].iov_base, HDR3_CHAR, HDR3_LEN);
    hdtr.headers[2].iov_len  = HDR3_LEN;

    hdtr.trailers = trailers;
    hdtr.numtrailers = 3;
    hdtr.trailers[0].iov_base = TRL1;
    hdtr.trailers[0].iov_len  = strlen(hdtr.trailers[0].iov_base);
    hdtr.trailers[1].iov_base = TRL2;
    hdtr.trailers[1].iov_len  = strlen(hdtr.trailers[1].iov_base);
    hdtr.trailers[2].iov_base = malloc(TRL3_LEN);
    memset(hdtr.trailers[2].iov_base, TRL3_CHAR, TRL3_LEN);
    assert(hdtr.trailers[2].iov_base);
    hdtr.trailers[2].iov_len  = TRL3_LEN;

    expected_len = 
        strlen(HDR1) + strlen(HDR2) + HDR3_LEN +
        strlen(TRL1) + strlen(TRL2) + TRL3_LEN +
        FILE_LENGTH;
    
    if (socket_mode == BLK) {
        current_file_offset = 0;
        len = FILE_LENGTH;
        rv = kuda_socket_sendfile(sock, f, &hdtr, &current_file_offset, &len, 0);
        if (rv != KUDA_SUCCESS) {
            kudaerr("kuda_socket_sendfile()", rv);
        }
        
        printf("kuda_socket_sendfile() updated offset with %ld\n",
               (long int)current_file_offset);
        
        printf("kuda_socket_sendfile() updated len with %ld\n",
               (long int)len);
        
        printf("bytes really sent: %" KUDA_SIZE_T_FMT "\n",
               expected_len);

        if (len != expected_len) {
            fprintf(stderr, "kuda_socket_sendfile() didn't report the correct "
                    "number of bytes sent!\n");
            exit(1);
        }
    }
    else {
        /* non-blocking... wooooooo */
        kuda_size_t total_bytes_sent;
        kuda_pollfd_t pfd;

        pset = NULL;
        rv = kuda_pollset_create(&pset, 1, p, 0);
        assert(!rv);
        pfd.p = p;
        pfd.desc_type = KUDA_POLL_SOCKET;
        pfd.reqevents = KUDA_POLLOUT;
        pfd.rtnevents = 0;
        pfd.desc.s = sock;
        pfd.client_data = NULL;

        rv = kuda_pollset_add(pset, &pfd);        
        assert(!rv);

        total_bytes_sent = 0;
        current_file_offset = 0;
        len = FILE_LENGTH;
        do {
            kuda_size_t tmplen;

            tmplen = len; /* bytes remaining to send from the file */
            printf("Calling kuda_socket_sendfile()...\n");
            printf("Headers (%d):\n", hdtr.numheaders);
            for (i = 0; i < hdtr.numheaders; i++) {
                printf("\t%ld bytes (%c)\n",
                       (long)hdtr.headers[i].iov_len,
                       *(char *)hdtr.headers[i].iov_base);
            }
            printf("File: %ld bytes from offset %ld\n",
                   (long)tmplen, (long)current_file_offset);
            printf("Trailers (%d):\n", hdtr.numtrailers);
            for (i = 0; i < hdtr.numtrailers; i++) {
                printf("\t%ld bytes\n",
                       (long)hdtr.trailers[i].iov_len);
            }

            rv = kuda_socket_sendfile(sock, f, &hdtr, &current_file_offset, &tmplen, 0);
            printf("kuda_socket_sendfile()->%d, sent %ld bytes\n", rv, (long)tmplen);
            if (rv) {
                if (KUDA_STATUS_IS_EAGAIN(rv)) {
                    assert(tmplen == 0);
                    nsocks = 1;
                    tmprv = kuda_pollset_poll(pset, -1, &nsocks, NULL);
                    assert(!tmprv);
                    assert(nsocks == 1);
                    /* continue; */
                }
            }

            total_bytes_sent += tmplen;

            /* Adjust hdtr to compensate for partially-written
             * data.
             */

            /* First, skip over any header data which might have
             * been written.
             */
            while (tmplen && hdtr.numheaders) {
                if (tmplen >= hdtr.headers[0].iov_len) {
                    tmplen -= hdtr.headers[0].iov_len;
                    --hdtr.numheaders;
                    ++hdtr.headers;
                }
                else {
                    hdtr.headers[0].iov_len -= tmplen;
                    hdtr.headers[0].iov_base = 
			(char*) hdtr.headers[0].iov_base + tmplen;
                    tmplen = 0;
                }
            }

            /* Now, skip over any file data which might have been
             * written.
             */

            if (tmplen <= len) {
                current_file_offset += tmplen;
                len -= tmplen;
                tmplen = 0;
            }
            else {
                tmplen -= len;
                len = 0;
                current_file_offset = 0;
            }

            /* Last, skip over any trailer data which might have
             * been written.
             */

            while (tmplen && hdtr.numtrailers) {
                if (tmplen >= hdtr.trailers[0].iov_len) {
                    tmplen -= hdtr.trailers[0].iov_len;
                    --hdtr.numtrailers;
                    ++hdtr.trailers;
                }
                else {
                    hdtr.trailers[0].iov_len -= tmplen;
                    hdtr.trailers[0].iov_base = 
			(char *)hdtr.trailers[0].iov_base + tmplen;
                    tmplen = 0;
                }
            }

        } while (total_bytes_sent < expected_len &&
                 (rv == KUDA_SUCCESS || 
                 (KUDA_STATUS_IS_EAGAIN(rv) && socket_mode != TIMEOUT)));
        if (total_bytes_sent != expected_len) {
            fprintf(stderr,
                    "client problem: sent %ld of %ld bytes\n",
                    (long)total_bytes_sent, (long)expected_len);
            exit(1);
        }

        if (rv) {
            fprintf(stderr,
                    "client problem: rv %d\n",
                    rv);
            exit(1);
        }
    }
    
    current_file_offset = 0;
    rv = kuda_file_seek(f, KUDA_CUR, &current_file_offset);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_file_seek()", rv);
    }

    printf("After kuda_socket_sendfile(), the kernel file pointer is "
           "at offset %ld.\n",
           (long int)current_file_offset);

    rv = kuda_socket_shutdown(sock, KUDA_SHUTDOWN_WRITE);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_socket_shutdown()", rv);
    }

    /* in case this is the non-blocking test, set socket timeout;
     * we're just waiting for EOF */

    rv = kuda_socket_timeout_set(sock, kuda_time_from_sec(3));
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_socket_timeout_set()", rv);
    }
    
    bytes_read = 1;
    rv = kuda_socket_recv(sock, buf, &bytes_read);
    if (rv != KUDA_EOF) {
        kudaerr("kuda_socket_recv() (expected KUDA_EOF)", rv);
    }
    if (bytes_read != 0) {
        fprintf(stderr, "We expected to get 0 bytes read with KUDA_EOF\n"
                "but instead we read %ld bytes.\n",
                (long int)bytes_read);
        exit(1);
    }

    printf("client: kuda_socket_sendfile() worked as expected!\n");

    rv = kuda_file_remove(TESTFILE, p);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_file_remove()", rv);
    }

    if (start_server) {
        kuda_exit_why_e exitwhy;
        kuda_size_t nbytes;
        char responsebuf[1024];
        int exitcode;

        rv = kuda_file_pipe_timeout_set(server.out, kuda_time_from_sec(2));
        if (rv != KUDA_SUCCESS) {
            kudaerr("kuda_file_pipe_timeout_set()", rv);
        }
        nbytes = sizeof(responsebuf);
        rv = kuda_file_read(server.out, responsebuf, &nbytes);
        if (rv != KUDA_SUCCESS) {
            kudaerr("kuda_file_read() messages from server", rv);
        }
        printf("%.*s", (int)nbytes, responsebuf);
        rv = kuda_proc_wait(&server, &exitcode, &exitwhy, KUDA_WAIT);
        if (rv != KUDA_CHILD_DONE) {
            kudaerr("kuda_proc_wait() (expected KUDA_CHILD_DONE)", rv);
        }
        if (exitcode != 0) {
            fprintf(stderr, "sendfile server returned %d\n", exitcode);
            exit(1);
        }
    }

    return 0;
}

static int server(kuda_pool_t *p)
{
    kuda_status_t rv;
    kuda_socket_t *sock;
    char buf[120];
    int i;
    kuda_socket_t *newsock = NULL;
    kuda_size_t bytes_read;
    kuda_sockaddr_t *localsa;
    int family;

    family = KUDA_INET;
    kuda_setup(p, &sock, &family);

    rv = kuda_socket_opt_set(sock, KUDA_SO_REUSEADDR, 1);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_socket_opt_set()", rv);
    }

    rv = kuda_sockaddr_info_get(&localsa, NULL, family, TESTSF_PORT, 0, p);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_sockaddr_info_get()", rv);
    }

    rv = kuda_socket_bind(sock, localsa);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_socket_bind()", rv);
    }

    rv = kuda_socket_listen(sock, 5);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_socket_listen()", rv);
    }

    printf("Waiting for a client to connect...\n");

    rv = kuda_socket_accept(&newsock, sock, p);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_socket_accept()", rv);
    }

    printf("Processing a client...\n");

    assert(sizeof buf > strlen(HDR1));
    bytes_read = strlen(HDR1);
    rv = kuda_socket_recv(newsock, buf, &bytes_read);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_socket_recv()", rv);
    }
    if (bytes_read != strlen(HDR1)) {
        fprintf(stderr, "wrong data read (1)\n");
        exit(1);
    }
    if (memcmp(buf, HDR1, strlen(HDR1))) {
        fprintf(stderr, "wrong data read (2)\n");
        fprintf(stderr, "received: `%.*s'\nexpected: `%s'\n",
                (int)bytes_read, buf, HDR1);
        exit(1);
    }
        
    assert(sizeof buf > strlen(HDR2));
    bytes_read = strlen(HDR2);
    rv = kuda_socket_recv(newsock, buf, &bytes_read);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_socket_recv()", rv);
    }
    if (bytes_read != strlen(HDR2)) {
        fprintf(stderr, "wrong data read (3)\n");
        exit(1);
    }
    if (memcmp(buf, HDR2, strlen(HDR2))) {
        fprintf(stderr, "wrong data read (4)\n");
        fprintf(stderr, "received: `%.*s'\nexpected: `%s'\n",
                (int)bytes_read, buf, HDR2);
        exit(1);
    }

    for (i = 0; i < HDR3_LEN; i++) {
        bytes_read = 1;
        rv = kuda_socket_recv(newsock, buf, &bytes_read);
        if (rv != KUDA_SUCCESS) {
            kudaerr("kuda_socket_recv()", rv);
        }
        if (bytes_read != 1) {
            fprintf(stderr, "kuda_socket_recv()->%ld bytes instead of 1\n",
                    (long int)bytes_read);
            exit(1);
        }
        if (buf[0] != HDR3_CHAR) {
            fprintf(stderr,
                    "problem with data read (byte %d of hdr 3):\n",
                    i);
            fprintf(stderr, "read `%c' (0x%x) from client; expected "
                    "`%c'\n",
                    buf[0], buf[0], HDR3_CHAR);
            exit(1);
        }
    }
        
    for (i = 0; i < FILE_LENGTH; i++) {
        bytes_read = 1;
        rv = kuda_socket_recv(newsock, buf, &bytes_read);
        if (rv != KUDA_SUCCESS) {
            kudaerr("kuda_socket_recv()", rv);
        }
        if (bytes_read != 1) {
            fprintf(stderr, "kuda_socket_recv()->%ld bytes instead of 1\n",
                    (long int)bytes_read);
            exit(1);
        }
        if (buf[0] != FILE_DATA_CHAR) {
            fprintf(stderr,
                    "problem with data read (byte %d of file):\n",
                    i);
            fprintf(stderr, "read `%c' (0x%x) from client; expected "
                    "`%c'\n",
                    buf[0], buf[0], FILE_DATA_CHAR);
            exit(1);
        }
    }
        
    assert(sizeof buf > strlen(TRL1));
    bytes_read = strlen(TRL1);
    rv = kuda_socket_recv(newsock, buf, &bytes_read);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_socket_recv()", rv);
    }
    if (bytes_read != strlen(TRL1)) {
        fprintf(stderr, "wrong data read (5)\n");
        exit(1);
    }
    if (memcmp(buf, TRL1, strlen(TRL1))) {
        fprintf(stderr, "wrong data read (6)\n");
        fprintf(stderr, "received: `%.*s'\nexpected: `%s'\n",
                (int)bytes_read, buf, TRL1);
        exit(1);
    }
        
    assert(sizeof buf > strlen(TRL2));
    bytes_read = strlen(TRL2);
    rv = kuda_socket_recv(newsock, buf, &bytes_read);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_socket_recv()", rv);
    }
    if (bytes_read != strlen(TRL2)) {
        fprintf(stderr, "wrong data read (7)\n");
        exit(1);
    }
    if (memcmp(buf, TRL2, strlen(TRL2))) {
        fprintf(stderr, "wrong data read (8)\n");
        fprintf(stderr, "received: `%.*s'\nexpected: `%s'\n",
                (int)bytes_read, buf, TRL2);
        exit(1);
    }

    for (i = 0; i < TRL3_LEN; i++) {
        bytes_read = 1;
        rv = kuda_socket_recv(newsock, buf, &bytes_read);
        if (rv != KUDA_SUCCESS) {
            kudaerr("kuda_socket_recv()", rv);
        }
        if (bytes_read != 1) {
            fprintf(stderr, "kuda_socket_recv()->%ld bytes instead of 1\n",
                    (long int)bytes_read);
            exit(1);
        }
        if (buf[0] != TRL3_CHAR) {
            fprintf(stderr,
                    "problem with data read (byte %d of trl 3):\n",
                    i);
            fprintf(stderr, "read `%c' (0x%x) from client; expected "
                    "`%c'\n",
                    buf[0], buf[0], TRL3_CHAR);
            exit(1);
        }
    }
        
    bytes_read = 1;
    rv = kuda_socket_recv(newsock, buf, &bytes_read);
    if (rv != KUDA_EOF) {
        kudaerr("kuda_socket_recv() (expected KUDA_EOF)", rv);
    }
    if (bytes_read != 0) {
        fprintf(stderr, "We expected to get 0 bytes read with KUDA_EOF\n"
                "but instead we read %ld bytes (%c).\n",
                (long int)bytes_read, buf[0]);
        exit(1);
    }

    printf("server: kuda_socket_sendfile() worked as expected!\n");

    return 0;
}

int main(int argc, char *argv[])
{
    kuda_pool_t *p;
    kuda_status_t rv;

#ifdef SIGPIPE
    signal(SIGPIPE, SIG_IGN);
#endif

    rv = kuda_initialize();
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_initialize()", rv);
    }

    atexit(kuda_terminate);

    rv = kuda_pool_create(&p, NULL);
    if (rv != KUDA_SUCCESS) {
        kudaerr("kuda_pool_create()", rv);
    }

    if (argc >= 2 && !strcmp(argv[1], "client")) {
        const char *host = NULL;
        int mode = BLK;
        int start_server = 0;
        int i;

        for (i = 2; i < argc; i++) {
            if (!strcmp(argv[i], "blocking")) {
                mode = BLK;
            }
            else if (!strcmp(argv[i], "timeout")) {
                mode = TIMEOUT;
            }
            else if (!strcmp(argv[i], "nonblocking")) {
                mode = NONBLK;
            }
            else if (!strcmp(argv[i], "startserver")) {
                start_server = 1;
            }
            else {
                host = argv[i];
            }	
        }
        return client(p, mode, host, start_server);
    }
    else if (argc == 2 && !strcmp(argv[1], "server")) {
        return server(p);
    }

    fprintf(stderr, 
            "Usage: %s client {blocking|nonblocking|timeout} [startserver] [server-host]\n"
            "       %s server\n",
            argv[0], argv[0]);
    return -1;
}

#endif /* !KUDA_HAS_SENDFILE */
