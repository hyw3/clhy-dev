#include "kuda.h"
#include "kuda_file_io.h"
#include "kuda.h"

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif

int main(void)
{
    char buf[256];
    kuda_file_t *err;
    kuda_pool_t *p;

    kuda_initialize();
    atexit(kuda_terminate);

    kuda_pool_create(&p, NULL);
    kuda_file_open_stdin(&err, p);

    while (1) {
        kuda_size_t length = 256;
        kuda_file_read(err, buf, &length);
    }
    exit(0); /* just to keep the compiler happy */
}
