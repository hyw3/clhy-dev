/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testutil.h"
#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_general.h"
#include "kuda_pools.h"
#include "kuda_hash.h"

#define MAX_LTH 256
#define MAX_DEPTH 11

static int comp_string(const void *str1, const void *str2)
{
    return strcmp(str1,str2);
}

static void dump_hash(kuda_pool_t *p, kuda_hash_t *h, char str[][MAX_LTH]) 
{
    kuda_hash_index_t *hi;
    int i = 0;

    for (hi = kuda_hash_first(p, h); hi; hi = kuda_hash_next(hi)) {
        const char *key = kuda_hash_this_key(hi);
        kuda_ssize_t len = kuda_hash_this_key_len(hi);
        char *val = kuda_hash_this_val(hi);

        str[i][0]='\0';
        kuda_snprintf(str[i], MAX_LTH, "%sKey %s (%" KUDA_SSIZE_T_FMT ") Value %s\n",
                 str[i], key, len, val);
        i++;
    }
    str[i][0]='\0';
    kuda_snprintf(str[i], MAX_LTH, "%s#entries %d\n", str[i], i);

    /* Sort the result strings so that they can be checked for expected results easily,
     * without having to worry about platform quirks
     */
    qsort(
        str, /* Pointer to elements */
        i,   /* number of elements */
        MAX_LTH, /* size of one element */
        comp_string /* Pointer to comparison routine */
    );
}

static void sum_hash(kuda_pool_t *p, kuda_hash_t *h, int *pcount, int *keySum, int *valSum) 
{
    kuda_hash_index_t *hi;
    void *val, *key;
    int count = 0;

    *keySum = 0;
    *valSum = 0;
    *pcount = 0;
    for (hi = kuda_hash_first(p, h); hi; hi = kuda_hash_next(hi)) {
        kuda_hash_this(hi, (void*)&key, NULL, &val);
        *valSum += *(int *)val;
        *keySum += *(int *)key;
        count++;
    }
    *pcount=count;
}

static void hash_make(abts_case *tc, void *data)
{
    kuda_hash_t *h = NULL;

    h = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, h);
}

static void hash_set(abts_case *tc, void *data)
{
    kuda_hash_t *h = NULL;
    char *result = NULL;

    h = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, h);

    kuda_hash_set(h, "key", KUDA_HASH_KEY_STRING, "value");
    result = kuda_hash_get(h, "key", KUDA_HASH_KEY_STRING);
    ABTS_STR_EQUAL(tc, "value", result);
}

static void hash_reset(abts_case *tc, void *data)
{
    kuda_hash_t *h = NULL;
    char *result = NULL;

    h = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, h);

    kuda_hash_set(h, "key", KUDA_HASH_KEY_STRING, "value");
    result = kuda_hash_get(h, "key", KUDA_HASH_KEY_STRING);
    ABTS_STR_EQUAL(tc, "value", result);

    kuda_hash_set(h, "key", KUDA_HASH_KEY_STRING, "new");
    result = kuda_hash_get(h, "key", KUDA_HASH_KEY_STRING);
    ABTS_STR_EQUAL(tc, "new", result);
}

static void same_value(abts_case *tc, void *data)
{
    kuda_hash_t *h = NULL;
    char *result = NULL;

    h = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, h);

    kuda_hash_set(h, "same1", KUDA_HASH_KEY_STRING, "same");
    result = kuda_hash_get(h, "same1", KUDA_HASH_KEY_STRING);
    ABTS_STR_EQUAL(tc, "same", result);

    kuda_hash_set(h, "same2", KUDA_HASH_KEY_STRING, "same");
    result = kuda_hash_get(h, "same2", KUDA_HASH_KEY_STRING);
    ABTS_STR_EQUAL(tc, "same", result);
}

static unsigned int hash_custom( const char *key, kuda_ssize_t *klen)
{
    unsigned int hash = 0;
    while( *klen ) {
        (*klen) --;
        hash = hash * 33 + key[ *klen ];
    }
    return hash;
}

static void same_value_custom(abts_case *tc, void *data)
{
    kuda_hash_t *h = NULL;
    char *result = NULL;

    h = kuda_hash_make_custom(p, hash_custom);
    ABTS_PTR_NOTNULL(tc, h);

    kuda_hash_set(h, "same1", 5, "same");
    result = kuda_hash_get(h, "same1", 5);
    ABTS_STR_EQUAL(tc, "same", result);

    kuda_hash_set(h, "same2", 5, "same");
    result = kuda_hash_get(h, "same2", 5);
    ABTS_STR_EQUAL(tc, "same", result);
}

static void key_space(abts_case *tc, void *data)
{
    kuda_hash_t *h = NULL;
    char *result = NULL;

    h = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, h);

    kuda_hash_set(h, "key with space", KUDA_HASH_KEY_STRING, "value");
    result = kuda_hash_get(h, "key with space", KUDA_HASH_KEY_STRING);
    ABTS_STR_EQUAL(tc, "value", result);
}

static void hash_clear(abts_case *tc, void *data)
{
    kuda_hash_t *h;
    int i, *e;

    h = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, h);

    for (i = 1; i <= 10; i++) {
        e = kuda_palloc(p, sizeof(int));
        *e = i;
        kuda_hash_set(h, e, sizeof(*e), e);
    }
    kuda_hash_clear(h);
    i = kuda_hash_count(h);
    ABTS_INT_EQUAL(tc, 0, i);
}

/* This is kind of a hack, but I am just keeping an existing test.  This is
 * really testing kuda_hash_first, kuda_hash_next, and kuda_hash_this which 
 * should be tested in three separate tests, but this will do for now.
 */
static void hash_traverse(abts_case *tc, void *data)
{
    kuda_hash_t *h;
    char StrArray[MAX_DEPTH][MAX_LTH];

    h = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, h);

    kuda_hash_set(h, "OVERWRITE", KUDA_HASH_KEY_STRING, "should not see this");
    kuda_hash_set(h, "FOO3", KUDA_HASH_KEY_STRING, "bar3");
    kuda_hash_set(h, "FOO3", KUDA_HASH_KEY_STRING, "bar3");
    kuda_hash_set(h, "FOO1", KUDA_HASH_KEY_STRING, "bar1");
    kuda_hash_set(h, "FOO2", KUDA_HASH_KEY_STRING, "bar2");
    kuda_hash_set(h, "FOO4", KUDA_HASH_KEY_STRING, "bar4");
    kuda_hash_set(h, "SAME1", KUDA_HASH_KEY_STRING, "same");
    kuda_hash_set(h, "SAME2", KUDA_HASH_KEY_STRING, "same");
    kuda_hash_set(h, "OVERWRITE", KUDA_HASH_KEY_STRING, "Overwrite key");

    dump_hash(p, h, StrArray);

    ABTS_STR_EQUAL(tc, "Key FOO1 (4) Value bar1\n", StrArray[0]);
    ABTS_STR_EQUAL(tc, "Key FOO2 (4) Value bar2\n", StrArray[1]);
    ABTS_STR_EQUAL(tc, "Key FOO3 (4) Value bar3\n", StrArray[2]);
    ABTS_STR_EQUAL(tc, "Key FOO4 (4) Value bar4\n", StrArray[3]);
    ABTS_STR_EQUAL(tc, "Key OVERWRITE (9) Value Overwrite key\n", StrArray[4]);
    ABTS_STR_EQUAL(tc, "Key SAME1 (5) Value same\n", StrArray[5]);
    ABTS_STR_EQUAL(tc, "Key SAME2 (5) Value same\n", StrArray[6]);
    ABTS_STR_EQUAL(tc, "#entries 7\n", StrArray[7]);
}

/* This is kind of a hack, but I am just keeping an existing test.  This is
 * really testing kuda_hash_first, kuda_hash_next, and kuda_hash_this which 
 * should be tested in three separate tests, but this will do for now.
 */
static void summation_test(abts_case *tc, void *data)
{
    kuda_hash_t *h;
    int sumKeys, sumVal, trySumKey, trySumVal;
    int i, j, *val, *key;

    h =kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, h);

    sumKeys = 0;
    sumVal = 0;
    trySumKey = 0;
    trySumVal = 0;

    for (i = 0; i < 100; i++) {
        j = i * 10 + 1;
        sumKeys += j;
        sumVal += i;
        key = kuda_palloc(p, sizeof(int));
        *key = j;
        val = kuda_palloc(p, sizeof(int));
        *val = i;
        kuda_hash_set(h, key, sizeof(int), val);
    }

    sum_hash(p, h, &i, &trySumKey, &trySumVal);
    ABTS_INT_EQUAL(tc, 100, i);
    ABTS_INT_EQUAL(tc, sumVal, trySumVal);
    ABTS_INT_EQUAL(tc, sumKeys, trySumKey);
}

static void delete_key(abts_case *tc, void *data)
{
    kuda_hash_t *h = NULL;
    char *result = NULL;

    h = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, h);

    kuda_hash_set(h, "key", KUDA_HASH_KEY_STRING, "value");
    kuda_hash_set(h, "key2", KUDA_HASH_KEY_STRING, "value2");

    result = kuda_hash_get(h, "key", KUDA_HASH_KEY_STRING);
    ABTS_STR_EQUAL(tc, "value", result);

    result = kuda_hash_get(h, "key2", KUDA_HASH_KEY_STRING);
    ABTS_STR_EQUAL(tc, "value2", result);

    kuda_hash_set(h, "key", KUDA_HASH_KEY_STRING, NULL);

    result = kuda_hash_get(h, "key", KUDA_HASH_KEY_STRING);
    ABTS_PTR_EQUAL(tc, NULL, result);

    result = kuda_hash_get(h, "key2", KUDA_HASH_KEY_STRING);
    ABTS_STR_EQUAL(tc, "value2", result);
}

static void hash_count_0(abts_case *tc, void *data)
{
    kuda_hash_t *h = NULL;
    int count;

    h = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, h);

    count = kuda_hash_count(h);
    ABTS_INT_EQUAL(tc, 0, count);
}

static void hash_count_1(abts_case *tc, void *data)
{
    kuda_hash_t *h = NULL;
    int count;

    h = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, h);

    kuda_hash_set(h, "key", KUDA_HASH_KEY_STRING, "value");

    count = kuda_hash_count(h);
    ABTS_INT_EQUAL(tc, 1, count);
}

static void hash_count_5(abts_case *tc, void *data)
{
    kuda_hash_t *h = NULL;
    int count;

    h = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, h);

    kuda_hash_set(h, "key1", KUDA_HASH_KEY_STRING, "value1");
    kuda_hash_set(h, "key2", KUDA_HASH_KEY_STRING, "value2");
    kuda_hash_set(h, "key3", KUDA_HASH_KEY_STRING, "value3");
    kuda_hash_set(h, "key4", KUDA_HASH_KEY_STRING, "value4");
    kuda_hash_set(h, "key5", KUDA_HASH_KEY_STRING, "value5");

    count = kuda_hash_count(h);
    ABTS_INT_EQUAL(tc, 5, count);
}

static void overlay_empty(abts_case *tc, void *data)
{
    kuda_hash_t *base = NULL;
    kuda_hash_t *overlay = NULL;
    kuda_hash_t *result = NULL;
    int count;
    char StrArray[MAX_DEPTH][MAX_LTH];

    base = kuda_hash_make(p);
    overlay = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, base);
    ABTS_PTR_NOTNULL(tc, overlay);

    kuda_hash_set(base, "key1", KUDA_HASH_KEY_STRING, "value1");
    kuda_hash_set(base, "key2", KUDA_HASH_KEY_STRING, "value2");
    kuda_hash_set(base, "key3", KUDA_HASH_KEY_STRING, "value3");
    kuda_hash_set(base, "key4", KUDA_HASH_KEY_STRING, "value4");
    kuda_hash_set(base, "key5", KUDA_HASH_KEY_STRING, "value5");

    result = kuda_hash_overlay(p, overlay, base);

    count = kuda_hash_count(result);
    ABTS_INT_EQUAL(tc, 5, count);

    dump_hash(p, result, StrArray);

    ABTS_STR_EQUAL(tc, "Key key1 (4) Value value1\n", StrArray[0]);
    ABTS_STR_EQUAL(tc, "Key key2 (4) Value value2\n", StrArray[1]);
    ABTS_STR_EQUAL(tc, "Key key3 (4) Value value3\n", StrArray[2]);
    ABTS_STR_EQUAL(tc, "Key key4 (4) Value value4\n", StrArray[3]);
    ABTS_STR_EQUAL(tc, "Key key5 (4) Value value5\n", StrArray[4]);
    ABTS_STR_EQUAL(tc, "#entries 5\n", StrArray[5]);
}

static void overlay_2unique(abts_case *tc, void *data)
{
    kuda_hash_t *base = NULL;
    kuda_hash_t *overlay = NULL;
    kuda_hash_t *result = NULL;
    int count;
    char StrArray[MAX_DEPTH][MAX_LTH];

    base = kuda_hash_make(p);
    overlay = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, base);
    ABTS_PTR_NOTNULL(tc, overlay);

    kuda_hash_set(base, "base1", KUDA_HASH_KEY_STRING, "value1");
    kuda_hash_set(base, "base2", KUDA_HASH_KEY_STRING, "value2");
    kuda_hash_set(base, "base3", KUDA_HASH_KEY_STRING, "value3");
    kuda_hash_set(base, "base4", KUDA_HASH_KEY_STRING, "value4");
    kuda_hash_set(base, "base5", KUDA_HASH_KEY_STRING, "value5");

    kuda_hash_set(overlay, "overlay1", KUDA_HASH_KEY_STRING, "value1");
    kuda_hash_set(overlay, "overlay2", KUDA_HASH_KEY_STRING, "value2");
    kuda_hash_set(overlay, "overlay3", KUDA_HASH_KEY_STRING, "value3");
    kuda_hash_set(overlay, "overlay4", KUDA_HASH_KEY_STRING, "value4");
    kuda_hash_set(overlay, "overlay5", KUDA_HASH_KEY_STRING, "value5");

    result = kuda_hash_overlay(p, overlay, base);

    count = kuda_hash_count(result);
    ABTS_INT_EQUAL(tc, 10, count);

    dump_hash(p, result, StrArray);

    ABTS_STR_EQUAL(tc, "Key base1 (5) Value value1\n", StrArray[0]);
    ABTS_STR_EQUAL(tc, "Key base2 (5) Value value2\n", StrArray[1]);
    ABTS_STR_EQUAL(tc, "Key base3 (5) Value value3\n", StrArray[2]);
    ABTS_STR_EQUAL(tc, "Key base4 (5) Value value4\n", StrArray[3]);
    ABTS_STR_EQUAL(tc, "Key base5 (5) Value value5\n", StrArray[4]);
    ABTS_STR_EQUAL(tc, "Key overlay1 (8) Value value1\n", StrArray[5]);
    ABTS_STR_EQUAL(tc, "Key overlay2 (8) Value value2\n", StrArray[6]);
    ABTS_STR_EQUAL(tc, "Key overlay3 (8) Value value3\n", StrArray[7]);
    ABTS_STR_EQUAL(tc, "Key overlay4 (8) Value value4\n", StrArray[8]);
    ABTS_STR_EQUAL(tc, "Key overlay5 (8) Value value5\n", StrArray[9]);
    ABTS_STR_EQUAL(tc, "#entries 10\n", StrArray[10]);
}

static void overlay_same(abts_case *tc, void *data)
{
    kuda_hash_t *base = NULL;
    kuda_hash_t *result = NULL;
    int count;
    char StrArray[MAX_DEPTH][MAX_LTH];

    base = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, base);

    kuda_hash_set(base, "base1", KUDA_HASH_KEY_STRING, "value1");
    kuda_hash_set(base, "base2", KUDA_HASH_KEY_STRING, "value2");
    kuda_hash_set(base, "base3", KUDA_HASH_KEY_STRING, "value3");
    kuda_hash_set(base, "base4", KUDA_HASH_KEY_STRING, "value4");
    kuda_hash_set(base, "base5", KUDA_HASH_KEY_STRING, "value5");

    result = kuda_hash_overlay(p, base, base);

    count = kuda_hash_count(result);
    ABTS_INT_EQUAL(tc, 5, count);

    dump_hash(p, result, StrArray);

    ABTS_STR_EQUAL(tc, "Key base1 (5) Value value1\n", StrArray[0]);
    ABTS_STR_EQUAL(tc, "Key base2 (5) Value value2\n", StrArray[1]);
    ABTS_STR_EQUAL(tc, "Key base3 (5) Value value3\n", StrArray[2]);
    ABTS_STR_EQUAL(tc, "Key base4 (5) Value value4\n", StrArray[3]);
    ABTS_STR_EQUAL(tc, "Key base5 (5) Value value5\n", StrArray[4]);
    ABTS_STR_EQUAL(tc, "#entries 5\n", StrArray[5]);
}

static void overlay_fetch(abts_case *tc, void *data)
{
    kuda_hash_t *base = NULL;
    kuda_hash_t *overlay = NULL;
    kuda_hash_t *result = NULL;
    int count;

    base = kuda_hash_make(p);
    overlay = kuda_hash_make(p);
    ABTS_PTR_NOTNULL(tc, base);
    ABTS_PTR_NOTNULL(tc, overlay);

    kuda_hash_set(base, "base1", KUDA_HASH_KEY_STRING, "value1");
    kuda_hash_set(base, "base2", KUDA_HASH_KEY_STRING, "value2");
    kuda_hash_set(base, "base3", KUDA_HASH_KEY_STRING, "value3");
    kuda_hash_set(base, "base4", KUDA_HASH_KEY_STRING, "value4");
    kuda_hash_set(base, "base5", KUDA_HASH_KEY_STRING, "value5");

    kuda_hash_set(overlay, "overlay1", KUDA_HASH_KEY_STRING, "value1");
    kuda_hash_set(overlay, "overlay2", KUDA_HASH_KEY_STRING, "value2");
    kuda_hash_set(overlay, "overlay3", KUDA_HASH_KEY_STRING, "value3");
    kuda_hash_set(overlay, "overlay4", KUDA_HASH_KEY_STRING, "value4");
    kuda_hash_set(overlay, "overlay5", KUDA_HASH_KEY_STRING, "value5");

    result = kuda_hash_overlay(p, overlay, base);

    count = kuda_hash_count(result);
    ABTS_INT_EQUAL(tc, 10, count);

    ABTS_STR_EQUAL(tc, "value1",
                       kuda_hash_get(result, "base1", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value2",
                       kuda_hash_get(result, "base2", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value3",
                       kuda_hash_get(result, "base3", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value4",
                       kuda_hash_get(result, "base4", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value5",
                       kuda_hash_get(result, "base5", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value1",
                       kuda_hash_get(result, "overlay1", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value2",
                       kuda_hash_get(result, "overlay2", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value3",
                       kuda_hash_get(result, "overlay3", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value4",
                       kuda_hash_get(result, "overlay4", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value5",
                       kuda_hash_get(result, "overlay5", KUDA_HASH_KEY_STRING));

    ABTS_STR_EQUAL(tc, "value1",
                       kuda_hash_get(base, "base1", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value2",
                       kuda_hash_get(base, "base2", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value3",
                       kuda_hash_get(base, "base3", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value4",
                       kuda_hash_get(base, "base4", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value5",
                       kuda_hash_get(base, "base5", KUDA_HASH_KEY_STRING));

    ABTS_STR_EQUAL(tc, "value1",
                       kuda_hash_get(overlay, "overlay1", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value2",
                       kuda_hash_get(overlay, "overlay2", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value3",
                       kuda_hash_get(overlay, "overlay3", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value4",
                       kuda_hash_get(overlay, "overlay4", KUDA_HASH_KEY_STRING));
    ABTS_STR_EQUAL(tc, "value5",
                       kuda_hash_get(overlay, "overlay5", KUDA_HASH_KEY_STRING));
}

abts_suite *testhash(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, hash_make, NULL);
    abts_run_test(suite, hash_set, NULL);
    abts_run_test(suite, hash_reset, NULL);
    abts_run_test(suite, same_value, NULL);
    abts_run_test(suite, same_value_custom, NULL);
    abts_run_test(suite, key_space, NULL);
    abts_run_test(suite, delete_key, NULL);

    abts_run_test(suite, hash_count_0, NULL);
    abts_run_test(suite, hash_count_1, NULL);
    abts_run_test(suite, hash_count_5, NULL);

    abts_run_test(suite, hash_clear, NULL);
    abts_run_test(suite, hash_traverse, NULL);
    abts_run_test(suite, summation_test, NULL);

    abts_run_test(suite, overlay_empty, NULL);
    abts_run_test(suite, overlay_2unique, NULL);
    abts_run_test(suite, overlay_same, NULL);
    abts_run_test(suite, overlay_fetch, NULL);

    return suite;
}

