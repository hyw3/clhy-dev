/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "kuda_strings.h"
#include "kuda_pools.h"
#include "kuda_general.h"
#include "kuda_hash.h"
#include "kuda_lib.h"
#include "kuda_time.h"
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv) {
    kuda_pool_t *context;
    regex_t regex;
    int rc;
    int i;
    int iters;
    kuda_time_t now;
    kuda_time_t end;
    kuda_hash_t *h;
    

    if (argc !=4 ) {
            fprintf(stderr, "Usage %s match string #iterations\n",argv[0]);
            return -1;
    }
    iters = atoi( argv[3]);
    
    kuda_initialize() ;
    atexit(kuda_terminate);
    if (kuda_pool_create(&context, NULL) != KUDA_SUCCESS) {
        fprintf(stderr, "Something went wrong\n");
        exit(-1);
    }
    rc = regcomp( &regex, argv[1], REG_EXTENDED|REG_NOSUB);


    if (rc) {
        char errbuf[2000];
        regerror(rc, &regex,errbuf,2000);
        fprintf(stderr,"Couldn't compile regex ;(\n%s\n ",errbuf);
        return -1;
    }
    if ( regexec( &regex, argv[2], 0, NULL,0) == 0 ) {
        fprintf(stderr,"Match\n");
    }
    else {
        fprintf(stderr,"No Match\n");
    }
    now = kuda_time_now();
    for (i=0;i<iters;i++) {
        regexec( &regex, argv[2], 0, NULL,0) ;
    }
    end=kuda_time_now();
    puts(kuda_psprintf( context, "Time to run %d regex's          %8lld\n",iters,end-now));
    h = kuda_hash_make( context);
    for (i=0;i<70;i++) {
            kuda_hash_set(h,kuda_psprintf(context, "%dkey",i),KUDA_HASH_KEY_STRING,"1");
    }
    now = kuda_time_now();
    for (i=0;i<iters;i++) {
        kuda_hash_get( h, argv[2], KUDA_HASH_KEY_STRING);
    }
    end=kuda_time_now();
    puts(kuda_psprintf( context, "Time to run %d hash (no find)'s %8lld\n",iters,end-now));
    kuda_hash_set(h, argv[2],KUDA_HASH_KEY_STRING,"1");
    now = kuda_time_now();
    for (i=0;i<iters;i++) {
        kuda_hash_get( h, argv[2], KUDA_HASH_KEY_STRING);
    }
    end=kuda_time_now();
    puts(kuda_psprintf( context, "Time to run %d hash (find)'s    %8lld\n",iters,end-now));
 
    return 0;
}
