/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_thread_proc.h"
#include "kuda_file_io.h"
#include "kuda_thread_mutex.h"
#include "kuda_thread_rwlock.h"
#include "kuda_thread_cond.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_getopt.h"
#include "testutil.h"

#if KUDA_HAS_THREADS

#define MAX_ITER 40000
#define MAX_COUNTER 100000
#define MAX_RETRY 5

static void *KUDA_THREAD_FUNC thread_rwlock_func(kuda_thread_t *thd, void *data);
static void *KUDA_THREAD_FUNC thread_mutex_function(kuda_thread_t *thd, void *data);
static void *KUDA_THREAD_FUNC thread_cond_producer(kuda_thread_t *thd, void *data);
static void *KUDA_THREAD_FUNC thread_cond_consumer(kuda_thread_t *thd, void *data);

static kuda_thread_mutex_t *thread_mutex;
static kuda_thread_rwlock_t *rwlock;
static int i = 0, x = 0;

static int buff[MAX_COUNTER];

struct {
    kuda_thread_mutex_t *mutex;
    int                nput;
    int                nval;
} put;

struct {
    kuda_thread_mutex_t *mutex;
    kuda_thread_cond_t  *cond;
    int                nready;
} nready;

static kuda_thread_mutex_t *timeout_mutex;
static kuda_thread_cond_t *timeout_cond;

static void *KUDA_THREAD_FUNC thread_rwlock_func(kuda_thread_t *thd, void *data)
{
    int exitLoop = 1;

    while (1)
    {
        kuda_thread_rwlock_rdlock(rwlock);
        if (i == MAX_ITER)
            exitLoop = 0;
        kuda_thread_rwlock_unlock(rwlock);

        if (!exitLoop)
            break;

        kuda_thread_rwlock_wrlock(rwlock);
        if (i != MAX_ITER)
        {
            i++;
            x++;
        }
        kuda_thread_rwlock_unlock(rwlock);
    }
    return NULL;
} 

static void *KUDA_THREAD_FUNC thread_mutex_function(kuda_thread_t *thd, void *data)
{
    int exitLoop = 1;

    /* slight delay to allow things to settle */
    kuda_sleep (1);
    
    while (1)
    {
        if (data) {
            kuda_thread_mutex_timedlock(thread_mutex, *(kuda_interval_time_t *)data);
        }
        else {
            kuda_thread_mutex_lock(thread_mutex);
        }
        if (i == MAX_ITER)
            exitLoop = 0;
        else 
        {
            i++;
            x++;
        }
        kuda_thread_mutex_unlock(thread_mutex);

        if (!exitLoop)
            break;
    }
    return NULL;
} 

static void *KUDA_THREAD_FUNC thread_cond_producer(kuda_thread_t *thd, void *data)
{
    for (;;) {
        kuda_thread_mutex_lock(put.mutex);
        if (put.nput >= MAX_COUNTER) {
            kuda_thread_mutex_unlock(put.mutex);
            return NULL;
        }
        buff[put.nput] = put.nval;
        put.nput++;
        put.nval++;
        kuda_thread_mutex_unlock(put.mutex);

        kuda_thread_mutex_lock(nready.mutex);
        if (nready.nready == 0)
            kuda_thread_cond_signal(nready.cond);
        nready.nready++;
        kuda_thread_mutex_unlock(nready.mutex);

        *((int *) data) += 1;
    }

    return NULL;
}

static void *KUDA_THREAD_FUNC thread_cond_consumer(kuda_thread_t *thd, void *data)
{
    int i;

    for (i = 0; i < MAX_COUNTER; i++) {
        kuda_thread_mutex_lock(nready.mutex);
        while (nready.nready == 0)
            kuda_thread_cond_wait(nready.cond, nready.mutex);
        nready.nready--;
        kuda_thread_mutex_unlock(nready.mutex);

        if (buff[i] != i)
            printf("buff[%d] = %d\n", i, buff[i]);
    }

    return NULL;
}

static void test_thread_mutex(abts_case *tc, void *data)
{
    kuda_thread_t *t1, *t2, *t3, *t4;
    kuda_status_t s1, s2, s3, s4;

    s1 = kuda_thread_mutex_create(&thread_mutex, KUDA_THREAD_MUTEX_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s1);
    ABTS_PTR_NOTNULL(tc, thread_mutex);

    i = 0;
    x = 0;

    s1 = kuda_thread_create(&t1, NULL, thread_mutex_function, NULL, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s1);
    s2 = kuda_thread_create(&t2, NULL, thread_mutex_function, NULL, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s2);
    s3 = kuda_thread_create(&t3, NULL, thread_mutex_function, NULL, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s3);
    s4 = kuda_thread_create(&t4, NULL, thread_mutex_function, NULL, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s4);

    kuda_thread_join(&s1, t1);
    kuda_thread_join(&s2, t2);
    kuda_thread_join(&s3, t3);
    kuda_thread_join(&s4, t4);

    ABTS_INT_EQUAL(tc, MAX_ITER, x);
}

#if KUDA_HAS_TIMEDLOCKS
static void test_thread_timedmutex(abts_case *tc, void *data)
{
    kuda_thread_t *t1, *t2, *t3, *t4;
    kuda_status_t s1, s2, s3, s4;
    kuda_interval_time_t timeout;

    s1 = kuda_thread_mutex_create(&thread_mutex, KUDA_THREAD_MUTEX_TIMED, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s1);
    ABTS_PTR_NOTNULL(tc, thread_mutex);

    i = 0;
    x = 0;

    timeout = kuda_time_from_sec(5);

    s1 = kuda_thread_create(&t1, NULL, thread_mutex_function, &timeout, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s1);
    s2 = kuda_thread_create(&t2, NULL, thread_mutex_function, &timeout, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s2);
    s3 = kuda_thread_create(&t3, NULL, thread_mutex_function, &timeout, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s3);
    s4 = kuda_thread_create(&t4, NULL, thread_mutex_function, &timeout, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s4);

    kuda_thread_join(&s1, t1);
    kuda_thread_join(&s2, t2);
    kuda_thread_join(&s3, t3);
    kuda_thread_join(&s4, t4);

    ABTS_INT_EQUAL(tc, MAX_ITER, x);
}
#endif

static void test_thread_rwlock(abts_case *tc, void *data)
{
    kuda_thread_t *t1, *t2, *t3, *t4;
    kuda_status_t s1, s2, s3, s4;

    s1 = kuda_thread_rwlock_create(&rwlock, p);
    if (s1 == KUDA_ENOTIMPL) {
        ABTS_NOT_IMPL(tc, "rwlocks not implemented");
        return;
    }
    KUDA_ASSERT_SUCCESS(tc, "rwlock_create", s1);
    ABTS_PTR_NOTNULL(tc, rwlock);

    i = 0;
    x = 0;

    s1 = kuda_thread_create(&t1, NULL, thread_rwlock_func, NULL, p);
    KUDA_ASSERT_SUCCESS(tc, "create thread 1", s1);
    s2 = kuda_thread_create(&t2, NULL, thread_rwlock_func, NULL, p);
    KUDA_ASSERT_SUCCESS(tc, "create thread 2", s2);
    s3 = kuda_thread_create(&t3, NULL, thread_rwlock_func, NULL, p);
    KUDA_ASSERT_SUCCESS(tc, "create thread 3", s3);
    s4 = kuda_thread_create(&t4, NULL, thread_rwlock_func, NULL, p);
    KUDA_ASSERT_SUCCESS(tc, "create thread 4", s4);

    kuda_thread_join(&s1, t1);
    kuda_thread_join(&s2, t2);
    kuda_thread_join(&s3, t3);
    kuda_thread_join(&s4, t4);

    ABTS_INT_EQUAL(tc, MAX_ITER, x);

    kuda_thread_rwlock_destroy(rwlock);
}

static void test_cond(abts_case *tc, void *data)
{
    kuda_thread_t *p1, *p2, *p3, *p4, *c1;
    kuda_status_t s0, s1, s2, s3, s4;
    int count1, count2, count3, count4;
    int sum;
    
    KUDA_ASSERT_SUCCESS(tc, "create put mutex",
                       kuda_thread_mutex_create(&put.mutex, 
                                               KUDA_THREAD_MUTEX_DEFAULT, p));
    ABTS_PTR_NOTNULL(tc, put.mutex);

    KUDA_ASSERT_SUCCESS(tc, "create nready mutex",
                       kuda_thread_mutex_create(&nready.mutex, 
                                               KUDA_THREAD_MUTEX_DEFAULT, p));
    ABTS_PTR_NOTNULL(tc, nready.mutex);

    KUDA_ASSERT_SUCCESS(tc, "create condvar",
                       kuda_thread_cond_create(&nready.cond, p));
    ABTS_PTR_NOTNULL(tc, nready.cond);

    count1 = count2 = count3 = count4 = 0;
    put.nput = put.nval = 0;
    nready.nready = 0;
    i = 0;
    x = 0;

    s0 = kuda_thread_create(&p1, NULL, thread_cond_producer, &count1, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s0);
    s1 = kuda_thread_create(&p2, NULL, thread_cond_producer, &count2, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s1);
    s2 = kuda_thread_create(&p3, NULL, thread_cond_producer, &count3, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s2);
    s3 = kuda_thread_create(&p4, NULL, thread_cond_producer, &count4, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s3);
    s4 = kuda_thread_create(&c1, NULL, thread_cond_consumer, NULL, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s4);

    kuda_thread_join(&s0, p1);
    kuda_thread_join(&s1, p2);
    kuda_thread_join(&s2, p3);
    kuda_thread_join(&s3, p4);
    kuda_thread_join(&s4, c1);

    KUDA_ASSERT_SUCCESS(tc, "destroy condvar", 
                       kuda_thread_cond_destroy(nready.cond));

    sum = count1 + count2 + count3 + count4;
    /*
    printf("count1 = %d count2 = %d count3 = %d count4 = %d\n",
            count1, count2, count3, count4);
    */
    ABTS_INT_EQUAL(tc, MAX_COUNTER, sum);
}

static void test_timeoutcond(abts_case *tc, void *data)
{
    kuda_status_t s;
    kuda_interval_time_t timeout;
    kuda_time_t begin, end;
    int i;

    s = kuda_thread_mutex_create(&timeout_mutex, KUDA_THREAD_MUTEX_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s);
    ABTS_PTR_NOTNULL(tc, timeout_mutex);

    s = kuda_thread_cond_create(&timeout_cond, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s);
    ABTS_PTR_NOTNULL(tc, timeout_cond);

    timeout = kuda_time_from_sec(5);

    for (i = 0; i < MAX_RETRY; i++) {
        kuda_thread_mutex_lock(timeout_mutex);

        begin = kuda_time_now();
        s = kuda_thread_cond_timedwait(timeout_cond, timeout_mutex, timeout);
        end = kuda_time_now();
        kuda_thread_mutex_unlock(timeout_mutex);
        
        if (s != KUDA_SUCCESS && !KUDA_STATUS_IS_TIMEUP(s)) {
            continue;
        }
        ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_TIMEUP(s));
        ABTS_ASSERT(tc, "Timer returned too late", end - begin - timeout < 500000);
        break;
    }
    ABTS_ASSERT(tc, "Too many retries", i < MAX_RETRY);
    KUDA_ASSERT_SUCCESS(tc, "Unable to destroy the conditional",
                       kuda_thread_cond_destroy(timeout_cond));
}

#if KUDA_HAS_TIMEDLOCKS
static void test_timeoutmutex(abts_case *tc, void *data)
{
    kuda_status_t s;
    kuda_interval_time_t timeout;
    kuda_time_t begin, end;
    int i;

    s = kuda_thread_mutex_create(&timeout_mutex, KUDA_THREAD_MUTEX_TIMED, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s);
    ABTS_PTR_NOTNULL(tc, timeout_mutex);

    timeout = kuda_time_from_sec(5);

    ABTS_INT_EQUAL(tc, 0, kuda_thread_mutex_lock(timeout_mutex));
    for (i = 0; i < MAX_RETRY; i++) {
        begin = kuda_time_now();
        s = kuda_thread_mutex_timedlock(timeout_mutex, timeout);
        end = kuda_time_now();

        if (s != KUDA_SUCCESS && !KUDA_STATUS_IS_TIMEUP(s)) {
            continue;
        }
        ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_TIMEUP(s));
        ABTS_ASSERT(tc, "Timer returned too late", end - begin - timeout < 1000000);
        break;
    }
    ABTS_ASSERT(tc, "Too many retries", i < MAX_RETRY);
    ABTS_INT_EQUAL(tc, 0, kuda_thread_mutex_unlock(timeout_mutex));
    KUDA_ASSERT_SUCCESS(tc, "Unable to destroy the mutex",
                       kuda_thread_mutex_destroy(timeout_mutex));
}
#endif

#endif /* !KUDA_HAS_THREADS */

#if !KUDA_HAS_THREADS
static void threads_not_impl(abts_case *tc, void *data)
{
    ABTS_NOT_IMPL(tc, "Threads not implemented on this platform");
}
#endif


abts_suite *testlock(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

#if !KUDA_HAS_THREADS
    abts_run_test(suite, threads_not_impl, NULL);
#else
    abts_run_test(suite, test_thread_mutex, NULL);
#if KUDA_HAS_TIMEDLOCKS
    abts_run_test(suite, test_thread_timedmutex, NULL);
#endif
    abts_run_test(suite, test_thread_rwlock, NULL);
    abts_run_test(suite, test_cond, NULL);
    abts_run_test(suite, test_timeoutcond, NULL);
#if KUDA_HAS_TIMEDLOCKS
    abts_run_test(suite, test_timeoutmutex, NULL);
#endif
#endif

    return suite;
}

