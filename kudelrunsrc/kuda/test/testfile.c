/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_file_io.h"
#include "kuda_file_info.h"
#include "kuda_network_io.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_poll.h"
#include "kuda_lib.h"
#include "testutil.h"

#define DIRNAME "data"
#define FILENAME DIRNAME "/file_datafile.txt"
#define TESTSTR  "This is the file data file."

#define TESTREAD_BLKSIZE 1024
#define KUDA_BUFFERSIZE   4096 /* This should match KUDA's buffer size. */



static void test_open_noreadwrite(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *thefile = NULL;

    rv = kuda_file_open(&thefile, FILENAME,
                       KUDA_FOPEN_CREATE | KUDA_FOPEN_EXCL,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    ABTS_TRUE(tc, rv != KUDA_SUCCESS);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_EACCES(rv));
    ABTS_PTR_EQUAL(tc, NULL, thefile); 
}

static void test_open_excl(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *thefile = NULL;

    rv = kuda_file_open(&thefile, FILENAME,
                       KUDA_FOPEN_CREATE | KUDA_FOPEN_EXCL | KUDA_FOPEN_WRITE,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    ABTS_TRUE(tc, rv != KUDA_SUCCESS);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_EEXIST(rv));
    ABTS_PTR_EQUAL(tc, NULL, thefile); 
}

static void test_open_read(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *filetest = NULL;

    rv = kuda_file_open(&filetest, FILENAME, 
                       KUDA_FOPEN_READ,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, filetest);
    kuda_file_close(filetest);
}

static void link_existing(abts_case *tc, void *data)
{
    kuda_status_t rv;
    
    rv = kuda_file_link("data/file_datafile.txt", "data/file_datafile2.txt");
    kuda_file_remove("data/file_datafile2.txt", p);
    ABTS_ASSERT(tc, "Couldn't create hardlink to file", rv == KUDA_SUCCESS);
}

static void link_nonexisting(abts_case *tc, void *data)
{
    kuda_status_t rv;
    
    rv = kuda_file_link("data/does_not_exist.txt", "data/fake.txt");
    ABTS_ASSERT(tc, "", rv != KUDA_SUCCESS);
}

static void test_read(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_size_t nbytes = 256;
    char *str = kuda_pcalloc(p, nbytes + 1);
    kuda_file_t *filetest = NULL;
    
    rv = kuda_file_open(&filetest, FILENAME, 
                       KUDA_FOPEN_READ,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);

    KUDA_ASSERT_SUCCESS(tc, "Opening test file " FILENAME, rv);
    rv = kuda_file_read(filetest, str, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, strlen(TESTSTR), nbytes);
    ABTS_STR_EQUAL(tc, TESTSTR, str);

    kuda_file_close(filetest);
}

static void test_readzero(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_size_t nbytes = 0;
    char *str = NULL;
    kuda_file_t *filetest;
    
    rv = kuda_file_open(&filetest, FILENAME, KUDA_FOPEN_READ, KUDA_PLATFORM_DEFAULT, p);
    KUDA_ASSERT_SUCCESS(tc, "Opening test file " FILENAME, rv);

    rv = kuda_file_read(filetest, str, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, 0, nbytes);

    kuda_file_close(filetest);
}

static void test_filename(abts_case *tc, void *data)
{
    const char *str;
    kuda_status_t rv;
    kuda_file_t *filetest = NULL;
    
    rv = kuda_file_open(&filetest, FILENAME, 
                       KUDA_FOPEN_READ,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    KUDA_ASSERT_SUCCESS(tc, "Opening test file " FILENAME, rv);

    rv = kuda_file_name_get(&str, filetest);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_STR_EQUAL(tc, FILENAME, str);

    kuda_file_close(filetest);
}
    
static void test_fileclose(abts_case *tc, void *data)
{
    char str;
    kuda_status_t rv;
    kuda_size_t one = 1;
    kuda_file_t *filetest = NULL;
    
    rv = kuda_file_open(&filetest, FILENAME, 
                       KUDA_FOPEN_READ,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    KUDA_ASSERT_SUCCESS(tc, "Opening test file " FILENAME, rv);

    rv = kuda_file_close(filetest);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    /* We just closed the file, so this should fail */
    rv = kuda_file_read(filetest, &str, &one);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_EBADF(rv));
}

static void test_file_remove(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *filetest = NULL;

    rv = kuda_file_remove(FILENAME, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_open(&filetest, FILENAME, KUDA_FOPEN_READ,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_ENOENT(rv));
}

static void test_open_write(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *filetest = NULL;

    filetest = NULL;
    rv = kuda_file_open(&filetest, FILENAME, 
                       KUDA_FOPEN_WRITE,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_ENOENT(rv));
    ABTS_PTR_EQUAL(tc, NULL, filetest);
}

static void test_open_writecreate(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *filetest = NULL;

    filetest = NULL;
    rv = kuda_file_open(&filetest, FILENAME, 
                       KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    kuda_file_close(filetest);
}

static void test_write(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_size_t bytes = strlen(TESTSTR);
    kuda_file_t *filetest = NULL;

    rv = kuda_file_open(&filetest, FILENAME, 
                       KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_write(filetest, TESTSTR, &bytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    kuda_file_close(filetest);
}

static void test_open_readwrite(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *filetest = NULL;

    filetest = NULL;
    rv = kuda_file_open(&filetest, FILENAME, 
                       KUDA_FOPEN_READ | KUDA_FOPEN_WRITE,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, filetest);

    kuda_file_close(filetest);
}

static void test_seek(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_off_t offset = 5;
    kuda_size_t nbytes = 256;
    char *str = kuda_pcalloc(p, nbytes + 1);
    kuda_file_t *filetest = NULL;

    rv = kuda_file_open(&filetest, FILENAME, 
                       KUDA_FOPEN_READ,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    KUDA_ASSERT_SUCCESS(tc, "Open test file " FILENAME, rv);

    rv = kuda_file_read(filetest, str, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, strlen(TESTSTR), nbytes);
    ABTS_STR_EQUAL(tc, TESTSTR, str);

    memset(str, 0, nbytes + 1);

    rv = kuda_file_seek(filetest, SEEK_SET, &offset);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    
    rv = kuda_file_read(filetest, str, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, strlen(TESTSTR) - 5, nbytes);
    ABTS_STR_EQUAL(tc, TESTSTR + 5, str);

    kuda_file_close(filetest);

    /* Test for regression of sign error bug with SEEK_END and
       buffered files. */
    rv = kuda_file_open(&filetest, FILENAME,
                       KUDA_FOPEN_READ | KUDA_FOPEN_BUFFERED,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    KUDA_ASSERT_SUCCESS(tc, "Open test file " FILENAME, rv);

    offset = -5;
    rv = kuda_file_seek(filetest, SEEK_END, &offset);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, strlen(TESTSTR) - 5, nbytes);

    memset(str, 0, nbytes + 1);
    nbytes = 256;
    rv = kuda_file_read(filetest, str, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, 5, nbytes);
    ABTS_STR_EQUAL(tc, TESTSTR + strlen(TESTSTR) - 5, str);

    kuda_file_close(filetest);
}                

static void test_userdata_set(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *filetest = NULL;

    rv = kuda_file_open(&filetest, FILENAME, 
                       KUDA_FOPEN_WRITE,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_data_set(filetest, "This is a test",
                           "test", kuda_pool_cleanup_null);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    kuda_file_close(filetest);
}

static void test_userdata_get(abts_case *tc, void *data)
{
    kuda_status_t rv;
    void *udata;
    char *teststr;
    kuda_file_t *filetest = NULL;

    rv = kuda_file_open(&filetest, FILENAME, 
                       KUDA_FOPEN_WRITE,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_data_set(filetest, "This is a test",
                           "test", kuda_pool_cleanup_null);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_data_get(&udata, "test", filetest);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    teststr = udata;
    ABTS_STR_EQUAL(tc, "This is a test", teststr);

    kuda_file_close(filetest);
}

static void test_userdata_getnokey(abts_case *tc, void *data)
{
    kuda_status_t rv;
    void *teststr;
    kuda_file_t *filetest = NULL;

    rv = kuda_file_open(&filetest, FILENAME, 
                       KUDA_FOPEN_WRITE,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_data_get(&teststr, "nokey", filetest);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_EQUAL(tc, NULL, teststr);
    kuda_file_close(filetest);
}

static void test_buffer_set_get(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_size_t bufsize;
    kuda_file_t *filetest = NULL;
    char   * buffer;

    rv = kuda_file_open(&filetest, FILENAME, 
                       KUDA_FOPEN_WRITE | KUDA_FOPEN_BUFFERED,
                       KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    bufsize = kuda_file_buffer_size_get(filetest);
    ABTS_SIZE_EQUAL(tc, KUDA_BUFFERSIZE, bufsize);
 
    buffer = kuda_pcalloc(p, 10240);
    rv = kuda_file_buffer_set(filetest, buffer, 10240);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    
    bufsize = kuda_file_buffer_size_get(filetest);
    ABTS_SIZE_EQUAL(tc, 10240, bufsize);
    
    rv = kuda_file_buffer_set(filetest, buffer, 12);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    
    bufsize = kuda_file_buffer_size_get(filetest);
    ABTS_SIZE_EQUAL(tc, 12, bufsize);
    
    kuda_file_close(filetest);
}
static void test_getc(abts_case *tc, void *data)
{
    kuda_file_t *f = NULL;
    kuda_status_t rv;
    char ch;

    rv = kuda_file_open(&f, FILENAME, KUDA_FOPEN_READ, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    kuda_file_getc(&ch, f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_INT_EQUAL(tc, (int)TESTSTR[0], (int)ch);
    kuda_file_close(f);
}

static void test_ungetc(abts_case *tc, void *data)
{
    kuda_file_t *f = NULL;
    kuda_status_t rv;
    char ch;

    rv = kuda_file_open(&f, FILENAME, KUDA_FOPEN_READ, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    kuda_file_getc(&ch, f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_INT_EQUAL(tc, (int)TESTSTR[0], (int)ch);

    kuda_file_ungetc('X', f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    kuda_file_getc(&ch, f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_INT_EQUAL(tc, 'X', (int)ch);

    kuda_file_close(f);
}

static void test_gets(abts_case *tc, void *data)
{
    kuda_file_t *f = NULL;
    kuda_status_t rv;
    char *str = kuda_palloc(p, 256);

    rv = kuda_file_open(&f, FILENAME, KUDA_FOPEN_READ, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_gets(str, 256, f);
    /* Only one line in the test file, so KUDA will encounter EOF on the first
     * call to gets, but we should get KUDA_SUCCESS on this call and
     * KUDA_EOF on the next.
     */
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_STR_EQUAL(tc, TESTSTR, str);
    rv = kuda_file_gets(str, 256, f);
    ABTS_INT_EQUAL(tc, KUDA_EOF, rv);
    ABTS_STR_EQUAL(tc, "", str);
    kuda_file_close(f);
}

static void test_gets_buffered(abts_case *tc, void *data)
{
    kuda_file_t *f = NULL;
    kuda_status_t rv;
    char *str = kuda_palloc(p, 256);

    /* This will deadlock gets before the r524355 fix. */
    rv = kuda_file_open(&f, FILENAME, KUDA_FOPEN_READ|KUDA_FOPEN_BUFFERED|KUDA_FOPEN_XTHREAD, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_gets(str, 256, f);
    /* Only one line in the test file, so KUDA will encounter EOF on the first
     * call to gets, but we should get KUDA_SUCCESS on this call and
     * KUDA_EOF on the next.
     */
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_STR_EQUAL(tc, TESTSTR, str);
    rv = kuda_file_gets(str, 256, f);
    ABTS_INT_EQUAL(tc, KUDA_EOF, rv);
    ABTS_STR_EQUAL(tc, "", str);
    kuda_file_close(f);
}

static void test_bigread(abts_case *tc, void *data)
{
    kuda_file_t *f = NULL;
    kuda_status_t rv;
    char buf[KUDA_BUFFERSIZE * 2];
    kuda_size_t nbytes;

    /* Create a test file with known content.
     */
    rv = kuda_file_open(&f, "data/created_file", 
                       KUDA_FOPEN_CREATE | KUDA_FOPEN_WRITE | KUDA_FOPEN_TRUNCATE,
                       KUDA_UREAD | KUDA_UWRITE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    nbytes = KUDA_BUFFERSIZE;
    memset(buf, 0xFE, nbytes);

    rv = kuda_file_write(f, buf, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, KUDA_BUFFERSIZE, nbytes);

    rv = kuda_file_close(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    f = NULL;
    rv = kuda_file_open(&f, "data/created_file", KUDA_FOPEN_READ, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    nbytes = sizeof buf;
    rv = kuda_file_read(f, buf, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, KUDA_BUFFERSIZE, nbytes);

    rv = kuda_file_close(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_remove("data/created_file", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

/* This is a horrible name for this function.  We are testing KUDA, not how
 * cLHy uses KUDA.  And, this function tests _way_ too much stuff.
 */
static void test_capi_neg(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *f;
    const char *s;
    int i;
    kuda_size_t nbytes;
    char buf[8192];
    kuda_off_t cur;
    const char *fname = "data/modneg.dat";

    rv = kuda_file_open(&f, fname, 
                       KUDA_FOPEN_CREATE | KUDA_FOPEN_WRITE, KUDA_UREAD | KUDA_UWRITE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    s = "body56789\n";
    nbytes = strlen(s);
    rv = kuda_file_write(f, s, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, strlen(s), nbytes);
    
    for (i = 0; i < 7980; i++) {
        s = "0";
        nbytes = strlen(s);
        rv = kuda_file_write(f, s, &nbytes);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
        ABTS_SIZE_EQUAL(tc, strlen(s), nbytes);
    }
    
    s = "end456789\n";
    nbytes = strlen(s);
    rv = kuda_file_write(f, s, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, strlen(s), nbytes);

    for (i = 0; i < 10000; i++) {
        s = "1";
        nbytes = strlen(s);
        rv = kuda_file_write(f, s, &nbytes);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
        ABTS_SIZE_EQUAL(tc, strlen(s), nbytes);
    }
    
    rv = kuda_file_close(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_open(&f, fname, KUDA_FOPEN_READ, 0, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_gets(buf, 11, f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_STR_EQUAL(tc, "body56789\n", buf);

    cur = 0;
    rv = kuda_file_seek(f, KUDA_CUR, &cur);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_ASSERT(tc, "File Pointer Mismatch, expected 10", cur == 10);

    nbytes = sizeof(buf);
    rv = kuda_file_read(f, buf, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, nbytes, sizeof(buf));

    cur = -((kuda_off_t)nbytes - 7980);
    rv = kuda_file_seek(f, KUDA_CUR, &cur);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_ASSERT(tc, "File Pointer Mismatch, expected 7990", cur == 7990);

    rv = kuda_file_gets(buf, 11, f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_STR_EQUAL(tc, "end456789\n", buf);

    rv = kuda_file_close(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_remove(fname, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

/* Test that the contents of file FNAME are equal to data EXPECT of
 * length EXPECTLEN. */
static void file_contents_equal(abts_case *tc,
                                const char *fname,
                                const void *expect,
                                kuda_size_t expectlen)
{
    void *actual = kuda_palloc(p, expectlen);
    kuda_file_t *f;

    KUDA_ASSERT_SUCCESS(tc, "open file",
                       kuda_file_open(&f, fname, KUDA_FOPEN_READ|KUDA_FOPEN_BUFFERED,
                                     0, p));
    KUDA_ASSERT_SUCCESS(tc, "read from file",
                       kuda_file_read_full(f, actual, expectlen, NULL));
    
    ABTS_ASSERT(tc, "matched expected file contents",
                memcmp(expect, actual, expectlen) == 0);

    KUDA_ASSERT_SUCCESS(tc, "close file", kuda_file_close(f));
}

#define LINE1 "this is a line of text\n"
#define LINE2 "this is a second line of text\n"

static void test_puts(abts_case *tc, void *data)
{
    kuda_file_t *f;
    const char *fname = "data/testputs.txt";

    KUDA_ASSERT_SUCCESS(tc, "open file for writing",
                       kuda_file_open(&f, fname, 
                                     KUDA_FOPEN_WRITE|KUDA_FOPEN_CREATE|KUDA_FOPEN_TRUNCATE,
                                     KUDA_PLATFORM_DEFAULT, p));
    
    KUDA_ASSERT_SUCCESS(tc, "write line to file", 
                       kuda_file_puts(LINE1, f));
    KUDA_ASSERT_SUCCESS(tc, "write second line to file", 
                       kuda_file_puts(LINE2, f));
    
    KUDA_ASSERT_SUCCESS(tc, "close for writing",
                       kuda_file_close(f));

    file_contents_equal(tc, fname, LINE1 LINE2, strlen(LINE1 LINE2));
}

static void test_writev(abts_case *tc, void *data)
{
    kuda_file_t *f;
    kuda_size_t nbytes;
    struct iovec vec[5];
    const char *fname = "data/testwritev.txt";

    KUDA_ASSERT_SUCCESS(tc, "open file for writing",
                       kuda_file_open(&f, fname, 
                                     KUDA_FOPEN_WRITE|KUDA_FOPEN_CREATE|KUDA_FOPEN_TRUNCATE,
                                     KUDA_PLATFORM_DEFAULT, p));
    
    vec[0].iov_base = LINE1;
    vec[0].iov_len = strlen(LINE1);

    KUDA_ASSERT_SUCCESS(tc, "writev of size 1 to file",
                       kuda_file_writev(f, vec, 1, &nbytes));

    file_contents_equal(tc, fname, LINE1, strlen(LINE1));
    
    vec[0].iov_base = LINE1;
    vec[0].iov_len = strlen(LINE1);
    vec[1].iov_base = LINE2;
    vec[1].iov_len = strlen(LINE2);
    vec[2].iov_base = LINE1;
    vec[2].iov_len = strlen(LINE1);
    vec[3].iov_base = LINE1;
    vec[3].iov_len = strlen(LINE1);
    vec[4].iov_base = LINE2;
    vec[4].iov_len = strlen(LINE2);

    KUDA_ASSERT_SUCCESS(tc, "writev of size 5 to file",
                       kuda_file_writev(f, vec, 5, &nbytes));

    KUDA_ASSERT_SUCCESS(tc, "close for writing",
                       kuda_file_close(f));

    file_contents_equal(tc, fname, LINE1 LINE1 LINE2 LINE1 LINE1 LINE2, 
                        strlen(LINE1)*4 + strlen(LINE2)*2);

}

static void test_writev_full(abts_case *tc, void *data)
{
    kuda_file_t *f;
    kuda_size_t nbytes;
    struct iovec vec[5];
    const char *fname = "data/testwritev_full.txt";

    KUDA_ASSERT_SUCCESS(tc, "open file for writing",
                       kuda_file_open(&f, fname, 
                                     KUDA_FOPEN_WRITE|KUDA_FOPEN_CREATE|KUDA_FOPEN_TRUNCATE,
                                     KUDA_PLATFORM_DEFAULT, p));
    
    vec[0].iov_base = LINE1;
    vec[0].iov_len = strlen(LINE1);
    vec[1].iov_base = LINE2;
    vec[1].iov_len = strlen(LINE2);
    vec[2].iov_base = LINE1;
    vec[2].iov_len = strlen(LINE1);
    vec[3].iov_base = LINE1;
    vec[3].iov_len = strlen(LINE1);
    vec[4].iov_base = LINE2;
    vec[4].iov_len = strlen(LINE2);

    KUDA_ASSERT_SUCCESS(tc, "writev_full of size 5 to file",
                       kuda_file_writev_full(f, vec, 5, &nbytes));

    ABTS_SIZE_EQUAL(tc, strlen(LINE1)*3 + strlen(LINE2)*2, nbytes);

    KUDA_ASSERT_SUCCESS(tc, "close for writing",
                       kuda_file_close(f));

    file_contents_equal(tc, fname, LINE1 LINE2 LINE1 LINE1 LINE2, 
                        strlen(LINE1)*3 + strlen(LINE2)*2);

}

static void test_writev_buffered(abts_case *tc, void *data)
{
    kuda_file_t *f;
    kuda_size_t nbytes;
    struct iovec vec[2];
    const char *fname = "data/testwritev_buffered.dat";

    KUDA_ASSERT_SUCCESS(tc, "open file for writing",
                       kuda_file_open(&f, fname,
                                     KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE | KUDA_FOPEN_TRUNCATE |
                                     KUDA_FOPEN_BUFFERED, KUDA_PLATFORM_DEFAULT, p));

    nbytes = strlen(TESTSTR);
    KUDA_ASSERT_SUCCESS(tc, "buffered write",
                       kuda_file_write(f, TESTSTR, &nbytes));

    vec[0].iov_base = LINE1;
    vec[0].iov_len = strlen(LINE1);
    vec[1].iov_base = LINE2;
    vec[1].iov_len = strlen(LINE2);

    KUDA_ASSERT_SUCCESS(tc, "writev of size 2 to file",
                       kuda_file_writev(f, vec, 2, &nbytes));

    KUDA_ASSERT_SUCCESS(tc, "close for writing",
                       kuda_file_close(f));

    file_contents_equal(tc, fname, TESTSTR LINE1 LINE2,
                        strlen(TESTSTR) + strlen(LINE1) + strlen(LINE2));
}

static void test_writev_buffered_seek(abts_case *tc, void *data)
{
    kuda_file_t *f;
    kuda_status_t rv;
    kuda_off_t off = 0;
    struct iovec vec[3];
    kuda_size_t nbytes = strlen(TESTSTR);
    char *str = kuda_pcalloc(p, nbytes+1);
    const char *fname = "data/testwritev_buffered.dat";

    KUDA_ASSERT_SUCCESS(tc, "open file for writing",
                       kuda_file_open(&f, fname,
                                     KUDA_FOPEN_WRITE | KUDA_FOPEN_READ | KUDA_FOPEN_BUFFERED,
                                     KUDA_PLATFORM_DEFAULT, p));

    rv = kuda_file_read(f, str, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_STR_EQUAL(tc, TESTSTR, str);
    KUDA_ASSERT_SUCCESS(tc, "buffered seek", kuda_file_seek(f, KUDA_SET, &off));

    vec[0].iov_base = LINE1;
    vec[0].iov_len = strlen(LINE1);
    vec[1].iov_base = LINE2;
    vec[1].iov_len = strlen(LINE2);
    vec[2].iov_base = TESTSTR;
    vec[2].iov_len = strlen(TESTSTR);

    KUDA_ASSERT_SUCCESS(tc, "writev of size 2 to file",
                       kuda_file_writev(f, vec, 3, &nbytes));

    KUDA_ASSERT_SUCCESS(tc, "close for writing",
                       kuda_file_close(f));

    file_contents_equal(tc, fname, LINE1 LINE2 TESTSTR,
                        strlen(LINE1) + strlen(LINE2) + strlen(TESTSTR));

    KUDA_ASSERT_SUCCESS(tc, "remove file", kuda_file_remove(fname, p));
}

static void test_truncate(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *f;
    const char *fname = "data/testtruncate.dat";
    const char *s;
    kuda_size_t nbytes;
    kuda_finfo_t finfo;

    kuda_file_remove(fname, p);

    rv = kuda_file_open(&f, fname,
                       KUDA_FOPEN_CREATE | KUDA_FOPEN_WRITE, KUDA_UREAD | KUDA_UWRITE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    
    s = "some data";
    nbytes = strlen(s);
    rv = kuda_file_write(f, s, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, strlen(s), nbytes);

    rv = kuda_file_close(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_open(&f, fname,
                       KUDA_FOPEN_TRUNCATE | KUDA_FOPEN_WRITE, KUDA_UREAD | KUDA_UWRITE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_close(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_stat(&finfo, fname, KUDA_FINFO_SIZE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_ASSERT(tc, "File size mismatch, expected 0 (empty)", finfo.size == 0);

    rv = kuda_file_remove(fname, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

static void test_file_trunc(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *f;
    const char *fname = "data/testtruncate.dat";
    const char *s;
    kuda_size_t nbytes;
    kuda_finfo_t finfo;

    kuda_file_remove(fname, p);

    /* Test unbuffered */
    rv = kuda_file_open(&f, fname,
                        KUDA_FOPEN_CREATE | KUDA_FOPEN_READ |
                        KUDA_FOPEN_WRITE,
                        KUDA_FPROT_UREAD | KUDA_FPROT_UWRITE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    s = "some data";
    nbytes = strlen(s);
    rv = kuda_file_write(f, s, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, strlen(s), nbytes);
    rv = kuda_file_trunc(f, 4);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_file_close(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_stat(&finfo, fname, KUDA_FINFO_SIZE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_ASSERT(tc, "File size mismatch, expected 4", finfo.size == 4);

    rv = kuda_file_remove(fname, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    /* Test buffered */
    rv = kuda_file_open(&f, fname,
                        KUDA_FOPEN_CREATE | KUDA_FOPEN_READ |
                        KUDA_FOPEN_WRITE | KUDA_FOPEN_BUFFERED,
                        KUDA_FPROT_UREAD | KUDA_FPROT_UWRITE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    nbytes = strlen(s);
    rv = kuda_file_write(f, s, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, strlen(s), nbytes);
    rv = kuda_file_trunc(f, 4);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_file_close(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_stat(&finfo, fname, KUDA_FINFO_SIZE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_ASSERT(tc, "File size mismatch, expected 4", finfo.size == 4);

    rv = kuda_file_remove(fname, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

static void test_file_trunc2(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *f;
    const char *fname = "data/testtruncate.dat";
    const char *s;
    kuda_size_t nbytes;
    kuda_finfo_t finfo;
    char c;

    kuda_file_remove(fname, p);

    rv = kuda_file_open(&f, fname,
                        KUDA_FOPEN_CREATE | KUDA_FOPEN_READ |
                        KUDA_FOPEN_WRITE,
                        KUDA_FPROT_UREAD | KUDA_FPROT_UWRITE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    s = "some data";
    nbytes = strlen(s);
    rv = kuda_file_write(f, s, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, strlen(s), nbytes);
    rv = kuda_file_trunc(f, 4);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    /* Test kuda_file_info_get(). */
    rv = kuda_file_info_get(&finfo, KUDA_FINFO_SIZE, f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_INT_EQUAL(tc, 4, (int)finfo.size);
    /* EOF is not reported until the next read. */
    rv = kuda_file_eof(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_file_getc(&c, f);
    ABTS_INT_EQUAL(tc, KUDA_EOF, rv);
    rv = kuda_file_eof(f);
    ABTS_INT_EQUAL(tc, KUDA_EOF, rv);

    rv = kuda_file_close(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_stat(&finfo, fname, KUDA_FINFO_SIZE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_INT_EQUAL(tc, 4, (int)finfo.size);

    rv = kuda_file_remove(fname, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

static void test_file_trunc_buffered_write(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *f;
    const char *fname = "data/testtruncate_buffered_write.dat";
    const char *s;
    kuda_size_t nbytes;
    kuda_finfo_t finfo;
    char c;

    kuda_file_remove(fname, p);

    rv = kuda_file_open(&f, fname,
                        KUDA_FOPEN_CREATE | KUDA_FOPEN_READ |
                        KUDA_FOPEN_WRITE | KUDA_FOPEN_BUFFERED,
                        KUDA_FPROT_UREAD | KUDA_FPROT_UWRITE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    s = "some data";
    nbytes = strlen(s);
    rv = kuda_file_write(f, s, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, strlen(s), nbytes);
    rv = kuda_file_trunc(f, 4);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    /* Test kuda_file_info_get(). */
    rv = kuda_file_info_get(&finfo, KUDA_FINFO_SIZE, f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_INT_EQUAL(tc, 4, (int)finfo.size);
    /* EOF is not reported until the next read. */
    rv = kuda_file_eof(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_file_getc(&c, f);
    ABTS_INT_EQUAL(tc, KUDA_EOF, rv);
    rv = kuda_file_eof(f);
    ABTS_INT_EQUAL(tc, KUDA_EOF, rv);

    rv = kuda_file_close(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_stat(&finfo, fname, KUDA_FINFO_SIZE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_INT_EQUAL(tc, 4, (int)finfo.size);

    rv = kuda_file_remove(fname, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

static void test_file_trunc_buffered_write2(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *f;
    const char *fname = "data/testtruncate_buffered_write2.dat";
    kuda_finfo_t finfo;
    char c;

    kuda_file_remove(fname, p);

    rv = kuda_file_open(&f, fname,
                       KUDA_FOPEN_CREATE | KUDA_FOPEN_READ |
                       KUDA_FOPEN_WRITE | KUDA_FOPEN_BUFFERED,
                       KUDA_FPROT_PLATFORM_DEFAULT, p);
    KUDA_ASSERT_SUCCESS(tc, "open test file", rv);

    rv = kuda_file_puts("abc", f);
    KUDA_ASSERT_SUCCESS(tc, "write first string", rv);
    rv = kuda_file_flush(f);
    KUDA_ASSERT_SUCCESS(tc, "flush", rv);
    rv = kuda_file_puts("def", f);
    KUDA_ASSERT_SUCCESS(tc, "write second string", rv);
    /* Truncate behind the write buffer. */
    rv = kuda_file_trunc(f, 2);
    KUDA_ASSERT_SUCCESS(tc, "truncate the file", rv);
    /* Test kuda_file_info_get(). */
    rv = kuda_file_info_get(&finfo, KUDA_FINFO_SIZE, f);
    KUDA_ASSERT_SUCCESS(tc, "get file info", rv);
    ABTS_INT_EQUAL(tc, 2, (int)finfo.size);
    /* EOF is not reported until the next read. */
    rv = kuda_file_eof(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_file_getc(&c, f);
    ABTS_INT_EQUAL(tc, KUDA_EOF, rv);
    rv = kuda_file_eof(f);
    ABTS_INT_EQUAL(tc, KUDA_EOF, rv);

    kuda_file_close(f);

    rv = kuda_stat(&finfo, fname, KUDA_FINFO_SIZE, p);
    KUDA_ASSERT_SUCCESS(tc, "stat file", rv);
    ABTS_INT_EQUAL(tc, 2, (int)finfo.size);

    kuda_file_remove(fname, p);
}

static void test_file_trunc_buffered_read(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_file_t *f;
    const char *fname = "data/testtruncate_buffered_read.dat";
    kuda_finfo_t finfo;
    char c;

    kuda_file_remove(fname, p);

    rv = kuda_file_open(&f, fname,
                       KUDA_FOPEN_CREATE | KUDA_FOPEN_READ |
                       KUDA_FOPEN_WRITE, KUDA_FPROT_PLATFORM_DEFAULT, p);
    KUDA_ASSERT_SUCCESS(tc, "open test file", rv);

    rv = kuda_file_puts("abc", f);
    KUDA_ASSERT_SUCCESS(tc, "write test data", rv);
    kuda_file_close(f);

    rv = kuda_file_open(&f, fname,
                       KUDA_FOPEN_READ | KUDA_FOPEN_WRITE |
                       KUDA_FOPEN_BUFFERED, KUDA_FPROT_PLATFORM_DEFAULT, p);
    KUDA_ASSERT_SUCCESS(tc, "re-open test file", rv);

    /* Read to fill in the buffer. */
    rv = kuda_file_getc(&c, f);
    KUDA_ASSERT_SUCCESS(tc, "read char", rv);
    /* Truncate the file. */
    rv = kuda_file_trunc(f, 1);
    KUDA_ASSERT_SUCCESS(tc, "truncate the file", rv);
    /* Test kuda_file_info_get(). */
    rv = kuda_file_info_get(&finfo, KUDA_FINFO_SIZE, f);
    KUDA_ASSERT_SUCCESS(tc, "get file info", rv);
    ABTS_INT_EQUAL(tc, 1, (int)finfo.size);
    /* EOF is not reported until the next read. */
    rv = kuda_file_eof(f);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_file_getc(&c, f);
    ABTS_INT_EQUAL(tc, KUDA_EOF, rv);
    rv = kuda_file_eof(f);
    ABTS_INT_EQUAL(tc, KUDA_EOF, rv);

    kuda_file_close(f);

    rv = kuda_stat(&finfo, fname, KUDA_FINFO_SIZE, p);
    KUDA_ASSERT_SUCCESS(tc, "stat file", rv);
    ABTS_INT_EQUAL(tc, 1, (int)finfo.size);

    kuda_file_remove(fname, p);
}

static void test_bigfprintf(abts_case *tc, void *data)
{
    kuda_file_t *f;
    const char *fname = "data/testbigfprintf.dat";
    char *to_write;
    int i;

    kuda_file_remove(fname, p);

    KUDA_ASSERT_SUCCESS(tc, "open test file",
                       kuda_file_open(&f, fname,
                                     KUDA_FOPEN_CREATE|KUDA_FOPEN_WRITE,
                                     KUDA_UREAD|KUDA_UWRITE, p));
    

    to_write = malloc(HUGE_STRING_LEN + 3);

    for (i = 0; i < HUGE_STRING_LEN + 1; ++i)
        to_write[i] = 'A' + i%26;

    strcpy(to_write + HUGE_STRING_LEN, "42");

    i = kuda_file_printf(f, "%s", to_write);
    ABTS_INT_EQUAL(tc, HUGE_STRING_LEN + 2, i);

    kuda_file_close(f);

    file_contents_equal(tc, fname, to_write, HUGE_STRING_LEN + 2);

    free(to_write);
}

static void test_fail_write_flush(abts_case *tc, void *data)
{
    kuda_file_t *f;
    const char *fname = "data/testflush.dat";
    kuda_status_t rv;
    char buf[KUDA_BUFFERSIZE];
    int n;

    kuda_file_remove(fname, p);

    KUDA_ASSERT_SUCCESS(tc, "open test file",
                       kuda_file_open(&f, fname,
                                     KUDA_FOPEN_CREATE|KUDA_FOPEN_READ|KUDA_FOPEN_BUFFERED,
                                     KUDA_UREAD|KUDA_UWRITE, p));

    memset(buf, 'A', sizeof buf);

    /* Try three writes.  One of these should fail when it exceeds the
     * internal buffer and actually tries to write to the file, which
     * was opened read-only and hence should be unwritable. */
    for (n = 0, rv = KUDA_SUCCESS; n < 4 && rv == KUDA_SUCCESS; n++) {
        kuda_size_t bytes = sizeof buf;
        rv = kuda_file_write(f, buf, &bytes);
    }

    ABTS_ASSERT(tc, "failed to write to read-only buffered fd",
                rv != KUDA_SUCCESS);

    kuda_file_close(f);
}

static void test_fail_read_flush(abts_case *tc, void *data)
{
    kuda_file_t *f;
    const char *fname = "data/testflush.dat";
    kuda_status_t rv;
    char buf[2];

    kuda_file_remove(fname, p);

    KUDA_ASSERT_SUCCESS(tc, "open test file",
                       kuda_file_open(&f, fname,
                                     KUDA_FOPEN_CREATE|KUDA_FOPEN_READ|KUDA_FOPEN_BUFFERED,
                                     KUDA_UREAD|KUDA_UWRITE, p));

    /* this write should be buffered. */
    KUDA_ASSERT_SUCCESS(tc, "buffered write should succeed",
                       kuda_file_puts("hello", f));

    /* Now, trying a read should fail since the write must be flushed,
     * and should fail with something other than EOF since the file is
     * opened read-only. */
    rv = kuda_file_read_full(f, buf, 2, NULL);

    ABTS_ASSERT(tc, "read should flush buffered write and fail",
                rv != KUDA_SUCCESS && rv != KUDA_EOF);

    /* Likewise for gets */
    rv = kuda_file_gets(buf, 2, f);

    ABTS_ASSERT(tc, "gets should flush buffered write and fail",
                rv != KUDA_SUCCESS && rv != KUDA_EOF);

    /* Likewise for seek. */
    {
        kuda_off_t offset = 0;

        rv = kuda_file_seek(f, KUDA_SET, &offset);
    }

    ABTS_ASSERT(tc, "seek should flush buffered write and fail",
                rv != KUDA_SUCCESS && rv != KUDA_EOF);

    kuda_file_close(f);
}

static void test_xthread(abts_case *tc, void *data)
{
    kuda_file_t *f;
    const char *fname = "data/testxthread.dat";
    kuda_status_t rv;
    kuda_int32_t flags = KUDA_FOPEN_CREATE|KUDA_FOPEN_READ|KUDA_FOPEN_WRITE|KUDA_FOPEN_APPEND|KUDA_FOPEN_XTHREAD;
    char buf[128] = { 0 };

    /* Test for bug 38438, opening file with append + xthread and seeking to 
       the end of the file resulted in writes going to the beginning not the
       end. */

    kuda_file_remove(fname, p);

    KUDA_ASSERT_SUCCESS(tc, "open test file",
                       kuda_file_open(&f, fname, flags,
                                     KUDA_UREAD|KUDA_UWRITE, p));

    KUDA_ASSERT_SUCCESS(tc, "write should succeed",
                       kuda_file_puts("hello", f));

    kuda_file_close(f);
    
    KUDA_ASSERT_SUCCESS(tc, "open test file",
                       kuda_file_open(&f, fname, flags,
                                     KUDA_UREAD|KUDA_UWRITE, p));

    /* Seek to the end. */
    {
        kuda_off_t offset = 0;

        rv = kuda_file_seek(f, KUDA_END, &offset);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    }

    KUDA_ASSERT_SUCCESS(tc, "more writes should succeed",
                       kuda_file_puts("world", f));

    /* Back to the beginning. */
    {
        kuda_off_t offset = 0;
        
        rv = kuda_file_seek(f, KUDA_SET, &offset);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    }
    
    kuda_file_read_full(f, buf, sizeof(buf), NULL);

    ABTS_STR_EQUAL(tc, "helloworld", buf);

    kuda_file_close(f);
}

abts_suite *testfile(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, test_open_noreadwrite, NULL);
    abts_run_test(suite, test_open_excl, NULL);
    abts_run_test(suite, test_open_read, NULL);
    abts_run_test(suite, test_open_readwrite, NULL);
    abts_run_test(suite, link_existing, NULL);
    abts_run_test(suite, link_nonexisting, NULL);
    abts_run_test(suite, test_read, NULL); 
    abts_run_test(suite, test_readzero, NULL); 
    abts_run_test(suite, test_seek, NULL);
    abts_run_test(suite, test_filename, NULL);
    abts_run_test(suite, test_fileclose, NULL);
    abts_run_test(suite, test_file_remove, NULL);
    abts_run_test(suite, test_open_write, NULL);
    abts_run_test(suite, test_open_writecreate, NULL);
    abts_run_test(suite, test_write, NULL);
    abts_run_test(suite, test_userdata_set, NULL);
    abts_run_test(suite, test_userdata_get, NULL);
    abts_run_test(suite, test_userdata_getnokey, NULL);
    abts_run_test(suite, test_getc, NULL);
    abts_run_test(suite, test_ungetc, NULL);
    abts_run_test(suite, test_gets, NULL);
    abts_run_test(suite, test_gets_buffered, NULL);
    abts_run_test(suite, test_puts, NULL);
    abts_run_test(suite, test_writev, NULL);
    abts_run_test(suite, test_writev_full, NULL);
    abts_run_test(suite, test_writev_buffered, NULL);
    abts_run_test(suite, test_writev_buffered_seek, NULL);
    abts_run_test(suite, test_bigread, NULL);
    abts_run_test(suite, test_capi_neg, NULL);
    abts_run_test(suite, test_truncate, NULL);
    abts_run_test(suite, test_file_trunc, NULL);
    abts_run_test(suite, test_file_trunc2, NULL);
    abts_run_test(suite, test_file_trunc_buffered_write, NULL);
    abts_run_test(suite, test_file_trunc_buffered_write2, NULL);
    abts_run_test(suite, test_file_trunc_buffered_read, NULL);
    abts_run_test(suite, test_bigfprintf, NULL);
    abts_run_test(suite, test_fail_write_flush, NULL);
    abts_run_test(suite, test_fail_read_flush, NULL);
    abts_run_test(suite, test_buffer_set_get, NULL);
    abts_run_test(suite, test_xthread, NULL);

    return suite;
}

