/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_env.h"
#include "kuda_errno.h"
#include "testutil.h"

#define TEST_ENVVAR_NAME "kuda_test_envvar"
#define TEST_ENVVAR2_NAME "kuda_test_envvar2"
#define TEST_ENVVAR_VALUE "Just a value that we'll check"

static int have_env_set;
static int have_env_get;
static int have_env_del;

static void test_setenv(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_env_set(TEST_ENVVAR_NAME, TEST_ENVVAR_VALUE, p);
    have_env_set = (rv != KUDA_ENOTIMPL);
    if (!have_env_set) {
        ABTS_NOT_IMPL(tc, "kuda_env_set");
    } else {
        KUDA_ASSERT_SUCCESS(tc, "set environment variable", rv);
    }
}

static void test_getenv(abts_case *tc, void *data)
{
    char *value;
    kuda_status_t rv;

    if (!have_env_set) {
        ABTS_NOT_IMPL(tc, "kuda_env_set (skip test for kuda_env_get)");
        return;
    }

    rv = kuda_env_get(&value, TEST_ENVVAR_NAME, p);
    have_env_get = (rv != KUDA_ENOTIMPL);
    if (!have_env_get) {
        ABTS_NOT_IMPL(tc, "kuda_env_get");
        return;
    }
    KUDA_ASSERT_SUCCESS(tc, "get environment variable", rv);
    ABTS_STR_EQUAL(tc, TEST_ENVVAR_VALUE, value);
}

static void test_delenv(abts_case *tc, void *data)
{
    char *value;
    kuda_status_t rv;

    if (!have_env_set) {
        ABTS_NOT_IMPL(tc, "kuda_env_set (skip test for kuda_env_delete)");
        return;
    }

    rv = kuda_env_delete(TEST_ENVVAR_NAME, p);
    have_env_del = (rv != KUDA_ENOTIMPL);
    if (!have_env_del) {
        ABTS_NOT_IMPL(tc, "kuda_env_delete");
        return;
    }
    KUDA_ASSERT_SUCCESS(tc, "delete environment variable", rv);

    if (!have_env_get) {
        ABTS_NOT_IMPL(tc, "kuda_env_get (skip sanity check for kuda_env_delete)");
        return;
    }
    rv = kuda_env_get(&value, TEST_ENVVAR_NAME, p);
    ABTS_INT_EQUAL(tc, KUDA_ENOENT, rv);
}

/* Testing of empty environment works on any platforms, not only win32 */
static void test_emptyenv(abts_case *tc, void *data)
{
    char *value;
    kuda_status_t rv;

    if (!(have_env_set && have_env_get)) {
        ABTS_NOT_IMPL(tc, "kuda_env_set (skip test_emptyenv)");
        return;
    }
    /** Set empty string and test that rv != ENOENT) */
    rv = kuda_env_set(TEST_ENVVAR_NAME, "", p);
    KUDA_ASSERT_SUCCESS(tc, "set environment variable", rv);
    rv = kuda_env_get(&value, TEST_ENVVAR_NAME, p);
    KUDA_ASSERT_SUCCESS(tc, "get environment variable", rv);
    ABTS_STR_EQUAL(tc, "", value);

    if (!have_env_del) {
        ABTS_NOT_IMPL(tc, "kuda_env (skip recycle test_emptyenv)");
        return;
    }
    /** Delete and retest */
    rv = kuda_env_delete(TEST_ENVVAR_NAME, p);
    KUDA_ASSERT_SUCCESS(tc, "delete environment variable", rv);
    rv = kuda_env_get(&value, TEST_ENVVAR_NAME, p);
    ABTS_INT_EQUAL(tc, KUDA_ENOENT, rv);

    /** Set second variable + test*/
    rv = kuda_env_set(TEST_ENVVAR2_NAME, TEST_ENVVAR_VALUE, p);
    KUDA_ASSERT_SUCCESS(tc, "set second environment variable", rv);
    rv = kuda_env_get(&value, TEST_ENVVAR2_NAME, p);
    KUDA_ASSERT_SUCCESS(tc, "get second environment variable", rv);
    ABTS_STR_EQUAL(tc, TEST_ENVVAR_VALUE, value);

    /** Finally, test ENOENT (first variable) followed by second != ENOENT) */
    rv = kuda_env_get(&value, TEST_ENVVAR_NAME, p);
    ABTS_INT_EQUAL(tc, KUDA_ENOENT, rv);
    rv = kuda_env_get(&value, TEST_ENVVAR2_NAME, p);
    KUDA_ASSERT_SUCCESS(tc, "verify second environment variable", rv);
    ABTS_STR_EQUAL(tc, TEST_ENVVAR_VALUE, value);

    /** Cleanup */
    kuda_env_delete(TEST_ENVVAR2_NAME, p);
}

abts_suite *testenv(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, test_setenv, NULL);
    abts_run_test(suite, test_getenv, NULL);
    abts_run_test(suite, test_delenv, NULL);
    abts_run_test(suite, test_emptyenv, NULL);

    return suite;
}

