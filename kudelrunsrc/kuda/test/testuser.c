/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testutil.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_user.h"

#if KUDA_HAS_USER
static void uid_current(abts_case *tc, void *data)
{
    kuda_uid_t uid;
    kuda_gid_t gid;

    KUDA_ASSERT_SUCCESS(tc, "kuda_uid_current failed",
                       kuda_uid_current(&uid, &gid, p));
}

static void username(abts_case *tc, void *data)
{
    kuda_uid_t uid;
    kuda_gid_t gid;
    kuda_uid_t retreived_uid;
    kuda_gid_t retreived_gid;
    char *uname = NULL;

    KUDA_ASSERT_SUCCESS(tc, "kuda_uid_current failed",
                       kuda_uid_current(&uid, &gid, p));
   
    KUDA_ASSERT_SUCCESS(tc, "kuda_uid_name_get failed",
                       kuda_uid_name_get(&uname, uid, p));
    ABTS_PTR_NOTNULL(tc, uname);

    if (uname == NULL)
        return;

    KUDA_ASSERT_SUCCESS(tc, "kuda_uid_get failed",
                       kuda_uid_get(&retreived_uid, &retreived_gid, uname, p));

    KUDA_ASSERT_SUCCESS(tc, "kuda_uid_compare failed",
                       kuda_uid_compare(uid, retreived_uid));
#ifdef WIN32
    /* ### this fudge was added for Win32 but makes the test return NotImpl
     * on Unix if run as root, when !gid is also true. */
    if (!gid || !retreived_gid) {
        /* The function had no way to recover the gid (this would have been
         * an ENOTIMPL if kuda_uid_ functions didn't try to double-up and
         * also return kuda_gid_t values, which was bogus.
         */
        if (!gid) {
            ABTS_NOT_IMPL(tc, "Groups from kuda_uid_current");
        }
        else {
            ABTS_NOT_IMPL(tc, "Groups from kuda_uid_get");
        }        
    }
    else {
#endif
        KUDA_ASSERT_SUCCESS(tc, "kuda_gid_compare failed",
                           kuda_gid_compare(gid, retreived_gid));
#ifdef WIN32
    }
#endif
}

static void groupname(abts_case *tc, void *data)
{
    kuda_uid_t uid;
    kuda_gid_t gid;
    kuda_gid_t retreived_gid;
    char *gname = NULL;

    KUDA_ASSERT_SUCCESS(tc, "kuda_uid_current failed",
                       kuda_uid_current(&uid, &gid, p));

    KUDA_ASSERT_SUCCESS(tc, "kuda_gid_name_get failed",
                       kuda_gid_name_get(&gname, gid, p));
    ABTS_PTR_NOTNULL(tc, gname);

    if (gname == NULL)
        return;

    KUDA_ASSERT_SUCCESS(tc, "kuda_gid_get failed",
                       kuda_gid_get(&retreived_gid, gname, p));

    KUDA_ASSERT_SUCCESS(tc, "kuda_gid_compare failed",
                       kuda_gid_compare(gid, retreived_gid));
}

#ifdef KUDA_UID_GID_NUMERIC

static void fail_userinfo(abts_case *tc, void *data)
{
    kuda_uid_t uid;
    kuda_gid_t gid;
    kuda_status_t rv;
    char *tmp;

    errno = 0;
    gid = uid = 9999999;
    tmp = NULL;
    rv = kuda_uid_name_get(&tmp, uid, p);
    ABTS_ASSERT(tc, "kuda_uid_name_get should fail or "
                "return a user name",
                rv != KUDA_SUCCESS || tmp != NULL);

    errno = 0;
    tmp = NULL;
    rv = kuda_gid_name_get(&tmp, gid, p);
    ABTS_ASSERT(tc, "kuda_gid_name_get should fail or "
                "return a group name",
                rv != KUDA_SUCCESS || tmp != NULL);

    gid = 424242;
    errno = 0;
    rv = kuda_gid_get(&gid, "I_AM_NOT_A_GROUP", p);
    ABTS_ASSERT(tc, "kuda_gid_get should fail or "
                "set a group number",
                rv != KUDA_SUCCESS || gid == 424242);

    gid = uid = 424242;
    errno = 0;
    rv = kuda_uid_get(&uid, &gid, "I_AM_NOT_A_USER", p);
    ABTS_ASSERT(tc, "kuda_gid_get should fail or "
                "set a user and group number",
                rv != KUDA_SUCCESS || uid == 424242 || gid == 4242442);

    errno = 0;
    tmp = NULL;
    rv = kuda_uid_homepath_get(&tmp, "I_AM_NOT_A_USER", p);
    ABTS_ASSERT(tc, "kuda_uid_homepath_get should fail or "
                "set a path name",
                rv != KUDA_SUCCESS || tmp != NULL);
}

#endif

#else
static void users_not_impl(abts_case *tc, void *data)
{
    ABTS_NOT_IMPL(tc, "Users not implemented on this platform");
}
#endif

abts_suite *testuser(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

#if !KUDA_HAS_USER
    abts_run_test(suite, users_not_impl, NULL);
#else
    abts_run_test(suite, uid_current, NULL);
    abts_run_test(suite, username, NULL);
    abts_run_test(suite, groupname, NULL);
#ifdef KUDA_UID_GID_NUMERIC
    abts_run_test(suite, fail_userinfo, NULL);
#endif
#endif

    return suite;
}
