/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include "kuda_file_io.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "testutil.h"

static kuda_pool_t *pool;
static char *testdata;
static int cleanup_called = 0;

static kuda_status_t string_cleanup(void *data)
{
    cleanup_called = 1;
    return KUDA_SUCCESS;
}

static void set_userdata(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_pool_userdata_set(testdata, "TEST", string_cleanup, pool);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

static void get_userdata(abts_case *tc, void *data)
{
    kuda_status_t rv;
    void *retdata;

    rv = kuda_pool_userdata_get(&retdata, "TEST", pool);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_STR_EQUAL(tc, testdata, retdata);
}

static void get_nonexistkey(abts_case *tc, void *data)
{
    kuda_status_t rv;
    void *retdata;

    rv = kuda_pool_userdata_get(&retdata, "DOESNTEXIST", pool);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_EQUAL(tc, NULL, retdata);
}

static void post_pool_clear(abts_case *tc, void *data)
{
    kuda_status_t rv;
    void *retdata;

    rv = kuda_pool_userdata_get(&retdata, "DOESNTEXIST", pool);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_EQUAL(tc, NULL, retdata);
}

abts_suite *testud(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    kuda_pool_create(&pool, p);
    testdata = kuda_pstrdup(pool, "This is a test\n");

    abts_run_test(suite, set_userdata, NULL);
    abts_run_test(suite, get_userdata, NULL);
    abts_run_test(suite, get_nonexistkey, NULL);

    kuda_pool_clear(pool);

    abts_run_test(suite, post_pool_clear, NULL);

    return suite;
}

