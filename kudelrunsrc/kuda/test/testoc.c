/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testutil.h"
#include "kuda_thread_proc.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_strings.h"

#if KUDA_HAS_OTHER_CHILD

static char reasonstr[256];

static void ocmaint(int reason, void *data, int status)
{
    switch (reason) {
    case KUDA_OC_REASON_DEATH:
        kuda_cpystrn(reasonstr, "KUDA_OC_REASON_DEATH", 
                    strlen("KUDA_OC_REASON_DEATH") + 1);
        break;
    case KUDA_OC_REASON_LOST:
        kuda_cpystrn(reasonstr, "KUDA_OC_REASON_LOST", 
                    strlen("KUDA_OC_REASON_LOST") + 1);
        break;
    case KUDA_OC_REASON_UNWRITABLE:
        kuda_cpystrn(reasonstr, "KUDA_OC_REASON_UNWRITEABLE", 
                    strlen("KUDA_OC_REASON_UNWRITEABLE") + 1);
        break;
    case KUDA_OC_REASON_RESTART:
        kuda_cpystrn(reasonstr, "KUDA_OC_REASON_RESTART", 
                    strlen("KUDA_OC_REASON_RESTART") + 1);
        break;
    }
}

#ifndef SIGKILL
#define SIGKILL 1
#endif

/* It would be great if we could stress this stuff more, and make the test
 * more granular.
 */
static void test_child_kill(abts_case *tc, void *data)
{
    kuda_file_t *std = NULL;
    kuda_proc_t newproc;
    kuda_procattr_t *procattr = NULL;
    const char *args[3];
    kuda_status_t rv;

    args[0] = kuda_pstrdup(p, "occhild" EXTENSION);
    args[1] = kuda_pstrdup(p, "-X");
    args[2] = NULL;

    rv = kuda_procattr_create(&procattr, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_procattr_io_set(procattr, KUDA_FULL_BLOCK, KUDA_NO_PIPE, 
                             KUDA_NO_PIPE);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_procattr_cmdtype_set(procattr, KUDA_PROGRAM_ENV);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't set copy environment", rv);

    rv = kuda_proc_create(&newproc, TESTBINPATH "occhild" EXTENSION, args, NULL, procattr, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, newproc.in);
    ABTS_PTR_EQUAL(tc, NULL, newproc.out);
    ABTS_PTR_EQUAL(tc, NULL, newproc.err);

    std = newproc.in;

    kuda_proc_other_child_register(&newproc, ocmaint, NULL, std, p);

    kuda_sleep(kuda_time_from_sec(1));
    rv = kuda_proc_kill(&newproc, SIGKILL);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    
    /* allow time for things to settle... */
    kuda_sleep(kuda_time_from_sec(3));
    
    kuda_proc_other_child_refresh_all(KUDA_OC_REASON_RUNNING);
    ABTS_STR_EQUAL(tc, "KUDA_OC_REASON_DEATH", reasonstr);
}    
#else

static void oc_not_impl(abts_case *tc, void *data)
{
    ABTS_NOT_IMPL(tc, "Other child logic not implemented on this platform");
}
#endif

abts_suite *testoc(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

#if !KUDA_HAS_OTHER_CHILD
    abts_run_test(suite, oc_not_impl, NULL);
#else

    abts_run_test(suite, test_child_kill, NULL); 

#endif
    return suite;
}

