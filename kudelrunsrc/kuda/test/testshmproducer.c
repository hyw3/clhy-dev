/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_shm.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_time.h"
#include "testshm.h"
#include "kuda.h"

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif


#if KUDA_HAS_SHARED_MEMORY
static void msgput(int boxnum, char *msg)
{
    kuda_cpystrn(boxes[boxnum].msg, msg, strlen(msg) + 1);
    boxes[boxnum].msgavail = 1;
}

int main(void)
{
    kuda_status_t rv;
    kuda_pool_t *pool;
    kuda_shm_t *shm;
    int i;
    int sent = 0;

    kuda_initialize();
    
    if (kuda_pool_create(&pool, NULL) != KUDA_SUCCESS) {
        exit(-1);
    }

    rv = kuda_shm_attach(&shm, SHARED_FILENAME, pool);
    if (rv != KUDA_SUCCESS) {
        exit(-2);
    }

    boxes = kuda_shm_baseaddr_get(shm);

    /* produce messages on all of the boxes, in descending order,
     * Yes, we could just return N_BOXES, but I want to have a double-check
     * in this code.  The original code actually sent N_BOXES - 1 messages,
     * so rather than rely on possibly buggy code, this way we know that we
     * are returning the right number.
     */
    for (i = N_BOXES - 1, sent = 0; i >= 0; i--, sent++) {
        msgput(i, MSG);
        kuda_sleep(kuda_time_from_sec(1));
    }

    rv = kuda_shm_detach(shm);
    if (rv != KUDA_SUCCESS) {
        exit(-3);
    }

    return sent;
}

#else /* KUDA_HAS_SHARED_MEMORY */

int main(void)
{
    /* Just return, this program will never be launched, so there is no
     * reason to print a message.
     */
    return 0;
}

#endif /* KUDA_HAS_SHARED_MEMORY */

