/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_file_io.h"
#include "kuda_file_info.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_poll.h"
#include "kuda_strings.h"
#include "kuda_lib.h"
#include "kuda_mmap.h"
#include "testutil.h"

/* TODO: in 1.3.0 this becomes KUDA_HAS_SPARSE_FILES, HOWEVER we will
 * still need to test csize before proceeding, because having sparse
 * file support in the PLATFORM/KUDA does not mean this volume supports it!
 */
#if KUDA_HAS_LARGE_FILES

/* Tests which create an 8GB sparse file and then check it can be used
 * as normal. */

static kuda_off_t oneMB = KUDA_INT64_C(2) << 19;
static kuda_off_t eightGB = KUDA_INT64_C(2) << 32;

static int madefile = 0;

#define PRECOND if (!madefile) { ABTS_NOT_IMPL(tc, "Large file tests not enabled"); return; }

#define TESTDIR "lfstests"
#define TESTFILE "large.bin"
#define TESTFN "lfstests/large.bin"

static void test_open(abts_case *tc, void *data)
{
    kuda_file_t *f;
    kuda_finfo_t testsize;
    kuda_status_t rv;

    rv = kuda_dir_make(TESTDIR, KUDA_PLATFORM_DEFAULT, p);
    if (rv && !KUDA_STATUS_IS_EEXIST(rv)) {
        KUDA_ASSERT_SUCCESS(tc, "make test directory", rv);
    }

    /* First attempt a 1MB sparse file so we don't tax the poor test box */
    rv = kuda_file_open(&f, TESTFN, KUDA_FOPEN_CREATE | KUDA_FOPEN_WRITE
                                 | KUDA_FOPEN_TRUNCATE | KUDA_FOPEN_SPARSE,
                       KUDA_PLATFORM_DEFAULT, p);

    KUDA_ASSERT_SUCCESS(tc, "open file", rv);

    KUDA_ASSERT_SUCCESS(tc, "Truncate to 1MB", rv = kuda_file_trunc(f, oneMB+1));

    if (rv == KUDA_SUCCESS) {
        rv = kuda_file_info_get(&testsize, KUDA_FINFO_CSIZE, f);
    }

    /* give up if we can't determine the allocation size of the file,
     * or if it's not an obviously small allocation but the allocation
     * unit doesn't appear insanely large - on most platforms, it's just
     * zero physical bytes at this point.
     */
    if (rv != KUDA_SUCCESS || (testsize.csize > oneMB
                              && testsize.csize < oneMB * 2)) {
        ABTS_NOT_IMPL(tc, "Creation of large file (apparently not sparse)");

        madefile = 0;
    } 
    else {
        /* Proceed with our 8GB sparse file now */
        rv = kuda_file_trunc(f, eightGB);

        /* 8GB may pass rlimits or filesystem limits */

        if (KUDA_STATUS_IS_EINVAL(rv)
#ifdef EFBIG
            || rv == EFBIG
#endif
            ) {
            ABTS_NOT_IMPL(tc, "Creation of large file (rlimit, quota or fs)");
        } 
        else {
            KUDA_ASSERT_SUCCESS(tc, "truncate file to 8gb", rv);
        }
        madefile = rv == KUDA_SUCCESS;
    }

    KUDA_ASSERT_SUCCESS(tc, "close large file", kuda_file_close(f));

    if (!madefile) {
        KUDA_ASSERT_SUCCESS(tc, "remove large file", kuda_file_remove(TESTFN, p));
    }
}

static void test_reopen(abts_case *tc, void *data)
{
    kuda_file_t *fh;
    kuda_finfo_t finfo;
    kuda_status_t rv;

    PRECOND;
    
    rv = kuda_file_open(&fh, TESTFN, KUDA_FOPEN_SPARSE | KUDA_FOPEN_READ,
                       KUDA_PLATFORM_DEFAULT, p);
    KUDA_ASSERT_SUCCESS(tc, "re-open 8GB file", rv);

    KUDA_ASSERT_SUCCESS(tc, "file_info_get failed",
                       kuda_file_info_get(&finfo, KUDA_FINFO_NORM, fh));
    
    ABTS_ASSERT(tc, "file_info_get gave incorrect size",
             finfo.size == eightGB);

    KUDA_ASSERT_SUCCESS(tc, "re-close large file", kuda_file_close(fh));
}

static void test_stat(abts_case *tc, void *data)
{
    kuda_finfo_t finfo;

    PRECOND;

    KUDA_ASSERT_SUCCESS(tc, "stat large file", 
                       kuda_stat(&finfo, TESTFN, KUDA_FINFO_NORM, p));
    
    ABTS_ASSERT(tc, "stat gave incorrect size", finfo.size == eightGB);
}

static void test_readdir(abts_case *tc, void *data)
{
    kuda_dir_t *dh;
    kuda_status_t rv;

    PRECOND;

    KUDA_ASSERT_SUCCESS(tc, "open test directory", 
                       kuda_dir_open(&dh, TESTDIR, p));

    do {
        kuda_finfo_t finfo;
        
        rv = kuda_dir_read(&finfo, KUDA_FINFO_MIN, dh);
        
        if (rv == KUDA_SUCCESS && strcmp(finfo.name, TESTFILE) == 0) {
            ABTS_ASSERT(tc, "kuda_dir_read gave incorrect size for large file", 
                     finfo.size == eightGB);
        }

    } while (rv == KUDA_SUCCESS);
        
    if (!KUDA_STATUS_IS_ENOENT(rv)) {
        KUDA_ASSERT_SUCCESS(tc, "kuda_dir_read failed", rv);
    }
    
    KUDA_ASSERT_SUCCESS(tc, "close test directory",
                       kuda_dir_close(dh));
}

#define TESTSTR "Hello, world."

static void test_append(abts_case *tc, void *data)
{
    kuda_file_t *fh;
    kuda_finfo_t finfo;
    kuda_status_t rv;
    
    PRECOND;

    rv = kuda_file_open(&fh, TESTFN, KUDA_FOPEN_SPARSE | KUDA_FOPEN_WRITE 
                                  | KUDA_FOPEN_APPEND, 
                       KUDA_PLATFORM_DEFAULT, p);
    KUDA_ASSERT_SUCCESS(tc, "open 8GB file for append", rv);

    KUDA_ASSERT_SUCCESS(tc, "append to 8GB file",
                       kuda_file_write_full(fh, TESTSTR, strlen(TESTSTR), NULL));

    KUDA_ASSERT_SUCCESS(tc, "file_info_get failed",
                       kuda_file_info_get(&finfo, KUDA_FINFO_NORM, fh));
    
    ABTS_ASSERT(tc, "file_info_get gave incorrect size",
             finfo.size == eightGB + strlen(TESTSTR));

    KUDA_ASSERT_SUCCESS(tc, "close 8GB file", kuda_file_close(fh));
}

static void test_seek(abts_case *tc, void *data)
{
    kuda_file_t *fh;
    kuda_off_t pos;
    kuda_status_t rv;

    PRECOND;
    
    rv = kuda_file_open(&fh, TESTFN, KUDA_FOPEN_SPARSE | KUDA_FOPEN_WRITE,
                       KUDA_PLATFORM_DEFAULT, p);
    KUDA_ASSERT_SUCCESS(tc, "open 8GB file for writing", rv);

    pos = 0;
    KUDA_ASSERT_SUCCESS(tc, "relative seek to end", 
                       kuda_file_seek(fh, KUDA_END, &pos));
    ABTS_ASSERT(tc, "seek to END gave 8GB", pos == eightGB);
    
    pos = eightGB;
    KUDA_ASSERT_SUCCESS(tc, "seek to 8GB", kuda_file_seek(fh, KUDA_SET, &pos));
    ABTS_ASSERT(tc, "seek gave 8GB offset", pos == eightGB);

    pos = 0;
    KUDA_ASSERT_SUCCESS(tc, "relative seek to 0", kuda_file_seek(fh, KUDA_CUR, &pos));
    ABTS_ASSERT(tc, "relative seek gave 8GB offset", pos == eightGB);

    kuda_file_close(fh);
}

static void test_write(abts_case *tc, void *data)
{
    kuda_file_t *fh;
    kuda_off_t pos = eightGB - 4;
    kuda_status_t rv;

    PRECOND;

    rv = kuda_file_open(&fh, TESTFN, KUDA_FOPEN_SPARSE | KUDA_FOPEN_WRITE,
                       KUDA_PLATFORM_DEFAULT, p);
    KUDA_ASSERT_SUCCESS(tc, "re-open 8GB file", rv);

    KUDA_ASSERT_SUCCESS(tc, "seek to 8GB - 4", 
                       kuda_file_seek(fh, KUDA_SET, &pos));
    ABTS_ASSERT(tc, "seek gave 8GB-4 offset", pos == eightGB - 4);

    KUDA_ASSERT_SUCCESS(tc, "write magic string to 8GB-4",
                       kuda_file_write_full(fh, "FISH", 4, NULL));

    KUDA_ASSERT_SUCCESS(tc, "close 8GB file", kuda_file_close(fh));
}


#if KUDA_HAS_MMAP
static void test_mmap(abts_case *tc, void *data)
{
    kuda_mmap_t *map;
    kuda_file_t *fh;
    kuda_size_t len = 65536; /* hopefully a multiple of the page size */
    kuda_off_t off = eightGB - len; 
    kuda_status_t rv;
    void *ptr;

    PRECOND;

    rv = kuda_file_open(&fh, TESTFN, KUDA_FOPEN_SPARSE | KUDA_FOPEN_READ,
                       KUDA_PLATFORM_DEFAULT, p);
    KUDA_ASSERT_SUCCESS(tc, "open 8gb file for mmap", rv);
    
    KUDA_ASSERT_SUCCESS(tc, "mmap 8GB file",
                       kuda_mmap_create(&map, fh, off, len, KUDA_MMAP_READ, p));

    KUDA_ASSERT_SUCCESS(tc, "close file", kuda_file_close(fh));

    ABTS_ASSERT(tc, "mapped a 64K block", map->size == len);
    
    KUDA_ASSERT_SUCCESS(tc, "get pointer into mmaped region",
                       kuda_mmap_offset(&ptr, map, len - 4));
    ABTS_ASSERT(tc, "pointer was not NULL", ptr != NULL);

    ABTS_ASSERT(tc, "found the magic string", memcmp(ptr, "FISH", 4) == 0);

    KUDA_ASSERT_SUCCESS(tc, "delete mmap handle", kuda_mmap_delete(map));
}
#endif /* KUDA_HAS_MMAP */

static void test_format(abts_case *tc, void *data)
{
    kuda_off_t off;

    PRECOND;

    off = kuda_atoi64(kuda_off_t_toa(p, eightGB));

    ABTS_ASSERT(tc, "kuda_atoi64 parsed kuda_off_t_toa result incorrectly",
             off == eightGB);
}

#define TESTBUFFN TESTDIR "/buffer.bin"

static void test_buffered(abts_case *tc, void *data)
{
    kuda_off_t off;
    kuda_file_t *f;
    kuda_status_t rv;

    PRECOND;

    rv = kuda_file_open(&f, TESTBUFFN, KUDA_FOPEN_CREATE | KUDA_FOPEN_WRITE
                                    | KUDA_FOPEN_TRUNCATE | KUDA_FOPEN_BUFFERED
                                    | KUDA_FOPEN_SPARSE,
                       KUDA_PLATFORM_DEFAULT, p);
    KUDA_ASSERT_SUCCESS(tc, "open buffered file", rv);

    KUDA_ASSERT_SUCCESS(tc, "truncate to 8GB",
                       kuda_file_trunc(f, eightGB));

    off = eightGB;
    KUDA_ASSERT_SUCCESS(tc, "seek to 8GB",
                       kuda_file_seek(f, KUDA_SET, &off));
    ABTS_ASSERT(tc, "returned seek position still 8GB",
                off == eightGB);

    off = 0;
    KUDA_ASSERT_SUCCESS(tc, "relative seek",
                       kuda_file_seek(f, KUDA_CUR, &off));
    ABTS_ASSERT(tc, "relative seek still at 8GB",
                off == eightGB);

    off = 0;
    KUDA_ASSERT_SUCCESS(tc, "end-relative seek",
                       kuda_file_seek(f, KUDA_END, &off));
    ABTS_ASSERT(tc, "end-relative seek still at 8GB",
                off == eightGB);

    off = -eightGB;
    KUDA_ASSERT_SUCCESS(tc, "relative seek to beginning",
                       kuda_file_seek(f, KUDA_CUR, &off));
    ABTS_ASSERT(tc, "seek to beginning got zero",
                off == 0);

    KUDA_ASSERT_SUCCESS(tc, "close buffered file",
                       kuda_file_close(f));
}

#else /* !KUDA_HAS_LARGE_FILES */

static void test_nolfs(abts_case *tc, void *data)
{
    if (sizeof(off_t) < 8) {
        ABTS_NOT_IMPL(tc, "Large Files not supported");
    }
    else {
        ABTS_NOT_IMPL(tc, "LFS support a no-op in 64-bit builds");
    }
}

#endif

abts_suite *testlfs(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

#if KUDA_HAS_LARGE_FILES
    abts_run_test(suite, test_open, NULL);
    abts_run_test(suite, test_reopen, NULL);
    abts_run_test(suite, test_stat, NULL);
    abts_run_test(suite, test_readdir, NULL);
    abts_run_test(suite, test_seek, NULL);
    abts_run_test(suite, test_append, NULL);
    abts_run_test(suite, test_write, NULL);
#if KUDA_HAS_MMAP
    abts_run_test(suite, test_mmap, NULL);
#endif
    abts_run_test(suite, test_format, NULL);
    abts_run_test(suite, test_buffered, NULL);
#else
    abts_run_test(suite, test_nolfs, NULL);
#endif

    return suite;
}

