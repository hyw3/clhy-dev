/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_pools.h"
#include "kuda_general.h"
#include "abts.h"

#ifndef KUDA_TEST_UTIL
#define KUDA_TEST_UTIL

/* XXX: FIXME - these all should become much more utilitarian 
 * and part of kuda, itself
 */

#ifdef WIN32
#ifdef BINPATH
#define TESTBINPATH KUDA_STRINGIFY(BINPATH) "/"
#else
#define TESTBINPATH ""
#endif
#else
#define TESTBINPATH "./"
#endif

#ifdef WIN32
#define EXTENSION ".exe"
#elif NETWARE
#define EXTENSION ".nlm"
#else
#define EXTENSION
#endif

#define STRING_MAX 8096

/* Some simple functions to make the test apps easier to write and
 * a bit more consistent...
 */

extern kuda_pool_t *p;

/* Assert that RV is an KUDA_SUCCESS value; else fail giving strerror
 * for RV and CONTEXT message. */
void kuda_assert_success(abts_case* tc, const char *context, 
                        kuda_status_t rv, int lineno);
#define KUDA_ASSERT_SUCCESS(tc, ctxt, rv) \
             kuda_assert_success(tc, ctxt, rv, __LINE__)

void initialize(void);

abts_suite *testatomic(abts_suite *suite);
abts_suite *testdir(abts_suite *suite);
abts_suite *testdso(abts_suite *suite);
abts_suite *testdup(abts_suite *suite);
abts_suite *testencode(abts_suite *suite);
abts_suite *testenv(abts_suite *suite);
abts_suite *testescape(abts_suite *suite);
abts_suite *testfile(abts_suite *suite);
abts_suite *testfilecopy(abts_suite *suite);
abts_suite *testfileinfo(abts_suite *suite);
abts_suite *testflock(abts_suite *suite);
abts_suite *testfmt(abts_suite *suite);
abts_suite *testfnmatch(abts_suite *suite);
abts_suite *testgetopt(abts_suite *suite);
abts_suite *testglobalmutex(abts_suite *suite);
abts_suite *testhash(abts_suite *suite);
abts_suite *testipsub(abts_suite *suite);
abts_suite *testlock(abts_suite *suite);
abts_suite *testcond(abts_suite *suite);
abts_suite *testlfs(abts_suite *suite);
abts_suite *testmmap(abts_suite *suite);
abts_suite *testnames(abts_suite *suite);
abts_suite *testoc(abts_suite *suite);
abts_suite *testpath(abts_suite *suite);
abts_suite *testpipe(abts_suite *suite);
abts_suite *testpoll(abts_suite *suite);
abts_suite *testpool(abts_suite *suite);
abts_suite *testproc(abts_suite *suite);
abts_suite *testprocmutex(abts_suite *suite);
abts_suite *testrand(abts_suite *suite);
abts_suite *testsleep(abts_suite *suite);
abts_suite *testshm(abts_suite *suite);
abts_suite *testsock(abts_suite *suite);
abts_suite *testsockets(abts_suite *suite);
abts_suite *testsockopt(abts_suite *suite);
abts_suite *teststr(abts_suite *suite);
abts_suite *teststrnatcmp(abts_suite *suite);
abts_suite *testtable(abts_suite *suite);
abts_suite *testtemp(abts_suite *suite);
abts_suite *testthread(abts_suite *suite);
abts_suite *testtime(abts_suite *suite);
abts_suite *testud(abts_suite *suite);
abts_suite *testuser(abts_suite *suite);
abts_suite *testvsn(abts_suite *suite);
abts_suite *testskiplist(abts_suite *suite);

#endif /* KUDA_TEST_INCLUDES */
