/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TESTGLOBALMUTEX_H
#define TESTGLOBALMUTEX_H

/* set this to 255 so that the child processes can return it successfully. */
#define MAX_ITER 255
#define MAX_COUNTER (MAX_ITER * 4)

#define LOCKNAME "data/kuda_globalmutex.lock"

#endif

