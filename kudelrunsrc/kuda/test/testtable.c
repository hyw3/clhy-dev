/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testutil.h"
#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_general.h"
#include "kuda_pools.h"
#include "kuda_tables.h"
#if KUDA_HAVE_STDIO_H
#include <stdio.h>
#endif
#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif
#if KUDA_HAVE_STRING_H
#include <string.h>
#endif

static kuda_array_header_t *a1 = NULL;
static kuda_table_t *t1 = NULL;

static void array_clear(abts_case *tc, void *data)
{
    a1 = kuda_array_make(p, 2, sizeof(const char *));
    KUDA_ARRAY_PUSH(a1, const char *) = "foo";
    KUDA_ARRAY_PUSH(a1, const char *) = "bar";
    kuda_array_clear(a1);
    ABTS_INT_EQUAL(tc, 0, a1->nelts);
}

static void table_make(abts_case *tc, void *data)
{
    t1 = kuda_table_make(p, 5);
    ABTS_PTR_NOTNULL(tc, t1);
}

static void table_get(abts_case *tc, void *data)
{
    const char *val;

    kuda_table_set(t1, "foo", "bar");
    val = kuda_table_get(t1, "foo");
    ABTS_STR_EQUAL(tc, "bar", val);
}

static void table_getm(abts_case *tc, void *data)
{
    const char *orig, *val;
    kuda_pool_t *subp;

    kuda_pool_create(&subp, p);

    orig = "bar";
    kuda_table_setn(t1, "foo", orig);
    val = kuda_table_getm(subp, t1, "foo");
    ABTS_PTR_EQUAL(tc, orig, val);
    ABTS_STR_EQUAL(tc, "bar", val);
    kuda_table_add(t1, "foo", "baz");
    val = kuda_table_getm(subp, t1, "foo");
    ABTS_STR_EQUAL(tc, "bar,baz", val);

    kuda_pool_destroy(subp);
}

static void table_set(abts_case *tc, void *data)
{
    const char *val;

    kuda_table_set(t1, "setkey", "bar");
    kuda_table_set(t1, "setkey", "2ndtry");
    val = kuda_table_get(t1, "setkey");
    ABTS_STR_EQUAL(tc, "2ndtry", val);
}

static void table_getnotthere(abts_case *tc, void *data)
{
    const char *val;

    val = kuda_table_get(t1, "keynotthere");
    ABTS_PTR_EQUAL(tc, NULL, (void *)val);
}

static void table_add(abts_case *tc, void *data)
{
    const char *val;

    kuda_table_add(t1, "addkey", "bar");
    kuda_table_add(t1, "addkey", "foo");
    val = kuda_table_get(t1, "addkey");
    ABTS_STR_EQUAL(tc, "bar", val);

}

static void table_nelts(abts_case *tc, void *data)
{
    const char *val;
    kuda_table_t *t = kuda_table_make(p, 1);

    kuda_table_set(t, "abc", "def");
    kuda_table_set(t, "def", "abc");
    kuda_table_set(t, "foo", "zzz");
    val = kuda_table_get(t, "foo");
    ABTS_STR_EQUAL(tc, "zzz", val);
    val = kuda_table_get(t, "abc");
    ABTS_STR_EQUAL(tc, "def", val);
    val = kuda_table_get(t, "def");
    ABTS_STR_EQUAL(tc, "abc", val);
    ABTS_INT_EQUAL(tc, 3, kuda_table_elts(t)->nelts);
}

static void table_clear(abts_case *tc, void *data)
{
    kuda_table_clear(t1);
    ABTS_INT_EQUAL(tc, 0, kuda_table_elts(t1)->nelts);
}

static void table_unset(abts_case *tc, void *data)
{
    const char *val;
    kuda_table_t *t = kuda_table_make(p, 1);

    kuda_table_set(t, "a", "1");
    kuda_table_set(t, "b", "2");
    kuda_table_unset(t, "b");
    ABTS_INT_EQUAL(tc, 1, kuda_table_elts(t)->nelts);
    val = kuda_table_get(t, "a");
    ABTS_STR_EQUAL(tc, "1", val);
    val = kuda_table_get(t, "b");
    ABTS_PTR_EQUAL(tc, (void *)NULL, (void *)val);
}

static void table_overlap(abts_case *tc, void *data)
{
    const char *val;
    kuda_table_t *t1 = kuda_table_make(p, 1);
    kuda_table_t *t2 = kuda_table_make(p, 1);

    kuda_table_addn(t1, "a", "0");
    kuda_table_addn(t1, "g", "7");
    kuda_table_addn(t2, "a", "1");
    kuda_table_addn(t2, "b", "2");
    kuda_table_addn(t2, "c", "3");
    kuda_table_addn(t2, "b", "2.0");
    kuda_table_addn(t2, "d", "4");
    kuda_table_addn(t2, "e", "5");
    kuda_table_addn(t2, "b", "2.");
    kuda_table_addn(t2, "f", "6");
    kuda_table_overlap(t1, t2, KUDA_OVERLAP_TABLES_SET);
    
    ABTS_INT_EQUAL(tc, 7, kuda_table_elts(t1)->nelts);
    val = kuda_table_get(t1, "a");
    ABTS_STR_EQUAL(tc, "1", val);
    val = kuda_table_get(t1, "b");
    ABTS_STR_EQUAL(tc, "2.", val);
    val = kuda_table_get(t1, "c");
    ABTS_STR_EQUAL(tc, "3", val);
    val = kuda_table_get(t1, "d");
    ABTS_STR_EQUAL(tc, "4", val);
    val = kuda_table_get(t1, "e");
    ABTS_STR_EQUAL(tc, "5", val);
    val = kuda_table_get(t1, "f");
    ABTS_STR_EQUAL(tc, "6", val);
    val = kuda_table_get(t1, "g");
    ABTS_STR_EQUAL(tc, "7", val);
}

static void table_overlap2(abts_case *tc, void *data)
{
    kuda_pool_t *subp;
    kuda_table_t *t1, *t2;

    kuda_pool_create(&subp, p);

    t1 = kuda_table_make(subp, 1);
    t2 = kuda_table_make(p, 1);
    kuda_table_addn(t1, "t1", "one");
    kuda_table_addn(t2, "t2", "two");
    
    kuda_table_overlap(t1, t2, KUDA_OVERLAP_TABLES_SET);
    
    ABTS_INT_EQUAL(tc, 2, kuda_table_elts(t1)->nelts);
    
    ABTS_STR_EQUAL(tc, "one", kuda_table_get(t1, "t1"));
    ABTS_STR_EQUAL(tc, "two", kuda_table_get(t1, "t2"));

}

static void table_overlap3(abts_case *tc, void *data)
{
    kuda_pool_t *subp;
    kuda_table_t *t1, *t2;

    kuda_pool_create(&subp, p);

    t1 = kuda_table_make(subp, 1);
    t2 = kuda_table_make(p, 1);
    kuda_table_addn(t1, "t1", "one");
    kuda_table_addn(t1, "t1", "overlay");
    kuda_table_addn(t2, "t2", "two");
    kuda_table_addn(t2, "t2", "overlay");

    kuda_table_overlap(t1, t2, KUDA_OVERLAP_TABLES_ADD);

    ABTS_INT_EQUAL(tc, 4, kuda_table_elts(t1)->nelts);

    ABTS_STR_EQUAL(tc, "one", kuda_table_get(t1, "t1"));
    ABTS_STR_EQUAL(tc, "two", kuda_table_get(t1, "t2"));

}

abts_suite *testtable(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, array_clear, NULL);
    abts_run_test(suite, table_make, NULL);
    abts_run_test(suite, table_get, NULL);
    abts_run_test(suite, table_getm, NULL);
    abts_run_test(suite, table_set, NULL);
    abts_run_test(suite, table_getnotthere, NULL);
    abts_run_test(suite, table_add, NULL);
    abts_run_test(suite, table_nelts, NULL);
    abts_run_test(suite, table_clear, NULL);
    abts_run_test(suite, table_unset, NULL);
    abts_run_test(suite, table_overlap, NULL);
    abts_run_test(suite, table_overlap2, NULL);
    abts_run_test(suite, table_overlap3, NULL);

    return suite;
}

