/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testutil.h"
#include "kuda_file_io.h"
#include "kuda_strings.h"

static void test_temp_dir(abts_case *tc, void *data)
{
    const char *tempdir = NULL;
    kuda_status_t rv;

    rv = kuda_temp_dir_get(&tempdir, p);
    KUDA_ASSERT_SUCCESS(tc, "Error finding Temporary Directory", rv);
    ABTS_PTR_NOTNULL(tc, tempdir);
}

static void test_mktemp(abts_case *tc, void *data)
{
    kuda_file_t *f = NULL;
    const char *tempdir = NULL;
    char *filetemplate;
    kuda_status_t rv;

    rv = kuda_temp_dir_get(&tempdir, p);
    KUDA_ASSERT_SUCCESS(tc, "Error finding Temporary Directory", rv);
    
    filetemplate = kuda_pstrcat(p, tempdir, "/tempfileXXXXXX", NULL);
    rv = kuda_file_mktemp(&f, filetemplate, 0, p);
    KUDA_ASSERT_SUCCESS(tc, "Error opening Temporary file", rv);
}

abts_suite *testtemp(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, test_temp_dir, NULL);
    abts_run_test(suite, test_mktemp, NULL);

    return suite;
}

