/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testglobalmutex.h"
#include "kuda_thread_proc.h"
#include "kuda_global_mutex.h"
#include "kuda_strings.h"
#include "kuda_errno.h"
#include "testutil.h"

static void launch_child(abts_case *tc, kuda_lockmech_e mech,
                         kuda_proc_t *proc, kuda_pool_t *p)
{
    kuda_procattr_t *procattr;
    const char *args[3];
    kuda_status_t rv;

    rv = kuda_procattr_create(&procattr, p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't create procattr", rv);

    rv = kuda_procattr_io_set(procattr, KUDA_NO_PIPE, KUDA_NO_PIPE,
            KUDA_NO_PIPE);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't set io in procattr", rv);

    rv = kuda_procattr_error_check_set(procattr, 1);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't set error check in procattr", rv);

    args[0] = "globalmutexchild" EXTENSION;
    args[1] = (const char*)kuda_itoa(p, (int)mech);
    args[2] = NULL;
    rv = kuda_proc_create(proc, TESTBINPATH "globalmutexchild" EXTENSION, args, NULL,
            procattr, p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't launch program", rv);
}

static int wait_child(abts_case *tc, kuda_proc_t *proc)
{
    int exitcode;
    kuda_exit_why_e why;

    ABTS_ASSERT(tc, "Error waiting for child process",
            kuda_proc_wait(proc, &exitcode, &why, KUDA_WAIT) == KUDA_CHILD_DONE);

    ABTS_ASSERT(tc, "child didn't terminate normally", why == KUDA_PROC_EXIT);
    return exitcode;
}

/* return symbolic name for a locking meechanism */
static const char *mutexname(kuda_lockmech_e mech)
{
    switch (mech) {
    case KUDA_LOCK_FCNTL: return "fcntl";
    case KUDA_LOCK_FLOCK: return "flock";
    case KUDA_LOCK_SYSVSEM: return "sysvsem";
    case KUDA_LOCK_PROC_PTHREAD: return "proc_pthread";
    case KUDA_LOCK_POSIXSEM: return "posixsem";
    case KUDA_LOCK_DEFAULT: return "default";
    case KUDA_LOCK_DEFAULT_TIMED: return "default_timed";
    default: return "unknown";
    }
}

static void test_exclusive(abts_case *tc, void *data)
{
    kuda_lockmech_e mech = *(kuda_lockmech_e *)data;
    kuda_proc_t p1, p2, p3, p4;
    kuda_status_t rv;
    kuda_global_mutex_t *global_lock;
    int x = 0;
    abts_log_message("lock mechanism is: ");
    abts_log_message(mutexname(mech));
 
    rv = kuda_global_mutex_create(&global_lock, LOCKNAME, mech, p);
    if (rv == KUDA_ENOTIMPL) {
        /* MacOS lacks TIMED implementation, so don't fail for ENOTIMPL */
        ABTS_NOT_IMPL(tc, "global mutex TIMED not implemented");
        return;
    }
    KUDA_ASSERT_SUCCESS(tc, "Error creating mutex", rv);

    launch_child(tc, mech, &p1, p);
    launch_child(tc, mech, &p2, p);
    launch_child(tc, mech, &p3, p);
    launch_child(tc, mech, &p4, p);
 
    x += wait_child(tc, &p1);
    x += wait_child(tc, &p2);
    x += wait_child(tc, &p3);
    x += wait_child(tc, &p4);

    if (x != MAX_COUNTER) {
        char buf[200];
        sprintf(buf, "global mutex '%s' failed: %d not %d",
                mutexname(mech), x, MAX_COUNTER);
        abts_fail(tc, buf, __LINE__);
    }
}

abts_suite *testglobalmutex(abts_suite *suite)
{
    kuda_lockmech_e mech = KUDA_LOCK_DEFAULT;

    suite = ADD_SUITE(suite)
    abts_run_test(suite, test_exclusive, &mech);
#if KUDA_HAS_POSIXSEM_SERIALIZE
    mech = KUDA_LOCK_POSIXSEM;
    abts_run_test(suite, test_exclusive, &mech);
#endif
#if KUDA_HAS_SYSVSEM_SERIALIZE
    mech = KUDA_LOCK_SYSVSEM;
    abts_run_test(suite, test_exclusive, &mech);
#endif
#if KUDA_HAS_PROC_PTHREAD_SERIALIZE
    mech = KUDA_LOCK_PROC_PTHREAD;
    abts_run_test(suite, test_exclusive, &mech);
#endif
#if KUDA_HAS_FCNTL_SERIALIZE
    mech = KUDA_LOCK_FCNTL;
    abts_run_test(suite, test_exclusive, &mech);
#endif
#if KUDA_HAS_FLOCK_SERIALIZE
    mech = KUDA_LOCK_FLOCK;
    abts_run_test(suite, test_exclusive, &mech);
#endif
    mech = KUDA_LOCK_DEFAULT_TIMED;
    abts_run_test(suite, test_exclusive, &mech);

    return suite;
}

