/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_thread_proc.h"
#include "kuda_thread_mutex.h"
#include "kuda_thread_rwlock.h"
#include "kuda_file_io.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_getopt.h"
#include "errno.h"
#include <stdio.h>
#include <stdlib.h>
#include "testutil.h"

#if !KUDA_HAS_THREADS
int main(void)
{
    printf("This program won't work on this platform because there is no "
           "support for threads.\n");
    return 0;
}
#else /* !KUDA_HAS_THREADS */

#define DEFAULT_MAX_COUNTER 1000000
#define MAX_THREADS 6

static int verbose = 0;
static long mutex_counter;
static long max_counter = DEFAULT_MAX_COUNTER;

static kuda_thread_mutex_t *thread_lock;
void * KUDA_THREAD_FUNC thread_mutex_func(kuda_thread_t *thd, void *data);
kuda_status_t test_thread_mutex(int num_threads); /* kuda_thread_mutex_t */

static kuda_thread_rwlock_t *thread_rwlock;
void * KUDA_THREAD_FUNC thread_rwlock_func(kuda_thread_t *thd, void *data);
kuda_status_t test_thread_rwlock(int num_threads); /* kuda_thread_rwlock_t */

int test_thread_mutex_nested(int num_threads);

kuda_pool_t *pool;
int i = 0, x = 0;

void * KUDA_THREAD_FUNC thread_mutex_func(kuda_thread_t *thd, void *data)
{
    int i;

    for (i = 0; i < max_counter; i++) {
        if (data) {
            kuda_thread_mutex_timedlock(thread_lock, *(kuda_interval_time_t *)data);
        }
        else {
            kuda_thread_mutex_lock(thread_lock);
        }
        mutex_counter++;
        kuda_thread_mutex_unlock(thread_lock);
    }
    return NULL;
}

void * KUDA_THREAD_FUNC thread_rwlock_func(kuda_thread_t *thd, void *data)
{
    int i;

    for (i = 0; i < max_counter; i++) {
        kuda_thread_rwlock_wrlock(thread_rwlock);
        mutex_counter++;
        kuda_thread_rwlock_unlock(thread_rwlock);
    }
    return NULL;
}

int test_thread_mutex(int num_threads)
{
    kuda_thread_t *t[MAX_THREADS];
    kuda_status_t s[MAX_THREADS];
    kuda_time_t time_start, time_stop;
    int i;

    mutex_counter = 0;

    printf("kuda_thread_mutex_t Tests\n");
    printf("%-60s", "    Initializing the kuda_thread_mutex_t (UNNESTED)");
    s[0] = kuda_thread_mutex_create(&thread_lock, KUDA_THREAD_MUTEX_UNNESTED, pool);
    if (s[0] != KUDA_SUCCESS) {
        printf("Failed!\n");
        return s[0];
    }
    printf("OK\n");

    kuda_thread_mutex_lock(thread_lock);
    /* set_concurrency(4)? -aaron */
    printf("    Starting %d threads    ", num_threads); 
    for (i = 0; i < num_threads; ++i) {
        s[i] = kuda_thread_create(&t[i], NULL, thread_mutex_func, NULL, pool);
        if (s[i] != KUDA_SUCCESS) {
            printf("Failed!\n");
            return s[i];
        }
    }
    printf("OK\n");

    time_start = kuda_time_now();
    kuda_thread_mutex_unlock(thread_lock);

    /* printf("%-60s", "    Waiting for threads to exit"); */
    for (i = 0; i < num_threads; ++i) {
        kuda_thread_join(&s[i], t[i]);
    }
    /* printf("OK\n"); */

    time_stop = kuda_time_now();
    printf("microseconds: %" KUDA_INT64_T_FMT " usec\n",
           (time_stop - time_start));
    if (mutex_counter != max_counter * num_threads)
        printf("error: counter = %ld\n", mutex_counter);

    return KUDA_SUCCESS;
}

int test_thread_mutex_nested(int num_threads)
{
    kuda_thread_t *t[MAX_THREADS];
    kuda_status_t s[MAX_THREADS];
    kuda_time_t time_start, time_stop;
    int i;

    mutex_counter = 0;

    printf("kuda_thread_mutex_t Tests\n");
    printf("%-60s", "    Initializing the kuda_thread_mutex_t (NESTED)");
    s[0] = kuda_thread_mutex_create(&thread_lock, KUDA_THREAD_MUTEX_NESTED, pool);
    if (s[0] != KUDA_SUCCESS) {
        printf("Failed!\n");
        return s[0];
    }
    printf("OK\n");

    kuda_thread_mutex_lock(thread_lock);
    /* set_concurrency(4)? -aaron */
    printf("    Starting %d threads    ", num_threads); 
    for (i = 0; i < num_threads; ++i) {
        s[i] = kuda_thread_create(&t[i], NULL, thread_mutex_func, NULL, pool);
        if (s[i] != KUDA_SUCCESS) {
            printf("Failed!\n");
            return s[i];
        }
    }
    printf("OK\n");

    time_start = kuda_time_now();
    kuda_thread_mutex_unlock(thread_lock);

    /* printf("%-60s", "    Waiting for threads to exit"); */
    for (i = 0; i < num_threads; ++i) {
        kuda_thread_join(&s[i], t[i]);
    }
    /* printf("OK\n"); */

    time_stop = kuda_time_now();
    printf("microseconds: %" KUDA_INT64_T_FMT " usec\n",
           (time_stop - time_start));
    if (mutex_counter != max_counter * num_threads)
        printf("error: counter = %ld\n", mutex_counter);

    return KUDA_SUCCESS;
}

static int test_thread_mutex_timed(int num_threads)
{
    kuda_thread_t *t[MAX_THREADS];
    kuda_status_t s[MAX_THREADS];
    kuda_time_t time_start, time_stop;
    kuda_time_t timeout;
    int i;

    mutex_counter = 0;

    timeout = kuda_time_from_sec(5);

    printf("kuda_thread_mutex_t Tests\n");
    printf("%-60s", "    Initializing the kuda_thread_mutex_t (TIMED)");
    s[0] = kuda_thread_mutex_create(&thread_lock, KUDA_THREAD_MUTEX_TIMED, pool);
    if (s[0] != KUDA_SUCCESS) {
        printf("Failed!\n");
        return s[0];
    }
    printf("OK\n");

    kuda_thread_mutex_lock(thread_lock);
    /* set_concurrency(4)? -aaron */
    printf("    Starting %d threads    ", num_threads); 
    for (i = 0; i < num_threads; ++i) {
        s[i] = kuda_thread_create(&t[i], NULL, thread_mutex_func, &timeout, pool);
        if (s[i] != KUDA_SUCCESS) {
            printf("Failed!\n");
            return s[i];
        }
    }
    printf("OK\n");

    time_start = kuda_time_now();
    kuda_thread_mutex_unlock(thread_lock);

    /* printf("%-60s", "    Waiting for threads to exit"); */
    for (i = 0; i < num_threads; ++i) {
        kuda_thread_join(&s[i], t[i]);
    }
    /* printf("OK\n"); */

    time_stop = kuda_time_now();
    printf("microseconds: %" KUDA_INT64_T_FMT " usec\n",
           (time_stop - time_start));
    if (mutex_counter != max_counter * num_threads)
        printf("error: counter = %ld\n", mutex_counter);

    return KUDA_SUCCESS;
}

int test_thread_rwlock(int num_threads)
{
    kuda_thread_t *t[MAX_THREADS];
    kuda_status_t s[MAX_THREADS];
    kuda_time_t time_start, time_stop;
    int i;

    mutex_counter = 0;

    printf("kuda_thread_rwlock_t Tests\n");
    printf("%-60s", "    Initializing the kuda_thread_rwlock_t");
    s[0] = kuda_thread_rwlock_create(&thread_rwlock, pool);
    if (s[0] != KUDA_SUCCESS) {
        printf("Failed!\n");
        return s[0];
    }
    printf("OK\n");

    kuda_thread_rwlock_wrlock(thread_rwlock);
    /* set_concurrency(4)? -aaron */
    printf("    Starting %d threads    ", num_threads); 
    for (i = 0; i < num_threads; ++i) {
        s[i] = kuda_thread_create(&t[i], NULL, thread_rwlock_func, NULL, pool);
        if (s[i] != KUDA_SUCCESS) {
            printf("Failed!\n");
            return s[i];
        }
    }
    printf("OK\n");

    time_start = kuda_time_now();
    kuda_thread_rwlock_unlock(thread_rwlock);

    /* printf("%-60s", "    Waiting for threads to exit"); */
    for (i = 0; i < num_threads; ++i) {
        kuda_thread_join(&s[i], t[i]);
    }
    /* printf("OK\n"); */

    time_stop = kuda_time_now();
    printf("microseconds: %" KUDA_INT64_T_FMT " usec\n",
           (time_stop - time_start));
    if (mutex_counter != max_counter * num_threads)
        printf("error: counter = %ld\n", mutex_counter);

    return KUDA_SUCCESS;
}

int main(int argc, const char * const *argv)
{
    kuda_status_t rv;
    char errmsg[200];
    kuda_getopt_t *opt;
    char optchar;
    const char *optarg;

    printf("KUDA Lock Performance Test\n==============\n\n");
        
    kuda_initialize();
    atexit(kuda_terminate);

    if (kuda_pool_create(&pool, NULL) != KUDA_SUCCESS)
        exit(-1);

    if ((rv = kuda_getopt_init(&opt, pool, argc, argv)) != KUDA_SUCCESS) {
        fprintf(stderr, "Could not set up to parse options: [%d] %s\n",
                rv, kuda_strerror(rv, errmsg, sizeof errmsg));
        exit(-1);
    }
        
    while ((rv = kuda_getopt(opt, "c:v", &optchar, &optarg)) == KUDA_SUCCESS) {
        if (optchar == 'c') {
            max_counter = atol(optarg);
        }
        else if (optchar == 'v') {
            verbose = 1;
        }
    }

    if (rv != KUDA_SUCCESS && rv != KUDA_EOF) {
        fprintf(stderr, "Could not parse options: [%d] %s\n",
                rv, kuda_strerror(rv, errmsg, sizeof errmsg));
        exit(-1);
    }

    for (i = 1; i <= MAX_THREADS; ++i) {
        if ((rv = test_thread_mutex(i)) != KUDA_SUCCESS) {
            fprintf(stderr,"thread_mutex test failed : [%d] %s\n",
                    rv, kuda_strerror(rv, (char*)errmsg, 200));
            exit(-3);
        }

        if ((rv = test_thread_mutex_nested(i)) != KUDA_SUCCESS) {
            fprintf(stderr,"thread_mutex (NESTED) test failed : [%d] %s\n",
                    rv, kuda_strerror(rv, (char*)errmsg, 200));
            exit(-4);
        }

        if ((rv = test_thread_mutex_timed(i)) != KUDA_SUCCESS) {
            fprintf(stderr,"thread_mutex (TIMED) test failed : [%d] %s\n",
                    rv, kuda_strerror(rv, (char*)errmsg, 200));
            exit(-5);
        }

        if ((rv = test_thread_rwlock(i)) != KUDA_SUCCESS) {
            fprintf(stderr,"thread_rwlock test failed : [%d] %s\n",
                    rv, kuda_strerror(rv, (char*)errmsg, 200));
            exit(-6);
        }
    }

    return 0;
}

#endif /* !KUDA_HAS_THREADS */
