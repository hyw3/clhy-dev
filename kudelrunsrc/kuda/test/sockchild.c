/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include "testsock.h"
#include "kuda_network_io.h"
#include "kuda_pools.h"

int main(int argc, char *argv[])
{
    kuda_pool_t *p;
    kuda_socket_t *sock;
    kuda_status_t rv;
    kuda_sockaddr_t *remote_sa;

    kuda_initialize();
    atexit(kuda_terminate);
    kuda_pool_create(&p, NULL);

    if (argc < 3) {
        exit(-1);
    }

    rv = kuda_sockaddr_info_get(&remote_sa, argv[2], KUDA_UNSPEC, 8021, 0, p);
    if (rv != KUDA_SUCCESS) {
        exit(-1);
    }

    if (kuda_socket_create(&sock, remote_sa->family, SOCK_STREAM, 0,
                p) != KUDA_SUCCESS) {
        exit(-1);
    }

    rv = kuda_socket_timeout_set(sock, kuda_time_from_sec(3));
    if (rv) {
        exit(-1);
    }

    kuda_socket_connect(sock, remote_sa);
        
    if (!strcmp("read", argv[1])) {
        char datarecv[STRLEN];
        kuda_size_t length = STRLEN;
        kuda_status_t rv;

        memset(datarecv, 0, STRLEN);
        rv = kuda_socket_recv(sock, datarecv, &length);
        kuda_socket_close(sock);
        if (KUDA_STATUS_IS_TIMEUP(rv)) {
            exit(SOCKET_TIMEOUT); 
        }

        if (strcmp(datarecv, DATASTR)) {
            exit(-1);
        }
        
        exit((int)length);
    }
    else if (!strcmp("write", argv[1])
             || !strcmp("write_after_delay", argv[1])) {
        kuda_size_t length = strlen(DATASTR);

        if (!strcmp("write_after_delay", argv[1])) {
            kuda_sleep(kuda_time_from_sec(2));
        }

        kuda_socket_send(sock, DATASTR, &length);

        kuda_socket_close(sock);
        exit((int)length);
    }
    else if (!strcmp("close", argv[1])) {
        kuda_socket_close(sock);
        exit(0);
    }
    exit(-1);
}
