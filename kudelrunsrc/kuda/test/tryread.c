/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testflock.h"
#include "kuda_pools.h"
#include "kuda_file_io.h"
#include "kuda_general.h"
#include "kuda.h"

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif

int main(int argc, const char * const *argv)
{
    kuda_file_t *file;
    kuda_status_t status;
    kuda_pool_t *p;

    kuda_initialize();
    kuda_pool_create(&p, NULL);

    if (kuda_file_open(&file, TESTFILE, KUDA_FOPEN_WRITE, KUDA_PLATFORM_DEFAULT, p)
        != KUDA_SUCCESS) {
        
        exit(UNEXPECTED_ERROR);
    }
    status = kuda_file_lock(file, KUDA_FLOCK_EXCLUSIVE | KUDA_FLOCK_NONBLOCK);
    if (status == KUDA_SUCCESS) {
        exit(SUCCESSFUL_READ);
    }
    if (KUDA_STATUS_IS_EAGAIN(status)) {
        exit(FAILED_READ);
    }
    exit(UNEXPECTED_ERROR);
}
