/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_TEST_INCLUDES
#define KUDA_TEST_INCLUDES

#include "abts.h"
#include "testutil.h"

const struct testlist {
    abts_suite *(*func)(abts_suite *suite);
} alltests[] = {
    {testatomic},
    {testdir},
    {testdso},
    {testdup},
    {testencode},
    {testenv},
    {testescape},
    {testfile},
    {testfilecopy},
    {testfileinfo},
    {testflock},
    {testfmt},
    {testfnmatch},
    {testgetopt},
#if 0 /* not ready yet due to API issues */
    {testglobalmutex},
#endif
    {testhash},
    {testipsub},
    {testlock},
    {testcond},
    {testlfs},
    {testmmap},
    {testnames},
    {testoc},
    {testpath},
    {testpipe},
    {testpoll},
    {testpool},
    {testproc},
    {testprocmutex},
    {testrand},
    {testsleep},
    {testshm},
    {testsock},
    {testsockets},
    {testsockopt},
    {teststr},
    {teststrnatcmp},
    {testtable},
    {testtemp},
    {testthread},
    {testtime},
    {testud},
    {testuser},
    {testvsn},
    {testskiplist}
};

#endif /* KUDA_TEST_INCLUDES */
