/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testutil.h"
#include "kuda_file_io.h"
#include "kuda_file_info.h"
#include "kuda_errno.h"
#include "kuda_pools.h"

static void copy_helper(abts_case *tc, const char *from, const char * to,
                        kuda_fileperms_t perms, int append, kuda_pool_t *p)
{
    kuda_status_t rv;
    kuda_status_t dest_rv;
    kuda_finfo_t copy;
    kuda_finfo_t orig;
    kuda_finfo_t dest;
    
    dest_rv = kuda_stat(&dest, to, KUDA_FINFO_SIZE, p);
    
    if (!append) {
        rv = kuda_file_copy(from, to, perms, p);
    }
    else {
        rv = kuda_file_append(from, to, perms, p);
    }
    KUDA_ASSERT_SUCCESS(tc, "Error copying file", rv);

    rv = kuda_stat(&orig, from, KUDA_FINFO_SIZE, p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't stat original file", rv);

    rv = kuda_stat(&copy, to, KUDA_FINFO_SIZE, p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't stat copy file", rv);

    if (!append) {
        ABTS_ASSERT(tc, "File size differs", orig.size == copy.size);
    }
    else {
        ABTS_ASSERT(tc, "File size differs", 
			            ((dest_rv == KUDA_SUCCESS) 
			              ? dest.size : 0) + orig.size == copy.size);
    }
}

static void copy_short_file(abts_case *tc, void *data)
{
    kuda_status_t rv;

    /* make absolutely sure that the dest file doesn't exist. */
    kuda_file_remove("data/file_copy.txt", p);
    
    copy_helper(tc, "data/file_datafile.txt", "data/file_copy.txt", 
                KUDA_FILE_SOURCE_PERMS, 0, p);
    rv = kuda_file_remove("data/file_copy.txt", p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't remove copy file", rv);
}

static void copy_over_existing(abts_case *tc, void *data)
{
    kuda_status_t rv;
    
    /* make absolutely sure that the dest file doesn't exist. */
    kuda_file_remove("data/file_copy.txt", p);
    
    /* This is a cheat.  I don't want to create a new file, so I just copy
     * one file, then I copy another.  If the second copy succeeds, then
     * this works.
     */
    copy_helper(tc, "data/file_datafile.txt", "data/file_copy.txt", 
                KUDA_FILE_SOURCE_PERMS, 0, p);
    
    copy_helper(tc, "data/mmap_datafile.txt", "data/file_copy.txt", 
                KUDA_FILE_SOURCE_PERMS, 0, p);
  
    rv = kuda_file_remove("data/file_copy.txt", p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't remove copy file", rv);
}

static void append_nonexist(abts_case *tc, void *data)
{
    kuda_status_t rv;

    /* make absolutely sure that the dest file doesn't exist. */
    kuda_file_remove("data/file_copy.txt", p);

    copy_helper(tc, "data/file_datafile.txt", "data/file_copy.txt", 
                KUDA_FILE_SOURCE_PERMS, 0, p);
    rv = kuda_file_remove("data/file_copy.txt", p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't remove copy file", rv);
}

static void append_exist(abts_case *tc, void *data)
{
    kuda_status_t rv;
    
    /* make absolutely sure that the dest file doesn't exist. */
    kuda_file_remove("data/file_copy.txt", p);
    
    /* This is a cheat.  I don't want to create a new file, so I just copy
     * one file, then I copy another.  If the second copy succeeds, then
     * this works.
     */
    copy_helper(tc, "data/file_datafile.txt", "data/file_copy.txt", 
                KUDA_FILE_SOURCE_PERMS, 0, p);
    
    copy_helper(tc, "data/mmap_datafile.txt", "data/file_copy.txt", 
                KUDA_FILE_SOURCE_PERMS, 1, p);
  
    rv = kuda_file_remove("data/file_copy.txt", p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't remove copy file", rv);
}

abts_suite *testfilecopy(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, copy_short_file, NULL);
    abts_run_test(suite, copy_over_existing, NULL);

    abts_run_test(suite, append_nonexist, NULL);
    abts_run_test(suite, append_exist, NULL);

    return suite;
}

