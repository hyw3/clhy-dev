/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "kuda_file_io.h"
#include "kuda_file_info.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_thread_proc.h"
#include "testutil.h"

static void test_mkdir(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_finfo_t finfo;

    rv = kuda_dir_make("data/testdir", KUDA_UREAD | KUDA_UWRITE | KUDA_UEXECUTE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_stat(&finfo, "data/testdir", KUDA_FINFO_TYPE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_INT_EQUAL(tc, KUDA_DIR, finfo.filetype);
}

static void test_mkdir_recurs(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_finfo_t finfo;

    rv = kuda_dir_make_recursive("data/one/two/three", 
                                KUDA_UREAD | KUDA_UWRITE | KUDA_UEXECUTE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_stat(&finfo, "data/one", KUDA_FINFO_TYPE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_INT_EQUAL(tc, KUDA_DIR, finfo.filetype);

    rv = kuda_stat(&finfo, "data/one/two", KUDA_FINFO_TYPE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_INT_EQUAL(tc, KUDA_DIR, finfo.filetype);

    rv = kuda_stat(&finfo, "data/one/two/three", KUDA_FINFO_TYPE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_INT_EQUAL(tc, KUDA_DIR, finfo.filetype);
}

struct thread_data
{
    abts_case *tc;
    kuda_pool_t *pool;
};

static void *KUDA_THREAD_FUNC thread_mkdir_func(kuda_thread_t *thd, void *data)
{
    struct thread_data *td = data;
    kuda_status_t s1, s2, s3, s4, s5;

    s1 = kuda_dir_make_recursive("data/prll/one/thwo/three",
                                KUDA_FPROT_UREAD | KUDA_FPROT_UWRITE | KUDA_FPROT_UEXECUTE,
                                td->pool);
    s2 = kuda_dir_make_recursive("data/prll/four/five/six/seven/eight",
                                KUDA_FPROT_UREAD | KUDA_FPROT_UWRITE | KUDA_FPROT_UEXECUTE,
                                td->pool);
    s3 = kuda_dir_make_recursive("data/prll/nine/ten",
                                KUDA_FPROT_UREAD | KUDA_FPROT_UWRITE | KUDA_FPROT_UEXECUTE,
                                td->pool);
    s4 = kuda_dir_make_recursive("data/prll/11/12/13/14/15/16/17/18/19/20",
                                KUDA_FPROT_UREAD | KUDA_FPROT_UWRITE | KUDA_FPROT_UEXECUTE,
                                td->pool);
    s5 = kuda_dir_make_recursive("data/fortytwo",
                                KUDA_FPROT_UREAD | KUDA_FPROT_UWRITE | KUDA_FPROT_UEXECUTE,
                                td->pool);

    ABTS_INT_EQUAL(td->tc, KUDA_SUCCESS, s1);
    ABTS_INT_EQUAL(td->tc, KUDA_SUCCESS, s2);
    ABTS_INT_EQUAL(td->tc, KUDA_SUCCESS, s3);
    ABTS_INT_EQUAL(td->tc, KUDA_SUCCESS, s4);
    ABTS_INT_EQUAL(td->tc, KUDA_SUCCESS, s5);
    return NULL;
}

static void test_mkdir_recurs_parallel(abts_case *tc, void *data)
{
    struct thread_data td1, td2, td3, td4;
    kuda_thread_t *t1, *t2, *t3, *t4;
    kuda_status_t s1, s2, s3, s4;

    td1.tc = td2.tc = td3.tc = td4.tc = tc;
    kuda_pool_create(&td1.pool, p);
    kuda_pool_create(&td2.pool, p);
    kuda_pool_create(&td3.pool, p);
    kuda_pool_create(&td4.pool, p);

    s1 = kuda_thread_create(&t1, NULL, thread_mkdir_func, &td1, td1.pool);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s1);
    s2 = kuda_thread_create(&t2, NULL, thread_mkdir_func, &td2, td2.pool);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s2);
    s3 = kuda_thread_create(&t3, NULL, thread_mkdir_func, &td3, td3.pool);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s3);
    s4 = kuda_thread_create(&t4, NULL, thread_mkdir_func, &td4, td4.pool);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s4);

    kuda_thread_join(&s1, t1);
    kuda_thread_join(&s2, t2);
    kuda_thread_join(&s3, t3);
    kuda_thread_join(&s4, t4);

    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s1);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s2);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s3);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, s4);
}

static void test_remove(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_finfo_t finfo;

    rv = kuda_dir_remove("data/testdir", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_stat(&finfo, "data/testdir", KUDA_FINFO_TYPE, p);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_ENOENT(rv));
}

static void test_removeall_fail(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_dir_remove("data/one", p);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_ENOTEMPTY(rv));
}

static void test_removeall(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_dir_remove("data/one/two/three", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/one/two", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/one", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/one/thwo/three", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/one/thwo", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/one", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/four/five/six/seven/eight", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/four/five/six/seven", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/four/five/six", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/four/five", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/four", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/nine/ten", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/nine", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/11/12/13/14/15/16/17/18/19/20", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/11/12/13/14/15/16/17/18/19", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/11/12/13/14/15/16/17/18", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/11/12/13/14/15/16/17", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/11/12/13/14/15/16", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/11/12/13/14/15", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/11/12/13/14", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/11/12/13", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/11/12", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll/11", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/prll", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_remove("data/fortytwo", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

static void test_remove_notthere(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_dir_remove("data/notthere", p);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_ENOENT(rv));
}

static void test_mkdir_twice(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_dir_make("data/testdir", KUDA_UREAD | KUDA_UWRITE | KUDA_UEXECUTE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_dir_make("data/testdir", KUDA_UREAD | KUDA_UWRITE | KUDA_UEXECUTE, p);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_EEXIST(rv));

    rv = kuda_dir_remove("data/testdir", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

static void test_opendir(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_dir_t *dir;

    rv = kuda_dir_open(&dir, "data", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    kuda_dir_close(dir);
}

static void test_opendir_notthere(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_dir_t *dir;

    rv = kuda_dir_open(&dir, "notthere", p);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_ENOENT(rv));
}

static void test_closedir(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_dir_t *dir;

    rv = kuda_dir_open(&dir, "data", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_dir_close(dir);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

static void test_rewind(abts_case *tc, void *data)
{
    kuda_dir_t *dir;
    kuda_finfo_t first, second;

    KUDA_ASSERT_SUCCESS(tc, "kuda_dir_open failed", kuda_dir_open(&dir, "data", p));

    KUDA_ASSERT_SUCCESS(tc, "kuda_dir_read failed",
                       kuda_dir_read(&first, KUDA_FINFO_DIRENT, dir));

    KUDA_ASSERT_SUCCESS(tc, "kuda_dir_rewind failed", kuda_dir_rewind(dir));

    KUDA_ASSERT_SUCCESS(tc, "second kuda_dir_read failed",
                       kuda_dir_read(&second, KUDA_FINFO_DIRENT, dir));

    KUDA_ASSERT_SUCCESS(tc, "kuda_dir_close failed", kuda_dir_close(dir));

    ABTS_STR_EQUAL(tc, first.name, second.name);
}

/* Test for a (fixed) bug in kuda_dir_read().  This bug only happened
   in threadless cases. */
static void test_uncleared_errno(abts_case *tc, void *data)
{
    kuda_file_t *thefile = NULL;
    kuda_finfo_t finfo;
    kuda_int32_t finfo_flags = KUDA_FINFO_TYPE | KUDA_FINFO_NAME;
    kuda_dir_t *this_dir;
    kuda_status_t rv; 

    rv = kuda_dir_make("dir1", KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_dir_make("dir2", KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_file_open(&thefile, "dir1/file1",
                       KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE, KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_file_close(thefile);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    /* Try to remove dir1.  This should fail because it's not empty.
       However, on a platform with threads disabled (such as FreeBSD),
       `errno' will be set as a result. */
    rv = kuda_dir_remove("dir1", p);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_ENOTEMPTY(rv));
    
    /* Read `.' and `..' out of dir2. */
    rv = kuda_dir_open(&this_dir, "dir2", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_dir_read(&finfo, finfo_flags, this_dir);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_dir_read(&finfo, finfo_flags, this_dir);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    /* Now, when we attempt to do a third read of empty dir2, and the
       underlying system readdir() returns NULL, the old value of
       errno shouldn't cause a false alarm.  We should get an ENOENT
       back from kuda_dir_read, and *not* the old errno. */
    rv = kuda_dir_read(&finfo, finfo_flags, this_dir);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_ENOENT(rv));

    rv = kuda_dir_close(this_dir);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
		 
    /* Cleanup */
    rv = kuda_file_remove("dir1/file1", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_dir_remove("dir1", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    rv = kuda_dir_remove("dir2", p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

}

static void test_rmkdir_nocwd(abts_case *tc, void *data)
{
    char *cwd, *path;

    KUDA_ASSERT_SUCCESS(tc, "make temp dir",
                       kuda_dir_make("dir3", KUDA_PLATFORM_DEFAULT, p));

    KUDA_ASSERT_SUCCESS(tc, "obtain cwd", kuda_filepath_get(&cwd, 0, p));

    KUDA_ASSERT_SUCCESS(tc, "determine path to temp dir",
                       kuda_filepath_merge(&path, cwd, "dir3", 0, p));

    KUDA_ASSERT_SUCCESS(tc, "change to temp dir", kuda_filepath_set(path, p));

    KUDA_ASSERT_SUCCESS(tc, "restore cwd", kuda_filepath_set(cwd, p));

    KUDA_ASSERT_SUCCESS(tc, "remove cwd", kuda_dir_remove(path, p));
}


abts_suite *testdir(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, test_mkdir, NULL);
    abts_run_test(suite, test_mkdir_recurs, NULL);
    abts_run_test(suite, test_mkdir_recurs_parallel, NULL);
    abts_run_test(suite, test_remove, NULL);
    abts_run_test(suite, test_removeall_fail, NULL);
    abts_run_test(suite, test_removeall, NULL);
    abts_run_test(suite, test_remove_notthere, NULL);
    abts_run_test(suite, test_mkdir_twice, NULL);
    abts_run_test(suite, test_rmkdir_nocwd, NULL);

    abts_run_test(suite, test_rewind, NULL);

    abts_run_test(suite, test_opendir, NULL);
    abts_run_test(suite, test_opendir_notthere, NULL);
    abts_run_test(suite, test_closedir, NULL);
    abts_run_test(suite, test_uncleared_errno, NULL);

    return suite;
}

