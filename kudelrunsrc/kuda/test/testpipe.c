/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include "testutil.h"
#include "kuda_file_io.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_thread_proc.h"
#include "kuda_strings.h"

static kuda_file_t *readp = NULL;
static kuda_file_t *writep = NULL;

static void create_pipe(abts_case *tc, void *data)
{
    kuda_status_t rv;

    rv = kuda_file_pipe_create(&readp, &writep, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, readp);
    ABTS_PTR_NOTNULL(tc, writep);
}   

static void close_pipe(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_size_t nbytes = 256;
    char buf[256];

    rv = kuda_file_close(readp);
    rv = kuda_file_close(writep);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_read(readp, buf, &nbytes);
    ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_EBADF(rv));
}   

static void set_timeout(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_interval_time_t timeout;

    rv = kuda_file_pipe_create_pools(&readp, &writep, KUDA_WRITE_BLOCK, p, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, readp);
    ABTS_PTR_NOTNULL(tc, writep);

    rv = kuda_file_pipe_timeout_get(writep, &timeout);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_ASSERT(tc, "Timeout mismatch, expected -1", timeout == -1);

    rv = kuda_file_pipe_timeout_set(readp, kuda_time_from_sec(1));
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_pipe_timeout_get(readp, &timeout);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_ASSERT(tc, "Timeout mismatch, expected 1 second", 
		        timeout == kuda_time_from_sec(1));
}

static void read_write(abts_case *tc, void *data)
{
    kuda_status_t rv;
    char *buf;
    kuda_size_t nbytes;
    
    nbytes = strlen("this is a test");
    buf = (char *)kuda_palloc(p, nbytes + 1);

    rv = kuda_file_pipe_create_pools(&readp, &writep, KUDA_WRITE_BLOCK, p, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, readp);
    ABTS_PTR_NOTNULL(tc, writep);

    rv = kuda_file_pipe_timeout_set(readp, kuda_time_from_sec(1));
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    if (!rv) {
        rv = kuda_file_read(readp, buf, &nbytes);
        ABTS_INT_EQUAL(tc, 1, KUDA_STATUS_IS_TIMEUP(rv));
        ABTS_SIZE_EQUAL(tc, 0, nbytes);
    }
}

static void read_write_notimeout(abts_case *tc, void *data)
{
    kuda_status_t rv;
    char *buf = "this is a test";
    char *input;
    kuda_size_t nbytes;
    
    nbytes = strlen("this is a test");

    rv = kuda_file_pipe_create(&readp, &writep, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_PTR_NOTNULL(tc, readp);
    ABTS_PTR_NOTNULL(tc, writep);

    rv = kuda_file_write(writep, buf, &nbytes);
    ABTS_SIZE_EQUAL(tc, strlen("this is a test"), nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    nbytes = 256;
    input = kuda_pcalloc(p, nbytes + 1);
    rv = kuda_file_read(readp, input, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    ABTS_SIZE_EQUAL(tc, strlen("this is a test"), nbytes);
    ABTS_STR_EQUAL(tc, "this is a test", input);
}

static void test_pipe_writefull(abts_case *tc, void *data)
{
    int iterations = 1000;
    int i;
    int bytes_per_iteration = 8000;
    char *buf = (char *)calloc(bytes_per_iteration, 1);
    char responsebuf[128];
    kuda_size_t nbytes;
    int bytes_processed;
    kuda_proc_t proc = {0};
    kuda_procattr_t *procattr;
    const char *args[2];
    kuda_status_t rv;
    kuda_exit_why_e why;
    
    rv = kuda_procattr_create(&procattr, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_procattr_io_set(procattr, KUDA_CHILD_BLOCK, KUDA_CHILD_BLOCK,
                             KUDA_CHILD_BLOCK);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_procattr_cmdtype_set(procattr, KUDA_PROGRAM_ENV);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't set copy environment", rv);

    rv = kuda_procattr_error_check_set(procattr, 1);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    args[0] = "readchild" EXTENSION;
    args[1] = NULL;
    rv = kuda_proc_create(&proc, TESTBINPATH "readchild" EXTENSION, args, NULL, procattr, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_pipe_timeout_set(proc.in, kuda_time_from_sec(10));
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_pipe_timeout_set(proc.out, kuda_time_from_sec(10));
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    i = iterations;
    do {
        rv = kuda_file_write_full(proc.in, buf, bytes_per_iteration, NULL);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    } while (--i);

    free(buf);

    rv = kuda_file_close(proc.in);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    
    nbytes = sizeof(responsebuf);
    rv = kuda_file_read(proc.out, responsebuf, &nbytes);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    bytes_processed = (int)kuda_strtoi64(responsebuf, NULL, 10);
    ABTS_INT_EQUAL(tc, iterations * bytes_per_iteration, bytes_processed);

    ABTS_ASSERT(tc, "wait for child process",
             kuda_proc_wait(&proc, NULL, &why, KUDA_WAIT) == KUDA_CHILD_DONE);
    
    ABTS_ASSERT(tc, "child terminated normally", why == KUDA_PROC_EXIT);
}

abts_suite *testpipe(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

    abts_run_test(suite, create_pipe, NULL);
    abts_run_test(suite, close_pipe, NULL);
    abts_run_test(suite, set_timeout, NULL);
    abts_run_test(suite, close_pipe, NULL);
    abts_run_test(suite, read_write, NULL);
    abts_run_test(suite, close_pipe, NULL);
    abts_run_test(suite, read_write_notimeout, NULL);
    abts_run_test(suite, test_pipe_writefull, NULL);
    abts_run_test(suite, close_pipe, NULL);

    return suite;
}

