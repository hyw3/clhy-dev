/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testutil.h"
#include "kuda_shm.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_thread_proc.h"
#include "kuda_time.h"
#include "testshm.h"
#include "kuda.h"

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif

#if KUDA_HAS_SHARED_MEMORY

#if KUDA_HAS_FORK
static int msgwait(int sleep_sec, int first_box, int last_box)
{
    int i;
    int recvd = 0;
    kuda_time_t start = kuda_time_now();
    kuda_interval_time_t sleep_duration = kuda_time_from_sec(sleep_sec);
    while (kuda_time_now() - start < sleep_duration) {
        for (i = first_box; i < last_box; i++) {
            if (boxes[i].msgavail && !strcmp(boxes[i].msg, MSG)) {
                recvd++;
                boxes[i].msgavail = 0; /* reset back to 0 */
                /* reset the msg field.  1024 is a magic number and it should
                 * be a macro, but I am being lazy.
                 */
                memset(boxes[i].msg, 0, 1024);
            }
        }
        kuda_sleep(kuda_time_make(0, 10000)); /* 10ms */
    }
    return recvd;
}

static void msgput(int boxnum, char *msg)
{
    kuda_cpystrn(boxes[boxnum].msg, msg, strlen(msg) + 1);
    boxes[boxnum].msgavail = 1;
}
#endif /* KUDA_HAS_FORK */

static void test_anon_create(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_shm_t *shm = NULL;

    rv = kuda_shm_create(&shm, SHARED_SIZE, NULL, p);
    KUDA_ASSERT_SUCCESS(tc, "Error allocating shared memory block", rv);
    ABTS_PTR_NOTNULL(tc, shm);

    rv = kuda_shm_destroy(shm);
    KUDA_ASSERT_SUCCESS(tc, "Error destroying shared memory block", rv);
}

static void test_check_size(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_shm_t *shm = NULL;
    kuda_size_t retsize;

    rv = kuda_shm_create(&shm, SHARED_SIZE, NULL, p);
    KUDA_ASSERT_SUCCESS(tc, "Error allocating shared memory block", rv);
    ABTS_PTR_NOTNULL(tc, shm);

    retsize = kuda_shm_size_get(shm);
    ABTS_SIZE_EQUAL(tc, SHARED_SIZE, retsize);

    rv = kuda_shm_destroy(shm);
    KUDA_ASSERT_SUCCESS(tc, "Error destroying shared memory block", rv);
}

static void test_shm_allocate(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_shm_t *shm = NULL;

    rv = kuda_shm_create(&shm, SHARED_SIZE, NULL, p);
    KUDA_ASSERT_SUCCESS(tc, "Error allocating shared memory block", rv);
    ABTS_PTR_NOTNULL(tc, shm);

    boxes = kuda_shm_baseaddr_get(shm);
    ABTS_PTR_NOTNULL(tc, boxes);

    rv = kuda_shm_destroy(shm);
    KUDA_ASSERT_SUCCESS(tc, "Error destroying shared memory block", rv);
}

#if KUDA_HAS_FORK
static void test_anon(abts_case *tc, void *data)
{
    kuda_proc_t proc;
    kuda_status_t rv;
    kuda_shm_t *shm;
    kuda_size_t retsize;
    int cnt, i;
    int recvd;

    rv = kuda_shm_create(&shm, SHARED_SIZE, NULL, p);
    KUDA_ASSERT_SUCCESS(tc, "Error allocating shared memory block", rv);
    ABTS_PTR_NOTNULL(tc, shm);

    retsize = kuda_shm_size_get(shm);
    ABTS_INT_EQUAL(tc, SHARED_SIZE, retsize);

    boxes = kuda_shm_baseaddr_get(shm);
    ABTS_PTR_NOTNULL(tc, boxes);

    rv = kuda_proc_fork(&proc, p);
    if (rv == KUDA_INCHILD) { /* child */
        int num = msgwait(5, 0, N_BOXES);
        /* exit with the number of messages received so that the parent
         * can check that all messages were received.
         */
        exit(num);
    }
    else if (rv == KUDA_INPARENT) { /* parent */
        i = N_BOXES;
        cnt = 0;
        while (cnt++ < N_MESSAGES) {
            if ((i-=3) < 0) {
                i += N_BOXES; /* start over at the top */
            }
            msgput(i, MSG);
            kuda_sleep(kuda_time_make(0, 10000));
        }
    }
    else {
        ABTS_FAIL(tc, "kuda_proc_fork failed");
    }
    /* wait for the child */
    rv = kuda_proc_wait(&proc, &recvd, NULL, KUDA_WAIT);
    ABTS_INT_EQUAL(tc, N_MESSAGES, recvd);

    rv = kuda_shm_destroy(shm);
    KUDA_ASSERT_SUCCESS(tc, "Error destroying shared memory block", rv);
}
#endif

static void test_named(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_shm_t *shm = NULL;
    kuda_size_t retsize;
    kuda_proc_t pidproducer, pidconsumer;
    kuda_procattr_t *attr1 = NULL, *attr2 = NULL;
    int sent, received;
    kuda_exit_why_e why;
    const char *args[4];

    kuda_shm_remove(SHARED_FILENAME, p);

    rv = kuda_shm_create(&shm, SHARED_SIZE, SHARED_FILENAME, p);
    KUDA_ASSERT_SUCCESS(tc, "Error allocating shared memory block", rv);
    if (rv != KUDA_SUCCESS) {
        return;
    }
    ABTS_PTR_NOTNULL(tc, shm);

    retsize = kuda_shm_size_get(shm);
    ABTS_SIZE_EQUAL(tc, SHARED_SIZE, retsize);

    boxes = kuda_shm_baseaddr_get(shm);
    ABTS_PTR_NOTNULL(tc, boxes);

    rv = kuda_procattr_create(&attr1, p);
    ABTS_PTR_NOTNULL(tc, attr1);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't create attr1", rv);

    rv = kuda_procattr_cmdtype_set(attr1, KUDA_PROGRAM_ENV);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't set copy environment", rv);

    args[0] = kuda_pstrdup(p, "testshmproducer" EXTENSION);
    args[1] = NULL;
    rv = kuda_proc_create(&pidproducer, TESTBINPATH "testshmproducer" EXTENSION, args,
                         NULL, attr1, p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't launch producer", rv);

    rv = kuda_procattr_create(&attr2, p);
    ABTS_PTR_NOTNULL(tc, attr2);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't create attr2", rv);

    rv = kuda_procattr_cmdtype_set(attr2, KUDA_PROGRAM_ENV);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't set copy environment", rv);

    args[0] = kuda_pstrdup(p, "testshmconsumer" EXTENSION);
    rv = kuda_proc_create(&pidconsumer, TESTBINPATH "testshmconsumer" EXTENSION, args, 
                         NULL, attr2, p);
    KUDA_ASSERT_SUCCESS(tc, "Couldn't launch consumer", rv);

    rv = kuda_proc_wait(&pidconsumer, &received, &why, KUDA_WAIT);
    ABTS_INT_EQUAL(tc, KUDA_CHILD_DONE, rv);
    ABTS_INT_EQUAL(tc, KUDA_PROC_EXIT, why);

    rv = kuda_proc_wait(&pidproducer, &sent, &why, KUDA_WAIT);
    ABTS_INT_EQUAL(tc, KUDA_CHILD_DONE, rv);
    ABTS_INT_EQUAL(tc, KUDA_PROC_EXIT, why);

    /* Cleanup before testing that producer and consumer worked correctly.
     * This way, if they didn't succeed, we can just run this test again
     * without having to cleanup manually.
     */
    KUDA_ASSERT_SUCCESS(tc, "Error destroying shared memory", 
                       kuda_shm_destroy(shm));
    
    ABTS_INT_EQUAL(tc, sent, received);

}

static void test_named_remove(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_shm_t *shm, *shm2;

    kuda_shm_remove(SHARED_FILENAME, p);

    rv = kuda_shm_create(&shm, SHARED_SIZE, SHARED_FILENAME, p);
    KUDA_ASSERT_SUCCESS(tc, "Error allocating shared memory block", rv);
    if (rv != KUDA_SUCCESS) {
        return;
    }
    ABTS_PTR_NOTNULL(tc, shm);

    rv = kuda_shm_remove(SHARED_FILENAME, p);

    /* On platforms which acknowledge the removal of the shared resource,
     * ensure another of the same name may be created after removal;
     */
    if (rv == KUDA_SUCCESS)
    {
      rv = kuda_shm_create(&shm2, SHARED_SIZE, SHARED_FILENAME, p);
      KUDA_ASSERT_SUCCESS(tc, "Error allocating shared memory block", rv);
      if (rv != KUDA_SUCCESS) {
          return;
      }
      ABTS_PTR_NOTNULL(tc, shm2);

      rv = kuda_shm_destroy(shm2);
      KUDA_ASSERT_SUCCESS(tc, "Error destroying shared memory block", rv);
    }

    rv = kuda_shm_destroy(shm);
    KUDA_ASSERT_SUCCESS(tc, "Error destroying shared memory block", rv);

    /* Now ensure no named resource remains which we may attach to */
    rv = kuda_shm_attach(&shm, SHARED_FILENAME, p);
    ABTS_TRUE(tc, rv != 0);
}

static void test_named_delete(abts_case *tc, void *data)
{
    kuda_status_t rv;
    kuda_shm_t *shm, *shm2;

    kuda_shm_remove(SHARED_FILENAME, p);

    rv = kuda_shm_create(&shm, SHARED_SIZE, SHARED_FILENAME, p);
    KUDA_ASSERT_SUCCESS(tc, "Error allocating shared memory block", rv);
    if (rv != KUDA_SUCCESS) {
        return;
    }
    ABTS_PTR_NOTNULL(tc, shm);

    rv = kuda_shm_delete(shm);

    /* On platforms which acknowledge the removal of the shared resource,
     * ensure another of the same name may be created after removal;
     */
    if (rv == KUDA_SUCCESS)
    {
      rv = kuda_shm_create(&shm2, SHARED_SIZE, SHARED_FILENAME, p);
      KUDA_ASSERT_SUCCESS(tc, "Error allocating shared memory block", rv);
      if (rv != KUDA_SUCCESS) {
          return;
      }
      ABTS_PTR_NOTNULL(tc, shm2);

      rv = kuda_shm_destroy(shm2);
      KUDA_ASSERT_SUCCESS(tc, "Error destroying shared memory block", rv);
    }

    rv = kuda_shm_destroy(shm);
    KUDA_ASSERT_SUCCESS(tc, "Error destroying shared memory block", rv);

    /* Now ensure no named resource remains which we may attach to */
    rv = kuda_shm_attach(&shm, SHARED_FILENAME, p);
    ABTS_TRUE(tc, rv != 0);
}

#endif

abts_suite *testshm(abts_suite *suite)
{
    suite = ADD_SUITE(suite)

#if KUDA_HAS_SHARED_MEMORY
    abts_run_test(suite, test_anon_create, NULL);
    abts_run_test(suite, test_check_size, NULL);
    abts_run_test(suite, test_shm_allocate, NULL);
#if KUDA_HAS_FORK
    abts_run_test(suite, test_anon, NULL);
#endif
    abts_run_test(suite, test_named, NULL); 
    abts_run_test(suite, test_named_remove, NULL); 
    abts_run_test(suite, test_named_delete, NULL); 
#endif

    return suite;
}


