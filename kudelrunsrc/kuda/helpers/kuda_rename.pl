#!/usr/bin/perl -w
use strict;
use ExtUtils::MakeMaker qw(prompt);
use File::Find;

my $just_check = @ARGV ? $ARGV[0] eq '-c' : 0;
shift if $just_check;
my $dir = shift || '.';
my %names;

my $prefix = 'kuda_';

while (<DATA>) {
    chomp;
    my($old, $new) = grep { s/^$prefix//o } split;
    next unless $old and $new;
    $names{$old} = $new;
}

my $pattern = join '|', keys %names;
#print "replacement pattern=$pattern\n";

find sub {
    chomp;
    return unless /\.[ch]$/;
    my $file = "$File::Find::dir/$_";
    print "looking in $file\n";

    replace($_, !$just_check);

}, $dir;

sub replace {
    my($file, $replace) = @_;
    local *IN, *OUT;
    my @lines;
    my $found = 0;

    open IN, $file or die "open $file: $!";

    while (<IN>) {
        for (m/[^_\"]*$prefix($pattern)\b/og) {
            $found++;
            print "   $file:$. kuda_$_ -> kuda_$names{$_}\n";
        }
        push @lines, $_ if $replace;
    }

    close IN;

    return unless $found and $replace;

#    my $ans = prompt("replace?", 'y');
#    return unless $ans =~ /^y/i;

    open OUT, ">$file" or die "open $file: $!";

    for (@lines) {
        unless (/^\#include/) {
            s/([^_\"]*$prefix)($pattern)\b/$1$names{$2}/og;
        }
        print OUT $_;
    }

    close OUT;
}

__DATA__
kuda_time_t:
kuda_implode_gmt              kuda_time_exp_gmt_get

kuda_socket_t:
kuda_close_socket             kuda_socket_close
kuda_create_socket            kuda_socket_create
kuda_get_sockaddr             kuda_socket_addr_get
kuda_get_socketdata           kuda_socket_data_get
kuda_set_socketdata           kuda_socket_data_set
kuda_shutdown                 kuda_socket_shutdown
kuda_bind                     kuda_socket_bind
kuda_listen                   kuda_socket_listen
kuda_accept                   kuda_socket_accept
kuda_connect                  kuda_socket_connect
kuda_send                     kuda_socket_send
kuda_sendv                    kuda_socket_sendv
kuda_sendto                   kuda_socket_sendto
kuda_recvfrom                 kuda_socket_recvfrom
kuda_sendfile                 kuda_socket_sendfile
kuda_recv                     kuda_socket_recv

kuda_filepath_*:
kuda_filename_of_pathname     kuda_filepath_name_get

kuda_gid_t:
kuda_get_groupid              kuda_gid_get
kuda_get_groupname            kuda_gid_name_get
kuda_group_name_get           kuda_gid_name_get
kuda_compare_groups           kuda_gid_compare

kuda_uid_t:
kuda_get_home_directory       kuda_uid_homepath_get
kuda_get_userid               kuda_uid_get
kuda_current_userid           kuda_uid_current
kuda_compare_users            kuda_uid_compare
kuda_get_username             kuda_uid_name_get
kuda_compare_users            kuda_uid_compare

