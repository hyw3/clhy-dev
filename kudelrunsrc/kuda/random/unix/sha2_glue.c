/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <kuda.h>
#include <kuda_random.h>
#include <kuda_pools.h>
#include "sha2.h"

static void sha256_init(kuda_crypto_hash_t *h)
{
    kuda__SHA256_Init(h->data);
}

static void sha256_add(kuda_crypto_hash_t *h,const void *data,
                       kuda_size_t bytes)
{
    kuda__SHA256_Update(h->data,data,bytes);
}

static void sha256_finish(kuda_crypto_hash_t *h,unsigned char *result)
{
    kuda__SHA256_Final(result,h->data);
}

KUDA_DECLARE(kuda_crypto_hash_t *) kuda_crypto_sha256_new(kuda_pool_t *p)
{
    kuda_crypto_hash_t *h=kuda_palloc(p,sizeof *h);

    h->data=kuda_palloc(p,sizeof(SHA256_CTX));
    h->init=sha256_init;
    h->add=sha256_add;
    h->finish=sha256_finish;
    h->size=256/8;

    return h;
}
