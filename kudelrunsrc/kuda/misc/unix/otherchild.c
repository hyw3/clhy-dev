/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_arch_misc.h"
#include "kuda_arch_threadproc.h"
#include "kuda_arch_file_io.h"

#if KUDA_HAS_OTHER_CHILD

#ifdef HAVE_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_SYS_SELECT_H
#include <sys/select.h>
#endif
#if KUDA_HAVE_SYS_WAIT_H
#include <sys/wait.h>
#endif
#ifdef BEOS
#include <sys/socket.h> /* for fd_set definition! */
#endif

static kuda_other_child_rec_t *other_children = NULL;

static kuda_status_t other_child_cleanup(void *data)
{
    kuda_other_child_rec_t **pocr, *nocr;

    for (pocr = &other_children; *pocr; pocr = &(*pocr)->next) {
        if ((*pocr)->data == data) {
            nocr = (*pocr)->next;
            (*(*pocr)->maintenance) (KUDA_OC_REASON_UNREGISTER, (*pocr)->data, -1);
            *pocr = nocr;
            /* XXX: um, well we've just wasted some space in pconf ? */
            return KUDA_SUCCESS;
        }
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(void) kuda_proc_other_child_register(kuda_proc_t *proc,
                     void (*maintenance) (int reason, void *, int status),
                     void *data, kuda_file_t *write_fd, kuda_pool_t *p)
{
    kuda_other_child_rec_t *ocr;

    ocr = kuda_palloc(p, sizeof(*ocr));
    ocr->p = p;
    ocr->proc = proc;
    ocr->maintenance = maintenance;
    ocr->data = data;
    if (write_fd == NULL) {
        ocr->write_fd = (kuda_platform_file_t) -1;
    }
    else {
#ifdef WIN32
        /* This should either go away as part of eliminating kuda_proc_probe_writable_fds
         * or write_fd should point to an kuda_file_t
         */
        ocr->write_fd = write_fd->filehand; 
#else
        ocr->write_fd = write_fd->filedes;
#endif

    }
    ocr->next = other_children;
    other_children = ocr;
    kuda_pool_cleanup_register(p, ocr->data, other_child_cleanup, 
                              kuda_pool_cleanup_null);
}

KUDA_DECLARE(void) kuda_proc_other_child_unregister(void *data)
{
    kuda_other_child_rec_t *cur;

    cur = other_children;
    while (cur) {
        if (cur->data == data) {
            break;
        }
        cur = cur->next;
    }

    /* segfault if this function called with invalid parm */
    kuda_pool_cleanup_kill(cur->p, cur->data, other_child_cleanup);
    other_child_cleanup(data);
}

KUDA_DECLARE(kuda_status_t) kuda_proc_other_child_alert(kuda_proc_t *proc,
                                                     int reason,
                                                     int status)
{
    kuda_other_child_rec_t *ocr, *nocr;

    for (ocr = other_children; ocr; ocr = nocr) {
        nocr = ocr->next;
        if (ocr->proc->pid != proc->pid)
            continue;

        ocr->proc = NULL;
        (*ocr->maintenance) (reason, ocr->data, status);
        return KUDA_SUCCESS;
    }
    return KUDA_EPROC_UNKNOWN;
}

KUDA_DECLARE(void) kuda_proc_other_child_refresh(kuda_other_child_rec_t *ocr,
                                               int reason)
{
    /* Todo: 
     * Implement code to detect if pipes are still alive.
     */
#ifdef WIN32
    DWORD status;

    if (ocr->proc == NULL)
        return;

    if (!ocr->proc->hproc) {
        /* Already mopped up, perhaps we kuda_proc_kill'ed it,
         * they should have already unregistered!
         */
        ocr->proc = NULL;
        (*ocr->maintenance) (KUDA_OC_REASON_LOST, ocr->data, -1);
    }
    else if (!GetExitCodeProcess(ocr->proc->hproc, &status)) {
        CloseHandle(ocr->proc->hproc);
        ocr->proc->hproc = NULL;
        ocr->proc = NULL;
        (*ocr->maintenance) (KUDA_OC_REASON_LOST, ocr->data, -1);
    }
    else if (status == STILL_ACTIVE) {
        (*ocr->maintenance) (reason, ocr->data, -1);
    }
    else {
        CloseHandle(ocr->proc->hproc);
        ocr->proc->hproc = NULL;
        ocr->proc = NULL;
        (*ocr->maintenance) (KUDA_OC_REASON_DEATH, ocr->data, status);
    }

#else /* ndef Win32 */
    pid_t waitret; 
    int status;

    if (ocr->proc == NULL)
        return;

    waitret = waitpid(ocr->proc->pid, &status, WNOHANG);
    if (waitret == ocr->proc->pid) {
        ocr->proc = NULL;
        (*ocr->maintenance) (KUDA_OC_REASON_DEATH, ocr->data, status);
    }
    else if (waitret == 0) {
        (*ocr->maintenance) (reason, ocr->data, -1);
    }
    else if (waitret == -1) {
        /* uh what the heck? they didn't call unregister? */
        ocr->proc = NULL;
        (*ocr->maintenance) (KUDA_OC_REASON_LOST, ocr->data, -1);
    }
#endif
}

KUDA_DECLARE(void) kuda_proc_other_child_refresh_all(int reason)
{
    kuda_other_child_rec_t *ocr, *next_ocr;

    for (ocr = other_children; ocr; ocr = next_ocr) {
        next_ocr = ocr->next;
        kuda_proc_other_child_refresh(ocr, reason);
    }
}

#else /* !KUDA_HAS_OTHER_CHILD */

KUDA_DECLARE(void) kuda_proc_other_child_register(kuda_proc_t *proc,
                     void (*maintenance) (int reason, void *, int status),
                     void *data, kuda_file_t *write_fd, kuda_pool_t *p)
{
    return;
}

KUDA_DECLARE(void) kuda_proc_other_child_unregister(void *data)
{
    return;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_other_child_alert(kuda_proc_t *proc,
                                                     int reason,
                                                     int status)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(void) kuda_proc_other_child_refresh(kuda_other_child_rec_t *ocr,
                                               int reason)
{
    return;
}

KUDA_DECLARE(void) kuda_proc_other_child_refresh_all(int reason)
{
    return;
}

#endif /* KUDA_HAS_OTHER_CHILD */
