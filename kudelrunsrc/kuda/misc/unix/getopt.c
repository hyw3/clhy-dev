/*
 * Copyright (c) 1987, 1993, 1994
 *      The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by the University of
 *      California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "kuda_arch_misc.h"
#include "kuda_strings.h"
#include "kuda_lib.h"

#define EMSG    ""

KUDA_DECLARE(kuda_status_t) kuda_getopt_init(kuda_getopt_t **platforms, kuda_pool_t *cont,
                                      int argc, const char *const *argv)
{
    void *argv_buff;

    *platforms = kuda_palloc(cont, sizeof(kuda_getopt_t));
    (*platforms)->cont = cont;
    (*platforms)->reset = 0;
    (*platforms)->errfn = (kuda_getopt_err_fn_t*)(fprintf);
    (*platforms)->errarg = (void*)(stderr);

    (*platforms)->place = EMSG;
    (*platforms)->argc = argc;

    /* The argv parameter must be compatible with main()'s argv, since
       that's the primary purpose of this function.  But people might
       want to use this function with arrays other than the main argv,
       and we shouldn't touch the caller's data.  So we copy. */
    argv_buff = kuda_palloc(cont, (argc + 1) * sizeof(const char *));
    memcpy(argv_buff, argv, argc * sizeof(const char *));
    (*platforms)->argv = argv_buff;
    (*platforms)->argv[argc] = NULL;

    (*platforms)->interleave = 0;
    (*platforms)->ind = 1;
    (*platforms)->skip_start = 1;
    (*platforms)->skip_end = 1;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_getopt(kuda_getopt_t *platforms, const char *opts, 
                                     char *optch, const char **optarg)
{
    const char *oli;  /* option letter list index */

    if (platforms->reset || !*platforms->place) {   /* update scanning pointer */
        platforms->reset = 0;
        if (platforms->ind >= platforms->argc || *(platforms->place = platforms->argv[platforms->ind]) != '-') {
            platforms->place = EMSG;
            *optch = platforms->opt;
            return (KUDA_EOF);
        }
        if (platforms->place[1] && *++platforms->place == '-') {        /* found "--" */
            ++platforms->ind;
            platforms->place = EMSG;
            *optch = platforms->opt;
            return (KUDA_EOF);
        }
    }                                /* option letter okay? */
    if ((platforms->opt = (int) *platforms->place++) == (int) ':' ||
        !(oli = strchr(opts, platforms->opt))) {
        /*
         * if the user didn't specify '-' as an option,
         * assume it means -1.
         */
        if (platforms->opt == (int) '-') {
            *optch = platforms->opt;
            return (KUDA_EOF);
        }
        if (!*platforms->place)
            ++platforms->ind;
        if (platforms->errfn && *opts != ':') {
            (platforms->errfn)(platforms->errarg, "%s: illegal option -- %c\n",
                        kuda_filepath_name_get(*platforms->argv), platforms->opt);
        }
        *optch = platforms->opt;
        return (KUDA_BADCH);
    }
    if (*++oli != ':') {        /* don't need argument */
        *optarg = NULL;
        if (!*platforms->place)
            ++platforms->ind;
    }
    else {                        /* need an argument */
        if (*platforms->place)                /* no white space */
            *optarg = platforms->place;
        else if (platforms->argc <= ++platforms->ind) {        /* no arg */
            platforms->place = EMSG;
            if (*opts == ':') {
                *optch = platforms->opt;
                return (KUDA_BADARG);
            }
            if (platforms->errfn) {
                (platforms->errfn)(platforms->errarg, 
                            "%s: option requires an argument -- %c\n",
                            kuda_filepath_name_get(*platforms->argv), platforms->opt);
            }
            *optch = platforms->opt;
            return (KUDA_BADCH);
        }
        else                        /* white space */
            *optarg = platforms->argv[platforms->ind];
        platforms->place = EMSG;
        ++platforms->ind;
    }
    *optch = platforms->opt;
    return KUDA_SUCCESS;
}

/* Reverse the sequence argv[start..start+len-1]. */
static void reverse(const char **argv, int start, int len)
{
    const char *temp;

    for (; len >= 2; start++, len -= 2) {
        temp = argv[start];
        argv[start] = argv[start + len - 1];
        argv[start + len - 1] = temp;
    }
}

/*
 * Permute platforms->argv with the goal that non-option arguments will all
 * appear at the end.  platforms->skip_start is where we started skipping
 * non-option arguments, platforms->skip_end is where we stopped, and platforms->ind
 * is where we are now.
 */
static void permute(kuda_getopt_t *platforms)
{
    int len1 = platforms->skip_end - platforms->skip_start;
    int len2 = platforms->ind - platforms->skip_end;

    if (platforms->interleave) {
        /*
         * Exchange the sequences argv[platforms->skip_start..platforms->skip_end-1] and
         * argv[platforms->skip_end..platforms->ind-1].  The easiest way to do that is
         * to reverse the entire range and then reverse the two
         * sub-ranges.
         */
        reverse(platforms->argv, platforms->skip_start, len1 + len2);
        reverse(platforms->argv, platforms->skip_start, len2);
        reverse(platforms->argv, platforms->skip_start + len2, len1);
    }

    /* Reset skip range to the new location of the non-option sequence. */
    platforms->skip_start += len2;
    platforms->skip_end += len2;
}

/* Helper function to print out an error involving a long option */
static kuda_status_t serr(kuda_getopt_t *platforms, const char *err, const char *str,
                         kuda_status_t status)
{
    if (platforms->errfn)
        (platforms->errfn)(platforms->errarg, "%s: %s: %s\n", 
                    kuda_filepath_name_get(*platforms->argv), err, str);
    return status;
}

/* Helper function to print out an error involving a short option */
static kuda_status_t cerr(kuda_getopt_t *platforms, const char *err, int ch,
                         kuda_status_t status)
{
    if (platforms->errfn)
        (platforms->errfn)(platforms->errarg, "%s: %s: %c\n", 
                    kuda_filepath_name_get(*platforms->argv), err, ch);
    return status;
}

KUDA_DECLARE(kuda_status_t) kuda_getopt_long(kuda_getopt_t *platforms,
                                          const kuda_getopt_option_t *opts,
                                          int *optch, const char **optarg)
{
    const char *p;
    int i;

    /* Let the calling program reset option processing. */
    if (platforms->reset) {
        platforms->place = EMSG;
        platforms->ind = 1;
        platforms->reset = 0;
    }

    /*
     * We can be in one of two states: in the middle of processing a
     * run of short options, or about to process a new argument.
     * Since the second case can lead to the first one, handle that
     * one first.  */
    p = platforms->place;
    if (*p == '\0') {
        /* If we are interleaving, skip non-option arguments. */
        if (platforms->interleave) {
            while (platforms->ind < platforms->argc && *platforms->argv[platforms->ind] != '-')
                platforms->ind++;
            platforms->skip_end = platforms->ind;
        }
        if (platforms->ind >= platforms->argc || *platforms->argv[platforms->ind] != '-') {
            platforms->ind = platforms->skip_start;
            return KUDA_EOF;
        }

        p = platforms->argv[platforms->ind++] + 1;
        if (*p == '-' && p[1] != '\0') {        /* Long option */
            /* Search for the long option name in the caller's table. */
            kuda_size_t len = 0;

            p++;
            for (i = 0; ; i++) {
                if (opts[i].optch == 0)             /* No match */
                    return serr(platforms, "invalid option", p - 2, KUDA_BADCH);

                if (opts[i].name) {
                    len = strlen(opts[i].name);
                    if (strncmp(p, opts[i].name, len) == 0
                        && (p[len] == '\0' || p[len] == '='))
                        break;
                }
            }
            *optch = opts[i].optch;

            if (opts[i].has_arg) {
                if (p[len] == '=')             /* Argument inline */
                    *optarg = p + len + 1;
                else { 
                    if (platforms->ind >= platforms->argc)   /* Argument missing */
                        return serr(platforms, "missing argument", p - 2, KUDA_BADARG);
                    else                       /* Argument in next arg */
                        *optarg = platforms->argv[platforms->ind++];
                }
            } else {
                *optarg = NULL;
                if (p[len] == '=')
                    return serr(platforms, "erroneous argument", p - 2, KUDA_BADARG);
            }
            permute(platforms);
            return KUDA_SUCCESS;
        } else {
            if (*p == '-') {                 /* Bare "--"; we're done */
                permute(platforms);
                platforms->ind = platforms->skip_start;
                return KUDA_EOF;
            }
            else 
                if (*p == '\0')                    /* Bare "-" is illegal */
                    return serr(platforms, "invalid option", p, KUDA_BADCH);
        }
    }

    /*
     * Now we're in a run of short options, and *p is the next one.
     * Look for it in the caller's table.
     */
    for (i = 0; ; i++) {
        if (opts[i].optch == 0)                     /* No match */
            return cerr(platforms, "invalid option character", *p, KUDA_BADCH);

        if (*p == opts[i].optch)
            break;
    }
    *optch = *p++;

    if (opts[i].has_arg) {
        if (*p != '\0')                         /* Argument inline */
            *optarg = p;
        else { 
            if (platforms->ind >= platforms->argc)           /* Argument missing */
                return cerr(platforms, "missing argument", *optch, KUDA_BADARG);
            else                               /* Argument in next arg */
                *optarg = platforms->argv[platforms->ind++];
        }
        platforms->place = EMSG;
    } else {
        *optarg = NULL;
        platforms->place = p;
    }

    permute(platforms);
    return KUDA_SUCCESS;
}
