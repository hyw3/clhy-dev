/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_general.h"
#include "kuda_pools.h"
#include "kuda_signal.h"
#include "kuda_atomic.h"

#include "kuda_arch_proc_mutex.h" /* for kuda_proc_mutex_unix_setup_lock() */
#include "kuda_arch_internal_time.h"


KUDA_DECLARE(kuda_status_t) kuda_app_initialize(int *argc, 
                                             const char * const * *argv, 
                                             const char * const * *env)
{
    /* An absolute noop.  At present, only Win32 requires this stub, but it's
     * required in order to move command arguments passed through the service
     * control manager into the process, and it's required to fix the char*
     * data passed in from win32 unicode into utf-8, win32's kuda internal fmt.
     */
    return kuda_initialize();
}

static int initialized = 0;

KUDA_DECLARE(kuda_status_t) kuda_initialize(void)
{
    kuda_pool_t *pool;
    kuda_status_t status;

    if (initialized++) {
        return KUDA_SUCCESS;
    }

#if !defined(BEOS) && !defined(OS2)
    kuda_proc_mutex_unix_setup_lock();
    kuda_unix_setup_time();
#endif

    if ((status = kuda_pool_initialize()) != KUDA_SUCCESS)
        return status;
    
    if (kuda_pool_create(&pool, NULL) != KUDA_SUCCESS) {
        return KUDA_ENOPOOL;
    }

    kuda_pool_tag(pool, "kuda_initialize");

    /* kuda_atomic_init() used to be called from here aswell.
     * Pools rely on mutexes though, which can be backed by
     * atomics.  Due to this circular dependency
     * kuda_pool_initialize() is taking care of calling
     * kuda_atomic_init() at the correct time.
     */

    kuda_signal_init(pool);

    return KUDA_SUCCESS;
}

KUDA_DECLARE_NONSTD(void) kuda_terminate(void)
{
    initialized--;
    if (initialized) {
        return;
    }
    kuda_pool_terminate();
    
}

KUDA_DECLARE(void) kuda_terminate2(void)
{
    kuda_terminate();
}
