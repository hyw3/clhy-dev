/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"
#include "kuda.h"
#include "kuda_private.h"
#include "kuda_env.h"
#include "kuda_strings.h"

#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif

KUDA_DECLARE(kuda_status_t) kuda_env_get(char **value,
                                      const char *envvar,
                                      kuda_pool_t *pool)
{
#ifdef HAVE_GETENV

    char *val = getenv(envvar);
    if (!val)
        return KUDA_ENOENT;
    *value = val;
    return KUDA_SUCCESS;

#else
    return KUDA_ENOTIMPL;
#endif
}


KUDA_DECLARE(kuda_status_t) kuda_env_set(const char *envvar,
                                      const char *value,
                                      kuda_pool_t *pool)
{
#if defined(HAVE_SETENV)

    if (0 > setenv(envvar, value, 1))
        return KUDA_ENOMEM;
    return KUDA_SUCCESS;

#elif defined(HAVE_PUTENV)

    if (0 > putenv(kuda_pstrcat(pool, envvar, "=", value, NULL)))
        return KUDA_ENOMEM;
    return KUDA_SUCCESS;

#else
    return KUDA_ENOTIMPL;
#endif
}


KUDA_DECLARE(kuda_status_t) kuda_env_delete(const char *envvar, kuda_pool_t *pool)
{
#ifdef HAVE_UNSETENV

    unsetenv(envvar);
    return KUDA_SUCCESS;

#else
    /* hint: some platforms allow envvars to be unset via
     *       putenv("varname")...  that isn't Single Unix spec,
     *       but if your platform doesn't have unsetenv() it is
     *       worth investigating and potentially adding a
     *       configure check to decide when to use that form of
     *       putenv() here
     */
    return KUDA_ENOTIMPL;
#endif
}
