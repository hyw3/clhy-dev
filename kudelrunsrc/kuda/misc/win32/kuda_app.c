/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Usage Notes:
 *
 *   this cAPI, and the misc/win32/utf8.c cAPIs must be
 *   compiled KUDA_EXPORT_STATIC and linked to an application with
 *   the /entry:wmainCRTStartup flag (which this cAPI kindly
 *   provides to the developer who links to libkudaapp-1.lib).
 *   This cAPI becomes the true wmain entry point, and passes
 *   utf-8 reformatted argv and env arrays to the application's
 *   main() function as if nothing happened.
 *
 *   This cAPI is only compatible with Unicode operating systems.
 *   Mixed (Win9x backwards compatible) binaries should refer instead
 *   to the kuda_startup.c cAPI.
 *
 *   _dbg_malloc/realloc is used in place of the usual API, in order
 *   to convince the MSVCRT that it created these entities.  If we
 *   do not create them as _CRT_BLOCK entities, the crt will fault
 *   on an assert.  We are not worrying about the crt's locks here, 
 *   since we are single threaded [so far].
 */

#include "kuda_general.h"
#include "ShellAPI.h"
#include "wchar.h"
#include "kuda_arch_file_io.h"
#include "assert.h"
#include "kuda_private.h"
#include "kuda_arch_misc.h"

#pragma comment(linker,"/ENTRY:wmainCRTStartup")

extern int main(int argc, const char **argv, const char **env);

int wmain(int argc, const wchar_t **wargv, const wchar_t **wenv)
{
    char **argv;
    char **env;
    int dupenv;

    (void)kuda_wastrtoastr(&argv, wargv, argc);

    dupenv = kuda_wastrtoastr(&env, wenv, -1);

    _environ = kuda_malloc_dbg((dupenv + 1) * sizeof (char *),
                              __FILE__, __LINE__ );
    memcpy(_environ, env, (dupenv + 1) * sizeof (char *));

    /* MSVCRT will attempt to maintain the wide environment calls
     * on _putenv(), which is bogus if we've passed a non-ascii
     * string to _putenv(), since they use MultiByteToWideChar
     * and breaking the implicit utf-8 assumption we've built.
     *
     * Reset _wenviron for good measure.
     */
    if (_wenviron) {
        wenv = _wenviron;
        _wenviron = NULL;
        free((wchar_t **)wenv);
    }

    kuda_app_init_complete = 1;

    return main(argc, argv, env);
}
