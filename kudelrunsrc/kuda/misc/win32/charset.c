/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_portable.h"


KUDA_DECLARE(const char*) kuda_platform_default_encoding (kuda_pool_t *pool)
{
    return kuda_psprintf(pool, "CP%u", (unsigned) GetACP());
}


KUDA_DECLARE(const char*) kuda_platform_locale_encoding (kuda_pool_t *pool)
{
#ifdef _UNICODE
    int i;
#endif
#if defined(_WIN32_WCE)
    LCID locale = GetUserDefaultLCID();
#else
    LCID locale = GetThreadLocale();
#endif
    int len = GetLocaleInfo(locale, LOCALE_IDEFAULTANSICODEPAGE, NULL, 0);
    char *cp = kuda_palloc(pool, (len * sizeof(TCHAR)) + 2);
    if (0 < GetLocaleInfo(locale, LOCALE_IDEFAULTANSICODEPAGE, (TCHAR*) (cp + 2), len))
    {
        /* Fix up the returned number to make a valid codepage name of
          the form "CPnnnn". */
        cp[0] = 'C';
        cp[1] = 'P';
#ifdef _UNICODE
        for(i = 0; i < len; i++) {
            cp[i + 2] = (char) ((TCHAR*) (cp + 2))[i];
        }
#endif
        return cp;
    }

    return kuda_platform_default_encoding(pool);
}
