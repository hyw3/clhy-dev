/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_private.h"

#include "kuda_arch_misc.h"
#include "kuda_arch_file_io.h"
#include <assert.h>

/* This cAPI is the source of -static- helper functions that are
 * entirely internal to kuda.  If the fn is exported - it does not
 * belong here.
 *
 * Namespace decoration is still required to protect us from symbol
 * clashes in static linkages.
 */


/* Shared by kuda_app.c and start.c 
 *
 * An internal kuda function to convert an array of strings (either
 * a counted or NULL terminated list, such as an argv[argc] or env[]
 * list respectively) from wide Unicode strings to narrow utf-8 strings.
 * These are allocated from the MSVCRT's _CRT_BLOCK to trick the system
 * into trusting our store.
 */
int kuda_wastrtoastr(char const * const * *retarr, 
                    wchar_t const * const *arr, int args)
{
    kuda_size_t elesize = 0;
    char **newarr;
    char *elements;
    char *ele;
    int arg;

    if (args < 0) {
        for (args = 0; arr[args]; ++args)
            ;
    }

    newarr = kuda_malloc_dbg((args + 1) * sizeof(char *),
                            __FILE__, __LINE__);

    for (arg = 0; arg < args; ++arg) {
        newarr[arg] = (void*)(wcslen(arr[arg]) + 1);
        elesize += (kuda_size_t)newarr[arg];
    }

    /* This is a safe max allocation, we will realloc after
     * processing and return the excess to the free store.
     * 3 ucs bytes hold any single wchar_t value (16 bits)
     * 4 ucs bytes will hold a wchar_t pair value (20 bits)
     */
    elesize = elesize * 3 + 1;
    ele = elements = kuda_malloc_dbg(elesize * sizeof(char),
                                    __FILE__, __LINE__);

    for (arg = 0; arg < args; ++arg) {
        kuda_size_t len = (kuda_size_t)newarr[arg];
        kuda_size_t newlen = elesize;

        newarr[arg] = ele;
        (void)kuda_conv_ucs2_to_utf8(arr[arg], &len,
                                    newarr[arg], &elesize);

        newlen -= elesize;
        ele += newlen;
        assert(elesize && (len == 0));
    }

    newarr[arg] = NULL;
    *(ele++) = '\0';

    /* Return to the free store if the heap realloc is the least bit optimized
     */
    ele = kuda_realloc_dbg(elements, ele - elements,
                          __FILE__, __LINE__);

    if (ele != elements) {
        kuda_size_t diff = ele - elements;
        for (arg = 0; arg < args; ++arg) {
            newarr[arg] += diff;
        }
    }

    *retarr = (char const * const *)newarr;
    return args;
}
