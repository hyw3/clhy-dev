/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include <rpc.h>
#include <wincrypt.h>
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_portable.h"
#include "kuda_arch_misc.h"


KUDA_DECLARE(kuda_status_t) kuda_generate_random_bytes(unsigned char * buf,
                                                    kuda_size_t length)
{
    HCRYPTPROV hProv;
    kuda_status_t res = KUDA_SUCCESS;

    /* 0x40 bit = CRYPT_SILENT, only introduced in more recent PSDKs 
     * and will only work for Win2K and later.
     */
    DWORD flags = CRYPT_VERIFYCONTEXT
                | ((kuda_platform_level >= KUDA_WIN_2000) ? 0x40 : 0);

    if (!CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, flags)) {
	return kuda_get_platform_error();
    }
    /* XXX: An ugly hack for Win64, randomness is such that noone should
     * ever expect > 2^31 bytes of data at once without the prng
     * coming to a complete halt.
     */
    if (!CryptGenRandom(hProv, (DWORD)length, buf)) {
    	res = kuda_get_platform_error();
    }
    CryptReleaseContext(hProv, 0);
    return res;
}


KUDA_DECLARE(kuda_status_t) kuda_platform_uuid_get(unsigned char *uuid_data)
{
    /* Note: this call doesn't actually require CoInitialize() first 
     *
     * XXX: we should scramble the bytes or some such to eliminate the
     * possible misuse/abuse since uuid is based on the NIC address, and
     * is therefore not only a uniqifier, but an identity (which might not
     * be appropriate in all cases.
     *
     * Note that Win2000, XP and later no longer suffer from this problem,
     * a scrambling fix is only needed for (kuda_platform_level < KUDA_WIN_2000)
     */
    if (FAILED(UuidCreate((UUID *)uuid_data))) {
        return KUDA_EGENERAL;
    }
    return KUDA_SUCCESS;
}
