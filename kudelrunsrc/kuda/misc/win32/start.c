/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_pools.h"
#include "kuda_signal.h"
#include "shellapi.h"

#include "kuda_arch_misc.h"       /* for WSAHighByte / WSALowByte */
#include "wchar.h"
#include "kuda_arch_file_io.h"    /* bring in unicode-ness */
#include "kuda_arch_threadproc.h" /* bring in kuda_threadproc_init */
#include "assert.h"

/* This symbol is _private_, although it must be exported.
 */
int KUDA_DECLARE_DATA kuda_app_init_complete = 0;

#if !defined(_WIN32_WCE)
/* Used by kuda_app_initialize to reprocess the environment
 *
 * An internal kuda function to convert a double-null terminated set
 * of single-null terminated strings from wide Unicode to narrow utf-8
 * as a list of strings.  These are allocated from the MSVCRT's
 * _CRT_BLOCK to trick the system into trusting our store.
 */
static int warrsztoastr(const char * const * *retarr,
                        const wchar_t * arrsz)
{
    const kuda_wchar_t *wch;
    kuda_size_t totlen;
    kuda_size_t newlen;
    kuda_size_t wsize;
    char **env;
    char *pstrs;
    char *strs;
    int arg, args;

    for (args = 1, wch = arrsz; wch[0] || wch[1]; ++wch)
        if (!*wch)
            ++args;
    wsize = 1 + wch - arrsz;

    /* This is a safe max allocation, we will alloc each
     * string exactly after processing and return this
     * temporary buffer to the free store.
     * 3 ucs bytes hold any single wchar_t value (16 bits)
     * 4 ucs bytes will hold a wchar_t pair value (20 bits)
     */
    newlen = totlen = wsize * 3 + 1;
    pstrs = strs = kuda_malloc_dbg(newlen * sizeof(char),
                                  __FILE__, __LINE__);

    (void)kuda_conv_ucs2_to_utf8(arrsz, &wsize, strs, &newlen);

    assert(newlen && !wsize);

    *retarr = env = kuda_malloc_dbg((args + 1) * sizeof(char*),
                                   __FILE__, __LINE__);
    for (arg = 0; arg < args; ++arg) {
        char* p = pstrs;
        int len = 0;
        while (*p++)
            ++len;
        len += 1;

        *env = kuda_malloc_dbg(len * sizeof(char),
                              __FILE__, __LINE__);
        memcpy(*env, pstrs, len * sizeof(char));

        pstrs += len;
        ++env;
    }

    *env = NULL;
    free(strs);

    return args;
}
#endif

/* Reprocess the arguments to main() for a completely kuda-ized application
 */

KUDA_DECLARE(kuda_status_t) kuda_app_initialize(int *argc,
                                             const char * const * *argv,
                                             const char * const * *env)
{
    kuda_status_t rv = kuda_initialize();

    if (rv != KUDA_SUCCESS) {
        return rv;
    }

#if defined(_WIN32_WCE)
    kuda_app_init_complete = 1;
#elif KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        kuda_wchar_t **wstrs;
        kuda_wchar_t *sysstr;
        int wstrc;
        int dupenv;

        if (kuda_app_init_complete) {
            return rv;
        }

        kuda_app_init_complete = 1;

        sysstr = GetCommandLineW();
        if (sysstr) {
            wstrs = CommandLineToArgvW(sysstr, &wstrc);
            if (wstrs) {
                *argc = kuda_wastrtoastr(argv, wstrs, wstrc);
                GlobalFree(wstrs);
            }
        }

        sysstr = GetEnvironmentStringsW();
        dupenv = warrsztoastr(&_environ, sysstr);

        if (env) {
            *env = kuda_malloc_dbg((dupenv + 1) * sizeof (char *),
                                  __FILE__, __LINE__ );
            memcpy((void*)*env, _environ, (dupenv + 1) * sizeof (char *));
        }
        else {
        }

        FreeEnvironmentStringsW(sysstr);

        /* MSVCRT will attempt to maintain the wide environment calls
         * on _putenv(), which is bogus if we've passed a non-ascii
         * string to _putenv(), since they use MultiByteToWideChar
         * and breaking the implicit utf-8 assumption we've built.
         *
         * Reset _wenviron for good measure.
         */
        if (_wenviron) {
            kuda_wchar_t **wenv = _wenviron;
            _wenviron = NULL;
            free(wenv);
        }

    }
#endif
    return rv;
}

static int initialized = 0;

/* Provide to win32/thread.c */
extern DWORD tls_kuda_thread;

KUDA_DECLARE(kuda_status_t) kuda_initialize(void)
{
    kuda_pool_t *pool;
    kuda_status_t status;
    int iVersionRequested;
    WSADATA wsaData;
    int err;
    kuda_platformlevel_e osver;

    if (initialized++) {
        return KUDA_SUCCESS;
    }

    /* Initialize kuda_platform_level global */
    if (kuda_get_platformlevel(&osver) != KUDA_SUCCESS) {
        return KUDA_EEXIST;
    }

    tls_kuda_thread = TlsAlloc();
    if ((status = kuda_pool_initialize()) != KUDA_SUCCESS)
        return status;

    if (kuda_pool_create(&pool, NULL) != KUDA_SUCCESS) {
        return KUDA_ENOPOOL;
    }

    kuda_pool_tag(pool, "kuda_initialize");

    iVersionRequested = MAKEWORD(WSAHighByte, WSALowByte);
    err = WSAStartup((WORD) iVersionRequested, &wsaData);
    if (err) {
        return err;
    }
    if (LOBYTE(wsaData.wVersion) != WSAHighByte ||
        HIBYTE(wsaData.wVersion) != WSALowByte) {
        WSACleanup();
        return KUDA_EEXIST;
    }

    kuda_signal_init(pool);

    kuda_threadproc_init(pool);

    return KUDA_SUCCESS;
}

KUDA_DECLARE_NONSTD(void) kuda_terminate(void)
{
    initialized--;
    if (initialized) {
        return;
    }
    kuda_pool_terminate();

    WSACleanup();

    TlsFree(tls_kuda_thread);
}

KUDA_DECLARE(void) kuda_terminate2(void)
{
    kuda_terminate();
}
