/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"
#include "kuda.h"
#include "kuda_arch_misc.h"
#include "kuda_arch_utf8.h"
#include "kuda_env.h"
#include "kuda_errno.h"
#include "kuda_pools.h"
#include "kuda_strings.h"

#if KUDA_HAS_UNICODE_FS && !defined(_WIN32_WCE)
static kuda_status_t widen_envvar_name (kuda_wchar_t *buffer,
                                       kuda_size_t bufflen,
                                       const char *envvar)
{
    kuda_size_t inchars;
    kuda_status_t status;

    inchars = strlen(envvar) + 1;
    status = kuda_conv_utf8_to_ucs2(envvar, &inchars, buffer, &bufflen);
    if (status == KUDA_INCOMPLETE)
        status = KUDA_ENAMETOOLONG;

    return status;
}
#endif


KUDA_DECLARE(kuda_status_t) kuda_env_get(char **value,
                                      const char *envvar,
                                      kuda_pool_t *pool)
{
#if defined(_WIN32_WCE)
    return KUDA_ENOTIMPL;
#else
    char *val = NULL;
    DWORD size;

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        kuda_wchar_t wenvvar[KUDA_PATH_MAX];
        kuda_size_t inchars, outchars;
        kuda_wchar_t *wvalue, dummy;
        kuda_status_t status;

        status = widen_envvar_name(wenvvar, KUDA_PATH_MAX, envvar);
        if (status)
            return status;

        SetLastError(0);
        size = GetEnvironmentVariableW(wenvvar, &dummy, 0);
        if (GetLastError() == ERROR_ENVVAR_NOT_FOUND)
            /* The environment variable doesn't exist. */
            return KUDA_ENOENT;

        if (size == 0) {
            /* The environment value exists, but is zero-length. */
            *value = kuda_pstrdup(pool, "");
            return KUDA_SUCCESS;
        }

        wvalue = kuda_palloc(pool, size * sizeof(*wvalue));
        size = GetEnvironmentVariableW(wenvvar, wvalue, size);

        inchars = wcslen(wvalue) + 1;
        outchars = 3 * inchars; /* Enough for any UTF-8 representation */
        val = kuda_palloc(pool, outchars);
        status = kuda_conv_ucs2_to_utf8(wvalue, &inchars, val, &outchars);
        if (status)
            return status;
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        char dummy;

        SetLastError(0);
        size = GetEnvironmentVariableA(envvar, &dummy, 0);
        if (GetLastError() == ERROR_ENVVAR_NOT_FOUND)
            /* The environment variable doesn't exist. */
            return KUDA_ENOENT;

        if (size == 0) {
            /* The environment value exists, but is zero-length. */
            *value = kuda_pstrdup(pool, "");
            return KUDA_SUCCESS;
        }

        val = kuda_palloc(pool, size);
        size = GetEnvironmentVariableA(envvar, val, size);
        if (size == 0)
            /* Mid-air collision?. Somebody must've changed the env. var. */
            return KUDA_INCOMPLETE;
    }
#endif

    *value = val;
    return KUDA_SUCCESS;
#endif
}


KUDA_DECLARE(kuda_status_t) kuda_env_set(const char *envvar,
                                      const char *value,
                                      kuda_pool_t *pool)
{
#if defined(_WIN32_WCE)
    return KUDA_ENOTIMPL;
#else
#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        kuda_wchar_t wenvvar[KUDA_PATH_MAX];
        kuda_wchar_t *wvalue;
        kuda_size_t inchars, outchars;
        kuda_status_t status;

        status = widen_envvar_name(wenvvar, KUDA_PATH_MAX, envvar);
        if (status)
            return status;

        outchars = inchars = strlen(value) + 1;
        wvalue = kuda_palloc(pool, outchars * sizeof(*wvalue));
        status = kuda_conv_utf8_to_ucs2(value, &inchars, wvalue, &outchars);
        if (status)
            return status;

        if (!SetEnvironmentVariableW(wenvvar, wvalue))
            return kuda_get_platform_error();
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        if (!SetEnvironmentVariableA(envvar, value))
            return kuda_get_platform_error();
    }
#endif

    return KUDA_SUCCESS;
#endif
}


KUDA_DECLARE(kuda_status_t) kuda_env_delete(const char *envvar, kuda_pool_t *pool)
{
#if defined(_WIN32_WCE)
    return KUDA_ENOTIMPL;
#else
#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        kuda_wchar_t wenvvar[KUDA_PATH_MAX];
        kuda_status_t status;

        status = widen_envvar_name(wenvvar, KUDA_PATH_MAX, envvar);
        if (status)
            return status;

        if (!SetEnvironmentVariableW(wenvvar, NULL))
            return kuda_get_platform_error();
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        if (!SetEnvironmentVariableA(envvar, NULL))
            return kuda_get_platform_error();
    }
#endif

    return KUDA_SUCCESS;
#endif
}
