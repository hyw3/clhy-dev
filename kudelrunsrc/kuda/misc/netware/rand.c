/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define KUDA_WANT_MEMFUNC
#include "kuda_want.h"
#include "kuda_general.h"
#include "kuda_private.h"

#if KUDA_HAS_RANDOM

#include <nks/plat.h>

static int NXSeedRandomInternal( size_t width, void *seed )
{
    static int init = 0;
    int                        *s = (int *) seed;
    union { int x; char y[4]; } u;

    if (!init) {
        srand(NXGetSystemTick());
        init = 1;
    }
 
    if (width > 3)
    {
        do
        {
            *s++ = rand();
        }
        while ((width -= 4) > 3);
    }
 
    if (width > 0)
    {
        char *p = (char *) s;

        u.x = rand();
 
        while (width > 0)
           *p++ = u.y[width--];
    }
 
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_generate_random_bytes(unsigned char *buf, 
                                                    kuda_size_t length)
{
    if (NXSeedRandom(length, buf) != 0) {
        return NXSeedRandomInternal (length, buf);
    }
    return KUDA_SUCCESS;
}



#endif /* KUDA_HAS_RANDOM */
