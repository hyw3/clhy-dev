/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_portable.h"

/* static struct utsname sysinfo; */

/* XXX This needs to be fixed to produce the correct system language */

KUDA_DECLARE(const char*) kuda_platform_default_encoding (kuda_pool_t *pool)
{
    return kuda_pstrdup(pool, "CP1252");
}


KUDA_DECLARE(const char*) kuda_platform_locale_encoding (kuda_pool_t *pool)
{
    return kuda_platform_default_encoding(pool);
}
