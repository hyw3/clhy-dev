/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_POLL_H
#define KUDA_POLL_H
/**
 * @file kuda_poll.h
 * @brief KUDA Poll interface
 */
#include "kuda.h"
#include "kuda_pools.h"
#include "kuda_errno.h"
#include "kuda_inherit.h" 
#include "kuda_file_io.h" 
#include "kuda_network_io.h" 

#if KUDA_HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_poll Poll Routines
 * @ingroup KUDA 
 * @{
 */

/**
 * @defgroup pollopts Poll options
 * @ingroup kuda_poll
 * @{
 */
#define KUDA_POLLIN    0x001     /**< Can read without blocking */
#define KUDA_POLLPRI   0x002     /**< Priority data available */
#define KUDA_POLLOUT   0x004     /**< Can write without blocking */
#define KUDA_POLLERR   0x010     /**< Pending error */
#define KUDA_POLLHUP   0x020     /**< Hangup occurred */
#define KUDA_POLLNVAL  0x040     /**< Descriptor invalid */
/** @} */

/**
 * @defgroup pollflags Pollset Flags
 * @ingroup kuda_poll
 * @{
 */
#define KUDA_POLLSET_THREADSAFE 0x001 /**< Adding or removing a descriptor is
                                      * thread-safe
                                      */
#define KUDA_POLLSET_NOCOPY     0x002 /**< Descriptors passed to kuda_pollset_add()
                                      * are not copied
                                      */
#define KUDA_POLLSET_WAKEABLE   0x004 /**< Poll operations are interruptable by
                                      * kuda_pollset_wakeup() or kuda_pollcb_wakeup()
                                      */
#define KUDA_POLLSET_NODEFAULT  0x010 /**< Do not try to use the default method if
                                      * the specified non-default method cannot be
                                      * used
                                      */
/** @} */

/**
 * Pollset Methods
 */
typedef enum {
    KUDA_POLLSET_DEFAULT,        /**< Platform default poll method */
    KUDA_POLLSET_SELECT,         /**< Poll uses select method */
    KUDA_POLLSET_KQUEUE,         /**< Poll uses kqueue method */
    KUDA_POLLSET_PORT,           /**< Poll uses Solaris event port method */
    KUDA_POLLSET_EPOLL,          /**< Poll uses epoll method */
    KUDA_POLLSET_POLL,           /**< Poll uses poll method */
    KUDA_POLLSET_AIO_MSGQ        /**< Poll uses z/PLATFORM asio method */
} kuda_pollset_method_e;

/** Used in kuda_pollfd_t to determine what the kuda_descriptor is */
typedef enum { 
    KUDA_NO_DESC,                /**< nothing here */
    KUDA_POLL_SOCKET,            /**< descriptor refers to a socket */
    KUDA_POLL_FILE,              /**< descriptor refers to a file */
    KUDA_POLL_LASTDESC           /**< @deprecated descriptor is the last one in the list */
} kuda_datatype_e ;

/** Union of either an KUDA file or socket. */
typedef union {
    kuda_file_t *f;              /**< file */
    kuda_socket_t *s;            /**< socket */
} kuda_descriptor;

/** @see kuda_pollfd_t */
typedef struct kuda_pollfd_t kuda_pollfd_t;

/** Poll descriptor set. */
struct kuda_pollfd_t {
    kuda_pool_t *p;              /**< associated pool */
    kuda_datatype_e desc_type;   /**< descriptor type */
    kuda_int16_t reqevents;      /**< requested events */
    kuda_int16_t rtnevents;      /**< returned events */
    kuda_descriptor desc;        /**< @see kuda_descriptor */
    void *client_data;          /**< allows app to associate context */
};


/* General-purpose poll API for arbitrarily large numbers of
 * file descriptors
 */

/** Opaque structure used for pollset API */
typedef struct kuda_pollset_t kuda_pollset_t;

/**
 * Set up a pollset object
 * @param pollset  The pointer in which to return the newly created object 
 * @param size The maximum number of descriptors that this pollset can hold
 * @param p The pool from which to allocate the pollset
 * @param flags Optional flags to modify the operation of the pollset.
 *
 * @remark If flags contains KUDA_POLLSET_THREADSAFE, then a pollset is
 *         created on which it is safe to make concurrent calls to
 *         kuda_pollset_add(), kuda_pollset_remove() and kuda_pollset_poll()
 *         from separate threads.  This feature is only supported on some
 *         platforms; the kuda_pollset_create() call will fail with
 *         KUDA_ENOTIMPL on platforms where it is not supported.
 * @remark If flags contains KUDA_POLLSET_WAKEABLE, then a pollset is
 *         created with an additional internal pipe object used for the
 *         kuda_pollset_wakeup() call. The actual size of pollset is
 *         in that case @a size + 1. This feature is only supported on some
 *         platforms; the kuda_pollset_create() call will fail with
 *         KUDA_ENOTIMPL on platforms where it is not supported.
 * @remark If flags contains KUDA_POLLSET_NOCOPY, then the kuda_pollfd_t
 *         structures passed to kuda_pollset_add() are not copied and
 *         must have a lifetime at least as long as the pollset.
 * @remark Some poll methods (including KUDA_POLLSET_KQUEUE,
 *         KUDA_POLLSET_PORT, and KUDA_POLLSET_EPOLL) do not have a
 *         fixed limit on the size of the pollset. For these methods,
 *         the size parameter controls the maximum number of
 *         descriptors that will be returned by a single call to
 *         kuda_pollset_poll().
 */
KUDA_DECLARE(kuda_status_t) kuda_pollset_create(kuda_pollset_t **pollset,
                                             kuda_uint32_t size,
                                             kuda_pool_t *p,
                                             kuda_uint32_t flags);

/**
 * Set up a pollset object
 * @param pollset  The pointer in which to return the newly created object 
 * @param size The maximum number of descriptors that this pollset can hold
 * @param p The pool from which to allocate the pollset
 * @param flags Optional flags to modify the operation of the pollset.
 * @param method Poll method to use. See #kuda_pollset_method_e.  If this
 *         method cannot be used, the default method will be used unless the
 *         KUDA_POLLSET_NODEFAULT flag has been specified.
 *
 * @remark If flags contains KUDA_POLLSET_THREADSAFE, then a pollset is
 *         created on which it is safe to make concurrent calls to
 *         kuda_pollset_add(), kuda_pollset_remove() and kuda_pollset_poll()
 *         from separate threads.  This feature is only supported on some
 *         platforms; the kuda_pollset_create_ex() call will fail with
 *         KUDA_ENOTIMPL on platforms where it is not supported.
 * @remark If flags contains KUDA_POLLSET_WAKEABLE, then a pollset is
 *         created with additional internal pipe object used for the
 *         kuda_pollset_wakeup() call. The actual size of pollset is
 *         in that case size + 1. This feature is only supported on some
 *         platforms; the kuda_pollset_create_ex() call will fail with
 *         KUDA_ENOTIMPL on platforms where it is not supported.
 * @remark If flags contains KUDA_POLLSET_NOCOPY, then the kuda_pollfd_t
 *         structures passed to kuda_pollset_add() are not copied and
 *         must have a lifetime at least as long as the pollset.
 * @remark Some poll methods (including KUDA_POLLSET_KQUEUE,
 *         KUDA_POLLSET_PORT, and KUDA_POLLSET_EPOLL) do not have a
 *         fixed limit on the size of the pollset. For these methods,
 *         the size parameter controls the maximum number of
 *         descriptors that will be returned by a single call to
 *         kuda_pollset_poll().
 */
KUDA_DECLARE(kuda_status_t) kuda_pollset_create_ex(kuda_pollset_t **pollset,
                                                kuda_uint32_t size,
                                                kuda_pool_t *p,
                                                kuda_uint32_t flags,
                                                kuda_pollset_method_e method);

/**
 * Destroy a pollset object
 * @param pollset The pollset to destroy
 */
KUDA_DECLARE(kuda_status_t) kuda_pollset_destroy(kuda_pollset_t *pollset);

/**
 * Add a socket or file descriptor to a pollset
 * @param pollset The pollset to which to add the descriptor
 * @param descriptor The descriptor to add
 * @remark If you set client_data in the descriptor, that value
 *         will be returned in the client_data field whenever this
 *         descriptor is signalled in kuda_pollset_poll().
 * @remark If the pollset has been created with KUDA_POLLSET_THREADSAFE
 *         and thread T1 is blocked in a call to kuda_pollset_poll() for
 *         this same pollset that is being modified via kuda_pollset_add()
 *         in thread T2, the currently executing kuda_pollset_poll() call in
 *         T1 will either: (1) automatically include the newly added descriptor
 *         in the set of descriptors it is watching or (2) return immediately
 *         with KUDA_EINTR.  Option (1) is recommended, but option (2) is
 *         allowed for implementations where option (1) is impossible
 *         or impractical.
 * @remark If the pollset has been created with KUDA_POLLSET_NOCOPY, the 
 *         kuda_pollfd_t structure referenced by descriptor will not be copied
 *         and must have a lifetime at least as long as the pollset.
 * @remark Do not add the same socket or file descriptor to the same pollset
 *         multiple times, even if the requested events differ for the 
 *         different calls to kuda_pollset_add().  If the events of interest
 *         for a descriptor change, you must first remove the descriptor 
 *         from the pollset with kuda_pollset_remove(), then add it again 
 *         specifying all requested events.
 */
KUDA_DECLARE(kuda_status_t) kuda_pollset_add(kuda_pollset_t *pollset,
                                          const kuda_pollfd_t *descriptor);

/**
 * Remove a descriptor from a pollset
 * @param pollset The pollset from which to remove the descriptor
 * @param descriptor The descriptor to remove
 * @remark If the descriptor is not found, KUDA_NOTFOUND is returned.
 * @remark If the pollset has been created with KUDA_POLLSET_THREADSAFE
 *         and thread T1 is blocked in a call to kuda_pollset_poll() for
 *         this same pollset that is being modified via kuda_pollset_remove()
 *         in thread T2, the currently executing kuda_pollset_poll() call in
 *         T1 will either: (1) automatically exclude the newly added descriptor
 *         in the set of descriptors it is watching or (2) return immediately
 *         with KUDA_EINTR.  Option (1) is recommended, but option (2) is
 *         allowed for implementations where option (1) is impossible
 *         or impractical.
 * @remark kuda_pollset_remove() cannot be used to remove a subset of requested
 *         events for a descriptor.  The reqevents field in the kuda_pollfd_t
 *         parameter must contain the same value when removing as when adding.
 */
KUDA_DECLARE(kuda_status_t) kuda_pollset_remove(kuda_pollset_t *pollset,
                                             const kuda_pollfd_t *descriptor);

/**
 * Block for activity on the descriptor(s) in a pollset
 * @param pollset The pollset to use
 * @param timeout The amount of time in microseconds to wait.  This is a
 *                maximum, not a minimum.  If a descriptor is signalled, the
 *                function will return before this time.  If timeout is
 *                negative, the function will block until a descriptor is
 *                signalled or until kuda_pollset_wakeup() has been called.
 * @param num Number of signalled descriptors (output parameter)
 * @param descriptors Array of signalled descriptors (output parameter)
 * @remark KUDA_EINTR will be returned if the pollset has been created with
 *         KUDA_POLLSET_WAKEABLE, kuda_pollset_wakeup() has been called while
 *         waiting for activity, and there were no signalled descriptors at the
 *         time of the wakeup call.
 * @remark Multiple signalled conditions for the same descriptor may be reported
 *         in one or more returned kuda_pollfd_t structures, depending on the
 *         implementation.
 */
KUDA_DECLARE(kuda_status_t) kuda_pollset_poll(kuda_pollset_t *pollset,
                                           kuda_interval_time_t timeout,
                                           kuda_int32_t *num,
                                           const kuda_pollfd_t **descriptors);

/**
 * Interrupt the blocked kuda_pollset_poll() call.
 * @param pollset The pollset to use
 * @remark If the pollset was not created with KUDA_POLLSET_WAKEABLE the
 *         return value is KUDA_EINIT.
 */
KUDA_DECLARE(kuda_status_t) kuda_pollset_wakeup(kuda_pollset_t *pollset);

/**
 * Poll the descriptors in the poll structure
 * @param kudaset The poll structure we will be using. 
 * @param numsock The number of descriptors we are polling
 * @param nsds The number of descriptors signalled (output parameter)
 * @param timeout The amount of time in microseconds to wait.  This is a
 *                maximum, not a minimum.  If a descriptor is signalled, the
 *                function will return before this time.  If timeout is
 *                negative, the function will block until a descriptor is
 *                signalled or until kuda_pollset_wakeup() has been called.
 * @remark The number of descriptors signalled is returned in the third argument. 
 *         This is a blocking call, and it will not return until either a 
 *         descriptor has been signalled or the timeout has expired. 
 * @remark The rtnevents field in the kuda_pollfd_t array will only be filled-
 *         in if the return value is KUDA_SUCCESS.
 */
KUDA_DECLARE(kuda_status_t) kuda_poll(kuda_pollfd_t *kudaset, kuda_int32_t numsock,
                                   kuda_int32_t *nsds, 
                                   kuda_interval_time_t timeout);

/**
 * Return a printable representation of the pollset method.
 * @param pollset The pollset to use
 */
KUDA_DECLARE(const char *) kuda_pollset_method_name(kuda_pollset_t *pollset);

/**
 * Return a printable representation of the default pollset method
 * (KUDA_POLLSET_DEFAULT).
 */
KUDA_DECLARE(const char *) kuda_poll_method_defname(void);

/** Opaque structure used for pollcb API */
typedef struct kuda_pollcb_t kuda_pollcb_t;

/**
 * Set up a pollcb object
 * @param pollcb  The pointer in which to return the newly created object 
 * @param size The maximum number of descriptors that a single _poll can return.
 * @param p The pool from which to allocate the pollcb
 * @param flags Optional flags to modify the operation of the pollcb.
 *
 * @remark If flags contains KUDA_POLLSET_WAKEABLE, then a pollcb is
 *         created with an additional internal pipe object used for the
 *         kuda_pollcb_wakeup() call. The actual size of pollcb is
 *         in that case @a size + 1.
 * @remark Pollcb is only supported on some platforms; the kuda_pollcb_create()
 *         call will fail with KUDA_ENOTIMPL on platforms where it is not supported.
 */
KUDA_DECLARE(kuda_status_t) kuda_pollcb_create(kuda_pollcb_t **pollcb,
                                            kuda_uint32_t size,
                                            kuda_pool_t *p,
                                            kuda_uint32_t flags);

/**
 * Set up a pollcb object
 * @param pollcb  The pointer in which to return the newly created object 
 * @param size The maximum number of descriptors that a single _poll can return.
 * @param p The pool from which to allocate the pollcb
 * @param flags Optional flags to modify the operation of the pollcb.
 * @param method Poll method to use. See #kuda_pollset_method_e.  If this
 *         method cannot be used, the default method will be used unless the
 *         KUDA_POLLSET_NODEFAULT flag has been specified.
 *
 * @remark If flags contains KUDA_POLLSET_WAKEABLE, then a pollcb is
 *         created with an additional internal pipe object used for the
 *         kuda_pollcb_wakeup() call. The actual size of pollcb is
 *         in that case @a size + 1.
 * @remark Pollcb is only supported on some platforms; the kuda_pollcb_create_ex()
 *         call will fail with KUDA_ENOTIMPL on platforms where it is not supported.
 */
KUDA_DECLARE(kuda_status_t) kuda_pollcb_create_ex(kuda_pollcb_t **pollcb,
                                               kuda_uint32_t size,
                                               kuda_pool_t *p,
                                               kuda_uint32_t flags,
                                               kuda_pollset_method_e method);

/**
 * Add a socket or file descriptor to a pollcb
 * @param pollcb The pollcb to which to add the descriptor
 * @param descriptor The descriptor to add
 * @remark If you set client_data in the descriptor, that value will be
 *         returned in the client_data field whenever this descriptor is
 *         signalled in kuda_pollcb_poll().
 * @remark Unlike the kuda_pollset API, the descriptor is not copied, and users 
 *         must retain the memory used by descriptor, as the same pointer will
 *         be returned to them from kuda_pollcb_poll.
 * @remark Do not add the same socket or file descriptor to the same pollcb
 *         multiple times, even if the requested events differ for the 
 *         different calls to kuda_pollcb_add().  If the events of interest
 *         for a descriptor change, you must first remove the descriptor 
 *         from the pollcb with kuda_pollcb_remove(), then add it again 
 *         specifying all requested events.
 */
KUDA_DECLARE(kuda_status_t) kuda_pollcb_add(kuda_pollcb_t *pollcb,
                                         kuda_pollfd_t *descriptor);
/**
 * Remove a descriptor from a pollcb
 * @param pollcb The pollcb from which to remove the descriptor
 * @param descriptor The descriptor to remove
 * @remark If the descriptor is not found, KUDA_NOTFOUND is returned.
 * @remark kuda_pollcb_remove() cannot be used to remove a subset of requested
 *         events for a descriptor.  The reqevents field in the kuda_pollfd_t
 *         parameter must contain the same value when removing as when adding.
 */
KUDA_DECLARE(kuda_status_t) kuda_pollcb_remove(kuda_pollcb_t *pollcb,
                                            kuda_pollfd_t *descriptor);

/**
 * Function prototype for pollcb handlers 
 * @param baton Opaque baton passed into kuda_pollcb_poll()
 * @param descriptor Contains the notification for an active descriptor. 
 *                   The @a rtnevents member describes which events were triggered
 *                   for this descriptor.
 * @remark If the pollcb handler does not return KUDA_SUCCESS, the kuda_pollcb_poll()
 *         call returns with the handler's return value.
 */
typedef kuda_status_t (*kuda_pollcb_cb_t)(void *baton, kuda_pollfd_t *descriptor);

/**
 * Block for activity on the descriptor(s) in a pollcb
 * @param pollcb The pollcb to use
 * @param timeout The amount of time in microseconds to wait.  This is a
 *                maximum, not a minimum.  If a descriptor is signalled, the
 *                function will return before this time.  If timeout is
 *                negative, the function will block until a descriptor is
 *                signalled or until kuda_pollcb_wakeup() has been called.
 * @param func Callback function to call for each active descriptor.
 * @param baton Opaque baton passed to the callback function.
 * @remark Multiple signalled conditions for the same descriptor may be reported
 *         in one or more calls to the callback function, depending on the
 *         implementation.
 * @remark KUDA_EINTR will be returned if the pollset has been created with
 *         KUDA_POLLSET_WAKEABLE and kuda_pollcb_wakeup() has been called while
 *         waiting for activity.
 */
KUDA_DECLARE(kuda_status_t) kuda_pollcb_poll(kuda_pollcb_t *pollcb,
                                          kuda_interval_time_t timeout,
                                          kuda_pollcb_cb_t func,
                                          void *baton);

/**
 * Interrupt the blocked kuda_pollcb_poll() call.
 * @param pollcb The pollcb to use
 * @remark If the pollcb was not created with KUDA_POLLSET_WAKEABLE the
 *         return value is KUDA_EINIT.
 */
KUDA_DECLARE(kuda_status_t) kuda_pollcb_wakeup(kuda_pollcb_t *pollcb);

/**
 * Return a printable representation of the pollcb method.
 * @param pollcb The pollcb to use
 */
KUDA_DECLARE(const char *) kuda_pollcb_method_name(kuda_pollcb_t *pollcb);

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_POLL_H */

