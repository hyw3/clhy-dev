/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef KUDA_H
#define KUDA_H

/* GENERATED FILE WARNING!  DO NOT EDIT kuda.h
 *
 * You must modify kuda.h.in instead.
 *
 * And please, make an effort to stub kuda.hw and kuda.hnw in the process.
 */

/**
 * @file kuda.h
 * @brief KUDA Platform Definitions
 * @remark This is a generated header generated from include/kuda.h.in by
 * ./configure, or copied from include/kuda.hw or include/kuda.hnw 
 * for Win32 or Netware by those build environments, respectively.
 */

/**
 * @defgroup KUDA cLHy Portability Runtime library
 * @{
 */
/**
 * @defgroup kuda_platform Platform Definitions
 * @{
 * @warning
 * <strong><em>The actual values of macros and typedefs on this page<br>
 * are platform specific and should NOT be relied upon!</em></strong>
 */

/* So that we can use inline on some critical functions, and use
 * GNUC attributes (such as to get -Wall warnings for printf-like
 * functions).  Only do this in gcc 2.7 or later ... it may work
 * on earlier stuff, but why chance it.
 *
 * We've since discovered that the gcc shipped with NeXT systems
 * as "cc" is completely broken.  It claims to be __GNUC__ and so
 * on, but it doesn't implement half of the things that __GNUC__
 * means.  In particular it's missing inline and the __attribute__
 * stuff.  So we hack around it.  PR#1613. -djg
 */
#if !defined(__GNUC__) || __GNUC__ < 2 || \
    (__GNUC__ == 2 && __GNUC_MINOR__ < 7) ||\
    defined(NEXT)
#ifndef __attribute__
#define __attribute__(__x)
#endif
#define KUDA_INLINE
#define KUDA_HAS_INLINE           0
#else
#define KUDA_INLINE __inline__
#define KUDA_HAS_INLINE           1
#endif

#define KUDA_HAVE_ARPA_INET_H     1
#define KUDA_HAVE_CONIO_H         0
#define KUDA_HAVE_CRYPT_H         1
#define KUDA_HAVE_CTYPE_H         1
#define KUDA_HAVE_DIRENT_H        1
#define KUDA_HAVE_ERRNO_H         1
#define KUDA_HAVE_FCNTL_H         1
#define KUDA_HAVE_IO_H            0
#define KUDA_HAVE_LIMITS_H        1
#define KUDA_HAVE_NETDB_H         1
#define KUDA_HAVE_NETINET_IN_H    1
#define KUDA_HAVE_NETINET_SCTP_H  0
#define KUDA_HAVE_NETINET_SCTP_UIO_H 0
#define KUDA_HAVE_NETINET_TCP_H   1
#define KUDA_HAVE_PROCESS_H       0
#define KUDA_HAVE_PTHREAD_H       1
#define KUDA_HAVE_SEMAPHORE_H     1
#define KUDA_HAVE_SIGNAL_H        1
#define KUDA_HAVE_STDARG_H        1
#define KUDA_HAVE_STDINT_H        1
#define KUDA_HAVE_STDIO_H         1
#define KUDA_HAVE_STDLIB_H        1
#define KUDA_HAVE_STRING_H        1
#define KUDA_HAVE_STRINGS_H       1
#define KUDA_HAVE_INTTYPES_H      1
#define KUDA_HAVE_SYS_IOCTL_H     1
#define KUDA_HAVE_SYS_SENDFILE_H  1
#define KUDA_HAVE_SYS_SIGNAL_H    1
#define KUDA_HAVE_SYS_SOCKET_H    1
#define KUDA_HAVE_SYS_SOCKIO_H    0
#define KUDA_HAVE_SYS_SYSLIMITS_H 0
#define KUDA_HAVE_SYS_TIME_H      1
#define KUDA_HAVE_SYS_TYPES_H     1
#define KUDA_HAVE_SYS_UIO_H       1
#define KUDA_HAVE_SYS_UN_H        1
#define KUDA_HAVE_SYS_WAIT_H      1
#define KUDA_HAVE_TIME_H          1
#define KUDA_HAVE_UNISTD_H        1
#define KUDA_HAVE_WINDOWS_H       0
#define KUDA_HAVE_WINSOCK2_H      0

/** @} */
/** @} */

/* We don't include our conditional headers within the doxyblocks 
 * or the extern "C" namespace 
 */

#if KUDA_HAVE_WINDOWS_H && defined(WIN32)
/* If windows.h was already included, our preferences don't matter.
 * If not, include a restricted set of windows headers to our tastes.
 */
#ifndef _WINDOWS_

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef _WIN32_WINNT
/* Restrict the server to a subset of Windows XP header files by default
 */
#define _WIN32_WINNT 0x0501
#endif

#ifndef NOUSER
#define NOUSER
#endif
#ifndef NOMCX
#define NOMCX
#endif
#ifndef NOIME
#define NOIME
#endif

#include <windows.h>
/* 
 * Add a _very_few_ declarations missing from the restricted set of headers
 * (If this list becomes extensive, re-enable the required headers above!)
 * winsock headers were excluded by WIN32_LEAN_AND_MEAN, so include them now
 */
#define SW_HIDE             0
#ifndef _WIN32_WCE
#include <winsock2.h>
#include <ws2tcpip.h>
#include <mswsock.h>
#else
#include <winsock.h>
#endif

#endif /* ndef _WINDOWS_ */
#endif /* KUDA_HAVE_WINDOWS_H */

#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#if KUDA_HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif

#if KUDA_HAVE_STDINT_H
#ifdef __cplusplus
/* C99 7.18.4 requires that stdint.h only exposes INT64_C 
 * and UINT64_C for C++ implementations if this is defined: */
#ifndef __STDC_CONSTANT_MACROS
#define __STDC_CONSTANT_MACROS
#endif
/* C++ needs this too for PRI*NN formats: */
#ifndef __STDC_FORMAT_MACROS
#define __STDC_FORMAT_MACROS
#endif
#endif /* __cplusplus */
#include <stdint.h>
#endif

#if KUDA_HAVE_INTTYPES_H
#include <inttypes.h>
#endif

#if KUDA_HAVE_SYS_WAIT_H
#include <sys/wait.h>
#endif

#ifdef OS2
#define INCL_DOS
#define INCL_DOSERRORS
#include <os2.h>
#endif

/* header files for PATH_MAX, _POSIX_PATH_MAX */
#if KUDA_HAVE_LIMITS_H
#include <limits.h>
#else
#if KUDA_HAVE_SYS_SYSLIMITS_H
#include <sys/syslimits.h>
#endif
#endif

/* __APPLE__ is now the official pre-defined macro for macOS */
#ifdef __APPLE__
#undef DARWIN
#undef DARWIN_10
#define DARWIN
#define DARWIN_10
#endif /* __APPLE__ */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup kuda_platform
 * @ingroup KUDA 
 * @{
 */

#define KUDA_HAVE_SHMEM_MMAP_TMP     1
#define KUDA_HAVE_SHMEM_MMAP_SHM     1
#define KUDA_HAVE_SHMEM_MMAP_ZERO    1
#define KUDA_HAVE_SHMEM_SHMGET_ANON  1
#define KUDA_HAVE_SHMEM_SHMGET       1
#define KUDA_HAVE_SHMEM_MMAP_ANON    1
#define KUDA_HAVE_SHMEM_BEOS         0

#define KUDA_USE_SHMEM_MMAP_TMP     0
#define KUDA_USE_SHMEM_MMAP_SHM     0
#define KUDA_USE_SHMEM_MMAP_ZERO    0
#define KUDA_USE_SHMEM_SHMGET_ANON  0
#define KUDA_USE_SHMEM_SHMGET       1
#define KUDA_USE_SHMEM_MMAP_ANON    1
#define KUDA_USE_SHMEM_BEOS         0

#define KUDA_USE_FLOCK_SERIALIZE           0 
#define KUDA_USE_SYSVSEM_SERIALIZE         0
#define KUDA_USE_POSIXSEM_SERIALIZE        0
#define KUDA_USE_FCNTL_SERIALIZE           0
#define KUDA_USE_PROC_PTHREAD_SERIALIZE    1 
#define KUDA_USE_PTHREAD_SERIALIZE         1 

#define KUDA_HAS_FLOCK_SERIALIZE           1
#define KUDA_HAS_SYSVSEM_SERIALIZE         1
#define KUDA_HAS_POSIXSEM_SERIALIZE        1
#define KUDA_HAS_FCNTL_SERIALIZE           1
#define KUDA_HAS_PROC_PTHREAD_SERIALIZE    1

#define KUDA_PROCESS_LOCK_IS_GLOBAL        0

#define KUDA_HAVE_CORKABLE_TCP   1 
#define KUDA_HAVE_GETRLIMIT      1
#define KUDA_HAVE_IN_ADDR        1
#define KUDA_HAVE_INET_ADDR      1
#define KUDA_HAVE_INET_NETWORK   1
#define KUDA_HAVE_IPV6           1
#define KUDA_HAVE_SOCKADDR_UN    1
#define KUDA_HAVE_MEMMOVE        1
#define KUDA_HAVE_SETRLIMIT      1
#define KUDA_HAVE_SIGACTION      1
#define KUDA_HAVE_SIGSUSPEND     1
#define KUDA_HAVE_SIGWAIT        1
#define KUDA_HAVE_SA_STORAGE     1
#define KUDA_HAVE_STRCASECMP     1
#define KUDA_HAVE_STRDUP         1
#define KUDA_HAVE_STRICMP        0
#define KUDA_HAVE_STRNCASECMP    1
#define KUDA_HAVE_STRNICMP       0
#define KUDA_HAVE_STRSTR         1
#define KUDA_HAVE_MEMCHR         1
#define KUDA_HAVE_STRUCT_RLIMIT  1
#define KUDA_HAVE_UNION_SEMUN    0
#define KUDA_HAVE_SCTP           0
#define KUDA_HAVE_IOVEC          1

/*  KUDA Feature Macros */
#define KUDA_HAS_SHARED_MEMORY     1
#define KUDA_HAS_THREADS           1
#define KUDA_HAS_SENDFILE          1
#define KUDA_HAS_MMAP              1
#define KUDA_HAS_FORK              1
#define KUDA_HAS_RANDOM            1
#define KUDA_HAS_OTHER_CHILD       1
#define KUDA_HAS_DSO               1
#define KUDA_HAS_SO_ACCEPTFILTER   0
#define KUDA_HAS_UNICODE_FS        0
#define KUDA_HAS_PROC_INVOKED      0
#define KUDA_HAS_USER              1
#define KUDA_HAS_LARGE_FILES       0
#define KUDA_HAS_XTHREAD_FILES     0
#define KUDA_HAS_PLATFORM_UUID           1
#define KUDA_HAS_TIMEDLOCKS        1

#define KUDA_PROCATTR_USER_SET_REQUIRES_PASSWORD 0

/* KUDA sets KUDA_FILES_AS_SOCKETS to 1 on systems where it is possible
 * to poll on files/pipes.
 */
#define KUDA_FILES_AS_SOCKETS      1

/* This macro indicates whether or not EBCDIC is the native character set.
 */
#define KUDA_CHARSET_EBCDIC        0

/* If we have a TCP implementation that can be "corked", what flag
 * do we use?
 */
#define KUDA_TCP_NOPUSH_FLAG       TCP_CORK

/* Is the TCP_NODELAY socket option inherited from listening sockets?
*/
#define KUDA_TCP_NODELAY_INHERITED 1

/* Is the O_NONBLOCK flag inherited from listening sockets?
*/
#define KUDA_O_NONBLOCK_INHERITED 0

/* Typedefs that KUDA needs. */

typedef  unsigned char           kuda_byte_t;

typedef  short           kuda_int16_t;
typedef  unsigned short  kuda_uint16_t;

typedef  int             kuda_int32_t;
typedef  unsigned int    kuda_uint32_t;

#define KUDA_SIZEOF_VOIDP 8

/*
 * Darwin 10's default compiler (gcc42) builds for both 64 and
 * 32 bit architectures unless specifically told not to.
 * In those cases, we need to override types depending on how
 * we're being built at compile time.
 * NOTE: This is an ugly work-around for Darwin's
 * concept of universal binaries, a single package
 * (executable, lib, etc...) which contains both 32
 * and 64 bit versions. The issue is that if KUDA is
 * built universally, if something else is compiled
 * against it, some bit sizes will depend on whether
 * it is 32 or 64 bit. This is determined by the __LP64__
 * flag. Since we need to support both, we have to
 * handle PLATFORM X unqiuely.
 */
#ifdef DARWIN_10
#undef KUDA_SIZEOF_VOIDP
#undef KUDA_INT64_C
#undef KUDA_UINT64_C
#ifdef __LP64__
 typedef  long            kuda_int64_t;
 typedef  unsigned long   kuda_uint64_t;
 #define KUDA_SIZEOF_VOIDP     8
 #define KUDA_INT64_C(v)   (v ## L)
 #define KUDA_UINT64_C(v)  (v ## UL)
#else
 typedef  long long            kuda_int64_t;
 typedef  unsigned long long   kuda_uint64_t;
 #define KUDA_SIZEOF_VOIDP     4
 #define KUDA_INT64_C(v)   (v ## LL)
 #define KUDA_UINT64_C(v)  (v ## ULL)
#endif
#else
 typedef  int64_t           kuda_int64_t;
 typedef  uint64_t          kuda_uint64_t;

 /* Mechanisms to properly type numeric literals */
 #define KUDA_INT64_C(val) INT64_C(val)
 #define KUDA_UINT64_C(val) UINT64_C(val)
#endif

typedef  size_t          kuda_size_t;
typedef  ssize_t         kuda_ssize_t;
typedef  off_t           kuda_off_t;
typedef  socklen_t       kuda_socklen_t;
typedef  ino_t           kuda_ino_t;

#if KUDA_SIZEOF_VOIDP == 8
typedef  kuda_uint64_t            kuda_uintptr_t;
#else
typedef  kuda_uint32_t            kuda_uintptr_t;
#endif

/* Are we big endian? */
#define KUDA_IS_BIGENDIAN	0

#ifdef INT16_MIN
#define KUDA_INT16_MIN   INT16_MIN
#else
#define KUDA_INT16_MIN   (-0x7fff - 1)
#endif

#ifdef INT16_MAX
#define KUDA_INT16_MAX  INT16_MAX
#else
#define KUDA_INT16_MAX   (0x7fff)
#endif

#ifdef UINT16_MAX
#define KUDA_UINT16_MAX  UINT16_MAX
#else
#define KUDA_UINT16_MAX  (0xffff)
#endif

#ifdef INT32_MIN
#define KUDA_INT32_MIN   INT32_MIN
#else
#define KUDA_INT32_MIN   (-0x7fffffff - 1)
#endif

#ifdef INT32_MAX
#define KUDA_INT32_MAX  INT32_MAX
#else
#define KUDA_INT32_MAX  0x7fffffff
#endif

#ifdef UINT32_MAX
#define KUDA_UINT32_MAX  UINT32_MAX
#else
#define KUDA_UINT32_MAX  (0xffffffffU)
#endif

#ifdef INT64_MIN
#define KUDA_INT64_MIN   INT64_MIN
#else
#define KUDA_INT64_MIN   (KUDA_INT64_C(-0x7fffffffffffffff) - 1)
#endif

#ifdef INT64_MAX
#define KUDA_INT64_MAX   INT64_MAX
#else
#define KUDA_INT64_MAX   KUDA_INT64_C(0x7fffffffffffffff)
#endif

#ifdef UINT64_MAX
#define KUDA_UINT64_MAX  UINT64_MAX
#else
#define KUDA_UINT64_MAX  KUDA_UINT64_C(0xffffffffffffffff)
#endif

#define KUDA_SIZE_MAX    (~((kuda_size_t)0))


/* Definitions that KUDA programs need to work properly. */

/**
 * KUDA public API wrap for C++ compilers.
 */
#ifdef __cplusplus
#define KUDA_BEGIN_DECLS     extern "C" {
#define KUDA_END_DECLS       }
#else
#define KUDA_BEGIN_DECLS
#define KUDA_END_DECLS
#endif

/** 
 * Thread callbacks from KUDA functions must be declared with KUDA_THREAD_FUNC, 
 * so that they follow the platform's calling convention.
 * <PRE>
 *
 * void* KUDA_THREAD_FUNC my_thread_entry_fn(kuda_thread_t *thd, void *data);
 *
 * </PRE>
 */
#define KUDA_THREAD_FUNC       

#if defined(DOXYGEN) || !defined(WIN32)

/**
 * The public KUDA functions are declared with KUDA_DECLARE(), so they may
 * use the most appropriate calling convention.  Public KUDA functions with 
 * variable arguments must use KUDA_DECLARE_NONSTD().
 *
 * @remark Both the declaration and implementations must use the same macro.
 *
 * <PRE>
 * KUDA_DECLARE(rettype) kuda_func(args)
 * </PRE>
 * @see KUDA_DECLARE_NONSTD @see KUDA_DECLARE_DATA
 * @remark Note that when KUDA compiles the library itself, it passes the 
 * symbol -DKUDA_DECLARE_EXPORT to the compiler on some platforms (e.g. Win32) 
 * to export public symbols from the dynamic library build.\n
 * The user must define the KUDA_DECLARE_STATIC when compiling to target
 * the static KUDA library on some platforms (e.g. Win32.)  The public symbols 
 * are neither exported nor imported when KUDA_DECLARE_STATIC is defined.\n
 * By default, compiling an application and including the KUDA public
 * headers, without defining KUDA_DECLARE_STATIC, will prepare the code to be
 * linked to the dynamic library.
 */
#define KUDA_DECLARE(type)            type 

/**
 * The public KUDA functions using variable arguments are declared with 
 * KUDA_DECLARE_NONSTD(), as they must follow the C language calling convention.
 * @see KUDA_DECLARE @see KUDA_DECLARE_DATA
 * @remark Both the declaration and implementations must use the same macro.
 * <PRE>
 *
 * KUDA_DECLARE_NONSTD(rettype) kuda_func(args, ...);
 *
 * </PRE>
 */
#define KUDA_DECLARE_NONSTD(type)     type

/**
 * The public KUDA variables are declared with CLHY_CAPI_DECLARE_DATA.
 * This assures the appropriate indirection is invoked at compile time.
 * @see KUDA_DECLARE @see KUDA_DECLARE_NONSTD
 * @remark Note that the declaration and implementations use different forms,
 * but both must include the macro.
 * 
 * <PRE>
 *
 * extern KUDA_DECLARE_DATA type kuda_variable;\n
 * KUDA_DECLARE_DATA type kuda_variable = value;
 *
 * </PRE>
 */
#define KUDA_DECLARE_DATA

#elif defined(KUDA_DECLARE_STATIC)
#define KUDA_DECLARE(type)            type __stdcall
#define KUDA_DECLARE_NONSTD(type)     type __cdecl
#define KUDA_DECLARE_DATA
#elif defined(KUDA_DECLARE_EXPORT)
#define KUDA_DECLARE(type)            __declspec(dllexport) type __stdcall
#define KUDA_DECLARE_NONSTD(type)     __declspec(dllexport) type __cdecl
#define KUDA_DECLARE_DATA             __declspec(dllexport)
#else
#define KUDA_DECLARE(type)            __declspec(dllimport) type __stdcall
#define KUDA_DECLARE_NONSTD(type)     __declspec(dllimport) type __cdecl
#define KUDA_DECLARE_DATA             __declspec(dllimport)
#endif

/* Define KUDA_SSIZE_T_FMT.  
 * If ssize_t is an integer we define it to be "d",
 * if ssize_t is a long int we define it to be "ld",
 * if ssize_t is neither we declare an error here.
 * I looked for a better way to define this here, but couldn't find one, so
 * to find the logic for this definition search for "ssize_t_fmt" in
 * configure.in.
 */

#define KUDA_SSIZE_T_FMT "ld"

/* And KUDA_SIZE_T_FMT */
#define KUDA_SIZE_T_FMT "lu"

/* And KUDA_OFF_T_FMT */
#define KUDA_OFF_T_FMT "ld"

/* And KUDA_PID_T_FMT */
#define KUDA_PID_T_FMT "d"

/* And KUDA_INT64_T_FMT */
#define KUDA_INT64_T_FMT PRId64

/* And KUDA_UINT64_T_FMT */
#define KUDA_UINT64_T_FMT PRIu64

/* And KUDA_UINT64_T_HEX_FMT */
#define KUDA_UINT64_T_HEX_FMT PRIx64

/*
 * Ensure we work with universal binaries on Darwin
 */
#ifdef DARWIN_10

#undef KUDA_HAS_LARGE_FILES
#undef KUDA_SIZEOF_VOIDP
#undef KUDA_INT64_T_FMT
#undef KUDA_UINT64_T_FMT
#undef KUDA_UINT64_T_HEX_FMT

#ifdef __LP64__
 #define KUDA_HAS_LARGE_FILES  0
 #define KUDA_SIZEOF_VOIDP     8
 #define KUDA_INT64_T_FMT      "ld"
 #define KUDA_UINT64_T_FMT     "lu"
 #define KUDA_UINT64_T_HEX_FMT "lx"
#else
 #define KUDA_HAS_LARGE_FILES  1
 #define KUDA_SIZEOF_VOIDP     4
 #define KUDA_INT64_T_FMT      "lld"
 #define KUDA_UINT64_T_FMT     "llu"
 #define KUDA_UINT64_T_HEX_FMT "llx"
#endif

#undef KUDA_IS_BIGENDIAN
#ifdef __BIG_ENDIAN__
 #define KUDA_IS_BIGENDIAN	1
#else
 #define KUDA_IS_BIGENDIAN	0
#endif

#undef KUDA_OFF_T_FMT
#define KUDA_OFF_T_FMT "lld"

#endif /* DARWIN_10 */

/* Does the proc mutex lock threads too */
#define KUDA_PROC_MUTEX_IS_GLOBAL      0

/* Local machine definition for console and log output. */
#define KUDA_EOL_STR              "\n"

#if KUDA_HAVE_SYS_WAIT_H
#ifdef WEXITSTATUS
#define kuda_wait_t       int
#else
#define kuda_wait_t       union wait
#define WEXITSTATUS(status)    (int)((status).w_retcode)
#define WTERMSIG(status)       (int)((status).w_termsig)
#endif /* !WEXITSTATUS */
#elif defined(__MINGW32__)
typedef int kuda_wait_t;
#endif /* HAVE_SYS_WAIT_H */

#if defined(PATH_MAX)
#define KUDA_PATH_MAX       PATH_MAX
#elif defined(_POSIX_PATH_MAX)
#define KUDA_PATH_MAX       _POSIX_PATH_MAX
#else
#error no decision has been made on KUDA_PATH_MAX for your platform
#endif

#define KUDA_DSOPATH "LD_LIBRARY_PATH"

/** @} */

/* Definitions that only Win32 programs need to compile properly. */

/* XXX These simply don't belong here, perhaps in kuda_portable.h
 * based on some KUDA_HAVE_PID/GID/UID?
 */
#ifdef __MINGW32__
#ifndef __GNUC__
typedef  int         pid_t;
#endif
typedef  int         uid_t;
typedef  int         gid_t;
#endif

#ifdef __cplusplus
}
#endif

#endif /* KUDA_H */
