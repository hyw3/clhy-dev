/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_INHERIT_H
#define KUDA_INHERIT_H

/**
 * @file kuda_inherit.h 
 * @brief KUDA File Handle Inheritance Helpers
 * @remark This internal header includes internal declaration helpers 
 * for other headers to declare kuda_foo_inherit_[un]set functions.
 */

/**
 * Prototype for type-specific declarations of kuda_foo_inherit_set 
 * functions.  
 * @remark Doxygen unwraps this macro (via doxygen.conf) to provide 
 * actual help for each specific occurrence of kuda_foo_inherit_set.
 * @remark the linkage is specified for KUDA. It would be possible to expand
 *       the macros to support other linkages.
 */
#define KUDA_DECLARE_INHERIT_SET(type) \
    KUDA_DECLARE(kuda_status_t) kuda_##type##_inherit_set( \
                                          kuda_##type##_t *the##type)

/**
 * Prototype for type-specific declarations of kuda_foo_inherit_unset 
 * functions.  
 * @remark Doxygen unwraps this macro (via doxygen.conf) to provide 
 * actual help for each specific occurrence of kuda_foo_inherit_unset.
 * @remark the linkage is specified for KUDA. It would be possible to expand
 *       the macros to support other linkages.
 */
#define KUDA_DECLARE_INHERIT_UNSET(type) \
    KUDA_DECLARE(kuda_status_t) kuda_##type##_inherit_unset( \
                                          kuda_##type##_t *the##type)

#endif	/* ! KUDA_INHERIT_H */
