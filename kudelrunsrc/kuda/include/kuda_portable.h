/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* This header file is where you should put ANY platform specific information.
 * This should be the only header file that programs need to include that 
 * actually has platform dependent code which refers to the .
 */
#ifndef KUDA_PORTABLE_H
#define KUDA_PORTABLE_H
/**
 * @file kuda_portable.h
 * @brief KUDA Portability Routines
 */

#include "kuda.h"
#include "kuda_pools.h"
#include "kuda_thread_proc.h"
#include "kuda_file_io.h"
#include "kuda_network_io.h"
#include "kuda_errno.h"
#include "kuda_global_mutex.h"
#include "kuda_proc_mutex.h"
#include "kuda_time.h"
#include "kuda_dso.h"
#include "kuda_shm.h"

#if KUDA_HAVE_DIRENT_H
#include <dirent.h>
#endif
#if KUDA_HAVE_FCNTL_H
#include <fcntl.h>
#endif
#if KUDA_HAVE_PTHREAD_H
#include <pthread.h>
#endif
#if KUDA_HAVE_SEMAPHORE_H
#include <semaphore.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_portabile Portability Routines
 * @ingroup KUDA 
 * @{
 */

#ifdef WIN32
/* The primitives for Windows types */
typedef HANDLE                kuda_platform_file_t;
typedef HANDLE                kuda_platform_dir_t;
typedef SOCKET                kuda_platform_sock_t;
typedef HANDLE                kuda_platform_proc_mutex_t;
typedef HANDLE                kuda_platform_thread_t;
typedef HANDLE                kuda_platform_proc_t;
typedef DWORD                 kuda_platform_threadkey_t; 
typedef FILETIME              kuda_platform_imp_time_t;
typedef SYSTEMTIME            kuda_platform_exp_time_t;
typedef HANDLE                kuda_platform_dso_handle_t;
typedef HANDLE                kuda_platform_shm_t;

#elif defined(OS2)
typedef HFILE                 kuda_platform_file_t;
typedef HDIR                  kuda_platform_dir_t;
typedef int                   kuda_platform_sock_t;
typedef HMTX                  kuda_platform_proc_mutex_t;
typedef TID                   kuda_platform_thread_t;
typedef PID                   kuda_platform_proc_t;
typedef PULONG                kuda_platform_threadkey_t; 
typedef struct timeval        kuda_platform_imp_time_t;
typedef struct tm             kuda_platform_exp_time_t;
typedef HCAPI               kuda_platform_dso_handle_t;
typedef void*                 kuda_platform_shm_t;

#elif defined(__BEOS__)
#include <kernel/OS.h>
#include <kernel/image.h>

struct kuda_platform_proc_mutex_t {
	sem_id sem;
	int32  ben;
};

typedef int                   kuda_platform_file_t;
typedef DIR                   kuda_platform_dir_t;
typedef int                   kuda_platform_sock_t;
typedef struct kuda_platform_proc_mutex_t  kuda_platform_proc_mutex_t;
typedef thread_id             kuda_platform_thread_t;
typedef thread_id             kuda_platform_proc_t;
typedef int                   kuda_platform_threadkey_t;
typedef struct timeval        kuda_platform_imp_time_t;
typedef struct tm             kuda_platform_exp_time_t;
typedef image_id              kuda_platform_dso_handle_t;
typedef void*                 kuda_platform_shm_t;

#elif defined(NETWARE)
typedef int                   kuda_platform_file_t;
typedef DIR                   kuda_platform_dir_t;
typedef int                   kuda_platform_sock_t;
typedef NXMutex_t             kuda_platform_proc_mutex_t;
typedef NXThreadId_t          kuda_platform_thread_t;
typedef long                  kuda_platform_proc_t;
typedef NXKey_t               kuda_platform_threadkey_t; 
typedef struct timeval        kuda_platform_imp_time_t;
typedef struct tm             kuda_platform_exp_time_t;
typedef void *                kuda_platform_dso_handle_t;
typedef void*                 kuda_platform_shm_t;

#else
/* Any other PLATFORM should go above this one.  This is the lowest common
 * denominator typedefs for  all UNIX-like systems.  :)
 */

/** Basic PLATFORM process mutex structure. */
struct kuda_platform_proc_mutex_t {
#if KUDA_HAS_SYSVSEM_SERIALIZE || KUDA_HAS_FCNTL_SERIALIZE || KUDA_HAS_FLOCK_SERIALIZE
    /** Value used for SYS V Semaphore, FCNTL and FLOCK serialization */
    int crossproc;
#endif
#if KUDA_HAS_PROC_PTHREAD_SERIALIZE
    /** Value used for PTHREAD serialization */
    pthread_mutex_t *pthread_interproc;
#endif
#if KUDA_HAS_THREADS
    /* If no threads, no need for thread locks */
#if KUDA_USE_PTHREAD_SERIALIZE
    /** This value is currently unused within KUDA and cLHy */ 
    pthread_mutex_t *intraproc;
#endif
#endif
#if KUDA_HAS_POSIXSEM_SERIALIZE
    /** Value used for POSIX semaphores serialization */
    sem_t *psem_interproc;
#endif
};

typedef int                   kuda_platform_file_t;        /**< native file */
typedef DIR                   kuda_platform_dir_t;         /**< native dir */
typedef int                   kuda_platform_sock_t;        /**< native dir */
typedef struct kuda_platform_proc_mutex_t  kuda_platform_proc_mutex_t; /**< native process
                                                          *   mutex
                                                          */
#if KUDA_HAS_THREADS && KUDA_HAVE_PTHREAD_H 
typedef pthread_t             kuda_platform_thread_t;      /**< native thread */
typedef pthread_key_t         kuda_platform_threadkey_t;   /**< native thread address
                                                     *   space */
#endif
typedef pid_t                 kuda_platform_proc_t;        /**< native pid */
typedef struct timeval        kuda_platform_imp_time_t;    /**< native timeval */
typedef struct tm             kuda_platform_exp_time_t;    /**< native tm */
/** @var kuda_platform_dso_handle_t
 * native dso types
 */
#if defined(HPUX) || defined(HPUX10) || defined(HPUX11)
#include <dl.h>
typedef shl_t                 kuda_platform_dso_handle_t;
#elif defined(DARWIN)
#include <mach-o/dyld.h>
typedef NScAPI              kuda_platform_dso_handle_t;
#else
typedef void *                kuda_platform_dso_handle_t;
#endif
typedef void*                 kuda_platform_shm_t;         /**< native SHM */

#endif

/**
 * @typedef kuda_platform_sock_info_t
 * @brief alias for local PLATFORM socket
 */
/**
 * everything KUDA needs to know about an active socket to construct
 * an KUDA socket from it; currently, this is platform-independent
 */
struct kuda_platform_sock_info_t {
    kuda_platform_sock_t *platform_sock; /**< always required */
    struct sockaddr *local; /**< NULL if not yet bound */
    struct sockaddr *remote; /**< NULL if not connected */
    int family;             /**< always required (KUDA_INET, KUDA_INET6, etc.) */
    int type;               /**< always required (SOCK_STREAM, SOCK_DGRAM, etc.) */
    int protocol;           /**< 0 or actual protocol (KUDA_PROTO_SCTP, KUDA_PROTO_TCP, etc.) */
};

typedef struct kuda_platform_sock_info_t kuda_platform_sock_info_t;

#if KUDA_PROC_MUTEX_IS_GLOBAL || defined(DOXYGEN)
/** Opaque global mutex type */
#define kuda_platform_global_mutex_t kuda_platform_proc_mutex_t
/** @return kuda_platform_global_mutex */
#define kuda_platform_global_mutex_get kuda_platform_proc_mutex_get
#else
    /** Thread and process mutex for those platforms where process mutexes
     *  are not held in threads.
     */
    struct kuda_platform_global_mutex_t {
        kuda_pool_t *pool;
        kuda_proc_mutex_t *proc_mutex;
#if KUDA_HAS_THREADS
        kuda_thread_mutex_t *thread_mutex;
#endif /* KUDA_HAS_THREADS */
    };
    typedef struct kuda_platform_global_mutex_t kuda_platform_global_mutex_t;

KUDA_DECLARE(kuda_status_t) kuda_platform_global_mutex_get(kuda_platform_global_mutex_t *ospmutex, 
                                                kuda_global_mutex_t *pmutex);
#endif


/**
 * convert the file from kuda type to platforms specific type.
 * @param thefile The platforms specific file we are converting to
 * @param file The kuda file to convert.
 * @remark On Unix, it is only possible to get a file descriptor from 
 *         an kuda file type.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_file_get(kuda_platform_file_t *thefile,
                                          kuda_file_t *file);

/**
 * convert the dir from kuda type to platforms specific type.
 * @param thedir The platforms specific dir we are converting to
 * @param dir The kuda dir to convert.
 */   
KUDA_DECLARE(kuda_status_t) kuda_platform_dir_get(kuda_platform_dir_t **thedir, 
                                         kuda_dir_t *dir);

/**
 * Convert the socket from an kuda type to an PLATFORM specific socket
 * @param thesock The socket to convert.
 * @param sock The platforms specific equivalent of the kuda socket..
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_sock_get(kuda_platform_sock_t *thesock,
                                          kuda_socket_t *sock);

/**
 * Convert the proc mutex from kuda type to platforms specific type
 * @param ospmutex The platforms specific proc mutex we are converting to.
 * @param pmutex The kuda proc mutex to convert.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_get(kuda_platform_proc_mutex_t *ospmutex, 
                                                kuda_proc_mutex_t *pmutex);

/**
 * Convert the proc mutex from kuda type to platforms specific type, also
 * providing the mechanism used by the kuda mutex.
 * @param ospmutex The platforms specific proc mutex we are converting to.
 * @param pmutex The kuda proc mutex to convert.
 * @param mech The mechanism used by the kuda proc mutex (if not NULL).
 * @remark Allows for disambiguation for platforms with multiple mechanisms
 *         available.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_get_ex(kuda_platform_proc_mutex_t *ospmutex, 
                                                   kuda_proc_mutex_t *pmutex,
                                                   kuda_lockmech_e *mech);

/**
 * Get the exploded time in the platforms native format.
 * @param ostime the native time format
 * @param kudatime the time to convert
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_exp_time_get(kuda_platform_exp_time_t **ostime,
                                 kuda_time_exp_t *kudatime);

/**
 * Get the imploded time in the platforms native format.
 * @param ostime  the native time format
 * @param kudatime the time to convert
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_imp_time_get(kuda_platform_imp_time_t **ostime, 
                                              kuda_time_t *kudatime);

/**
 * convert the shm from kuda type to platforms specific type.
 * @param osshm The platforms specific shm representation
 * @param shm The kuda shm to convert.
 */   
KUDA_DECLARE(kuda_status_t) kuda_platform_shm_get(kuda_platform_shm_t *osshm,
                                         kuda_shm_t *shm);

#if KUDA_HAS_THREADS || defined(DOXYGEN)
/** 
 * @defgroup kuda_platform_thread Thread portability Routines
 * @{ 
 */
/**
 * convert the thread to platforms specific type from kuda type.
 * @param thethd The kuda thread to convert
 * @param thd The platforms specific thread we are converting to
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_thread_get(kuda_platform_thread_t **thethd, 
                                            kuda_thread_t *thd);

/**
 * convert the thread private memory key to platforms specific type from an kuda type.
 * @param thekey The kuda handle we are converting from.
 * @param key The platforms specific handle we are converting to.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_threadkey_get(kuda_platform_threadkey_t *thekey,
                                               kuda_threadkey_t *key);

/**
 * convert the thread from platforms specific type to kuda type.
 * @param thd The kuda thread we are converting to.
 * @param thethd The platforms specific thread to convert
 * @param cont The pool to use if it is needed.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_thread_put(kuda_thread_t **thd,
                                            kuda_platform_thread_t *thethd,
                                            kuda_pool_t *cont);

/**
 * convert the thread private memory key from platforms specific type to kuda type.
 * @param key The kuda handle we are converting to.
 * @param thekey The platforms specific handle to convert
 * @param cont The pool to use if it is needed.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_threadkey_put(kuda_threadkey_t **key,
                                               kuda_platform_threadkey_t *thekey,
                                               kuda_pool_t *cont);
/**
 * Get the thread ID
 */
KUDA_DECLARE(kuda_platform_thread_t) kuda_platform_thread_current(void);

/**
 * Compare two thread id's
 * @param tid1 1st Thread ID to compare
 * @param tid2 2nd Thread ID to compare
 * @return non-zero if the two threads are equal, zero otherwise
 */ 
KUDA_DECLARE(int) kuda_platform_thread_equal(kuda_platform_thread_t tid1, 
                                     kuda_platform_thread_t tid2);

/** @} */
#endif /* KUDA_HAS_THREADS */

/**
 * convert the file from platforms specific type to kuda type.
 * @param file The kuda file we are converting to.
 * @param thefile The platforms specific file to convert
 * @param flags The flags that were used to open this file.
 * @param cont The pool to use if it is needed.
 * @remark On Unix, it is only possible to put a file descriptor into
 *         an kuda file type.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_file_put(kuda_file_t **file,
                                          kuda_platform_file_t *thefile,
                                          kuda_int32_t flags, kuda_pool_t *cont); 

/**
 * convert the file from platforms specific type to kuda type.
 * @param file The kuda file we are converting to.
 * @param thefile The platforms specific pipe to convert
 * @param cont The pool to use if it is needed.
 * @remark On Unix, it is only possible to put a file descriptor into
 *         an kuda file type.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_pipe_put(kuda_file_t **file,
                                          kuda_platform_file_t *thefile,
                                          kuda_pool_t *cont);

/**
 * convert the file from platforms specific type to kuda type.
 * @param file The kuda file we are converting to.
 * @param thefile The platforms specific pipe to convert
 * @param register_cleanup A cleanup will be registered on the kuda_file_t
 *   to issue kuda_file_close().
 * @param cont The pool to use if it is needed.
 * @remark On Unix, it is only possible to put a file descriptor into
 *         an kuda file type.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_pipe_put_ex(kuda_file_t **file,
                                             kuda_platform_file_t *thefile,
                                             int register_cleanup,
                                             kuda_pool_t *cont);

/**
 * convert the dir from platforms specific type to kuda type.
 * @param dir The kuda dir we are converting to.
 * @param thedir The platforms specific dir to convert
 * @param cont The pool to use when creating to kuda directory.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_dir_put(kuda_dir_t **dir,
                                         kuda_platform_dir_t *thedir,
                                         kuda_pool_t *cont); 

/**
 * Convert a socket from the platforms specific type to the KUDA type. If
 * sock points to NULL, a socket will be created from the pool
 * provided. If **sock does not point to NULL, the structure pointed
 * to by sock will be reused and updated with the given socket.
 * @param sock The pool to use.
 * @param thesock The socket to convert to.
 * @param cont The socket we are converting to an kuda type.
 * @remark If it is a true socket, it is best to call kuda_platform_sock_make()
 *         and provide KUDA with more information about the socket.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_sock_put(kuda_socket_t **sock, 
                                          kuda_platform_sock_t *thesock, 
                                          kuda_pool_t *cont);

/**
 * Create a socket from an existing descriptor and local and remote
 * socket addresses.
 * @param kuda_sock The new socket that has been set up
 * @param platform_sock_info The platforms representation of the socket handle and
 *        other characteristics of the socket
 * @param cont The pool to use
 * @remark If you only know the descriptor/handle or if it isn't really
 *         a true socket, use kuda_platform_sock_put() instead.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_sock_make(kuda_socket_t **kuda_sock,
                                           kuda_platform_sock_info_t *platform_sock_info,
                                           kuda_pool_t *cont);

/**
 * Convert the proc mutex from platforms specific type to kuda type
 * @param pmutex The kuda proc mutex we are converting to.
 * @param ospmutex The platforms specific proc mutex to convert.
 * @param cont The pool to use if it is needed.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_put(kuda_proc_mutex_t **pmutex,
                                                kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_pool_t *cont); 

/**
 * Convert the proc mutex from platforms specific type to kuda type, using the
 * specified mechanism.
 * @param pmutex The kuda proc mutex we are converting to.
 * @param ospmutex The platforms specific proc mutex to convert.
 * @param mech The kuda mutex locking mechanism
 * @param register_cleanup Whether to destroy the platforms mutex with the kuda
 *        one (either on explicit destroy or pool cleanup).
 * @param cont The pool to use if it is needed.
 * @remark Allows for disambiguation for platforms with multiple mechanisms
 *         available.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_put_ex(kuda_proc_mutex_t **pmutex,
                                                kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_lockmech_e mech,
                                                int register_cleanup,
                                                kuda_pool_t *cont); 

/**
 * Put the imploded time in the KUDA format.
 * @param kudatime the KUDA time format
 * @param ostime the time to convert
 * @param cont the pool to use if necessary
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_imp_time_put(kuda_time_t *kudatime,
                                              kuda_platform_imp_time_t **ostime,
                                              kuda_pool_t *cont); 

/**
 * Put the exploded time in the KUDA format.
 * @param kudatime the KUDA time format
 * @param ostime the time to convert
 * @param cont the pool to use if necessary
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_exp_time_put(kuda_time_exp_t *kudatime,
                                              kuda_platform_exp_time_t **ostime,
                                              kuda_pool_t *cont); 

/**
 * convert the shared memory from platforms specific type to kuda type.
 * @param shm The kuda shm representation of osshm
 * @param osshm The platforms specific shm identity
 * @param cont The pool to use if it is needed.
 * @remark On fork()ed architectures, this is typically nothing more than
 * the memory block mapped.  On non-fork architectures, this is typically
 * some internal handle to pass the mapping from process to process.
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_shm_put(kuda_shm_t **shm,
                                         kuda_platform_shm_t *osshm,
                                         kuda_pool_t *cont); 


#if KUDA_HAS_DSO || defined(DOXYGEN)
/** 
 * @defgroup kuda_platform_dso DSO (Dynamic Loading) Portability Routines
 * @{
 */
/**
 * convert the dso handle from platforms specific to kuda
 * @param dso The kuda handle we are converting to
 * @param thedso the platforms specific handle to convert
 * @param pool the pool to use if it is needed
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_put(kuda_dso_handle_t **dso,
                                                kuda_platform_dso_handle_t thedso,
                                                kuda_pool_t *pool);

/**
 * convert the kuda dso handle into an platforms specific one
 * @param kudadso The kuda dso handle to convert
 * @param dso The platforms specific dso to return
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_get(kuda_platform_dso_handle_t *dso,
                                                kuda_dso_handle_t *kudadso);

/** @} */
#endif /* KUDA_HAS_DSO */


#if KUDA_HAS_PLATFORM_UUID
/**
 * Private: kuda-delman's kuda_uuid cAPI when supported by the platform
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_uuid_get(unsigned char *uuid_data);
#endif


/**
 * Get the name of the system default character set.
 * @param pool the pool to allocate the name from, if needed
 */
KUDA_DECLARE(const char*) kuda_platform_default_encoding(kuda_pool_t *pool);


/**
 * Get the name of the current locale character set.
 * @param pool the pool to allocate the name from, if needed
 * @remark Defers to kuda_platform_default_encoding() if the current locale's
 * data can't be retrieved on this system.
 */
KUDA_DECLARE(const char*) kuda_platform_locale_encoding(kuda_pool_t *pool);

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_PORTABLE_H */
