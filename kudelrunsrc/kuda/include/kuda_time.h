/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_TIME_H
#define KUDA_TIME_H

/**
 * @file kuda_time.h
 * @brief KUDA Time Library
 */

#include "kuda.h"
#include "kuda_errno.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_time Time Routines
 * @ingroup KUDA 
 * @{
 */

/** month names */
KUDA_DECLARE_DATA extern const char kuda_month_snames[12][4];
/** day names */
KUDA_DECLARE_DATA extern const char kuda_day_snames[7][4];


/** number of microseconds since 00:00:00 January 1, 1970 UTC */
typedef kuda_int64_t kuda_time_t;


/** mechanism to properly type kuda_time_t literals */
#define KUDA_TIME_C(val) KUDA_INT64_C(val)

/** mechanism to properly print kuda_time_t values */
#define KUDA_TIME_T_FMT KUDA_INT64_T_FMT

/** intervals for I/O timeouts, in microseconds */
typedef kuda_int64_t kuda_interval_time_t;
/** short interval for I/O timeouts, in microseconds */
typedef kuda_int32_t kuda_short_interval_time_t;

/** number of microseconds per second */
#define KUDA_USEC_PER_SEC KUDA_TIME_C(1000000)

/** @return kuda_time_t as a second */
#define kuda_time_sec(time) ((time) / KUDA_USEC_PER_SEC)

/** @return kuda_time_t as a usec */
#define kuda_time_usec(time) ((time) % KUDA_USEC_PER_SEC)

/** @return kuda_time_t as a msec */
#define kuda_time_msec(time) (((time) / 1000) % 1000)

/** @return kuda_time_t as a msec */
#define kuda_time_as_msec(time) ((time) / 1000)

/** @return milliseconds as an kuda_time_t */
#define kuda_time_from_msec(msec) ((kuda_time_t)(msec) * 1000)

/** @return seconds as an kuda_time_t */
#define kuda_time_from_sec(sec) ((kuda_time_t)(sec) * KUDA_USEC_PER_SEC)

/** @return a second and usec combination as an kuda_time_t */
#define kuda_time_make(sec, usec) ((kuda_time_t)(sec) * KUDA_USEC_PER_SEC \
                                + (kuda_time_t)(usec))

/**
 * @return the current time
 */
KUDA_DECLARE(kuda_time_t) kuda_time_now(void);

/** @see kuda_time_exp_t */
typedef struct kuda_time_exp_t kuda_time_exp_t;

/**
 * a structure similar to ANSI struct tm with the following differences:
 *  - tm_usec isn't an ANSI field
 *  - tm_gmtoff isn't an ANSI field (it's a BSDism)
 */
struct kuda_time_exp_t {
    /** microseconds past tm_sec */
    kuda_int32_t tm_usec;
    /** (0-61) seconds past tm_min */
    kuda_int32_t tm_sec;
    /** (0-59) minutes past tm_hour */
    kuda_int32_t tm_min;
    /** (0-23) hours past midnight */
    kuda_int32_t tm_hour;
    /** (1-31) day of the month */
    kuda_int32_t tm_mday;
    /** (0-11) month of the year */
    kuda_int32_t tm_mon;
    /** year since 1900 */
    kuda_int32_t tm_year;
    /** (0-6) days since Sunday */
    kuda_int32_t tm_wday;
    /** (0-365) days since January 1 */
    kuda_int32_t tm_yday;
    /** daylight saving time */
    kuda_int32_t tm_isdst;
    /** seconds east of UTC */
    kuda_int32_t tm_gmtoff;
};

/* Delayed the include to avoid a circular reference */
#include "kuda_pools.h"

/**
 * Convert an ansi time_t to an kuda_time_t
 * @param result the resulting kuda_time_t
 * @param input the time_t to convert
 */
KUDA_DECLARE(kuda_status_t) kuda_time_ansi_put(kuda_time_t *result, 
                                                    time_t input);

/**
 * Convert a time to its human readable components using an offset
 * from GMT.
 * @param result the exploded time
 * @param input the time to explode
 * @param offs the number of seconds offset to apply
 */
KUDA_DECLARE(kuda_status_t) kuda_time_exp_tz(kuda_time_exp_t *result,
                                          kuda_time_t input,
                                          kuda_int32_t offs);

/**
 * Convert a time to its human readable components (GMT).
 * @param result the exploded time
 * @param input the time to explode
 */
KUDA_DECLARE(kuda_status_t) kuda_time_exp_gmt(kuda_time_exp_t *result, 
                                           kuda_time_t input);

/**
 * Convert a time to its human readable components in the local timezone.
 * @param result the exploded time
 * @param input the time to explode
 */
KUDA_DECLARE(kuda_status_t) kuda_time_exp_lt(kuda_time_exp_t *result, 
                                          kuda_time_t input);

/**
 * Convert time value from human readable format to a numeric kuda_time_t
 * (elapsed microseconds since the epoch).
 * @param result the resulting imploded time
 * @param input the input exploded time
 */
KUDA_DECLARE(kuda_status_t) kuda_time_exp_get(kuda_time_t *result, 
                                           kuda_time_exp_t *input);

/**
 * Convert time value from human readable format to a numeric kuda_time_t that
 * always represents GMT.
 * @param result the resulting imploded time
 * @param input the input exploded time
 */
KUDA_DECLARE(kuda_status_t) kuda_time_exp_gmt_get(kuda_time_t *result, 
                                               kuda_time_exp_t *input);

/**
 * Sleep for the specified number of micro-seconds.
 * @param t desired amount of time to sleep.
 * @warning May sleep for longer than the specified time. 
 */
KUDA_DECLARE(void) kuda_sleep(kuda_interval_time_t t);

/** length of a RFC822 Date */
#define KUDA_RFC822_DATE_LEN (30)
/**
 * kuda_rfc822_date formats dates in the RFC822
 * format in an efficient manner.  It is a fixed length
 * format which requires KUDA_RFC822_DATA_LEN bytes of storage,
 * including the trailing NUL terminator.
 * @param date_str String to write to.
 * @param t the time to convert 
 */
KUDA_DECLARE(kuda_status_t) kuda_rfc822_date(char *date_str, kuda_time_t t);

/** length of a CTIME date */
#define KUDA_CTIME_LEN (25)
/**
 * kuda_ctime formats dates in the ctime() format
 * in an efficient manner.  It is a fixed length format
 * and requires KUDA_CTIME_LEN bytes of storage including
 * the trailing NUL terminator.
 * Unlike ANSI/ISO C ctime(), kuda_ctime() does not include
 * a \\n at the end of the string.
 * @param date_str String to write to.
 * @param t the time to convert 
 */
KUDA_DECLARE(kuda_status_t) kuda_ctime(char *date_str, kuda_time_t t);

/**
 * Formats the exploded time according to the format specified
 * @param s string to write to
 * @param retsize The length of the returned string
 * @param max The maximum length of the string
 * @param format The format for the time string
 * @param tm The time to convert
 */
KUDA_DECLARE(kuda_status_t) kuda_strftime(char *s, kuda_size_t *retsize, 
                                       kuda_size_t max, const char *format, 
                                       kuda_time_exp_t *tm);

/**
 * Improve the clock resolution for the lifetime of the given pool.
 * Generally this is only desirable on benchmarking and other very
 * time-sensitive applications, and has no impact on most platforms.
 * @param p The pool to associate the finer clock resolution 
 */
KUDA_DECLARE(void) kuda_time_clock_hires(kuda_pool_t *p);

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_TIME_H */
