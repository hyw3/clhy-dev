/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_THREAD_MUTEX_H
#define KUDA_THREAD_MUTEX_H

/**
 * @file kuda_thread_mutex.h
 * @brief KUDA Thread Mutex Routines
 */

#include "kuda.h"
#include "kuda_errno.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#if KUDA_HAS_THREADS || defined(DOXYGEN)

/**
 * @defgroup kuda_thread_mutex Thread Mutex Routines
 * @ingroup KUDA 
 * @{
 */

/** Opaque thread-local mutex structure */
typedef struct kuda_thread_mutex_t kuda_thread_mutex_t;

#define KUDA_THREAD_MUTEX_DEFAULT  0x0   /**< platform-optimal lock behavior */
#define KUDA_THREAD_MUTEX_NESTED   0x1   /**< enable nested (recursive) locks */
#define KUDA_THREAD_MUTEX_UNNESTED 0x2   /**< disable nested locks */
#define KUDA_THREAD_MUTEX_TIMED    0x4   /**< enable timed locks */

/* Delayed the include to avoid a circular reference */
#include "kuda_pools.h"
#include "kuda_time.h"

/**
 * Create and initialize a mutex that can be used to synchronize threads.
 * @param mutex the memory address where the newly created mutex will be
 *        stored.
 * @param flags Or'ed value of:
 * <PRE>
 *           KUDA_THREAD_MUTEX_DEFAULT   platform-optimal lock behavior.
 *           KUDA_THREAD_MUTEX_NESTED    enable nested (recursive) locks.
 *           KUDA_THREAD_MUTEX_UNNESTED  disable nested locks (non-recursive).
 * </PRE>
 * @param pool the pool from which to allocate the mutex.
 * @warning Be cautious in using KUDA_THREAD_MUTEX_DEFAULT.  While this is the
 * most optimal mutex based on a given platform's performance characteristics,
 * it will behave as either a nested or an unnested lock.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_create(kuda_thread_mutex_t **mutex,
                                                  unsigned int flags,
                                                  kuda_pool_t *pool);
/**
 * Acquire the lock for the given mutex. If the mutex is already locked,
 * the current thread will be put to sleep until the lock becomes available.
 * @param mutex the mutex on which to acquire the lock.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_lock(kuda_thread_mutex_t *mutex);

/**
 * Attempt to acquire the lock for the given mutex. If the mutex has already
 * been acquired, the call returns immediately with KUDA_EBUSY. Note: it
 * is important that the KUDA_STATUS_IS_EBUSY(s) macro be used to determine
 * if the return value was KUDA_EBUSY, for portability reasons.
 * @param mutex the mutex on which to attempt the lock acquiring.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_trylock(kuda_thread_mutex_t *mutex);

/**
 * Attempt to acquire the lock for the given mutex until timeout expires.
 * If the acquisition time outs, the call returns with KUDA_TIMEUP.
 * @param mutex the mutex on which to attempt the lock acquiring.
 * @param timeout the relative timeout (microseconds).
 * @note A timeout negative or nul means immediate attempt, returning
 *       KUDA_TIMEUP without blocking if it the lock is already acquired.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_timedlock(kuda_thread_mutex_t *mutex,
                                                 kuda_interval_time_t timeout);

/**
 * Release the lock for the given mutex.
 * @param mutex the mutex from which to release the lock.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_unlock(kuda_thread_mutex_t *mutex);

/**
 * Destroy the mutex and free the memory associated with the lock.
 * @param mutex the mutex to destroy.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_destroy(kuda_thread_mutex_t *mutex);

/**
 * Get the pool used by this thread_mutex.
 * @return kuda_pool_t the pool
 */
KUDA_POOL_DECLARE_ACCESSOR(thread_mutex);

#endif /* KUDA_HAS_THREADS */

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_THREAD_MUTEX_H */
