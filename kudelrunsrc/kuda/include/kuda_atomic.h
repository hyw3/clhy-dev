/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_ATOMIC_H
#define KUDA_ATOMIC_H

/**
 * @file kuda_atomic.h
 * @brief KUDA Atomic Operations
 */

#include "kuda.h"
#include "kuda_pools.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup kuda_atomic Atomic Operations
 * @ingroup KUDA 
 * @{
 */

/**
 * this function is required on some platforms to initialize the
 * atomic operation's internal structures
 * @param p pool
 * @return KUDA_SUCCESS on successful completion
 * @remark Programs do NOT need to call this directly. KUDA will call this
 *         automatically from kuda_initialize().
 * @internal
 */
KUDA_DECLARE(kuda_status_t) kuda_atomic_init(kuda_pool_t *p);

/*
 * Atomic operations on 32-bit values
 * Note: Each of these functions internally implements a memory barrier
 * on platforms that require it
 */

/**
 * atomically read an kuda_uint32_t from memory
 * @param mem the pointer
 */
KUDA_DECLARE(kuda_uint32_t) kuda_atomic_read32(volatile kuda_uint32_t *mem);

/**
 * atomically set an kuda_uint32_t in memory
 * @param mem pointer to the object
 * @param val value that the object will assume
 */
KUDA_DECLARE(void) kuda_atomic_set32(volatile kuda_uint32_t *mem, kuda_uint32_t val);

/**
 * atomically add 'val' to an kuda_uint32_t
 * @param mem pointer to the object
 * @param val amount to add
 * @return old value pointed to by mem
 */
KUDA_DECLARE(kuda_uint32_t) kuda_atomic_add32(volatile kuda_uint32_t *mem, kuda_uint32_t val);

/**
 * atomically subtract 'val' from an kuda_uint32_t
 * @param mem pointer to the object
 * @param val amount to subtract
 */
KUDA_DECLARE(void) kuda_atomic_sub32(volatile kuda_uint32_t *mem, kuda_uint32_t val);

/**
 * atomically increment an kuda_uint32_t by 1
 * @param mem pointer to the object
 * @return old value pointed to by mem
 */
KUDA_DECLARE(kuda_uint32_t) kuda_atomic_inc32(volatile kuda_uint32_t *mem);

/**
 * atomically decrement an kuda_uint32_t by 1
 * @param mem pointer to the atomic value
 * @return zero if the value becomes zero on decrement, otherwise non-zero
 */
KUDA_DECLARE(int) kuda_atomic_dec32(volatile kuda_uint32_t *mem);

/**
 * compare an kuda_uint32_t's value with 'cmp'.
 * If they are the same swap the value with 'with'
 * @param mem pointer to the value
 * @param with what to swap it with
 * @param cmp the value to compare it to
 * @return the old value of *mem
 */
KUDA_DECLARE(kuda_uint32_t) kuda_atomic_cas32(volatile kuda_uint32_t *mem, kuda_uint32_t with,
                              kuda_uint32_t cmp);

/**
 * exchange an kuda_uint32_t's value with 'val'.
 * @param mem pointer to the value
 * @param val what to swap it with
 * @return the old value of *mem
 */
KUDA_DECLARE(kuda_uint32_t) kuda_atomic_xchg32(volatile kuda_uint32_t *mem, kuda_uint32_t val);

/*
 * Atomic operations on 64-bit values
 * Note: Each of these functions internally implements a memory barrier
 * on platforms that require it
 */

/**
 * atomically read an kuda_uint64_t from memory
 * @param mem the pointer
 */
KUDA_DECLARE(kuda_uint64_t) kuda_atomic_read64(volatile kuda_uint64_t *mem);

/**
 * atomically set an kuda_uint64_t in memory
 * @param mem pointer to the object
 * @param val value that the object will assume
 */
KUDA_DECLARE(void) kuda_atomic_set64(volatile kuda_uint64_t *mem, kuda_uint64_t val);

/**
 * atomically add 'val' to an kuda_uint64_t
 * @param mem pointer to the object
 * @param val amount to add
 * @return old value pointed to by mem
 */
KUDA_DECLARE(kuda_uint64_t) kuda_atomic_add64(volatile kuda_uint64_t *mem, kuda_uint64_t val);

/**
 * atomically subtract 'val' from an kuda_uint64_t
 * @param mem pointer to the object
 * @param val amount to subtract
 */
KUDA_DECLARE(void) kuda_atomic_sub64(volatile kuda_uint64_t *mem, kuda_uint64_t val);

/**
 * atomically increment an kuda_uint64_t by 1
 * @param mem pointer to the object
 * @return old value pointed to by mem
 */
KUDA_DECLARE(kuda_uint64_t) kuda_atomic_inc64(volatile kuda_uint64_t *mem);

/**
 * atomically decrement an kuda_uint64_t by 1
 * @param mem pointer to the atomic value
 * @return zero if the value becomes zero on decrement, otherwise non-zero
 */
KUDA_DECLARE(int) kuda_atomic_dec64(volatile kuda_uint64_t *mem);

/**
 * compare an kuda_uint64_t's value with 'cmp'.
 * If they are the same swap the value with 'with'
 * @param mem pointer to the value
 * @param with what to swap it with
 * @param cmp the value to compare it to
 * @return the old value of *mem
 */
KUDA_DECLARE(kuda_uint64_t) kuda_atomic_cas64(volatile kuda_uint64_t *mem, kuda_uint64_t with,
                              kuda_uint64_t cmp);

/**
 * exchange an kuda_uint64_t's value with 'val'.
 * @param mem pointer to the value
 * @param val what to swap it with
 * @return the old value of *mem
 */
KUDA_DECLARE(kuda_uint64_t) kuda_atomic_xchg64(volatile kuda_uint64_t *mem, kuda_uint64_t val);

/**
 * compare the pointer's value with cmp.
 * If they are the same swap the value with 'with'
 * @param mem pointer to the pointer
 * @param with what to swap it with
 * @param cmp the value to compare it to
 * @return the old value of the pointer
 */
KUDA_DECLARE(void*) kuda_atomic_casptr(volatile void **mem, void *with, const void *cmp);

/**
 * exchange a pair of pointer values
 * @param mem pointer to the pointer
 * @param with what to swap it with
 * @return the old value of the pointer
 */
KUDA_DECLARE(void*) kuda_atomic_xchgptr(volatile void **mem, void *with);

/** @} */

#ifdef __cplusplus
}
#endif

#endif	/* !KUDA_ATOMIC_H */
