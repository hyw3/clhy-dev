/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_THREAD_PROC_H
#define KUDA_THREAD_PROC_H

/**
 * @file kuda_thread_proc.h
 * @brief KUDA Thread and Process Library
 */

#include "kuda.h"
#include "kuda_file_io.h"
#include "kuda_pools.h"
#include "kuda_errno.h"
#include "kuda_perms_set.h"

#if KUDA_HAVE_STRUCT_RLIMIT
#include <sys/time.h>
#include <sys/resource.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_thread_proc Threads and Process Functions
 * @ingroup KUDA 
 * @{
 */

typedef enum {
    KUDA_SHELLCMD,           /**< use the shell to invoke the program */
    KUDA_PROGRAM,            /**< invoke the program directly, no copied env */
    KUDA_PROGRAM_ENV,        /**< invoke the program, replicating our environment */
    KUDA_PROGRAM_PATH,       /**< find program on PATH, use our environment */
    KUDA_SHELLCMD_ENV        /**< use the shell to invoke the program,
                             *   replicating our environment
                             */
} kuda_cmdtype_e;

typedef enum {
    KUDA_WAIT,           /**< wait for the specified process to finish */
    KUDA_NOWAIT          /**< do not wait -- just see if it has finished */
} kuda_wait_how_e;

/* I am specifically calling out the values so that the macros below make
 * more sense.  Yes, I know I don't need to, but I am hoping this makes what
 * I am doing more clear.  If you want to add more reasons to exit, continue
 * to use bitmasks.
 */
typedef enum {
    KUDA_PROC_EXIT = 1,          /**< process exited normally */
    KUDA_PROC_SIGNAL = 2,        /**< process exited due to a signal */
    KUDA_PROC_SIGNAL_CORE = 4    /**< process exited and dumped a core file */
} kuda_exit_why_e;

/** did we exit the process */
#define KUDA_PROC_CHECK_EXIT(x)        (x & KUDA_PROC_EXIT)
/** did we get a signal */
#define KUDA_PROC_CHECK_SIGNALED(x)    (x & KUDA_PROC_SIGNAL)
/** did we get core */
#define KUDA_PROC_CHECK_CORE_DUMP(x)   (x & KUDA_PROC_SIGNAL_CORE)

/** @see kuda_procattr_io_set */
#define KUDA_NO_PIPE          0
/** @see kuda_procattr_io_set and kuda_file_pipe_create_ex */
#define KUDA_FULL_BLOCK       1
/** @see kuda_procattr_io_set and kuda_file_pipe_create_ex */
#define KUDA_FULL_NONBLOCK    2
/** @see kuda_procattr_io_set */
#define KUDA_PARENT_BLOCK     3
/** @see kuda_procattr_io_set */
#define KUDA_CHILD_BLOCK      4
/** @see kuda_procattr_io_set */
#define KUDA_NO_FILE          8

/** @see kuda_file_pipe_create_ex */
#define KUDA_READ_BLOCK       3
/** @see kuda_file_pipe_create_ex */
#define KUDA_WRITE_BLOCK      4

/** @see kuda_procattr_io_set 
 * @note Win32 only effective with version 1.2.12, portably introduced in 1.3.0
 */
#define KUDA_NO_FILE          8

/** @see kuda_procattr_limit_set */
#define KUDA_LIMIT_CPU        0
/** @see kuda_procattr_limit_set */
#define KUDA_LIMIT_MEM        1
/** @see kuda_procattr_limit_set */
#define KUDA_LIMIT_NPROC      2
/** @see kuda_procattr_limit_set */
#define KUDA_LIMIT_NOFILE     3

/**
 * @defgroup KUDA_OC Other Child Flags
 * @{
 */
#define KUDA_OC_REASON_DEATH         0     /**< child has died, caller must call
                                           * unregister still */
#define KUDA_OC_REASON_UNWRITABLE    1     /**< write_fd is unwritable */
#define KUDA_OC_REASON_RESTART       2     /**< a restart is occurring, perform
                                           * any necessary cleanup (including
                                           * sending a special signal to child)
                                           */
#define KUDA_OC_REASON_UNREGISTER    3     /**< unregister has been called, do
                                           * whatever is necessary (including
                                           * kill the child) */
#define KUDA_OC_REASON_LOST          4     /**< somehow the child exited without
                                           * us knowing ... buggy platforms? */
#define KUDA_OC_REASON_RUNNING       5     /**< a health check is occurring, 
                                           * for most maintainence functions
                                           * this is a no-op.
                                           */
/** @} */

/** The KUDA process type */
typedef struct kuda_proc_t {
    /** The process ID */
    pid_t pid;
    /** Parent's side of pipe to child's stdin */
    kuda_file_t *in;
    /** Parent's side of pipe to child's stdout */
    kuda_file_t *out;
    /** Parent's side of pipe to child's stdouterr */
    kuda_file_t *err;
#if KUDA_HAS_PROC_INVOKED || defined(DOXYGEN)
    /** Diagnositics/debugging string of the command invoked for 
     *  this process [only present if KUDA_HAS_PROC_INVOKED is true]
     * @remark Only enabled on Win32 by default.
     * @bug This should either always or never be present in release
     * builds - since it breaks binary compatibility.  We may enable
     * it always in KUDA 1.0 yet leave it undefined in most cases.
     */
    char *invoked;
#endif
#if defined(WIN32) || defined(DOXYGEN)
    /** (Win32 only) Creator's handle granting access to the process
     * @remark This handle is closed and reset to NULL in every case
     * corresponding to a waitpid() on Unix which returns the exit status.
     * Therefore Win32 correspond's to Unix's zombie reaping characteristics
     * and avoids potential handle leaks.
     */
    HANDLE hproc;
#endif
} kuda_proc_t;

/**
 * The prototype for KUDA child errfn functions.  (See the description
 * of kuda_procattr_child_errfn_set() for more information.)
 * It is passed the following parameters:
 * @param pool Pool associated with the kuda_proc_t.  If your child
 *             error function needs user data, associate it with this
 *             pool.
 * @param err KUDA error code describing the error
 * @param description Text description of type of processing which failed
 */
typedef void (kuda_child_errfn_t)(kuda_pool_t *proc, kuda_status_t err,
                                 const char *description);

/** Opaque Thread structure. */
typedef struct kuda_thread_t           kuda_thread_t;

/** Opaque Thread attributes structure. */
typedef struct kuda_threadattr_t       kuda_threadattr_t;

/** Opaque Process attributes structure. */
typedef struct kuda_procattr_t         kuda_procattr_t;

/** Opaque control variable for one-time atomic variables.  */
typedef struct kuda_thread_once_t      kuda_thread_once_t;

/** Opaque thread private address space. */
typedef struct kuda_threadkey_t        kuda_threadkey_t;

/** Opaque record of child process. */
typedef struct kuda_other_child_rec_t  kuda_other_child_rec_t;

/**
 * The prototype for any KUDA thread worker functions.
 */
typedef void *(KUDA_THREAD_FUNC *kuda_thread_start_t)(kuda_thread_t*, void*);

typedef enum {
    KUDA_KILL_NEVER,             /**< process is never killed (i.e., never sent
                                 * any signals), but it will be reaped if it exits
                                 * before the pool is cleaned up */
    KUDA_KILL_ALWAYS,            /**< process is sent SIGKILL on kuda_pool_t cleanup */
    KUDA_KILL_AFTER_TIMEOUT,     /**< SIGTERM, wait 3 seconds, SIGKILL */
    KUDA_JUST_WAIT,              /**< wait forever for the process to complete */
    KUDA_KILL_ONLY_ONCE          /**< send SIGTERM and then wait */
} kuda_kill_conditions_e;

/* Thread Function definitions */

#if KUDA_HAS_THREADS

/**
 * Create and initialize a new threadattr variable
 * @param new_attr The newly created threadattr.
 * @param cont The pool to use
 */
KUDA_DECLARE(kuda_status_t) kuda_threadattr_create(kuda_threadattr_t **new_attr, 
                                                kuda_pool_t *cont);

/**
 * Set if newly created threads should be created in detached state.
 * @param attr The threadattr to affect 
 * @param on Non-zero if detached threads should be created.
 */
KUDA_DECLARE(kuda_status_t) kuda_threadattr_detach_set(kuda_threadattr_t *attr, 
                                                    kuda_int32_t on);

/**
 * Get the detach state for this threadattr.
 * @param attr The threadattr to reference
 * @return KUDA_DETACH if threads are to be detached, or KUDA_NOTDETACH
 * if threads are to be joinable. 
 */
KUDA_DECLARE(kuda_status_t) kuda_threadattr_detach_get(kuda_threadattr_t *attr);

/**
 * Set the stack size of newly created threads.
 * @param attr The threadattr to affect 
 * @param stacksize The stack size in bytes
 */
KUDA_DECLARE(kuda_status_t) kuda_threadattr_stacksize_set(kuda_threadattr_t *attr,
                                                       kuda_size_t stacksize);

/**
 * Set the stack guard area size of newly created threads.
 * @param attr The threadattr to affect 
 * @param guardsize The stack guard area size in bytes
 * @note Thread library implementations commonly use a "guard area"
 * after each thread's stack which is not readable or writable such that
 * stack overflows cause a segfault; this consumes e.g. 4K of memory
 * and increases memory management overhead.  Setting the guard area
 * size to zero hence trades off reliable behaviour on stack overflow
 * for performance. */
KUDA_DECLARE(kuda_status_t) kuda_threadattr_guardsize_set(kuda_threadattr_t *attr,
                                                       kuda_size_t guardsize);

/**
 * Create a new thread of execution
 * @param new_thread The newly created thread handle.
 * @param attr The threadattr to use to determine how to create the thread
 * @param func The function to start the new thread in
 * @param data Any data to be passed to the starting function
 * @param cont The pool to use
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_create(kuda_thread_t **new_thread, 
                                            kuda_threadattr_t *attr, 
                                            kuda_thread_start_t func, 
                                            void *data, kuda_pool_t *cont);

/**
 * stop the current thread
 * @param thd The thread to stop
 * @param retval The return value to pass back to any thread that cares
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_exit(kuda_thread_t *thd, 
                                          kuda_status_t retval);

/**
 * block until the desired thread stops executing.
 * @param retval The return value from the dead thread.
 * @param thd The thread to join
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_join(kuda_status_t *retval, 
                                          kuda_thread_t *thd); 

/**
 * force the current thread to yield the processor
 */
KUDA_DECLARE(void) kuda_thread_yield(void);

/**
 * Initialize the control variable for kuda_thread_once.  If this isn't
 * called, kuda_initialize won't work.
 * @param control The control variable to initialize
 * @param p The pool to allocate data from.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_once_init(kuda_thread_once_t **control,
                                               kuda_pool_t *p);

/**
 * Run the specified function one time, regardless of how many threads
 * call it.
 * @param control The control variable.  The same variable should
 *                be passed in each time the function is tried to be
 *                called.  This is how the underlying functions determine
 *                if the function has ever been called before.
 * @param func The function to call.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_once(kuda_thread_once_t *control,
                                          void (*func)(void));

/**
 * detach a thread
 * @param thd The thread to detach 
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_detach(kuda_thread_t *thd);

/**
 * Return user data associated with the current thread.
 * @param data The user data associated with the thread.
 * @param key The key to associate with the data
 * @param thread The currently open thread.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_data_get(void **data, const char *key,
                                             kuda_thread_t *thread);

/**
 * Set user data associated with the current thread.
 * @param data The user data to associate with the thread.
 * @param key The key to use for associating the data with the thread
 * @param cleanup The cleanup routine to use when the thread is destroyed.
 * @param thread The currently open thread.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_data_set(void *data, const char *key,
                                             kuda_status_t (*cleanup) (void *),
                                             kuda_thread_t *thread);

/**
 * Create and initialize a new thread private address space
 * @param key The thread private handle.
 * @param dest The destructor to use when freeing the private memory.
 * @param cont The pool to use
 */
KUDA_DECLARE(kuda_status_t) kuda_threadkey_private_create(kuda_threadkey_t **key, 
                                                    void (*dest)(void *),
                                                    kuda_pool_t *cont);

/**
 * Get a pointer to the thread private memory
 * @param new_mem The data stored in private memory 
 * @param key The handle for the desired thread private memory 
 */
KUDA_DECLARE(kuda_status_t) kuda_threadkey_private_get(void **new_mem, 
                                                 kuda_threadkey_t *key);

/**
 * Set the data to be stored in thread private memory
 * @param priv The data to be stored in private memory 
 * @param key The handle for the desired thread private memory 
 */
KUDA_DECLARE(kuda_status_t) kuda_threadkey_private_set(void *priv, 
                                                 kuda_threadkey_t *key);

/**
 * Free the thread private memory
 * @param key The handle for the desired thread private memory 
 */
KUDA_DECLARE(kuda_status_t) kuda_threadkey_private_delete(kuda_threadkey_t *key);

/**
 * Return the pool associated with the current threadkey.
 * @param data The user data associated with the threadkey.
 * @param key The key associated with the data
 * @param threadkey The currently open threadkey.
 */
KUDA_DECLARE(kuda_status_t) kuda_threadkey_data_get(void **data, const char *key,
                                                kuda_threadkey_t *threadkey);

/**
 * Return the pool associated with the current threadkey.
 * @param data The data to set.
 * @param key The key to associate with the data.
 * @param cleanup The cleanup routine to use when the file is destroyed.
 * @param threadkey The currently open threadkey.
 */
KUDA_DECLARE(kuda_status_t) kuda_threadkey_data_set(void *data, const char *key,
                                                kuda_status_t (*cleanup) (void *),
                                                kuda_threadkey_t *threadkey);

#endif

/**
 * Create and initialize a new procattr variable
 * @param new_attr The newly created procattr. 
 * @param cont The pool to use
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_create(kuda_procattr_t **new_attr,
                                                  kuda_pool_t *cont);

/**
 * Determine if any of stdin, stdout, or stderr should be linked to pipes 
 * when starting a child process.
 * @param attr The procattr we care about. 
 * @param in Should stdin be a pipe back to the parent?
 * @param out Should stdout be a pipe back to the parent?
 * @param err Should stderr be a pipe back to the parent?
 * @note If KUDA_NO_PIPE, there will be no special channel, the child
 * inherits the parent's corresponding stdio stream.  If KUDA_NO_FILE is 
 * specified, that corresponding stream is closed in the child (and will
 * be INVALID_HANDLE_VALUE when inspected on Win32). This can have ugly 
 * side effects, as the next file opened in the child on Unix will fall
 * into the stdio stream fd slot!
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_io_set(kuda_procattr_t *attr, 
                                             kuda_int32_t in, kuda_int32_t out,
                                             kuda_int32_t err);

/**
 * Set the child_in and/or parent_in values to existing kuda_file_t values.
 * @param attr The procattr we care about. 
 * @param child_in kuda_file_t value to use as child_in. Must be a valid file.
 * @param parent_in kuda_file_t value to use as parent_in. Must be a valid file.
 * @remark  This is NOT a required initializer function. This is
 *          useful if you have already opened a pipe (or multiple files)
 *          that you wish to use, perhaps persistently across multiple
 *          process invocations - such as a log file. You can save some 
 *          extra function calls by not creating your own pipe since this
 *          creates one in the process space for you.
 * @bug Note that calling this function with two NULL files on some platforms
 * creates an KUDA_FULL_BLOCK pipe, but this behavior is neither portable nor
 * is it supported.  @see kuda_procattr_io_set instead for simple pipes.
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_child_in_set(struct kuda_procattr_t *attr,
                                                  kuda_file_t *child_in,
                                                  kuda_file_t *parent_in);

/**
 * Set the child_out and parent_out values to existing kuda_file_t values.
 * @param attr The procattr we care about. 
 * @param child_out kuda_file_t value to use as child_out. Must be a valid file.
 * @param parent_out kuda_file_t value to use as parent_out. Must be a valid file.
 * @remark This is NOT a required initializer function. This is
 *         useful if you have already opened a pipe (or multiple files)
 *         that you wish to use, perhaps persistently across multiple
 *         process invocations - such as a log file. 
 * @bug Note that calling this function with two NULL files on some platforms
 * creates an KUDA_FULL_BLOCK pipe, but this behavior is neither portable nor
 * is it supported.  @see kuda_procattr_io_set instead for simple pipes.
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_child_out_set(struct kuda_procattr_t *attr,
                                                   kuda_file_t *child_out,
                                                   kuda_file_t *parent_out);

/**
 * Set the child_err and parent_err values to existing kuda_file_t values.
 * @param attr The procattr we care about. 
 * @param child_err kuda_file_t value to use as child_err. Must be a valid file.
 * @param parent_err kuda_file_t value to use as parent_err. Must be a valid file.
 * @remark This is NOT a required initializer function. This is
 *         useful if you have already opened a pipe (or multiple files)
 *         that you wish to use, perhaps persistently across multiple
 *         process invocations - such as a log file. 
 * @bug Note that calling this function with two NULL files on some platforms
 * creates an KUDA_FULL_BLOCK pipe, but this behavior is neither portable nor
 * is it supported.  @see kuda_procattr_io_set instead for simple pipes.
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_child_err_set(struct kuda_procattr_t *attr,
                                                   kuda_file_t *child_err,
                                                   kuda_file_t *parent_err);

/**
 * Set which directory the child process should start executing in.
 * @param attr The procattr we care about. 
 * @param dir Which dir to start in.  By default, this is the same dir as
 *            the parent currently resides in, when the createprocess call
 *            is made. 
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_dir_set(kuda_procattr_t *attr, 
                                              const char *dir);

/**
 * Set what type of command the child process will call.
 * @param attr The procattr we care about. 
 * @param cmd The type of command.  One of:
 * <PRE>
 *            KUDA_SHELLCMD     --  Anything that the shell can handle
 *            KUDA_PROGRAM      --  Executable program   (default) 
 *            KUDA_PROGRAM_ENV  --  Executable program, copy environment
 *            KUDA_PROGRAM_PATH --  Executable program on PATH, copy env
 * </PRE>
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_cmdtype_set(kuda_procattr_t *attr,
                                                  kuda_cmdtype_e cmd);

/**
 * Determine if the child should start in detached state.
 * @param attr The procattr we care about. 
 * @param detach Should the child start in detached state?  Default is no. 
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_detach_set(kuda_procattr_t *attr, 
                                                 kuda_int32_t detach);

#if KUDA_HAVE_STRUCT_RLIMIT
/**
 * Set the Resource Utilization limits when starting a new process.
 * @param attr The procattr we care about. 
 * @param what Which limit to set, one of:
 * <PRE>
 *                 KUDA_LIMIT_CPU
 *                 KUDA_LIMIT_MEM
 *                 KUDA_LIMIT_NPROC
 *                 KUDA_LIMIT_NOFILE
 * </PRE>
 * @param limit Value to set the limit to.
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_limit_set(kuda_procattr_t *attr, 
                                                kuda_int32_t what,
                                                struct rlimit *limit);
#endif

/**
 * Specify an error function to be called in the child process if KUDA
 * encounters an error in the child prior to running the specified program.
 * @param attr The procattr describing the child process to be created.
 * @param errfn The function to call in the child process.
 * @remark At the present time, it will only be called from kuda_proc_create()
 *         on platforms where fork() is used.  It will never be called on other
 *         platforms, on those platforms kuda_proc_create() will return the error
 *         in the parent process rather than invoke the callback in the now-forked
 *         child process.
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_child_errfn_set(kuda_procattr_t *attr,
                                                       kuda_child_errfn_t *errfn);

/**
 * Specify that kuda_proc_create() should do whatever it can to report
 * failures to the caller of kuda_proc_create(), rather than find out in
 * the child.
 * @param attr The procattr describing the child process to be created.
 * @param chk Flag to indicate whether or not extra work should be done
 *            to try to report failures to the caller.
 * @remark This flag only affects kuda_proc_create() on platforms where
 *         fork() is used.  This leads to extra overhead in the calling
 *         process, but that may help the application handle such
 *         errors more gracefully.
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_error_check_set(kuda_procattr_t *attr,
                                                       kuda_int32_t chk);

/**
 * Determine if the child should start in its own address space or using the 
 * current one from its parent
 * @param attr The procattr we care about. 
 * @param addrspace Should the child start in its own address space?  Default
 *                  is no on NetWare and yes on other platforms.
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_addrspace_set(kuda_procattr_t *attr,
                                                       kuda_int32_t addrspace);

/**
 * Set the username used for running process
 * @param attr The procattr we care about. 
 * @param username The username used
 * @param password User password if needed. Password is needed on WIN32
 *                 or any other platform having
 *                 KUDA_PROCATTR_USER_SET_REQUIRES_PASSWORD set.
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_user_set(kuda_procattr_t *attr,
                                                const char *username,
                                                const char *password);

/**
 * Set the group used for running process
 * @param attr The procattr we care about. 
 * @param groupname The group name  used
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_group_set(kuda_procattr_t *attr,
                                                 const char *groupname);


/**
 * Register permission set function
 * @param attr The procattr we care about. 
 * @param perms_set_fn Permission set callback
 * @param data Data to pass to permission callback function
 * @param perms Permissions to set
 */
KUDA_DECLARE(kuda_status_t) kuda_procattr_perms_set_register(kuda_procattr_t *attr,
                                                 kuda_perms_setfn_t *perms_set_fn,
                                                 void *data,
                                                 kuda_fileperms_t perms);

#if KUDA_HAS_FORK
/**
 * This is currently the only non-portable call in KUDA.  This executes 
 * a standard unix fork.
 * @param proc The resulting process handle. 
 * @param cont The pool to use. 
 * @remark returns KUDA_INCHILD for the child, and KUDA_INPARENT for the parent
 * or an error.
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_fork(kuda_proc_t *proc, kuda_pool_t *cont);
#endif

/**
 * Create a new process and execute a new program within that process.
 * @param new_proc The resulting process handle.
 * @param progname The program to run 
 * @param args the arguments to pass to the new program.  The first 
 *             one should be the program name.
 * @param env The new environment table for the new process.  This 
 *            should be a list of NULL-terminated strings. This argument
 *            is ignored for KUDA_PROGRAM_ENV, KUDA_PROGRAM_PATH, and
 *            KUDA_SHELLCMD_ENV types of commands.
 * @param attr the procattr we should use to determine how to create the new
 *         process
 * @param pool The pool to use.
 * @note This function returns without waiting for the new process to terminate;
 * use kuda_proc_wait for that.
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_create(kuda_proc_t *new_proc,
                                          const char *progname,
                                          const char * const *args,
                                          const char * const *env, 
                                          kuda_procattr_t *attr, 
                                          kuda_pool_t *pool);

/**
 * Wait for a child process to die
 * @param proc The process handle that corresponds to the desired child process 
 * @param exitcode The returned exit status of the child, if a child process 
 *                 dies, or the signal that caused the child to die.
 *                 On platforms that don't support obtaining this information, 
 *                 the status parameter will be returned as KUDA_ENOTIMPL.
 * @param exitwhy Why the child died, the bitwise or of:
 * <PRE>
 *            KUDA_PROC_EXIT         -- process terminated normally
 *            KUDA_PROC_SIGNAL       -- process was killed by a signal
 *            KUDA_PROC_SIGNAL_CORE  -- process was killed by a signal, and
 *                                     generated a core dump.
 * </PRE>
 * @param waithow How should we wait.  One of:
 * <PRE>
 *            KUDA_WAIT   -- block until the child process dies.
 *            KUDA_NOWAIT -- return immediately regardless of if the 
 *                          child is dead or not.
 * </PRE>
 * @remark The child's status is in the return code to this process.  It is one of:
 * <PRE>
 *            KUDA_CHILD_DONE     -- child is no longer running.
 *            KUDA_CHILD_NOTDONE  -- child is still running.
 * </PRE>
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_wait(kuda_proc_t *proc,
                                        int *exitcode, kuda_exit_why_e *exitwhy,
                                        kuda_wait_how_e waithow);

/**
 * Wait for any current child process to die and return information 
 * about that child.
 * @param proc Pointer to NULL on entry, will be filled out with child's 
 *             information 
 * @param exitcode The returned exit status of the child, if a child process 
 *                 dies, or the signal that caused the child to die.
 *                 On platforms that don't support obtaining this information, 
 *                 the status parameter will be returned as KUDA_ENOTIMPL.
 * @param exitwhy Why the child died, the bitwise or of:
 * <PRE>
 *            KUDA_PROC_EXIT         -- process terminated normally
 *            KUDA_PROC_SIGNAL       -- process was killed by a signal
 *            KUDA_PROC_SIGNAL_CORE  -- process was killed by a signal, and
 *                                     generated a core dump.
 * </PRE>
 * @param waithow How should we wait.  One of:
 * <PRE>
 *            KUDA_WAIT   -- block until the child process dies.
 *            KUDA_NOWAIT -- return immediately regardless of if the 
 *                          child is dead or not.
 * </PRE>
 * @param p Pool to allocate child information out of.
 * @bug Passing proc as a *proc rather than **proc was an odd choice
 * for some platforms... this should be revisited in 1.0
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_wait_all_procs(kuda_proc_t *proc,
                                                  int *exitcode,
                                                  kuda_exit_why_e *exitwhy,
                                                  kuda_wait_how_e waithow,
                                                  kuda_pool_t *p);

#define KUDA_PROC_DETACH_FOREGROUND 0    /**< Do not detach */
#define KUDA_PROC_DETACH_DAEMONIZE 1     /**< Detach */

/**
 * Detach the process from the controlling terminal.
 * @param daemonize set to non-zero if the process should daemonize
 *                  and become a background process, else it will
 *                  stay in the foreground.
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_detach(int daemonize);

/**
 * Register an other_child -- a child associated to its registered 
 * maintence callback.  This callback is invoked when the process
 * dies, is disconnected or disappears.
 * @param proc The child process to register.
 * @param maintenance maintenance is a function that is invoked with a 
 *                    reason and the data pointer passed here.
 * @param data Opaque context data passed to the maintenance function.
 * @param write_fd An fd that is probed for writing.  If it is ever unwritable
 *                 then the maintenance is invoked with reason 
 *                 OC_REASON_UNWRITABLE.
 * @param p The pool to use for allocating memory.
 * @bug write_fd duplicates the proc->out stream, it's really redundant
 * and should be replaced in the KUDA 1.0 API with a bitflag of which
 * proc->in/out/err handles should be health checked.
 * @bug no platform currently tests the pipes health.
 */
KUDA_DECLARE(void) kuda_proc_other_child_register(kuda_proc_t *proc, 
                                           void (*maintenance) (int reason, 
                                                                void *, 
                                                                int status),
                                           void *data, kuda_file_t *write_fd,
                                           kuda_pool_t *p);

/**
 * Stop watching the specified other child.  
 * @param data The data to pass to the maintenance function.  This is
 *             used to find the process to unregister.
 * @warning Since this can be called by a maintenance function while we're
 *          scanning the other_children list, all scanners should protect 
 *          themself by loading ocr->next before calling any maintenance 
 *          function.
 */
KUDA_DECLARE(void) kuda_proc_other_child_unregister(void *data);

/**
 * Notify the maintenance callback of a registered other child process
 * that application has detected an event, such as death.
 * @param proc The process to check
 * @param reason The reason code to pass to the maintenance function
 * @param status The status to pass to the maintenance function
 * @remark An example of code using this behavior;
 * <pre>
 * rv = kuda_proc_wait_all_procs(&proc, &exitcode, &status, KUDA_WAIT, p);
 * if (KUDA_STATUS_IS_CHILD_DONE(rv)) {
 * \#if KUDA_HAS_OTHER_CHILD
 *     if (kuda_proc_other_child_alert(&proc, KUDA_OC_REASON_DEATH, status)
 *             == KUDA_SUCCESS) {
 *         ;  (already handled)
 *     }
 *     else
 * \#endif
 *         [... handling non-otherchild processes death ...]
 * </pre>
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_other_child_alert(kuda_proc_t *proc, 
                                                     int reason,
                                                     int status);

/**
 * Test one specific other child processes and invoke the maintenance callback 
 * with the appropriate reason code, if still running, or the appropriate reason 
 * code if the process is no longer healthy.
 * @param ocr The registered other child
 * @param reason The reason code (e.g. KUDA_OC_REASON_RESTART) if still running
 */
KUDA_DECLARE(void) kuda_proc_other_child_refresh(kuda_other_child_rec_t *ocr,
                                               int reason);

/**
 * Test all registered other child processes and invoke the maintenance callback 
 * with the appropriate reason code, if still running, or the appropriate reason 
 * code if the process is no longer healthy.
 * @param reason The reason code (e.g. KUDA_OC_REASON_RESTART) to running processes
 */
KUDA_DECLARE(void) kuda_proc_other_child_refresh_all(int reason);

/** 
 * Terminate a process.
 * @param proc The process to terminate.
 * @param sig How to kill the process.
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_kill(kuda_proc_t *proc, int sig);

/**
 * Register a process to be killed when a pool dies.
 * @param a The pool to use to define the processes lifetime 
 * @param proc The process to register
 * @param how How to kill the process, one of:
 * <PRE>
 *         KUDA_KILL_NEVER         -- process is never sent any signals
 *         KUDA_KILL_ALWAYS        -- process is sent SIGKILL on kuda_pool_t cleanup
 *         KUDA_KILL_AFTER_TIMEOUT -- SIGTERM, wait 3 seconds, SIGKILL
 *         KUDA_JUST_WAIT          -- wait forever for the process to complete
 *         KUDA_KILL_ONLY_ONCE     -- send SIGTERM and then wait
 * </PRE>
 */
KUDA_DECLARE(void) kuda_pool_note_subprocess(kuda_pool_t *a, kuda_proc_t *proc,
                                           kuda_kill_conditions_e how);

#if KUDA_HAS_THREADS 

#if (KUDA_HAVE_SIGWAIT || KUDA_HAVE_SIGSUSPEND) && !defined(OS2)

/**
 * Setup the process for a single thread to be used for all signal handling.
 * @warning This must be called before any threads are created
 */
KUDA_DECLARE(kuda_status_t) kuda_setup_signal_thread(void);

/**
 * Make the current thread listen for signals.  This thread will loop
 * forever, calling a provided function whenever it receives a signal.  That
 * functions should return 1 if the signal has been handled, 0 otherwise.
 * @param signal_handler The function to call when a signal is received
 * kuda_status_t kuda_signal_thread((int)(*signal_handler)(int signum))
 * @note Synchronous signals like SIGABRT/SIGSEGV/SIGBUS/... are ignored by
 * kuda_signal_thread() and thus can't be waited by this function (they remain
 * handled by the operating system or its native signals interface).
 * @remark In KUDA version 1.6 and ealier, SIGUSR2 was part of these ignored
 * signals and thus was never passed in to the signal_handler. From KUDA 1.7
 * this is no more the case so SIGUSR2 can be handled in signal_handler and
 * acted upon like the other asynchronous signals.
 */
KUDA_DECLARE(kuda_status_t) kuda_signal_thread(int(*signal_handler)(int signum));

#endif /* (KUDA_HAVE_SIGWAIT || KUDA_HAVE_SIGSUSPEND) && !defined(OS2) */

/**
 * Get the child-pool used by the thread from the thread info.
 * @return kuda_pool_t the pool
 */
KUDA_POOL_DECLARE_ACCESSOR(thread);

#endif /* KUDA_HAS_THREADS */

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_THREAD_PROC_H */

