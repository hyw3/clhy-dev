/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_FILE_INFO_H
#define KUDA_FILE_INFO_H

/**
 * @file kuda_file_info.h
 * @brief KUDA File Information
 */

#include "kuda.h"
#include "kuda_user.h"
#include "kuda_pools.h"
#include "kuda_tables.h"
#include "kuda_time.h"
#include "kuda_errno.h"

#if KUDA_HAVE_SYS_UIO_H
#include <sys/uio.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_file_info File Information
 * @ingroup KUDA 
 * @{
 */

/* Many applications use the type member to determine the
 * existance of a file or initialization of the file info,
 * so the KUDA_NOFILE value must be distinct from KUDA_UNKFILE.
 */

/** kuda_filetype_e values for the filetype member of the 
 * kuda_file_info_t structure
 * @warning Not all of the filetypes below can be determined.
 * For example, a given platform might not correctly report 
 * a socket descriptor as KUDA_SOCK if that type isn't 
 * well-identified on that platform.  In such cases where
 * a filetype exists but cannot be described by the recognized
 * flags below, the filetype will be KUDA_UNKFILE.  If the
 * filetype member is not determined, the type will be KUDA_NOFILE.
 */

typedef enum {
    KUDA_NOFILE = 0,     /**< no file type determined */
    KUDA_REG,            /**< a regular file */
    KUDA_DIR,            /**< a directory */
    KUDA_CHR,            /**< a character device */
    KUDA_BLK,            /**< a block device */
    KUDA_PIPE,           /**< a FIFO / pipe */
    KUDA_LNK,            /**< a symbolic link */
    KUDA_SOCK,           /**< a [unix domain] socket */
    KUDA_UNKFILE = 127   /**< a file of some other unknown type */
} kuda_filetype_e; 

/**
 * @defgroup kuda_file_permissions File Permissions flags 
 * @{
 */

#define KUDA_FPROT_USETID      0x8000 /**< Set user id */
#define KUDA_FPROT_UREAD       0x0400 /**< Read by user */
#define KUDA_FPROT_UWRITE      0x0200 /**< Write by user */
#define KUDA_FPROT_UEXECUTE    0x0100 /**< Execute by user */

#define KUDA_FPROT_GSETID      0x4000 /**< Set group id */
#define KUDA_FPROT_GREAD       0x0040 /**< Read by group */
#define KUDA_FPROT_GWRITE      0x0020 /**< Write by group */
#define KUDA_FPROT_GEXECUTE    0x0010 /**< Execute by group */

#define KUDA_FPROT_WSTICKY     0x2000 /**< Sticky bit */
#define KUDA_FPROT_WREAD       0x0004 /**< Read by others */
#define KUDA_FPROT_WWRITE      0x0002 /**< Write by others */
#define KUDA_FPROT_WEXECUTE    0x0001 /**< Execute by others */

#define KUDA_FPROT_PLATFORM_DEFAULT  0x0FFF /**< use PLATFORM's default permissions */

/* additional permission flags for kuda_file_copy  and kuda_file_append */
#define KUDA_FPROT_FILE_SOURCE_PERMS 0x1000 /**< Copy source file's permissions */
    
/* backcompat */
#define KUDA_USETID     KUDA_FPROT_USETID     /**< @deprecated @see KUDA_FPROT_USETID     */
#define KUDA_UREAD      KUDA_FPROT_UREAD      /**< @deprecated @see KUDA_FPROT_UREAD      */
#define KUDA_UWRITE     KUDA_FPROT_UWRITE     /**< @deprecated @see KUDA_FPROT_UWRITE     */
#define KUDA_UEXECUTE   KUDA_FPROT_UEXECUTE   /**< @deprecated @see KUDA_FPROT_UEXECUTE   */
#define KUDA_GSETID     KUDA_FPROT_GSETID     /**< @deprecated @see KUDA_FPROT_GSETID     */
#define KUDA_GREAD      KUDA_FPROT_GREAD      /**< @deprecated @see KUDA_FPROT_GREAD      */
#define KUDA_GWRITE     KUDA_FPROT_GWRITE     /**< @deprecated @see KUDA_FPROT_GWRITE     */
#define KUDA_GEXECUTE   KUDA_FPROT_GEXECUTE   /**< @deprecated @see KUDA_FPROT_GEXECUTE   */
#define KUDA_WSTICKY    KUDA_FPROT_WSTICKY    /**< @deprecated @see KUDA_FPROT_WSTICKY    */
#define KUDA_WREAD      KUDA_FPROT_WREAD      /**< @deprecated @see KUDA_FPROT_WREAD      */
#define KUDA_WWRITE     KUDA_FPROT_WWRITE     /**< @deprecated @see KUDA_FPROT_WWRITE     */
#define KUDA_WEXECUTE   KUDA_FPROT_WEXECUTE   /**< @deprecated @see KUDA_FPROT_WEXECUTE   */
#define KUDA_PLATFORM_DEFAULT KUDA_FPROT_PLATFORM_DEFAULT /**< @deprecated @see KUDA_FPROT_PLATFORM_DEFAULT */
#define KUDA_FILE_SOURCE_PERMS KUDA_FPROT_FILE_SOURCE_PERMS /**< @deprecated @see KUDA_FPROT_FILE_SOURCE_PERMS */
    
/** @} */


/**
 * Structure for referencing directories.
 */
typedef struct kuda_dir_t          kuda_dir_t;
/**
 * Structure for determining file permissions.
 */
typedef kuda_int32_t               kuda_fileperms_t;
#if (defined WIN32) || (defined NETWARE)
/**
 * Structure for determining the device the file is on.
 */
typedef kuda_uint32_t              kuda_dev_t;
#else
/**
 * Structure for determining the device the file is on.
 */
typedef dev_t                     kuda_dev_t;
#endif

/**
 * @defgroup kuda_file_stat Stat Functions
 * @{
 */
/** file info structure */
typedef struct kuda_finfo_t        kuda_finfo_t;

#define KUDA_FINFO_LINK   0x00000001 /**< Stat the link not the file itself if it is a link */
#define KUDA_FINFO_MTIME  0x00000010 /**< Modification Time */
#define KUDA_FINFO_CTIME  0x00000020 /**< Creation or inode-changed time */
#define KUDA_FINFO_ATIME  0x00000040 /**< Access Time */
#define KUDA_FINFO_SIZE   0x00000100 /**< Size of the file */
#define KUDA_FINFO_CSIZE  0x00000200 /**< Storage size consumed by the file */
#define KUDA_FINFO_DEV    0x00001000 /**< Device */
#define KUDA_FINFO_INODE  0x00002000 /**< Inode */
#define KUDA_FINFO_NLINK  0x00004000 /**< Number of links */
#define KUDA_FINFO_TYPE   0x00008000 /**< Type */
#define KUDA_FINFO_USER   0x00010000 /**< User */
#define KUDA_FINFO_GROUP  0x00020000 /**< Group */
#define KUDA_FINFO_UPROT  0x00100000 /**< User protection bits */
#define KUDA_FINFO_GPROT  0x00200000 /**< Group protection bits */
#define KUDA_FINFO_WPROT  0x00400000 /**< World protection bits */
#define KUDA_FINFO_ICASE  0x01000000 /**< if dev is case insensitive */
#define KUDA_FINFO_NAME   0x02000000 /**< ->name in proper case */

#define KUDA_FINFO_MIN    0x00008170 /**< type, mtime, ctime, atime, size */
#define KUDA_FINFO_IDENT  0x00003000 /**< dev and inode */
#define KUDA_FINFO_OWNER  0x00030000 /**< user and group */
#define KUDA_FINFO_PROT   0x00700000 /**<  all protections */
#define KUDA_FINFO_NORM   0x0073b170 /**<  an atomic unix kuda_stat() */
#define KUDA_FINFO_DIRENT 0x02000000 /**<  an atomic unix kuda_dir_read() */

/**
 * The file information structure.  This is analogous to the POSIX
 * stat structure.
 */
struct kuda_finfo_t {
    /** Allocates memory and closes lingering handles in the specified pool */
    kuda_pool_t *pool;
    /** The bitmask describing valid fields of this kuda_finfo_t structure 
     *  including all available 'wanted' fields and potentially more */
    kuda_int32_t valid;
    /** The access permissions of the file.  Mimics Unix access rights. */
    kuda_fileperms_t protection;
    /** The type of file.  One of KUDA_REG, KUDA_DIR, KUDA_CHR, KUDA_BLK, KUDA_PIPE, 
     * KUDA_LNK or KUDA_SOCK.  If the type is undetermined, the value is KUDA_NOFILE.
     * If the type cannot be determined, the value is KUDA_UNKFILE.
     */
    kuda_filetype_e filetype;
    /** The user id that owns the file */
    kuda_uid_t user;
    /** The group id that owns the file */
    kuda_gid_t group;
    /** The inode of the file. */
    kuda_ino_t inode;
    /** The id of the device the file is on. */
    kuda_dev_t device;
    /** The number of hard links to the file. */
    kuda_int32_t nlink;
    /** The size of the file */
    kuda_off_t size;
    /** The storage size consumed by the file */
    kuda_off_t csize;
    /** The time the file was last accessed */
    kuda_time_t atime;
    /** The time the file was last modified */
    kuda_time_t mtime;
    /** The time the file was created, or the inode was last changed */
    kuda_time_t ctime;
    /** The pathname of the file (possibly unrooted) */
    const char *fname;
    /** The file's name (no path) in filesystem case */
    const char *name;
    /** Unused */
    struct kuda_file_t *filehand;
};

/**
 * get the specified file's stats.  The file is specified by filename, 
 * instead of using a pre-opened file.
 * @param finfo Where to store the information about the file, which is
 * never touched if the call fails.
 * @param fname The name of the file to stat.
 * @param wanted The desired kuda_finfo_t fields, as a bit flag of KUDA_FINFO_
                 values 
 * @param pool the pool to use to allocate the new file. 
 *
 * @note If @c KUDA_INCOMPLETE is returned all the fields in @a finfo may
 *       not be filled in, and you need to check the @c finfo->valid bitmask
 *       to verify that what you're looking for is there.
 */ 
KUDA_DECLARE(kuda_status_t) kuda_stat(kuda_finfo_t *finfo, const char *fname,
                                   kuda_int32_t wanted, kuda_pool_t *pool);

/** @} */
/**
 * @defgroup kuda_dir Directory Manipulation Functions
 * @{
 */

/**
 * Open the specified directory.
 * @param new_dir The opened directory descriptor.
 * @param dirname The full path to the directory (use / on all systems)
 * @param pool The pool to use.
 */                        
KUDA_DECLARE(kuda_status_t) kuda_dir_open(kuda_dir_t **new_dir, 
                                       const char *dirname, 
                                       kuda_pool_t *pool);

/**
 * close the specified directory. 
 * @param thedir the directory descriptor to close.
 */                        
KUDA_DECLARE(kuda_status_t) kuda_dir_close(kuda_dir_t *thedir);

/**
 * Read the next entry from the specified directory. 
 * @param finfo the file info structure and filled in by kuda_dir_read
 * @param wanted The desired kuda_finfo_t fields, as a bit flag of KUDA_FINFO_
                 values 
 * @param thedir the directory descriptor returned from kuda_dir_open
 * @remark No ordering is guaranteed for the entries read.
 *
 * @note If @c KUDA_INCOMPLETE is returned all the fields in @a finfo may
 *       not be filled in, and you need to check the @c finfo->valid bitmask
 *       to verify that what you're looking for is there. When no more
 *       entries are available, KUDA_ENOENT is returned.
 */                        
KUDA_DECLARE(kuda_status_t) kuda_dir_read(kuda_finfo_t *finfo, kuda_int32_t wanted,
                                       kuda_dir_t *thedir);

/**
 * Rewind the directory to the first entry.
 * @param thedir the directory descriptor to rewind.
 */                        
KUDA_DECLARE(kuda_status_t) kuda_dir_rewind(kuda_dir_t *thedir);
/** @} */

/**
 * @defgroup kuda_filepath Filepath Manipulation Functions
 * @{
 */

/** Cause kuda_filepath_merge to fail if addpath is above rootpath 
 * @bug in KUDA 0.9 and 1.x, this flag's behavior is undefined
 * if the rootpath is NULL or empty.  In KUDA 2.0 this should be
 * changed to imply NOTABSOLUTE if the rootpath is NULL or empty.
 */
#define KUDA_FILEPATH_NOTABOVEROOT   0x01

/** internal: Only meaningful with KUDA_FILEPATH_NOTABOVEROOT */
#define KUDA_FILEPATH_SECUREROOTTEST 0x02

/** Cause kuda_filepath_merge to fail if addpath is above rootpath,
 * even given a rootpath /foo/bar and an addpath ../bar/bash
 */
#define KUDA_FILEPATH_SECUREROOT     0x03

/** Fail kuda_filepath_merge if the merged path is relative */
#define KUDA_FILEPATH_NOTRELATIVE    0x04

/** Fail kuda_filepath_merge if the merged path is absolute */
#define KUDA_FILEPATH_NOTABSOLUTE    0x08

/** Return the file system's native path format (e.g. path delimiters
 * of ':' on MacOS9, '\' on Win32, etc.) */
#define KUDA_FILEPATH_NATIVE         0x10

/** Resolve the true case of existing directories and file elements
 * of addpath, (resolving any aliases on Win32) and append a proper 
 * trailing slash if a directory
 */
#define KUDA_FILEPATH_TRUENAME       0x20

/**
 * Extract the rootpath from the given filepath
 * @param rootpath the root file path returned with KUDA_SUCCESS or KUDA_EINCOMPLETE
 * @param filepath the pathname to parse for its root component
 * @param flags the desired rules to apply, from
 * <PRE>
 *      KUDA_FILEPATH_NATIVE    Use native path separators (e.g. '\' on Win32)
 *      KUDA_FILEPATH_TRUENAME  Tests that the root exists, and makes it proper
 * </PRE>
 * @param p the pool to allocate the new path string from
 * @remark on return, filepath points to the first non-root character in the
 * given filepath.  In the simplest example, given a filepath of "/foo", 
 * returns the rootpath of "/" and filepath points at "foo".  This is far 
 * more complex on other platforms, which will canonicalize the root form
 * to a consistant format, given the KUDA_FILEPATH_TRUENAME flag, and also
 * test for the validity of that root (e.g., that a drive d:/ or network 
 * share //machine/foovol/). 
 * The function returns KUDA_ERELATIVE if filepath isn't rooted (an
 * error), KUDA_EINCOMPLETE if the root path is ambiguous (but potentially
 * legitimate, e.g. "/" on Windows is incomplete because it doesn't specify
 * the drive letter), or KUDA_EBADPATH if the root is simply invalid.
 * KUDA_SUCCESS is returned if filepath is an absolute path.
 */
KUDA_DECLARE(kuda_status_t) kuda_filepath_root(const char **rootpath, 
                                            const char **filepath, 
                                            kuda_int32_t flags,
                                            kuda_pool_t *p);

/**
 * Merge additional file path onto the previously processed rootpath
 * @param newpath the merged paths returned
 * @param rootpath the root file path (NULL uses the current working path)
 * @param addpath the path to add to the root path
 * @param flags the desired KUDA_FILEPATH_ rules to apply when merging
 * @param p the pool to allocate the new path string from
 * @remark if the flag KUDA_FILEPATH_TRUENAME is given, and the addpath 
 * contains wildcard characters ('*', '?') on platforms that don't support 
 * such characters within filenames, the paths will be merged, but the 
 * result code will be KUDA_EPATHWILD, and all further segments will not
 * reflect the true filenames including the wildcard and following segments.
 */                        
KUDA_DECLARE(kuda_status_t) kuda_filepath_merge(char **newpath, 
                                             const char *rootpath,
                                             const char *addpath, 
                                             kuda_int32_t flags,
                                             kuda_pool_t *p);

/**
 * Split a search path into separate components
 * @param pathelts the returned components of the search path
 * @param liststr the search path (e.g., <tt>getenv("PATH")</tt>)
 * @param p the pool to allocate the array and path components from
 * @remark empty path components do not become part of @a pathelts.
 * @remark the path separator in @a liststr is system specific;
 * e.g., ':' on Unix, ';' on Windows, etc.
 */
KUDA_DECLARE(kuda_status_t) kuda_filepath_list_split(kuda_array_header_t **pathelts,
                                                  const char *liststr,
                                                  kuda_pool_t *p);

/**
 * Merge a list of search path components into a single search path
 * @param liststr the returned search path; may be NULL if @a pathelts is empty
 * @param pathelts the components of the search path
 * @param p the pool to allocate the search path from
 * @remark emtpy strings in the source array are ignored.
 * @remark the path separator in @a liststr is system specific;
 * e.g., ':' on Unix, ';' on Windows, etc.
 */
KUDA_DECLARE(kuda_status_t) kuda_filepath_list_merge(char **liststr,
                                                  kuda_array_header_t *pathelts,
                                                  kuda_pool_t *p);

/**
 * Return the default file path (for relative file names)
 * @param path the default path string returned
 * @param flags optional flag KUDA_FILEPATH_NATIVE to retrieve the
 *              default file path in platforms-native format.
 * @param p the pool to allocate the default path string from
 */
KUDA_DECLARE(kuda_status_t) kuda_filepath_get(char **path, kuda_int32_t flags,
                                           kuda_pool_t *p);

/**
 * Set the default file path (for relative file names)
 * @param path the default path returned
 * @param p the pool to allocate any working storage
 */
KUDA_DECLARE(kuda_status_t) kuda_filepath_set(const char *path, kuda_pool_t *p);

/** The FilePath character encoding is unknown */
#define KUDA_FILEPATH_ENCODING_UNKNOWN  0

/** The FilePath character encoding is locale-dependent */
#define KUDA_FILEPATH_ENCODING_LOCALE   1

/** The FilePath character encoding is UTF-8 */
#define KUDA_FILEPATH_ENCODING_UTF8     2

/**
 * Determine the encoding used internally by the FilePath functions
 * @param style points to a variable which receives the encoding style flag
 * @param p the pool to allocate any working storage
 * @remark Use kuda_platform_locale_encoding() and/or kuda_platform_default_encoding()
 * to get the name of the path encoding if it's not UTF-8.
 */
KUDA_DECLARE(kuda_status_t) kuda_filepath_encoding(int *style, kuda_pool_t *p);
/** @} */

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_FILE_INFO_H */
