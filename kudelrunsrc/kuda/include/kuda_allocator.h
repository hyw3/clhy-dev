/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_ALLOCATOR_H
#define KUDA_ALLOCATOR_H

/**
 * @file kuda_allocator.h
 * @brief KUDA Internal Memory Allocation
 */

#include "kuda.h"
#include "kuda_errno.h"
#define KUDA_WANT_MEMFUNC /**< For no good reason? */
#include "kuda_want.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup kuda_allocator Internal Memory Allocation
 * @ingroup KUDA 
 * @{
 */

/** the allocator structure */
typedef struct kuda_allocator_t kuda_allocator_t;
/** the structure which holds information about the allocation */
typedef struct kuda_memnode_t kuda_memnode_t;

/** basic memory node structure
 * @note The next, ref and first_avail fields are available for use by the
 *       caller of kuda_allocator_alloc(), the remaining fields are read-only.
 *       The next field has to be used with caution and sensibly set when the
 *       memnode is passed back to kuda_allocator_free().  See kuda_allocator_free()
 *       for details.  
 *       The ref and first_avail fields will be properly restored by
 *       kuda_allocator_free().
 */
struct kuda_memnode_t {
    kuda_memnode_t *next;            /**< next memnode */
    kuda_memnode_t **ref;            /**< reference to self */
    kuda_uint32_t   index;           /**< size */
    kuda_uint32_t   free_index;      /**< how much free */
    char          *first_avail;     /**< pointer to first free memory */
    char          *endp;            /**< pointer to end of free memory */
};

/** The base size of a memory node - aligned.  */
#define KUDA_MEMNODE_T_SIZE KUDA_ALIGN_DEFAULT(sizeof(kuda_memnode_t))

/** Symbolic constants */
#define KUDA_ALLOCATOR_MAX_FREE_UNLIMITED 0

/**
 * Create a new allocator
 * @param allocator The allocator we have just created.
 *
 */
KUDA_DECLARE(kuda_status_t) kuda_allocator_create(kuda_allocator_t **allocator)
                          __attribute__((nonnull(1)));

/**
 * Destroy an allocator
 * @param allocator The allocator to be destroyed
 * @remark Any memnodes not given back to the allocator prior to destroying
 *         will _not_ be free()d.
 */
KUDA_DECLARE(void) kuda_allocator_destroy(kuda_allocator_t *allocator)
                  __attribute__((nonnull(1)));

/**
 * Allocate a block of mem from the allocator
 * @param allocator The allocator to allocate from
 * @param size The size of the mem to allocate (excluding the
 *        memnode structure)
 */
KUDA_DECLARE(kuda_memnode_t *) kuda_allocator_alloc(kuda_allocator_t *allocator,
                                                 kuda_size_t size)
                             __attribute__((nonnull(1)));

/**
 * Free a list of blocks of mem, giving them back to the allocator.
 * The list is typically terminated by a memnode with its next field
 * set to NULL.
 * @param allocator The allocator to give the mem back to
 * @param memnode The memory node to return
 */
KUDA_DECLARE(void) kuda_allocator_free(kuda_allocator_t *allocator,
                                     kuda_memnode_t *memnode)
                  __attribute__((nonnull(1,2)));
 
/**
 * Get the true size that would be allocated for the given size (including
 * the header and alignment).
 * @param allocator The allocator from which to the memory would be allocated
 * @param size The size to align
 * @return The aligned size (or zero on kuda_size_t overflow)
 */
KUDA_DECLARE(kuda_size_t) kuda_allocator_align(kuda_allocator_t *allocator,
                                            kuda_size_t size);

#include "kuda_pools.h"

/**
 * Set the owner of the allocator
 * @param allocator The allocator to set the owner for
 * @param pool The pool that is to own the allocator
 * @remark Typically pool is the highest level pool using the allocator
 */
/*
 * XXX: see if we can come up with something a bit better.  Currently
 * you can make a pool an owner, but if the pool doesn't use the allocator
 * the allocator will never be destroyed.
 */
KUDA_DECLARE(void) kuda_allocator_owner_set(kuda_allocator_t *allocator,
                                          kuda_pool_t *pool)
                  __attribute__((nonnull(1)));

/**
 * Get the current owner of the allocator
 * @param allocator The allocator to get the owner from
 */
KUDA_DECLARE(kuda_pool_t *) kuda_allocator_owner_get(kuda_allocator_t *allocator)
                          __attribute__((nonnull(1)));

/**
 * Set the current threshold at which the allocator should start
 * giving blocks back to the system.
 * @param allocator The allocator to set the threshold on
 * @param size The threshold.  0 == unlimited.
 */
KUDA_DECLARE(void) kuda_allocator_max_free_set(kuda_allocator_t *allocator,
                                             kuda_size_t size)
                  __attribute__((nonnull(1)));

#include "kuda_thread_mutex.h"

#if KUDA_HAS_THREADS
/**
 * Set a mutex for the allocator to use
 * @param allocator The allocator to set the mutex for
 * @param mutex The mutex
 */
KUDA_DECLARE(void) kuda_allocator_mutex_set(kuda_allocator_t *allocator,
                                          kuda_thread_mutex_t *mutex)
                  __attribute__((nonnull(1)));

/**
 * Get the mutex currently set for the allocator
 * @param allocator The allocator
 */
KUDA_DECLARE(kuda_thread_mutex_t *) kuda_allocator_mutex_get(
                                          kuda_allocator_t *allocator)
                                  __attribute__((nonnull(1)));

#endif /* KUDA_HAS_THREADS */

/** @} */

#ifdef __cplusplus
}
#endif

#endif /* !KUDA_ALLOCATOR_H */
