/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_PROC_MUTEX_H
#define KUDA_PROC_MUTEX_H

/**
 * @file kuda_proc_mutex.h
 * @brief KUDA Process Locking Routines
 */

#include "kuda.h"
#include "kuda_pools.h"
#include "kuda_errno.h"
#include "kuda_perms_set.h"
#include "kuda_time.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_proc_mutex Process Locking Routines
 * @ingroup KUDA 
 * @{
 */

/** 
 * Enumerated potential types for KUDA process locking methods
 * @warning Check KUDA_HAS_foo_SERIALIZE defines to see if the platform supports
 *          KUDA_LOCK_foo.  Only KUDA_LOCK_DEFAULT is portable.
 */
typedef enum {
    KUDA_LOCK_FCNTL,         /**< fcntl() */
    KUDA_LOCK_FLOCK,         /**< flock() */
    KUDA_LOCK_SYSVSEM,       /**< System V Semaphores */
    KUDA_LOCK_PROC_PTHREAD,  /**< POSIX pthread process-based locking */
    KUDA_LOCK_POSIXSEM,      /**< POSIX semaphore process-based locking */
    KUDA_LOCK_DEFAULT,       /**< Use the default process lock */
    KUDA_LOCK_DEFAULT_TIMED  /**< Use the default process timed lock */
} kuda_lockmech_e;

/** Opaque structure representing a process mutex. */
typedef struct kuda_proc_mutex_t kuda_proc_mutex_t;

/*   Function definitions */

/**
 * Create and initialize a mutex that can be used to synchronize processes.
 * @param mutex the memory address where the newly created mutex will be
 *        stored.
 * @param fname A file name to use if the lock mechanism requires one.  This
 *        argument should always be provided.  The lock code itself will
 *        determine if it should be used.
 * @param mech The mechanism to use for the interprocess lock, if any; one of
 * <PRE>
 *            KUDA_LOCK_FCNTL
 *            KUDA_LOCK_FLOCK
 *            KUDA_LOCK_SYSVSEM
 *            KUDA_LOCK_POSIXSEM
 *            KUDA_LOCK_PROC_PTHREAD
 *            KUDA_LOCK_DEFAULT     pick the default mechanism for the platform
 * </PRE>
 * @param pool the pool from which to allocate the mutex.
 * @see kuda_lockmech_e
 * @warning Check KUDA_HAS_foo_SERIALIZE defines to see if the platform supports
 *          KUDA_LOCK_foo.  Only KUDA_LOCK_DEFAULT is portable.
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_create(kuda_proc_mutex_t **mutex,
                                                const char *fname,
                                                kuda_lockmech_e mech,
                                                kuda_pool_t *pool);

/**
 * Re-open a mutex in a child process.
 * @param mutex The newly re-opened mutex structure.
 * @param fname A file name to use if the mutex mechanism requires one.  This
 *              argument should always be provided.  The mutex code itself will
 *              determine if it should be used.  This filename should be the 
 *              same one that was passed to kuda_proc_mutex_create().
 * @param pool The pool to operate on.
 * @remark This function must be called to maintain portability, even
 *         if the underlying lock mechanism does not require it.
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_child_init(kuda_proc_mutex_t **mutex,
                                                    const char *fname,
                                                    kuda_pool_t *pool);

/**
 * Acquire the lock for the given mutex. If the mutex is already locked,
 * the current thread will be put to sleep until the lock becomes available.
 * @param mutex the mutex on which to acquire the lock.
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_lock(kuda_proc_mutex_t *mutex);

/**
 * Attempt to acquire the lock for the given mutex. If the mutex has already
 * been acquired, the call returns immediately with KUDA_EBUSY. Note: it
 * is important that the KUDA_STATUS_IS_EBUSY(s) macro be used to determine
 * if the return value was KUDA_EBUSY, for portability reasons.
 * @param mutex the mutex on which to attempt the lock acquiring.
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_trylock(kuda_proc_mutex_t *mutex);

/**
 * Attempt to acquire the lock for the given mutex until timeout expires.
 * If the acquisition time outs, the call returns with KUDA_TIMEUP.
 * @param mutex the mutex on which to attempt the lock acquiring.
 * @param timeout the relative timeout (microseconds).
 * @note A negative or nul timeout means immediate attempt, returning
 *       KUDA_TIMEUP without blocking if it the lock is already acquired.
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_timedlock(kuda_proc_mutex_t *mutex,
                                               kuda_interval_time_t timeout);

/**
 * Release the lock for the given mutex.
 * @param mutex the mutex from which to release the lock.
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_unlock(kuda_proc_mutex_t *mutex);

/**
 * Destroy the mutex and free the memory associated with the lock.
 * @param mutex the mutex to destroy.
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_destroy(kuda_proc_mutex_t *mutex);

/**
 * Destroy the mutex and free the memory associated with the lock.
 * @param mutex the mutex to destroy.
 * @note This function is generally used to kill a cleanup on an already
 *       created mutex
 */
KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_cleanup(void *mutex);

/**
 * Return the name of the lockfile for the mutex, or NULL
 * if the mutex doesn't use a lock file
 */

KUDA_DECLARE(const char *) kuda_proc_mutex_lockfile(kuda_proc_mutex_t *mutex);

/**
 * Get the mechanism of the mutex, as it relates to the actual method
 * used for the underlying kuda_proc_mutex_t.
 * @param mutex the mutex to get the mechanism from.
 */
KUDA_DECLARE(kuda_lockmech_e) kuda_proc_mutex_mech(kuda_proc_mutex_t *mutex);

/**
 * Get the mechanism's name of the mutex, as it relates to the actual method
 * used for the underlying kuda_proc_mutex_t.
 * @param mutex the mutex to get the mechanism's name from.
 */
KUDA_DECLARE(const char *) kuda_proc_mutex_name(kuda_proc_mutex_t *mutex);

/**
 * Display the name of the default mutex: KUDA_LOCK_DEFAULT
 */
KUDA_DECLARE(const char *) kuda_proc_mutex_defname(void);

/**
 * Set mutex permissions.
 */
KUDA_PERMS_SET_IMPLEMENT(proc_mutex);

/**
 * Get the pool used by this proc_mutex.
 * @return kuda_pool_t the pool
 */
KUDA_POOL_DECLARE_ACCESSOR(proc_mutex);

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_PROC_MUTEX_H */
