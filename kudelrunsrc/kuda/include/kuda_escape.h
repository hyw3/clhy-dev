/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @file kuda_escape.h
 * @brief Kuda-Delman Escaping
 */
#ifndef KUDA_ESCAPE_H
#define KUDA_ESCAPE_H
#include "kuda.h"
#include "kuda_general.h"
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup KUDA_Util_Escaping Escape functions
 * @ingroup KUDA
 * @{
 */

/* Simple escape/unescape functions.
 *
 * The design goal of these functions are:
 *
 * - Avoid unnecessary work.
 *
 * In most cases the strings passed in do not need to be escaped at all. In
 * these cases the original string will be returned.
 *
 * - Lowest possible memory footprint.
 *
 * The amount of memory allocated for a given encoding is calculated based
 * on the exact amount of memory needed, and not the theoretical worst case
 * scenario.
 *
 */

/**
 * When passing a string to one of the escape functions, this value can be
 * passed to indicate a string-valued key, and have the length computed
 * automatically.
 */
#define KUDA_ESCAPE_STRING      (-1)

/**
 * Apply LDAP distinguished name escaping as per RFC4514.
 */
#define KUDA_ESCAPE_LDAP_DN     (0x01)

/**
 * Apply LDAP filter escaping as per RFC4515.
 */
#define KUDA_ESCAPE_LDAP_FILTER (0x02)

/**
 * Apply both RFC4514 and RFC4515 LDAP escaping.
 */
#define KUDA_ESCAPE_LDAP_ALL    (0x03)

/**
 * Perform shell escaping on the provided string.
 * 
 * Shell escaping causes characters to be prefixed with a '\' character.
 * @param escaped Optional buffer to write the encoded string, can be
 * NULL
 * @param str The original string
 * @param slen The length of the original string, or KUDA_ESCAPE_STRING
 * @param len If present, returns the length of the string
 * @return KUDA_SUCCESS, or KUDA_NOTFOUND if no changes to the string were
 * detected or the string was NULL
 */
KUDA_DECLARE(kuda_status_t) kuda_escape_shell(char *escaped, const char *str,
        kuda_ssize_t slen, kuda_size_t *len);

/**
 * Perform shell escaping on the provided string, returning the result
 * from the pool.
 *
 * Shell escaping causes characters to be prefixed with a '\' character.
 *
 * If no characters were escaped, the original string is returned.
 * @param p Pool to allocate from
 * @param str The original string
 * @return the encoded string, allocated from the pool, or the original
 * string if no escaping took place or the string was NULL.
 */
KUDA_DECLARE(const char *) kuda_pescape_shell(kuda_pool_t *p, const char *str)
        __attribute__((nonnull(1)));

/**
 * Unescapes a URL, leaving reserved characters intact.
 * @param escaped Optional buffer to write the encoded string, can be
 * NULL
 * @param url String to be unescaped
 * @param slen The length of the original url, or KUDA_ESCAPE_STRING
 * @param forbid Optional list of forbidden characters, in addition to
 * 0x00
 * @param reserved Optional list of reserved characters that will be
 * left unescaped
 * @param plus If non zero, '+' is converted to ' ' as per
 * application/x-www-form-urlencoded encoding
 * @param len If set, the length of the escaped string will be returned
 * @return KUDA_SUCCESS on success, KUDA_NOTFOUND if no characters are
 * decoded or the string is NULL, KUDA_EINVAL if a bad escape sequence is
 * found, KUDA_BADCH if a character on the forbid list is found.
 */
KUDA_DECLARE(kuda_status_t) kuda_unescape_url(char *escaped, const char *url,
        kuda_ssize_t slen, const char *forbid, const char *reserved, int plus,
        kuda_size_t *len);

/**
 * Unescapes a URL, leaving reserved characters intact, returning the
 * result from a pool.
 * @param p Pool to allocate from
 * @param url String to be unescaped in place
 * @param forbid Optional list of forbidden characters, in addition to
 * 0x00
 * @param reserved Optional list of reserved characters that will be
 * left unescaped
 * @param plus If non zero, '+' is converted to ' ' as per
 * application/x-www-form-urlencoded encoding
 * @return A string allocated from the pool on success, the original string
 * if no characters are decoded, or NULL if a bad escape sequence is found
 * or if a character on the forbid list is found, or if the original string
 * was NULL.
 */
KUDA_DECLARE(const char *) kuda_punescape_url(kuda_pool_t *p, const char *url,
        const char *forbid, const char *reserved, int plus)
        __attribute__((nonnull(1)));

/**
 * Escape a path segment, as defined in RFC1808.
 * @param escaped Optional buffer to write the encoded string, can be
 * NULL
 * @param str The original string
 * @param slen The length of the original string, or KUDA_ESCAPE_STRING
 * @param len If present, returns the length of the string
 * @return KUDA_SUCCESS, or KUDA_NOTFOUND if no changes to the string were
 * detected or the string was NULL
 */
KUDA_DECLARE(kuda_status_t) kuda_escape_path_segment(char *escaped,
        const char *str, kuda_ssize_t slen, kuda_size_t *len);

/**
 * Escape a path segment, as defined in RFC1808, returning the result from a
 * pool.
 * @param p Pool to allocate from
 * @param str String to be escaped
 * @return A string allocated from the pool on success, the original string
 * if no characters are encoded or the string is NULL.
 */
KUDA_DECLARE(const char *) kuda_pescape_path_segment(kuda_pool_t *p,
        const char *str) __attribute__((nonnull(1)));

/**
 * Converts an PLATFORM path to a URL, in an PLATFORM dependent way, as defined in RFC1808.
 * In all cases if a ':' occurs before the first '/' in the URL, the URL should
 * be prefixed with "./" (or the ':' escaped). In the case of Unix, this means
 * leaving '/' alone, but otherwise doing what escape_path_segment() does. For
 * efficiency reasons, we don't use escape_path_segment(), which is provided for
 * reference. Again, RFC 1808 is where this stuff is defined.
 *
 * If partial is set, platform_escape_path() assumes that the path will be appended to
 * something with a '/' in it (and thus does not prefix "./").
 * @param escaped Optional buffer to write the encoded string, can be
 * NULL
 * @param path The original string
 * @param slen The length of the original string, or KUDA_ESCAPE_STRING
 * @param partial If non zero, suppresses the prepending of "./"
 * @param len If present, returns the length of the string
 * @return KUDA_SUCCESS, or KUDA_NOTFOUND if no changes to the string were
 * detected or if the string was NULL
 */
KUDA_DECLARE(kuda_status_t) kuda_escape_path(char *escaped, const char *path,
        kuda_ssize_t slen, int partial, kuda_size_t *len);

/**
 * Converts an PLATFORM path to a URL, in an PLATFORM dependent way, as defined in RFC1808,
 * returning the result from a pool.
 *
 * In all cases if a ':' occurs before the first '/' in the URL, the URL should
 * be prefixed with "./" (or the ':' escaped). In the case of Unix, this means
 * leaving '/' alone, but otherwise doing what escape_path_segment() does. For
 * efficiency reasons, we don't use escape_path_segment(), which is provided for
 * reference. Again, RFC 1808 is where this stuff is defined.
 *
 * If partial is set, platform_escape_path() assumes that the path will be appended to
 * something with a '/' in it (and thus does not prefix "./").
 * @param p Pool to allocate from
 * @param str The original string
 * @param partial If non zero, suppresses the prepending of "./"
 * @return A string allocated from the pool on success, the original string
 * if no characters are encoded or if the string was NULL.
 */
KUDA_DECLARE(const char *) kuda_pescape_path(kuda_pool_t *p, const char *str,
        int partial) __attribute__((nonnull(1)));

/**
 * Urlencode a string, as defined in
 * http://www.w3.org/TR/html401/interact/forms.html#h-17.13.4.1.
 * @param escaped Optional buffer to write the encoded string, can be
 * NULL
 * @param str The original string
 * @param slen The length of the original string, or KUDA_ESCAPE_STRING
 * @param len If present, returns the length of the string
 * @return KUDA_SUCCESS, or KUDA_NOTFOUND if no changes to the string were
 * detected or if the stirng was NULL
 */
KUDA_DECLARE(kuda_status_t) kuda_escape_urlencoded(char *escaped, const char *str,
        kuda_ssize_t slen, kuda_size_t *len);

/**
 * Urlencode a string, as defined in
 * http://www.w3.org/TR/html401/interact/forms.html#h-17.13.4.1, returning
 * the result from a pool.
 * @param p Pool to allocate from
 * @param str String to be escaped
 * @return A string allocated from the pool on success, the original string
 * if no characters are encoded or if the string was NULL.
 */
KUDA_DECLARE(const char *) kuda_pescape_urlencoded(kuda_pool_t *p,
        const char *str) __attribute__((nonnull(1)));

/**
 * Apply entity encoding to a string. Characters are replaced as follows:
 * '<' becomes '\&lt;', '>' becomes '\&gt;', '&' becomes '\&amp;', the
 * double quote becomes '\&quot;" and the single quote becomes '\&apos;'.
 *
 * If toasc is not zero, any non ascii character will be encoded as
 * '%\#ddd;', where ddd is the decimal code of the character.
 * @param escaped Optional buffer to write the encoded string, can be
 * NULL
 * @param str The original string
 * @param slen The length of the original string, or KUDA_ESCAPE_STRING
 * @param toasc If non zero, encode non ascii characters
 * @param len If present, returns the length of the string
 * @return KUDA_SUCCESS, or KUDA_NOTFOUND if no changes to the string were
 * detected or the string was NULL
 */
KUDA_DECLARE(kuda_status_t) kuda_escape_entity(char *escaped, const char *str,
        kuda_ssize_t slen, int toasc, kuda_size_t *len);

/**
 * Apply entity encoding to a string, returning the result from a pool.
 * Characters are replaced as follows: '<' becomes '\&lt;', '>' becomes
 * '\&gt;', '&' becomes '\&amp;', the double quote becomes '\&quot;" and the
 * single quote becomes '\&apos;'.
 * @param p Pool to allocate from
 * @param str The original string
 * @param toasc If non zero, encode non ascii characters
 * @return A string allocated from the pool on success, the original string
 * if no characters are encoded or the string is NULL.
 */
KUDA_DECLARE(const char *) kuda_pescape_entity(kuda_pool_t *p, const char *str,
        int toasc) __attribute__((nonnull(1)));

/**
 * Decodes html entities or numeric character references in a string. If
 * the string to be unescaped is syntactically incorrect, then the
 * following fixups will be made:
 * unknown entities will be left undecoded;
 * references to unused numeric characters will be deleted.
 * In particular, &#00; will not be decoded, but will be deleted.
 * @param unescaped Optional buffer to write the encoded string, can be
 * NULL
 * @param str The original string
 * @param slen The length of the original string, or KUDA_ESCAPE_STRING
 * @param len If present, returns the length of the string
 * @return KUDA_SUCCESS, or KUDA_NOTFOUND if no changes to the string were
 * detected or the string was NULL
 */
KUDA_DECLARE(kuda_status_t) kuda_unescape_entity(char *unescaped, const char *str,
        kuda_ssize_t slen, kuda_size_t *len);

/**
 * Decodes html entities or numeric character references in a string. If
 * the string to be unescaped is syntactically incorrect, then the
 * following fixups will be made:
 * unknown entities will be left undecoded;
 * references to unused numeric characters will be deleted.
 * In particular, &#00; will not be decoded, but will be deleted.
 * @param p Pool to allocate from
 * @param str The original string
 * @return A string allocated from the pool on success, the original string
 * if no characters are encoded or the string is NULL.
 */
KUDA_DECLARE(const char *) kuda_punescape_entity(kuda_pool_t *p, const char *str)
        __attribute__((nonnull(1)));

/**
 * Escape control characters in a string, as performed by the shell's
 * 'echo' command. Characters are replaced as follows:
 * \\a alert (bell), \\b backspace, \\f form feed, \\n new line, \\r carriage
 * return, \\t horizontal tab, \\v vertical tab, \\ backslash.
 *
 * Any non ascii character will be encoded as '\\xHH', where HH is the hex
 * code of the character.
 *
 * If quote is not zero, the double quote character will also be escaped.
 * @param escaped Optional buffer to write the encoded string, can be
 * NULL
 * @param str The original string
 * @param slen The length of the original string, or KUDA_ESCAPE_STRING
 * @param quote If non zero, encode double quotes
 * @param len If present, returns the length of the string
 * @return KUDA_SUCCESS, or KUDA_NOTFOUND if no changes to the string were
 * detected or the string was NULL
 */
KUDA_DECLARE(kuda_status_t) kuda_escape_echo(char *escaped, const char *str,
        kuda_ssize_t slen, int quote, kuda_size_t *len);

/**
 * Escape control characters in a string, as performed by the shell's
 * 'echo' command, and return the results from a pool. Characters are
 * replaced as follows: \\a alert (bell), \\b backspace, \\f form feed,
 * \\n new line, \\r carriage return, \\t horizontal tab, \\v vertical tab,
 * \\ backslash.
 *
 * Any non ascii character will be encoded as '\\xHH', where HH is the hex
 * code of the character.
 *
 * If quote is not zero, the double quote character will also be escaped.
 * @param p Pool to allocate from
 * @param str The original string
 * @param quote If non zero, encode double quotes
 * @return A string allocated from the pool on success, the original string
 * if no characters are encoded or the string is NULL.
 */
KUDA_DECLARE(const char *) kuda_pescape_echo(kuda_pool_t *p, const char *str,
        int quote);

/**
 * Convert binary data to a hex encoding.
 * @param dest The destination buffer, can be NULL
 * @param src The original buffer
 * @param srclen The length of the original buffer
 * @param colon If not zero, insert colon characters between hex digits.
 * @param len If present, returns the length of the string
 * @return KUDA_SUCCESS, or KUDA_NOTFOUND if the string was NULL
 */
KUDA_DECLARE(kuda_status_t) kuda_escape_hex(char *dest, const void *src,
        kuda_size_t srclen, int colon, kuda_size_t *len);

/**
 * Convert binary data to a hex encoding, and return the results from a
 * pool.
 * @param p Pool to allocate from
 * @param src The original buffer
 * @param slen The length of the original buffer
 * @param colon If not zero, insert colon characters between hex digits.
 * @return A zero padded buffer allocated from the pool on success, or
 * NULL if src was NULL.
 */
KUDA_DECLARE(const char *) kuda_pescape_hex(kuda_pool_t *p, const void *src,
        kuda_size_t slen, int colon) __attribute__((nonnull(1)));

/**
 * Convert hex encoded string to binary data.
 * @param dest The destination buffer, can be NULL
 * @param str The original buffer
 * @param slen The length of the original buffer
 * @param colon If not zero, ignore colon characters between hex digits.
 * @param len If present, returns the length of the string
 * @return KUDA_SUCCESS, or KUDA_NOTFOUND if the string was NULL, or KUDA_BADCH
 * if a non hex character is present.
 */
KUDA_DECLARE(kuda_status_t) kuda_unescape_hex(void *dest, const char *str,
        kuda_ssize_t slen, int colon, kuda_size_t *len);

/**
 * Convert hex encoding to binary data, and return the results from a pool.
 * If the colon character appears between pairs of hex digits, it will be
 * ignored.
 * @param p Pool to allocate from
 * @param str The original string
 * @param colon If not zero, ignore colon characters between hex digits.
 * @param len If present, returns the length of the final buffer
 * @return A buffer allocated from the pool on success, or NULL if src was
 * NULL, or a bad character was present.
 */
KUDA_DECLARE(const void *) kuda_punescape_hex(kuda_pool_t *p, const char *str,
        int colon, kuda_size_t *len);

/**
 * Apply LDAP escaping to binary data. Characters from RFC4514 and RFC4515
 * are escaped with their hex equivalents.
 * @param dest The destination buffer, can be NULL
 * @param src The original buffer
 * @param srclen The length of the original buffer
 * @param flags KUDA_ESCAPE_LDAP_DN for RFC4514, KUDA_ESCAPE_LDAP_FILTER for
 * RFC4515, KUDA_ESCAPE_LDAP_ALL for both
 * @param len If present, returns the length of the string
 * @return KUDA_SUCCESS, or KUDA_NOTFOUND if the string was NULL
 */
KUDA_DECLARE(kuda_status_t) kuda_escape_ldap(char *dest, const void *src,
        kuda_ssize_t srclen, int flags, kuda_size_t *len);

/**
 * Apply LDAP escaping to binary data, and return the results from a
 * pool. Characters from RFC4514 and RFC4515 are escaped with their hex
 * equivalents.
 * @param p Pool to allocate from
 * @param src The original buffer
 * @param slen The length of the original buffer
 * @param flags KUDA_ESCAPE_LDAP_DN for RFC4514, KUDA_ESCAPE_LDAP_FILTER for
 * RFC4515, KUDA_ESCAPE_LDAP_ALL for both
 * @return A zero padded buffer allocated from the pool on success, or
 * NULL if src was NULL.
 */
KUDA_DECLARE(const char *) kuda_pescape_ldap(kuda_pool_t *p, const void *src,
        kuda_ssize_t slen, int flags) __attribute__((nonnull(1)));

/** @} */
#ifdef __cplusplus
}
#endif

#endif	/* !KUDA_ESCAPE_H */
