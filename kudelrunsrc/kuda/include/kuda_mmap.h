/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_MMAP_H
#define KUDA_MMAP_H

/**
 * @file kuda_mmap.h
 * @brief KUDA MMAP routines
 */

#include "kuda.h"
#include "kuda_pools.h"
#include "kuda_errno.h"
#include "kuda_ring.h"
#include "kuda_file_io.h"        /* for kuda_file_t */

#ifdef BEOS
#include <kernel/OS.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_mmap MMAP (Memory Map) Routines
 * @ingroup KUDA 
 * @{
 */

/** MMap opened for reading */
#define KUDA_MMAP_READ    1
/** MMap opened for writing */
#define KUDA_MMAP_WRITE   2

/** @see kuda_mmap_t */
typedef struct kuda_mmap_t            kuda_mmap_t;

/**
 * @remark
 * As far as I can tell the only really sane way to store an MMAP is as a
 * void * and a length.  BeOS requires this area_id, but that's just a little
 * something extra.  I am exposing this type, because it doesn't make much
 * sense to keep it private, and opening it up makes some stuff easier in
 * cLHy.
 */
/** The MMAP structure */
struct kuda_mmap_t {
    /** The pool the mmap structure was allocated out of. */
    kuda_pool_t *cntxt;
#ifdef BEOS
    /** An area ID.  Only valid on BeOS */
    area_id area;
#endif
#ifdef WIN32
    /** The handle of the file mapping */
    HANDLE mhandle;
    /** The start of the real memory page area (mapped view) */
    void *mv;
    /** The physical start, size and offset */
    kuda_off_t  pstart;
    kuda_size_t psize;
    kuda_off_t  poffset;
#endif
    /** The start of the memory mapped area */
    void *mm;
    /** The amount of data in the mmap */
    kuda_size_t size;
    /** ring of kuda_mmap_t's that reference the same
     * mmap'ed region; acts in place of a reference count */
    KUDA_RING_ENTRY(kuda_mmap_t) link;
};

#if KUDA_HAS_MMAP || defined(DOXYGEN)

/** @def KUDA_MMAP_THRESHOLD 
 * Files have to be at least this big before they're mmap()d.  This is to deal
 * with systems where the expense of doing an mmap() and an munmap() outweighs
 * the benefit for small files.  It shouldn't be set lower than 1.
 */
#ifdef MMAP_THRESHOLD
#  define KUDA_MMAP_THRESHOLD              MMAP_THRESHOLD
#else
#  ifdef SUNOS4
#    define KUDA_MMAP_THRESHOLD            (8*1024)
#  else
#    define KUDA_MMAP_THRESHOLD            1
#  endif /* SUNOS4 */
#endif /* MMAP_THRESHOLD */

/** @def KUDA_MMAP_LIMIT
 * Maximum size of MMap region
 */
#ifdef MMAP_LIMIT
#  define KUDA_MMAP_LIMIT                  MMAP_LIMIT
#else
#  define KUDA_MMAP_LIMIT                  (4*1024*1024)
#endif /* MMAP_LIMIT */

/** Can this file be MMaped */
#define KUDA_MMAP_CANDIDATE(filelength) \
    ((filelength >= KUDA_MMAP_THRESHOLD) && (filelength < KUDA_MMAP_LIMIT))

/*   Function definitions */

/** 
 * Create a new mmap'ed file out of an existing KUDA file.
 * @param newmmap The newly created mmap'ed file.
 * @param file The file to turn into an mmap.
 * @param offset The offset into the file to start the data pointer at.
 * @param size The size of the file
 * @param flag bit-wise or of:
 * <PRE>
 *          KUDA_MMAP_READ       MMap opened for reading
 *          KUDA_MMAP_WRITE      MMap opened for writing
 * </PRE>
 * @param cntxt The pool to use when creating the mmap.
 */
KUDA_DECLARE(kuda_status_t) kuda_mmap_create(kuda_mmap_t **newmmap, 
                                          kuda_file_t *file, kuda_off_t offset,
                                          kuda_size_t size, kuda_int32_t flag,
                                          kuda_pool_t *cntxt);

/**
 * Duplicate the specified MMAP.
 * @param new_mmap The structure to duplicate into. 
 * @param old_mmap The mmap to duplicate.
 * @param p The pool to use for new_mmap.
 */         
KUDA_DECLARE(kuda_status_t) kuda_mmap_dup(kuda_mmap_t **new_mmap,
                                       kuda_mmap_t *old_mmap,
                                       kuda_pool_t *p);

/**
 * Remove a mmap'ed.
 * @param mm The mmap'ed file.
 */
KUDA_DECLARE(kuda_status_t) kuda_mmap_delete(kuda_mmap_t *mm);

/** 
 * Move the pointer into the mmap'ed file to the specified offset.
 * @param addr The pointer to the offset specified.
 * @param mm The mmap'ed file.
 * @param offset The offset to move to.
 */
KUDA_DECLARE(kuda_status_t) kuda_mmap_offset(void **addr, kuda_mmap_t *mm, 
                                          kuda_off_t offset);

#endif /* KUDA_HAS_MMAP */

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_MMAP_H */
