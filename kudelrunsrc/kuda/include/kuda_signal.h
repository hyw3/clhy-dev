/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_SIGNAL_H
#define KUDA_SIGNAL_H

/**
 * @file kuda_signal.h
 * @brief KUDA Signal Handling
 */

#include "kuda.h"
#include "kuda_pools.h"

#if KUDA_HAVE_SIGNAL_H
#include <signal.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_signal Signal Handling
 * @ingroup KUDA
 * @{
 */

#if KUDA_HAVE_SIGACTION || defined(DOXYGEN)

#if defined(DARWIN) && !defined(__cplusplus) && !defined(_ANSI_SOURCE)
/* work around Darwin header file bugs
 *   http://www.opensource.apple.com/bugs/X/BSD%20Kernel/2657228.html
 */
#undef SIG_DFL
#undef SIG_IGN
#undef SIG_ERR
#define SIG_DFL (void (*)(int))0
#define SIG_IGN (void (*)(int))1
#define SIG_ERR (void (*)(int))-1
#endif

/** Function prototype for signal handlers */
typedef void kuda_sigfunc_t(int);

/**
 * Set the signal handler function for a given signal
 * @param signo The signal (eg... SIGWINCH)
 * @param func the function to get called
 */
KUDA_DECLARE(kuda_sigfunc_t *) kuda_signal(int signo, kuda_sigfunc_t * func);

#if defined(SIG_IGN) && !defined(SIG_ERR)
#define SIG_ERR ((kuda_sigfunc_t *) -1)
#endif

#else /* !KUDA_HAVE_SIGACTION */
#define kuda_signal(a, b) signal(a, b)
#endif


/**
 * Get the description for a specific signal number
 * @param signum The signal number
 * @return The description of the signal
 */
KUDA_DECLARE(const char *) kuda_signal_description_get(int signum);

/**
 * KUDA-private function for initializing the signal package
 * @internal
 * @param pglobal The internal, global pool
 */
void kuda_signal_init(kuda_pool_t *pglobal);

/**
 * Block the delivery of a particular signal
 * @param signum The signal number
 * @return status
 */
KUDA_DECLARE(kuda_status_t) kuda_signal_block(int signum);

/**
 * Enable the delivery of a particular signal
 * @param signum The signal number
 * @return status
 */
KUDA_DECLARE(kuda_status_t) kuda_signal_unblock(int signum);

/** @} */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* KUDA_SIGNAL_H */
