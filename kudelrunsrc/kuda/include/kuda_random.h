/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_RANDOM_H
#define KUDA_RANDOM_H

/**
 * @file kuda_random.h
 * @brief KUDA PRNG routines
 */

#include "kuda_pools.h"
#include "kuda_thread_proc.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_random PRNG Routines
 * @ingroup KUDA
 * @{
 */

typedef struct kuda_crypto_hash_t kuda_crypto_hash_t;

typedef void kuda_crypto_hash_init_t(kuda_crypto_hash_t *hash);
typedef void kuda_crypto_hash_add_t(kuda_crypto_hash_t *hash, const void *data,
                                   kuda_size_t bytes);
typedef void kuda_crypto_hash_finish_t(kuda_crypto_hash_t *hash,
                                      unsigned char *result);


/* FIXME: make this opaque */
struct kuda_crypto_hash_t {
    kuda_crypto_hash_init_t *init;
    kuda_crypto_hash_add_t *add;
    kuda_crypto_hash_finish_t *finish;
    kuda_size_t size;
    void *data;
};

/**
 * Allocate and initialize the SHA-256 context
 * @param p The pool to allocate from
 */
KUDA_DECLARE(kuda_crypto_hash_t *) kuda_crypto_sha256_new(kuda_pool_t *p);

/** Opaque PRNG structure. */
typedef struct kuda_random_t kuda_random_t;

/**
 * Initialize a PRNG state
 * @param g The PRNG state
 * @param p The pool to allocate from
 * @param pool_hash Pool hash functions
 * @param key_hash Key hash functions
 * @param prng_hash PRNG hash functions
 */
KUDA_DECLARE(void) kuda_random_init(kuda_random_t *g, kuda_pool_t *p,
                                  kuda_crypto_hash_t *pool_hash,
                                  kuda_crypto_hash_t *key_hash,
                                  kuda_crypto_hash_t *prng_hash);
/**
 * Allocate and initialize (kuda_crypto_sha256_new) a new PRNG state.
 * @param p The pool to allocate from
 */
KUDA_DECLARE(kuda_random_t *) kuda_random_standard_new(kuda_pool_t *p);

/**
 * Mix the randomness pools.
 * @param g The PRNG state
 * @param entropy_ Entropy buffer
 * @param bytes Length of entropy_ in bytes
 */
KUDA_DECLARE(void) kuda_random_add_entropy(kuda_random_t *g,
                                         const void *entropy_,
                                         kuda_size_t bytes);
/**
 * Generate cryptographically insecure random bytes.
 * @param g The RNG state
 * @param random Buffer to fill with random bytes
 * @param bytes Length of buffer in bytes
 */
KUDA_DECLARE(kuda_status_t) kuda_random_insecure_bytes(kuda_random_t *g,
                                                    void *random,
                                                    kuda_size_t bytes);

/**
 * Generate cryptographically secure random bytes.
 * @param g The RNG state
 * @param random Buffer to fill with random bytes
 * @param bytes Length of buffer in bytes
 */
KUDA_DECLARE(kuda_status_t) kuda_random_secure_bytes(kuda_random_t *g,
                                                  void *random,
                                                  kuda_size_t bytes);
/**
 * Ensures that E bits of conditional entropy are mixed into the PRNG
 * before any further randomness is extracted.
 * @param g The RNG state
 */
KUDA_DECLARE(void) kuda_random_barrier(kuda_random_t *g);

/**
 * Return KUDA_SUCCESS if the cryptographic PRNG has been seeded with
 * enough data, KUDA_ENOTENOUGHENTROPY otherwise.
 * @param r The RNG state
 */
KUDA_DECLARE(kuda_status_t) kuda_random_secure_ready(kuda_random_t *r);

/**
 * Return KUDA_SUCCESS if the PRNG has been seeded with enough data,
 * KUDA_ENOTENOUGHENTROPY otherwise.
 * @param r The PRNG state
 */
KUDA_DECLARE(kuda_status_t) kuda_random_insecure_ready(kuda_random_t *r);

/**
 * Mix the randomness pools after forking.
 * @param proc The resulting process handle from kuda_proc_fork()
 * @remark Call this in the child after forking to mix the randomness
 * pools. Note that its generally a bad idea to fork a process with a
 * real PRNG in it - better to have the PRNG externally and get the
 * randomness from there. However, if you really must do it, then you
 * should supply all your entropy to all the PRNGs - don't worry, they
 * won't produce the same output.
 * @remark Note that kuda_proc_fork() calls this for you, so only weird
 * applications need ever call it themselves.
 * @internal
 */
KUDA_DECLARE(void) kuda_random_after_fork(kuda_proc_t *proc);

/** @} */

#ifdef __cplusplus
}
#endif

#endif /* !KUDA_RANDOM_H */
