/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file contains private declarations common to all architectures.
 */

#ifndef KUDA_PRIVATE_COMMON_H
#define KUDA_PRIVATE_COMMON_H

#include "kuda_pools.h"
#include "kuda_tables.h"

kuda_status_t kuda_filepath_list_split_impl(kuda_array_header_t **pathelts,
                                          const char *liststr,
                                          char separator,
                                          kuda_pool_t *p);

kuda_status_t kuda_filepath_list_merge_impl(char **liststr,
                                          kuda_array_header_t *pathelts,
                                          char separator,
                                          kuda_pool_t *p);

/* temporary defines to handle 64bit compile mismatches */
#define KUDA_INT_TRUNC_CAST    int
#define KUDA_UINT32_TRUNC_CAST kuda_uint32_t

#endif  /*KUDA_PRIVATE_COMMON_H*/
