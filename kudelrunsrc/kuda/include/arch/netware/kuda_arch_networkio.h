/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NETWORK_IO_H

#ifdef USE_WINSOCK
/* Making sure that we include the correct networkio.h since the
    the project file is configured to first look for headers in
    arch/netware and then arch/unix. But in this specific case we 
    want arch/win32.
*/
#include <../win32/kuda_arch_networkio.h>
#else
#include <../unix/kuda_arch_networkio.h>
#endif

#endif  /* ! NETWORK_IO_H */

