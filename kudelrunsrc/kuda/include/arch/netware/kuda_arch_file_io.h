/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FILE_IO_H
#define FILE_IO_H

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_tables.h"
#include "kuda_file_io.h"
#include "kuda_file_info.h"
#include "kuda_errno.h"
#include "kuda_lib.h"
#include "kuda_poll.h"

/* System headers the file I/O library needs */
#if KUDA_HAVE_FCNTL_H
#include <fcntl.h>
#endif
#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#if KUDA_HAVE_ERRNO_H
#include <errno.h>
#endif
#if KUDA_HAVE_STRING_H
#include <string.h>
#endif
#if KUDA_HAVE_STRINGS_H
#include <strings.h>
#endif
#if KUDA_HAVE_DIRENT_H
#include <dirent.h>
#endif
#ifdef HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#if KUDA_HAVE_STDIO_H
#include <stdio.h>
#endif
#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif
#if KUDA_HAVE_SYS_UIO_H
#include <sys/uio.h>
#endif
#if KUDA_HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

#include <fsio.h>

/* End System headers */

#define KUDA_FILE_DEFAULT_BUFSIZE 4096
/* For backwards compat */
#define KUDA_FILE_BUFSIZE KUDA_FILE_DEFAULT_BUFSIZE

#if KUDA_HAS_THREADS
#define file_lock(f)   do { \
                           if ((f)->thlock) \
                               kuda_thread_mutex_lock((f)->thlock); \
                       } while (0)
#define file_unlock(f) do { \
                           if ((f)->thlock) \
                               kuda_thread_mutex_unlock((f)->thlock); \
                       } while (0)
#else
#define file_lock(f)   do {} while (0)
#define file_unlock(f) do {} while (0)
#endif

#if KUDA_HAS_LARGE_FILES
#define lseek(f,o,w) lseek64(f,o,w)
#define ftruncate(f,l) ftruncate64(f,l)
#endif

typedef struct stat struct_stat;

struct kuda_file_t {
    kuda_pool_t *pool;
    int filedes;
    char *fname;
    kuda_int32_t flags;
    int eof_hit;
    int is_pipe;
    kuda_interval_time_t timeout;
    int buffered;
    enum {BLK_UNKNOWN, BLK_OFF, BLK_ON } blocking;
    int ungetchar;    /* Last char provided by an unget op. (-1 = no char)*/

    /* if there is a timeout set, then this pollset is used */
    kuda_pollset_t *pollset;

    /* Stuff for buffered mode */
    char *buffer;
    kuda_size_t bufpos;    /* Read/Write position in buffer */
    kuda_size_t bufsize;   /* The buffer size */
    kuda_off_t dataRead;   /* amount of valid data read into buffer */
    int direction;            /* buffer being used for 0 = read, 1 = write */
    kuda_off_t filePtr;    /* position in file of handle */
#if KUDA_HAS_THREADS
    struct kuda_thread_mutex_t *thlock;
#endif
};

struct kuda_dir_t {
    kuda_pool_t *pool;
    char *dirname;
    DIR *dirstruct;
    struct dirent *entry;
};

typedef struct kuda_stat_entry_t kuda_stat_entry_t;

struct kuda_stat_entry_t {
    struct stat info;
    char *casedName;
    kuda_time_t expire;
    NXPathCtx_t pathCtx;
};

#define MAX_SERVER_NAME     64
#define MAX_VOLUME_NAME     64
#define MAX_PATH_NAME       256
#define MAX_FILE_NAME       256

#define DRIVE_ONLY          1

/* If the user passes d: vs. D: (or //mach/share vs. //MACH/SHARE),
 * we need to fold the case to canonical form.  This function is
 * supposed to do so.
 */
kuda_status_t filepath_root_case(char **rootpath, char *root, kuda_pool_t *p);

/* This function check to see of the given path includes a drive/volume
 * specifier.  If the _only_ parameter is set to DRIVE_ONLY then it 
 * check to see of the path only contains a drive/volume specifier and
 * nothing else.
 */
kuda_status_t filepath_has_drive(const char *rootpath, int only, kuda_pool_t *p);

/* This function compares the drive/volume specifiers for each given path.
 * It returns zero if they match or non-zero if not. 
 */
kuda_status_t filepath_compare_drive(const char *path1, const char *path2, kuda_pool_t *p);

kuda_status_t kuda_unix_file_cleanup(void *);
kuda_status_t kuda_unix_child_file_cleanup(void *);

mode_t kuda_unix_perms2mode(kuda_fileperms_t perms);
kuda_fileperms_t kuda_unix_mode2perms(mode_t mode);

kuda_status_t kuda_file_flush_locked(kuda_file_t *thefile);
kuda_status_t kuda_file_info_get_locked(kuda_finfo_t *finfo, kuda_int32_t wanted,
                                      kuda_file_t *thefile);

#endif  /* ! FILE_IO_H */

