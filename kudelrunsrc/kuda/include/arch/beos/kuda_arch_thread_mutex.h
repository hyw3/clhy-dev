/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef THREAD_MUTEX_H
#define THREAD_MUTEX_H

#include <kernel/OS.h>
#include "kuda_pools.h"
#include "kuda_thread_mutex.h"
#include "kuda_file_io.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_portable.h"

struct kuda_thread_mutex_t {
    kuda_pool_t *pool;
    
    /* Our lock :) */
    sem_id Lock;
    int32  LockCount;

    /* If we nest locks we need these... */
    int nested;  
    kuda_platform_thread_t owner;
    int owner_ref;
};

#endif  /* THREAD_MUTEX_H */

