/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_thread_proc.h"
#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_thread_proc.h"
#include "kuda_general.h"
#include "kuda_portable.h"
#include <kernel/OS.h>
#include <signal.h>
#include <string.h>
#include <sys/wait.h>
#include <image.h>

#ifndef THREAD_PROC_H
#define THREAD_PROC_H

#define SHELL_PATH "/bin/sh"

#define PTHREAD_CANCEL_AYNCHRONOUS  CANCEL_ASYNCH; 
#define PTHREAD_CANCEL_DEFERRED     CANCEL_DEFER; 
                                   
#define PTHREAD_CANCEL_ENABLE       CANCEL_ENABLE; 
#define PTHREAD_CANCEL_DISABLE      CANCEL_DISABLE; 

#define BEOS_MAX_DATAKEYS	128

struct kuda_thread_t {
    kuda_pool_t *pool;
    thread_id td;
    void *data;
    kuda_thread_start_t func;
    kuda_status_t exitval;
};

struct kuda_threadattr_t {
    kuda_pool_t *pool;
    int32 attr;
    int detached;
    int joinable;
};

struct kuda_threadkey_t {
    kuda_pool_t *pool;
	int32  key;
};

struct beos_private_data {
	const void ** data;
	int count;
	volatile thread_id  td;
};

struct beos_key {
	int  assigned;
	int  count;
	sem_id  lock;
	int32  ben_lock;
	void (* destructor) (void *);
};

struct kuda_procattr_t {
    kuda_pool_t *pool;
    kuda_file_t *parent_in;
    kuda_file_t *child_in;
    kuda_file_t *parent_out;
    kuda_file_t *child_out;
    kuda_file_t *parent_err;
    kuda_file_t *child_err;
    char *currdir;
    kuda_int32_t cmdtype;
    kuda_int32_t detached;
};

struct kuda_thread_once_t {
    sem_id sem;
    int hit;
};

#endif  /* ! THREAD_PROC_H */

