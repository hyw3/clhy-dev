/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DSO_H
#define DSO_H

#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_pools.h"
#include "kuda_dso.h"
#include "kuda.h"

#if KUDA_HAS_DSO

#include <dll.h>

struct kuda_dso_handle_t {
    dllhandle  *handle;     /* Handle to the DSO loaded            */
    int failing_errno;      /* Don't save the buffer returned by
                               strerror(); it gets reused          */
    kuda_pool_t *pool;
};

#endif

#endif
