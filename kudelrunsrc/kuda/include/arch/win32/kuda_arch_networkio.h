/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NETWORK_IO_H
#define NETWORK_IO_H

#include "kuda_network_io.h"
#include "kuda_general.h"
#include "kuda_poll.h"

typedef struct sock_userdata_t sock_userdata_t;
struct sock_userdata_t {
    sock_userdata_t *next;
    const char *key;
    void *data;
};

struct kuda_socket_t {
    kuda_pool_t         *pool;
    SOCKET              socketdes;
    int                 type; /* SOCK_STREAM, SOCK_DGRAM */
    int                 protocol;
    kuda_sockaddr_t     *local_addr;
    kuda_sockaddr_t     *remote_addr;
    int                 timeout_ms; /* MUST MATCH if timeout > 0 */
    kuda_interval_time_t timeout;
    kuda_int32_t         disconnected;
    int                 local_port_unknown;
    int                 local_interface_unknown;
    int                 remote_addr_unknown;
    kuda_int32_t         options;
    kuda_int32_t         inherit;
#if KUDA_HAS_SENDFILE
    /* As of 07.20.04, the overlapped structure is only used by 
     * kuda_socket_sendfile and that's where it will be allocated 
     * and initialized.
     */
    OVERLAPPED         *overlapped;
#endif
    sock_userdata_t    *userdata;

    /* if there is a timeout set, then this pollset is used */
    kuda_pollset_t *pollset;
};

#ifdef _WIN32_WCE
#ifndef WSABUF
typedef struct _WSABUF {
    u_long      len;     /* the length of the buffer */
    char FAR *  buf;     /* the pointer to the buffer */
} WSABUF, FAR * LPWSABUF;
#endif
#else
#ifdef _MSC_VER
#define HAVE_STRUCT_IPMREQ
#endif
#endif

kuda_status_t status_from_res_error(int);

const char *kuda_inet_ntop(int af, const void *src, char *dst, kuda_size_t size);
int kuda_inet_pton(int af, const char *src, void *dst);
void kuda_sockaddr_vars_set(kuda_sockaddr_t *, int, kuda_port_t);

#define kuda_is_option_set(skt, option)  \
    (((skt)->options & (option)) == (option))

#define kuda_set_option(skt, option, on) \
    do {                                 \
        if (on)                          \
            (skt)->options |= (option);         \
        else                             \
            (skt)->options &= ~(option);        \
    } while (0)

#endif  /* ! NETWORK_IO_H */

