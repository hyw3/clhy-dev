/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INHERIT_H
#define INHERIT_H

#include "kuda_inherit.h"

#define KUDA_INHERIT (1 << 24)    /* Must not conflict with other bits */

#if KUDA_HAS_UNICODE_FS && KUDA_HAS_ANSI_FS
/* !defined(_WIN32_WCE) is implicit here */

#define KUDA_IMPLEMENT_INHERIT_SET(name, flag, pool, cleanup)        \
KUDA_DECLARE(kuda_status_t) kuda_##name##_inherit_set(kuda_##name##_t *the##name) \
{                                                                   \
    IF_WIN_PLATFORM_IS_UNICODE                                            \
    {                                                               \
/*     if (!SetHandleInformation(the##name->filehand,              \
 *                                HANDLE_FLAG_INHERIT,              \
 *                                HANDLE_FLAG_INHERIT))             \
 *          return kuda_get_platform_error();                              \
 */  }                                                               \
    ELSE_WIN_PLATFORM_IS_ANSI                                             \
    {                                                               \
        HANDLE temp, hproc = GetCurrentProcess();                   \
        if (!DuplicateHandle(hproc, the##name->filehand,            \
                             hproc, &temp, 0, TRUE,                 \
                             DUPLICATE_SAME_ACCESS))                \
            return kuda_get_platform_error();                              \
        CloseHandle(the##name->filehand);                           \
        the##name->filehand = temp;                                 \
    }                                                               \
    return KUDA_SUCCESS;                                             \
}

#define KUDA_IMPLEMENT_INHERIT_UNSET(name, flag, pool, cleanup)      \
KUDA_DECLARE(kuda_status_t) kuda_##name##_inherit_unset(kuda_##name##_t *the##name)\
{                                                                   \
    IF_WIN_PLATFORM_IS_UNICODE                                            \
    {                                                               \
/*      if (!SetHandleInformation(the##name->filehand,              \
 *                                HANDLE_FLAG_INHERIT, 0))          \
 *          return kuda_get_platform_error();                              \
 */ }                                                               \
    ELSE_WIN_PLATFORM_IS_ANSI                                             \
    {                                                               \
        HANDLE temp, hproc = GetCurrentProcess();                   \
        if (!DuplicateHandle(hproc, the##name->filehand,            \
                             hproc, &temp, 0, FALSE,                \
                             DUPLICATE_SAME_ACCESS))                \
            return kuda_get_platform_error();                              \
        CloseHandle(the##name->filehand);                           \
        the##name->filehand = temp;                                 \
    }                                                               \
    return KUDA_SUCCESS;                                             \
}

#elif KUDA_HAS_ANSI_FS || defined(_WIN32_WCE)

#define KUDA_IMPLEMENT_INHERIT_SET(name, flag, pool, cleanup)        \
KUDA_DECLARE(kuda_status_t) kuda_##name##_inherit_set(kuda_##name##_t *the##name) \
{                                                                   \
    HANDLE temp, hproc = GetCurrentProcess();                       \
    if (!DuplicateHandle(hproc, the##name->filehand,                \
                         hproc, &temp, 0, TRUE,                     \
                         DUPLICATE_SAME_ACCESS))                    \
        return kuda_get_platform_error();                                  \
    CloseHandle(the##name->filehand);                               \
    the##name->filehand = temp;                                     \
    return KUDA_SUCCESS;                                             \
}

#define KUDA_IMPLEMENT_INHERIT_UNSET(name, flag, pool, cleanup)      \
KUDA_DECLARE(kuda_status_t) kuda_##name##_inherit_unset(kuda_##name##_t *the##name)\
{                                                                   \
    HANDLE temp, hproc = GetCurrentProcess();                       \
    if (!DuplicateHandle(hproc, the##name->filehand,                \
                         hproc, &temp, 0, FALSE,                    \
                         DUPLICATE_SAME_ACCESS))                    \
        return kuda_get_platform_error();                                  \
    CloseHandle(the##name->filehand);                               \
    the##name->filehand = temp;                                     \
    return KUDA_SUCCESS;                                             \
}

#else /* KUDA_HAS_UNICODE_FS && !KUDA_HAS_ANSI_FS && !defined(_WIN32_WCE) */

#define KUDA_IMPLEMENT_INHERIT_SET(name, flag, pool, cleanup)        \
KUDA_DECLARE(kuda_status_t) kuda_##name##_inherit_set(kuda_##name##_t *the##name) \
{                                                                   \
/*  if (!SetHandleInformation(the##name->filehand,                  \
 *                            HANDLE_FLAG_INHERIT,                  \
 *                            HANDLE_FLAG_INHERIT))                 \
 *      return kuda_get_platform_error();                                  \
 */ return KUDA_SUCCESS;                                             \
}

#define KUDA_IMPLEMENT_INHERIT_UNSET(name, flag, pool, cleanup)      \
KUDA_DECLARE(kuda_status_t) kuda_##name##_inherit_unset(kuda_##name##_t *the##name)\
{                                                                   \
/*  if (!SetHandleInformation(the##name->filehand,                  \
 *                            HANDLE_FLAG_INHERIT, 0))              \
 *      return kuda_get_platform_error();                                  \
 */ return KUDA_SUCCESS;                                             \
}

#endif /* defined(KUDA_HAS_UNICODE_FS) */

#endif	/* ! INHERIT_H */
