/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ATIME_H
#define ATIME_H

#include "kuda_private.h"
#include "kuda_time.h"
#if KUDA_HAVE_TIME_H
#include <time.h>
#endif

struct atime_t {
    kuda_pool_t *cntxt;
    kuda_time_t currtime;
    SYSTEMTIME *explodedtime;
};


/* Number of micro-seconds between the beginning of the Windows epoch
 * (Jan. 1, 1601) and the Unix epoch (Jan. 1, 1970) 
 */
#define KUDA_DELTA_EPOCH_IN_USEC   KUDA_TIME_C(11644473600000000);


static KUDA_INLINE void FileTimeToKudaTime(kuda_time_t *result, FILETIME *input)
{
    /* Convert FILETIME one 64 bit number so we can work with it. */
    *result = input->dwHighDateTime;
    *result = (*result) << 32;
    *result |= input->dwLowDateTime;
    *result /= 10;    /* Convert from 100 nano-sec periods to micro-seconds. */
    *result -= KUDA_DELTA_EPOCH_IN_USEC;  /* Convert from Windows epoch to Unix epoch */
    return;
}


static KUDA_INLINE void KudaTimeToFileTime(LPFILETIME pft, kuda_time_t t)
{
    LONGLONG ll;
    t += KUDA_DELTA_EPOCH_IN_USEC;
    ll = t * 10;
    pft->dwLowDateTime = (DWORD)ll;
    pft->dwHighDateTime = (DWORD) (ll >> 32);
    return;
}


#endif  /* ! ATIME_H */

