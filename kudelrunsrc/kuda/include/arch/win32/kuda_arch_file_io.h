/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FILE_IO_H
#define FILE_IO_H

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_pools.h"
#include "kuda_general.h"
#include "kuda_tables.h"
#include "kuda_thread_mutex.h"
#include "kuda_file_io.h"
#include "kuda_file_info.h"
#include "kuda_errno.h"
#include "kuda_arch_misc.h"
#include "kuda_poll.h"

#ifdef HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif
#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#ifdef HAVE_SYS_FCNTL_H
#include <fcntl.h>
#endif
#ifdef HAVE_TIME_H
#include <time.h>
#endif
#if KUDA_HAVE_DIRENT_H
#include <dirent.h>
#endif
#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif

#if KUDA_HAS_UNICODE_FS
#include "arch/win32/kuda_arch_utf8.h"
#include <wchar.h>

/* Helper functions for the WinNT ApiW() functions.  KUDA treats all
 * resource identifiers (files, etc) by their UTF-8 name, to provide 
 * access to all named identifiers.  [UTF-8 completely maps Unicode 
 * into char type strings.]
 *
 * The _path flavors below provide us fast mappings of the
 * Unicode filename //?/D:/path and //?/UNC/mach/share/path mappings,
 * which allow unlimited (well, 32000 wide character) length names.
 * These prefixes may appear in Unicode, but must not appear in the
 * Ascii API calls.  So we tack them on in utf8_to_unicode_path, and
 * strip them right back off in unicode_to_utf8_path.
 */
kuda_status_t utf8_to_unicode_path(kuda_wchar_t* dststr, kuda_size_t dstchars, 
                                  const char* srcstr);
kuda_status_t unicode_to_utf8_path(char* dststr, kuda_size_t dstchars, 
                                  const kuda_wchar_t* srcstr);

#endif /* KUDA_HAS_UNICODE_FS */

/* Another Helper functions for the WinNT ApiW() functions.  We need to
 * derive some 'resource' names (max length 255 characters, prefixed with
 * Global/ or Local/ on WinNT) from something that looks like a filename.
 * Since 'resource' names never contain slashes, convert these to '_'s
 * and return the appropriate char* or wchar* for ApiA or ApiW calls.
 */

void *res_name_from_filename(const char *file, int global, kuda_pool_t *pool);

#define KUDA_FILE_MAX MAX_PATH

#define KUDA_FILE_DEFAULT_BUFSIZE 4096
/* For backwards-compat */
#define KUDA_FILE_BUFSIZE KUDA_FILE_DEFAULT_BUFSIZE

/* obscure ommissions from msvc's sys/stat.h */
#ifdef _MSC_VER
#define S_IFIFO        _S_IFIFO /* pipe */
#define S_IFBLK        0060000  /* Block Special */
#define S_IFLNK        0120000  /* Symbolic Link */
#define S_IFSOCK       0140000  /* Socket */
#define S_IFWHT        0160000  /* Whiteout */
#endif

/* Internal Flags for kuda_file_open */
#define KUDA_OPENINFO     0x00100000 /* Open without READ or WRITE access */
#define KUDA_OPENLINK     0x00200000 /* Open a link itself, if supported */
#define KUDA_READCONTROL  0x00400000 /* Read the file's owner/perms */
#define KUDA_WRITECONTROL 0x00800000 /* Modify the file's owner/perms */
/* #define KUDA_INHERIT   0x01000000 -- Defined in kuda_arch_inherit.h! */
#define KUDA_STDIN_FLAG   0x02000000 /* Obtained via kuda_file_open_stdin() */
#define KUDA_STDOUT_FLAG  0x04000000 /* Obtained via kuda_file_open_stdout() */
#define KUDA_STDERR_FLAG  0x06000000 /* Obtained via kuda_file_open_stderr() */
#define KUDA_STD_FLAGS    (KUDA_STDIN_FLAG | KUDA_STDOUT_FLAG | KUDA_STDERR_FLAG)
#define KUDA_WRITEATTRS   0x08000000 /* Modify the file's attributes */

/* Entries missing from the MSVC 5.0 Win32 SDK:
 */
#ifndef FILE_ATTRIBUTE_DEVICE
#define FILE_ATTRIBUTE_DEVICE        0x00000040
#endif
#ifndef FILE_ATTRIBUTE_REPARSE_POINT
#define FILE_ATTRIBUTE_REPARSE_POINT 0x00000400
#endif
#ifndef FILE_FLAG_OPEN_NO_RECALL
#define FILE_FLAG_OPEN_NO_RECALL     0x00100000
#endif
#ifndef FILE_FLAG_OPEN_REPARSE_POINT
#define FILE_FLAG_OPEN_REPARSE_POINT 0x00200000
#endif
#ifndef TRUSTEE_IS_WELL_KNOWN_GROUP
#define TRUSTEE_IS_WELL_KNOWN_GROUP  5
#endif

/* Information bits available from the WIN32 FindFirstFile function */
#define KUDA_FINFO_WIN32_DIR (KUDA_FINFO_NAME  | KUDA_FINFO_TYPE \
                           | KUDA_FINFO_CTIME | KUDA_FINFO_ATIME \
                           | KUDA_FINFO_MTIME | KUDA_FINFO_SIZE)

/* Sneak the Readonly bit through finfo->protection for internal use _only_ */
#define KUDA_FREADONLY 0x10000000 

/* Private function for kuda_stat/lstat/getfileinfo/dir_read */
int fillin_fileinfo(kuda_finfo_t *finfo, WIN32_FILE_ATTRIBUTE_DATA *wininfo, 
                    int byhandle, int finddata, const char *fname,
                    kuda_int32_t wanted);

/* Private function that extends kuda_stat/lstat/getfileinfo/dir_read */
kuda_status_t more_finfo(kuda_finfo_t *finfo, const void *ufile, 
                        kuda_int32_t wanted, int whatfile);

/* whatfile types for the ufile arg */
#define MORE_OF_HANDLE 0
#define MORE_OF_FSPEC  1
#define MORE_OF_WFSPEC 2

/* quick run-down of fields in windows' kuda_file_t structure that may have 
 * obvious uses.
 * fname --  the filename as passed to the open call.
 * dwFileAttricutes -- Attributes used to open the file.
 * append -- Windows doesn't support the append concept when opening files.
 *           KUDA needs to keep track of this, and always make sure we append
 *           correctly when writing to a file with this flag set TRUE.
 */

/* for kuda_poll.c */
#define filedes filehand

struct kuda_file_t {
    kuda_pool_t *pool;
    HANDLE filehand;
    BOOLEAN pipe;              /* Is this a pipe of a file? */
    OVERLAPPED *pOverlapped;
    kuda_interval_time_t timeout;
    kuda_int32_t flags;

    /* File specific info */
    kuda_finfo_t *finfo;
    char *fname;
    DWORD dwFileAttributes;
    int eof_hit;
    BOOLEAN buffered;          /* Use buffered I/O? */
    int ungetchar;             /* Last char provided by an unget op. (-1 = no char) */
    int append; 

    /* Stuff for buffered mode */
    char *buffer;
    kuda_size_t bufpos;         /* Read/Write position in buffer */
    kuda_size_t bufsize;        /* The size of the buffer */
    kuda_size_t dataRead;       /* amount of valid data read into buffer */
    int direction;             /* buffer being used for 0 = read, 1 = write */
    kuda_off_t filePtr;         /* position in file of handle */
    kuda_thread_mutex_t *mutex; /* mutex semaphore, must be owned to access
                                * the above fields                          */

#if KUDA_FILES_AS_SOCKETS
    /* if there is a timeout set, then this pollset is used */
    kuda_pollset_t *pollset;
#endif
    /* Pipe specific info */    
};

struct kuda_dir_t {
    kuda_pool_t *pool;
    HANDLE dirhand;
    kuda_size_t rootlen;
    char *dirname;
    char *name;
    union {
#if KUDA_HAS_UNICODE_FS
        struct {
            WIN32_FIND_DATAW *entry;
        } w;
#endif
#if KUDA_HAS_ANSI_FS
        struct {
            WIN32_FIND_DATAA *entry;
        } n;
#endif        
    };
    int bof;
};

/* There are many goofy characters the filesystem can't accept
 * or can confound the cmd.exe shell.  Here's the list
 * [declared in filesys.c]
 */
extern const char kuda_c_is_fnchar[256];

#define IS_FNCHAR(c) (kuda_c_is_fnchar[(unsigned char)(c)] & 1)
#define IS_SHCHAR(c) ((kuda_c_is_fnchar[(unsigned char)(c)] & 2) == 2)


/* If the user passes KUDA_FILEPATH_TRUENAME to either
 * kuda_filepath_root or kuda_filepath_merge, this fn determines
 * that the root really exists.  It's expensive, wouldn't want
 * to do this too frequenly.
 */
kuda_status_t filepath_root_test(char *path, kuda_pool_t *p);


/* The kuda_filepath_merge wants to canonicalize the cwd to the 
 * addpath if the user passes NULL as the old root path (this
 * isn't true of an empty string "", which won't be concatenated.
 *
 * But we need to figure out what the cwd of a given volume is,
 * when the user passes D:foo.  This fn will determine D:'s cwd.
 *
 * If flags includes the bit KUDA_FILEPATH_NATIVE, the path returned
 * is in the platforms-native format.
 */
kuda_status_t filepath_drive_get(char **rootpath, char drive, 
                                kuda_int32_t flags, kuda_pool_t *p);


/* If the user passes d: vs. D: (or //mach/share vs. //MACH/SHARE),
 * we need to fold the case to canonical form.  This function is
 * supposed to do so.
 */
kuda_status_t filepath_root_case(char **rootpath, char *root, kuda_pool_t *p);


kuda_status_t file_cleanup(void *);

extern kuda_status_t
kuda_file_socket_pipe_create(kuda_file_t **in,
                            kuda_file_t **out,
                            kuda_pool_t *p);

extern kuda_status_t
kuda_file_socket_pipe_close(kuda_file_t *file);

#endif  /* ! FILE_IO_H */
