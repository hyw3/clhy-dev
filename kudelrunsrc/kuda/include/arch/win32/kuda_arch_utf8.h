/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UTF8_H
#define UTF8_H

#include "kuda.h"
#include "kuda_lib.h"
#include "kuda_errno.h"

/* If we ever support anything more exciting than char... this could move.
 */
typedef kuda_uint16_t kuda_wchar_t;

/**
 * An KUDA internal function for fast utf-8 octet-encoded Unicode conversion
 * to the ucs-2 wide Unicode format.  This function is used for filename and 
 * other resource conversions for platforms providing native Unicode support.
 *
 * @tip Only the errors KUDA_EINVAL and KUDA_INCOMPLETE may occur, the former
 * when the character code is invalid (in or out of context) and the later
 * when more characters were expected, but insufficient characters remain.
 */
KUDA_DECLARE(kuda_status_t) kuda_conv_utf8_to_ucs2(const char *in, 
                                                kuda_size_t *inbytes,
                                                kuda_wchar_t *out, 
                                                kuda_size_t *outwords);

/**
 * An KUDA internal function for fast ucs-2 wide Unicode format conversion to 
 * the utf-8 octet-encoded Unicode.  This function is used for filename and 
 * other resource conversions for platforms providing native Unicode support.
 *
 * @tip Only the errors KUDA_EINVAL and KUDA_INCOMPLETE may occur, the former
 * when the character code is invalid (in or out of context) and the later
 * when more words were expected, but insufficient words remain.
 */
KUDA_DECLARE(kuda_status_t) kuda_conv_ucs2_to_utf8(const kuda_wchar_t *in, 
                                                kuda_size_t *inwords,
                                                char *out, 
                                                kuda_size_t *outbytes);

#endif /* def UTF8_H */
