/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NETWORK_IO_H
#define NETWORK_IO_H

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_network_io.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#ifndef WAITIO_USES_POLL
#include "kuda_poll.h"
#endif

/* System headers the network I/O library needs */
#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#if KUDA_HAVE_SYS_UIO_H
#include <sys/uio.h>
#endif
#ifdef HAVE_SYS_SELECT_H
#include <sys/select.h>
#endif
#if KUDA_HAVE_ERRNO_H
#include <errno.h>
#endif
#if KUDA_HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#if KUDA_HAVE_STRING_H
#include <string.h>
#endif
#if KUDA_HAVE_NETINET_TCP_H
#include <netinet/tcp.h>
#endif
#if KUDA_HAVE_NETINET_SCTP_UIO_H
#include <netinet/sctp_uio.h>
#endif
#if KUDA_HAVE_NETINET_SCTP_H
#include <netinet/sctp.h>
#endif
#if KUDA_HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif
#if KUDA_HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif
#if KUDA_HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#if KUDA_HAVE_SYS_SOCKIO_H
#include <sys/sockio.h>
#endif
#if KUDA_HAVE_NETDB_H
#include <netdb.h>
#endif
#if KUDA_HAVE_FCNTL_H
#include <fcntl.h>
#endif
#if KUDA_HAVE_SYS_SENDFILE_H
#include <sys/sendfile.h>
#endif
#if KUDA_HAVE_SYS_IOCTL_H
#include <sys/ioctl.h>
#endif
/* End System Headers */

#ifndef HAVE_POLLIN
#define POLLIN   1
#define POLLPRI  2
#define POLLOUT  4
#define POLLERR  8
#define POLLHUP  16
#define POLLNVAL 32
#endif

typedef struct sock_userdata_t sock_userdata_t;
struct sock_userdata_t {
    sock_userdata_t *next;
    const char *key;
    void *data;
};

struct kuda_socket_t {
    kuda_pool_t *pool;
    int socketdes;
    int type;
    int protocol;
    kuda_sockaddr_t *local_addr;
    kuda_sockaddr_t *remote_addr;
    kuda_interval_time_t timeout; 
#ifndef HAVE_POLL
    int connected;
#endif
#if KUDA_HAVE_SOCKADDR_UN
    int bound;
#endif
    int local_port_unknown;
    int local_interface_unknown;
    int remote_addr_unknown;
    kuda_int32_t options;
    kuda_int32_t inherit;
    sock_userdata_t *userdata;
#ifndef WAITIO_USES_POLL
    /* if there is a timeout set, then this pollset is used */
    kuda_pollset_t *pollset;
#endif
};

const char *kuda_inet_ntop(int af, const void *src, char *dst, kuda_size_t size);
int kuda_inet_pton(int af, const char *src, void *dst);
void kuda_sockaddr_vars_set(kuda_sockaddr_t *, int, kuda_port_t);

#define kuda_is_option_set(skt, option)  \
    (((skt)->options & (option)) == (option))

#define kuda_set_option(skt, option, on) \
    do {                                 \
        if (on)                          \
            (skt)->options |= (option);         \
        else                             \
            (skt)->options &= ~(option);        \
    } while (0)

#endif  /* ! NETWORK_IO_H */

