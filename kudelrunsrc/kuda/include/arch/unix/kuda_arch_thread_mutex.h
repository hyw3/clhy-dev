/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef THREAD_MUTEX_H
#define THREAD_MUTEX_H

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_thread_mutex.h"
#include "kuda_thread_cond.h"
#include "kuda_portable.h"
#include "kuda_atomic.h"

#if KUDA_HAVE_PTHREAD_H
#include <pthread.h>
#endif

#if KUDA_HAS_THREADS
struct kuda_thread_mutex_t {
    kuda_pool_t *pool;
    pthread_mutex_t mutex;
    kuda_thread_cond_t *cond;
    int locked, num_waiters;
};
#endif

#endif  /* THREAD_MUTEX_H */

