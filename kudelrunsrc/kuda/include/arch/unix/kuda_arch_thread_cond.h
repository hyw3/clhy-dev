/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef THREAD_COND_H
#define THREAD_COND_H

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_thread_mutex.h"
#include "kuda_thread_cond.h"
#include "kuda_pools.h"

#if KUDA_HAVE_PTHREAD_H
#include <pthread.h>
#endif

/* XXX: Should we have a better autoconf search, something like
 * KUDA_HAS_PTHREAD_COND? -aaron */
#if KUDA_HAS_THREADS
struct kuda_thread_cond_t {
    kuda_pool_t *pool;
    pthread_cond_t cond;
};
#endif

#endif  /* THREAD_COND_H */

