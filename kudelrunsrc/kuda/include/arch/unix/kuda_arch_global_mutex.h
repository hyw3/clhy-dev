/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GLOBAL_MUTEX_H
#define GLOBAL_MUTEX_H

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_global_mutex.h"
#include "kuda_arch_proc_mutex.h"
#include "kuda_arch_thread_mutex.h"

struct kuda_global_mutex_t {
    kuda_pool_t *pool;
    kuda_proc_mutex_t *proc_mutex;
#if KUDA_HAS_THREADS
    kuda_thread_mutex_t *thread_mutex;
#endif /* KUDA_HAS_THREADS */
};

#endif  /* GLOBAL_MUTEX_H */

