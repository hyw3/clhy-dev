/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_thread_proc.h"
#include "kuda_file_io.h"
#include "kuda_arch_file_io.h"
#include "kuda_perms_set.h"

/* System headers required for thread/process library */
#if KUDA_HAVE_PTHREAD_H
#include <pthread.h>
#endif
#ifdef HAVE_SYS_RESOURCE_H
#include <sys/resource.h>
#endif
#if KUDA_HAVE_SIGNAL_H
#include <signal.h>
#endif
#if KUDA_HAVE_STRING_H
#include <string.h>
#endif
#if KUDA_HAVE_SYS_WAIT_H
#include <sys/wait.h>
#endif
#if KUDA_HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_SCHED_H
#include <sched.h>
#endif
/* End System Headers */


#ifndef THREAD_PROC_H
#define THREAD_PROC_H

#define SHELL_PATH "/bin/sh"

#if KUDA_HAS_THREADS

struct kuda_thread_t {
    kuda_pool_t *pool;
    pthread_t *td;
    void *data;
    kuda_thread_start_t func;
    kuda_status_t exitval;
};

struct kuda_threadattr_t {
    kuda_pool_t *pool;
    pthread_attr_t attr;
};

struct kuda_threadkey_t {
    kuda_pool_t *pool;
    pthread_key_t key;
};

struct kuda_thread_once_t {
    pthread_once_t once;
};

#endif

typedef struct kuda_procattr_pscb_t kuda_procattr_pscb_t;
struct kuda_procattr_pscb_t {
    struct kuda_procattr_pscb_t *next;
    kuda_perms_setfn_t *perms_set_fn;
    kuda_fileperms_t perms;
    const void *data;
};

struct kuda_procattr_t {
    kuda_pool_t *pool;
    kuda_file_t *parent_in;
    kuda_file_t *child_in;
    kuda_file_t *parent_out;
    kuda_file_t *child_out;
    kuda_file_t *parent_err;
    kuda_file_t *child_err;
    char *currdir;
    kuda_int32_t cmdtype;
    kuda_int32_t detached;
#ifdef RLIMIT_CPU
    struct rlimit *limit_cpu;
#endif
#if defined (RLIMIT_DATA) || defined (RLIMIT_VMEM) || defined(RLIMIT_AS)
    struct rlimit *limit_mem;
#endif
#ifdef RLIMIT_NPROC
    struct rlimit *limit_nproc;
#endif
#ifdef RLIMIT_NOFILE
    struct rlimit *limit_nofile;
#endif
    kuda_child_errfn_t *errfn;
    kuda_int32_t errchk;
    kuda_uid_t   uid;
    kuda_gid_t   gid;
    kuda_procattr_pscb_t *perms_set_callbacks;
};

#endif  /* ! THREAD_PROC_H */

