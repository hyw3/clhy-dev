/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INHERIT_H
#define INHERIT_H

#include "kuda_inherit.h"

#define KUDA_INHERIT (1 << 24)    /* Must not conflict with other bits */

#define KUDA_IMPLEMENT_INHERIT_SET(name, flag, pool, cleanup)        \
kuda_status_t kuda_##name##_inherit_set(kuda_##name##_t *the##name)    \
{                                                                   \
    if (the##name->flag & KUDA_FOPEN_NOCLEANUP)                      \
        return KUDA_EINVAL;                                          \
    if (!(the##name->flag & KUDA_INHERIT)) {                         \
        int flags = fcntl(the##name->name##des, F_GETFD);           \
        if (flags == -1)                                            \
            return errno;                                           \
        flags &= ~(FD_CLOEXEC);                                     \
        if (fcntl(the##name->name##des, F_SETFD, flags) == -1)      \
            return errno;                                           \
        the##name->flag |= KUDA_INHERIT;                             \
        kuda_pool_child_cleanup_set(the##name->pool,                 \
                                   (void *)the##name,               \
                                   cleanup, kuda_pool_cleanup_null); \
    }                                                               \
    return KUDA_SUCCESS;                                             \
}

#define KUDA_IMPLEMENT_INHERIT_UNSET(name, flag, pool, cleanup)      \
kuda_status_t kuda_##name##_inherit_unset(kuda_##name##_t *the##name)  \
{                                                                   \
    if (the##name->flag & KUDA_FOPEN_NOCLEANUP)                      \
        return KUDA_EINVAL;                                          \
    if (the##name->flag & KUDA_INHERIT) {                            \
        int flags;                                                  \
        if ((flags = fcntl(the##name->name##des, F_GETFD)) == -1)   \
            return errno;                                           \
        flags |= FD_CLOEXEC;                                        \
        if (fcntl(the##name->name##des, F_SETFD, flags) == -1)      \
            return errno;                                           \
        the##name->flag &= ~KUDA_INHERIT;                            \
        kuda_pool_child_cleanup_set(the##name->pool,                 \
                                   (void *)the##name,               \
                                   cleanup, cleanup);               \
    }                                                               \
    return KUDA_SUCCESS;                                             \
}

#endif	/* ! INHERIT_H */
