/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_ARCH_POLL_PRIVATE_H
#define KUDA_ARCH_POLL_PRIVATE_H

#if HAVE_POLL_H
#include <poll.h>
#endif

#if HAVE_SYS_POLL_H
#include <sys/poll.h>
#endif

#ifdef HAVE_PORT_CREATE
#include <port.h>
#include <sys/port_impl.h>
#endif

#ifdef HAVE_KQUEUE
#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>
#endif

#ifdef HAVE_EPOLL
#include <sys/epoll.h>
#endif

#ifdef NETWARE
#define HAS_SOCKETS(dt) (dt == KUDA_POLL_SOCKET) ? 1 : 0
#define HAS_PIPES(dt) (dt == KUDA_POLL_FILE) ? 1 : 0
#endif

#if defined(HAVE_AIO_H) && defined(HAVE_AIO_MSGQ)
#define _AIO_OS390	/* enable a bunch of z/PLATFORM aio.h definitions */
#include <aio.h>	/* aiocb	*/
#endif

/* Choose the best method platform specific to use in kuda_pollset */
#ifdef HAVE_KQUEUE
#define POLLSET_USES_KQUEUE
#define POLLSET_DEFAULT_METHOD KUDA_POLLSET_KQUEUE
#elif defined(HAVE_PORT_CREATE)
#define POLLSET_USES_PORT
#define POLLSET_DEFAULT_METHOD KUDA_POLLSET_PORT
#elif defined(HAVE_EPOLL)
#define POLLSET_USES_EPOLL
#define POLLSET_DEFAULT_METHOD KUDA_POLLSET_EPOLL
#elif defined(HAVE_AIO_MSGQ)
#define POLLSET_USES_AIO_MSGQ
#define POLLSET_DEFAULT_METHOD KUDA_POLLSET_AIO_MSGQ
#elif defined(HAVE_POLL)
#define POLLSET_USES_POLL
#define POLLSET_DEFAULT_METHOD KUDA_POLLSET_POLL
#else
#define POLLSET_USES_SELECT
#define POLLSET_DEFAULT_METHOD KUDA_POLLSET_SELECT
#endif

#ifdef WIN32
#define POLL_USES_SELECT
#undef POLLSET_DEFAULT_METHOD
#define POLLSET_DEFAULT_METHOD KUDA_POLLSET_SELECT
#else
#ifdef HAVE_POLL
#define POLL_USES_POLL
#else
#define POLL_USES_SELECT
#endif
#endif

#if defined(POLLSET_USES_KQUEUE) || defined(POLLSET_USES_EPOLL) || defined(POLLSET_USES_PORT) || defined(POLLSET_USES_AIO_MSGQ)

#include "kuda_ring.h"

#if KUDA_HAS_THREADS
#include "kuda_thread_mutex.h"
#define pollset_lock_rings() \
    if (pollset->flags & KUDA_POLLSET_THREADSAFE) \
        kuda_thread_mutex_lock(pollset->p->ring_lock);
#define pollset_unlock_rings() \
    if (pollset->flags & KUDA_POLLSET_THREADSAFE) \
        kuda_thread_mutex_unlock(pollset->p->ring_lock);
#else
#define pollset_lock_rings()
#define pollset_unlock_rings()
#endif

typedef struct pfd_elem_t pfd_elem_t;

struct pfd_elem_t {
    KUDA_RING_ENTRY(pfd_elem_t) link;
    kuda_pollfd_t pfd;
#ifdef HAVE_PORT_CREATE
   int on_query_ring;
#endif
};

#endif

typedef struct kuda_pollset_private_t kuda_pollset_private_t;
typedef struct kuda_pollset_provider_t kuda_pollset_provider_t;
typedef struct kuda_pollcb_provider_t kuda_pollcb_provider_t;

struct kuda_pollset_t
{
    kuda_pool_t *pool;
    kuda_uint32_t nelts;
    kuda_uint32_t nalloc;
    kuda_uint32_t flags;
    /* Pipe descriptors used for wakeup */
    kuda_file_t *wakeup_pipe[2];
    kuda_pollfd_t wakeup_pfd;
    kuda_pollset_private_t *p;
    const kuda_pollset_provider_t *provider;
};

typedef union {
#if defined(HAVE_EPOLL)
    struct epoll_event *epoll;
#endif
#if defined(HAVE_PORT_CREATE)
    port_event_t *port;
#endif
#if defined(HAVE_KQUEUE)
    struct kevent *ke;
#endif
#if defined(HAVE_POLL)
    struct pollfd *ps;
#endif
    void *undef;
} kuda_pollcb_pset;

struct kuda_pollcb_t {
    kuda_pool_t *pool;
    kuda_uint32_t nelts;
    kuda_uint32_t nalloc;
    kuda_uint32_t flags;
    /* Pipe descriptors used for wakeup */
    kuda_file_t *wakeup_pipe[2];
    kuda_pollfd_t wakeup_pfd;
    int fd;
    kuda_pollcb_pset pollset;
    kuda_pollfd_t **copyset;
    const kuda_pollcb_provider_t *provider;
};

struct kuda_pollset_provider_t {
    kuda_status_t (*create)(kuda_pollset_t *, kuda_uint32_t, kuda_pool_t *, kuda_uint32_t);
    kuda_status_t (*add)(kuda_pollset_t *, const kuda_pollfd_t *);
    kuda_status_t (*remove)(kuda_pollset_t *, const kuda_pollfd_t *);
    kuda_status_t (*poll)(kuda_pollset_t *, kuda_interval_time_t, kuda_int32_t *, const kuda_pollfd_t **);
    kuda_status_t (*cleanup)(kuda_pollset_t *);
    const char *name;
};

struct kuda_pollcb_provider_t {
    kuda_status_t (*create)(kuda_pollcb_t *, kuda_uint32_t, kuda_pool_t *, kuda_uint32_t);
    kuda_status_t (*add)(kuda_pollcb_t *, kuda_pollfd_t *);
    kuda_status_t (*remove)(kuda_pollcb_t *, kuda_pollfd_t *);
    kuda_status_t (*poll)(kuda_pollcb_t *, kuda_interval_time_t, kuda_pollcb_cb_t, void *);
    kuda_status_t (*cleanup)(kuda_pollcb_t *);
    const char *name;
};

/* 
 * Private functions used for the implementation of both kuda_pollcb_* and 
 * kuda_pollset_*
 */
kuda_status_t kuda_poll_create_wakeup_pipe(kuda_pool_t *pool, kuda_pollfd_t *pfd, 
                                         kuda_file_t **wakeup_pipe);
kuda_status_t kuda_poll_close_wakeup_pipe(kuda_file_t **wakeup_pipe);
void kuda_poll_drain_wakeup_pipe(kuda_file_t **wakeup_pipe);

#endif /* KUDA_ARCH_POLL_PRIVATE_H */
