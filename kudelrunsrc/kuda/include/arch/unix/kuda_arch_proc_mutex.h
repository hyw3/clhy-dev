/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PROC_MUTEX_H
#define PROC_MUTEX_H

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_proc_mutex.h"
#include "kuda_pools.h"
#include "kuda_portable.h"
#include "kuda_file_io.h"
#include "kuda_arch_file_io.h"
#include "kuda_time.h"

/* System headers required by Locks library */
#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#if KUDA_HAVE_STDIO_H
#include <stdio.h>
#endif
#if KUDA_HAVE_FCNTL_H
#include <fcntl.h>
#endif

#ifdef HAVE_SYS_IPC_H
#include <sys/ipc.h>
#endif
#ifdef HAVE_SYS_SEM_H
#include <sys/sem.h>
#endif
#ifdef HAVE_SYS_FILE_H
#include <sys/file.h>
#endif
#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#if KUDA_HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_SYS_MMAN_H
#include <sys/mman.h>
#endif
#if KUDA_HAVE_PTHREAD_H
#include <pthread.h>
#endif
/* End System Headers */

struct kuda_proc_mutex_unix_lock_methods_t {
    unsigned int flags;
    kuda_status_t (*create)(kuda_proc_mutex_t *, const char *);
    kuda_status_t (*acquire)(kuda_proc_mutex_t *);
    kuda_status_t (*tryacquire)(kuda_proc_mutex_t *);
    kuda_status_t (*timedacquire)(kuda_proc_mutex_t *, kuda_interval_time_t);
    kuda_status_t (*release)(kuda_proc_mutex_t *);
    kuda_status_t (*cleanup)(void *);
    kuda_status_t (*child_init)(kuda_proc_mutex_t **, kuda_pool_t *, const char *);
    kuda_status_t (*perms_set)(kuda_proc_mutex_t *, kuda_fileperms_t, kuda_uid_t, kuda_gid_t);
    kuda_lockmech_e mech;
    const char *name;
};
typedef struct kuda_proc_mutex_unix_lock_methods_t kuda_proc_mutex_unix_lock_methods_t;

/* bit values for flags field in kuda_unix_lock_methods_t */
#define KUDA_PROCESS_LOCK_MECH_IS_GLOBAL          1

#if !KUDA_HAVE_UNION_SEMUN && defined(KUDA_HAS_SYSVSEM_SERIALIZE)
union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};
#endif

struct kuda_proc_mutex_t {
    kuda_pool_t *pool;
    const kuda_proc_mutex_unix_lock_methods_t *meth;
    int curr_locked;
    char *fname;

    kuda_platform_proc_mutex_t platforms;     /* Native mutex holder. */

#if KUDA_HAS_FCNTL_SERIALIZE || KUDA_HAS_FLOCK_SERIALIZE
    kuda_file_t *interproc;      /* For kuda_file_ calls on native fd. */
    int interproc_closing;      /* whether the native fd is opened/closed with
                                 * 'interproc' or kuda_platform_file_put()ed (hence
                                 * needing an an explicit close for consistency
                                 * with other methods).
                                 */
#endif
#if KUDA_HAS_PROC_PTHREAD_SERIALIZE
    int pthread_refcounting;    /* Whether the native mutex is refcounted or
                                 * kuda_platform_proc_mutex_put()ed, which makes
                                 * refcounting impossible/undesirable.
                                 */
#endif
};

void kuda_proc_mutex_unix_setup_lock(void);

#endif  /* PROC_MUTEX_H */

