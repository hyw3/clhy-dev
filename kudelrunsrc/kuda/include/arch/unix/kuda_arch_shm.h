/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SHM_H
#define SHM_H

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_shm.h"
#include "kuda_pools.h"
#include "kuda_file_io.h"
#include "kuda_network_io.h"
#include "kuda_portable.h"

#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_SYS_MMAN_H
#include <sys/mman.h>
#endif
#ifdef HAVE_SYS_IPC_H
#include <sys/ipc.h>
#endif
#ifdef HAVE_SYS_MUTEX_H
#include <sys/mutex.h>
#endif
#ifdef HAVE_SYS_SHM_H
#include <sys/shm.h>
#endif
#if !defined(SHM_R)
#define SHM_R 0400
#endif
#if !defined(SHM_W)
#define SHM_W 0200
#endif
#ifdef HAVE_SYS_FILE_H
#include <sys/file.h>
#endif

/* Not all systems seem to have MAP_FAILED defined, but it should always
 * just be (void *)-1. */
#ifndef MAP_FAILED
#define MAP_FAILED ((void *)-1)
#endif

struct kuda_shm_t {
    kuda_pool_t *pool;
    void *base;          /* base real address */
    void *usable;        /* base usable address */
    kuda_size_t reqsize;  /* requested segment size */
    kuda_size_t realsize; /* actual segment size */
    const char *filename;      /* NULL if anonymous */
#if KUDA_USE_SHMEM_SHMGET || KUDA_USE_SHMEM_SHMGET_ANON
    int shmid;          /* shmem ID returned from shmget() */
    key_t shmkey;       /* shmem key IPC_ANON or returned from ftok() */
#endif
};

#endif /* SHM_H */
