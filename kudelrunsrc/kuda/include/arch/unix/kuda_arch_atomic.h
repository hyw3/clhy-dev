/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ATOMIC_H
#define ATOMIC_H

#include "kuda.h"
#include "kuda_pools.h"
#include "kuda_private.h"
#include "kuda_atomic.h"

#if defined(USE_ATOMICS_GENERIC)
/* noop */
#elif HAVE_ATOMIC_BUILTINS
#   define USE_ATOMICS_BUILTINS
#elif defined(SOLARIS2) && SOLARIS2 >= 10
#   define USE_ATOMICS_SOLARIS
#   define NEED_ATOMICS_GENERIC64
#elif defined(__GNUC__) && defined(__STRICT_ANSI__)
/* force use of generic atomics if building e.g. with -std=c89, which
 * doesn't allow inline asm */
#   define USE_ATOMICS_GENERIC
#elif defined(__GNUC__) && (defined(__i386__) || defined(__x86_64__))
#   define USE_ATOMICS_IA32
#   define NEED_ATOMICS_GENERIC64
#elif defined(__GNUC__) && (defined(__PPC__) || defined(__ppc__))
#   define USE_ATOMICS_PPC
#   define NEED_ATOMICS_GENERIC64
#elif defined(__GNUC__) && (defined(__s390__) || defined(__s390x__))
#   define USE_ATOMICS_S390
#   define NEED_ATOMICS_GENERIC64
#else
#   define USE_ATOMICS_GENERIC
#endif

#if defined(USE_ATOMICS_GENERIC) || defined (NEED_ATOMICS_GENERIC64)
kuda_status_t kuda__atomic_generic64_init(kuda_pool_t *p);
#endif

#endif /* ATOMIC_H */
