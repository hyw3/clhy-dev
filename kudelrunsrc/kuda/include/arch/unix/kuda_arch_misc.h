/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MISC_H
#define MISC_H

#include "kuda.h"
#include "kuda_portable.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_pools.h"
#include "kuda_getopt.h"
#include "kuda_thread_proc.h"
#include "kuda_file_io.h"
#include "kuda_errno.h"
#include "kuda_getopt.h"

#if KUDA_HAVE_STDIO_H
#include <stdio.h>
#endif
#if KUDA_HAVE_SIGNAL_H
#include <signal.h>
#endif
#if KUDA_HAVE_PTHREAD_H
#include <pthread.h>
#endif

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif
#if KUDA_HAVE_STRING_H
#include <string.h>
#endif

#ifdef BEOS
#include <kernel/OS.h>
#endif

struct kuda_other_child_rec_t {
    kuda_pool_t *p;
    struct kuda_other_child_rec_t *next;
    kuda_proc_t *proc;
    void (*maintenance) (int, void *, int);
    void *data;
    kuda_platform_file_t write_fd;
};

#if defined(WIN32) || defined(NETWARE)
#define WSAHighByte 2
#define WSALowByte 0
#endif

#endif  /* ! MISC_H */

