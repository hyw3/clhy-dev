/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_errno.h"
#include <sys/types.h>
#include <sys/socket.h>

extern int (*kuda_os2_socket)(int, int, int);
extern int (*kuda_os2_select)(int *, int, int, int, long);
extern int (*kuda_os2_sock_errno)();
extern int (*kuda_os2_accept)(int, struct sockaddr *, int *);
extern int (*kuda_os2_bind)(int, struct sockaddr *, int);
extern int (*kuda_os2_connect)(int, struct sockaddr *, int);
extern int (*kuda_os2_getpeername)(int, struct sockaddr *, int *);
extern int (*kuda_os2_getsockname)(int, struct sockaddr *, int *);
extern int (*kuda_os2_getsockopt)(int, int, int, char *, int *);
extern int (*kuda_os2_ioctl)(int, int, caddr_t, int);
extern int (*kuda_os2_listen)(int, int);
extern int (*kuda_os2_recv)(int, char *, int, int);
extern int (*kuda_os2_send)(int, const char *, int, int);
extern int (*kuda_os2_setsockopt)(int, int, int, char *, int);
extern int (*kuda_os2_shutdown)(int, int);
extern int (*kuda_os2_soclose)(int);
extern int (*kuda_os2_writev)(int, struct iovec *, int);
extern int (*kuda_os2_sendto)(int, const char *, int, int, const struct sockaddr *, int);
extern int (*kuda_os2_recvfrom)(int, char *, int, int, struct sockaddr *, int *);

#define socket kuda_os2_socket
#define select kuda_os2_select
#define sock_errno kuda_os2_sock_errno
#define accept kuda_os2_accept
#define bind kuda_os2_bind
#define connect kuda_os2_connect
#define getpeername kuda_os2_getpeername
#define getsockname kuda_os2_getsockname
#define getsockopt kuda_os2_getsockopt
#define ioctl kuda_os2_ioctl
#define listen kuda_os2_listen
#define recv kuda_os2_recv
#define send kuda_os2_send
#define setsockopt kuda_os2_setsockopt
#define shutdown kuda_os2_shutdown
#define soclose kuda_os2_soclose
#define writev kuda_os2_writev
#define sendto kuda_os2_sendto
#define recvfrom kuda_os2_recvfrom
