/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INHERIT_H
#define INHERIT_H

#include "kuda_inherit.h"

#define KUDA_INHERIT (1 << 24)    /* Must not conflict with other bits */

#define KUDA_IMPLEMENT_INHERIT_SET(name, flag, pool, cleanup)        \
KUDA_DECLARE(kuda_status_t) kuda_##name##_inherit_set(kuda_##name##_t *the##name) \
{                                                                   \
    int rv;                                                         \
    ULONG state;                                                    \
    if (((rv = DosQueryFHState(attr->parent_err->filedes, &state))  \
            != 0) ||                                                \
        ((rv = DosSetFHState(attr->parent_err->filedes,             \
                            state & ~OPEN_FLAGS_NOINHERIT)) != 0))  \
        return KUDA_FROM_PLATFORM_ERROR(rv);                               \
    return KUDA_SUCCESS;                                             \
}

#define KUDA_IMPLEMENT_INHERIT_UNSET(name, flag, pool, cleanup)      \
KUDA_DECLARE(kuda_status_t) kuda_##name##_inherit_unset(kuda_##name##_t *the##name)\
{                                                                   \
    int rv;                                                         \
    ULONG state;                                                    \
    if (((rv = DosQueryFHState(attr->parent_err->filedes, &state))  \
            != 0) ||                                                \
        ((rv = DosSetFHState(attr->parent_err->filedes,             \
                            state | OPEN_FLAGS_NOINHERIT)) != 0))   \
        return KUDA_FROM_PLATFORM_ERROR(rv);                               \
    return KUDA_SUCCESS;                                             \
}

#endif	/* ! INHERIT_H */
