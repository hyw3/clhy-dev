/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FILE_IO_H
#define FILE_IO_H

#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_thread_mutex.h"
#include "kuda_file_io.h"
#include "kuda_file_info.h"
#include "kuda_errno.h"
#include "kuda_poll.h"

/* We have an implementation of mkstemp but it's not very multi-threading 
 * friendly & is part of the POSIX emulation rather than native so don't
 * use it.
 */
#undef HAVE_MKSTEMP

#define KUDA_FILE_DEFAULT_BUFSIZE 4096
#define KUDA_FILE_BUFSIZE KUDA_FILE_DEFAULT_BUFSIZE

struct kuda_file_t {
    kuda_pool_t *pool;
    HFILE filedes;
    char * fname;
    int isopen;
    int buffered;
    int eof_hit;
    kuda_int32_t flags;
    int timeout;
    int pipe;
    HEV pipeSem;
    enum { BLK_UNKNOWN, BLK_OFF, BLK_ON } blocking;

    /* Stuff for buffered mode */
    char *buffer;
    kuda_size_t bufsize;        /* Read/Write position in buffer             */
    kuda_size_t bufpos;         /* Read/Write position in buffer             */
    unsigned long dataRead;    /* amount of valid data read into buffer     */
    int direction;             /* buffer being used for 0 = read, 1 = write */
    unsigned long filePtr;     /* position in file of handle                */
    kuda_thread_mutex_t *mutex; /* mutex semaphore, must be owned to access
                                  the above fields                          */
};

struct kuda_dir_t {
    kuda_pool_t *pool;
    char *dirname;
    ULONG handle;
    FILEFINDBUF3 entry;
    int validentry;
};

kuda_status_t kuda_file_cleanup(void *);
kuda_status_t kuda_os2_time_to_kuda_time(kuda_time_t *result, FDATE os2date, 
                                      FTIME os2time);
kuda_status_t kuda_kuda_time_to_os2_time(FDATE *os2date, FTIME *os2time,
                                      kuda_time_t kudatime);

/* see win32/fileio.h for description of these */
extern const char c_is_fnchar[256];

#define IS_FNCHAR(c) c_is_fnchar[(unsigned char)c]

kuda_status_t filepath_root_test(char *path, kuda_pool_t *p);
kuda_status_t filepath_drive_get(char **rootpath, char drive, 
                                kuda_int32_t flags, kuda_pool_t *p);
kuda_status_t filepath_root_case(char **rootpath, char *root, kuda_pool_t *p);

#endif  /* ! FILE_IO_H */

