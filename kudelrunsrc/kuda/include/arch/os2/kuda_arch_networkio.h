/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NETWORK_IO_H
#define NETWORK_IO_H

#include "kuda_private.h"
#include "kuda_network_io.h"
#include "kuda_general.h"
#include "kuda_arch_os2calls.h"
#include "kuda_poll.h"

#if KUDA_HAVE_NETDB_H
#include <netdb.h>
#endif

typedef struct sock_userdata_t sock_userdata_t;
struct sock_userdata_t {
    sock_userdata_t *next;
    const char *key;
    void *data;
};

struct kuda_socket_t {
    kuda_pool_t *pool;
    int socketdes;
    int type;
    int protocol;
    kuda_sockaddr_t *local_addr;
    kuda_sockaddr_t *remote_addr;
    kuda_interval_time_t timeout;
    int nonblock;
    int local_port_unknown;
    int local_interface_unknown;
    int remote_addr_unknown;
    kuda_int32_t options;
    kuda_int32_t inherit;
    sock_userdata_t *userdata;

    /* if there is a timeout set, then this pollset is used */
    kuda_pollset_t *pollset;
};

/* Error codes returned from sock_errno() */
#define SOCBASEERR              10000
#define SOCEPERM                (SOCBASEERR+1)             /* Not owner */
#define SOCESRCH                (SOCBASEERR+3)             /* No such process */
#define SOCEINTR                (SOCBASEERR+4)             /* Interrupted system call */
#define SOCENXIO                (SOCBASEERR+6)             /* No such device or address */
#define SOCEBADF                (SOCBASEERR+9)             /* Bad file number */
#define SOCEACCES               (SOCBASEERR+13)            /* Permission denied */
#define SOCEFAULT               (SOCBASEERR+14)            /* Bad address */
#define SOCEINVAL               (SOCBASEERR+22)            /* Invalid argument */
#define SOCEMFILE               (SOCBASEERR+24)            /* Too many open files */
#define SOCEPIPE                (SOCBASEERR+32)            /* Broken pipe */
#define SOCEOS2ERR              (SOCBASEERR+100)            /* OS2 Error */

const char *kuda_inet_ntop(int af, const void *src, char *dst, kuda_size_t size);
int kuda_inet_pton(int af, const char *src, void *dst);
void kuda_sockaddr_vars_set(kuda_sockaddr_t *, int, kuda_port_t);

#endif  /* ! NETWORK_IO_H */

