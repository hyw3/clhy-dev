/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_thread_proc.h"
#include "kuda_file_io.h"

#ifndef THREAD_PROC_H
#define THREAD_PROC_H

#define KUDA_THREADATTR_DETACHED 1

#define SHELL_PATH "cmd.exe"
#define KUDA_THREAD_STACKSIZE 65536

struct kuda_threadattr_t {
    kuda_pool_t *pool;
    unsigned long attr;
    kuda_size_t stacksize;
};

struct kuda_thread_t {
    kuda_pool_t *pool;
    struct kuda_threadattr_t *attr;
    unsigned long tid;
    kuda_thread_start_t func;
    void *data;
    kuda_status_t exitval;
};

struct kuda_threadkey_t {
    kuda_pool_t *pool;
    unsigned long *key;
};

struct kuda_procattr_t {
    kuda_pool_t *pool;
    kuda_file_t *parent_in;
    kuda_file_t *child_in;
    kuda_file_t *parent_out;
    kuda_file_t *child_out;
    kuda_file_t *parent_err;
    kuda_file_t *child_err;
    char *currdir;
    kuda_int32_t cmdtype;
    kuda_int32_t detached;
};

struct kuda_thread_once_t {
    unsigned long sem;
    char hit;
};

#endif  /* ! THREAD_PROC_H */

