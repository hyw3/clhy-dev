/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_THREAD_RWLOCK_H
#define KUDA_THREAD_RWLOCK_H

/**
 * @file kuda_thread_rwlock.h
 * @brief KUDA Reader/Writer Lock Routines
 */

#include "kuda.h"
#include "kuda_pools.h"
#include "kuda_errno.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#if KUDA_HAS_THREADS

/**
 * @defgroup kuda_thread_rwlock Reader/Writer Lock Routines
 * @ingroup KUDA 
 * @{
 */

/** Opaque read-write thread-safe lock. */
typedef struct kuda_thread_rwlock_t kuda_thread_rwlock_t;

/**
 * Note: The following operations have undefined results: unlocking a
 * read-write lock which is not locked in the calling thread; write
 * locking a read-write lock which is already locked by the calling
 * thread; destroying a read-write lock more than once; clearing or
 * destroying the pool from which a <b>locked</b> read-write lock is
 * allocated.
 */

/**
 * Create and initialize a read-write lock that can be used to synchronize
 * threads.
 * @param rwlock the memory address where the newly created readwrite lock
 *        will be stored.
 * @param pool the pool from which to allocate the mutex.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_create(kuda_thread_rwlock_t **rwlock,
                                                   kuda_pool_t *pool);
/**
 * Acquire a shared-read lock on the given read-write lock. This will allow
 * multiple threads to enter the same critical section while they have acquired
 * the read lock.
 * @param rwlock the read-write lock on which to acquire the shared read.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_rdlock(kuda_thread_rwlock_t *rwlock);

/**
 * Attempt to acquire the shared-read lock on the given read-write lock. This
 * is the same as kuda_thread_rwlock_rdlock(), only that the function fails
 * if there is another thread holding the write lock, or if there are any
 * write threads blocking on the lock. If the function fails for this case,
 * KUDA_EBUSY will be returned. Note: it is important that the
 * KUDA_STATUS_IS_EBUSY(s) macro be used to determine if the return value was
 * KUDA_EBUSY, for portability reasons.
 * @param rwlock the rwlock on which to attempt the shared read.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_tryrdlock(kuda_thread_rwlock_t *rwlock);

/**
 * Acquire an exclusive-write lock on the given read-write lock. This will
 * allow only one single thread to enter the critical sections. If there
 * are any threads currently holding the read-lock, this thread is put to
 * sleep until it can have exclusive access to the lock.
 * @param rwlock the read-write lock on which to acquire the exclusive write.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_wrlock(kuda_thread_rwlock_t *rwlock);

/**
 * Attempt to acquire the exclusive-write lock on the given read-write lock. 
 * This is the same as kuda_thread_rwlock_wrlock(), only that the function fails
 * if there is any other thread holding the lock (for reading or writing),
 * in which case the function will return KUDA_EBUSY. Note: it is important
 * that the KUDA_STATUS_IS_EBUSY(s) macro be used to determine if the return
 * value was KUDA_EBUSY, for portability reasons.
 * @param rwlock the rwlock on which to attempt the exclusive write.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_trywrlock(kuda_thread_rwlock_t *rwlock);

/**
 * Release either the read or write lock currently held by the calling thread
 * associated with the given read-write lock.
 * @param rwlock the read-write lock to be released (unlocked).
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_unlock(kuda_thread_rwlock_t *rwlock);

/**
 * Destroy the read-write lock and free the associated memory.
 * @param rwlock the rwlock to destroy.
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_destroy(kuda_thread_rwlock_t *rwlock);

/**
 * Get the pool used by this thread_rwlock.
 * @return kuda_pool_t the pool
 */
KUDA_POOL_DECLARE_ACCESSOR(thread_rwlock);

#endif  /* KUDA_HAS_THREADS */

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_THREAD_RWLOCK_H */
