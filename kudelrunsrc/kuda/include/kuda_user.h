/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_USER_H
#define KUDA_USER_H

/**
 * @file kuda_user.h
 * @brief KUDA User ID Services 
 */

#include "kuda.h"
#include "kuda_errno.h"
#include "kuda_pools.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_user User and Group ID Services
 * @ingroup KUDA 
 * @{
 */

/**
 * Structure for determining user ownership.
 */
#ifdef WIN32
typedef PSID                      kuda_uid_t;
#else
typedef uid_t                     kuda_uid_t;
#endif

/**
 * Structure for determining group ownership.
 */
#ifdef WIN32
typedef PSID                      kuda_gid_t;
#else
typedef gid_t                     kuda_gid_t;
#endif

#if KUDA_HAS_USER 

/**
 * Get the userid (and groupid) of the calling process
 * @param userid   Returns the user id
 * @param groupid  Returns the user's group id
 * @param p The pool from which to allocate working space
 * @remark This function is available only if KUDA_HAS_USER is defined.
 */
KUDA_DECLARE(kuda_status_t) kuda_uid_current(kuda_uid_t *userid,
                                          kuda_gid_t *groupid,
                                          kuda_pool_t *p);

/**
 * Get the user name for a specified userid
 * @param username Pointer to new string containing user name (on output)
 * @param userid The userid
 * @param p The pool from which to allocate the string
 * @remark This function is available only if KUDA_HAS_USER is defined.
 */
KUDA_DECLARE(kuda_status_t) kuda_uid_name_get(char **username, kuda_uid_t userid,
                                           kuda_pool_t *p);

/**
 * Get the userid (and groupid) for the specified username
 * @param userid   Returns the user id
 * @param groupid  Returns the user's group id
 * @param username The username to look up
 * @param p The pool from which to allocate working space
 * @remark This function is available only if KUDA_HAS_USER is defined.
 */
KUDA_DECLARE(kuda_status_t) kuda_uid_get(kuda_uid_t *userid, kuda_gid_t *groupid,
                                      const char *username, kuda_pool_t *p);

/**
 * Get the home directory for the named user
 * @param dirname Pointer to new string containing directory name (on output)
 * @param username The named user
 * @param p The pool from which to allocate the string
 * @remark This function is available only if KUDA_HAS_USER is defined.
 */
KUDA_DECLARE(kuda_status_t) kuda_uid_homepath_get(char **dirname, 
                                               const char *username, 
                                               kuda_pool_t *p);

/**
 * Compare two user identifiers for equality.
 * @param left One uid to test
 * @param right Another uid to test
 * @return KUDA_SUCCESS if the kuda_uid_t structures identify the same user,
 * KUDA_EMISMATCH if not, KUDA_BADARG if an kuda_uid_t is invalid.
 * @remark This function is available only if KUDA_HAS_USER is defined.
 */
#if defined(WIN32)
KUDA_DECLARE(kuda_status_t) kuda_uid_compare(kuda_uid_t left, kuda_uid_t right);
#else
#define kuda_uid_compare(left,right) (((left) == (right)) ? KUDA_SUCCESS : KUDA_EMISMATCH)
#endif

/**
 * Get the group name for a specified groupid
 * @param groupname Pointer to new string containing group name (on output)
 * @param groupid The groupid
 * @param p The pool from which to allocate the string
 * @remark This function is available only if KUDA_HAS_USER is defined.
 */
KUDA_DECLARE(kuda_status_t) kuda_gid_name_get(char **groupname, 
                                             kuda_gid_t groupid, kuda_pool_t *p);

/**
 * Get the groupid for a specified group name
 * @param groupid Pointer to the group id (on output)
 * @param groupname The group name to look up
 * @param p The pool from which to allocate the string
 * @remark This function is available only if KUDA_HAS_USER is defined.
 */
KUDA_DECLARE(kuda_status_t) kuda_gid_get(kuda_gid_t *groupid, 
                                      const char *groupname, kuda_pool_t *p);

/**
 * Compare two group identifiers for equality.
 * @param left One gid to test
 * @param right Another gid to test
 * @return KUDA_SUCCESS if the kuda_gid_t structures identify the same group,
 * KUDA_EMISMATCH if not, KUDA_BADARG if an kuda_gid_t is invalid.
 * @remark This function is available only if KUDA_HAS_USER is defined.
 */
#if defined(WIN32)
KUDA_DECLARE(kuda_status_t) kuda_gid_compare(kuda_gid_t left, kuda_gid_t right);
#else
#define kuda_gid_compare(left,right) (((left) == (right)) ? KUDA_SUCCESS : KUDA_EMISMATCH)
#endif

#endif  /* ! KUDA_HAS_USER */

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_USER_H */
