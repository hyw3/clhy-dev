/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_CLSERVICES_H
#define KUDA_CLSERVICES_H

/**
 * @file kuda_clservices.h
 * @brief KUDA CLServices functions
 */

#include "kuda.h"
#include "kuda_network_io.h"
#include "kuda_file_io.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_clservices Internal KUDA support functions
 * @ingroup KUDA 
 * @{
 */

/**
 * Wait for IO to occur or timeout.
 *
 * @param f The file to wait on.
 * @param s The socket to wait on if @a f is @c NULL.
 * @param for_read If non-zero wait for data to be available to read,
 *        otherwise wait for data to be able to be written. 
 * @return KUDA_TIMEUP if we run out of time.
 */
kuda_status_t kuda_wait_for_io_or_timeout(kuda_file_t *f, kuda_socket_t *s,
                                        int for_read);

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_CLSERVICES_H */
