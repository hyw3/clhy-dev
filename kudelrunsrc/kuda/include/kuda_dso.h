/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_DSO_DOT_H
#define KUDA_DSO_DOT_H

/**
 * @file kuda_dso.h
 * @brief KUDA Dynamic Object Handling Routines
 */

#include "kuda.h"
#include "kuda_pools.h"
#include "kuda_errno.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup kuda_dso Dynamic Object Handling
 * @ingroup KUDA 
 * @{
 */

#if KUDA_HAS_DSO || defined(DOXYGEN)

/**
 * Structure for referencing dynamic objects
 */
typedef struct kuda_dso_handle_t       kuda_dso_handle_t;

/**
 * Structure for referencing symbols from dynamic objects
 */
typedef void *                        kuda_dso_handle_sym_t;

/**
 * Load a DSO library.
 * @param res_handle Location to store new handle for the DSO.
 * @param path Path to the DSO library
 * @param ctx Pool to use.
 * @bug We aught to provide an alternative to RTLD_GLOBAL, which
 * is the only supported method of loading DSOs today.
 */
KUDA_DECLARE(kuda_status_t) kuda_dso_load(kuda_dso_handle_t **res_handle, 
                                       const char *path, kuda_pool_t *ctx);

/**
 * Close a DSO library.
 * @param handle handle to close.
 */
KUDA_DECLARE(kuda_status_t) kuda_dso_unload(kuda_dso_handle_t *handle);

/**
 * Load a symbol from a DSO handle.
 * @param ressym Location to store the loaded symbol
 * @param handle handle to load the symbol from.
 * @param symname Name of the symbol to load.
 */
KUDA_DECLARE(kuda_status_t) kuda_dso_sym(kuda_dso_handle_sym_t *ressym, 
                                      kuda_dso_handle_t *handle,
                                      const char *symname);

/**
 * Report more information when a DSO function fails.
 * @param dso The dso handle that has been opened
 * @param buf Location to store the dso error
 * @param bufsize The size of the provided buffer
 */
KUDA_DECLARE(const char *) kuda_dso_error(kuda_dso_handle_t *dso, char *buf, kuda_size_t bufsize);

#endif /* KUDA_HAS_DSO */

/** @} */

#ifdef __cplusplus
}
#endif

#endif
