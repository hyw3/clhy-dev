/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_SKIPLIST_H
#define KUDA_SKIPLIST_H
/**
 * @file kuda_skiplist.h
 * @brief KUDA skip list implementation
 */

#include "kuda.h"
#include "kuda_portable.h"
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_skiplist Skip list implementation
 * Refer to http://en.wikipedia.org/wiki/Skip_list for information
 * about the purpose of and ideas behind skip lists.
 * @ingroup KUDA
 * @{
 */

/**
 * kuda_skiplist_compare is the function type that must be implemented 
 * per object type that is used in a skip list for comparisons to maintain
 * order
 * */
typedef int (*kuda_skiplist_compare) (void *, void *);

/**
 * kuda_skiplist_freefunc is the function type that must be implemented
 * to handle elements as they are removed from a skip list.
 */
typedef void (*kuda_skiplist_freefunc) (void *);

/** Opaque structure used to represent the skip list */
struct kuda_skiplist;
/** Opaque structure used to represent the skip list */
typedef struct kuda_skiplist kuda_skiplist;

/** 
 * Opaque structure used to represent abstract nodes in the skip list
 * (an abstraction above the raw elements which are collected in the
 * skip list).
 */
struct kuda_skiplistnode;
/** Opaque structure */
typedef struct kuda_skiplistnode kuda_skiplistnode;

/**
 * Allocate memory using the same mechanism as the skip list.
 * @param sl The skip list
 * @param size The amount to allocate
 * @remark If a pool was provided to kuda_skiplist_init(), memory will
 * be allocated from the pool or from a free list maintained with
 * the skip list.  Otherwise, memory will be allocated using the
 * C standard library heap functions.
 */
KUDA_DECLARE(void *) kuda_skiplist_alloc(kuda_skiplist *sl, size_t size);

/**
 * Free memory using the same mechanism as the skip list.
 * @param sl The skip list
 * @param mem The object to free
 * @remark If a pool was provided to kuda_skiplist_init(), memory will
 * be added to a free list maintained with the skip list and be available
 * to operations on the skip list or to other calls to kuda_skiplist_alloc().
 * Otherwise, memory will be freed using the  C standard library heap
 * functions.
 */
KUDA_DECLARE(void) kuda_skiplist_free(kuda_skiplist *sl, void *mem);

/**
 * Allocate a new skip list
 * @param sl The pointer in which to return the newly created skip list
 * @param p The pool from which to allocate the skip list (optional).
 * @remark Unlike most KUDA functions, a pool is optional.  If no pool
 * is provided, the C standard library heap functions will be used instead.
 */
KUDA_DECLARE(kuda_status_t) kuda_skiplist_init(kuda_skiplist **sl, kuda_pool_t *p);

/**
 * Set the comparison functions to be used for searching the skip list.
 * @param sl The skip list
 * @param XXX1 FIXME
 * @param XXX2 FIXME
 *
 * @remark If existing comparison functions are being replaced, the index
 * will be replaced during this call.  That is a potentially expensive
 * operation.
 */
KUDA_DECLARE(void) kuda_skiplist_set_compare(kuda_skiplist *sl, kuda_skiplist_compare XXX1,
                             kuda_skiplist_compare XXX2);

/**
 * Set the indexing functions to the specified comparison functions and
 * rebuild the index.
 * @param sl The skip list
 * @param XXX1 FIXME
 * @param XXX2 FIXME
 *
 * @remark If an index already exists, it will not be replaced and the
 * comparison functions will not be changed.
 */
KUDA_DECLARE(void) kuda_skiplist_add_index(kuda_skiplist *sl, kuda_skiplist_compare XXX1,
                        kuda_skiplist_compare XXX2);

/**
 * Return the list maintained by the skip list abstraction.
 * @param sl The skip list
 */
KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_getlist(kuda_skiplist *sl);

/**
 * Return the next matching element in the skip list using the specified
 * comparison function.
 * @param sl The skip list
 * @param data The value to search for
 * @param iter A pointer to the returned skip list node representing the element
 * found
 * @param func The comparison function to use
 */
KUDA_DECLARE(void *) kuda_skiplist_find_compare(kuda_skiplist *sl,
                               void *data,
                               kuda_skiplistnode **iter,
                               kuda_skiplist_compare func);

/**
 * Return the next matching element in the skip list using the current comparison
 * function.
 * @param sl The skip list
 * @param data The value to search for
 * @param iter A pointer to the returned skip list node representing the element
 * found
 */
KUDA_DECLARE(void *) kuda_skiplist_find(kuda_skiplist *sl, void *data, kuda_skiplistnode **iter);

/**
 * Return the last matching element in the skip list using the specified
 * comparison function.
 * @param sl The skip list
 * @param data The value to search for
 * @param iter A pointer to the returned skip list node representing the element
 * found
 * @param comp The comparison function to use
 */
KUDA_DECLARE(void *) kuda_skiplist_last_compare(kuda_skiplist *sl, void *data,
                                              kuda_skiplistnode **iter,
                                              kuda_skiplist_compare comp);

/**
 * Return the last matching element in the skip list using the current comparison
 * function.
 * @param sl The skip list
 * @param data The value to search for
 * @param iter A pointer to the returned skip list node representing the element
 * found
 */
KUDA_DECLARE(void *) kuda_skiplist_last(kuda_skiplist *sl, void *data,
                                      kuda_skiplistnode **iter);

/**
 * Return the next element in the skip list.
 * @param sl The skip list
 * @param iter On entry, a pointer to the skip list node to start with; on return,
 * a pointer to the skip list node representing the element returned
 * @remark If iter points to a NULL value on entry, NULL will be returned.
 */
KUDA_DECLARE(void *) kuda_skiplist_next(kuda_skiplist *sl, kuda_skiplistnode **iter);

/**
 * Return the previous element in the skip list.
 * @param sl The skip list
 * @param iter On entry, a pointer to the skip list node to start with; on return,
 * a pointer to the skip list node representing the element returned
 * @remark If iter points to a NULL value on entry, NULL will be returned.
 */
KUDA_DECLARE(void *) kuda_skiplist_previous(kuda_skiplist *sl, kuda_skiplistnode **iter);

/**
 * Return the element of the skip list node
 * @param iter The skip list node
 */
KUDA_DECLARE(void *) kuda_skiplist_element(kuda_skiplistnode *iter);

/**
 * Insert an element into the skip list using the specified comparison function
 * if it does not already exist.
 * @param sl The skip list
 * @param data The element to insert
 * @param comp The comparison function to use for placement into the skip list
 */
KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_insert_compare(kuda_skiplist *sl,
                                          void *data, kuda_skiplist_compare comp);

/**
 * Insert an element into the skip list using the existing comparison function
 * if it does not already exist.
 * @param sl The skip list
 * @param data The element to insert
 * @remark If no comparison function has been set for the skip list, the element
 * will not be inserted and NULL will be returned.
 */
KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_insert(kuda_skiplist* sl, void *data);

/**
 * Add an element into the skip list using the specified comparison function
 * allowing for duplicates.
 * @param sl The skip list
 * @param data The element to add
 * @param comp The comparison function to use for placement into the skip list
 */
KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_add_compare(kuda_skiplist *sl,
                                          void *data, kuda_skiplist_compare comp);

/**
 * Add an element into the skip list using the existing comparison function
 * allowing for duplicates.
 * @param sl The skip list
 * @param data The element to insert
 * @remark If no comparison function has been set for the skip list, the element
 * will not be inserted and NULL will be returned.
 */
KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_add(kuda_skiplist* sl, void *data);

/**
 * Add an element into the skip list using the specified comparison function
 * removing the existing duplicates.
 * @param sl The skip list
 * @param data The element to insert
 * @param comp The comparison function to use for placement into the skip list
 * @param myfree A function to be called for each removed duplicate
 * @remark If no comparison function has been set for the skip list, the element
 * will not be inserted, none will be replaced, and NULL will be returned.
 */
KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_replace_compare(kuda_skiplist *sl,
                                    void *data, kuda_skiplist_freefunc myfree,
                                    kuda_skiplist_compare comp);

/**
 * Add an element into the skip list using the existing comparison function
 * removing the existing duplicates.
 * @param sl The skip list
 * @param data The element to insert
 * @param myfree A function to be called for each removed duplicate
 * @remark If no comparison function has been set for the skip list, the element
 * will not be inserted, none will be replaced, and NULL will be returned.
 */
KUDA_DECLARE(kuda_skiplistnode *) kuda_skiplist_replace(kuda_skiplist *sl,
                                    void *data, kuda_skiplist_freefunc myfree);

/**
 * Remove a node from the skip list.
 * @param sl The skip list
 * @param iter The skip list node to remove
 * @param myfree A function to be called for the removed element
 */
KUDA_DECLARE(int) kuda_skiplist_remove_node(kuda_skiplist *sl,
                                          kuda_skiplistnode *iter,
                                          kuda_skiplist_freefunc myfree);

/**
 * Remove an element from the skip list using the specified comparison function for
 * locating the element. In the case of duplicates, the 1st entry will be removed.
 * @param sl The skip list
 * @param data The element to remove
 * @param myfree A function to be called for each removed element
 * @param comp The comparison function to use for placement into the skip list
 * @remark If the element is not found, 0 will be returned.  Otherwise, the heightXXX
 * will be returned.
 */
KUDA_DECLARE(int) kuda_skiplist_remove_compare(kuda_skiplist *sl, void *data,
                               kuda_skiplist_freefunc myfree, kuda_skiplist_compare comp);

/**
 * Remove an element from the skip list using the existing comparison function for
 * locating the element. In the case of duplicates, the 1st entry will be removed.
 * @param sl The skip list
 * @param data The element to remove
 * @param myfree A function to be called for each removed element
 * @remark If the element is not found, 0 will be returned.  Otherwise, the heightXXX
 * will be returned.
 * @remark If no comparison function has been set for the skip list, the element
 * will not be removed and 0 will be returned.
 */
KUDA_DECLARE(int) kuda_skiplist_remove(kuda_skiplist *sl, void *data, kuda_skiplist_freefunc myfree);

/**
 * Remove all elements from the skip list.
 * @param sl The skip list
 * @param myfree A function to be called for each removed element
 */
KUDA_DECLARE(void) kuda_skiplist_remove_all(kuda_skiplist *sl, kuda_skiplist_freefunc myfree);

/**
 * Remove each element from the skip list.
 * @param sl The skip list
 * @param myfree A function to be called for each removed element
 */
KUDA_DECLARE(void) kuda_skiplist_destroy(kuda_skiplist *sl, kuda_skiplist_freefunc myfree);

/**
 * Return the first element in the skip list, removing the element from the skip list.
 * @param sl The skip list
 * @param myfree A function to be called for the removed element
 * @remark NULL will be returned if there are no elements
 */
KUDA_DECLARE(void *) kuda_skiplist_pop(kuda_skiplist *sl, kuda_skiplist_freefunc myfree);

/**
 * Return the first element in the skip list, leaving the element in the skip list.
 * @param sl The skip list
 * @remark NULL will be returned if there are no elements
 */
KUDA_DECLARE(void *) kuda_skiplist_peek(kuda_skiplist *sl);

/**
 * Return the size of the list (number of elements), in O(1).
 * @param sl The skip list
 */
KUDA_DECLARE(size_t) kuda_skiplist_size(const kuda_skiplist *sl);

/**
 * Return the height of the list (number of skip paths), in O(1).
 * @param sl The skip list
 */
KUDA_DECLARE(int) kuda_skiplist_height(const kuda_skiplist *sl);

/**
 * Return the predefined maximum height of the skip list.
 * @param sl The skip list
 */
KUDA_DECLARE(int) kuda_skiplist_preheight(const kuda_skiplist *sl);

/**
 * Set a predefined maximum height for the skip list.
 * @param sl The skip list
 * @param to The preheight to set, or a nul/negative value to disable.
 * @remark When a preheight is used, the height of each inserted element is
 * computed randomly up to this preheight instead of the current skip list's
 * height plus one used by the default implementation. Using a preheight can
 * probably ensure more fairness with long living elements (since with an
 * adaptative height, former elements may have been created with a low height,
 * hence a longest path to reach them while the skip list grows). On the other
 * hand, the default behaviour (preheight <= 0) with a growing and decreasing
 * maximum height is more adaptative/suitable for short living values.
 * @note Should be called before any insertion/add.
 */
KUDA_DECLARE(void) kuda_skiplist_set_preheight(kuda_skiplist *sl, int to);

/**
 * Merge two skip lists.  XXX SEMANTICS
 * @param sl1 One of two skip lists to be merged
 * @param sl2 The other of two skip lists to be merged
 */
KUDA_DECLARE(kuda_skiplist *) kuda_skiplist_merge(kuda_skiplist *sl1, kuda_skiplist *sl2);

/** @} */

#ifdef __cplusplus
}
#endif

#endif /* ! KUDA_SKIPLIST_H */
