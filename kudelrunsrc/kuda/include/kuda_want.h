/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"        /* configuration data */
/**
 * @file kuda_want.h
 * @brief KUDA Standard Headers Support
 *
 * <PRE>
 * Features:
 *
 *   KUDA_WANT_STRFUNC:  strcmp, strcat, strcpy, etc
 *   KUDA_WANT_MEMFUNC:  memcmp, memcpy, etc
 *   KUDA_WANT_STDIO:    <stdio.h> and related bits
 *   KUDA_WANT_IOVEC:    struct iovec
 *   KUDA_WANT_BYTEFUNC: htons, htonl, ntohl, ntohs
 *
 * Typical usage:
 *
 *   \#define KUDA_WANT_STRFUNC
 *   \#define KUDA_WANT_MEMFUNC
 *   \#include "kuda_want.h"
 *
 * The appropriate headers will be included.
 *
 * Note: it is safe to use this in a header (it won't interfere with other
 *       headers' or source files' use of kuda_want.h)
 * </PRE>
 */

/* --------------------------------------------------------------------- */

#ifdef KUDA_WANT_STRFUNC

#if KUDA_HAVE_STRING_H
#include <string.h>
#endif
#if KUDA_HAVE_STRINGS_H
#include <strings.h>
#endif

#undef KUDA_WANT_STRFUNC
#endif

/* --------------------------------------------------------------------- */

#ifdef KUDA_WANT_MEMFUNC

#if KUDA_HAVE_STRING_H
#include <string.h>
#endif

#undef KUDA_WANT_MEMFUNC
#endif

/* --------------------------------------------------------------------- */

#ifdef KUDA_WANT_STDIO

#if KUDA_HAVE_STDIO_H
#include <stdio.h>
#endif

#undef KUDA_WANT_STDIO
#endif

/* --------------------------------------------------------------------- */

#ifdef KUDA_WANT_IOVEC

#if KUDA_HAVE_IOVEC

#if KUDA_HAVE_SYS_UIO_H
#include <sys/uio.h>
#endif

#else

#ifndef KUDA_IOVEC_DEFINED
#define KUDA_IOVEC_DEFINED
struct iovec
{
    void *iov_base;
    size_t iov_len;
};
#endif /* !KUDA_IOVEC_DEFINED */

#endif /* KUDA_HAVE_IOVEC */

#undef KUDA_WANT_IOVEC
#endif

/* --------------------------------------------------------------------- */

#ifdef KUDA_WANT_BYTEFUNC

/* Single Unix says they are in arpa/inet.h.  Linux has them in
 * netinet/in.h.  FreeBSD has them in arpa/inet.h but requires that
 * netinet/in.h be included first.
 */
#if KUDA_HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif
#if KUDA_HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif

#undef KUDA_WANT_BYTEFUNC
#endif

/* --------------------------------------------------------------------- */
