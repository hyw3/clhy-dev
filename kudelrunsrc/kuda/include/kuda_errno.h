/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_ERRNO_H
#define KUDA_ERRNO_H

/**
 * @file kuda_errno.h
 * @brief KUDA Error Codes
 */

#include "kuda.h"

#if KUDA_HAVE_ERRNO_H
#include <errno.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_errno Error Codes
 * @ingroup KUDA
 * @{
 */

/**
 * Type for specifying an error or status code.
 */
typedef int kuda_status_t;

/**
 * Return a human readable string describing the specified error.
 * @param statcode The error code to get a string for.
 * @param buf A buffer to hold the error string.
 * @param bufsize Size of the buffer to hold the string.
 */
KUDA_DECLARE(char *) kuda_strerror(kuda_status_t statcode, char *buf,
                                 kuda_size_t bufsize);

#if defined(DOXYGEN)
/**
 * @def KUDA_FROM_PLATFORM_ERROR(platform_err_type syserr)
 * Fold a platform specific error into an kuda_status_t code.
 * @return kuda_status_t
 * @param e The platform platforms error code.
 * @warning  macro implementation; the syserr argument may be evaluated
 *      multiple times.
 */
#define KUDA_FROM_PLATFORM_ERROR(e) (e == 0 ? KUDA_SUCCESS : e + KUDA_PLATFORM_START_SYSERR)

/**
 * @def KUDA_TO_PLATFORM_ERROR(kuda_status_t statcode)
 * @return platform_err_type
 * Fold an kuda_status_t code back to the native platform defined error.
 * @param e The kuda_status_t folded platform platforms error code.
 * @warning  macro implementation; the statcode argument may be evaluated
 *      multiple times.  If the statcode was not created by kuda_get_platform_error
 *      or KUDA_FROM_PLATFORM_ERROR, the results are undefined.
 */
#define KUDA_TO_PLATFORM_ERROR(e) (e == 0 ? KUDA_SUCCESS : e - KUDA_PLATFORM_START_SYSERR)

/** @def kuda_get_platform_error()
 * @return kuda_status_t the last platform error, folded into kuda_status_t, on most platforms
 * @remark This retrieves errno, or calls a GetLastError() style function, and
 *      folds it with KUDA_FROM_PLATFORM_ERROR.  Some platforms (such as OS2) have no
 *      such mechanism, so this call may be unsupported.  Do NOT use this
 *      call for socket errors from socket, send, recv etc!
 */

/** @def kuda_set_platform_error(e)
 * Reset the last platform error, unfolded from an kuda_status_t, on some platforms
 * @param e The PLATFORM error folded in a prior call to KUDA_FROM_PLATFORM_ERROR()
 * @warning This is a macro implementation; the statcode argument may be evaluated
 *      multiple times.  If the statcode was not created by kuda_get_platform_error
 *      or KUDA_FROM_PLATFORM_ERROR, the results are undefined.  This macro sets
 *      errno, or calls a SetLastError() style function, unfolding statcode
 *      with KUDA_TO_PLATFORM_ERROR.  Some platforms (such as OS2) have no such
 *      mechanism, so this call may be unsupported.
 */

/** @def kuda_get_netos_error()
 * Return the last socket error, folded into kuda_status_t, on all platforms
 * @remark This retrieves errno or calls a GetLastSocketError() style function,
 *      and folds it with KUDA_FROM_PLATFORM_ERROR.
 */

/** @def kuda_set_netos_error(e)
 * Reset the last socket error, unfolded from an kuda_status_t
 * @param e The socket error folded in a prior call to KUDA_FROM_PLATFORM_ERROR()
 * @warning This is a macro implementation; the statcode argument may be evaluated
 *      multiple times.  If the statcode was not created by kuda_get_platform_error
 *      or KUDA_FROM_PLATFORM_ERROR, the results are undefined.  This macro sets
 *      errno, or calls a WSASetLastError() style function, unfolding
 *      socketcode with KUDA_TO_PLATFORM_ERROR.
 */

#endif /* defined(DOXYGEN) */

/**
 * KUDA_PLATFORM_START_ERROR is where the KUDA specific error values start.
 */
#define KUDA_PLATFORM_START_ERROR     20000
/**
 * KUDA_PLATFORM_ERRSPACE_SIZE is the maximum number of errors you can fit
 *    into one of the error/status ranges below -- except for
 *    KUDA_PLATFORM_START_USERERR, which see.
 */
#define KUDA_PLATFORM_ERRSPACE_SIZE 50000
/**
 * KUDA_UTIL_ERRSPACE_SIZE is the size of the space that is reserved for
 * use within kuda-delman. This space is reserved above that used by KUDA
 * internally.
 * @note This number MUST be smaller than KUDA_PLATFORM_ERRSPACE_SIZE by a
 *       large enough amount that KUDA has sufficient room for its
 *       codes.
 */
#define KUDA_UTIL_ERRSPACE_SIZE 20000
/**
 * KUDA_PLATFORM_START_STATUS is where the KUDA specific status codes start.
 */
#define KUDA_PLATFORM_START_STATUS    (KUDA_PLATFORM_START_ERROR + KUDA_PLATFORM_ERRSPACE_SIZE)
/**
 * KUDA_UTIL_START_STATUS is where Kuda-Delman starts defining its
 * status codes.
 */
#define KUDA_UTIL_START_STATUS   (KUDA_PLATFORM_START_STATUS + \
                           (KUDA_PLATFORM_ERRSPACE_SIZE - KUDA_UTIL_ERRSPACE_SIZE))
/**
 * KUDA_PLATFORM_START_USERERR are reserved for applications that use KUDA that
 *     layer their own error codes along with KUDA's.  Note that the
 *     error immediately following this one is set ten times farther
 *     away than usual, so that users of kuda have a lot of room in
 *     which to declare custom error codes.
 *
 * In general applications should try and create unique error codes. To try
 * and assist in finding suitable ranges of numbers to use, the following
 * ranges are known to be used by the listed applications. If your
 * application defines error codes please advise the range of numbers it
 * uses to dev@clhy.hyang.org/kuda for inclusion in this list.
 *
 * Ranges shown are in relation to KUDA_PLATFORM_START_USERERR
 *
 * Subversion - Defined ranges, of less than 100, at intervals of 5000
 *              starting at an offset of 5000, e.g.
 *               +5000 to 5100,  +10000 to 10100
 *
 * cLHy WWHY - +2000 to 2999
 */
#define KUDA_PLATFORM_START_USERERR    (KUDA_PLATFORM_START_STATUS + KUDA_PLATFORM_ERRSPACE_SIZE)
/**
 * KUDA_PLATFORM_START_USEERR is obsolete, defined for compatibility only.
 * Use KUDA_PLATFORM_START_USERERR instead.
 */
#define KUDA_PLATFORM_START_USEERR     KUDA_PLATFORM_START_USERERR
/**
 * KUDA_PLATFORM_START_CANONERR is where KUDA versions of errno values are defined
 *     on systems which don't have the corresponding errno.
 */
#define KUDA_PLATFORM_START_CANONERR  (KUDA_PLATFORM_START_USERERR \
                                 + (KUDA_PLATFORM_ERRSPACE_SIZE * 10))
/**
 * KUDA_PLATFORM_START_EAIERR folds EAI_ error codes from getaddrinfo() into
 *     kuda_status_t values.
 */
#define KUDA_PLATFORM_START_EAIERR    (KUDA_PLATFORM_START_CANONERR + KUDA_PLATFORM_ERRSPACE_SIZE)
/**
 * KUDA_PLATFORM_START_SYSERR folds platform-specific system error values into
 *     kuda_status_t values.
 */
#define KUDA_PLATFORM_START_SYSERR    (KUDA_PLATFORM_START_EAIERR + KUDA_PLATFORM_ERRSPACE_SIZE)

/**
 * @defgroup KUDA_ERROR_map KUDA Error Space
 * <PRE>
 * The following attempts to show the relation of the various constants
 * used for mapping KUDA Status codes.
 *
 *       0
 *
 *  20,000     KUDA_PLATFORM_START_ERROR
 *
 *         + KUDA_PLATFORM_ERRSPACE_SIZE (50,000)
 *
 *  70,000      KUDA_PLATFORM_START_STATUS
 *
 *         + KUDA_PLATFORM_ERRSPACE_SIZE - KUDA_UTIL_ERRSPACE_SIZE (30,000)
 *
 * 100,000      KUDA_UTIL_START_STATUS
 *
 *         + KUDA_UTIL_ERRSPACE_SIZE (20,000)
 *
 * 120,000      KUDA_PLATFORM_START_USERERR
 *
 *         + 10 x KUDA_PLATFORM_ERRSPACE_SIZE (50,000 * 10)
 *
 * 620,000      KUDA_PLATFORM_START_CANONERR
 *
 *         + KUDA_PLATFORM_ERRSPACE_SIZE (50,000)
 *
 * 670,000      KUDA_PLATFORM_START_EAIERR
 *
 *         + KUDA_PLATFORM_ERRSPACE_SIZE (50,000)
 *
 * 720,000      KUDA_PLATFORM_START_SYSERR
 *
 * </PRE>
 */

/** no error. */
#define KUDA_SUCCESS 0

/**
 * @defgroup KUDA_Error KUDA Error Values
 * <PRE>
 * <b>KUDA ERROR VALUES</b>
 * KUDA_ENOSTAT      KUDA was unable to perform a stat on the file
 * KUDA_ENOPOOL      KUDA was not provided a pool with which to allocate memory
 * KUDA_EBADDATE     KUDA was given an invalid date
 * KUDA_EINVALSOCK   KUDA was given an invalid socket
 * KUDA_ENOPROC      KUDA was not given a process structure
 * KUDA_ENOTIME      KUDA was not given a time structure
 * KUDA_ENODIR       KUDA was not given a directory structure
 * KUDA_ENOLOCK      KUDA was not given a lock structure
 * KUDA_ENOPOLL      KUDA was not given a poll structure
 * KUDA_ENOSOCKET    KUDA was not given a socket
 * KUDA_ENOTHREAD    KUDA was not given a thread structure
 * KUDA_ENOTHDKEY    KUDA was not given a thread key structure
 * KUDA_ENOSHMAVAIL  There is no more shared memory available
 * KUDA_EDSOOPEN     KUDA was unable to open the dso object.  For more
 *                  information call kuda_dso_error().
 * KUDA_EGENERAL     General failure (specific information not available)
 * KUDA_EBADIP       The specified IP address is invalid
 * KUDA_EBADMASK     The specified netmask is invalid
 * KUDA_ESYMNOTFOUND Could not find the requested symbol
 * KUDA_ENOTENOUGHENTROPY Not enough entropy to continue
 * </PRE>
 *
 * <PRE>
 * <b>KUDA STATUS VALUES</b>
 * KUDA_INCHILD        Program is currently executing in the child
 * KUDA_INPARENT       Program is currently executing in the parent
 * KUDA_DETACH         The thread is detached
 * KUDA_NOTDETACH      The thread is not detached
 * KUDA_CHILD_DONE     The child has finished executing
 * KUDA_CHILD_NOTDONE  The child has not finished executing
 * KUDA_TIMEUP         The operation did not finish before the timeout
 * KUDA_INCOMPLETE     The operation was incomplete although some processing
 *                    was performed and the results are partially valid
 * KUDA_BADCH          Getopt found an option not in the option string
 * KUDA_BADARG         Getopt found an option that is missing an argument
 *                    and an argument was specified in the option string
 * KUDA_EOF            KUDA has encountered the end of the file
 * KUDA_NOTFOUND       KUDA was unable to find the socket in the poll structure
 * KUDA_ANONYMOUS      KUDA is using anonymous shared memory
 * KUDA_FILEBASED      KUDA is using a file name as the key to the shared memory
 * KUDA_KEYBASED       KUDA is using a shared key as the key to the shared memory
 * KUDA_EINIT          Ininitalizer value.  If no option has been found, but
 *                    the status variable requires a value, this should be used
 * KUDA_ENOTIMPL       The KUDA function has not been implemented on this
 *                    platform, either because nobody has gotten to it yet,
 *                    or the function is impossible on this platform.
 * KUDA_EMISMATCH      Two passwords do not match.
 * KUDA_EABSOLUTE      The given path was absolute.
 * KUDA_ERELATIVE      The given path was relative.
 * KUDA_EINCOMPLETE    The given path was neither relative nor absolute.
 * KUDA_EABOVEROOT     The given path was above the root path.
 * KUDA_EBUSY          The given lock was busy.
 * KUDA_EPROC_UNKNOWN  The given process wasn't recognized by KUDA
 * </PRE>
 * @{
 */
/** @see KUDA_STATUS_IS_ENOSTAT */
#define KUDA_ENOSTAT        (KUDA_PLATFORM_START_ERROR + 1)
/** @see KUDA_STATUS_IS_ENOPOOL */
#define KUDA_ENOPOOL        (KUDA_PLATFORM_START_ERROR + 2)
/* empty slot: +3 */
/** @see KUDA_STATUS_IS_EBADDATE */
#define KUDA_EBADDATE       (KUDA_PLATFORM_START_ERROR + 4)
/** @see KUDA_STATUS_IS_EINVALSOCK */
#define KUDA_EINVALSOCK     (KUDA_PLATFORM_START_ERROR + 5)
/** @see KUDA_STATUS_IS_ENOPROC */
#define KUDA_ENOPROC        (KUDA_PLATFORM_START_ERROR + 6)
/** @see KUDA_STATUS_IS_ENOTIME */
#define KUDA_ENOTIME        (KUDA_PLATFORM_START_ERROR + 7)
/** @see KUDA_STATUS_IS_ENODIR */
#define KUDA_ENODIR         (KUDA_PLATFORM_START_ERROR + 8)
/** @see KUDA_STATUS_IS_ENOLOCK */
#define KUDA_ENOLOCK        (KUDA_PLATFORM_START_ERROR + 9)
/** @see KUDA_STATUS_IS_ENOPOLL */
#define KUDA_ENOPOLL        (KUDA_PLATFORM_START_ERROR + 10)
/** @see KUDA_STATUS_IS_ENOSOCKET */
#define KUDA_ENOSOCKET      (KUDA_PLATFORM_START_ERROR + 11)
/** @see KUDA_STATUS_IS_ENOTHREAD */
#define KUDA_ENOTHREAD      (KUDA_PLATFORM_START_ERROR + 12)
/** @see KUDA_STATUS_IS_ENOTHDKEY */
#define KUDA_ENOTHDKEY      (KUDA_PLATFORM_START_ERROR + 13)
/** @see KUDA_STATUS_IS_EGENERAL */
#define KUDA_EGENERAL       (KUDA_PLATFORM_START_ERROR + 14)
/** @see KUDA_STATUS_IS_ENOSHMAVAIL */
#define KUDA_ENOSHMAVAIL    (KUDA_PLATFORM_START_ERROR + 15)
/** @see KUDA_STATUS_IS_EBADIP */
#define KUDA_EBADIP         (KUDA_PLATFORM_START_ERROR + 16)
/** @see KUDA_STATUS_IS_EBADMASK */
#define KUDA_EBADMASK       (KUDA_PLATFORM_START_ERROR + 17)
/* empty slot: +18 */
/** @see KUDA_STATUS_IS_EDSOPEN */
#define KUDA_EDSOOPEN       (KUDA_PLATFORM_START_ERROR + 19)
/** @see KUDA_STATUS_IS_EABSOLUTE */
#define KUDA_EABSOLUTE      (KUDA_PLATFORM_START_ERROR + 20)
/** @see KUDA_STATUS_IS_ERELATIVE */
#define KUDA_ERELATIVE      (KUDA_PLATFORM_START_ERROR + 21)
/** @see KUDA_STATUS_IS_EINCOMPLETE */
#define KUDA_EINCOMPLETE    (KUDA_PLATFORM_START_ERROR + 22)
/** @see KUDA_STATUS_IS_EABOVEROOT */
#define KUDA_EABOVEROOT     (KUDA_PLATFORM_START_ERROR + 23)
/** @see KUDA_STATUS_IS_EBADPATH */
#define KUDA_EBADPATH       (KUDA_PLATFORM_START_ERROR + 24)
/** @see KUDA_STATUS_IS_EPATHWILD */
#define KUDA_EPATHWILD      (KUDA_PLATFORM_START_ERROR + 25)
/** @see KUDA_STATUS_IS_ESYMNOTFOUND */
#define KUDA_ESYMNOTFOUND   (KUDA_PLATFORM_START_ERROR + 26)
/** @see KUDA_STATUS_IS_EPROC_UNKNOWN */
#define KUDA_EPROC_UNKNOWN  (KUDA_PLATFORM_START_ERROR + 27)
/** @see KUDA_STATUS_IS_ENOTENOUGHENTROPY */
#define KUDA_ENOTENOUGHENTROPY (KUDA_PLATFORM_START_ERROR + 28)
/** @} */

/**
 * @defgroup KUDA_STATUS_IS Status Value Tests
 * @warning For any particular error condition, more than one of these tests
 *      may match. This is because platform-specific error codes may not
 *      always match the semantics of the POSIX codes these tests (and the
 *      corresponding KUDA error codes) are named after. A notable example
 *      are the KUDA_STATUS_IS_ENOENT and KUDA_STATUS_IS_ENOTDIR tests on
 *      Win32 platforms. The programmer should always be aware of this and
 *      adjust the order of the tests accordingly.
 * @{
 */
/**
 * KUDA was unable to perform a stat on the file
 * @warning always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_ENOSTAT(s)        ((s) == KUDA_ENOSTAT)
/**
 * KUDA was not provided a pool with which to allocate memory
 * @warning always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_ENOPOOL(s)        ((s) == KUDA_ENOPOOL)
/** KUDA was given an invalid date  */
#define KUDA_STATUS_IS_EBADDATE(s)       ((s) == KUDA_EBADDATE)
/** KUDA was given an invalid socket */
#define KUDA_STATUS_IS_EINVALSOCK(s)     ((s) == KUDA_EINVALSOCK)
/** KUDA was not given a process structure */
#define KUDA_STATUS_IS_ENOPROC(s)        ((s) == KUDA_ENOPROC)
/** KUDA was not given a time structure */
#define KUDA_STATUS_IS_ENOTIME(s)        ((s) == KUDA_ENOTIME)
/** KUDA was not given a directory structure */
#define KUDA_STATUS_IS_ENODIR(s)         ((s) == KUDA_ENODIR)
/** KUDA was not given a lock structure */
#define KUDA_STATUS_IS_ENOLOCK(s)        ((s) == KUDA_ENOLOCK)
/** KUDA was not given a poll structure */
#define KUDA_STATUS_IS_ENOPOLL(s)        ((s) == KUDA_ENOPOLL)
/** KUDA was not given a socket */
#define KUDA_STATUS_IS_ENOSOCKET(s)      ((s) == KUDA_ENOSOCKET)
/** KUDA was not given a thread structure */
#define KUDA_STATUS_IS_ENOTHREAD(s)      ((s) == KUDA_ENOTHREAD)
/** KUDA was not given a thread key structure */
#define KUDA_STATUS_IS_ENOTHDKEY(s)      ((s) == KUDA_ENOTHDKEY)
/** Generic Error which can not be put into another spot */
#define KUDA_STATUS_IS_EGENERAL(s)       ((s) == KUDA_EGENERAL)
/** There is no more shared memory available */
#define KUDA_STATUS_IS_ENOSHMAVAIL(s)    ((s) == KUDA_ENOSHMAVAIL)
/** The specified IP address is invalid */
#define KUDA_STATUS_IS_EBADIP(s)         ((s) == KUDA_EBADIP)
/** The specified netmask is invalid */
#define KUDA_STATUS_IS_EBADMASK(s)       ((s) == KUDA_EBADMASK)
/* empty slot: +18 */
/**
 * KUDA was unable to open the dso object.
 * For more information call kuda_dso_error().
 */
#if defined(WIN32)
#define KUDA_STATUS_IS_EDSOOPEN(s)       ((s) == KUDA_EDSOOPEN \
                       || KUDA_TO_PLATFORM_ERROR(s) == ERROR_CAPI_NOT_FOUND)
#else
#define KUDA_STATUS_IS_EDSOOPEN(s)       ((s) == KUDA_EDSOOPEN)
#endif
/** The given path was absolute. */
#define KUDA_STATUS_IS_EABSOLUTE(s)      ((s) == KUDA_EABSOLUTE)
/** The given path was relative. */
#define KUDA_STATUS_IS_ERELATIVE(s)      ((s) == KUDA_ERELATIVE)
/** The given path was neither relative nor absolute. */
#define KUDA_STATUS_IS_EINCOMPLETE(s)    ((s) == KUDA_EINCOMPLETE)
/** The given path was above the root path. */
#define KUDA_STATUS_IS_EABOVEROOT(s)     ((s) == KUDA_EABOVEROOT)
/** The given path was bad. */
#define KUDA_STATUS_IS_EBADPATH(s)       ((s) == KUDA_EBADPATH)
/** The given path contained wildcards. */
#define KUDA_STATUS_IS_EPATHWILD(s)      ((s) == KUDA_EPATHWILD)
/** Could not find the requested symbol.
 * For more information call kuda_dso_error().
 */
#if defined(WIN32)
#define KUDA_STATUS_IS_ESYMNOTFOUND(s)   ((s) == KUDA_ESYMNOTFOUND \
                       || KUDA_TO_PLATFORM_ERROR(s) == ERROR_PROC_NOT_FOUND)
#else
#define KUDA_STATUS_IS_ESYMNOTFOUND(s)   ((s) == KUDA_ESYMNOTFOUND)
#endif
/** The given process was not recognized by KUDA. */
#define KUDA_STATUS_IS_EPROC_UNKNOWN(s)  ((s) == KUDA_EPROC_UNKNOWN)
/** KUDA could not gather enough entropy to continue. */
#define KUDA_STATUS_IS_ENOTENOUGHENTROPY(s) ((s) == KUDA_ENOTENOUGHENTROPY)

/** @} */

/**
 * @addtogroup KUDA_Error
 * @{
 */
/** @see KUDA_STATUS_IS_INCHILD */
#define KUDA_INCHILD        (KUDA_PLATFORM_START_STATUS + 1)
/** @see KUDA_STATUS_IS_INPARENT */
#define KUDA_INPARENT       (KUDA_PLATFORM_START_STATUS + 2)
/** @see KUDA_STATUS_IS_DETACH */
#define KUDA_DETACH         (KUDA_PLATFORM_START_STATUS + 3)
/** @see KUDA_STATUS_IS_NOTDETACH */
#define KUDA_NOTDETACH      (KUDA_PLATFORM_START_STATUS + 4)
/** @see KUDA_STATUS_IS_CHILD_DONE */
#define KUDA_CHILD_DONE     (KUDA_PLATFORM_START_STATUS + 5)
/** @see KUDA_STATUS_IS_CHILD_NOTDONE */
#define KUDA_CHILD_NOTDONE  (KUDA_PLATFORM_START_STATUS + 6)
/** @see KUDA_STATUS_IS_TIMEUP */
#define KUDA_TIMEUP         (KUDA_PLATFORM_START_STATUS + 7)
/** @see KUDA_STATUS_IS_INCOMPLETE */
#define KUDA_INCOMPLETE     (KUDA_PLATFORM_START_STATUS + 8)
/* empty slot: +9 */
/* empty slot: +10 */
/* empty slot: +11 */
/** @see KUDA_STATUS_IS_BADCH */
#define KUDA_BADCH          (KUDA_PLATFORM_START_STATUS + 12)
/** @see KUDA_STATUS_IS_BADARG */
#define KUDA_BADARG         (KUDA_PLATFORM_START_STATUS + 13)
/** @see KUDA_STATUS_IS_EOF */
#define KUDA_EOF            (KUDA_PLATFORM_START_STATUS + 14)
/** @see KUDA_STATUS_IS_NOTFOUND */
#define KUDA_NOTFOUND       (KUDA_PLATFORM_START_STATUS + 15)
/* empty slot: +16 */
/* empty slot: +17 */
/* empty slot: +18 */
/** @see KUDA_STATUS_IS_ANONYMOUS */
#define KUDA_ANONYMOUS      (KUDA_PLATFORM_START_STATUS + 19)
/** @see KUDA_STATUS_IS_FILEBASED */
#define KUDA_FILEBASED      (KUDA_PLATFORM_START_STATUS + 20)
/** @see KUDA_STATUS_IS_KEYBASED */
#define KUDA_KEYBASED       (KUDA_PLATFORM_START_STATUS + 21)
/** @see KUDA_STATUS_IS_EINIT */
#define KUDA_EINIT          (KUDA_PLATFORM_START_STATUS + 22)
/** @see KUDA_STATUS_IS_ENOTIMPL */
#define KUDA_ENOTIMPL       (KUDA_PLATFORM_START_STATUS + 23)
/** @see KUDA_STATUS_IS_EMISMATCH */
#define KUDA_EMISMATCH      (KUDA_PLATFORM_START_STATUS + 24)
/** @see KUDA_STATUS_IS_EBUSY */
#define KUDA_EBUSY          (KUDA_PLATFORM_START_STATUS + 25)
/** @} */

/**
 * @addtogroup KUDA_STATUS_IS
 * @{
 */
/**
 * Program is currently executing in the child
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code */
#define KUDA_STATUS_IS_INCHILD(s)        ((s) == KUDA_INCHILD)
/**
 * Program is currently executing in the parent
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_INPARENT(s)       ((s) == KUDA_INPARENT)
/**
 * The thread is detached
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_DETACH(s)         ((s) == KUDA_DETACH)
/**
 * The thread is not detached
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_NOTDETACH(s)      ((s) == KUDA_NOTDETACH)
/**
 * The child has finished executing
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_CHILD_DONE(s)     ((s) == KUDA_CHILD_DONE)
/**
 * The child has not finished executing
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_CHILD_NOTDONE(s)  ((s) == KUDA_CHILD_NOTDONE)
/**
 * The operation did not finish before the timeout
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_TIMEUP(s)         ((s) == KUDA_TIMEUP)
/**
 * The operation was incomplete although some processing was performed
 * and the results are partially valid.
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_INCOMPLETE(s)     ((s) == KUDA_INCOMPLETE)
/* empty slot: +9 */
/* empty slot: +10 */
/* empty slot: +11 */
/**
 * Getopt found an option not in the option string
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_BADCH(s)          ((s) == KUDA_BADCH)
/**
 * Getopt found an option not in the option string and an argument was
 * specified in the option string
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_BADARG(s)         ((s) == KUDA_BADARG)
/**
 * KUDA has encountered the end of the file
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_EOF(s)            ((s) == KUDA_EOF)
/**
 * KUDA was unable to find the socket in the poll structure
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_NOTFOUND(s)       ((s) == KUDA_NOTFOUND)
/* empty slot: +16 */
/* empty slot: +17 */
/* empty slot: +18 */
/**
 * KUDA is using anonymous shared memory
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_ANONYMOUS(s)      ((s) == KUDA_ANONYMOUS)
/**
 * KUDA is using a file name as the key to the shared memory
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_FILEBASED(s)      ((s) == KUDA_FILEBASED)
/**
 * KUDA is using a shared key as the key to the shared memory
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_KEYBASED(s)       ((s) == KUDA_KEYBASED)
/**
 * Ininitalizer value.  If no option has been found, but
 * the status variable requires a value, this should be used
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_EINIT(s)          ((s) == KUDA_EINIT)
/**
 * The KUDA function has not been implemented on this
 * platform, either because nobody has gotten to it yet,
 * or the function is impossible on this platform.
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_ENOTIMPL(s)       ((s) == KUDA_ENOTIMPL)
/**
 * Two passwords do not match.
 * @warning
 * always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_EMISMATCH(s)      ((s) == KUDA_EMISMATCH)
/**
 * The given lock was busy
 * @warning always use this test, as platform-specific variances may meet this
 * more than one error code
 */
#define KUDA_STATUS_IS_EBUSY(s)          ((s) == KUDA_EBUSY)

/** @} */

/**
 * @addtogroup KUDA_Error KUDA Error Values
 * @{
 */
/* KUDA CANONICAL ERROR VALUES */
/** @see KUDA_STATUS_IS_EACCES */
#ifdef EACCES
#define KUDA_EACCES EACCES
#else
#define KUDA_EACCES         (KUDA_PLATFORM_START_CANONERR + 1)
#endif

/** @see KUDA_STATUS_IS_EEXIST */
#ifdef EEXIST
#define KUDA_EEXIST EEXIST
#else
#define KUDA_EEXIST         (KUDA_PLATFORM_START_CANONERR + 2)
#endif

/** @see KUDA_STATUS_IS_ENAMETOOLONG */
#ifdef ENAMETOOLONG
#define KUDA_ENAMETOOLONG ENAMETOOLONG
#else
#define KUDA_ENAMETOOLONG   (KUDA_PLATFORM_START_CANONERR + 3)
#endif

/** @see KUDA_STATUS_IS_ENOENT */
#ifdef ENOENT
#define KUDA_ENOENT ENOENT
#else
#define KUDA_ENOENT         (KUDA_PLATFORM_START_CANONERR + 4)
#endif

/** @see KUDA_STATUS_IS_ENOTDIR */
#ifdef ENOTDIR
#define KUDA_ENOTDIR ENOTDIR
#else
#define KUDA_ENOTDIR        (KUDA_PLATFORM_START_CANONERR + 5)
#endif

/** @see KUDA_STATUS_IS_ENOSPC */
#ifdef ENOSPC
#define KUDA_ENOSPC ENOSPC
#else
#define KUDA_ENOSPC         (KUDA_PLATFORM_START_CANONERR + 6)
#endif

/** @see KUDA_STATUS_IS_ENOMEM */
#ifdef ENOMEM
#define KUDA_ENOMEM ENOMEM
#else
#define KUDA_ENOMEM         (KUDA_PLATFORM_START_CANONERR + 7)
#endif

/** @see KUDA_STATUS_IS_EMFILE */
#ifdef EMFILE
#define KUDA_EMFILE EMFILE
#else
#define KUDA_EMFILE         (KUDA_PLATFORM_START_CANONERR + 8)
#endif

/** @see KUDA_STATUS_IS_ENFILE */
#ifdef ENFILE
#define KUDA_ENFILE ENFILE
#else
#define KUDA_ENFILE         (KUDA_PLATFORM_START_CANONERR + 9)
#endif

/** @see KUDA_STATUS_IS_EBADF */
#ifdef EBADF
#define KUDA_EBADF EBADF
#else
#define KUDA_EBADF          (KUDA_PLATFORM_START_CANONERR + 10)
#endif

/** @see KUDA_STATUS_IS_EINVAL */
#ifdef EINVAL
#define KUDA_EINVAL EINVAL
#else
#define KUDA_EINVAL         (KUDA_PLATFORM_START_CANONERR + 11)
#endif

/** @see KUDA_STATUS_IS_ESPIPE */
#ifdef ESPIPE
#define KUDA_ESPIPE ESPIPE
#else
#define KUDA_ESPIPE         (KUDA_PLATFORM_START_CANONERR + 12)
#endif

/**
 * @see KUDA_STATUS_IS_EAGAIN
 * @warning use KUDA_STATUS_IS_EAGAIN instead of just testing this value
 */
#ifdef EAGAIN
#define KUDA_EAGAIN EAGAIN
#elif defined(EWOULDBLOCK)
#define KUDA_EAGAIN EWOULDBLOCK
#else
#define KUDA_EAGAIN         (KUDA_PLATFORM_START_CANONERR + 13)
#endif

/** @see KUDA_STATUS_IS_EINTR */
#ifdef EINTR
#define KUDA_EINTR EINTR
#else
#define KUDA_EINTR          (KUDA_PLATFORM_START_CANONERR + 14)
#endif

/** @see KUDA_STATUS_IS_ENOTSOCK */
#ifdef ENOTSOCK
#define KUDA_ENOTSOCK ENOTSOCK
#else
#define KUDA_ENOTSOCK       (KUDA_PLATFORM_START_CANONERR + 15)
#endif

/** @see KUDA_STATUS_IS_ECONNREFUSED */
#ifdef ECONNREFUSED
#define KUDA_ECONNREFUSED ECONNREFUSED
#else
#define KUDA_ECONNREFUSED   (KUDA_PLATFORM_START_CANONERR + 16)
#endif

/** @see KUDA_STATUS_IS_EINPROGRESS */
#ifdef EINPROGRESS
#define KUDA_EINPROGRESS EINPROGRESS
#else
#define KUDA_EINPROGRESS    (KUDA_PLATFORM_START_CANONERR + 17)
#endif

/**
 * @see KUDA_STATUS_IS_ECONNABORTED
 * @warning use KUDA_STATUS_IS_ECONNABORTED instead of just testing this value
 */

#ifdef ECONNABORTED
#define KUDA_ECONNABORTED ECONNABORTED
#else
#define KUDA_ECONNABORTED   (KUDA_PLATFORM_START_CANONERR + 18)
#endif

/** @see KUDA_STATUS_IS_ECONNRESET */
#ifdef ECONNRESET
#define KUDA_ECONNRESET ECONNRESET
#else
#define KUDA_ECONNRESET     (KUDA_PLATFORM_START_CANONERR + 19)
#endif

/** @see KUDA_STATUS_IS_ETIMEDOUT
 *  @deprecated */
#ifdef ETIMEDOUT
#define KUDA_ETIMEDOUT ETIMEDOUT
#else
#define KUDA_ETIMEDOUT      (KUDA_PLATFORM_START_CANONERR + 20)
#endif

/** @see KUDA_STATUS_IS_EHOSTUNREACH */
#ifdef EHOSTUNREACH
#define KUDA_EHOSTUNREACH EHOSTUNREACH
#else
#define KUDA_EHOSTUNREACH   (KUDA_PLATFORM_START_CANONERR + 21)
#endif

/** @see KUDA_STATUS_IS_ENETUNREACH */
#ifdef ENETUNREACH
#define KUDA_ENETUNREACH ENETUNREACH
#else
#define KUDA_ENETUNREACH    (KUDA_PLATFORM_START_CANONERR + 22)
#endif

/** @see KUDA_STATUS_IS_EFTYPE */
#ifdef EFTYPE
#define KUDA_EFTYPE EFTYPE
#else
#define KUDA_EFTYPE        (KUDA_PLATFORM_START_CANONERR + 23)
#endif

/** @see KUDA_STATUS_IS_EPIPE */
#ifdef EPIPE
#define KUDA_EPIPE EPIPE
#else
#define KUDA_EPIPE         (KUDA_PLATFORM_START_CANONERR + 24)
#endif

/** @see KUDA_STATUS_IS_EXDEV */
#ifdef EXDEV
#define KUDA_EXDEV EXDEV
#else
#define KUDA_EXDEV         (KUDA_PLATFORM_START_CANONERR + 25)
#endif

/** @see KUDA_STATUS_IS_ENOTEMPTY */
#ifdef ENOTEMPTY
#define KUDA_ENOTEMPTY ENOTEMPTY
#else
#define KUDA_ENOTEMPTY     (KUDA_PLATFORM_START_CANONERR + 26)
#endif

/** @see KUDA_STATUS_IS_EAFNOSUPPORT */
#ifdef EAFNOSUPPORT
#define KUDA_EAFNOSUPPORT EAFNOSUPPORT
#else
#define KUDA_EAFNOSUPPORT  (KUDA_PLATFORM_START_CANONERR + 27)
#endif

/** @see KUDA_STATUS_IS_EOPNOTSUPP */
#ifdef EOPNOTSUPP
#define KUDA_EOPNOTSUPP EOPNOTSUPP
#else
#define KUDA_EOPNOTSUPP    (KUDA_PLATFORM_START_CANONERR + 28)
#endif

/** @see KUDA_STATUS_IS_ERANGE */
#ifdef ERANGE
#define KUDA_ERANGE ERANGE
#else
#define KUDA_ERANGE          (KUDA_PLATFORM_START_CANONERR + 29)
#endif

/** @} */

#if defined(OS2) && !defined(DOXYGEN)

#define KUDA_FROM_PLATFORM_ERROR(e) (e == 0 ? KUDA_SUCCESS : e + KUDA_PLATFORM_START_SYSERR)
#define KUDA_TO_PLATFORM_ERROR(e)   (e == 0 ? KUDA_SUCCESS : e - KUDA_PLATFORM_START_SYSERR)

#define INCL_DOSERRORS
#define INCL_DOS

/* Leave these undefined.
 * OS2 doesn't rely on the errno concept.
 * The API calls always return a result codes which
 * should be filtered through KUDA_FROM_PLATFORM_ERROR().
 *
 * #define kuda_get_platform_error()   (KUDA_FROM_PLATFORM_ERROR(GetLastError()))
 * #define kuda_set_platform_error(e)  (SetLastError(KUDA_TO_PLATFORM_ERROR(e)))
 */

/* A special case, only socket calls require this;
 */
#define kuda_get_netos_error()   (KUDA_FROM_PLATFORM_ERROR(errno))
#define kuda_set_netos_error(e)  (errno = KUDA_TO_PLATFORM_ERROR(e))

/* And this needs to be greped away for good:
 */
#define KUDA_OS2_STATUS(e) (KUDA_FROM_PLATFORM_ERROR(e))

/* These can't sit in a private header, so in spite of the extra size,
 * they need to be made available here.
 */
#define SOCBASEERR              10000
#define SOCEPERM                (SOCBASEERR+1)             /* Not owner */
#define SOCESRCH                (SOCBASEERR+3)             /* No such process */
#define SOCEINTR                (SOCBASEERR+4)             /* Interrupted system call */
#define SOCENXIO                (SOCBASEERR+6)             /* No such device or address */
#define SOCEBADF                (SOCBASEERR+9)             /* Bad file number */
#define SOCEACCES               (SOCBASEERR+13)            /* Permission denied */
#define SOCEFAULT               (SOCBASEERR+14)            /* Bad address */
#define SOCEINVAL               (SOCBASEERR+22)            /* Invalid argument */
#define SOCEMFILE               (SOCBASEERR+24)            /* Too many open files */
#define SOCEPIPE                (SOCBASEERR+32)            /* Broken pipe */
#define SOCEOS2ERR              (SOCBASEERR+100)           /* OS2 Error */
#define SOCEWOULDBLOCK          (SOCBASEERR+35)            /* Operation would block */
#define SOCEINPROGRESS          (SOCBASEERR+36)            /* Operation now in progress */
#define SOCEALREADY             (SOCBASEERR+37)            /* Operation already in progress */
#define SOCENOTSOCK             (SOCBASEERR+38)            /* Socket operation on non-socket */
#define SOCEDESTADDRREQ         (SOCBASEERR+39)            /* Destination address required */
#define SOCEMSGSIZE             (SOCBASEERR+40)            /* Message too long */
#define SOCEPROTOTYPE           (SOCBASEERR+41)            /* Protocol wrong type for socket */
#define SOCENOPROTOOPT          (SOCBASEERR+42)            /* Protocol not available */
#define SOCEPROTONOSUPPORT      (SOCBASEERR+43)            /* Protocol not supported */
#define SOCESOCKTNOSUPPORT      (SOCBASEERR+44)            /* Socket type not supported */
#define SOCEOPNOTSUPP           (SOCBASEERR+45)            /* Operation not supported on socket */
#define SOCEPFNOSUPPORT         (SOCBASEERR+46)            /* Protocol family not supported */
#define SOCEAFNOSUPPORT         (SOCBASEERR+47)            /* Address family not supported by protocol family */
#define SOCEADDRINUSE           (SOCBASEERR+48)            /* Address already in use */
#define SOCEADDRNOTAVAIL        (SOCBASEERR+49)            /* Can't assign requested address */
#define SOCENETDOWN             (SOCBASEERR+50)            /* Network is down */
#define SOCENETUNREACH          (SOCBASEERR+51)            /* Network is unreachable */
#define SOCENETRESET            (SOCBASEERR+52)            /* Network dropped connection on reset */
#define SOCECONNABORTED         (SOCBASEERR+53)            /* Software caused connection abort */
#define SOCECONNRESET           (SOCBASEERR+54)            /* Connection reset by peer */
#define SOCENOBUFS              (SOCBASEERR+55)            /* No buffer space available */
#define SOCEISCONN              (SOCBASEERR+56)            /* Socket is already connected */
#define SOCENOTCONN             (SOCBASEERR+57)            /* Socket is not connected */
#define SOCESHUTDOWN            (SOCBASEERR+58)            /* Can't send after socket shutdown */
#define SOCETOOMANYREFS         (SOCBASEERR+59)            /* Too many references: can't splice */
#define SOCETIMEDOUT            (SOCBASEERR+60)            /* Connection timed out */
#define SOCECONNREFUSED         (SOCBASEERR+61)            /* Connection refused */
#define SOCELOOP                (SOCBASEERR+62)            /* Too many levels of symbolic links */
#define SOCENAMETOOLONG         (SOCBASEERR+63)            /* File name too long */
#define SOCEHOSTDOWN            (SOCBASEERR+64)            /* Host is down */
#define SOCEHOSTUNREACH         (SOCBASEERR+65)            /* No route to host */
#define SOCENOTEMPTY            (SOCBASEERR+66)            /* Directory not empty */

/* KUDA CANONICAL ERROR TESTS */
#define KUDA_STATUS_IS_EACCES(s)         ((s) == KUDA_EACCES \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_ACCESS_DENIED \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_SHARING_VIOLATION)
#define KUDA_STATUS_IS_EEXIST(s)         ((s) == KUDA_EEXIST \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_OPEN_FAILED \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_FILE_EXISTS \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_ALREADY_EXISTS \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_ACCESS_DENIED)
#define KUDA_STATUS_IS_ENAMETOOLONG(s)   ((s) == KUDA_ENAMETOOLONG \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_FILENAME_EXCED_RANGE \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCENAMETOOLONG)
#define KUDA_STATUS_IS_ENOENT(s)         ((s) == KUDA_ENOENT \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_FILE_NOT_FOUND \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_PATH_NOT_FOUND \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NO_MORE_FILES \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_OPEN_FAILED)
#define KUDA_STATUS_IS_ENOTDIR(s)        ((s) == KUDA_ENOTDIR)
#define KUDA_STATUS_IS_ENOSPC(s)         ((s) == KUDA_ENOSPC \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_DISK_FULL)
#define KUDA_STATUS_IS_ENOMEM(s)         ((s) == KUDA_ENOMEM)
#define KUDA_STATUS_IS_EMFILE(s)         ((s) == KUDA_EMFILE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_TOO_MANY_OPEN_FILES)
#define KUDA_STATUS_IS_ENFILE(s)         ((s) == KUDA_ENFILE)
#define KUDA_STATUS_IS_EBADF(s)          ((s) == KUDA_EBADF \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_HANDLE)
#define KUDA_STATUS_IS_EINVAL(s)         ((s) == KUDA_EINVAL \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_PARAMETER \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_FUNCTION)
#define KUDA_STATUS_IS_ESPIPE(s)         ((s) == KUDA_ESPIPE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NEGATIVE_SEEK)
#define KUDA_STATUS_IS_EAGAIN(s)         ((s) == KUDA_EAGAIN \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NO_DATA \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCEWOULDBLOCK \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_LOCK_VIOLATION)
#define KUDA_STATUS_IS_EINTR(s)          ((s) == KUDA_EINTR \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCEINTR)
#define KUDA_STATUS_IS_ENOTSOCK(s)       ((s) == KUDA_ENOTSOCK \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCENOTSOCK)
#define KUDA_STATUS_IS_ECONNREFUSED(s)   ((s) == KUDA_ECONNREFUSED \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCECONNREFUSED)
#define KUDA_STATUS_IS_EINPROGRESS(s)    ((s) == KUDA_EINPROGRESS \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCEINPROGRESS)
#define KUDA_STATUS_IS_ECONNABORTED(s)   ((s) == KUDA_ECONNABORTED \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCECONNABORTED)
#define KUDA_STATUS_IS_ECONNRESET(s)     ((s) == KUDA_ECONNRESET \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCECONNRESET)
/* XXX deprecated */
#define KUDA_STATUS_IS_ETIMEDOUT(s)         ((s) == KUDA_ETIMEDOUT \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCETIMEDOUT)
#undef KUDA_STATUS_IS_TIMEUP
#define KUDA_STATUS_IS_TIMEUP(s)         ((s) == KUDA_TIMEUP \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCETIMEDOUT)
#define KUDA_STATUS_IS_EHOSTUNREACH(s)   ((s) == KUDA_EHOSTUNREACH \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCEHOSTUNREACH)
#define KUDA_STATUS_IS_ENETUNREACH(s)    ((s) == KUDA_ENETUNREACH \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCENETUNREACH)
#define KUDA_STATUS_IS_EFTYPE(s)         ((s) == KUDA_EFTYPE)
#define KUDA_STATUS_IS_EPIPE(s)          ((s) == KUDA_EPIPE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_BROKEN_PIPE \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCEPIPE)
#define KUDA_STATUS_IS_EXDEV(s)          ((s) == KUDA_EXDEV \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NOT_SAME_DEVICE)
#define KUDA_STATUS_IS_ENOTEMPTY(s)      ((s) == KUDA_ENOTEMPTY \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_DIR_NOT_EMPTY \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_ACCESS_DENIED)
#define KUDA_STATUS_IS_EAFNOSUPPORT(s)   ((s) == KUDA_AFNOSUPPORT \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCEAFNOSUPPORT)
#define KUDA_STATUS_IS_EOPNOTSUPP(s)     ((s) == KUDA_EOPNOTSUPP \
                || (s) == KUDA_PLATFORM_START_SYSERR + SOCEOPNOTSUPP)
#define KUDA_STATUS_IS_ERANGE(s)         ((s) == KUDA_ERANGE)

/*
    Sorry, too tired to wrap this up for OS2... feel free to
    fit the following into their best matches.

    { ERROR_NO_SIGNAL_SENT,     ESRCH           },
    { SOCEALREADY,              EALREADY        },
    { SOCEDESTADDRREQ,          EDESTADDRREQ    },
    { SOCEMSGSIZE,              EMSGSIZE        },
    { SOCEPROTOTYPE,            EPROTOTYPE      },
    { SOCENOPROTOOPT,           ENOPROTOOPT     },
    { SOCEPROTONOSUPPORT,       EPROTONOSUPPORT },
    { SOCESOCKTNOSUPPORT,       ESOCKTNOSUPPORT },
    { SOCEPFNOSUPPORT,          EPFNOSUPPORT    },
    { SOCEADDRINUSE,            EADDRINUSE      },
    { SOCEADDRNOTAVAIL,         EADDRNOTAVAIL   },
    { SOCENETDOWN,              ENETDOWN        },
    { SOCENETRESET,             ENETRESET       },
    { SOCENOBUFS,               ENOBUFS         },
    { SOCEISCONN,               EISCONN         },
    { SOCENOTCONN,              ENOTCONN        },
    { SOCESHUTDOWN,             ESHUTDOWN       },
    { SOCETOOMANYREFS,          ETOOMANYREFS    },
    { SOCELOOP,                 ELOOP           },
    { SOCEHOSTDOWN,             EHOSTDOWN       },
    { SOCENOTEMPTY,             ENOTEMPTY       },
    { SOCEPIPE,                 EPIPE           }
*/

#elif defined(WIN32) && !defined(DOXYGEN) /* !defined(OS2) */

#define KUDA_FROM_PLATFORM_ERROR(e) (e == 0 ? KUDA_SUCCESS : e + KUDA_PLATFORM_START_SYSERR)
#define KUDA_TO_PLATFORM_ERROR(e)   (e == 0 ? KUDA_SUCCESS : e - KUDA_PLATFORM_START_SYSERR)

#define kuda_get_platform_error()   (KUDA_FROM_PLATFORM_ERROR(GetLastError()))
#define kuda_set_platform_error(e)  (SetLastError(KUDA_TO_PLATFORM_ERROR(e)))

/* A special case, only socket calls require this:
 */
#define kuda_get_netos_error()   (KUDA_FROM_PLATFORM_ERROR(WSAGetLastError()))
#define kuda_set_netos_error(e)   (WSASetLastError(KUDA_TO_PLATFORM_ERROR(e)))

/* KUDA CANONICAL ERROR TESTS */
#define KUDA_STATUS_IS_EACCES(s)         ((s) == KUDA_EACCES \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_ACCESS_DENIED \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_CANNOT_MAKE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_CURRENT_DIRECTORY \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_DRIVE_LOCKED \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_FAIL_I24 \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_LOCK_VIOLATION \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_LOCK_FAILED \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NOT_LOCKED \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NETWORK_ACCESS_DENIED \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_SHARING_VIOLATION)
#define KUDA_STATUS_IS_EEXIST(s)         ((s) == KUDA_EEXIST \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_FILE_EXISTS \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_ALREADY_EXISTS)
#define KUDA_STATUS_IS_ENAMETOOLONG(s)   ((s) == KUDA_ENAMETOOLONG \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_FILENAME_EXCED_RANGE \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAENAMETOOLONG)
#define KUDA_STATUS_IS_ENOENT(s)         ((s) == KUDA_ENOENT \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_FILE_NOT_FOUND \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_PATH_NOT_FOUND \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_OPEN_FAILED \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NO_MORE_FILES)
#define KUDA_STATUS_IS_ENOTDIR(s)        ((s) == KUDA_ENOTDIR \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_PATH_NOT_FOUND \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_BAD_NETPATH \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_BAD_NET_NAME \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_BAD_PATHNAME \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_DRIVE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_DIRECTORY)
#define KUDA_STATUS_IS_ENOSPC(s)         ((s) == KUDA_ENOSPC \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_DISK_FULL)
#define KUDA_STATUS_IS_ENOMEM(s)         ((s) == KUDA_ENOMEM \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_ARENA_TRASHED \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NOT_ENOUGH_MEMORY \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_BLOCK \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NOT_ENOUGH_QUOTA \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_OUTOFMEMORY)
#define KUDA_STATUS_IS_EMFILE(s)         ((s) == KUDA_EMFILE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_TOO_MANY_OPEN_FILES)
#define KUDA_STATUS_IS_ENFILE(s)         ((s) == KUDA_ENFILE)
#define KUDA_STATUS_IS_EBADF(s)          ((s) == KUDA_EBADF \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_HANDLE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_TARGET_HANDLE)
#define KUDA_STATUS_IS_EINVAL(s)         ((s) == KUDA_EINVAL \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_ACCESS \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_DATA \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_FUNCTION \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_HANDLE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_PARAMETER \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NEGATIVE_SEEK)
#define KUDA_STATUS_IS_ESPIPE(s)         ((s) == KUDA_ESPIPE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_SEEK_ON_DEVICE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NEGATIVE_SEEK)
#define KUDA_STATUS_IS_EAGAIN(s)         ((s) == KUDA_EAGAIN \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NO_DATA \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NO_PROC_SLOTS \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NESTING_NOT_ALLOWED \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_MAX_THRDS_REACHED \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_LOCK_VIOLATION \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAEWOULDBLOCK)
#define KUDA_STATUS_IS_EINTR(s)          ((s) == KUDA_EINTR \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAEINTR)
#define KUDA_STATUS_IS_ENOTSOCK(s)       ((s) == KUDA_ENOTSOCK \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAENOTSOCK)
#define KUDA_STATUS_IS_ECONNREFUSED(s)   ((s) == KUDA_ECONNREFUSED \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAECONNREFUSED)
#define KUDA_STATUS_IS_EINPROGRESS(s)    ((s) == KUDA_EINPROGRESS \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAEINPROGRESS)
#define KUDA_STATUS_IS_ECONNABORTED(s)   ((s) == KUDA_ECONNABORTED \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAECONNABORTED)
#define KUDA_STATUS_IS_ECONNRESET(s)     ((s) == KUDA_ECONNRESET \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NETNAME_DELETED \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAECONNRESET)
/* XXX deprecated */
#define KUDA_STATUS_IS_ETIMEDOUT(s)         ((s) == KUDA_ETIMEDOUT \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAETIMEDOUT \
                || (s) == KUDA_PLATFORM_START_SYSERR + WAIT_TIMEOUT)
#undef KUDA_STATUS_IS_TIMEUP
#define KUDA_STATUS_IS_TIMEUP(s)         ((s) == KUDA_TIMEUP \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAETIMEDOUT \
                || (s) == KUDA_PLATFORM_START_SYSERR + WAIT_TIMEOUT)
#define KUDA_STATUS_IS_EHOSTUNREACH(s)   ((s) == KUDA_EHOSTUNREACH \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAEHOSTUNREACH)
#define KUDA_STATUS_IS_ENETUNREACH(s)    ((s) == KUDA_ENETUNREACH \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAENETUNREACH)
#define KUDA_STATUS_IS_EFTYPE(s)         ((s) == KUDA_EFTYPE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_EXE_MACHINE_TYPE_MISMATCH \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_DLL \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_CAPITYPE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_BAD_EXE_FORMAT \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_INVALID_EXE_SIGNATURE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_FILE_CORRUPT \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_BAD_FORMAT)
#define KUDA_STATUS_IS_EPIPE(s)          ((s) == KUDA_EPIPE \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_BROKEN_PIPE)
#define KUDA_STATUS_IS_EXDEV(s)          ((s) == KUDA_EXDEV \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_NOT_SAME_DEVICE)
#define KUDA_STATUS_IS_ENOTEMPTY(s)      ((s) == KUDA_ENOTEMPTY \
                || (s) == KUDA_PLATFORM_START_SYSERR + ERROR_DIR_NOT_EMPTY)
#define KUDA_STATUS_IS_EAFNOSUPPORT(s)   ((s) == KUDA_EAFNOSUPPORT \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAEAFNOSUPPORT)
#define KUDA_STATUS_IS_EOPNOTSUPP(s)     ((s) == KUDA_EOPNOTSUPP \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAEOPNOTSUPP)
#define KUDA_STATUS_IS_ERANGE(s)         ((s) == KUDA_ERANGE)

#elif defined(NETWARE) && defined(USE_WINSOCK) && !defined(DOXYGEN) /* !defined(OS2) && !defined(WIN32) */

#define KUDA_FROM_PLATFORM_ERROR(e) (e == 0 ? KUDA_SUCCESS : e + KUDA_PLATFORM_START_SYSERR)
#define KUDA_TO_PLATFORM_ERROR(e)   (e == 0 ? KUDA_SUCCESS : e - KUDA_PLATFORM_START_SYSERR)

#define kuda_get_platform_error()    (errno)
#define kuda_set_platform_error(e)   (errno = (e))

/* A special case, only socket calls require this: */
#define kuda_get_netos_error()   (KUDA_FROM_PLATFORM_ERROR(WSAGetLastError()))
#define kuda_set_netos_error(e)  (WSASetLastError(KUDA_TO_PLATFORM_ERROR(e)))

/* KUDA CANONICAL ERROR TESTS */
#define KUDA_STATUS_IS_EACCES(s)         ((s) == KUDA_EACCES)
#define KUDA_STATUS_IS_EEXIST(s)         ((s) == KUDA_EEXIST)
#define KUDA_STATUS_IS_ENAMETOOLONG(s)   ((s) == KUDA_ENAMETOOLONG)
#define KUDA_STATUS_IS_ENOENT(s)         ((s) == KUDA_ENOENT)
#define KUDA_STATUS_IS_ENOTDIR(s)        ((s) == KUDA_ENOTDIR)
#define KUDA_STATUS_IS_ENOSPC(s)         ((s) == KUDA_ENOSPC)
#define KUDA_STATUS_IS_ENOMEM(s)         ((s) == KUDA_ENOMEM)
#define KUDA_STATUS_IS_EMFILE(s)         ((s) == KUDA_EMFILE)
#define KUDA_STATUS_IS_ENFILE(s)         ((s) == KUDA_ENFILE)
#define KUDA_STATUS_IS_EBADF(s)          ((s) == KUDA_EBADF)
#define KUDA_STATUS_IS_EINVAL(s)         ((s) == KUDA_EINVAL)
#define KUDA_STATUS_IS_ESPIPE(s)         ((s) == KUDA_ESPIPE)

#define KUDA_STATUS_IS_EAGAIN(s)         ((s) == KUDA_EAGAIN \
                || (s) ==                       EWOULDBLOCK \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAEWOULDBLOCK)
#define KUDA_STATUS_IS_EINTR(s)          ((s) == KUDA_EINTR \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAEINTR)
#define KUDA_STATUS_IS_ENOTSOCK(s)       ((s) == KUDA_ENOTSOCK \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAENOTSOCK)
#define KUDA_STATUS_IS_ECONNREFUSED(s)   ((s) == KUDA_ECONNREFUSED \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAECONNREFUSED)
#define KUDA_STATUS_IS_EINPROGRESS(s)    ((s) == KUDA_EINPROGRESS \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAEINPROGRESS)
#define KUDA_STATUS_IS_ECONNABORTED(s)   ((s) == KUDA_ECONNABORTED \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAECONNABORTED)
#define KUDA_STATUS_IS_ECONNRESET(s)     ((s) == KUDA_ECONNRESET \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAECONNRESET)
/* XXX deprecated */
#define KUDA_STATUS_IS_ETIMEDOUT(s)       ((s) == KUDA_ETIMEDOUT \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAETIMEDOUT \
                || (s) == KUDA_PLATFORM_START_SYSERR + WAIT_TIMEOUT)
#undef KUDA_STATUS_IS_TIMEUP
#define KUDA_STATUS_IS_TIMEUP(s)         ((s) == KUDA_TIMEUP \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAETIMEDOUT \
                || (s) == KUDA_PLATFORM_START_SYSERR + WAIT_TIMEOUT)
#define KUDA_STATUS_IS_EHOSTUNREACH(s)   ((s) == KUDA_EHOSTUNREACH \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAEHOSTUNREACH)
#define KUDA_STATUS_IS_ENETUNREACH(s)    ((s) == KUDA_ENETUNREACH \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAENETUNREACH)
#define KUDA_STATUS_IS_ENETDOWN(s)       ((s) == KUDA_PLATFORM_START_SYSERR + WSAENETDOWN)
#define KUDA_STATUS_IS_EFTYPE(s)         ((s) == KUDA_EFTYPE)
#define KUDA_STATUS_IS_EPIPE(s)          ((s) == KUDA_EPIPE)
#define KUDA_STATUS_IS_EXDEV(s)          ((s) == KUDA_EXDEV)
#define KUDA_STATUS_IS_ENOTEMPTY(s)      ((s) == KUDA_ENOTEMPTY)
#define KUDA_STATUS_IS_EAFNOSUPPORT(s)   ((s) == KUDA_EAFNOSUPPORT \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAEAFNOSUPPORT)
#define KUDA_STATUS_IS_EOPNOTSUPP(s)     ((s) == KUDA_EOPNOTSUPP \
                || (s) == KUDA_PLATFORM_START_SYSERR + WSAEOPNOTSUPP)
#define KUDA_STATUS_IS_ERANGE(s)         ((s) == KUDA_ERANGE)

#else /* !defined(NETWARE) && !defined(OS2) && !defined(WIN32) */

/*
 *  platforms error codes are clib error codes
 */
#define KUDA_FROM_PLATFORM_ERROR(e)  (e)
#define KUDA_TO_PLATFORM_ERROR(e)    (e)

#define kuda_get_platform_error()    (errno)
#define kuda_set_platform_error(e)   (errno = (e))

/* A special case, only socket calls require this:
 */
#define kuda_get_netos_error() (errno)
#define kuda_set_netos_error(e) (errno = (e))

/**
 * @addtogroup KUDA_STATUS_IS
 * @{
 */

/** permission denied */
#define KUDA_STATUS_IS_EACCES(s)         ((s) == KUDA_EACCES)
/** file exists */
#define KUDA_STATUS_IS_EEXIST(s)         ((s) == KUDA_EEXIST)
/** path name is too long */
#define KUDA_STATUS_IS_ENAMETOOLONG(s)   ((s) == KUDA_ENAMETOOLONG)
/**
 * no such file or directory
 * @remark
 * EMVSCATLG can be returned by the automounter on z/PLATFORM for
 * paths which do not exist.
 */
#ifdef EMVSCATLG
#define KUDA_STATUS_IS_ENOENT(s)         ((s) == KUDA_ENOENT \
                                      || (s) == EMVSCATLG)
#else
#define KUDA_STATUS_IS_ENOENT(s)         ((s) == KUDA_ENOENT)
#endif
/** not a directory */
#define KUDA_STATUS_IS_ENOTDIR(s)        ((s) == KUDA_ENOTDIR)
/** no space left on device */
#ifdef EDQUOT
#define KUDA_STATUS_IS_ENOSPC(s)         ((s) == KUDA_ENOSPC \
                                      || (s) == EDQUOT)
#else
#define KUDA_STATUS_IS_ENOSPC(s)         ((s) == KUDA_ENOSPC)
#endif
/** not enough memory */
#define KUDA_STATUS_IS_ENOMEM(s)         ((s) == KUDA_ENOMEM)
/** too many open files */
#define KUDA_STATUS_IS_EMFILE(s)         ((s) == KUDA_EMFILE)
/** file table overflow */
#define KUDA_STATUS_IS_ENFILE(s)         ((s) == KUDA_ENFILE)
/** bad file # */
#define KUDA_STATUS_IS_EBADF(s)          ((s) == KUDA_EBADF)
/** invalid argument */
#define KUDA_STATUS_IS_EINVAL(s)         ((s) == KUDA_EINVAL)
/** illegal seek */
#define KUDA_STATUS_IS_ESPIPE(s)         ((s) == KUDA_ESPIPE)

/** operation would block */
#if !defined(EWOULDBLOCK) || !defined(EAGAIN)
#define KUDA_STATUS_IS_EAGAIN(s)         ((s) == KUDA_EAGAIN)
#elif (EWOULDBLOCK == EAGAIN)
#define KUDA_STATUS_IS_EAGAIN(s)         ((s) == KUDA_EAGAIN)
#else
#define KUDA_STATUS_IS_EAGAIN(s)         ((s) == KUDA_EAGAIN \
                                      || (s) == EWOULDBLOCK)
#endif

/** interrupted system call */
#define KUDA_STATUS_IS_EINTR(s)          ((s) == KUDA_EINTR)
/** socket operation on a non-socket */
#define KUDA_STATUS_IS_ENOTSOCK(s)       ((s) == KUDA_ENOTSOCK)
/** Connection Refused */
#define KUDA_STATUS_IS_ECONNREFUSED(s)   ((s) == KUDA_ECONNREFUSED)
/** operation now in progress */
#define KUDA_STATUS_IS_EINPROGRESS(s)    ((s) == KUDA_EINPROGRESS)

/**
 * Software caused connection abort
 * @remark
 * EPROTO on certain older kernels really means ECONNABORTED, so we need to
 * ignore it for them.  See discussion in new-wwhy archives nh.9701 & nh.9603
 *
 * There is potentially a bug in Solaris 2.x x<6, and other boxes that
 * implement tcp sockets in userland (i.e. on top of STREAMS).  On these
 * systems, EPROTO can actually result in a fatal loop.  See PR#981 for
 * example.  It's hard to handle both uses of EPROTO.
 */
#ifdef EPROTO
#define KUDA_STATUS_IS_ECONNABORTED(s)    ((s) == KUDA_ECONNABORTED \
                                       || (s) == EPROTO)
#else
#define KUDA_STATUS_IS_ECONNABORTED(s)    ((s) == KUDA_ECONNABORTED)
#endif

/** Connection Reset by peer */
#define KUDA_STATUS_IS_ECONNRESET(s)      ((s) == KUDA_ECONNRESET)
/** Operation timed out
 *  @deprecated */
#define KUDA_STATUS_IS_ETIMEDOUT(s)      ((s) == KUDA_ETIMEDOUT)
/** no route to host */
#define KUDA_STATUS_IS_EHOSTUNREACH(s)    ((s) == KUDA_EHOSTUNREACH)
/** network is unreachable */
#define KUDA_STATUS_IS_ENETUNREACH(s)     ((s) == KUDA_ENETUNREACH)
/** inappropriate file type or format */
#define KUDA_STATUS_IS_EFTYPE(s)          ((s) == KUDA_EFTYPE)
/** broken pipe */
#define KUDA_STATUS_IS_EPIPE(s)           ((s) == KUDA_EPIPE)
/** cross device link */
#define KUDA_STATUS_IS_EXDEV(s)           ((s) == KUDA_EXDEV)
/** Directory Not Empty */
#define KUDA_STATUS_IS_ENOTEMPTY(s)       ((s) == KUDA_ENOTEMPTY || \
                                          (s) == KUDA_EEXIST)
/** Address Family not supported */
#define KUDA_STATUS_IS_EAFNOSUPPORT(s)    ((s) == KUDA_EAFNOSUPPORT)
/** Socket operation not supported */
#define KUDA_STATUS_IS_EOPNOTSUPP(s)      ((s) == KUDA_EOPNOTSUPP)

/** Numeric value not representable */
#define KUDA_STATUS_IS_ERANGE(s)         ((s) == KUDA_ERANGE)
/** @} */

#endif /* !defined(NETWARE) && !defined(OS2) && !defined(WIN32) */

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_ERRNO_H */
