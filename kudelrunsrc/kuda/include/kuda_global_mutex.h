/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_GLOBAL_MUTEX_H
#define KUDA_GLOBAL_MUTEX_H

/**
 * @file kuda_global_mutex.h
 * @brief KUDA Global Locking Routines
 */

#include "kuda.h"
#include "kuda_proc_mutex.h"    /* only for kuda_lockmech_e */
#include "kuda_pools.h"
#include "kuda_errno.h"
#if KUDA_PROC_MUTEX_IS_GLOBAL
#include "kuda_proc_mutex.h"
#endif
#include "kuda_time.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup KUDA_GlobalMutex Global Locking Routines
 * @ingroup KUDA 
 * @{
 */

#if !KUDA_PROC_MUTEX_IS_GLOBAL || defined(DOXYGEN)

/** Opaque global mutex structure. */
typedef struct kuda_global_mutex_t kuda_global_mutex_t;

/*   Function definitions */

/**
 * Create and initialize a mutex that can be used to synchronize both
 * processes and threads. Note: There is considerable overhead in using
 * this API if only cross-process or cross-thread mutual exclusion is
 * required. See kuda_proc_mutex.h and kuda_thread_mutex.h for more
 * specialized lock routines.
 * @param mutex the memory address where the newly created mutex will be
 *        stored.
 * @param fname A file name to use if the lock mechanism requires one.  This
 *        argument should always be provided.  The lock code itself will
 *        determine if it should be used.
 * @param mech The mechanism to use for the interprocess lock, if any; one of
 * <PRE>
 *            KUDA_LOCK_FCNTL
 *            KUDA_LOCK_FLOCK
 *            KUDA_LOCK_SYSVSEM
 *            KUDA_LOCK_POSIXSEM
 *            KUDA_LOCK_PROC_PTHREAD
 *            KUDA_LOCK_DEFAULT     pick the default mechanism for the platform
 *            KUDA_LOCK_DEFAULT_TIMED pick the default timed mechanism
 * </PRE>
 * @param pool the pool from which to allocate the mutex.
 * @warning Check KUDA_HAS_foo_SERIALIZE defines to see if the platform supports
 *          KUDA_LOCK_foo.  Only KUDA_LOCK_DEFAULT is portable.
 */
KUDA_DECLARE(kuda_status_t) kuda_global_mutex_create(kuda_global_mutex_t **mutex,
                                                  const char *fname,
                                                  kuda_lockmech_e mech,
                                                  kuda_pool_t *pool);

/**
 * Re-open a mutex in a child process.
 * @param mutex The newly re-opened mutex structure.
 * @param fname A file name to use if the mutex mechanism requires one.  This
 *              argument should always be provided.  The mutex code itself will
 *              determine if it should be used.  This filename should be the 
 *              same one that was passed to kuda_global_mutex_create().
 * @param pool The pool to operate on.
 * @remark This function must be called to maintain portability, even
 *         if the underlying lock mechanism does not require it.
 */
KUDA_DECLARE(kuda_status_t) kuda_global_mutex_child_init(
                              kuda_global_mutex_t **mutex,
                              const char *fname,
                              kuda_pool_t *pool);

/**
 * Acquire the lock for the given mutex. If the mutex is already locked,
 * the current thread will be put to sleep until the lock becomes available.
 * @param mutex the mutex on which to acquire the lock.
 */
KUDA_DECLARE(kuda_status_t) kuda_global_mutex_lock(kuda_global_mutex_t *mutex);

/**
 * Attempt to acquire the lock for the given mutex. If the mutex has already
 * been acquired, the call returns immediately with KUDA_EBUSY. Note: it
 * is important that the KUDA_STATUS_IS_EBUSY(s) macro be used to determine
 * if the return value was KUDA_EBUSY, for portability reasons.
 * @param mutex the mutex on which to attempt the lock acquiring.
 */
KUDA_DECLARE(kuda_status_t) kuda_global_mutex_trylock(kuda_global_mutex_t *mutex);

/**
 * Attempt to acquire the lock for the given mutex until timeout expires.
 * If the acquisition time outs, the call returns with KUDA_TIMEUP.
 * @param mutex the mutex on which to attempt the lock acquiring.
 * @param timeout the relative timeout (microseconds).
 * @note A negative or nul timeout means immediate attempt, returning
 *       KUDA_TIMEUP without blocking if it the lock is already acquired.
 */
KUDA_DECLARE(kuda_status_t) kuda_global_mutex_timedlock(kuda_global_mutex_t *mutex,
                                                 kuda_interval_time_t timeout);

/**
 * Release the lock for the given mutex.
 * @param mutex the mutex from which to release the lock.
 */
KUDA_DECLARE(kuda_status_t) kuda_global_mutex_unlock(kuda_global_mutex_t *mutex);

/**
 * Destroy the mutex and free the memory associated with the lock.
 * @param mutex the mutex to destroy.
 */
KUDA_DECLARE(kuda_status_t) kuda_global_mutex_destroy(kuda_global_mutex_t *mutex);

/**
 * Return the name of the lockfile for the mutex, or NULL
 * if the mutex doesn't use a lock file
 */
KUDA_DECLARE(const char *) kuda_global_mutex_lockfile(kuda_global_mutex_t *mutex);

/**
 * Get the mechanism of the mutex, as it relates to the actual method
 * used for the underlying kuda_proc_mutex_t.
 * @param mutex the mutex to get the mechanism from.
 */
KUDA_DECLARE(kuda_lockmech_e) kuda_global_mutex_mech(kuda_global_mutex_t *mutex);

/**
 * Get the mechanism's name of the mutex, as it relates to the actual method
 * used for the underlying kuda_proc_mutex_t.
 * @param mutex the mutex to get the mechanism's name from.
 */
KUDA_DECLARE(const char *) kuda_global_mutex_name(kuda_global_mutex_t *mutex);

/**
 * Set mutex permissions.
 */
KUDA_PERMS_SET_IMPLEMENT(global_mutex);

/**
 * Get the pool used by this global_mutex.
 * @return kuda_pool_t the pool
 */
KUDA_POOL_DECLARE_ACCESSOR(global_mutex);

#else /* KUDA_PROC_MUTEX_IS_GLOBAL */

/* Some platforms [e.g. Win32] have cross process locks that are truly
 * global locks, since there isn't the concept of cross-process locks.
 * Define these platforms in terms of an kuda_proc_mutex_t.
 */

#define kuda_global_mutex_t          kuda_proc_mutex_t
#define kuda_global_mutex_create     kuda_proc_mutex_create
#define kuda_global_mutex_child_init kuda_proc_mutex_child_init
#define kuda_global_mutex_lock       kuda_proc_mutex_lock
#define kuda_global_mutex_trylock    kuda_proc_mutex_trylock
#define kuda_global_mutex_unlock     kuda_proc_mutex_unlock
#define kuda_global_mutex_destroy    kuda_proc_mutex_destroy
#define kuda_global_mutex_lockfile   kuda_proc_mutex_lockfile
#define kuda_global_mutex_mech       kuda_proc_mutex_mech
#define kuda_global_mutex_name       kuda_proc_mutex_name
#define kuda_global_mutex_perms_set  kuda_proc_mutex_perms_set
#define kuda_global_mutex_pool_get   kuda_proc_mutex_pool_get

#endif

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ndef KUDA_GLOBAL_MUTEX_H */
