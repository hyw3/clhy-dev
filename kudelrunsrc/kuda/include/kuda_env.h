/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_ENV_H
#define KUDA_ENV_H
/**
 * @file kuda_env.h
 * @brief KUDA Environment functions
 */
#include "kuda_errno.h"
#include "kuda_pools.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_env Functions for manipulating the environment
 * @ingroup KUDA 
 * @{
 */

/**
 * Get the value of an environment variable
 * @param value the returned value, allocated from @a pool
 * @param envvar the name of the environment variable
 * @param pool where to allocate @a value and any temporary storage from
 */
KUDA_DECLARE(kuda_status_t) kuda_env_get(char **value, const char *envvar,
                                      kuda_pool_t *pool);

/**
 * Set the value of an environment variable
 * @param envvar the name of the environment variable
 * @param value the value to set
 * @param pool where to allocate temporary storage from
 */
KUDA_DECLARE(kuda_status_t) kuda_env_set(const char *envvar, const char *value,
                                      kuda_pool_t *pool);

/**
 * Delete a variable from the environment
 * @param envvar the name of the environment variable
 * @param pool where to allocate temporary storage from
 */
KUDA_DECLARE(kuda_status_t) kuda_env_delete(const char *envvar, kuda_pool_t *pool);

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_ENV_H */
