/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_NETWORK_IO_H
#define KUDA_NETWORK_IO_H
/**
 * @file kuda_network_io.h
 * @brief KUDA Network library
 */

#include "kuda.h"
#include "kuda_pools.h"
#include "kuda_file_io.h"
#include "kuda_errno.h"
#include "kuda_inherit.h" 
#include "kuda_perms_set.h"

#if KUDA_HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif
#if KUDA_HAVE_SYS_UN_H
#include <sys/un.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_network_io Network Routines
 * @ingroup KUDA 
 * @{
 */

#ifndef KUDA_MAX_SECS_TO_LINGER
/** Maximum seconds to linger */
#define KUDA_MAX_SECS_TO_LINGER 30
#endif

#ifndef KUDAMAXHOSTLEN
/** Maximum hostname length */
#define KUDAMAXHOSTLEN 256
#endif

#ifndef KUDA_ANYADDR
/** Default 'any' address */
#define KUDA_ANYADDR "0.0.0.0"
#endif

/**
 * @defgroup kuda_sockopt Socket option definitions
 * @{
 */
#define KUDA_SO_LINGER        1    /**< Linger */
#define KUDA_SO_KEEPALIVE     2    /**< Keepalive */
#define KUDA_SO_DEBUG         4    /**< Debug */
#define KUDA_SO_NONBLOCK      8    /**< Non-blocking IO */
#define KUDA_SO_REUSEADDR     16   /**< Reuse addresses */
#define KUDA_SO_SNDBUF        64   /**< Send buffer */
#define KUDA_SO_RCVBUF        128  /**< Receive buffer */
#define KUDA_SO_DISCONNECTED  256  /**< Disconnected */
#define KUDA_TCP_NODELAY      512  /**< For SCTP sockets, this is mapped
                                   * to STCP_NODELAY internally.
                                   */
#define KUDA_TCP_NOPUSH       1024 /**< No push */
#define KUDA_RESET_NODELAY    2048 /**< This flag is ONLY set internally
                                   * when we set KUDA_TCP_NOPUSH with
                                   * KUDA_TCP_NODELAY set to tell us that
                                   * KUDA_TCP_NODELAY should be turned on
                                   * again when NOPUSH is turned off
                                   */
#define KUDA_INCOMPLETE_READ 4096  /**< Set on non-blocking sockets
				   * (timeout != 0) on which the
				   * previous read() did not fill a buffer
				   * completely.  the next kuda_socket_recv() 
                                   * will first call select()/poll() rather than
				   * going straight into read().  (Can also
				   * be set by an application to force a
				   * select()/poll() call before the next
				   * read, in cases where the app expects
				   * that an immediate read would fail.)
				   */
#define KUDA_INCOMPLETE_WRITE 8192 /**< like KUDA_INCOMPLETE_READ, but for write
                                   * @see KUDA_INCOMPLETE_READ
                                   */
#define KUDA_IPV6_V6ONLY     16384 /**< Don't accept IPv4 connections on an
                                   * IPv6 listening socket.
                                   */
#define KUDA_TCP_DEFER_ACCEPT 32768 /**< Delay accepting of new connections 
                                    * until data is available.
                                    * @see kuda_socket_accept_filter
                                    */
#define KUDA_SO_BROADCAST     65536 /**< Allow broadcast
                                    */
#define KUDA_SO_FREEBIND     131072 /**< Allow binding to addresses not owned
                                    * by any interface
                                    */

/** @} */

/** Define what type of socket shutdown should occur. */
typedef enum {
    KUDA_SHUTDOWN_READ,          /**< no longer allow read request */
    KUDA_SHUTDOWN_WRITE,         /**< no longer allow write requests */
    KUDA_SHUTDOWN_READWRITE      /**< no longer allow read or write requests */
} kuda_shutdown_how_e;

#define KUDA_IPV4_ADDR_OK  0x01  /**< @see kuda_sockaddr_info_get() */
#define KUDA_IPV6_ADDR_OK  0x02  /**< @see kuda_sockaddr_info_get() */

#if (!KUDA_HAVE_IN_ADDR)
/**
 * We need to make sure we always have an in_addr type, so KUDA will just
 * define it ourselves, if the platform doesn't provide it.
 */
struct in_addr {
    kuda_uint32_t  s_addr; /**< storage to hold the IP# */
};
#endif

/** @def KUDA_INADDR_NONE
 * Not all platforms have a real INADDR_NONE.  This macro replaces
 * INADDR_NONE on all platforms.
 */
#ifdef INADDR_NONE
#define KUDA_INADDR_NONE INADDR_NONE
#else
#define KUDA_INADDR_NONE ((unsigned int) 0xffffffff)
#endif

/**
 * @def KUDA_INET
 * Not all platforms have these defined, so we'll define them here
 * The default values come from FreeBSD 4.1.1
 */
#define KUDA_INET     AF_INET
/** @def KUDA_UNSPEC
 * Let the system decide which address family to use
 */
#ifdef AF_UNSPEC
#define KUDA_UNSPEC   AF_UNSPEC
#else
#define KUDA_UNSPEC   0
#endif
#if KUDA_HAVE_IPV6
/** @def KUDA_INET6
* IPv6 Address Family. Not all platforms may have this defined.
*/

#define KUDA_INET6    AF_INET6
#endif

#if KUDA_HAVE_SOCKADDR_UN
#if defined (AF_UNIX)
#define KUDA_UNIX    AF_UNIX
#elif defined(AF_LOCAL)
#define KUDA_UNIX    AF_LOCAL
#else
#error "Neither AF_UNIX nor AF_LOCAL is defined"
#endif
#else /* !KUDA_HAVE_SOCKADDR_UN */
#if defined (AF_UNIX)
#define KUDA_UNIX    AF_UNIX
#elif defined(AF_LOCAL)
#define KUDA_UNIX    AF_LOCAL
#else
/* TODO: Use a smarter way to detect unique KUDA_UNIX value */
#define KUDA_UNIX    1234
#endif
#endif

/**
 * @defgroup IP_Proto IP Protocol Definitions for use when creating sockets
 * @{
 */
#define KUDA_PROTO_TCP       6   /**< TCP  */
#define KUDA_PROTO_UDP      17   /**< UDP  */
#define KUDA_PROTO_SCTP    132   /**< SCTP */
/** @} */

/**
 * Enum used to denote either the local and remote endpoint of a
 * connection.
 */
typedef enum {
    KUDA_LOCAL,   /**< Socket information for local end of connection */
    KUDA_REMOTE   /**< Socket information for remote end of connection */
} kuda_interface_e;

/**
 * The specific declaration of inet_addr's ... some platforms fall back
 * inet_network (this is not good, but necessary)
 */

#if KUDA_HAVE_INET_ADDR
#define kuda_inet_addr    inet_addr
#elif KUDA_HAVE_INET_NETWORK        /* only DGUX, as far as I know */
/**
 * @warning
 * not generally safe... inet_network() and inet_addr() perform
 * different functions */
#define kuda_inet_addr    inet_network
#endif

/** A structure to represent sockets */
typedef struct kuda_socket_t     kuda_socket_t;
/**
 * A structure to encapsulate headers and trailers for kuda_socket_sendfile
 */
typedef struct kuda_hdtr_t       kuda_hdtr_t;
/** A structure to represent in_addr */
typedef struct in_addr          kuda_in_addr_t;
/** A structure to represent an IP subnet */
typedef struct kuda_ipsubnet_t kuda_ipsubnet_t;

/** @remark use kuda_uint16_t just in case some system has a short that isn't 16 bits... */
typedef kuda_uint16_t            kuda_port_t;

/** @remark It's defined here as I think it should all be platform safe...
 * @see kuda_sockaddr_t
 */
typedef struct kuda_sockaddr_t kuda_sockaddr_t;
/**
 * KUDAs socket address type, used to ensure protocol independence
 */
struct kuda_sockaddr_t {
    /** The pool to use... */
    kuda_pool_t *pool;
    /** The hostname */
    char *hostname;
    /** Either a string of the port number or the service name for the port */
    char *servname;
    /** The numeric port */
    kuda_port_t port;
    /** The family */
    kuda_int32_t family;
    /** How big is the sockaddr we're using? */
    kuda_socklen_t salen;
    /** How big is the ip address structure we're using? */
    int ipaddr_len;
    /** How big should the address buffer be?  16 for v4 or 46 for v6
     *  used in inet_ntop... */
    int addr_str_len;
    /** This points to the IP address structure within the appropriate
     *  sockaddr structure.  */
    void *ipaddr_ptr;
    /** If multiple addresses were found by kuda_sockaddr_info_get(), this 
     *  points to a representation of the next address. */
    kuda_sockaddr_t *next;
    /** Union of either IPv4 or IPv6 sockaddr. */
    union {
        /** IPv4 sockaddr structure */
        struct sockaddr_in sin;
#if KUDA_HAVE_IPV6
        /** IPv6 sockaddr structure */
        struct sockaddr_in6 sin6;
#endif
#if KUDA_HAVE_SA_STORAGE
        /** Placeholder to ensure that the size of this union is not
         * dependent on whether KUDA_HAVE_IPV6 is defined. */
        struct sockaddr_storage sas;
#endif
#if KUDA_HAVE_SOCKADDR_UN
        /** Unix domain socket sockaddr structure */
        struct sockaddr_un unx;
#endif
    } sa;
};

#if KUDA_HAS_SENDFILE
/** 
 * Support reusing the socket on platforms which support it (from disconnect,
 * specifically Win32.
 * @remark Optional flag passed into kuda_socket_sendfile() 
 */
#define KUDA_SENDFILE_DISCONNECT_SOCKET      1
#endif

/** A structure to encapsulate headers and trailers for kuda_socket_sendfile */
struct kuda_hdtr_t {
    /** An iovec to store the headers sent before the file. */
    struct iovec* headers;
    /** number of headers in the iovec */
    int numheaders;
    /** An iovec to store the trailers sent after the file. */
    struct iovec* trailers;
    /** number of trailers in the iovec */
    int numtrailers;
};

/* function definitions */

/**
 * Create a socket.
 * @param new_sock The new socket that has been set up.
 * @param family The address family of the socket (e.g., KUDA_INET).
 * @param type The type of the socket (e.g., SOCK_STREAM).
 * @param protocol The protocol of the socket (e.g., KUDA_PROTO_TCP).
 * @param cont The pool for the kuda_socket_t and associated storage.
 * @note The pool will be used by various functions that operate on the
 *       socket. The caller must ensure that it is not used by other threads
 *       at the same time.
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_create(kuda_socket_t **new_sock, 
                                            int family, int type,
                                            int protocol,
                                            kuda_pool_t *cont);

/**
 * Shutdown either reading, writing, or both sides of a socket.
 * @param thesocket The socket to close 
 * @param how How to shutdown the socket.  One of:
 * <PRE>
 *            KUDA_SHUTDOWN_READ         no longer allow read requests
 *            KUDA_SHUTDOWN_WRITE        no longer allow write requests
 *            KUDA_SHUTDOWN_READWRITE    no longer allow read or write requests 
 * </PRE>
 * @see kuda_shutdown_how_e
 * @remark This does not actually close the socket descriptor, it just
 *      controls which calls are still valid on the socket.
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_shutdown(kuda_socket_t *thesocket,
                                              kuda_shutdown_how_e how);

/**
 * Close a socket.
 * @param thesocket The socket to close 
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_close(kuda_socket_t *thesocket);

/**
 * Bind the socket to its associated port
 * @param sock The socket to bind 
 * @param sa The socket address to bind to
 * @remark This may be where we will find out if there is any other process
 *      using the selected port.
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_bind(kuda_socket_t *sock, 
                                          kuda_sockaddr_t *sa);

/**
 * Listen to a bound socket for connections.
 * @param sock The socket to listen on 
 * @param backlog The number of outstanding connections allowed in the sockets
 *                listen queue.  If this value is less than zero, the listen
 *                queue size is set to zero.  
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_listen(kuda_socket_t *sock, 
                                            kuda_int32_t backlog);

/**
 * Accept a new connection request
 * @param new_sock A copy of the socket that is connected to the socket that
 *                 made the connection request.  This is the socket which should
 *                 be used for all future communication.
 * @param sock The socket we are listening on.
 * @param connection_pool The pool for the new socket.
 * @note The pool will be used by various functions that operate on the
 *       socket. The caller must ensure that it is not used by other threads
 *       at the same time.
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_accept(kuda_socket_t **new_sock, 
                                            kuda_socket_t *sock,
                                            kuda_pool_t *connection_pool);

/**
 * Issue a connection request to a socket either on the same machine 
 * or a different one.
 * @param sock The socket we wish to use for our side of the connection 
 * @param sa The address of the machine we wish to connect to.
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_connect(kuda_socket_t *sock,
                                             kuda_sockaddr_t *sa);

/**
 * Determine whether the receive part of the socket has been closed by
 * the peer (such that a subsequent call to kuda_socket_read would
 * return KUDA_EOF), if the socket's receive buffer is empty.  This
 * function does not block waiting for I/O.
 *
 * @param sock The socket to check
 * @param atreadeof If KUDA_SUCCESS is returned, *atreadeof is set to
 *                  non-zero if a subsequent read would return KUDA_EOF
 * @return an error is returned if it was not possible to determine the
 *         status, in which case *atreadeof is not changed.
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_atreadeof(kuda_socket_t *sock,
                                               int *atreadeof);

/**
 * Create kuda_sockaddr_t from hostname, address family, and port.
 * @param sa The new kuda_sockaddr_t.
 * @param hostname The hostname or numeric address string to resolve/parse, or
 *               NULL to build an address that corresponds to 0.0.0.0 or ::
 *               or in case of KUDA_UNIX family it is absolute socket filename.
 * @param family The address family to use, or KUDA_UNSPEC if the system should 
 *               decide.
 * @param port The port number.
 * @param flags Special processing flags:
 * <PRE>
 *       KUDA_IPV4_ADDR_OK          first query for IPv4 addresses; only look
 *                                 for IPv6 addresses if the first query failed;
 *                                 only valid if family is KUDA_UNSPEC and hostname
 *                                 isn't NULL; mutually exclusive with
 *                                 KUDA_IPV6_ADDR_OK
 *       KUDA_IPV6_ADDR_OK          first query for IPv6 addresses; only look
 *                                 for IPv4 addresses if the first query failed;
 *                                 only valid if family is KUDA_UNSPEC and hostname
 *                                 isn't NULL and KUDA_HAVE_IPV6; mutually exclusive
 *                                 with KUDA_IPV4_ADDR_OK
 * </PRE>
 * @param p The pool for the kuda_sockaddr_t and associated storage.
 */
KUDA_DECLARE(kuda_status_t) kuda_sockaddr_info_get(kuda_sockaddr_t **sa,
                                          const char *hostname,
                                          kuda_int32_t family,
                                          kuda_port_t port,
                                          kuda_int32_t flags,
                                          kuda_pool_t *p);

/**
 * Copy kuda_sockaddr_t src to dst on pool p.
 * @param dst The destination kuda_sockaddr_t.
 * @param src The source kuda_sockaddr_t.
 * @param p The pool for the kuda_sockaddr_t and associated storage.
 */
KUDA_DECLARE(kuda_status_t) kuda_sockaddr_info_copy(kuda_sockaddr_t **dst,
                                                 const kuda_sockaddr_t *src,
                                                 kuda_pool_t *p);

/* Set the zone of an IPv6 link-local address object.
 * @param sa Socket address object
 * @param zone_id Zone ID (textual "eth0" or numeric "3").
 * @return Returns KUDA_EBADIP for non-IPv6 socket or an IPv6 address
 * which isn't link-local.
 */
KUDA_DECLARE(kuda_status_t) kuda_sockaddr_zone_set(kuda_sockaddr_t *sa,
                                                const char *zone_id);


/* Retrieve the zone of an IPv6 link-local address object.
 * @param sa Socket address object
 * @param name If non-NULL, set to the textual representation of the zone id
 * @param id If non-NULL, set to the integer zone id
 * @param p Pool from which *name is allocated if used.
 * @return Returns KUDA_EBADIP for non-IPv6 socket or socket without any zone id
 * set, or other error if the interface could not be mapped to a name.
 * @remark Both name and id may be NULL, neither are modified if
 * non-NULL in error cases.
 */
KUDA_DECLARE(kuda_status_t) kuda_sockaddr_zone_get(const kuda_sockaddr_t *sa,
                                                const char **name,
                                                kuda_uint32_t *id,
                                                kuda_pool_t *p);                                                
    
/**
 * Look up the host name from an kuda_sockaddr_t.
 * @param hostname The hostname.
 * @param sa The kuda_sockaddr_t.
 * @param flags Special processing flags.
 * @remark Results can vary significantly between platforms
 * when processing wildcard socket addresses.
 */
KUDA_DECLARE(kuda_status_t) kuda_getnameinfo(char **hostname,
                                          kuda_sockaddr_t *sa,
                                          kuda_int32_t flags);

/**
 * Parse hostname/IP address with scope id and port.
 *
 * Any of the following strings are accepted:
 *   8080                  (just the port number)
 *   clhy.hyang.org        (just the hostname)
 *   clhy.hyang.org:8080   (hostname and port number)
 *   [fe80::1]:80          (IPv6 numeric address string only)
 *   [fe80::1%eth0]        (IPv6 numeric address string and scope id)
 *
 * Invalid strings:
 *                         (empty string)
 *   [abc]                 (not valid IPv6 numeric address string)
 *   abc:65536             (invalid port number)
 *
 * @param addr The new buffer containing just the hostname.  On output, *addr 
 *             will be NULL if no hostname/IP address was specfied.
 * @param scope_id The new buffer containing just the scope id.  On output, 
 *                 *scope_id will be NULL if no scope id was specified.
 * @param port The port number.  On output, *port will be 0 if no port was 
 *             specified.
 *             ### FIXME: 0 is a legal port (per RFC 1700). this should
 *             ### return something besides zero if the port is missing.
 * @param str The input string to be parsed.
 * @param p The pool from which *addr and *scope_id are allocated.
 * @remark If scope id shouldn't be allowed, check for scope_id != NULL in 
 *         addition to checking the return code.  If addr/hostname should be 
 *         required, check for addr == NULL in addition to checking the 
 *         return code.
 */
KUDA_DECLARE(kuda_status_t) kuda_parse_addr_port(char **addr,
                                              char **scope_id,
                                              kuda_port_t *port,
                                              const char *str,
                                              kuda_pool_t *p);

/**
 * Get name of the current machine
 * @param buf A buffer to store the hostname in.
 * @param len The maximum length of the hostname that can be stored in the
 *            buffer provided.  The suggested length is KUDAMAXHOSTLEN + 1.
 * @param cont The pool to use.
 * @remark If the buffer was not large enough, an error will be returned.
 */
KUDA_DECLARE(kuda_status_t) kuda_gethostname(char *buf, int len, kuda_pool_t *cont);

/**
 * Return the data associated with the current socket
 * @param data The user data associated with the socket.
 * @param key The key to associate with the user data.
 * @param sock The currently open socket.
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_data_get(void **data, const char *key,
                                              kuda_socket_t *sock);

/**
 * Set the data associated with the current socket.
 * @param sock The currently open socket.
 * @param data The user data to associate with the socket.
 * @param key The key to associate with the data.
 * @param cleanup The cleanup to call when the socket is destroyed.
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_data_set(kuda_socket_t *sock, void *data,
                                              const char *key,
                                              kuda_status_t (*cleanup)(void*));

/**
 * Send data over a network.
 * @param sock The socket to send the data over.
 * @param buf The buffer which contains the data to be sent. 
 * @param len On entry, the number of bytes to send; on exit, the number
 *            of bytes sent.
 * @remark
 * <PRE>
 * This functions acts like a blocking write by default.  To change 
 * this behavior, use kuda_socket_timeout_set() or the KUDA_SO_NONBLOCK
 * socket option.
 *
 * It is possible for both bytes to be sent and an error to be returned.
 *
 * KUDA_EINTR is never returned.
 * </PRE>
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_send(kuda_socket_t *sock, const char *buf, 
                                          kuda_size_t *len);

/**
 * Send multiple buffers over a network.
 * @param sock The socket to send the data over.
 * @param vec The array of iovec structs containing the data to send 
 * @param nvec The number of iovec structs in the array
 * @param len Receives the number of bytes actually written
 * @remark
 * <PRE>
 * This functions acts like a blocking write by default.  To change 
 * this behavior, use kuda_socket_timeout_set() or the KUDA_SO_NONBLOCK
 * socket option.
 * The number of bytes actually sent is stored in argument 4.
 *
 * It is possible for both bytes to be sent and an error to be returned.
 *
 * KUDA_EINTR is never returned.
 * </PRE>
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_sendv(kuda_socket_t *sock, 
                                           const struct iovec *vec,
                                           kuda_int32_t nvec, kuda_size_t *len);

/**
 * @param sock The socket to send from
 * @param where The kuda_sockaddr_t describing where to send the data
 * @param flags The flags to use
 * @param buf  The data to send
 * @param len  The length of the data to send
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_sendto(kuda_socket_t *sock, 
                                            kuda_sockaddr_t *where,
                                            kuda_int32_t flags, const char *buf, 
                                            kuda_size_t *len);

/**
 * Read data from a socket.  On success, the address of the peer from
 * which the data was sent is copied into the @a from parameter, and the
 * @a len parameter is updated to give the number of bytes written to
 * @a buf.
 *
 * @param from Updated with the address from which the data was received
 * @param sock The socket to use
 * @param flags The flags to use
 * @param buf  The buffer to use
 * @param len  The length of the available buffer
 */

KUDA_DECLARE(kuda_status_t) kuda_socket_recvfrom(kuda_sockaddr_t *from, 
                                              kuda_socket_t *sock,
                                              kuda_int32_t flags, char *buf, 
                                              kuda_size_t *len);
 
#if KUDA_HAS_SENDFILE || defined(DOXYGEN)

/**
 * Send a file from an open file descriptor to a socket, along with 
 * optional headers and trailers
 * @param sock The socket to which we're writing
 * @param file The open file from which to read
 * @param hdtr A structure containing the headers and trailers to send
 * @param offset Offset into the file where we should begin writing
 * @param len (input)  - Number of bytes to send from the file 
 *            (output) - Number of bytes actually sent, 
 *                       including headers, file, and trailers
 * @param flags KUDA flags that are mapped to PLATFORM specific flags
 * @remark This functions acts like a blocking write by default.  To change 
 *         this behavior, use kuda_socket_timeout_set() or the
 *         KUDA_SO_NONBLOCK socket option.
 * The number of bytes actually sent is stored in the len parameter.
 * The offset parameter is passed by reference for no reason; its
 * value will never be modified by the kuda_socket_sendfile() function.
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_sendfile(kuda_socket_t *sock, 
                                              kuda_file_t *file,
                                              kuda_hdtr_t *hdtr,
                                              kuda_off_t *offset,
                                              kuda_size_t *len,
                                              kuda_int32_t flags);

#endif /* KUDA_HAS_SENDFILE */

/**
 * Read data from a network.
 * @param sock The socket to read the data from.
 * @param buf The buffer to store the data in. 
 * @param len On entry, the number of bytes to receive; on exit, the number
 *            of bytes received.
 * @remark
 * <PRE>
 * This functions acts like a blocking read by default.  To change 
 * this behavior, use kuda_socket_timeout_set() or the KUDA_SO_NONBLOCK
 * socket option.
 * The number of bytes actually received is stored in argument 3.
 *
 * It is possible for both bytes to be received and an KUDA_EOF or
 * other error to be returned.
 *
 * KUDA_EINTR is never returned.
 * </PRE>
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_recv(kuda_socket_t *sock, 
                                   char *buf, kuda_size_t *len);

/**
 * Setup socket options for the specified socket
 * @param sock The socket to set up.
 * @param opt The option we would like to configure.  One of:
 * <PRE>
 *            KUDA_SO_DEBUG      --  turn on debugging information 
 *            KUDA_SO_KEEPALIVE  --  keep connections active
 *            KUDA_SO_LINGER     --  lingers on close if data is present
 *            KUDA_SO_NONBLOCK   --  Turns blocking on/off for socket
 *                                  When this option is enabled, use
 *                                  the KUDA_STATUS_IS_EAGAIN() macro to
 *                                  see if a send or receive function
 *                                  could not transfer data without
 *                                  blocking.
 *            KUDA_SO_REUSEADDR  --  The rules used in validating addresses
 *                                  supplied to bind should allow reuse
 *                                  of local addresses.
 *            KUDA_SO_SNDBUF     --  Set the SendBufferSize
 *            KUDA_SO_RCVBUF     --  Set the ReceiveBufferSize
 *            KUDA_SO_FREEBIND   --  Allow binding to non-local IP address.
 * </PRE>
 * @param on Value for the option.
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_opt_set(kuda_socket_t *sock,
                                             kuda_int32_t opt, kuda_int32_t on);

/**
 * Setup socket timeout for the specified socket
 * @param sock The socket to set up.
 * @param t Value for the timeout.
 * <PRE>
 *   t > 0  -- read and write calls return KUDA_TIMEUP if specified time
 *             elapsess with no data read or written
 *   t == 0 -- read and write calls never block
 *   t < 0  -- read and write calls block
 * </PRE>
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_timeout_set(kuda_socket_t *sock,
                                                 kuda_interval_time_t t);

/**
 * Query socket options for the specified socket
 * @param sock The socket to query
 * @param opt The option we would like to query.  One of:
 * <PRE>
 *            KUDA_SO_DEBUG      --  turn on debugging information 
 *            KUDA_SO_KEEPALIVE  --  keep connections active
 *            KUDA_SO_LINGER     --  lingers on close if data is present
 *            KUDA_SO_NONBLOCK   --  Turns blocking on/off for socket
 *            KUDA_SO_REUSEADDR  --  The rules used in validating addresses
 *                                  supplied to bind should allow reuse
 *                                  of local addresses.
 *            KUDA_SO_SNDBUF     --  Set the SendBufferSize
 *            KUDA_SO_RCVBUF     --  Set the ReceiveBufferSize
 *            KUDA_SO_DISCONNECTED -- Query the disconnected state of the socket.
 *                                  (Currently only used on Windows)
 * </PRE>
 * @param on Socket option returned on the call.
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_opt_get(kuda_socket_t *sock, 
                                             kuda_int32_t opt, kuda_int32_t *on);

/**
 * Query socket timeout for the specified socket
 * @param sock The socket to query
 * @param t Socket timeout returned from the query.
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_timeout_get(kuda_socket_t *sock, 
                                                 kuda_interval_time_t *t);

/**
 * Query the specified socket if at the OOB/Urgent data mark
 * @param sock The socket to query
 * @param atmark Is set to true if socket is at the OOB/urgent mark,
 *               otherwise is set to false.
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_atmark(kuda_socket_t *sock, 
                                            int *atmark);

/**
 * Return an address associated with a socket; either the address to
 * which the socket is bound locally or the address of the peer
 * to which the socket is connected.
 * @param sa The returned kuda_sockaddr_t.
 * @param which Whether to retrieve the local or remote address
 * @param sock The socket to use
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_addr_get(kuda_sockaddr_t **sa,
                                              kuda_interface_e which,
                                              kuda_socket_t *sock);
 
/**
 * Return the IP address (in numeric address string format) in
 * an KUDA socket address.  KUDA will allocate storage for the IP address 
 * string from the pool of the kuda_sockaddr_t.
 * @param addr The IP address.
 * @param sockaddr The socket address to reference.
 */
KUDA_DECLARE(kuda_status_t) kuda_sockaddr_ip_get(char **addr, 
                                              kuda_sockaddr_t *sockaddr);

/**
 * Write the IP address (in numeric address string format) of the KUDA
 * socket address @a sockaddr into the buffer @a buf (of size @a buflen).
 * @param sockaddr The socket address to reference.
 */
KUDA_DECLARE(kuda_status_t) kuda_sockaddr_ip_getbuf(char *buf, kuda_size_t buflen,
                                                 kuda_sockaddr_t *sockaddr);

/**
 * See if the IP addresses in two KUDA socket addresses are
 * equivalent.  Appropriate logic is present for comparing
 * IPv4-mapped IPv6 addresses with IPv4 addresses.
 *
 * @param addr1 One of the KUDA socket addresses.
 * @param addr2 The other KUDA socket address.
 * @remark The return value will be non-zero if the addresses
 * are equivalent.
 */
KUDA_DECLARE(int) kuda_sockaddr_equal(const kuda_sockaddr_t *addr1,
                                    const kuda_sockaddr_t *addr2);

/**
 * See if the IP address in an KUDA socket address refers to the wildcard
 * address for the protocol family (e.g., INADDR_ANY for IPv4).
 *
 * @param addr The KUDA socket address to examine.
 * @remark The return value will be non-zero if the address is
 * initialized and is the wildcard address.
 */
KUDA_DECLARE(int) kuda_sockaddr_is_wildcard(const kuda_sockaddr_t *addr);

/**
* Return the type of the socket.
* @param sock The socket to query.
* @param type The returned type (e.g., SOCK_STREAM).
*/
KUDA_DECLARE(kuda_status_t) kuda_socket_type_get(kuda_socket_t *sock,
                                              int *type);
 
/**
 * Given an kuda_sockaddr_t and a service name, set the port for the service
 * @param sockaddr The kuda_sockaddr_t that will have its port set
 * @param servname The name of the service you wish to use
 */
KUDA_DECLARE(kuda_status_t) kuda_getservbyname(kuda_sockaddr_t *sockaddr, 
                                            const char *servname);
/**
 * Build an ip-subnet representation from an IP address and optional netmask or
 * number-of-bits.
 * @param ipsub The new ip-subnet representation
 * @param ipstr The input IP address string
 * @param mask_or_numbits The input netmask or number-of-bits string, or NULL
 * @param p The pool to allocate from
 */
KUDA_DECLARE(kuda_status_t) kuda_ipsubnet_create(kuda_ipsubnet_t **ipsub, 
                                              const char *ipstr, 
                                              const char *mask_or_numbits, 
                                              kuda_pool_t *p);

/**
 * Test the IP address in an kuda_sockaddr_t against a pre-built ip-subnet
 * representation.
 * @param ipsub The ip-subnet representation
 * @param sa The socket address to test
 * @return non-zero if the socket address is within the subnet, 0 otherwise
 */
KUDA_DECLARE(int) kuda_ipsubnet_test(kuda_ipsubnet_t *ipsub, kuda_sockaddr_t *sa);

#if KUDA_HAS_SO_ACCEPTFILTER || defined(DOXYGEN)
/**
 * Set an PLATFORM level accept filter.
 * @param sock The socket to put the accept filter on.
 * @param name The accept filter
 * @param args Any extra args to the accept filter.  Passing NULL here removes
 *             the accept filter. 
 * @bug name and args should have been declared as const char *, as they are in
 * KUDA 2.0
 */
kuda_status_t kuda_socket_accept_filter(kuda_socket_t *sock, char *name,
                                      char *args);
#endif

/**
 * Return the protocol of the socket.
 * @param sock The socket to query.
 * @param protocol The returned protocol (e.g., KUDA_PROTO_TCP).
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_protocol_get(kuda_socket_t *sock,
                                                  int *protocol);

/**
 * Get the pool used by the socket.
 */
KUDA_POOL_DECLARE_ACCESSOR(socket);

/**
 * Set a socket to be inherited by child processes.
 */
KUDA_DECLARE_INHERIT_SET(socket);

/**
 * Unset a socket from being inherited by child processes.
 */
KUDA_DECLARE_INHERIT_UNSET(socket);

/**
 * Set socket permissions.
 */
KUDA_PERMS_SET_IMPLEMENT(socket);

/**
 * @defgroup kuda_mcast IP Multicast
 * @{
 */

/**
 * Join a Multicast Group
 * @param sock The socket to join a multicast group
 * @param join The address of the multicast group to join
 * @param iface Address of the interface to use.  If NULL is passed, the 
 *              default multicast interface will be used. (PLATFORM Dependent)
 * @param source Source Address to accept transmissions from (non-NULL 
 *               implies Source-Specific Multicast)
 */
KUDA_DECLARE(kuda_status_t) kuda_mcast_join(kuda_socket_t *sock,
                                         kuda_sockaddr_t *join,
                                         kuda_sockaddr_t *iface,
                                         kuda_sockaddr_t *source);

/**
 * Leave a Multicast Group.  All arguments must be the same as
 * kuda_mcast_join.
 * @param sock The socket to leave a multicast group
 * @param addr The address of the multicast group to leave
 * @param iface Address of the interface to use.  If NULL is passed, the 
 *              default multicast interface will be used. (PLATFORM Dependent)
 * @param source Source Address to accept transmissions from (non-NULL 
 *               implies Source-Specific Multicast)
 */
KUDA_DECLARE(kuda_status_t) kuda_mcast_leave(kuda_socket_t *sock,
                                          kuda_sockaddr_t *addr,
                                          kuda_sockaddr_t *iface,
                                          kuda_sockaddr_t *source);

/**
 * Set the Multicast Time to Live (ttl) for a multicast transmission.
 * @param sock The socket to set the multicast ttl
 * @param ttl Time to live to Assign. 0-255, default=1
 * @remark If the TTL is 0, packets will only be seen by sockets on 
 * the local machine, and only when multicast loopback is enabled.
 */
KUDA_DECLARE(kuda_status_t) kuda_mcast_hops(kuda_socket_t *sock,
                                         kuda_byte_t ttl);

/**
 * Toggle IP Multicast Loopback
 * @param sock The socket to set multicast loopback
 * @param opt 0=disable, 1=enable
 */
KUDA_DECLARE(kuda_status_t) kuda_mcast_loopback(kuda_socket_t *sock,
                                             kuda_byte_t opt);


/**
 * Set the Interface to be used for outgoing Multicast Transmissions.
 * @param sock The socket to set the multicast interface on
 * @param iface Address of the interface to use for Multicast
 */
KUDA_DECLARE(kuda_status_t) kuda_mcast_interface(kuda_socket_t *sock,
                                              kuda_sockaddr_t *iface);

/** @} */

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_NETWORK_IO_H */

