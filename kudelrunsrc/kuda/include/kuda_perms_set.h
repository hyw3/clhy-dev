/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_PERMS_SET_H
#define KUDA_PERMS_SET_H

/**
 * @file kuda_perms_set.h
 * @brief KUDA Process Locking Routines
 */

#include "kuda.h"
#include "kuda_pools.h"
#include "kuda_errno.h"
#include "kuda_user.h"
#include "kuda_file_info.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_perms_set Object permission set functions
 * @ingroup KUDA 
 * @{
 */

/** Permission set callback function. */
typedef kuda_status_t (kuda_perms_setfn_t)(void *object, kuda_fileperms_t perms,
                                         kuda_uid_t uid, kuda_gid_t gid);

#define KUDA_PERMS_SET_IMPLEMENT(type) \
    KUDA_DECLARE(kuda_status_t) kuda_##type##_perms_set \
        (void *the##type, kuda_fileperms_t perms, \
         kuda_uid_t uid, kuda_gid_t gid)

#define KUDA_PERMS_SET_ENOTIMPL(type) \
    KUDA_DECLARE(kuda_status_t) kuda_##type##_perms_set \
        (void *the##type, kuda_fileperms_t perms, \
         kuda_uid_t uid, kuda_gid_t gid) \
        { return KUDA_ENOTIMPL ; }

#define KUDA_PERMS_SET_FN(type) kuda_##type##_perms_set


/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_PERMS_SET */
