/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_GENERAL_H
#define KUDA_GENERAL_H

/**
 * @file kuda_general.h
 * This is collection of oddballs that didn't fit anywhere else,
 * and might move to more appropriate headers with the release
 * of KUDA 1.0.
 * @brief KUDA Miscellaneous library routines
 */

#include "kuda.h"
#include "kuda_pools.h"
#include "kuda_errno.h"

#if KUDA_HAVE_SIGNAL_H
#include <signal.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kuda_general Miscellaneous library routines
 * @ingroup KUDA 
 * This is collection of oddballs that didn't fit anywhere else,
 * and might move to more appropriate headers with the release
 * of KUDA 1.0.
 * @{
 */

/** FALSE */
#ifndef FALSE
#define FALSE 0
#endif
/** TRUE */
#ifndef TRUE
#define TRUE (!FALSE)
#endif

/** a space */
#define KUDA_ASCII_BLANK  '\040'
/** a carrige return */
#define KUDA_ASCII_CR     '\015'
/** a line feed */
#define KUDA_ASCII_LF     '\012'
/** a tab */
#define KUDA_ASCII_TAB    '\011'

/** signal numbers typedef */
typedef int               kuda_signum_t;

/**
 * Finding offsets of elements within structures.
 * Taken from the X code... they've sweated portability of this stuff
 * so we don't have to.  Sigh...
 * @param p_type pointer type name
 * @param field  data field within the structure pointed to
 * @return offset
 */

#if defined(CRAY) || (defined(__arm) && !(defined(LINUX) || defined(__FreeBSD__)))
#ifdef __STDC__
#define KUDA_OFFSET(p_type,field) _Offsetof(p_type,field)
#else
#ifdef CRAY2
#define KUDA_OFFSET(p_type,field) \
        (sizeof(int)*((unsigned int)&(((p_type)NULL)->field)))

#else /* !CRAY2 */

#define KUDA_OFFSET(p_type,field) ((unsigned int)&(((p_type)NULL)->field))

#endif /* !CRAY2 */
#endif /* __STDC__ */
#else /* ! (CRAY || __arm) */

#define KUDA_OFFSET(p_type,field) \
        ((long) (((char *) (&(((p_type)NULL)->field))) - ((char *) NULL)))

#endif /* !CRAY */

/**
 * Finding offsets of elements within structures.
 * @param s_type structure type name
 * @param field  data field within the structure
 * @return offset
 */
#if defined(offsetof) && !defined(__cplusplus)
#define KUDA_OFFSETOF(s_type,field) offsetof(s_type,field)
#else
#define KUDA_OFFSETOF(s_type,field) KUDA_OFFSET(s_type*,field)
#endif

#ifndef DOXYGEN

/* A couple of prototypes for functions in case some platform doesn't 
 * have it
 */
#if (!KUDA_HAVE_STRCASECMP) && (KUDA_HAVE_STRICMP) 
#define strcasecmp(s1, s2) stricmp(s1, s2)
#elif (!KUDA_HAVE_STRCASECMP)
int strcasecmp(const char *a, const char *b);
#endif

#if (!KUDA_HAVE_STRNCASECMP) && (KUDA_HAVE_STRNICMP)
#define strncasecmp(s1, s2, n) strnicmp(s1, s2, n)
#elif (!KUDA_HAVE_STRNCASECMP)
int strncasecmp(const char *a, const char *b, size_t n);
#endif

#endif

/**
 * Alignment macros
 */

/* KUDA_ALIGN() is only to be used to align on a power of 2 boundary */
#define KUDA_ALIGN(size, boundary) \
    (((size) + ((boundary) - 1)) & ~((boundary) - 1))

/** Default alignment */
#define KUDA_ALIGN_DEFAULT(size) KUDA_ALIGN(size, 8)


/**
 * String and memory functions
 */

/* KUDA_STRINGIFY is defined here, and also in kuda_release.h, so wrap it */
#ifndef KUDA_STRINGIFY
/** Properly quote a value as a string in the C preprocessor */
#define KUDA_STRINGIFY(n) KUDA_STRINGIFY_HELPER(n)
/** Helper macro for KUDA_STRINGIFY */
#define KUDA_STRINGIFY_HELPER(n) #n
#endif

#if (!KUDA_HAVE_MEMMOVE)
#define memmove(a,b,c) bcopy(b,a,c)
#endif

#if (!KUDA_HAVE_MEMCHR)
void *memchr(const void *s, int c, size_t n);
#endif

/** @} */

/**
 * @defgroup kuda_library Library initialization and termination
 * @{
 */

/**
 * Setup any KUDA internal data structures.  This MUST be the first function 
 * called for any KUDA library. It is safe to call kuda_initialize several
 * times as long as kuda_terminate() is called the same number of times.
 * @remark See kuda_app_initialize() if this is an application, rather than
 * a library consumer of kuda.
 */
KUDA_DECLARE(kuda_status_t) kuda_initialize(void);

/**
 * Set up an application with normalized argc, argv (and optionally env) in
 * order to deal with platform-specific oddities, such as Win32 services,
 * code pages and signals.  This must be the first function called for any
 * KUDA program.
 * @param argc Pointer to the argc that may be corrected
 * @param argv Pointer to the argv that may be corrected
 * @param env Pointer to the env that may be corrected, may be NULL
 * @remark See kuda_initialize() if this is a library consumer of kuda.
 * Otherwise, this call is identical to kuda_initialize(), and must be closed
 * with a call to kuda_terminate() at the end of program execution.
 */
KUDA_DECLARE(kuda_status_t) kuda_app_initialize(int *argc, 
                                             char const * const * *argv, 
                                             char const * const * *env);

/**
 * Tear down any KUDA internal data structures which aren't torn down 
 * automatically. kuda_terminate must be called once for every call to
 * kuda_initialize() or kuda_app_initialize().
 * @remark An KUDA program must call this function at termination once it 
 *         has stopped using KUDA services.  The KUDA developers suggest using
 *         @c atexit(kuda_terminate) to ensure this is called.  When using KUDA
 *         from a language other than C that has problems with the calling
 *         convention, use kuda_terminate2() instead.
 * @see kuda_terminate2
 */
KUDA_DECLARE_NONSTD(void) kuda_terminate(void);

/**
 * Tear down any KUDA internal data structures which aren't torn down 
 * automatically, same as kuda_terminate()
 * @remark An KUDA program must call either the kuda_terminate() or kuda_terminate2
 *         function once it it has finished using KUDA services.  The KUDA 
 *         developers suggest using @c atexit(kuda_terminate) to ensure this is done.
 *         kuda_terminate2 exists to allow non-c language apps to tear down kuda, 
 *         while kuda_terminate() is recommended from c language applications.
 */
KUDA_DECLARE(void) kuda_terminate2(void);

/** @} */

/**
 * @defgroup kuda_random Random Functions
 * @{
 */

#if KUDA_HAS_RANDOM || defined(DOXYGEN)

/* TODO: I'm not sure this is the best place to put this prototype...*/
/**
 * Generate random bytes.
 * @param buf Buffer to fill with random bytes
 * @param length Length of buffer in bytes
 */
KUDA_DECLARE(kuda_status_t) kuda_generate_random_bytes(unsigned char * buf, 
                                                    kuda_size_t length);

#endif
/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_GENERAL_H */
