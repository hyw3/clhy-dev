# Microsoft Developer Studio Project File - Name="prekudaapp" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=prekudaapp - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "prekudaapp.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "prekudaapp.mak" CFG="prekudaapp - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "prekudaapp - Win32 Release" (based on "Win32 (x86) External Target")
!MESSAGE "prekudaapp - Win32 Debug" (based on "Win32 (x86) External Target")
!MESSAGE "prekudaapp - Win32 Release9x" (based on "Win32 (x86) External Target")
!MESSAGE "prekudaapp - Win32 Debug9x" (based on "Win32 (x86) External Target")
!MESSAGE "prekudaapp - x64 Release" (based on "Win32 (x86) External Target")
!MESSAGE "prekudaapp - x64 Debug" (based on "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "prekudaapp - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prekudaapp.exe"
# PROP BASE Bsc_Name "prekudaapp.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "prekudaapp - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prekudaapp.exe"
# PROP BASE Bsc_Name "prekudaapp.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "prekudaapp - Win32 Release9x"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prekudaapp.exe"
# PROP BASE Bsc_Name "prekudaapp.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "prekudaapp - Win32 Debug9x"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prekudaapp.exe"
# PROP BASE Bsc_Name "prekudaapp.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "prekudaapp - x64 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prekudaapp.exe"
# PROP BASE Bsc_Name "prekudaapp.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "prekudaapp - x64 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prekudaapp.exe"
# PROP BASE Bsc_Name "prekudaapp.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "prekudaapp - Win32 Release"
# Name "prekudaapp - Win32 Debug"
# Name "prekudaapp - Win32 Release9x"
# Name "prekudaapp - Win32 Debug9x"
# Name "prekudaapp - x64 Release"
# Name "prekudaapp - x64 Debug"

!IF  "$(CFG)" == "prekudaapp - Win32 Release"

!ELSEIF  "$(CFG)" == "prekudaapp - Win32 Debug"

!ELSEIF  "$(CFG)" == "prekudaapp - Win32 Release9x"

!ELSEIF  "$(CFG)" == "prekudaapp - Win32 Debug9x"

!ELSEIF  "$(CFG)" == "prekudaapp - x64 Release"

!ELSEIF  "$(CFG)" == "prekudaapp - x64 Debug"

!ENDIF 

# End Target
# End Project
