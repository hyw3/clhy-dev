# Microsoft Developer Studio Project File - Name="prelibkudaapp" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=prelibkudaapp - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "prelibkudaapp.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "prelibkudaapp.mak" CFG="prelibkudaapp - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "prelibkudaapp - Win32 Release" (based on "Win32 (x86) External Target")
!MESSAGE "prelibkudaapp - Win32 Debug" (based on "Win32 (x86) External Target")
!MESSAGE "prelibkudaapp - Win32 Release9x" (based on "Win32 (x86) External Target")
!MESSAGE "prelibkudaapp - Win32 Debug9x" (based on "Win32 (x86) External Target")
!MESSAGE "prelibkudaapp - x64 Release" (based on "Win32 (x86) External Target")
!MESSAGE "prelibkudaapp - x64 Debug" (based on "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "prelibkudaapp - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prelibkudaapp.exe"
# PROP BASE Bsc_Name "prelibkudaapp.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "prelibkudaapp - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prelibkudaapp.exe"
# PROP BASE Bsc_Name "prelibkudaapp.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "prelibkudaapp - Win32 Release9x"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prelibkudaapp.exe"
# PROP BASE Bsc_Name "prelibkudaapp.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "prelibkudaapp - Win32 Debug9x"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prelibkudaapp.exe"
# PROP BASE Bsc_Name "prelibkudaapp.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "prelibkudaapp - x64 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prelibkudaapp.exe"
# PROP BASE Bsc_Name "prelibkudaapp.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "prelibkudaapp - x64 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prelibkudaapp.exe"
# PROP BASE Bsc_Name "prelibkudaapp.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "prelibkudaapp - Win32 Release"
# Name "prelibkudaapp - Win32 Debug"
# Name "prelibkudaapp - Win32 Release9x"
# Name "prelibkudaapp - Win32 Debug9x"
# Name "prelibkudaapp - x64 Release"
# Name "prelibkudaapp - x64 Debug"

!IF  "$(CFG)" == "prelibkudaapp - Win32 Release"

!ELSEIF  "$(CFG)" == "prelibkudaapp - Win32 Debug"

!ELSEIF  "$(CFG)" == "prelibkudaapp - Win32 Release9x"

!ELSEIF  "$(CFG)" == "prelibkudaapp - Win32 Debug9x"

!ELSEIF  "$(CFG)" == "prelibkudaapp - x64 Release"

!ELSEIF  "$(CFG)" == "prelibkudaapp - x64 Debug"

!ENDIF 

# End Target
# End Project
