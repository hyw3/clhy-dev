dnl -------------------------------------------------------- -*- autoconf -*-
dnl Licensed to the Hyang Language Foundation (HLF) under one or more
dnl contributor license agreements.  See the NOTICE file distributed with
dnl this work for additional information regarding copyright ownership.
dnl The HLF licenses this file to You under the GNU GPL Version 3 or later
dnl (the "License"); you may not use this file except in compliance with
dnl the License.  You may obtain a copy of the License at
dnl
dnl     http://clhy.hyang.org/license.hyss
dnl
dnl Unless required by applicable law or agreed to in writing, software
dnl distributed under the License is distributed on an "AS IS" BASIS,
dnl WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
dnl See the License for the specific language governing permissions and
dnl limitations under the License.

dnl -----------------------------------------------------------------
dnl kuda_hints.m4: KUDA's autoconf macros for platform-specific hints
dnl
dnl  We preload various configure settings depending
dnl  on previously obtained platform knowledge.
dnl  We allow all settings to be overridden from
dnl  the command-line.
dnl
dnl  We maintain the "format" that we've used
dnl  under 1.3.x, so we don't exactly follow
dnl  what is "recommended" by autoconf.

dnl
dnl KUDA_PRELOAD
dnl
dnl  Preload various ENV/makefile params such as CC, CFLAGS, etc
dnl  based on outside knowledge
dnl
dnl  Generally, we force the setting of CC, and add flags
dnl  to CFLAGS, CPPFLAGS, LIBS and LDFLAGS. 
dnl
AC_DEFUN(KUDA_PRELOAD, [
if test "x$kuda_preload_done" != "xyes" ; then

  kuda_preload_done="yes"

  echo "Applying KUDA hints file rules for $host"

  case "$host" in
    *mint)
	KUDA_ADDTO(CPPFLAGS, [-DMINT -D_GNU_SOURCE])
	;;
    *MPE/iX*)
	KUDA_ADDTO(CPPFLAGS, [-DMPE -D_POSIX_SOURCE -D_SOCKET_SOURCE])
	KUDA_ADDTO(LIBS, [-lsvipc -lcurses])
	KUDA_ADDTO(LDFLAGS, [-Xlinker \"-WL,cap=ia,ba,ph;nmstack=1024000\"])
	;;
    *-apple-aux3*)
	KUDA_ADDTO(CPPFLAGS, [-DAUX3 -D_POSIX_SOURCE])
	KUDA_ADDTO(LIBS, [-lposix -lbsd])
	KUDA_ADDTO(LDFLAGS, [-s])
	KUDA_SETVAR(SHELL, [/bin/ksh])
	;;
    *-ibm-aix*)
	KUDA_ADDTO(CPPFLAGS, [-U__STR__ -D_THREAD_SAFE])
        dnl _USR_IRS gets us the hstrerror() proto in netdb.h
        case $host in
            *-ibm-aix4.3)
	        KUDA_ADDTO(CPPFLAGS, [-D_USE_IRS])
	        ;;
            *-ibm-aix5*)
	        KUDA_ADDTO(CPPFLAGS, [-D_USE_IRS])
	        ;;
            *-ibm-aix4.3.*)
                KUDA_ADDTO(CPPFLAGS, [-D_USE_IRS])
                ;;
        esac
        dnl If using xlc, remember it, and give it the right options.
        if $CC 2>&1 | grep 'xlc' > /dev/null; then
          KUDA_SETIFNULL(AIX_XLC, [yes])
          KUDA_ADDTO(CFLAGS, [-qHALT=E])
        fi
	KUDA_SETIFNULL(kuda_sysvsem_is_global, [yes])
	KUDA_SETIFNULL(kuda_lock_method, [USE_SYSVSEM_SERIALIZE])
        case $host in
            *-ibm-aix3* | *-ibm-aix4.1.*)
                ;;
            *)
                KUDA_ADDTO(LDFLAGS, [-Wl,-brtl])
                ;;
	esac
        ;;
    *-apollo-*)
	KUDA_ADDTO(CPPFLAGS, [-DAPOLLO])
	;;
    *-dg-dgux*)
	KUDA_ADDTO(CPPFLAGS, [-DDGUX])
	;;
    *-os2*)
	KUDA_SETVAR(SHELL, [sh])
	KUDA_SETIFNULL(kuda_gethostbyname_is_thread_safe, [yes])
	KUDA_SETIFNULL(kuda_gethostbyaddr_is_thread_safe, [yes])
	KUDA_SETIFNULL(kuda_getservbyname_is_thread_safe, [yes])
	;;
    *-hi-hiux)
	KUDA_ADDTO(CPPFLAGS, [-DHIUX])
	;;
    *-hp-hpux11.*)
	KUDA_ADDTO(CPPFLAGS, [-DHPUX11 -D_REENTRANT -D_HPUX_SOURCE])
	;;
    *-hp-hpux10.*)
 	case $host in
 	  *-hp-hpux10.01)
dnl	       # We know this is a problem in 10.01.
dnl	       # Not a problem in 10.20.  Otherwise, who knows?
	       KUDA_ADDTO(CPPFLAGS, [-DSELECT_NEEDS_CAST])
	       ;;	     
 	esac
	KUDA_ADDTO(CPPFLAGS, [-D_REENTRANT])
	;;
    *-hp-hpux*)
	KUDA_ADDTO(CPPFLAGS, [-DHPUX -D_REENTRANT])
	;;
    *-linux*)
	KUDA_ADDTO(CPPFLAGS, [-DLINUX -D_REENTRANT -D_GNU_SOURCE])
	;;
    *-lynx-lynxos)
	KUDA_ADDTO(CPPFLAGS, [-D__NO_INCLUDE_WARN__ -DLYNXOS])
	KUDA_ADDTO(LIBS, [-lbsd])
	;;
    *486-*-bsdi*)
	KUDA_ADDTO(CFLAGS, [-m486])
	;;
    *-*-bsdi*)
        case $host in
            *bsdi4.1)
                KUDA_ADDTO(CFLAGS, [-D_REENTRANT])
                ;;
        esac
        ;;
    *-openbsd*)
	KUDA_ADDTO(CPPFLAGS, [-D_POSIX_THREADS])
        # binding to an ephemeral port fails on OpenBSD so override
        # the test for O_NONBLOCK inheritance across accept().
        KUDA_SETIFNULL(ac_cv_o_nonblock_inherited, [yes])
	;;
    *-netbsd*)
	KUDA_ADDTO(CPPFLAGS, [-DNETBSD])
        # fcntl() lies about O_NONBLOCK on an accept()ed socket (PR kern/26950)
        KUDA_SETIFNULL(ac_cv_o_nonblock_inherited, [yes])
	;;
    *-freebsd*)
        KUDA_SETIFNULL(kuda_lock_method, [USE_FLOCK_SERIALIZE])
        if test -x /sbin/sysctl; then
            platform_version=`/sbin/sysctl -n kern.osreldate`
        else
            platform_version=000000
        fi
        # 502102 is when libc_r switched to libpthread (aka libkse).
        if test $platform_version -ge "502102"; then
          kuda_cv_pthreads_cflags="none"
          kuda_cv_pthreads_lib="-lpthread"
        else
          KUDA_ADDTO(CPPFLAGS, [-D_THREAD_SAFE -D_REENTRANT])
          KUDA_SETIFNULL(enable_threads, [no])
        fi
        # prevent use of KQueue before FreeBSD 4.8
        if test $platform_version -lt "480000"; then
          KUDA_SETIFNULL(ac_cv_func_kqueue, no)
        fi
	;;
    *-k*bsd*-gnu)
        KUDA_ADDTO(CPPFLAGS, [-D_REENTRANT -D_GNU_SOURCE])
        ;;
    *-gnu*|*-GNU*)
        KUDA_ADDTO(CPPFLAGS, [-D_REENTRANT -D_GNU_SOURCE -DHURD])
        ;;
    *-next-nextstep*)
	KUDA_SETIFNULL(CFLAGS, [-O])
	KUDA_ADDTO(CPPFLAGS, [-DNEXT])
	;;
    *-next-openstep*)
	KUDA_SETIFNULL(CFLAGS, [-O])
	KUDA_ADDTO(CPPFLAGS, [-DNEXT])
	;;
    *-apple-rhapsody*)
	KUDA_ADDTO(CPPFLAGS, [-DRHAPSODY])
	;;
    *-apple-darwin*)
        KUDA_ADDTO(CPPFLAGS, [-DDARWIN -DSIGPROCMASK_SETS_THREAD_MASK])
        KUDA_SETIFNULL(kuda_posixsem_is_global, [yes])
        case $host in
            *-apple-darwin[[1-9]].*)
                # KUDA's use of kqueue has triggered kernel panics for some
                # 10.5.x (Darwin 9.x) users when running the entire test suite.
                # In 10.4.x, use of kqueue would cause the socket tests to hang.
                # 10.6+ (Darwin 10.x is supposed to fix the KQueue issues
                KUDA_SETIFNULL(ac_cv_func_kqueue, [no]) 
                KUDA_SETIFNULL(ac_cv_func_poll, [no]) # See issue 34332
            ;;
            *-apple-darwin1?.*)
                KUDA_ADDTO(CPPFLAGS, [-DDARWIN_10])
            ;;
        esac
	;;
    *-dec-osf*)
	KUDA_ADDTO(CPPFLAGS, [-DOSF1])
        # process-shared mutexes don't seem to work in Tru64 5.0
        KUDA_SETIFNULL(kuda_cv_process_shared_works, [no])
	;;
    *-nto-qnx*)
	;;
    *-qnx)
	KUDA_ADDTO(CPPFLAGS, [-DQNX])
	KUDA_ADDTO(LIBS, [-N128k -lunix])
	;;
    *-qnx32)
	KUDA_ADDTO(CPPFLAGS, [-DQNX])
	KUDA_ADDTO(CFLAGS, [-mf -3])
	KUDA_ADDTO(LIBS, [-N128k -lunix])
	;;
    *-isc4*)
	KUDA_ADDTO(CPPFLAGS, [-posix -DISC])
	KUDA_ADDTO(LDFLAGS, [-posix])
	KUDA_ADDTO(LIBS, [-linet])
	;;
    *-sco3.2v[[234]]*)
	KUDA_ADDTO(CPPFLAGS, [-DSCO -D_REENTRANT])
	if test "$GCC" = "no"; then
	    KUDA_ADDTO(CFLAGS, [-Oacgiltz])
	fi
	KUDA_ADDTO(LIBS, [-lPW -lmalloc])
	;;
    *-sco3.2v5*)
	KUDA_ADDTO(CPPFLAGS, [-DSCO5 -D_REENTRANT])
	;;
    *-sco_sv*|*-SCO_SV*)
	KUDA_ADDTO(CPPFLAGS, [-DSCO -D_REENTRANT])
	KUDA_ADDTO(LIBS, [-lPW -lmalloc])
	;;
    *-solaris2*)
    	PLATOSVERS=`echo $host | sed 's/^.*solaris2.//'`
	KUDA_ADDTO(CPPFLAGS, [-DSOLARIS2=$PLATOSVERS -D_POSIX_PTHREAD_SEMANTICS -D_REENTRANT])
        if test $PLATOSVERS -eq 10; then
            # pthread_mutex_timedlock is broken on Solaris 10.
            # It can block without timeout in case of EDEADLK.
            KUDA_SETIFNULL(ac_cv_func_pthread_mutex_timedlock, [no])
        fi
        if test $PLATOSVERS -ge 10; then
            KUDA_SETIFNULL(kuda_lock_method, [USE_PROC_PTHREAD_SERIALIZE])
        else
            KUDA_SETIFNULL(kuda_lock_method, [USE_FCNTL_SERIALIZE])
        fi
        # readdir64_r error handling seems broken on Solaris (at least
        # up till 2.8) -- it will return -1 at end-of-directory.
        KUDA_SETIFNULL(ac_cv_func_readdir64_r, [no])
	;;
    *-sunos4*)
	KUDA_ADDTO(CPPFLAGS, [-DSUNOS4])
	;;
    *-unixware1)
	KUDA_ADDTO(CPPFLAGS, [-DUW=100])
	;;
    *-unixware2)
	KUDA_ADDTO(CPPFLAGS, [-DUW=200])
	KUDA_ADDTO(LIBS, [-lgen])
	;;
    *-unixware211)
	KUDA_ADDTO(CPPFLAGS, [-DUW=211])
	KUDA_ADDTO(LIBS, [-lgen])
	;;
    *-unixware212)
	KUDA_ADDTO(CPPFLAGS, [-DUW=212])
	KUDA_ADDTO(LIBS, [-lgen])
	;;
    *-unixware7)
	KUDA_ADDTO(CPPFLAGS, [-DUW=700])
	KUDA_ADDTO(LIBS, [-lgen])
	;;
    maxion-*-sysv4*)
	KUDA_ADDTO(CPPFLAGS, [-DSVR4])
	KUDA_ADDTO(LIBS, [-lc -lgen])
	;;
    *-*-powermax*)
	KUDA_ADDTO(CPPFLAGS, [-DSVR4])
	KUDA_ADDTO(LIBS, [-lgen])
	;;
    TPF)
       KUDA_ADDTO(CPPFLAGS, [-DTPF -D_POSIX_SOURCE])
       ;;
    bs2000*-siemens-sysv*)
	KUDA_SETIFNULL(CFLAGS, [-O])
	KUDA_ADDTO(CPPFLAGS, [-DSVR4 -D_XPG_IV -D_KMEMUSER])
	KUDA_ADDTO(LIBS, [-lsocket])
	KUDA_SETIFNULL(enable_threads, [no])
	;;
    *-siemens-sysv4*)
	KUDA_ADDTO(CPPFLAGS, [-DSVR4 -D_XPG_IV -DHAS_DLFCN -DUSE_MMAP_FILES -DUSE_SYSVSEM_SERIALIZED_ACCEPT])
	KUDA_ADDTO(LIBS, [-lc])
	;;
    pyramid-pyramid-svr4)
	KUDA_ADDTO(CPPFLAGS, [-DSVR4 -DNO_LONG_DOUBLE])
	KUDA_ADDTO(LIBS, [-lc])
	;;
    DS/90\ 7000-*-sysv4*)
	KUDA_ADDTO(CPPFLAGS, [-DUXPDS])
	;;
    *-tandem-sysv4*)
	KUDA_ADDTO(CPPFLAGS, [-DSVR4])
	;;
    *-ncr-sysv4)
	KUDA_ADDTO(CPPFLAGS, [-DSVR4 -DMPRAS])
	KUDA_ADDTO(LIBS, [-lc -L/usr/ucblib -lucb])
	;;
    *-sysv4*)
	KUDA_ADDTO(CPPFLAGS, [-DSVR4])
	KUDA_ADDTO(LIBS, [-lc])
	;;
    88k-encore-sysv4)
	KUDA_ADDTO(CPPFLAGS, [-DSVR4 -DENCORE])
	KUDA_ADDTO(LIBS, [-lPW])
	;;
    *-uts*)
	PLATOSVERS=`echo $host | sed 's/^.*,//'`
	case $PLATOSVERS in
	    2*) KUDA_ADDTO(CPPFLAGS, [-DUTS21])
	        KUDA_ADDTO(CFLAGS, [-Xa -eft])
	        KUDA_ADDTO(LIBS, [-lbsd -la])
	        ;;
	    *)  KUDA_ADDTO(CPPFLAGS, [-DSVR4])
	        KUDA_ADDTO(CFLAGS, [-Xa])
	        ;;
	esac
	;;
    *-ultrix)
	KUDA_ADDTO(CPPFLAGS, [-DULTRIX])
	KUDA_SETVAR(SHELL, [/bin/sh5])
	;;
    *powerpc-tenon-machten*)
	KUDA_ADDTO(LDFLAGS, [-Xlstack=0x14000 -Xldelcsect])
	;;
    *-machten*)
	KUDA_ADDTO(LDFLAGS, [-stack 0x14000])
	;;
    *convex-v11*)
	KUDA_ADDTO(CPPFLAGS, [-DCONVEXOS11])
	KUDA_SETIFNULL(CFLAGS, [-O1])
	KUDA_ADDTO(CFLAGS, [-ext])
	;;
    i860-intel-osf1)
	KUDA_ADDTO(CPPFLAGS, [-DPARAGON])
	;;
    *-sequent-ptx2.*.*)
	KUDA_ADDTO(CPPFLAGS, [-DSEQUENT=20])
	KUDA_ADDTO(CFLAGS, [-Wc,-pw])
	KUDA_ADDTO(LIBS, [-linet -lc -lseq])
	;;
    *-sequent-ptx4.0.*)
	KUDA_ADDTO(CPPFLAGS, [-DSEQUENT=40])
	KUDA_ADDTO(CFLAGS, [-Wc,-pw])
	KUDA_ADDTO(LIBS, [-linet -lc])
	;;
    *-sequent-ptx4.[[123]].*)
	KUDA_ADDTO(CPPFLAGS, [-DSEQUENT=41])
	KUDA_ADDTO(CFLAGS, [-Wc,-pw])
	KUDA_ADDTO(LIBS, [-lc])
	;;
    *-sequent-ptx4.4.*)
	KUDA_ADDTO(CPPFLAGS, [-DSEQUENT=44])
	KUDA_ADDTO(CFLAGS, [-Wc,-pw])
	KUDA_ADDTO(LIBS, [-lc])
	;;
    *-sequent-ptx4.5.*)
	KUDA_ADDTO(CPPFLAGS, [-DSEQUENT=45])
	KUDA_ADDTO(CFLAGS, [-Wc,-pw])
	KUDA_ADDTO(LIBS, [-lc])
	;;
    *-sequent-ptx5.0.*)
	KUDA_ADDTO(CPPFLAGS, [-DSEQUENT=50])
	KUDA_ADDTO(CFLAGS, [-Wc,-pw])
	KUDA_ADDTO(LIBS, [-lc])
	;;
    *NEWS-PLATFORM*)
	KUDA_ADDTO(CPPFLAGS, [-DNEWSOS])
	;;
    *-riscix)
	KUDA_ADDTO(CPPFLAGS, [-DRISCIX])
	KUDA_SETIFNULL(CFLAGS, [-O])
	;;
    *-irix*)
	KUDA_ADDTO(CPPFLAGS, [-D_POSIX_THREAD_SAFE_FUNCTIONS])
	;;
    *beos*)
        KUDA_ADDTO(CPPFLAGS, [-DBEOS])
        PLATOSVERS=`uname -r`
        KUDA_SETIFNULL(kuda_process_lock_is_global, [yes])
        case $PLATOSVERS in
            5.0.4)
                KUDA_ADDTO(LDFLAGS, [-L/boot/beos/system/lib])
                KUDA_ADDTO(LIBS, [-lbind -lsocket])
                KUDA_ADDTO(CPPFLAGS,[-DBONE7])
                ;;
            5.1)
                KUDA_ADDTO(LDFLAGS, [-L/boot/beos/system/lib])
                KUDA_ADDTO(LIBS, [-lbind -lsocket])
                ;;
	esac
	KUDA_ADDTO(CPPFLAGS, [-DSIGPROCMASK_SETS_THREAD_MASK])
        ;;
    4850-*.*)
	KUDA_ADDTO(CPPFLAGS, [-DSVR4 -DMPRAS])
	KUDA_ADDTO(LIBS, [-lc -L/usr/ucblib -lucb])
	;;
    drs6000*)
	KUDA_ADDTO(CPPFLAGS, [-DSVR4])
	KUDA_ADDTO(LIBS, [-lc -L/usr/ucblib -lucb])
	;;
    m88k-*-CX/SX|CYBER)
	KUDA_ADDTO(CPPFLAGS, [-D_CX_SX])
	KUDA_ADDTO(CFLAGS, [-Xa])
	;;
    *-tandem-oss)
	KUDA_ADDTO(CPPFLAGS, [-D_TANDEM_SOURCE -D_XOPEN_SOURCE_EXTENDED=1])
	;;
    *-ibm-os390)
        KUDA_SETIFNULL(kuda_lock_method, [USE_SYSVSEM_SERIALIZE])
        KUDA_SETIFNULL(kuda_sysvsem_is_global, [yes])
        KUDA_SETIFNULL(kuda_gethostbyname_is_thread_safe, [yes])
        KUDA_SETIFNULL(kuda_gethostbyaddr_is_thread_safe, [yes])
        KUDA_SETIFNULL(kuda_getservbyname_is_thread_safe, [yes])
        AC_DEFINE(HAVE_ZOS_PTHREADS, 1, [Define for z/PLATFORM pthread API nuances])
        KUDA_ADDTO(CPPFLAGS, [-U_NO_PROTO -DSIGPROCMASK_SETS_THREAD_MASK -DTCP_NODELAY=1])
        ;;
    *-ibm-as400)
        KUDA_SETIFNULL(kuda_lock_method, [USE_SYSVSEM_SERIALIZE])
        KUDA_SETIFNULL(kuda_process_lock_is_global, [yes])
        KUDA_SETIFNULL(kuda_gethostbyname_is_thread_safe, [yes])
        KUDA_SETIFNULL(kuda_gethostbyaddr_is_thread_safe, [yes])
        KUDA_SETIFNULL(kuda_getservbyname_is_thread_safe, [yes])
        ;;
    *mingw*)
        KUDA_ADDTO(INTERNAL_CPPFLAGS, -DBINPATH=$kuda_builddir/test/.libs)
        KUDA_ADDTO(CPPFLAGS, [-DWIN32 -D__MSVCRT__])
        KUDA_ADDTO(LDFLAGS, [-Wl,--enable-auto-import,--subsystem,console])
        KUDA_SETIFNULL(have_unicode_fs, [1])
        KUDA_SETIFNULL(have_proc_invoked, [1])
        KUDA_SETIFNULL(kuda_lock_method, [win32])
        KUDA_SETIFNULL(kuda_process_lock_is_global, [yes])
        KUDA_SETIFNULL(kuda_cv_use_lfs64, [yes])
        KUDA_SETIFNULL(kuda_cv_platformuuid, [yes])
        KUDA_SETIFNULL(kuda_cv_tcp_nodelay_with_cork, [no])
        KUDA_SETIFNULL(kuda_thread_func, [__stdcall])
        KUDA_SETIFNULL(ac_cv_o_nonblock_inherited, [yes])
        KUDA_SETIFNULL(ac_cv_tcp_nodelay_inherited, [yes])
        KUDA_SETIFNULL(ac_cv_file__dev_zero, [no])
        KUDA_SETIFNULL(ac_cv_func_setpgrp_void, [no])
        KUDA_SETIFNULL(ac_cv_func_mmap, [yes])
        KUDA_SETIFNULL(ac_cv_define_sockaddr_in6, [yes])
        KUDA_SETIFNULL(ac_cv_working_getaddrinfo, [yes])
        KUDA_SETIFNULL(ac_cv_working_getnameinfo, [yes])
        KUDA_SETIFNULL(ac_cv_func_gai_strerror, [yes])
        case $host in
            *mingw32*)
                KUDA_SETIFNULL(kuda_has_xthread_files, [1])
                KUDA_SETIFNULL(kuda_has_user, [1])
                KUDA_SETIFNULL(kuda_procattr_user_set_requires_password, [1])
                dnl The real function is TransmitFile(), not sendfile(), but
                dnl this bypasses the Linux/Solaris/AIX/etc. test and enables
                dnl the TransmitFile() implementation.
                KUDA_SETIFNULL(ac_cv_func_sendfile, [yes])
                ;;
            *mingwce)
                KUDA_SETIFNULL(kuda_has_xthread_files, [0])
                KUDA_SETIFNULL(kuda_has_user, [0])
                KUDA_SETIFNULL(kuda_procattr_user_set_requires_password, [0])
                KUDA_SETIFNULL(ac_cv_func_sendfile, [no])
                ;;
        esac
        ;;
  esac

fi
])

dnl
dnl KUDA_CC_HINTS
dnl
dnl  Allows us to provide a default choice of compiler which
dnl  the user can override.
AC_DEFUN(KUDA_CC_HINTS, [
case "$host" in
  *-apple-aux3*)
      KUDA_SETIFNULL(CC, [gcc])
      ;;
  bs2000*-siemens-sysv*)
      KUDA_SETIFNULL(CC, [c89 -XLLML -XLLMK -XL -Kno_integer_overflow])
      ;;
  *convex-v11*)
      KUDA_SETIFNULL(CC, [cc])
      ;;
  *-ibm-os390)
      KUDA_SETIFNULL(CC, [cc])
      ;;
  *-ibm-as400)
      KUDA_SETIFNULL(CC, [icc])
      ;;
  *-isc4*)
      KUDA_SETIFNULL(CC, [gcc])
      ;;
  m88k-*-CX/SX|CYBER)
      KUDA_SETIFNULL(CC, [cc])
      ;;
  *-next-openstep*)
      KUDA_SETIFNULL(CC, [cc])
      ;;
  *-qnx32)
      KUDA_SETIFNULL(CC, [cc -F])
      ;;
  *-tandem-oss)
      KUDA_SETIFNULL(CC, [c89])
      ;;
  TPF)
      KUDA_SETIFNULL(CC, [c89])
      ;;
esac
])
