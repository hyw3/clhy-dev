/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_arch_networkio.h"
#include "kuda_poll.h"
#include "kuda_errno.h"
#include "kuda_clservices.h"

/* The only case where we don't use wait_for_io_or_timeout is on
 * pre-BONE BeOS, so this check should be sufficient and simpler */
#if !defined(BEOS_R5) && !defined(OS2) && KUDA_FILES_AS_SOCKETS
#define USE_WAIT_FOR_IO
#endif

#ifdef USE_WAIT_FOR_IO

#ifdef WAITIO_USES_POLL

#ifdef HAVE_POLL_H
#include <poll.h>
#endif
#ifdef HAVE_SYS_POLL_H
#include <sys/poll.h>
#endif

kuda_status_t kuda_wait_for_io_or_timeout(kuda_file_t *f, kuda_socket_t *s,
                                        int for_read)
{
    struct pollfd pfd;
    int rc, timeout;

    timeout    = f        ? f->timeout / 1000 : s->timeout / 1000;
    pfd.fd     = f        ? f->filedes        : s->socketdes;
    pfd.events = for_read ? POLLIN            : POLLOUT;

    do {
        rc = poll(&pfd, 1, timeout);
    } while (rc == -1 && errno == EINTR);
    if (rc == 0) {
        return KUDA_TIMEUP;
    }
    else if (rc > 0) {
        return KUDA_SUCCESS;
    }
    else {
        return errno;
    }
}

#else /* !WAITIO_USES_POLL */

kuda_status_t kuda_wait_for_io_or_timeout(kuda_file_t *f, kuda_socket_t *s,
                                        int for_read)
{
    kuda_interval_time_t timeout;
    kuda_pollfd_t pfd;
    int type = for_read ? KUDA_POLLIN : KUDA_POLLOUT;
    kuda_pollset_t *pollset;
    kuda_status_t status;

    /* TODO - timeout should be less each time through this loop */
    if (f) {
        pfd.desc_type = KUDA_POLL_FILE;
        pfd.desc.f = f;

        pollset = f->pollset;
        if (pollset == NULL) {
            status = kuda_pollset_create(&(f->pollset), 1, f->pool, 0);
            if (status != KUDA_SUCCESS) {
                return status;
            }
            pollset = f->pollset;
        }
        timeout = f->timeout;
    }
    else {
        pfd.desc_type = KUDA_POLL_SOCKET;
        pfd.desc.s = s;

        pollset = s->pollset;
        timeout = s->timeout;
    }
    pfd.reqevents = type;

    /* Remove the object if it was in the pollset, then add in the new
     * object with the correct reqevents value. Ignore the status result
     * on the remove, because it might not be in there (yet).
     */
    (void) kuda_pollset_remove(pollset, &pfd);

    /* ### check status code */
    (void) kuda_pollset_add(pollset, &pfd);

    do {
        int numdesc;
        const kuda_pollfd_t *pdesc;

        status = kuda_pollset_poll(pollset, timeout, &numdesc, &pdesc);

        if (numdesc == 1 && (pdesc[0].rtnevents & type) != 0) {
            return KUDA_SUCCESS;
        }
    } while (KUDA_STATUS_IS_EINTR(status));

    return status;
}
#endif /* WAITIO_USES_POLL */

#endif /* USE_WAIT_FOR_IO */
