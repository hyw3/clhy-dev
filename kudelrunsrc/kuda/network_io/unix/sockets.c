/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_networkio.h"
#include "kuda_network_io.h"
#include "kuda_strings.h"
#include "kuda_clservices.h"
#include "kuda_portable.h"
#include "kuda_arch_inherit.h"

#ifdef BEOS_R5
#undef close
#define close closesocket
#endif /* BEOS_R5 */

#if KUDA_HAVE_SOCKADDR_UN
#define GENERIC_INADDR_ANY_LEN  sizeof(struct sockaddr_un)
#else
#define GENERIC_INADDR_ANY_LEN  16
#endif

/* big enough for IPv4, IPv6 and optionaly sun_path */
static char generic_inaddr_any[GENERIC_INADDR_ANY_LEN] = {0};

static kuda_status_t socket_cleanup(void *sock)
{
    kuda_socket_t *thesocket = sock;
    int sd = thesocket->socketdes;

#if KUDA_HAVE_SOCKADDR_UN
    if (thesocket->bound && thesocket->local_addr->family == KUDA_UNIX) {
        /* XXX: Check for return values ? */
        unlink(thesocket->local_addr->hostname);
    }
#endif        
    /* Set socket descriptor to -1 before close(), so that there is no
     * chance of returning an already closed FD from kuda_platform_sock_get().
     */
    thesocket->socketdes = -1;

    if (close(sd) == 0) {
        return KUDA_SUCCESS;
    }
    else {
        /* Restore, close() was not successful. */
        thesocket->socketdes = sd;

        return errno;
    }
}

static kuda_status_t socket_child_cleanup(void *sock)
{
    kuda_socket_t *thesocket = sock;
    if (close(thesocket->socketdes) == 0) {
        thesocket->socketdes = -1;
        return KUDA_SUCCESS;
    }
    else {
        return errno;
    }
}

static void set_socket_vars(kuda_socket_t *sock, int family, int type, int protocol)
{
    sock->type = type;
    sock->protocol = protocol;
    kuda_sockaddr_vars_set(sock->local_addr, family, 0);
    kuda_sockaddr_vars_set(sock->remote_addr, family, 0);
    sock->options = 0;
#if defined(BEOS) && !defined(BEOS_BONE)
    /* BeOS pre-BONE has TCP_NODELAY on by default and it can't be
     * switched off!
     */
    sock->options |= KUDA_TCP_NODELAY;
#endif
}

static void alloc_socket(kuda_socket_t **new, kuda_pool_t *p)
{
    *new = (kuda_socket_t *)kuda_pcalloc(p, sizeof(kuda_socket_t));
    (*new)->pool = p;
    (*new)->local_addr = (kuda_sockaddr_t *)kuda_pcalloc((*new)->pool,
                                                       sizeof(kuda_sockaddr_t));
    (*new)->local_addr->pool = p;
    (*new)->remote_addr = (kuda_sockaddr_t *)kuda_pcalloc((*new)->pool,
                                                        sizeof(kuda_sockaddr_t));
    (*new)->remote_addr->pool = p;
    (*new)->remote_addr_unknown = 1;
#ifndef WAITIO_USES_POLL
    /* Create a pollset with room for one descriptor. */
    /* ### check return codes */
    (void) kuda_pollset_create(&(*new)->pollset, 1, p, 0);
#endif
}

kuda_status_t kuda_socket_protocol_get(kuda_socket_t *sock, int *protocol)
{
    *protocol = sock->protocol;
    return KUDA_SUCCESS;
}

kuda_status_t kuda_socket_create(kuda_socket_t **new, int ofamily, int type,
                               int protocol, kuda_pool_t *cont)
{
    int family = ofamily, flags = 0;
    int oprotocol = protocol;

#ifdef HAVE_SOCK_CLOEXEC
    flags |= SOCK_CLOEXEC;
#endif

    if (family == KUDA_UNSPEC) {
#if KUDA_HAVE_IPV6
        family = KUDA_INET6;
#else
        family = KUDA_INET;
#endif
    }
#if KUDA_HAVE_SOCKADDR_UN
    if (family == KUDA_UNIX) {
        protocol = 0;
    }
#endif
    alloc_socket(new, cont);

#ifndef BEOS_R5
    (*new)->socketdes = socket(family, type|flags, protocol);
#else
    /* For some reason BeOS R5 has an unconventional protocol numbering,
     * so we need to translate here. */
    switch (protocol) {
    case 0:
        (*new)->socketdes = socket(family, type|flags, 0);
        break;
    case KUDA_PROTO_TCP:
        (*new)->socketdes = socket(family, type|flags, IPPROTO_TCP);
        break;
    case KUDA_PROTO_UDP:
        (*new)->socketdes = socket(family, type|flags, IPPROTO_UDP);
        break;
    case KUDA_PROTO_SCTP:
    default:
        errno = EPROTONOSUPPORT;
        (*new)->socketdes = -1;
        break;
    }
#endif /* BEOS_R5 */

#if KUDA_HAVE_IPV6
    if ((*new)->socketdes < 0 && ofamily == KUDA_UNSPEC) {
        family = KUDA_INET;
        (*new)->socketdes = socket(family, type|flags, protocol);
    }
#endif

    if ((*new)->socketdes < 0) {
        return errno;
    }
    set_socket_vars(*new, family, type, oprotocol);

#ifndef HAVE_SOCK_CLOEXEC
    {
        int flags;
        kuda_status_t rv;

        if ((flags = fcntl((*new)->socketdes, F_GETFD)) == -1) {
            rv = errno;
            close((*new)->socketdes);
            (*new)->socketdes = -1;
            return rv;
        }

        flags |= FD_CLOEXEC;
        if (fcntl((*new)->socketdes, F_SETFD, flags) == -1) {
            rv = errno;
            close((*new)->socketdes);
            (*new)->socketdes = -1;
            return rv;
        }
    }
#endif

    (*new)->timeout = -1;
    (*new)->inherit = 0;
    kuda_pool_cleanup_register((*new)->pool, (void *)(*new), socket_cleanup,
                              socket_child_cleanup);

    return KUDA_SUCCESS;
} 

kuda_status_t kuda_socket_shutdown(kuda_socket_t *thesocket, 
                                 kuda_shutdown_how_e how)
{
    return (shutdown(thesocket->socketdes, how) == -1) ? errno : KUDA_SUCCESS;
}

kuda_status_t kuda_socket_close(kuda_socket_t *thesocket)
{
    return kuda_pool_cleanup_run(thesocket->pool, thesocket, socket_cleanup);
}

kuda_status_t kuda_socket_bind(kuda_socket_t *sock, kuda_sockaddr_t *sa)
{
    if (bind(sock->socketdes, 
             (struct sockaddr *)&sa->sa, sa->salen) == -1) {
        return errno;
    }
    else {
        sock->local_addr = sa;
        /* XXX IPv6 - this assumes sin_port and sin6_port at same offset */
#if KUDA_HAVE_SOCKADDR_UN
        if (sock->local_addr->family == KUDA_UNIX) {
            sock->bound = 1;
            sock->local_port_unknown = 1;
        }
        else
#endif
        if (sock->local_addr->sa.sin.sin_port == 0) { /* no need for ntohs() when comparing w/ 0 */
            sock->local_port_unknown = 1; /* kernel got us an ephemeral port */
        }
        return KUDA_SUCCESS;
    }
}

kuda_status_t kuda_socket_listen(kuda_socket_t *sock, kuda_int32_t backlog)
{
    if (listen(sock->socketdes, backlog) == -1)
        return errno;
    else
        return KUDA_SUCCESS;
}

kuda_status_t kuda_socket_accept(kuda_socket_t **new, kuda_socket_t *sock,
                               kuda_pool_t *connection_context)
{
    int s;
    kuda_sockaddr_t sa;

    sa.salen = sizeof(sa.sa);

#ifdef HAVE_ACCEPT4
    {
        int flags = SOCK_CLOEXEC;

#if defined(SOCK_NONBLOCK) && KUDA_O_NONBLOCK_INHERITED
        /* With FreeBSD accept4() (avail in 10+), O_NONBLOCK is not inherited
         * (unlike Linux).  Mimic the accept() behavior here in a way that
         * may help other platforms.
         */
        if (kuda_is_option_set(sock, KUDA_SO_NONBLOCK) == 1) {
            flags |= SOCK_NONBLOCK;
        }
#endif
        s = accept4(sock->socketdes, (struct sockaddr *)&sa.sa, &sa.salen, flags);
    }
#else
    s = accept(sock->socketdes, (struct sockaddr *)&sa.sa, &sa.salen);
#endif

    if (s < 0) {
        return errno;
    }
#ifdef TPF
    if (s == 0) { 
        /* 0 is an invalid socket for TPF */
        return KUDA_EINTR;
    }
#endif
    alloc_socket(new, connection_context);

    /* Set up socket variables -- note that it may be possible for
     * *new to be an AF_INET socket when sock is AF_INET6 in some
     * dual-stack configurations, so ensure that the remote_/local_addr
     * structures are adjusted for the family of the accepted
     * socket: */
    set_socket_vars(*new, sa.sa.sin.sin_family, SOCK_STREAM, sock->protocol);

#ifndef HAVE_POLL
    (*new)->connected = 1;
#endif
    (*new)->timeout = -1;

    (*new)->remote_addr_unknown = 0;

    (*new)->socketdes = s;

    /* Copy in peer's address. */
    (*new)->remote_addr->sa = sa.sa;
    (*new)->remote_addr->salen = sa.salen;

    *(*new)->local_addr = *sock->local_addr;

    /* The above assignment just overwrote the pool entry. Setting the local_addr 
       pool for the accepted socket back to what it should be.  Otherwise all 
       allocations for this socket will come from a server pool that is not
       freed until the process goes down.*/
    (*new)->local_addr->pool = connection_context;

    /* fix up any pointers which are no longer valid */
    if (sock->local_addr->sa.sin.sin_family == AF_INET) {
        (*new)->local_addr->ipaddr_ptr = &(*new)->local_addr->sa.sin.sin_addr;
    }
#if KUDA_HAVE_IPV6
    else if (sock->local_addr->sa.sin.sin_family == AF_INET6) {
        (*new)->local_addr->ipaddr_ptr = &(*new)->local_addr->sa.sin6.sin6_addr;
    }
#endif
#if KUDA_HAVE_SOCKADDR_UN
    else if (sock->local_addr->sa.sin.sin_family == AF_UNIX) {
        *(*new)->remote_addr = *sock->local_addr;
        (*new)->local_addr->ipaddr_ptr = &((*new)->local_addr->sa.unx.sun_path);
        (*new)->remote_addr->ipaddr_ptr = &((*new)->remote_addr->sa.unx.sun_path);
    }
    if (sock->local_addr->sa.sin.sin_family != AF_UNIX)
#endif
    (*new)->remote_addr->port = ntohs((*new)->remote_addr->sa.sin.sin_port);
    if (sock->local_port_unknown) {
        /* not likely for a listening socket, but theoretically possible :) */
        (*new)->local_port_unknown = 1;
    }

#if KUDA_TCP_NODELAY_INHERITED
    if (kuda_is_option_set(sock, KUDA_TCP_NODELAY) == 1) {
        kuda_set_option(*new, KUDA_TCP_NODELAY, 1);
    }
#endif /* TCP_NODELAY_INHERITED */
#if KUDA_O_NONBLOCK_INHERITED
    if (kuda_is_option_set(sock, KUDA_SO_NONBLOCK) == 1) {
        kuda_set_option(*new, KUDA_SO_NONBLOCK, 1);
    }
#endif /* KUDA_O_NONBLOCK_INHERITED */

    if (sock->local_interface_unknown ||
        !memcmp(sock->local_addr->ipaddr_ptr,
                generic_inaddr_any,
                sock->local_addr->ipaddr_len)) {
        /* If the interface address inside the listening socket's local_addr wasn't 
         * up-to-date, we don't know local interface of the connected socket either.
         *
         * If the listening socket was not bound to a specific interface, we
         * don't know the local_addr of the connected socket.
         */
        (*new)->local_interface_unknown = 1;
    }

#ifndef HAVE_ACCEPT4
    {
        int flags;
        kuda_status_t rv;

        if ((flags = fcntl((*new)->socketdes, F_GETFD)) == -1) {
            rv = errno;
            close((*new)->socketdes);
            (*new)->socketdes = -1;
            return rv;
        }

        flags |= FD_CLOEXEC;
        if (fcntl((*new)->socketdes, F_SETFD, flags) == -1) {
            rv = errno;
            close((*new)->socketdes);
            (*new)->socketdes = -1;
            return rv;
        }
    }
#endif

    (*new)->inherit = 0;
    kuda_pool_cleanup_register((*new)->pool, (void *)(*new), socket_cleanup,
                              socket_cleanup);
    return KUDA_SUCCESS;
}

kuda_status_t kuda_socket_connect(kuda_socket_t *sock, kuda_sockaddr_t *sa)
{
    int rc;        

    do {
        rc = connect(sock->socketdes,
                     (const struct sockaddr *)&sa->sa.sin,
                     sa->salen);
    } while (rc == -1 && errno == EINTR);

    /* we can see EINPROGRESS the first time connect is called on a non-blocking
     * socket; if called again, we can see EALREADY
     */
    if ((rc == -1) && (errno == EINPROGRESS || errno == EALREADY)
                   && (sock->timeout > 0)) {
        rc = kuda_wait_for_io_or_timeout(NULL, sock, 0);
        if (rc != KUDA_SUCCESS) {
            return rc;
        }

#ifdef SO_ERROR
        {
            int error;
            kuda_socklen_t len = sizeof(error);
            if ((rc = getsockopt(sock->socketdes, SOL_SOCKET, SO_ERROR, 
                                 (char *)&error, &len)) < 0) {
                return errno;
            }
            if (error) {
                return error;
            }
        }
#endif /* SO_ERROR */
    }

    if (memcmp(sa->ipaddr_ptr, generic_inaddr_any, sa->ipaddr_len)) {
        /* A real remote address was passed in.  If the unspecified
         * address was used, the actual remote addr will have to be
         * determined using getpeername() if required. */
        sock->remote_addr_unknown = 0;

        /* Copy the address structure details in. */
        sock->remote_addr->sa = sa->sa;
        sock->remote_addr->salen = sa->salen;
        /* Adjust ipaddr_ptr et al. */
        kuda_sockaddr_vars_set(sock->remote_addr, sa->family, sa->port);
    }

    if (sock->local_addr->port == 0) {
        /* connect() got us an ephemeral port */
        sock->local_port_unknown = 1;
    }
#if KUDA_HAVE_SOCKADDR_UN
    if (sock->local_addr->sa.sin.sin_family == AF_UNIX) {
        /* Assign connect address as local. */
        sock->local_addr = sa;
    }
    else
#endif
    if (!memcmp(sock->local_addr->ipaddr_ptr,
                generic_inaddr_any,
                sock->local_addr->ipaddr_len)) {
        /* not bound to specific local interface; connect() had to assign
         * one for the socket
         */
        sock->local_interface_unknown = 1;
    }

    if (rc == -1 && errno != EISCONN) {
        return errno;
    }

#ifndef HAVE_POLL
    sock->connected=1;
#endif
    return KUDA_SUCCESS;
}

kuda_status_t kuda_socket_type_get(kuda_socket_t *sock, int *type)
{
    *type = sock->type;
    return KUDA_SUCCESS;
}

kuda_status_t kuda_socket_data_get(void **data, const char *key, kuda_socket_t *sock)
{
    sock_userdata_t *cur = sock->userdata;

    *data = NULL;

    while (cur) {
        if (!strcmp(cur->key, key)) {
            *data = cur->data;
            break;
        }
        cur = cur->next;
    }

    return KUDA_SUCCESS;
}

kuda_status_t kuda_socket_data_set(kuda_socket_t *sock, void *data, const char *key,
                                 kuda_status_t (*cleanup) (void *))
{
    sock_userdata_t *new = kuda_palloc(sock->pool, sizeof(sock_userdata_t));

    new->key = kuda_pstrdup(sock->pool, key);
    new->data = data;
    new->next = sock->userdata;
    sock->userdata = new;

    if (cleanup) {
        kuda_pool_cleanup_register(sock->pool, data, cleanup, cleanup);
    }

    return KUDA_SUCCESS;
}

kuda_status_t kuda_platform_sock_get(kuda_platform_sock_t *thesock, kuda_socket_t *sock)
{
    *thesock = sock->socketdes;
    return KUDA_SUCCESS;
}

kuda_status_t kuda_platform_sock_make(kuda_socket_t **kuda_sock, 
                              kuda_platform_sock_info_t *platform_sock_info, 
                              kuda_pool_t *cont)
{
    alloc_socket(kuda_sock, cont);
    set_socket_vars(*kuda_sock, platform_sock_info->family, platform_sock_info->type, platform_sock_info->protocol);
    (*kuda_sock)->timeout = -1;
    (*kuda_sock)->socketdes = *platform_sock_info->platform_sock;
    if (platform_sock_info->local) {
        memcpy(&(*kuda_sock)->local_addr->sa.sin, 
               platform_sock_info->local, 
               (*kuda_sock)->local_addr->salen);
        /* XXX IPv6 - this assumes sin_port and sin6_port at same offset */
        (*kuda_sock)->local_addr->port = ntohs((*kuda_sock)->local_addr->sa.sin.sin_port);
    }
    else {
        (*kuda_sock)->local_port_unknown = (*kuda_sock)->local_interface_unknown = 1;
    }
    if (platform_sock_info->remote) {
#ifndef HAVE_POLL
        (*kuda_sock)->connected = 1;
#endif
        memcpy(&(*kuda_sock)->remote_addr->sa.sin, 
               platform_sock_info->remote,
               (*kuda_sock)->remote_addr->salen);
        /* XXX IPv6 - this assumes sin_port and sin6_port at same offset */
        (*kuda_sock)->remote_addr->port = ntohs((*kuda_sock)->remote_addr->sa.sin.sin_port);
    }
    else {
        (*kuda_sock)->remote_addr_unknown = 1;
    }
        
    (*kuda_sock)->inherit = 0;
    kuda_pool_cleanup_register((*kuda_sock)->pool, (void *)(*kuda_sock), 
                              socket_cleanup, socket_cleanup);
    return KUDA_SUCCESS;
}

kuda_status_t kuda_platform_sock_put(kuda_socket_t **sock, kuda_platform_sock_t *thesock, 
                           kuda_pool_t *cont)
{
    /* XXX Bogus assumption that *sock points at anything legit */
    if ((*sock) == NULL) {
        alloc_socket(sock, cont);
        /* XXX IPv6 figure out the family here! */
        /* XXX figure out the actual socket type here */
        /* *or* just decide that kuda_platform_sock_put() has to be told the family and type */
        set_socket_vars(*sock, KUDA_INET, SOCK_STREAM, 0);
        (*sock)->timeout = -1;
    }
    (*sock)->local_port_unknown = (*sock)->local_interface_unknown = 1;
    (*sock)->remote_addr_unknown = 1;
    (*sock)->socketdes = *thesock;
    return KUDA_SUCCESS;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(socket)

KUDA_IMPLEMENT_INHERIT_SET(socket, inherit, pool, socket_cleanup)

KUDA_IMPLEMENT_INHERIT_UNSET(socket, inherit, pool, socket_cleanup)
