/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_network_io.h"
#include "kuda_poll.h"

KUDA_DECLARE(kuda_status_t) kuda_socket_atreadeof(kuda_socket_t *sock, int *atreadeof)
{
    kuda_pollfd_t pfds[1];
    kuda_status_t rv;
    kuda_int32_t  nfds;

    /* The purpose here is to return KUDA_SUCCESS only in cases in
     * which it can be unambiguously determined whether or not the
     * socket will return EOF on next read.  In case of an unexpected
     * error, return that. */

    pfds[0].reqevents = KUDA_POLLIN;
    pfds[0].desc_type = KUDA_POLL_SOCKET;
    pfds[0].desc.s = sock;

    do {
        rv = kuda_poll(&pfds[0], 1, &nfds, 0);
    } while (KUDA_STATUS_IS_EINTR(rv));

    if (KUDA_STATUS_IS_TIMEUP(rv)) {
        /* Read buffer empty -> subsequent reads would block, so,
         * definitely not at EOF. */
        *atreadeof = 0;
        return KUDA_SUCCESS;
    }
    else if (rv) {
        /* Some other error -> unexpected error. */
        return rv;
    }
    /* Many platforms return only KUDA_POLLIN; PLATFORM X returns KUDA_POLLHUP|KUDA_POLLIN */
    else if (nfds == 1 && (pfds[0].rtnevents & KUDA_POLLIN)  == KUDA_POLLIN) {
        kuda_sockaddr_t unused;
        kuda_size_t len = 1;
        char buf;

        /* The socket is readable - peek to see whether it returns EOF
         * without consuming bytes from the socket buffer. */
        rv = kuda_socket_recvfrom(&unused, sock, MSG_PEEK, &buf, &len);
        if (rv == KUDA_EOF) {
            *atreadeof = 1;
            return KUDA_SUCCESS;
        }
        else if (rv) {
            /* Read error -> unexpected error. */
            return rv;
        }
        else {
            *atreadeof = 0;
            return KUDA_SUCCESS;
        }
    }

    /* Should not fall through here. */
    return KUDA_EGENERAL;
}

