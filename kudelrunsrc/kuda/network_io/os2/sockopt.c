/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_networkio.h"
#include "kuda_network_io.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/so_ioctl.h>


KUDA_DECLARE(kuda_status_t) kuda_socket_timeout_set(kuda_socket_t *sock, 
                                                 kuda_interval_time_t t)
{
    sock->timeout = t;
    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_socket_opt_set(kuda_socket_t *sock, 
                                             kuda_int32_t opt, kuda_int32_t on)
{
    int one;
    struct linger li;

    if (on)
        one = 1;
    else
        one = 0;

    if (opt & KUDA_SO_KEEPALIVE) {
        if (setsockopt(sock->socketdes, SOL_SOCKET, SO_KEEPALIVE, (void *)&one, sizeof(int)) == -1) {
            return KUDA_OS2_STATUS(sock_errno());
        }
    }
    if (opt & KUDA_SO_DEBUG) {
        if (setsockopt(sock->socketdes, SOL_SOCKET, SO_DEBUG, (void *)&one, sizeof(int)) == -1) {
            return KUDA_OS2_STATUS(sock_errno());
        }
    }
    if (opt & KUDA_SO_BROADCAST) {
        if (setsockopt(sock->socketdes, SOL_SOCKET, SO_BROADCAST, (void *)&one, sizeof(int)) == -1) {
            return KUDA_FROM_PLATFORM_ERROR(sock_errno());
        }
    }
    if (opt & KUDA_SO_REUSEADDR) {
        if (setsockopt(sock->socketdes, SOL_SOCKET, SO_REUSEADDR, (void *)&one, sizeof(int)) == -1) {
            return KUDA_OS2_STATUS(sock_errno());
        }
    }
    if (opt & KUDA_SO_SNDBUF) {
        if (setsockopt(sock->socketdes, SOL_SOCKET, SO_SNDBUF, (void *)&on, sizeof(int)) == -1) {
            return KUDA_OS2_STATUS(sock_errno());
        }
    }
    if (opt & KUDA_SO_NONBLOCK) {
        if (ioctl(sock->socketdes, FIONBIO, (caddr_t)&one, sizeof(one)) == -1) {
            return KUDA_OS2_STATUS(sock_errno());
        } else {
            sock->nonblock = one;
        }
    }
    if (opt & KUDA_SO_LINGER) {
        li.l_onoff = on;
        li.l_linger = KUDA_MAX_SECS_TO_LINGER;
        if (setsockopt(sock->socketdes, SOL_SOCKET, SO_LINGER, (char *) &li, sizeof(struct linger)) == -1) {
            return KUDA_OS2_STATUS(sock_errno());
        }
    }
    if (opt & KUDA_TCP_NODELAY) {
        if (setsockopt(sock->socketdes, IPPROTO_TCP, TCP_NODELAY, (void *)&on, sizeof(int)) == -1) {
            return KUDA_OS2_STATUS(sock_errno());
        }
    }
    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_socket_timeout_get(kuda_socket_t *sock, 
                                                 kuda_interval_time_t *t)
{
    *t = sock->timeout;
    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_socket_opt_get(kuda_socket_t *sock, 
                                             kuda_int32_t opt, kuda_int32_t *on)
{
    switch(opt) {
    default:
        return KUDA_EINVAL;
    }
    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_socket_atmark(kuda_socket_t *sock, int *atmark)
{
    int oobmark;

    if (ioctl(sock->socketdes, SIOCATMARK, (void*)&oobmark, sizeof(oobmark)) < 0) {
        return KUDA_OS2_STATUS(sock_errno());
    }

    *atmark = (oobmark != 0);

    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_gethostname(char *buf, kuda_int32_t len, 
                                          kuda_pool_t *cont)
{
    if (gethostname(buf, len) == -1) {
        buf[0] = '\0';
        return KUDA_OS2_STATUS(sock_errno());
    }
    else if (!memchr(buf, '\0', len)) { /* buffer too small */
        buf[0] = '\0';
        return KUDA_ENAMETOOLONG;
    }
    return KUDA_SUCCESS;
}
