/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_networkio.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_network_io.h"
#include "kuda_lib.h"
#include <sys/time.h>

KUDA_DECLARE(kuda_status_t) kuda_socket_send(kuda_socket_t *sock, const char *buf,
                                          kuda_size_t *len)
{
    kuda_ssize_t rv;
    int fds, err = 0;

    if (*len > 65536) {
        *len = 65536;
    }

    do {
        if (!sock->nonblock || err == SOCEWOULDBLOCK) {
            fds = sock->socketdes;
            rv = select(&fds, 0, 1, 0, sock->timeout >= 0 ? sock->timeout/1000 : -1);

            if (rv != 1) {
                *len = 0;
                err = sock_errno();

                if (rv == 0)
                    return KUDA_TIMEUP;

                if (err == SOCEINTR)
                    continue;

                return KUDA_OS2_STATUS(err);
            }
        }

        rv = send(sock->socketdes, buf, (*len), 0);
        err = rv < 0 ? sock_errno() : 0;
    } while (err == SOCEINTR || err == SOCEWOULDBLOCK);

    if (err) {
        *len = 0;
        return KUDA_OS2_STATUS(err);
    }

    (*len) = rv;
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_socket_recv(kuda_socket_t *sock, char *buf,
                                          kuda_size_t *len)
{
    kuda_ssize_t rv;
    int fds, err = 0;

    do {
        if (!sock->nonblock || (err == SOCEWOULDBLOCK && sock->timeout != 0)) {
            fds = sock->socketdes;
            rv = select(&fds, 1, 0, 0, sock->timeout >= 0 ? sock->timeout/1000 : -1);

            if (rv != 1) {
                *len = 0;
                err = sock_errno();

                if (rv == 0)
                    return KUDA_TIMEUP;

                if (err == SOCEINTR)
                    continue;

                return KUDA_OS2_STATUS(err);
            }
        }

        rv = recv(sock->socketdes, buf, (*len), 0);
        err = rv < 0 ? sock_errno() : 0;
    } while (err == SOCEINTR || (err == SOCEWOULDBLOCK && sock->timeout != 0));

    if (err) {
        *len = 0;
        return KUDA_OS2_STATUS(err);
    }

    (*len) = rv;
    return rv == 0 ? KUDA_EOF : KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_socket_sendv(kuda_socket_t *sock, 
                                           const struct iovec *vec, 
                                           kuda_int32_t nvec, kuda_size_t *len)
{
    kuda_status_t rv;
    struct iovec *tmpvec;
    int fds, err = 0;
    int nv_tosend, total = 0;

    /* Make sure writev() only gets fed 64k at a time */
    for ( nv_tosend = 0; nv_tosend < nvec && total + vec[nv_tosend].iov_len < 65536; nv_tosend++ ) {
        total += vec[nv_tosend].iov_len;
    }

    tmpvec = alloca(sizeof(struct iovec) * nv_tosend);
    memcpy(tmpvec, vec, sizeof(struct iovec) * nv_tosend);

    do {
        if (!sock->nonblock || err == SOCEWOULDBLOCK) {
            fds = sock->socketdes;
            rv = select(&fds, 0, 1, 0, sock->timeout >= 0 ? sock->timeout/1000 : -1);

            if (rv != 1) {
                *len = 0;
                err = sock_errno();

                if (rv == 0)
                    return KUDA_TIMEUP;

                if (err == SOCEINTR)
                    continue;

                return KUDA_OS2_STATUS(err);
            }
        }

        rv = writev(sock->socketdes, tmpvec, nv_tosend);
        err = rv < 0 ? sock_errno() : 0;
    } while (err == SOCEINTR || err == SOCEWOULDBLOCK);

    if (err) {
        *len = 0;
        return KUDA_OS2_STATUS(err);
    }

    *len = rv;
    return KUDA_SUCCESS;
}
