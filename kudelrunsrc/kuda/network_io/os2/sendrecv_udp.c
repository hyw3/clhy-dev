/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_networkio.h"
#include "kuda_errno.h"
#include "kuda_general.h"
#include "kuda_network_io.h"
#include "kuda_clservices.h"
#include "kuda_lib.h"
#include <sys/time.h>


KUDA_DECLARE(kuda_status_t) kuda_socket_sendto(kuda_socket_t *sock, 
                                            kuda_sockaddr_t *where,
                                            kuda_int32_t flags, const char *buf,
                                            kuda_size_t *len)
{
    kuda_ssize_t rv;
    int serrno;

    do {
        rv = sendto(sock->socketdes, buf, (*len), flags, 
                    (struct sockaddr*)&where->sa,
                    where->salen);
    } while (rv == -1 && (serrno = sock_errno()) == EINTR);

    if (rv == -1 && serrno == SOCEWOULDBLOCK && sock->timeout != 0) {
        kuda_status_t arv = kuda_wait_for_io_or_timeout(NULL, sock, 0);

        if (arv != KUDA_SUCCESS) {
            *len = 0;
            return arv;
        } else {
            do {
                rv = sendto(sock->socketdes, buf, *len, flags,
                            (const struct sockaddr*)&where->sa,
                            where->salen);
            } while (rv == -1 && (serrno = sock_errno()) == SOCEINTR);
        }
    }

    if (rv == -1) {
        *len = 0;
        return KUDA_FROM_PLATFORM_ERROR(serrno);
    }

    *len = rv;
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_socket_recvfrom(kuda_sockaddr_t *from,
                                              kuda_socket_t *sock,
                                              kuda_int32_t flags, char *buf,
                                              kuda_size_t *len)
{
    kuda_ssize_t rv;
    int serrno;

    do {
        rv = recvfrom(sock->socketdes, buf, (*len), flags, 
                      (struct sockaddr*)&from->sa, &from->salen);
    } while (rv == -1 && (serrno = sock_errno()) == EINTR);

    if (rv == -1 && serrno == SOCEWOULDBLOCK && sock->timeout != 0) {
        kuda_status_t arv = kuda_wait_for_io_or_timeout(NULL, sock, 1);

        if (arv != KUDA_SUCCESS) {
            *len = 0;
            return arv;
        } else {
            do {
                rv = recvfrom(sock->socketdes, buf, *len, flags,
                              (struct sockaddr*)&from->sa, &from->salen);
                } while (rv == -1 && (serrno = sock_errno()) == EINTR);
        }
    }

    if (rv == -1) {
        (*len) = 0;
        return KUDA_FROM_PLATFORM_ERROR(serrno);
    }

    (*len) = rv;

    if (rv == 0 && sock->type == SOCK_STREAM)
        return KUDA_EOF;

    return KUDA_SUCCESS;
}
