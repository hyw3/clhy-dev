/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_networkio.h"
#include "kuda_network_io.h"
#include "kuda_portable.h"
#include "kuda_general.h"
#include "kuda_lib.h"

static int os2_socket_init(int, int ,int);

int (*kuda_os2_socket)(int, int, int) = os2_socket_init;
int (*kuda_os2_select)(int *, int, int, int, long) = NULL;
int (*kuda_os2_sock_errno)() = NULL;
int (*kuda_os2_accept)(int, struct sockaddr *, int *) = NULL;
int (*kuda_os2_bind)(int, struct sockaddr *, int) = NULL;
int (*kuda_os2_connect)(int, struct sockaddr *, int) = NULL;
int (*kuda_os2_getpeername)(int, struct sockaddr *, int *) = NULL;
int (*kuda_os2_getsockname)(int, struct sockaddr *, int *) = NULL;
int (*kuda_os2_getsockopt)(int, int, int, char *, int *) = NULL;
int (*kuda_os2_ioctl)(int, int, caddr_t, int) = NULL;
int (*kuda_os2_listen)(int, int) = NULL;
int (*kuda_os2_recv)(int, char *, int, int) = NULL;
int (*kuda_os2_send)(int, const char *, int, int) = NULL;
int (*kuda_os2_setsockopt)(int, int, int, char *, int) = NULL;
int (*kuda_os2_shutdown)(int, int) = NULL;
int (*kuda_os2_soclose)(int) = NULL;
int (*kuda_os2_writev)(int, struct iovec *, int) = NULL;
int (*kuda_os2_sendto)(int, const char *, int, int, const struct sockaddr *, int);
int (*kuda_os2_recvfrom)(int, char *, int, int, struct sockaddr *, int *);

static HCAPI hSO32DLL;

static int os2_fn_link()
{
    DosEnterCritSec(); /* Stop two threads doing this at the same time */

    if (kuda_os2_socket == os2_socket_init) {
        ULONG rc;
        char errorstr[200];

        rc = DosActivatecAPI(errorstr, sizeof(errorstr), "SO32DLL", &hSO32DLL);

        if (rc)
            return KUDA_OS2_STATUS(rc);

        rc = DosQueryProcAddr(hSO32DLL, 0, "SOCKET", &kuda_os2_socket);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "SELECT", &kuda_os2_select);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "SOCK_ERRNO", &kuda_os2_sock_errno);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "ACCEPT", &kuda_os2_accept);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "BIND", &kuda_os2_bind);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "CONNECT", &kuda_os2_connect);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "GETPEERNAME", &kuda_os2_getpeername);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "GETSOCKNAME", &kuda_os2_getsockname);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "GETSOCKOPT", &kuda_os2_getsockopt);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "IOCTL", &kuda_os2_ioctl);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "LISTEN", &kuda_os2_listen);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "RECV", &kuda_os2_recv);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "SEND", &kuda_os2_send);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "SETSOCKOPT", &kuda_os2_setsockopt);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "SHUTDOWN", &kuda_os2_shutdown);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "SOCLOSE", &kuda_os2_soclose);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "WRITEV", &kuda_os2_writev);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "SENDTO", &kuda_os2_sendto);

        if (!rc)
            rc = DosQueryProcAddr(hSO32DLL, 0, "RECVFROM", &kuda_os2_recvfrom);

        if (rc)
            return KUDA_OS2_STATUS(rc);
    }

    DosExitCritSec();
    return KUDA_SUCCESS;
}



static int os2_socket_init(int domain, int type, int protocol)
{
    int rc = os2_fn_link();
    if (rc == KUDA_SUCCESS)
        return kuda_os2_socket(domain, type, protocol);
    return rc;
}
