/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_networkio.h"
#include "kuda_arch_inherit.h"
#include "kuda_network_io.h"
#include "kuda_general.h"
#include "kuda_portable.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "kuda_arch_os2calls.h"

static kuda_status_t socket_cleanup(void *sock)
{
    kuda_socket_t *thesocket = sock;

    if (thesocket->socketdes < 0) {
        return KUDA_EINVALSOCK;
    }

    if (soclose(thesocket->socketdes) == 0) {
        thesocket->socketdes = -1;
        return KUDA_SUCCESS;
    }
    else {
        return KUDA_OS2_STATUS(sock_errno());
    }
}

static void set_socket_vars(kuda_socket_t *sock, int family, int type, int protocol)
{
    sock->type = type;
    sock->protocol = protocol;
    kuda_sockaddr_vars_set(sock->local_addr, family, 0);
    kuda_sockaddr_vars_set(sock->remote_addr, family, 0);
}

static void alloc_socket(kuda_socket_t **new, kuda_pool_t *p)
{
    *new = (kuda_socket_t *)kuda_pcalloc(p, sizeof(kuda_socket_t));
    (*new)->pool = p;
    (*new)->local_addr = (kuda_sockaddr_t *)kuda_pcalloc((*new)->pool,
                                                       sizeof(kuda_sockaddr_t));
    (*new)->local_addr->pool = p;

    (*new)->remote_addr = (kuda_sockaddr_t *)kuda_pcalloc((*new)->pool,
                                                        sizeof(kuda_sockaddr_t));
    (*new)->remote_addr->pool = p;
    (*new)->remote_addr_unknown = 1;

    /* Create a pollset with room for one descriptor. */
    /* ### check return codes */
    (void) kuda_pollset_create(&(*new)->pollset, 1, p, 0);
}

KUDA_DECLARE(kuda_status_t) kuda_socket_protocol_get(kuda_socket_t *sock, int *protocol)
{
    *protocol = sock->protocol;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_socket_create(kuda_socket_t **new, int family, int type,
                                            int protocol, kuda_pool_t *cont)
{
    int downgrade = (family == AF_UNSPEC);
    kuda_pollfd_t pfd;

    if (family == AF_UNSPEC) {
#if KUDA_HAVE_IPV6
        family = AF_INET6;
#else
        family = AF_INET;
#endif
    }

    alloc_socket(new, cont);

    (*new)->socketdes = socket(family, type, protocol);
#if KUDA_HAVE_IPV6
    if ((*new)->socketdes < 0 && downgrade) {
        family = AF_INET;
        (*new)->socketdes = socket(family, type, protocol);
    }
#endif

    if ((*new)->socketdes < 0) {
        return KUDA_OS2_STATUS(sock_errno());
    }
    set_socket_vars(*new, family, type, protocol);

    (*new)->timeout = -1;
    (*new)->nonblock = FALSE;
    kuda_pool_cleanup_register((*new)->pool, (void *)(*new), 
                        socket_cleanup, kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
} 

KUDA_DECLARE(kuda_status_t) kuda_socket_shutdown(kuda_socket_t *thesocket, 
                                              kuda_shutdown_how_e how)
{
    if (shutdown(thesocket->socketdes, how) == 0) {
        return KUDA_SUCCESS;
    }
    else {
        return KUDA_OS2_STATUS(sock_errno());
    }
}

KUDA_DECLARE(kuda_status_t) kuda_socket_close(kuda_socket_t *thesocket)
{
    kuda_pool_cleanup_kill(thesocket->pool, thesocket, socket_cleanup);
    return socket_cleanup(thesocket);
}

KUDA_DECLARE(kuda_status_t) kuda_socket_bind(kuda_socket_t *sock,
                                          kuda_sockaddr_t *sa)
{
    if (bind(sock->socketdes, 
             (struct sockaddr *)&sa->sa,
             sa->salen) == -1)
        return KUDA_OS2_STATUS(sock_errno());
    else {
        sock->local_addr = sa;
        /* XXX IPv6 - this assumes sin_port and sin6_port at same offset */
        if (sock->local_addr->sa.sin.sin_port == 0) { /* no need for ntohs() when comparing w/ 0 */
            sock->local_port_unknown = 1; /* kernel got us an ephemeral port */
        }
        return KUDA_SUCCESS;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_socket_listen(kuda_socket_t *sock, 
                                            kuda_int32_t backlog)
{
    if (listen(sock->socketdes, backlog) == -1)
        return KUDA_OS2_STATUS(sock_errno());
    else
        return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_socket_accept(kuda_socket_t **new, 
                                            kuda_socket_t *sock,
                                            kuda_pool_t *connection_context)
{
    alloc_socket(new, connection_context);
    set_socket_vars(*new, sock->local_addr->sa.sin.sin_family, SOCK_STREAM, sock->protocol);

    (*new)->timeout = -1;
    (*new)->nonblock = FALSE;

    (*new)->socketdes = accept(sock->socketdes, 
                               (struct sockaddr *)&(*new)->remote_addr->sa,
                               &(*new)->remote_addr->salen);

    if ((*new)->socketdes < 0) {
        return KUDA_OS2_STATUS(sock_errno());
    }

    *(*new)->local_addr = *sock->local_addr;
    (*new)->local_addr->pool = connection_context;
    (*new)->remote_addr->port = ntohs((*new)->remote_addr->sa.sin.sin_port);

    /* fix up any pointers which are no longer valid */
    if (sock->local_addr->sa.sin.sin_family == AF_INET) {
        (*new)->local_addr->ipaddr_ptr = &(*new)->local_addr->sa.sin.sin_addr;
    }

    kuda_pool_cleanup_register((*new)->pool, (void *)(*new), 
                        socket_cleanup, kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_socket_connect(kuda_socket_t *sock,
                                             kuda_sockaddr_t *sa)
{
    if ((connect(sock->socketdes, (struct sockaddr *)&sa->sa.sin, 
                 sa->salen) < 0) &&
        (sock_errno() != SOCEINPROGRESS)) {
        return KUDA_OS2_STATUS(sock_errno());
    }
    else {
        int namelen = sizeof(sock->local_addr->sa.sin);
        getsockname(sock->socketdes, (struct sockaddr *)&sock->local_addr->sa.sin, 
                    &namelen);
        sock->remote_addr = sa;
        return KUDA_SUCCESS;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_socket_type_get(kuda_socket_t *sock, int *type)
{
    *type = sock->type;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_socket_data_get(void **data, const char *key,
                                     kuda_socket_t *sock)
{
    sock_userdata_t *cur = sock->userdata;

    *data = NULL;

    while (cur) {
        if (!strcmp(cur->key, key)) {
            *data = cur->data;
            break;
        }
        cur = cur->next;
    }

    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_socket_data_set(kuda_socket_t *sock, void *data, const char *key,
                                     kuda_status_t (*cleanup) (void *))
{
    sock_userdata_t *new = kuda_palloc(sock->pool, sizeof(sock_userdata_t));

    new->key = kuda_pstrdup(sock->pool, key);
    new->data = data;
    new->next = sock->userdata;
    sock->userdata = new;

    if (cleanup) {
        kuda_pool_cleanup_register(sock->pool, data, cleanup, cleanup);
    }

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_sock_get(kuda_platform_sock_t *thesock, kuda_socket_t *sock)
{
    *thesock = sock->socketdes;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_sock_make(kuda_socket_t **kuda_sock, 
                                           kuda_platform_sock_info_t *platform_sock_info, 
                                           kuda_pool_t *cont)
{
    alloc_socket(kuda_sock, cont);
    set_socket_vars(*kuda_sock, platform_sock_info->family, platform_sock_info->type, platform_sock_info->protocol);
    (*kuda_sock)->timeout = -1;
    (*kuda_sock)->socketdes = *platform_sock_info->platform_sock;
    if (platform_sock_info->local) {
        memcpy(&(*kuda_sock)->local_addr->sa.sin, 
               platform_sock_info->local, 
               (*kuda_sock)->local_addr->salen);
        /* XXX IPv6 - this assumes sin_port and sin6_port at same offset */
        (*kuda_sock)->local_addr->port = ntohs((*kuda_sock)->local_addr->sa.sin.sin_port);
    }
    else {
        (*kuda_sock)->local_port_unknown = (*kuda_sock)->local_interface_unknown = 1;
    }
    if (platform_sock_info->remote) {
        memcpy(&(*kuda_sock)->remote_addr->sa.sin, 
               platform_sock_info->remote,
               (*kuda_sock)->remote_addr->salen);
        /* XXX IPv6 - this assumes sin_port and sin6_port at same offset */
        (*kuda_sock)->remote_addr->port = ntohs((*kuda_sock)->remote_addr->sa.sin.sin_port);
    }
    else {
        (*kuda_sock)->remote_addr_unknown = 1;
    }
        
    kuda_pool_cleanup_register((*kuda_sock)->pool, (void *)(*kuda_sock), 
                        socket_cleanup, kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_sock_put(kuda_socket_t **sock, kuda_platform_sock_t *thesock, kuda_pool_t *cont)
{
    if (cont == NULL) {
        return KUDA_ENOPOOL;
    }
    if ((*sock) == NULL) {
        alloc_socket(sock, cont);
        set_socket_vars(*sock, AF_INET, SOCK_STREAM, 0);
        (*sock)->timeout = -1;
    }

    (*sock)->local_port_unknown = (*sock)->local_interface_unknown = 1;
    (*sock)->remote_addr_unknown = 1;
    (*sock)->socketdes = *thesock;
    return KUDA_SUCCESS;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(socket);

KUDA_IMPLEMENT_INHERIT_SET(socket, inherit, pool, socket_cleanup)

KUDA_IMPLEMENT_INHERIT_UNSET(socket, inherit, pool, socket_cleanup)

