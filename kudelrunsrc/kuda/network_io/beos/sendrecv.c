/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_private.h"
#if BEOS_BONE /* BONE uses the unix code - woohoo */
#include "../unix/sendrecv.c"
#else
#include "kuda_arch_networkio.h"
#include "kuda_time.h"

static kuda_status_t wait_for_io_or_timeout(kuda_socket_t *sock, int for_read)
{
    struct timeval tv, *tvptr;
    fd_set fdset;
    int srv;

    do {
        FD_ZERO(&fdset);
        FD_SET(sock->socketdes, &fdset);
        if (sock->timeout < 0) {
            tvptr = NULL;
        }
        else {
            tv.tv_sec = sock->timeout / KUDA_USEC_PER_SEC;
            tv.tv_usec = sock->timeout % KUDA_USEC_PER_SEC;
            tvptr = &tv;
        }
        srv = select(sock->socketdes + 1,
            for_read ? &fdset : NULL,
            for_read ? NULL : &fdset, 
            NULL,
            tvptr);
            /* TODO - timeout should be smaller on repeats of this loop */
    } while (srv == -1 && errno == EINTR);

    if (srv == 0) {
        return KUDA_TIMEUP;
    }
    else if (srv < 0) {
        return errno;
    }
    return KUDA_SUCCESS;
}

#define SEND_WAIT KUDA_USEC_PER_SEC / 10

KUDA_DECLARE(kuda_status_t) kuda_socket_send(kuda_socket_t *sock, const char *buf,
                                          kuda_size_t *len)
{
    kuda_ssize_t rv;
	
    do {
        rv = send(sock->socketdes, buf, (*len), 0);
    } while (rv == -1 && errno == EINTR);

    if (rv == -1 && errno == EWOULDBLOCK && sock->timeout > 0) {
        kuda_int32_t snooze_val = SEND_WAIT;
        kuda_int32_t zzz = 0;  
        
        do {
            rv = send(sock->socketdes, buf, (*len), 0);
            if (rv == -1 && errno == EWOULDBLOCK){
                kuda_sleep (snooze_val);
                zzz += snooze_val;
                snooze_val += SEND_WAIT;
                /* have we passed our timeout value */
                if (zzz > (sock->timeout * KUDA_USEC_PER_SEC))
                    break;
            }
        } while (rv == -1 && (errno == EINTR || errno == EWOULDBLOCK));
    }
    if (rv == -1) {
        *len = 0;
        return errno;
    }
    (*len) = rv;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_socket_recv(kuda_socket_t *sock, char *buf, 
                                          kuda_size_t *len)
{
    kuda_ssize_t rv;
   
    do {
        rv = recv(sock->socketdes, buf, (*len), 0);
    } while (rv == -1 && errno == EINTR);

    if (rv == -1 && errno == EWOULDBLOCK && sock->timeout > 0) {
        kuda_status_t arv = wait_for_io_or_timeout(sock, 1);
        if (arv != KUDA_SUCCESS) {
            *len = 0;
            return arv;
        }
        else {
            do {
                rv = recv(sock->socketdes, buf, (*len), 0);
            } while (rv == -1 && errno == EINTR);
        }
    }
    if (rv == -1) {
        (*len) = 0;
        return errno;
    }
    (*len) = rv;
    if (rv == 0)
        return KUDA_EOF;
    return KUDA_SUCCESS;
}

/* BeOS doesn't have writev for sockets so we use the following instead...
 */
KUDA_DECLARE(kuda_status_t) kuda_socket_sendv(kuda_socket_t * sock, 
                                           const struct iovec *vec,
                                           kuda_int32_t nvec, kuda_size_t *len)
{
    *len = vec[0].iov_len;
    return kuda_socket_send(sock, vec[0].iov_base, len);
}

KUDA_DECLARE(kuda_status_t) kuda_socket_sendto(kuda_socket_t *sock, 
                                            kuda_sockaddr_t *where,
                                            kuda_int32_t flags, const char *buf,
                                            kuda_size_t *len)
{
    kuda_ssize_t rv;

    do {
        rv = sendto(sock->socketdes, buf, (*len), flags,
                    (const struct sockaddr*)&where->sa,
                    where->salen);
    } while (rv == -1 && errno == EINTR);

    if (rv == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)
        && sock->timeout != 0) {
        kuda_status_t arv = wait_for_io_or_timeout(sock, 0);
        if (arv != KUDA_SUCCESS) {
            *len = 0;
            return arv;
        } else {
            do {
                rv = sendto(sock->socketdes, buf, (*len), flags,
                            (const struct sockaddr*)&where->sa,
                            where->salen);
            } while (rv == -1 && errno == EINTR);
        }
    }
    if (rv == -1) {
        *len = 0;
        return errno;
    }
    *len = rv;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_socket_recvfrom(kuda_sockaddr_t *from,
                                              kuda_socket_t *sock,
                                              kuda_int32_t flags, char *buf,
                                              kuda_size_t *len)
{
    kuda_ssize_t rv;

    if (from == NULL){
        return KUDA_ENOMEM;
        /* Not sure if this is correct.  Maybe we should just allocate
           the memory??
         */
    }

    do {
        rv = recvfrom(sock->socketdes, buf, (*len), flags,
                      (struct sockaddr*)&from->sa, &from->salen);
    } while (rv == -1 && errno == EINTR);

    if (rv == -1 && (errno == EAGAIN || errno == EWOULDBLOCK) &&
        sock->timeout != 0) {
        kuda_status_t arv = wait_for_io_or_timeout(sock, 1);
        if (arv != KUDA_SUCCESS) {
            *len = 0;
            return arv;
        } else {
            do {
                rv = recvfrom(sock->socketdes, buf, (*len), flags,
                              (struct sockaddr*)&from->sa, &from->salen);
                } while (rv == -1 && errno == EINTR);
        }
    }
    if (rv == -1) {
        (*len) = 0;
        return errno;
    }
	
    from->port = ntohs(from->sa.sin.sin_port);
	
    (*len) = rv;
    if (rv == 0)
        return KUDA_EOF;

    return KUDA_SUCCESS;
}

#endif /* ! BEOS_BONE */
