/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_networkio.h"
#include "kuda_arch_misc.h" /* kuda_platform_level */
#include "kuda_network_io.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include <string.h>

/* IPV6_V6ONLY is missing from pre-Windows 2008 SDK as well as MinGW
 * (at least up through 1.0.16).
 * Runtime support is a separate issue.
 */
#ifndef IPV6_V6ONLY
#define IPV6_V6ONLY 27
#endif

static kuda_status_t soblock(SOCKET sd)
{
    u_long zero = 0;

    if (ioctlsocket(sd, FIONBIO, &zero) == SOCKET_ERROR) {
        return kuda_get_netos_error();
    }
    return KUDA_SUCCESS;
}

static kuda_status_t sononblock(SOCKET sd)
{
    u_long one = 1;

    if (ioctlsocket(sd, FIONBIO, &one) == SOCKET_ERROR) {
        return kuda_get_netos_error();
    }
    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_socket_timeout_set(kuda_socket_t *sock, kuda_interval_time_t t)
{
    kuda_status_t stat;

    if (t == 0) {
        /* Set the socket non-blocking if it was previously blocking */
        if (sock->timeout != 0) {
            if ((stat = sononblock(sock->socketdes)) != KUDA_SUCCESS)
                return stat;
        }
    }
    else if (t > 0) {
        /* Set the socket to blocking if it was previously non-blocking */
        if (sock->timeout == 0 || kuda_is_option_set(sock, KUDA_SO_NONBLOCK)) {
            if ((stat = soblock(sock->socketdes)) != KUDA_SUCCESS)
                return stat;
            kuda_set_option(sock, KUDA_SO_NONBLOCK, 0);
        }
        /* Reset socket timeouts if the new timeout differs from the old timeout */
        if (sock->timeout != t) 
        {
            /* Win32 timeouts are in msec, represented as int */
            sock->timeout_ms = (int)kuda_time_as_msec(t);
            setsockopt(sock->socketdes, SOL_SOCKET, SO_RCVTIMEO, 
                       (char *) &sock->timeout_ms, 
                       sizeof(sock->timeout_ms));
            setsockopt(sock->socketdes, SOL_SOCKET, SO_SNDTIMEO, 
                       (char *) &sock->timeout_ms, 
                       sizeof(sock->timeout_ms));
        }
    }
    else if (t < 0) {
        int zero = 0;
        /* Set the socket to blocking with infinite timeouts */
        if ((stat = soblock(sock->socketdes)) != KUDA_SUCCESS)
            return stat;
        setsockopt(sock->socketdes, SOL_SOCKET, SO_RCVTIMEO, 
                   (char *) &zero, sizeof(zero));
        setsockopt(sock->socketdes, SOL_SOCKET, SO_SNDTIMEO, 
                   (char *) &zero, sizeof(zero));
    }
    sock->timeout = t;
    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_socket_opt_set(kuda_socket_t *sock,
                                             kuda_int32_t opt, kuda_int32_t on)
{
    int one;
    kuda_status_t stat;

    one = on ? 1 : 0;

    switch (opt) {
    case KUDA_SO_KEEPALIVE:
        if (on != kuda_is_option_set(sock, KUDA_SO_KEEPALIVE)) {
            if (setsockopt(sock->socketdes, SOL_SOCKET, SO_KEEPALIVE, 
                           (void *)&one, sizeof(int)) == -1) {
                return kuda_get_netos_error();
            }
            kuda_set_option(sock, KUDA_SO_KEEPALIVE, on);
        }
        break;
    case KUDA_SO_DEBUG:
        if (on != kuda_is_option_set(sock, KUDA_SO_DEBUG)) {
            if (setsockopt(sock->socketdes, SOL_SOCKET, SO_DEBUG, 
                           (void *)&one, sizeof(int)) == -1) {
                return kuda_get_netos_error();
            }
            kuda_set_option(sock, KUDA_SO_DEBUG, on);
        }
        break;
    case KUDA_SO_SNDBUF:
        if (setsockopt(sock->socketdes, SOL_SOCKET, SO_SNDBUF,
                       (void *)&on, sizeof(int)) == -1) {
            return kuda_get_netos_error();
        }
        break;
    case KUDA_SO_RCVBUF:
        if (setsockopt(sock->socketdes, SOL_SOCKET, SO_RCVBUF,
                       (void *)&on, sizeof(int)) == -1) {
            return kuda_get_netos_error();
        }
        break;
    case KUDA_SO_BROADCAST:
        if (on != kuda_is_option_set(sock, KUDA_SO_BROADCAST)) {
           if (setsockopt(sock->socketdes, SOL_SOCKET, SO_BROADCAST, 
                           (void *)&one, sizeof(int)) == -1) {
                return kuda_get_netos_error();
            }
            kuda_set_option(sock, KUDA_SO_BROADCAST, on);
        }
        break;
    case KUDA_SO_REUSEADDR:
        if (on != kuda_is_option_set(sock, KUDA_SO_REUSEADDR)) {
            if (setsockopt(sock->socketdes, SOL_SOCKET, SO_REUSEADDR, 
                           (void *)&one, sizeof(int)) == -1) {
                return kuda_get_netos_error();
            }
            kuda_set_option(sock, KUDA_SO_REUSEADDR, on);
        }
        break;
    case KUDA_SO_NONBLOCK:
        if (kuda_is_option_set(sock, KUDA_SO_NONBLOCK) != on) {
            if (on) {
                if ((stat = sononblock(sock->socketdes)) != KUDA_SUCCESS) 
                    return stat;
            }
            else {
                if ((stat = soblock(sock->socketdes)) != KUDA_SUCCESS)
                    return stat;
            }
            kuda_set_option(sock, KUDA_SO_NONBLOCK, on);
        }
        break;
    case KUDA_SO_LINGER:
    {
        if (kuda_is_option_set(sock, KUDA_SO_LINGER) != on) {
            struct linger li;
            li.l_onoff = on;
            li.l_linger = KUDA_MAX_SECS_TO_LINGER;
            if (setsockopt(sock->socketdes, SOL_SOCKET, SO_LINGER, 
                           (char *) &li, sizeof(struct linger)) == -1) {
                return kuda_get_netos_error();
            }
            kuda_set_option(sock, KUDA_SO_LINGER, on);
        }
        break;
    }
    case KUDA_TCP_DEFER_ACCEPT:
#if defined(TCP_DEFER_ACCEPT)
        if (kuda_is_option_set(sock, KUDA_TCP_DEFER_ACCEPT) != on) {
            int optlevel = IPPROTO_TCP;
            int optname = TCP_DEFER_ACCEPT;

            if (setsockopt(sock->socketdes, optlevel, optname, 
                           (void *)&on, sizeof(int)) == -1) {
                return kuda_get_netos_error();
            }
            kuda_set_option(sock, KUDA_TCP_DEFER_ACCEPT, on);
        }
#else
        return KUDA_ENOTIMPL;
#endif
    case KUDA_TCP_NODELAY:
        if (kuda_is_option_set(sock, KUDA_TCP_NODELAY) != on) {
            int optlevel = IPPROTO_TCP;
            int optname = TCP_NODELAY;

#if KUDA_HAVE_SCTP
            if (sock->protocol == IPPROTO_SCTP) {
                optlevel = IPPROTO_SCTP;
                optname = SCTP_NODELAY;
            }
#endif
            if (setsockopt(sock->socketdes, optlevel, optname,
                           (void *)&on, sizeof(int)) == -1) {
                return kuda_get_netos_error();
            }
            kuda_set_option(sock, KUDA_TCP_NODELAY, on);
        }
        break;
    case KUDA_IPV6_V6ONLY:
#if KUDA_HAVE_IPV6
        if (kuda_platform_level < KUDA_WIN_VISTA && 
            sock->local_addr->family == AF_INET6) {
            /* kuda_set_option() called at socket creation */
            if (on) {
                return KUDA_SUCCESS;
            }
            else {
                return KUDA_ENOTIMPL;
            }
        }
        /* we don't know the initial setting of this option,
         * so don't check sock->options since that optimization
         * won't work
         */
        if (setsockopt(sock->socketdes, IPPROTO_IPV6, IPV6_V6ONLY,
                       (void *)&on, sizeof(int)) == -1) {
            return kuda_get_netos_error();
        }
        kuda_set_option(sock, KUDA_IPV6_V6ONLY, on);
#else
        return KUDA_ENOTIMPL;
#endif
        break;
    default:
        return KUDA_EINVAL;
        break;
    }
    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_socket_timeout_get(kuda_socket_t *sock, kuda_interval_time_t *t)
{
    *t = sock->timeout;
    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_socket_opt_get(kuda_socket_t *sock,
                                             kuda_int32_t opt, kuda_int32_t *on)
{
    switch (opt) {
    case KUDA_SO_DISCONNECTED:
        *on = sock->disconnected;
        break;
    case KUDA_SO_KEEPALIVE:
    case KUDA_SO_DEBUG:
    case KUDA_SO_REUSEADDR:
    case KUDA_SO_NONBLOCK:
    case KUDA_SO_LINGER:
    default:
        *on = kuda_is_option_set(sock, opt);
        break;
    }
    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_socket_atmark(kuda_socket_t *sock, int *atmark)
{
    u_long oobmark;

    if (ioctlsocket(sock->socketdes, SIOCATMARK, (void*) &oobmark) < 0)
        return kuda_get_netos_error();

    *atmark = (oobmark != 0);

    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_gethostname(char *buf, int len,
                                          kuda_pool_t *cont)
{
    if (gethostname(buf, len) == -1) {
        buf[0] = '\0';
        return kuda_get_netos_error();
    }
    else if (!memchr(buf, '\0', len)) { /* buffer too small */
        buf[0] = '\0';
        return KUDA_ENAMETOOLONG;
    }
    return KUDA_SUCCESS;
}

