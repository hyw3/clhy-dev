/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define INCL_DOSERRORS
#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include <string.h>
#include <process.h>

static kuda_status_t file_pipe_create(kuda_file_t **in, kuda_file_t **out,
        kuda_pool_t *pool_in, kuda_pool_t *pool_out)
{
    ULONG filedes[2];
    ULONG rc, action;
    static int id = 0;
    char pipename[50];

    sprintf(pipename, "/pipe/%d.%d", getpid(), id++);
    rc = DosCreateNPipe(pipename, filedes, NP_ACCESS_INBOUND, NP_NOWAIT|1, 4096, 4096, 0);

    if (rc)
        return KUDA_FROM_PLATFORM_ERROR(rc);

    rc = DosConnectNPipe(filedes[0]);

    if (rc && rc != ERROR_PIPE_NOT_CONNECTED) {
        DosClose(filedes[0]);
        return KUDA_FROM_PLATFORM_ERROR(rc);
    }

    rc = DosOpen (pipename, filedes+1, &action, 0, FILE_NORMAL,
                  OPEN_ACTION_OPEN_IF_EXISTS | OPEN_ACTION_FAIL_IF_NEW,
                  OPEN_ACCESS_WRITEONLY | OPEN_SHARE_DENYREADWRITE,
                  NULL);

    if (rc) {
        DosClose(filedes[0]);
        return KUDA_FROM_PLATFORM_ERROR(rc);
    }

    (*in) = (kuda_file_t *)kuda_palloc(pool_in, sizeof(kuda_file_t));
    rc = DosCreateEventSem(NULL, &(*in)->pipeSem, DC_SEM_SHARED, FALSE);

    if (rc) {
        DosClose(filedes[0]);
        DosClose(filedes[1]);
        return KUDA_FROM_PLATFORM_ERROR(rc);
    }

    rc = DosSetNPipeSem(filedes[0], (HSEM)(*in)->pipeSem, 1);

    if (!rc) {
        rc = DosSetNPHState(filedes[0], NP_WAIT);
    }

    if (rc) {
        DosClose(filedes[0]);
        DosClose(filedes[1]);
        DosCloseEventSem((*in)->pipeSem);
        return KUDA_FROM_PLATFORM_ERROR(rc);
    }

    (*in)->pool = pool_in;
    (*in)->filedes = filedes[0];
    (*in)->fname = kuda_pstrdup(pool_in, pipename);
    (*in)->isopen = TRUE;
    (*in)->buffered = FALSE;
    (*in)->flags = 0;
    (*in)->pipe = 1;
    (*in)->timeout = -1;
    (*in)->blocking = BLK_ON;
    kuda_pool_cleanup_register(pool_in, *in, kuda_file_cleanup,
            kuda_pool_cleanup_null);

    (*out) = (kuda_file_t *)kuda_palloc(pool_out, sizeof(kuda_file_t));
    (*out)->pool = pool_out;
    (*out)->filedes = filedes[1];
    (*out)->fname = kuda_pstrdup(pool_out, pipename);
    (*out)->isopen = TRUE;
    (*out)->buffered = FALSE;
    (*out)->flags = 0;
    (*out)->pipe = 1;
    (*out)->timeout = -1;
    (*out)->blocking = BLK_ON;
    kuda_pool_cleanup_register(pool_out, *out, kuda_file_cleanup,
            kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}

static void file_pipe_block(kuda_file_t **in, kuda_file_t **out,
        kuda_int32_t blocking)
{
    switch (blocking) {
    case KUDA_FULL_BLOCK:
        break;
    case KUDA_READ_BLOCK:
        kuda_file_pipe_timeout_set(*out, 0);
        break;
    case KUDA_WRITE_BLOCK:
        kuda_file_pipe_timeout_set(*in, 0);
        break;
    default:
        kuda_file_pipe_timeout_set(*out, 0);
        kuda_file_pipe_timeout_set(*in, 0);
        break;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_create(kuda_file_t **in,
                                               kuda_file_t **out,
                                               kuda_pool_t *pool)
{
    return file_pipe_create(in, out, pool, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_create_ex(kuda_file_t **in, 
                                                  kuda_file_t **out, 
                                                  kuda_int32_t blocking,
                                                  kuda_pool_t *pool)
{
    kuda_status_t status;

    if ((status = file_pipe_create(in, out, pool, pool)) != KUDA_SUCCESS)
        return status;

    file_pipe_block(in, out, blocking);

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_create_pools(kuda_file_t **in,
                                                     kuda_file_t **out,
                                                     kuda_int32_t blocking,
                                                     kuda_pool_t *pool_in,
                                                     kuda_pool_t *pool_out)
{
    kuda_status_t status;

    if ((status = file_pipe_create(in, out, pool_in, pool_out)) != KUDA_SUCCESS)
        return status;

    file_pipe_block(in, out, blocking);

    return KUDA_SUCCESS;
}
    
    
KUDA_DECLARE(kuda_status_t) kuda_file_namedpipe_create(const char *filename, kuda_fileperms_t perm, kuda_pool_t *pool)
{
    /* Not yet implemented, interface not suitable */
    return KUDA_ENOTIMPL;
} 

 

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_timeout_set(kuda_file_t *thepipe, kuda_interval_time_t timeout)
{
    if (thepipe->pipe == 1) {
        thepipe->timeout = timeout;

        if (thepipe->timeout >= 0) {
            if (thepipe->blocking != BLK_OFF) {
                thepipe->blocking = BLK_OFF;
                return KUDA_FROM_PLATFORM_ERROR(DosSetNPHState(thepipe->filedes, NP_NOWAIT));
            }
        }
        else if (thepipe->timeout == -1) {
            if (thepipe->blocking != BLK_ON) {
                thepipe->blocking = BLK_ON;
                return KUDA_FROM_PLATFORM_ERROR(DosSetNPHState(thepipe->filedes, NP_WAIT));
            }
        }
    }
    return KUDA_EINVAL;
}



KUDA_DECLARE(kuda_status_t) kuda_file_pipe_timeout_get(kuda_file_t *thepipe, kuda_interval_time_t *timeout)
{
    if (thepipe->pipe == 1) {
        *timeout = thepipe->timeout;
        return KUDA_SUCCESS;
    }
    return KUDA_EINVAL;
}



KUDA_DECLARE(kuda_status_t) kuda_platform_pipe_put_ex(kuda_file_t **file,
                                             kuda_platform_file_t *thefile,
                                             int register_cleanup,
                                             kuda_pool_t *pool)
{
    (*file) = kuda_pcalloc(pool, sizeof(kuda_file_t));
    (*file)->pool = pool;
    (*file)->isopen = TRUE;
    (*file)->pipe = 1;
    (*file)->blocking = BLK_UNKNOWN; /* app needs to make a timeout call */
    (*file)->timeout = -1;
    (*file)->filedes = *thefile;

    if (register_cleanup) {
        kuda_pool_cleanup_register(pool, *file, kuda_file_cleanup,
                                  kuda_pool_cleanup_null);
    }

    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_platform_pipe_put(kuda_file_t **file,
                                          kuda_platform_file_t *thefile,
                                          kuda_pool_t *pool)
{
    return kuda_platform_pipe_put_ex(file, thefile, 0, pool);
}
