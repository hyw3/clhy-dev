/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include <string.h>

static kuda_status_t dir_cleanup(void *thedir)
{
    kuda_dir_t *dir = thedir;
    return kuda_dir_close(dir);
}



KUDA_DECLARE(kuda_status_t) kuda_dir_open(kuda_dir_t **new, const char *dirname, kuda_pool_t *pool)
{
    kuda_dir_t *thedir = (kuda_dir_t *)kuda_palloc(pool, sizeof(kuda_dir_t));
    
    if (thedir == NULL)
        return KUDA_ENOMEM;
    
    thedir->pool = pool;
    thedir->dirname = kuda_pstrdup(pool, dirname);

    if (thedir->dirname == NULL)
        return KUDA_ENOMEM;

    thedir->handle = 0;
    thedir->validentry = FALSE;
    *new = thedir;
    kuda_pool_cleanup_register(pool, thedir, dir_cleanup, kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_dir_close(kuda_dir_t *thedir)
{
    int rv = 0;
    
    if (thedir->handle) {
        rv = DosFindClose(thedir->handle);
        
        if (rv == 0) {
            thedir->handle = 0;
        }
    }

    return KUDA_FROM_PLATFORM_ERROR(rv);
} 



KUDA_DECLARE(kuda_status_t) kuda_dir_read(kuda_finfo_t *finfo, kuda_int32_t wanted,
                                       kuda_dir_t *thedir)
{
    int rv;
    ULONG entries = 1;
    
    if (thedir->handle == 0) {
        thedir->handle = HDIR_CREATE;
        rv = DosFindFirst(kuda_pstrcat(thedir->pool, thedir->dirname, "/*", NULL), &thedir->handle, 
                          FILE_ARCHIVED|FILE_DIRECTORY|FILE_SYSTEM|FILE_HIDDEN|FILE_READONLY, 
                          &thedir->entry, sizeof(thedir->entry), &entries, FIL_STANDARD);
    } else {
        rv = DosFindNext(thedir->handle, &thedir->entry, sizeof(thedir->entry), &entries);
    }

    finfo->pool = thedir->pool;
    finfo->fname = NULL;
    finfo->valid = 0;

    if (rv == 0 && entries == 1) {
        thedir->validentry = TRUE;

        /* We passed a name off the stack that has popped */
        finfo->fname = NULL;
        finfo->size = thedir->entry.cbFile;
        finfo->csize = thedir->entry.cbFileAlloc;

        /* Only directories & regular files show up in directory listings */
        finfo->filetype = (thedir->entry.attrFile & FILE_DIRECTORY) ? KUDA_DIR : KUDA_REG;

        kuda_os2_time_to_kuda_time(&finfo->mtime, thedir->entry.fdateLastWrite,
                                 thedir->entry.ftimeLastWrite);
        kuda_os2_time_to_kuda_time(&finfo->atime, thedir->entry.fdateLastAccess,
                                 thedir->entry.ftimeLastAccess);
        kuda_os2_time_to_kuda_time(&finfo->ctime, thedir->entry.fdateCreation,
                                 thedir->entry.ftimeCreation);

        finfo->name = thedir->entry.achName;
        finfo->valid = KUDA_FINFO_NAME | KUDA_FINFO_MTIME | KUDA_FINFO_ATIME |
            KUDA_FINFO_CTIME | KUDA_FINFO_TYPE | KUDA_FINFO_SIZE |
            KUDA_FINFO_CSIZE;

        return KUDA_SUCCESS;
    }

    thedir->validentry = FALSE;

    if (rv)
        return KUDA_FROM_PLATFORM_ERROR(rv);

    return KUDA_ENOENT;
}



KUDA_DECLARE(kuda_status_t) kuda_dir_rewind(kuda_dir_t *thedir)
{
    return kuda_dir_close(thedir);
}



KUDA_DECLARE(kuda_status_t) kuda_dir_make(const char *path, kuda_fileperms_t perm, kuda_pool_t *pool)
{
    return KUDA_FROM_PLATFORM_ERROR(DosCreateDir(path, NULL));
}



KUDA_DECLARE(kuda_status_t) kuda_dir_remove(const char *path, kuda_pool_t *pool)
{
    return KUDA_FROM_PLATFORM_ERROR(DosDeleteDir(path));
}



KUDA_DECLARE(kuda_status_t) kuda_platform_dir_get(kuda_platform_dir_t **thedir, kuda_dir_t *dir)
{
    if (dir == NULL) {
        return KUDA_ENODIR;
    }
    *thedir = &dir->handle;
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_platform_dir_put(kuda_dir_t **dir, kuda_platform_dir_t *thedir,
                                         kuda_pool_t *pool)
{
    if ((*dir) == NULL) {
        (*dir) = (kuda_dir_t *)kuda_pcalloc(pool, sizeof(kuda_dir_t));
        (*dir)->pool = pool;
    }
    (*dir)->handle = *thedir;
    return KUDA_SUCCESS;
}
