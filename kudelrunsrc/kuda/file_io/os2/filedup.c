/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include <string.h>
#include "kuda_arch_inherit.h"

static kuda_status_t file_dup(kuda_file_t **new_file, kuda_file_t *old_file, kuda_pool_t *p)
{
    int rv;
    kuda_file_t *dup_file;

    if (*new_file == NULL) {
        dup_file = (kuda_file_t *)kuda_palloc(p, sizeof(kuda_file_t));

        if (dup_file == NULL) {
            return KUDA_ENOMEM;
        }

        dup_file->filedes = -1;
    } else {
      dup_file = *new_file;
    }

    dup_file->pool = p;
    rv = DosDupHandle(old_file->filedes, &dup_file->filedes);

    if (rv) {
        return KUDA_FROM_PLATFORM_ERROR(rv);
    }

    dup_file->fname = kuda_pstrdup(dup_file->pool, old_file->fname);
    dup_file->buffered = old_file->buffered;
    dup_file->isopen = old_file->isopen;
    dup_file->flags = old_file->flags & ~KUDA_INHERIT;
    /* TODO - dup pipes correctly */
    dup_file->pipe = old_file->pipe;

    if (*new_file == NULL) {
        kuda_pool_cleanup_register(dup_file->pool, dup_file, kuda_file_cleanup,
                            kuda_pool_cleanup_null);
        *new_file = dup_file;
    }

    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_file_dup(kuda_file_t **new_file, kuda_file_t *old_file, kuda_pool_t *p)
{
  if (*new_file) {
      kuda_file_close(*new_file);
      (*new_file)->filedes = -1;
  }

  return file_dup(new_file, old_file, p);
}



KUDA_DECLARE(kuda_status_t) kuda_file_dup2(kuda_file_t *new_file, kuda_file_t *old_file, kuda_pool_t *p)
{
  return file_dup(&new_file, old_file, p);
}



KUDA_DECLARE(kuda_status_t) kuda_file_setaside(kuda_file_t **new_file,
                                            kuda_file_t *old_file,
                                            kuda_pool_t *p)
{
    *new_file = (kuda_file_t *)kuda_pmemdup(p, old_file, sizeof(kuda_file_t));
    (*new_file)->pool = p;

    if (old_file->buffered) {
        (*new_file)->buffer = kuda_palloc(p, old_file->bufsize);
        (*new_file)->bufsize = old_file->bufsize;

        if (old_file->direction == 1) {
            memcpy((*new_file)->buffer, old_file->buffer, old_file->bufpos);
        }
        else {
            memcpy((*new_file)->buffer, old_file->buffer, old_file->dataRead);
        }

        if (old_file->mutex) {
            kuda_thread_mutex_create(&((*new_file)->mutex),
                                    KUDA_THREAD_MUTEX_DEFAULT, p);
            kuda_thread_mutex_destroy(old_file->mutex);
        }
    }

    if (old_file->fname) {
        (*new_file)->fname = kuda_pstrdup(p, old_file->fname);
    }

    if (!(old_file->flags & KUDA_FOPEN_NOCLEANUP)) {
        kuda_pool_cleanup_register(p, (void *)(*new_file), 
                                  kuda_file_cleanup,
                                  kuda_file_cleanup);
    }

    old_file->filedes = -1;
    kuda_pool_cleanup_kill(old_file->pool, (void *)old_file,
                          kuda_file_cleanup);

    return KUDA_SUCCESS;
}
