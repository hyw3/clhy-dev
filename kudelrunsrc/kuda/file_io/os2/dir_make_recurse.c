/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_file_io.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include <string.h>

#define IS_SEP(c) (c == '/' || c == '\\')

/* Remove trailing separators that don't affect the meaning of PATH. */
static const char *path_canonicalize(const char *path, kuda_pool_t *pool)
{
    /* At some point this could eliminate redundant components.  For
     * now, it just makes sure there is no trailing slash. */
    kuda_size_t len = strlen(path);
    kuda_size_t orig_len = len;

    while ((len > 0) && IS_SEP(path[len - 1])) {
        len--;
    }

    if (len != orig_len) {
        return kuda_pstrndup(pool, path, len);
    }
    else {
        return path;
    }
}



/* Remove one component off the end of PATH. */
static char *path_remove_last_component(const char *path, kuda_pool_t *pool)
{
    const char *newpath = path_canonicalize(path, pool);
    int i;

    for (i = strlen(newpath) - 1; i >= 0; i--) {
        if (IS_SEP(path[i])) {
            break;
        }
    }

    return kuda_pstrndup(pool, path, (i < 0) ? 0 : i);
}



kuda_status_t kuda_dir_make_recursive(const char *path, kuda_fileperms_t perm,
                                    kuda_pool_t *pool)
{
    kuda_status_t kuda_err = KUDA_SUCCESS;
    
    kuda_err = kuda_dir_make(path, perm, pool); /* Try to make PATH right out */

    if (KUDA_STATUS_IS_ENOENT(kuda_err)) { /* Missing an intermediate dir */
        char *dir;

        dir = path_remove_last_component(path, pool);
        kuda_err = kuda_dir_make_recursive(dir, perm, pool);

        if (!kuda_err) {
            kuda_err = kuda_dir_make(path, perm, pool);
        }
    }

    /*
     * It's OK if PATH exists. Timing issues can lead to the second
     * kuda_dir_make being called on existing dir, therefore this check
     * has to come last.
     */
    if (KUDA_STATUS_IS_EEXIST(kuda_err))
        return KUDA_SUCCESS;

    return kuda_err;
}
