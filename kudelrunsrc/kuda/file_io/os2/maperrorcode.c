/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define INCL_DOSERRORS
#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include <errno.h>
#include <string.h>
#include "kuda_errno.h"

static int errormap[][2] = {
    { NO_ERROR,                   KUDA_SUCCESS      },
    { ERROR_FILE_NOT_FOUND,       KUDA_ENOENT       },
    { ERROR_PATH_NOT_FOUND,       KUDA_ENOENT       },
    { ERROR_TOO_MANY_OPEN_FILES,  KUDA_EMFILE       },
    { ERROR_ACCESS_DENIED,        KUDA_EACCES       },
    { ERROR_SHARING_VIOLATION,    KUDA_EACCES       },
    { ERROR_INVALID_PARAMETER,    KUDA_EINVAL       },
    { ERROR_OPEN_FAILED,          KUDA_ENOENT       },
    { ERROR_DISK_FULL,            KUDA_ENOSPC       },
    { ERROR_FILENAME_EXCED_RANGE, KUDA_ENAMETOOLONG },
    { ERROR_INVALID_FUNCTION,     KUDA_EINVAL       },
    { ERROR_INVALID_HANDLE,       KUDA_EBADF        },
    { ERROR_NEGATIVE_SEEK,        KUDA_ESPIPE       },
    { ERROR_NO_SIGNAL_SENT,       ESRCH            },
    { ERROR_NO_DATA,              KUDA_EAGAIN       },
    { SOCEINTR,                 EINTR           },
    { SOCEWOULDBLOCK,           EWOULDBLOCK     },
    { SOCEINPROGRESS,           EINPROGRESS     },
    { SOCEALREADY,              EALREADY        },
    { SOCENOTSOCK,              ENOTSOCK        },
    { SOCEDESTADDRREQ,          EDESTADDRREQ    },
    { SOCEMSGSIZE,              EMSGSIZE        },
    { SOCEPROTOTYPE,            EPROTOTYPE      },
    { SOCENOPROTOOPT,           ENOPROTOOPT     },
    { SOCEPROTONOSUPPORT,       EPROTONOSUPPORT },
    { SOCESOCKTNOSUPPORT,       ESOCKTNOSUPPORT },
    { SOCEOPNOTSUPP,            EOPNOTSUPP      },
    { SOCEPFNOSUPPORT,          EPFNOSUPPORT    },
    { SOCEAFNOSUPPORT,          EAFNOSUPPORT    },
    { SOCEADDRINUSE,            EADDRINUSE      },
    { SOCEADDRNOTAVAIL,         EADDRNOTAVAIL   },
    { SOCENETDOWN,              ENETDOWN        },
    { SOCENETUNREACH,           ENETUNREACH     },
    { SOCENETRESET,             ENETRESET       },
    { SOCECONNABORTED,          ECONNABORTED    },
    { SOCECONNRESET,            ECONNRESET      },
    { SOCENOBUFS,               ENOBUFS         },
    { SOCEISCONN,               EISCONN         },
    { SOCENOTCONN,              ENOTCONN        },
    { SOCESHUTDOWN,             ESHUTDOWN       },
    { SOCETOOMANYREFS,          ETOOMANYREFS    },
    { SOCETIMEDOUT,             ETIMEDOUT       },
    { SOCECONNREFUSED,          ECONNREFUSED    },
    { SOCELOOP,                 ELOOP           },
    { SOCENAMETOOLONG,          ENAMETOOLONG    },
    { SOCEHOSTDOWN,             EHOSTDOWN       },
    { SOCEHOSTUNREACH,          EHOSTUNREACH    },
    { SOCENOTEMPTY,             ENOTEMPTY       },
    { SOCEPIPE,                 EPIPE           }
};

#define MAPSIZE (sizeof(errormap)/sizeof(errormap[0]))

int kuda_canonical_error(kuda_status_t err)
{
    int rv = -1, index;

    if (err < KUDA_PLATFORM_START_SYSERR)
        return err;

    err -= KUDA_PLATFORM_START_SYSERR;

    for (index=0; index<MAPSIZE && errormap[index][0] != err; index++);
    
    if (index<MAPSIZE)
        rv = errormap[index][1];
    else
        fprintf(stderr, "kuda_canonical_error: Unknown OS2 error code %d\n", err );
        
    return rv;
}
