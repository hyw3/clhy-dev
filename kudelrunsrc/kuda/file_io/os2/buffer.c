/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_thread_mutex.h"

KUDA_DECLARE(kuda_status_t) kuda_file_buffer_set(kuda_file_t *file, 
                                              char * buffer,
                                              kuda_size_t bufsize)
{
    kuda_status_t rv;

    kuda_thread_mutex_lock(file->mutex);
 
    if(file->buffered) {
        /* Flush the existing buffer */
        rv = kuda_file_flush(file);
        if (rv != KUDA_SUCCESS) {
            kuda_thread_mutex_unlock(file->mutex);
            return rv;
        }
    }
        
    file->buffer = buffer;
    file->bufsize = bufsize;
    file->buffered = 1;
    file->bufpos = 0;
    file->direction = 0;
    file->dataRead = 0;
 
    if (file->bufsize == 0) {
            /* Setting the buffer size to zero is equivalent to turning 
             * buffering off. 
             */
            file->buffered = 0;
    }
    
    kuda_thread_mutex_unlock(file->mutex);

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_size_t) kuda_file_buffer_size_get(kuda_file_t *file)
{
    return file->bufsize;
}
