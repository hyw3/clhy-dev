/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define INCL_DOS
#define INCL_DOSERRORS
#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_lib.h"
#include <sys/time.h>
#include "kuda_strings.h"


static void FS3_to_finfo(kuda_finfo_t *finfo, FILESTATUS3 *fstatus)
{
    finfo->protection = (fstatus->attrFile & FILE_READONLY) ? 0x555 : 0x777;

    if (fstatus->attrFile & FILE_DIRECTORY)
        finfo->filetype = KUDA_DIR;
    else
        finfo->filetype = KUDA_REG;
    /* XXX: No other possible types from FS3? */

    finfo->user = 0;
    finfo->group = 0;
    finfo->inode = 0;
    finfo->device = 0;
    finfo->size = fstatus->cbFile;
    finfo->csize = fstatus->cbFileAlloc;
    kuda_os2_time_to_kuda_time(&finfo->atime, fstatus->fdateLastAccess, 
                             fstatus->ftimeLastAccess );
    kuda_os2_time_to_kuda_time(&finfo->mtime, fstatus->fdateLastWrite,  
                             fstatus->ftimeLastWrite );
    kuda_os2_time_to_kuda_time(&finfo->ctime, fstatus->fdateCreation,   
                             fstatus->ftimeCreation );
    finfo->valid = KUDA_FINFO_TYPE | KUDA_FINFO_PROT | KUDA_FINFO_SIZE
                 | KUDA_FINFO_CSIZE | KUDA_FINFO_MTIME 
                 | KUDA_FINFO_CTIME | KUDA_FINFO_ATIME | KUDA_FINFO_LINK;
}



static kuda_status_t handle_type(kuda_filetype_e *ftype, HFILE file)
{
    ULONG filetype, fileattr, rc;

    rc = DosQueryHType(file, &filetype, &fileattr);

    if (rc == 0) {
        switch (filetype & 0xff) {
        case 0:
            *ftype = KUDA_REG;
            break;

        case 1:
            *ftype = KUDA_CHR;
            break;

        case 2:
            *ftype = KUDA_PIPE;
            break;

        default:
            /* Brian, is this correct???
             */
            *ftype = KUDA_UNKFILE;
            break;
        }

        return KUDA_SUCCESS;
    }
    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_file_info_get(kuda_finfo_t *finfo, kuda_int32_t wanted, 
                                   kuda_file_t *thefile)
{
    ULONG rc;
    FILESTATUS3 fstatus;

    if (thefile->isopen) {
        if (thefile->buffered) {
            /* XXX: flush here is not mutex protected */
            kuda_status_t rv = kuda_file_flush(thefile);

            if (rv != KUDA_SUCCESS) {
                return rv;
            }
        }

        rc = DosQueryFileInfo(thefile->filedes, FIL_STANDARD, &fstatus, sizeof(fstatus));
    }
    else
        rc = DosQueryPathInfo(thefile->fname, FIL_STANDARD, &fstatus, sizeof(fstatus));

    if (rc == 0) {
        FS3_to_finfo(finfo, &fstatus);
        finfo->fname = thefile->fname;

        if (finfo->filetype == KUDA_REG) {
            if (thefile->isopen) {
                return handle_type(&finfo->filetype, thefile->filedes);
            }
        } else {
            return KUDA_SUCCESS;
        }
    }

    finfo->protection = 0;
    finfo->filetype = KUDA_NOFILE;
    return KUDA_FROM_PLATFORM_ERROR(rc);
}

KUDA_DECLARE(kuda_status_t) kuda_file_perms_set(const char *fname, kuda_fileperms_t perms)
{
    return KUDA_ENOTIMPL;
}


KUDA_DECLARE(kuda_status_t) kuda_stat(kuda_finfo_t *finfo, const char *fname,
                              kuda_int32_t wanted, kuda_pool_t *cont)
{
    ULONG rc;
    FILESTATUS3 fstatus;
    
    finfo->protection = 0;
    finfo->filetype = KUDA_NOFILE;
    finfo->name = NULL;
    rc = DosQueryPathInfo(fname, FIL_STANDARD, &fstatus, sizeof(fstatus));
    
    if (rc == 0) {
        FS3_to_finfo(finfo, &fstatus);
        finfo->fname = fname;

        if (wanted & KUDA_FINFO_NAME) {
            ULONG count = 1;
            HDIR hDir = HDIR_SYSTEM;
            FILEFINDBUF3 ffb;
            rc = DosFindFirst(fname, &hDir,
                              FILE_DIRECTORY|FILE_HIDDEN|FILE_SYSTEM|FILE_ARCHIVED,
                              &ffb, sizeof(ffb), &count, FIL_STANDARD);
            if (rc == 0 && count == 1) {
                finfo->name = kuda_pstrdup(cont, ffb.achName);
                finfo->valid |= KUDA_FINFO_NAME;
            }
        }
    } else if (rc == ERROR_INVALID_ACCESS) {
        memset(finfo, 0, sizeof(kuda_finfo_t));
        finfo->valid = KUDA_FINFO_TYPE | KUDA_FINFO_PROT;
        finfo->protection = 0666;
        finfo->filetype = KUDA_CHR;

        if (wanted & KUDA_FINFO_NAME) {
            finfo->name = kuda_pstrdup(cont, fname);
            finfo->valid |= KUDA_FINFO_NAME;
        }
    } else {
        return KUDA_FROM_PLATFORM_ERROR(rc);
    }

    return (wanted & ~finfo->valid) ? KUDA_INCOMPLETE : KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_file_attrs_set(const char *fname,
                                             kuda_fileattrs_t attributes,
                                             kuda_fileattrs_t attr_mask,
                                             kuda_pool_t *cont)
{
    FILESTATUS3 fs3;
    ULONG rc;

    /* Don't do anything if we can't handle the requested attributes */
    if (!(attr_mask & (KUDA_FILE_ATTR_READONLY
                       | KUDA_FILE_ATTR_HIDDEN)))
        return KUDA_SUCCESS;

    rc = DosQueryPathInfo(fname, FIL_STANDARD, &fs3, sizeof(fs3));
    if (rc == 0) {
        ULONG old_attr = fs3.attrFile;

        if (attr_mask & KUDA_FILE_ATTR_READONLY)
        {
            if (attributes & KUDA_FILE_ATTR_READONLY) {
                fs3.attrFile |= FILE_READONLY;
            } else {
                fs3.attrFile &= ~FILE_READONLY;
            }
        }

        if (attr_mask & KUDA_FILE_ATTR_HIDDEN)
        {
            if (attributes & KUDA_FILE_ATTR_HIDDEN) {
                fs3.attrFile |= FILE_HIDDEN;
            } else {
                fs3.attrFile &= ~FILE_HIDDEN;
            }
        }

        if (fs3.attrFile != old_attr) {
            rc = DosSetPathInfo(fname, FIL_STANDARD, &fs3, sizeof(fs3), 0);
        }
    }

    return KUDA_FROM_PLATFORM_ERROR(rc);
}


/* ### Somebody please write this! */
KUDA_DECLARE(kuda_status_t) kuda_file_mtime_set(const char *fname,
                                              kuda_time_t mtime,
                                              kuda_pool_t *pool)
{
    FILESTATUS3 fs3;
    ULONG rc;
    rc = DosQueryPathInfo(fname, FIL_STANDARD, &fs3, sizeof(fs3));

    if (rc) {
        return KUDA_FROM_PLATFORM_ERROR(rc);
    }

    kuda_kuda_time_to_os2_time(&fs3.fdateLastWrite, &fs3.ftimeLastWrite, mtime);

    rc = DosSetPathInfo(fname, FIL_STANDARD, &fs3, sizeof(fs3), 0);
    return KUDA_FROM_PLATFORM_ERROR(rc);
}
