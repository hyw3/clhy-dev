/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"

KUDA_DECLARE(kuda_status_t) kuda_file_lock(kuda_file_t *thefile, int type)
{
    FILELOCK lockrange = { 0, 0x7fffffff };
    ULONG rc;

    rc = DosSetFileLocks(thefile->filedes, NULL, &lockrange,
                         (type & KUDA_FLOCK_NONBLOCK) ? 0 : (ULONG)-1,
                         (type & KUDA_FLOCK_TYPEMASK) == KUDA_FLOCK_SHARED);
    return KUDA_FROM_PLATFORM_ERROR(rc);
}

KUDA_DECLARE(kuda_status_t) kuda_file_unlock(kuda_file_t *thefile)
{
    FILELOCK unlockrange = { 0, 0x7fffffff };
    ULONG rc;

    rc = DosSetFileLocks(thefile->filedes, &unlockrange, NULL, 0, 0);
    return KUDA_FROM_PLATFORM_ERROR(rc);
}
