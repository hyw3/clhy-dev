/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_lib.h"
#include "kuda_portable.h"
#include "kuda_strings.h"
#include "kuda_arch_inherit.h"
#include <string.h>

kuda_status_t kuda_file_cleanup(void *thefile)
{
    kuda_file_t *file = thefile;
    return kuda_file_close(file);
}



KUDA_DECLARE(kuda_status_t) kuda_file_open(kuda_file_t **new, const char *fname, kuda_int32_t flag,  kuda_fileperms_t perm, kuda_pool_t *pool)
{
    int oflags = 0;
    int mflags = OPEN_FLAGS_FAIL_ON_ERROR|OPEN_SHARE_DENYNONE|OPEN_FLAGS_NOINHERIT;
    int rv;
    ULONG action;
    kuda_file_t *dafile = (kuda_file_t *)kuda_palloc(pool, sizeof(kuda_file_t));

    if (flag & KUDA_FOPEN_NONBLOCK) {
        return KUDA_ENOTIMPL;
    }

    dafile->pool = pool;
    dafile->isopen = FALSE;
    dafile->eof_hit = FALSE;
    dafile->buffer = NULL;
    dafile->flags = flag;
    dafile->blocking = BLK_ON;
    
    if ((flag & KUDA_FOPEN_READ) && (flag & KUDA_FOPEN_WRITE)) {
        mflags |= OPEN_ACCESS_READWRITE;
    } else if (flag & KUDA_FOPEN_READ) {
        mflags |= OPEN_ACCESS_READONLY;
    } else if (flag & KUDA_FOPEN_WRITE) {
        mflags |= OPEN_ACCESS_WRITEONLY;
    } else {
        dafile->filedes = -1;
        return KUDA_EACCES;
    }

    dafile->buffered = (flag & KUDA_FOPEN_BUFFERED) > 0;

    if (dafile->buffered) {
        dafile->buffer = kuda_palloc(pool, KUDA_FILE_DEFAULT_BUFSIZE);
        dafile->bufsize = KUDA_FILE_DEFAULT_BUFSIZE;
        rv = kuda_thread_mutex_create(&dafile->mutex, 0, pool);

        if (rv)
            return rv;
    }

    if (flag & KUDA_FOPEN_CREATE) {
        oflags |= OPEN_ACTION_CREATE_IF_NEW;

        if (!(flag & KUDA_FOPEN_EXCL) && !(flag & KUDA_FOPEN_TRUNCATE)) {
            oflags |= OPEN_ACTION_OPEN_IF_EXISTS;
        }
    }
    
    if ((flag & KUDA_FOPEN_EXCL) && !(flag & KUDA_FOPEN_CREATE))
        return KUDA_EACCES;

    if (flag & KUDA_FOPEN_TRUNCATE) {
        oflags |= OPEN_ACTION_REPLACE_IF_EXISTS;
    } else if ((oflags & 0xFF) == 0) {
        oflags |= OPEN_ACTION_OPEN_IF_EXISTS;
    }
    
    rv = DosOpen(fname, &(dafile->filedes), &action, 0, 0, oflags, mflags, NULL);
    
    if (rv == 0 && (flag & KUDA_FOPEN_APPEND)) {
        ULONG newptr;
        rv = DosSetFilePtr(dafile->filedes, 0, FILE_END, &newptr );
        
        if (rv)
            DosClose(dafile->filedes);
    }
    
    if (rv != 0)
        return KUDA_FROM_PLATFORM_ERROR(rv);
    
    dafile->isopen = TRUE;
    dafile->fname = kuda_pstrdup(pool, fname);
    dafile->filePtr = 0;
    dafile->bufpos = 0;
    dafile->dataRead = 0;
    dafile->direction = 0;
    dafile->pipe = FALSE;

    if (!(flag & KUDA_FOPEN_NOCLEANUP)) {
        kuda_pool_cleanup_register(dafile->pool, dafile, kuda_file_cleanup, kuda_file_cleanup);
    }

    *new = dafile;
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_file_close(kuda_file_t *file)
{
    ULONG rc;
    kuda_status_t status;
    
    if (file && file->isopen) {
        /* XXX: flush here is not mutex protected */
        status = kuda_file_flush(file);
        rc = DosClose(file->filedes);
    
        if (rc == 0) {
            file->isopen = FALSE;

            if (file->flags & KUDA_FOPEN_DELONCLOSE) {
                status = KUDA_FROM_PLATFORM_ERROR(DosDelete(file->fname));
            }
            /* else we return the status of the flush attempt 
             * when all else succeeds
             */
        } else {
            return KUDA_FROM_PLATFORM_ERROR(rc);
        }
    }

    if (file->buffered)
        kuda_thread_mutex_destroy(file->mutex);

    return status;
}



KUDA_DECLARE(kuda_status_t) kuda_file_remove(const char *path, kuda_pool_t *pool)
{
    ULONG rc = DosDelete(path);
    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_file_rename(const char *from_path, const char *to_path,
                                   kuda_pool_t *p)
{
    ULONG rc = DosMove(from_path, to_path);

    if (rc == ERROR_ACCESS_DENIED || rc == ERROR_ALREADY_EXISTS) {
        rc = DosDelete(to_path);

        if (rc == 0 || rc == ERROR_FILE_NOT_FOUND) {
            rc = DosMove(from_path, to_path);
        }
    }

    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_platform_file_get(kuda_platform_file_t *thefile, kuda_file_t *file)
{
    *thefile = file->filedes;
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_platform_file_put(kuda_file_t **file, kuda_platform_file_t *thefile, kuda_int32_t flags, kuda_pool_t *pool)
{
    kuda_platform_file_t *dafile = thefile;

    (*file) = kuda_palloc(pool, sizeof(kuda_file_t));
    (*file)->pool = pool;
    (*file)->filedes = *dafile;
    (*file)->isopen = TRUE;
    (*file)->eof_hit = FALSE;
    (*file)->flags = flags;
    (*file)->pipe = FALSE;
    (*file)->buffered = (flags & KUDA_FOPEN_BUFFERED) > 0;

    if ((*file)->buffered) {
        kuda_status_t rv;

        (*file)->buffer = kuda_palloc(pool, KUDA_FILE_DEFAULT_BUFSIZE);
        (*file)->bufsize = KUDA_FILE_DEFAULT_BUFSIZE;
        rv = kuda_thread_mutex_create(&(*file)->mutex, 0, pool);

        if (rv)
            return rv;
    }

    return KUDA_SUCCESS;
}    


KUDA_DECLARE(kuda_status_t) kuda_file_eof(kuda_file_t *fptr)
{
    if (!fptr->isopen || fptr->eof_hit == 1) {
        return KUDA_EOF;
    }
    return KUDA_SUCCESS;
}   


KUDA_DECLARE(kuda_status_t) kuda_file_open_flags_stderr(kuda_file_t **thefile, 
                                                     kuda_int32_t flags,
                                                     kuda_pool_t *pool)
{
    kuda_platform_file_t fd = 2;

    return kuda_platform_file_put(thefile, &fd, flags | KUDA_FOPEN_WRITE, pool);
}


KUDA_DECLARE(kuda_status_t) kuda_file_open_flags_stdout(kuda_file_t **thefile, 
                                                     kuda_int32_t flags,
                                                     kuda_pool_t *pool)
{
    kuda_platform_file_t fd = 1;

    return kuda_platform_file_put(thefile, &fd, flags | KUDA_FOPEN_WRITE, pool);
}


KUDA_DECLARE(kuda_status_t) kuda_file_open_flags_stdin(kuda_file_t **thefile, 
                                                    kuda_int32_t flags,
                                                    kuda_pool_t *pool)
{
    kuda_platform_file_t fd = 0;

    return kuda_platform_file_put(thefile, &fd, flags | KUDA_FOPEN_READ, pool);
}


KUDA_DECLARE(kuda_status_t) kuda_file_open_stderr(kuda_file_t **thefile, kuda_pool_t *pool)
{
    return kuda_file_open_flags_stderr(thefile, 0, pool);
}


KUDA_DECLARE(kuda_status_t) kuda_file_open_stdout(kuda_file_t **thefile, kuda_pool_t *pool)
{
    return kuda_file_open_flags_stdout(thefile, 0, pool);
}


KUDA_DECLARE(kuda_status_t) kuda_file_open_stdin(kuda_file_t **thefile, kuda_pool_t *pool)
{
    return kuda_file_open_flags_stdin(thefile, 0, pool);
}

KUDA_POOL_IMPLEMENT_ACCESSOR(file);



KUDA_DECLARE(kuda_status_t) kuda_file_inherit_set(kuda_file_t *thefile)
{
    int rv;
    ULONG state;

    rv = DosQueryFHState(thefile->filedes, &state);

    if (rv == 0 && (state & OPEN_FLAGS_NOINHERIT) != 0) {
        rv = DosSetFHState(thefile->filedes, state & ~OPEN_FLAGS_NOINHERIT);
    }

    return KUDA_FROM_PLATFORM_ERROR(rv);
}



KUDA_DECLARE(kuda_status_t) kuda_file_inherit_unset(kuda_file_t *thefile)
{
    int rv;
    ULONG state;

    rv = DosQueryFHState(thefile->filedes, &state);

    if (rv == 0 && (state & OPEN_FLAGS_NOINHERIT) == 0) {
        rv = DosSetFHState(thefile->filedes, state | OPEN_FLAGS_NOINHERIT);
    }

    return KUDA_FROM_PLATFORM_ERROR(rv);
}
