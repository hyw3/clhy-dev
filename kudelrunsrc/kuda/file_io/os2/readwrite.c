/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define INCL_DOS
#define INCL_DOSERRORS

#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_lib.h"
#include "kuda_strings.h"

#include <malloc.h>

KUDA_DECLARE(kuda_status_t) kuda_file_read(kuda_file_t *thefile, void *buf, kuda_size_t *nbytes)
{
    ULONG rc = 0;
    ULONG bytesread;

    if (!thefile->isopen) {
        *nbytes = 0;
        return KUDA_EBADF;
    }

    if (thefile->buffered) {
        char *pos = (char *)buf;
        ULONG blocksize;
        ULONG size = *nbytes;

        kuda_thread_mutex_lock(thefile->mutex);

        if (thefile->direction == 1) {
            int rv = kuda_file_flush(thefile);

            if (rv != KUDA_SUCCESS) {
                kuda_thread_mutex_unlock(thefile->mutex);
                return rv;
            }

            thefile->bufpos = 0;
            thefile->direction = 0;
            thefile->dataRead = 0;
        }

        while (rc == 0 && size > 0) {
            if (thefile->bufpos >= thefile->dataRead) {
                ULONG bytesread;
                rc = DosRead(thefile->filedes, thefile->buffer,
                             thefile->bufsize, &bytesread);

                if (bytesread == 0) {
                    if (rc == 0)
                        thefile->eof_hit = TRUE;
                    break;
                }

                thefile->dataRead = bytesread;
                thefile->filePtr += thefile->dataRead;
                thefile->bufpos = 0;
            }

            blocksize = size > thefile->dataRead - thefile->bufpos ? thefile->dataRead - thefile->bufpos : size;
            memcpy(pos, thefile->buffer + thefile->bufpos, blocksize);
            thefile->bufpos += blocksize;
            pos += blocksize;
            size -= blocksize;
        }

        *nbytes = rc == 0 ? pos - (char *)buf : 0;
        kuda_thread_mutex_unlock(thefile->mutex);

        if (*nbytes == 0 && rc == 0 && thefile->eof_hit) {
            return KUDA_EOF;
        }

        return KUDA_FROM_PLATFORM_ERROR(rc);
    } else {
        if (thefile->pipe)
            DosResetEventSem(thefile->pipeSem, &rc);

        rc = DosRead(thefile->filedes, buf, *nbytes, &bytesread);

        if (rc == ERROR_NO_DATA && thefile->timeout != 0) {
            int rcwait = DosWaitEventSem(thefile->pipeSem, thefile->timeout >= 0 ? thefile->timeout / 1000 : SEM_INDEFINITE_WAIT);

            if (rcwait == 0) {
                rc = DosRead(thefile->filedes, buf, *nbytes, &bytesread);
            }
            else if (rcwait == ERROR_TIMEOUT) {
                *nbytes = 0;
                return KUDA_TIMEUP;
            }
        }

        if (rc) {
            *nbytes = 0;
            return KUDA_FROM_PLATFORM_ERROR(rc);
        }

        *nbytes = bytesread;
        
        if (bytesread == 0) {
            thefile->eof_hit = TRUE;
            return KUDA_EOF;
        }

        return KUDA_SUCCESS;
    }
}



KUDA_DECLARE(kuda_status_t) kuda_file_write(kuda_file_t *thefile, const void *buf, kuda_size_t *nbytes)
{
    ULONG rc = 0;
    ULONG byteswritten;

    if (!thefile->isopen) {
        *nbytes = 0;
        return KUDA_EBADF;
    }

    if (thefile->buffered) {
        char *pos = (char *)buf;
        int blocksize;
        int size = *nbytes;

        kuda_thread_mutex_lock(thefile->mutex);

        if ( thefile->direction == 0 ) {
            /* Position file pointer for writing at the offset we are logically reading from */
            ULONG offset = thefile->filePtr - thefile->dataRead + thefile->bufpos;
            if (offset != thefile->filePtr)
                DosSetFilePtr(thefile->filedes, offset, FILE_BEGIN, &thefile->filePtr );
            thefile->bufpos = thefile->dataRead = 0;
            thefile->direction = 1;
        }

        while (rc == 0 && size > 0) {
            if (thefile->bufpos == thefile->bufsize) /* write buffer is full */
                /* XXX bug; - rc is double-transformed platforms->kuda below */
                rc = kuda_file_flush(thefile);

            blocksize = size > thefile->bufsize - thefile->bufpos ? thefile->bufsize - thefile->bufpos : size;
            memcpy(thefile->buffer + thefile->bufpos, pos, blocksize);
            thefile->bufpos += blocksize;
            pos += blocksize;
            size -= blocksize;
        }

        kuda_thread_mutex_unlock(thefile->mutex);
        return KUDA_FROM_PLATFORM_ERROR(rc);
    } else {
        if (thefile->flags & KUDA_FOPEN_APPEND) {
            FILELOCK all = { 0, 0x7fffffff };
            ULONG newpos;
            rc = DosSetFileLocks(thefile->filedes, NULL, &all, -1, 0);

            if (rc == 0) {
                rc = DosSetFilePtr(thefile->filedes, 0, FILE_END, &newpos);

                if (rc == 0) {
                    rc = DosWrite(thefile->filedes, buf, *nbytes, &byteswritten);
                }

                DosSetFileLocks(thefile->filedes, &all, NULL, -1, 0);
            }
        } else {
            rc = DosWrite(thefile->filedes, buf, *nbytes, &byteswritten);
        }

        if (rc) {
            *nbytes = 0;
            return KUDA_FROM_PLATFORM_ERROR(rc);
        }

        *nbytes = byteswritten;
        return KUDA_SUCCESS;
    }
}



#ifdef HAVE_WRITEV

KUDA_DECLARE(kuda_status_t) kuda_file_writev(kuda_file_t *thefile, const struct iovec *vec, kuda_size_t nvec, kuda_size_t *nbytes)
{
    int bytes;

    if (thefile->buffered) {
        kuda_status_t rv = kuda_file_flush(thefile);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
    }

    if ((bytes = writev(thefile->filedes, vec, nvec)) < 0) {
        *nbytes = 0;
        return errno;
    }
    else {
        *nbytes = bytes;
        return KUDA_SUCCESS;
    }
}
#endif



KUDA_DECLARE(kuda_status_t) kuda_file_putc(char ch, kuda_file_t *thefile)
{
    ULONG rc;
    ULONG byteswritten;

    if (!thefile->isopen) {
        return KUDA_EBADF;
    }

    rc = DosWrite(thefile->filedes, &ch, 1, &byteswritten);

    if (rc) {
        return KUDA_FROM_PLATFORM_ERROR(rc);
    }
    
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_file_ungetc(char ch, kuda_file_t *thefile)
{
    kuda_off_t offset = -1;
    return kuda_file_seek(thefile, KUDA_CUR, &offset);
}


KUDA_DECLARE(kuda_status_t) kuda_file_getc(char *ch, kuda_file_t *thefile)
{
    ULONG rc;
    kuda_size_t bytesread;

    if (!thefile->isopen) {
        return KUDA_EBADF;
    }

    bytesread = 1;
    rc = kuda_file_read(thefile, ch, &bytesread);

    if (rc) {
        return rc;
    }
    
    if (bytesread == 0) {
        thefile->eof_hit = TRUE;
        return KUDA_EOF;
    }
    
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_file_puts(const char *str, kuda_file_t *thefile)
{
    kuda_size_t len;

    len = strlen(str);
    return kuda_file_write(thefile, str, &len); 
}


KUDA_DECLARE(kuda_status_t) kuda_file_flush(kuda_file_t *thefile)
{
    if (thefile->buffered) {
        ULONG written = 0;
        int rc = 0;

        if (thefile->direction == 1 && thefile->bufpos) {
            rc = DosWrite(thefile->filedes, thefile->buffer, thefile->bufpos, &written);
            thefile->filePtr += written;

            if (rc == 0)
                thefile->bufpos = 0;
        }

        return KUDA_FROM_PLATFORM_ERROR(rc);
    } else {
        /* There isn't anything to do if we aren't buffering the output
         * so just return success.
         */
        return KUDA_SUCCESS;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_file_sync(kuda_file_t *thefile)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_file_datasync(kuda_file_t *thefile)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_file_gets(char *str, int len, kuda_file_t *thefile)
{
    kuda_size_t readlen;
    kuda_status_t rv = KUDA_SUCCESS;
    int i;    

    for (i = 0; i < len-1; i++) {
        readlen = 1;
        rv = kuda_file_read(thefile, str+i, &readlen);

        if (rv != KUDA_SUCCESS) {
            break;
        }

        if (readlen != 1) {
            rv = KUDA_EOF;
            break;
        }
        
        if (str[i] == '\n') {
            i++;
            break;
        }
    }
    str[i] = 0;
    if (i > 0) {
        /* we stored chars; don't report EOF or any other errors;
         * the app will find out about that on the next call
         */
        return KUDA_SUCCESS;
    }
    return rv;
}



KUDA_DECLARE_NONSTD(int) kuda_file_printf(kuda_file_t *fptr, 
                                        const char *format, ...)
{
    int cc;
    va_list clhy;
    char *buf;
    int len;

    buf = malloc(HUGE_STRING_LEN);
    if (buf == NULL) {
        return 0;
    }
    va_start(clhy, format);
    len = kuda_vsnprintf(buf, HUGE_STRING_LEN, format, clhy);
    cc = kuda_file_puts(buf, fptr);
    va_end(clhy);
    free(buf);
    return (cc == KUDA_SUCCESS) ? len : -1;
}



kuda_status_t kuda_file_check_read(kuda_file_t *fd)
{
    int rc;

    if (!fd->pipe)
        return KUDA_SUCCESS; /* Not a pipe, assume no waiting */

    rc = DosWaitEventSem(fd->pipeSem, SEM_IMMEDIATE_RETURN);

    if (rc == ERROR_TIMEOUT)
        return KUDA_TIMEUP;

    return KUDA_FROM_PLATFORM_ERROR(rc);
}
