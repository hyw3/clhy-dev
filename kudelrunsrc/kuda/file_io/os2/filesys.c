/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_arch_file_io.h"
#include "kuda_strings.h"
#include "kuda_lib.h"
#include <ctype.h>

/* OS2 Exceptions:
 *
 * Note that trailing spaces and trailing periods are never recorded
 * in the file system.
 *
 * Leading spaces and periods are accepted, however.
 * The * ? < > codes all have wildcard side effects
 * The " / \ : are exclusively component separator tokens 
 * The system doesn't accept | for any (known) purpose 
 * Oddly, \x7f _is_ acceptable ;)
 */

const char c_is_fnchar[256] =
{/* Reject all ctrl codes...                                         */
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
 /*     "               *         /                      :   <   > ? */
    1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,0, 1,1,1,1,1,1,1,1,1,1,0,1,0,1,0,0,
 /*                                                          \       */
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,
 /*                                                          |       */
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,
 /* High bit codes are accepted                                      */
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
};


#define IS_SLASH(c) (c == '/' || c == '\\')


kuda_status_t filepath_root_test(char *path, kuda_pool_t *p)
{
    char drive = kuda_toupper(path[0]);

    if (drive >= 'A' && drive <= 'Z' && path[1] == ':' && IS_SLASH(path[2]))
        return KUDA_SUCCESS;

    return KUDA_EBADPATH;
}


kuda_status_t filepath_drive_get(char **rootpath, char drive, 
                                kuda_int32_t flags, kuda_pool_t *p)
{
    char path[KUDA_PATH_MAX];
    char *pos;
    ULONG rc;
    ULONG bufsize = sizeof(path) - 3;

    path[0] = drive;
    path[1] = ':';
    path[2] = '/';

    rc = DosQueryCurrentDir(kuda_toupper(drive) - 'A', path+3, &bufsize);

    if (rc) {
        return KUDA_FROM_PLATFORM_ERROR(rc);
    }

    if (!(flags & KUDA_FILEPATH_NATIVE)) {
        for (pos=path; *pos; pos++) {
            if (*pos == '\\')
                *pos = '/';
        }
    }

    *rootpath = kuda_pstrdup(p, path);
    return KUDA_SUCCESS;
}


kuda_status_t filepath_root_case(char **rootpath, char *root, kuda_pool_t *p)
{
    if (root[0] && kuda_islower(root[0]) && root[1] == ':') {
        *rootpath = kuda_pstrdup(p, root);
        (*rootpath)[0] = kuda_toupper((*rootpath)[0]);
    }
    else {
       *rootpath = root;
    }
    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_filepath_get(char **defpath, kuda_int32_t flags,
                                           kuda_pool_t *p)
{
    char path[KUDA_PATH_MAX];
    ULONG drive;
    ULONG drivemap;
    ULONG rv, pathlen = sizeof(path) - 3;
    char *pos;

    DosQueryCurrentDisk(&drive, &drivemap);
    path[0] = '@' + drive;
    strcpy(path+1, ":\\");
    rv = DosQueryCurrentDir(drive, path+3, &pathlen);

    *defpath = kuda_pstrdup(p, path);

    if (!(flags & KUDA_FILEPATH_NATIVE)) {
        for (pos=*defpath; *pos; pos++) {
            if (*pos == '\\')
                *pos = '/';
        }
    }

    return KUDA_SUCCESS;
}    



KUDA_DECLARE(kuda_status_t) kuda_filepath_set(const char *path, kuda_pool_t *p)
{
    ULONG rv = 0;

    if (path[1] == ':')
        rv = DosSetDefaultDisk(kuda_toupper(path[0]) - '@');

    if (rv == 0)
        rv = DosSetCurrentDir(path);

    return KUDA_FROM_PLATFORM_ERROR(rv);
}
