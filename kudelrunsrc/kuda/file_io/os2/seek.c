/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_lib.h"
#include <string.h>
#include <io.h>


static kuda_status_t setptr(kuda_file_t *thefile, unsigned long pos )
{
    long newbufpos;
    ULONG rc;

    if (thefile->direction == 1) {
        /* XXX: flush here is not mutex protected */
        kuda_status_t rv = kuda_file_flush(thefile);

        if (rv != KUDA_SUCCESS) {
            return rv;
        }

        thefile->bufpos = thefile->direction = thefile->dataRead = 0;
    }

    newbufpos = pos - (thefile->filePtr - thefile->dataRead);
    if (newbufpos >= 0 && newbufpos <= thefile->dataRead) {
        thefile->bufpos = newbufpos;
        rc = 0;
    } else {
        rc = DosSetFilePtr(thefile->filedes, pos, FILE_BEGIN, &thefile->filePtr );

        if ( !rc )
            thefile->bufpos = thefile->dataRead = 0;
    }

    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_file_seek(kuda_file_t *thefile, kuda_seek_where_t where, kuda_off_t *offset)
{
    if (!thefile->isopen) {
        return KUDA_EBADF;
    }

    thefile->eof_hit = 0;

    if (thefile->buffered) {
        int rc = EINVAL;
        kuda_finfo_t finfo;

        switch (where) {
        case KUDA_SET:
            rc = setptr(thefile, *offset);
            break;

        case KUDA_CUR:
            rc = setptr(thefile, thefile->filePtr - thefile->dataRead + thefile->bufpos + *offset);
            break;

        case KUDA_END:
            rc = kuda_file_info_get(&finfo, KUDA_FINFO_NORM, thefile);
            if (rc == KUDA_SUCCESS)
                rc = setptr(thefile, finfo.size + *offset);
            break;
        }

        *offset = thefile->filePtr - thefile->dataRead + thefile->bufpos;
        return rc;
    } else {
        switch (where) {
        case KUDA_SET:
            where = FILE_BEGIN;
            break;

        case KUDA_CUR:
            where = FILE_CURRENT;
            break;

        case KUDA_END:
            where = FILE_END;
            break;
        }

        return KUDA_FROM_PLATFORM_ERROR(DosSetFilePtr(thefile->filedes, *offset, where, (ULONG *)offset));
    }
}



KUDA_DECLARE(kuda_status_t) kuda_file_trunc(kuda_file_t *fp, kuda_off_t offset)
{
    int rc = DosSetFileSize(fp->filedes, offset);

    if (rc != 0) {
        return KUDA_FROM_PLATFORM_ERROR(rc);
    }

    if (fp->buffered) {
        return setptr(fp, offset);
    }

    return KUDA_SUCCESS;
}
