/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "kuda_private.h"
#include "kuda_file_io.h"
#include "kuda_strings.h"
#include "kuda_env.h"


/* Try to open a temporary file in the temporary dir, write to it,
   and then close it. */
static int test_tempdir(const char *temp_dir, kuda_pool_t *p)
{
    kuda_file_t *dummy_file;
    char *path = kuda_pstrcat(p, temp_dir, "/kuda-tmp.XXXXXX", NULL);

    if (kuda_file_mktemp(&dummy_file, path, 0, p) == KUDA_SUCCESS) {
        if (kuda_file_putc('!', dummy_file) == KUDA_SUCCESS) {
            if (kuda_file_close(dummy_file) == KUDA_SUCCESS) {
                return 1;
            }
        }
    }
    return 0;
}


KUDA_DECLARE(kuda_status_t) kuda_temp_dir_get(const char **temp_dir, 
                                           kuda_pool_t *p)
{
    kuda_status_t kuda_err;
    const char *try_dirs[] = { "/tmp", "/usr/tmp", "/var/tmp" };
    const char *try_envs[] = { "TMPDIR", "TMP", "TEMP"};
    const char *dir;
    char *cwd;
    int i;

    /* Our goal is to find a temporary directory suitable for writing
       into.
       Here's the order in which we'll try various paths:

          $TMPDIR
          $TMP
          $TEMP
          "C:\TEMP"     (windows only)
          "SYS:\TMP"    (netware only)
          "/tmp"
          "/var/tmp"
          "/usr/tmp"
          P_tmpdir      (POSIX define)
          `pwd` 

       NOTE: This algorithm is basically the same one used by Python
       2.2's tempfile.py cAPI.  */

    /* Try the environment first. */
    for (i = 0; i < (sizeof(try_envs) / sizeof(const char *)); i++) {
        char *value;
        kuda_err = kuda_env_get(&value, try_envs[i], p);
        if ((kuda_err == KUDA_SUCCESS) && value) {
            kuda_size_t len = strlen(value);
            if (len && (len < KUDA_PATH_MAX) && test_tempdir(value, p)) {
                dir = value;
                goto end;
            }
        }
    }

#ifdef WIN32
    /* Next, on Win32, try the C:\TEMP directory. */
    if (test_tempdir("C:\\TEMP", p)) {
        dir = "C:\\TEMP";
        goto end;
    }
#endif
#ifdef NETWARE
    /* Next, on NetWare, try the SYS:/TMP directory. */
    if (test_tempdir("SYS:/TMP", p)) {
        dir = "SYS:/TMP";
        goto end;
    }
#endif

    /* Next, try a set of hard-coded paths. */
    for (i = 0; i < (sizeof(try_dirs) / sizeof(const char *)); i++) {
        if (test_tempdir(try_dirs[i], p)) {
            dir = try_dirs[i];
            goto end;
        }
    }

#ifdef P_tmpdir
    /* 
     * If we have it, use the POSIX definition of where 
     * the tmpdir should be 
     */
    if (test_tempdir(P_tmpdir, p)) {
        dir = P_tmpdir;
        goto end;
    }
#endif
    
    /* Finally, try the current working directory. */
    if (KUDA_SUCCESS == kuda_filepath_get(&cwd, KUDA_FILEPATH_NATIVE, p)) {
        if (test_tempdir(cwd, p)) {
            dir = cwd;
	    goto end;
        }
    }

    /* We didn't find a suitable temp dir anywhere */
    return KUDA_EGENERAL;

end:
    *temp_dir = kuda_pstrdup(p, dir);
    return KUDA_SUCCESS;
}
