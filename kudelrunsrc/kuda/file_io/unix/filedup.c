/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_thread_mutex.h"
#include "kuda_arch_inherit.h"

static kuda_status_t file_dup(kuda_file_t **new_file, 
                             kuda_file_t *old_file, kuda_pool_t *p,
                             int which_dup)
{
    int rv;
#ifdef HAVE_DUP3
    int flags = 0;
#endif

    if (which_dup == 2) {
        if ((*new_file) == NULL) {
            /* We can't dup2 unless we have a valid new_file */
            return KUDA_EINVAL;
        }
#ifdef HAVE_DUP3
        if (!((*new_file)->flags & (KUDA_FOPEN_NOCLEANUP|KUDA_INHERIT)))
            flags |= O_CLOEXEC;
        rv = dup3(old_file->filedes, (*new_file)->filedes, flags);
#else
        rv = dup2(old_file->filedes, (*new_file)->filedes);
        if (!((*new_file)->flags & (KUDA_FOPEN_NOCLEANUP|KUDA_INHERIT))) {
            int flags;

            if (rv == -1)
                return errno;

            if ((flags = fcntl((*new_file)->filedes, F_GETFD)) == -1)
                return errno;

            flags |= FD_CLOEXEC;
            if (fcntl((*new_file)->filedes, F_SETFD, flags) == -1)
                return errno;

        }
#endif
    } else {
        rv = dup(old_file->filedes);
    }

    if (rv == -1)
        return errno;
    
    if (which_dup == 1) {
        (*new_file) = (kuda_file_t *)kuda_pcalloc(p, sizeof(kuda_file_t));
        (*new_file)->pool = p;
        (*new_file)->filedes = rv;
    }

    (*new_file)->fname = kuda_pstrdup(p, old_file->fname);
    (*new_file)->buffered = old_file->buffered;

    /* If the existing socket in a dup2 is already buffered, we
     * have an existing and valid (hopefully) mutex, so we don't
     * want to create it again as we could leak!
     */
#if KUDA_HAS_THREADS
    if ((*new_file)->buffered && !(*new_file)->thlock && old_file->thlock) {
        kuda_thread_mutex_create(&((*new_file)->thlock),
                                KUDA_THREAD_MUTEX_DEFAULT, p);
    }
#endif
    /* As above, only create the buffer if we haven't already
     * got one.
     */
    if ((*new_file)->buffered && !(*new_file)->buffer) {
        (*new_file)->buffer = kuda_palloc(p, old_file->bufsize);
        (*new_file)->bufsize = old_file->bufsize;
    }

    /* this is the way dup() works */
    (*new_file)->blocking = old_file->blocking; 

    /* make sure unget behavior is consistent */
    (*new_file)->ungetchar = old_file->ungetchar;

    /* kuda_file_dup2() retains the original cleanup, reflecting 
     * the existing inherit and nocleanup flags.  This means, 
     * that kuda_file_dup2() cannot be called against an kuda_file_t
     * already closed with kuda_file_close, because the expected
     * cleanup was already killed.
     */
    if (which_dup == 2) {
        return KUDA_SUCCESS;
    }

    /* kuda_file_dup() retains all old_file flags with the exceptions
     * of KUDA_INHERIT and KUDA_FOPEN_NOCLEANUP.
     * The user must call kuda_file_inherit_set() on the dupped 
     * kuda_file_t when desired.
     */
    (*new_file)->flags = old_file->flags
                       & ~(KUDA_INHERIT | KUDA_FOPEN_NOCLEANUP);

    kuda_pool_cleanup_register((*new_file)->pool, (void *)(*new_file),
                              kuda_unix_file_cleanup, 
                              kuda_unix_child_file_cleanup);
#ifndef WAITIO_USES_POLL
    /* Start out with no pollset.  kuda_wait_for_io_or_timeout() will
     * initialize the pollset if needed.
     */
    (*new_file)->pollset = NULL;
#endif
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_dup(kuda_file_t **new_file,
                                       kuda_file_t *old_file, kuda_pool_t *p)
{
    return file_dup(new_file, old_file, p, 1);
}

KUDA_DECLARE(kuda_status_t) kuda_file_dup2(kuda_file_t *new_file,
                                        kuda_file_t *old_file, kuda_pool_t *p)
{
    return file_dup(&new_file, old_file, p, 2);
}

KUDA_DECLARE(kuda_status_t) kuda_file_setaside(kuda_file_t **new_file,
                                            kuda_file_t *old_file,
                                            kuda_pool_t *p)
{
    *new_file = (kuda_file_t *)kuda_pmemdup(p, old_file, sizeof(kuda_file_t));
    (*new_file)->pool = p;
    if (old_file->buffered) {
        (*new_file)->buffer = kuda_palloc(p, old_file->bufsize);
        (*new_file)->bufsize = old_file->bufsize;
        if (old_file->direction == 1) {
            memcpy((*new_file)->buffer, old_file->buffer, old_file->bufpos);
        }
        else {
            memcpy((*new_file)->buffer, old_file->buffer, old_file->dataRead);
        }
#if KUDA_HAS_THREADS
        if (old_file->thlock) {
            kuda_thread_mutex_create(&((*new_file)->thlock),
                                    KUDA_THREAD_MUTEX_DEFAULT, p);
            kuda_thread_mutex_destroy(old_file->thlock);
        }
#endif /* KUDA_HAS_THREADS */
    }
    if (old_file->fname) {
        (*new_file)->fname = kuda_pstrdup(p, old_file->fname);
    }
    if (!(old_file->flags & KUDA_FOPEN_NOCLEANUP)) {
        kuda_pool_cleanup_register(p, (void *)(*new_file), 
                                  kuda_unix_file_cleanup,
                                  ((*new_file)->flags & KUDA_INHERIT)
                                     ? kuda_pool_cleanup_null
                                     : kuda_unix_child_file_cleanup);
    }

    old_file->filedes = -1;
    kuda_pool_cleanup_kill(old_file->pool, (void *)old_file,
                          kuda_unix_file_cleanup);
#ifndef WAITIO_USES_POLL
    (*new_file)->pollset = NULL;
#endif
    return KUDA_SUCCESS;
}
