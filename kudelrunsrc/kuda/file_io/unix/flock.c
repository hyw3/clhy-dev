/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"

#if KUDA_HAVE_FCNTL_H
#include <fcntl.h>
#endif
#ifdef HAVE_SYS_FILE_H
#include <sys/file.h>
#endif

KUDA_DECLARE(kuda_status_t) kuda_file_lock(kuda_file_t *thefile, int type)
{
    int rc;

#if defined(HAVE_FCNTL_H)
    {
        struct flock l = { 0 };
        int fc;

        l.l_whence = SEEK_SET;  /* count l_start from start of file */
        l.l_start = 0;          /* lock from start of file */
        l.l_len = 0;            /* lock to end of file */
        if ((type & KUDA_FLOCK_TYPEMASK) == KUDA_FLOCK_SHARED)
            l.l_type = F_RDLCK;
        else
            l.l_type = F_WRLCK;

        fc = (type & KUDA_FLOCK_NONBLOCK) ? F_SETLK : F_SETLKW;

        /* keep trying if fcntl() gets interrupted (by a signal) */
        while ((rc = fcntl(thefile->filedes, fc, &l)) < 0 && errno == EINTR)
            continue;

        if (rc == -1) {
            /* on some Unix boxes (e.g., Tru64), we get EACCES instead
             * of EAGAIN; we don't want KUDA_STATUS_IS_EAGAIN() matching EACCES
             * since that breaks other things, so fix up the retcode here
             */
            if (errno == EACCES) {
                return EAGAIN;
            }
            return errno;
        }
    }
#elif defined(HAVE_SYS_FILE_H)
    {
        int ltype;

        if ((type & KUDA_FLOCK_TYPEMASK) == KUDA_FLOCK_SHARED)
            ltype = LOCK_SH;
        else
            ltype = LOCK_EX;
        if ((type & KUDA_FLOCK_NONBLOCK) != 0)
            ltype |= LOCK_NB;

        /* keep trying if flock() gets interrupted (by a signal) */
        while ((rc = flock(thefile->filedes, ltype)) < 0 && errno == EINTR)
            continue;

        if (rc == -1)
            return errno;
    }
#else
#error No file locking mechanism is available.
#endif

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_unlock(kuda_file_t *thefile)
{
    int rc;

#if defined(HAVE_FCNTL_H)
    {
        struct flock l = { 0 };

        l.l_whence = SEEK_SET;  /* count l_start from start of file */
        l.l_start = 0;          /* lock from start of file */
        l.l_len = 0;            /* lock to end of file */
        l.l_type = F_UNLCK;

        /* keep trying if fcntl() gets interrupted (by a signal) */
        while ((rc = fcntl(thefile->filedes, F_SETLKW, &l)) < 0
               && errno == EINTR)
            continue;

        if (rc == -1)
            return errno;
    }
#elif defined(HAVE_SYS_FILE_H)
    {
        /* keep trying if flock() gets interrupted (by a signal) */
        while ((rc = flock(thefile->filedes, LOCK_UN)) < 0 && errno == EINTR)
            continue;

        if (rc == -1)
            return errno;
    }
#else
#error No file locking mechanism is available.
#endif

    return KUDA_SUCCESS;
}
