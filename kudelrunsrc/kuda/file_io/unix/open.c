/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_thread_mutex.h"
#include "kuda_arch_inherit.h"

#ifdef NETWARE
#include "nks/dirio.h"
#include "kuda_hash.h"
#include "fsio.h"
#endif

static kuda_status_t file_cleanup(kuda_file_t *file, int is_child)
{
    kuda_status_t rv = KUDA_SUCCESS;
    int fd = file->filedes;

    /* Set file descriptor to -1 before close(), so that there is no
     * chance of returning an already closed FD from kuda_platform_file_get().
     */
    file->filedes = -1;

    if (close(fd) == 0) {
        /* Only the parent process should delete the file! */
        if (!is_child && (file->flags & KUDA_FOPEN_DELONCLOSE)) {
            unlink(file->fname);
        }
#if KUDA_HAS_THREADS
        if (file->thlock) {
            rv = kuda_thread_mutex_destroy(file->thlock);
        }
#endif
    }
    else {
        /* Restore, close() was not successful. */
        file->filedes = fd;

        /* Are there any error conditions other than EINTR or EBADF? */
        rv = errno;
    }
#ifndef WAITIO_USES_POLL
    if (file->pollset != NULL) {
        kuda_status_t pollset_rv = kuda_pollset_destroy(file->pollset);
        /* If the file close failed, return its error value,
         * not kuda_pollset_destroy()'s.
         */
        if (rv == KUDA_SUCCESS) {
            rv = pollset_rv;
        }
    }
#endif /* !WAITIO_USES_POLL */
    return rv;
}

kuda_status_t kuda_unix_file_cleanup(void *thefile)
{
    kuda_file_t *file = thefile;
    kuda_status_t flush_rv = KUDA_SUCCESS, rv = KUDA_SUCCESS;

    if (file->buffered) {
        flush_rv = kuda_file_flush(file);
    }

    rv = file_cleanup(file, 0);

    return rv != KUDA_SUCCESS ? rv : flush_rv;
}

kuda_status_t kuda_unix_child_file_cleanup(void *thefile)
{
    return file_cleanup(thefile, 1);
}

KUDA_DECLARE(kuda_status_t) kuda_file_open(kuda_file_t **new, 
                                        const char *fname, 
                                        kuda_int32_t flag, 
                                        kuda_fileperms_t perm, 
                                        kuda_pool_t *pool)
{
    kuda_platform_file_t fd;
    int oflags = 0;
#if KUDA_HAS_THREADS
    kuda_thread_mutex_t *thlock;
    kuda_status_t rv;
#endif

    if ((flag & KUDA_FOPEN_READ) && (flag & KUDA_FOPEN_WRITE)) {
        oflags = O_RDWR;
    }
    else if (flag & KUDA_FOPEN_READ) {
        oflags = O_RDONLY;
    }
    else if (flag & KUDA_FOPEN_WRITE) {
        oflags = O_WRONLY;
    }
    else {
        return KUDA_EACCES; 
    }

    if (flag & KUDA_FOPEN_CREATE) {
        oflags |= O_CREAT;
        if (flag & KUDA_FOPEN_EXCL) {
            oflags |= O_EXCL;
        }
    }
    if ((flag & KUDA_FOPEN_EXCL) && !(flag & KUDA_FOPEN_CREATE)) {
        return KUDA_EACCES;
    }   

    if (flag & KUDA_FOPEN_APPEND) {
        oflags |= O_APPEND;
    }
    if (flag & KUDA_FOPEN_TRUNCATE) {
        oflags |= O_TRUNC;
    }
#ifdef O_BINARY
    if (flag & KUDA_FOPEN_BINARY) {
        oflags |= O_BINARY;
    }
#endif

    if (flag & KUDA_FOPEN_NONBLOCK) {
#ifdef O_NONBLOCK
        oflags |= O_NONBLOCK;
#else
        return KUDA_ENOTIMPL;
#endif
    }

#ifdef O_CLOEXEC
    /* Introduced in Linux 2.6.23. Silently ignored on earlier Linux kernels.
     */
    if (!(flag & KUDA_FOPEN_NOCLEANUP)) {
        oflags |= O_CLOEXEC;
}
#endif
 
#if KUDA_HAS_LARGE_FILES && defined(_LARGEFILE64_SOURCE)
    oflags |= O_LARGEFILE;
#elif defined(O_LARGEFILE)
    if (flag & KUDA_FOPEN_LARGEFILE) {
        oflags |= O_LARGEFILE;
    }
#endif

#if KUDA_HAS_THREADS
    if ((flag & KUDA_FOPEN_BUFFERED) && (flag & KUDA_FOPEN_XTHREAD)) {
        rv = kuda_thread_mutex_create(&thlock,
                                     KUDA_THREAD_MUTEX_DEFAULT, pool);
        if (rv) {
            return rv;
        }
    }
#endif

    if (perm == KUDA_PLATFORM_DEFAULT) {
        fd = open(fname, oflags, 0666);
    }
    else {
        fd = open(fname, oflags, kuda_unix_perms2mode(perm));
    } 
    if (fd < 0) {
       return errno;
    }
    if (!(flag & KUDA_FOPEN_NOCLEANUP)) {
#ifdef O_CLOEXEC
        static int has_o_cloexec = 0;
        if (!has_o_cloexec)
#endif
        {
            int flags;

            if ((flags = fcntl(fd, F_GETFD)) == -1) {
                close(fd);
                return errno;
            }
            if ((flags & FD_CLOEXEC) == 0) {
                flags |= FD_CLOEXEC;
                if (fcntl(fd, F_SETFD, flags) == -1) {
                    close(fd);
                    return errno;
                }
            }
#ifdef O_CLOEXEC
            else {
                has_o_cloexec = 1;
            }
#endif
        }
    }

    (*new) = (kuda_file_t *)kuda_pcalloc(pool, sizeof(kuda_file_t));
    (*new)->pool = pool;
    (*new)->flags = flag;
    (*new)->filedes = fd;

    (*new)->fname = kuda_pstrdup(pool, fname);

    (*new)->blocking = BLK_ON;
    (*new)->buffered = (flag & KUDA_FOPEN_BUFFERED) > 0;

    if ((*new)->buffered) {
        (*new)->buffer = kuda_palloc(pool, KUDA_FILE_DEFAULT_BUFSIZE);
        (*new)->bufsize = KUDA_FILE_DEFAULT_BUFSIZE;
#if KUDA_HAS_THREADS
        if ((*new)->flags & KUDA_FOPEN_XTHREAD) {
            (*new)->thlock = thlock;
        }
#endif
    }
    else {
        (*new)->buffer = NULL;
    }

    (*new)->is_pipe = 0;
    (*new)->timeout = -1;
    (*new)->ungetchar = -1;
    (*new)->eof_hit = 0;
    (*new)->filePtr = 0;
    (*new)->bufpos = 0;
    (*new)->dataRead = 0;
    (*new)->direction = 0;
#ifndef WAITIO_USES_POLL
    /* Start out with no pollset.  kuda_wait_for_io_or_timeout() will
     * initialize the pollset if needed.
     */
    (*new)->pollset = NULL;
#endif
    if (!(flag & KUDA_FOPEN_NOCLEANUP)) {
        kuda_pool_cleanup_register((*new)->pool, (void *)(*new), 
                                  kuda_unix_file_cleanup, 
                                  kuda_unix_child_file_cleanup);
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_close(kuda_file_t *file)
{
    return kuda_pool_cleanup_run(file->pool, file, kuda_unix_file_cleanup);
}

KUDA_DECLARE(kuda_status_t) kuda_file_remove(const char *path, kuda_pool_t *pool)
{
    if (unlink(path) == 0) {
        return KUDA_SUCCESS;
    }
    else {
        return errno;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_file_rename(const char *from_path, 
                                          const char *to_path,
                                          kuda_pool_t *p)
{
    if (rename(from_path, to_path) != 0) {
        return errno;
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_file_get(kuda_platform_file_t *thefile, 
                                          kuda_file_t *file)
{
    *thefile = file->filedes;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_file_put(kuda_file_t **file, 
                                          kuda_platform_file_t *thefile,
                                          kuda_int32_t flags, kuda_pool_t *pool)
{
    int *dafile = thefile;
    
    (*file) = kuda_pcalloc(pool, sizeof(kuda_file_t));
    (*file)->pool = pool;
    (*file)->eof_hit = 0;
    (*file)->blocking = BLK_UNKNOWN; /* in case it is a pipe */
    (*file)->timeout = -1;
    (*file)->ungetchar = -1; /* no char avail */
    (*file)->filedes = *dafile;
    (*file)->flags = flags | KUDA_FOPEN_NOCLEANUP;
    (*file)->buffered = (flags & KUDA_FOPEN_BUFFERED) > 0;

#ifndef WAITIO_USES_POLL
    /* Start out with no pollset.  kuda_wait_for_io_or_timeout() will
     * initialize the pollset if needed.
     */
    (*file)->pollset = NULL;
#endif

    if ((*file)->buffered) {
        (*file)->buffer = kuda_palloc(pool, KUDA_FILE_DEFAULT_BUFSIZE);
        (*file)->bufsize = KUDA_FILE_DEFAULT_BUFSIZE;
#if KUDA_HAS_THREADS
        if ((*file)->flags & KUDA_FOPEN_XTHREAD) {
            kuda_status_t rv;
            rv = kuda_thread_mutex_create(&((*file)->thlock),
                                         KUDA_THREAD_MUTEX_DEFAULT, pool);
            if (rv) {
                return rv;
            }
        }
#endif
    }
    return KUDA_SUCCESS;
}    

KUDA_DECLARE(kuda_status_t) kuda_file_eof(kuda_file_t *fptr)
{
    if (fptr->eof_hit == 1) {
        return KUDA_EOF;
    }
    return KUDA_SUCCESS;
}   

KUDA_DECLARE(kuda_status_t) kuda_file_open_flags_stderr(kuda_file_t **thefile, 
                                                     kuda_int32_t flags,
                                                     kuda_pool_t *pool)
{
    int fd = STDERR_FILENO;

    return kuda_platform_file_put(thefile, &fd, flags | KUDA_FOPEN_WRITE, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_file_open_flags_stdout(kuda_file_t **thefile, 
                                                     kuda_int32_t flags,
                                                     kuda_pool_t *pool)
{
    int fd = STDOUT_FILENO;

    return kuda_platform_file_put(thefile, &fd, flags | KUDA_FOPEN_WRITE, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_file_open_flags_stdin(kuda_file_t **thefile, 
                                                    kuda_int32_t flags,
                                                    kuda_pool_t *pool)
{
    int fd = STDIN_FILENO;

    return kuda_platform_file_put(thefile, &fd, flags | KUDA_FOPEN_READ, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_file_open_stderr(kuda_file_t **thefile, 
                                               kuda_pool_t *pool)
{
    return kuda_file_open_flags_stderr(thefile, 0, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_file_open_stdout(kuda_file_t **thefile, 
                                               kuda_pool_t *pool)
{
    return kuda_file_open_flags_stdout(thefile, 0, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_file_open_stdin(kuda_file_t **thefile, 
                                              kuda_pool_t *pool)
{
    return kuda_file_open_flags_stdin(thefile, 0, pool);
}

KUDA_IMPLEMENT_INHERIT_SET(file, flags, pool, kuda_unix_file_cleanup)

/* We need to do this by hand instead of using KUDA_IMPLEMENT_INHERIT_UNSET
 * because the macro sets both cleanups to the same function, which is not
 * suitable on Unix (see PR 41119). */
KUDA_DECLARE(kuda_status_t) kuda_file_inherit_unset(kuda_file_t *thefile)
{
    if (thefile->flags & KUDA_FOPEN_NOCLEANUP) {
        return KUDA_EINVAL;
    }
    if (thefile->flags & KUDA_INHERIT) {
        int flags;

        if ((flags = fcntl(thefile->filedes, F_GETFD)) == -1)
            return errno;

        flags |= FD_CLOEXEC;
        if (fcntl(thefile->filedes, F_SETFD, flags) == -1)
            return errno;

        thefile->flags &= ~KUDA_INHERIT;
        kuda_pool_child_cleanup_set(thefile->pool,
                                   (void *)thefile,
                                   kuda_unix_file_cleanup,
                                   kuda_unix_child_file_cleanup);
    }
    return KUDA_SUCCESS;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(file)

KUDA_DECLARE(kuda_status_t) kuda_file_link(const char *from_path, 
                                          const char *to_path)
{
    if (link(from_path, to_path) == -1) {
        return errno;
    }

    return KUDA_SUCCESS;
}
