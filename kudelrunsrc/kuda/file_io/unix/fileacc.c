/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strings.h"
#include "kuda_arch_file_io.h"

/* A file to put ALL of the accessor functions for kuda_file_t types. */

KUDA_DECLARE(kuda_status_t) kuda_file_name_get(const char **fname,
                                           kuda_file_t *thefile)
{
    *fname = thefile->fname;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_int32_t) kuda_file_flags_get(kuda_file_t *f)
{
    return f->flags;
}

#if !defined(OS2) && !defined(WIN32)
mode_t kuda_unix_perms2mode(kuda_fileperms_t perms)
{
    mode_t mode = 0;

    if (perms & KUDA_USETID)
        mode |= S_ISUID;
    if (perms & KUDA_UREAD)
        mode |= S_IRUSR;
    if (perms & KUDA_UWRITE)
        mode |= S_IWUSR;
    if (perms & KUDA_UEXECUTE)
        mode |= S_IXUSR;

    if (perms & KUDA_GSETID)
        mode |= S_ISGID;
    if (perms & KUDA_GREAD)
        mode |= S_IRGRP;
    if (perms & KUDA_GWRITE)
        mode |= S_IWGRP;
    if (perms & KUDA_GEXECUTE)
        mode |= S_IXGRP;

#ifdef S_ISVTX
    if (perms & KUDA_WSTICKY)
        mode |= S_ISVTX;
#endif
    if (perms & KUDA_WREAD)
        mode |= S_IROTH;
    if (perms & KUDA_WWRITE)
        mode |= S_IWOTH;
    if (perms & KUDA_WEXECUTE)
        mode |= S_IXOTH;

    return mode;
}

kuda_fileperms_t kuda_unix_mode2perms(mode_t mode)
{
    kuda_fileperms_t perms = 0;

    if (mode & S_ISUID)
        perms |= KUDA_USETID;
    if (mode & S_IRUSR)
        perms |= KUDA_UREAD;
    if (mode & S_IWUSR)
        perms |= KUDA_UWRITE;
    if (mode & S_IXUSR)
        perms |= KUDA_UEXECUTE;

    if (mode & S_ISGID)
        perms |= KUDA_GSETID;
    if (mode & S_IRGRP)
        perms |= KUDA_GREAD;
    if (mode & S_IWGRP)
        perms |= KUDA_GWRITE;
    if (mode & S_IXGRP)
        perms |= KUDA_GEXECUTE;

#ifdef S_ISVTX
    if (mode & S_ISVTX)
        perms |= KUDA_WSTICKY;
#endif
    if (mode & S_IROTH)
        perms |= KUDA_WREAD;
    if (mode & S_IWOTH)
        perms |= KUDA_WWRITE;
    if (mode & S_IXOTH)
        perms |= KUDA_WEXECUTE;

    return perms;
}
#endif

KUDA_DECLARE(kuda_status_t) kuda_file_data_get(void **data, const char *key,
                                           kuda_file_t *file)
{    
    return kuda_pool_userdata_get(data, key, file->pool);
}

KUDA_DECLARE(kuda_status_t) kuda_file_data_set(kuda_file_t *file, void *data,
                                           const char *key,
                                           kuda_status_t (*cleanup)(void *))
{    
    return kuda_pool_userdata_set(data, key, cleanup, file->pool);
}
