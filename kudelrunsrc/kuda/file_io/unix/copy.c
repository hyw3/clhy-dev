/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"

static kuda_status_t kuda_file_transfer_contents(const char *from_path,
                                               const char *to_path,
                                               kuda_int32_t flags,
                                               kuda_fileperms_t to_perms,
                                               kuda_pool_t *pool)
{
    kuda_file_t *s, *d;
    kuda_status_t status;
    kuda_finfo_t finfo;
    kuda_fileperms_t perms;

    /* Open source file. */
    status = kuda_file_open(&s, from_path, KUDA_FOPEN_READ, KUDA_PLATFORM_DEFAULT, pool);
    if (status)
        return status;

    /* Maybe get its permissions. */
    if (to_perms == KUDA_FILE_SOURCE_PERMS) {
        status = kuda_file_info_get(&finfo, KUDA_FINFO_PROT, s);
        if (status != KUDA_SUCCESS && status != KUDA_INCOMPLETE) {
            kuda_file_close(s);  /* toss any error */
            return status;
        }
        perms = finfo.protection;
        kuda_file_perms_set(to_path, perms); /* ignore any failure */
    }
    else
        perms = to_perms;

    /* Open dest file. */
    status = kuda_file_open(&d, to_path, flags, perms, pool);
    if (status) {
        kuda_file_close(s);  /* toss any error */
        return status;
    }

#if BUFSIZ > KUDA_FILE_DEFAULT_BUFSIZE
#define COPY_BUFSIZ BUFSIZ
#else
#define COPY_BUFSIZ KUDA_FILE_DEFAULT_BUFSIZE
#endif

    /* Copy bytes till the cows come home. */
    while (1) {
        char buf[COPY_BUFSIZ];
        kuda_size_t bytes_this_time = sizeof(buf);
        kuda_status_t read_err;
        kuda_status_t write_err;

        /* Read 'em. */
        read_err = kuda_file_read(s, buf, &bytes_this_time);
        if (read_err && !KUDA_STATUS_IS_EOF(read_err)) {
            kuda_file_close(s);  /* toss any error */
            kuda_file_close(d);  /* toss any error */
            return read_err;
        }

        /* Write 'em. */
        write_err = kuda_file_write_full(d, buf, bytes_this_time, NULL);
        if (write_err) {
            kuda_file_close(s);  /* toss any error */
            kuda_file_close(d);  /* toss any error */
            return write_err;
        }

        if (read_err && KUDA_STATUS_IS_EOF(read_err)) {
            status = kuda_file_close(s);
            if (status) {
                kuda_file_close(d);  /* toss any error */
                return status;
            }

            /* return the results of this close: an error, or success */
            return kuda_file_close(d);
        }
    }
    /* NOTREACHED */
}

KUDA_DECLARE(kuda_status_t) kuda_file_copy(const char *from_path,
                                        const char *to_path,
                                        kuda_fileperms_t perms,
                                        kuda_pool_t *pool)
{
    return kuda_file_transfer_contents(from_path, to_path,
                                      (KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE | KUDA_FOPEN_TRUNCATE),
                                      perms,
                                      pool);
}

KUDA_DECLARE(kuda_status_t) kuda_file_append(const char *from_path,
                                          const char *to_path,
                                          kuda_fileperms_t perms,
                                          kuda_pool_t *pool)
{
    return kuda_file_transfer_contents(from_path, to_path,
                                      (KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE | KUDA_FOPEN_APPEND),
                                      perms,
                                      pool);
}
