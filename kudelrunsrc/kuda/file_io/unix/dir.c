/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#if KUDA_HAVE_SYS_SYSLIMITS_H
#include <sys/syslimits.h>
#endif
#if KUDA_HAVE_LIMITS_H
#include <limits.h>
#endif

#ifndef NAME_MAX
#define NAME_MAX 255
#endif

static kuda_status_t dir_cleanup(void *thedir)
{
    kuda_dir_t *dir = thedir;
    if (closedir(dir->dirstruct) == 0) {
        return KUDA_SUCCESS;
    }
    else {
        return errno;
    }
} 

#define PATH_SEPARATOR '/'

/* Remove trailing separators that don't affect the meaning of PATH. */
static const char *path_canonicalize (const char *path, kuda_pool_t *pool)
{
    /* At some point this could eliminate redundant components.  For
     * now, it just makes sure there is no trailing slash. */
    kuda_size_t len = strlen (path);
    kuda_size_t orig_len = len;
    
    while ((len > 0) && (path[len - 1] == PATH_SEPARATOR))
        len--;
    
    if (len != orig_len)
        return kuda_pstrndup (pool, path, len);
    else
        return path;
}

/* Remove one component off the end of PATH. */
static char *path_remove_last_component (const char *path, kuda_pool_t *pool)
{
    const char *newpath = path_canonicalize (path, pool);
    int i;
    
    for (i = (strlen(newpath) - 1); i >= 0; i--) {
        if (path[i] == PATH_SEPARATOR)
            break;
    }

    return kuda_pstrndup (pool, path, (i < 0) ? 0 : i);
}

kuda_status_t kuda_dir_open(kuda_dir_t **new, const char *dirname, 
                          kuda_pool_t *pool)
{
    DIR *dir = opendir(dirname);

    if (!dir) {
        return errno;
    }

    (*new) = (kuda_dir_t *)kuda_palloc(pool, sizeof(kuda_dir_t));

    (*new)->pool = pool;
    (*new)->dirname = kuda_pstrdup(pool, dirname);
    (*new)->dirstruct = dir;

#if KUDA_HAS_THREADS && defined(_POSIX_THREAD_SAFE_FUNCTIONS) \
                    && !defined(READDIR_IS_THREAD_SAFE)
    /* On some platforms (e.g., Linux+GNU libc), d_name[] in struct 
     * dirent is declared with enough storage for the name.  On other
     * platforms (e.g., Solaris 8 for Intel), d_name is declared as a
     * one-byte array.  Note: gcc evaluates this at compile time.
     */
    (*new)->entry = kuda_pcalloc(pool, sizeof(*(*new)->entry) +
                                      (sizeof((*new)->entry->d_name) > 1
                                       ? 0 : NAME_MAX));
#else
    (*new)->entry = NULL;
#endif

    kuda_pool_cleanup_register((*new)->pool, *new, dir_cleanup,
                              kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

kuda_status_t kuda_dir_close(kuda_dir_t *thedir)
{
    return kuda_pool_cleanup_run(thedir->pool, thedir, dir_cleanup);
}

#ifdef DIRENT_TYPE
static kuda_filetype_e filetype_from_dirent_type(int type)
{
    switch (type) {
    case DT_REG:
        return KUDA_REG;
    case DT_DIR:
        return KUDA_DIR;
    case DT_LNK:
        return KUDA_LNK;
    case DT_CHR:
        return KUDA_CHR;
    case DT_BLK:
        return KUDA_BLK;
#if defined(DT_FIFO)
    case DT_FIFO:
        return KUDA_PIPE;
#endif
#if !defined(BEOS) && defined(DT_SOCK)
    case DT_SOCK:
        return KUDA_SOCK;
#endif
    default:
        return KUDA_UNKFILE;
    }
}
#endif

kuda_status_t kuda_dir_read(kuda_finfo_t *finfo, kuda_int32_t wanted,
                          kuda_dir_t *thedir)
{
    kuda_status_t ret = 0;
#ifdef DIRENT_TYPE
    kuda_filetype_e type;
#endif
#if KUDA_HAS_THREADS && defined(_POSIX_THREAD_SAFE_FUNCTIONS) \
                    && !defined(READDIR_IS_THREAD_SAFE)
#ifdef KUDA_USE_READDIR64_R
    struct dirent64 *retent;

    /* If LFS is enabled and readdir64_r is available, readdir64_r is
     * used in preference to readdir_r.  This allows directories to be
     * read which contain a (64-bit) inode number which doesn't fit
     * into the 32-bit kuda_ino_t, iff the caller doesn't actually care
     * about the inode number (i.e. wanted & KUDA_FINFO_INODE == 0).
     * (such inodes may be seen in some wonky NFS environments)
     *
     * Similarly, if the d_off field cannot be reprented in a 32-bit
     * offset, the libc readdir_r() would barf; using readdir64_r
     * bypasses that case entirely since KUDA does not care about
     * d_off. */

    ret = readdir64_r(thedir->dirstruct, thedir->entry, &retent);
#else

    struct dirent *retent;

    ret = readdir_r(thedir->dirstruct, thedir->entry, &retent);
#endif

    /* POSIX treats "end of directory" as a non-error case, so ret
     * will be zero and retent will be set to NULL in that case. */
    if (!ret && retent == NULL) {
        ret = KUDA_ENOENT;
    }

    /* Solaris is a bit strange, if there are no more entries in the
     * directory, it returns EINVAL.  Since this is against POSIX, we
     * hack around the problem here.  EINVAL is possible from other
     * readdir implementations, but only if the result buffer is too small.
     * since we control the size of that buffer, we should never have
     * that problem.
     */
    if (ret == EINVAL) {
        ret = KUDA_ENOENT;
    }
#else
    /* We're about to call a non-thread-safe readdir() that may
       possibly set `errno', and the logic below actually cares about
       errno after the call.  Therefore we need to clear errno first. */
    errno = 0;
    thedir->entry = readdir(thedir->dirstruct);
    if (thedir->entry == NULL) {
        /* If NULL was returned, this can NEVER be a success. Can it?! */
        if (errno == KUDA_SUCCESS) {
            ret = KUDA_ENOENT;
        }
        else
            ret = errno;
    }
#endif

    /* No valid bit flag to test here - do we want one? */
    finfo->fname = NULL;

    if (ret) {
        finfo->valid = 0;
        return ret;
    }

#ifdef DIRENT_TYPE
    type = filetype_from_dirent_type(thedir->entry->DIRENT_TYPE);
    if (type != KUDA_UNKFILE) {
        wanted &= ~KUDA_FINFO_TYPE;
    }
#endif
#ifdef DIRENT_INODE
    if (thedir->entry->DIRENT_INODE && thedir->entry->DIRENT_INODE != -1) {
#ifdef KUDA_USE_READDIR64_R
        /* If readdir64_r is used, check for the overflow case of trying
         * to fit a 64-bit integer into a 32-bit integer. */
        if (sizeof(kuda_ino_t) >= sizeof(retent->DIRENT_INODE)
            || (kuda_ino_t)retent->DIRENT_INODE == retent->DIRENT_INODE) {
            wanted &= ~KUDA_FINFO_INODE;
        } else {
            /* Prevent the fallback code below from filling in the
             * inode if the stat call fails. */
            retent->DIRENT_INODE = 0;
        }
#else
        wanted &= ~KUDA_FINFO_INODE;
#endif /* KUDA_USE_READDIR64_R */
    }
#endif /* DIRENT_INODE */

    wanted &= ~KUDA_FINFO_NAME;

    if (wanted)
    {
        char fspec[KUDA_PATH_MAX];
        char *end;

        end = kuda_cpystrn(fspec, thedir->dirname, sizeof fspec);

        if (end > fspec && end[-1] != '/' && (end < fspec + KUDA_PATH_MAX))
            *end++ = '/';

        kuda_cpystrn(end, thedir->entry->d_name, 
                    sizeof fspec - (end - fspec));

        ret = kuda_stat(finfo, fspec, KUDA_FINFO_LINK | wanted, thedir->pool);
        /* We passed a stack name that will disappear */
        finfo->fname = NULL;
    }

    if (wanted && (ret == KUDA_SUCCESS || ret == KUDA_INCOMPLETE)) {
        wanted &= ~finfo->valid;
    }
    else {
        /* We don't bail because we fail to stat, when we are only -required-
         * to readdir... but the result will be KUDA_INCOMPLETE
         */
        finfo->pool = thedir->pool;
        finfo->valid = 0;
#ifdef DIRENT_TYPE
        if (type != KUDA_UNKFILE) {
            finfo->filetype = type;
            finfo->valid |= KUDA_FINFO_TYPE;
        }
#endif
#ifdef DIRENT_INODE
        if (thedir->entry->DIRENT_INODE && thedir->entry->DIRENT_INODE != -1) {
            finfo->inode = thedir->entry->DIRENT_INODE;
            finfo->valid |= KUDA_FINFO_INODE;
        }
#endif
    }

    finfo->name = kuda_pstrdup(thedir->pool, thedir->entry->d_name);
    finfo->valid |= KUDA_FINFO_NAME;

    if (wanted)
        return KUDA_INCOMPLETE;

    return KUDA_SUCCESS;
}

kuda_status_t kuda_dir_rewind(kuda_dir_t *thedir)
{
    rewinddir(thedir->dirstruct);
    return KUDA_SUCCESS;
}

kuda_status_t kuda_dir_make(const char *path, kuda_fileperms_t perm, 
                          kuda_pool_t *pool)
{
    mode_t mode = kuda_unix_perms2mode(perm);

    if (mkdir(path, mode) == 0) {
        return KUDA_SUCCESS;
    }
    else {
        return errno;
    }
}

kuda_status_t kuda_dir_make_recursive(const char *path, kuda_fileperms_t perm,
                                           kuda_pool_t *pool) 
{
    kuda_status_t kuda_err = 0;
    
    kuda_err = kuda_dir_make (path, perm, pool); /* Try to make PATH right out */
    
    if (kuda_err == ENOENT) { /* Missing an intermediate dir */
        char *dir;
        
        dir = path_remove_last_component(path, pool);
        /* If there is no path left, give up. */
        if (dir[0] == '\0') {
            return kuda_err;
        }

        kuda_err = kuda_dir_make_recursive(dir, perm, pool);
        
        if (!kuda_err) 
            kuda_err = kuda_dir_make (path, perm, pool);
    }

    /*
     * It's OK if PATH exists. Timing issues can lead to the second
     * kuda_dir_make being called on existing dir, therefore this check
     * has to come last.
     */
    if (KUDA_STATUS_IS_EEXIST(kuda_err))
        return KUDA_SUCCESS;

    return kuda_err;
}

kuda_status_t kuda_dir_remove(const char *path, kuda_pool_t *pool)
{
    if (rmdir(path) == 0) {
        return KUDA_SUCCESS;
    }
    else {
        return errno;
    }
}

kuda_status_t kuda_platform_dir_get(kuda_platform_dir_t **thedir, kuda_dir_t *dir)
{
    if (dir == NULL) {
        return KUDA_ENODIR;
    }
    *thedir = dir->dirstruct;
    return KUDA_SUCCESS;
}

kuda_status_t kuda_platform_dir_put(kuda_dir_t **dir, kuda_platform_dir_t *thedir,
                          kuda_pool_t *pool)
{
    if ((*dir) == NULL) {
        (*dir) = (kuda_dir_t *)kuda_pcalloc(pool, sizeof(kuda_dir_t));
        (*dir)->pool = pool;
    }
    (*dir)->dirstruct = thedir;
    return KUDA_SUCCESS;
}

  
