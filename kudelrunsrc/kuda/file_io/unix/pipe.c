/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_strings.h"
#include "kuda_portable.h"

#include "kuda_arch_inherit.h"

/* Figure out how to get pipe block/nonblock on BeOS...
 * Basically, BONE7 changed things again so that ioctl didn't work,
 * but now fcntl does, hence we need to do this extra checking.
 * The joys of beta programs. :-)
 */
#if defined(BEOS)
#if !defined(BONE7)
# define BEOS_BLOCKING 1
#else
# define BEOS_BLOCKING 0
#endif
#endif

static kuda_status_t pipeblock(kuda_file_t *thepipe)
{
#if !defined(BEOS) || !BEOS_BLOCKING
      int fd_flags;

      fd_flags = fcntl(thepipe->filedes, F_GETFL, 0);
#  if defined(O_NONBLOCK)
      fd_flags &= ~O_NONBLOCK;
#  elif defined(O_NDELAY)
      fd_flags &= ~O_NDELAY;
#  elif defined(O_FNDELAY)
      fd_flags &= ~O_FNDELAY;
#  else 
      /* XXXX: this breaks things, but an alternative isn't obvious...*/
      return KUDA_ENOTIMPL;
#  endif
      if (fcntl(thepipe->filedes, F_SETFL, fd_flags) == -1) {
          return errno;
      }
#else /* BEOS_BLOCKING */

#  if BEOS_BONE /* This only works on BONE 0-6 */
      int on = 0;
      if (ioctl(thepipe->filedes, FIONBIO, &on, sizeof(on)) < 0) {
          return errno;
      }
#  else /* "classic" BeOS doesn't support this at all */
      return KUDA_ENOTIMPL;
#  endif 
 
#endif /* !BEOS_BLOCKING */

    thepipe->blocking = BLK_ON;
    return KUDA_SUCCESS;
}

static kuda_status_t pipenonblock(kuda_file_t *thepipe)
{
#if !defined(BEOS) || !BEOS_BLOCKING
      int fd_flags = fcntl(thepipe->filedes, F_GETFL, 0);

#  if defined(O_NONBLOCK)
      fd_flags |= O_NONBLOCK;
#  elif defined(O_NDELAY)
      fd_flags |= O_NDELAY;
#  elif defined(O_FNDELAY)
      fd_flags |= O_FNDELAY;
#  else
      /* XXXX: this breaks things, but an alternative isn't obvious...*/
      return KUDA_ENOTIMPL;
#  endif
      if (fcntl(thepipe->filedes, F_SETFL, fd_flags) == -1) {
          return errno;
      }
    
#else /* BEOS_BLOCKING */

#  if BEOS_BONE /* This only works on BONE 0-6 */
      int on = 1;
      if (ioctl(thepipe->filedes, FIONBIO, &on, sizeof(on)) < 0) {
          return errno;
      }
#  else /* "classic" BeOS doesn't support this at all */
      return KUDA_ENOTIMPL;
#  endif

#endif /* !BEOS_BLOCKING */

    thepipe->blocking = BLK_OFF;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_timeout_set(kuda_file_t *thepipe, kuda_interval_time_t timeout)
{
    if (thepipe->is_pipe == 1) {
        thepipe->timeout = timeout;
        if (timeout >= 0) {
            if (thepipe->blocking != BLK_OFF) { /* blocking or unknown state */
                return pipenonblock(thepipe);
            }
        }
        else {
            if (thepipe->blocking != BLK_ON) { /* non-blocking or unknown state */
                return pipeblock(thepipe);
            }
        }
        return KUDA_SUCCESS;
    }
    return KUDA_EINVAL;
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_timeout_get(kuda_file_t *thepipe, kuda_interval_time_t *timeout)
{
    if (thepipe->is_pipe == 1) {
        *timeout = thepipe->timeout;
        return KUDA_SUCCESS;
    }
    return KUDA_EINVAL;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_pipe_put_ex(kuda_file_t **file,
                                             kuda_platform_file_t *thefile,
                                             int register_cleanup,
                                             kuda_pool_t *pool)
{
    int *dafile = thefile;
    
    (*file) = kuda_pcalloc(pool, sizeof(kuda_file_t));
    (*file)->pool = pool;
    (*file)->eof_hit = 0;
    (*file)->is_pipe = 1;
    (*file)->blocking = BLK_UNKNOWN; /* app needs to make a timeout call */
    (*file)->timeout = -1;
    (*file)->ungetchar = -1; /* no char avail */
    (*file)->filedes = *dafile;
    if (!register_cleanup) {
        (*file)->flags = KUDA_FOPEN_NOCLEANUP;
    }
    (*file)->buffered = 0;
#if KUDA_HAS_THREADS
    (*file)->thlock = NULL;
#endif
    if (register_cleanup) {
        kuda_pool_cleanup_register((*file)->pool, (void *)(*file),
                                  kuda_unix_file_cleanup,
                                  kuda_pool_cleanup_null);
    }
#ifndef WAITIO_USES_POLL
    /* Start out with no pollset.  kuda_wait_for_io_or_timeout() will
     * initialize the pollset if needed.
     */
    (*file)->pollset = NULL;
#endif
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_pipe_put(kuda_file_t **file,
                                          kuda_platform_file_t *thefile,
                                          kuda_pool_t *pool)
{
    return kuda_platform_pipe_put_ex(file, thefile, 0, pool);
}

static kuda_status_t file_pipe_create(kuda_file_t **in, kuda_file_t **out,
        kuda_pool_t *pool_in, kuda_pool_t *pool_out)
{
    int filedes[2];

    if (pipe(filedes) == -1) {
        return errno;
    }
    
    (*in) = (kuda_file_t *)kuda_pcalloc(pool_in, sizeof(kuda_file_t));
    (*in)->pool = pool_in;
    (*in)->filedes = filedes[0];
    (*in)->is_pipe = 1;
    (*in)->fname = NULL;
    (*in)->buffered = 0;
    (*in)->blocking = BLK_ON;
    (*in)->timeout = -1;
    (*in)->ungetchar = -1;
    (*in)->flags = KUDA_INHERIT;
#if KUDA_HAS_THREADS
    (*in)->thlock = NULL;
#endif
#ifndef WAITIO_USES_POLL
    (*in)->pollset = NULL;
#endif
    (*out) = (kuda_file_t *)kuda_pcalloc(pool_out, sizeof(kuda_file_t));
    (*out)->pool = pool_out;
    (*out)->filedes = filedes[1];
    (*out)->is_pipe = 1;
    (*out)->fname = NULL;
    (*out)->buffered = 0;
    (*out)->blocking = BLK_ON;
    (*out)->flags = KUDA_INHERIT;
    (*out)->timeout = -1;
#if KUDA_HAS_THREADS
    (*out)->thlock = NULL;
#endif
#ifndef WAITIO_USES_POLL
    (*out)->pollset = NULL;
#endif
    kuda_pool_cleanup_register((*in)->pool, (void *)(*in), kuda_unix_file_cleanup,
                         kuda_pool_cleanup_null);
    kuda_pool_cleanup_register((*out)->pool, (void *)(*out), kuda_unix_file_cleanup,
                         kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

static void file_pipe_block(kuda_file_t **in, kuda_file_t **out, kuda_int32_t blocking)
{
    switch (blocking) {
    case KUDA_FULL_BLOCK:
        break;
    case KUDA_READ_BLOCK:
        kuda_file_pipe_timeout_set(*out, 0);
        break;
    case KUDA_WRITE_BLOCK:
        kuda_file_pipe_timeout_set(*in, 0);
        break;
    default:
        kuda_file_pipe_timeout_set(*out, 0);
        kuda_file_pipe_timeout_set(*in, 0);
        break;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_create(kuda_file_t **in,
        kuda_file_t **out, kuda_pool_t *pool)
{
    return file_pipe_create(in, out, pool, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_create_ex(kuda_file_t **in, 
                                                  kuda_file_t **out, 
                                                  kuda_int32_t blocking,
                                                  kuda_pool_t *pool)
{
    kuda_status_t status;

    if ((status = file_pipe_create(in, out, pool, pool)) != KUDA_SUCCESS) {
        return status;
    }

    file_pipe_block(in, out, blocking);

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_create_pools(kuda_file_t **in,
        kuda_file_t **out, kuda_int32_t blocking, kuda_pool_t *pool_in, kuda_pool_t *pool_out)
{
    kuda_status_t status;

    if ((status = file_pipe_create(in, out, pool_in, pool_out)) != KUDA_SUCCESS) {
        return status;
    }

    file_pipe_block(in, out, blocking);

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_namedpipe_create(const char *filename, 
                                                    kuda_fileperms_t perm, kuda_pool_t *pool)
{
    mode_t mode = kuda_unix_perms2mode(perm);

    if (mkfifo(filename, mode) == -1) {
        return errno;
    }
    return KUDA_SUCCESS;
} 

    

