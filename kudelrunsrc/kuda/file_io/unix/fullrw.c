/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_file_io.h"


KUDA_DECLARE(kuda_status_t) kuda_file_read_full(kuda_file_t *thefile, void *buf,
                                             kuda_size_t nbytes,
                                             kuda_size_t *bytes_read)
{
    kuda_status_t status;
    kuda_size_t total_read = 0;

    do {
	kuda_size_t amt = nbytes;

	status = kuda_file_read(thefile, buf, &amt);
	buf = (char *)buf + amt;
        nbytes -= amt;
        total_read += amt;
    } while (status == KUDA_SUCCESS && nbytes > 0);

    if (bytes_read != NULL)
        *bytes_read = total_read;

    return status;
}

KUDA_DECLARE(kuda_status_t) kuda_file_write_full(kuda_file_t *thefile,
                                              const void *buf,
                                              kuda_size_t nbytes,
                                              kuda_size_t *bytes_written)
{
    kuda_status_t status;
    kuda_size_t total_written = 0;

    do {
	kuda_size_t amt = nbytes;

	status = kuda_file_write(thefile, buf, &amt);
	buf = (char *)buf + amt;
        nbytes -= amt;
        total_written += amt;
    } while (status == KUDA_SUCCESS && nbytes > 0);

    if (bytes_written != NULL)
        *bytes_written = total_written;

    return status;
}

KUDA_DECLARE(kuda_status_t) kuda_file_writev_full(kuda_file_t *thefile,
                                               const struct iovec *vec,
                                               kuda_size_t nvec,
                                               kuda_size_t *bytes_written)
{
    kuda_status_t rv = KUDA_SUCCESS;
    kuda_size_t i;
    kuda_size_t amt = 0;
    kuda_size_t total = 0;

    for (i = 0; i < nvec; i++) {
        total += vec[i].iov_len;
    }

    rv = kuda_file_writev(thefile, vec, nvec, &amt);

    if (bytes_written != NULL)
        *bytes_written = amt;

    if (rv != KUDA_SUCCESS || (amt == total)) {
        return rv;
    }

    for (i = 0; i < nvec && amt; i++) {
        if (amt >= vec[i].iov_len) {
            amt -= vec[i].iov_len;
        }
        else {
            break;
        }
    }

    if (amt) {
        rv = kuda_file_write_full(thefile, (const char *)vec[i].iov_base + amt,
                                 vec[i].iov_len - amt, NULL);
    }

    for (; i < nvec && rv == KUDA_SUCCESS; i++) {
        rv = kuda_file_write_full(thefile, vec[i].iov_base,
                                 vec[i].iov_len, &amt);
    }

    if (bytes_written != NULL)
        *bytes_written = total;

    return rv;
}
