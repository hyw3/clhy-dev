/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_strings.h"
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif

/* Win32 malpropism that can go away once everyone believes this
 * code is golden, and I'm not testing it anymore :-)
 */
#if KUDA_HAVE_DIRENT_H
#include <dirent.h>
#endif

/* Any PLATFORM that requires/refuses trailing slashes should be dealt with here.
 */
KUDA_DECLARE(kuda_status_t) kuda_filepath_get(char **defpath, kuda_int32_t flags,
                                           kuda_pool_t *p)
{
    char path[KUDA_PATH_MAX];

    if (!getcwd(path, sizeof(path))) {
        if (errno == ERANGE)
            return KUDA_ENAMETOOLONG;
        else
            return errno;
    }
    *defpath = kuda_pstrdup(p, path);

    return KUDA_SUCCESS;
}


/* Any PLATFORM that requires/refuses trailing slashes should be dealt with here
 */
KUDA_DECLARE(kuda_status_t) kuda_filepath_set(const char *path, kuda_pool_t *p)
{
    if (chdir(path) != 0)
        return errno;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_filepath_root(const char **rootpath,
                                            const char **inpath,
                                            kuda_int32_t flags,
                                            kuda_pool_t *p)
{
    if (**inpath == '/') {
        *rootpath = kuda_pstrdup(p, "/");
        do {
            ++(*inpath);
        } while (**inpath == '/');

        return KUDA_SUCCESS;
    }

    return KUDA_ERELATIVE;
}

KUDA_DECLARE(kuda_status_t) kuda_filepath_merge(char **newpath,
                                             const char *rootpath,
                                             const char *addpath,
                                             kuda_int32_t flags,
                                             kuda_pool_t *p)
{
    char *path;
    kuda_size_t rootlen; /* is the length of the src rootpath */
    kuda_size_t maxlen;  /* maximum total path length */
    kuda_size_t keptlen; /* is the length of the retained rootpath */
    kuda_size_t pathlen; /* is the length of the result path */
    kuda_size_t seglen;  /* is the end of the current segment */
    kuda_status_t rv;

    /* Treat null as an empty path.
     */
    if (!addpath)
        addpath = "";

    if (addpath[0] == '/') {
        /* If addpath is rooted, then rootpath is unused.
         * Ths violates any KUDA_FILEPATH_SECUREROOTTEST and
         * KUDA_FILEPATH_NOTABSOLUTE flags specified.
         */
        if (flags & KUDA_FILEPATH_SECUREROOTTEST)
            return KUDA_EABOVEROOT;
        if (flags & KUDA_FILEPATH_NOTABSOLUTE)
            return KUDA_EABSOLUTE;

        /* If KUDA_FILEPATH_NOTABOVEROOT wasn't specified,
         * we won't test the root again, it's ignored.
         * Waste no CPU retrieving the working path.
         */
        if (!rootpath && !(flags & KUDA_FILEPATH_NOTABOVEROOT))
            rootpath = "";
    }
    else {
        /* If KUDA_FILEPATH_NOTABSOLUTE is specified, the caller
         * requires a relative result.  If the rootpath is
         * ommitted, we do not retrieve the working path,
         * if rootpath was supplied as absolute then fail.
         */
        if (flags & KUDA_FILEPATH_NOTABSOLUTE) {
            if (!rootpath)
                rootpath = "";
            else if (rootpath[0] == '/')
                return KUDA_EABSOLUTE;
        }
    }

    if (!rootpath) {
        /* Start with the current working path.  This is bass akwards,
         * but required since the compiler (at least vc) doesn't like
         * passing the address of a char const* for a char** arg.
         */
        char *getpath;
        rv = kuda_filepath_get(&getpath, flags, p);
        rootpath = getpath;
        if (rv != KUDA_SUCCESS)
            return errno;

        /* XXX: Any kernel subject to goofy, uncanonical results
         * must run the rootpath against the user's given flags.
         * Simplest would be a recursive call to kuda_filepath_merge
         * with an empty (not null) rootpath and addpath of the cwd.
         */
    }

    rootlen = strlen(rootpath);
    maxlen = rootlen + strlen(addpath) + 4; /* 4 for slashes at start, after
                                             * root, and at end, plus trailing
                                             * null */
    if (maxlen > KUDA_PATH_MAX) {
        return KUDA_ENAMETOOLONG;
    }
    path = (char *)kuda_palloc(p, maxlen);

    if (addpath[0] == '/') {
        /* Ignore the given root path, strip off leading
         * '/'s to a single leading '/' from the addpath,
         * and leave addpath at the first non-'/' character.
         */
        keptlen = 0;
        while (addpath[0] == '/')
            ++addpath;
        path[0] = '/';
        pathlen = 1;
    }
    else {
        /* If both paths are relative, fail early
         */
        if (rootpath[0] != '/' && (flags & KUDA_FILEPATH_NOTRELATIVE))
            return KUDA_ERELATIVE;

        /* Base the result path on the rootpath
         */
        keptlen = rootlen;
        memcpy(path, rootpath, rootlen);

        /* Always '/' terminate the given root path
         */
        if (keptlen && path[keptlen - 1] != '/') {
            path[keptlen++] = '/';
        }
        pathlen = keptlen;
    }

    while (*addpath) {
        /* Parse each segment, find the closing '/'
         */
        const char *next = addpath;
        while (*next && (*next != '/')) {
            ++next;
        }
        seglen = next - addpath;

        if (seglen == 0 || (seglen == 1 && addpath[0] == '.')) {
            /* noop segment (/ or ./) so skip it
             */
        }
        else if (seglen == 2 && addpath[0] == '.' && addpath[1] == '.') {
            /* backpath (../) */
            if (pathlen == 1 && path[0] == '/') {
                /* Attempt to move above root.  Always die if the
                 * KUDA_FILEPATH_SECUREROOTTEST flag is specified.
                 */
                if (flags & KUDA_FILEPATH_SECUREROOTTEST) {
                    return KUDA_EABOVEROOT;
                }

                /* Otherwise this is simply a noop, above root is root.
                 * Flag that rootpath was entirely replaced.
                 */
                keptlen = 0;
            }
            else if (pathlen == 0
                     || (pathlen == 3
                         && !memcmp(path + pathlen - 3, "../", 3))
                     || (pathlen  > 3
                         && !memcmp(path + pathlen - 4, "/../", 4))) {
                /* Path is already backpathed or empty, if the
                 * KUDA_FILEPATH_SECUREROOTTEST.was given die now.
                 */
                if (flags & KUDA_FILEPATH_SECUREROOTTEST) {
                    return KUDA_EABOVEROOT;
                }

                /* Otherwise append another backpath, including
                 * trailing slash if present.
                 */
                memcpy(path + pathlen, "../", *next ? 3 : 2);
                pathlen += *next ? 3 : 2;
            }
            else {
                /* otherwise crop the prior segment
                 */
                do {
                    --pathlen;
                } while (pathlen && path[pathlen - 1] != '/');
            }

            /* Now test if we are above where we started and back up
             * the keptlen offset to reflect the added/altered path.
             */
            if (pathlen < keptlen) {
                if (flags & KUDA_FILEPATH_SECUREROOTTEST) {
                    return KUDA_EABOVEROOT;
                }
                keptlen = pathlen;
            }
        }
        else {
            /* An actual segment, append it to the destination path
             */
            if (*next) {
                seglen++;
            }
            memcpy(path + pathlen, addpath, seglen);
            pathlen += seglen;
        }

        /* Skip over trailing slash to the next segment
         */
        if (*next) {
            ++next;
        }

        addpath = next;
    }
    path[pathlen] = '\0';

    /* keptlen will be the rootlen unless the addpath contained
     * backpath elements.  If so, and KUDA_FILEPATH_NOTABOVEROOT
     * is specified (KUDA_FILEPATH_SECUREROOTTEST was caught above),
     * compare the original root to assure the result path is
     * still within given root path.
     */
    if ((flags & KUDA_FILEPATH_NOTABOVEROOT) && keptlen < rootlen) {
        if (strncmp(rootpath, path, rootlen)) {
            return KUDA_EABOVEROOT;
        }
        if (rootpath[rootlen - 1] != '/'
            && path[rootlen] && path[rootlen] != '/') {
            return KUDA_EABOVEROOT;
        }
    }

    *newpath = path;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_filepath_list_split(kuda_array_header_t **pathelts,
                                                  const char *liststr,
                                                  kuda_pool_t *p)
{
    return kuda_filepath_list_split_impl(pathelts, liststr, ':', p);
}

KUDA_DECLARE(kuda_status_t) kuda_filepath_list_merge(char **liststr,
                                                  kuda_array_header_t *pathelts,
                                                  kuda_pool_t *p)
{
    return kuda_filepath_list_merge_impl(liststr, pathelts, ':', p);
}

KUDA_DECLARE(kuda_status_t) kuda_filepath_encoding(int *style, kuda_pool_t *p)
{
#if defined(DARWIN)
    *style = KUDA_FILEPATH_ENCODING_UTF8;
#else
    *style = KUDA_FILEPATH_ENCODING_LOCALE;
#endif
    return KUDA_SUCCESS;
}
