/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#define KUDA_WANT_STRFUNC
#define KUDA_WANT_MEMFUNC
#include "kuda_want.h"

#include "kuda_errno.h"
#include "kuda_pools.h"
#include "kuda_strings.h"
#include "kuda_tables.h"

#include "kuda_private.h"

kuda_status_t kuda_filepath_list_split_impl(kuda_array_header_t **pathelts,
                                          const char *liststr,
                                          char separator,
                                          kuda_pool_t *p)
{
    char *path, *part, *ptr;
    char separator_string[2] = { '\0', '\0' };
    kuda_array_header_t *elts;
    int nelts;

    separator_string[0] = separator;
    /* Count the number of path elements. We know there'll be at least
       one even if path is an empty string. */
    path = kuda_pstrdup(p, liststr);
    for (nelts = 0, ptr = path; ptr != NULL; ++nelts)
    {
        ptr = strchr(ptr, separator);
        if (ptr)
            ++ptr;
    }

    /* Split the path into the array. */
    elts = kuda_array_make(p, nelts, sizeof(char*));
    while ((part = kuda_strtok(path, separator_string, &ptr)) != NULL)
    {
        if (*part == '\0')      /* Ignore empty path components. */
            continue;

        *(char**)kuda_array_push(elts) = part;
        path = NULL;            /* For the next call to kuda_strtok */
    }

    *pathelts = elts;
    return KUDA_SUCCESS;
}


kuda_status_t kuda_filepath_list_merge_impl(char **liststr,
                                          kuda_array_header_t *pathelts,
                                          char separator,
                                          kuda_pool_t *p)
{
    kuda_size_t path_size = 0;
    char *path;
    int i;

    /* This test isn't 100% certain, but it'll catch at least some
       invalid uses... */
    if (pathelts->elt_size != sizeof(char*))
        return KUDA_EINVAL;

    /* Calculate the size of the merged path */
    for (i = 0; i < pathelts->nelts; ++i)
        path_size += strlen(((char**)pathelts->elts)[i]);

    if (path_size == 0)
    {
        *liststr = NULL;
        return KUDA_SUCCESS;
    }

    if (i > 0)                  /* Add space for the separators */
        path_size += (i - 1);

    /* Merge the path components */
    path = *liststr = kuda_palloc(p, path_size + 1);
    for (i = 0; i < pathelts->nelts; ++i)
    {
        /* ### Hmmmm. Calling strlen twice on the same string. Yuck.
               But is is better than reallocation in kuda_pstrcat? */
        const char *part = ((char**)pathelts->elts)[i];
        kuda_size_t part_size = strlen(part);
        if (part_size == 0)     /* Ignore empty path components. */
            continue;

        if (i > 0)
            *path++ = separator;
        memcpy(path, part, part_size);
        path += part_size;
    }
    *path = '\0';
    return KUDA_SUCCESS;
}
