/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_errno.h"

#ifdef HAVE_UTIME
#include <utime.h>
#endif

static kuda_filetype_e filetype_from_mode(mode_t mode)
{
    kuda_filetype_e type;

    switch (mode & S_IFMT) {
    case S_IFREG:
        type = KUDA_REG;  break;
    case S_IFDIR:
        type = KUDA_DIR;  break;
    case S_IFLNK:
        type = KUDA_LNK;  break;
    case S_IFCHR:
        type = KUDA_CHR;  break;
    case S_IFBLK:
        type = KUDA_BLK;  break;
#if defined(S_IFFIFO)
    case S_IFFIFO:
        type = KUDA_PIPE; break;
#endif
#if !defined(BEOS) && defined(S_IFSOCK)
    case S_IFSOCK:
        type = KUDA_SOCK; break;
#endif

    default:
	/* Work around missing S_IFxxx values above
         * for Linux et al.
         */
#if !defined(S_IFFIFO) && defined(S_ISFIFO)
    	if (S_ISFIFO(mode)) {
            type = KUDA_PIPE;
	} else
#endif
#if !defined(BEOS) && !defined(S_IFSOCK) && defined(S_ISSOCK)
    	if (S_ISSOCK(mode)) {
            type = KUDA_SOCK;
	} else
#endif
        type = KUDA_UNKFILE;
    }
    return type;
}

static void fill_out_finfo(kuda_finfo_t *finfo, struct_stat *info,
                           kuda_int32_t wanted)
{ 
    finfo->valid = KUDA_FINFO_MIN | KUDA_FINFO_IDENT | KUDA_FINFO_NLINK
                 | KUDA_FINFO_OWNER | KUDA_FINFO_PROT;
    finfo->protection = kuda_unix_mode2perms(info->st_mode);
    finfo->filetype = filetype_from_mode(info->st_mode);
    finfo->user = info->st_uid;
    finfo->group = info->st_gid;
    finfo->size = info->st_size;
    finfo->device = info->st_dev;
    finfo->nlink = info->st_nlink;

    /* Check for overflow if storing a 64-bit st_ino in a 32-bit
     * kuda_ino_t for LFS builds: */
    if (sizeof(kuda_ino_t) >= sizeof(info->st_ino)
        || (kuda_ino_t)info->st_ino == info->st_ino) {
        finfo->inode = info->st_ino;
    } else {
        finfo->valid &= ~KUDA_FINFO_INODE;
    }

    kuda_time_ansi_put(&finfo->atime, info->st_atime);
#ifdef HAVE_STRUCT_STAT_ST_ATIM_TV_NSEC
    finfo->atime += info->st_atim.tv_nsec / KUDA_TIME_C(1000);
#elif defined(HAVE_STRUCT_STAT_ST_ATIMENSEC)
    finfo->atime += info->st_atimensec / KUDA_TIME_C(1000);
#elif defined(HAVE_STRUCT_STAT_ST_ATIME_N)
    finfo->atime += info->st_atime_n / KUDA_TIME_C(1000);
#endif

    kuda_time_ansi_put(&finfo->mtime, info->st_mtime);
#ifdef HAVE_STRUCT_STAT_ST_MTIM_TV_NSEC
    finfo->mtime += info->st_mtim.tv_nsec / KUDA_TIME_C(1000);
#elif defined(HAVE_STRUCT_STAT_ST_MTIMENSEC)
    finfo->mtime += info->st_mtimensec / KUDA_TIME_C(1000);
#elif defined(HAVE_STRUCT_STAT_ST_MTIME_N)
    finfo->mtime += info->st_mtime_n / KUDA_TIME_C(1000);
#endif

    kuda_time_ansi_put(&finfo->ctime, info->st_ctime);
#ifdef HAVE_STRUCT_STAT_ST_CTIM_TV_NSEC
    finfo->ctime += info->st_ctim.tv_nsec / KUDA_TIME_C(1000);
#elif defined(HAVE_STRUCT_STAT_ST_CTIMENSEC)
    finfo->ctime += info->st_ctimensec / KUDA_TIME_C(1000);
#elif defined(HAVE_STRUCT_STAT_ST_CTIME_N)
    finfo->ctime += info->st_ctime_n / KUDA_TIME_C(1000);
#endif

#ifdef HAVE_STRUCT_STAT_ST_BLOCKS
#ifdef DEV_BSIZE
    finfo->csize = (kuda_off_t)info->st_blocks * (kuda_off_t)DEV_BSIZE;
#else
    finfo->csize = (kuda_off_t)info->st_blocks * (kuda_off_t)512;
#endif
    finfo->valid |= KUDA_FINFO_CSIZE;
#endif
}

kuda_status_t kuda_file_info_get_locked(kuda_finfo_t *finfo, kuda_int32_t wanted,
                                      kuda_file_t *thefile)
{
    struct_stat info;

    if (thefile->buffered) {
        kuda_status_t rv = kuda_file_flush_locked(thefile);
        if (rv != KUDA_SUCCESS)
            return rv;
    }

    if (fstat(thefile->filedes, &info) == 0) {
        finfo->pool = thefile->pool;
        finfo->fname = thefile->fname;
        fill_out_finfo(finfo, &info, wanted);
        return (wanted & ~finfo->valid) ? KUDA_INCOMPLETE : KUDA_SUCCESS;
    }
    else {
        return errno;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_file_info_get(kuda_finfo_t *finfo, 
                                            kuda_int32_t wanted,
                                            kuda_file_t *thefile)
{
    struct_stat info;

    if (thefile->buffered) {
        kuda_status_t rv = kuda_file_flush(thefile);
        if (rv != KUDA_SUCCESS)
            return rv;
    }

    if (fstat(thefile->filedes, &info) == 0) {
        finfo->pool = thefile->pool;
        finfo->fname = thefile->fname;
        fill_out_finfo(finfo, &info, wanted);
        return (wanted & ~finfo->valid) ? KUDA_INCOMPLETE : KUDA_SUCCESS;
    }
    else {
        return errno;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_file_perms_set(const char *fname, 
                                             kuda_fileperms_t perms)
{
    mode_t mode = kuda_unix_perms2mode(perms);

    if (chmod(fname, mode) == -1)
        return errno;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_attrs_set(const char *fname,
                                             kuda_fileattrs_t attributes,
                                             kuda_fileattrs_t attr_mask,
                                             kuda_pool_t *pool)
{
    kuda_status_t status;
    kuda_finfo_t finfo;

    /* Don't do anything if we can't handle the requested attributes */
    if (!(attr_mask & (KUDA_FILE_ATTR_READONLY
                       | KUDA_FILE_ATTR_EXECUTABLE)))
        return KUDA_SUCCESS;

    status = kuda_stat(&finfo, fname, KUDA_FINFO_PROT, pool);
    if (status)
        return status;

    /* ### TODO: should added bits be umask'd? */
    if (attr_mask & KUDA_FILE_ATTR_READONLY)
    {
        if (attributes & KUDA_FILE_ATTR_READONLY)
        {
            finfo.protection &= ~KUDA_UWRITE;
            finfo.protection &= ~KUDA_GWRITE;
            finfo.protection &= ~KUDA_WWRITE;
        }
        else
        {
            /* ### umask this! */
            finfo.protection |= KUDA_UWRITE;
            finfo.protection |= KUDA_GWRITE;
            finfo.protection |= KUDA_WWRITE;
        }
    }

    if (attr_mask & KUDA_FILE_ATTR_EXECUTABLE)
    {
        if (attributes & KUDA_FILE_ATTR_EXECUTABLE)
        {
            /* ### umask this! */
            finfo.protection |= KUDA_UEXECUTE;
            finfo.protection |= KUDA_GEXECUTE;
            finfo.protection |= KUDA_WEXECUTE;
        }
        else
        {
            finfo.protection &= ~KUDA_UEXECUTE;
            finfo.protection &= ~KUDA_GEXECUTE;
            finfo.protection &= ~KUDA_WEXECUTE;
        }
    }

    return kuda_file_perms_set(fname, finfo.protection);
}


KUDA_DECLARE(kuda_status_t) kuda_file_mtime_set(const char *fname,
                                              kuda_time_t mtime,
                                              kuda_pool_t *pool)
{
    kuda_status_t status;
    kuda_finfo_t finfo;

    status = kuda_stat(&finfo, fname, KUDA_FINFO_ATIME, pool);
    if (status) {
        return status;
    }

#ifdef HAVE_UTIMES
    {
      struct timeval tvp[2];
    
      tvp[0].tv_sec = kuda_time_sec(finfo.atime);
      tvp[0].tv_usec = kuda_time_usec(finfo.atime);
      tvp[1].tv_sec = kuda_time_sec(mtime);
      tvp[1].tv_usec = kuda_time_usec(mtime);
      
      if (utimes(fname, tvp) == -1) {
        return errno;
      }
    }
#elif defined(HAVE_UTIME)
    {
      struct utimbuf buf;
      
      buf.actime = (time_t) (finfo.atime / KUDA_USEC_PER_SEC);
      buf.modtime = (time_t) (mtime / KUDA_USEC_PER_SEC);
      
      if (utime(fname, &buf) == -1) {
        return errno;
      }
    }
#else
    return KUDA_ENOTIMPL;
#endif

    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_stat(kuda_finfo_t *finfo, 
                                   const char *fname, 
                                   kuda_int32_t wanted, kuda_pool_t *pool)
{
    struct_stat info;
    int srv;

    if (wanted & KUDA_FINFO_LINK)
        srv = lstat(fname, &info);
    else
        srv = stat(fname, &info);

    if (srv == 0) {
        finfo->pool = pool;
        finfo->fname = fname;
        fill_out_finfo(finfo, &info, wanted);
        if (wanted & KUDA_FINFO_LINK)
            wanted &= ~KUDA_FINFO_LINK;
        return (wanted & ~finfo->valid) ? KUDA_INCOMPLETE : KUDA_SUCCESS;
    }
    else {
#if !defined(ENOENT) || !defined(ENOTDIR)
#error ENOENT || ENOTDIR not defined; please see the
#error comments at this line in the source for a workaround.
        /*
         * If ENOENT || ENOTDIR is not defined in one of the your PLATFORM's
         * include files, KUDA cannot report a good reason why the stat()
         * of the file failed; there are cases where it can fail even though
         * the file exists.  This opens holes in cLHy, for example, because
         * it becomes possible for someone to get a directory listing of a 
         * directory even though there is an index (eg. index.html) file in 
         * it.  If you do not have a problem with this, delete the above 
         * #error lines and start the compile again.  If you need to do this,
         * submit new ticket at http://clhy.hyang.org/clhy-dev/clhy/ticket
         * letting us know that you needed to do this.  Please be sure to 
         * include the operating system you are using.
         */
        /* WARNING: All errors will be handled as not found
         */
#if !defined(ENOENT) 
        return KUDA_ENOENT;
#else
        /* WARNING: All errors but not found will be handled as not directory
         */
        if (errno != ENOENT)
            return KUDA_ENOENT;
        else
            return errno;
#endif
#else /* All was defined well, report the usual: */
        return errno;
#endif
    }
}


