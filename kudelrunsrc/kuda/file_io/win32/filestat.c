/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include <aclapi.h>
#include "kuda_private.h"
#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_errno.h"
#include "kuda_time.h"
#include <sys/stat.h>
#include "kuda_arch_atime.h"
#include "kuda_arch_misc.h"

/* We have to assure that the file name contains no '*'s, or other
 * wildcards when using FindFirstFile to recover the true file name.
 */
static kuda_status_t test_safe_name(const char *name)
{
    /* Only accept ':' in the second position of the filename,
     * as the drive letter delimiter:
     */
    if (kuda_isalpha(*name) && (name[1] == ':')) {
        name += 2;
    }
    while (*name) {
        if (!IS_FNCHAR(*name) && (*name != '\\') && (*name != '/')) {
            if (*name == '?' || *name == '*')
                return KUDA_EPATHWILD;
            else
                return KUDA_EBADPATH;
        }
        ++name;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t free_localheap(void *heap) {
    LocalFree(heap);
    return KUDA_SUCCESS;
}

static kuda_gid_t worldid = NULL;

static void free_world(void)
{
    if (worldid) {
        FreeSid(worldid);
        worldid = NULL;
    }
}

/* Left bit shifts from World scope to given scope */
typedef enum prot_scope_e {
    prot_scope_world = 0,
    prot_scope_group = 4,
    prot_scope_user =  8
} prot_scope_e;

static kuda_fileperms_t convert_prot(ACCESS_MASK acc, prot_scope_e scope)
{
    /* These choices are based on the single filesystem bit that controls
     * the given behavior.  They are -not- recommended for any set protection
     * function, such a function should -set- use GENERIC_READ/WRITE/EXECUTE
     */
    kuda_fileperms_t prot = 0;
    if (acc & FILE_EXECUTE)
        prot |= KUDA_WEXECUTE;
    if (acc & FILE_WRITE_DATA)
        prot |= KUDA_WWRITE;
    if (acc & FILE_READ_DATA)
        prot |= KUDA_WREAD;
    return (prot << scope);
}

static void resolve_prot(kuda_finfo_t *finfo, kuda_int32_t wanted, PACL dacl)
{
    TRUSTEE_W ident = {NULL, NO_MULTIPLE_TRUSTEE, TRUSTEE_IS_SID};
    ACCESS_MASK acc;
    /*
     * This function is only invoked for WinNT, 
     * there is no reason for platform_level testing here.
     */
    if ((wanted & KUDA_FINFO_WPROT) && !worldid) {
        SID_IDENTIFIER_AUTHORITY SIDAuth = {SECURITY_WORLD_SID_AUTHORITY};
        if (AllocateAndInitializeSid(&SIDAuth, 1, SECURITY_WORLD_RID,
                                     0, 0, 0, 0, 0, 0, 0, &worldid))
            atexit(free_world);
        else
            worldid = NULL;
    }
    if ((wanted & KUDA_FINFO_UPROT) && (finfo->valid & KUDA_FINFO_USER)) {
        ident.TrusteeType = TRUSTEE_IS_USER;
        ident.ptstrName = finfo->user;
        /* GetEffectiveRightsFromAcl isn't supported under Win9x,
         * which shouldn't come as a surprize.  Since we are passing
         * TRUSTEE_IS_SID, always skip the A->W layer.
         */
        if (GetEffectiveRightsFromAclW(dacl, &ident, &acc) == ERROR_SUCCESS) {
            finfo->protection |= convert_prot(acc, prot_scope_user);
            finfo->valid |= KUDA_FINFO_UPROT;
        }
    }
    /* Windows NT: did not return group rights.
     * Windows 2000 returns group rights information.
     * Since WinNT kernels don't follow the unix model of 
     * group associations, this all all pretty mute.
     */
    if ((wanted & KUDA_FINFO_GPROT) && (finfo->valid & KUDA_FINFO_GROUP)) {
        ident.TrusteeType = TRUSTEE_IS_GROUP;
        ident.ptstrName = finfo->group;
        if (GetEffectiveRightsFromAclW(dacl, &ident, &acc) == ERROR_SUCCESS) {
            finfo->protection |= convert_prot(acc, prot_scope_group);
            finfo->valid |= KUDA_FINFO_GPROT;
        }
    }
    if ((wanted & KUDA_FINFO_WPROT) && (worldid)) {
        ident.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
        ident.ptstrName = worldid;
        if (GetEffectiveRightsFromAclW(dacl, &ident, &acc) == ERROR_SUCCESS) {
            finfo->protection |= convert_prot(acc, prot_scope_world);
            finfo->valid |= KUDA_FINFO_WPROT;
        }
    }
}

static kuda_status_t resolve_ident(kuda_finfo_t *finfo, const char *fname,
                                  kuda_int32_t wanted, kuda_pool_t *pool)
{
    kuda_file_t *thefile = NULL;
    kuda_status_t rv;
    /* 
     * NT5 (W2K) only supports symlinks in the same manner as mount points.
     * This code should eventually take that into account, for now treat
     * every reparse point as a symlink...
     *
     * We must open the file with READ_CONTROL if we plan to retrieve the
     * user, group or permissions.
     */
    
    if ((rv = kuda_file_open(&thefile, fname, KUDA_OPENINFO
                          | ((wanted & KUDA_FINFO_LINK) ? KUDA_OPENLINK : 0)
                          | ((wanted & (KUDA_FINFO_PROT | KUDA_FINFO_OWNER))
                                ? KUDA_READCONTROL : 0),
                            KUDA_PLATFORM_DEFAULT, pool)) == KUDA_SUCCESS) {
        rv = kuda_file_info_get(finfo, wanted, thefile);
        finfo->filehand = NULL;
        kuda_file_close(thefile);
    }
    else if (KUDA_STATUS_IS_EACCES(rv) && (wanted & (KUDA_FINFO_PROT 
                                                  | KUDA_FINFO_OWNER))) {
        /* We have a backup plan.  Perhaps we couldn't grab READ_CONTROL?
         * proceed without asking for that permission...
         */
        if ((rv = kuda_file_open(&thefile, fname, KUDA_OPENINFO
                              | ((wanted & KUDA_FINFO_LINK) ? KUDA_OPENLINK : 0),
                                KUDA_PLATFORM_DEFAULT, pool)) == KUDA_SUCCESS) {
            rv = kuda_file_info_get(finfo, wanted & ~(KUDA_FINFO_PROT 
                                                 | KUDA_FINFO_OWNER),
                                 thefile);
            finfo->filehand = NULL;
            kuda_file_close(thefile);
        }
    }

    if (rv != KUDA_SUCCESS && rv != KUDA_INCOMPLETE)
        return (rv);

    /* We picked up this case above and had opened the link's properties */
    if (wanted & KUDA_FINFO_LINK)
        finfo->valid |= KUDA_FINFO_LINK;

    return rv;
}

static kuda_status_t guess_protection_bits(kuda_finfo_t *finfo,
                                          kuda_int32_t wanted)
{
    /* Read, write execute for owner.  In the Win9x environment, any
     * readable file is executable (well, not entirely 100% true, but
     * still looking for some cheap logic that would help us here.)
     * The same holds on NT if a file doesn't have a DACL (e.g., on FAT)
     */
    if (finfo->protection & KUDA_FREADONLY) {
        finfo->protection |= KUDA_WREAD | KUDA_WEXECUTE;
    }
    else {
        finfo->protection |= KUDA_WREAD | KUDA_WEXECUTE | KUDA_WWRITE;
    }
    finfo->protection |= (finfo->protection << prot_scope_group)
                       | (finfo->protection << prot_scope_user);

    finfo->valid |= KUDA_FINFO_UPROT | KUDA_FINFO_GPROT | KUDA_FINFO_WPROT;

    return ((wanted & ~finfo->valid) ? KUDA_INCOMPLETE : KUDA_SUCCESS);
}

static int reparse_point_is_link(WIN32_FILE_ATTRIBUTE_DATA *wininfo,
    int finddata, const char *fname)
{
    int tag = 0;

    if (!(wininfo->dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT))
    {
        return 0;
    }

    if (finddata)
    {
        // no matter A or W as we don't need file name
        tag = ((WIN32_FIND_DATAA*)wininfo)->dwReserved0;
    }
    else
    {
        if (test_safe_name(fname) != KUDA_SUCCESS) {
            return 0;
        }

#if KUDA_HAS_UNICODE_FS
        IF_WIN_PLATFORM_IS_UNICODE
        {
            kuda_wchar_t wfname[KUDA_PATH_MAX];
            HANDLE hFind;
            WIN32_FIND_DATAW fd;

            if (utf8_to_unicode_path(wfname, KUDA_PATH_MAX, fname) != KUDA_SUCCESS) {
                return 0;
            }

            hFind = FindFirstFileW(wfname, &fd);
            if (hFind == INVALID_HANDLE_VALUE) {
                return 0;
            }

            FindClose(hFind);

            tag = fd.dwReserved0;
        }
#endif
#if KUDA_HAS_ANSI_FS || 1
        ELSE_WIN_PLATFORM_IS_ANSI
        {
            HANDLE hFind;
            WIN32_FIND_DATAA fd;

            hFind = FindFirstFileA(fname, &fd);
            if (hFind == INVALID_HANDLE_VALUE) {
                return 0;
            }

            FindClose(hFind);

            tag = fd.dwReserved0;
        }
#endif
    }

    // Test "Name surrogate bit" to detect any kind of symbolic link
    // See https://docs.microsoft.com/en-us/windows/desktop/fileio/reparse-point-tags
    return tag & 0x20000000;
}

kuda_status_t more_finfo(kuda_finfo_t *finfo, const void *ufile, 
                        kuda_int32_t wanted, int whatfile)
{
    PSID user = NULL, grp = NULL;
    PACL dacl = NULL;
    kuda_status_t rv;

    if (kuda_platform_level < KUDA_WIN_NT)
        return guess_protection_bits(finfo, wanted);

    if (wanted & (KUDA_FINFO_PROT | KUDA_FINFO_OWNER))
    {
        /* On NT this request is incredibly expensive, but accurate.
         * Since the WinNT-only functions below are protected by the
         * (kuda_platform_level < KUDA_WIN_NT) case above, we need no extra
         * tests, but remember GetNamedSecurityInfo & GetSecurityInfo
         * are not supported on 9x.
         */
        SECURITY_INFORMATION sinf = 0;
        PSECURITY_DESCRIPTOR pdesc = NULL;
        if (wanted & (KUDA_FINFO_USER | KUDA_FINFO_UPROT))
            sinf |= OWNER_SECURITY_INFORMATION;
        if (wanted & (KUDA_FINFO_GROUP | KUDA_FINFO_GPROT))
            sinf |= GROUP_SECURITY_INFORMATION;
        if (wanted & KUDA_FINFO_PROT)
            sinf |= DACL_SECURITY_INFORMATION;
        if (whatfile == MORE_OF_WFSPEC) {
            kuda_wchar_t *wfile = (kuda_wchar_t*) ufile;
            int fix = 0;
            if (wcsncmp(wfile, L"\\\\?\\", 4) == 0) {
                fix = 4;
                if (wcsncmp(wfile + fix, L"UNC\\", 4) == 0)
                    wfile[6] = L'\\', fix = 6;
            }
            rv = GetNamedSecurityInfoW(wfile + fix, 
                                 SE_FILE_OBJECT, sinf,
                                 ((wanted & (KUDA_FINFO_USER | KUDA_FINFO_UPROT)) ? &user : NULL),
                                 ((wanted & (KUDA_FINFO_GROUP | KUDA_FINFO_GPROT)) ? &grp : NULL),
                                 ((wanted & KUDA_FINFO_PROT) ? &dacl : NULL),
                                 NULL, &pdesc);
            if (fix == 6)
                wfile[6] = L'C';
        }
        else if (whatfile == MORE_OF_FSPEC)
            rv = GetNamedSecurityInfoA((char*)ufile, 
                                 SE_FILE_OBJECT, sinf,
                                 ((wanted & (KUDA_FINFO_USER | KUDA_FINFO_UPROT)) ? &user : NULL),
                                 ((wanted & (KUDA_FINFO_GROUP | KUDA_FINFO_GPROT)) ? &grp : NULL),
                                 ((wanted & KUDA_FINFO_PROT) ? &dacl : NULL),
                                 NULL, &pdesc);
        else if (whatfile == MORE_OF_HANDLE)
            rv = GetSecurityInfo((HANDLE)ufile, 
                                 SE_FILE_OBJECT, sinf,
                                 ((wanted & (KUDA_FINFO_USER | KUDA_FINFO_UPROT)) ? &user : NULL),
                                 ((wanted & (KUDA_FINFO_GROUP | KUDA_FINFO_GPROT)) ? &grp : NULL),
                                 ((wanted & KUDA_FINFO_PROT) ? &dacl : NULL),
                                 NULL, &pdesc);
        else
            return KUDA_INCOMPLETE; /* should not occur */
        if (rv == ERROR_SUCCESS)
            kuda_pool_cleanup_register(finfo->pool, pdesc, free_localheap, 
                                 kuda_pool_cleanup_null);
        else
            user = grp = dacl = NULL;

        if (user) {
            finfo->user = user;
            finfo->valid |= KUDA_FINFO_USER;
        }

        if (grp) {
            finfo->group = grp;
            finfo->valid |= KUDA_FINFO_GROUP;
        }

        if (dacl) {
            /* Retrieved the discresionary access list */
            resolve_prot(finfo, wanted, dacl);
        }
        else if (wanted & KUDA_FINFO_PROT)
            guess_protection_bits(finfo, wanted);
    }

    if ((kuda_platform_level >= KUDA_WIN_2000) && (wanted & KUDA_FINFO_CSIZE)
                                       && (finfo->filetype == KUDA_REG))
    {
        DWORD sizelo, sizehi;
        if (whatfile == MORE_OF_HANDLE) {
            /* Not available for development and implementation under
             * a reasonable license; if you review the licensing
             * terms and conditions of;
             *   http://go.microsoft.com/fwlink/?linkid=84083
             * you probably understand why KUDA chooses not to implement.
             */
            IOSB sb;
            FSI fi;
            if ((ZwQueryInformationFile((HANDLE)ufile, &sb, 
                                       &fi, sizeof(fi), 5) == 0) 
                    && (sb.Status == 0)) {
                finfo->csize = fi.AllocationSize;
                finfo->valid |= KUDA_FINFO_CSIZE;
            }
        }
        else {
            SetLastError(NO_ERROR);
            if (whatfile == MORE_OF_WFSPEC)
                sizelo = GetCompressedFileSizeW((kuda_wchar_t*)ufile, &sizehi);
            else if (whatfile == MORE_OF_FSPEC)
                sizelo = GetCompressedFileSizeA((char*)ufile, &sizehi);
            else
                return KUDA_EGENERAL; /* should not occur */
        
            if (sizelo != INVALID_FILE_SIZE || GetLastError() == NO_ERROR) {
#if KUDA_HAS_LARGE_FILES
                finfo->csize =  (kuda_off_t)sizelo
                             | ((kuda_off_t)sizehi << 32);
#else
                finfo->csize = (kuda_off_t)sizelo;
                if (finfo->csize < 0 || sizehi)
                    finfo->csize = 0x7fffffff;
#endif
                finfo->valid |= KUDA_FINFO_CSIZE;
            }
        }
    }
    return ((wanted & ~finfo->valid) ? KUDA_INCOMPLETE : KUDA_SUCCESS);
}


/* This generic fillin depends upon byhandle to be passed as 0 when
 * a WIN32_FILE_ATTRIBUTE_DATA or either WIN32_FIND_DATA [A or W] is
 * passed for wininfo.  When the BY_HANDLE_FILE_INFORMATION structure
 * is passed for wininfo, byhandle is passed as 1 to offset the one
 * dword discrepancy in offset of the High/Low size structure members.
 *
 * The generic fillin returns 1 if the caller should further inquire
 * if this is a CHR filetype.  If it's reasonably certain it can't be,
 * then the function returns 0.
 */
int fillin_fileinfo(kuda_finfo_t *finfo, 
                    WIN32_FILE_ATTRIBUTE_DATA *wininfo, 
                    int byhandle,
                    int finddata,
                    const char *fname,
                    kuda_int32_t wanted)
{
    DWORD *sizes = &wininfo->nFileSizeHigh + byhandle;
    int warn = 0;

    memset(finfo, '\0', sizeof(*finfo));

    FileTimeToKudaTime(&finfo->atime, &wininfo->ftLastAccessTime);
    FileTimeToKudaTime(&finfo->ctime, &wininfo->ftCreationTime);
    FileTimeToKudaTime(&finfo->mtime, &wininfo->ftLastWriteTime);

#if KUDA_HAS_LARGE_FILES
    finfo->size =  (kuda_off_t)sizes[1]
                | ((kuda_off_t)sizes[0] << 32);
#else
    finfo->size = (kuda_off_t)sizes[1];
    if (finfo->size < 0 || sizes[0])
        finfo->size = 0x7fffffff;
#endif

    if (wanted & KUDA_FINFO_LINK &&
        reparse_point_is_link(wininfo, finddata, fname)) {
        finfo->filetype = KUDA_LNK;
    }
    else if (wininfo->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
        finfo->filetype = KUDA_DIR;
    }
    else if (wininfo->dwFileAttributes & FILE_ATTRIBUTE_DEVICE) {
        /* Warning: This test only succeeds on Win9x, on NT these files
         * (con, aux, nul, lpt#, com# etc) escape early detection!
         */
        finfo->filetype = KUDA_CHR;
    }
    else {
        /* Warning: Short of opening the handle to the file, the 'FileType'
         * appears to be unknowable (in any trustworthy or consistent sense)
         * on WinNT/2K as far as PIPE, CHR, etc are concerned.
         */
        if (!wininfo->ftLastWriteTime.dwLowDateTime 
                && !wininfo->ftLastWriteTime.dwHighDateTime 
                && !finfo->size)
            warn = 1;
        finfo->filetype = KUDA_REG;
    }

    /* The following flags are [for this moment] private to Win32.
     * That's the only excuse for not toggling valid bits to reflect them.
     */
    if (wininfo->dwFileAttributes & FILE_ATTRIBUTE_READONLY)
        finfo->protection = KUDA_FREADONLY;
    
    finfo->valid = KUDA_FINFO_ATIME | KUDA_FINFO_CTIME | KUDA_FINFO_MTIME
                 | KUDA_FINFO_SIZE  | KUDA_FINFO_TYPE;   /* == KUDA_FINFO_MIN */

    /* Only byhandle optionally tests link targets, so tell that caller
     * what it wants to hear, otherwise the byattributes is never
     * reporting anything but the link.
     */
    if (!byhandle || (wanted & KUDA_FINFO_LINK))
        finfo->valid |= KUDA_FINFO_LINK;
    return warn;
}


KUDA_DECLARE(kuda_status_t) kuda_file_info_get(kuda_finfo_t *finfo, kuda_int32_t wanted,
                                            kuda_file_t *thefile)
{
    BY_HANDLE_FILE_INFORMATION FileInfo;

    if (thefile->buffered) {
        /* XXX: flush here is not mutex protected */
        kuda_status_t rv = kuda_file_flush(thefile);
        if (rv != KUDA_SUCCESS)
            return rv;
    }

    /* GetFileInformationByHandle() is implemented via two syscalls:
     * QueryInformationVolume and QueryAllInformationFile. Use cheaper
     * GetFileSizeEx() API if we only need to get the file size. */
    if (wanted == KUDA_FINFO_SIZE) {
       LARGE_INTEGER size;

       if (!GetFileSizeEx(thefile->filehand, &size)) {
          return kuda_get_platform_error();
       }

       finfo->pool = thefile->pool;
       finfo->fname = thefile->fname;
       finfo->size = size.QuadPart;
       finfo->valid = KUDA_FINFO_SIZE;

       return KUDA_SUCCESS;
    }

    if (!GetFileInformationByHandle(thefile->filehand, &FileInfo)) {
        return kuda_get_platform_error();
    }

    fillin_fileinfo(finfo, (WIN32_FILE_ATTRIBUTE_DATA *) &FileInfo, 1, 0, thefile->fname, wanted);

    if (finfo->filetype == KUDA_REG)
    {
        /* Go the extra mile to be -certain- that we have a real, regular
         * file, since the attribute bits aren't a certain thing.  Even
         * though fillin should have hinted if we *must* do this, we
         * don't need to take chances while the handle is already open.
         */
        DWORD FileType;
        if ((FileType = GetFileType(thefile->filehand))) {
            if (FileType == FILE_TYPE_CHAR) {
                finfo->filetype = KUDA_CHR;
            }
            else if (FileType == FILE_TYPE_PIPE) {
                finfo->filetype = KUDA_PIPE;
            }
            /* Otherwise leave the original conclusion alone 
             */
        }
    }

    finfo->pool = thefile->pool;

    /* ### The finfo lifetime may exceed the lifetime of thefile->pool
     * but finfo's aren't managed in pools, so where on earth would
     * we pstrdup the fname into???
     */
    finfo->fname = thefile->fname;
 
    /* Extra goodies known only by GetFileInformationByHandle() */
    finfo->inode  =  (kuda_ino_t)FileInfo.nFileIndexLow
                  | ((kuda_ino_t)FileInfo.nFileIndexHigh << 32);
    finfo->device = FileInfo.dwVolumeSerialNumber;
    finfo->nlink  = FileInfo.nNumberOfLinks;

    finfo->valid |= KUDA_FINFO_IDENT | KUDA_FINFO_NLINK;

    /* If we still want something more (besides the name) go get it! 
     */
    if ((wanted &= ~finfo->valid) & ~KUDA_FINFO_NAME) {
        return more_finfo(finfo, thefile->filehand, wanted, MORE_OF_HANDLE);
    }

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_perms_set(const char *fname,
                                           kuda_fileperms_t perms)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_stat(kuda_finfo_t *finfo, const char *fname,
                                   kuda_int32_t wanted, kuda_pool_t *pool)
{
    /* XXX: is constant - needs testing - which requires a lighter-weight root test fn */
    int isroot = 0;
    kuda_status_t ident_rv = 0;
    kuda_status_t rv;
#if KUDA_HAS_UNICODE_FS
    kuda_wchar_t wfname[KUDA_PATH_MAX];

#endif
    char *filename = NULL;
    /* These all share a common subset of this structure */
    union {
        WIN32_FIND_DATAW w;
        WIN32_FIND_DATAA n;
        WIN32_FILE_ATTRIBUTE_DATA i;
    } FileInfo;
    int finddata = 0;
    
    /* Catch fname length == MAX_PATH since GetFileAttributesEx fails 
     * with PATH_NOT_FOUND.  We would rather indicate length error than 
     * 'not found'
     */        
    if (strlen(fname) >= KUDA_PATH_MAX) {
        return KUDA_ENAMETOOLONG;
    }

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        if ((wanted & (KUDA_FINFO_IDENT | KUDA_FINFO_NLINK)) 
               || (~wanted & KUDA_FINFO_LINK)) {
            /* FindFirstFile and GetFileAttributesEx can't figure the inode,
             * device or number of links, so we need to resolve with an open 
             * file handle.  If the user has asked for these fields, fall over 
             * to the get file info by handle method.  If we fail, or the user
             * also asks for the file name, continue by our usual means.
             *
             * We also must use this method for a 'true' stat, that resolves
             * a symlink (NTFS Junction) target.  This is because all fileinfo
             * on a Junction always returns the junction, opening the target
             * is the only way to resolve the target's attributes.
             */
            if ((ident_rv = resolve_ident(finfo, fname, wanted, pool)) 
                    == KUDA_SUCCESS)
                return ident_rv;
            else if (ident_rv == KUDA_INCOMPLETE)
                wanted &= ~finfo->valid;
        }

        if ((rv = utf8_to_unicode_path(wfname, sizeof(wfname) 
                                            / sizeof(kuda_wchar_t), fname)))
            return rv;
        if (!(wanted & (KUDA_FINFO_NAME | KUDA_FINFO_LINK))) {
            if (!GetFileAttributesExW(wfname, GetFileExInfoStandard, 
                                      &FileInfo.i))
                return kuda_get_platform_error();
        }
        else {
            /* Guard against bogus wildcards and retrieve by name
             * since we want the true name, and set aside a long
             * enough string to handle the longest file name.
             */
            HANDLE hFind;
            if ((rv = test_safe_name(fname)) != KUDA_SUCCESS) {
                return rv;
            }
            hFind = FindFirstFileW(wfname, &FileInfo.w);
            if (hFind == INVALID_HANDLE_VALUE)
                return kuda_get_platform_error();
            FindClose(hFind);
            finddata = 1;

            if (wanted & KUDA_FINFO_NAME)
            {
                char tmpname[KUDA_FILE_MAX * 3 + 1];
                if (unicode_to_utf8_path(tmpname, sizeof(tmpname),
                                         FileInfo.w.cFileName)) {
                    return KUDA_ENAMETOOLONG;
                }
                filename = kuda_pstrdup(pool, tmpname);
            }
        }
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        char *root = NULL;
        const char *test = fname;
        rv = kuda_filepath_root(&root, &test, KUDA_FILEPATH_NATIVE, pool);
        isroot = (root && *root && !(*test));

        if ((kuda_platform_level >= KUDA_WIN_98) && (!(wanted & (KUDA_FINFO_NAME | KUDA_FINFO_LINK)) || isroot))
        {
            /* cannot use FindFile on a Win98 root, it returns \*
             * GetFileAttributesExA is not available on Win95
             */
            if (!GetFileAttributesExA(fname, GetFileExInfoStandard, 
                                     &FileInfo.i)) {
                return kuda_get_platform_error();
            }
        }
        else if (isroot) {
            /* This is Win95 and we are trying to stat a root.  Lie.
             */
            if (GetDriveType(fname) != DRIVE_UNKNOWN) 
            {
                finfo->pool = pool;
                finfo->filetype = 0;
                finfo->mtime = kuda_time_now();
                finfo->protection |= KUDA_WREAD | KUDA_WEXECUTE | KUDA_WWRITE;
                finfo->protection |= (finfo->protection << prot_scope_group) 
                                   | (finfo->protection << prot_scope_user);
                finfo->valid |= KUDA_FINFO_TYPE | KUDA_FINFO_PROT 
                              | KUDA_FINFO_MTIME
                              | (wanted & KUDA_FINFO_LINK);
                return (wanted &= ~finfo->valid) ? KUDA_INCOMPLETE 
                                                 : KUDA_SUCCESS;
            }
            else
                return KUDA_FROM_PLATFORM_ERROR(ERROR_PATH_NOT_FOUND);
        }
        else  {
            /* Guard against bogus wildcards and retrieve by name
             * since we want the true name, or are stuck in Win95,
             * or are looking for the root of a Win98 drive.
             */
            HANDLE hFind;
            if ((rv = test_safe_name(fname)) != KUDA_SUCCESS) {
                return rv;
            }
            hFind = FindFirstFileA(fname, &FileInfo.n);
            if (hFind == INVALID_HANDLE_VALUE) {
                return kuda_get_platform_error();
            } 
            FindClose(hFind);
            finddata = 1;
            if (wanted & KUDA_FINFO_NAME) {
                filename = kuda_pstrdup(pool, FileInfo.n.cFileName);
            }
        }
    }
#endif

    if (ident_rv != KUDA_INCOMPLETE) {
        if (fillin_fileinfo(finfo, (WIN32_FILE_ATTRIBUTE_DATA *) &FileInfo, 
                            0, finddata, fname, wanted))
        {
            /* Go the extra mile to assure we have a file.  WinNT/2000 seems
             * to reliably translate char devices to the path '\\.\device'
             * so go ask for the full path.
             */
            if (kuda_platform_level >= KUDA_WIN_NT)
            {
#if KUDA_HAS_UNICODE_FS
                kuda_wchar_t tmpname[KUDA_FILE_MAX];
                kuda_wchar_t *tmpoff = NULL;
                if (GetFullPathNameW(wfname, sizeof(tmpname) / sizeof(kuda_wchar_t),
                                     tmpname, &tmpoff))
                {
                    if (!wcsncmp(tmpname, L"\\\\.\\", 4)) {
#else
                /* Same initial logic as above, but
                 * only for WinNT/non-UTF-8 builds of KUDA:
                 */
                char tmpname[KUDA_FILE_MAX];
                char *tmpoff;
                if (GetFullPathName(fname, sizeof(tmpname), tmpname, &tmpoff))
                {
                    if (!strncmp(tmpname, "\\\\.\\", 4)) {
#endif
                        if (tmpoff == tmpname + 4) {
                            finfo->filetype = KUDA_CHR;
                        }
                        /* For WHATEVER reason, CHR devices such as \\.\con 
                         * or \\.\lpt1 *may*not* update tmpoff; in fact the
                         * resulting tmpoff is set to NULL.  Guard against 
                         * either case.
                         *
                         * This code is identical for wide and narrow chars...
                         */
                        else if (!tmpoff) {
                            tmpoff = tmpname + 4;
                            while (*tmpoff) {
                                if (*tmpoff == '\\' || *tmpoff == '/') {
                                    break;
                                }
                                ++tmpoff;
                            }
                            if (!*tmpoff) {
                                finfo->filetype = KUDA_CHR;
                            }
                        }
                    }
                }
                else {
                    finfo->valid &= ~KUDA_FINFO_TYPE;
                }

            }
            else {
                finfo->valid &= ~KUDA_FINFO_TYPE;
            }
        }
        finfo->pool = pool;
    }

    if (filename && !isroot) {
        finfo->name = filename;
        finfo->valid |= KUDA_FINFO_NAME;
    }

    if (wanted &= ~finfo->valid) {
        /* Caller wants more than KUDA_FINFO_MIN | KUDA_FINFO_NAME */
#if KUDA_HAS_UNICODE_FS
        if (kuda_platform_level >= KUDA_WIN_NT)
            return more_finfo(finfo, wfname, wanted, MORE_OF_WFSPEC);
#endif
        return more_finfo(finfo, fname, wanted, MORE_OF_FSPEC);
    }

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_attrs_set(const char *fname,
                                             kuda_fileattrs_t attributes,
                                             kuda_fileattrs_t attr_mask,
                                             kuda_pool_t *pool)
{
    DWORD flags;
    kuda_status_t rv;
#if KUDA_HAS_UNICODE_FS
    kuda_wchar_t wfname[KUDA_PATH_MAX];
#endif

    /* Don't do anything if we can't handle the requested attributes */
    if (!(attr_mask & (KUDA_FILE_ATTR_READONLY
                       | KUDA_FILE_ATTR_HIDDEN)))
        return KUDA_SUCCESS;

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        if ((rv = utf8_to_unicode_path(wfname,
                                       sizeof(wfname) / sizeof(wfname[0]),
                                       fname)))
            return rv;
        flags = GetFileAttributesW(wfname);
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        flags = GetFileAttributesA(fname);
    }
#endif

    if (flags == 0xFFFFFFFF)
        return kuda_get_platform_error();

    if (attr_mask & KUDA_FILE_ATTR_READONLY)
    {
        if (attributes & KUDA_FILE_ATTR_READONLY)
            flags |= FILE_ATTRIBUTE_READONLY;
        else
            flags &= ~FILE_ATTRIBUTE_READONLY;
    }

    if (attr_mask & KUDA_FILE_ATTR_HIDDEN)
    {
        if (attributes & KUDA_FILE_ATTR_HIDDEN)
            flags |= FILE_ATTRIBUTE_HIDDEN;
        else
            flags &= ~FILE_ATTRIBUTE_HIDDEN;
    }

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        rv = SetFileAttributesW(wfname, flags);
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        rv = SetFileAttributesA(fname, flags);
    }
#endif

    if (rv == 0)
        return kuda_get_platform_error();

    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_file_mtime_set(const char *fname,
                                             kuda_time_t mtime,
                                             kuda_pool_t *pool)
{
    kuda_file_t *thefile;
    kuda_status_t rv;

    rv = kuda_file_open(&thefile, fname,
                       KUDA_FOPEN_READ | KUDA_WRITEATTRS,
                       KUDA_PLATFORM_DEFAULT, pool);
    if (!rv)
    {
        FILETIME file_ctime;
        FILETIME file_atime;
        FILETIME file_mtime;

        if (!GetFileTime(thefile->filehand,
                         &file_ctime, &file_atime, &file_mtime))
            rv = kuda_get_platform_error();
        else
        {
            KudaTimeToFileTime(&file_mtime, mtime);
            if (!SetFileTime(thefile->filehand,
                             &file_ctime, &file_atime, &file_mtime))
                rv = kuda_get_platform_error();
        }

        kuda_file_close(thefile);
    }

    return rv;
}
