/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"

KUDA_DECLARE(kuda_status_t) kuda_file_lock(kuda_file_t *thefile, int type)
{
#ifdef _WIN32_WCE
    /* The File locking is unsuported on WCE */
    return KUDA_ENOTIMPL;
#else
    const DWORD len = 0xffffffff;
    DWORD flags; 

    flags = ((type & KUDA_FLOCK_NONBLOCK) ? LOCKFILE_FAIL_IMMEDIATELY : 0)
          + (((type & KUDA_FLOCK_TYPEMASK) == KUDA_FLOCK_SHARED) 
                                       ? 0 : LOCKFILE_EXCLUSIVE_LOCK);
    if (kuda_platform_level >= KUDA_WIN_NT) {
        /* Syntax is correct, len is passed for LengthLow and LengthHigh*/
        OVERLAPPED offset;
        memset (&offset, 0, sizeof(offset));
        if (!LockFileEx(thefile->filehand, flags, 0, len, len, &offset))
            return kuda_get_platform_error();
    }
    else {
        /* On Win9x, LockFile() never blocks.  Hack in a crufty poll.
         *
         * Note that this hack exposes threads to being unserviced forever,
         * in the situation that the given lock has low availability.
         * When implemented in the kernel, LockFile will typically use
         * FIFO or round robin distribution to ensure all threads get 
         * one crack at the lock; but in this case we can't emulate that.
         *
         * However Win9x are barely maintainable anyways, if the user does
         * choose to build to them, this is the best we can do.
         */
        while (!LockFile(thefile->filehand, 0, 0, len, 0)) {
            DWORD err = GetLastError();
            if ((err == ERROR_LOCK_VIOLATION) && !(type & KUDA_FLOCK_NONBLOCK))
            {
                Sleep(500); /* pause for a half second */
                continue;   /* ... and then poll again */
            }
            return KUDA_FROM_PLATFORM_ERROR(err);
        }
    }

    return KUDA_SUCCESS;
#endif /* !defined(_WIN32_WCE) */
}

KUDA_DECLARE(kuda_status_t) kuda_file_unlock(kuda_file_t *thefile)
{
#ifdef _WIN32_WCE
    return KUDA_ENOTIMPL;
#else
    DWORD len = 0xffffffff;

    if (kuda_platform_level >= KUDA_WIN_NT) {
        /* Syntax is correct, len is passed for LengthLow and LengthHigh*/
        OVERLAPPED offset;
        memset (&offset, 0, sizeof(offset));
        if (!UnlockFileEx(thefile->filehand, 0, len, len, &offset))
            return kuda_get_platform_error();
    }
    else {
        if (!UnlockFile(thefile->filehand, 0, 0, len, 0))
            return kuda_get_platform_error();
    }

    return KUDA_SUCCESS;
#endif /* !defined(_WIN32_WCE) */
}
