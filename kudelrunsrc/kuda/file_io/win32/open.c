/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_private.h"
#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_thread_mutex.h"
#if KUDA_HAVE_ERRNO_H
#include <errno.h>
#endif
#include <winbase.h>
#include <string.h>
#ifdef HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif
#include "kuda_arch_misc.h"
#include "kuda_arch_inherit.h"
#include <io.h>
#include <winioctl.h>

#if KUDA_HAS_UNICODE_FS
kuda_status_t utf8_to_unicode_path(kuda_wchar_t* retstr, kuda_size_t retlen, 
                                  const char* srcstr)
{
    /* TODO: The computations could preconvert the string to determine
     * the true size of the retstr, but that's a memory over speed
     * tradeoff that isn't appropriate this early in development.
     *
     * Allocate the maximum string length based on leading 4 
     * characters of \\?\ (allowing nearly unlimited path lengths) 
     * plus the trailing null, then transform /'s into \\'s since
     * the \\?\ form doesn't allow '/' path seperators.
     *
     * Note that the \\?\ form only works for local drive paths, and
     * \\?\UNC\ is needed UNC paths.
     */
    kuda_size_t srcremains = strlen(srcstr) + 1;
    kuda_wchar_t *t = retstr;
    kuda_status_t rv;

    /* This is correct, we don't twist the filename if it is will
     * definitely be shorter than 248 characters.  It merits some 
     * performance testing to see if this has any effect, but there
     * seem to be applications that get confused by the resulting
     * Unicode \\?\ style file names, especially if they use argv[0]
     * or call the Win32 API functions such as GetcAPIName, etc.
     * Not every application is prepared to handle such names.
     * 
     * Note also this is shorter than MAX_PATH, as directory paths 
     * are actually limited to 248 characters. 
     *
     * Note that a utf-8 name can never result in more wide chars
     * than the original number of utf-8 narrow chars.
     */
    if (srcremains > 248) {
        if (srcstr[1] == ':' && (srcstr[2] == '/' || srcstr[2] == '\\')) {
            wcscpy (retstr, L"\\\\?\\");
            retlen -= 4;
            t += 4;
        }
        else if ((srcstr[0] == '/' || srcstr[0] == '\\')
              && (srcstr[1] == '/' || srcstr[1] == '\\')
              && (srcstr[2] != '?')) {
            /* Skip the slashes */
            srcstr += 2;
            srcremains -= 2;
            wcscpy (retstr, L"\\\\?\\UNC\\");
            retlen -= 8;
            t += 8;
        }
    }

    if ((rv = kuda_conv_utf8_to_ucs2(srcstr, &srcremains, t, &retlen))) {
        return (rv == KUDA_INCOMPLETE) ? KUDA_EINVAL : rv;
    }
    if (srcremains) {
        return KUDA_ENAMETOOLONG;
    }
    for (; *t; ++t)
        if (*t == L'/')
            *t = L'\\';
    return KUDA_SUCCESS;
}

kuda_status_t unicode_to_utf8_path(char* retstr, kuda_size_t retlen,
                                  const kuda_wchar_t* srcstr)
{
    /* Skip the leading 4 characters if the path begins \\?\, or substitute
     * // for the \\?\UNC\ path prefix, allocating the maximum string
     * length based on the remaining string, plus the trailing null.
     * then transform \\'s back into /'s since the \\?\ form never
     * allows '/' path seperators, and KUDA always uses '/'s.
     */
    kuda_size_t srcremains = wcslen(srcstr) + 1;
    kuda_status_t rv;
    char *t = retstr;
    if (srcstr[0] == L'\\' && srcstr[1] == L'\\' && 
        srcstr[2] == L'?'  && srcstr[3] == L'\\') {
        if (srcstr[4] == L'U' && srcstr[5] == L'N' && 
            srcstr[6] == L'C' && srcstr[7] == L'\\') {
            srcremains -= 8;
            srcstr += 8;
            retstr[0] = '\\';
            retstr[1] = '\\';
            retlen -= 2;
            t += 2;
        }
        else {
            srcremains -= 4;
            srcstr += 4;
        }
    }
        
    if ((rv = kuda_conv_ucs2_to_utf8(srcstr, &srcremains, t, &retlen))) {
        return rv;
    }
    if (srcremains) {
        return KUDA_ENAMETOOLONG;
    }
    return KUDA_SUCCESS;
}
#endif

void *res_name_from_filename(const char *file, int global, kuda_pool_t *pool)
{
#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        kuda_wchar_t *wpre, *wfile, *ch;
        kuda_size_t n = strlen(file) + 1;
        kuda_size_t r, d;

        if (kuda_platform_level >= KUDA_WIN_2000) {
            if (global)
                wpre = L"Global\\";
            else
                wpre = L"Local\\";
        }
        else
            wpre = L"";
        r = wcslen(wpre);

        if (n > 256 - r) {
            file += n - 256 - r;
            n = 256;
            /* skip utf8 continuation bytes */
            while ((*file & 0xC0) == 0x80) {
                ++file;
                --n;
            }
        }
        wfile = kuda_palloc(pool, (r + n) * sizeof(kuda_wchar_t));
        wcscpy(wfile, wpre);
        d = n;
        if (kuda_conv_utf8_to_ucs2(file, &n, wfile + r, &d)) {
            return NULL;
        }
        for (ch = wfile + r; *ch; ++ch) {
            if (*ch == ':' || *ch == '/' || *ch == '\\')
                *ch = '_';
        }
        return wfile;
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        char *nfile, *ch;
        kuda_size_t n = strlen(file) + 1;

#if !KUDA_HAS_UNICODE_FS
        kuda_size_t r, d;
        char *pre;

        if (kuda_platform_level >= KUDA_WIN_2000) {
            if (global)
                pre = "Global\\";
            else
                pre = "Local\\";
        }
        else
            pre = "";
        r = strlen(pre);

        if (n > 256 - r) {
            file += n - 256 - r;
            n = 256;
        }
        nfile = kuda_palloc(pool, (r + n) * sizeof(kuda_wchar_t));
        memcpy(nfile, pre, r);
        memcpy(nfile + r, file, n);
#else
        const kuda_size_t r = 0;
        if (n > 256) {
            file += n - 256;
            n = 256;
        }
        nfile = kuda_pmemdup(pool, file, n);
#endif
        for (ch = nfile + r; *ch; ++ch) {
            if (*ch == ':' || *ch == '/' || *ch == '\\')
                *ch = '_';
        }
        return nfile;
    }
#endif
}

#if KUDA_HAS_UNICODE_FS
static kuda_status_t make_sparse_file(kuda_file_t *file)
{
    BY_HANDLE_FILE_INFORMATION info;
    kuda_status_t rv;
    DWORD bytesread = 0;
    DWORD res;

    /* test */

    if (GetFileInformationByHandle(file->filehand, &info)
            && (info.dwFileAttributes & FILE_ATTRIBUTE_SPARSE_FILE))
        return KUDA_SUCCESS;

    if (file->pOverlapped) {
        file->pOverlapped->Offset     = 0;
        file->pOverlapped->OffsetHigh = 0;
    }

    if (DeviceIoControl(file->filehand, FSCTL_SET_SPARSE, NULL, 0, NULL, 0,
                        &bytesread, file->pOverlapped)) {
        rv = KUDA_SUCCESS;
    }
    else 
    {
        rv = kuda_get_platform_error();

        if (rv == KUDA_FROM_PLATFORM_ERROR(ERROR_IO_PENDING))
        {
            do {
                res = WaitForSingleObject(file->pOverlapped->hEvent, 
                                          (file->timeout > 0)
                                            ? (DWORD)(file->timeout/1000)
                                            : ((file->timeout == -1) 
                                                 ? INFINITE : 0));
            } while (res == WAIT_ABANDONED);

            if (res != WAIT_OBJECT_0) {
                CancelIo(file->filehand);
            }

            if (GetOverlappedResult(file->filehand, file->pOverlapped, 
                                    &bytesread, TRUE))
                rv = KUDA_SUCCESS;
            else
                rv = kuda_get_platform_error();
        }
    }
    return rv;
}
#endif

kuda_status_t file_cleanup(void *thefile)
{
    kuda_file_t *file = thefile;
    kuda_status_t flush_rv = KUDA_SUCCESS;

    if (file->filehand != INVALID_HANDLE_VALUE) {

        if (file->buffered) {
            /* XXX: flush here is not mutex protected */
            flush_rv = kuda_file_flush((kuda_file_t *)thefile);
        }

        /* In order to avoid later segfaults with handle 'reuse',
         * we must protect against the case that a dup2'ed handle
         * is being closed, and invalidate the corresponding StdHandle 
         * We also tell msvcrt when stdhandles are closed.
         */
        if (file->flags & KUDA_STD_FLAGS)
        {
            if ((file->flags & KUDA_STD_FLAGS) == KUDA_STDERR_FLAG) {
                _close(2);
                SetStdHandle(STD_ERROR_HANDLE, INVALID_HANDLE_VALUE);
            }
            else if ((file->flags & KUDA_STD_FLAGS) == KUDA_STDOUT_FLAG) {
                _close(1);
                SetStdHandle(STD_OUTPUT_HANDLE, INVALID_HANDLE_VALUE);
            }
            else if ((file->flags & KUDA_STD_FLAGS) == KUDA_STDIN_FLAG) {
                _close(0);
                SetStdHandle(STD_INPUT_HANDLE, INVALID_HANDLE_VALUE);
            }
        }
        else
            CloseHandle(file->filehand);

        file->filehand = INVALID_HANDLE_VALUE;
    }
    if (file->pOverlapped && file->pOverlapped->hEvent) {
        CloseHandle(file->pOverlapped->hEvent);
        file->pOverlapped = NULL;
    }
    return flush_rv;
}

KUDA_DECLARE(kuda_status_t) kuda_file_open(kuda_file_t **new, const char *fname,
                                   kuda_int32_t flag, kuda_fileperms_t perm,
                                   kuda_pool_t *pool)
{
    HANDLE handle = INVALID_HANDLE_VALUE;
    DWORD oflags = 0;
    DWORD createflags = 0;
    DWORD attributes = 0;
    DWORD sharemode = FILE_SHARE_READ | FILE_SHARE_WRITE;
    kuda_status_t rv;

    if (flag & KUDA_FOPEN_NONBLOCK) {
        return KUDA_ENOTIMPL;
    }
    if (flag & KUDA_FOPEN_READ) {
        oflags |= GENERIC_READ;
    }
    if (flag & KUDA_FOPEN_WRITE) {
        oflags |= GENERIC_WRITE;
    }
    if (flag & KUDA_WRITEATTRS) {
        oflags |= FILE_WRITE_ATTRIBUTES;
    }

    if (kuda_platform_level >= KUDA_WIN_NT) 
        sharemode |= FILE_SHARE_DELETE;

    if (flag & KUDA_FOPEN_CREATE) {
        if (flag & KUDA_FOPEN_EXCL) {
            /* only create new if file does not already exist */
            createflags = CREATE_NEW;
        } else if (flag & KUDA_FOPEN_TRUNCATE) {
            /* truncate existing file or create new */
            createflags = CREATE_ALWAYS;
        } else {
            /* open existing but create if necessary */
            createflags = OPEN_ALWAYS;
        }
    } else if (flag & KUDA_FOPEN_TRUNCATE) {
        /* only truncate if file already exists */
        createflags = TRUNCATE_EXISTING;
    } else {
        /* only open if file already exists */
        createflags = OPEN_EXISTING;
    }

    if ((flag & KUDA_FOPEN_EXCL) && !(flag & KUDA_FOPEN_CREATE)) {
        return KUDA_EACCES;
    }   
    
    if (flag & KUDA_FOPEN_DELONCLOSE) {
        attributes |= FILE_FLAG_DELETE_ON_CLOSE;
    }

    if (flag & KUDA_OPENLINK) {
       attributes |= FILE_FLAG_OPEN_REPARSE_POINT;
    }

    /* Without READ or WRITE, we fail unless kuda called kuda_file_open
     * internally with the private KUDA_OPENINFO flag.
     *
     * With the KUDA_OPENINFO flag on NT, use the option flag
     * FILE_FLAG_BACKUP_SEMANTICS to allow us to open directories.
     * See the static resolve_ident() fn in file_io/win32/filestat.c
     */
    if (!(flag & (KUDA_FOPEN_READ | KUDA_FOPEN_WRITE))) {
        if (flag & KUDA_OPENINFO) {
            if (kuda_platform_level >= KUDA_WIN_NT) {
                attributes |= FILE_FLAG_BACKUP_SEMANTICS;
            }
        }
        else {
            return KUDA_EACCES;
        }
        if (flag & KUDA_READCONTROL)
            oflags |= READ_CONTROL;
    }

    if (flag & KUDA_FOPEN_XTHREAD) {
        /* This win32 specific feature is required 
         * to allow multiple threads to work with the file.
         */
        attributes |= FILE_FLAG_OVERLAPPED;
    }

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        kuda_wchar_t wfname[KUDA_PATH_MAX];

        if (flag & KUDA_FOPEN_SENDFILE_ENABLED) {
            /* This feature is required to enable sendfile operations
             * against the file on Win32. Also implies KUDA_FOPEN_XTHREAD.
             */
            flag |= KUDA_FOPEN_XTHREAD;
            attributes |= FILE_FLAG_SEQUENTIAL_SCAN | FILE_FLAG_OVERLAPPED;
        }

        if ((rv = utf8_to_unicode_path(wfname, sizeof(wfname) 
                                             / sizeof(kuda_wchar_t), fname)))
            return rv;
        handle = CreateFileW(wfname, oflags, sharemode,
                             NULL, createflags, attributes, 0);
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI {
        handle = CreateFileA(fname, oflags, sharemode,
                             NULL, createflags, attributes, 0);
        /* This feature is not supported on this platform. */
        flag &= ~KUDA_FOPEN_SENDFILE_ENABLED;
    }
#endif
    if (handle == INVALID_HANDLE_VALUE) {
        return kuda_get_platform_error();
    }

    (*new) = (kuda_file_t *)kuda_pcalloc(pool, sizeof(kuda_file_t));
    (*new)->pool = pool;
    (*new)->filehand = handle;
    (*new)->fname = kuda_pstrdup(pool, fname);
    (*new)->flags = flag;
    (*new)->timeout = -1;
    (*new)->ungetchar = -1;

    if (flag & KUDA_FOPEN_APPEND) {
        (*new)->append = 1;
        SetFilePointer((*new)->filehand, 0, NULL, FILE_END);
    }
    if (flag & KUDA_FOPEN_BUFFERED) {
        (*new)->buffered = 1;
        (*new)->buffer = kuda_palloc(pool, KUDA_FILE_DEFAULT_BUFSIZE);
        (*new)->bufsize = KUDA_FILE_DEFAULT_BUFSIZE;
    }
    /* Need the mutex to handled buffered and O_APPEND style file i/o */
    if ((*new)->buffered || (*new)->append) {
        rv = kuda_thread_mutex_create(&(*new)->mutex, 
                                     KUDA_THREAD_MUTEX_DEFAULT, pool);
        if (rv) {
            if (file_cleanup(*new) == KUDA_SUCCESS) {
                kuda_pool_cleanup_kill(pool, *new, file_cleanup);
            }
            return rv;
        }
    }

#if KUDA_HAS_UNICODE_FS
    if ((kuda_platform_level >= KUDA_WIN_2000) && ((*new)->flags & KUDA_FOPEN_SPARSE)) {
        if ((rv = make_sparse_file(*new)) != KUDA_SUCCESS)
            /* The great mystery; do we close the file and return an error?
             * Do we add a new KUDA_INCOMPLETE style error saying opened, but
             * NOTSPARSE?  For now let's simply mark the file as not-sparse.
             */
            (*new)->flags &= ~KUDA_FOPEN_SPARSE;
    }
    else
#endif
        /* This feature is not supported on this platform. */
        (*new)->flags &= ~KUDA_FOPEN_SPARSE;

#if KUDA_FILES_AS_SOCKETS
    /* Create a pollset with room for one descriptor. */
    /* ### check return codes */
    (void) kuda_pollset_create(&(*new)->pollset, 1, pool, 0);
#endif
    if (!(flag & KUDA_FOPEN_NOCLEANUP)) {
        kuda_pool_cleanup_register((*new)->pool, (void *)(*new), file_cleanup,
                                  kuda_pool_cleanup_null);
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_close(kuda_file_t *file)
{
    kuda_status_t stat;
    if ((stat = file_cleanup(file)) == KUDA_SUCCESS) {
        kuda_pool_cleanup_kill(file->pool, file, file_cleanup);

        if (file->mutex) {
            kuda_thread_mutex_destroy(file->mutex);
        }

        return KUDA_SUCCESS;
    }
    return stat;
}

KUDA_DECLARE(kuda_status_t) kuda_file_remove(const char *path, kuda_pool_t *pool)
{
#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        kuda_wchar_t wpath[KUDA_PATH_MAX];
        kuda_status_t rv;
        if ((rv = utf8_to_unicode_path(wpath, sizeof(wpath) 
                                            / sizeof(kuda_wchar_t), path))) {
            return rv;
        }
        if (DeleteFileW(wpath))
            return KUDA_SUCCESS;
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
        if (DeleteFile(path))
            return KUDA_SUCCESS;
#endif
    return kuda_get_platform_error();
}

KUDA_DECLARE(kuda_status_t) kuda_file_rename(const char *frompath,
                                          const char *topath,
                                          kuda_pool_t *pool)
{
    IF_WIN_PLATFORM_IS_UNICODE
    {
#if KUDA_HAS_UNICODE_FS
        kuda_wchar_t wfrompath[KUDA_PATH_MAX], wtopath[KUDA_PATH_MAX];
        kuda_status_t rv;
        if ((rv = utf8_to_unicode_path(wfrompath,
                                       sizeof(wfrompath) / sizeof(kuda_wchar_t),
                                       frompath))) {
            return rv;
        }
        if ((rv = utf8_to_unicode_path(wtopath,
                                       sizeof(wtopath) / sizeof(kuda_wchar_t),
                                       topath))) {
            return rv;
        }
#ifndef _WIN32_WCE
        if (MoveFileExW(wfrompath, wtopath, MOVEFILE_REPLACE_EXISTING |
                                            MOVEFILE_COPY_ALLOWED))
#else
        if (MoveFileW(wfrompath, wtopath))
#endif
            return KUDA_SUCCESS;
#else
        if (MoveFileEx(frompath, topath, MOVEFILE_REPLACE_EXISTING |
                                         MOVEFILE_COPY_ALLOWED))
            return KUDA_SUCCESS;
#endif
    }
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        /* Windows 95 and 98 do not support MoveFileEx, so we'll use
         * the old MoveFile function.  However, MoveFile requires that
         * the new file not already exist...so we have to delete that
         * file if it does.  Perhaps we should back up the to-be-deleted
         * file in case something happens?
         */
        HANDLE handle = INVALID_HANDLE_VALUE;

        if ((handle = CreateFile(topath, GENERIC_WRITE, 0, 0,  
            OPEN_EXISTING, 0, 0 )) != INVALID_HANDLE_VALUE )
        {
            CloseHandle(handle);
            if (!DeleteFile(topath))
                return kuda_get_platform_error();
        }
        if (MoveFile(frompath, topath))
            return KUDA_SUCCESS;
    }        
#endif
    return kuda_get_platform_error();
}

KUDA_DECLARE(kuda_status_t) kuda_file_link(const char *from_path, 
                                           const char *to_path)
{
    kuda_status_t rv = KUDA_SUCCESS;

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        kuda_wchar_t wfrom_path[KUDA_PATH_MAX];
        kuda_wchar_t wto_path[KUDA_PATH_MAX];

        if ((rv = utf8_to_unicode_path(wfrom_path,
                                       sizeof(wfrom_path) / sizeof(kuda_wchar_t),
                                       from_path)))
            return rv;
        if ((rv = utf8_to_unicode_path(wto_path,
                                       sizeof(wto_path) / sizeof(kuda_wchar_t),
                                       to_path)))
            return rv;

        if (!CreateHardLinkW(wto_path, wfrom_path, NULL))
                return kuda_get_platform_error();
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI {
        if (!CreateHardLinkA(to_path, from_path, NULL))
                return kuda_get_platform_error();
    }
#endif
    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_file_get(kuda_platform_file_t *thefile,
                                          kuda_file_t *file)
{
    *thefile = file->filehand;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_file_put(kuda_file_t **file,
                                          kuda_platform_file_t *thefile,
                                          kuda_int32_t flags,
                                          kuda_pool_t *pool)
{
    (*file) = kuda_pcalloc(pool, sizeof(kuda_file_t));
    (*file)->pool = pool;
    (*file)->filehand = *thefile;
    (*file)->ungetchar = -1; /* no char avail */
    (*file)->timeout = -1;
    (*file)->flags = flags;

    if (flags & KUDA_FOPEN_APPEND) {
        (*file)->append = 1;
    }
    if (flags & KUDA_FOPEN_BUFFERED) {
        (*file)->buffered = 1;
        (*file)->buffer = kuda_palloc(pool, KUDA_FILE_DEFAULT_BUFSIZE);
        (*file)->bufsize = KUDA_FILE_DEFAULT_BUFSIZE;
    }

    if ((*file)->append || (*file)->buffered) {
        kuda_status_t rv;
        rv = kuda_thread_mutex_create(&(*file)->mutex, 
                                     KUDA_THREAD_MUTEX_DEFAULT, pool);
        if (rv) {
            return rv;
        }
    }

#if KUDA_FILES_AS_SOCKETS
    /* Create a pollset with room for one descriptor. */
    /* ### check return codes */
    (void) kuda_pollset_create(&(*file)->pollset, 1, pool, 0);
#endif
    /* Should we be testing if thefile is a handle to 
     * a PIPE and set up the mechanics appropriately?
     *
     *  (*file)->pipe;
     */
    return KUDA_SUCCESS;
}    

KUDA_DECLARE(kuda_status_t) kuda_file_eof(kuda_file_t *fptr)
{
    if (fptr->eof_hit == 1) {
        return KUDA_EOF;
    }
    return KUDA_SUCCESS;
}   

KUDA_DECLARE(kuda_status_t) kuda_file_open_flags_stderr(kuda_file_t **thefile, 
                                                     kuda_int32_t flags, 
                                                     kuda_pool_t *pool)
{
#ifdef _WIN32_WCE
    return KUDA_ENOTIMPL;
#else
    kuda_platform_file_t file_handle;

    kuda_set_platform_error(KUDA_SUCCESS);
    file_handle = GetStdHandle(STD_ERROR_HANDLE);
    if (!file_handle)
        file_handle = INVALID_HANDLE_VALUE;

    return kuda_platform_file_put(thefile, &file_handle,
                           flags | KUDA_FOPEN_WRITE | KUDA_STDERR_FLAG, pool);
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_file_open_flags_stdout(kuda_file_t **thefile, 
                                                     kuda_int32_t flags,
                                                     kuda_pool_t *pool)
{
#ifdef _WIN32_WCE
    return KUDA_ENOTIMPL;
#else
    kuda_platform_file_t file_handle;

    kuda_set_platform_error(KUDA_SUCCESS);
    file_handle = GetStdHandle(STD_OUTPUT_HANDLE);
    if (!file_handle)
        file_handle = INVALID_HANDLE_VALUE;

    return kuda_platform_file_put(thefile, &file_handle,
                           flags | KUDA_FOPEN_WRITE | KUDA_STDOUT_FLAG, pool);
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_file_open_flags_stdin(kuda_file_t **thefile, 
                                                    kuda_int32_t flags,
                                                    kuda_pool_t *pool)
{
#ifdef _WIN32_WCE
    return KUDA_ENOTIMPL;
#else
    kuda_platform_file_t file_handle;

    kuda_set_platform_error(KUDA_SUCCESS);
    file_handle = GetStdHandle(STD_INPUT_HANDLE);
    if (!file_handle)
        file_handle = INVALID_HANDLE_VALUE;

    return kuda_platform_file_put(thefile, &file_handle,
                           flags | KUDA_FOPEN_READ | KUDA_STDIN_FLAG, pool);
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_file_open_stderr(kuda_file_t **thefile, kuda_pool_t *pool)
{
    return kuda_file_open_flags_stderr(thefile, 0, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_file_open_stdout(kuda_file_t **thefile, kuda_pool_t *pool)
{
    return kuda_file_open_flags_stdout(thefile, 0, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_file_open_stdin(kuda_file_t **thefile, kuda_pool_t *pool)
{
    return kuda_file_open_flags_stdin(thefile, 0, pool);
}

KUDA_POOL_IMPLEMENT_ACCESSOR(file);

KUDA_IMPLEMENT_INHERIT_SET(file, flags, pool, file_cleanup)
 
KUDA_IMPLEMENT_INHERIT_UNSET(file, flags, pool, file_cleanup)
