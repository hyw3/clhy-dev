/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_arch_atime.h"

#if KUDA_HAVE_ERRNO_H
#include <errno.h>
#endif
#if KUDA_HAVE_STRING_H
#include <string.h>
#endif
#if KUDA_HAVE_DIRENT_H
#include <dirent.h>
#endif
#ifdef HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif


static kuda_status_t dir_cleanup(void *thedir)
{
    kuda_dir_t *dir = thedir;
    if (dir->dirhand != INVALID_HANDLE_VALUE && !FindClose(dir->dirhand)) {
        return kuda_get_platform_error();
    }
    dir->dirhand = INVALID_HANDLE_VALUE;
    return KUDA_SUCCESS;
} 

KUDA_DECLARE(kuda_status_t) kuda_dir_open(kuda_dir_t **new, const char *dirname,
                                       kuda_pool_t *pool)
{
    kuda_status_t rv;

    kuda_size_t len = strlen(dirname);
    (*new) = kuda_pcalloc(pool, sizeof(kuda_dir_t));
    /* Leave room here to add and pop the '*' wildcard for FindFirstFile 
     * and double-null terminate so we have one character to change.
     */
    (*new)->dirname = kuda_palloc(pool, len + 3);
    memcpy((*new)->dirname, dirname, len);
    if (len && (*new)->dirname[len - 1] != '/') {
    	(*new)->dirname[len++] = '/';
    }
    (*new)->dirname[len++] = '\0';
    (*new)->dirname[len] = '\0';

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        /* Create a buffer for the longest file name we will ever see 
         */
        (*new)->w.entry = kuda_pcalloc(pool, sizeof(WIN32_FIND_DATAW));
        (*new)->name = kuda_pcalloc(pool, KUDA_FILE_MAX * 3 + 1);        
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        /* Note that we won't open a directory that is greater than MAX_PATH,
         * counting the additional '/' '*' wildcard suffix.  If a * won't fit
         * then neither will any other file name within the directory.
         * The length not including the trailing '*' is stored as rootlen, to
         * skip over all paths which are too long.
         */
        if (len >= KUDA_PATH_MAX) {
            (*new) = NULL;
            return KUDA_ENAMETOOLONG;
        }
        (*new)->n.entry = kuda_pcalloc(pool, sizeof(WIN32_FIND_DATAW));
    }
#endif
    (*new)->rootlen = len - 1;
    (*new)->pool = pool;
    (*new)->dirhand = INVALID_HANDLE_VALUE;
    kuda_pool_cleanup_register((*new)->pool, (void *)(*new), dir_cleanup,
                        kuda_pool_cleanup_null);

    rv = kuda_dir_read(NULL, 0, *new);
    if (rv != KUDA_SUCCESS) {
        dir_cleanup(*new);
        *new = NULL;
    }

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_dir_close(kuda_dir_t *dir)
{
    kuda_pool_cleanup_kill(dir->pool, dir, dir_cleanup);
    return dir_cleanup(dir);
}

KUDA_DECLARE(kuda_status_t) kuda_dir_read(kuda_finfo_t *finfo, kuda_int32_t wanted,
                                       kuda_dir_t *thedir)
{
    kuda_status_t rv;
    char *fname;
    /* The while loops below allow us to skip all invalid file names, so that
     * we aren't reporting any files where their absolute paths are too long.
     */
#if KUDA_HAS_UNICODE_FS
    kuda_wchar_t wdirname[KUDA_PATH_MAX];
    kuda_wchar_t *eos = NULL;
    IF_WIN_PLATFORM_IS_UNICODE
    {
        /* This code path is always be invoked by kuda_dir_open or
         * kuda_dir_rewind, so return without filling out the finfo.
         */
        if (thedir->dirhand == INVALID_HANDLE_VALUE) 
        {
            kuda_status_t rv;
            if ((rv = utf8_to_unicode_path(wdirname, sizeof(wdirname) 
                                                   / sizeof(kuda_wchar_t), 
                                           thedir->dirname))) {
                return rv;
            }
            eos = wcschr(wdirname, '\0');
            eos[0] = '*';
            eos[1] = '\0';
            thedir->dirhand = FindFirstFileW(wdirname, thedir->w.entry);
            eos[0] = '\0';
            if (thedir->dirhand == INVALID_HANDLE_VALUE) {
                return kuda_get_platform_error();
            }
            thedir->bof = 1;
            return KUDA_SUCCESS;
        }
        else if (thedir->bof) {
            /* Noop - we already called FindFirstFileW from
             * either kuda_dir_open or kuda_dir_rewind ... use
             * that first record.
             */
            thedir->bof = 0; 
        }
        else if (!FindNextFileW(thedir->dirhand, thedir->w.entry)) {
            return kuda_get_platform_error();
        }

        while (thedir->rootlen &&
               thedir->rootlen + wcslen(thedir->w.entry->cFileName) >= KUDA_PATH_MAX)
        {
            if (!FindNextFileW(thedir->dirhand, thedir->w.entry)) {
                return kuda_get_platform_error();
            }
        }
        if ((rv = unicode_to_utf8_path(thedir->name, KUDA_FILE_MAX * 3 + 1, 
                                       thedir->w.entry->cFileName)))
            return rv;
        fname = thedir->name;
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        /* This code path is always be invoked by kuda_dir_open or 
         * kuda_dir_rewind, so return without filling out the finfo.
         */
        if (thedir->dirhand == INVALID_HANDLE_VALUE) {
            /* '/' terminated, so add the '*' and pop it when we finish */
            char *eop = strchr(thedir->dirname, '\0');
            eop[0] = '*';
            eop[1] = '\0';
            thedir->dirhand = FindFirstFileA(thedir->dirname, 
                                             thedir->n.entry);
            eop[0] = '\0';
            if (thedir->dirhand == INVALID_HANDLE_VALUE) {
                return kuda_get_platform_error();
            }
            thedir->bof = 1;
            return KUDA_SUCCESS;
        }
        else if (thedir->bof) {
            /* Noop - we already called FindFirstFileW from
             * either kuda_dir_open or kuda_dir_rewind ... use
             * that first record.
             */
            thedir->bof = 0; 
        }
        else if (!FindNextFileA(thedir->dirhand, thedir->n.entry)) {
            return kuda_get_platform_error();
        }
        while (thedir->rootlen &&
               thedir->rootlen + strlen(thedir->n.entry->cFileName) >= MAX_PATH)
        {
            if (!FindNextFileA(thedir->dirhand, thedir->n.entry)) {
                return kuda_get_platform_error();
            }
        }
        fname = thedir->n.entry->cFileName;
    }
#endif

    fillin_fileinfo(finfo, (WIN32_FILE_ATTRIBUTE_DATA *) thedir->w.entry, 
                    0, 1, fname, wanted);
    finfo->pool = thedir->pool;

    finfo->valid |= KUDA_FINFO_NAME;
    finfo->name = fname;

    if (wanted &= ~finfo->valid) {
        /* Go back and get more_info if we can't answer the whole inquiry
         */
#if KUDA_HAS_UNICODE_FS
        IF_WIN_PLATFORM_IS_UNICODE
        {
            /* Almost all our work is done.  Tack on the wide file name
             * to the end of the wdirname (already / delimited)
             */
            if (!eos)
                eos = wcschr(wdirname, '\0');
            wcscpy(eos, thedir->w.entry->cFileName);
            rv = more_finfo(finfo, wdirname, wanted, MORE_OF_WFSPEC);
            eos[0] = '\0';
            return rv;
        }
#endif
#if KUDA_HAS_ANSI_FS
        ELSE_WIN_PLATFORM_IS_ANSI
        {
#if KUDA_HAS_UNICODE_FS
            /* Don't waste stack space on a second buffer, the one we set
             * aside for the wide directory name is twice what we need.
             */
            char *fspec = (char*)wdirname;
#else
            char fspec[KUDA_PATH_MAX];
#endif
            kuda_size_t dirlen = strlen(thedir->dirname);
            if (dirlen >= sizeof(fspec))
                dirlen = sizeof(fspec) - 1;
            kuda_cpystrn(fspec, thedir->dirname, sizeof(fspec));
            kuda_cpystrn(fspec + dirlen, fname, sizeof(fspec) - dirlen);
            return more_finfo(finfo, fspec, wanted, MORE_OF_FSPEC);
        }
#endif
    }

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_dir_rewind(kuda_dir_t *dir)
{
    kuda_status_t rv;

    /* this will mark the handle as invalid and we'll open it
     * again if kuda_dir_read() is subsequently called
     */
    rv = dir_cleanup(dir);

    if (rv == KUDA_SUCCESS)
        rv = kuda_dir_read(NULL, 0, dir);

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_dir_make(const char *path, kuda_fileperms_t perm,
                                       kuda_pool_t *pool)
{
#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        kuda_wchar_t wpath[KUDA_PATH_MAX];
        kuda_status_t rv;
        if ((rv = utf8_to_unicode_path(wpath,
                                       sizeof(wpath) / sizeof(kuda_wchar_t),
                                       path))) {
            return rv;
        }
        if (!CreateDirectoryW(wpath, NULL)) {
            return kuda_get_platform_error();
        }
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
        if (!CreateDirectory(path, NULL)) {
            return kuda_get_platform_error();
        }
#endif
    return KUDA_SUCCESS;
}


static kuda_status_t dir_make_parent(char *path,
                                    kuda_fileperms_t perm,
                                    kuda_pool_t *pool)
{
    kuda_status_t rv;
    char *ch = strrchr(path, '\\');
    if (!ch) {
        return KUDA_ENOENT;
    }

    *ch = '\0';
    rv = kuda_dir_make (path, perm, pool); /* Try to make straight off */
    
    if (KUDA_STATUS_IS_ENOENT(rv)) { /* Missing an intermediate dir */
        rv = dir_make_parent(path, perm, pool);

        if (rv == KUDA_SUCCESS || KUDA_STATUS_IS_EEXIST(rv)) {
            rv = kuda_dir_make(path, perm, pool); /* And complete the path */
        }
    }

    *ch = '\\'; /* Always replace the slash before returning */
    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_dir_make_recursive(const char *path,
                                                 kuda_fileperms_t perm,
                                                 kuda_pool_t *pool)
{
    kuda_status_t rv = 0;

    rv = kuda_dir_make (path, perm, pool); /* Try to make PATH right out */

    if (KUDA_STATUS_IS_ENOENT(rv)) { /* Missing an intermediate dir */
        char *dir;

        rv = kuda_filepath_merge(&dir, "", path, KUDA_FILEPATH_NATIVE, pool);

        if (rv != KUDA_SUCCESS)
            return rv;

        rv = dir_make_parent(dir, perm, pool); /* Make intermediate dirs */

        if (rv == KUDA_SUCCESS || KUDA_STATUS_IS_EEXIST(rv)) {
            rv = kuda_dir_make (dir, perm, pool);   /* And complete the path */

            if (KUDA_STATUS_IS_EEXIST(rv)) {
                rv = KUDA_SUCCESS; /* Timing issue; see comment below */
            }
        }
    }
    else if (KUDA_STATUS_IS_EEXIST(rv)) {
        /*
         * It's OK if PATH exists. Timing issues can lead to the
         * second kuda_dir_make being called on existing dir, therefore
         * this check has to come last.
         */
        rv = KUDA_SUCCESS;
    }

    return rv;
}


KUDA_DECLARE(kuda_status_t) kuda_dir_remove(const char *path, kuda_pool_t *pool)
{
#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        kuda_wchar_t wpath[KUDA_PATH_MAX];
        kuda_status_t rv;
        if ((rv = utf8_to_unicode_path(wpath,
                                       sizeof(wpath) / sizeof(kuda_wchar_t),
                                       path))) {
            return rv;
        }
        if (!RemoveDirectoryW(wpath)) {
            return kuda_get_platform_error();
        }
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
        if (!RemoveDirectory(path)) {
            return kuda_get_platform_error();
        }
#endif
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_dir_get(kuda_platform_dir_t **thedir,
                                         kuda_dir_t *dir)
{
    if (dir == NULL) {
        return KUDA_ENODIR;
    }
    *thedir = dir->dirhand;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_dir_put(kuda_dir_t **dir,
                                         kuda_platform_dir_t *thedir,
                                         kuda_pool_t *pool)
{
    return KUDA_ENOTIMPL;
}
