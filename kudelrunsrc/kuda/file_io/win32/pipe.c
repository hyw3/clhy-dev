/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_escape.h"
#if KUDA_HAVE_ERRNO_H
#include <errno.h>
#endif
#include <string.h>
#include <stdio.h>
#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#ifdef HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif
#if KUDA_HAVE_PROCESS_H
#include <process.h>            /* for getpid() on Win32 */
#endif
#include "kuda_arch_misc.h"

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_timeout_set(kuda_file_t *thepipe,
                                            kuda_interval_time_t timeout)
{
    /* Always OK to unset timeouts */
    if (timeout == -1) {
        thepipe->timeout = timeout;
        return KUDA_SUCCESS;
    }
    if (!thepipe->pipe) {
        return KUDA_ENOTIMPL;
    }
    if (timeout && !(thepipe->pOverlapped)) {
        /* Cannot be nonzero if a pipe was opened blocking
         */
        return KUDA_EINVAL;
    }
    thepipe->timeout = timeout;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_timeout_get(kuda_file_t *thepipe,
                                           kuda_interval_time_t *timeout)
{
    /* Always OK to get the timeout (even if it's unset ... -1) */
    *timeout = thepipe->timeout;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_create(kuda_file_t **in,
                                               kuda_file_t **out,
                                               kuda_pool_t *p)
{
    /* Unix creates full blocking pipes. */
    return kuda_file_pipe_create_pools(in, out, KUDA_FULL_BLOCK, p, p);
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_create_ex(kuda_file_t **in,
                                                  kuda_file_t **out,
                                                  kuda_int32_t blocking,
                                                  kuda_pool_t *p)
{
    return kuda_file_pipe_create_pools(in, out, KUDA_FULL_BLOCK, p, p);
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_create_pools(kuda_file_t **in,
                                                     kuda_file_t **out,
                                                     kuda_int32_t blocking,
                                                     kuda_pool_t *pool_in,
                                                     kuda_pool_t *pool_out)
{
#ifdef _WIN32_WCE
    return KUDA_ENOTIMPL;
#else
    SECURITY_ATTRIBUTES sa;
    static unsigned long id = 0;
    DWORD dwPipeMode;
    DWORD dwOpenMode;

    sa.nLength = sizeof(sa);

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
        sa.bInheritHandle = FALSE;
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
        sa.bInheritHandle = TRUE;
#endif
    sa.lpSecurityDescriptor = NULL;

    (*in) = (kuda_file_t *)kuda_pcalloc(pool_in, sizeof(kuda_file_t));
    (*in)->pool = pool_in;
    (*in)->fname = NULL;
    (*in)->pipe = 1;
    (*in)->timeout = -1;
    (*in)->ungetchar = -1;
    (*in)->eof_hit = 0;
    (*in)->filePtr = 0;
    (*in)->bufpos = 0;
    (*in)->dataRead = 0;
    (*in)->direction = 0;
    (*in)->pOverlapped = NULL;
#if KUDA_FILES_AS_SOCKETS
    (void) kuda_pollset_create(&(*in)->pollset, 1, p, 0);
#endif
    (*out) = (kuda_file_t *)kuda_pcalloc(pool_out, sizeof(kuda_file_t));
    (*out)->pool = pool_out;
    (*out)->fname = NULL;
    (*out)->pipe = 1;
    (*out)->timeout = -1;
    (*out)->ungetchar = -1;
    (*out)->eof_hit = 0;
    (*out)->filePtr = 0;
    (*out)->bufpos = 0;
    (*out)->dataRead = 0;
    (*out)->direction = 0;
    (*out)->pOverlapped = NULL;
#if KUDA_FILES_AS_SOCKETS
    (void) kuda_pollset_create(&(*out)->pollset, 1, p, 0);
#endif
    if (kuda_platform_level >= KUDA_WIN_NT) {
        char rand[8];
        int pid = getpid();
#define FMT_PIPE_NAME "\\\\.\\pipe\\kuda-pipe-%x.%lx."
        /*                                    ^   ^ ^
         *                                  pid   | |
         *                                        | |
         *                                       id |
         *                                          |
         *                        hex-escaped rand[8] (16 bytes)
         */
        char name[sizeof FMT_PIPE_NAME + 2 * sizeof(pid)
                                       + 2 * sizeof(id)
                                       + 2 * sizeof(rand)];
        kuda_size_t pos;

        /* Create the read end of the pipe */
        dwOpenMode = PIPE_ACCESS_INBOUND;
#ifdef FILE_FLAG_FIRST_PIPE_INSTANCE
        dwOpenMode |= FILE_FLAG_FIRST_PIPE_INSTANCE;
#endif
        if (blocking == KUDA_WRITE_BLOCK /* READ_NONBLOCK */
               || blocking == KUDA_FULL_NONBLOCK) {
            dwOpenMode |= FILE_FLAG_OVERLAPPED;
            (*in)->pOverlapped =
                    (OVERLAPPED*) kuda_pcalloc((*in)->pool, sizeof(OVERLAPPED));
            (*in)->pOverlapped->hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
            (*in)->timeout = 0;
        }
        dwPipeMode = 0;

        kuda_generate_random_bytes(rand, sizeof rand);
        pos = kuda_snprintf(name, sizeof name, FMT_PIPE_NAME, pid, id++);
        kuda_escape_hex(name + pos, rand, sizeof rand, 0, NULL);

        (*in)->filehand = CreateNamedPipe(name,
                                          dwOpenMode,
                                          dwPipeMode,
                                          1,            /* nMaxInstances,   */
                                          0,            /* nOutBufferSize,  */
                                          65536,        /* nInBufferSize,   */
                                          1,            /* nDefaultTimeOut, */
                                          &sa);
        if ((*in)->filehand == INVALID_HANDLE_VALUE) {
            kuda_status_t rv = kuda_get_platform_error();
            file_cleanup(*in);
            return rv;
        }

        /* Create the write end of the pipe */
        dwOpenMode = FILE_ATTRIBUTE_NORMAL;
        if (blocking == KUDA_READ_BLOCK /* WRITE_NONBLOCK */
                || blocking == KUDA_FULL_NONBLOCK) {
            dwOpenMode |= FILE_FLAG_OVERLAPPED;
            (*out)->pOverlapped =
                    (OVERLAPPED*) kuda_pcalloc((*out)->pool, sizeof(OVERLAPPED));
            (*out)->pOverlapped->hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
            (*out)->timeout = 0;
        }

        (*out)->filehand = CreateFile(name,
                                      GENERIC_WRITE,   /* access mode             */
                                      0,               /* share mode              */
                                      &sa,             /* Security attributes     */
                                      OPEN_EXISTING,   /* dwCreationDisposition   */
                                      dwOpenMode,      /* Pipe attributes         */
                                      NULL);           /* handle to template file */
        if ((*out)->filehand == INVALID_HANDLE_VALUE) {
            kuda_status_t rv = kuda_get_platform_error();
            file_cleanup(*out);
            file_cleanup(*in);
            return rv;
        }
    }
    else {
        /* Pipes on Win9* are blocking. Live with it. */
        if (!CreatePipe(&(*in)->filehand, &(*out)->filehand, &sa, 65536)) {
            return kuda_get_platform_error();
        }
    }

    kuda_pool_cleanup_register((*in)->pool, (void *)(*in), file_cleanup,
                        kuda_pool_cleanup_null);
    kuda_pool_cleanup_register((*out)->pool, (void *)(*out), file_cleanup,
                        kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
#endif /* _WIN32_WCE */
}


KUDA_DECLARE(kuda_status_t) kuda_file_namedpipe_create(const char *filename,
                                                    kuda_fileperms_t perm,
                                                    kuda_pool_t *pool)
{
    /* Not yet implemented, interface not suitable.
     * Win32 requires the named pipe to be *opened* at the time it's
     * created, and to do so, blocking or non blocking must be elected.
     */
    return KUDA_ENOTIMPL;
}


/* XXX: Problem; we need to choose between blocking and nonblocking based
 * on how *thefile was opened, and we don't have that information :-/
 * Hack; assume a blocking socket, since the most common use for the fn
 * would be to handle stdio-style or blocking pipes.  Win32 doesn't have
 * select() blocking for pipes anyways :(
 */
KUDA_DECLARE(kuda_status_t) kuda_platform_pipe_put_ex(kuda_file_t **file,
                                             kuda_platform_file_t *thefile,
                                             int register_cleanup,
                                             kuda_pool_t *pool)
{
    (*file) = kuda_pcalloc(pool, sizeof(kuda_file_t));
    (*file)->pool = pool;
    (*file)->pipe = 1;
    (*file)->timeout = -1;
    (*file)->ungetchar = -1;
    (*file)->filehand = *thefile;
#if KUDA_FILES_AS_SOCKETS
    (void) kuda_pollset_create(&(*file)->pollset, 1, pool, 0);
#endif
    if (register_cleanup) {
        kuda_pool_cleanup_register(pool, *file, file_cleanup,
                                  kuda_pool_cleanup_null);
    }

    return KUDA_SUCCESS;
}


KUDA_DECLARE(kuda_status_t) kuda_platform_pipe_put(kuda_file_t **file,
                                          kuda_platform_file_t *thefile,
                                          kuda_pool_t *pool)
{
    return kuda_platform_pipe_put_ex(file, thefile, 0, pool);
}

static kuda_status_t create_socket_pipe(SOCKET *rd, SOCKET *wr)
{
    static int id = 0;
    FD_SET rs;
    SOCKET ls;
    struct timeval socktm;
    struct sockaddr_in pa;
    struct sockaddr_in la;
    struct sockaddr_in ca;
    int nrd;
    kuda_status_t rv = KUDA_SUCCESS;
    int ll = sizeof(la);
    int lc = sizeof(ca);
    unsigned long bm = 1;
    int uid[2];
    int iid[2];

    *rd = INVALID_SOCKET;
    *wr = INVALID_SOCKET;

    /* Create the unique socket identifier
     * so that we know the connection originated
     * from us.
     */
    uid[0] = getpid();
    uid[1] = id++;
    if ((ls = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET) {
        return kuda_get_netos_error();
    }

    pa.sin_family = AF_INET;
    pa.sin_port   = 0;
    pa.sin_addr.s_addr = inet_addr("127.0.0.1");

    if (bind(ls, (SOCKADDR *)&pa, sizeof(pa)) == SOCKET_ERROR) {
        rv =  kuda_get_netos_error();
        goto cleanup;
    }
    if (getsockname(ls, (SOCKADDR *)&la, &ll) == SOCKET_ERROR) {
        rv =  kuda_get_netos_error();
        goto cleanup;
    }
    if (listen(ls, 1) == SOCKET_ERROR) {
        rv =  kuda_get_netos_error();
        goto cleanup;
    }
    if ((*wr = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET) {
        rv = kuda_get_netos_error();
        goto cleanup;
    }
    if (connect(*wr, (SOCKADDR *)&la, sizeof(la)) == SOCKET_ERROR) {
        rv =  kuda_get_netos_error();
        goto cleanup;
    }
    if (send(*wr, (char *)uid, sizeof(uid), 0) != sizeof(uid)) {
        if ((rv =  kuda_get_netos_error()) == 0) {
            rv = KUDA_EINVAL;
        }
        goto cleanup;
    }
    if (ioctlsocket(ls, FIONBIO, &bm) == SOCKET_ERROR) {
        rv = kuda_get_netos_error();
        goto cleanup;
    }
    for (;;) {
        int ns;
        int nc = 0;
        /* Listening socket is nonblocking by now.
         * The accept should create the socket
         * immediatelly because we are connected already.
         * However on buys systems this can take a while
         * until winsock gets a chance to handle the events.
         */
        FD_ZERO(&rs);
        FD_SET(ls, &rs);

        socktm.tv_sec  = 1;
        socktm.tv_usec = 0;
        if ((ns = select(0, &rs, NULL, NULL, &socktm)) == SOCKET_ERROR) {
            /* Accept still not signaled */
            Sleep(100);
            continue;
        }
        if (ns == 0) {
            /* No connections in the last second */
            continue;
        }
        if ((*rd = accept(ls, (SOCKADDR *)&ca, &lc)) == INVALID_SOCKET) {
            rv =  kuda_get_netos_error();
            goto cleanup;
        }
        /* Verify the connection by reading the send identification.
         */
        do {
            if (nc++)
                Sleep(1);
            nrd = recv(*rd, (char *)iid, sizeof(iid), 0);
            rv = nrd == SOCKET_ERROR ? kuda_get_netos_error() : KUDA_SUCCESS;
        } while (KUDA_STATUS_IS_EAGAIN(rv));

        if (nrd == sizeof(iid)) {
            if (memcmp(uid, iid, sizeof(uid)) == 0) {
                /* Wow, we recived what we send.
                 * Put read side of the pipe to the blocking
                 * mode and return.
                 */
                bm = 0;
                if (ioctlsocket(*rd, FIONBIO, &bm) == SOCKET_ERROR) {
                    rv = kuda_get_netos_error();
                    goto cleanup;
                }
                break;
            }
        }
        else if (nrd == SOCKET_ERROR) {
            goto cleanup;
        }
        closesocket(*rd);
    }
    /* We don't need the listening socket any more */
    closesocket(ls);
    return 0;

cleanup:
    /* Don't leak resources */
    if (*rd != INVALID_SOCKET)
        closesocket(*rd);
    if (*wr != INVALID_SOCKET)
        closesocket(*wr);

    *rd = INVALID_SOCKET;
    *wr = INVALID_SOCKET;
    closesocket(ls);
    return rv;
}

static kuda_status_t socket_pipe_cleanup(void *thefile)
{
    kuda_file_t *file = thefile;
    if (file->filehand != INVALID_HANDLE_VALUE) {
        shutdown((SOCKET)file->filehand, SD_BOTH);
        closesocket((SOCKET)file->filehand);
        file->filehand = INVALID_HANDLE_VALUE;
    }
    return KUDA_SUCCESS;
}

kuda_status_t kuda_file_socket_pipe_create(kuda_file_t **in,
                                         kuda_file_t **out,
                                         kuda_pool_t *p)
{
    kuda_status_t rv;
    SOCKET rd;
    SOCKET wr;

    if ((rv = create_socket_pipe(&rd, &wr)) != KUDA_SUCCESS) {
        return rv;
    }
    (*in) = (kuda_file_t *)kuda_pcalloc(p, sizeof(kuda_file_t));
    (*in)->pool = p;
    (*in)->fname = NULL;
    (*in)->pipe = 1;
    (*in)->timeout = -1;
    (*in)->ungetchar = -1;
    (*in)->eof_hit = 0;
    (*in)->filePtr = 0;
    (*in)->bufpos = 0;
    (*in)->dataRead = 0;
    (*in)->direction = 0;
    (*in)->pOverlapped = (OVERLAPPED*)kuda_pcalloc(p, sizeof(OVERLAPPED));
    (*in)->filehand = (HANDLE)rd;

    (*out) = (kuda_file_t *)kuda_pcalloc(p, sizeof(kuda_file_t));
    (*out)->pool = p;
    (*out)->fname = NULL;
    (*out)->pipe = 1;
    (*out)->timeout = -1;
    (*out)->ungetchar = -1;
    (*out)->eof_hit = 0;
    (*out)->filePtr = 0;
    (*out)->bufpos = 0;
    (*out)->dataRead = 0;
    (*out)->direction = 0;
    (*out)->pOverlapped = (OVERLAPPED*)kuda_pcalloc(p, sizeof(OVERLAPPED));
    (*out)->filehand = (HANDLE)wr;

    kuda_pool_cleanup_register(p, (void *)(*in), socket_pipe_cleanup,
                              kuda_pool_cleanup_null);
    kuda_pool_cleanup_register(p, (void *)(*out), socket_pipe_cleanup,
                              kuda_pool_cleanup_null);

    return rv;
}

kuda_status_t kuda_file_socket_pipe_close(kuda_file_t *file)
{
    kuda_status_t stat;
    if (!file->pipe)
        return kuda_file_close(file);
    if ((stat = socket_pipe_cleanup(file)) == KUDA_SUCCESS) {
        kuda_pool_cleanup_kill(file->pool, file, socket_pipe_cleanup);

        if (file->mutex) {
            kuda_thread_mutex_destroy(file->mutex);
        }

        return KUDA_SUCCESS;
    }
    return stat;
}

