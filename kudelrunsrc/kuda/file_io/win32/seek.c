/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "kuda_file_io.h"
#include <errno.h>
#include <string.h>

static kuda_status_t setptr(kuda_file_t *thefile, kuda_off_t pos )
{
    kuda_off_t newbufpos;
    kuda_status_t rv;
    DWORD rc;

    if (thefile->direction == 1) {
        /* XXX: flush here is not mutex protected */
        rv = kuda_file_flush(thefile);
        if (rv != KUDA_SUCCESS)
            return rv;
        thefile->bufpos = thefile->dataRead = 0;
        thefile->direction = 0;
    }

    /* We may be truncating to size here. 
     * XXX: testing an 'unsigned' as >= 0 below indicates a bug
     */
    newbufpos = pos - (thefile->filePtr - thefile->dataRead);

    if (newbufpos >= 0 && newbufpos <= (kuda_off_t)thefile->dataRead) {
        thefile->bufpos = (kuda_size_t)newbufpos;
        rv = KUDA_SUCCESS;
    } else {
        DWORD offlo = (DWORD)pos;
        LONG  offhi = (LONG)(pos >> 32);
        rc = SetFilePointer(thefile->filehand, offlo, &offhi, FILE_BEGIN);

        if (rc == (DWORD)-1)
            /* A legal value, perhaps?  MSDN implies prior SetLastError isn't
             * needed, googling for SetLastError SetFilePointer seems
             * to confirm this.  INVALID_SET_FILE_POINTER is too recently
             * added for us to rely on it as a constant.
             */
            rv = kuda_get_platform_error();
        else
            rv = KUDA_SUCCESS;

        if (rv == KUDA_SUCCESS) {
            rv = KUDA_SUCCESS;
            thefile->eof_hit = 0;
            thefile->bufpos = thefile->dataRead = 0;
            thefile->filePtr = pos;
        }
    }

    return rv;
}


KUDA_DECLARE(kuda_status_t) kuda_file_seek(kuda_file_t *thefile, kuda_seek_where_t where, kuda_off_t *offset)
{
    kuda_finfo_t finfo;
    kuda_status_t rc = KUDA_SUCCESS;

    thefile->eof_hit = 0;

    if (thefile->buffered) {
        switch (where) {
            case KUDA_SET:
                rc = setptr(thefile, *offset);
                break;

            case KUDA_CUR:
                rc = setptr(thefile, thefile->filePtr - thefile->dataRead 
                                      + thefile->bufpos + *offset);
                break;

            case KUDA_END:
                rc = kuda_file_info_get(&finfo, KUDA_FINFO_SIZE, thefile);
                if (rc == KUDA_SUCCESS)
                    rc = setptr(thefile, finfo.size + *offset);
                break;

            default:
                return KUDA_EINVAL;
        }

        *offset = thefile->filePtr - thefile->dataRead + thefile->bufpos;
        return rc;
    }
    /* A file opened with KUDA_FOPEN_XTHREAD has been opened for overlapped i/o.
     * KUDA must explicitly track the file pointer in this case.
     */
    else if (thefile->pOverlapped || thefile->flags & KUDA_FOPEN_XTHREAD) {
        switch(where) {
            case KUDA_SET:
                thefile->filePtr = *offset;
                break;
        
            case KUDA_CUR:
                thefile->filePtr += *offset;
                break;
        
            case KUDA_END:
                rc = kuda_file_info_get(&finfo, KUDA_FINFO_SIZE, thefile);
                if (rc == KUDA_SUCCESS && finfo.size + *offset >= 0)
                    thefile->filePtr = finfo.size + *offset;
                break;

            default:
                return KUDA_EINVAL;
        }
        *offset = thefile->filePtr;
        return rc;
    }
    else {
        DWORD howmove;
        DWORD offlo = (DWORD)*offset;
        DWORD offhi = (DWORD)(*offset >> 32);

        switch(where) {
            case KUDA_SET:
                howmove = FILE_BEGIN;   break;
            case KUDA_CUR:
                howmove = FILE_CURRENT; break;
            case KUDA_END:
                howmove = FILE_END;     break;
            default:
                return KUDA_EINVAL;
        }
        offlo = SetFilePointer(thefile->filehand, (LONG)offlo, 
                               (LONG*)&offhi, howmove);
        if (offlo == 0xFFFFFFFF)
            rc = kuda_get_platform_error();
        else
            rc = KUDA_SUCCESS;
        /* Since we can land at 0xffffffff we will measure our KUDA_SUCCESS */
        if (rc == KUDA_SUCCESS)
            *offset = ((kuda_off_t)offhi << 32) | offlo;
        return rc;
    }
}


KUDA_DECLARE(kuda_status_t) kuda_file_trunc(kuda_file_t *thefile, kuda_off_t offset)
{
    kuda_status_t rv;
    DWORD offlo = (DWORD)offset;
    LONG  offhi = (LONG)(offset >> 32);
    DWORD rc;

    if (thefile->buffered) {
        if (thefile->direction == 1) {
            /* Figure out what needs to be flushed.  Don't flush the part
             * of the write buffer that will get truncated anyway.
             */
            if (offset < thefile->filePtr) {
                thefile->bufpos = 0;
            }
            else if (offset < thefile->filePtr + (kuda_off_t)thefile->bufpos) {
                thefile->bufpos = offset - thefile->filePtr;
            }

            if (thefile->bufpos != 0) {
                rv = kuda_file_flush(thefile);
                if (rv != KUDA_SUCCESS)
                    return rv;
            }
        }
        else if (thefile->direction == 0) {
            /* Discard the read buffer, as we are about to reposition
             * ourselves to the end of file.
             */
            thefile->bufpos = 0;
            thefile->dataRead = 0;
        }
    }

    rc = SetFilePointer(thefile->filehand, offlo, &offhi, FILE_BEGIN);
    if (rc == 0xFFFFFFFF)
        if ((rv = kuda_get_platform_error()) != KUDA_SUCCESS)
            return rv;
    thefile->filePtr = offset;
    /* Don't report EOF until the next read. */
    thefile->eof_hit = 0;

    if (!SetEndOfFile(thefile->filehand))
        return kuda_get_platform_error();

    return KUDA_SUCCESS;
}
