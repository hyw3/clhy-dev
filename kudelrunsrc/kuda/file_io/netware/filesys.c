/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_arch_file_io.h"
#include "kuda_strings.h"

kuda_status_t filepath_root_case(char **rootpath, char *root, kuda_pool_t *p)
{
/* See the Windows code to figure out what to do here.
    It probably checks to make sure that the root exists 
    and case it correctly according to the file system.
*/
    *rootpath = kuda_pstrdup(p, root);
    return KUDA_SUCCESS;
}

kuda_status_t filepath_has_drive(const char *rootpath, int only, kuda_pool_t *p)
{
    char *s;

    if (rootpath) {
        s = strchr (rootpath, ':');
        if (only)
            /* Test if the path only has a drive/volume and nothing else
            */
            return (s && (s != rootpath) && !s[1]);
        else
            /* Test if the path includes a drive/volume
            */
            return (s && (s != rootpath));
    }
    return 0;
}

kuda_status_t filepath_compare_drive(const char *path1, const char *path2, kuda_pool_t *p)
{
    char *s1, *s2;

    if (path1 && path2) {
        s1 = strchr (path1, ':');
        s2 = strchr (path2, ':');

        /* Make sure that they both have a drive/volume delimiter
            and are the same size.  Then see if they match.
        */
        if (s1 && s2 && ((s1-path1) == (s2-path2))) {
            return strnicmp (s1, s2, s1-path1);
        }
    }
    return -1;
}

KUDA_DECLARE(kuda_status_t) kuda_filepath_get(char **rootpath, kuda_int32_t flags,
                                           kuda_pool_t *p)
{
    char path[KUDA_PATH_MAX];
    char *ptr;

    /* use getcwdpath to make sure that we get the volume name*/
    if (!getcwdpath(path, NULL, 0)) {
        if (errno == ERANGE)
            return KUDA_ENAMETOOLONG;
        else
            return errno;
    }
    /* Strip off the server name if there is one*/
    ptr = strpbrk(path, "\\/:");
    if (!ptr) {
        return KUDA_ENOENT;
    }
    if (*ptr == ':') {
        ptr = path;
    }
    *rootpath = kuda_pstrdup(p, ptr);
    if (!(flags & KUDA_FILEPATH_NATIVE)) {
        for (ptr = *rootpath; *ptr; ++ptr) {
            if (*ptr == '\\')
                *ptr = '/';
        }
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_filepath_set(const char *rootpath,
                                           kuda_pool_t *p)
{
    if (chdir2(rootpath) != 0)
        return errno;
    return KUDA_SUCCESS;
}


