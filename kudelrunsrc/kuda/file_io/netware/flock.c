/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <nks/fsio.h>
#include "kuda_arch_file_io.h"


kuda_status_t kuda_file_lock(kuda_file_t *thefile, int type)
{
	int fc;

	fc = (type & KUDA_FLOCK_NONBLOCK) ? NX_RANGE_LOCK_TRYLOCK : NX_RANGE_LOCK_CHECK;

    if(NXFileRangeLock(thefile->filedes,fc, 0, 0) == -1)
		return errno;
            
    return KUDA_SUCCESS;
}

kuda_status_t kuda_file_unlock(kuda_file_t *thefile)
{
    if(NXFileRangeUnlock(thefile->filedes,NX_RANGE_LOCK_CANCEL,0 , 0) == -1)
		return errno;
   
    return KUDA_SUCCESS;
}
