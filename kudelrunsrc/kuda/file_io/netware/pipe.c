/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <nks/fsio.h>
#include <nks/errno.h>

#include "kuda_arch_file_io.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_arch_inherit.h"

static kuda_status_t pipeblock(kuda_file_t *thepipe)
{
#ifdef USE_FLAGS
	unsigned long	flags;

	if (fcntl(thepipe->filedes, F_GETFL, &flags) != -1)
	{
		flags &= ~FNDELAY;
		fcntl(thepipe->filedes, F_SETFL, flags);
	}
#else
        errno = 0;
		fcntl(thepipe->filedes, F_SETFL, 0);
#endif

    if (errno)
        return errno;

    thepipe->blocking = BLK_ON;
    return KUDA_SUCCESS;
}

static kuda_status_t pipenonblock(kuda_file_t *thepipe)
{
#ifdef USE_FLAGS
	unsigned long	flags;

    errno = 0;
	if (fcntl(thepipe->filedes, F_GETFL, &flags) != -1)
	{
		flags |= FNDELAY;
		fcntl(thepipe->filedes, F_SETFL, flags);
	}
#else
        errno = 0;
		fcntl(thepipe->filedes, F_SETFL, FNDELAY);
#endif

    if (errno)
        return errno;

    thepipe->blocking = BLK_OFF;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_timeout_set(kuda_file_t *thepipe, kuda_interval_time_t timeout)
{
    if (thepipe->is_pipe == 1) {
        thepipe->timeout = timeout;
        if (timeout >= 0) {
            if (thepipe->blocking != BLK_OFF) { /* blocking or unknown state */
                return pipenonblock(thepipe);
            }
        }
        else {
            if (thepipe->blocking != BLK_ON) { /* non-blocking or unknown state */
                return pipeblock(thepipe);
            }
        }
        return KUDA_SUCCESS;
    }
    return KUDA_EINVAL;
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_timeout_get(kuda_file_t *thepipe, kuda_interval_time_t *timeout)
{
    if (thepipe->is_pipe == 1) {
        *timeout = thepipe->timeout;
        return KUDA_SUCCESS;
    }
    return KUDA_EINVAL;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_pipe_put_ex(kuda_file_t **file,
                                             kuda_platform_file_t *thefile,
                                             int register_cleanup,
                                             kuda_pool_t *pool)
{
    int *dafile = thefile;
    
    (*file) = kuda_pcalloc(pool, sizeof(kuda_file_t));
    (*file)->pool = pool;
    (*file)->eof_hit = 0;
    (*file)->is_pipe = 1;
    (*file)->blocking = BLK_UNKNOWN; /* app needs to make a timeout call */
    (*file)->timeout = -1;
    (*file)->ungetchar = -1; /* no char avail */
    (*file)->filedes = *dafile;
    if (!register_cleanup) {
        (*file)->flags = KUDA_FOPEN_NOCLEANUP;
    }
    (*file)->buffered = 0;
#if KUDA_HAS_THREADS
    (*file)->thlock = NULL;
#endif
    if (register_cleanup) {
        kuda_pool_cleanup_register((*file)->pool, (void *)(*file),
                                  kuda_unix_file_cleanup,
                                  kuda_pool_cleanup_null);
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_pipe_put(kuda_file_t **file,
                                          kuda_platform_file_t *thefile,
                                          kuda_pool_t *pool)
{
    return kuda_platform_pipe_put_ex(file, thefile, 0, pool);
}

static kuda_status_t file_pipe_create(kuda_file_t **in, kuda_file_t **out,
        kuda_pool_t *pool_in, kuda_pool_t *pool_out)
{
	int     	filedes[2];

    if (pipe(filedes) == -1) {
        return errno;
    }

    (*in) = (kuda_file_t *)kuda_pcalloc(pool_in, sizeof(kuda_file_t));
    (*out) = (kuda_file_t *)kuda_pcalloc(pool_out, sizeof(kuda_file_t));

    (*in)->pool     = pool_in;
    (*out)->pool    = pool_out;
    (*in)->filedes   = filedes[0];
    (*out)->filedes  = filedes[1];
    (*in)->flags     = KUDA_INHERIT;
    (*out)->flags    = KUDA_INHERIT;
    (*in)->is_pipe      =
    (*out)->is_pipe     = 1;
    (*out)->fname    = 
    (*in)->fname     = NULL;
    (*in)->buffered  =
    (*out)->buffered = 0;
    (*in)->blocking  =
    (*out)->blocking = BLK_ON;
    (*in)->timeout   =
    (*out)->timeout  = -1;
    (*in)->ungetchar = -1;
    (*in)->thlock    =
    (*out)->thlock   = NULL;
    (void) kuda_pollset_create(&(*in)->pollset, 1, pool_in, 0);
    (void) kuda_pollset_create(&(*out)->pollset, 1, pool_out, 0);

    kuda_pool_cleanup_register((*in)->pool, (void *)(*in), kuda_unix_file_cleanup,
                         kuda_pool_cleanup_null);
    kuda_pool_cleanup_register((*out)->pool, (void *)(*out), kuda_unix_file_cleanup,
                         kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}

static void file_pipe_block(kuda_file_t **in, kuda_file_t **out,
        kuda_int32_t blocking)
{
    switch (blocking) {
    case KUDA_FULL_BLOCK:
        break;
    case KUDA_READ_BLOCK:
        kuda_file_pipe_timeout_set(*out, 0);
        break;
    case KUDA_WRITE_BLOCK:
        kuda_file_pipe_timeout_set(*in, 0);
        break;
    default:
        kuda_file_pipe_timeout_set(*out, 0);
        kuda_file_pipe_timeout_set(*in, 0);
        break;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_create(kuda_file_t **in, kuda_file_t **out, kuda_pool_t *pool)
{
    return file_pipe_create(in, out, pool, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_create_ex(kuda_file_t **in, 
                                                  kuda_file_t **out, 
                                                  kuda_int32_t blocking,
                                                  kuda_pool_t *pool)
{
    kuda_status_t status;

    if ((status = file_pipe_create(in, out, pool, pool)) != KUDA_SUCCESS) {
        return status;
    }

    file_pipe_block(in, out, blocking);

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_pipe_create_pools(kuda_file_t **in,
        kuda_file_t **out, kuda_int32_t blocking, kuda_pool_t *pool_in, kuda_pool_t *pool_out)
{
    kuda_status_t status;

    if ((status = file_pipe_create(in, out, pool_in, pool_out)) != KUDA_SUCCESS) {
        return status;
    }

    file_pipe_block(in, out, blocking);

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_namedpipe_create(const char *filename, 
                                                    kuda_fileperms_t perm, kuda_pool_t *pool)
{
    return KUDA_ENOTIMPL;
} 

    

