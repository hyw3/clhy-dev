/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_file_io.h"
#include "fsio.h"
#include "nks/dirio.h"
#include "kuda_file_io.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_errno.h"
#include "kuda_hash.h"
#include "kuda_thread_rwlock.h"

#ifdef HAVE_UTIME_H
#include <utime.h>
#endif

#define KUDA_HAS_PSA

static kuda_filetype_e filetype_from_mode(mode_t mode)
{
    kuda_filetype_e type = KUDA_NOFILE;

    if (S_ISREG(mode))
        type = KUDA_REG;
    else if (S_ISDIR(mode))
        type = KUDA_DIR;
    else if (S_ISCHR(mode))
        type = KUDA_CHR;
    else if (S_ISBLK(mode))
        type = KUDA_BLK;
    else if (S_ISFIFO(mode))
        type = KUDA_PIPE;
    else if (S_ISLNK(mode))
        type = KUDA_LNK;
    else if (S_ISSOCK(mode))
        type = KUDA_SOCK;
    else
        type = KUDA_UNKFILE;
    return type;
}

static void fill_out_finfo(kuda_finfo_t *finfo, struct stat *info,
                           kuda_int32_t wanted)
{ 
    finfo->valid = KUDA_FINFO_MIN | KUDA_FINFO_IDENT | KUDA_FINFO_NLINK 
                    | KUDA_FINFO_OWNER | KUDA_FINFO_PROT;

    finfo->protection = kuda_unix_mode2perms(info->st_mode);
    finfo->filetype = filetype_from_mode(info->st_mode);
    finfo->user = info->st_uid;
    finfo->group = info->st_gid;
    finfo->size = info->st_size;
    finfo->inode = info->st_ino;
    finfo->device = info->st_dev;
    finfo->nlink = info->st_nlink;

    kuda_time_ansi_put(&finfo->atime, info->st_atime.tv_sec);
    kuda_time_ansi_put(&finfo->mtime, info->st_mtime.tv_sec);
    kuda_time_ansi_put(&finfo->ctime, info->st_ctime.tv_sec);

#ifdef HAVE_STRUCT_STAT_ST_BLOCKS
#ifdef DEV_BSIZE
    finfo->csize = (kuda_off_t)info->st_blocks * (kuda_off_t)DEV_BSIZE;
#else
    finfo->csize = (kuda_off_t)info->st_blocks * (kuda_off_t)512;
#endif
    finfo->valid |= KUDA_FINFO_CSIZE;
#endif
}

kuda_status_t kuda_file_info_get_locked(kuda_finfo_t *finfo, kuda_int32_t wanted,
                                      kuda_file_t *thefile)
{
    struct_stat info;

    if (thefile->buffered) {
        kuda_status_t rv = kuda_file_flush_locked(thefile);
        if (rv != KUDA_SUCCESS)
            return rv;
    }

    if (fstat(thefile->filedes, &info) == 0) {
        finfo->pool = thefile->pool;
        finfo->fname = thefile->fname;
        fill_out_finfo(finfo, &info, wanted);
        return (wanted & ~finfo->valid) ? KUDA_INCOMPLETE : KUDA_SUCCESS;
    }
    else {
        return errno;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_file_info_get(kuda_finfo_t *finfo, 
                                            kuda_int32_t wanted,
                                            kuda_file_t *thefile)
{
    struct stat info;

    if (thefile->buffered) {
        /* XXX: flush here is not mutex protected */
        kuda_status_t rv = kuda_file_flush(thefile);
        if (rv != KUDA_SUCCESS)
            return rv;
    }

    if (fstat(thefile->filedes, &info) == 0) {
        finfo->pool = thefile->pool;
        finfo->fname = thefile->fname;
        fill_out_finfo(finfo, &info, wanted);
        return (wanted & ~finfo->valid) ? KUDA_INCOMPLETE : KUDA_SUCCESS;
    }
    else {
        return errno;
    }
}

KUDA_DECLARE(kuda_status_t) kuda_file_perms_set(const char *fname, 
                                             kuda_fileperms_t perms)
{
    mode_t mode = kuda_unix_perms2mode(perms);

    if (chmod(fname, mode) == -1)
        return errno;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_file_attrs_set(const char *fname,
                                             kuda_fileattrs_t attributes,
                                             kuda_fileattrs_t attr_mask,
                                             kuda_pool_t *pool)
{
    kuda_status_t status;
    kuda_finfo_t finfo;

    /* Don't do anything if we can't handle the requested attributes */
    if (!(attr_mask & (KUDA_FILE_ATTR_READONLY
                       | KUDA_FILE_ATTR_EXECUTABLE)))
        return KUDA_SUCCESS;

    status = kuda_stat(&finfo, fname, KUDA_FINFO_PROT, pool);
    if (status)
        return status;

    /* ### TODO: should added bits be umask'd? */
    if (attr_mask & KUDA_FILE_ATTR_READONLY)
    {
        if (attributes & KUDA_FILE_ATTR_READONLY)
        {
            finfo.protection &= ~KUDA_UWRITE;
            finfo.protection &= ~KUDA_GWRITE;
            finfo.protection &= ~KUDA_WWRITE;
        }
        else
        {
            /* ### umask this! */
            finfo.protection |= KUDA_UWRITE;
            finfo.protection |= KUDA_GWRITE;
            finfo.protection |= KUDA_WWRITE;
        }
    }

    if (attr_mask & KUDA_FILE_ATTR_EXECUTABLE)
    {
        if (attributes & KUDA_FILE_ATTR_EXECUTABLE)
        {
            /* ### umask this! */
            finfo.protection |= KUDA_UEXECUTE;
            finfo.protection |= KUDA_GEXECUTE;
            finfo.protection |= KUDA_WEXECUTE;
        }
        else
        {
            finfo.protection &= ~KUDA_UEXECUTE;
            finfo.protection &= ~KUDA_GEXECUTE;
            finfo.protection &= ~KUDA_WEXECUTE;
        }
    }

    return kuda_file_perms_set(fname, finfo.protection);
}

#ifndef KUDA_HAS_PSA
static kuda_status_t stat_cache_cleanup(void *data)
{
    kuda_pool_t *p = (kuda_pool_t *)getGlobalPool();
    kuda_hash_index_t *hi;
    kuda_hash_t *statCache = (kuda_hash_t*)data;
	char *key;
    kuda_ssize_t keylen;
    NXPathCtx_t pathctx;

    for (hi = kuda_hash_first(p, statCache); hi; hi = kuda_hash_next(hi)) {
        kuda_hash_this(hi, (const void**)&key, &keylen, (void**)&pathctx);

        if (pathctx) {
            NXFreePathContext(pathctx);
        }
    }

    return KUDA_SUCCESS;
}

int cstat (NXPathCtx_t ctx, char *path, struct stat *buf, unsigned long requestmap, kuda_pool_t *p)
{
    kuda_pool_t *gPool = (kuda_pool_t *)getGlobalPool();
    kuda_hash_t *statCache = NULL;
    kuda_thread_rwlock_t *rwlock = NULL;

    NXPathCtx_t pathctx = 0;
    char *ptr = NULL, *tr;
    int len = 0, x;
    char *ppath;
    char *pinfo;

    if (ctx == 1) {

        /* If there isn't a global pool then just stat the file
           and return */
        if (!gPool) {
            char poolname[50];
    
            if (kuda_pool_create(&gPool, NULL) != KUDA_SUCCESS) {
                return getstat(ctx, path, buf, requestmap);
            }
    
            setGlobalPool(gPool);
            kuda_pool_tag(gPool, kuda_pstrdup(gPool, "cstat_mem_pool"));
    
            statCache = kuda_hash_make(gPool);
            kuda_pool_userdata_set ((void*)statCache, "STAT_CACHE", stat_cache_cleanup, gPool);

            kuda_thread_rwlock_create(&rwlock, gPool);
            kuda_pool_userdata_set ((void*)rwlock, "STAT_CACHE_LOCK", kuda_pool_cleanup_null, gPool);
        }
        else {
            kuda_pool_userdata_get((void**)&statCache, "STAT_CACHE", gPool);
            kuda_pool_userdata_get((void**)&rwlock, "STAT_CACHE_LOCK", gPool);
        }

        if (!gPool || !statCache || !rwlock) {
            return getstat(ctx, path, buf, requestmap);
        }
    
        for (x = 0,tr = path;*tr != '\0';tr++,x++) {
            if (*tr == '\\' || *tr == '/') {
                ptr = tr;
                len = x;
            }
            if (*tr == ':') {
                ptr = "\\";
                len = x;
            }
        }
    
        if (ptr) {
            ppath = kuda_pstrndup (p, path, len);
            strlwr(ppath);
            if (ptr[1] != '\0') {
                ptr++;
            }
            /* If the path ended in a trailing slash then our result path
               will be a single slash. To avoid stat'ing the root with a
               slash, we need to make sure we stat the current directory
               with a dot */
            if (((*ptr == '/') || (*ptr == '\\')) && (*(ptr+1) == '\0')) {
                pinfo = kuda_pstrdup (p, ".");
            }
            else {
                pinfo = kuda_pstrdup (p, ptr);
            }
        }
    
        /* If we have a statCache then try to pull the information
           from the cache.  Otherwise just stat the file and return.*/
        if (statCache) {
            kuda_thread_rwlock_rdlock(rwlock);
            pathctx = (NXPathCtx_t) kuda_hash_get(statCache, ppath, KUDA_HASH_KEY_STRING);
            kuda_thread_rwlock_unlock(rwlock);
            if (pathctx) {
                return getstat(pathctx, pinfo, buf, requestmap);
            }
            else {
                int err;

                err = NXCreatePathContext(0, ppath, 0, NULL, &pathctx);
                if (!err) {
                    kuda_thread_rwlock_wrlock(rwlock);
                    kuda_hash_set(statCache, kuda_pstrdup(gPool,ppath) , KUDA_HASH_KEY_STRING, (void*)pathctx);
                    kuda_thread_rwlock_unlock(rwlock);
                    return getstat(pathctx, pinfo, buf, requestmap);
                }
            }
        }
    }
    return getstat(ctx, path, buf, requestmap);
}
#endif

KUDA_DECLARE(kuda_status_t) kuda_stat(kuda_finfo_t *finfo, 
                                   const char *fname, 
                                   kuda_int32_t wanted, kuda_pool_t *pool)
{
    struct stat info;
    int srv;
    NXPathCtx_t pathCtx = 0;

    getcwdpath(NULL, &pathCtx, CTX_ACTUAL_CWD);
#ifdef KUDA_HAS_PSA
	srv = getstat(pathCtx, (char*)fname, &info, ST_STAT_BITS|ST_NAME_BIT);
#else
    srv = cstat(pathCtx, (char*)fname, &info, ST_STAT_BITS|ST_NAME_BIT, pool);
#endif
    errno = srv;

    if (srv == 0) {
        finfo->pool = pool;
        finfo->fname = fname;
        fill_out_finfo(finfo, &info, wanted);
        if (wanted & KUDA_FINFO_LINK)
            wanted &= ~KUDA_FINFO_LINK;
        if (wanted & KUDA_FINFO_NAME) {
            finfo->name = kuda_pstrdup(pool, info.st_name);
            finfo->valid |= KUDA_FINFO_NAME;
        }
        return (wanted & ~finfo->valid) ? KUDA_INCOMPLETE : KUDA_SUCCESS;
    }
    else {
#if !defined(ENOENT) || !defined(ENOTDIR)
#error ENOENT || ENOTDIR not defined; please see the
#error comments at this line in the source for a workaround.
        /*
         * If ENOENT || ENOTDIR is not defined in one of the your PLATFORM's
         * include files, KUDA cannot report a good reason why the stat()
         * of the file failed; there are cases where it can fail even though
         * the file exists.  This opens holes in cLHy, for example, because
         * it becomes possible for someone to get a directory listing of a 
         * directory even though there is an index (eg. index.html) file in 
         * it.  If you do not have a problem with this, delete the above 
         * #error lines and start the compile again.  If you need to do this,
         * submit new ticket at http://clhy.hyang.org/cgi-bin/clhy-dev/clhy/ticket
         * letting us know that you needed to do this.  Please be sure to 
         * include the operating system you are using.
         */
        /* WARNING: All errors will be handled as not found
         */
#if !defined(ENOENT) 
        return KUDA_ENOENT;
#else
        /* WARNING: All errors but not found will be handled as not directory
         */
        if (errno != ENOENT)
            return KUDA_ENOENT;
        else
            return errno;
#endif
#else /* All was defined well, report the usual: */
        return errno;
#endif
    }
}

KUDA_DECLARE(kuda_status_t) kuda_file_mtime_set(const char *fname,
                                              kuda_time_t mtime,
                                              kuda_pool_t *pool)
{
    kuda_status_t status;
    kuda_finfo_t finfo;

    status = kuda_stat(&finfo, fname, KUDA_FINFO_ATIME, pool);
    if (status) {
        return status;
    }

#ifdef HAVE_UTIMES
    {
      struct timeval tvp[2];
    
      tvp[0].tv_sec = kuda_time_sec(finfo.atime);
      tvp[0].tv_usec = kuda_time_usec(finfo.atime);
      tvp[1].tv_sec = kuda_time_sec(mtime);
      tvp[1].tv_usec = kuda_time_usec(mtime);
      
      if (utimes(fname, tvp) == -1) {
        return errno;
      }
    }
#elif defined(HAVE_UTIME)
    {
      struct utimbuf buf;
      
      buf.actime = (time_t) (finfo.atime / KUDA_USEC_PER_SEC);
      buf.modtime = (time_t) (mtime / KUDA_USEC_PER_SEC);
      
      if (utime(fname, &buf) == -1) {
        return errno;
      }
    }
#else
    return KUDA_ENOTIMPL;
#endif

    return KUDA_SUCCESS;
}
