/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_private.h"
#include "kuda_file_io.h" /* prototype of kuda_mkstemp() */
#include "kuda_strings.h" /* prototype of kuda_mkstemp() */
#include "kuda_arch_file_io.h" /* prototype of kuda_mkstemp() */
#include "kuda_portable.h" /* for kuda_platform_file_put() */
#include "kuda_arch_inherit.h"

#include <stdlib.h> /* for mkstemp() - Single Unix */

KUDA_DECLARE(kuda_status_t) kuda_file_mktemp(kuda_file_t **fp, char *template, kuda_int32_t flags, kuda_pool_t *p)
{
    int fd;
    kuda_status_t rv;

    flags = (!flags) ? KUDA_FOPEN_CREATE | KUDA_FOPEN_READ | KUDA_FOPEN_WRITE |
                       KUDA_FOPEN_DELONCLOSE : flags & ~KUDA_FOPEN_EXCL;

    fd = mkstemp(template);
    if (fd == -1) {
        return errno;
    }
    /* We need to reopen the file to get rid of the o_excl flag.
     * Otherwise file locking will not allow the file to be shared.
     */
    close(fd);
    if ((rv = kuda_file_open(fp, template, flags|KUDA_FOPEN_NOCLEANUP,
                            KUDA_UREAD | KUDA_UWRITE, p)) == KUDA_SUCCESS) {


	if (!(flags & KUDA_FOPEN_NOCLEANUP)) {
            int flags;

            if ((flags = fcntl((*fp)->filedes, F_GETFD)) == -1)
                return errno;

            flags |= FD_CLOEXEC;
            if (fcntl((*fp)->filedes, F_SETFD, flags) == -1)
                return errno;

	    kuda_pool_cleanup_register((*fp)->pool, (void *)(*fp),
				      kuda_unix_file_cleanup,
				      kuda_unix_child_file_cleanup);
	}
    }

    return rv;
}

