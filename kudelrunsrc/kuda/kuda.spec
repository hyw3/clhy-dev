
%define kudaver 1

Summary: Kuda Runtime Library
Name: kuda
Version: 1.7.3
Release: 1
License: GNU GPL version 3 or later
Group: System Environment/Libraries
URL: http://clhy.hyang.org/archives/kuda/
Source0: http://clhy.hyang.org/archives/kuda/%{name}-%{version}.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildRequires: autoconf, libtool, doxygen, libuuid-devel, python

%description
The mission of the Kuda Runtime library (KUDA) is to provide a
free library of C data structures and routines, forming a system
portability layer to as many operating systems as possible,
including Unices, MS Win32, BeOS and OS2.

%package devel
Group: Development/Libraries
Summary: KUDA library development kit
Requires: kuda = %{version}

%description devel
This package provides the support files which can be used to 
build applications using the KUDA library.  The mission of the
Kuda Runtime library (KUDA) is to provide a free library of 
C data structures and routines.

%prep
%setup -q

%build
# regenerate configure script etc.
./makeconf
%configure \
        --prefix=/usr \
        --includedir=%{_includedir}/kuda-%{kudaver} \
        --with-installbuilddir=%{_libdir}/kuda/build-%{kudaver} \
        --with-devrandom=/dev/urandom \
        CC=gcc CXX=g++
make %{?_smp_mflags} && make dox

%check
# Run non-interactive tests
pushd test
make %{?_smp_mflags} all CFLAGS=-fno-strict-aliasing
make check || exit 1
popd

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

# Move docs to more convenient location
mv docs/dox/html html

# Unpackaged files:
rm -f $RPM_BUILD_ROOT%{_libdir}/kuda.exp

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc hyscmterm.id LICENSE NOTICE
%{_libdir}/libkuda-%{kudaver}.so.*

%files devel
%defattr(-,root,root,-)
%doc docs/KUDADesign.html docs/canonical_filenames.html
%doc docs/incomplete_types docs/non_kuda_programs
%doc html
%{_bindir}/kuda*config
%{_libdir}/libkuda-%{kudaver}.*a
%{_libdir}/libkuda-%{kudaver}.so
%dir %{_libdir}/kuda
%dir %{_libdir}/kuda/build-%{kudaver}
%{_libdir}/kuda/build-%{kudaver}/*
%{_libdir}/pkgconfig/kuda-%{kudaver}.pc
%dir %{_includedir}/kuda-%{kudaver}
%{_includedir}/kuda-%{kudaver}/*.h

%changelog
* October 11, 2019 :
- The archive type for Kuda will use bzip2.
- Update license to GNU GPL version 3

