/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*Read/Write locking implementation based on the MultiLock code from
 * Stephen Beaulieu <hippo@be.com>
 */
 
#include "kuda_arch_thread_mutex.h"
#include "kuda_strings.h"
#include "kuda_portable.h"

static kuda_status_t _thread_mutex_cleanup(void * data)
{
    kuda_thread_mutex_t *lock = (kuda_thread_mutex_t*)data;
    if (lock->LockCount != 0) {
        /* we're still locked... */
        while (atomic_add(&lock->LockCount , -1) > 1){
            /* OK we had more than one person waiting on the lock so 
             * the sem is also locked. Release it until we have no more
             * locks left.
             */
            release_sem (lock->Lock);
        }
    }
    delete_sem(lock->Lock);
    return KUDA_SUCCESS;
}    

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_create(kuda_thread_mutex_t **mutex,
                                                  unsigned int flags,
                                                  kuda_pool_t *pool)
{
    kuda_thread_mutex_t *new_m;
    kuda_status_t stat = KUDA_SUCCESS;
  
    new_m = (kuda_thread_mutex_t *)kuda_pcalloc(pool, sizeof(kuda_thread_mutex_t));
    if (new_m == NULL){
        return KUDA_ENOMEM;
    }
    
    if ((stat = create_sem(0, "KUDA_Lock")) < B_NO_ERROR) {
        _thread_mutex_cleanup(new_m);
        return stat;
    }
    new_m->LockCount = 0;
    new_m->Lock = stat;  
    new_m->pool  = pool;

    /* Optimal default is KUDA_THREAD_MUTEX_UNNESTED, 
     * no additional checks required for either flag.
     */
    new_m->nested = flags & KUDA_THREAD_MUTEX_NESTED;

    kuda_pool_cleanup_register(new_m->pool, (void *)new_m, _thread_mutex_cleanup,
                              kuda_pool_cleanup_null);

    (*mutex) = new_m;
    return KUDA_SUCCESS;
}

#if KUDA_HAS_CREATE_LOCKS_NP
KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_create_np(kuda_thread_mutex_t **mutex,
                                                   const char *fname,
                                                   kuda_lockmech_e_np mech,
                                                   kuda_pool_t *pool)
{
    return KUDA_ENOTIMPL;
}       
#endif
  
KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_lock(kuda_thread_mutex_t *mutex)
{
    int32 stat;
    thread_id me = find_thread(NULL);
    
    if (mutex->nested && mutex->owner == me) {
        mutex->owner_ref++;
        return KUDA_SUCCESS;
    }
    
    if (atomic_add(&mutex->LockCount, 1) > 0) {
        if ((stat = acquire_sem(mutex->Lock)) < B_NO_ERROR) {
            /* Oh dear, acquire_sem failed!!  */
            atomic_add(&mutex->LockCount, -1);
            return stat;
        }
    }

    mutex->owner = me;
    mutex->owner_ref = 1;
    
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_trylock(kuda_thread_mutex_t *mutex)
{
    int32 stat;
    thread_id me = find_thread(NULL);
    
    if (mutex->nested && mutex->owner == me) {
        mutex->owner_ref++;
        return KUDA_SUCCESS;
    }
    
    if (atomic_add(&mutex->LockCount, 1) > 0) {
        if ((stat = acquire_sem_etc(mutex->Lock, 1, 0, 0)) < B_NO_ERROR) {
            atomic_add(&mutex->LockCount, -1);
            if (stat == B_WOULD_BLOCK) {
                stat = KUDA_EBUSY;
            }
            return stat;
        }
    }

    mutex->owner = me;
    mutex->owner_ref = 1;
    
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_timedlock(kuda_thread_mutex_t *mutex,
                                                 kuda_interval_time_t timeout)
{
    int32 stat;
    thread_id me = find_thread(NULL);
    
    if (mutex->nested && mutex->owner == me) {
        mutex->owner_ref++;
        return KUDA_SUCCESS;
    }
    
    if (atomic_add(&mutex->LockCount, 1) > 0) {
        if (timeout <= 0) {
            stat = B_TIMED_OUT;
        }
        else {
            stat = acquire_sem_etc(mutex->Lock, 1, B_RELATIVE_TIMEOUT,
                                   timeout);
        }
        if (stat < B_NO_ERROR) {
            atomic_add(&mutex->LockCount, -1);
            if (stat == B_TIMED_OUT) {
                stat = KUDA_TIMEUP;
            }
            return stat;
        }
    }

    mutex->owner = me;
    mutex->owner_ref = 1;
    
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_unlock(kuda_thread_mutex_t *mutex)
{
    int32 stat;
        
    if (mutex->nested && mutex->owner == find_thread(NULL)) {
        mutex->owner_ref--;
        if (mutex->owner_ref > 0)
            return KUDA_SUCCESS;
    }
    
    if (atomic_add(&mutex->LockCount, -1) > 1) {
        if ((stat = release_sem(mutex->Lock)) < B_NO_ERROR) {
            atomic_add(&mutex->LockCount, 1);
            return stat;
        }
    }

    mutex->owner = -1;
    mutex->owner_ref = 0;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_destroy(kuda_thread_mutex_t *mutex)
{
    kuda_status_t stat;
    if ((stat = _thread_mutex_cleanup(mutex)) == KUDA_SUCCESS) {
        kuda_pool_cleanup_kill(mutex->pool, mutex, _thread_mutex_cleanup);
        return KUDA_SUCCESS;
    }
    return stat;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread_mutex)

