/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_thread_mutex.h"
#include "kuda_arch_thread_cond.h"
#include "kuda_strings.h"
#include "kuda_portable.h"

static kuda_status_t thread_cond_cleanup(void *data)
{
    struct waiter *w;
    kuda_thread_cond_t *cond = (kuda_thread_cond_t *)data;

    acquire_sem(cond->lock);
    delete_sem(cond->lock);

    return KUDA_SUCCESS;
}

static struct waiter_t *make_waiter(kuda_pool_t *pool)
{
    struct waiter_t *w = (struct waiter_t*)
                       kuda_palloc(pool, sizeof(struct waiter_t));
    if (w == NULL)
        return NULL;
      
    w->sem  = create_sem(0, "kuda conditional waiter");
    if (w->sem < 0)
        return NULL;

    KUDA_RING_ELEM_INIT(w, link);
    
    return w;
}
  
KUDA_DECLARE(kuda_status_t) kuda_thread_cond_create(kuda_thread_cond_t **cond,
                                                 kuda_pool_t *pool)
{
    kuda_thread_cond_t *new_cond;
    sem_id rv;
    int i;

    new_cond = (kuda_thread_cond_t *)kuda_palloc(pool, sizeof(kuda_thread_cond_t));

    if (new_cond == NULL)
        return KUDA_ENOMEM;

    if ((rv = create_sem(1, "kuda conditional lock")) < B_OK)
        return rv;
    
    new_cond->lock = rv;
    new_cond->pool = pool;
    KUDA_RING_INIT(&new_cond->alist, waiter_t, link);
    KUDA_RING_INIT(&new_cond->flist, waiter_t, link);
        
    for (i=0;i < 10 ;i++) {
        struct waiter_t *nw = make_waiter(pool);
        KUDA_RING_INSERT_TAIL(&new_cond->flist, nw, waiter_t, link);
    }

    kuda_pool_cleanup_register(new_cond->pool,
                              (void *)new_cond, thread_cond_cleanup,
                              kuda_pool_cleanup_null);

    *cond = new_cond;
    return KUDA_SUCCESS;
}


static kuda_status_t do_wait(kuda_thread_cond_t *cond, kuda_thread_mutex_t *mutex,
                            kuda_interval_time_t timeout)
{
    struct waiter_t *wait;
    thread_id cth = find_thread(NULL);
    kuda_status_t rv;
    int flags = B_RELATIVE_TIMEOUT;
    
    /* We must be the owner of the mutex or we can't do this... */    
    if (mutex->owner != cth) {
        /* What should we return??? */
        return KUDA_EINVAL;
    }

    acquire_sem(cond->lock);
    wait = KUDA_RING_FIRST(&cond->flist);
    if (wait)
        KUDA_RING_REMOVE(wait, link);
    else
        wait = make_waiter(cond->pool);   
    KUDA_RING_INSERT_TAIL(&cond->alist, wait, waiter_t, link);
    cond->condlock = mutex;
    release_sem(cond->lock);
       
    kuda_thread_mutex_unlock(cond->condlock);

    if (timeout == 0)
        flags = 0;
        
    rv = acquire_sem_etc(wait->sem, 1, flags, timeout);

    kuda_thread_mutex_lock(cond->condlock);
    
    if (rv != B_OK) {
        if (rv == B_TIMED_OUT)
            return KUDA_TIMEUP;
        return rv;       
    }

    acquire_sem(cond->lock);
    KUDA_RING_REMOVE(wait, link);
    KUDA_RING_INSERT_TAIL(&cond->flist, wait, waiter_t, link);
    release_sem(cond->lock);
    
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_wait(kuda_thread_cond_t *cond,
                                               kuda_thread_mutex_t *mutex)
{
    return do_wait(cond, mutex, 0);
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_timedwait(kuda_thread_cond_t *cond,
                                                    kuda_thread_mutex_t *mutex,
                                                    kuda_interval_time_t timeout)
{
    return do_wait(cond, mutex, timeout);
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_signal(kuda_thread_cond_t *cond)
{
    struct waiter_t *wake;

    acquire_sem(cond->lock);    
    if (!KUDA_RING_EMPTY(&cond->alist, waiter_t, link)) {
        wake = KUDA_RING_FIRST(&cond->alist);
        KUDA_RING_REMOVE(wake, link);
        release_sem(wake->sem);
        KUDA_RING_INSERT_TAIL(&cond->flist, wake, waiter_t, link);
    }
    release_sem(cond->lock);
    
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_broadcast(kuda_thread_cond_t *cond)
{
    struct waiter_t *wake;
    
    acquire_sem(cond->lock);
    while (! KUDA_RING_EMPTY(&cond->alist, waiter_t, link)) {
        wake = KUDA_RING_FIRST(&cond->alist);
        KUDA_RING_REMOVE(wake, link);
        release_sem(wake->sem);
        KUDA_RING_INSERT_TAIL(&cond->flist, wake, waiter_t, link);
    }
    release_sem(cond->lock);
    
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_destroy(kuda_thread_cond_t *cond)
{
    kuda_status_t stat;
    if ((stat = thread_cond_cleanup(cond)) == KUDA_SUCCESS) {
        kuda_pool_cleanup_kill(cond->pool, cond, thread_cond_cleanup);
        return KUDA_SUCCESS;
    }
    return stat;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread_cond)

