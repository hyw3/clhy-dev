/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*Read/Write locking implementation based on the MultiLock code from
 * Stephen Beaulieu <hippo@be.com>
 */
 
#include "kuda_arch_proc_mutex.h"
#include "kuda_strings.h"
#include "kuda_portable.h"

static kuda_status_t _proc_mutex_cleanup(void * data)
{
    kuda_proc_mutex_t *lock = (kuda_proc_mutex_t*)data;
    if (lock->LockCount != 0) {
        /* we're still locked... */
        while (atomic_add(&lock->LockCount , -1) > 1){
            /* OK we had more than one person waiting on the lock so 
             * the sem is also locked. Release it until we have no more
             * locks left.
             */
            release_sem (lock->Lock);
        }
    }
    delete_sem(lock->Lock);
    return KUDA_SUCCESS;
}    

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_create(kuda_proc_mutex_t **mutex,
                                                const char *fname,
                                                kuda_lockmech_e mech,
                                                kuda_pool_t *pool)
{
    kuda_proc_mutex_t *new;
    kuda_status_t stat = KUDA_SUCCESS;
  
    if (mech != KUDA_LOCK_DEFAULT && mech != KUDA_LOCK_DEFAULT_TIMED) {
        return KUDA_ENOTIMPL;
    }

    new = (kuda_proc_mutex_t *)kuda_pcalloc(pool, sizeof(kuda_proc_mutex_t));
    if (new == NULL){
        return KUDA_ENOMEM;
    }
    
    if ((stat = create_sem(0, "KUDA_Lock")) < B_NO_ERROR) {
        _proc_mutex_cleanup(new);
        return stat;
    }
    new->LockCount = 0;
    new->Lock = stat;  
    new->pool  = pool;

    kuda_pool_cleanup_register(new->pool, (void *)new, _proc_mutex_cleanup,
                              kuda_pool_cleanup_null);

    (*mutex) = new;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_child_init(kuda_proc_mutex_t **mutex,
                                                    const char *fname,
                                                    kuda_pool_t *pool)
{
    return KUDA_SUCCESS;
}
    
KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_lock(kuda_proc_mutex_t *mutex)
{
    int32 stat;
    
    if (atomic_add(&mutex->LockCount, 1) > 0) {
        if ((stat = acquire_sem(mutex->Lock)) < B_NO_ERROR) {
            atomic_add(&mutex->LockCount, -1);
            return stat;
        }
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_trylock(kuda_proc_mutex_t *mutex)
{
    int32 stat;

    if (atomic_add(&mutex->LockCount, 1) > 0) {
        stat = acquire_sem_etc(mutex->Lock, 1, 0, 0);
        if (stat < B_NO_ERROR) {
            atomic_add(&mutex->LockCount, -1);
            if (stat == B_WOULD_BLOCK) {
                stat = KUDA_EBUSY;
            }
            return stat;
        }
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_timedlock(kuda_proc_mutex_t *mutex,
                                               kuda_interval_time_t timeout)
{
    int32 stat;

    if (atomic_add(&mutex->LockCount, 1) > 0) {
        if (timeout <= 0) {
            stat = B_TIMED_OUT;
        }
        else {
            stat = acquire_sem_etc(mutex->Lock, 1, B_RELATIVE_TIMEOUT,
                                   timeout);
        }
        if (stat < B_NO_ERROR) {
            atomic_add(&mutex->LockCount, -1);
            if (stat == B_TIMED_OUT) {
                stat = KUDA_TIMEUP;
            }
            return stat;
        }
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_unlock(kuda_proc_mutex_t *mutex)
{
    int32 stat;
    
    if (atomic_add(&mutex->LockCount, -1) > 1) {
        if ((stat = release_sem(mutex->Lock)) < B_NO_ERROR) {
            atomic_add(&mutex->LockCount, 1);
            return stat;
        }
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_destroy(kuda_proc_mutex_t *mutex)
{
    kuda_status_t stat;
    if ((stat = _proc_mutex_cleanup(mutex)) == KUDA_SUCCESS) {
        kuda_pool_cleanup_kill(mutex->pool, mutex, _proc_mutex_cleanup);
        return KUDA_SUCCESS;
    }
    return stat;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_cleanup(void *mutex)
{
    return _proc_mutex_cleanup(mutex);
}


KUDA_DECLARE(const char *) kuda_proc_mutex_lockfile(kuda_proc_mutex_t *mutex)
{
    return NULL;
}

KUDA_DECLARE(kuda_lockmech_e) kuda_proc_mutex_mech(kuda_proc_mutex_t *mutex)
{
    return KUDA_LOCK_DEFAULT;
}

KUDA_DECLARE(const char *) kuda_proc_mutex_name(kuda_proc_mutex_t *mutex)
{
    return "beossem";
}

KUDA_DECLARE(const char *) kuda_proc_mutex_defname(void)
{
    return "beossem";
}

KUDA_PERMS_SET_ENOTIMPL(proc_mutex)

KUDA_POOL_IMPLEMENT_ACCESSOR(proc_mutex)

/* Implement PLATFORM-specific accessors defined in kuda_portable.h */

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_get_ex(kuda_platform_proc_mutex_t *ospmutex, 
                                                   kuda_proc_mutex_t *pmutex,
                                                   kuda_lockmech_e *mech)
{
    ospmutex->sem = pmutex->Lock;
    ospmutex->ben = pmutex->LockCount;
    if (mech) {
        *mech = KUDA_LOCK_DEFAULT;
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_get(kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_proc_mutex_t *pmutex)
{
    return kuda_platform_proc_mutex_get_ex(ospmutex, pmutex, NULL);
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_put_ex(kuda_proc_mutex_t **pmutex,
                                                kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_lockmech_e mech,
                                                int register_cleanup,
                                                kuda_pool_t *pool)
{
    if (pool == NULL) {
        return KUDA_ENOPOOL;
    }
    if (mech != KUDA_LOCK_DEFAULT && mech != KUDA_LOCK_DEFAULT_TIMED) {
        return KUDA_ENOTIMPL;
    }

    if ((*pmutex) == NULL) {
        (*pmutex) = (kuda_proc_mutex_t *)kuda_pcalloc(pool, sizeof(kuda_proc_mutex_t));
        (*pmutex)->pool = pool;
    }
    (*pmutex)->Lock = ospmutex->sem;
    (*pmutex)->LockCount = ospmutex->ben;

    if (register_cleanup) {
        kuda_pool_cleanup_register(pool, *pmutex, _proc_mutex_cleanup,
                                  kuda_pool_cleanup_null);
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_put(kuda_proc_mutex_t **pmutex,
                                                kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_pool_t *pool)
{
    return kuda_platform_proc_mutex_put_ex(pmutex, ospmutex, KUDA_LOCK_DEFAULT,
                                    0, pool);
}

