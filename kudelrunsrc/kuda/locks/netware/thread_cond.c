/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <nks/errno.h>

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_arch_thread_mutex.h"
#include "kuda_arch_thread_cond.h"
#include "kuda_portable.h"

static kuda_status_t thread_cond_cleanup(void *data)
{
    kuda_thread_cond_t *cond = (kuda_thread_cond_t *)data;

    NXCondFree(cond->cond);        
    return KUDA_SUCCESS;
} 

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_create(kuda_thread_cond_t **cond,
                                                 kuda_pool_t *pool)
{
    kuda_thread_cond_t *new_cond = NULL;

    new_cond = (kuda_thread_cond_t *)kuda_pcalloc(pool, sizeof(kuda_thread_cond_t));
	
	if(new_cond ==NULL) {
        return KUDA_ENOMEM;
    }     
    new_cond->pool = pool;

    new_cond->cond = NXCondAlloc(NULL);
    
    if(new_cond->cond == NULL)
        return KUDA_ENOMEM;

    kuda_pool_cleanup_register(new_cond->pool, new_cond, 
                                (void*)thread_cond_cleanup,
                                kuda_pool_cleanup_null);
   *cond = new_cond;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_wait(kuda_thread_cond_t *cond,
                                               kuda_thread_mutex_t *mutex)
{
    if (NXCondWait(cond->cond, mutex->mutex) != 0)
        return KUDA_EINTR;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_timedwait(kuda_thread_cond_t *cond,
                                                    kuda_thread_mutex_t *mutex,
                                                    kuda_interval_time_t timeout)
{
    int rc;
    if (timeout < 0) {
        rc = NXCondWait(cond->cond, mutex->mutex);
    }
    else {
        timeout = timeout * 1000 / NXGetSystemTick();
        rc = NXCondTimedWait(cond->cond, mutex->mutex, timeout);
        if (rc == NX_ETIMEDOUT) {
            return KUDA_TIMEUP;
        }
    }
    if (rc != 0) {
        return KUDA_EINTR;
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_signal(kuda_thread_cond_t *cond)
{
    NXCondSignal(cond->cond);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_broadcast(kuda_thread_cond_t *cond)
{
    NXCondBroadcast(cond->cond);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_destroy(kuda_thread_cond_t *cond)
{
    kuda_status_t stat;
    if ((stat = thread_cond_cleanup(cond)) == KUDA_SUCCESS) {
        kuda_pool_cleanup_kill(cond->pool, cond, thread_cond_cleanup);
        return KUDA_SUCCESS;
    }
    return stat;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread_cond)

