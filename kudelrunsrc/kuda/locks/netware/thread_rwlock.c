/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_arch_thread_rwlock.h"
#include "kuda_portable.h"

static kuda_status_t thread_rwlock_cleanup(void *data)
{
    kuda_thread_rwlock_t *rwlock = (kuda_thread_rwlock_t *)data;

    NXRwLockFree (rwlock->rwlock);
    return KUDA_SUCCESS;
} 

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_create(kuda_thread_rwlock_t **rwlock,
                                                   kuda_pool_t *pool)
{
    kuda_thread_rwlock_t *new_rwlock = NULL;
   
    NXHierarchy_t hierarchy = 1;   /* for libc NKS NXRwLockAlloc */
    NXLockInfo_t *info;            /* for libc NKS NXRwLockAlloc */

    new_rwlock = (kuda_thread_rwlock_t *)kuda_pcalloc(pool, sizeof(kuda_thread_rwlock_t));

    if(new_rwlock ==NULL) {
        return KUDA_ENOMEM;
    }     
    new_rwlock->pool = pool;
    
    info = (NXLockInfo_t *)kuda_pcalloc(pool, sizeof(NXLockInfo_t));
    new_rwlock->rwlock = NXRwLockAlloc(hierarchy, info);
    if(new_rwlock->rwlock == NULL)
        return KUDA_ENOMEM;

    kuda_pool_cleanup_register(new_rwlock->pool, new_rwlock, thread_rwlock_cleanup,
                              kuda_pool_cleanup_null);
    *rwlock = new_rwlock;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_rdlock(kuda_thread_rwlock_t *rwlock)
{
    NXRdLock(rwlock->rwlock);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_tryrdlock(kuda_thread_rwlock_t *rwlock)
{
    if (!NXTryRdLock(rwlock->rwlock))
        return KUDA_EBUSY;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_wrlock(kuda_thread_rwlock_t *rwlock)
{
    NXWrLock(rwlock->rwlock);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_trywrlock(kuda_thread_rwlock_t *rwlock)
{
    if (!NXTryWrLock(rwlock->rwlock))
        return KUDA_EBUSY;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_unlock(kuda_thread_rwlock_t *rwlock)
{
    NXRwUnlock(rwlock->rwlock);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_destroy(kuda_thread_rwlock_t *rwlock)
{
    kuda_status_t stat;
    if ((stat = thread_rwlock_cleanup(rwlock)) == KUDA_SUCCESS) {
        kuda_pool_cleanup_kill(rwlock->pool, rwlock, thread_rwlock_cleanup);
        return KUDA_SUCCESS;
    }
    return stat;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread_rwlock)

