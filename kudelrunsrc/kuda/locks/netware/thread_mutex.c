/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_arch_thread_mutex.h"
#include "kuda_thread_cond.h"
#include "kuda_portable.h"

static kuda_status_t thread_mutex_cleanup(void *data)
{
    kuda_thread_mutex_t *mutex = (kuda_thread_mutex_t *)data;

    NXMutexFree(mutex->mutex);        
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_create(kuda_thread_mutex_t **mutex,
                                                  unsigned int flags,
                                                  kuda_pool_t *pool)
{
    kuda_thread_mutex_t *new_mutex = NULL;

    /* XXX: Implement _UNNESTED flavor and favor _DEFAULT for performance
     */
    if (flags & KUDA_THREAD_MUTEX_UNNESTED) {
        return KUDA_ENOTIMPL;
    }
    new_mutex = (kuda_thread_mutex_t *)kuda_pcalloc(pool, sizeof(kuda_thread_mutex_t));

    if (new_mutex == NULL) {
        return KUDA_ENOMEM;
    }     
    new_mutex->pool = pool;

    new_mutex->mutex = NXMutexAlloc(NX_MUTEX_RECURSIVE, 0, NULL);
    
    if(new_mutex->mutex == NULL)
        return KUDA_ENOMEM;

    if (flags & KUDA_THREAD_MUTEX_TIMED) {
        kuda_status_t rv = kuda_thread_cond_create(&new_mutex->cond, pool);
        if (rv != KUDA_SUCCESS) {
            NXMutexFree(new_mutex->mutex);        
            return rv;
        }
    }

    kuda_pool_cleanup_register(new_mutex->pool, new_mutex, 
                                (void*)thread_mutex_cleanup,
                                kuda_pool_cleanup_null);
   *mutex = new_mutex;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_lock(kuda_thread_mutex_t *mutex)
{
    if (mutex->cond) {
        kuda_status_t rv;
        NXLock(mutex->mutex);
        if (mutex->locked) {
            mutex->num_waiters++;
            rv = kuda_thread_cond_wait(mutex->cond, mutex);
            mutex->num_waiters--;
        }
        else {
            mutex->locked = 1;
            rv = KUDA_SUCCESS;
        }
        NXUnlock(mutex->mutex);
        return rv;
    }

    NXLock(mutex->mutex);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_trylock(kuda_thread_mutex_t *mutex)
{
    if (mutex->cond) {
        kuda_status_t rv;
        NXLock(mutex->mutex);
        if (mutex->locked) {
            rv = KUDA_EBUSY;
        }
        else {
            mutex->locked = 1;
            rv = KUDA_SUCCESS;
        }
        NXUnlock(mutex->mutex);
        return rv;
    }

    if (!NXTryLock(mutex->mutex))
        return KUDA_EBUSY;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_timedlock(kuda_thread_mutex_t *mutex,
                                                 kuda_interval_time_t timeout)
{
    if (mutex->cond) {
        kuda_status_t rv = KUDA_SUCCESS;

        NXLock(mutex->mutex);
        if (mutex->locked) {
            if (timeout <= 0) {
                rv = KUDA_TIMEUP;
            }
            else {
                mutex->num_waiters++;
                do {
                    rv = kuda_thread_cond_timedwait(mutex->cond, mutex,
                                                   timeout);
                } while (rv == KUDA_SUCCESS && mutex->locked);
                mutex->num_waiters--;
            }
        }
        if (rv == KUDA_SUCCESS) {
            mutex->locked = 1;
        }
        NXUnlock(mutex->mutex);
        return rv;
    }

    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_unlock(kuda_thread_mutex_t *mutex)
{
    kuda_status_t rv = KUDA_SUCCESS;

    if (mutex->cond) {
        NXLock(mutex->mutex);

        if (!mutex->locked) {
            rv = KUDA_EINVAL;
        }
        else if (mutex->num_waiters) {
            rv = kuda_thread_cond_signal(mutex->cond);
        }
        if (rv == KUDA_SUCCESS) {
            mutex->locked = 0;
        }
    }

    NXUnlock(mutex->mutex);
    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_destroy(kuda_thread_mutex_t *mutex)
{
    kuda_status_t stat, rv = KUDA_SUCCESS;
    if (mutex->cond) {
        rv = kuda_thread_cond_destroy(mutex->cond);
        mutex->cond = NULL;
    }
    stat = kuda_pool_cleanup_run(mutex->pool, mutex, thread_mutex_cleanup);
    if (stat == KUDA_SUCCESS && rv) {
        stat = rv;
    }
    return stat;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread_mutex)

