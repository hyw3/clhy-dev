/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_portable.h"
#include "kuda_arch_proc_mutex.h"
#include "kuda_arch_thread_mutex.h"

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_create(kuda_proc_mutex_t **mutex,
                                                const char *fname,
                                                kuda_lockmech_e mech,
                                                kuda_pool_t *pool)
{
    kuda_status_t ret;
    kuda_proc_mutex_t *new_mutex;
    unsigned int flags = KUDA_THREAD_MUTEX_DEFAULT;

    *mutex = NULL;
    if (mech == KUDA_LOCK_DEFAULT_TIMED) {
        flags |= KUDA_THREAD_MUTEX_TIMED;
    }
    else if (mech != KUDA_LOCK_DEFAULT) {
        return KUDA_ENOTIMPL;
    }

    new_mutex = (kuda_proc_mutex_t *)kuda_pcalloc(pool, sizeof(kuda_proc_mutex_t));
    if (new_mutex == NULL) {
        return KUDA_ENOMEM;
    }     
    
    new_mutex->pool = pool;
    ret = kuda_thread_mutex_create(&(new_mutex->mutex), flags, pool);

    if (ret == KUDA_SUCCESS)
        *mutex = new_mutex;

    return ret;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_child_init(kuda_proc_mutex_t **mutex,
                                                    const char *fname,
                                                    kuda_pool_t *pool)
{
    return KUDA_SUCCESS;
}
    
KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_lock(kuda_proc_mutex_t *mutex)
{
    if (mutex)
        return kuda_thread_mutex_lock(mutex->mutex);
    return KUDA_ENOLOCK;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_trylock(kuda_proc_mutex_t *mutex)
{
    if (mutex)
        return kuda_thread_mutex_trylock(mutex->mutex);
    return KUDA_ENOLOCK;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_timedlock(kuda_proc_mutex_t *mutex,
                                               kuda_interval_time_t timeout)
{
    if (mutex)
        return kuda_thread_mutex_timedlock(mutex->mutex, timeout);
    return KUDA_ENOLOCK;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_unlock(kuda_proc_mutex_t *mutex)
{
    if (mutex)
        return kuda_thread_mutex_unlock(mutex->mutex);
    return KUDA_ENOLOCK;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_cleanup(void *mutex)
{
    return kuda_proc_mutex_destroy(mutex);
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_destroy(kuda_proc_mutex_t *mutex)
{
    if (mutex)
        return kuda_thread_mutex_destroy(mutex->mutex);
    return KUDA_ENOLOCK;
}

KUDA_DECLARE(const char *) kuda_proc_mutex_lockfile(kuda_proc_mutex_t *mutex)
{
    return NULL;
}

KUDA_DECLARE(kuda_lockmech_e) kuda_proc_mutex_mech(kuda_proc_mutex_t *mutex)
{
    return KUDA_LOCK_DEFAULT;
}

KUDA_DECLARE(const char *) kuda_proc_mutex_name(kuda_proc_mutex_t *mutex)
{
    return "netwarethread";
}

KUDA_DECLARE(const char *) kuda_proc_mutex_defname(void)
{
    return "netwarethread";
}

KUDA_PERMS_SET_ENOTIMPL(proc_mutex)

KUDA_POOL_IMPLEMENT_ACCESSOR(proc_mutex)

/* Implement PLATFORM-specific accessors defined in kuda_portable.h */

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_get_ex(kuda_platform_proc_mutex_t *ospmutex, 
                                                   kuda_proc_mutex_t *pmutex,
                                                   kuda_lockmech_e *mech)
{
    if (pmutex && pmutex->mutex)
        ospmutex = pmutex->mutex->mutex;
    return KUDA_ENOLOCK;
#if 0
    /* We need to change kuda_platform_proc_mutex_t to a pointer type
     * to be able to implement this function.
     */
    *ospmutex = pmutex->mutex->mutex;
    if (mech) {
        *mech = KUDA_LOCK_DEFAULT;
    }
    return KUDA_SUCCESS;
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_get(kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_proc_mutex_t *pmutex)
{
    return kuda_platform_proc_mutex_get_ex(ospmutex, pmutex, NULL);
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_put_ex(kuda_proc_mutex_t **pmutex,
                                                kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_lockmech_e mech,
                                                int register_cleanup,
                                                kuda_pool_t *pool)
{
    if (pool == NULL) {
        return KUDA_ENOPOOL;
    }
    if (mech != KUDA_LOCK_DEFAULT && mech != KUDA_LOCK_DEFAULT_TIMED) {
        return KUDA_ENOTIMPL;
    }
#if 0
    /* We need to change kuda_platform_proc_mutex_t to a pointer type
     * to be able to implement this function.
     */
    if ((*pmutex) == NULL) {
        (*pmutex) = kuda_pcalloc(pool, sizeof(kuda_proc_mutex_t));
        (*pmutex)->pool = pool;
    }
    (*pmutex)->mutex = kuda_pcalloc(pool, sizeof(kuda_thread_mutex_t));
    (*pmutex)->mutex->mutex = *ospmutex;
    (*pmutex)->mutex->pool = pool;

    if (register_cleanup) {
        kuda_pool_cleanup_register(pool, *pmutex, kuda_proc_mutex_cleanup,
                                  kuda_pool_cleanup_null);
    }
    return KUDA_SUCCESS;
#else
    return KUDA_ENOTIMPL;
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_put(kuda_proc_mutex_t **pmutex,
                                                kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_pool_t *pool)
{
    return kuda_platform_proc_mutex_put_ex(pmutex, ospmutex, KUDA_LOCK_DEFAULT,
                                    0, pool);
}

