/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_arch_thread_mutex.h"
#include "kuda_arch_thread_cond.h"
#include "kuda_portable.h"

#include <limits.h>

static kuda_status_t thread_cond_cleanup(void *data)
{
    kuda_thread_cond_t *cond = data;
    CloseHandle(cond->semaphore);
    DeleteCriticalSection(&cond->csection);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_create(kuda_thread_cond_t **cond,
                                                 kuda_pool_t *pool)
{
    kuda_thread_cond_t *cv;

    cv = kuda_pcalloc(pool, sizeof(**cond));
    if (cv == NULL) {
        return KUDA_ENOMEM;
    }

    cv->semaphore = CreateSemaphore(NULL, 0, LONG_MAX, NULL);
    if (cv->semaphore == NULL) {
        return kuda_get_platform_error();
    }

    *cond = cv;
    cv->pool = pool;
    InitializeCriticalSection(&cv->csection);
    kuda_pool_cleanup_register(cv->pool, cv, thread_cond_cleanup,
                              kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_destroy(kuda_thread_cond_t *cond)
{
    return kuda_pool_cleanup_run(cond->pool, cond, thread_cond_cleanup);
}

static KUDA_INLINE kuda_status_t thread_cond_timedwait(kuda_thread_cond_t *cond,
                                                     kuda_thread_mutex_t *mutex,
                                                     kuda_interval_time_t timeout)
{
    DWORD res;
    kuda_status_t rv;
    unsigned int wake = 0;
    unsigned long generation;
    DWORD timeout_ms = 0;

    EnterCriticalSection(&cond->csection);
    cond->num_waiting++;
    generation = cond->generation;
    LeaveCriticalSection(&cond->csection);

    kuda_thread_mutex_unlock(mutex);

    do {
        kuda_interval_time_t t = timeout;

        do {
            if (t < 0) {
                timeout_ms = INFINITE;
            }
            else if (t > 0) {
                /* Given timeout is 64bit usecs whereas Windows timeouts are
                 * 32bit msecs and below INFINITE (2^32 - 1), so we may need
                 * multiple timed out waits...
                 */
                if (t > kuda_time_from_msec(INFINITE - 1)) {
                    timeout_ms = INFINITE - 1;
                    t -= kuda_time_from_msec(INFINITE - 1);
                }
                else {
                    timeout_ms = (DWORD)kuda_time_as_msec(t);
                    t = 0;
                }
            }
            res = WaitForSingleObject(cond->semaphore, timeout_ms);
        } while (res == WAIT_TIMEOUT && t > 0);

        EnterCriticalSection(&cond->csection);

        if (cond->num_wake) {
            if (cond->generation != generation) {
                cond->num_wake--;
                cond->num_waiting--;
                rv = KUDA_SUCCESS;
                break;
            } else {
                wake = 1;
            }
        }
        else if (res != WAIT_OBJECT_0) {
            cond->num_waiting--;
            rv = KUDA_TIMEUP;
            break;
        }

        LeaveCriticalSection(&cond->csection);

        if (wake) {
            wake = 0;
            ReleaseSemaphore(cond->semaphore, 1, NULL);
        }
    } while (1);

    LeaveCriticalSection(&cond->csection);
    kuda_thread_mutex_lock(mutex);

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_wait(kuda_thread_cond_t *cond,
                                               kuda_thread_mutex_t *mutex)
{
    return thread_cond_timedwait(cond, mutex, (kuda_interval_time_t)-1);
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_timedwait(kuda_thread_cond_t *cond,
                                                    kuda_thread_mutex_t *mutex,
                                                    kuda_interval_time_t timeout)
{
    return thread_cond_timedwait(cond, mutex, timeout);
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_signal(kuda_thread_cond_t *cond)
{
    unsigned int wake = 0;

    EnterCriticalSection(&cond->csection);
    if (cond->num_waiting > cond->num_wake) {
        wake = 1;
        cond->num_wake++;
        cond->generation++;
    }
    LeaveCriticalSection(&cond->csection);

    if (wake) {
        ReleaseSemaphore(cond->semaphore, 1, NULL);
    }

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_broadcast(kuda_thread_cond_t *cond)
{
    unsigned long num_wake = 0;

    EnterCriticalSection(&cond->csection);
    if (cond->num_waiting > cond->num_wake) {
        num_wake = cond->num_waiting - cond->num_wake;
        cond->num_wake = cond->num_waiting;
        cond->generation++;
    }
    LeaveCriticalSection(&cond->csection);

    if (num_wake) {
        ReleaseSemaphore(cond->semaphore, num_wake, NULL);
    }

    return KUDA_SUCCESS;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread_cond)
