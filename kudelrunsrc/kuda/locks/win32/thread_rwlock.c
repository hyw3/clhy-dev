/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_arch_thread_rwlock.h"
#include "kuda_portable.h"

static kuda_status_t thread_rwlock_cleanup(void *data)
{
    kuda_thread_rwlock_t *rwlock = data;
    
    if (! CloseHandle(rwlock->read_event))
        return kuda_get_platform_error();

    if (! CloseHandle(rwlock->write_mutex))
        return kuda_get_platform_error();
    
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t)kuda_thread_rwlock_create(kuda_thread_rwlock_t **rwlock,
                                                  kuda_pool_t *pool)
{
    *rwlock = kuda_palloc(pool, sizeof(**rwlock));

    (*rwlock)->pool        = pool;
    (*rwlock)->readers     = 0;

    if (! ((*rwlock)->read_event = CreateEvent(NULL, TRUE, FALSE, NULL))) {
        *rwlock = NULL;
        return kuda_get_platform_error();
    }

    if (! ((*rwlock)->write_mutex = CreateMutex(NULL, FALSE, NULL))) {
        CloseHandle((*rwlock)->read_event);
        *rwlock = NULL;
        return kuda_get_platform_error();
    }

    kuda_pool_cleanup_register(pool, *rwlock, thread_rwlock_cleanup,
                              kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}

static kuda_status_t kuda_thread_rwlock_rdlock_core(kuda_thread_rwlock_t *rwlock,
                                                  DWORD  milliseconds)
{
    DWORD   code = WaitForSingleObject(rwlock->write_mutex, milliseconds);

    if (code == WAIT_FAILED || code == WAIT_TIMEOUT)
        return KUDA_FROM_PLATFORM_ERROR(code);

    /* We've successfully acquired the writer mutex, we can't be locked
     * for write, so it's OK to add the reader lock.  The writer mutex
     * doubles as race condition protection for the readers counter.   
     */
    InterlockedIncrement(&rwlock->readers);
    
    if (! ResetEvent(rwlock->read_event))
        return kuda_get_platform_error();
    
    if (! ReleaseMutex(rwlock->write_mutex))
        return kuda_get_platform_error();
    
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_rdlock(kuda_thread_rwlock_t *rwlock)
{
    return kuda_thread_rwlock_rdlock_core(rwlock, INFINITE);
}

KUDA_DECLARE(kuda_status_t) 
kuda_thread_rwlock_tryrdlock(kuda_thread_rwlock_t *rwlock)
{
    return kuda_thread_rwlock_rdlock_core(rwlock, 0);
}

static kuda_status_t 
kuda_thread_rwlock_wrlock_core(kuda_thread_rwlock_t *rwlock, DWORD milliseconds)
{
    DWORD   code = WaitForSingleObject(rwlock->write_mutex, milliseconds);

    if (code == WAIT_FAILED || code == WAIT_TIMEOUT)
        return KUDA_FROM_PLATFORM_ERROR(code);

    /* We've got the writer lock but we have to wait for all readers to
     * unlock before it's ok to use it.
     */
    if (rwlock->readers) {
        /* Must wait for readers to finish before returning, unless this
         * is an trywrlock (milliseconds == 0):
         */
        code = milliseconds
          ? WaitForSingleObject(rwlock->read_event, milliseconds)
          : WAIT_TIMEOUT;
        
        if (code == WAIT_FAILED || code == WAIT_TIMEOUT) {
            /* Unable to wait for readers to finish, release write lock: */
            if (! ReleaseMutex(rwlock->write_mutex))
                return kuda_get_platform_error();
            
            return KUDA_FROM_PLATFORM_ERROR(code);
        }
    }

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_wrlock(kuda_thread_rwlock_t *rwlock)
{
    return kuda_thread_rwlock_wrlock_core(rwlock, INFINITE);
}

KUDA_DECLARE(kuda_status_t)kuda_thread_rwlock_trywrlock(kuda_thread_rwlock_t *rwlock)
{
    return kuda_thread_rwlock_wrlock_core(rwlock, 0);
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_unlock(kuda_thread_rwlock_t *rwlock)
{
    kuda_status_t rv = 0;

    /* First, guess that we're unlocking a writer */
    if (! ReleaseMutex(rwlock->write_mutex))
        rv = kuda_get_platform_error();
    
    if (rv == KUDA_FROM_PLATFORM_ERROR(ERROR_NOT_OWNER)) {
        /* Nope, we must have a read lock */
        if (rwlock->readers &&
            ! InterlockedDecrement(&rwlock->readers) &&
            ! SetEvent(rwlock->read_event)) {
            rv = kuda_get_platform_error();
        }
        else {
            rv = 0;
        }
    }

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_destroy(kuda_thread_rwlock_t *rwlock)
{
    return kuda_pool_cleanup_run(rwlock->pool, rwlock, thread_rwlock_cleanup);
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread_rwlock)
