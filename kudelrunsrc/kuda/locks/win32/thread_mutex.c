/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_arch_thread_mutex.h"
#include "kuda_thread_mutex.h"
#include "kuda_portable.h"
#include "kuda_arch_misc.h"

static kuda_status_t thread_mutex_cleanup(void *data)
{
    kuda_thread_mutex_t *lock = data;

    if (lock->type == thread_mutex_critical_section) {
        lock->type = -1;
        DeleteCriticalSection(&lock->section);
    }
    else {
        if (!CloseHandle(lock->handle)) {
            return kuda_get_platform_error();
        }
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_create(kuda_thread_mutex_t **mutex,
                                                  unsigned int flags,
                                                  kuda_pool_t *pool)
{
    (*mutex) = (kuda_thread_mutex_t *)kuda_palloc(pool, sizeof(**mutex));

    (*mutex)->pool = pool;

    if (flags & KUDA_THREAD_MUTEX_UNNESTED) {
        /* Use an auto-reset signaled event, ready to accept one
         * waiting thread.
         */
        (*mutex)->type = thread_mutex_unnested_event;
        (*mutex)->handle = CreateEvent(NULL, FALSE, TRUE, NULL);
    }
    else if (flags & KUDA_THREAD_MUTEX_TIMED) {
        (*mutex)->type = thread_mutex_nested_mutex;
        (*mutex)->handle = CreateMutex(NULL, FALSE, NULL);
    }
    else {
#if KUDA_HAS_UNICODE_FS
        /* Critical Sections are terrific, performance-wise, on NT.
         * On Win9x, we cannot 'try' on a critical section, so we 
         * use a [slower] mutex object, instead.
         */
        IF_WIN_PLATFORM_IS_UNICODE {
            InitializeCriticalSection(&(*mutex)->section);
            (*mutex)->type = thread_mutex_critical_section;
            (*mutex)->handle = NULL;
        }
#endif
#if KUDA_HAS_ANSI_FS
        ELSE_WIN_PLATFORM_IS_ANSI {
            (*mutex)->type = thread_mutex_nested_mutex;
            (*mutex)->handle = CreateMutex(NULL, FALSE, NULL);

        }
#endif
    }

    kuda_pool_cleanup_register((*mutex)->pool, (*mutex), thread_mutex_cleanup,
                              kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_lock(kuda_thread_mutex_t *mutex)
{
    if (mutex->type == thread_mutex_critical_section) {
        EnterCriticalSection(&mutex->section);
    }
    else {
        DWORD rv = WaitForSingleObject(mutex->handle, INFINITE);
        if ((rv != WAIT_OBJECT_0) && (rv != WAIT_ABANDONED)) {
            return (rv == WAIT_TIMEOUT) ? KUDA_EBUSY : kuda_get_platform_error();
        }
    }        
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_trylock(kuda_thread_mutex_t *mutex)
{
    if (mutex->type == thread_mutex_critical_section) {
        if (!TryEnterCriticalSection(&mutex->section)) {
            return KUDA_EBUSY;
        }
    }
    else {
        DWORD rv = WaitForSingleObject(mutex->handle, 0);
        if ((rv != WAIT_OBJECT_0) && (rv != WAIT_ABANDONED)) {
            return (rv == WAIT_TIMEOUT) ? KUDA_EBUSY : kuda_get_platform_error();
        }
    }        
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_timedlock(kuda_thread_mutex_t *mutex,
                                                 kuda_interval_time_t timeout)
{
    if (mutex->type != thread_mutex_critical_section) {
        DWORD rv, timeout_ms = 0;
        kuda_interval_time_t t = timeout;

        do {
            if (t > 0) {
                /* Given timeout is 64bit usecs whereas Windows timeouts are
                 * 32bit msecs and below INFINITE (2^32 - 1), so we may need
                 * multiple timed out waits...
                 */
                if (t > kuda_time_from_msec(INFINITE - 1)) {
                    timeout_ms = INFINITE - 1;
                    t -= kuda_time_from_msec(INFINITE - 1);
                }
                else {
                    timeout_ms = (DWORD)kuda_time_as_msec(t);
                    t = 0;
                }
            }
            rv = WaitForSingleObject(mutex->handle, timeout_ms);
        } while (rv == WAIT_TIMEOUT && t > 0);

        if ((rv != WAIT_OBJECT_0) && (rv != WAIT_ABANDONED)) {
            return (rv == WAIT_TIMEOUT) ? KUDA_TIMEUP : kuda_get_platform_error();
        }
        return KUDA_SUCCESS;
    }        

    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_unlock(kuda_thread_mutex_t *mutex)
{
    if (mutex->type == thread_mutex_critical_section) {
        LeaveCriticalSection(&mutex->section);
    }
    else if (mutex->type == thread_mutex_unnested_event) {
        if (!SetEvent(mutex->handle)) {
            return kuda_get_platform_error();
        }
    }
    else if (mutex->type == thread_mutex_nested_mutex) {
        if (!ReleaseMutex(mutex->handle)) {
            return kuda_get_platform_error();
        }
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_destroy(kuda_thread_mutex_t *mutex)
{
    return kuda_pool_cleanup_run(mutex->pool, mutex, thread_mutex_cleanup);
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread_mutex)

