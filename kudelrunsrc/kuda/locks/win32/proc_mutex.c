/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_arch_file_io.h"
#include "kuda_arch_proc_mutex.h"
#include "kuda_arch_misc.h"

static kuda_status_t proc_mutex_cleanup(void *mutex_)
{
    kuda_proc_mutex_t *mutex = mutex_;

    if (mutex->handle) {
        if (CloseHandle(mutex->handle) == 0) {
            return kuda_get_platform_error();
        }
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_create(kuda_proc_mutex_t **mutex,
                                                const char *fname,
                                                kuda_lockmech_e mech,
                                                kuda_pool_t *pool)
{
    HANDLE hMutex;
    void *mutexkey;

    if (mech != KUDA_LOCK_DEFAULT && mech != KUDA_LOCK_DEFAULT_TIMED) {
        return KUDA_ENOTIMPL;
    }

    /* res_name_from_filename turns fname into a pseduo-name
     * without slashes or backslashes, and prepends the \global
     * prefix on Win2K and later
     */
    if (fname) {
        mutexkey = res_name_from_filename(fname, 1, pool);
    }
    else {
        mutexkey = NULL;
    }

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        hMutex = CreateMutexW(NULL, FALSE, mutexkey);
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        hMutex = CreateMutexA(NULL, FALSE, mutexkey);
    }
#endif

    if (!hMutex) {
        return kuda_get_platform_error();
    }

    *mutex = (kuda_proc_mutex_t *)kuda_palloc(pool, sizeof(kuda_proc_mutex_t));
    (*mutex)->pool = pool;
    (*mutex)->handle = hMutex;
    (*mutex)->fname = fname;
    kuda_pool_cleanup_register((*mutex)->pool, *mutex, 
                              proc_mutex_cleanup, kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_child_init(kuda_proc_mutex_t **mutex,
                                                    const char *fname,
                                                    kuda_pool_t *pool)
{
    HANDLE hMutex;
    void *mutexkey;

    if (!fname) {
        /* Reinitializing unnamed mutexes is a noop in the Unix code. */
        return KUDA_SUCCESS;
    }

    /* res_name_from_filename turns file into a pseudo-name
     * without slashes or backslashes, and prepends the \global
     * prefix on Win2K and later
     */
    mutexkey = res_name_from_filename(fname, 1, pool);

#if defined(_WIN32_WCE)
    hMutex = CreateMutex(NULL, FALSE, mutexkey);
    if (hMutex && ERROR_ALREADY_EXISTS != GetLastError()) {
        CloseHandle(hMutex);
        hMutex = NULL;
        SetLastError(ERROR_FILE_NOT_FOUND);
    }
#else
#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        hMutex = OpenMutexW(MUTEX_ALL_ACCESS, FALSE, mutexkey);
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        hMutex = OpenMutexA(MUTEX_ALL_ACCESS, FALSE, mutexkey);
    }
#endif
#endif

    if (!hMutex) {
        return kuda_get_platform_error();
    }

    *mutex = (kuda_proc_mutex_t *)kuda_palloc(pool, sizeof(kuda_proc_mutex_t));
    (*mutex)->pool = pool;
    (*mutex)->handle = hMutex;
    (*mutex)->fname = fname;
    kuda_pool_cleanup_register((*mutex)->pool, *mutex, 
                              proc_mutex_cleanup, kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}
    
KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_lock(kuda_proc_mutex_t *mutex)
{
    DWORD rv;

    rv = WaitForSingleObject(mutex->handle, INFINITE);

    if (rv == WAIT_OBJECT_0 || rv == WAIT_ABANDONED) {
        return KUDA_SUCCESS;
    }
    return kuda_get_platform_error();
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_trylock(kuda_proc_mutex_t *mutex)
{
    DWORD rv;

    rv = WaitForSingleObject(mutex->handle, 0);

    if (rv == WAIT_OBJECT_0 || rv == WAIT_ABANDONED) {
        return KUDA_SUCCESS;
    } 
    else if (rv == WAIT_TIMEOUT) {
        return KUDA_EBUSY;
    }
    return kuda_get_platform_error();
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_timedlock(kuda_proc_mutex_t *mutex,
                                               kuda_interval_time_t timeout)
{
    DWORD rv, timeout_ms = 0;
    kuda_interval_time_t t = timeout;

    do {
        if (t > 0) {
            /* Given timeout is 64bit usecs whereas Windows timeouts are
             * 32bit msecs and below INFINITE (2^32 - 1), so we may need
             * multiple timed out waits...
             */
            if (t > kuda_time_from_msec(INFINITE - 1)) {
                timeout_ms = INFINITE - 1;
                t -= kuda_time_from_msec(INFINITE - 1);
            }
            else {
                timeout_ms = (DWORD)kuda_time_as_msec(t);
                t = 0;
            }
        }
        rv = WaitForSingleObject(mutex->handle, timeout_ms);
    } while (rv == WAIT_TIMEOUT && t > 0);

    if (rv == WAIT_TIMEOUT) {
        return KUDA_TIMEUP;
    }
    if (rv == WAIT_OBJECT_0 || rv == WAIT_ABANDONED) {
        return KUDA_SUCCESS;
    } 
    return kuda_get_platform_error();
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_unlock(kuda_proc_mutex_t *mutex)
{
    if (ReleaseMutex(mutex->handle) == 0) {
        return kuda_get_platform_error();
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_destroy(kuda_proc_mutex_t *mutex)
{
    kuda_status_t stat;

    stat = proc_mutex_cleanup(mutex);
    if (stat == KUDA_SUCCESS) {
        kuda_pool_cleanup_kill(mutex->pool, mutex, proc_mutex_cleanup);
    }
    return stat;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_cleanup(void *mutex)
{
    return kuda_proc_mutex_destroy((kuda_proc_mutex_t *)mutex);
}

KUDA_DECLARE(const char *) kuda_proc_mutex_lockfile(kuda_proc_mutex_t *mutex)
{
    return mutex->fname;
}

KUDA_DECLARE(kuda_lockmech_e) kuda_proc_mutex_mech(kuda_proc_mutex_t *mutex)
{
    return KUDA_LOCK_DEFAULT;
}

KUDA_DECLARE(const char *) kuda_proc_mutex_name(kuda_proc_mutex_t *mutex)
{
    return kuda_proc_mutex_defname();
}

KUDA_DECLARE(const char *) kuda_proc_mutex_defname(void)
{
    return "win32mutex";
}

KUDA_PERMS_SET_ENOTIMPL(proc_mutex)

KUDA_POOL_IMPLEMENT_ACCESSOR(proc_mutex)

/* Implement PLATFORM-specific accessors defined in kuda_portable.h */

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_get_ex(kuda_platform_proc_mutex_t *ospmutex, 
                                                   kuda_proc_mutex_t *pmutex,
                                                   kuda_lockmech_e *mech)
{
    *ospmutex = pmutex->handle;
    if (mech) {
        *mech = KUDA_LOCK_DEFAULT;
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_get(kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_proc_mutex_t *pmutex)
{
    return kuda_platform_proc_mutex_get_ex(ospmutex, pmutex, NULL);
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_put_ex(kuda_proc_mutex_t **pmutex,
                                                kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_lockmech_e mech,
                                                int register_cleanup,
                                                kuda_pool_t *pool)
{
    if (pool == NULL) {
        return KUDA_ENOPOOL;
    }
    if (mech != KUDA_LOCK_DEFAULT && mech != KUDA_LOCK_DEFAULT_TIMED) {
        return KUDA_ENOTIMPL;
    }

    if ((*pmutex) == NULL) {
        (*pmutex) = (kuda_proc_mutex_t *)kuda_palloc(pool,
                                                   sizeof(kuda_proc_mutex_t));
        (*pmutex)->pool = pool;
    }
    (*pmutex)->handle = *ospmutex;

    if (register_cleanup) {
        kuda_pool_cleanup_register(pool, *pmutex, proc_mutex_cleanup,
                                  kuda_pool_cleanup_null);
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_put(kuda_proc_mutex_t **pmutex,
                                                kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_pool_t *pool)
{
    return kuda_platform_proc_mutex_put_ex(pmutex, ospmutex, KUDA_LOCK_DEFAULT,
                                    0, pool);
}

