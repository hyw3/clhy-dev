/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_thread_mutex.h"
#define KUDA_WANT_MEMFUNC
#include "kuda_want.h"

#if KUDA_HAS_THREADS

static kuda_status_t thread_mutex_cleanup(void *data)
{
    kuda_thread_mutex_t *mutex = data;
    kuda_status_t rv;

    rv = pthread_mutex_destroy(&mutex->mutex);
#ifdef HAVE_ZOS_PTHREADS
    if (rv) {
        rv = errno;
    }
#endif
    return rv;
} 

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_create(kuda_thread_mutex_t **mutex,
                                                  unsigned int flags,
                                                  kuda_pool_t *pool)
{
    kuda_thread_mutex_t *new_mutex;
    kuda_status_t rv;
    
#ifndef HAVE_PTHREAD_MUTEX_RECURSIVE
    if (flags & KUDA_THREAD_MUTEX_NESTED) {
        return KUDA_ENOTIMPL;
    }
#endif

    new_mutex = kuda_pcalloc(pool, sizeof(kuda_thread_mutex_t));
    new_mutex->pool = pool;

#ifdef HAVE_PTHREAD_MUTEX_RECURSIVE
    if (flags & KUDA_THREAD_MUTEX_NESTED) {
        pthread_mutexattr_t mattr;
        
        rv = pthread_mutexattr_init(&mattr);
        if (rv) return rv;
        
        rv = pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_RECURSIVE);
        if (rv) {
            pthread_mutexattr_destroy(&mattr);
            return rv;
        }
         
        rv = pthread_mutex_init(&new_mutex->mutex, &mattr);
        
        pthread_mutexattr_destroy(&mattr);
    } else
#endif
        rv = pthread_mutex_init(&new_mutex->mutex, NULL);

    if (rv) {
#ifdef HAVE_ZOS_PTHREADS
        rv = errno;
#endif
        return rv;
    }

#ifndef HAVE_PTHREAD_MUTEX_TIMEDLOCK
    if (flags & KUDA_THREAD_MUTEX_TIMED) {
        rv = kuda_thread_cond_create(&new_mutex->cond, pool);
        if (rv) {
#ifdef HAVE_ZOS_PTHREADS
            rv = errno;
#endif
            pthread_mutex_destroy(&new_mutex->mutex);
            return rv;
        }
    }
#endif

    kuda_pool_cleanup_register(new_mutex->pool,
                              new_mutex, thread_mutex_cleanup,
                              kuda_pool_cleanup_null);

    *mutex = new_mutex;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_lock(kuda_thread_mutex_t *mutex)
{
    kuda_status_t rv;

    if (mutex->cond) {
        kuda_status_t rv2;

        rv = pthread_mutex_lock(&mutex->mutex);
        if (rv) {
#ifdef HAVE_ZOS_PTHREADS
            rv = errno;
#endif
            return rv;
        }

        if (mutex->locked) {
            mutex->num_waiters++;
            rv = kuda_thread_cond_wait(mutex->cond, mutex);
            mutex->num_waiters--;
        }
        else {
            mutex->locked = 1;
        }

        rv2 = pthread_mutex_unlock(&mutex->mutex);
        if (rv2 && !rv) {
#ifdef HAVE_ZOS_PTHREADS
            rv = errno;
#else
            rv = rv2;
#endif
        }

        return rv;
    }

    rv = pthread_mutex_lock(&mutex->mutex);
#ifdef HAVE_ZOS_PTHREADS
    if (rv) {
        rv = errno;
    }
#endif

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_trylock(kuda_thread_mutex_t *mutex)
{
    kuda_status_t rv;

    if (mutex->cond) {
        kuda_status_t rv2;

        rv = pthread_mutex_lock(&mutex->mutex);
        if (rv) {
#ifdef HAVE_ZOS_PTHREADS
            rv = errno;
#endif
            return rv;
        }

        if (mutex->locked) {
            rv = KUDA_EBUSY;
        }
        else {
            mutex->locked = 1;
        }

        rv2 = pthread_mutex_unlock(&mutex->mutex);
        if (rv2) {
#ifdef HAVE_ZOS_PTHREADS
            rv = errno;
#else
            rv = rv2;
#endif
        }

        return rv;
    }

    rv = pthread_mutex_trylock(&mutex->mutex);
    if (rv) {
#ifdef HAVE_ZOS_PTHREADS
        rv = errno;
#endif
        return (rv == EBUSY) ? KUDA_EBUSY : rv;
    }

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_timedlock(kuda_thread_mutex_t *mutex,
                                                 kuda_interval_time_t timeout)
{
    kuda_status_t rv = KUDA_ENOTIMPL;
#if KUDA_HAS_TIMEDLOCKS

#ifdef HAVE_PTHREAD_MUTEX_TIMEDLOCK
    if (timeout <= 0) {
        rv = pthread_mutex_trylock(&mutex->mutex);
        if (rv) {
#ifdef HAVE_ZOS_PTHREADS
            rv = errno;
#endif
            if (rv == EBUSY) {
                rv = KUDA_TIMEUP;
            }
        }
    }
    else {
        struct timespec abstime;

        timeout += kuda_time_now();
        abstime.tv_sec = kuda_time_sec(timeout);
        abstime.tv_nsec = kuda_time_usec(timeout) * 1000; /* nanoseconds */

        rv = pthread_mutex_timedlock(&mutex->mutex, &abstime);
        if (rv) {
#ifdef HAVE_ZOS_PTHREADS
            rv = errno;
#endif
            if (rv == ETIMEDOUT) {
                rv = KUDA_TIMEUP;
            }
        }
    }

#else /* HAVE_PTHREAD_MUTEX_TIMEDLOCK */

    if (mutex->cond) {
        rv = pthread_mutex_lock(&mutex->mutex);
        if (rv) {
#ifdef HAVE_ZOS_PTHREADS
            rv = errno;
#endif
            return rv;
        }

        if (mutex->locked) {
            if (timeout <= 0) {
                rv = KUDA_TIMEUP;
            }
            else {
                mutex->num_waiters++;
                do {
                    rv = kuda_thread_cond_timedwait(mutex->cond, mutex,
                                                   timeout);
                    if (rv) {
#ifdef HAVE_ZOS_PTHREADS
                        rv = errno;
#endif
                        break;
                    }
                } while (mutex->locked);
                mutex->num_waiters--;
            }
            if (rv) {
                pthread_mutex_unlock(&mutex->mutex);
                return rv;
            }
        }

        mutex->locked = 1;

        rv = pthread_mutex_unlock(&mutex->mutex);
        if (rv) {
#ifdef HAVE_ZOS_PTHREADS
            rv = errno;
#endif
            return rv;
        }
    }

#endif /* HAVE_PTHREAD_MUTEX_TIMEDLOCK */

#endif /* KUDA_HAS_TIMEDLOCKS */
    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_unlock(kuda_thread_mutex_t *mutex)
{
    kuda_status_t status;

    if (mutex->cond) {
        status = pthread_mutex_lock(&mutex->mutex);
        if (status) {
#ifdef HAVE_ZOS_PTHREADS
            status = errno;
#endif
            return status;
        }

        if (!mutex->locked) {
            status = KUDA_EINVAL;
        }
        else if (mutex->num_waiters) {
            status = kuda_thread_cond_signal(mutex->cond);
        }
        if (status) {
            pthread_mutex_unlock(&mutex->mutex);
            return status;
        }

        mutex->locked = 0;
    }

    status = pthread_mutex_unlock(&mutex->mutex);
#ifdef HAVE_ZOS_PTHREADS
    if (status) {
        status = errno;
    }
#endif

    return status;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_destroy(kuda_thread_mutex_t *mutex)
{
    kuda_status_t rv, rv2 = KUDA_SUCCESS;

    if (mutex->cond) {
        rv2 = kuda_thread_cond_destroy(mutex->cond);
    }
    rv = kuda_pool_cleanup_run(mutex->pool, mutex, thread_mutex_cleanup);
    if (rv == KUDA_SUCCESS) {
        rv = rv2;
    }
    
    return rv;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread_mutex)

#endif /* KUDA_HAS_THREADS */
