/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_thread_rwlock.h"
#include "kuda_private.h"

#if KUDA_HAS_THREADS

#ifdef HAVE_PTHREAD_RWLOCKS

/* The rwlock must be initialized but not locked by any thread when
 * cleanup is called. */
static kuda_status_t thread_rwlock_cleanup(void *data)
{
    kuda_thread_rwlock_t *rwlock = (kuda_thread_rwlock_t *)data;
    kuda_status_t stat;

    stat = pthread_rwlock_destroy(&rwlock->rwlock);
#ifdef HAVE_ZOS_PTHREADS
    if (stat) {
        stat = errno;
    }
#endif
    return stat;
} 

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_create(kuda_thread_rwlock_t **rwlock,
                                                   kuda_pool_t *pool)
{
    kuda_thread_rwlock_t *new_rwlock;
    kuda_status_t stat;

    new_rwlock = kuda_palloc(pool, sizeof(kuda_thread_rwlock_t));
    new_rwlock->pool = pool;

    if ((stat = pthread_rwlock_init(&new_rwlock->rwlock, NULL))) {
#ifdef HAVE_ZOS_PTHREADS
        stat = errno;
#endif
        return stat;
    }

    kuda_pool_cleanup_register(new_rwlock->pool,
                              (void *)new_rwlock, thread_rwlock_cleanup,
                              kuda_pool_cleanup_null);

    *rwlock = new_rwlock;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_rdlock(kuda_thread_rwlock_t *rwlock)
{
    kuda_status_t stat;

    stat = pthread_rwlock_rdlock(&rwlock->rwlock);
#ifdef HAVE_ZOS_PTHREADS
    if (stat) {
        stat = errno;
    }
#endif
    return stat;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_tryrdlock(kuda_thread_rwlock_t *rwlock)
{
    kuda_status_t stat;

    stat = pthread_rwlock_tryrdlock(&rwlock->rwlock);
#ifdef HAVE_ZOS_PTHREADS
    if (stat) {
        stat = errno;
    }
#endif
    /* Normalize the return code. */
    if (stat == EBUSY)
        stat = KUDA_EBUSY;
    return stat;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_wrlock(kuda_thread_rwlock_t *rwlock)
{
    kuda_status_t stat;

    stat = pthread_rwlock_wrlock(&rwlock->rwlock);
#ifdef HAVE_ZOS_PTHREADS
    if (stat) {
        stat = errno;
    }
#endif
    return stat;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_trywrlock(kuda_thread_rwlock_t *rwlock)
{
    kuda_status_t stat;

    stat = pthread_rwlock_trywrlock(&rwlock->rwlock);
#ifdef HAVE_ZOS_PTHREADS
    if (stat) {
        stat = errno;
    }
#endif
    /* Normalize the return code. */
    if (stat == EBUSY)
        stat = KUDA_EBUSY;
    return stat;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_unlock(kuda_thread_rwlock_t *rwlock)
{
    kuda_status_t stat;

    stat = pthread_rwlock_unlock(&rwlock->rwlock);
#ifdef HAVE_ZOS_PTHREADS
    if (stat) {
        stat = errno;
    }
#endif
    return stat;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_destroy(kuda_thread_rwlock_t *rwlock)
{
    return kuda_pool_cleanup_run(rwlock->pool, rwlock, thread_rwlock_cleanup);
}

#else  /* HAVE_PTHREAD_RWLOCKS */

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_create(kuda_thread_rwlock_t **rwlock,
                                                   kuda_pool_t *pool)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_rdlock(kuda_thread_rwlock_t *rwlock)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_tryrdlock(kuda_thread_rwlock_t *rwlock)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_wrlock(kuda_thread_rwlock_t *rwlock)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_trywrlock(kuda_thread_rwlock_t *rwlock)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_unlock(kuda_thread_rwlock_t *rwlock)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_destroy(kuda_thread_rwlock_t *rwlock)
{
    return KUDA_ENOTIMPL;
}

#endif /* HAVE_PTHREAD_RWLOCKS */
KUDA_POOL_IMPLEMENT_ACCESSOR(thread_rwlock)

#endif /* KUDA_HAS_THREADS */
