/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"

#if KUDA_HAS_THREADS

#include "kuda_arch_thread_mutex.h"
#include "kuda_arch_thread_cond.h"

static kuda_status_t thread_cond_cleanup(void *data)
{
    kuda_thread_cond_t *cond = (kuda_thread_cond_t *)data;
    kuda_status_t rv;

    rv = pthread_cond_destroy(&cond->cond);
#ifdef HAVE_ZOS_PTHREADS
    if (rv) {
        rv = errno;
    }
#endif
    return rv;
} 

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_create(kuda_thread_cond_t **cond,
                                                 kuda_pool_t *pool)
{
    kuda_thread_cond_t *new_cond;
    kuda_status_t rv;

    new_cond = kuda_palloc(pool, sizeof(kuda_thread_cond_t));

    new_cond->pool = pool;

    if ((rv = pthread_cond_init(&new_cond->cond, NULL))) {
#ifdef HAVE_ZOS_PTHREADS
        rv = errno;
#endif
        return rv;
    }

    kuda_pool_cleanup_register(new_cond->pool,
                              (void *)new_cond, thread_cond_cleanup,
                              kuda_pool_cleanup_null);

    *cond = new_cond;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_wait(kuda_thread_cond_t *cond,
                                               kuda_thread_mutex_t *mutex)
{
    kuda_status_t rv;

    rv = pthread_cond_wait(&cond->cond, &mutex->mutex);
#ifdef HAVE_ZOS_PTHREADS
    if (rv) {
        rv = errno;
    }
#endif
    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_timedwait(kuda_thread_cond_t *cond,
                                                    kuda_thread_mutex_t *mutex,
                                                    kuda_interval_time_t timeout)
{
    kuda_status_t rv;
    if (timeout < 0) {
        rv = pthread_cond_wait(&cond->cond, &mutex->mutex);
#ifdef HAVE_ZOS_PTHREADS
        if (rv) {
            rv = errno;
        }
#endif
    }
    else {
        kuda_time_t then;
        struct timespec abstime;

        then = kuda_time_now() + timeout;
        abstime.tv_sec = kuda_time_sec(then);
        abstime.tv_nsec = kuda_time_usec(then) * 1000; /* nanoseconds */

        rv = pthread_cond_timedwait(&cond->cond, &mutex->mutex, &abstime);
#ifdef HAVE_ZOS_PTHREADS
        if (rv) {
            rv = errno;
        }
#endif
        if (ETIMEDOUT == rv) {
            return KUDA_TIMEUP;
        }
    }
    return rv;
}


KUDA_DECLARE(kuda_status_t) kuda_thread_cond_signal(kuda_thread_cond_t *cond)
{
    kuda_status_t rv;

    rv = pthread_cond_signal(&cond->cond);
#ifdef HAVE_ZOS_PTHREADS
    if (rv) {
        rv = errno;
    }
#endif
    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_broadcast(kuda_thread_cond_t *cond)
{
    kuda_status_t rv;

    rv = pthread_cond_broadcast(&cond->cond);
#ifdef HAVE_ZOS_PTHREADS
    if (rv) {
        rv = errno;
    }
#endif
    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_thread_cond_destroy(kuda_thread_cond_t *cond)
{
    return kuda_pool_cleanup_run(cond->pool, cond, thread_cond_cleanup);
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread_cond)

#endif /* KUDA_HAS_THREADS */
