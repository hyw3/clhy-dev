/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_arch_proc_mutex.h"
#include "kuda_arch_file_io.h" /* for kuda_mkstemp() */
#include "kuda_hash.h"
#include "kuda_atomic.h"

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_destroy(kuda_proc_mutex_t *mutex)
{
    return kuda_pool_cleanup_run(mutex->pool, mutex, kuda_proc_mutex_cleanup);
}

#if KUDA_HAS_POSIXSEM_SERIALIZE || KUDA_HAS_FCNTL_SERIALIZE || \
    KUDA_HAS_SYSVSEM_SERIALIZE
static kuda_status_t proc_mutex_no_child_init(kuda_proc_mutex_t **mutex,
                                             kuda_pool_t *cont,
                                             const char *fname)
{
    return KUDA_SUCCESS;
}
#endif    

#if KUDA_HAS_POSIXSEM_SERIALIZE || KUDA_HAS_PROC_PTHREAD_SERIALIZE
static kuda_status_t proc_mutex_no_perms_set(kuda_proc_mutex_t *mutex,
                                            kuda_fileperms_t perms,
                                            kuda_uid_t uid,
                                            kuda_gid_t gid)
{
    return KUDA_ENOTIMPL;
}
#endif    

#if KUDA_HAS_FCNTL_SERIALIZE \
    || KUDA_HAS_FLOCK_SERIALIZE \
    || (KUDA_HAS_SYSVSEM_SERIALIZE \
        && !defined(HAVE_SEMTIMEDOP)) \
    || (KUDA_HAS_POSIXSEM_SERIALIZE \
        && !defined(HAVE_SEM_TIMEDWAIT)) \
    || (KUDA_HAS_PROC_PTHREAD_SERIALIZE \
        && !defined(HAVE_PTHREAD_MUTEX_TIMEDLOCK) \
        && !defined(HAVE_PTHREAD_CONDATTR_SETPSHARED))
static kuda_status_t proc_mutex_spinsleep_timedacquire(kuda_proc_mutex_t *mutex,
                                                  kuda_interval_time_t timeout)
{
#define SLEEP_TIME kuda_time_from_msec(10)
    kuda_status_t rv;
    for (;;) {
        rv = kuda_proc_mutex_trylock(mutex);
        if (!KUDA_STATUS_IS_EBUSY(rv)) {
            if (rv == KUDA_SUCCESS) {
                mutex->curr_locked = 1;
            }
            break;
        }
        if (timeout <= 0) {
            rv = KUDA_TIMEUP;
            break;
        }
        if (timeout > SLEEP_TIME) {
            kuda_sleep(SLEEP_TIME);
            timeout -= SLEEP_TIME;
        }
        else {
            kuda_sleep(timeout);
            timeout = 0;
        }
    }
    return rv;
}
#endif

#if KUDA_HAS_POSIXSEM_SERIALIZE

#ifndef SEM_FAILED
#define SEM_FAILED (-1)
#endif

static kuda_status_t proc_mutex_posix_cleanup(void *mutex_)
{
    kuda_proc_mutex_t *mutex = mutex_;
    
    if (sem_close(mutex->platforms.psem_interproc) < 0) {
        return errno;
    }

    return KUDA_SUCCESS;
}    

static unsigned int rshash (char *p) {
    /* hash function from Robert Sedgwicks 'Algorithms in C' book */
   unsigned int b    = 378551;
   unsigned int a    = 63689;
   unsigned int retval = 0;

   for( ; *p; p++)
   {
      retval = retval * a + (*p);
      a *= b;
   }

   return retval;
}

static kuda_status_t proc_mutex_posix_create(kuda_proc_mutex_t *new_mutex,
                                            const char *fname)
{
    #define KUDA_POSIXSEM_NAME_MIN 13
    sem_t *psem;
    char semname[32];
    
    /*
     * This bogusness is to follow what appears to be the
     * lowest common denominator in Posix semaphore naming:
     *   - start with '/'
     *   - be at most 14 chars
     *   - be unique and not match anything on the filesystem
     *
     * Because of this, we use fname to generate a (unique) hash
     * and use that as the name of the semaphore. If no filename was
     * given, we create one based on the time. We tuck the name
     * away, since it might be useful for debugging. We use 2 hashing
     * functions to try to avoid collisions.
     *
     * To  make this as robust as possible, we initially try something
     * larger (and hopefully more unique) and gracefully fail down to the
     * LCD above.
     *
     * NOTE: Darwin (Mac OS X) seems to be the most restrictive
     * implementation. Versions previous to Darwin 6.2 had the 14
     * char limit, but later rev's allow up to 31 characters.
     *
     */
    if (fname) {
        kuda_ssize_t flen = strlen(fname);
        char *p = kuda_pstrndup(new_mutex->pool, fname, strlen(fname));
        unsigned int h1, h2;
        h1 = (kuda_hashfunc_default((const char *)p, &flen) & 0xffffffff);
        h2 = (rshash(p) & 0xffffffff);
        kuda_snprintf(semname, sizeof(semname), "/KudaD.%xH%x", h1, h2);
    } else {
        kuda_time_t now;
        unsigned long sec;
        unsigned long usec;
        now = kuda_time_now();
        sec = kuda_time_sec(now);
        usec = kuda_time_usec(now);
        kuda_snprintf(semname, sizeof(semname), "/KudaD.%lxZ%lx", sec, usec);
    }
    do {
        psem = sem_open(semname, O_CREAT | O_EXCL, 0644, 1);
    } while (psem == (sem_t *)SEM_FAILED && errno == EINTR);
    if (psem == (sem_t *)SEM_FAILED) {
        if (errno == ENAMETOOLONG) {
            /* Oh well, good try */
            semname[KUDA_POSIXSEM_NAME_MIN] = '\0';
        } else {
            return errno;
        }
        do {
            psem = sem_open(semname, O_CREAT | O_EXCL, 0644, 1);
        } while (psem == (sem_t *)SEM_FAILED && errno == EINTR);
    }

    if (psem == (sem_t *)SEM_FAILED) {
        return errno;
    }
    /* Ahhh. The joys of Posix sems. Predelete it... */
    sem_unlink(semname);
    new_mutex->platforms.psem_interproc = psem;
    new_mutex->fname = kuda_pstrdup(new_mutex->pool, semname);
    kuda_pool_cleanup_register(new_mutex->pool, (void *)new_mutex,
                              kuda_proc_mutex_cleanup, 
                              kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_posix_acquire(kuda_proc_mutex_t *mutex)
{
    int rc;

    do {
        rc = sem_wait(mutex->platforms.psem_interproc);
    } while (rc < 0 && errno == EINTR);
    if (rc < 0) {
        return errno;
    }
    mutex->curr_locked = 1;
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_posix_tryacquire(kuda_proc_mutex_t *mutex)
{
    int rc;

    do {
        rc = sem_trywait(mutex->platforms.psem_interproc);
    } while (rc < 0 && errno == EINTR);
    if (rc < 0) {
        if (errno == EAGAIN) {
            return KUDA_EBUSY;
        }
        return errno;
    }
    mutex->curr_locked = 1;
    return KUDA_SUCCESS;
}

#if defined(HAVE_SEM_TIMEDWAIT)
static kuda_status_t proc_mutex_posix_timedacquire(kuda_proc_mutex_t *mutex,
                                              kuda_interval_time_t timeout)
{
    if (timeout <= 0) {
        kuda_status_t rv = proc_mutex_posix_tryacquire(mutex);
        return (rv == KUDA_EBUSY) ? KUDA_TIMEUP : rv;
    }
    else {
        int rc;
        struct timespec abstime;

        timeout += kuda_time_now();
        abstime.tv_sec = kuda_time_sec(timeout);
        abstime.tv_nsec = kuda_time_usec(timeout) * 1000; /* nanoseconds */
        
        do {
            rc = sem_timedwait(mutex->platforms.psem_interproc, &abstime);
        } while (rc < 0 && errno == EINTR);
        if (rc < 0) {
            if (errno == ETIMEDOUT) {
                return KUDA_TIMEUP;
            }
            return errno;
        }
    }
    mutex->curr_locked = 1;
    return KUDA_SUCCESS;
}
#endif

static kuda_status_t proc_mutex_posix_release(kuda_proc_mutex_t *mutex)
{
    mutex->curr_locked = 0;
    if (sem_post(mutex->platforms.psem_interproc) < 0) {
        /* any failure is probably fatal, so no big deal to leave
         * ->curr_locked at 0. */
        return errno;
    }
    return KUDA_SUCCESS;
}

static const kuda_proc_mutex_unix_lock_methods_t mutex_posixsem_methods =
{
#if KUDA_PROCESS_LOCK_IS_GLOBAL || !KUDA_HAS_THREADS || defined(POSIXSEM_IS_GLOBAL)
    KUDA_PROCESS_LOCK_MECH_IS_GLOBAL,
#else
    0,
#endif
    proc_mutex_posix_create,
    proc_mutex_posix_acquire,
    proc_mutex_posix_tryacquire,
#if defined(HAVE_SEM_TIMEDWAIT)
    proc_mutex_posix_timedacquire,
#else
    proc_mutex_spinsleep_timedacquire,
#endif
    proc_mutex_posix_release,
    proc_mutex_posix_cleanup,
    proc_mutex_no_child_init,
    proc_mutex_no_perms_set,
    KUDA_LOCK_POSIXSEM,
    "posixsem"
};

#endif /* Posix sem implementation */

#if KUDA_HAS_SYSVSEM_SERIALIZE

static struct sembuf proc_mutex_op_on;
static struct sembuf proc_mutex_op_try;
static struct sembuf proc_mutex_op_off;

static void proc_mutex_sysv_setup(void)
{
    proc_mutex_op_on.sem_num = 0;
    proc_mutex_op_on.sem_op = -1;
    proc_mutex_op_on.sem_flg = SEM_UNDO;
    proc_mutex_op_try.sem_num = 0;
    proc_mutex_op_try.sem_op = -1;
    proc_mutex_op_try.sem_flg = SEM_UNDO | IPC_NOWAIT;
    proc_mutex_op_off.sem_num = 0;
    proc_mutex_op_off.sem_op = 1;
    proc_mutex_op_off.sem_flg = SEM_UNDO;
}

static kuda_status_t proc_mutex_sysv_cleanup(void *mutex_)
{
    kuda_proc_mutex_t *mutex=mutex_;
    union semun ick;
    
    if (mutex->platforms.crossproc != -1) {
        ick.val = 0;
        semctl(mutex->platforms.crossproc, 0, IPC_RMID, ick);
    }
    return KUDA_SUCCESS;
}    

static kuda_status_t proc_mutex_sysv_create(kuda_proc_mutex_t *new_mutex,
                                           const char *fname)
{
    union semun ick;
    kuda_status_t rv;
    
    new_mutex->platforms.crossproc = semget(IPC_PRIVATE, 1, IPC_CREAT | 0600);
    if (new_mutex->platforms.crossproc == -1) {
        rv = errno;
        proc_mutex_sysv_cleanup(new_mutex);
        return rv;
    }
    ick.val = 1;
    if (semctl(new_mutex->platforms.crossproc, 0, SETVAL, ick) < 0) {
        rv = errno;
        proc_mutex_sysv_cleanup(new_mutex);
        new_mutex->platforms.crossproc = -1;
        return rv;
    }
    new_mutex->curr_locked = 0;
    kuda_pool_cleanup_register(new_mutex->pool,
                              (void *)new_mutex, kuda_proc_mutex_cleanup, 
                              kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_sysv_acquire(kuda_proc_mutex_t *mutex)
{
    int rc;

    do {
        rc = semop(mutex->platforms.crossproc, &proc_mutex_op_on, 1);
    } while (rc < 0 && errno == EINTR);
    if (rc < 0) {
        return errno;
    }
    mutex->curr_locked = 1;
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_sysv_tryacquire(kuda_proc_mutex_t *mutex)
{
    int rc;

    do {
        rc = semop(mutex->platforms.crossproc, &proc_mutex_op_try, 1);
    } while (rc < 0 && errno == EINTR);
    if (rc < 0) {
        if (errno == EAGAIN) {
            return KUDA_EBUSY;
        }
        return errno;
    }
    mutex->curr_locked = 1;
    return KUDA_SUCCESS;
}

#if defined(HAVE_SEMTIMEDOP)
static kuda_status_t proc_mutex_sysv_timedacquire(kuda_proc_mutex_t *mutex,
                                             kuda_interval_time_t timeout)
{
    if (timeout <= 0) {
        kuda_status_t rv = proc_mutex_sysv_tryacquire(mutex);
        return (rv == KUDA_EBUSY) ? KUDA_TIMEUP : rv;
    }
    else {
        int rc;
        struct timespec reltime;

        reltime.tv_sec = kuda_time_sec(timeout);
        reltime.tv_nsec = kuda_time_usec(timeout) * 1000; /* nanoseconds */

        do {
            rc = semtimedop(mutex->platforms.crossproc, &proc_mutex_op_on, 1,
                            &reltime);
        } while (rc < 0 && errno == EINTR);
        if (rc < 0) {
            if (errno == EAGAIN) {
                return KUDA_TIMEUP;
            }
            return errno;
        }
    }
    mutex->curr_locked = 1;
    return KUDA_SUCCESS;
}
#endif

static kuda_status_t proc_mutex_sysv_release(kuda_proc_mutex_t *mutex)
{
    int rc;

    mutex->curr_locked = 0;
    do {
        rc = semop(mutex->platforms.crossproc, &proc_mutex_op_off, 1);
    } while (rc < 0 && errno == EINTR);
    if (rc < 0) {
        return errno;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_sysv_perms_set(kuda_proc_mutex_t *mutex,
                                              kuda_fileperms_t perms,
                                              kuda_uid_t uid,
                                              kuda_gid_t gid)
{

    union semun ick;
    struct semid_ds buf;
    buf.sem_perm.uid = uid;
    buf.sem_perm.gid = gid;
    buf.sem_perm.mode = kuda_unix_perms2mode(perms);
    ick.buf = &buf;
    if (semctl(mutex->platforms.crossproc, 0, IPC_SET, ick) < 0) {
        return errno;
    }
    return KUDA_SUCCESS;
}

static const kuda_proc_mutex_unix_lock_methods_t mutex_sysv_methods =
{
#if KUDA_PROCESS_LOCK_IS_GLOBAL || !KUDA_HAS_THREADS || defined(SYSVSEM_IS_GLOBAL)
    KUDA_PROCESS_LOCK_MECH_IS_GLOBAL,
#else
    0,
#endif
    proc_mutex_sysv_create,
    proc_mutex_sysv_acquire,
    proc_mutex_sysv_tryacquire,
#if defined(HAVE_SEMTIMEDOP)
    proc_mutex_sysv_timedacquire,
#else
    proc_mutex_spinsleep_timedacquire,
#endif
    proc_mutex_sysv_release,
    proc_mutex_sysv_cleanup,
    proc_mutex_no_child_init,
    proc_mutex_sysv_perms_set,
    KUDA_LOCK_SYSVSEM,
    "sysvsem"
};

#endif /* SysV sem implementation */

#if KUDA_HAS_PROC_PTHREAD_SERIALIZE

#ifndef KUDA_USE_PROC_PTHREAD_MUTEX_COND
#define KUDA_USE_PROC_PTHREAD_MUTEX_COND \
            (defined(HAVE_PTHREAD_CONDATTR_SETPSHARED) \
             && !defined(HAVE_PTHREAD_MUTEX_TIMEDLOCK))
#endif

/* The mmap()ed pthread_interproc is the native pthread_mutex_t followed
 * by a refcounter to track children using it.  We want to avoid calling
 * pthread_mutex_destroy() on the shared mutex area while it is in use by
 * another process, because this may mark the shared pthread_mutex_t as
 * invalid for everyone, including forked children (unlike "sysvsem" for
 * example), causing unexpected errors or deadlocks (PR 49504).  So the
 * last process (parent or child) referencing the mutex will effectively
 * destroy it.
 */
typedef struct {
#define proc_pthread_cast(m) \
    ((proc_pthread_mutex_t *)(m)->platforms.pthread_interproc)
    pthread_mutex_t mutex;
#define proc_pthread_mutex(m) \
    (proc_pthread_cast(m)->mutex)
#if KUDA_USE_PROC_PTHREAD_MUTEX_COND
    pthread_cond_t  cond;
#define proc_pthread_mutex_cond(m) \
    (proc_pthread_cast(m)->cond)
    kuda_int32_t     cond_locked;
#define proc_pthread_mutex_cond_locked(m) \
    (proc_pthread_cast(m)->cond_locked)
    kuda_uint32_t    cond_num_waiters;
#define proc_pthread_mutex_cond_num_waiters(m) \
    (proc_pthread_cast(m)->cond_num_waiters)
#define proc_pthread_mutex_is_cond(m) \
    ((m)->pthread_refcounting && proc_pthread_mutex_cond_locked(m) != -1)
#endif /* KUDA_USE_PROC_PTHREAD_MUTEX_COND */
    kuda_uint32_t refcount;
#define proc_pthread_mutex_refcount(m) \
    (proc_pthread_cast(m)->refcount)
} proc_pthread_mutex_t;


static KUDA_INLINE int proc_pthread_mutex_inc(kuda_proc_mutex_t *mutex)
{
    if (mutex->pthread_refcounting) {
        kuda_atomic_inc32(&proc_pthread_mutex_refcount(mutex));
        return 1;
    }
    return 0;
}

static KUDA_INLINE int proc_pthread_mutex_dec(kuda_proc_mutex_t *mutex)
{
    if (mutex->pthread_refcounting) {
        return kuda_atomic_dec32(&proc_pthread_mutex_refcount(mutex));
    }
    return 0;
}

static kuda_status_t proc_pthread_mutex_unref(void *mutex_)
{
    kuda_proc_mutex_t *mutex=mutex_;
    kuda_status_t rv;

#if KUDA_USE_PROC_PTHREAD_MUTEX_COND
    if (proc_pthread_mutex_is_cond(mutex)) {
        mutex->curr_locked = 0;
    }
    else
#endif /* KUDA_USE_PROC_PTHREAD_MUTEX_COND */
    if (mutex->curr_locked == 1) {
        if ((rv = pthread_mutex_unlock(&proc_pthread_mutex(mutex)))) {
#ifdef HAVE_ZOS_PTHREADS
            rv = errno;
#endif
            return rv;
        }
    }
    if (!proc_pthread_mutex_dec(mutex)) {
#if KUDA_USE_PROC_PTHREAD_MUTEX_COND
        if (proc_pthread_mutex_is_cond(mutex) &&
                (rv = pthread_cond_destroy(&proc_pthread_mutex_cond(mutex)))) {
#ifdef HAVE_ZOS_PTHREADS
            rv = errno;
#endif
            return rv;
        }
#endif /* KUDA_USE_PROC_PTHREAD_MUTEX_COND */

        if ((rv = pthread_mutex_destroy(&proc_pthread_mutex(mutex)))) {
#ifdef HAVE_ZOS_PTHREADS
            rv = errno;
#endif
            return rv;
        }
    }
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_pthread_cleanup(void *mutex_)
{
    kuda_proc_mutex_t *mutex=mutex_;
    kuda_status_t rv;

    /* curr_locked is set to -1 until the mutex has been created */
    if (mutex->curr_locked != -1) {
        if ((rv = proc_pthread_mutex_unref(mutex))) {
            return rv;
        }
    }
    if (munmap(mutex->platforms.pthread_interproc, sizeof(proc_pthread_mutex_t))) {
        return errno;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_pthread_create(kuda_proc_mutex_t *new_mutex,
                                              const char *fname)
{
    kuda_status_t rv;
    int fd;
    pthread_mutexattr_t mattr;

    fd = open("/dev/zero", O_RDWR);
    if (fd < 0) {
        return errno;
    }

    new_mutex->platforms.pthread_interproc = mmap(NULL, sizeof(proc_pthread_mutex_t),
                                           PROT_READ | PROT_WRITE, MAP_SHARED,
                                           fd, 0); 
    if (new_mutex->platforms.pthread_interproc == MAP_FAILED) {
        new_mutex->platforms.pthread_interproc = NULL;
        rv = errno;
        close(fd);
        return rv;
    }
    close(fd);

    new_mutex->pthread_refcounting = 1;
    new_mutex->curr_locked = -1; /* until the mutex has been created */
#if KUDA_USE_PROC_PTHREAD_MUTEX_COND
    proc_pthread_mutex_cond_locked(new_mutex) = -1;
#endif

    if ((rv = pthread_mutexattr_init(&mattr))) {
#ifdef HAVE_ZOS_PTHREADS
        rv = errno;
#endif
        proc_mutex_pthread_cleanup(new_mutex);
        return rv;
    }
    if ((rv = pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_SHARED))) {
#ifdef HAVE_ZOS_PTHREADS
        rv = errno;
#endif
        proc_mutex_pthread_cleanup(new_mutex);
        pthread_mutexattr_destroy(&mattr);
        return rv;
    }

#if defined(HAVE_PTHREAD_MUTEX_ROBUST) || defined(HAVE_PTHREAD_MUTEX_ROBUST_NP)
#ifdef HAVE_PTHREAD_MUTEX_ROBUST
    rv = pthread_mutexattr_setrobust(&mattr, PTHREAD_MUTEX_ROBUST);
#else
    rv = pthread_mutexattr_setrobust_np(&mattr, PTHREAD_MUTEX_ROBUST_NP);
#endif
    if (rv) {
#ifdef HAVE_ZOS_PTHREADS
        rv = errno;
#endif
        proc_mutex_pthread_cleanup(new_mutex);
        pthread_mutexattr_destroy(&mattr);
        return rv;
    }
    if ((rv = pthread_mutexattr_setprotocol(&mattr, PTHREAD_PRIO_INHERIT))) {
#ifdef HAVE_ZOS_PTHREADS
        rv = errno;
#endif
        proc_mutex_pthread_cleanup(new_mutex);
        pthread_mutexattr_destroy(&mattr);
        return rv;
    }
#endif /* HAVE_PTHREAD_MUTEX_ROBUST[_NP] */

    if ((rv = pthread_mutex_init(&proc_pthread_mutex(new_mutex), &mattr))) {
#ifdef HAVE_ZOS_PTHREADS
        rv = errno;
#endif
        proc_mutex_pthread_cleanup(new_mutex);
        pthread_mutexattr_destroy(&mattr);
        return rv;
    }

    proc_pthread_mutex_refcount(new_mutex) = 1; /* first/parent reference */
    new_mutex->curr_locked = 0; /* mutex created now */

    if ((rv = pthread_mutexattr_destroy(&mattr))) {
#ifdef HAVE_ZOS_PTHREADS
        rv = errno;
#endif
        proc_mutex_pthread_cleanup(new_mutex);
        return rv;
    }

    kuda_pool_cleanup_register(new_mutex->pool,
                              (void *)new_mutex,
                              kuda_proc_mutex_cleanup, 
                              kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_pthread_child_init(kuda_proc_mutex_t **mutex,
                                                  kuda_pool_t *pool, 
                                                  const char *fname)
{
    (*mutex)->curr_locked = 0;
    if (proc_pthread_mutex_inc(*mutex)) {
        kuda_pool_cleanup_register(pool, *mutex, proc_pthread_mutex_unref, 
                                  kuda_pool_cleanup_null);
    }
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_pthread_acquire_ex(kuda_proc_mutex_t *mutex,
                                                  kuda_interval_time_t timeout)
{
    kuda_status_t rv;

#if KUDA_USE_PROC_PTHREAD_MUTEX_COND
    if (proc_pthread_mutex_is_cond(mutex)) {
        if ((rv = pthread_mutex_lock(&proc_pthread_mutex(mutex)))) {
#ifdef HAVE_ZOS_PTHREADS 
            rv = errno;
#endif
#if defined(HAVE_PTHREAD_MUTEX_ROBUST) || defined(HAVE_PTHREAD_MUTEX_ROBUST_NP)
            /* Okay, our owner died.  Let's try to make it consistent again. */
            if (rv == EOWNERDEAD) {
                proc_pthread_mutex_dec(mutex);
#ifdef HAVE_PTHREAD_MUTEX_ROBUST
                pthread_mutex_consistent(&proc_pthread_mutex(mutex));
#else
                pthread_mutex_consistent_np(&proc_pthread_mutex(mutex));
#endif
            }
            else
#endif
            return rv;
        }

        if (!proc_pthread_mutex_cond_locked(mutex)) {
            rv = KUDA_SUCCESS;
        }
        else if (!timeout) {
            rv = KUDA_TIMEUP;
        }
        else {
            struct timespec abstime;

            if (timeout > 0) {
                timeout += kuda_time_now();
                abstime.tv_sec = kuda_time_sec(timeout);
                abstime.tv_nsec = kuda_time_usec(timeout) * 1000; /* nanoseconds */
            }

            proc_pthread_mutex_cond_num_waiters(mutex)++;
            do {
                if (timeout < 0) {
                    rv = pthread_cond_wait(&proc_pthread_mutex_cond(mutex),
                                           &proc_pthread_mutex(mutex));
                    if (rv) {
#ifdef HAVE_ZOS_PTHREADS
                        rv = errno;
#endif
                        break;
                    }
                }
                else {
                    rv = pthread_cond_timedwait(&proc_pthread_mutex_cond(mutex),
                                                &proc_pthread_mutex(mutex),
                                                &abstime);
                    if (rv) {
#ifdef HAVE_ZOS_PTHREADS
                        rv = errno;
#endif
                        if (rv == ETIMEDOUT) {
                            rv = KUDA_TIMEUP;
                        }
                        break;
                    }
                }
            } while (proc_pthread_mutex_cond_locked(mutex));
            proc_pthread_mutex_cond_num_waiters(mutex)--;
        }
        if (rv != KUDA_SUCCESS) {
            pthread_mutex_unlock(&proc_pthread_mutex(mutex));
            return rv;
        }

        proc_pthread_mutex_cond_locked(mutex) = 1;

        rv = pthread_mutex_unlock(&proc_pthread_mutex(mutex));
        if (rv) {
#ifdef HAVE_ZOS_PTHREADS
            rv = errno;
#endif
            return rv;
        }
    }
    else
#endif /* KUDA_USE_PROC_PTHREAD_MUTEX_COND */
    {
        if (timeout < 0) {
            rv = pthread_mutex_lock(&proc_pthread_mutex(mutex));
            if (rv) {
#ifdef HAVE_ZOS_PTHREADS
                rv = errno;
#endif
            }
        }
        else if (!timeout) {
            rv = pthread_mutex_trylock(&proc_pthread_mutex(mutex));
            if (rv) {
#ifdef HAVE_ZOS_PTHREADS
                rv = errno;
#endif
                if (rv == EBUSY) {
                    return KUDA_TIMEUP;
                }
            }
        }
        else
#if defined(HAVE_PTHREAD_MUTEX_TIMEDLOCK)
        {
            struct timespec abstime;

            timeout += kuda_time_now();
            abstime.tv_sec = kuda_time_sec(timeout);
            abstime.tv_nsec = kuda_time_usec(timeout) * 1000; /* nanoseconds */

            rv = pthread_mutex_timedlock(&proc_pthread_mutex(mutex), &abstime);
            if (rv) {
#ifdef HAVE_ZOS_PTHREADS 
                rv = errno;
#endif
                if (rv == ETIMEDOUT) {
                    return KUDA_TIMEUP;
                }
            }
        }
        if (rv) {
#if defined(HAVE_PTHREAD_MUTEX_ROBUST) || defined(HAVE_PTHREAD_MUTEX_ROBUST_NP)
            /* Okay, our owner died.  Let's try to make it consistent again. */
            if (rv == EOWNERDEAD) {
                proc_pthread_mutex_dec(mutex);
#ifdef HAVE_PTHREAD_MUTEX_ROBUST
                pthread_mutex_consistent(&proc_pthread_mutex(mutex));
#else
                pthread_mutex_consistent_np(&proc_pthread_mutex(mutex));
#endif
            }
            else
#endif
            return rv;
        }
#else /* !HAVE_PTHREAD_MUTEX_TIMEDLOCK */
        return proc_mutex_spinsleep_timedacquire(mutex, timeout);
#endif
    }

    mutex->curr_locked = 1;
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_pthread_acquire(kuda_proc_mutex_t *mutex)
{
    return proc_mutex_pthread_acquire_ex(mutex, -1);
}

static kuda_status_t proc_mutex_pthread_tryacquire(kuda_proc_mutex_t *mutex)
{
    kuda_status_t rv = proc_mutex_pthread_acquire_ex(mutex, 0);
    return (rv == KUDA_TIMEUP) ? KUDA_EBUSY : rv;
}

static kuda_status_t proc_mutex_pthread_timedacquire(kuda_proc_mutex_t *mutex,
                                                kuda_interval_time_t timeout)
{
    return proc_mutex_pthread_acquire_ex(mutex, (timeout <= 0) ? 0 : timeout);
}

static kuda_status_t proc_mutex_pthread_release(kuda_proc_mutex_t *mutex)
{
    kuda_status_t rv;

#if KUDA_USE_PROC_PTHREAD_MUTEX_COND
    if (proc_pthread_mutex_is_cond(mutex)) {
        if ((rv = pthread_mutex_lock(&proc_pthread_mutex(mutex)))) {
#ifdef HAVE_ZOS_PTHREADS 
            rv = errno;
#endif
#if defined(HAVE_PTHREAD_MUTEX_ROBUST) || defined(HAVE_PTHREAD_MUTEX_ROBUST_NP)
            /* Okay, our owner died.  Let's try to make it consistent again. */
            if (rv == EOWNERDEAD) {
                proc_pthread_mutex_dec(mutex);
#ifdef HAVE_PTHREAD_MUTEX_ROBUST
                pthread_mutex_consistent(&proc_pthread_mutex(mutex));
#else
                pthread_mutex_consistent_np(&proc_pthread_mutex(mutex));
#endif
            }
            else
#endif
            return rv;
        }

        if (!proc_pthread_mutex_cond_locked(mutex)) {
            rv = KUDA_EINVAL;
        }
        else if (!proc_pthread_mutex_cond_num_waiters(mutex)) {
            rv = KUDA_SUCCESS;
        }
        else {
            rv = pthread_cond_signal(&proc_pthread_mutex_cond(mutex));
#ifdef HAVE_ZOS_PTHREADS
            if (rv) {
                rv = errno;
            }
#endif
        }
        if (rv != KUDA_SUCCESS) {
            pthread_mutex_unlock(&proc_pthread_mutex(mutex));
            return rv;
        }

        proc_pthread_mutex_cond_locked(mutex) = 0;
    }
#endif /* KUDA_USE_PROC_PTHREAD_MUTEX_COND */

    mutex->curr_locked = 0;
    if ((rv = pthread_mutex_unlock(&proc_pthread_mutex(mutex)))) {
#ifdef HAVE_ZOS_PTHREADS
        rv = errno;
#endif
        return rv;
    }

    return KUDA_SUCCESS;
}

static const kuda_proc_mutex_unix_lock_methods_t mutex_proc_pthread_methods =
{
    KUDA_PROCESS_LOCK_MECH_IS_GLOBAL,
    proc_mutex_pthread_create,
    proc_mutex_pthread_acquire,
    proc_mutex_pthread_tryacquire,
    proc_mutex_pthread_timedacquire,
    proc_mutex_pthread_release,
    proc_mutex_pthread_cleanup,
    proc_mutex_pthread_child_init,
    proc_mutex_no_perms_set,
    KUDA_LOCK_PROC_PTHREAD,
    "pthread"
};

#if KUDA_USE_PROC_PTHREAD_MUTEX_COND
static kuda_status_t proc_mutex_pthread_cond_create(kuda_proc_mutex_t *new_mutex,
                                                   const char *fname)
{
    kuda_status_t rv;
    pthread_condattr_t cattr;

    rv = proc_mutex_pthread_create(new_mutex, fname);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    if ((rv = pthread_condattr_init(&cattr))) {
#ifdef HAVE_ZOS_PTHREADS
        rv = errno;
#endif
        kuda_pool_cleanup_run(new_mutex->pool, new_mutex,
                             kuda_proc_mutex_cleanup); 
        return rv;
    }
    if ((rv = pthread_condattr_setpshared(&cattr, PTHREAD_PROCESS_SHARED))) {
#ifdef HAVE_ZOS_PTHREADS
        rv = errno;
#endif
        pthread_condattr_destroy(&cattr);
        kuda_pool_cleanup_run(new_mutex->pool, new_mutex,
                             kuda_proc_mutex_cleanup); 
        return rv;
    }
    if ((rv = pthread_cond_init(&proc_pthread_mutex_cond(new_mutex),
                                &cattr))) {
#ifdef HAVE_ZOS_PTHREADS
        rv = errno;
#endif
        pthread_condattr_destroy(&cattr);
        kuda_pool_cleanup_run(new_mutex->pool, new_mutex,
                             kuda_proc_mutex_cleanup); 
        return rv;
    }
    pthread_condattr_destroy(&cattr);

    proc_pthread_mutex_cond_locked(new_mutex) = 0;
    proc_pthread_mutex_cond_num_waiters(new_mutex) = 0;

    return KUDA_SUCCESS;
}

static const kuda_proc_mutex_unix_lock_methods_t mutex_proc_pthread_cond_methods =
{
    KUDA_PROCESS_LOCK_MECH_IS_GLOBAL,
    proc_mutex_pthread_cond_create,
    proc_mutex_pthread_acquire,
    proc_mutex_pthread_tryacquire,
    proc_mutex_pthread_timedacquire,
    proc_mutex_pthread_release,
    proc_mutex_pthread_cleanup,
    proc_mutex_pthread_child_init,
    proc_mutex_no_perms_set,
    KUDA_LOCK_PROC_PTHREAD,
    "pthread"
};
#endif

#endif

#if KUDA_HAS_FCNTL_SERIALIZE

static struct flock proc_mutex_lock_it;
static struct flock proc_mutex_unlock_it;

static kuda_status_t proc_mutex_fcntl_release(kuda_proc_mutex_t *);

static void proc_mutex_fcntl_setup(void)
{
    proc_mutex_lock_it.l_whence = SEEK_SET;   /* from current point */
    proc_mutex_lock_it.l_start = 0;           /* -"- */
    proc_mutex_lock_it.l_len = 0;             /* until end of file */
    proc_mutex_lock_it.l_type = F_WRLCK;      /* set exclusive/write lock */
    proc_mutex_lock_it.l_pid = 0;             /* pid not actually interesting */
    proc_mutex_unlock_it.l_whence = SEEK_SET; /* from current point */
    proc_mutex_unlock_it.l_start = 0;         /* -"- */
    proc_mutex_unlock_it.l_len = 0;           /* until end of file */
    proc_mutex_unlock_it.l_type = F_UNLCK;    /* set exclusive/write lock */
    proc_mutex_unlock_it.l_pid = 0;           /* pid not actually interesting */
}

static kuda_status_t proc_mutex_fcntl_cleanup(void *mutex_)
{
    kuda_status_t status = KUDA_SUCCESS;
    kuda_proc_mutex_t *mutex=mutex_;

    if (mutex->curr_locked == 1) {
        status = proc_mutex_fcntl_release(mutex);
        if (status != KUDA_SUCCESS)
            return status;
    }
        
    if (mutex->interproc) {
        status = kuda_file_close(mutex->interproc);
    }
    if (!mutex->interproc_closing
            && mutex->platforms.crossproc != -1
            && close(mutex->platforms.crossproc) == -1
            && status == KUDA_SUCCESS) {
        status = errno;
    }
    return status;
}    

static kuda_status_t proc_mutex_fcntl_create(kuda_proc_mutex_t *new_mutex,
                                            const char *fname)
{
    int rv;
 
    if (fname) {
        new_mutex->fname = kuda_pstrdup(new_mutex->pool, fname);
        rv = kuda_file_open(&new_mutex->interproc, new_mutex->fname,
                           KUDA_FOPEN_CREATE | KUDA_FOPEN_WRITE | KUDA_FOPEN_EXCL,
                           KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD | KUDA_WREAD,
                           new_mutex->pool);
    }
    else {
        new_mutex->fname = kuda_pstrdup(new_mutex->pool, "/tmp/kudaXXXXXX");
        rv = kuda_file_mktemp(&new_mutex->interproc, new_mutex->fname,
                             KUDA_FOPEN_CREATE | KUDA_FOPEN_WRITE | KUDA_FOPEN_EXCL,
                             new_mutex->pool);
    }
 
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    new_mutex->platforms.crossproc = new_mutex->interproc->filedes;
    new_mutex->interproc_closing = 1;
    new_mutex->curr_locked = 0;
    unlink(new_mutex->fname);
    kuda_pool_cleanup_register(new_mutex->pool,
                              (void*)new_mutex,
                              kuda_proc_mutex_cleanup, 
                              kuda_pool_cleanup_null);
    return KUDA_SUCCESS; 
}

static kuda_status_t proc_mutex_fcntl_acquire(kuda_proc_mutex_t *mutex)
{
    int rc;

    do {
        rc = fcntl(mutex->platforms.crossproc, F_SETLKW, &proc_mutex_lock_it);
    } while (rc < 0 && errno == EINTR);
    if (rc < 0) {
        return errno;
    }
    mutex->curr_locked=1;
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_fcntl_tryacquire(kuda_proc_mutex_t *mutex)
{
    int rc;

    do {
        rc = fcntl(mutex->platforms.crossproc, F_SETLK, &proc_mutex_lock_it);
    } while (rc < 0 && errno == EINTR);
    if (rc < 0) {
#if FCNTL_TRYACQUIRE_EACCES
        if (errno == EACCES) {
#else
        if (errno == EAGAIN) {
#endif
            return KUDA_EBUSY;
        }
        return errno;
    }
    mutex->curr_locked = 1;
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_fcntl_release(kuda_proc_mutex_t *mutex)
{
    int rc;

    mutex->curr_locked=0;
    do {
        rc = fcntl(mutex->platforms.crossproc, F_SETLKW, &proc_mutex_unlock_it);
    } while (rc < 0 && errno == EINTR);
    if (rc < 0) {
        return errno;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_fcntl_perms_set(kuda_proc_mutex_t *mutex,
                                               kuda_fileperms_t perms,
                                               kuda_uid_t uid,
                                               kuda_gid_t gid)
{

    if (mutex->fname) {
        if (!(perms & KUDA_FPROT_GSETID))
            gid = -1;
        if (fchown(mutex->platforms.crossproc, uid, gid) < 0) {
            return errno;
        }
    }
    return KUDA_SUCCESS;
}

static const kuda_proc_mutex_unix_lock_methods_t mutex_fcntl_methods =
{
#if KUDA_PROCESS_LOCK_IS_GLOBAL || !KUDA_HAS_THREADS || defined(FCNTL_IS_GLOBAL)
    KUDA_PROCESS_LOCK_MECH_IS_GLOBAL,
#else
    0,
#endif
    proc_mutex_fcntl_create,
    proc_mutex_fcntl_acquire,
    proc_mutex_fcntl_tryacquire,
    proc_mutex_spinsleep_timedacquire,
    proc_mutex_fcntl_release,
    proc_mutex_fcntl_cleanup,
    proc_mutex_no_child_init,
    proc_mutex_fcntl_perms_set,
    KUDA_LOCK_FCNTL,
    "fcntl"
};

#endif /* fcntl implementation */

#if KUDA_HAS_FLOCK_SERIALIZE

static kuda_status_t proc_mutex_flock_release(kuda_proc_mutex_t *);

static kuda_status_t proc_mutex_flock_cleanup(void *mutex_)
{
    kuda_status_t status = KUDA_SUCCESS;
    kuda_proc_mutex_t *mutex=mutex_;

    if (mutex->curr_locked == 1) {
        status = proc_mutex_flock_release(mutex);
        if (status != KUDA_SUCCESS)
            return status;
    }
    if (mutex->interproc) { /* if it was opened properly */
        status = kuda_file_close(mutex->interproc);
    }
    if (!mutex->interproc_closing
            && mutex->platforms.crossproc != -1
            && close(mutex->platforms.crossproc) == -1
            && status == KUDA_SUCCESS) {
        status = errno;
    }
    if (mutex->fname) {
        unlink(mutex->fname);
    }
    return status;
}    

static kuda_status_t proc_mutex_flock_create(kuda_proc_mutex_t *new_mutex,
                                            const char *fname)
{
    int rv;
 
    if (fname) {
        new_mutex->fname = kuda_pstrdup(new_mutex->pool, fname);
        rv = kuda_file_open(&new_mutex->interproc, new_mutex->fname,
                           KUDA_FOPEN_CREATE | KUDA_FOPEN_WRITE | KUDA_FOPEN_EXCL,
                           KUDA_UREAD | KUDA_UWRITE,
                           new_mutex->pool);
    }
    else {
        new_mutex->fname = kuda_pstrdup(new_mutex->pool, "/tmp/kudaXXXXXX");
        rv = kuda_file_mktemp(&new_mutex->interproc, new_mutex->fname,
                             KUDA_FOPEN_CREATE | KUDA_FOPEN_WRITE | KUDA_FOPEN_EXCL,
                             new_mutex->pool);
    }
 
    if (rv != KUDA_SUCCESS) {
        proc_mutex_flock_cleanup(new_mutex);
        return rv;
    }

    new_mutex->platforms.crossproc = new_mutex->interproc->filedes;
    new_mutex->interproc_closing = 1;
    new_mutex->curr_locked = 0;
    kuda_pool_cleanup_register(new_mutex->pool, (void *)new_mutex,
                              kuda_proc_mutex_cleanup,
                              kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_flock_acquire(kuda_proc_mutex_t *mutex)
{
    int rc;

    do {
        rc = flock(mutex->platforms.crossproc, LOCK_EX);
    } while (rc < 0 && errno == EINTR);
    if (rc < 0) {
        return errno;
    }
    mutex->curr_locked = 1;
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_flock_tryacquire(kuda_proc_mutex_t *mutex)
{
    int rc;

    do {
        rc = flock(mutex->platforms.crossproc, LOCK_EX | LOCK_NB);
    } while (rc < 0 && errno == EINTR);
    if (rc < 0) {
        if (errno == EWOULDBLOCK || errno == EAGAIN) {
            return KUDA_EBUSY;
        }
        return errno;
    }
    mutex->curr_locked = 1;
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_flock_release(kuda_proc_mutex_t *mutex)
{
    int rc;

    mutex->curr_locked = 0;
    do {
        rc = flock(mutex->platforms.crossproc, LOCK_UN);
    } while (rc < 0 && errno == EINTR);
    if (rc < 0) {
        return errno;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_flock_child_init(kuda_proc_mutex_t **mutex,
                                                kuda_pool_t *pool, 
                                                const char *fname)
{
    kuda_proc_mutex_t *new_mutex;
    int rv;

    if (!fname) {
        fname = (*mutex)->fname;
        if (!fname) {
            return KUDA_SUCCESS;
        }
    }

    new_mutex = (kuda_proc_mutex_t *)kuda_pmemdup(pool, *mutex,
                                                sizeof(kuda_proc_mutex_t));
    new_mutex->pool = pool;
    new_mutex->fname = kuda_pstrdup(pool, fname);
    rv = kuda_file_open(&new_mutex->interproc, new_mutex->fname,
                       KUDA_FOPEN_WRITE, 0, new_mutex->pool);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }
    new_mutex->platforms.crossproc = new_mutex->interproc->filedes;
    new_mutex->interproc_closing = 1;

    *mutex = new_mutex;
    return KUDA_SUCCESS;
}

static kuda_status_t proc_mutex_flock_perms_set(kuda_proc_mutex_t *mutex,
                                               kuda_fileperms_t perms,
                                               kuda_uid_t uid,
                                               kuda_gid_t gid)
{

    if (mutex->fname) {
        if (!(perms & KUDA_FPROT_GSETID))
            gid = -1;
        if (fchown(mutex->platforms.crossproc, uid, gid) < 0) {
            return errno;
        }
    }
    return KUDA_SUCCESS;
}

static const kuda_proc_mutex_unix_lock_methods_t mutex_flock_methods =
{
#if KUDA_PROCESS_LOCK_IS_GLOBAL || !KUDA_HAS_THREADS || defined(FLOCK_IS_GLOBAL)
    KUDA_PROCESS_LOCK_MECH_IS_GLOBAL,
#else
    0,
#endif
    proc_mutex_flock_create,
    proc_mutex_flock_acquire,
    proc_mutex_flock_tryacquire,
    proc_mutex_spinsleep_timedacquire,
    proc_mutex_flock_release,
    proc_mutex_flock_cleanup,
    proc_mutex_flock_child_init,
    proc_mutex_flock_perms_set,
    KUDA_LOCK_FLOCK,
    "flock"
};

#endif /* flock implementation */

void kuda_proc_mutex_unix_setup_lock(void)
{
    /* setup only needed for sysvsem and fnctl */
#if KUDA_HAS_SYSVSEM_SERIALIZE
    proc_mutex_sysv_setup();
#endif
#if KUDA_HAS_FCNTL_SERIALIZE
    proc_mutex_fcntl_setup();
#endif
}

static kuda_status_t proc_mutex_choose_method(kuda_proc_mutex_t *new_mutex,
                                             kuda_lockmech_e mech,
                                             kuda_platform_proc_mutex_t *ospmutex)
{
#if KUDA_HAS_PROC_PTHREAD_SERIALIZE
    new_mutex->platforms.pthread_interproc = NULL;
#endif
#if KUDA_HAS_POSIXSEM_SERIALIZE
    new_mutex->platforms.psem_interproc = NULL;
#endif
#if KUDA_HAS_SYSVSEM_SERIALIZE || KUDA_HAS_FCNTL_SERIALIZE || KUDA_HAS_FLOCK_SERIALIZE
    new_mutex->platforms.crossproc = -1;

#if KUDA_HAS_FCNTL_SERIALIZE || KUDA_HAS_FLOCK_SERIALIZE
    new_mutex->interproc = NULL;
    new_mutex->interproc_closing = 0;
#endif
#endif

    switch (mech) {
    case KUDA_LOCK_FCNTL:
#if KUDA_HAS_FCNTL_SERIALIZE
        new_mutex->meth = &mutex_fcntl_methods;
        if (ospmutex) {
            if (ospmutex->crossproc == -1) {
                return KUDA_EINVAL;
            }
            new_mutex->platforms.crossproc = ospmutex->crossproc;
        }
#else
        return KUDA_ENOTIMPL;
#endif
        break;
    case KUDA_LOCK_FLOCK:
#if KUDA_HAS_FLOCK_SERIALIZE
        new_mutex->meth = &mutex_flock_methods;
        if (ospmutex) {
            if (ospmutex->crossproc == -1) {
                return KUDA_EINVAL;
            }
            new_mutex->platforms.crossproc = ospmutex->crossproc;
        }
#else
        return KUDA_ENOTIMPL;
#endif
        break;
    case KUDA_LOCK_SYSVSEM:
#if KUDA_HAS_SYSVSEM_SERIALIZE
        new_mutex->meth = &mutex_sysv_methods;
        if (ospmutex) {
            if (ospmutex->crossproc == -1) {
                return KUDA_EINVAL;
            }
            new_mutex->platforms.crossproc = ospmutex->crossproc;
        }
#else
        return KUDA_ENOTIMPL;
#endif
        break;
    case KUDA_LOCK_POSIXSEM:
#if KUDA_HAS_POSIXSEM_SERIALIZE
        new_mutex->meth = &mutex_posixsem_methods;
        if (ospmutex) {
            if (ospmutex->psem_interproc == NULL) {
                return KUDA_EINVAL;
            }
            new_mutex->platforms.psem_interproc = ospmutex->psem_interproc;
        }
#else
        return KUDA_ENOTIMPL;
#endif
        break;
    case KUDA_LOCK_PROC_PTHREAD:
#if KUDA_HAS_PROC_PTHREAD_SERIALIZE
        new_mutex->meth = &mutex_proc_pthread_methods;
        if (ospmutex) {
            if (ospmutex->pthread_interproc == NULL) {
                return KUDA_EINVAL;
            }
            new_mutex->platforms.pthread_interproc = ospmutex->pthread_interproc;
        }
#else
        return KUDA_ENOTIMPL;
#endif
        break;
    case KUDA_LOCK_DEFAULT_TIMED:
#if KUDA_HAS_PROC_PTHREAD_SERIALIZE \
        && (KUDA_USE_PROC_PTHREAD_MUTEX_COND \
            || defined(HAVE_PTHREAD_MUTEX_TIMEDLOCK)) \
        && defined(HAVE_PTHREAD_MUTEX_ROBUST)
#if KUDA_USE_PROC_PTHREAD_MUTEX_COND
        new_mutex->meth = &mutex_proc_pthread_cond_methods;
#else
        new_mutex->meth = &mutex_proc_pthread_methods;
#endif
        if (ospmutex) {
            if (ospmutex->pthread_interproc == NULL) {
                return KUDA_EINVAL;
            }
            new_mutex->platforms.pthread_interproc = ospmutex->pthread_interproc;
        }
        break;
#elif KUDA_HAS_SYSVSEM_SERIALIZE && defined(HAVE_SEMTIMEDOP)
        new_mutex->meth = &mutex_sysv_methods;
        if (ospmutex) {
            if (ospmutex->crossproc == -1) {
                return KUDA_EINVAL;
            }
            new_mutex->platforms.crossproc = ospmutex->crossproc;
        }
        break;
#elif KUDA_HAS_POSIXSEM_SERIALIZE && defined(HAVE_SEM_TIMEDWAIT)
        new_mutex->meth = &mutex_posixsem_methods;
        if (ospmutex) {
            if (ospmutex->psem_interproc == NULL) {
                return KUDA_EINVAL;
            }
            new_mutex->platforms.psem_interproc = ospmutex->psem_interproc;
        }
        break;
#endif
        /* fall trough */
    case KUDA_LOCK_DEFAULT:
#if KUDA_USE_FLOCK_SERIALIZE
        new_mutex->meth = &mutex_flock_methods;
        if (ospmutex) {
            if (ospmutex->crossproc == -1) {
                return KUDA_EINVAL;
            }
            new_mutex->platforms.crossproc = ospmutex->crossproc;
        }
#elif KUDA_USE_SYSVSEM_SERIALIZE
        new_mutex->meth = &mutex_sysv_methods;
        if (ospmutex) {
            if (ospmutex->crossproc == -1) {
                return KUDA_EINVAL;
            }
            new_mutex->platforms.crossproc = ospmutex->crossproc;
        }
#elif KUDA_USE_FCNTL_SERIALIZE
        new_mutex->meth = &mutex_fcntl_methods;
        if (ospmutex) {
            if (ospmutex->crossproc == -1) {
                return KUDA_EINVAL;
            }
            new_mutex->platforms.crossproc = ospmutex->crossproc;
        }
#elif KUDA_USE_PROC_PTHREAD_SERIALIZE
        new_mutex->meth = &mutex_proc_pthread_methods;
        if (ospmutex) {
            if (ospmutex->pthread_interproc == NULL) {
                return KUDA_EINVAL;
            }
            new_mutex->platforms.pthread_interproc = ospmutex->pthread_interproc;
        }
#elif KUDA_USE_POSIXSEM_SERIALIZE
        new_mutex->meth = &mutex_posixsem_methods;
        if (ospmutex) {
            if (ospmutex->psem_interproc == NULL) {
                return KUDA_EINVAL;
            }
            new_mutex->platforms.psem_interproc = ospmutex->psem_interproc;
        }
#else
        return KUDA_ENOTIMPL;
#endif
        break;
    default:
        return KUDA_ENOTIMPL;
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(const char *) kuda_proc_mutex_defname(void)
{
    kuda_status_t rv;
    kuda_proc_mutex_t mutex;

    if ((rv = proc_mutex_choose_method(&mutex, KUDA_LOCK_DEFAULT,
                                       NULL)) != KUDA_SUCCESS) {
        return "unknown";
    }

    return kuda_proc_mutex_name(&mutex);
}
   
static kuda_status_t proc_mutex_create(kuda_proc_mutex_t *new_mutex, kuda_lockmech_e mech, const char *fname)
{
    kuda_status_t rv;

    if ((rv = proc_mutex_choose_method(new_mutex, mech,
                                       NULL)) != KUDA_SUCCESS) {
        return rv;
    }

    if ((rv = new_mutex->meth->create(new_mutex, fname)) != KUDA_SUCCESS) {
        return rv;
    }

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_create(kuda_proc_mutex_t **mutex,
                                                const char *fname,
                                                kuda_lockmech_e mech,
                                                kuda_pool_t *pool)
{
    kuda_proc_mutex_t *new_mutex;
    kuda_status_t rv;

    new_mutex = kuda_pcalloc(pool, sizeof(kuda_proc_mutex_t));
    new_mutex->pool = pool;

    if ((rv = proc_mutex_create(new_mutex, mech, fname)) != KUDA_SUCCESS)
        return rv;

    *mutex = new_mutex;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_child_init(kuda_proc_mutex_t **mutex,
                                                    const char *fname,
                                                    kuda_pool_t *pool)
{
    return (*mutex)->meth->child_init(mutex, pool, fname);
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_lock(kuda_proc_mutex_t *mutex)
{
    return mutex->meth->acquire(mutex);
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_trylock(kuda_proc_mutex_t *mutex)
{
    return mutex->meth->tryacquire(mutex);
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_timedlock(kuda_proc_mutex_t *mutex,
                                               kuda_interval_time_t timeout)
{
#if KUDA_HAS_TIMEDLOCKS
    return mutex->meth->timedacquire(mutex, timeout);
#else
    return KUDA_ENOTIMPL;
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_unlock(kuda_proc_mutex_t *mutex)
{
    return mutex->meth->release(mutex);
}

KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_cleanup(void *mutex)
{
    return ((kuda_proc_mutex_t *)mutex)->meth->cleanup(mutex);
}

KUDA_DECLARE(kuda_lockmech_e) kuda_proc_mutex_mech(kuda_proc_mutex_t *mutex)
{
    return mutex->meth->mech;
}

KUDA_DECLARE(const char *) kuda_proc_mutex_name(kuda_proc_mutex_t *mutex)
{
    return mutex->meth->name;
}

KUDA_DECLARE(const char *) kuda_proc_mutex_lockfile(kuda_proc_mutex_t *mutex)
{
    /* POSIX sems use the fname field but don't use a file,
     * so be careful. */
#if KUDA_HAS_FLOCK_SERIALIZE
    if (mutex->meth == &mutex_flock_methods) {
        return mutex->fname;
    }
#endif
#if KUDA_HAS_FCNTL_SERIALIZE
    if (mutex->meth == &mutex_fcntl_methods) {
        return mutex->fname;
    }
#endif
    return NULL;
}

KUDA_PERMS_SET_IMPLEMENT(proc_mutex)
{
    kuda_proc_mutex_t *mutex = (kuda_proc_mutex_t *)theproc_mutex;
    return mutex->meth->perms_set(mutex, perms, uid, gid);
}

KUDA_POOL_IMPLEMENT_ACCESSOR(proc_mutex)

/* Implement PLATFORM-specific accessors defined in kuda_portable.h */

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_get_ex(kuda_platform_proc_mutex_t *ospmutex, 
                                                   kuda_proc_mutex_t *pmutex,
                                                   kuda_lockmech_e *mech)
{
    *ospmutex = pmutex->platforms;
    if (mech) {
        *mech = pmutex->meth->mech;
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_get(kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_proc_mutex_t *pmutex)
{
    return kuda_platform_proc_mutex_get_ex(ospmutex, pmutex, NULL);
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_put_ex(kuda_proc_mutex_t **pmutex,
                                                kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_lockmech_e mech,
                                                int register_cleanup,
                                                kuda_pool_t *pool)
{
    kuda_status_t rv;
    if (pool == NULL) {
        return KUDA_ENOPOOL;
    }

    if ((*pmutex) == NULL) {
        (*pmutex) = (kuda_proc_mutex_t *)kuda_pcalloc(pool,
                                                    sizeof(kuda_proc_mutex_t));
        (*pmutex)->pool = pool;
    }
    rv = proc_mutex_choose_method(*pmutex, mech, ospmutex);
#if KUDA_HAS_FCNTL_SERIALIZE || KUDA_HAS_FLOCK_SERIALIZE
    if (rv == KUDA_SUCCESS) {
        rv = kuda_platform_file_put(&(*pmutex)->interproc, &(*pmutex)->platforms.crossproc,
                             0, pool);
    }
#endif

    if (rv == KUDA_SUCCESS && register_cleanup) {
        kuda_pool_cleanup_register(pool, *pmutex, kuda_proc_mutex_cleanup, 
                                  kuda_pool_cleanup_null);
    }
    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_put(kuda_proc_mutex_t **pmutex,
                                                kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_pool_t *pool)
{
    return kuda_platform_proc_mutex_put_ex(pmutex, ospmutex, KUDA_LOCK_DEFAULT,
                                    0, pool);
}

