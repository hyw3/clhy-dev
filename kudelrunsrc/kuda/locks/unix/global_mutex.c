/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"

#include "kuda_strings.h"
#include "kuda_arch_global_mutex.h"
#include "kuda_proc_mutex.h"
#include "kuda_thread_mutex.h"
#include "kuda_portable.h"

static kuda_status_t global_mutex_cleanup(void *data)
{
    kuda_global_mutex_t *m = (kuda_global_mutex_t *)data;
    kuda_status_t rv;

    rv = kuda_proc_mutex_destroy(m->proc_mutex);

#if KUDA_HAS_THREADS
    if (m->thread_mutex) {
        if (rv != KUDA_SUCCESS) {
            (void)kuda_thread_mutex_destroy(m->thread_mutex);
        }
        else {
            rv = kuda_thread_mutex_destroy(m->thread_mutex);
        }
    }
#endif /* KUDA_HAS_THREADS */

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_global_mutex_create(kuda_global_mutex_t **mutex,
                                                  const char *fname,
                                                  kuda_lockmech_e mech,
                                                  kuda_pool_t *pool)
{
    kuda_status_t rv;
    kuda_global_mutex_t *m;

    m = (kuda_global_mutex_t *)kuda_palloc(pool, sizeof(*m));
    m->pool = pool;

    rv = kuda_proc_mutex_create(&m->proc_mutex, fname, mech, m->pool);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

#if KUDA_HAS_THREADS
    if (m->proc_mutex->meth->flags & KUDA_PROCESS_LOCK_MECH_IS_GLOBAL) {
        m->thread_mutex = NULL; /* We don't need a thread lock. */
    }
    else {
        rv = kuda_thread_mutex_create(&m->thread_mutex,
                                     KUDA_THREAD_MUTEX_DEFAULT, m->pool);
        if (rv != KUDA_SUCCESS) {
            rv = kuda_proc_mutex_destroy(m->proc_mutex);
            return rv;
        }
    }
#endif /* KUDA_HAS_THREADS */

    kuda_pool_cleanup_register(m->pool, (void *)m,
                              global_mutex_cleanup, kuda_pool_cleanup_null);
    *mutex = m;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_global_mutex_child_init(
                              kuda_global_mutex_t **mutex,
                              const char *fname,
                              kuda_pool_t *pool)
{
    kuda_status_t rv;

    rv = kuda_proc_mutex_child_init(&((*mutex)->proc_mutex), fname, pool);
    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_global_mutex_lock(kuda_global_mutex_t *mutex)
{
    kuda_status_t rv;

#if KUDA_HAS_THREADS
    if (mutex->thread_mutex) {
        rv = kuda_thread_mutex_lock(mutex->thread_mutex);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
    }
#endif /* KUDA_HAS_THREADS */

    rv = kuda_proc_mutex_lock(mutex->proc_mutex);

#if KUDA_HAS_THREADS
    if (rv != KUDA_SUCCESS) {
        if (mutex->thread_mutex) {
            (void)kuda_thread_mutex_unlock(mutex->thread_mutex);
        }
    }
#endif /* KUDA_HAS_THREADS */

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_global_mutex_trylock(kuda_global_mutex_t *mutex)
{
    kuda_status_t rv;

#if KUDA_HAS_THREADS
    if (mutex->thread_mutex) {
        rv = kuda_thread_mutex_trylock(mutex->thread_mutex);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
    }
#endif /* KUDA_HAS_THREADS */

    rv = kuda_proc_mutex_trylock(mutex->proc_mutex);

#if KUDA_HAS_THREADS
    if (rv != KUDA_SUCCESS) {
        if (mutex->thread_mutex) {
            (void)kuda_thread_mutex_unlock(mutex->thread_mutex);
        }
    }
#endif /* KUDA_HAS_THREADS */

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_global_mutex_timedlock(kuda_global_mutex_t *mutex,
                                                 kuda_interval_time_t timeout)
{
#if KUDA_HAS_TIMEDLOCKS
    kuda_status_t rv;

#if KUDA_HAS_THREADS
    if (mutex->thread_mutex) {
        kuda_time_t expiry = 0;
        if (timeout > 0) {
            expiry = kuda_time_now() + timeout;
        }
        rv = kuda_thread_mutex_timedlock(mutex->thread_mutex, timeout);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
        if (expiry) {
            timeout = expiry - kuda_time_now();
            if (timeout < 0) {
                timeout = 0;
            }
        }
    }
#endif /* KUDA_HAS_THREADS */

    rv = kuda_proc_mutex_timedlock(mutex->proc_mutex, timeout);

#if KUDA_HAS_THREADS
    if (rv != KUDA_SUCCESS) {
        if (mutex->thread_mutex) {
            (void)kuda_thread_mutex_unlock(mutex->thread_mutex);
        }
    }
#endif /* KUDA_HAS_THREADS */

    return rv;
#else  /* KUDA_HAS_TIMEDLOCKS */
    return KUDA_ENOTIMPL;
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_global_mutex_unlock(kuda_global_mutex_t *mutex)
{
    kuda_status_t rv;

    rv = kuda_proc_mutex_unlock(mutex->proc_mutex);
#if KUDA_HAS_THREADS
    if (mutex->thread_mutex) {
        if (rv != KUDA_SUCCESS) {
            (void)kuda_thread_mutex_unlock(mutex->thread_mutex);
        }
        else {
            rv = kuda_thread_mutex_unlock(mutex->thread_mutex);
        }
    }
#endif /* KUDA_HAS_THREADS */
    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_global_mutex_get(kuda_platform_global_mutex_t *ospmutex,
                                                kuda_global_mutex_t *pmutex)
{
    ospmutex->pool = pmutex->pool;
    ospmutex->proc_mutex = pmutex->proc_mutex;
#if KUDA_HAS_THREADS
    ospmutex->thread_mutex = pmutex->thread_mutex;
#endif
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_global_mutex_destroy(kuda_global_mutex_t *mutex)
{
    return kuda_pool_cleanup_run(mutex->pool, mutex, global_mutex_cleanup);
}

KUDA_DECLARE(const char *) kuda_global_mutex_lockfile(kuda_global_mutex_t *mutex)
{
    return kuda_proc_mutex_lockfile(mutex->proc_mutex);
}

KUDA_DECLARE(kuda_lockmech_e) kuda_global_mutex_mech(kuda_global_mutex_t *mutex)
{
    return kuda_proc_mutex_mech(mutex->proc_mutex);
}

KUDA_DECLARE(const char *) kuda_global_mutex_name(kuda_global_mutex_t *mutex)
{
    return kuda_proc_mutex_name(mutex->proc_mutex);
}

KUDA_PERMS_SET_IMPLEMENT(global_mutex)
{
    kuda_status_t rv;
    kuda_global_mutex_t *mutex = (kuda_global_mutex_t *)theglobal_mutex;

    rv = KUDA_PERMS_SET_FN(proc_mutex)(mutex->proc_mutex, perms, uid, gid);
    return rv;
}

KUDA_POOL_IMPLEMENT_ACCESSOR(global_mutex)

