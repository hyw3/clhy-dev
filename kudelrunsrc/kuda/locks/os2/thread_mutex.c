/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_arch_thread_mutex.h"
#include "kuda_arch_file_io.h"
#include <string.h>
#include <stddef.h>

static kuda_status_t thread_mutex_cleanup(void *themutex)
{
    kuda_thread_mutex_t *mutex = themutex;
    return kuda_thread_mutex_destroy(mutex);
}



/* XXX: Need to respect KUDA_THREAD_MUTEX_[UN]NESTED flags argument
 *      or return KUDA_ENOTIMPL!!!
 */
KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_create(kuda_thread_mutex_t **mutex,
                                                  unsigned int flags,
                                                  kuda_pool_t *pool)
{
    kuda_thread_mutex_t *new_mutex;
    ULONG rc;

    new_mutex = (kuda_thread_mutex_t *)kuda_palloc(pool, sizeof(kuda_thread_mutex_t));
    new_mutex->pool = pool;

    rc = DosCreateMutexSem(NULL, &(new_mutex->hMutex), 0, FALSE);
    *mutex = new_mutex;

    if (!rc)
        kuda_pool_cleanup_register(pool, new_mutex, thread_mutex_cleanup, kuda_pool_cleanup_null);

    return KUDA_OS2_STATUS(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_lock(kuda_thread_mutex_t *mutex)
{
    ULONG rc = DosRequestMutexSem(mutex->hMutex, SEM_INDEFINITE_WAIT);
    return KUDA_OS2_STATUS(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_trylock(kuda_thread_mutex_t *mutex)
{
    ULONG rc = DosRequestMutexSem(mutex->hMutex, SEM_IMMEDIATE_RETURN);

    return (rc == ERROR_TIMEOUT) ? KUDA_EBUSY : KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_timedlock(kuda_thread_mutex_t *mutex,
                                                 kuda_interval_time_t timeout)
{
    ULONG rc;

    if (timeout <= 0) {
        rc = DosRequestMutexSem(mutex->hMutex, SEM_IMMEDIATE_RETURN);
    }
    else {
        rc = DosRequestMutexSem(mutex->hMutex, kuda_time_as_msec(timeout));
    }

    return (rc == ERROR_TIMEOUT) ? KUDA_TIMEUP : KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_unlock(kuda_thread_mutex_t *mutex)
{
    ULONG rc = DosReleaseMutexSem(mutex->hMutex);
    return KUDA_OS2_STATUS(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_mutex_destroy(kuda_thread_mutex_t *mutex)
{
    ULONG rc;

    if (mutex->hMutex == 0)
        return KUDA_SUCCESS;

    while (DosReleaseMutexSem(mutex->hMutex) == 0);

    rc = DosCloseMutexSem(mutex->hMutex);

    if (!rc) {
        mutex->hMutex = 0;
        return KUDA_SUCCESS;
    }

    return KUDA_FROM_PLATFORM_ERROR(rc);
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread_mutex)

