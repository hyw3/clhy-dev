/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_arch_thread_rwlock.h"
#include "kuda_arch_file_io.h"
#include <string.h>

static kuda_status_t thread_rwlock_cleanup(void *therwlock)
{
    kuda_thread_rwlock_t *rwlock = therwlock;
    return kuda_thread_rwlock_destroy(rwlock);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_create(kuda_thread_rwlock_t **rwlock,
                                                   kuda_pool_t *pool)
{
    kuda_thread_rwlock_t *new_rwlock;
    ULONG rc;

    new_rwlock = (kuda_thread_rwlock_t *)kuda_palloc(pool, sizeof(kuda_thread_rwlock_t));
    new_rwlock->pool = pool;
    new_rwlock->readers = 0;

    rc = DosCreateMutexSem(NULL, &(new_rwlock->write_lock), 0, FALSE);

    if (rc)
        return KUDA_FROM_PLATFORM_ERROR(rc);

    rc = DosCreateEventSem(NULL, &(new_rwlock->read_done), 0, FALSE);

    if (rc)
        return KUDA_FROM_PLATFORM_ERROR(rc);

    *rwlock = new_rwlock;

    if (!rc)
        kuda_pool_cleanup_register(pool, new_rwlock, thread_rwlock_cleanup,
                                  kuda_pool_cleanup_null);

    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_rdlock(kuda_thread_rwlock_t *rwlock)
{
    ULONG rc, posts;

    rc = DosRequestMutexSem(rwlock->write_lock, SEM_INDEFINITE_WAIT);

    if (rc)
        return KUDA_FROM_PLATFORM_ERROR(rc);

    /* We've successfully acquired the writer mutex so we can't be locked
     * for write which means it's ok to add a reader lock. The writer mutex
     * doubles as race condition protection for the readers counter.
     */
    rwlock->readers++;
    DosResetEventSem(rwlock->read_done, &posts);
    rc = DosReleaseMutexSem(rwlock->write_lock);
    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_tryrdlock(kuda_thread_rwlock_t *rwlock)
{
    /* As above but with different wait time */
    ULONG rc, posts;

    rc = DosRequestMutexSem(rwlock->write_lock, SEM_IMMEDIATE_RETURN);

    if (rc)
        return KUDA_FROM_PLATFORM_ERROR(rc);

    rwlock->readers++;
    DosResetEventSem(rwlock->read_done, &posts);
    rc = DosReleaseMutexSem(rwlock->write_lock);
    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_wrlock(kuda_thread_rwlock_t *rwlock)
{
    ULONG rc;

    rc = DosRequestMutexSem(rwlock->write_lock, SEM_INDEFINITE_WAIT);

    if (rc)
        return KUDA_FROM_PLATFORM_ERROR(rc);

    /* We've got the writer lock but we have to wait for all readers to
     * unlock before it's ok to use it
     */

    if (rwlock->readers) {
        rc = DosWaitEventSem(rwlock->read_done, SEM_INDEFINITE_WAIT);

        if (rc)
            DosReleaseMutexSem(rwlock->write_lock);
    }

    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_trywrlock(kuda_thread_rwlock_t *rwlock)
{
    ULONG rc;

    rc = DosRequestMutexSem(rwlock->write_lock, SEM_IMMEDIATE_RETURN);

    if (rc)
        return KUDA_FROM_PLATFORM_ERROR(rc);

    /* We've got the writer lock but we have to wait for all readers to
     * unlock before it's ok to use it
     */

    if (rwlock->readers) {
        /* There are readers active, give up */
        DosReleaseMutexSem(rwlock->write_lock);
        rc = ERROR_TIMEOUT;
    }

    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_unlock(kuda_thread_rwlock_t *rwlock)
{
    ULONG rc;

    /* First, guess that we're unlocking a writer */
    rc = DosReleaseMutexSem(rwlock->write_lock);

    if (rc == ERROR_NOT_OWNER) {
        /* Nope, we must have a read lock */
        if (rwlock->readers) {
            DosEnterCritSec();
            rwlock->readers--;

            if (rwlock->readers == 0) {
                DosPostEventSem(rwlock->read_done);
            }

            DosExitCritSec();
            rc = 0;
        }
    }

    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_rwlock_destroy(kuda_thread_rwlock_t *rwlock)
{
    ULONG rc;

    if (rwlock->write_lock == 0)
        return KUDA_SUCCESS;

    while (DosReleaseMutexSem(rwlock->write_lock) == 0);

    rc = DosCloseMutexSem(rwlock->write_lock);

    if (!rc) {
        rwlock->write_lock = 0;
        DosCloseEventSem(rwlock->read_done);
        return KUDA_SUCCESS;
    }

    return KUDA_FROM_PLATFORM_ERROR(rc);
}

KUDA_POOL_IMPLEMENT_ACCESSOR(thread_rwlock)

