/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_arch_proc_mutex.h"
#include "kuda_arch_file_io.h"
#include <string.h>
#include <stddef.h>

#define CurrentTid (*_threadid)

static char *fixed_name(const char *fname, kuda_pool_t *pool)
{
    char *semname;

    if (fname == NULL)
        semname = NULL;
    else {
        /* Semaphores don't live in the file system, fix up the name */
        while (*fname == '/' || *fname == '\\') {
            fname++;
        }

        semname = kuda_pstrcat(pool, "/SEM32/", fname, NULL);

        if (semname[8] == ':') {
            semname[8] = '$';
        }
    }

    return semname;
}



KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_cleanup(void *vmutex)
{
    kuda_proc_mutex_t *mutex = vmutex;
    return kuda_proc_mutex_destroy(mutex);
}

KUDA_DECLARE(const char *) kuda_proc_mutex_lockfile(kuda_proc_mutex_t *mutex)
{
    return NULL;
}

KUDA_DECLARE(kuda_lockmech_e) kuda_proc_mutex_mech(kuda_proc_mutex_t *mutex)
{
    return KUDA_LOCK_DEFAULT;
}

KUDA_DECLARE(const char *) kuda_proc_mutex_name(kuda_proc_mutex_t *mutex)
{
    return "os2sem";
}

KUDA_DECLARE(const char *) kuda_proc_mutex_defname(void)
{
    return "os2sem";
}


KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_create(kuda_proc_mutex_t **mutex,
                                                const char *fname,
                                                kuda_lockmech_e mech,
                                                kuda_pool_t *pool)
{
    kuda_proc_mutex_t *new;
    ULONG rc;
    char *semname;

    if (mech != KUDA_LOCK_DEFAULT && mech != KUDA_LOCK_DEFAULT_TIMED) {
        return KUDA_ENOTIMPL;
    }

    new = (kuda_proc_mutex_t *)kuda_palloc(pool, sizeof(kuda_proc_mutex_t));
    new->pool       = pool;
    new->owner      = 0;
    new->lock_count = 0;
    *mutex = new;

    semname = fixed_name(fname, pool);
    rc = DosCreateMutexSem(semname, &(new->hMutex), DC_SEM_SHARED, FALSE);

    if (!rc) {
        kuda_pool_cleanup_register(pool, new, kuda_proc_mutex_cleanup, kuda_pool_cleanup_null);
    }

    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_child_init(kuda_proc_mutex_t **mutex,
                                                    const char *fname,
                                                    kuda_pool_t *pool)
{
    kuda_proc_mutex_t *new;
    ULONG rc;
    char *semname;

    new = (kuda_proc_mutex_t *)kuda_palloc(pool, sizeof(kuda_proc_mutex_t));
    new->pool       = pool;
    new->owner      = 0;
    new->lock_count = 0;

    semname = fixed_name(fname, pool);
    rc = DosOpenMutexSem(semname, &(new->hMutex));
    *mutex = new;

    if (!rc) {
        kuda_pool_cleanup_register(pool, new, kuda_proc_mutex_cleanup, kuda_pool_cleanup_null);
    }

    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_lock(kuda_proc_mutex_t *mutex)
{
    ULONG rc = DosRequestMutexSem(mutex->hMutex, SEM_INDEFINITE_WAIT);

    if (rc == 0) {
        mutex->owner = CurrentTid;
        mutex->lock_count++;
    }

    return KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_trylock(kuda_proc_mutex_t *mutex)
{
    ULONG rc = DosRequestMutexSem(mutex->hMutex, SEM_IMMEDIATE_RETURN);

    if (rc == 0) {
        mutex->owner = CurrentTid;
        mutex->lock_count++;
    }

    return (rc == ERROR_TIMEOUT) ? KUDA_EBUSY : KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_timedlock(kuda_proc_mutex_t *mutex,
                                               kuda_interval_time_t timeout)
{
    ULONG rc;
    
    if (timeout <= 0) {
        rc = DosRequestMutexSem(mutex->hMutex, SEM_IMMEDIATE_RETURN);
    }
    else {
        rc = DosRequestMutexSem(mutex->hMutex, kuda_time_as_msec(timeout));
    }

    if (rc == 0) {
        mutex->owner = CurrentTid;
        mutex->lock_count++;
    }

    return (rc == ERROR_TIMEOUT) ? KUDA_TIMEUP : KUDA_FROM_PLATFORM_ERROR(rc);
}



KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_unlock(kuda_proc_mutex_t *mutex)
{
    ULONG rc;

    if (mutex->owner == CurrentTid && mutex->lock_count > 0) {
        mutex->lock_count--;
        rc = DosReleaseMutexSem(mutex->hMutex);
        return KUDA_FROM_PLATFORM_ERROR(rc);
    }

    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_proc_mutex_destroy(kuda_proc_mutex_t *mutex)
{
    ULONG rc;
    kuda_status_t status = KUDA_SUCCESS;

    if (mutex->owner == CurrentTid) {
        while (mutex->lock_count > 0 && status == KUDA_SUCCESS) {
            status = kuda_proc_mutex_unlock(mutex);
        }
    }

    if (status != KUDA_SUCCESS) {
        return status;
    }

    if (mutex->hMutex == 0) {
        return KUDA_SUCCESS;
    }

    rc = DosCloseMutexSem(mutex->hMutex);

    if (!rc) {
        mutex->hMutex = 0;
    }

    return KUDA_FROM_PLATFORM_ERROR(rc);
}

KUDA_PERMS_SET_ENOTIMPL(proc_mutex)

KUDA_POOL_IMPLEMENT_ACCESSOR(proc_mutex)



/* Implement PLATFORM-specific accessors defined in kuda_portable.h */

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_get_ex(kuda_platform_proc_mutex_t *ospmutex, 
                                                   kuda_proc_mutex_t *pmutex,
                                                   kuda_lockmech_e *mech)
{
    *ospmutex = pmutex->hMutex;
    if (mech) {
        *mech = KUDA_LOCK_DEFAULT;
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_get(kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_proc_mutex_t *pmutex)
{
    return kuda_platform_proc_mutex_get_ex(ospmutex, pmutex, NULL);
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_put_ex(kuda_proc_mutex_t **pmutex,
                                                kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_lockmech_e mech,
                                                int register_cleanup,
                                                kuda_pool_t *pool)
{
    kuda_proc_mutex_t *new;
    if (pool == NULL) {
        return KUDA_ENOPOOL;
    }
    if (mech != KUDA_LOCK_DEFAULT && mech != KUDA_LOCK_DEFAULT_TIMED) {
        return KUDA_ENOTIMPL;
    }

    new = (kuda_proc_mutex_t *)kuda_palloc(pool, sizeof(kuda_proc_mutex_t));
    new->pool       = pool;
    new->owner      = 0;
    new->lock_count = 0;
    new->hMutex     = *ospmutex;
    *pmutex = new;

    if (register_cleanup) {
        kuda_pool_cleanup_register(pool, *pmutex, kuda_proc_mutex_cleanup,
                                  kuda_pool_cleanup_null);
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_proc_mutex_put(kuda_proc_mutex_t **pmutex,
                                                kuda_platform_proc_mutex_t *ospmutex,
                                                kuda_pool_t *pool)
{
    return kuda_platform_proc_mutex_put_ex(pmutex, ospmutex, KUDA_LOCK_DEFAULT,
                                    0, pool);
}

