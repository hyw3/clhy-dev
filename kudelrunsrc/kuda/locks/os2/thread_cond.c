/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_arch_thread_mutex.h"
#include "kuda_arch_thread_cond.h"
#include "kuda_arch_file_io.h"
#include <string.h>

#ifndef DCE_POSTONE
#define DCE_POSTONE   0x0800 // Post one flag
#endif

static kuda_status_t thread_cond_cleanup(void *data)
{
    kuda_thread_cond_t *cv = data;

    if (cv->semaphore) {
        DosCloseEventSem(cv->semaphore);
    }

    if (cv->mutex) {
        DosCloseMutexSem(cv->mutex);
    }

    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_thread_cond_create(kuda_thread_cond_t **cond,
                                                 kuda_pool_t *pool)
{
    int rc;
    kuda_thread_cond_t *cv;

    cv = kuda_pcalloc(pool, sizeof(**cond));
    rc = DosCreateEventSem(NULL, &cv->semaphore, DCE_POSTONE, FALSE);

    if (rc == 0) {
        rc = DosCreateMutexSem(NULL, &cv->mutex, 0, FALSE);
    }

    *cond = cv;
    cv->pool = pool;
    kuda_pool_cleanup_register(cv->pool, cv, thread_cond_cleanup,
                              kuda_pool_cleanup_null);

    return KUDA_FROM_PLATFORM_ERROR(rc);
}



static kuda_status_t thread_cond_timedwait(kuda_thread_cond_t *cond,
                                          kuda_thread_mutex_t *mutex,
                                          ULONG timeout_ms )
{
    ULONG rc;
    kuda_status_t rv = KUDA_SUCCESS;
    int wake = FALSE;
    unsigned long generation;

    DosRequestMutexSem(cond->mutex, SEM_INDEFINITE_WAIT);
    cond->num_waiting++;
    generation = cond->generation;
    DosReleaseMutexSem(cond->mutex);

    kuda_thread_mutex_unlock(mutex);

    do {
        rc = DosWaitEventSem(cond->semaphore, timeout_ms);

        DosRequestMutexSem(cond->mutex, SEM_INDEFINITE_WAIT);

        if (cond->num_wake) {
            if (cond->generation != generation) {
                cond->num_wake--;
                cond->num_waiting--;
                rv = KUDA_SUCCESS;
                break;
            } else {
                wake = TRUE;
            }
        }
        else if (rc != 0) {
            cond->num_waiting--;
            rv = KUDA_TIMEUP;
            break;
        }

        DosReleaseMutexSem(cond->mutex);

        if (wake) {
            wake = FALSE;
            DosPostEventSem(cond->semaphore);
        }
    } while (1);

    DosReleaseMutexSem(cond->mutex);
    kuda_thread_mutex_lock(mutex);
    return rv;
}



KUDA_DECLARE(kuda_status_t) kuda_thread_cond_wait(kuda_thread_cond_t *cond,
                                               kuda_thread_mutex_t *mutex)
{
    return thread_cond_timedwait(cond, mutex, SEM_INDEFINITE_WAIT);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_cond_timedwait(kuda_thread_cond_t *cond,
                                                    kuda_thread_mutex_t *mutex,
                                                    kuda_interval_time_t timeout)
{
    ULONG timeout_ms = (timeout >= 0) ? kuda_time_as_msec(timeout)
                                      : SEM_INDEFINITE_WAIT;
    return thread_cond_timedwait(cond, mutex, timeout_ms);
}



KUDA_DECLARE(kuda_status_t) kuda_thread_cond_signal(kuda_thread_cond_t *cond)
{
    int wake = FALSE;

    DosRequestMutexSem(cond->mutex, SEM_INDEFINITE_WAIT);

    if (cond->num_waiting > cond->num_wake) {
        wake = TRUE;
        cond->num_wake++;
        cond->generation++;
    }

    DosReleaseMutexSem(cond->mutex);

    if (wake) {
        DosPostEventSem(cond->semaphore);
    }

    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_thread_cond_broadcast(kuda_thread_cond_t *cond)
{
    unsigned long num_wake = 0;

    DosRequestMutexSem(cond->mutex, SEM_INDEFINITE_WAIT);

    if (cond->num_waiting > cond->num_wake) {
        num_wake = cond->num_waiting - cond->num_wake;
        cond->num_wake = cond->num_waiting;
        cond->generation++;
    }

    DosReleaseMutexSem(cond->mutex);

    for (; num_wake; num_wake--) {
        DosPostEventSem(cond->semaphore);
    }

    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_thread_cond_destroy(kuda_thread_cond_t *cond)
{
    return kuda_pool_cleanup_run(cond->pool, cond, thread_cond_cleanup);
}



KUDA_POOL_IMPLEMENT_ACCESSOR(thread_cond)
