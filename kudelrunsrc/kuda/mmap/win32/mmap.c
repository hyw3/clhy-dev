/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_mmap.h"
#include "kuda_errno.h"
#include "kuda_arch_file_io.h"
#include "kuda_portable.h"
#include "kuda_strings.h"

#if KUDA_HAS_MMAP

static kuda_status_t mmap_cleanup(void *themmap)
{
    kuda_mmap_t *mm = themmap;
    kuda_mmap_t *next = KUDA_RING_NEXT(mm,link);

    /* we no longer refer to the mmaped region */
    KUDA_RING_REMOVE(mm,link);
    KUDA_RING_NEXT(mm,link) = NULL;
    KUDA_RING_PREV(mm,link) = NULL;

    if (next != mm) {
        /* more references exist, so we're done */
        return KUDA_SUCCESS;
    }

    if (mm->mv) {
        if (!UnmapViewOfFile(mm->mv))
        {
            kuda_status_t rv = kuda_get_platform_error();
            CloseHandle(mm->mhandle);
            mm->mv = NULL;
            mm->mhandle = NULL;
            return rv;
        }
        mm->mv = NULL;
    }
    if (mm->mhandle) 
    {
        if (!CloseHandle(mm->mhandle))
        {
            kuda_status_t rv = kuda_get_platform_error();
            CloseHandle(mm->mhandle);
            mm->mhandle = NULL;
            return rv;
        }
        mm->mhandle = NULL;
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_mmap_create(kuda_mmap_t **new, kuda_file_t *file,
                                          kuda_off_t offset, kuda_size_t size,
                                          kuda_int32_t flag, kuda_pool_t *cont)
{
    static DWORD memblock = 0;
    DWORD fmaccess = 0;
    DWORD mvaccess = 0;
    DWORD offlo;
    DWORD offhi;

    if (size == 0)
        return KUDA_EINVAL;
    
    if (flag & KUDA_MMAP_WRITE)
        fmaccess |= PAGE_READWRITE;
    else if (flag & KUDA_MMAP_READ)
        fmaccess |= PAGE_READONLY;

    if (flag & KUDA_MMAP_READ)
        mvaccess |= FILE_MAP_READ;
    if (flag & KUDA_MMAP_WRITE)
        mvaccess |= FILE_MAP_WRITE;

    if (!file || !file->filehand || file->filehand == INVALID_HANDLE_VALUE
        || file->buffered)
        return KUDA_EBADF;

    if (!memblock)
    {
        SYSTEM_INFO si;
        GetSystemInfo(&si);
        memblock = si.dwAllocationGranularity;
    }   
    
    *new = kuda_pcalloc(cont, sizeof(kuda_mmap_t));
    (*new)->pstart = (offset / memblock) * memblock;
    (*new)->poffset = offset - (*new)->pstart;
    (*new)->psize = (kuda_size_t)((*new)->poffset) + size;
    /* The size of the CreateFileMapping object is the current size
     * of the size of the mmap object (e.g. file size), not the size 
     * of the mapped region!
     */

    (*new)->mhandle = CreateFileMapping(file->filehand, NULL, fmaccess,
                                        0, 0, NULL);
    if (!(*new)->mhandle || (*new)->mhandle == INVALID_HANDLE_VALUE)
    {
        *new = NULL;
        return kuda_get_platform_error();
    }

    offlo = (DWORD)(*new)->pstart;
    offhi = (DWORD)((*new)->pstart >> 32);
    (*new)->mv = MapViewOfFile((*new)->mhandle, mvaccess, offhi,
                               offlo, (*new)->psize);
    if (!(*new)->mv)
    {
        kuda_status_t rv = kuda_get_platform_error();
        CloseHandle((*new)->mhandle);
        *new = NULL;
        return rv;
    }

    (*new)->mm = (char*)((*new)->mv) + (*new)->poffset;
    (*new)->size = size;
    (*new)->cntxt = cont;
    KUDA_RING_ELEM_INIT(*new, link);

    /* register the cleanup... */
    kuda_pool_cleanup_register((*new)->cntxt, (void*)(*new), mmap_cleanup,
                         kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_mmap_dup(kuda_mmap_t **new_mmap,
                                       kuda_mmap_t *old_mmap,
                                       kuda_pool_t *p)
{
    *new_mmap = (kuda_mmap_t *)kuda_pmemdup(p, old_mmap, sizeof(kuda_mmap_t));
    (*new_mmap)->cntxt = p;

    KUDA_RING_INSERT_AFTER(old_mmap, *new_mmap, link);

    kuda_pool_cleanup_register(p, *new_mmap, mmap_cleanup,
                              kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_mmap_delete(kuda_mmap_t *mm)
{
    return kuda_pool_cleanup_run(mm->cntxt, mm, mmap_cleanup);
}

#endif
