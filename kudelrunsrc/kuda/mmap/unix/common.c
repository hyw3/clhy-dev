/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* common .c
 * This file has any function that is truly common and platform
 * neutral.  Or at least that's the theory.
 * 
 * The header files are a problem so there are a few #ifdef's to take
 * care of those.
 *
 */

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_mmap.h"
#include "kuda_errno.h"

#if KUDA_HAS_MMAP || defined(BEOS)

KUDA_DECLARE(kuda_status_t) kuda_mmap_offset(void **addr, kuda_mmap_t *mmap,
                                          kuda_off_t offset)
{
    if (offset < 0 || (kuda_size_t)offset > mmap->size)
        return KUDA_EINVAL;
    
    (*addr) = (char *) mmap->mm + offset;
    return KUDA_SUCCESS;
}

#endif
