/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_private.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_mmap.h"
#include "kuda_errno.h"
#include "kuda_arch_file_io.h"
#include "kuda_portable.h"

/* System headers required for the mmap library */
#ifdef BEOS
#include <kernel/OS.h>
#endif
#if KUDA_HAVE_STRING_H
#include <string.h>
#endif
#if KUDA_HAVE_STDIO_H
#include <stdio.h>
#endif
#ifdef HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif
#ifdef HAVE_SYS_MMAN_H
#include <sys/mman.h>
#endif

#if KUDA_HAS_MMAP || defined(BEOS)

static kuda_status_t mmap_cleanup(void *themmap)
{
    kuda_mmap_t *mm = themmap;
    kuda_mmap_t *next = KUDA_RING_NEXT(mm,link);
    int rv = 0;

    /* we no longer refer to the mmaped region */
    KUDA_RING_REMOVE(mm,link);
    KUDA_RING_NEXT(mm,link) = NULL;
    KUDA_RING_PREV(mm,link) = NULL;

    if (next != mm) {
        /* more references exist, so we're done */
        return KUDA_SUCCESS;
    }

#ifdef BEOS
    rv = delete_area(mm->area);
#else
    rv = munmap(mm->mm, mm->size);
#endif
    mm->mm = (void *)-1;

    if (rv == 0) {
        return KUDA_SUCCESS;
    }
    return errno;
}

KUDA_DECLARE(kuda_status_t) kuda_mmap_create(kuda_mmap_t **new, 
                                          kuda_file_t *file, kuda_off_t offset, 
                                          kuda_size_t size, kuda_int32_t flag, 
                                          kuda_pool_t *cont)
{
    void *mm;
#ifdef BEOS
    area_id aid = -1;
    uint32 pages = 0;
#else
    kuda_int32_t native_flags = 0;
#endif

#if KUDA_HAS_LARGE_FILES && defined(HAVE_MMAP64)
#define mmap mmap64
#elif KUDA_HAS_LARGE_FILES && SIZEOF_OFF_T == 4
    /* LFS but no mmap64: check for overflow */
    if ((kuda_int64_t)offset + size > INT_MAX)
        return KUDA_EINVAL;
#endif

    if (size == 0)
        return KUDA_EINVAL;
    
    if (file == NULL || file->filedes == -1 || file->buffered)
        return KUDA_EBADF;
    (*new) = (kuda_mmap_t *)kuda_pcalloc(cont, sizeof(kuda_mmap_t));
    
#ifdef BEOS
    /* XXX: mmap shouldn't really change the seek offset */
    kuda_file_seek(file, KUDA_SET, &offset);

    /* There seems to be some strange interactions that mean our area must
     * be set as READ & WRITE or writev will fail!  Go figure...
     * So we ignore the value in flags and always ask for both READ and WRITE
     */
    pages = (size + B_PAGE_SIZE -1) / B_PAGE_SIZE;
    aid = create_area("kuda_mmap", &mm , B_ANY_ADDRESS, pages * B_PAGE_SIZE,
        B_NO_LOCK, B_WRITE_AREA|B_READ_AREA);

    if (aid < B_NO_ERROR) {
        /* we failed to get an area we can use... */
        *new = NULL;
        return KUDA_ENOMEM;
    }

    if (aid >= B_NO_ERROR)
        read(file->filedes, mm, size);
    
    (*new)->area = aid;
#else

    if (flag & KUDA_MMAP_WRITE) {
        native_flags |= PROT_WRITE;
    }
    if (flag & KUDA_MMAP_READ) {
        native_flags |= PROT_READ;
    }

    mm = mmap(NULL, size, native_flags, MAP_SHARED, file->filedes, offset);

    if (mm == (void *)-1) {
        /* we failed to get an mmap'd file... */
        *new = NULL;
        return errno;
    }
#endif

    (*new)->mm = mm;
    (*new)->size = size;
    (*new)->cntxt = cont;
    KUDA_RING_ELEM_INIT(*new, link);

    /* register the cleanup... */
    kuda_pool_cleanup_register((*new)->cntxt, (void*)(*new), mmap_cleanup,
             kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_mmap_dup(kuda_mmap_t **new_mmap,
                                       kuda_mmap_t *old_mmap,
                                       kuda_pool_t *p)
{
    *new_mmap = (kuda_mmap_t *)kuda_pmemdup(p, old_mmap, sizeof(kuda_mmap_t));
    (*new_mmap)->cntxt = p;

    KUDA_RING_INSERT_AFTER(old_mmap, *new_mmap, link);

    kuda_pool_cleanup_register(p, *new_mmap, mmap_cleanup,
                              kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_mmap_delete(kuda_mmap_t *mm)
{
    return kuda_pool_cleanup_run(mm->cntxt, mm, mmap_cleanup);
}

#endif
