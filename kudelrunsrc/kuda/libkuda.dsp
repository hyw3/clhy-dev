# Microsoft Developer Studio Project File - Name="libkuda" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=libkuda - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "libkuda.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libkuda.mak" CFG="libkuda - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libkuda - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libkuda - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libkuda - x64 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libkuda - x64 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libkuda - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FD /c
# ADD CPP /nologo /MD /W3 /Zi /O2 /Oy- /I "./include" /I "./include/arch" /I "./include/arch/win32" /I "./include/arch/unix" /I "./include/private" /D "NDEBUG" /D "KUDA_DECLARE_EXPORT" /D "WIN32" /D "WINNT" /D "_WINDOWS" /Fo"$(INTDIR)\" /Fd"$(INTDIR)\libkuda_src" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /i "./include" /d "NDEBUG" /d "KUDA_VERSION_ONLY"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib shell32.lib rpcrt4.lib /nologo /base:"0x6EEC0000" /subsystem:windows /dll /incremental:no /debug /opt:ref
# ADD LINK32 kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib shell32.lib rpcrt4.lib /nologo /base:"0x6EEC0000" /subsystem:windows /dll /incremental:no /debug /out:"Release\libkuda-1.dll" /pdb:"Release\libkuda-1.pdb" /implib:"Release\libkuda-1.lib" /MACHINE:X86 /opt:ref
# Begin Special Build Tool
TargetPath=Release\libkuda-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ELSEIF  "$(CFG)" == "libkuda - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FD /EHsc /c
# ADD CPP /nologo /MDd /W3 /Zi /Od /I "./include" /I "./include/arch" /I "./include/arch/win32" /I "./include/arch/unix" /I "./include/private" /D "_DEBUG" /D "KUDA_DECLARE_EXPORT" /D "WIN32" /D "WINNT" /D "_WINDOWS" /Fo"$(INTDIR)\" /Fd"$(INTDIR)\libkuda_src" /FD /EHsc /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /i "./include" /d "_DEBUG" /d "KUDA_VERSION_ONLY"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib shell32.lib rpcrt4.lib /nologo /base:"0x6EEC0000" /subsystem:windows /dll /incremental:no /debug
# ADD LINK32 kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib shell32.lib rpcrt4.lib /nologo /base:"0x6EEC0000" /subsystem:windows /dll /incremental:no /debug /out:"Debug\libkuda-1.dll" /pdb:"Debug\libkuda-1.pdb" /implib:"Debug\libkuda-1.lib" /MACHINE:X86
# Begin Special Build Tool
TargetPath=Debug\libkuda-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ELSEIF  "$(CFG)" == "libkuda - x64 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "x64\Release"
# PROP BASE Intermediate_Dir "x64\Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "x64\Release"
# PROP Intermediate_Dir "x64\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FD /c
# ADD CPP /nologo /MD /W3 /Zi /O2 /Oy- /I "./include" /I "./include/arch" /I "./include/arch/win32" /I "./include/arch/unix" /I "./include/private" /D "NDEBUG" /D "KUDA_DECLARE_EXPORT" /D "WIN32" /D "WINNT" /D "_WINDOWS" /Fo"$(INTDIR)\" /Fd"$(INTDIR)\libkuda_src" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /i "./include" /d "NDEBUG" /d "KUDA_VERSION_ONLY"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib shell32.lib rpcrt4.lib /nologo /base:"0x6EEC0000" /subsystem:windows /dll /incremental:no /debug /opt:ref
# ADD LINK32 kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib shell32.lib rpcrt4.lib /nologo /base:"0x6EEC0000" /subsystem:windows /dll /incremental:no /debug /out:"x64\Release\libkuda-1.dll" /pdb:"x64\Release\libkuda-1.pdb" /implib:"x64\Release\libkuda-1.lib" /MACHINE:X64 /opt:ref
# Begin Special Build Tool
TargetPath=x64\Release\libkuda-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ELSEIF  "$(CFG)" == "libkuda - x64 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "x64\Debug"
# PROP BASE Intermediate_Dir "x64\Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "x64\Debug"
# PROP Intermediate_Dir "x64\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FD /EHsc /c
# ADD CPP /nologo /MDd /W3 /Zi /Od /I "./include" /I "./include/arch" /I "./include/arch/win32" /I "./include/arch/unix" /I "./include/private" /D "_DEBUG" /D "KUDA_DECLARE_EXPORT" /D "WIN32" /D "WINNT" /D "_WINDOWS" /Fo"$(INTDIR)\" /Fd"$(INTDIR)\libkuda_src" /FD /EHsc /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /i "./include" /d "_DEBUG" /d "KUDA_VERSION_ONLY"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib shell32.lib rpcrt4.lib /nologo /base:"0x6EEC0000" /subsystem:windows /dll /incremental:no /debug
# ADD LINK32 kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib shell32.lib rpcrt4.lib /nologo /base:"0x6EEC0000" /subsystem:windows /dll /incremental:no /debug /out:"x64\Debug\libkuda-1.dll" /pdb:"x64\Debug\libkuda-1.pdb" /implib:"x64\Debug\libkuda-1.lib" /MACHINE:X64
# Begin Special Build Tool
TargetPath=x64\Debug\libkuda-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "libkuda - Win32 Release"
# Name "libkuda - Win32 Debug"
# Name "libkuda - x64 Release"
# Name "libkuda - x64 Debug"
# Begin Group "Source Files"

# PROP Default_Filter ".c"
# Begin Group "atomic"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\atomic\win32\kuda_atomic.c
# End Source File
# Begin Source File

SOURCE=.\atomic\win32\kuda_atomic64.c
# End Source File
# End Group
# Begin Group "dso"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\dso\win32\dso.c
# End Source File
# End Group
# Begin Group "encoding"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\encoding\kuda_encode.c
# End Source File
# Begin Source File

SOURCE=.\encoding\kuda_escape.c
# End Source File
# End Group
# Begin Group "file_io"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\file_io\win32\buffer.c
# End Source File
# Begin Source File

SOURCE=.\file_io\unix\copy.c
# End Source File
# Begin Source File

SOURCE=.\file_io\win32\dir.c
# End Source File
# Begin Source File

SOURCE=.\file_io\unix\fileacc.c
# End Source File
# Begin Source File

SOURCE=.\file_io\win32\filedup.c
# End Source File
# Begin Source File

SOURCE=.\file_io\win32\filepath.c
# End Source File
# Begin Source File

SOURCE=.\file_io\unix\filepath_util.c
# End Source File
# Begin Source File

SOURCE=.\file_io\win32\filestat.c
# End Source File
# Begin Source File

SOURCE=.\file_io\win32\filesys.c
# End Source File
# Begin Source File

SOURCE=.\file_io\win32\flock.c
# End Source File
# Begin Source File

SOURCE=.\file_io\unix\fullrw.c
# End Source File
# Begin Source File

SOURCE=.\file_io\unix\mktemp.c
# End Source File
# Begin Source File

SOURCE=.\file_io\win32\open.c
# End Source File
# Begin Source File

SOURCE=.\file_io\win32\pipe.c
# End Source File
# Begin Source File

SOURCE=.\file_io\win32\readwrite.c
# End Source File
# Begin Source File

SOURCE=.\file_io\win32\seek.c
# End Source File
# Begin Source File

SOURCE=.\file_io\unix\tempdir.c
# End Source File
# End Group
# Begin Group "locks"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\locks\win32\proc_mutex.c
# End Source File
# Begin Source File

SOURCE=.\locks\win32\thread_cond.c
# End Source File
# Begin Source File

SOURCE=.\locks\win32\thread_mutex.c
# End Source File
# Begin Source File

SOURCE=.\locks\win32\thread_rwlock.c
# End Source File
# End Group
# Begin Group "memory"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\memory\unix\kuda_pools.c
# End Source File
# End Group
# Begin Group "misc"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\misc\win32\kuda_app.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\misc\win32\charset.c
# End Source File
# Begin Source File

SOURCE=.\misc\win32\env.c
# End Source File
# Begin Source File

SOURCE=.\misc\unix\errorcodes.c
# End Source File
# Begin Source File

SOURCE=.\misc\unix\getopt.c
# End Source File
# Begin Source File

SOURCE=.\misc\win32\internal.c
# End Source File
# Begin Source File

SOURCE=.\misc\win32\misc.c
# End Source File
# Begin Source File

SOURCE=.\misc\unix\otherchild.c
# End Source File
# Begin Source File

SOURCE=.\misc\win32\rand.c
# End Source File
# Begin Source File

SOURCE=.\misc\win32\start.c
# End Source File
# Begin Source File

SOURCE=.\misc\win32\utf8.c
# End Source File
# Begin Source File

SOURCE=.\misc\unix\version.c
# End Source File
# End Group
# Begin Group "mmap"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\mmap\unix\common.c
# End Source File
# Begin Source File

SOURCE=.\mmap\win32\mmap.c
# End Source File
# End Group
# Begin Group "network_io"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\network_io\unix\inet_ntop.c
# End Source File
# Begin Source File

SOURCE=.\network_io\unix\inet_pton.c
# End Source File
# Begin Source File

SOURCE=.\network_io\unix\multicast.c
# End Source File
# Begin Source File

SOURCE=.\network_io\win32\sendrecv.c
# End Source File
# Begin Source File

SOURCE=.\network_io\unix\sockaddr.c
# End Source File
# Begin Source File

SOURCE=.\network_io\win32\sockets.c
# End Source File
# Begin Source File

SOURCE=.\network_io\unix\socket_util.c
# End Source File
# Begin Source File

SOURCE=.\network_io\win32\sockopt.c
# End Source File
# End Group
# Begin Group "passwd"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\passwd\kuda_getpass.c
# End Source File
# End Group
# Begin Group "poll"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\poll\unix\poll.c
# End Source File
# Begin Source File

SOURCE=.\poll\unix\pollcb.c
# End Source File
# Begin Source File

SOURCE=.\poll\unix\pollset.c
# End Source File
# Begin Source File

SOURCE=.\poll\unix\select.c
# End Source File
# Begin Source File

SOURCE=.\poll\unix\wakeup.c
# End Source File
# End Group
# Begin Group "random"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\random\unix\kuda_random.c
# End Source File
# Begin Source File

SOURCE=.\random\unix\sha2.c
# End Source File
# Begin Source File

SOURCE=.\random\unix\sha2_glue.c
# End Source File
# End Group
# Begin Group "shmem"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\shmem\win32\shm.c
# End Source File
# End Group
# Begin Group "strings"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\strings\kuda_cpystrn.c
# End Source File
# Begin Source File

SOURCE=.\strings\kuda_cstr.c
# End Source File
# Begin Source File

SOURCE=.\strings\kuda_fnmatch.c
# End Source File
# Begin Source File

SOURCE=.\strings\kuda_snprintf.c
# End Source File
# Begin Source File

SOURCE=.\strings\kuda_strings.c
# End Source File
# Begin Source File

SOURCE=.\strings\kuda_strnatcmp.c
# End Source File
# Begin Source File

SOURCE=.\strings\kuda_strtok.c
# End Source File
# End Group
# Begin Group "tables"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\tables\kuda_hash.c
# End Source File
# Begin Source File

SOURCE=.\tables\kuda_tables.c
# End Source File
# Begin Source File

SOURCE=.\tables\kuda_skiplist.c
# End Source File
# End Group
# Begin Group "threadproc"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\threadproc\win32\proc.c
# End Source File
# Begin Source File

SOURCE=.\threadproc\win32\signals.c
# End Source File
# Begin Source File

SOURCE=.\threadproc\win32\thread.c
# End Source File
# Begin Source File

SOURCE=.\threadproc\win32\threadpriv.c
# End Source File
# End Group
# Begin Group "time"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\time\win32\time.c
# End Source File
# Begin Source File

SOURCE=.\time\win32\timestr.c
# End Source File
# End Group
# Begin Group "user"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\user\win32\groupinfo.c
# End Source File
# Begin Source File

SOURCE=.\user\win32\userinfo.c
# End Source File
# End Group
# End Group
# Begin Group "Private Header Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\include\arch\win32\kuda_arch_atime.h
# End Source File
# Begin Source File

SOURCE=.\include\arch\win32\kuda_arch_dso.h
# End Source File
# Begin Source File

SOURCE=.\include\arch\win32\kuda_arch_file_io.h
# End Source File
# Begin Source File

SOURCE=.\include\arch\win32\kuda_arch_inherit.h
# End Source File
# Begin Source File

SOURCE=.\include\arch\win32\kuda_arch_misc.h
# End Source File
# Begin Source File

SOURCE=.\include\arch\win32\kuda_arch_networkio.h
# End Source File
# Begin Source File

SOURCE=.\include\arch\win32\kuda_arch_thread_mutex.h
# End Source File
# Begin Source File

SOURCE=.\include\arch\win32\kuda_arch_thread_rwlock.h
# End Source File
# Begin Source File

SOURCE=.\include\arch\win32\kuda_arch_threadproc.h
# End Source File
# Begin Source File

SOURCE=.\include\arch\win32\kuda_arch_utf8.h
# End Source File
# Begin Source File

SOURCE=.\include\arch\win32\kuda_private.h
# End Source File
# Begin Source File

SOURCE=.\include\arch\kuda_private_common.h
# End Source File
# End Group
# Begin Group "Public Header Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\include\kuda.h.in
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\include\kuda.hnw
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\include\kuda.hw

!IF  "$(CFG)" == "libkuda - Win32 Release"

# Begin Custom Build - Creating kuda.h from kuda.hw
InputPath=.\include\kuda.hw

".\include\kuda.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kuda.hw > .\include\kuda.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkuda - Win32 Debug"

# Begin Custom Build - Creating kuda.h from kuda.hw
InputPath=.\include\kuda.hw

".\include\kuda.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kuda.hw > .\include\kuda.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkuda - x64 Release"

# Begin Custom Build - Creating kuda.h from kuda.hw
InputPath=.\include\kuda.hw

".\include\kuda.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kuda.hw > .\include\kuda.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkuda - x64 Debug"

# Begin Custom Build - Creating kuda.h from kuda.hw
InputPath=.\include\kuda.hw

".\include\kuda.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kuda.hw > .\include\kuda.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\include\kuda_allocator.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_atomic.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_dso.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_env.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_errno.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_escape.h

!IF  "$(CFG)" == "libkuda - Win32 Release"

# Begin Custom Build - Creating gen_test_char.exe and kuda_escape_test_char.h
InputPath=.\include\kuda_escape.h

".\include\kuda_escape_test_char.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	cl.exe /nologo /W3 /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /FD /I ".\include" /Fo.\Release\gen_test_char /Fe.\Release\gen_test_char.exe .\tools\gen_test_char.c 
	.\Release\gen_test_char.exe > .\include\kuda_escape_test_char.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkuda - Win32 Debug"

# Begin Custom Build - Creating gen_test_char.exe and kuda_escape_test_char.h
InputPath=.\include\kuda_escape.h

".\include\kuda_escape_test_char.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	cl.exe /nologo /W3 /EHsc /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /FD /I ".\include" /Fo.\Debug\gen_test_char /Fe.\Debug\gen_test_char.exe .\tools\gen_test_char.c  
	.\Debug\gen_test_char.exe > .\include\kuda_escape_test_char.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkuda - x64 Release"

# Begin Custom Build - Creating gen_test_char.exe and kuda_escape_test_char.h
InputPath=.\include\kuda_escape.h

".\include\kuda_escape_test_char.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	cl.exe /nologo /W3 /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /FD /I ".\include" /Fo.\x64\Release\gen_test_char /Fe.\x64\Release\gen_test_char.exe .\tools\gen_test_char.c 
	.\x64\Release\gen_test_char.exe > .\include\kuda_escape_test_char.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkuda - x64 Debug"

# Begin Custom Build - Creating gen_test_char.exe and kuda_escape_test_char.h
InputPath=.\include\kuda_escape.h

".\include\kuda_escape_test_char.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	cl.exe /nologo /W3 /EHsc /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /FD /I ".\include" /Fo.\x64\Debug\gen_test_char /Fe.\x64\Debug\gen_test_char.exe .\tools\gen_test_char.c 
	.\x64\Debug\gen_test_char.exe > .\include\kuda_escape_test_char.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\include\kuda_file_info.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_file_io.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_fnmatch.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_general.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_getopt.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_global_mutex.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_hash.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_inherit.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_lib.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_mmap.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_network_io.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_poll.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_pools.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_portable.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_proc_mutex.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_random.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_ring.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_shm.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_signal.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_skiplist.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_strings.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_clservices.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_tables.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_thread_cond.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_thread_mutex.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_thread_proc.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_thread_rwlock.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_time.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_user.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_version.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_want.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\libkuda.rc
# End Source File
# End Target
# End Project
