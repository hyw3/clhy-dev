/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_poll.h"
#include "kuda_time.h"
#include "kuda_portable.h"
#include "kuda_arch_file_io.h"
#include "kuda_arch_networkio.h"
#include "kuda_arch_poll_private.h"
#include "kuda_arch_inherit.h"

#ifdef HAVE_KQUEUE

static kuda_int16_t get_kqueue_revent(kuda_int16_t event, kuda_int16_t flags)
{
    kuda_int16_t rv = 0;

    if (event == EVFILT_READ)
        rv |= KUDA_POLLIN;
    else if (event == EVFILT_WRITE)
        rv |= KUDA_POLLOUT;
    if (flags & EV_EOF)
        rv |= KUDA_POLLHUP;
    /* KUDA_POLLPRI, KUDA_POLLERR, and KUDA_POLLNVAL are not handled by this
     * implementation.
     * TODO: See if EV_ERROR + certain system errors in the returned data field
     * should map to KUDA_POLLNVAL.
     */
    return rv;
}

struct kuda_pollset_private_t
{
    int kqueue_fd;
    struct kevent kevent;
    kuda_uint32_t setsize;
    struct kevent *ke_set;
    kuda_pollfd_t *result_set;
#if KUDA_HAS_THREADS
    /* A thread mutex to protect operations on the rings */
    kuda_thread_mutex_t *ring_lock;
#endif
    /* A ring containing all of the pollfd_t that are active */
    KUDA_RING_HEAD(pfd_query_ring_t, pfd_elem_t) query_ring;
    /* A ring of pollfd_t that have been used, and then _remove'd */
    KUDA_RING_HEAD(pfd_free_ring_t, pfd_elem_t) free_ring;
    /* A ring of pollfd_t where rings that have been _remove'd but
       might still be inside a _poll */
    KUDA_RING_HEAD(pfd_dead_ring_t, pfd_elem_t) dead_ring;
};

static kuda_status_t impl_pollset_cleanup(kuda_pollset_t *pollset)
{
    close(pollset->p->kqueue_fd);
    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollset_create(kuda_pollset_t *pollset,
                                        kuda_uint32_t size,
                                        kuda_pool_t *p,
                                        kuda_uint32_t flags)
{
    kuda_status_t rv;
    pollset->p = kuda_palloc(p, sizeof(kuda_pollset_private_t));
#if KUDA_HAS_THREADS
    if (flags & KUDA_POLLSET_THREADSAFE &&
        ((rv = kuda_thread_mutex_create(&pollset->p->ring_lock,
                                       KUDA_THREAD_MUTEX_DEFAULT,
                                       p)) != KUDA_SUCCESS)) {
        pollset->p = NULL;
        return rv;
    }
#else
    if (flags & KUDA_POLLSET_THREADSAFE) {
        pollset->p = NULL;
        return KUDA_ENOTIMPL;
    }
#endif

    /* POLLIN and POLLOUT are represented in different returned
     * events, so we need 2 entries per descriptor in the result set,
     * both for what is returned by kevent() and what is returned to
     * the caller of kuda_pollset_poll() (since it doesn't spend the
     * CPU to coalesce separate KUDA_POLLIN and KUDA_POLLOUT events
     * for the same descriptor)
     */
    pollset->p->setsize = 2 * size;

    pollset->p->ke_set =
        (struct kevent *) kuda_palloc(p, pollset->p->setsize * sizeof(struct kevent));

    memset(pollset->p->ke_set, 0, pollset->p->setsize * sizeof(struct kevent));

    pollset->p->kqueue_fd = kqueue();

    if (pollset->p->kqueue_fd == -1) {
        pollset->p = NULL;
        return kuda_get_netos_error();
    }

    {
        int flags;

        if ((flags = fcntl(pollset->p->kqueue_fd, F_GETFD)) == -1) {
            rv = errno;
            close(pollset->p->kqueue_fd);
            pollset->p = NULL;
            return rv;
        }

        flags |= FD_CLOEXEC;
        if (fcntl(pollset->p->kqueue_fd, F_SETFD, flags) == -1) {
            rv = errno;
            close(pollset->p->kqueue_fd);
            pollset->p = NULL;
            return rv;
        }
    }

    pollset->p->result_set = kuda_palloc(p, pollset->p->setsize * sizeof(kuda_pollfd_t));

    KUDA_RING_INIT(&pollset->p->query_ring, pfd_elem_t, link);
    KUDA_RING_INIT(&pollset->p->free_ring, pfd_elem_t, link);
    KUDA_RING_INIT(&pollset->p->dead_ring, pfd_elem_t, link);

    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollset_add(kuda_pollset_t *pollset,
                                     const kuda_pollfd_t *descriptor)
{
    kuda_platform_sock_t fd;
    pfd_elem_t *elem;
    kuda_status_t rv = KUDA_SUCCESS;

    pollset_lock_rings();

    if (!KUDA_RING_EMPTY(&(pollset->p->free_ring), pfd_elem_t, link)) {
        elem = KUDA_RING_FIRST(&(pollset->p->free_ring));
        KUDA_RING_REMOVE(elem, link);
    }
    else {
        elem = (pfd_elem_t *) kuda_palloc(pollset->pool, sizeof(pfd_elem_t));
        KUDA_RING_ELEM_INIT(elem, link);
    }
    elem->pfd = *descriptor;

    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        fd = descriptor->desc.s->socketdes;
    }
    else {
        fd = descriptor->desc.f->filedes;
    }

    if (descriptor->reqevents & KUDA_POLLIN) {
        EV_SET(&pollset->p->kevent, fd, EVFILT_READ, EV_ADD, 0, 0, elem);

        if (kevent(pollset->p->kqueue_fd, &pollset->p->kevent, 1, NULL, 0,
                   NULL) == -1) {
            rv = kuda_get_netos_error();
        }
    }

    if (descriptor->reqevents & KUDA_POLLOUT && rv == KUDA_SUCCESS) {
        EV_SET(&pollset->p->kevent, fd, EVFILT_WRITE, EV_ADD, 0, 0, elem);

        if (kevent(pollset->p->kqueue_fd, &pollset->p->kevent, 1, NULL, 0,
                   NULL) == -1) {
            rv = kuda_get_netos_error();
        }
    }

    if (rv == KUDA_SUCCESS) {
        KUDA_RING_INSERT_TAIL(&(pollset->p->query_ring), elem, pfd_elem_t, link);
    }
    else {
        KUDA_RING_INSERT_TAIL(&(pollset->p->free_ring), elem, pfd_elem_t, link);
    }

    pollset_unlock_rings();

    return rv;
}

static kuda_status_t impl_pollset_remove(kuda_pollset_t *pollset,
                                        const kuda_pollfd_t *descriptor)
{
    pfd_elem_t *ep;
    kuda_status_t rv;
    kuda_platform_sock_t fd;

    pollset_lock_rings();

    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        fd = descriptor->desc.s->socketdes;
    }
    else {
        fd = descriptor->desc.f->filedes;
    }

    rv = KUDA_NOTFOUND; /* unless at least one of the specified conditions is */
    if (descriptor->reqevents & KUDA_POLLIN) {
        EV_SET(&pollset->p->kevent, fd, EVFILT_READ, EV_DELETE, 0, 0, NULL);

        if (kevent(pollset->p->kqueue_fd, &pollset->p->kevent, 1, NULL, 0,
                   NULL) != -1) {
            rv = KUDA_SUCCESS;
        }
    }

    if (descriptor->reqevents & KUDA_POLLOUT) {
        EV_SET(&pollset->p->kevent, fd, EVFILT_WRITE, EV_DELETE, 0, 0, NULL);

        if (kevent(pollset->p->kqueue_fd, &pollset->p->kevent, 1, NULL, 0,
                   NULL) != -1) {
            rv = KUDA_SUCCESS;
        }
    }

    for (ep = KUDA_RING_FIRST(&(pollset->p->query_ring));
         ep != KUDA_RING_SENTINEL(&(pollset->p->query_ring),
                                 pfd_elem_t, link);
         ep = KUDA_RING_NEXT(ep, link)) {

        if (descriptor->desc.s == ep->pfd.desc.s) {
            KUDA_RING_REMOVE(ep, link);
            KUDA_RING_INSERT_TAIL(&(pollset->p->dead_ring),
                                 ep, pfd_elem_t, link);
            break;
        }
    }

    pollset_unlock_rings();

    return rv;
}

static kuda_status_t impl_pollset_poll(kuda_pollset_t *pollset,
                                      kuda_interval_time_t timeout,
                                      kuda_int32_t *num,
                                      const kuda_pollfd_t **descriptors)
{
    int ret;
    struct timespec tv, *tvptr;
    kuda_status_t rv = KUDA_SUCCESS;

    *num = 0;

    if (timeout < 0) {
        tvptr = NULL;
    }
    else {
        tv.tv_sec = (long) kuda_time_sec(timeout);
        tv.tv_nsec = (long) kuda_time_usec(timeout) * 1000;
        tvptr = &tv;
    }

    ret = kevent(pollset->p->kqueue_fd, NULL, 0, pollset->p->ke_set,
                 pollset->p->setsize, tvptr);
    if (ret < 0) {
        rv = kuda_get_netos_error();
    }
    else if (ret == 0) {
        rv = KUDA_TIMEUP;
    }
    else {
        int i, j;
        const kuda_pollfd_t *fd;

        for (i = 0, j = 0; i < ret; i++) {
            fd = &((pfd_elem_t *)pollset->p->ke_set[i].udata)->pfd;
            if ((pollset->flags & KUDA_POLLSET_WAKEABLE) &&
                fd->desc_type == KUDA_POLL_FILE &&
                fd->desc.f == pollset->wakeup_pipe[0]) {
                kuda_poll_drain_wakeup_pipe(pollset->wakeup_pipe);
                rv = KUDA_EINTR;
            }
            else {
                pollset->p->result_set[j] = *fd;
                pollset->p->result_set[j].rtnevents =
                        get_kqueue_revent(pollset->p->ke_set[i].filter,
                                          pollset->p->ke_set[i].flags);
                j++;
            }
        }
        if ((*num = j)) { /* any event besides wakeup pipe? */
            rv = KUDA_SUCCESS;
            if (descriptors) {
                *descriptors = pollset->p->result_set;
            }
        }
    }

    pollset_lock_rings();

    /* Shift all PFDs in the Dead Ring to the Free Ring */
    KUDA_RING_CONCAT(&(pollset->p->free_ring), &(pollset->p->dead_ring),
                    pfd_elem_t, link);

    pollset_unlock_rings();

    return rv;
}

static const kuda_pollset_provider_t impl = {
    impl_pollset_create,
    impl_pollset_add,
    impl_pollset_remove,
    impl_pollset_poll,
    impl_pollset_cleanup,
    "kqueue"
};

const kuda_pollset_provider_t *kuda_pollset_provider_kqueue = &impl;

static kuda_status_t impl_pollcb_cleanup(kuda_pollcb_t *pollcb)
{
    close(pollcb->fd);
    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollcb_create(kuda_pollcb_t *pollcb,
                                       kuda_uint32_t size,
                                       kuda_pool_t *p,
                                       kuda_uint32_t flags)
{
    int fd;
    
    fd = kqueue();
    if (fd < 0) {
        return kuda_get_netos_error();
    }

    {
        int flags;
        kuda_status_t rv;

        if ((flags = fcntl(fd, F_GETFD)) == -1) {
            rv = errno;
            close(fd);
            pollcb->fd = -1;
            return rv;
        }

        flags |= FD_CLOEXEC;
        if (fcntl(fd, F_SETFD, flags) == -1) {
            rv = errno;
            close(fd);
            pollcb->fd = -1;
            return rv;
        }
    }
 
    pollcb->fd = fd;
    pollcb->pollset.ke = (struct kevent *) kuda_pcalloc(p, 2 * size * sizeof(struct kevent));
    
    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollcb_add(kuda_pollcb_t *pollcb,
                                    kuda_pollfd_t *descriptor)
{
    kuda_platform_sock_t fd;
    struct kevent ev;
    kuda_status_t rv = KUDA_SUCCESS;
    
    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        fd = descriptor->desc.s->socketdes;
    }
    else {
        fd = descriptor->desc.f->filedes;
    }
    
    if (descriptor->reqevents & KUDA_POLLIN) {
        EV_SET(&ev, fd, EVFILT_READ, EV_ADD, 0, 0, descriptor);
        
        if (kevent(pollcb->fd, &ev, 1, NULL, 0, NULL) == -1) {
            rv = kuda_get_netos_error();
        }
    }
    
    if (descriptor->reqevents & KUDA_POLLOUT && rv == KUDA_SUCCESS) {
        EV_SET(&ev, fd, EVFILT_WRITE, EV_ADD, 0, 0, descriptor);
        
        if (kevent(pollcb->fd, &ev, 1, NULL, 0, NULL) == -1) {
            rv = kuda_get_netos_error();
        }
    }
    
    return rv;
}

static kuda_status_t impl_pollcb_remove(kuda_pollcb_t *pollcb,
                                       kuda_pollfd_t *descriptor)
{
    kuda_status_t rv;
    struct kevent ev;
    kuda_platform_sock_t fd;
    
    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        fd = descriptor->desc.s->socketdes;
    }
    else {
        fd = descriptor->desc.f->filedes;
    }

    rv = KUDA_NOTFOUND; /* unless at least one of the specified conditions is */
    if (descriptor->reqevents & KUDA_POLLIN) {
        EV_SET(&ev, fd, EVFILT_READ, EV_DELETE, 0, 0, NULL);
        
        if (kevent(pollcb->fd, &ev, 1, NULL, 0, NULL) != -1) {
            rv = KUDA_SUCCESS;
        }
    }
    
    if (descriptor->reqevents & KUDA_POLLOUT) {
        EV_SET(&ev, fd, EVFILT_WRITE, EV_DELETE, 0, 0, NULL);
        
        if (kevent(pollcb->fd, &ev, 1, NULL, 0, NULL) != -1) {
            rv = KUDA_SUCCESS;
        }
    }
    
    return rv;
}


static kuda_status_t impl_pollcb_poll(kuda_pollcb_t *pollcb,
                                     kuda_interval_time_t timeout,
                                     kuda_pollcb_cb_t func,
                                     void *baton)
{
    int ret, i;
    struct timespec tv, *tvptr;
    kuda_status_t rv = KUDA_SUCCESS;
    
    if (timeout < 0) {
        tvptr = NULL;
    }
    else {
        tv.tv_sec = (long) kuda_time_sec(timeout);
        tv.tv_nsec = (long) kuda_time_usec(timeout) * 1000;
        tvptr = &tv;
    }
    
    ret = kevent(pollcb->fd, NULL, 0, pollcb->pollset.ke, 2 * pollcb->nalloc,
                 tvptr);

    if (ret < 0) {
        rv = kuda_get_netos_error();
    }
    else if (ret == 0) {
        rv = KUDA_TIMEUP;
    }
    else {
        for (i = 0; i < ret; i++) {
            kuda_pollfd_t *pollfd = (kuda_pollfd_t *)(pollcb->pollset.ke[i].udata);

            if ((pollcb->flags & KUDA_POLLSET_WAKEABLE) &&
                pollfd->desc_type == KUDA_POLL_FILE &&
                pollfd->desc.f == pollcb->wakeup_pipe[0]) {
                kuda_poll_drain_wakeup_pipe(pollcb->wakeup_pipe);
                return KUDA_EINTR;
            }

            pollfd->rtnevents = get_kqueue_revent(pollcb->pollset.ke[i].filter,
                                                  pollcb->pollset.ke[i].flags);
            
            rv = func(baton, pollfd);
            
            if (rv) {
                return rv;
            }
        }
    }

    return rv;
}

static const kuda_pollcb_provider_t impl_cb = {
    impl_pollcb_create,
    impl_pollcb_add,
    impl_pollcb_remove,
    impl_pollcb_poll,
    impl_pollcb_cleanup,
    "kqueue"
};

const kuda_pollcb_provider_t *kuda_pollcb_provider_kqueue = &impl_cb;

#endif /* HAVE_KQUEUE */
