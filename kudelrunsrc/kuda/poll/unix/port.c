/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_poll.h"
#include "kuda_time.h"
#include "kuda_portable.h"
#include "kuda_atomic.h"
#include "kuda_arch_file_io.h"
#include "kuda_arch_networkio.h"
#include "kuda_arch_poll_private.h"
#include "kuda_arch_inherit.h"

#if defined(HAVE_PORT_CREATE)

static kuda_int16_t get_event(kuda_int16_t event)
{
    kuda_int16_t rv = 0;

    if (event & KUDA_POLLIN)
        rv |= POLLIN;
    if (event & KUDA_POLLPRI)
        rv |= POLLPRI;
    if (event & KUDA_POLLOUT)
        rv |= POLLOUT;
    /* POLLERR, POLLHUP, and POLLNVAL aren't valid as requested events */

    return rv;
}

static kuda_int16_t get_revent(kuda_int16_t event)
{
    kuda_int16_t rv = 0;

    if (event & POLLIN)
        rv |= KUDA_POLLIN;
    if (event & POLLPRI)
        rv |= KUDA_POLLPRI;
    if (event & POLLOUT)
        rv |= KUDA_POLLOUT;
    if (event & POLLERR)
        rv |= KUDA_POLLERR;
    if (event & POLLHUP)
        rv |= KUDA_POLLHUP;
    if (event & POLLNVAL)
        rv |= KUDA_POLLNVAL;

    return rv;
}


struct kuda_pollset_private_t
{
    int port_fd;
    port_event_t *port_set;
    kuda_pollfd_t *result_set;
#if KUDA_HAS_THREADS
    /* A thread mutex to protect operations on the rings */
    kuda_thread_mutex_t *ring_lock;
#endif
    /* A ring containing all of the pollfd_t that are active */
    KUDA_RING_HEAD(pfd_query_ring_t, pfd_elem_t) query_ring;
    /* A ring containing the pollfd_t that will be added on the
     * next call to kuda_pollset_poll().
     */
    KUDA_RING_HEAD(pfd_add_ring_t, pfd_elem_t) add_ring;
    /* A ring of pollfd_t that have been used, and then _remove'd */
    KUDA_RING_HEAD(pfd_free_ring_t, pfd_elem_t) free_ring;
    /* A ring of pollfd_t where rings that have been _remove'd but
       might still be inside a _poll */
    KUDA_RING_HEAD(pfd_dead_ring_t, pfd_elem_t) dead_ring;
    /* number of threads in poll */
    volatile kuda_uint32_t waiting;
};

static kuda_status_t call_port_getn(int port, port_event_t list[], 
                                   unsigned int max, unsigned int *nget,
                                   kuda_interval_time_t timeout)
{
    struct timespec tv, *tvptr;
    int ret;
    kuda_status_t rv = KUDA_SUCCESS;

    if (timeout < 0) {
        tvptr = NULL;
    }
    else {
        tv.tv_sec = (long) kuda_time_sec(timeout);
        tv.tv_nsec = (long) kuda_time_usec(timeout) * 1000;
        tvptr = &tv;
    }

    list[0].portev_user = (void *)-1; /* so we can double check that an
                                       * event was returned
                                       */

    ret = port_getn(port, list, max, nget, tvptr);
    /* Note: 32-bit port_getn() on Solaris 10 x86 returns large negative 
     * values instead of 0 when returning immediately.
     */

    if (ret == -1) {
        rv = kuda_get_netos_error();

        switch(rv) {
        case EINTR:
        case ETIME:
            if (*nget > 0 && list[0].portev_user != (void *)-1) {
                /* This confusing API can return an event at the same time
                 * that it reports EINTR or ETIME.  If that occurs, just
                 * report the event.  With EINTR, nget can be > 0 without
                 * any event, so check that portev_user was filled in.
                 *
                 * (Maybe it will be simplified; see thread
                 *   http://mail.opensolaris.org
                 *   /pipermail/networking-discuss/2009-August/011979.html
                 *  This code will still work afterwards.)
                 */
                rv = KUDA_SUCCESS;
                break;
            }
            if (rv == ETIME) {
                rv = KUDA_TIMEUP;
            }
        /* fall-through */
        default:
            *nget = 0;
        }
    }
    else if (*nget == 0) {
        rv = KUDA_TIMEUP;
    }

    return rv;
}

static kuda_status_t impl_pollset_cleanup(kuda_pollset_t *pollset)
{
    close(pollset->p->port_fd);
    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollset_create(kuda_pollset_t *pollset,
                                             kuda_uint32_t size,
                                             kuda_pool_t *p,
                                             kuda_uint32_t flags)
{
    kuda_status_t rv = KUDA_SUCCESS;
    pollset->p = kuda_palloc(p, sizeof(kuda_pollset_private_t));
#if KUDA_HAS_THREADS
    if (flags & KUDA_POLLSET_THREADSAFE &&
        ((rv = kuda_thread_mutex_create(&pollset->p->ring_lock,
                                       KUDA_THREAD_MUTEX_DEFAULT,
                                       p)) != KUDA_SUCCESS)) {
        pollset->p = NULL;
        return rv;
    }
#else
    if (flags & KUDA_POLLSET_THREADSAFE) {
        pollset->p = NULL;
        return KUDA_ENOTIMPL;
    }
#endif
    pollset->p->waiting = 0;

    pollset->p->port_set = kuda_palloc(p, size * sizeof(port_event_t));

    pollset->p->port_fd = port_create();

    if (pollset->p->port_fd < 0) {
        pollset->p = NULL;
        return kuda_get_netos_error();
    }

    {
        int flags;

        if ((flags = fcntl(pollset->p->port_fd, F_GETFD)) == -1) {
            rv = errno;
            close(pollset->p->port_fd);
            pollset->p = NULL;
            return rv;
        }

        flags |= FD_CLOEXEC;
        if (fcntl(pollset->p->port_fd, F_SETFD, flags) == -1) {
            rv = errno;
            close(pollset->p->port_fd);
            pollset->p = NULL;
            return rv;
        }
    }

    pollset->p->result_set = kuda_palloc(p, size * sizeof(kuda_pollfd_t));

    KUDA_RING_INIT(&pollset->p->query_ring, pfd_elem_t, link);
    KUDA_RING_INIT(&pollset->p->add_ring, pfd_elem_t, link);
    KUDA_RING_INIT(&pollset->p->free_ring, pfd_elem_t, link);
    KUDA_RING_INIT(&pollset->p->dead_ring, pfd_elem_t, link);

    return rv;
}

static kuda_status_t impl_pollset_add(kuda_pollset_t *pollset,
                                     const kuda_pollfd_t *descriptor)
{
    kuda_platform_sock_t fd;
    pfd_elem_t *elem;
    int res;
    kuda_status_t rv = KUDA_SUCCESS;

    pollset_lock_rings();

    if (!KUDA_RING_EMPTY(&(pollset->p->free_ring), pfd_elem_t, link)) {
        elem = KUDA_RING_FIRST(&(pollset->p->free_ring));
        KUDA_RING_REMOVE(elem, link);
    }
    else {
        elem = (pfd_elem_t *) kuda_palloc(pollset->pool, sizeof(pfd_elem_t));
        KUDA_RING_ELEM_INIT(elem, link);
        elem->on_query_ring = 0;
    }
    elem->pfd = *descriptor;

    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        fd = descriptor->desc.s->socketdes;
    }
    else {
        fd = descriptor->desc.f->filedes;
    }

    /* If another thread is polling, notify the kernel immediately; otherwise,
     * wait until the next call to kuda_pollset_poll().
     */
    if (kuda_atomic_read32(&pollset->p->waiting)) {
        res = port_associate(pollset->p->port_fd, PORT_SOURCE_FD, fd, 
                             get_event(descriptor->reqevents), (void *)elem);

        if (res < 0) {
            rv = kuda_get_netos_error();
            KUDA_RING_INSERT_TAIL(&(pollset->p->free_ring), elem, pfd_elem_t, link);
        }
        else {
            elem->on_query_ring = 1;
            KUDA_RING_INSERT_TAIL(&(pollset->p->query_ring), elem, pfd_elem_t, link);
        }
    } 
    else {
        KUDA_RING_INSERT_TAIL(&(pollset->p->add_ring), elem, pfd_elem_t, link);
    }

    pollset_unlock_rings();

    return rv;
}

static kuda_status_t impl_pollset_remove(kuda_pollset_t *pollset,
                                        const kuda_pollfd_t *descriptor)
{
    kuda_platform_sock_t fd;
    pfd_elem_t *ep;
    kuda_status_t rv = KUDA_SUCCESS;
    int res;
    int err = 0;
    int found;

    pollset_lock_rings();

    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        fd = descriptor->desc.s->socketdes;
    }
    else {
        fd = descriptor->desc.f->filedes;
    }

    /* Search the add ring first.  This ring is often shorter,
     * and it often contains the descriptor being removed.  
     * (For the common scenario where kuda_pollset_poll() 
     * returns activity for the descriptor and the descriptor
     * is then removed from the pollset, it will have just 
     * been moved to the add ring by kuda_pollset_poll().)
     *
     * If it is on the add ring, it isn't associated with the
     * event port yet/anymore.
     */
    found = 0;
    for (ep = KUDA_RING_FIRST(&(pollset->p->add_ring));
         ep != KUDA_RING_SENTINEL(&(pollset->p->add_ring),
                                 pfd_elem_t, link);
         ep = KUDA_RING_NEXT(ep, link)) {

        if (descriptor->desc.s == ep->pfd.desc.s) {
            found = 1;
            KUDA_RING_REMOVE(ep, link);
            KUDA_RING_INSERT_TAIL(&(pollset->p->free_ring),
                                 ep, pfd_elem_t, link);
            break;
        }
    }

    if (!found) {
        res = port_dissociate(pollset->p->port_fd, PORT_SOURCE_FD, fd);

        if (res < 0) {
            /* The expected case for this failure is that another
             * thread's call to port_getn() returned this fd and
             * disassociated the fd from the event port, and 
             * impl_pollset_poll() is blocked on the ring lock,
             * which this thread holds.
             */
            err = errno;
            rv = KUDA_NOTFOUND;
        }

        for (ep = KUDA_RING_FIRST(&(pollset->p->query_ring));
             ep != KUDA_RING_SENTINEL(&(pollset->p->query_ring),
                                     pfd_elem_t, link);
             ep = KUDA_RING_NEXT(ep, link)) {

            if (descriptor->desc.s == ep->pfd.desc.s) {
                KUDA_RING_REMOVE(ep, link);
                ep->on_query_ring = 0;
                KUDA_RING_INSERT_TAIL(&(pollset->p->dead_ring),
                                     ep, pfd_elem_t, link);
                if (ENOENT == err) {
                    rv = KUDA_SUCCESS;
                }
                break;
            }
        }
    }

    pollset_unlock_rings();

    return rv;
}

static kuda_status_t impl_pollset_poll(kuda_pollset_t *pollset,
                                      kuda_interval_time_t timeout,
                                      kuda_int32_t *num,
                                      const kuda_pollfd_t **descriptors)
{
    kuda_platform_sock_t fd;
    int ret;
    unsigned int nget, i;
    kuda_int32_t j;
    pfd_elem_t *ep;
    kuda_status_t rv = KUDA_SUCCESS;

    *num = 0;
    nget = 1;

    pollset_lock_rings();

    kuda_atomic_inc32(&pollset->p->waiting);

    while (!KUDA_RING_EMPTY(&(pollset->p->add_ring), pfd_elem_t, link)) {
        ep = KUDA_RING_FIRST(&(pollset->p->add_ring));
        KUDA_RING_REMOVE(ep, link);

        if (ep->pfd.desc_type == KUDA_POLL_SOCKET) {
            fd = ep->pfd.desc.s->socketdes;
        }
        else {
            fd = ep->pfd.desc.f->filedes;
        }

        ret = port_associate(pollset->p->port_fd, PORT_SOURCE_FD, 
                             fd, get_event(ep->pfd.reqevents), ep);
        if (ret < 0) {
            rv = kuda_get_netos_error();
            KUDA_RING_INSERT_TAIL(&(pollset->p->free_ring), ep, pfd_elem_t, link);
            break;
        }

        ep->on_query_ring = 1;
        KUDA_RING_INSERT_TAIL(&(pollset->p->query_ring), ep, pfd_elem_t, link);
    }

    pollset_unlock_rings();

    if (rv != KUDA_SUCCESS) {
        kuda_atomic_dec32(&pollset->p->waiting);
        return rv;
    }

    rv = call_port_getn(pollset->p->port_fd, pollset->p->port_set, 
                        pollset->nalloc, &nget, timeout);

    /* decrease the waiting ASAP to reduce the window for calling 
       port_associate within kuda_pollset_add() */
    kuda_atomic_dec32(&pollset->p->waiting);

    pollset_lock_rings();

    for (i = 0, j = 0; i < nget; i++) {
        ep = (pfd_elem_t *)pollset->p->port_set[i].portev_user;
        if ((pollset->flags & KUDA_POLLSET_WAKEABLE) &&
            ep->pfd.desc_type == KUDA_POLL_FILE &&
            ep->pfd.desc.f == pollset->wakeup_pipe[0]) {
            kuda_poll_drain_wakeup_pipe(pollset->wakeup_pipe);
            rv = KUDA_EINTR;
        }
        else {
            pollset->p->result_set[j] = ep->pfd;
            pollset->p->result_set[j].rtnevents =
                get_revent(pollset->p->port_set[i].portev_events);
            j++;
        }
        /* If the ring element is still on the query ring, move it
         * to the add ring for re-association with the event port
         * later.  (It may have already been moved to the dead ring
         * by a call to pollset_remove on another thread.)
         */
        if (ep->on_query_ring) {
            KUDA_RING_REMOVE(ep, link);
            ep->on_query_ring = 0;
            KUDA_RING_INSERT_TAIL(&(pollset->p->add_ring), ep,
                                 pfd_elem_t, link);
        }
    }
    if ((*num = j)) { /* any event besides wakeup pipe? */
        rv = KUDA_SUCCESS;
        if (descriptors) {
            *descriptors = pollset->p->result_set;
        }
    }

    /* Shift all PFDs in the Dead Ring to the Free Ring */
    KUDA_RING_CONCAT(&(pollset->p->free_ring), &(pollset->p->dead_ring), pfd_elem_t, link);

    pollset_unlock_rings();

    return rv;
}

static const kuda_pollset_provider_t impl = {
    impl_pollset_create,
    impl_pollset_add,
    impl_pollset_remove,
    impl_pollset_poll,
    impl_pollset_cleanup,
    "port"
};

const kuda_pollset_provider_t *kuda_pollset_provider_port = &impl;

static kuda_status_t impl_pollcb_cleanup(kuda_pollcb_t *pollcb)
{
    close(pollcb->fd);
    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollcb_create(kuda_pollcb_t *pollcb,
                                       kuda_uint32_t size,
                                       kuda_pool_t *p,
                                       kuda_uint32_t flags)
{
    pollcb->fd = port_create();

    if (pollcb->fd < 0) {
        return kuda_get_netos_error();
    }

    {
        int flags;
        kuda_status_t rv;

        if ((flags = fcntl(pollcb->fd, F_GETFD)) == -1) {
            rv = errno;
            close(pollcb->fd);
            pollcb->fd = -1;
            return rv;
        }

        flags |= FD_CLOEXEC;
        if (fcntl(pollcb->fd, F_SETFD, flags) == -1) {
            rv = errno;
            close(pollcb->fd);
            pollcb->fd = -1;
            return rv;
        }
    }

    pollcb->pollset.port = kuda_palloc(p, size * sizeof(port_event_t));

    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollcb_add(kuda_pollcb_t *pollcb,
                                    kuda_pollfd_t *descriptor)
{
    int ret, fd;

    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        fd = descriptor->desc.s->socketdes;
    }
    else {
        fd = descriptor->desc.f->filedes;
    }

    ret = port_associate(pollcb->fd, PORT_SOURCE_FD, fd,
                         get_event(descriptor->reqevents), descriptor);

    if (ret == -1) {
        return kuda_get_netos_error();
    }

    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollcb_remove(kuda_pollcb_t *pollcb,
                                       kuda_pollfd_t *descriptor)
{
    int fd, ret;

    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        fd = descriptor->desc.s->socketdes;
    }
    else {
        fd = descriptor->desc.f->filedes;
    }

    ret = port_dissociate(pollcb->fd, PORT_SOURCE_FD, fd);

    if (ret < 0) {
        return KUDA_NOTFOUND;
    }

    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollcb_poll(kuda_pollcb_t *pollcb,
                                     kuda_interval_time_t timeout,
                                     kuda_pollcb_cb_t func,
                                     void *baton)
{
    kuda_status_t rv;
    unsigned int nget = 1;

    rv = call_port_getn(pollcb->fd, pollcb->pollset.port, pollcb->nalloc,
                        &nget, timeout);

    if (nget) {
        unsigned int i;

        for (i = 0; i < nget; i++) {
            kuda_pollfd_t *pollfd = (kuda_pollfd_t *)(pollcb->pollset.port[i].portev_user);

            if ((pollcb->flags & KUDA_POLLSET_WAKEABLE) &&
                pollfd->desc_type == KUDA_POLL_FILE &&
                pollfd->desc.f == pollcb->wakeup_pipe[0]) {
                kuda_poll_drain_wakeup_pipe(pollcb->wakeup_pipe);
                return KUDA_EINTR;
            }

            pollfd->rtnevents = get_revent(pollcb->pollset.port[i].portev_events);

            rv = func(baton, pollfd);
            if (rv) {
                return rv;
            }
            rv = kuda_pollcb_add(pollcb, pollfd);
        }
    }

    return rv;
}

static const kuda_pollcb_provider_t impl_cb = {
    impl_pollcb_create,
    impl_pollcb_add,
    impl_pollcb_remove,
    impl_pollcb_poll,
    impl_pollcb_cleanup,
    "port"
};

const kuda_pollcb_provider_t *kuda_pollcb_provider_port = &impl_cb;

#endif /* HAVE_PORT_CREATE */
