/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef WIN32
/* POSIX defines 1024 for the FD_SETSIZE */
#define FD_SETSIZE 1024
#endif

#include "kuda.h"
#include "kuda_poll.h"
#include "kuda_time.h"
#include "kuda_portable.h"
#include "kuda_arch_file_io.h"
#include "kuda_arch_networkio.h"
#include "kuda_arch_poll_private.h"

static kuda_pollset_method_e pollset_default_method = POLLSET_DEFAULT_METHOD;
#if defined(HAVE_KQUEUE)
extern const kuda_pollcb_provider_t *kuda_pollcb_provider_kqueue;
#endif
#if defined(HAVE_PORT_CREATE)
extern const kuda_pollcb_provider_t *kuda_pollcb_provider_port;
#endif
#if defined(HAVE_EPOLL)
extern const kuda_pollcb_provider_t *kuda_pollcb_provider_epoll;
#endif
#if defined(HAVE_POLL)
extern const kuda_pollcb_provider_t *kuda_pollcb_provider_poll;
#endif

static const kuda_pollcb_provider_t *pollcb_provider(kuda_pollset_method_e method)
{
    const kuda_pollcb_provider_t *provider = NULL;
    switch (method) {
        case KUDA_POLLSET_KQUEUE:
#if defined(HAVE_KQUEUE)
            provider = kuda_pollcb_provider_kqueue;
#endif
        break;
        case KUDA_POLLSET_PORT:
#if defined(HAVE_PORT_CREATE)
            provider = kuda_pollcb_provider_port;
#endif
        break;
        case KUDA_POLLSET_EPOLL:
#if defined(HAVE_EPOLL)
            provider = kuda_pollcb_provider_epoll;
#endif
        break;
        case KUDA_POLLSET_POLL:
#if defined(HAVE_POLL)
            provider = kuda_pollcb_provider_poll;
#endif
        break;
        case KUDA_POLLSET_SELECT:
        case KUDA_POLLSET_AIO_MSGQ:
        case KUDA_POLLSET_DEFAULT:
        break;
    }
    return provider;
}

static kuda_status_t pollcb_cleanup(void *p)
{
    kuda_pollcb_t *pollcb = (kuda_pollcb_t *) p;

    if (pollcb->provider->cleanup) {
        (*pollcb->provider->cleanup)(pollcb);
    }
    if (pollcb->flags & KUDA_POLLSET_WAKEABLE) {
        kuda_poll_close_wakeup_pipe(pollcb->wakeup_pipe);
    }

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_pollcb_create_ex(kuda_pollcb_t **ret_pollcb,
                                               kuda_uint32_t size,
                                               kuda_pool_t *p,
                                               kuda_uint32_t flags,
                                               kuda_pollset_method_e method)
{
    kuda_status_t rv;
    kuda_pollcb_t *pollcb;
    const kuda_pollcb_provider_t *provider = NULL;

    *ret_pollcb = NULL;

 #ifdef WIN32
    /* This will work only if ws2_32.dll has WSAPoll funtion.
     * We could check the presence of the function here,
     * but someone might implement other pollcb method in
     * the future.
     */
    if (method == KUDA_POLLSET_DEFAULT) {
        method = KUDA_POLLSET_POLL;
    }
 #endif

    if (method == KUDA_POLLSET_DEFAULT)
        method = pollset_default_method;
    while (provider == NULL) {
        provider = pollcb_provider(method);
        if (!provider) {
            if ((flags & KUDA_POLLSET_NODEFAULT) == KUDA_POLLSET_NODEFAULT)
                return KUDA_ENOTIMPL;
            if (method == pollset_default_method)
                return KUDA_ENOTIMPL;
            method = pollset_default_method;
        }
    }

    if (flags & KUDA_POLLSET_WAKEABLE) {
        /* Add room for wakeup descriptor */
        size++;
    }

    pollcb = kuda_palloc(p, sizeof(*pollcb));
    pollcb->nelts = 0;
    pollcb->nalloc = size;
    pollcb->flags = flags;
    pollcb->pool = p;
    pollcb->provider = provider;

    rv = (*provider->create)(pollcb, size, p, flags);
    if (rv == KUDA_ENOTIMPL) {
        if (method == pollset_default_method) {
            return rv;
        }

        if ((flags & KUDA_POLLSET_NODEFAULT) == KUDA_POLLSET_NODEFAULT) {
            return rv;
        }

        /* Try with default provider */
        provider = pollcb_provider(pollset_default_method);
        if (!provider) {
            return KUDA_ENOTIMPL;
        }
        rv = (*provider->create)(pollcb, size, p, flags);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
        pollcb->provider = provider;
    }
    else if (rv != KUDA_SUCCESS) {
        return rv;
    }

    if (flags & KUDA_POLLSET_WAKEABLE) {
        /* Create wakeup pipe */
        if ((rv = kuda_poll_create_wakeup_pipe(pollcb->pool, &pollcb->wakeup_pfd,
                                              pollcb->wakeup_pipe)) 
                != KUDA_SUCCESS) {
            return rv;
        }

        if ((rv = kuda_pollcb_add(pollcb, &pollcb->wakeup_pfd)) != KUDA_SUCCESS) {
            return rv;
        }
    }
    if ((flags & KUDA_POLLSET_WAKEABLE) || provider->cleanup)
        kuda_pool_cleanup_register(p, pollcb, pollcb_cleanup,
                                  kuda_pool_cleanup_null);

    *ret_pollcb = pollcb;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_pollcb_create(kuda_pollcb_t **pollcb,
                                            kuda_uint32_t size,
                                            kuda_pool_t *p,
                                            kuda_uint32_t flags)
{
    kuda_pollset_method_e method = KUDA_POLLSET_DEFAULT;
    return kuda_pollcb_create_ex(pollcb, size, p, flags, method);
}

KUDA_DECLARE(kuda_status_t) kuda_pollcb_add(kuda_pollcb_t *pollcb,
                                         kuda_pollfd_t *descriptor)
{
    return (*pollcb->provider->add)(pollcb, descriptor);
}

KUDA_DECLARE(kuda_status_t) kuda_pollcb_remove(kuda_pollcb_t *pollcb,
                                            kuda_pollfd_t *descriptor)
{
    return (*pollcb->provider->remove)(pollcb, descriptor);
}


KUDA_DECLARE(kuda_status_t) kuda_pollcb_poll(kuda_pollcb_t *pollcb,
                                          kuda_interval_time_t timeout,
                                          kuda_pollcb_cb_t func,
                                          void *baton)
{
    return (*pollcb->provider->poll)(pollcb, timeout, func, baton);
}

KUDA_DECLARE(kuda_status_t) kuda_pollcb_wakeup(kuda_pollcb_t *pollcb)
{
    if (pollcb->flags & KUDA_POLLSET_WAKEABLE)
        return kuda_file_putc(1, pollcb->wakeup_pipe[1]);
    else
        return KUDA_EINIT;
}

KUDA_DECLARE(const char *) kuda_pollcb_method_name(kuda_pollcb_t *pollcb)
{
    return pollcb->provider->name;
}
