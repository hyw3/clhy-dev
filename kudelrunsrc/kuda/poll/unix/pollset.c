/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef WIN32
/* POSIX defines 1024 for the FD_SETSIZE */
#define FD_SETSIZE 1024
#endif

#include "kuda.h"
#include "kuda_poll.h"
#include "kuda_time.h"
#include "kuda_portable.h"
#include "kuda_arch_file_io.h"
#include "kuda_arch_networkio.h"
#include "kuda_arch_poll_private.h"
#include "kuda_arch_inherit.h"

static kuda_pollset_method_e pollset_default_method = POLLSET_DEFAULT_METHOD;

static kuda_status_t pollset_cleanup(void *p)
{
    kuda_pollset_t *pollset = (kuda_pollset_t *) p;
    if (pollset->provider->cleanup) {
        (*pollset->provider->cleanup)(pollset);
    }
    if (pollset->flags & KUDA_POLLSET_WAKEABLE) {
        kuda_poll_close_wakeup_pipe(pollset->wakeup_pipe);
    }

    return KUDA_SUCCESS;
}

#if defined(HAVE_KQUEUE)
extern const kuda_pollset_provider_t *kuda_pollset_provider_kqueue;
#endif
#if defined(HAVE_PORT_CREATE)
extern const kuda_pollset_provider_t *kuda_pollset_provider_port;
#endif
#if defined(HAVE_EPOLL)
extern const kuda_pollset_provider_t *kuda_pollset_provider_epoll;
#endif
#if defined(HAVE_AIO_MSGQ)
extern const kuda_pollset_provider_t *kuda_pollset_provider_aio_msgq;
#endif
#if defined(HAVE_POLL)
extern const kuda_pollset_provider_t *kuda_pollset_provider_poll;
#endif
extern const kuda_pollset_provider_t *kuda_pollset_provider_select;

static const kuda_pollset_provider_t *pollset_provider(kuda_pollset_method_e method)
{
    const kuda_pollset_provider_t *provider = NULL;
    switch (method) {
        case KUDA_POLLSET_KQUEUE:
#if defined(HAVE_KQUEUE)
            provider = kuda_pollset_provider_kqueue;
#endif
        break;
        case KUDA_POLLSET_PORT:
#if defined(HAVE_PORT_CREATE)
            provider = kuda_pollset_provider_port;
#endif
        break;
        case KUDA_POLLSET_EPOLL:
#if defined(HAVE_EPOLL)
            provider = kuda_pollset_provider_epoll;
#endif
        break;
        case KUDA_POLLSET_AIO_MSGQ:
#if defined(HAVE_AIO_MSGQ)
            provider = kuda_pollset_provider_aio_msgq;
#endif
        break;
        case KUDA_POLLSET_POLL:
#if defined(HAVE_POLL)
            provider = kuda_pollset_provider_poll;
#endif
        break;
        case KUDA_POLLSET_SELECT:
            provider = kuda_pollset_provider_select;
        break;
        case KUDA_POLLSET_DEFAULT:
        break;
    }
    return provider;
}

KUDA_DECLARE(kuda_status_t) kuda_pollset_create_ex(kuda_pollset_t **ret_pollset,
                                                kuda_uint32_t size,
                                                kuda_pool_t *p,
                                                kuda_uint32_t flags,
                                                kuda_pollset_method_e method)
{
    kuda_status_t rv;
    kuda_pollset_t *pollset;
    const kuda_pollset_provider_t *provider = NULL;

    *ret_pollset = NULL;

 #ifdef WIN32
    /* Favor WSAPoll if supported.
     * This will work only if ws2_32.dll has WSAPoll funtion.
     * In other cases it will fall back to select() method unless
     * the KUDA_POLLSET_NODEFAULT is added to the flags.
     */
    if (method == KUDA_POLLSET_DEFAULT) {
        method = KUDA_POLLSET_POLL;
    }
 #endif

    if (method == KUDA_POLLSET_DEFAULT)
        method = pollset_default_method;
    while (provider == NULL) {
        provider = pollset_provider(method);
        if (!provider) {
            if ((flags & KUDA_POLLSET_NODEFAULT) == KUDA_POLLSET_NODEFAULT)
                return KUDA_ENOTIMPL;
            if (method == pollset_default_method)
                return KUDA_ENOTIMPL;
            method = pollset_default_method;
        }
    }
    if (flags & KUDA_POLLSET_WAKEABLE) {
        /* Add room for wakeup descriptor */
        size++;
    }

    pollset = kuda_palloc(p, sizeof(*pollset));
    pollset->nelts = 0;
    pollset->nalloc = size;
    pollset->pool = p;
    pollset->flags = flags;
    pollset->provider = provider;

    rv = (*provider->create)(pollset, size, p, flags);
    if (rv == KUDA_ENOTIMPL) {
        if (method == pollset_default_method) {
            return rv;
        }
        provider = pollset_provider(pollset_default_method);
        if (!provider) {
            return KUDA_ENOTIMPL;
        }
        rv = (*provider->create)(pollset, size, p, flags);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
        pollset->provider = provider;
    }
    else if (rv != KUDA_SUCCESS) {
        return rv;
    }
    if (flags & KUDA_POLLSET_WAKEABLE) {
        /* Create wakeup pipe */
        if ((rv = kuda_poll_create_wakeup_pipe(pollset->pool, &pollset->wakeup_pfd,
                                              pollset->wakeup_pipe))
                != KUDA_SUCCESS) {
            return rv;
        }

        if ((rv = kuda_pollset_add(pollset, &pollset->wakeup_pfd)) != KUDA_SUCCESS) {
            return rv;
        }
    }
    if ((flags & KUDA_POLLSET_WAKEABLE) || provider->cleanup)
        kuda_pool_cleanup_register(p, pollset, pollset_cleanup,
                                  kuda_pool_cleanup_null);

    *ret_pollset = pollset;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(const char *) kuda_pollset_method_name(kuda_pollset_t *pollset)
{
    return pollset->provider->name;
}

KUDA_DECLARE(const char *) kuda_poll_method_defname()
{
    const kuda_pollset_provider_t *provider = NULL;

    provider = pollset_provider(pollset_default_method);
    if (provider)
        return provider->name;
    else
        return "unknown";
}

KUDA_DECLARE(kuda_status_t) kuda_pollset_create(kuda_pollset_t **pollset,
                                             kuda_uint32_t size,
                                             kuda_pool_t *p,
                                             kuda_uint32_t flags)
{
    kuda_pollset_method_e method = KUDA_POLLSET_DEFAULT;
    return kuda_pollset_create_ex(pollset, size, p, flags, method);
}

KUDA_DECLARE(kuda_status_t) kuda_pollset_destroy(kuda_pollset_t * pollset)
{
    if (pollset->flags & KUDA_POLLSET_WAKEABLE ||
        pollset->provider->cleanup)
        return kuda_pool_cleanup_run(pollset->pool, pollset,
                                    pollset_cleanup);
    else
        return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_pollset_wakeup(kuda_pollset_t *pollset)
{
    if (pollset->flags & KUDA_POLLSET_WAKEABLE)
        return kuda_file_putc(1, pollset->wakeup_pipe[1]);
    else
        return KUDA_EINIT;
}

KUDA_DECLARE(kuda_status_t) kuda_pollset_add(kuda_pollset_t *pollset,
                                          const kuda_pollfd_t *descriptor)
{
    return (*pollset->provider->add)(pollset, descriptor);
}

KUDA_DECLARE(kuda_status_t) kuda_pollset_remove(kuda_pollset_t *pollset,
                                             const kuda_pollfd_t *descriptor)
{
    return (*pollset->provider->remove)(pollset, descriptor);
}

KUDA_DECLARE(kuda_status_t) kuda_pollset_poll(kuda_pollset_t *pollset,
                                           kuda_interval_time_t timeout,
                                           kuda_int32_t *num,
                                           const kuda_pollfd_t **descriptors)
{
    return (*pollset->provider->poll)(pollset, timeout, num, descriptors);
}
