/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_poll.h"
#include "kuda_time.h"
#include "kuda_portable.h"
#include "kuda_arch_file_io.h"
#include "kuda_arch_networkio.h"
#include "kuda_arch_poll_private.h"
#include "kuda_arch_inherit.h"

#if !KUDA_FILES_AS_SOCKETS

#ifdef WIN32

kuda_status_t kuda_poll_create_wakeup_pipe(kuda_pool_t *pool, kuda_pollfd_t *pfd,
                                         kuda_file_t **wakeup_pipe)
{
    kuda_status_t rv;

    if ((rv = kuda_file_socket_pipe_create(&wakeup_pipe[0], &wakeup_pipe[1],
                                      pool)) != KUDA_SUCCESS)
        return rv;

    pfd->reqevents = KUDA_POLLIN;
    pfd->desc_type = KUDA_POLL_FILE;
    pfd->desc.f = wakeup_pipe[0];
    return KUDA_SUCCESS;
}

kuda_status_t kuda_poll_close_wakeup_pipe(kuda_file_t **wakeup_pipe)
{
    kuda_status_t rv0 = KUDA_SUCCESS;
    kuda_status_t rv1 = KUDA_SUCCESS;

    /* Close both sides of the wakeup pipe */
    if (wakeup_pipe[0]) {
        rv0 = kuda_file_socket_pipe_close(wakeup_pipe[0]);
        wakeup_pipe[0] = NULL;
    }
    if (wakeup_pipe[1]) {
        rv1 = kuda_file_socket_pipe_close(wakeup_pipe[1]);
        wakeup_pipe[1] = NULL;
    }
    return rv0 ? rv0 : rv1;
}

#else /* !WIN32 */

kuda_status_t kuda_poll_create_wakeup_pipe(kuda_pollfd_t *pfd, kuda_file_t **wakeup_pipe)
{
    return KUDA_ENOTIMPL;
}

kuda_status_t kuda_poll_close_wakeup_pipe(kuda_file_t **wakeup_pipe)
{
    return KUDA_ENOTIMPL;
}

#endif /* !WIN32 */

#else  /* KUDA_FILES_AS_SOCKETS */

kuda_status_t kuda_poll_create_wakeup_pipe(kuda_pool_t *pool, kuda_pollfd_t *pfd,
                                         kuda_file_t **wakeup_pipe)
{
    kuda_status_t rv;

    if ((rv = kuda_file_pipe_create(&wakeup_pipe[0], &wakeup_pipe[1],
                                   pool)) != KUDA_SUCCESS)
        return rv;

    pfd->p = pool;
    pfd->reqevents = KUDA_POLLIN;
    pfd->desc_type = KUDA_POLL_FILE;
    pfd->desc.f = wakeup_pipe[0];

    {
        int flags;

        if ((flags = fcntl(wakeup_pipe[0]->filedes, F_GETFD)) == -1)
            return errno;

        flags |= FD_CLOEXEC;
        if (fcntl(wakeup_pipe[0]->filedes, F_SETFD, flags) == -1)
            return errno;
    }
    {
        int flags;

        if ((flags = fcntl(wakeup_pipe[1]->filedes, F_GETFD)) == -1)
            return errno;

        flags |= FD_CLOEXEC;
        if (fcntl(wakeup_pipe[1]->filedes, F_SETFD, flags) == -1)
            return errno;
    }

    return KUDA_SUCCESS;
}

kuda_status_t kuda_poll_close_wakeup_pipe(kuda_file_t **wakeup_pipe)
{
    kuda_status_t rv0 = KUDA_SUCCESS;
    kuda_status_t rv1 = KUDA_SUCCESS;

    /* Close both sides of the wakeup pipe */
    if (wakeup_pipe[0]) {
        rv0 = kuda_file_close(wakeup_pipe[0]);
        wakeup_pipe[0] = NULL;
    }
    if (wakeup_pipe[1]) {
        rv1 = kuda_file_close(wakeup_pipe[1]);
        wakeup_pipe[1] = NULL;
    }
    return rv0 ? rv0 : rv1;
}

#endif /* KUDA_FILES_AS_SOCKETS */

/* Read and discard whatever is in the wakeup pipe.
 */
void kuda_poll_drain_wakeup_pipe(kuda_file_t **wakeup_pipe)
{
    char rb[512];
    kuda_size_t nr = sizeof(rb);

    while (kuda_file_read(wakeup_pipe[0], rb, &nr) == KUDA_SUCCESS) {
        /* Although we write just one byte to the other end of the pipe
         * during wakeup, multiple threads could call the wakeup.
         * So simply drain out from the input side of the pipe all
         * the data.
         */
        if (nr != sizeof(rb))
            break;
    }
}
