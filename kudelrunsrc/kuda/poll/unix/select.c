/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef WIN32
/* POSIX defines 1024 for the FD_SETSIZE */
#define FD_SETSIZE 1024
#endif

#include "kuda.h"
#include "kuda_poll.h"
#include "kuda_time.h"
#include "kuda_portable.h"
#include "kuda_arch_file_io.h"
#include "kuda_arch_networkio.h"
#include "kuda_arch_poll_private.h"

#ifdef POLL_USES_SELECT

KUDA_DECLARE(kuda_status_t) kuda_poll(kuda_pollfd_t *kudaset, int num,
                                   kuda_int32_t *nsds,
                                   kuda_interval_time_t timeout)
{
    fd_set readset, writeset, exceptset;
    int rv, i;
    int maxfd = -1;
    struct timeval tv, *tvptr;
#ifdef NETWARE
    kuda_datatype_e set_type = KUDA_NO_DESC;
#endif

#ifdef WIN32
    /* On Win32, select() must be presented with at least one socket to
     * poll on, or select() will return WSAEINVAL.  So, we'll just
     * short-circuit and bail now.
     */
    if (num == 0) {
        (*nsds) = 0;
        if (timeout > 0) {
            kuda_sleep(timeout);
            return KUDA_TIMEUP;
        }
        return KUDA_SUCCESS;
    }
#endif

    if (timeout < 0) {
        tvptr = NULL;
    }
    else {
        tv.tv_sec = (long) kuda_time_sec(timeout);
        tv.tv_usec = (long) kuda_time_usec(timeout);
        tvptr = &tv;
    }

    FD_ZERO(&readset);
    FD_ZERO(&writeset);
    FD_ZERO(&exceptset);

    for (i = 0; i < num; i++) {
        kuda_platform_sock_t fd;

        kudaset[i].rtnevents = 0;

        if (kudaset[i].desc_type == KUDA_POLL_SOCKET) {
#ifdef NETWARE
            if (HAS_PIPES(set_type)) {
                return KUDA_EBADF;
            }
            else {
                set_type = KUDA_POLL_SOCKET;
            }
#endif
            fd = kudaset[i].desc.s->socketdes;
        }
        else if (kudaset[i].desc_type == KUDA_POLL_FILE) {
#if !KUDA_FILES_AS_SOCKETS
            return KUDA_EBADF;
#else
#ifdef NETWARE
            if (kudaset[i].desc.f->is_pipe && !HAS_SOCKETS(set_type)) {
                set_type = KUDA_POLL_FILE;
            }
            else
                return KUDA_EBADF;
#endif /* NETWARE */

            fd = kudaset[i].desc.f->filedes;

#endif /* KUDA_FILES_AS_SOCKETS */
        }
        else {
            break;
        }
#if !defined(WIN32) && !defined(NETWARE)        /* socket sets handled with array of handles */
        if (fd >= FD_SETSIZE) {
            /* XXX invent new error code so application has a clue */
            return KUDA_EBADF;
        }
#endif
        if (kudaset[i].reqevents & KUDA_POLLIN) {
            FD_SET(fd, &readset);
        }
        if (kudaset[i].reqevents & KUDA_POLLOUT) {
            FD_SET(fd, &writeset);
        }
        if (kudaset[i].reqevents &
            (KUDA_POLLPRI | KUDA_POLLERR | KUDA_POLLHUP | KUDA_POLLNVAL)) {
            FD_SET(fd, &exceptset);
        }
        if ((int) fd > maxfd) {
            maxfd = (int) fd;
        }
    }

#ifdef NETWARE
    if (HAS_PIPES(set_type)) {
        rv = pipe_select(maxfd + 1, &readset, &writeset, &exceptset, tvptr);
    }
    else {
#endif

        rv = select(maxfd + 1, &readset, &writeset, &exceptset, tvptr);

#ifdef NETWARE
    }
#endif

    (*nsds) = rv;
    if ((*nsds) == 0) {
        return KUDA_TIMEUP;
    }
    if ((*nsds) < 0) {
        return kuda_get_netos_error();
    }

    (*nsds) = 0;
    for (i = 0; i < num; i++) {
        kuda_platform_sock_t fd;

        if (kudaset[i].desc_type == KUDA_POLL_SOCKET) {
            fd = kudaset[i].desc.s->socketdes;
        }
        else if (kudaset[i].desc_type == KUDA_POLL_FILE) {
#if !KUDA_FILES_AS_SOCKETS
            return KUDA_EBADF;
#else
            fd = kudaset[i].desc.f->filedes;
#endif
        }
        else {
            break;
        }
        if (FD_ISSET(fd, &readset)) {
            kudaset[i].rtnevents |= KUDA_POLLIN;
        }
        if (FD_ISSET(fd, &writeset)) {
            kudaset[i].rtnevents |= KUDA_POLLOUT;
        }
        if (FD_ISSET(fd, &exceptset)) {
            kudaset[i].rtnevents |= KUDA_POLLERR;
        }
        if (kudaset[i].rtnevents) {
            (*nsds)++;
        }
    }

    return KUDA_SUCCESS;
}

#endif /* POLL_USES_SELECT */

struct kuda_pollset_private_t
{
    fd_set readset, writeset, exceptset;
    int maxfd;
    kuda_pollfd_t *query_set;
    kuda_pollfd_t *result_set;
    kuda_uint32_t flags;
#ifdef NETWARE
    int set_type;
#endif
};

static kuda_status_t impl_pollset_create(kuda_pollset_t *pollset,
                                        kuda_uint32_t size,
                                        kuda_pool_t *p,
                                        kuda_uint32_t flags)
{
    if (flags & KUDA_POLLSET_THREADSAFE) {                
        pollset->p = NULL;
        return KUDA_ENOTIMPL;
    }
#ifdef FD_SETSIZE
    if (size > FD_SETSIZE) {
        pollset->p = NULL;
        return KUDA_EINVAL;
    }
#endif
    pollset->p = kuda_palloc(p, sizeof(kuda_pollset_private_t));
    FD_ZERO(&(pollset->p->readset));
    FD_ZERO(&(pollset->p->writeset));
    FD_ZERO(&(pollset->p->exceptset));
    pollset->p->maxfd = 0;
#ifdef NETWARE
    pollset->p->set_type = KUDA_NO_DESC;
#endif
    pollset->p->query_set = kuda_palloc(p, size * sizeof(kuda_pollfd_t));
    pollset->p->result_set = kuda_palloc(p, size * sizeof(kuda_pollfd_t));

    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollset_add(kuda_pollset_t *pollset,
                                     const kuda_pollfd_t *descriptor)
{
    kuda_platform_sock_t fd;

    if (pollset->nelts == pollset->nalloc) {
        return KUDA_ENOMEM;
    }

    pollset->p->query_set[pollset->nelts] = *descriptor;

    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
#ifdef NETWARE
        /* NetWare can't handle mixed descriptor types in select() */
        if (HAS_PIPES(pollset->p->set_type)) {
            return KUDA_EBADF;
        }
        else {
            pollset->p->set_type = KUDA_POLL_SOCKET;
        }
#endif
        fd = descriptor->desc.s->socketdes;
    }
    else {
#if !KUDA_FILES_AS_SOCKETS
        if ((pollset->flags & KUDA_POLLSET_WAKEABLE) &&
            descriptor->desc.f == pollset->wakeup_pipe[0])
            fd = (kuda_platform_sock_t)descriptor->desc.f->filedes;
        else
            return KUDA_EBADF;
#else
#ifdef NETWARE
        /* NetWare can't handle mixed descriptor types in select() */
        if (descriptor->desc.f->is_pipe && !HAS_SOCKETS(pollset->p->set_type)) {
            pollset->p->set_type = KUDA_POLL_FILE;
            fd = descriptor->desc.f->filedes;
        }
        else {
            return KUDA_EBADF;
        }
#else
        fd = descriptor->desc.f->filedes;
#endif
#endif
    }
#if !defined(WIN32) && !defined(NETWARE)        /* socket sets handled with array of handles */
    if (fd >= FD_SETSIZE) {
        /* XXX invent new error code so application has a clue */
        return KUDA_EBADF;
    }
#endif
    if (descriptor->reqevents & KUDA_POLLIN) {
        FD_SET(fd, &(pollset->p->readset));
    }
    if (descriptor->reqevents & KUDA_POLLOUT) {
        FD_SET(fd, &(pollset->p->writeset));
    }
    if (descriptor->reqevents &
        (KUDA_POLLPRI | KUDA_POLLERR | KUDA_POLLHUP | KUDA_POLLNVAL)) {
        FD_SET(fd, &(pollset->p->exceptset));
    }
    if ((int) fd > pollset->p->maxfd) {
        pollset->p->maxfd = (int) fd;
    }
    pollset->nelts++;
    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollset_remove(kuda_pollset_t * pollset,
                                        const kuda_pollfd_t * descriptor)
{
    kuda_uint32_t i;
    kuda_platform_sock_t fd;

    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        fd = descriptor->desc.s->socketdes;
    }
    else {
#if !KUDA_FILES_AS_SOCKETS
        return KUDA_EBADF;
#else
        fd = descriptor->desc.f->filedes;
#endif
    }

    for (i = 0; i < pollset->nelts; i++) {
        if (descriptor->desc.s == pollset->p->query_set[i].desc.s) {
            /* Found an instance of the fd: remove this and any other copies */
            kuda_uint32_t dst = i;
            kuda_uint32_t old_nelts = pollset->nelts;
            pollset->nelts--;
            for (i++; i < old_nelts; i++) {
                if (descriptor->desc.s == pollset->p->query_set[i].desc.s) {
                    pollset->nelts--;
                }
                else {
                    pollset->p->query_set[dst] = pollset->p->query_set[i];
                    dst++;
                }
            }
            FD_CLR(fd, &(pollset->p->readset));
            FD_CLR(fd, &(pollset->p->writeset));
            FD_CLR(fd, &(pollset->p->exceptset));
            if (((int) fd == pollset->p->maxfd) && (pollset->p->maxfd > 0)) {
                pollset->p->maxfd--;
            }
            return KUDA_SUCCESS;
        }
    }

    return KUDA_NOTFOUND;
}

static kuda_status_t impl_pollset_poll(kuda_pollset_t *pollset,
                                      kuda_interval_time_t timeout,
                                      kuda_int32_t *num,
                                      const kuda_pollfd_t **descriptors)
{
    int rs;
    kuda_uint32_t i, j;
    struct timeval tv, *tvptr;
    fd_set readset, writeset, exceptset;
    kuda_status_t rv = KUDA_SUCCESS;

    *num = 0;

#ifdef WIN32
    /* On Win32, select() must be presented with at least one socket to
     * poll on, or select() will return WSAEINVAL.  So, we'll just
     * short-circuit and bail now.
     */
    if (pollset->nelts == 0) {
        if (timeout > 0) {
            kuda_sleep(timeout);
            return KUDA_TIMEUP;
        }
        return KUDA_SUCCESS;
    }
#endif

    if (timeout < 0) {
        tvptr = NULL;
    }
    else {
        tv.tv_sec = (long) kuda_time_sec(timeout);
        tv.tv_usec = (long) kuda_time_usec(timeout);
        tvptr = &tv;
    }

    memcpy(&readset, &(pollset->p->readset), sizeof(fd_set));
    memcpy(&writeset, &(pollset->p->writeset), sizeof(fd_set));
    memcpy(&exceptset, &(pollset->p->exceptset), sizeof(fd_set));

#ifdef NETWARE
    if (HAS_PIPES(pollset->p->set_type)) {
        rs = pipe_select(pollset->p->maxfd + 1, &readset, &writeset, &exceptset,
                         tvptr);
    }
    else
#endif
        rs = select(pollset->p->maxfd + 1, &readset, &writeset, &exceptset,
                    tvptr);

    if (rs < 0) {
        return kuda_get_netos_error();
    }
    if (rs == 0) {
        return KUDA_TIMEUP;
    }
    j = 0;
    for (i = 0; i < pollset->nelts; i++) {
        kuda_platform_sock_t fd;
        if (pollset->p->query_set[i].desc_type == KUDA_POLL_SOCKET) {
            fd = pollset->p->query_set[i].desc.s->socketdes;
        }
        else {
            if ((pollset->flags & KUDA_POLLSET_WAKEABLE) &&
                pollset->p->query_set[i].desc.f == pollset->wakeup_pipe[0]) {
                kuda_poll_drain_wakeup_pipe(pollset->wakeup_pipe);
                rv = KUDA_EINTR;
                continue;
            }
            else {
#if !KUDA_FILES_AS_SOCKETS
                return KUDA_EBADF;
#else
                fd = pollset->p->query_set[i].desc.f->filedes;
#endif
            }
        }
        if (FD_ISSET(fd, &readset) || FD_ISSET(fd, &writeset) ||
            FD_ISSET(fd, &exceptset)) {
            pollset->p->result_set[j] = pollset->p->query_set[i];
            pollset->p->result_set[j].rtnevents = 0;
            if (FD_ISSET(fd, &readset)) {
                pollset->p->result_set[j].rtnevents |= KUDA_POLLIN;
            }
            if (FD_ISSET(fd, &writeset)) {
                pollset->p->result_set[j].rtnevents |= KUDA_POLLOUT;
            }
            if (FD_ISSET(fd, &exceptset)) {
                pollset->p->result_set[j].rtnevents |= KUDA_POLLERR;
            }
            j++;
        }
    }
    if (((*num) = j) != 0)
        rv = KUDA_SUCCESS;

    if (descriptors)
        *descriptors = pollset->p->result_set;
    return rv;
}

static const kuda_pollset_provider_t impl = {
    impl_pollset_create,
    impl_pollset_add,
    impl_pollset_remove,
    impl_pollset_poll,
    NULL,
    "select"
};

const kuda_pollset_provider_t *kuda_pollset_provider_select = &impl;
