/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_poll.h"
#include "kuda_time.h"
#include "kuda_portable.h"
#include "kuda_arch_file_io.h"
#include "kuda_arch_networkio.h"
#include "kuda_arch_poll_private.h"
#include "kuda_arch_inherit.h"

#if defined(HAVE_EPOLL)

static kuda_int16_t get_epoll_event(kuda_int16_t event)
{
    kuda_int16_t rv = 0;

    if (event & KUDA_POLLIN)
        rv |= EPOLLIN;
    if (event & KUDA_POLLPRI)
        rv |= EPOLLPRI;
    if (event & KUDA_POLLOUT)
        rv |= EPOLLOUT;
    /* KUDA_POLLNVAL is not handled by epoll.  EPOLLERR and EPOLLHUP are return-only */

    return rv;
}

static kuda_int16_t get_epoll_revent(kuda_int16_t event)
{
    kuda_int16_t rv = 0;

    if (event & EPOLLIN)
        rv |= KUDA_POLLIN;
    if (event & EPOLLPRI)
        rv |= KUDA_POLLPRI;
    if (event & EPOLLOUT)
        rv |= KUDA_POLLOUT;
    if (event & EPOLLERR)
        rv |= KUDA_POLLERR;
    if (event & EPOLLHUP)
        rv |= KUDA_POLLHUP;
    /* KUDA_POLLNVAL is not handled by epoll. */

    return rv;
}

struct kuda_pollset_private_t
{
    int epoll_fd;
    struct epoll_event *pollset;
    kuda_pollfd_t *result_set;
#if KUDA_HAS_THREADS
    /* A thread mutex to protect operations on the rings */
    kuda_thread_mutex_t *ring_lock;
#endif
    /* A ring containing all of the pollfd_t that are active */
    KUDA_RING_HEAD(pfd_query_ring_t, pfd_elem_t) query_ring;
    /* A ring of pollfd_t that have been used, and then _remove()'d */
    KUDA_RING_HEAD(pfd_free_ring_t, pfd_elem_t) free_ring;
    /* A ring of pollfd_t where rings that have been _remove()`ed but
        might still be inside a _poll() */
    KUDA_RING_HEAD(pfd_dead_ring_t, pfd_elem_t) dead_ring;
};

static kuda_status_t impl_pollset_cleanup(kuda_pollset_t *pollset)
{
    close(pollset->p->epoll_fd);
    return KUDA_SUCCESS;
}


static kuda_status_t impl_pollset_create(kuda_pollset_t *pollset,
                                        kuda_uint32_t size,
                                        kuda_pool_t *p,
                                        kuda_uint32_t flags)
{
    kuda_status_t rv;
    int fd;

#ifdef HAVE_EPOLL_CREATE1
    fd = epoll_create1(EPOLL_CLOEXEC);
#else
    fd = epoll_create(size);
#endif
    if (fd < 0) {
        pollset->p = NULL;
        return kuda_get_netos_error();
    }

#ifndef HAVE_EPOLL_CREATE1
    {
        int fd_flags;

        if ((fd_flags = fcntl(fd, F_GETFD)) == -1) {
            rv = errno;
            close(fd);
            pollset->p = NULL;
            return rv;
        }

        fd_flags |= FD_CLOEXEC;
        if (fcntl(fd, F_SETFD, fd_flags) == -1) {
            rv = errno;
            close(fd);
            pollset->p = NULL;
            return rv;
        }
    }
#endif

    pollset->p = kuda_palloc(p, sizeof(kuda_pollset_private_t));
#if KUDA_HAS_THREADS
    if ((flags & KUDA_POLLSET_THREADSAFE) &&
        !(flags & KUDA_POLLSET_NOCOPY) &&
        ((rv = kuda_thread_mutex_create(&pollset->p->ring_lock,
                                       KUDA_THREAD_MUTEX_DEFAULT,
                                       p)) != KUDA_SUCCESS)) {
        close(fd);
        pollset->p = NULL;
        return rv;
    }
#else
    if (flags & KUDA_POLLSET_THREADSAFE) {
        close(fd);
        pollset->p = NULL;
        return KUDA_ENOTIMPL;
    }
#endif
    pollset->p->epoll_fd = fd;
    pollset->p->pollset = kuda_palloc(p, size * sizeof(struct epoll_event));
    pollset->p->result_set = kuda_palloc(p, size * sizeof(kuda_pollfd_t));

    if (!(flags & KUDA_POLLSET_NOCOPY)) {
        KUDA_RING_INIT(&pollset->p->query_ring, pfd_elem_t, link);
        KUDA_RING_INIT(&pollset->p->free_ring, pfd_elem_t, link);
        KUDA_RING_INIT(&pollset->p->dead_ring, pfd_elem_t, link);
    }
    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollset_add(kuda_pollset_t *pollset,
                                     const kuda_pollfd_t *descriptor)
{
    struct epoll_event ev = {0};
    int ret;
    pfd_elem_t *elem = NULL;
    kuda_status_t rv = KUDA_SUCCESS;

    ev.events = get_epoll_event(descriptor->reqevents);

    if (pollset->flags & KUDA_POLLSET_NOCOPY) {
        ev.data.ptr = (void *)descriptor;
    }
    else {
        pollset_lock_rings();

        if (!KUDA_RING_EMPTY(&(pollset->p->free_ring), pfd_elem_t, link)) {
            elem = KUDA_RING_FIRST(&(pollset->p->free_ring));
            KUDA_RING_REMOVE(elem, link);
        }
        else {
            elem = (pfd_elem_t *) kuda_palloc(pollset->pool, sizeof(pfd_elem_t));
            KUDA_RING_ELEM_INIT(elem, link);
        }
        elem->pfd = *descriptor;
        ev.data.ptr = elem;
    }
    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        ret = epoll_ctl(pollset->p->epoll_fd, EPOLL_CTL_ADD,
                        descriptor->desc.s->socketdes, &ev);
    }
    else {
        ret = epoll_ctl(pollset->p->epoll_fd, EPOLL_CTL_ADD,
                        descriptor->desc.f->filedes, &ev);
    }

    if (0 != ret) {
        rv = kuda_get_netos_error();
    }

    if (!(pollset->flags & KUDA_POLLSET_NOCOPY)) {
        if (rv != KUDA_SUCCESS) {
            KUDA_RING_INSERT_TAIL(&(pollset->p->free_ring), elem, pfd_elem_t, link);
        }
        else {
            KUDA_RING_INSERT_TAIL(&(pollset->p->query_ring), elem, pfd_elem_t, link);
        }
        pollset_unlock_rings();
    }

    return rv;
}

static kuda_status_t impl_pollset_remove(kuda_pollset_t *pollset,
                                        const kuda_pollfd_t *descriptor)
{
    pfd_elem_t *ep;
    kuda_status_t rv = KUDA_SUCCESS;
    struct epoll_event ev = {0}; /* ignored, but must be passed with
                                  * kernel < 2.6.9
                                  */
    int ret;

    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        ret = epoll_ctl(pollset->p->epoll_fd, EPOLL_CTL_DEL,
                        descriptor->desc.s->socketdes, &ev);
    }
    else {
        ret = epoll_ctl(pollset->p->epoll_fd, EPOLL_CTL_DEL,
                        descriptor->desc.f->filedes, &ev);
    }
    if (ret < 0) {
        rv = KUDA_NOTFOUND;
    }

    if (!(pollset->flags & KUDA_POLLSET_NOCOPY)) {
        pollset_lock_rings();

        for (ep = KUDA_RING_FIRST(&(pollset->p->query_ring));
             ep != KUDA_RING_SENTINEL(&(pollset->p->query_ring),
                                     pfd_elem_t, link);
             ep = KUDA_RING_NEXT(ep, link)) {
                
            if (descriptor->desc.s == ep->pfd.desc.s) {
                KUDA_RING_REMOVE(ep, link);
                KUDA_RING_INSERT_TAIL(&(pollset->p->dead_ring),
                                     ep, pfd_elem_t, link);
                break;
            }
        }

        pollset_unlock_rings();
    }

    return rv;
}

static kuda_status_t impl_pollset_poll(kuda_pollset_t *pollset,
                                           kuda_interval_time_t timeout,
                                           kuda_int32_t *num,
                                           const kuda_pollfd_t **descriptors)
{
    int ret;
    kuda_status_t rv = KUDA_SUCCESS;

    *num = 0;

    if (timeout > 0) {
        timeout /= 1000;
    }

    ret = epoll_wait(pollset->p->epoll_fd, pollset->p->pollset, pollset->nalloc,
                     timeout);
    if (ret < 0) {
        rv = kuda_get_netos_error();
    }
    else if (ret == 0) {
        rv = KUDA_TIMEUP;
    }
    else {
        int i, j;
        const kuda_pollfd_t *fdptr;

        for (i = 0, j = 0; i < ret; i++) {
            if (pollset->flags & KUDA_POLLSET_NOCOPY) {
                fdptr = (kuda_pollfd_t *)(pollset->p->pollset[i].data.ptr);
            }
            else {
                fdptr = &(((pfd_elem_t *) (pollset->p->pollset[i].data.ptr))->pfd);
            }
            /* Check if the polled descriptor is our
             * wakeup pipe. In that case do not put it result set.
             */
            if ((pollset->flags & KUDA_POLLSET_WAKEABLE) &&
                fdptr->desc_type == KUDA_POLL_FILE &&
                fdptr->desc.f == pollset->wakeup_pipe[0]) {
                kuda_poll_drain_wakeup_pipe(pollset->wakeup_pipe);
                rv = KUDA_EINTR;
            }
            else {
                pollset->p->result_set[j] = *fdptr;
                pollset->p->result_set[j].rtnevents =
                    get_epoll_revent(pollset->p->pollset[i].events);
                j++;
            }
        }
        if (((*num) = j)) { /* any event besides wakeup pipe? */
            rv = KUDA_SUCCESS;

            if (descriptors) {
                *descriptors = pollset->p->result_set;
            }
        }
    }

    if (!(pollset->flags & KUDA_POLLSET_NOCOPY)) {
        pollset_lock_rings();

        /* Shift all PFDs in the Dead Ring to the Free Ring */
        KUDA_RING_CONCAT(&(pollset->p->free_ring), &(pollset->p->dead_ring), pfd_elem_t, link);

        pollset_unlock_rings();
    }

    return rv;
}

static const kuda_pollset_provider_t impl = {
    impl_pollset_create,
    impl_pollset_add,
    impl_pollset_remove,
    impl_pollset_poll,
    impl_pollset_cleanup,
    "epoll"
};

const kuda_pollset_provider_t *const kuda_pollset_provider_epoll = &impl;

static kuda_status_t impl_pollcb_cleanup(kuda_pollcb_t *pollcb)
{
    close(pollcb->fd);
    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollcb_create(kuda_pollcb_t *pollcb,
                                       kuda_uint32_t size,
                                       kuda_pool_t *p,
                                       kuda_uint32_t flags)
{
    int fd;
    
#ifdef HAVE_EPOLL_CREATE1
    fd = epoll_create1(EPOLL_CLOEXEC);
#else
    fd = epoll_create(size);
#endif
    
    if (fd < 0) {
        return kuda_get_netos_error();
    }

#ifndef HAVE_EPOLL_CREATE1
    {
        int fd_flags;
        kuda_status_t rv;

        if ((fd_flags = fcntl(fd, F_GETFD)) == -1) {
            rv = errno;
            close(fd);
            pollcb->fd = -1;
            return rv;
        }

        fd_flags |= FD_CLOEXEC;
        if (fcntl(fd, F_SETFD, fd_flags) == -1) {
            rv = errno;
            close(fd);
            pollcb->fd = -1;
            return rv;
        }
    }
#endif
    
    pollcb->fd = fd;
    pollcb->pollset.epoll = kuda_palloc(p, size * sizeof(struct epoll_event));

    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollcb_add(kuda_pollcb_t *pollcb,
                                    kuda_pollfd_t *descriptor)
{
    struct epoll_event ev = { 0 };
    int ret;
    
    ev.events = get_epoll_event(descriptor->reqevents);
    ev.data.ptr = (void *) descriptor;

    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        ret = epoll_ctl(pollcb->fd, EPOLL_CTL_ADD,
                        descriptor->desc.s->socketdes, &ev);
    }
    else {
        ret = epoll_ctl(pollcb->fd, EPOLL_CTL_ADD,
                        descriptor->desc.f->filedes, &ev);
    }
    
    if (ret == -1) {
        return kuda_get_netos_error();
    }
    
    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollcb_remove(kuda_pollcb_t *pollcb,
                                       kuda_pollfd_t *descriptor)
{
    kuda_status_t rv = KUDA_SUCCESS;
    struct epoll_event ev = {0}; /* ignored, but must be passed with
                                  * kernel < 2.6.9
                                  */
    int ret;
    
    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        ret = epoll_ctl(pollcb->fd, EPOLL_CTL_DEL,
                        descriptor->desc.s->socketdes, &ev);
    }
    else {
        ret = epoll_ctl(pollcb->fd, EPOLL_CTL_DEL,
                        descriptor->desc.f->filedes, &ev);
    }
    
    if (ret < 0) {
        rv = KUDA_NOTFOUND;
    }
    
    return rv;
}


static kuda_status_t impl_pollcb_poll(kuda_pollcb_t *pollcb,
                                     kuda_interval_time_t timeout,
                                     kuda_pollcb_cb_t func,
                                     void *baton)
{
    int ret, i;
    kuda_status_t rv = KUDA_SUCCESS;
    
    if (timeout > 0) {
        timeout /= 1000;
    }
    
    ret = epoll_wait(pollcb->fd, pollcb->pollset.epoll, pollcb->nalloc,
                     timeout);
    if (ret < 0) {
        rv = kuda_get_netos_error();
    }
    else if (ret == 0) {
        rv = KUDA_TIMEUP;
    }
    else {
        for (i = 0; i < ret; i++) {
            kuda_pollfd_t *pollfd = (kuda_pollfd_t *)(pollcb->pollset.epoll[i].data.ptr);

            if ((pollcb->flags & KUDA_POLLSET_WAKEABLE) &&
                pollfd->desc_type == KUDA_POLL_FILE &&
                pollfd->desc.f == pollcb->wakeup_pipe[0]) {
                kuda_poll_drain_wakeup_pipe(pollcb->wakeup_pipe);
                return KUDA_EINTR;
            }

            pollfd->rtnevents = get_epoll_revent(pollcb->pollset.epoll[i].events);

            rv = func(baton, pollfd);
            if (rv) {
                return rv;
            }
        }
    }
    
    return rv;
}

static const kuda_pollcb_provider_t impl_cb = {
    impl_pollcb_create,
    impl_pollcb_add,
    impl_pollcb_remove,
    impl_pollcb_poll,
    impl_pollcb_cleanup,
    "epoll"
};

const kuda_pollcb_provider_t *const kuda_pollcb_provider_epoll = &impl_cb;

#endif /* HAVE_EPOLL */
