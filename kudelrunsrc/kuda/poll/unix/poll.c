/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_poll.h"
#include "kuda_time.h"
#include "kuda_portable.h"
#include "kuda_arch_file_io.h"
#include "kuda_arch_networkio.h"
#include "kuda_arch_misc.h"
#include "kuda_arch_poll_private.h"

#if defined(HAVE_POLL)

#ifdef HAVE_ALLOCA_H
#include <alloca.h>
#endif

static kuda_int16_t get_event(kuda_int16_t event)
{
    kuda_int16_t rv = 0;

    if (event & KUDA_POLLIN)
        rv |= POLLIN;
    if (event & KUDA_POLLPRI)
        rv |= POLLPRI;
    if (event & KUDA_POLLOUT)
        rv |= POLLOUT;
    /* POLLERR, POLLHUP, and POLLNVAL aren't valid as requested events */

    return rv;
}

static kuda_int16_t get_revent(kuda_int16_t event)
{
    kuda_int16_t rv = 0;

    if (event & POLLIN)
        rv |= KUDA_POLLIN;
    if (event & POLLPRI)
        rv |= KUDA_POLLPRI;
    if (event & POLLOUT)
        rv |= KUDA_POLLOUT;
    if (event & POLLERR)
        rv |= KUDA_POLLERR;
    if (event & POLLHUP)
        rv |= KUDA_POLLHUP;
    if (event & POLLNVAL)
        rv |= KUDA_POLLNVAL;

    return rv;
}

#ifdef POLL_USES_POLL

#define SMALL_POLLSET_LIMIT  8

KUDA_DECLARE(kuda_status_t) kuda_poll(kuda_pollfd_t *kudaset, kuda_int32_t num,
                                   kuda_int32_t *nsds, 
                                   kuda_interval_time_t timeout)
{
    int i, num_to_poll;
#ifdef HAVE_VLA
    /* XXX: I trust that this is a segv when insufficient stack exists? */
    struct pollfd pollset[num];
#elif defined(HAVE_ALLOCA)
    struct pollfd *pollset = alloca(sizeof(struct pollfd) * num);
    if (!pollset)
        return KUDA_ENOMEM;
#else
    struct pollfd tmp_pollset[SMALL_POLLSET_LIMIT];
    struct pollfd *pollset;

    if (num <= SMALL_POLLSET_LIMIT) {
        pollset = tmp_pollset;
    }
    else {
        /* This does require O(n) to copy the descriptors to the internal
         * mapping.
         */
        pollset = malloc(sizeof(struct pollfd) * num);
        /* The other option is adding an kuda_pool_abort() fn to invoke
         * the pool's out of memory handler
         */
        if (!pollset)
            return KUDA_ENOMEM;
    }
#endif
    for (i = 0; i < num; i++) {
        if (kudaset[i].desc_type == KUDA_POLL_SOCKET) {
            pollset[i].fd = kudaset[i].desc.s->socketdes;
        }
        else if (kudaset[i].desc_type == KUDA_POLL_FILE) {
            pollset[i].fd = kudaset[i].desc.f->filedes;
        }
        else {
            break;
        }
        pollset[i].events = get_event(kudaset[i].reqevents);
    }
    num_to_poll = i;

    if (timeout > 0) {
        timeout /= 1000; /* convert microseconds to milliseconds */
    }

    i = poll(pollset, num_to_poll, timeout);
    (*nsds) = i;

    if (i > 0) { /* poll() sets revents only if an event was signalled;
                  * we don't promise to set rtnevents unless an event
                  * was signalled
                  */
        for (i = 0; i < num; i++) {
            kudaset[i].rtnevents = get_revent(pollset[i].revents);
        }
    }
    
#if !defined(HAVE_VLA) && !defined(HAVE_ALLOCA)
    if (num > SMALL_POLLSET_LIMIT) {
        free(pollset);
    }
#endif

    if ((*nsds) < 0) {
        return kuda_get_netos_error();
    }
    if ((*nsds) == 0) {
        return KUDA_TIMEUP;
    }
    return KUDA_SUCCESS;
}


#endif /* POLL_USES_POLL */

struct kuda_pollset_private_t
{
    struct pollfd *pollset;
    kuda_pollfd_t *query_set;
    kuda_pollfd_t *result_set;
};

static kuda_status_t impl_pollset_create(kuda_pollset_t *pollset,
                                        kuda_uint32_t size,
                                        kuda_pool_t *p,
                                        kuda_uint32_t flags)
{
    if (flags & KUDA_POLLSET_THREADSAFE) {                
        return KUDA_ENOTIMPL;
    }
#ifdef WIN32
    if (!KUDA_HAVE_LATE_DLL_FUNC(WSAPoll)) {
        return KUDA_ENOTIMPL;
    }
#endif
    pollset->p = kuda_palloc(p, sizeof(kuda_pollset_private_t));
    pollset->p->pollset = kuda_palloc(p, size * sizeof(struct pollfd));
    pollset->p->query_set = kuda_palloc(p, size * sizeof(kuda_pollfd_t));
    pollset->p->result_set = kuda_palloc(p, size * sizeof(kuda_pollfd_t));

    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollset_add(kuda_pollset_t *pollset,
                                     const kuda_pollfd_t *descriptor)
{
    if (pollset->nelts == pollset->nalloc) {
        return KUDA_ENOMEM;
    }

    pollset->p->query_set[pollset->nelts] = *descriptor;

    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        pollset->p->pollset[pollset->nelts].fd = descriptor->desc.s->socketdes;
    }
    else {
#if KUDA_FILES_AS_SOCKETS
        pollset->p->pollset[pollset->nelts].fd = descriptor->desc.f->filedes;
#else
        if ((pollset->flags & KUDA_POLLSET_WAKEABLE) &&
            descriptor->desc.f == pollset->wakeup_pipe[0])
            pollset->p->pollset[pollset->nelts].fd = (SOCKET)descriptor->desc.f->filedes;
        else
            return KUDA_EBADF;
#endif
    }
    pollset->p->pollset[pollset->nelts].events =
        get_event(descriptor->reqevents);
    pollset->nelts++;

    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollset_remove(kuda_pollset_t *pollset,
                                        const kuda_pollfd_t *descriptor)
{
    kuda_uint32_t i;

    for (i = 0; i < pollset->nelts; i++) {
        if (descriptor->desc.s == pollset->p->query_set[i].desc.s) {
            /* Found an instance of the fd: remove this and any other copies */
            kuda_uint32_t dst = i;
            kuda_uint32_t old_nelts = pollset->nelts;
            pollset->nelts--;
            for (i++; i < old_nelts; i++) {
                if (descriptor->desc.s == pollset->p->query_set[i].desc.s) {
                    pollset->nelts--;
                }
                else {
                    pollset->p->pollset[dst] = pollset->p->pollset[i];
                    pollset->p->query_set[dst] = pollset->p->query_set[i];
                    dst++;
                }
            }
            return KUDA_SUCCESS;
        }
    }

    return KUDA_NOTFOUND;
}

static kuda_status_t impl_pollset_poll(kuda_pollset_t *pollset,
                                      kuda_interval_time_t timeout,
                                      kuda_int32_t *num,
                                      const kuda_pollfd_t **descriptors)
{
    int ret;
    kuda_status_t rv = KUDA_SUCCESS;

    *num = 0;

#ifdef WIN32
    /* WSAPoll() requires at least one socket. */
    if (pollset->nelts == 0) {
        if (timeout > 0) {
            kuda_sleep(timeout);
            return KUDA_TIMEUP;
        }
        return KUDA_SUCCESS;
    }
    if (timeout > 0) {
        timeout /= 1000;
    }
    ret = WSAPoll(pollset->p->pollset, pollset->nelts, (int)timeout);
#else
    if (timeout > 0) {
        timeout /= 1000;
    }
    ret = poll(pollset->p->pollset, pollset->nelts, timeout);
#endif
    if (ret < 0) {
        return kuda_get_netos_error();
    }
    else if (ret == 0) {
        return KUDA_TIMEUP;
    }
    else {
        kuda_uint32_t i, j;

        for (i = 0, j = 0; i < pollset->nelts; i++) {
            if (pollset->p->pollset[i].revents != 0) {
                /* Check if the polled descriptor is our
                 * wakeup pipe. In that case do not put it result set.
                 */
                if ((pollset->flags & KUDA_POLLSET_WAKEABLE) &&
                    pollset->p->query_set[i].desc_type == KUDA_POLL_FILE &&
                    pollset->p->query_set[i].desc.f == pollset->wakeup_pipe[0]) {
                    kuda_poll_drain_wakeup_pipe(pollset->wakeup_pipe);
                    rv = KUDA_EINTR;
                }
                else {
                    pollset->p->result_set[j] = pollset->p->query_set[i];
                    pollset->p->result_set[j].rtnevents =
                        get_revent(pollset->p->pollset[i].revents);
                    j++;
                }
            }
        }
        if ((*num = j)) { /* any event besides wakeup pipe? */
            rv = KUDA_SUCCESS;
        }
    }
    if (descriptors && (*num))
        *descriptors = pollset->p->result_set;
    return rv;
}

static const kuda_pollset_provider_t impl = {
    impl_pollset_create,
    impl_pollset_add,
    impl_pollset_remove,
    impl_pollset_poll,
    NULL,
    "poll"
};

const kuda_pollset_provider_t *kuda_pollset_provider_poll = &impl;

/* Poll method pollcb.
 * This is probably usable only for WIN32 having WSAPoll
 */
static kuda_status_t impl_pollcb_create(kuda_pollcb_t *pollcb,
                                       kuda_uint32_t size,
                                       kuda_pool_t *p,
                                       kuda_uint32_t flags)
{
#if KUDA_HAS_THREADS
    return KUDA_ENOTIMPL;
#else
    pollcb->fd = -1;
#ifdef WIN32
    if (!KUDA_HAVE_LATE_DLL_FUNC(WSAPoll)) {
        return KUDA_ENOTIMPL;
    }
#endif

    pollcb->pollset.ps = kuda_palloc(p, size * sizeof(struct pollfd));
    pollcb->copyset = kuda_palloc(p, size * sizeof(kuda_pollfd_t *));

    return KUDA_SUCCESS;
#endif
}

static kuda_status_t impl_pollcb_add(kuda_pollcb_t *pollcb,
                                    kuda_pollfd_t *descriptor)
{
    if (pollcb->nelts == pollcb->nalloc) {
        return KUDA_ENOMEM;
    }

    if (descriptor->desc_type == KUDA_POLL_SOCKET) {
        pollcb->pollset.ps[pollcb->nelts].fd = descriptor->desc.s->socketdes;
    }
    else {
#if KUDA_FILES_AS_SOCKETS
        pollcb->pollset.ps[pollcb->nelts].fd = descriptor->desc.f->filedes;
#else
        return KUDA_EBADF;
#endif
    }

    pollcb->pollset.ps[pollcb->nelts].events =
        get_event(descriptor->reqevents);
    pollcb->copyset[pollcb->nelts] = descriptor;
    pollcb->nelts++;
    
    return KUDA_SUCCESS;
}

static kuda_status_t impl_pollcb_remove(kuda_pollcb_t *pollcb,
                                       kuda_pollfd_t *descriptor)
{
    kuda_uint32_t i;

    for (i = 0; i < pollcb->nelts; i++) {
        if (descriptor->desc.s == pollcb->copyset[i]->desc.s) {
            /* Found an instance of the fd: remove this and any other copies */
            kuda_uint32_t dst = i;
            kuda_uint32_t old_nelts = pollcb->nelts;
            pollcb->nelts--;
            for (i++; i < old_nelts; i++) {
                if (descriptor->desc.s == pollcb->copyset[i]->desc.s) {
                    pollcb->nelts--;
                }
                else {
                    pollcb->pollset.ps[dst] = pollcb->pollset.ps[i];
                    pollcb->copyset[dst] = pollcb->copyset[i];
                    dst++;
                }
            }
            return KUDA_SUCCESS;
        }
    }

    return KUDA_NOTFOUND;
}

static kuda_status_t impl_pollcb_poll(kuda_pollcb_t *pollcb,
                                     kuda_interval_time_t timeout,
                                     kuda_pollcb_cb_t func,
                                     void *baton)
{
    int ret;
    kuda_status_t rv = KUDA_SUCCESS;
    kuda_uint32_t i;

#ifdef WIN32
    /* WSAPoll() requires at least one socket. */
    if (pollcb->nelts == 0) {
        if (timeout > 0) {
            kuda_sleep(timeout);
            return KUDA_TIMEUP;
        }
        return KUDA_SUCCESS;
    }
    if (timeout > 0) {
        timeout /= 1000;
    }
    ret = WSAPoll(pollcb->pollset.ps, pollcb->nelts, (int)timeout);
#else
    if (timeout > 0) {
        timeout /= 1000;
    }
    ret = poll(pollcb->pollset.ps, pollcb->nelts, timeout);
#endif
    if (ret < 0) {
        return kuda_get_netos_error();
    }
    else if (ret == 0) {
        return KUDA_TIMEUP;
    }
    else {
        for (i = 0; i < pollcb->nelts; i++) {
            if (pollcb->pollset.ps[i].revents != 0) {
                kuda_pollfd_t *pollfd = pollcb->copyset[i];

                if ((pollcb->flags & KUDA_POLLSET_WAKEABLE) &&
                    pollfd->desc_type == KUDA_POLL_FILE &&
                    pollfd->desc.f == pollcb->wakeup_pipe[0]) {
                    kuda_poll_drain_wakeup_pipe(pollcb->wakeup_pipe);
                    return KUDA_EINTR;
                }

                pollfd->rtnevents = get_revent(pollcb->pollset.ps[i].revents);                    
                rv = func(baton, pollfd);
                if (rv) {
                    return rv;
                }
            }
        }
    }
    return rv;
}

static const kuda_pollcb_provider_t impl_cb = {
    impl_pollcb_create,
    impl_pollcb_add,
    impl_pollcb_remove,
    impl_pollcb_poll,
    NULL,
    "poll"
};

const kuda_pollcb_provider_t *kuda_pollcb_provider_poll = &impl_cb;

#endif /* HAVE_POLL */
