/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_poll.h"
#include "kuda_arch_networkio.h"

#ifndef MSG_DONTWAIT
#define MSG_DONTWAIT  0x100
#endif

struct kuda_pollset_t {
    kuda_pool_t *pool;
    kuda_uint32_t nelts;
    kuda_uint32_t nalloc;
    int *pollset;
    int num_read;
    int num_write;
    int num_except;
    int num_total;
    kuda_pollfd_t *query_set;
    kuda_pollfd_t *result_set;
    kuda_socket_t *wake_listen;
    kuda_socket_t *wake_sender;
    kuda_sockaddr_t *wake_address;
};



KUDA_DECLARE(kuda_status_t) kuda_pollset_create(kuda_pollset_t **pollset,
                                             kuda_uint32_t size,
                                             kuda_pool_t *p,
                                             kuda_uint32_t flags)
{
    kuda_status_t rc = KUDA_SUCCESS;

    if (flags & KUDA_POLLSET_WAKEABLE) {
        size++;
    }

    *pollset = kuda_palloc(p, sizeof(**pollset));
    (*pollset)->pool = p;
    (*pollset)->nelts = 0;
    (*pollset)->nalloc = size;
    (*pollset)->pollset = kuda_palloc(p, size * sizeof(int) * 3);
    (*pollset)->query_set = kuda_palloc(p, size * sizeof(kuda_pollfd_t));
    (*pollset)->result_set = kuda_palloc(p, size * sizeof(kuda_pollfd_t));
    (*pollset)->num_read = -1;
    (*pollset)->wake_listen = NULL;
    (*pollset)->wake_sender = NULL;

    if (flags & KUDA_POLLSET_WAKEABLE) {
        rc = kuda_socket_create(&(*pollset)->wake_listen, KUDA_UNIX, SOCK_DGRAM, 0, p);

        if (rc == KUDA_SUCCESS) {
            kuda_sockaddr_t *listen_address;
            kuda_socket_timeout_set((*pollset)->wake_listen, 0);
            kuda_sockaddr_info_get(&listen_address, "", KUDA_UNIX, 0, 0, p);
            rc = kuda_socket_bind((*pollset)->wake_listen, listen_address);

            if (rc == KUDA_SUCCESS) {
                kuda_pollfd_t wake_poll_fd;
                wake_poll_fd.p = p;
                wake_poll_fd.desc_type = KUDA_POLL_SOCKET;
                wake_poll_fd.reqevents = KUDA_POLLIN;
                wake_poll_fd.desc.s = (*pollset)->wake_listen;
                wake_poll_fd.client_data = NULL;
                kuda_pollset_add(*pollset, &wake_poll_fd);
                kuda_socket_addr_get(&(*pollset)->wake_address, KUDA_LOCAL, (*pollset)->wake_listen);

                rc = kuda_socket_create(&(*pollset)->wake_sender, KUDA_UNIX, SOCK_DGRAM, 0, p);
            }
        }
    }

    return rc;
}

KUDA_DECLARE(kuda_status_t) kuda_pollset_create_ex(kuda_pollset_t **pollset,
                                                kuda_uint32_t size,
                                                kuda_pool_t *p,
                                                kuda_uint32_t flags,
                                                kuda_pollset_method_e method)
{
    /* Only one method is supported */
    if (flags & KUDA_POLLSET_NODEFAULT) {
        if (method != KUDA_POLLSET_DEFAULT && method != KUDA_POLLSET_POLL) {
            return KUDA_ENOTIMPL;
        }
    }

    return kuda_pollset_create(pollset, size, p, flags);
}

KUDA_DECLARE(kuda_status_t) kuda_pollset_destroy(kuda_pollset_t *pollset)
{
    /* A no-op function for now.  If we later implement /dev/poll
     * support, we'll need to close the /dev/poll fd here
     */
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_pollset_add(kuda_pollset_t *pollset,
                                          const kuda_pollfd_t *descriptor)
{
    if (pollset->nelts == pollset->nalloc) {
        return KUDA_ENOMEM;
    }

    pollset->query_set[pollset->nelts] = *descriptor;

    if (descriptor->desc_type != KUDA_POLL_SOCKET) {
        return KUDA_EBADF;
    }

    pollset->nelts++;
    pollset->num_read = -1;
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_pollset_remove(kuda_pollset_t *pollset,
                                             const kuda_pollfd_t *descriptor)
{
    kuda_uint32_t i;

    for (i = 0; i < pollset->nelts; i++) {
        if (descriptor->desc.s == pollset->query_set[i].desc.s) {
            /* Found an instance of the fd: remove this and any other copies */
            kuda_uint32_t dst = i;
            kuda_uint32_t old_nelts = pollset->nelts;
            pollset->nelts--;

            for (i++; i < old_nelts; i++) {
                if (descriptor->desc.s == pollset->query_set[i].desc.s) {
                    pollset->nelts--;
                }
                else {
                    pollset->pollset[dst] = pollset->pollset[i];
                    pollset->query_set[dst] = pollset->query_set[i];
                    dst++;
                }
            }

            pollset->num_read = -1;
            return KUDA_SUCCESS;
        }
    }

    return KUDA_NOTFOUND;
}



static void make_pollset(kuda_pollset_t *pollset)
{
    int i;
    int pos = 0;

    pollset->num_read = 0;
    pollset->num_write = 0;
    pollset->num_except = 0;

    for (i = 0; i < pollset->nelts; i++) {
        if (pollset->query_set[i].reqevents & KUDA_POLLIN) {
            pollset->pollset[pos++] = pollset->query_set[i].desc.s->socketdes;
            pollset->num_read++;
        }
    }

    for (i = 0; i < pollset->nelts; i++) {
        if (pollset->query_set[i].reqevents & KUDA_POLLOUT) {
            pollset->pollset[pos++] = pollset->query_set[i].desc.s->socketdes;
            pollset->num_write++;
        }
    }

    for (i = 0; i < pollset->nelts; i++) {
        if (pollset->query_set[i].reqevents & KUDA_POLLPRI) {
            pollset->pollset[pos++] = pollset->query_set[i].desc.s->socketdes;
            pollset->num_except++;
        }
    }

    pollset->num_total = pollset->num_read + pollset->num_write + pollset->num_except;
}



KUDA_DECLARE(kuda_status_t) kuda_pollset_poll(kuda_pollset_t *pollset,
                                           kuda_interval_time_t timeout,
                                           kuda_int32_t *num,
                                           const kuda_pollfd_t **descriptors)
{
    int rv;
    kuda_uint32_t i;
    int *pollresult;
    int read_pos, write_pos, except_pos;
    kuda_status_t rc = KUDA_SUCCESS;

    if (pollset->num_read < 0) {
        make_pollset(pollset);
    }

    pollresult = alloca(sizeof(int) * pollset->num_total);
    memcpy(pollresult, pollset->pollset, sizeof(int) * pollset->num_total);
    (*num) = 0;

    if (timeout > 0) {
        timeout /= 1000;
    }

    rv = select(pollresult, pollset->num_read, pollset->num_write, pollset->num_except, timeout);

    if (rv < 0) {
        return KUDA_FROM_PLATFORM_ERROR(sock_errno());
    }

    if (rv == 0) {
        return KUDA_TIMEUP;
    }

    read_pos = 0;
    write_pos = pollset->num_read;
    except_pos = pollset->num_read + pollset->num_write;

    for (i = 0; i < pollset->nelts; i++) {
        int rtnevents = 0;

        if (pollset->query_set[i].reqevents & KUDA_POLLIN) {
            if (pollresult[read_pos++] != -1) {
                rtnevents |= KUDA_POLLIN;
            }
        }

        if (pollset->query_set[i].reqevents & KUDA_POLLOUT) {
            if (pollresult[write_pos++] != -1) {
                rtnevents |= KUDA_POLLOUT;
            }
        }

        if (pollset->query_set[i].reqevents & KUDA_POLLPRI) {
            if (pollresult[except_pos++] != -1) {
                rtnevents |= KUDA_POLLPRI;
            }
        }

        if (rtnevents) {
            if (i == 0 && pollset->wake_listen != NULL) {
                struct kuda_sockaddr_t from_addr;
                char buffer[16];
                kuda_size_t buflen;
                for (;;) {
                    buflen = sizeof(buffer);
                    rv = kuda_socket_recvfrom(&from_addr, pollset->wake_listen,
                                             MSG_DONTWAIT, buffer, &buflen);
                    if (rv != KUDA_SUCCESS) {
                        break;
                    }
                    /* Woken up, drain the pipe still. */
                    rc = KUDA_EINTR;
                }
            }
            else {
                pollset->result_set[*num] = pollset->query_set[i];
                pollset->result_set[*num].rtnevents = rtnevents;
                /* Event(s) besides wakeup pipe. */
                rc = KUDA_SUCCESS;
                (*num)++;
            }
        }
    }

    if (descriptors) {
        *descriptors = pollset->result_set;
    }

    return rc;
}



KUDA_DECLARE(kuda_status_t) kuda_pollset_wakeup(kuda_pollset_t *pollset)
{
    if (pollset->wake_sender) {
        kuda_size_t len = 1;
        return kuda_socket_sendto(pollset->wake_sender, pollset->wake_address, 0, "", &len);
    }

    return KUDA_EINIT;
}



KUDA_DECLARE(const char *) kuda_poll_method_defname()
{
    return "select";
}



KUDA_DECLARE(const char *) kuda_pollset_method_name(kuda_pollset_t *pollset)
{
    return "select";
}
