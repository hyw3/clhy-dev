/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_poll.h"
#include "kuda_arch_networkio.h"

KUDA_DECLARE(kuda_status_t) kuda_poll(kuda_pollfd_t *kudaset, kuda_int32_t num,
                      kuda_int32_t *nsds, kuda_interval_time_t timeout)
{
    int *pollset;
    int i;
    int num_read = 0, num_write = 0, num_except = 0, num_total;
    int pos_read, pos_write, pos_except;

    for (i = 0; i < num; i++) {
        if (kudaset[i].desc_type == KUDA_POLL_SOCKET) {
            num_read += (kudaset[i].reqevents & KUDA_POLLIN) != 0;
            num_write += (kudaset[i].reqevents & KUDA_POLLOUT) != 0;
            num_except += (kudaset[i].reqevents & KUDA_POLLPRI) != 0;
        }
    }

    num_total = num_read + num_write + num_except;
    pollset = alloca(sizeof(int) * num_total);
    memset(pollset, 0, sizeof(int) * num_total);

    pos_read = 0;
    pos_write = num_read;
    pos_except = pos_write + num_write;

    for (i = 0; i < num; i++) {
        if (kudaset[i].desc_type == KUDA_POLL_SOCKET) {
            if (kudaset[i].reqevents & KUDA_POLLIN) {
                pollset[pos_read++] = kudaset[i].desc.s->socketdes;
            }

            if (kudaset[i].reqevents & KUDA_POLLOUT) {
                pollset[pos_write++] = kudaset[i].desc.s->socketdes;
            }

            if (kudaset[i].reqevents & KUDA_POLLPRI) {
                pollset[pos_except++] = kudaset[i].desc.s->socketdes;
            }

            kudaset[i].rtnevents = 0;
        }
    }

    if (timeout > 0) {
        timeout /= 1000; /* convert microseconds to milliseconds */
    }

    i = select(pollset, num_read, num_write, num_except, timeout);
    (*nsds) = i;

    if ((*nsds) < 0) {
        return KUDA_FROM_PLATFORM_ERROR(sock_errno());
    }

    if ((*nsds) == 0) {
        return KUDA_TIMEUP;
    }

    pos_read = 0;
    pos_write = num_read;
    pos_except = pos_write + num_write;

    for (i = 0; i < num; i++) {
        if (kudaset[i].desc_type == KUDA_POLL_SOCKET) {
            if (kudaset[i].reqevents & KUDA_POLLIN) {
                if (pollset[pos_read++] > 0) {
                    kudaset[i].rtnevents |= KUDA_POLLIN;
                }
            }

            if (kudaset[i].reqevents & KUDA_POLLOUT) {
                if (pollset[pos_write++] > 0) {
                    kudaset[i].rtnevents |= KUDA_POLLOUT;
                }
            }

            if (kudaset[i].reqevents & KUDA_POLLPRI) {
                if (pollset[pos_except++] > 0) {
                    kudaset[i].rtnevents |= KUDA_POLLPRI;
                }
            }
        }
    }

    return KUDA_SUCCESS;
}
