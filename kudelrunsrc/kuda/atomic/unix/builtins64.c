/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_atomic.h"

#ifdef USE_ATOMICS_BUILTINS

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_read64(volatile kuda_uint64_t *mem)
{
    return *mem;
}

KUDA_DECLARE(void) kuda_atomic_set64(volatile kuda_uint64_t *mem, kuda_uint64_t val)
{
    *mem = val;
}

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_add64(volatile kuda_uint64_t *mem, kuda_uint64_t val)
{
    return __sync_fetch_and_add(mem, val);
}

KUDA_DECLARE(void) kuda_atomic_sub64(volatile kuda_uint64_t *mem, kuda_uint64_t val)
{
    __sync_fetch_and_sub(mem, val);
}

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_inc64(volatile kuda_uint64_t *mem)
{
    return __sync_fetch_and_add(mem, 1);
}

KUDA_DECLARE(int) kuda_atomic_dec64(volatile kuda_uint64_t *mem)
{
    return __sync_sub_and_fetch(mem, 1);
}

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_cas64(volatile kuda_uint64_t *mem, kuda_uint64_t with,
                                           kuda_uint64_t cmp)
{
    return __sync_val_compare_and_swap(mem, cmp, with);
}

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_xchg64(volatile kuda_uint64_t *mem, kuda_uint64_t val)
{
    __sync_synchronize();

    return __sync_lock_test_and_set(mem, val);
}

#endif /* USE_ATOMICS_BUILTINS */
