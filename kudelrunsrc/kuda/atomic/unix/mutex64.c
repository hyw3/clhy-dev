/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_atomic.h"
#include "kuda_thread_mutex.h"

#if defined(USE_ATOMICS_GENERIC) || defined (NEED_ATOMICS_GENERIC64)

#include <stdlib.h>

#if KUDA_HAS_THREADS
#   define DECLARE_MUTEX_LOCKED(name, mem)  \
        kuda_thread_mutex_t *name = mutex_hash(mem)
#   define MUTEX_UNLOCK(name)                                   \
        do {                                                    \
            if (kuda_thread_mutex_unlock(name) != KUDA_SUCCESS)   \
                abort();                                        \
        } while (0)
#else
#   define DECLARE_MUTEX_LOCKED(name, mem)
#   define MUTEX_UNLOCK(name)
#   warning Be warned: using stubs for all atomic operations
#endif

#if KUDA_HAS_THREADS

static kuda_thread_mutex_t **hash_mutex;

#define NUM_ATOMIC_HASH 7
/* shift by 2 to get rid of alignment issues */
#define ATOMIC_HASH(x) (unsigned int)(((unsigned long)(x)>>2)%(unsigned int)NUM_ATOMIC_HASH)

static kuda_status_t atomic_cleanup(void *data)
{
    if (hash_mutex == data)
        hash_mutex = NULL;

    return KUDA_SUCCESS;
}

kuda_status_t kuda__atomic_generic64_init(kuda_pool_t *p)
{
    int i;
    kuda_status_t rv;

    if (hash_mutex != NULL)
        return KUDA_SUCCESS;

    hash_mutex = kuda_palloc(p, sizeof(kuda_thread_mutex_t*) * NUM_ATOMIC_HASH);
    kuda_pool_cleanup_register(p, hash_mutex, atomic_cleanup,
                              kuda_pool_cleanup_null);

    for (i = 0; i < NUM_ATOMIC_HASH; i++) {
        rv = kuda_thread_mutex_create(&(hash_mutex[i]),
                                     KUDA_THREAD_MUTEX_DEFAULT, p);
        if (rv != KUDA_SUCCESS) {
           return rv;
        }
    }

    return KUDA_SUCCESS;
}

static KUDA_INLINE kuda_thread_mutex_t *mutex_hash(volatile kuda_uint64_t *mem)
{
    kuda_thread_mutex_t *mutex = hash_mutex[ATOMIC_HASH(mem)];

    if (kuda_thread_mutex_lock(mutex) != KUDA_SUCCESS) {
        abort();
    }

    return mutex;
}

#else

kuda_status_t kuda__atomic_generic64_init(kuda_pool_t *p)
{
    return KUDA_SUCCESS;
}

#endif /* KUDA_HAS_THREADS */

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_read64(volatile kuda_uint64_t *mem)
{
    return *mem;
}

KUDA_DECLARE(void) kuda_atomic_set64(volatile kuda_uint64_t *mem, kuda_uint64_t val)
{
    DECLARE_MUTEX_LOCKED(mutex, mem);

    *mem = val;

    MUTEX_UNLOCK(mutex);
}

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_add64(volatile kuda_uint64_t *mem, kuda_uint64_t val)
{
    kuda_uint64_t old_value;
    DECLARE_MUTEX_LOCKED(mutex, mem);

    old_value = *mem;
    *mem += val;

    MUTEX_UNLOCK(mutex);

    return old_value;
}

KUDA_DECLARE(void) kuda_atomic_sub64(volatile kuda_uint64_t *mem, kuda_uint64_t val)
{
    DECLARE_MUTEX_LOCKED(mutex, mem);
    *mem -= val;
    MUTEX_UNLOCK(mutex);
}

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_inc64(volatile kuda_uint64_t *mem)
{
    return kuda_atomic_add64(mem, 1);
}

KUDA_DECLARE(int) kuda_atomic_dec64(volatile kuda_uint64_t *mem)
{
    kuda_uint64_t new;
    DECLARE_MUTEX_LOCKED(mutex, mem);

    (*mem)--;
    new = *mem;

    MUTEX_UNLOCK(mutex);

    return new;
}

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_cas64(volatile kuda_uint64_t *mem, kuda_uint64_t with,
                              kuda_uint64_t cmp)
{
    kuda_uint64_t prev;
    DECLARE_MUTEX_LOCKED(mutex, mem);

    prev = *mem;
    if (prev == cmp) {
        *mem = with;
    }

    MUTEX_UNLOCK(mutex);

    return prev;
}

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_xchg64(volatile kuda_uint64_t *mem, kuda_uint64_t val)
{
    kuda_uint64_t prev;
    DECLARE_MUTEX_LOCKED(mutex, mem);

    prev = *mem;
    *mem = val;

    MUTEX_UNLOCK(mutex);

    return prev;
}

#endif /* USE_ATOMICS_GENERIC64 */
