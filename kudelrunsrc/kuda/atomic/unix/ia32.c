/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_atomic.h"

#ifdef USE_ATOMICS_IA32

KUDA_DECLARE(kuda_status_t) kuda_atomic_init(kuda_pool_t *p)
{
#if defined (NEED_ATOMICS_GENERIC64)
    return kuda__atomic_generic64_init(p);
#else
    return KUDA_SUCCESS;
#endif
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_read32(volatile kuda_uint32_t *mem)
{
    return *mem;
}

KUDA_DECLARE(void) kuda_atomic_set32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    *mem = val;
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_add32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    asm volatile ("lock; xaddl %0,%1"
                  : "=r" (val), "=m" (*mem)
                  : "0" (val), "m" (*mem)
                  : "memory", "cc");
    return val;
}

KUDA_DECLARE(void) kuda_atomic_sub32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    asm volatile ("lock; subl %1, %0"
                  : /* no output */
                  : "m" (*(mem)), "r" (val)
                  : "memory", "cc");
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_inc32(volatile kuda_uint32_t *mem)
{
    return kuda_atomic_add32(mem, 1);
}

KUDA_DECLARE(int) kuda_atomic_dec32(volatile kuda_uint32_t *mem)
{
    unsigned char prev;

    asm volatile ("lock; decl %0; setnz %1"
                  : "=m" (*mem), "=qm" (prev)
                  : "m" (*mem)
                  : "memory");

    return prev;
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_cas32(volatile kuda_uint32_t *mem, kuda_uint32_t with,
                                           kuda_uint32_t cmp)
{
    kuda_uint32_t prev;

    asm volatile ("lock; cmpxchgl %1, %2"
                  : "=a" (prev)
                  : "r" (with), "m" (*(mem)), "0"(cmp)
                  : "memory", "cc");
    return prev;
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_xchg32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    kuda_uint32_t prev = val;

    asm volatile ("xchgl %0, %1"
                  : "=r" (prev), "+m" (*mem)
                  : "0" (prev));
    return prev;
}

KUDA_DECLARE(void*) kuda_atomic_casptr(volatile void **mem, void *with, const void *cmp)
{
    void *prev;
#if KUDA_SIZEOF_VOIDP == 4
    asm volatile ("lock; cmpxchgl %2, %1"
                  : "=a" (prev), "=m" (*mem)
                  : "r" (with), "m" (*mem), "0" (cmp));
#elif KUDA_SIZEOF_VOIDP == 8
    asm volatile ("lock; cmpxchgq %q2, %1"
                  : "=a" (prev), "=m" (*mem)
                  : "r" ((unsigned long)with), "m" (*mem),
                    "0" ((unsigned long)cmp));
#else
#error KUDA_SIZEOF_VOIDP value not supported
#endif
    return prev;
}

KUDA_DECLARE(void*) kuda_atomic_xchgptr(volatile void **mem, void *with)
{
    void *prev;
#if KUDA_SIZEOF_VOIDP == 4
    asm volatile ("xchgl %2, %1"
                  : "=a" (prev), "+m" (*mem)
                  : "0" (with));
#elif KUDA_SIZEOF_VOIDP == 8
    asm volatile ("xchgq %q2, %1"
                  : "=a" (prev), "+m" (*mem)
                  : "0" (with));
#else
#error KUDA_SIZEOF_VOIDP value not supported
#endif
    return prev;
}

#endif /* USE_ATOMICS_IA32 */
