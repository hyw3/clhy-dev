/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_atomic.h"

#ifdef USE_ATOMICS_S390

KUDA_DECLARE(kuda_status_t) kuda_atomic_init(kuda_pool_t *p)
{
#if defined (NEED_ATOMICS_GENERIC64)
    return kuda__atomic_generic64_init(p);
#else
    return KUDA_SUCCESS;
#endif
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_read32(volatile kuda_uint32_t *mem)
{
    return *mem;
}

KUDA_DECLARE(void) kuda_atomic_set32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    *mem = val;
}

static KUDA_INLINE kuda_uint32_t atomic_add(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    kuda_uint32_t prev = *mem, temp;

    asm volatile ("loop_%=:\n"
                  "    lr  %1,%0\n"
                  "    alr %1,%3\n"
                  "    cs  %0,%1,%2\n"
                  "    jl  loop_%=\n"
                  : "+d" (prev), "+d" (temp), "=Q" (*mem)
                  : "d" (val), "m" (*mem)
                  : "cc", "memory");

    return prev;
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_add32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    return atomic_add(mem, val);
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_inc32(volatile kuda_uint32_t *mem)
{
    return atomic_add(mem, 1);
}

static KUDA_INLINE kuda_uint32_t atomic_sub(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    kuda_uint32_t prev = *mem, temp;

    asm volatile ("loop_%=:\n"
                  "    lr  %1,%0\n"
                  "    slr %1,%3\n"
                  "    cs  %0,%1,%2\n"
                  "    jl  loop_%=\n"
                  : "+d" (prev), "+d" (temp), "=Q" (*mem)
                  : "d" (val), "m" (*mem)
                  : "cc", "memory");

    return temp;
}

KUDA_DECLARE(void) kuda_atomic_sub32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    atomic_sub(mem, val);
}

KUDA_DECLARE(int) kuda_atomic_dec32(volatile kuda_uint32_t *mem)
{
    return atomic_sub(mem, 1);
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_cas32(volatile kuda_uint32_t *mem, kuda_uint32_t with,
                                           kuda_uint32_t cmp)
{
    asm volatile ("    cs  %0,%2,%1\n"
                  : "+d" (cmp), "=Q" (*mem)
                  : "d" (with), "m" (*mem)
                  : "cc", "memory");

    return cmp;
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_xchg32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    kuda_uint32_t prev = *mem;

    asm volatile ("loop_%=:\n"
                  "    cs  %0,%2,%1\n"
                  "    jl  loop_%=\n"
                  : "+d" (prev), "=Q" (*mem)
                  : "d" (val), "m" (*mem)
                  : "cc", "memory");

    return prev;
}

KUDA_DECLARE(void*) kuda_atomic_casptr(volatile void **mem, void *with, const void *cmp)
{
    void *prev = (void *) cmp;
#if KUDA_SIZEOF_VOIDP == 4
    asm volatile ("    cs  %0,%2,%1\n"
                  : "+d" (prev), "=Q" (*mem)
                  : "d" (with), "m" (*mem)
                  : "cc", "memory");
#elif KUDA_SIZEOF_VOIDP == 8
    asm volatile ("    csg %0,%2,%1\n"
                  : "+d" (prev), "=Q" (*mem)
                  : "d" (with), "m" (*mem)
                  : "cc", "memory");
#else
#error KUDA_SIZEOF_VOIDP value not supported
#endif
    return prev;
}

KUDA_DECLARE(void*) kuda_atomic_xchgptr(volatile void **mem, void *with)
{
    void *prev = (void *) *mem;
#if KUDA_SIZEOF_VOIDP == 4
    asm volatile ("loop_%=:\n"
                  "    cs  %0,%2,%1\n"
                  "    jl  loop_%=\n"
                  : "+d" (prev), "=Q" (*mem)
                  : "d" (with), "m" (*mem)
                  : "cc", "memory");
#elif KUDA_SIZEOF_VOIDP == 8
    asm volatile ("loop_%=:\n"
                  "    csg %0,%2,%1\n"
                  "    jl  loop_%=\n"
                  : "+d" (prev), "=Q" (*mem)
                  : "d" (with), "m" (*mem)
                  : "cc", "memory");
#else
#error KUDA_SIZEOF_VOIDP value not supported
#endif
    return prev;
}

#endif /* USE_ATOMICS_S390 */
