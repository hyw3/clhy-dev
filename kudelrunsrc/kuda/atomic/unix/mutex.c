/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_atomic.h"
#include "kuda_thread_mutex.h"

#ifdef USE_ATOMICS_GENERIC

#include <stdlib.h>

#if KUDA_HAS_THREADS
#   define DECLARE_MUTEX_LOCKED(name, mem)  \
        kuda_thread_mutex_t *name = mutex_hash(mem)
#   define MUTEX_UNLOCK(name)                                   \
        do {                                                    \
            if (kuda_thread_mutex_unlock(name) != KUDA_SUCCESS)   \
                abort();                                        \
        } while (0)
#else
#   define DECLARE_MUTEX_LOCKED(name, mem)
#   define MUTEX_UNLOCK(name)
#   warning Be warned: using stubs for all atomic operations
#endif

#if KUDA_HAS_THREADS

static kuda_thread_mutex_t **hash_mutex;

#define NUM_ATOMIC_HASH 7
/* shift by 2 to get rid of alignment issues */
#define ATOMIC_HASH(x) (unsigned int)(((unsigned long)(x)>>2)%(unsigned int)NUM_ATOMIC_HASH)

static kuda_status_t atomic_cleanup(void *data)
{
    if (hash_mutex == data)
        hash_mutex = NULL;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_atomic_init(kuda_pool_t *p)
{
    int i;
    kuda_status_t rv;

    if (hash_mutex != NULL)
        return KUDA_SUCCESS;

    hash_mutex = kuda_palloc(p, sizeof(kuda_thread_mutex_t*) * NUM_ATOMIC_HASH);
    kuda_pool_cleanup_register(p, hash_mutex, atomic_cleanup,
                              kuda_pool_cleanup_null);

    for (i = 0; i < NUM_ATOMIC_HASH; i++) {
        rv = kuda_thread_mutex_create(&(hash_mutex[i]),
                                     KUDA_THREAD_MUTEX_DEFAULT, p);
        if (rv != KUDA_SUCCESS) {
           return rv;
        }
    }

    return kuda__atomic_generic64_init(p);
}

static KUDA_INLINE kuda_thread_mutex_t *mutex_hash(volatile kuda_uint32_t *mem)
{
    kuda_thread_mutex_t *mutex = hash_mutex[ATOMIC_HASH(mem)];

    if (kuda_thread_mutex_lock(mutex) != KUDA_SUCCESS) {
        abort();
    }

    return mutex;
}

#else

KUDA_DECLARE(kuda_status_t) kuda_atomic_init(kuda_pool_t *p)
{
    return kuda__atomic_generic64_init(p);
}

#endif /* KUDA_HAS_THREADS */

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_read32(volatile kuda_uint32_t *mem)
{
    return *mem;
}

KUDA_DECLARE(void) kuda_atomic_set32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    DECLARE_MUTEX_LOCKED(mutex, mem);

    *mem = val;

    MUTEX_UNLOCK(mutex);
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_add32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    kuda_uint32_t old_value;
    DECLARE_MUTEX_LOCKED(mutex, mem);

    old_value = *mem;
    *mem += val;

    MUTEX_UNLOCK(mutex);

    return old_value;
}

KUDA_DECLARE(void) kuda_atomic_sub32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    DECLARE_MUTEX_LOCKED(mutex, mem);
    *mem -= val;
    MUTEX_UNLOCK(mutex);
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_inc32(volatile kuda_uint32_t *mem)
{
    return kuda_atomic_add32(mem, 1);
}

KUDA_DECLARE(int) kuda_atomic_dec32(volatile kuda_uint32_t *mem)
{
    kuda_uint32_t new;
    DECLARE_MUTEX_LOCKED(mutex, mem);

    (*mem)--;
    new = *mem;

    MUTEX_UNLOCK(mutex);

    return new;
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_cas32(volatile kuda_uint32_t *mem, kuda_uint32_t with,
                              kuda_uint32_t cmp)
{
    kuda_uint32_t prev;
    DECLARE_MUTEX_LOCKED(mutex, mem);

    prev = *mem;
    if (prev == cmp) {
        *mem = with;
    }

    MUTEX_UNLOCK(mutex);

    return prev;
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_xchg32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    kuda_uint32_t prev;
    DECLARE_MUTEX_LOCKED(mutex, mem);

    prev = *mem;
    *mem = val;

    MUTEX_UNLOCK(mutex);

    return prev;
}

KUDA_DECLARE(void*) kuda_atomic_casptr(volatile void **mem, void *with, const void *cmp)
{
    void *prev;
    DECLARE_MUTEX_LOCKED(mutex, *mem);

    prev = *(void **)mem;
    if (prev == cmp) {
        *mem = with;
    }

    MUTEX_UNLOCK(mutex);

    return prev;
}

KUDA_DECLARE(void*) kuda_atomic_xchgptr(volatile void **mem, void *with)
{
    void *prev;
    DECLARE_MUTEX_LOCKED(mutex, *mem);

    prev = *(void **)mem;
    *mem = with;

    MUTEX_UNLOCK(mutex);

    return prev;
}

#endif /* USE_ATOMICS_GENERIC */
