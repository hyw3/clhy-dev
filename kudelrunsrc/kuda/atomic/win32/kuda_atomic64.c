/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_atomic.h"
#include "kuda_thread_mutex.h"

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_add64(volatile kuda_uint64_t *mem, kuda_uint64_t val)
{
#if (defined(_M_IA64) || defined(_M_AMD64))
    return InterlockedExchangeAdd64(mem, val);
#else
    return InterlockedExchangeAdd64((long *)mem, val);
#endif
}

/* Of course we want the 2's compliment of the unsigned value, val */
#ifdef _MSC_VER
#pragma warning(disable: 4146)
#endif

KUDA_DECLARE(void) kuda_atomic_sub64(volatile kuda_uint64_t *mem, kuda_uint64_t val)
{
#if (defined(_M_IA64) || defined(_M_AMD64))
    InterlockedExchangeAdd64(mem, -val);
#else
    InterlockedExchangeAdd64((long *)mem, -val);
#endif
}

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_inc64(volatile kuda_uint64_t *mem)
{
    /* we return old value, win64 returns new value :( */
#if (defined(_M_IA64) || defined(_M_AMD64)) && !defined(RC_INVOKED)
    return InterlockedIncrement64(mem) - 1;
#else
    return InterlockedIncrement64((long *)mem) - 1;
#endif
}

KUDA_DECLARE(int) kuda_atomic_dec64(volatile kuda_uint64_t *mem)
{
#if (defined(_M_IA64) || defined(_M_AMD64)) && !defined(RC_INVOKED)
    return InterlockedDecrement64(mem);
#else
    return InterlockedDecrement64((long *)mem);
#endif
}

KUDA_DECLARE(void) kuda_atomic_set64(volatile kuda_uint64_t *mem, kuda_uint64_t val)
{
#if (defined(_M_IA64) || defined(_M_AMD64)) && !defined(RC_INVOKED)
    InterlockedExchange64(mem, val);
#else
    InterlockedExchange64((long*)mem, val);
#endif
}

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_read64(volatile kuda_uint64_t *mem)
{
    return *mem;
}

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_cas64(volatile kuda_uint64_t *mem, kuda_uint64_t with,
                                           kuda_uint64_t cmp)
{
#if (defined(_M_IA64) || defined(_M_AMD64)) && !defined(RC_INVOKED)
    return InterlockedCompareExchange64(mem, with, cmp);
#else
    return InterlockedCompareExchange64((long*)mem, with, cmp);
#endif
}

KUDA_DECLARE(kuda_uint64_t) kuda_atomic_xchg64(volatile kuda_uint64_t *mem, kuda_uint64_t val)
{
#if (defined(_M_IA64) || defined(_M_AMD64)) && !defined(RC_INVOKED)
    return InterlockedExchange64(mem, val);
#else
    return InterlockedExchange64((long *)mem, val);
#endif
}
