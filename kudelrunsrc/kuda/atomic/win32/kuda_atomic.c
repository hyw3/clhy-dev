/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_atomic.h"

KUDA_DECLARE(kuda_status_t) kuda_atomic_init(kuda_pool_t *p)
{
#if defined (NEED_ATOMICS_GENERIC64)
    return kuda__atomic_generic64_init(p);
#else
    return KUDA_SUCCESS;
#endif
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_add32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
#if (defined(_M_IA64) || defined(_M_AMD64))
    return InterlockedExchangeAdd(mem, val);
#else
    return InterlockedExchangeAdd((long *)mem, val);
#endif
}

/* Of course we want the 2's compliment of the unsigned value, val */
#ifdef _MSC_VER
#pragma warning(disable: 4146)
#endif

KUDA_DECLARE(void) kuda_atomic_sub32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
#if (defined(_M_IA64) || defined(_M_AMD64))
    InterlockedExchangeAdd(mem, -val);
#else
    InterlockedExchangeAdd((long *)mem, -val);
#endif
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_inc32(volatile kuda_uint32_t *mem)
{
    /* we return old value, win32 returns new value :( */
#if (defined(_M_IA64) || defined(_M_AMD64)) && !defined(RC_INVOKED)
    return InterlockedIncrement(mem) - 1;
#else
    return InterlockedIncrement((long *)mem) - 1;
#endif
}

KUDA_DECLARE(int) kuda_atomic_dec32(volatile kuda_uint32_t *mem)
{
#if (defined(_M_IA64) || defined(_M_AMD64)) && !defined(RC_INVOKED)
    return InterlockedDecrement(mem);
#else
    return InterlockedDecrement((long *)mem);
#endif
}

KUDA_DECLARE(void) kuda_atomic_set32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
#if (defined(_M_IA64) || defined(_M_AMD64)) && !defined(RC_INVOKED)
    InterlockedExchange(mem, val);
#else
    InterlockedExchange((long*)mem, val);
#endif
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_read32(volatile kuda_uint32_t *mem)
{
    return *mem;
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_cas32(volatile kuda_uint32_t *mem, kuda_uint32_t with,
                                           kuda_uint32_t cmp)
{
#if (defined(_M_IA64) || defined(_M_AMD64)) && !defined(RC_INVOKED)
    return InterlockedCompareExchange(mem, with, cmp);
#else
    return InterlockedCompareExchange((long*)mem, with, cmp);
#endif
}

KUDA_DECLARE(void *) kuda_atomic_casptr(volatile void **mem, void *with, const void *cmp)
{
#if (defined(_M_IA64) || defined(_M_AMD64)) && !defined(RC_INVOKED)
    return InterlockedCompareExchangePointer((void* volatile*)mem, with, (void*)cmp);
#else
    return InterlockedCompareExchangePointer((void**)mem, with, (void*)cmp);
#endif
}

KUDA_DECLARE(kuda_uint32_t) kuda_atomic_xchg32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
#if (defined(_M_IA64) || defined(_M_AMD64)) && !defined(RC_INVOKED)
    return InterlockedExchange(mem, val);
#else
    return InterlockedExchange((long *)mem, val);
#endif
}

KUDA_DECLARE(void*) kuda_atomic_xchgptr(volatile void **mem, void *with)
{
    return InterlockedExchangePointer((void**)mem, with);
}
