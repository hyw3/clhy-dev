/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_atomic.h"

#include <stdlib.h>

kuda_status_t kuda_atomic_init(kuda_pool_t *p)
{
#if defined (NEED_ATOMICS_GENERIC64)
    return kuda__atomic_generic64_init(p);
#else
    return KUDA_SUCCESS;
#endif
}

kuda_uint32_t kuda_atomic_add32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    kuda_uint32_t old, new_val; 

    old = *mem;   /* old is automatically updated on cs failure */
    do {
        new_val = old + val;
    } while (__cs(&old, (cs_t *)mem, new_val));
    return old;
}

void kuda_atomic_sub32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
     kuda_uint32_t old, new_val;

     old = *mem;   /* old is automatically updated on cs failure */
     do {
         new_val = old - val;
     } while (__cs(&old, (cs_t *)mem, new_val));
}

kuda_uint32_t kuda_atomic_inc32(volatile kuda_uint32_t *mem)
{
    return kuda_atomic_add32(mem, 1);
}

int kuda_atomic_dec32(volatile kuda_uint32_t *mem)
{
    kuda_uint32_t old, new_val; 

    old = *mem;   /* old is automatically updated on cs failure */
    do {
        new_val = old - 1;
    } while (__cs(&old, (cs_t *)mem, new_val)); 

    return new_val != 0;
}

kuda_uint32_t kuda_atomic_read32(volatile kuda_uint32_t *mem)
{
    return *mem;
}

void kuda_atomic_set32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    *mem = val;
}

kuda_uint32_t kuda_atomic_cas32(volatile kuda_uint32_t *mem, kuda_uint32_t swap, 
                              kuda_uint32_t cmp)
{
    kuda_uint32_t old = cmp;
    
    __cs(&old, (cs_t *)mem, swap);
    return old; /* old is automatically updated from mem on cs failure */
}

#if KUDA_SIZEOF_VOIDP == 4
void *kuda_atomic_casptr(volatile void **mem_ptr,
                        void *swclhy_ptr,
                        const void *cmp_ptr)
{
     __cs1(&cmp_ptr,     /* automatically updated from mem on __cs1 failure  */
           mem_ptr,      /* set from swap when __cs1 succeeds                */
           &swclhy_ptr);
     return (void *)cmp_ptr;
}
#elif KUDA_SIZEOF_VOIDP == 8
void *kuda_atomic_casptr(volatile void **mem_ptr,
                        void *swclhy_ptr,
                        const void *cmp_ptr)
{
     __csg(&cmp_ptr,     /* automatically updated from mem on __csg failure  */
           mem_ptr,      /* set from swap when __csg succeeds                */
           &swclhy_ptr);  
     return (void *)cmp_ptr;
}
#else
#error KUDA_SIZEOF_VOIDP value not supported
#endif /* KUDA_SIZEOF_VOIDP */

kuda_uint32_t kuda_atomic_xchg32(volatile kuda_uint32_t *mem, kuda_uint32_t val)
{
    kuda_uint32_t old, new_val; 

    old = *mem;   /* old is automatically updated on cs failure */
    do {
        new_val = val;
    } while (__cs(&old, (cs_t *)mem, new_val)); 

    return old;
}

KUDA_DECLARE(void*) kuda_atomic_xchgptr(volatile void **mem_ptr, void *new_ptr)
{
    void *old_ptr;

    old_ptr = *(void **)mem_ptr; /* old is automatically updated on cs failure */
#if KUDA_SIZEOF_VOIDP == 4
    do {
    } while (__cs1(&old_ptr, mem_ptr, &new_ptr)); 
#elif KUDA_SIZEOF_VOIDP == 8
    do { 
    } while (__csg(&old_ptr, mem_ptr, &new_ptr)); 
#else
#error KUDA_SIZEOF_VOIDP value not supported
#endif /* KUDA_SIZEOF_VOIDP */

    return old_ptr;
}
