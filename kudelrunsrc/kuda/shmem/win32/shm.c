/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_general.h"
#include "kuda_errno.h"
#include "kuda_file_io.h"
#include "kuda_shm.h"
#include "kuda_strings.h"
#include "kuda_arch_file_io.h"
#include "limits.h"

typedef struct memblock_t {
    kuda_size_t size;
    kuda_size_t length;
} memblock_t;

struct kuda_shm_t {
    kuda_pool_t *pool;
    memblock_t *memblk;
    void       *usrmem;
    kuda_size_t  size;
    kuda_size_t  length;
    HANDLE      hMap;
    const char *filename;
};

static kuda_status_t shm_cleanup(void* shm)
{
    kuda_status_t rv = KUDA_SUCCESS;
    kuda_shm_t *m = shm;
    
    if (!UnmapViewOfFile(m->memblk)) {
        rv = kuda_get_platform_error();
    }
    if (!CloseHandle(m->hMap)) {
        rv = rv != KUDA_SUCCESS ? rv : kuda_get_platform_error();
    }
    if (m->filename) {
        /* Remove file if file backed */
        kuda_status_t rc = kuda_file_remove(m->filename, m->pool);
        rv = rv != KUDA_SUCCESS ? rv : rc;
    }
    return rv;
}

/* See if the caller is able to create a map in the global namespace by
 * checking if the SE_CREATE_GLOBAL_NAME privilege is enabled.
 *
 * Prior to KUDA 1.5.0, named shared memory segments were always created
 * in the global segment.  However, with recent versions of Windows this
 * fails for unprivileged processes.  Thus, with older KUDA, named shared
 * memory segments can't be created by unprivileged processes on newer
 * Windows.
 *
 * By checking if the caller has the privilege, shm APIs can decide
 * whether to use the Global or Local namespace.
 *
 * If running on an SDK without the required API definitions *OR*
 * some processing failure occurs trying to check the privilege, fall
 * back to earlier behavior -- always try to use the Global namespace.
 */
#ifdef SE_CREATE_GLOBAL_NAME
static int can_create_global_maps(void)
{
    BOOL ok, has_priv;
    LUID priv_id;
    PRIVILEGE_SET privs;
    HANDLE hToken;

    ok = OpenThreadToken(GetCurrentThread(), TOKEN_QUERY, TRUE, &hToken);
    if (!ok && GetLastError() == ERROR_NO_TOKEN) {
        /* no thread-specific access token, so try to get process access token
         */
        ok = OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken);
    }

    if (ok) {
        ok = LookupPrivilegeValue(NULL, SE_CREATE_GLOBAL_NAME, &priv_id);
    }

    if (ok) {
        privs.PrivilegeCount = 1;
        privs.Control = PRIVILEGE_SET_ALL_NECESSARY;
        privs.Privilege[0].Luid = priv_id;
        privs.Privilege[0].Attributes = SE_PRIVILEGE_ENABLED;
        ok = PrivilegeCheck(hToken, &privs, &has_priv);
    }

    if (ok && !has_priv) {
        return 0;
    }
    else {
        return 1;
    }
}
#else /* SE_CREATE_GLOBAL_NAME */
/* SDK definitions missing */
static int can_create_global_maps(void)
{
    return 1;
}
#endif /* SE_CREATE_GLOBAL_NAME */

KUDA_DECLARE(kuda_status_t) kuda_shm_create_ex(kuda_shm_t **m,
                                            kuda_size_t reqsize,
                                            const char *file,
                                            kuda_pool_t *pool,
                                            kuda_int32_t flags)
{
    static kuda_size_t memblock = 0;
    HANDLE hMap, hFile;
    kuda_status_t rv;
    kuda_size_t size;
    kuda_file_t *f;
    void *base;
    void *mapkey;
    DWORD err, sizelo, sizehi;

    reqsize += sizeof(memblock_t);

    if (!memblock)
    {
        SYSTEM_INFO si;
        GetSystemInfo(&si);
        memblock = si.dwAllocationGranularity;
    }   

    /* Compute the granualar multiple of the pagesize */
    size = memblock * (1 + (reqsize - 1) / memblock);
    sizelo = (DWORD)size;
#ifdef _WIN64
    sizehi = (DWORD)(size >> 32);
#else
    sizehi = 0;
#endif

    if (!file) {
        /* Do Anonymous, which must be passed as a duplicated handle */
#ifndef _WIN32_WCE
        hFile = INVALID_HANDLE_VALUE;
#endif
        mapkey = NULL;
    }
    else {
        int global;

        /* Do file backed, which is not an inherited handle 
         * While we could open KUDA_EXCL, it doesn't seem that Unix
         * ever did.  Ignore that error here, but fail later when
         * we discover we aren't the creator of the file map object.
         */
        rv = kuda_file_open(&f, file,
                           KUDA_READ | KUDA_WRITE | KUDA_BINARY | KUDA_CREATE,
                           KUDA_UREAD | KUDA_UWRITE, pool);
        if ((rv != KUDA_SUCCESS)
                || ((rv = kuda_platform_file_get(&hFile, f)) != KUDA_SUCCESS)) {
            return rv;
        }
        rv = kuda_file_trunc(f, size);

        /* res_name_from_filename turns file into a pseudo-name
         * without slashes or backslashes, and prepends the \global
         * or \local prefix on Win2K and later
         */
        if (flags & KUDA_SHM_NS_GLOBAL) {
            global = 1;
        }
        else if (flags & KUDA_SHM_NS_LOCAL) {
            global = 0;
        }
        else {
            global = can_create_global_maps();
        }
        mapkey = res_name_from_filename(file, global, pool);
    }

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        hMap = CreateFileMappingW(hFile, NULL, PAGE_READWRITE, 
                                  sizehi, sizelo, mapkey);
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        hMap = CreateFileMappingA(hFile, NULL, PAGE_READWRITE, 
                                  sizehi, sizelo, mapkey);
    }
#endif
    err = kuda_get_platform_error();

    if (file) {
        kuda_file_close(f);
    }

    if (hMap && KUDA_STATUS_IS_EEXIST(err)) {
        CloseHandle(hMap);
        return KUDA_EEXIST;
    }
    if (!hMap) {
        return err;
    }
    
    base = MapViewOfFile(hMap, FILE_MAP_READ | FILE_MAP_WRITE,
                         0, 0, size);
    if (!base) {
        CloseHandle(hMap);
        return kuda_get_platform_error();
    }
    
    *m = (kuda_shm_t *) kuda_palloc(pool, sizeof(kuda_shm_t));
    (*m)->pool = pool;
    (*m)->hMap = hMap;
    (*m)->memblk = base;
    (*m)->size = size;

    (*m)->usrmem = (char*)base + sizeof(memblock_t);
    (*m)->length = reqsize - sizeof(memblock_t);;
    
    (*m)->memblk->length = (*m)->length;
    (*m)->memblk->size = (*m)->size;
    (*m)->filename = file ? kuda_pstrdup(pool, file) : NULL;

    kuda_pool_cleanup_register((*m)->pool, *m, 
                              shm_cleanup, kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_shm_create(kuda_shm_t **m,
                                         kuda_size_t reqsize,
                                         const char *file,
                                         kuda_pool_t *pool)
{
    return kuda_shm_create_ex(m, reqsize, file, pool, 0);
}

KUDA_DECLARE(kuda_status_t) kuda_shm_destroy(kuda_shm_t *m) 
{
    kuda_status_t rv = shm_cleanup(m);
    kuda_pool_cleanup_kill(m->pool, m, shm_cleanup);
    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_shm_remove(const char *filename,
                                         kuda_pool_t *pool)
{
    return kuda_file_remove(filename, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_shm_delete(kuda_shm_t *m)
{
    if (m->filename) {
        return kuda_shm_remove(m->filename, m->pool);
    }
    else {
        return KUDA_ENOTIMPL;
    }
} 

static kuda_status_t shm_attach_internal(kuda_shm_t **m,
                                        const char *file,
                                        kuda_pool_t *pool,
                                        int global)
{
    HANDLE hMap;
    void *mapkey;
    void *base;

    /* res_name_from_filename turns file into a pseudo-name
     * without slashes or backslashes, and prepends the \global
     * or local prefix on Win2K and later
     */
    mapkey = res_name_from_filename(file, global, pool);

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
#ifndef _WIN32_WCE
        hMap = OpenFileMappingW(FILE_MAP_READ | FILE_MAP_WRITE, FALSE, mapkey);
#else
        /* The WCE 3.0 lacks OpenFileMapping. So we emulate one with
         * opening the existing shmem and reading its size from the header 
         */
        hMap = CreateFileMappingW(INVALID_HANDLE_VALUE, NULL, 
                                  PAGE_READWRITE, 0, sizeof(kuda_shm_t), mapkey);
#endif
    }
#endif
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        hMap = OpenFileMappingA(FILE_MAP_READ | FILE_MAP_WRITE, FALSE, mapkey);
    }
#endif

    if (!hMap) {
        return kuda_get_platform_error();
    }
    
    base = MapViewOfFile(hMap, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, 0);
    if (!base) {
        CloseHandle(hMap);
        return kuda_get_platform_error();
    }
    
    *m = (kuda_shm_t *) kuda_palloc(pool, sizeof(kuda_shm_t));
    (*m)->pool = pool;
    (*m)->memblk = base;
    /* Real (*m)->mem->size could be recovered with VirtualQuery */
    (*m)->size = (*m)->memblk->size;
#if _WIN32_WCE
    /* Reopen with real size  */
    UnmapViewOfFile(base);
    CloseHandle(hMap);

    hMap = CreateFileMappingW(INVALID_HANDLE_VALUE, NULL, 
                              PAGE_READWRITE, 0, (*m)->size, mapkey);
    if (!hMap) {
        return kuda_get_platform_error();
    }
    base = MapViewOfFile(hMap, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, 0);
    if (!base) {
        CloseHandle(hMap);
        return kuda_get_platform_error();
    }    
#endif
    (*m)->hMap = hMap;
    (*m)->length = (*m)->memblk->length;
    (*m)->usrmem = (char*)base + sizeof(memblock_t);
    (*m)->filename = NULL;

    kuda_pool_cleanup_register((*m)->pool, *m, 
                              shm_cleanup, kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_shm_attach_ex(kuda_shm_t **m,
                                            const char *file,
                                            kuda_pool_t *pool,
                                            kuda_int32_t flags)
{
    kuda_status_t rv;
    int can_create_global;
    int try_global_local[3] = {-1, -1, -1};
    int cur;

    if (!file) {
        return KUDA_EINVAL;
    }

    if (flags & KUDA_SHM_NS_LOCAL) {
        try_global_local[0] = 0; /* only search local */
    }
    else if (flags & KUDA_SHM_NS_GLOBAL) {
        try_global_local[0] = 1; /* only search global */
    }
    else {
        can_create_global = can_create_global_maps();
        if (!can_create_global) { /* unprivileged process */
            try_global_local[0] = 0; /* search local before global */
            try_global_local[1] = 1;
        }
        else {
            try_global_local[0] = 1; /* search global before local */
            try_global_local[1] = 0;
        }
    }

    for (cur = 0; try_global_local[cur] != -1; cur++) {
        rv = shm_attach_internal(m, file, pool, try_global_local[cur]);
        if (!KUDA_STATUS_IS_ENOENT(rv)) {
            break;
        }
    }

    return rv;
}

KUDA_DECLARE(kuda_status_t) kuda_shm_attach(kuda_shm_t **m,
                                         const char *file,
                                         kuda_pool_t *pool)
{
    return kuda_shm_attach_ex(m, file, pool, 0);
}

KUDA_DECLARE(kuda_status_t) kuda_shm_detach(kuda_shm_t *m)
{
    kuda_status_t rv = shm_cleanup(m);
    kuda_pool_cleanup_kill(m->pool, m, shm_cleanup);
    return rv;
}

KUDA_DECLARE(void *) kuda_shm_baseaddr_get(const kuda_shm_t *m)
{
    return m->usrmem;
}

KUDA_DECLARE(kuda_size_t) kuda_shm_size_get(const kuda_shm_t *m)
{
    return m->length;
}

KUDA_PERMS_SET_ENOTIMPL(shm)

KUDA_POOL_IMPLEMENT_ACCESSOR(shm)

KUDA_DECLARE(kuda_status_t) kuda_platform_shm_get(kuda_platform_shm_t *osshm,
                                         kuda_shm_t *shm)
{
    *osshm = shm->hMap;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_shm_put(kuda_shm_t **m,
                                         kuda_platform_shm_t *osshm,
                                         kuda_pool_t *pool)
{
    void* base;
    base = MapViewOfFile(*osshm, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, 0);
    if (!base) {
        return kuda_get_platform_error();
    }
    
    *m = (kuda_shm_t *) kuda_palloc(pool, sizeof(kuda_shm_t));
    (*m)->pool = pool;
    (*m)->hMap = *osshm;
    (*m)->memblk = base;
    (*m)->usrmem = (char*)base + sizeof(memblock_t);
    /* Real (*m)->mem->size could be recovered with VirtualQuery */
    (*m)->size = (*m)->memblk->size;
    (*m)->length = (*m)->memblk->length;
    (*m)->filename = NULL;

    kuda_pool_cleanup_register((*m)->pool, *m, 
                              shm_cleanup, kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}    

