/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_shm.h"
#include "kuda_arch_file_io.h"

#include "kuda_general.h"
#include "kuda_errno.h"
#include "kuda_user.h"
#include "kuda_strings.h"
#include "kuda_hash.h"

#if KUDA_USE_SHMEM_MMAP_SHM
/* 
 *   For portable use, a shared memory object should be identified by a name of
 *   the form /somename; that is, a null-terminated string of up to NAME_MAX
 *   (i.e., 255) characters consisting of an initial slash, followed by one or
 *   more characters, none of which are slashes.
 */
#ifndef NAME_MAX
#define NAME_MAX 255
#endif

/* See proc_mutex.c and sem_open for the reason for all this! */
static unsigned int rshash (const char *p) {
    /* hash function from Robert Sedgwicks 'Algorithms in C' book */
    unsigned int b    = 378551;
    unsigned int a    = 63689;
    unsigned int retval = 0;

    for( ; *p; p++) {
        retval = retval * a + (*p);
        a *= b;
    }

    return retval;
}

static const char *make_shm_open_safe_name(const char *filename,
                                           kuda_pool_t *pool)
{
    kuda_ssize_t flen;
    unsigned int h1, h2;

    if (filename == NULL) {
        return NULL;
    }

    flen = strlen(filename);
    h1 = (kuda_hashfunc_default(filename, &flen) & 0xffffffff);
    h2 = (rshash(filename) & 0xffffffff);
    return kuda_psprintf(pool, "/ShM.%xH%x", h1, h2);

}
#endif

#if KUDA_USE_SHMEM_SHMGET
static key_t our_ftok(const char *filename)
{
    /* to help avoid collisions while still using
     * an easily recreated proj_id */
    kuda_ssize_t slen = strlen(filename);
    return ftok(filename,
                (int)kuda_hashfunc_default(filename, &slen));
}
#endif

static kuda_status_t shm_cleanup_owner(void *m_)
{
    kuda_shm_t *m = (kuda_shm_t *)m_;

    /* anonymous shared memory */
    if (m->filename == NULL) {
#if KUDA_USE_SHMEM_MMAP_ZERO || KUDA_USE_SHMEM_MMAP_ANON
        if (munmap(m->base, m->realsize) == -1) {
            return errno;
        }
        return KUDA_SUCCESS;
#elif KUDA_USE_SHMEM_SHMGET_ANON
        if (shmdt(m->base) == -1) {
            return errno;
        }
        /* This segment will automatically remove itself after all
         * references have detached. */
        return KUDA_SUCCESS;
#endif
    }

    /* name-based shared memory */
    else {
#if KUDA_USE_SHMEM_MMAP_TMP
        if (munmap(m->base, m->realsize) == -1) {
            return errno;
        }
        if (access(m->filename, F_OK)) {
            return KUDA_SUCCESS;
        }
        else {
            return kuda_file_remove(m->filename, m->pool);
        }
#elif KUDA_USE_SHMEM_MMAP_SHM
        if (munmap(m->base, m->realsize) == -1) {
            return errno;
        }
        if (shm_unlink(make_shm_open_safe_name(m->filename, m->pool)) == -1 && errno != ENOENT) {
            return errno;
        }
        return KUDA_SUCCESS;
#elif KUDA_USE_SHMEM_SHMGET
        /* Indicate that the segment is to be destroyed as soon
         * as all processes have detached. This also disallows any
         * new attachments to the segment. */
        if (shmctl(m->shmid, IPC_RMID, NULL) == -1 && errno != EINVAL) {
            return errno;
        }
        if (shmdt(m->base) == -1) {
            return errno;
        }
        if (access(m->filename, F_OK)) {
            return KUDA_SUCCESS;
        }
        else {
            return kuda_file_remove(m->filename, m->pool);
        }
#else
        return KUDA_ENOTIMPL;
#endif
    }
}

KUDA_DECLARE(kuda_status_t) kuda_shm_create(kuda_shm_t **m,
                                         kuda_size_t reqsize, 
                                         const char *filename,
                                         kuda_pool_t *pool)
{
    kuda_shm_t *new_m;
    kuda_status_t status;
#if KUDA_USE_SHMEM_SHMGET || KUDA_USE_SHMEM_SHMGET_ANON
    struct shmid_ds shmbuf;
    kuda_uid_t uid;
    kuda_gid_t gid;
#endif
#if KUDA_USE_SHMEM_MMAP_TMP || KUDA_USE_SHMEM_MMAP_SHM || \
    KUDA_USE_SHMEM_MMAP_ZERO
    int tmpfd;
#endif
#if KUDA_USE_SHMEM_SHMGET
    kuda_size_t nbytes;
#endif
#if KUDA_USE_SHMEM_MMAP_ZERO || KUDA_USE_SHMEM_SHMGET || \
    KUDA_USE_SHMEM_MMAP_TMP || KUDA_USE_SHMEM_MMAP_SHM
    kuda_file_t *file;   /* file where metadata is stored */
#endif

    /* Check if they want anonymous or name-based shared memory */
    if (filename == NULL) {
#if KUDA_USE_SHMEM_MMAP_ZERO || KUDA_USE_SHMEM_MMAP_ANON
        new_m = kuda_palloc(pool, sizeof(kuda_shm_t));
        new_m->pool = pool;
        new_m->reqsize = reqsize;
        new_m->realsize = reqsize + 
            KUDA_ALIGN_DEFAULT(sizeof(kuda_size_t)); /* room for metadata */
        new_m->filename = NULL;
    
#if KUDA_USE_SHMEM_MMAP_ZERO
        status = kuda_file_open(&file, "/dev/zero", KUDA_READ | KUDA_WRITE, 
                               KUDA_PLATFORM_DEFAULT, pool);
        if (status != KUDA_SUCCESS) {
            return status;
        }
        status = kuda_platform_file_get(&tmpfd, file);
        if (status != KUDA_SUCCESS) {
            return status;
        }

        new_m->base = mmap(NULL, new_m->realsize, PROT_READ|PROT_WRITE,
                           MAP_SHARED, tmpfd, 0);
        if (new_m->base == (void *)MAP_FAILED) {
            return errno;
        }

        status = kuda_file_close(file);
        if (status != KUDA_SUCCESS) {
            return status;
        }

        /* store the real size in the metadata */
        *(kuda_size_t*)(new_m->base) = new_m->realsize;
        /* metadata isn't usable */
        new_m->usable = (char *)new_m->base + KUDA_ALIGN_DEFAULT(sizeof(kuda_size_t));

        kuda_pool_cleanup_register(new_m->pool, new_m, shm_cleanup_owner,
                                  kuda_pool_cleanup_null);
        *m = new_m;
        return KUDA_SUCCESS;

#elif KUDA_USE_SHMEM_MMAP_ANON
        new_m->base = mmap(NULL, new_m->realsize, PROT_READ|PROT_WRITE,
                           MAP_ANON|MAP_SHARED, -1, 0);
        if (new_m->base == (void *)MAP_FAILED) {
            return errno;
        }

        /* store the real size in the metadata */
        *(kuda_size_t*)(new_m->base) = new_m->realsize;
        /* metadata isn't usable */
        new_m->usable = (char *)new_m->base + KUDA_ALIGN_DEFAULT(sizeof(kuda_size_t));

        kuda_pool_cleanup_register(new_m->pool, new_m, shm_cleanup_owner,
                                  kuda_pool_cleanup_null);
        *m = new_m;
        return KUDA_SUCCESS;

#endif /* KUDA_USE_SHMEM_MMAP_ZERO */
#elif KUDA_USE_SHMEM_SHMGET_ANON
        new_m = kuda_palloc(pool, sizeof(kuda_shm_t));
        new_m->pool = pool;
        new_m->reqsize = reqsize;
        new_m->realsize = reqsize;
        new_m->filename = NULL;
        new_m->shmkey = IPC_PRIVATE;
        if ((new_m->shmid = shmget(new_m->shmkey, new_m->realsize,
                                   SHM_R | SHM_W | IPC_CREAT)) < 0) {
            return errno;
        }

        if ((new_m->base = shmat(new_m->shmid, NULL, 0)) == (void *)-1) {
            return errno;
        }
        new_m->usable = new_m->base;

        if (shmctl(new_m->shmid, IPC_STAT, &shmbuf) == -1) {
            return errno;
        }
        kuda_uid_current(&uid, &gid, pool);
        shmbuf.shm_perm.uid = uid;
        shmbuf.shm_perm.gid = gid;
        if (shmctl(new_m->shmid, IPC_SET, &shmbuf) == -1) {
            return errno;
        }

        /* Remove the segment once use count hits zero.
         * We will not attach to this segment again, since it is
         * anonymous memory, so it is ok to mark it for deletion.
         */
        if (shmctl(new_m->shmid, IPC_RMID, NULL) == -1) {
            return errno;
        }

        kuda_pool_cleanup_register(new_m->pool, new_m, shm_cleanup_owner,
                                  kuda_pool_cleanup_null);
        *m = new_m;
        return KUDA_SUCCESS;
#else
        /* It is an error if they want anonymous memory but we don't have it. */
        return KUDA_ENOTIMPL; /* requested anonymous but we don't have it */
#endif
    }

    /* Name-based shared memory */
    else {
        new_m = kuda_palloc(pool, sizeof(kuda_shm_t));
        new_m->pool = pool;
        new_m->reqsize = reqsize;
        new_m->filename = kuda_pstrdup(pool, filename);
#if KUDA_USE_SHMEM_MMAP_SHM
        const char *shm_name = make_shm_open_safe_name(filename, pool);
#endif
#if KUDA_USE_SHMEM_MMAP_TMP || KUDA_USE_SHMEM_MMAP_SHM
        new_m->realsize = reqsize + 
            KUDA_ALIGN_DEFAULT(sizeof(kuda_size_t)); /* room for metadata */
        /* FIXME: Ignore error for now. *
         * status = kuda_file_remove(file, pool);*/
        status = KUDA_SUCCESS;
    
#if KUDA_USE_SHMEM_MMAP_TMP
        /* FIXME: Is KUDA_PLATFORM_DEFAULT sufficient? */
        status = kuda_file_open(&file, filename, 
                               KUDA_READ | KUDA_WRITE | KUDA_CREATE | KUDA_EXCL,
                               KUDA_PLATFORM_DEFAULT, pool);
        if (status != KUDA_SUCCESS) {
            return status;
        }

        status = kuda_platform_file_get(&tmpfd, file);
        if (status != KUDA_SUCCESS) {
            kuda_file_close(file); /* ignore errors, we're failing */
            kuda_file_remove(new_m->filename, new_m->pool);
            return status;
        }

        status = kuda_file_trunc(file, new_m->realsize);
        if (status != KUDA_SUCCESS && status != KUDA_ESPIPE) {
            kuda_file_close(file); /* ignore errors, we're failing */
            kuda_file_remove(new_m->filename, new_m->pool);
            return status;
        }

        new_m->base = mmap(NULL, new_m->realsize, PROT_READ | PROT_WRITE,
                           MAP_SHARED, tmpfd, 0);
        /* FIXME: check for errors */

        status = kuda_file_close(file);
        if (status != KUDA_SUCCESS) {
            return status;
        }
#endif /* KUDA_USE_SHMEM_MMAP_TMP */
#if KUDA_USE_SHMEM_MMAP_SHM
        /* FIXME: SysV uses 0600... should we? */
        tmpfd = shm_open(shm_name, O_RDWR | O_CREAT | O_EXCL, 0644);
        if (tmpfd == -1) {
            return errno;
        }

        status = kuda_platform_file_put(&file, &tmpfd,
                                 KUDA_READ | KUDA_WRITE | KUDA_CREATE | KUDA_EXCL,
                                 pool); 
        if (status != KUDA_SUCCESS) {
            return status;
        }

        status = kuda_file_trunc(file, new_m->realsize);
        if (status != KUDA_SUCCESS && status != KUDA_ESPIPE) {
            shm_unlink(shm_name); /* we're failing, remove the object */
            return status;
        }
        new_m->base = mmap(NULL, new_m->realsize, PROT_READ | PROT_WRITE,
                           MAP_SHARED, tmpfd, 0);

        /* FIXME: check for errors */

        status = kuda_file_close(file);
        if (status != KUDA_SUCCESS) {
            return status;
        }
#endif /* KUDA_USE_SHMEM_MMAP_SHM */

        /* store the real size in the metadata */
        *(kuda_size_t*)(new_m->base) = new_m->realsize;
        /* metadata isn't usable */
        new_m->usable = (char *)new_m->base + KUDA_ALIGN_DEFAULT(sizeof(kuda_size_t));

        kuda_pool_cleanup_register(new_m->pool, new_m, shm_cleanup_owner,
                                  kuda_pool_cleanup_null);
        *m = new_m;
        return KUDA_SUCCESS;

#elif KUDA_USE_SHMEM_SHMGET
        new_m->realsize = reqsize;

        /* FIXME: KUDA_PLATFORM_DEFAULT is too permissive, switch to 600 I think. */
        status = kuda_file_open(&file, filename, 
                               KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE | KUDA_FOPEN_EXCL,
                               KUDA_PLATFORM_DEFAULT, pool);
        if (status != KUDA_SUCCESS) {
            return status;
        }

        /* ftok() (on solaris at least) requires that the file actually
         * exist before calling ftok(). */
        new_m->shmkey = our_ftok(filename);
        if (new_m->shmkey == (key_t)-1) {
            kuda_file_close(file);
            return errno;
        }

        if ((new_m->shmid = shmget(new_m->shmkey, new_m->realsize,
                                   SHM_R | SHM_W | IPC_CREAT | IPC_EXCL)) < 0) {
            kuda_file_close(file);
            return errno;
        }

        if ((new_m->base = shmat(new_m->shmid, NULL, 0)) == (void *)-1) {
            kuda_file_close(file);
            return errno;
        }
        new_m->usable = new_m->base;

        if (shmctl(new_m->shmid, IPC_STAT, &shmbuf) == -1) {
            kuda_file_close(file);
            return errno;
        }
        kuda_uid_current(&uid, &gid, pool);
        shmbuf.shm_perm.uid = uid;
        shmbuf.shm_perm.gid = gid;
        if (shmctl(new_m->shmid, IPC_SET, &shmbuf) == -1) {
            kuda_file_close(file);
            return errno;
        }

        nbytes = sizeof(reqsize);
        status = kuda_file_write(file, (const void *)&reqsize,
                                &nbytes);
        if (status != KUDA_SUCCESS) {
            kuda_file_close(file);
            return status;
        }
        status = kuda_file_close(file);
        if (status != KUDA_SUCCESS) {
            return status;
        }

        kuda_pool_cleanup_register(new_m->pool, new_m, shm_cleanup_owner,
                                  kuda_pool_cleanup_null);
        *m = new_m; 
        return KUDA_SUCCESS;

#else
        return KUDA_ENOTIMPL;
#endif
    }
}

KUDA_DECLARE(kuda_status_t) kuda_shm_create_ex(kuda_shm_t **m, 
                                            kuda_size_t reqsize, 
                                            const char *filename, 
                                            kuda_pool_t *p,
                                            kuda_int32_t flags)
{
    return kuda_shm_create(m, reqsize, filename, p);
}

KUDA_DECLARE(kuda_status_t) kuda_shm_remove(const char *filename,
                                         kuda_pool_t *pool)
{
#if KUDA_USE_SHMEM_SHMGET
    kuda_status_t status;
    kuda_file_t *file;  
    key_t shmkey;
    int shmid;
#endif

#if KUDA_USE_SHMEM_MMAP_TMP
    return kuda_file_remove(filename, pool);
#elif KUDA_USE_SHMEM_MMAP_SHM
    const char *shm_name = make_shm_open_safe_name(filename, pool);
    if (shm_unlink(shm_name) == -1) {
        return errno;
    }
    return KUDA_SUCCESS;
#elif KUDA_USE_SHMEM_SHMGET
    /* Presume that the file already exists; just open for writing */    
    status = kuda_file_open(&file, filename, KUDA_FOPEN_WRITE,
                           KUDA_PLATFORM_DEFAULT, pool);
    if (status) {
        return status;
    }

    /* ftok() (on solaris at least) requires that the file actually
     * exist before calling ftok(). */
    shmkey = our_ftok(filename);
    if (shmkey == (key_t)-1) {
        goto shm_remove_failed;
    }

    kuda_file_close(file);

    if ((shmid = shmget(shmkey, 0, SHM_R | SHM_W)) < 0) {
        goto shm_remove_failed;
    }

    /* Indicate that the segment is to be destroyed as soon
     * as all processes have detached. This also disallows any
     * new attachments to the segment. */
    if (shmctl(shmid, IPC_RMID, NULL) == -1) {
        goto shm_remove_failed;
    }
    return kuda_file_remove(filename, pool);

shm_remove_failed:
    status = errno;
    /* ensure the file has been removed anyway. */
    kuda_file_remove(filename, pool);
    return status;
#else

    /* No support for anonymous shm */
    return KUDA_ENOTIMPL;
#endif
} 

KUDA_DECLARE(kuda_status_t) kuda_shm_delete(kuda_shm_t *m)
{
    if (m->filename) {
        return kuda_shm_remove(m->filename, m->pool);
    }
    else {
        return KUDA_ENOTIMPL;
    }
} 

KUDA_DECLARE(kuda_status_t) kuda_shm_destroy(kuda_shm_t *m)
{
    return kuda_pool_cleanup_run(m->pool, m, shm_cleanup_owner);
}

static kuda_status_t shm_cleanup_attach(void *m_)
{
    kuda_shm_t *m = (kuda_shm_t *)m_;

    if (m->filename == NULL) {
        /* It doesn't make sense to detach from an anonymous memory segment. */
        return KUDA_EINVAL;
    }
    else {
#if KUDA_USE_SHMEM_MMAP_TMP || KUDA_USE_SHMEM_MMAP_SHM
        if (munmap(m->base, m->realsize) == -1) {
            return errno;
        }
        return KUDA_SUCCESS;
#elif KUDA_USE_SHMEM_SHMGET
        if (shmdt(m->base) == -1) {
            return errno;
        }
        return KUDA_SUCCESS;
#else
        return KUDA_ENOTIMPL;
#endif
    }
}

KUDA_DECLARE(kuda_status_t) kuda_shm_attach(kuda_shm_t **m,
                                         const char *filename,
                                         kuda_pool_t *pool)
{
    if (filename == NULL) {
        /* It doesn't make sense to attach to a segment if you don't know
         * the filename. */
        return KUDA_EINVAL;
    }
    else {
#if KUDA_USE_SHMEM_MMAP_TMP || KUDA_USE_SHMEM_MMAP_SHM
        kuda_shm_t *new_m;
        kuda_status_t status;
        int tmpfd;
        kuda_file_t *file;   /* file where metadata is stored */
        kuda_size_t nbytes;

        new_m = kuda_palloc(pool, sizeof(kuda_shm_t));
        new_m->pool = pool;
        new_m->filename = kuda_pstrdup(pool, filename);
#if KUDA_USE_SHMEM_MMAP_SHM
        const char *shm_name = make_shm_open_safe_name(filename, pool);

        /* FIXME: SysV uses 0600... should we? */
        tmpfd = shm_open(shm_name, O_RDWR, 0644);
        if (tmpfd == -1) {
            return errno;
        }

        status = kuda_platform_file_put(&file, &tmpfd,
                                 KUDA_READ | KUDA_WRITE,
                                 pool); 
        if (status != KUDA_SUCCESS) {
            return status;
        }

#elif KUDA_USE_SHMEM_MMAP_TMP
        status = kuda_file_open(&file, filename, 
                               KUDA_READ | KUDA_WRITE,
                               KUDA_PLATFORM_DEFAULT, pool);
        if (status != KUDA_SUCCESS) {
            return status;
        }
        status = kuda_platform_file_get(&tmpfd, file);
        if (status != KUDA_SUCCESS) {
            return status;
        }
#else
        return KUDA_ENOTIMPL;
#endif

        nbytes = sizeof(new_m->realsize);
        status = kuda_file_read(file, (void *)&(new_m->realsize),
                               &nbytes);
        if (status != KUDA_SUCCESS) {
            return status;
        }

        status = kuda_platform_file_get(&tmpfd, file);
        if (status != KUDA_SUCCESS) {
            kuda_file_close(file); /* ignore errors, we're failing */
            kuda_file_remove(new_m->filename, new_m->pool);
            return status;
        }

        new_m->reqsize = new_m->realsize - sizeof(kuda_size_t);

        new_m->base = mmap(NULL, new_m->realsize, PROT_READ | PROT_WRITE,
                           MAP_SHARED, tmpfd, 0);
        /* FIXME: check for errors */
        
        status = kuda_file_close(file);
        if (status != KUDA_SUCCESS) {
            return status;
        }

        /* metadata isn't part of the usable segment */
        new_m->usable = (char *)new_m->base + KUDA_ALIGN_DEFAULT(sizeof(kuda_size_t));

        kuda_pool_cleanup_register(new_m->pool, new_m, shm_cleanup_attach,
                                  kuda_pool_cleanup_null);
        *m = new_m;
        return KUDA_SUCCESS;

#elif KUDA_USE_SHMEM_SHMGET
        kuda_shm_t *new_m;
        kuda_status_t status;
        kuda_file_t *file;   /* file where metadata is stored */
        kuda_size_t nbytes;

        new_m = kuda_palloc(pool, sizeof(kuda_shm_t));

        status = kuda_file_open(&file, filename, 
                               KUDA_FOPEN_READ, KUDA_PLATFORM_DEFAULT, pool);
        if (status != KUDA_SUCCESS) {
            return status;
        }

        nbytes = sizeof(new_m->reqsize);
        status = kuda_file_read(file, (void *)&(new_m->reqsize),
                               &nbytes);
        if (status != KUDA_SUCCESS) {
            return status;
        }
        status = kuda_file_close(file);
        if (status != KUDA_SUCCESS) {
            return status;
        }

        new_m->filename = kuda_pstrdup(pool, filename);
        new_m->pool = pool;
        new_m->shmkey = our_ftok(filename);
        if (new_m->shmkey == (key_t)-1) {
            return errno;
        }
        if ((new_m->shmid = shmget(new_m->shmkey, 0, SHM_R | SHM_W)) == -1) {
            return errno;
        }
        if ((new_m->base = shmat(new_m->shmid, NULL, 0)) == (void *)-1) {
            return errno;
        }
        new_m->usable = new_m->base;
        new_m->realsize = new_m->reqsize;

        kuda_pool_cleanup_register(new_m->pool, new_m, shm_cleanup_attach,
                                  kuda_pool_cleanup_null);
        *m = new_m;
        return KUDA_SUCCESS;

#else
        return KUDA_ENOTIMPL;
#endif
    }
}

KUDA_DECLARE(kuda_status_t) kuda_shm_attach_ex(kuda_shm_t **m,
                                            const char *filename,
                                            kuda_pool_t *pool,
                                            kuda_int32_t flags)
{
    return kuda_shm_attach(m, filename, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_shm_detach(kuda_shm_t *m)
{
    kuda_status_t rv = shm_cleanup_attach(m);
    kuda_pool_cleanup_kill(m->pool, m, shm_cleanup_attach);
    return rv;
}

KUDA_DECLARE(void *) kuda_shm_baseaddr_get(const kuda_shm_t *m)
{
    return m->usable;
}

KUDA_DECLARE(kuda_size_t) kuda_shm_size_get(const kuda_shm_t *m)
{
    return m->reqsize;
}

KUDA_PERMS_SET_IMPLEMENT(shm)
{
#if KUDA_USE_SHMEM_SHMGET || KUDA_USE_SHMEM_SHMGET_ANON
    struct shmid_ds shmbuf;
    int shmid;
    kuda_shm_t *m = (kuda_shm_t *)theshm;

    if ((shmid = shmget(m->shmkey, 0, SHM_R | SHM_W)) == -1) {
        return errno;
    }
    shmbuf.shm_perm.uid  = uid;
    shmbuf.shm_perm.gid  = gid;
    shmbuf.shm_perm.mode = kuda_unix_perms2mode(perms);
    if (shmctl(shmid, IPC_SET, &shmbuf) == -1) {
        return errno;
    }
    return KUDA_SUCCESS;
#else
    return KUDA_ENOTIMPL;
#endif
}

KUDA_POOL_IMPLEMENT_ACCESSOR(shm)

KUDA_DECLARE(kuda_status_t) kuda_platform_shm_get(kuda_platform_shm_t *osshm,
                                         kuda_shm_t *shm)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_shm_put(kuda_shm_t **m,
                                         kuda_platform_shm_t *osshm,
                                         kuda_pool_t *pool)
{
    return KUDA_ENOTIMPL;
}    

