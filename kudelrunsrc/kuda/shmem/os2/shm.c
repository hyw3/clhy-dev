/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_general.h"
#include "kuda_shm.h"
#include "kuda_errno.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_portable.h"

struct kuda_shm_t {
    kuda_pool_t *pool;
    void *memblock;
};

KUDA_DECLARE(kuda_status_t) kuda_shm_create(kuda_shm_t **m,
                                         kuda_size_t reqsize,
                                         const char *filename,
                                         kuda_pool_t *pool)
{
    int rc;
    kuda_shm_t *newm = (kuda_shm_t *)kuda_palloc(pool, sizeof(kuda_shm_t));
    char *name = NULL;
    ULONG flags = PAG_COMMIT|PAG_READ|PAG_WRITE;

    newm->pool = pool;

    if (filename) {
        name = kuda_pstrcat(pool, "\\SHAREMEM\\", filename, NULL);
    }

    if (name == NULL) {
        flags |= OBJ_GETTABLE;
    }

    rc = DosAllocSharedMem(&(newm->memblock), name, reqsize, flags);

    if (rc) {
        return KUDA_OS2_STATUS(rc);
    }

    *m = newm;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_shm_create_ex(kuda_shm_t **m, 
                                            kuda_size_t reqsize, 
                                            const char *filename, 
                                            kuda_pool_t *p,
                                            kuda_int32_t flags)
{
    return kuda_shm_create(m, reqsize, filename, p);
}

KUDA_DECLARE(kuda_status_t) kuda_shm_destroy(kuda_shm_t *m)
{
    DosFreeMem(m->memblock);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_shm_remove(const char *filename,
                                         kuda_pool_t *pool)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_shm_delete(kuda_shm_t *m)
{
    return KUDA_ENOTIMPL;
} 

KUDA_DECLARE(kuda_status_t) kuda_shm_attach(kuda_shm_t **m,
                                         const char *filename,
                                         kuda_pool_t *pool)
{
    int rc;
    kuda_shm_t *newm = (kuda_shm_t *)kuda_palloc(pool, sizeof(kuda_shm_t));
    char *name = NULL;
    ULONG flags = PAG_READ|PAG_WRITE;

    newm->pool = pool;
    name = kuda_pstrcat(pool, "\\SHAREMEM\\", filename, NULL);

    rc = DosGetNamedSharedMem(&(newm->memblock), name, flags);

    if (rc) {
        return KUDA_FROM_PLATFORM_ERROR(rc);
    }

    *m = newm;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_shm_attach_ex(kuda_shm_t **m,
                                            const char *filename,
                                            kuda_pool_t *pool,
                                            kuda_int32_t flags)
{
    return kuda_shm_attach(m, filename, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_shm_detach(kuda_shm_t *m)
{
    int rc = 0;

    if (m->memblock) {
        rc = DosFreeMem(m->memblock);
    }

    return KUDA_FROM_PLATFORM_ERROR(rc);
}

KUDA_DECLARE(void *) kuda_shm_baseaddr_get(const kuda_shm_t *m)
{
    return m->memblock;
}

KUDA_DECLARE(kuda_size_t) kuda_shm_size_get(const kuda_shm_t *m)
{
    ULONG flags, size = 0x1000000;
    DosQueryMem(m->memblock, &size, &flags);
    return size;
}

KUDA_PERMS_SET_ENOTIMPL(shm)

KUDA_POOL_IMPLEMENT_ACCESSOR(shm)

KUDA_DECLARE(kuda_status_t) kuda_platform_shm_get(kuda_platform_shm_t *osshm,
                                         kuda_shm_t *shm)
{
    *osshm = shm->memblock;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_shm_put(kuda_shm_t **m,
                                         kuda_platform_shm_t *osshm,
                                         kuda_pool_t *pool)
{
    int rc;
    kuda_shm_t *newm = (kuda_shm_t *)kuda_palloc(pool, sizeof(kuda_shm_t));
    ULONG flags = PAG_COMMIT|PAG_READ|PAG_WRITE;

    newm->pool = pool;

    rc = DosGetSharedMem(&(newm->memblock), flags);

    if (rc) {
        return KUDA_FROM_PLATFORM_ERROR(rc);
    }

    *m = newm;
    return KUDA_SUCCESS;
}    

