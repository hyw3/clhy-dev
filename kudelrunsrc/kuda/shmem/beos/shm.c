/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_general.h"
#include "kuda_shm.h"
#include "kuda_errno.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include <stdio.h>
#include <stdlib.h>
#include <kernel/OS.h>
#include "kuda_portable.h"

struct kuda_shm_t {
    kuda_pool_t *pool;
    void *memblock;
    void *ptr;
    kuda_size_t reqsize;
    kuda_size_t avail;
    area_id aid;
};

KUDA_DECLARE(kuda_status_t) kuda_shm_create(kuda_shm_t **m, 
                                         kuda_size_t reqsize, 
                                         const char *filename, 
                                         kuda_pool_t *p)
{
    kuda_size_t pagesize;
    area_id newid;
    char *addr;
    char shname[B_PLATFORM_NAME_LENGTH];
    
    (*m) = (kuda_shm_t *)kuda_pcalloc(p, sizeof(kuda_shm_t));
    /* we MUST allocate in pages, so calculate how big an area we need... */
    pagesize = ((reqsize + B_PAGE_SIZE - 1) / B_PAGE_SIZE) * B_PAGE_SIZE;
     
    if (!filename) {
        int num = 0;
        snprintf(shname, B_PLATFORM_NAME_LENGTH, "kuda_shmem_%ld", find_thread(NULL));
        while (find_area(shname) >= 0)
            snprintf(shname, B_PLATFORM_NAME_LENGTH, "kuda_shmem_%ld_%d",
                     find_thread(NULL), num++);
    }
    newid = create_area(filename ? filename : shname, 
                        (void*)&addr, B_ANY_ADDRESS,
                        pagesize, B_LAZY_LOCK, B_READ_AREA|B_WRITE_AREA);

    if (newid < 0)
        return errno;

    (*m)->pool = p;
    (*m)->aid = newid;
    (*m)->memblock = addr;
    (*m)->ptr = (void*)addr;
    (*m)->avail = pagesize; /* record how big an area we actually created... */
    (*m)->reqsize = reqsize;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_shm_create_ex(kuda_shm_t **m, 
                                            kuda_size_t reqsize, 
                                            const char *filename, 
                                            kuda_pool_t *p,
                                            kuda_int32_t flags)
{
    return kuda_shm_create(m, reqsize, filename, p);
}

KUDA_DECLARE(kuda_status_t) kuda_shm_destroy(kuda_shm_t *m)
{
    delete_area(m->aid);
    m->avail = 0;
    m->memblock = NULL;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_shm_remove(const char *filename,
                                         kuda_pool_t *pool)
{
    area_id deleteme = find_area(filename);
    
    if (deleteme == B_NAME_NOT_FOUND)
        return KUDA_EINVAL;

    delete_area(deleteme);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_shm_delete(kuda_shm_t *m)
{
    if (m->filename) {
        return kuda_shm_remove(m->filename, m->pool);
    }
    else {
        return KUDA_ENOTIMPL;
    }
} 

KUDA_DECLARE(kuda_status_t) kuda_shm_attach(kuda_shm_t **m,
                                         const char *filename,
                                         kuda_pool_t *pool)
{
    area_info ai;
    thread_info ti;
    kuda_shm_t *new_m;
    area_id deleteme = find_area(filename);

    if (deleteme == B_NAME_NOT_FOUND)
        return KUDA_EINVAL;

    new_m = (kuda_shm_t*)kuda_palloc(pool, sizeof(kuda_shm_t*));
    if (new_m == NULL)
        return KUDA_ENOMEM;
    new_m->pool = pool;

    get_area_info(deleteme, &ai);
    get_thread_info(find_thread(NULL), &ti);

    if (ti.team != ai.team) {
        area_id narea;
        
        narea = clone_area(ai.name, &(ai.address), B_CLONE_ADDRESS,
                           B_READ_AREA|B_WRITE_AREA, ai.area);

        if (narea < B_OK)
            return narea;
            
        get_area_info(narea, &ai);
        new_m->aid = narea;
        new_m->memblock = ai.address;
        new_m->ptr = (void*)ai.address;
        new_m->avail = ai.size;
        new_m->reqsize = ai.size;
    }

    (*m) = new_m;
    
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_shm_attach_ex(kuda_shm_t **m,
                                            const char *filename,
                                            kuda_pool_t *pool,
                                            kuda_int32_t flags)
{
    return kuda_shm_attach(m, filename, pool);
}

KUDA_DECLARE(kuda_status_t) kuda_shm_detach(kuda_shm_t *m)
{
    delete_area(m->aid);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(void *) kuda_shm_baseaddr_get(const kuda_shm_t *m)
{
    return m->memblock;
}

KUDA_DECLARE(kuda_size_t) kuda_shm_size_get(const kuda_shm_t *m)
{
    return m->reqsize;
}

KUDA_PERMS_SET_ENOTIMPL(shm)

KUDA_POOL_IMPLEMENT_ACCESSOR(shm)

KUDA_DECLARE(kuda_status_t) kuda_platform_shm_get(kuda_platform_shm_t *osshm,
                                         kuda_shm_t *shm)
{
    return KUDA_ENOTIMPL;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_shm_put(kuda_shm_t **m,
                                         kuda_platform_shm_t *osshm,
                                         kuda_pool_t *pool)
{
    return KUDA_ENOTIMPL;
}    

