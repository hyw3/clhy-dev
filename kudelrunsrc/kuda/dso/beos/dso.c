/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_dso.h"
#include "kuda_portable.h"

#if KUDA_HAS_DSO

static kuda_status_t dso_cleanup(void *thedso)
{
    kuda_dso_handle_t *dso = thedso;

    if (dso->handle > 0 && unload_add_on(dso->handle) < B_NO_ERROR)
        return KUDA_EINIT;
    dso->handle = -1;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_dso_load(kuda_dso_handle_t **res_handle, 
                                       const char *path, kuda_pool_t *pool)
{
    image_id newid = -1;

    *res_handle = kuda_pcalloc(pool, sizeof(*res_handle));

    if((newid = load_add_on(path)) < B_NO_ERROR) {
        (*res_handle)->errormsg = strerror(newid);
        return KUDA_EDSOOPEN;
	}
	
    (*res_handle)->pool = pool;
    (*res_handle)->handle = newid;

    kuda_pool_cleanup_register(pool, *res_handle, dso_cleanup, kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_dso_unload(kuda_dso_handle_t *handle)
{
    return kuda_pool_cleanup_run(handle->pool, handle, dso_cleanup);
}

KUDA_DECLARE(kuda_status_t) kuda_dso_sym(kuda_dso_handle_sym_t *ressym, kuda_dso_handle_t *handle,
               const char *symname)
{
    int err;

    if (symname == NULL)
        return KUDA_ESYMNOTFOUND;

    err = get_image_symbol(handle->handle, symname, B_SYMBOL_TYPE_ANY, 
			 ressym);

    if(err != B_OK)
        return KUDA_ESYMNOTFOUND;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(const char *) kuda_dso_error(kuda_dso_handle_t *dso, char *buffer, kuda_size_t buflen)
{
    strncpy(buffer, strerror(errno), buflen);
    return buffer;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_put(kuda_dso_handle_t **kudadso,
                                                kuda_platform_dso_handle_t osdso,
                                                kuda_pool_t *pool)
{
    *kudadso = kuda_pcalloc(pool, sizeof **kudadso);
    (*kudadso)->handle = osdso;
    (*kudadso)->pool = pool;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_get(kuda_platform_dso_handle_t *osdso,
                                                kuda_dso_handle_t *kudadso)
{
    *osdso = kudadso->handle;
    return KUDA_SUCCESS;
}

#endif
