/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_dso.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include <stdio.h>
#include <string.h>

#if KUDA_HAS_DSO

static kuda_status_t dso_cleanup(void *thedso)
{
    kuda_dso_handle_t *dso = thedso;
    int rc;

    if (dso->handle == 0)
        return KUDA_SUCCESS;
       
    rc = DosFreecAPI(dso->handle);

    if (rc == 0)
        dso->handle = 0;

    return KUDA_FROM_PLATFORM_ERROR(rc);
}


KUDA_DECLARE(kuda_status_t) kuda_dso_load(kuda_dso_handle_t **res_handle, const char *path, kuda_pool_t *ctx)
{
    char failed_capi[200];
    HCAPI handle;
    int rc;

    *res_handle = kuda_pcalloc(ctx, sizeof(**res_handle));
    (*res_handle)->cont = ctx;
    (*res_handle)->load_error = KUDA_SUCCESS;
    (*res_handle)->failed_capi = NULL;

    if ((rc = DosActivatecAPI(failed_capi, sizeof(failed_capi), path, &handle)) != 0) {
        (*res_handle)->load_error = KUDA_FROM_PLATFORM_ERROR(rc);
        (*res_handle)->failed_capi = kuda_pstrdup(ctx, failed_capi);
        return KUDA_FROM_PLATFORM_ERROR(rc);
    }

    (*res_handle)->handle  = handle;
    kuda_pool_cleanup_register(ctx, *res_handle, dso_cleanup, kuda_pool_cleanup_null);
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_dso_unload(kuda_dso_handle_t *handle)
{
    return kuda_pool_cleanup_run(handle->cont, handle, dso_cleanup);
}



KUDA_DECLARE(kuda_status_t) kuda_dso_sym(kuda_dso_handle_sym_t *ressym, 
                                      kuda_dso_handle_t *handle, 
                                      const char *symname)
{
    PFN func;
    int rc;

    if (symname == NULL || ressym == NULL)
        return KUDA_ESYMNOTFOUND;

    if ((rc = DosQueryProcAddr(handle->handle, 0, symname, &func)) != 0) {
        handle->load_error = KUDA_FROM_PLATFORM_ERROR(rc);
        return handle->load_error;
    }

    *ressym = func;
    return KUDA_SUCCESS;
}



KUDA_DECLARE(const char *) kuda_dso_error(kuda_dso_handle_t *dso, char *buffer, kuda_size_t buflen)
{
    char message[200];
    kuda_strerror(dso->load_error, message, sizeof(message));

    if (dso->failed_capi != NULL) {
        strcat(message, " (");
        strcat(message, dso->failed_capi);
        strcat(message, ")");
    }

    kuda_cpystrn(buffer, message, buflen);
    return buffer;
}



KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_put(kuda_dso_handle_t **kudadso,
                                                kuda_platform_dso_handle_t osdso,
                                                kuda_pool_t *pool)
{
    *kudadso = kuda_pcalloc(pool, sizeof **kudadso);
    (*kudadso)->handle = osdso;
    (*kudadso)->cont = pool;
    (*kudadso)->load_error = KUDA_SUCCESS;
    (*kudadso)->failed_capi = NULL;
    return KUDA_SUCCESS;
}



KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_get(kuda_platform_dso_handle_t *osdso,
                                                kuda_dso_handle_t *kudadso)
{
    *osdso = kudadso->handle;
    return KUDA_SUCCESS;
}

#endif
