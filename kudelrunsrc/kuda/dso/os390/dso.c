/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_portable.h"
#include "kuda_strings.h"
#include "kuda_arch_dso.h"
#include <errno.h>
#include <string.h>

#if KUDA_HAS_DSO

KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_put(kuda_dso_handle_t **kudadso,
                                                kuda_platform_dso_handle_t osdso,
                                                kuda_pool_t *pool)
{   
    *kudadso = kuda_pcalloc(pool, sizeof **kudadso);
    (*kudadso)->handle = osdso;
    (*kudadso)->pool = pool;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_get(kuda_platform_dso_handle_t *osdso,
                                                kuda_dso_handle_t *kudadso)
{
    *osdso = kudadso->handle;
    return KUDA_SUCCESS;
}

static kuda_status_t dso_cleanup(void *thedso)
{
    kuda_dso_handle_t *dso = thedso;
    int rc;

    if (dso->handle == 0)
        return KUDA_SUCCESS;
       
    rc = dllfree(dso->handle);

    if (rc == 0) {
        dso->handle = 0;
        return KUDA_SUCCESS;
    }
    dso->failing_errno = errno;
    return errno;
}

KUDA_DECLARE(kuda_status_t) kuda_dso_load(kuda_dso_handle_t **res_handle, 
                                       const char *path, kuda_pool_t *ctx)
{
    dllhandle *handle;
    int rc;

    *res_handle = kuda_pcalloc(ctx, sizeof(**res_handle));
    (*res_handle)->pool = ctx;
    if ((handle = dllload(path)) != NULL) {
        (*res_handle)->handle  = handle;
        kuda_pool_cleanup_register(ctx, *res_handle, dso_cleanup, kuda_pool_cleanup_null);
        return KUDA_SUCCESS;
    }

    (*res_handle)->failing_errno = errno;
    return KUDA_EDSOOPEN;
}

KUDA_DECLARE(kuda_status_t) kuda_dso_unload(kuda_dso_handle_t *handle)
{
    return kuda_pool_cleanup_run(handle->pool, handle, dso_cleanup);
}

KUDA_DECLARE(kuda_status_t) kuda_dso_sym(kuda_dso_handle_sym_t *ressym, 
                                      kuda_dso_handle_t *handle, 
                                      const char *symname)
{
    void *func_ptr;
    void *var_ptr; 

    if ((var_ptr = dllqueryvar(handle->handle, symname)) != NULL) {
        *ressym = var_ptr;
        return KUDA_SUCCESS;
    }
    if ((func_ptr = (void *)dllqueryfn(handle->handle, symname)) != NULL) {
        *ressym = func_ptr;
        return KUDA_SUCCESS;
    }
    handle->failing_errno = errno;
    return KUDA_ESYMNOTFOUND;
}

KUDA_DECLARE(const char *) kuda_dso_error(kuda_dso_handle_t *handle, char *buffer, 
                          kuda_size_t buflen)
{
    kuda_cpystrn(buffer, strerror(handle->failing_errno), buflen);
    return buffer;
}

#endif
