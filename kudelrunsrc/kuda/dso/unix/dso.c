/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_dso.h"
#include "kuda_strings.h"
#include "kuda_portable.h"

#if KUDA_HAS_DSO

#if !defined(DSO_USE_DLFCN) && !defined(DSO_USE_SHL) && !defined(DSO_USE_DYLD)
#error No DSO implementation specified.
#endif

#ifdef HAVE_STDDEF_H
#include <stddef.h>
#endif
#if KUDA_HAVE_STDLIB_H
#include <stdlib.h> /* malloc(), free() */
#endif
#if KUDA_HAVE_STRING_H
#include <string.h> /* for strerror() on HP-UX */
#endif

#if defined(DSO_USE_DYLD)
#define DYLD_LIBRARY_HANDLE (void *)-1
#endif

KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_put(kuda_dso_handle_t **kudadso,
                                                kuda_platform_dso_handle_t osdso,
                                                kuda_pool_t *pool)
{
    *kudadso = kuda_pcalloc(pool, sizeof **kudadso);
    (*kudadso)->handle = osdso;
    (*kudadso)->pool = pool;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_get(kuda_platform_dso_handle_t *osdso,
                                                kuda_dso_handle_t *kudadso)
{
    *osdso = kudadso->handle;
    return KUDA_SUCCESS;
}

static kuda_status_t dso_cleanup(void *thedso)
{
    kuda_dso_handle_t *dso = thedso;

    if (dso->handle == NULL)
        return KUDA_SUCCESS;

#if defined(DSO_USE_SHL)
    shl_unload((shl_t)dso->handle);
#elif defined(DSO_USE_DYLD)
    if (dso->handle != DYLD_LIBRARY_HANDLE) {
        NSUnLinkcAPI(dso->handle, FALSE);
    }
#elif defined(DSO_USE_DLFCN)
    if (dlclose(dso->handle) != 0)
        return KUDA_EINIT;
#endif
    dso->handle = NULL;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_dso_load(kuda_dso_handle_t **res_handle, 
                                       const char *path, kuda_pool_t *pool)
{
#if defined(DSO_USE_SHL)
    shl_t platform_handle = shl_load(path, BIND_IMMEDIATE, 0L);

#elif defined(DSO_USE_DYLD)
    NSObjectFileImage image;
    NScAPI platform_handle = NULL;
    NSObjectFileImageReturnCode dsoerr;
    const char* err_msg = NULL;
    dsoerr = NSCreateObjectFileImageFromFile(path, &image);

    if (dsoerr == NSObjectFileImageSuccess) {
#if defined(NSLINKCAPI_OPTION_RETURN_ON_ERROR) && defined(NSLINKCAPI_OPTION_NONE)
        platform_handle = NSLinkcAPI(image, path,
                                 NSLINKCAPI_OPTION_RETURN_ON_ERROR |
                                 NSLINKCAPI_OPTION_NONE);
        /* If something went wrong, get the errors... */
        if (!platform_handle) {
            NSLinkEditErrors errors;
            int errorNumber;
            const char *fileName;
            NSLinkEditError(&errors, &errorNumber, &fileName, &err_msg);
        }
#else
        platform_handle = NSLinkcAPI(image, path, FALSE);
#endif
        NSDestroyObjectFileImage(image);
    }
    else if ((dsoerr == NSObjectFileImageFormat ||
             dsoerr == NSObjectFileImageInappropriateFile) &&
             NSAddLibrary(path) == TRUE) {
        platform_handle = (NScAPI)DYLD_LIBRARY_HANDLE;
    }
    else {
        err_msg = "cannot create object file image or add library";
    }

#elif defined(DSO_USE_DLFCN)
#if defined(OSF1) || defined(SEQUENT) || defined(SNI) ||\
    (defined(__FreeBSD_version) && (__FreeBSD_version >= 220000)) ||\
    defined(__DragonFly__)
    void *platform_handle = dlopen((char *)path, RTLD_NOW | RTLD_GLOBAL);

#else
    int flags = RTLD_NOW | RTLD_GLOBAL;
    void *platform_handle;
#ifdef _AIX
    if (strchr(path + 1, '(') && path[strlen(path) - 1] == ')')
    {
        /* This special archive.a(dso.so) syntax is required for
         * the way libtool likes to build shared libraries on AIX.
         * dlopen() support for such a library requires that the
         * RTLD_MEMBER flag be enabled.
         */
        flags |= RTLD_MEMBER;
    }
#endif
    platform_handle = dlopen(path, flags);
#endif    
#endif /* DSO_USE_x */

    *res_handle = kuda_pcalloc(pool, sizeof(**res_handle));

    if(platform_handle == NULL) {
#if defined(DSO_USE_SHL)
        (*res_handle)->errormsg = strerror(errno);
        return KUDA_EDSOOPEN;
#elif defined(DSO_USE_DYLD)
        (*res_handle)->errormsg = (err_msg) ? err_msg : "link failed";
        return KUDA_EDSOOPEN;
#elif defined(DSO_USE_DLFCN)
        (*res_handle)->errormsg = dlerror();
        return KUDA_EDSOOPEN;
#endif
    }

    (*res_handle)->handle = (void*)platform_handle;
    (*res_handle)->pool = pool;
    (*res_handle)->errormsg = NULL;

    kuda_pool_cleanup_register(pool, *res_handle, dso_cleanup, kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}
    
KUDA_DECLARE(kuda_status_t) kuda_dso_unload(kuda_dso_handle_t *handle)
{
    return kuda_pool_cleanup_run(handle->pool, handle, dso_cleanup);
}

KUDA_DECLARE(kuda_status_t) kuda_dso_sym(kuda_dso_handle_sym_t *ressym, 
                                      kuda_dso_handle_t *handle, 
                                      const char *symname)
{
#if defined(DSO_USE_SHL)
    void *symaddr = NULL;
    int status;

    errno = 0;
    status = shl_findsym((void *)&handle->handle, symname, TYPE_PROCEDURE, &symaddr);
    if (status == -1 && errno == 0) /* try TYPE_DATA instead */
        status = shl_findsym((void *)&handle->handle, symname, TYPE_DATA, &symaddr);
    if (status == -1)
        return KUDA_ESYMNOTFOUND;
    *ressym = symaddr;
    return KUDA_SUCCESS;

#elif defined(DSO_USE_DYLD)
    void *retval = NULL;
    NSSymbol symbol;
    char *symname2 = (char*)malloc(sizeof(char)*(strlen(symname)+2));
    sprintf(symname2, "_%s", symname);
#ifdef NSLINKCAPI_OPTION_PRIVATE
    if (handle->handle == DYLD_LIBRARY_HANDLE) {
        symbol = NSLookupAndBindSymbol(symname2);
    }
    else {
        symbol = NSLookupSymbolIncAPI((NScAPI)handle->handle, symname2);
    }
#else
    symbol = NSLookupAndBindSymbol(symname2);
#endif
    free(symname2);
    if (symbol == NULL) {
        handle->errormsg = "undefined symbol";
	return KUDA_ESYMNOTFOUND;
    }
    retval = NSAddressOfSymbol(symbol);
    if (retval == NULL) {
        handle->errormsg = "cannot resolve symbol";
	return KUDA_ESYMNOTFOUND;
    }
    *ressym = retval;
    return KUDA_SUCCESS;
#elif defined(DSO_USE_DLFCN)

#if defined(DLSYM_NEEDS_UNDERSCORE)
    void *retval;
    char *symbol = (char*)malloc(sizeof(char)*(strlen(symname)+2));
    sprintf(symbol, "_%s", symname);
    retval = dlsym(handle->handle, symbol);
    free(symbol);
#elif defined(SEQUENT) || defined(SNI)
    void *retval = dlsym(handle->handle, (char *)symname);
#else
    void *retval = dlsym(handle->handle, symname);
#endif /* DLSYM_NEEDS_UNDERSCORE */

    if (retval == NULL) {
        handle->errormsg = dlerror();
        return KUDA_ESYMNOTFOUND;
    }

    *ressym = retval;
    
    return KUDA_SUCCESS;
#endif /* DSO_USE_x */
}

KUDA_DECLARE(const char *) kuda_dso_error(kuda_dso_handle_t *dso, char *buffer, 
                                        kuda_size_t buflen)
{
    if (dso->errormsg) {
        kuda_cpystrn(buffer, dso->errormsg, buflen);
        return dso->errormsg;
    }
    return "No Error";
}

#endif
