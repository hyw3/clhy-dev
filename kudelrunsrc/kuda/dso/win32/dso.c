/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_dso.h"
#include "kuda_strings.h"
#include "kuda_private.h"
#include "kuda_arch_file_io.h"
#include "kuda_arch_utf8.h"

#if KUDA_HAS_DSO

KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_put(kuda_dso_handle_t **kudadso,
                                                kuda_platform_dso_handle_t osdso,
                                                kuda_pool_t *pool)
{
    *kudadso = kuda_pcalloc(pool, sizeof **kudadso);
    (*kudadso)->handle = osdso;
    (*kudadso)->cont = pool;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_get(kuda_platform_dso_handle_t *osdso,
                                                kuda_dso_handle_t *kudadso)
{
    *osdso = kudadso->handle;
    return KUDA_SUCCESS;
}

static kuda_status_t dso_cleanup(void *thedso)
{
    kuda_dso_handle_t *dso = thedso;

    if (dso->handle != NULL && !FreeLibrary(dso->handle)) {
        return kuda_get_platform_error();
    }
    dso->handle = NULL;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_dso_load(struct kuda_dso_handle_t **res_handle, 
                                       const char *path, kuda_pool_t *ctx)
{
    HINSTANCE platform_handle;
    kuda_status_t rv;
#ifndef _WIN32_WCE
    UINT em;
#endif

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE 
    {
        kuda_wchar_t wpath[KUDA_PATH_MAX];
        if ((rv = utf8_to_unicode_path(wpath, sizeof(wpath) 
                                            / sizeof(kuda_wchar_t), path))
                != KUDA_SUCCESS) {
            *res_handle = kuda_pcalloc(ctx, sizeof(**res_handle));
            return ((*res_handle)->load_error = rv);
        }
        /* Prevent ugly popups from killing our app */
#ifndef _WIN32_WCE
        em = SetErrorMode(SEM_FAILCRITICALERRORS);
#endif
        platform_handle = LoadLibraryExW(wpath, NULL, 0);
        if (!platform_handle)
            platform_handle = LoadLibraryExW(wpath, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
        if (!platform_handle)
            rv = kuda_get_platform_error();
#ifndef _WIN32_WCE
        SetErrorMode(em);
#endif
    }
#endif /* KUDA_HAS_UNICODE_FS */
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        char fspec[KUDA_PATH_MAX], *p = fspec;
        /* Must convert path from / to \ notation.
         * Per PR2555, the LoadLibraryEx function is very picky about slashes.
         * Debugging on NT 4 SP 6a reveals First Chance Exception within NTDLL.
         * LoadLibrary in the MS PSDK also reveals that it -explicitly- states
         * that backslashes must be used for the LoadLibrary family of calls.
         */
        kuda_cpystrn(fspec, path, sizeof(fspec));
        while ((p = strchr(p, '/')) != NULL)
            *p = '\\';
        
        /* Prevent ugly popups from killing our app */
        em = SetErrorMode(SEM_FAILCRITICALERRORS);
        platform_handle = LoadLibraryEx(path, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
        if (!platform_handle)
            platform_handle = LoadLibraryEx(path, NULL, 0);
        if (!platform_handle)
            rv = kuda_get_platform_error();
        else
            rv = KUDA_SUCCESS;
        SetErrorMode(em);
    }
#endif

    *res_handle = kuda_pcalloc(ctx, sizeof(**res_handle));
    (*res_handle)->cont = ctx;

    if (rv) {
        return ((*res_handle)->load_error = rv);
    }

    (*res_handle)->handle = (void*)platform_handle;
    (*res_handle)->load_error = KUDA_SUCCESS;

    kuda_pool_cleanup_register(ctx, *res_handle, dso_cleanup, kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}
    
KUDA_DECLARE(kuda_status_t) kuda_dso_unload(struct kuda_dso_handle_t *handle)
{
    return kuda_pool_cleanup_run(handle->cont, handle, dso_cleanup);
}

KUDA_DECLARE(kuda_status_t) kuda_dso_sym(kuda_dso_handle_sym_t *ressym, 
                         struct kuda_dso_handle_t *handle, 
                         const char *symname)
{
#ifdef _WIN32_WCE
    kuda_size_t symlen = strlen(symname) + 1;
    kuda_size_t wsymlen = 256;
    kuda_wchar_t wsymname[256];
    kuda_status_t rv;

    rv = kuda_conv_utf8_to_ucs2(wsymname, &wsymlen, symname, &symlen);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }
    else if (symlen) {
        return KUDA_ENAMETOOLONG;
    }

    *ressym = (kuda_dso_handle_sym_t)GetProcAddressW(handle->handle, wsymname);
#else
    *ressym = (kuda_dso_handle_sym_t)GetProcAddress(handle->handle, symname);
#endif
    if (!*ressym) {
        return kuda_get_platform_error();
    }
    return KUDA_SUCCESS;
}

KUDA_DECLARE(const char *) kuda_dso_error(kuda_dso_handle_t *dso, char *buf, kuda_size_t bufsize)
{
    return kuda_strerror(dso->load_error, buf, bufsize);
}

#endif
