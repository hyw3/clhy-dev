/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_dso.h"
#include "kuda_strings.h"
#include "kuda_portable.h"

#include <library.h>
#include <unistd.h>

KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_put(kuda_dso_handle_t **kudadso,
                                                kuda_platform_dso_handle_t osdso,
                                                kuda_pool_t *pool)
{
    *kudadso = kuda_pcalloc(pool, sizeof **kudadso);
    (*kudadso)->handle = osdso;
    (*kudadso)->pool = pool;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_dso_handle_get(kuda_platform_dso_handle_t *osdso,
                                                kuda_dso_handle_t *kudadso)
{
    *osdso = kudadso->handle;
    return KUDA_SUCCESS;
}

static kuda_status_t dso_cleanup(void *thedso)
{
    kuda_dso_handle_t *dso = thedso;
    sym_list *symbol = NULL;
    void *NLMHandle = getnlmhandle();

    if (dso->handle == NULL)
        return KUDA_SUCCESS;

    if (dso->symbols != NULL) {
        symbol = dso->symbols;
        while (symbol) {
            UnImportPublicObject(NLMHandle, symbol->symbol);
            symbol = symbol->next;
        }
    }

    if (dlclose(dso->handle) != 0)
        return KUDA_EINIT;

    dso->handle = NULL;
    dso->symbols = NULL;
    dso->path = NULL;

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_dso_load(kuda_dso_handle_t **res_handle, 
                                       const char *path, kuda_pool_t *pool)
{

    void *platform_handle = NULL;
    char *fullpath = NULL;
    kuda_status_t rv;

    if ((rv = kuda_filepath_merge(&fullpath, NULL, path, 
                                 KUDA_FILEPATH_NATIVE, pool)) != KUDA_SUCCESS) {
        return rv;
    }

    platform_handle = dlopen(fullpath, RTLD_NOW | RTLD_LOCAL);

    *res_handle = kuda_pcalloc(pool, sizeof(**res_handle));

    if(platform_handle == NULL) {
        (*res_handle)->errormsg = dlerror();
        return KUDA_EDSOOPEN;
    }

    (*res_handle)->handle = (void*)platform_handle;
    (*res_handle)->pool = pool;
    (*res_handle)->errormsg = NULL;
    (*res_handle)->symbols = NULL;
    (*res_handle)->path = kuda_pstrdup(pool, fullpath);

    kuda_pool_cleanup_register(pool, *res_handle, dso_cleanup, kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}
    
KUDA_DECLARE(kuda_status_t) kuda_dso_unload(kuda_dso_handle_t *handle)
{
    return kuda_pool_cleanup_run(handle->pool, handle, dso_cleanup);
}

KUDA_DECLARE(kuda_status_t) kuda_dso_sym(kuda_dso_handle_sym_t *ressym, 
                                      kuda_dso_handle_t *handle, 
                                      const char *symname)
{
    sym_list *symbol = NULL;
    void *retval = dlsym(handle->handle, symname);

    if (retval == NULL) {
        handle->errormsg = dlerror();
        return KUDA_ESYMNOTFOUND;
    }

    symbol = kuda_pcalloc(handle->pool, sizeof(sym_list));
    symbol->next = handle->symbols;
    handle->symbols = symbol;
    symbol->symbol = kuda_pstrdup(handle->pool, symname);

    *ressym = retval;
    
    return KUDA_SUCCESS;
}

KUDA_DECLARE(const char *) kuda_dso_error(kuda_dso_handle_t *dso, char *buffer, 
                                        kuda_size_t buflen)
{
    if (dso->errormsg) {
        kuda_cpystrn(buffer, dso->errormsg, buflen);
        return dso->errormsg;
    }
    return "No Error";
}

