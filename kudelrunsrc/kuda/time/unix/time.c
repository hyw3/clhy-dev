/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_portable.h"
#include "kuda_time.h"
#include "kuda_lib.h"
#include "kuda_private.h"
#include "kuda_strings.h"

/* private KUDA headers */
#include "kuda_arch_internal_time.h"

/* System Headers required for time library */
#if KUDA_HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_TIME_H
#include <time.h>
#endif
/* End System Headers */

#if !defined(HAVE_STRUCT_TM_TM_GMTOFF) && !defined(HAVE_STRUCT_TM___TM_GMTOFF)
static kuda_int32_t server_gmt_offset;
#define NO_GMTOFF_IN_STRUCT_TM
#endif          

static kuda_int32_t get_offset(struct tm *tm)
{
#if defined(HAVE_STRUCT_TM_TM_GMTOFF)
    return tm->tm_gmtoff;
#elif defined(HAVE_STRUCT_TM___TM_GMTOFF)
    return tm->__tm_gmtoff;
#else
#ifdef NETWARE
    /* Need to adjust the global variable each time otherwise
        the web server would have to be restarted when daylight
        savings changes.
    */
    if (daylightOnOff) {
        return server_gmt_offset + daylightOffset;
    }
#else
    if (tm->tm_isdst)
        return server_gmt_offset + 3600;
#endif
    return server_gmt_offset;
#endif
}

KUDA_DECLARE(kuda_status_t) kuda_time_ansi_put(kuda_time_t *result,
                                            time_t input)
{
    *result = (kuda_time_t)input * KUDA_USEC_PER_SEC;
    return KUDA_SUCCESS;
}

/* NB NB NB NB This returns GMT!!!!!!!!!! */
KUDA_DECLARE(kuda_time_t) kuda_time_now(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * KUDA_USEC_PER_SEC + tv.tv_usec;
}

static void explode_time(kuda_time_exp_t *xt, kuda_time_t t,
                         kuda_int32_t offset, int use_localtime)
{
    struct tm tm;
    time_t tt = (t / KUDA_USEC_PER_SEC) + offset;
    xt->tm_usec = t % KUDA_USEC_PER_SEC;

#if KUDA_HAS_THREADS && defined (_POSIX_THREAD_SAFE_FUNCTIONS)
    if (use_localtime)
        localtime_r(&tt, &tm);
    else
        gmtime_r(&tt, &tm);
#else
    if (use_localtime)
        tm = *localtime(&tt);
    else
        tm = *gmtime(&tt);
#endif

    xt->tm_sec  = tm.tm_sec;
    xt->tm_min  = tm.tm_min;
    xt->tm_hour = tm.tm_hour;
    xt->tm_mday = tm.tm_mday;
    xt->tm_mon  = tm.tm_mon;
    xt->tm_year = tm.tm_year;
    xt->tm_wday = tm.tm_wday;
    xt->tm_yday = tm.tm_yday;
    xt->tm_isdst = tm.tm_isdst;
    xt->tm_gmtoff = get_offset(&tm);
}

KUDA_DECLARE(kuda_status_t) kuda_time_exp_tz(kuda_time_exp_t *result,
                                          kuda_time_t input, kuda_int32_t offs)
{
    explode_time(result, input, offs, 0);
    result->tm_gmtoff = offs;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_time_exp_gmt(kuda_time_exp_t *result,
                                           kuda_time_t input)
{
    return kuda_time_exp_tz(result, input, 0);
}

KUDA_DECLARE(kuda_status_t) kuda_time_exp_lt(kuda_time_exp_t *result,
                                                kuda_time_t input)
{
#if defined(__EMX__)
    /* EMX gcc (OS2) has a timezone global we can use */
    return kuda_time_exp_tz(result, input, -timezone);
#else
    explode_time(result, input, 0, 1);
    return KUDA_SUCCESS;
#endif /* __EMX__ */
}

KUDA_DECLARE(kuda_status_t) kuda_time_exp_get(kuda_time_t *t, kuda_time_exp_t *xt)
{
    kuda_time_t year = xt->tm_year;
    kuda_time_t days;
    static const int dayoffset[12] =
    {306, 337, 0, 31, 61, 92, 122, 153, 184, 214, 245, 275};

    /* shift new year to 1st March in order to make leap year calc easy */

    if (xt->tm_mon < 2)
        year--;

    /* Find number of days since 1st March 1900 (in the Gregorian calendar). */

    days = year * 365 + year / 4 - year / 100 + (year / 100 + 3) / 4;
    days += dayoffset[xt->tm_mon] + xt->tm_mday - 1;
    days -= 25508;              /* 1 jan 1970 is 25508 days since 1 mar 1900 */
    days = ((days * 24 + xt->tm_hour) * 60 + xt->tm_min) * 60 + xt->tm_sec;

    if (days < 0) {
        return KUDA_EBADDATE;
    }
    *t = days * KUDA_USEC_PER_SEC + xt->tm_usec;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_time_exp_gmt_get(kuda_time_t *t, 
                                               kuda_time_exp_t *xt)
{
    kuda_status_t status = kuda_time_exp_get(t, xt);
    if (status == KUDA_SUCCESS)
        *t -= (kuda_time_t) xt->tm_gmtoff * KUDA_USEC_PER_SEC;
    return status;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_imp_time_get(kuda_platform_imp_time_t **ostime,
                                              kuda_time_t *kudatime)
{
    (*ostime)->tv_usec = *kudatime % KUDA_USEC_PER_SEC;
    (*ostime)->tv_sec = *kudatime / KUDA_USEC_PER_SEC;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_exp_time_get(kuda_platform_exp_time_t **ostime,
                                              kuda_time_exp_t *kudatime)
{
    (*ostime)->tm_sec  = kudatime->tm_sec;
    (*ostime)->tm_min  = kudatime->tm_min;
    (*ostime)->tm_hour = kudatime->tm_hour;
    (*ostime)->tm_mday = kudatime->tm_mday;
    (*ostime)->tm_mon  = kudatime->tm_mon;
    (*ostime)->tm_year = kudatime->tm_year;
    (*ostime)->tm_wday = kudatime->tm_wday;
    (*ostime)->tm_yday = kudatime->tm_yday;
    (*ostime)->tm_isdst = kudatime->tm_isdst;

#if defined(HAVE_STRUCT_TM_TM_GMTOFF)
    (*ostime)->tm_gmtoff = kudatime->tm_gmtoff;
#elif defined(HAVE_STRUCT_TM___TM_GMTOFF)
    (*ostime)->__tm_gmtoff = kudatime->tm_gmtoff;
#endif

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_imp_time_put(kuda_time_t *kudatime,
                                              kuda_platform_imp_time_t **ostime,
                                              kuda_pool_t *cont)
{
    *kudatime = (*ostime)->tv_sec * KUDA_USEC_PER_SEC + (*ostime)->tv_usec;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_exp_time_put(kuda_time_exp_t *kudatime,
                                              kuda_platform_exp_time_t **ostime,
                                              kuda_pool_t *cont)
{
    kudatime->tm_sec = (*ostime)->tm_sec;
    kudatime->tm_min = (*ostime)->tm_min;
    kudatime->tm_hour = (*ostime)->tm_hour;
    kudatime->tm_mday = (*ostime)->tm_mday;
    kudatime->tm_mon = (*ostime)->tm_mon;
    kudatime->tm_year = (*ostime)->tm_year;
    kudatime->tm_wday = (*ostime)->tm_wday;
    kudatime->tm_yday = (*ostime)->tm_yday;
    kudatime->tm_isdst = (*ostime)->tm_isdst;

#if defined(HAVE_STRUCT_TM_TM_GMTOFF)
    kudatime->tm_gmtoff = (*ostime)->tm_gmtoff;
#elif defined(HAVE_STRUCT_TM___TM_GMTOFF)
    kudatime->tm_gmtoff = (*ostime)->__tm_gmtoff;
#endif

    return KUDA_SUCCESS;
}

KUDA_DECLARE(void) kuda_sleep(kuda_interval_time_t t)
{
#ifdef OS2
    DosSleep(t/1000);
#elif defined(BEOS)
    snooze(t);
#elif defined(NETWARE)
    delay(t/1000);
#else
    struct timeval tv;
    tv.tv_usec = t % KUDA_USEC_PER_SEC;
    tv.tv_sec = t / KUDA_USEC_PER_SEC;
    select(0, NULL, NULL, NULL, &tv);
#endif
}

#ifdef OS2
KUDA_DECLARE(kuda_status_t) kuda_os2_time_to_kuda_time(kuda_time_t *result,
                                                   FDATE os2date,
                                                   FTIME os2time)
{
  struct tm tmpdate;

  memset(&tmpdate, 0, sizeof(tmpdate));
  tmpdate.tm_hour  = os2time.hours;
  tmpdate.tm_min   = os2time.minutes;
  tmpdate.tm_sec   = os2time.twosecs * 2;

  tmpdate.tm_mday  = os2date.day;
  tmpdate.tm_mon   = os2date.month - 1;
  tmpdate.tm_year  = os2date.year + 80;
  tmpdate.tm_isdst = -1;

  *result = mktime(&tmpdate) * KUDA_USEC_PER_SEC;
  return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_kuda_time_to_os2_time(FDATE *os2date,
                                                   FTIME *os2time,
                                                   kuda_time_t kudatime)
{
    time_t ansitime = kudatime / KUDA_USEC_PER_SEC;
    struct tm *lt;
    lt = localtime(&ansitime);
    os2time->hours    = lt->tm_hour;
    os2time->minutes  = lt->tm_min;
    os2time->twosecs  = lt->tm_sec / 2;

    os2date->day      = lt->tm_mday;
    os2date->month    = lt->tm_mon + 1;
    os2date->year     = lt->tm_year - 80;
    return KUDA_SUCCESS;
}
#endif

#ifdef NETWARE
KUDA_DECLARE(void) kuda_netware_setup_time(void)
{
    tzset();
    server_gmt_offset = -TZONE;
}
#else
KUDA_DECLARE(void) kuda_unix_setup_time(void)
{
#ifdef NO_GMTOFF_IN_STRUCT_TM
    /* Precompute the offset from GMT on systems where it's not
       in struct tm.

       Note: This offset is normalized to be independent of daylight
       savings time; if the calculation happens to be done in a
       time/place where a daylight savings adjustment is in effect,
       the returned offset has the same value that it would have
       in the same location if daylight savings were not in effect.
       The reason for this is that the returned offset can be
       applied to a past or future timestamp in explode_time(),
       so the DST adjustment obtained from the current time won't
       necessarily be applicable.

       mktime() is the inverse of localtime(); so, presumably,
       passing in a struct tm made by gmtime() let's us calculate
       the true GMT offset. However, there's a catch: if daylight
       savings is in effect, gmtime()will set the tm_isdst field
       and confuse mktime() into returning a time that's offset
       by one hour. In that case, we must adjust the calculated GMT
       offset.

     */

    struct timeval now;
    time_t t1, t2;
    struct tm t;

    gettimeofday(&now, NULL);
    t1 = now.tv_sec;
    t2 = 0;

#if KUDA_HAS_THREADS && defined(_POSIX_THREAD_SAFE_FUNCTIONS)
    gmtime_r(&t1, &t);
#else
    t = *gmtime(&t1);
#endif
    t.tm_isdst = 0; /* we know this GMT time isn't daylight-savings */
    t2 = mktime(&t);
    server_gmt_offset = (kuda_int32_t) difftime(t1, t2);
#endif /* NO_GMTOFF_IN_STRUCT_TM */
}

#endif

/* A noop on all known Unix implementations */
KUDA_DECLARE(void) kuda_time_clock_hires(kuda_pool_t *p)
{
    return;
}


