/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_arch_atime.h"
#include "kuda_time.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_portable.h"
#if KUDA_HAVE_TIME_H
#include <time.h>
#endif
#if KUDA_HAVE_ERRNO_H
#include <errno.h>
#endif
#include <string.h>
#include <winbase.h>
#include "kuda_arch_misc.h"

/* Leap year is any year divisible by four, but not by 100 unless also
 * divisible by 400
 */
#define IsLeapYear(y) ((!(y % 4)) ? (((y % 400) && !(y % 100)) ? 0 : 1) : 0)

static DWORD get_local_timezone(TIME_ZONE_INFORMATION **tzresult)
{
    static TIME_ZONE_INFORMATION tz;
    static DWORD result;
    static int init = 0;

    if (!init) {
        result = GetTimeZoneInformation(&tz);
        init = 1;
    }

    *tzresult = &tz;
    return result;
}

static void SystemTimeToKudaExpTime(kuda_time_exp_t *xt, SYSTEMTIME *tm)
{
    static const int dayoffset[12] =
    {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};

    /* Note; the caller is responsible for filling in detailed tm_usec,
     * tm_gmtoff and tm_isdst data when applicable.
     */
    xt->tm_usec = tm->wMilliseconds * 1000;
    xt->tm_sec  = tm->wSecond;
    xt->tm_min  = tm->wMinute;
    xt->tm_hour = tm->wHour;
    xt->tm_mday = tm->wDay;
    xt->tm_mon  = tm->wMonth - 1;
    xt->tm_year = tm->wYear - 1900;
    xt->tm_wday = tm->wDayOfWeek;
    xt->tm_yday = dayoffset[xt->tm_mon] + (tm->wDay - 1);
    xt->tm_isdst = 0;
    xt->tm_gmtoff = 0;

    /* If this is a leap year, and we're past the 28th of Feb. (the
     * 58th day after Jan. 1), we'll increment our tm_yday by one.
     */
    if (IsLeapYear(tm->wYear) && (xt->tm_yday > 58))
        xt->tm_yday++;
}

KUDA_DECLARE(kuda_status_t) kuda_time_ansi_put(kuda_time_t *result, 
                                                    time_t input)
{
    *result = (kuda_time_t) input * KUDA_USEC_PER_SEC;
    return KUDA_SUCCESS;
}

/* Return micro-seconds since the Unix epoch (jan. 1, 1970) UTC */
KUDA_DECLARE(kuda_time_t) kuda_time_now(void)
{
    LONGLONG kudatime = 0;
    FILETIME time;
#ifndef _WIN32_WCE
    GetSystemTimeAsFileTime(&time);
#else
    SYSTEMTIME st;
    GetSystemTime(&st);
    SystemTimeToFileTime(&st, &time);
#endif
    FileTimeToKudaTime(&kudatime, &time);
    return kudatime; 
}

KUDA_DECLARE(kuda_status_t) kuda_time_exp_gmt(kuda_time_exp_t *result,
                                           kuda_time_t input)
{
    FILETIME ft;
    SYSTEMTIME st;
    KudaTimeToFileTime(&ft, input);
    FileTimeToSystemTime(&ft, &st);
    /* The Platform SDK documents that SYSTEMTIME/FILETIME are
     * generally UTC, so no timezone info needed
     * The time value makes a roundtrip, st cannot be invalid below.
     */
    SystemTimeToKudaExpTime(result, &st);
    result->tm_usec = (kuda_int32_t) (input % KUDA_USEC_PER_SEC);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_time_exp_tz(kuda_time_exp_t *result, 
                                          kuda_time_t input, 
                                          kuda_int32_t offs)
{
    FILETIME ft;
    SYSTEMTIME st;
    KudaTimeToFileTime(&ft, input + (offs *  KUDA_USEC_PER_SEC));
    FileTimeToSystemTime(&ft, &st);
    /* The Platform SDK documents that SYSTEMTIME/FILETIME are
     * generally UTC, so we will simply note the offs used.
     * The time value makes a roundtrip, st cannot be invalid below.
     */
    SystemTimeToKudaExpTime(result, &st);
    result->tm_usec = (kuda_int32_t) (input % KUDA_USEC_PER_SEC);
    result->tm_gmtoff = offs;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_time_exp_lt(kuda_time_exp_t *result,
                                          kuda_time_t input)
{
    SYSTEMTIME st;
    FILETIME ft, localft;

    KudaTimeToFileTime(&ft, input);

#if KUDA_HAS_UNICODE_FS && !defined(_WIN32_WCE)
    IF_WIN_PLATFORM_IS_UNICODE
    {
        TIME_ZONE_INFORMATION *tz;
        SYSTEMTIME localst;
        kuda_time_t localtime;

        get_local_timezone(&tz);

        FileTimeToSystemTime(&ft, &st);

        /* The Platform SDK documents that SYSTEMTIME/FILETIME are
         * generally UTC.  We use SystemTimeToTzSpecificLocalTime
         * because FileTimeToLocalFileFime is documented that the
         * resulting time local file time would have DST relative
         * to the *present* date, not the date converted.
         * The time value makes a roundtrip, localst cannot be invalid below.
         */
        SystemTimeToTzSpecificLocalTime(tz, &st, &localst);
        SystemTimeToKudaExpTime(result, &localst);
        result->tm_usec = (kuda_int32_t) (input % KUDA_USEC_PER_SEC);


        /* Recover the resulting time as an kuda time and use the
         * delta for gmtoff in seconds (and ignore msec rounding) 
         */
        SystemTimeToFileTime(&localst, &localft);
        FileTimeToKudaTime(&localtime, &localft);
        result->tm_gmtoff = (int)kuda_time_sec(localtime) 
                          - (int)kuda_time_sec(input);

        /* To compute the dst flag, we compare the expected 
         * local (standard) timezone bias to the delta.
         * [Note, in war time or double daylight time the
         * resulting tm_isdst is, desireably, 2 hours]
         */
        result->tm_isdst = (result->tm_gmtoff / 3600)
                         - (-(tz->Bias + tz->StandardBias) / 60);
    }
#endif
#if KUDA_HAS_ANSI_FS || defined(_WIN32_WCE)
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        TIME_ZONE_INFORMATION tz;
	/* XXX: This code is simply *wrong*.  The time converted will always
         * map to the *now current* status of daylight savings time.
         * The time value makes a roundtrip, st cannot be invalid below.
         */

        FileTimeToLocalFileTime(&ft, &localft);
        FileTimeToSystemTime(&localft, &st);
        SystemTimeToKudaExpTime(result, &st);
        result->tm_usec = (kuda_int32_t) (input % KUDA_USEC_PER_SEC);

        switch (GetTimeZoneInformation(&tz)) {
            case TIME_ZONE_ID_UNKNOWN:
                result->tm_isdst = 0;
                /* Bias = UTC - local time in minutes
                 * tm_gmtoff is seconds east of UTC
                 */
                result->tm_gmtoff = tz.Bias * -60;
                break;
            case TIME_ZONE_ID_STANDARD:
                result->tm_isdst = 0;
                result->tm_gmtoff = (tz.Bias + tz.StandardBias) * -60;
                break;
            case TIME_ZONE_ID_DAYLIGHT:
                result->tm_isdst = 1;
                result->tm_gmtoff = (tz.Bias + tz.DaylightBias) * -60;
                break;
            default:
                /* noop */;
        }
    }
#endif

    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_time_exp_get(kuda_time_t *t,
                                           kuda_time_exp_t *xt)
{
    kuda_time_t year = xt->tm_year;
    kuda_time_t days;
    static const int dayoffset[12] =
    {306, 337, 0, 31, 61, 92, 122, 153, 184, 214, 245, 275};

    /* shift new year to 1st March in order to make leap year calc easy */

    if (xt->tm_mon < 2)
        year--;

    /* Find number of days since 1st March 1900 (in the Gregorian calendar). */

    days = year * 365 + year / 4 - year / 100 + (year / 100 + 3) / 4;
    days += dayoffset[xt->tm_mon] + xt->tm_mday - 1;
    days -= 25508;              /* 1 jan 1970 is 25508 days since 1 mar 1900 */

    days = ((days * 24 + xt->tm_hour) * 60 + xt->tm_min) * 60 + xt->tm_sec;

    if (days < 0) {
        return KUDA_EBADDATE;
    }
    *t = days * KUDA_USEC_PER_SEC + xt->tm_usec;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_time_exp_gmt_get(kuda_time_t *t,
                                               kuda_time_exp_t *xt)
{
    kuda_status_t status = kuda_time_exp_get(t, xt);
    if (status == KUDA_SUCCESS)
        *t -= (kuda_time_t) xt->tm_gmtoff * KUDA_USEC_PER_SEC;
    return status;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_imp_time_get(kuda_platform_imp_time_t **ostime,
                                              kuda_time_t *kudatime)
{
    /* TODO: Consider not passing in pointer to kuda_time_t (e.g., call by value) */
    KudaTimeToFileTime(*ostime, *kudatime);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_exp_time_get(kuda_platform_exp_time_t **ostime, 
                                              kuda_time_exp_t *kudaexptime)
{
    (*ostime)->wYear = kudaexptime->tm_year + 1900;
    (*ostime)->wMonth = kudaexptime->tm_mon + 1;
    (*ostime)->wDayOfWeek = kudaexptime->tm_wday;
    (*ostime)->wDay = kudaexptime->tm_mday;
    (*ostime)->wHour = kudaexptime->tm_hour;
    (*ostime)->wMinute = kudaexptime->tm_min;
    (*ostime)->wSecond = kudaexptime->tm_sec;
    (*ostime)->wMilliseconds = kudaexptime->tm_usec / 1000;
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_imp_time_put(kuda_time_t *kudatime,
                                              kuda_platform_imp_time_t **ostime,
                                              kuda_pool_t *cont)
{
    /* XXX: sanity failure, what is file time, gmt or local ?
     */
    FileTimeToKudaTime(kudatime, *ostime);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(kuda_status_t) kuda_platform_exp_time_put(kuda_time_exp_t *kudatime,
                                              kuda_platform_exp_time_t **ostime,
                                              kuda_pool_t *cont)
{
    /* The Platform SDK documents that SYSTEMTIME/FILETIME are
     * generally UTC, so no timezone info needed
     */
    if ((*ostime)->wMonth < 1 || (*ostime)->wMonth > 12)
        return KUDA_EBADDATE;

    SystemTimeToKudaExpTime(kudatime, *ostime);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(void) kuda_sleep(kuda_interval_time_t t)
{
    /* One of the few sane situations for a cast, Sleep
     * is in ms, not us, and passed as a DWORD value
     */
    Sleep((DWORD)(t / 1000));
}

#if defined(_WIN32_WCE)
/* A noop on WinCE, like Unix implementation */
KUDA_DECLARE(void) kuda_time_clock_hires(kuda_pool_t *p)
{
    return;
}
#else
static kuda_status_t clock_restore(void *unsetres)
{
    ULONG newRes;
    SetTimerResolution((ULONG)(kuda_ssize_t)unsetres, FALSE, &newRes);
    return KUDA_SUCCESS;
}

KUDA_DECLARE(void) kuda_time_clock_hires(kuda_pool_t *p)
{
    ULONG newRes;
    /* Timer resolution is stated in 100ns units.  Note that TRUE requests the
     * new clock resolution, FALSE above releases the request.
     */
    if (SetTimerResolution(10000, TRUE, &newRes) == 0 /* STATUS_SUCCESS */) {
        /* register the cleanup... */
        kuda_pool_cleanup_register(p, (void*)10000, clock_restore,
                                  kuda_pool_cleanup_null);
    }
}
#endif
