# Microsoft Developer Studio Generated NMAKE File, Based on libkudadelman.dsp
!IF "$(CFG)" == ""
CFG=libkudadelman - Win32 Release
!MESSAGE No configuration specified. Defaulting to libkudadelman - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "libkudadelman - Win32 Release" && "$(CFG)" != "libkudadelman - Win32 Debug" && "$(CFG)" != "libkudadelman - x64 Release" && "$(CFG)" != "libkudadelman - x64 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libkudadelman.mak" CFG="libkudadelman - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libkudadelman - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libkudadelman - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libkudadelman - x64 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libkudadelman - x64 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF "$(_HAVE_OSSL110)" == "1"
SSLINC=/I ../openssl/include
!ELSE 
SSLINC=/I ../openssl/inc32
!ENDIF 

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : ".\include\kudelman_want.h" "$(OUTDIR)\libkudadelman-1.dll" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "xml - Win32 Release" "libkudaiconv - Win32 Release" "libkuda - Win32 Release" ".\include\kudelman_want.h" "$(OUTDIR)\libkudadelman-1.dll" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudaiconv - Win32 ReleaseCLEAN" "xml - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\kuda_base64.obj"
	-@erase "$(INTDIR)\kuda_brigade.obj"
	-@erase "$(INTDIR)\kuda_buckets.obj"
	-@erase "$(INTDIR)\kuda_buckets_alloc.obj"
	-@erase "$(INTDIR)\kuda_buckets_eos.obj"
	-@erase "$(INTDIR)\kuda_buckets_file.obj"
	-@erase "$(INTDIR)\kuda_buckets_flush.obj"
	-@erase "$(INTDIR)\kuda_buckets_heap.obj"
	-@erase "$(INTDIR)\kuda_buckets_mmap.obj"
	-@erase "$(INTDIR)\kuda_buckets_pipe.obj"
	-@erase "$(INTDIR)\kuda_buckets_pool.obj"
	-@erase "$(INTDIR)\kuda_buckets_refcount.obj"
	-@erase "$(INTDIR)\kuda_buckets_simple.obj"
	-@erase "$(INTDIR)\kuda_buckets_socket.obj"
	-@erase "$(INTDIR)\kuda_crypto.obj"
	-@erase "$(INTDIR)\kuda_date.obj"
	-@erase "$(INTDIR)\kuda_dbd.obj"
	-@erase "$(INTDIR)\kuda_dbm.obj"
	-@erase "$(INTDIR)\kuda_dbm_sdbm.obj"
	-@erase "$(INTDIR)\kuda_hooks.obj"
	-@erase "$(INTDIR)\kuda_ldap_stub.obj"
	-@erase "$(INTDIR)\kuda_ldap_url.obj"
	-@erase "$(INTDIR)\kuda_md4.obj"
	-@erase "$(INTDIR)\kuda_md5.obj"
	-@erase "$(INTDIR)\kuda_memcache.obj"
	-@erase "$(INTDIR)\kuda_passwd.obj"
	-@erase "$(INTDIR)\kuda_queue.obj"
	-@erase "$(INTDIR)\kuda_redis.obj"
	-@erase "$(INTDIR)\kuda_reslist.obj"
	-@erase "$(INTDIR)\kuda_rmm.obj"
	-@erase "$(INTDIR)\kuda_sha1.obj"
	-@erase "$(INTDIR)\kuda_siphash.obj"
	-@erase "$(INTDIR)\kuda_strmatch.obj"
	-@erase "$(INTDIR)\kuda_thread_pool.obj"
	-@erase "$(INTDIR)\kuda_uri.obj"
	-@erase "$(INTDIR)\kuda_xml.obj"
	-@erase "$(INTDIR)\kudelman_dso.obj"
	-@erase "$(INTDIR)\kudelman_version.obj"
	-@erase "$(INTDIR)\crypt_blowfish.obj"
	-@erase "$(INTDIR)\getuuid.obj"
	-@erase "$(INTDIR)\libkudadelman.res"
	-@erase "$(INTDIR)\libkudadelman_src.idb"
	-@erase "$(INTDIR)\libkudadelman_src.pdb"
	-@erase "$(INTDIR)\sdbm.obj"
	-@erase "$(INTDIR)\sdbm_hash.obj"
	-@erase "$(INTDIR)\sdbm_lock.obj"
	-@erase "$(INTDIR)\sdbm_pair.obj"
	-@erase "$(INTDIR)\uuid.obj"
	-@erase "$(INTDIR)\xlate.obj"
	-@erase "$(OUTDIR)\libkudadelman-1.dll"
	-@erase "$(OUTDIR)\libkudadelman-1.exp"
	-@erase "$(OUTDIR)\libkudadelman-1.lib"
	-@erase "$(OUTDIR)\libkudadelman-1.pdb"
	-@erase ".\include\kudelman_want.h"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "./include" /I "../kuda/include" /I "./include/private" /I "../kuda-iconv/include" /I "./dbm/sdbm" /I "./xml/expat/lib" $(SSLINC) /D "NDEBUG" /D "KUDA_DECLARE_EXPORT" /D "KUDELMAN_DECLARE_EXPORT" /D "KUDELMAN_USE_SDBM" $(XML_OPTIONS) /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\libkudadelman_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL" 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\libkudadelman.res" /i "./include" /i "../kuda/include" /d "NDEBUG" /d "KUDELMAN_VERSION_ONLY" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libkudadelman.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=$(XML_PARSER).lib kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib /nologo /base:"0x6EE60000" /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\libkudadelman-1.pdb" /debug /out:"$(OUTDIR)\libkudadelman-1.dll" /implib:"$(OUTDIR)\libkudadelman-1.lib"  /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\kuda_brigade.obj" \
	"$(INTDIR)\kuda_buckets.obj" \
	"$(INTDIR)\kuda_buckets_alloc.obj" \
	"$(INTDIR)\kuda_buckets_eos.obj" \
	"$(INTDIR)\kuda_buckets_file.obj" \
	"$(INTDIR)\kuda_buckets_flush.obj" \
	"$(INTDIR)\kuda_buckets_heap.obj" \
	"$(INTDIR)\kuda_buckets_mmap.obj" \
	"$(INTDIR)\kuda_buckets_pipe.obj" \
	"$(INTDIR)\kuda_buckets_pool.obj" \
	"$(INTDIR)\kuda_buckets_refcount.obj" \
	"$(INTDIR)\kuda_buckets_simple.obj" \
	"$(INTDIR)\kuda_buckets_socket.obj" \
	"$(INTDIR)\kuda_crypto.obj" \
	"$(INTDIR)\kuda_md4.obj" \
	"$(INTDIR)\kuda_md5.obj" \
	"$(INTDIR)\kuda_passwd.obj" \
	"$(INTDIR)\kuda_sha1.obj" \
	"$(INTDIR)\kuda_siphash.obj" \
	"$(INTDIR)\crypt_blowfish.obj" \
	"$(INTDIR)\getuuid.obj" \
	"$(INTDIR)\uuid.obj" \
	"$(INTDIR)\kuda_dbd.obj" \
	"$(INTDIR)\kuda_dbm.obj" \
	"$(INTDIR)\kuda_dbm_sdbm.obj" \
	"$(INTDIR)\kuda_base64.obj" \
	"$(INTDIR)\kuda_hooks.obj" \
	"$(INTDIR)\kuda_ldap_stub.obj" \
	"$(INTDIR)\kuda_ldap_url.obj" \
	"$(INTDIR)\kuda_memcache.obj" \
	"$(INTDIR)\kuda_date.obj" \
	"$(INTDIR)\kudelman_dso.obj" \
	"$(INTDIR)\kuda_queue.obj" \
	"$(INTDIR)\kuda_redis.obj" \
	"$(INTDIR)\kuda_reslist.obj" \
	"$(INTDIR)\kuda_rmm.obj" \
	"$(INTDIR)\kuda_thread_pool.obj" \
	"$(INTDIR)\kudelman_version.obj" \
	"$(INTDIR)\sdbm.obj" \
	"$(INTDIR)\sdbm_hash.obj" \
	"$(INTDIR)\sdbm_lock.obj" \
	"$(INTDIR)\sdbm_pair.obj" \
	"$(INTDIR)\kuda_strmatch.obj" \
	"$(INTDIR)\kuda_uri.obj" \
	"$(INTDIR)\xlate.obj" \
	"$(INTDIR)\kuda_xml.obj" \
	"$(INTDIR)\libkudadelman.res" \
	"..\kuda\Release\libkuda-1.lib" \
	"..\kuda-iconv\Release\libkudaiconv-1.lib"

"$(OUTDIR)\libkudadelman-1.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\libkudadelman-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\libkudadelman-1.dll"
   if exist .\Release\libkudadelman-1.dll.manifest mt.exe -manifest .\Release\libkudadelman-1.dll.manifest -outputresource:.\Release\libkudadelman-1.dll;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : ".\include\private\kudelman_select_dbm.h" ".\include\private\kudelman_config.h" ".\include\kudelman_want.h" ".\include\kudelman.h" ".\include\kuda_ldap.h" "$(OUTDIR)\libkudadelman-1.dll" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "xml - Win32 Debug" "libkudaiconv - Win32 Debug" "libkuda - Win32 Debug" ".\include\private\kudelman_select_dbm.h" ".\include\private\kudelman_config.h" ".\include\kudelman_want.h" ".\include\kudelman.h" ".\include\kuda_ldap.h" "$(OUTDIR)\libkudadelman-1.dll" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudaiconv - Win32 DebugCLEAN" "xml - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\kuda_base64.obj"
	-@erase "$(INTDIR)\kuda_brigade.obj"
	-@erase "$(INTDIR)\kuda_buckets.obj"
	-@erase "$(INTDIR)\kuda_buckets_alloc.obj"
	-@erase "$(INTDIR)\kuda_buckets_eos.obj"
	-@erase "$(INTDIR)\kuda_buckets_file.obj"
	-@erase "$(INTDIR)\kuda_buckets_flush.obj"
	-@erase "$(INTDIR)\kuda_buckets_heap.obj"
	-@erase "$(INTDIR)\kuda_buckets_mmap.obj"
	-@erase "$(INTDIR)\kuda_buckets_pipe.obj"
	-@erase "$(INTDIR)\kuda_buckets_pool.obj"
	-@erase "$(INTDIR)\kuda_buckets_refcount.obj"
	-@erase "$(INTDIR)\kuda_buckets_simple.obj"
	-@erase "$(INTDIR)\kuda_buckets_socket.obj"
	-@erase "$(INTDIR)\kuda_crypto.obj"
	-@erase "$(INTDIR)\kuda_date.obj"
	-@erase "$(INTDIR)\kuda_dbd.obj"
	-@erase "$(INTDIR)\kuda_dbm.obj"
	-@erase "$(INTDIR)\kuda_dbm_sdbm.obj"
	-@erase "$(INTDIR)\kuda_hooks.obj"
	-@erase "$(INTDIR)\kuda_ldap_stub.obj"
	-@erase "$(INTDIR)\kuda_ldap_url.obj"
	-@erase "$(INTDIR)\kuda_md4.obj"
	-@erase "$(INTDIR)\kuda_md5.obj"
	-@erase "$(INTDIR)\kuda_memcache.obj"
	-@erase "$(INTDIR)\kuda_passwd.obj"
	-@erase "$(INTDIR)\kuda_queue.obj"
	-@erase "$(INTDIR)\kuda_redis.obj"
	-@erase "$(INTDIR)\kuda_reslist.obj"
	-@erase "$(INTDIR)\kuda_rmm.obj"
	-@erase "$(INTDIR)\kuda_sha1.obj"
	-@erase "$(INTDIR)\kuda_siphash.obj"
	-@erase "$(INTDIR)\kuda_strmatch.obj"
	-@erase "$(INTDIR)\kuda_thread_pool.obj"
	-@erase "$(INTDIR)\kuda_uri.obj"
	-@erase "$(INTDIR)\kuda_xml.obj"
	-@erase "$(INTDIR)\kudelman_dso.obj"
	-@erase "$(INTDIR)\kudelman_version.obj"
	-@erase "$(INTDIR)\crypt_blowfish.obj"
	-@erase "$(INTDIR)\getuuid.obj"
	-@erase "$(INTDIR)\libkudadelman.res"
	-@erase "$(INTDIR)\libkudadelman_src.idb"
	-@erase "$(INTDIR)\libkudadelman_src.pdb"
	-@erase "$(INTDIR)\sdbm.obj"
	-@erase "$(INTDIR)\sdbm_hash.obj"
	-@erase "$(INTDIR)\sdbm_lock.obj"
	-@erase "$(INTDIR)\sdbm_pair.obj"
	-@erase "$(INTDIR)\uuid.obj"
	-@erase "$(INTDIR)\xlate.obj"
	-@erase "$(OUTDIR)\libkudadelman-1.dll"
	-@erase "$(OUTDIR)\libkudadelman-1.exp"
	-@erase "$(OUTDIR)\libkudadelman-1.lib"
	-@erase "$(OUTDIR)\libkudadelman-1.pdb"
	-@erase ".\include\kuda_ldap.h"
	-@erase ".\include\kudelman.h"
	-@erase ".\include\kudelman_want.h"
	-@erase ".\include\private\kudelman_config.h"
	-@erase ".\include\private\kudelman_select_dbm.h"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "./include" /I "../kuda/include" /I "./include/private" /I "../kuda-iconv/include" /I "./dbm/sdbm" /I "./xml/expat/lib" $(SSLINC) /D "_DEBUG" /D "KUDA_DECLARE_EXPORT" /D "KUDELMAN_DECLARE_EXPORT" /D "KUDELMAN_USE_SDBM" $(XML_OPTIONS) /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\libkudadelman_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL" 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\libkudadelman.res" /i "./include" /i "../kuda/include" /d "_DEBUG" /d "KUDELMAN_VERSION_ONLY" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libkudadelman.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=$(XML_PARSER).lib kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib /nologo /base:"0x6EE60000" /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\libkudadelman-1.pdb" /debug /out:"$(OUTDIR)\libkudadelman-1.dll" /implib:"$(OUTDIR)\libkudadelman-1.lib"  
LINK32_OBJS= \
	"$(INTDIR)\kuda_brigade.obj" \
	"$(INTDIR)\kuda_buckets.obj" \
	"$(INTDIR)\kuda_buckets_alloc.obj" \
	"$(INTDIR)\kuda_buckets_eos.obj" \
	"$(INTDIR)\kuda_buckets_file.obj" \
	"$(INTDIR)\kuda_buckets_flush.obj" \
	"$(INTDIR)\kuda_buckets_heap.obj" \
	"$(INTDIR)\kuda_buckets_mmap.obj" \
	"$(INTDIR)\kuda_buckets_pipe.obj" \
	"$(INTDIR)\kuda_buckets_pool.obj" \
	"$(INTDIR)\kuda_buckets_refcount.obj" \
	"$(INTDIR)\kuda_buckets_simple.obj" \
	"$(INTDIR)\kuda_buckets_socket.obj" \
	"$(INTDIR)\kuda_crypto.obj" \
	"$(INTDIR)\kuda_md4.obj" \
	"$(INTDIR)\kuda_md5.obj" \
	"$(INTDIR)\kuda_passwd.obj" \
	"$(INTDIR)\kuda_sha1.obj" \
	"$(INTDIR)\kuda_siphash.obj" \
	"$(INTDIR)\crypt_blowfish.obj" \
	"$(INTDIR)\getuuid.obj" \
	"$(INTDIR)\uuid.obj" \
	"$(INTDIR)\kuda_dbd.obj" \
	"$(INTDIR)\kuda_dbm.obj" \
	"$(INTDIR)\kuda_dbm_sdbm.obj" \
	"$(INTDIR)\kuda_base64.obj" \
	"$(INTDIR)\kuda_hooks.obj" \
	"$(INTDIR)\kuda_ldap_stub.obj" \
	"$(INTDIR)\kuda_ldap_url.obj" \
	"$(INTDIR)\kuda_memcache.obj" \
	"$(INTDIR)\kuda_date.obj" \
	"$(INTDIR)\kudelman_dso.obj" \
	"$(INTDIR)\kuda_queue.obj" \
	"$(INTDIR)\kuda_redis.obj" \
	"$(INTDIR)\kuda_reslist.obj" \
	"$(INTDIR)\kuda_rmm.obj" \
	"$(INTDIR)\kuda_thread_pool.obj" \
	"$(INTDIR)\kudelman_version.obj" \
	"$(INTDIR)\sdbm.obj" \
	"$(INTDIR)\sdbm_hash.obj" \
	"$(INTDIR)\sdbm_lock.obj" \
	"$(INTDIR)\sdbm_pair.obj" \
	"$(INTDIR)\kuda_strmatch.obj" \
	"$(INTDIR)\kuda_uri.obj" \
	"$(INTDIR)\xlate.obj" \
	"$(INTDIR)\kuda_xml.obj" \
	"$(INTDIR)\libkudadelman.res" \
	"..\kuda\Debug\libkuda-1.lib" \
	"..\kuda-iconv\Debug\libkudaiconv-1.lib"

"$(OUTDIR)\libkudadelman-1.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\libkudadelman-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\libkudadelman-1.dll"
   if exist .\Debug\libkudadelman-1.dll.manifest mt.exe -manifest .\Debug\libkudadelman-1.dll.manifest -outputresource:.\Debug\libkudadelman-1.dll;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

OUTDIR=.\x64\Release
INTDIR=.\x64\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\x64\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : ".\include\private\kudelman_select_dbm.h" ".\include\private\kudelman_config.h" ".\include\kudelman_want.h" ".\include\kudelman.h" ".\include\kuda_ldap.h" "$(OUTDIR)\libkudadelman-1.dll" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "xml - x64 Release" "libkudaiconv - x64 Release" "libkuda - x64 Release" ".\include\private\kudelman_select_dbm.h" ".\include\private\kudelman_config.h" ".\include\kudelman_want.h" ".\include\kudelman.h" ".\include\kuda_ldap.h" "$(OUTDIR)\libkudadelman-1.dll" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - x64 ReleaseCLEAN" "libkudaiconv - x64 ReleaseCLEAN" "xml - x64 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\kuda_base64.obj"
	-@erase "$(INTDIR)\kuda_brigade.obj"
	-@erase "$(INTDIR)\kuda_buckets.obj"
	-@erase "$(INTDIR)\kuda_buckets_alloc.obj"
	-@erase "$(INTDIR)\kuda_buckets_eos.obj"
	-@erase "$(INTDIR)\kuda_buckets_file.obj"
	-@erase "$(INTDIR)\kuda_buckets_flush.obj"
	-@erase "$(INTDIR)\kuda_buckets_heap.obj"
	-@erase "$(INTDIR)\kuda_buckets_mmap.obj"
	-@erase "$(INTDIR)\kuda_buckets_pipe.obj"
	-@erase "$(INTDIR)\kuda_buckets_pool.obj"
	-@erase "$(INTDIR)\kuda_buckets_refcount.obj"
	-@erase "$(INTDIR)\kuda_buckets_simple.obj"
	-@erase "$(INTDIR)\kuda_buckets_socket.obj"
	-@erase "$(INTDIR)\kuda_crypto.obj"
	-@erase "$(INTDIR)\kuda_date.obj"
	-@erase "$(INTDIR)\kuda_dbd.obj"
	-@erase "$(INTDIR)\kuda_dbm.obj"
	-@erase "$(INTDIR)\kuda_dbm_sdbm.obj"
	-@erase "$(INTDIR)\kuda_hooks.obj"
	-@erase "$(INTDIR)\kuda_ldap_stub.obj"
	-@erase "$(INTDIR)\kuda_ldap_url.obj"
	-@erase "$(INTDIR)\kuda_md4.obj"
	-@erase "$(INTDIR)\kuda_md5.obj"
	-@erase "$(INTDIR)\kuda_memcache.obj"
	-@erase "$(INTDIR)\kuda_passwd.obj"
	-@erase "$(INTDIR)\kuda_queue.obj"
	-@erase "$(INTDIR)\kuda_redis.obj"
	-@erase "$(INTDIR)\kuda_reslist.obj"
	-@erase "$(INTDIR)\kuda_rmm.obj"
	-@erase "$(INTDIR)\kuda_sha1.obj"
	-@erase "$(INTDIR)\kuda_siphash.obj"
	-@erase "$(INTDIR)\kuda_strmatch.obj"
	-@erase "$(INTDIR)\kuda_thread_pool.obj"
	-@erase "$(INTDIR)\kuda_uri.obj"
	-@erase "$(INTDIR)\kuda_xml.obj"
	-@erase "$(INTDIR)\kudelman_dso.obj"
	-@erase "$(INTDIR)\kudelman_version.obj"
	-@erase "$(INTDIR)\crypt_blowfish.obj"
	-@erase "$(INTDIR)\getuuid.obj"
	-@erase "$(INTDIR)\libkudadelman.res"
	-@erase "$(INTDIR)\libkudadelman_src.idb"
	-@erase "$(INTDIR)\libkudadelman_src.pdb"
	-@erase "$(INTDIR)\sdbm.obj"
	-@erase "$(INTDIR)\sdbm_hash.obj"
	-@erase "$(INTDIR)\sdbm_lock.obj"
	-@erase "$(INTDIR)\sdbm_pair.obj"
	-@erase "$(INTDIR)\uuid.obj"
	-@erase "$(INTDIR)\xlate.obj"
	-@erase "$(OUTDIR)\libkudadelman-1.dll"
	-@erase "$(OUTDIR)\libkudadelman-1.exp"
	-@erase "$(OUTDIR)\libkudadelman-1.lib"
	-@erase "$(OUTDIR)\libkudadelman-1.pdb"
	-@erase ".\include\kuda_ldap.h"
	-@erase ".\include\kudelman.h"
	-@erase ".\include\kudelman_want.h"
	-@erase ".\include\private\kudelman_config.h"
	-@erase ".\include\private\kudelman_select_dbm.h"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "./include" /I "../kuda/include" /I "./include/private" /I "../kuda-iconv/include" /I "./dbm/sdbm" /I "./xml/expat/lib" $(SSLINC) /D "NDEBUG" /D "KUDA_DECLARE_EXPORT" /D "KUDELMAN_DECLARE_EXPORT" /D "KUDELMAN_USE_SDBM" $(XML_OPTIONS) /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\libkudadelman_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL" 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\libkudadelman.res" /i "./include" /i "../kuda/include" /d "NDEBUG" /d "KUDELMAN_VERSION_ONLY" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libkudadelman.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=$(XML_PARSER).lib kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib /nologo /base:"0x6EE60000" /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\libkudadelman-1.pdb" /debug /out:"$(OUTDIR)\libkudadelman-1.dll" /implib:"$(OUTDIR)\libkudadelman-1.lib"  /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\kuda_brigade.obj" \
	"$(INTDIR)\kuda_buckets.obj" \
	"$(INTDIR)\kuda_buckets_alloc.obj" \
	"$(INTDIR)\kuda_buckets_eos.obj" \
	"$(INTDIR)\kuda_buckets_file.obj" \
	"$(INTDIR)\kuda_buckets_flush.obj" \
	"$(INTDIR)\kuda_buckets_heap.obj" \
	"$(INTDIR)\kuda_buckets_mmap.obj" \
	"$(INTDIR)\kuda_buckets_pipe.obj" \
	"$(INTDIR)\kuda_buckets_pool.obj" \
	"$(INTDIR)\kuda_buckets_refcount.obj" \
	"$(INTDIR)\kuda_buckets_simple.obj" \
	"$(INTDIR)\kuda_buckets_socket.obj" \
	"$(INTDIR)\kuda_crypto.obj" \
	"$(INTDIR)\kuda_md4.obj" \
	"$(INTDIR)\kuda_md5.obj" \
	"$(INTDIR)\kuda_passwd.obj" \
	"$(INTDIR)\kuda_sha1.obj" \
	"$(INTDIR)\kuda_siphash.obj" \
	"$(INTDIR)\crypt_blowfish.obj" \
	"$(INTDIR)\getuuid.obj" \
	"$(INTDIR)\uuid.obj" \
	"$(INTDIR)\kuda_dbd.obj" \
	"$(INTDIR)\kuda_dbm.obj" \
	"$(INTDIR)\kuda_dbm_sdbm.obj" \
	"$(INTDIR)\kuda_base64.obj" \
	"$(INTDIR)\kuda_hooks.obj" \
	"$(INTDIR)\kuda_ldap_stub.obj" \
	"$(INTDIR)\kuda_ldap_url.obj" \
	"$(INTDIR)\kuda_memcache.obj" \
	"$(INTDIR)\kuda_date.obj" \
	"$(INTDIR)\kudelman_dso.obj" \
	"$(INTDIR)\kuda_queue.obj" \
	"$(INTDIR)\kuda_redis.obj" \
	"$(INTDIR)\kuda_reslist.obj" \
	"$(INTDIR)\kuda_rmm.obj" \
	"$(INTDIR)\kuda_thread_pool.obj" \
	"$(INTDIR)\kudelman_version.obj" \
	"$(INTDIR)\sdbm.obj" \
	"$(INTDIR)\sdbm_hash.obj" \
	"$(INTDIR)\sdbm_lock.obj" \
	"$(INTDIR)\sdbm_pair.obj" \
	"$(INTDIR)\kuda_strmatch.obj" \
	"$(INTDIR)\kuda_uri.obj" \
	"$(INTDIR)\xlate.obj" \
	"$(INTDIR)\kuda_xml.obj" \
	"$(INTDIR)\libkudadelman.res" \
	"..\kuda\x64\Release\libkuda-1.lib" \
	"..\kuda-iconv\x64\Release\libkudaiconv-1.lib"

"$(OUTDIR)\libkudadelman-1.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\x64\Release\libkudadelman-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\x64\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\libkudadelman-1.dll"
   if exist .\x64\Release\libkudadelman-1.dll.manifest mt.exe -manifest .\x64\Release\libkudadelman-1.dll.manifest -outputresource:.\x64\Release\libkudadelman-1.dll;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

OUTDIR=.\x64\Debug
INTDIR=.\x64\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\x64\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : ".\include\private\kudelman_select_dbm.h" ".\include\private\kudelman_config.h" ".\include\kudelman_want.h" ".\include\kudelman.h" ".\include\kuda_ldap.h" "$(OUTDIR)\libkudadelman-1.dll" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "xml - x64 Debug" "libkudaiconv - x64 Debug" "libkuda - x64 Debug" ".\include\private\kudelman_select_dbm.h" ".\include\private\kudelman_config.h" ".\include\kudelman_want.h" ".\include\kudelman.h" ".\include\kuda_ldap.h" "$(OUTDIR)\libkudadelman-1.dll" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - x64 DebugCLEAN" "libkudaiconv - x64 DebugCLEAN" "xml - x64 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\kuda_base64.obj"
	-@erase "$(INTDIR)\kuda_brigade.obj"
	-@erase "$(INTDIR)\kuda_buckets.obj"
	-@erase "$(INTDIR)\kuda_buckets_alloc.obj"
	-@erase "$(INTDIR)\kuda_buckets_eos.obj"
	-@erase "$(INTDIR)\kuda_buckets_file.obj"
	-@erase "$(INTDIR)\kuda_buckets_flush.obj"
	-@erase "$(INTDIR)\kuda_buckets_heap.obj"
	-@erase "$(INTDIR)\kuda_buckets_mmap.obj"
	-@erase "$(INTDIR)\kuda_buckets_pipe.obj"
	-@erase "$(INTDIR)\kuda_buckets_pool.obj"
	-@erase "$(INTDIR)\kuda_buckets_refcount.obj"
	-@erase "$(INTDIR)\kuda_buckets_simple.obj"
	-@erase "$(INTDIR)\kuda_buckets_socket.obj"
	-@erase "$(INTDIR)\kuda_crypto.obj"
	-@erase "$(INTDIR)\kuda_date.obj"
	-@erase "$(INTDIR)\kuda_dbd.obj"
	-@erase "$(INTDIR)\kuda_dbm.obj"
	-@erase "$(INTDIR)\kuda_dbm_sdbm.obj"
	-@erase "$(INTDIR)\kuda_hooks.obj"
	-@erase "$(INTDIR)\kuda_ldap_stub.obj"
	-@erase "$(INTDIR)\kuda_ldap_url.obj"
	-@erase "$(INTDIR)\kuda_md4.obj"
	-@erase "$(INTDIR)\kuda_md5.obj"
	-@erase "$(INTDIR)\kuda_memcache.obj"
	-@erase "$(INTDIR)\kuda_passwd.obj"
	-@erase "$(INTDIR)\kuda_queue.obj"
	-@erase "$(INTDIR)\kuda_redis.obj"
	-@erase "$(INTDIR)\kuda_reslist.obj"
	-@erase "$(INTDIR)\kuda_rmm.obj"
	-@erase "$(INTDIR)\kuda_sha1.obj"
	-@erase "$(INTDIR)\kuda_siphash.obj"
	-@erase "$(INTDIR)\kuda_strmatch.obj"
	-@erase "$(INTDIR)\kuda_thread_pool.obj"
	-@erase "$(INTDIR)\kuda_uri.obj"
	-@erase "$(INTDIR)\kuda_xml.obj"
	-@erase "$(INTDIR)\kudelman_dso.obj"
	-@erase "$(INTDIR)\kudelman_version.obj"
	-@erase "$(INTDIR)\crypt_blowfish.obj"
	-@erase "$(INTDIR)\getuuid.obj"
	-@erase "$(INTDIR)\libkudadelman.res"
	-@erase "$(INTDIR)\libkudadelman_src.idb"
	-@erase "$(INTDIR)\libkudadelman_src.pdb"
	-@erase "$(INTDIR)\sdbm.obj"
	-@erase "$(INTDIR)\sdbm_hash.obj"
	-@erase "$(INTDIR)\sdbm_lock.obj"
	-@erase "$(INTDIR)\sdbm_pair.obj"
	-@erase "$(INTDIR)\uuid.obj"
	-@erase "$(INTDIR)\xlate.obj"
	-@erase "$(OUTDIR)\libkudadelman-1.dll"
	-@erase "$(OUTDIR)\libkudadelman-1.exp"
	-@erase "$(OUTDIR)\libkudadelman-1.lib"
	-@erase "$(OUTDIR)\libkudadelman-1.pdb"
	-@erase ".\include\kuda_ldap.h"
	-@erase ".\include\kudelman.h"
	-@erase ".\include\kudelman_want.h"
	-@erase ".\include\private\kudelman_config.h"
	-@erase ".\include\private\kudelman_select_dbm.h"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "./include" /I "../kuda/include" /I "./include/private" /I "../kuda-iconv/include" /I "./dbm/sdbm" /I "./xml/expat/lib" $(SSLINC) /D "_DEBUG" /D "KUDA_DECLARE_EXPORT" /D "KUDELMAN_DECLARE_EXPORT" /D "KUDELMAN_USE_SDBM" $(XML_OPTIONS) /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\libkudadelman_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL" 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\libkudadelman.res" /i "./include" /i "../kuda/include" /d "_DEBUG" /d "KUDELMAN_VERSION_ONLY" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libkudadelman.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=$(XML_PARSER).lib kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib /nologo /base:"0x6EE60000" /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\libkudadelman-1.pdb" /debug /out:"$(OUTDIR)\libkudadelman-1.dll" /implib:"$(OUTDIR)\libkudadelman-1.lib"  
LINK32_OBJS= \
	"$(INTDIR)\kuda_brigade.obj" \
	"$(INTDIR)\kuda_buckets.obj" \
	"$(INTDIR)\kuda_buckets_alloc.obj" \
	"$(INTDIR)\kuda_buckets_eos.obj" \
	"$(INTDIR)\kuda_buckets_file.obj" \
	"$(INTDIR)\kuda_buckets_flush.obj" \
	"$(INTDIR)\kuda_buckets_heap.obj" \
	"$(INTDIR)\kuda_buckets_mmap.obj" \
	"$(INTDIR)\kuda_buckets_pipe.obj" \
	"$(INTDIR)\kuda_buckets_pool.obj" \
	"$(INTDIR)\kuda_buckets_refcount.obj" \
	"$(INTDIR)\kuda_buckets_simple.obj" \
	"$(INTDIR)\kuda_buckets_socket.obj" \
	"$(INTDIR)\kuda_crypto.obj" \
	"$(INTDIR)\kuda_md4.obj" \
	"$(INTDIR)\kuda_md5.obj" \
	"$(INTDIR)\kuda_passwd.obj" \
	"$(INTDIR)\kuda_sha1.obj" \
	"$(INTDIR)\kuda_siphash.obj" \
	"$(INTDIR)\crypt_blowfish.obj" \
	"$(INTDIR)\getuuid.obj" \
	"$(INTDIR)\uuid.obj" \
	"$(INTDIR)\kuda_dbd.obj" \
	"$(INTDIR)\kuda_dbm.obj" \
	"$(INTDIR)\kuda_dbm_sdbm.obj" \
	"$(INTDIR)\kuda_base64.obj" \
	"$(INTDIR)\kuda_hooks.obj" \
	"$(INTDIR)\kuda_ldap_stub.obj" \
	"$(INTDIR)\kuda_ldap_url.obj" \
	"$(INTDIR)\kuda_memcache.obj" \
	"$(INTDIR)\kuda_date.obj" \
	"$(INTDIR)\kudelman_dso.obj" \
	"$(INTDIR)\kuda_queue.obj" \
	"$(INTDIR)\kuda_redis.obj" \
	"$(INTDIR)\kuda_reslist.obj" \
	"$(INTDIR)\kuda_rmm.obj" \
	"$(INTDIR)\kuda_thread_pool.obj" \
	"$(INTDIR)\kudelman_version.obj" \
	"$(INTDIR)\sdbm.obj" \
	"$(INTDIR)\sdbm_hash.obj" \
	"$(INTDIR)\sdbm_lock.obj" \
	"$(INTDIR)\sdbm_pair.obj" \
	"$(INTDIR)\kuda_strmatch.obj" \
	"$(INTDIR)\kuda_uri.obj" \
	"$(INTDIR)\xlate.obj" \
	"$(INTDIR)\kuda_xml.obj" \
	"$(INTDIR)\libkudadelman.res" \
	"..\kuda\x64\Debug\libkuda-1.lib" \
	"..\kuda-iconv\x64\Debug\libkudaiconv-1.lib"

"$(OUTDIR)\libkudadelman-1.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\x64\Debug\libkudadelman-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\x64\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\libkudadelman-1.dll"
   if exist .\x64\Debug\libkudadelman-1.dll.manifest mt.exe -manifest .\x64\Debug\libkudadelman-1.dll.manifest -outputresource:.\x64\Debug\libkudadelman-1.dll;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("libkudadelman.dep")
!INCLUDE "libkudadelman.dep"
!ELSE 
!MESSAGE Warning: cannot find "libkudadelman.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "libkudadelman - Win32 Release" || "$(CFG)" == "libkudadelman - Win32 Debug" || "$(CFG)" == "libkudadelman - x64 Release" || "$(CFG)" == "libkudadelman - x64 Debug"
SOURCE=.\buckets\kuda_brigade.c

"$(INTDIR)\kuda_brigade.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets.c

"$(INTDIR)\kuda_buckets.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_alloc.c

"$(INTDIR)\kuda_buckets_alloc.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_eos.c

"$(INTDIR)\kuda_buckets_eos.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_file.c

"$(INTDIR)\kuda_buckets_file.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_flush.c

"$(INTDIR)\kuda_buckets_flush.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_heap.c

"$(INTDIR)\kuda_buckets_heap.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_mmap.c

"$(INTDIR)\kuda_buckets_mmap.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_pipe.c

"$(INTDIR)\kuda_buckets_pipe.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_pool.c

"$(INTDIR)\kuda_buckets_pool.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_refcount.c

"$(INTDIR)\kuda_buckets_refcount.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_simple.c

"$(INTDIR)\kuda_buckets_simple.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_socket.c

"$(INTDIR)\kuda_buckets_socket.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\kuda_crypto.c

"$(INTDIR)\kuda_crypto.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\kuda_md4.c

"$(INTDIR)\kuda_md4.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\kuda_md5.c

"$(INTDIR)\kuda_md5.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\kuda_passwd.c

"$(INTDIR)\kuda_passwd.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\kuda_sha1.c

"$(INTDIR)\kuda_sha1.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\kuda_siphash.c

"$(INTDIR)\kuda_siphash.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\crypt_blowfish.c

"$(INTDIR)\crypt_blowfish.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\getuuid.c

"$(INTDIR)\getuuid.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\uuid.c

"$(INTDIR)\uuid.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbd\kuda_dbd.c

"$(INTDIR)\kuda_dbd.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbd\kuda_dbd_mysql.c
SOURCE=.\dbd\kuda_dbd_odbc.c
SOURCE=.\dbd\kuda_dbd_oracle.c
SOURCE=.\dbd\kuda_dbd_pgsql.c
SOURCE=.\dbd\kuda_dbd_sqlite2.c
SOURCE=.\dbd\kuda_dbd_sqlite3.c
SOURCE=.\dbm\kuda_dbm.c

"$(INTDIR)\kuda_dbm.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h" ".\include\private\kudelman_select_dbm.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbm\kuda_dbm_berkeleydb.c
SOURCE=.\dbm\kuda_dbm_gdbm.c
SOURCE=.\dbm\kuda_dbm_sdbm.c

"$(INTDIR)\kuda_dbm_sdbm.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h" ".\include\private\kudelman_select_dbm.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\encoding\kuda_base64.c

"$(INTDIR)\kuda_base64.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\hooks\kuda_hooks.c

"$(INTDIR)\kuda_hooks.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\ldap\kuda_ldap_init.c
SOURCE=.\ldap\kuda_ldap_option.c
SOURCE=.\ldap\kuda_ldap_rebind.c
SOURCE=.\ldap\kuda_ldap_stub.c

"$(INTDIR)\kuda_ldap_stub.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h" ".\include\private\kudelman_config.h" ".\include\kuda_ldap.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\ldap\kuda_ldap_url.c

"$(INTDIR)\kuda_ldap_url.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h" ".\include\kuda_ldap.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\memcache\kuda_memcache.c

"$(INTDIR)\kuda_memcache.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kuda_date.c

"$(INTDIR)\kuda_date.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kuda_queue.c

"$(INTDIR)\kuda_queue.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kuda_reslist.c

"$(INTDIR)\kuda_reslist.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kuda_rmm.c

"$(INTDIR)\kuda_rmm.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kuda_thread_pool.c

"$(INTDIR)\kuda_thread_pool.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kudelman_dso.c

"$(INTDIR)\kudelman_dso.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kudelman_version.c

"$(INTDIR)\kudelman_version.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\redis\kuda_redis.c

"$(INTDIR)\kuda_redis.obj" : $(SOURCE) "$(INTDIR)" ".\include\kuda_redis.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbm\sdbm\sdbm.c

"$(INTDIR)\sdbm.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbm\sdbm\sdbm_hash.c

"$(INTDIR)\sdbm_hash.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbm\sdbm\sdbm_lock.c

"$(INTDIR)\sdbm_lock.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbm\sdbm\sdbm_pair.c

"$(INTDIR)\sdbm_pair.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\strmatch\kuda_strmatch.c

"$(INTDIR)\kuda_strmatch.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\uri\kuda_uri.c

"$(INTDIR)\kuda_uri.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\xlate\xlate.c

"$(INTDIR)\xlate.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h" ".\include\private\kudelman_config.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\xml\kuda_xml.c

"$(INTDIR)\kuda_xml.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\include\kuda_ldap.hw

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

InputPath=.\include\kuda_ldap.hw

".\include\kuda_ldap.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kuda_ldap.hw > .\include\kuda_ldap.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

InputPath=.\include\kuda_ldap.hw

".\include\kuda_ldap.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kuda_ldap.hw > .\include\kuda_ldap.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

InputPath=.\include\kuda_ldap.hw

".\include\kuda_ldap.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kuda_ldap.hw > .\include\kuda_ldap.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

InputPath=.\include\kuda_ldap.hw

".\include\kuda_ldap.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kuda_ldap.hw > .\include\kuda_ldap.h
<< 
	

!ENDIF 

SOURCE=.\include\kudelman.hw

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

InputPath=.\include\kudelman.hw

".\include\kudelman.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman.hw > .\include\kudelman.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

InputPath=.\include\kudelman.hw

".\include\kudelman.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman.hw > .\include\kudelman.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

InputPath=.\include\kudelman.hw

".\include\kudelman.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman.hw > .\include\kudelman.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

InputPath=.\include\kudelman.hw

".\include\kudelman.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman.hw > .\include\kudelman.h
<< 
	

!ENDIF 

SOURCE=.\include\private\kudelman_config.hw

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

InputPath=.\include\private\kudelman_config.hw

".\include\private\kudelman_config.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_config.hw > .\include\private\kudelman_config.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

InputPath=.\include\private\kudelman_config.hw

".\include\private\kudelman_config.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_config.hw > .\include\private\kudelman_config.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

InputPath=.\include\private\kudelman_config.hw

".\include\private\kudelman_config.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_config.hw > .\include\private\kudelman_config.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

InputPath=.\include\private\kudelman_config.hw

".\include\private\kudelman_config.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_config.hw > .\include\private\kudelman_config.h
<< 
	

!ENDIF 

SOURCE=.\include\private\kudelman_select_dbm.hw

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

InputPath=.\include\private\kudelman_select_dbm.hw

".\include\private\kudelman_select_dbm.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_select_dbm.hw > .\include\private\kudelman_select_dbm.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

InputPath=.\include\private\kudelman_select_dbm.hw

".\include\private\kudelman_select_dbm.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_select_dbm.hw > .\include\private\kudelman_select_dbm.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

InputPath=.\include\private\kudelman_select_dbm.hw

".\include\private\kudelman_select_dbm.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_select_dbm.hw > .\include\private\kudelman_select_dbm.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

InputPath=.\include\private\kudelman_select_dbm.hw

".\include\private\kudelman_select_dbm.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_select_dbm.hw > .\include\private\kudelman_select_dbm.h
<< 
	

!ENDIF 

SOURCE=.\include\kudelman_want.hw

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

InputPath=.\include\kudelman_want.hw

".\include\kudelman_want.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman_want.hw > .\include\kudelman_want.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

InputPath=.\include\kudelman_want.hw

".\include\kudelman_want.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman_want.hw > .\include\kudelman_want.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

InputPath=.\include\kudelman_want.hw

".\include\kudelman_want.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman_want.hw > .\include\kudelman_want.h
<< 
	

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

InputPath=.\include\kudelman_want.hw

".\include\kudelman_want.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman_want.hw > .\include\kudelman_want.h
<< 
	

!ENDIF 

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

"libkuda - Win32 Release" : 
   cd ".\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\kuda-delman"

"libkuda - Win32 ReleaseCLEAN" : 
   cd ".\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\kuda-delman"

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd ".\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\kuda-delman"

"libkuda - Win32 DebugCLEAN" : 
   cd ".\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\kuda-delman"

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

"libkuda - x64 Release" : 
   cd ".\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - x64 Release" 
   cd "..\kuda-delman"

"libkuda - x64 ReleaseCLEAN" : 
   cd ".\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - x64 Release" RECURSE=1 CLEAN 
   cd "..\kuda-delman"

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

"libkuda - x64 Debug" : 
   cd ".\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - x64 Debug" 
   cd "..\kuda-delman"

"libkuda - x64 DebugCLEAN" : 
   cd ".\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - x64 Debug" RECURSE=1 CLEAN 
   cd "..\kuda-delman"

!ENDIF 

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

"libkudaiconv - Win32 Release" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudaiconv.mak" CFG="libkudaiconv - Win32 Release" 
   cd "..\kuda-delman"

"libkudaiconv - Win32 ReleaseCLEAN" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudaiconv.mak" CFG="libkudaiconv - Win32 Release" RECURSE=1 CLEAN 
   cd "..\kuda-delman"

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

"libkudaiconv - Win32 Debug" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudaiconv.mak" CFG="libkudaiconv - Win32 Debug" 
   cd "..\kuda-delman"

"libkudaiconv - Win32 DebugCLEAN" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudaiconv.mak" CFG="libkudaiconv - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\kuda-delman"

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

"libkudaiconv - x64 Release" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudaiconv.mak" CFG="libkudaiconv - x64 Release" 
   cd "..\kuda-delman"

"libkudaiconv - x64 ReleaseCLEAN" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudaiconv.mak" CFG="libkudaiconv - x64 Release" RECURSE=1 CLEAN 
   cd "..\kuda-delman"

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

"libkudaiconv - x64 Debug" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudaiconv.mak" CFG="libkudaiconv - x64 Debug" 
   cd "..\kuda-delman"

"libkudaiconv - x64 DebugCLEAN" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudaiconv.mak" CFG="libkudaiconv - x64 Debug" RECURSE=1 CLEAN 
   cd "..\kuda-delman"

!ENDIF 

SOURCE=.\libkudadelman.rc

"$(INTDIR)\libkudadelman.res" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(RSC) $(RSC_PROJ) $(SOURCE)



!ENDIF 

