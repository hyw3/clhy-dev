/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ctype.h>
#include <stdio.h>

#include "kudelman_config.h"
#include "kudelman.h"
#include "kuda_pools.h"
#include "kuda_dso.h"
#include "kuda_strings.h"
#include "kuda_hash.h"
#include "kuda_thread_mutex.h"
#include "kuda_lib.h"

#if KUDELMAN_HAVE_CRYPTO

#include "kudelman_internal.h"
#include "kuda_crypto_internal.h"
#include "kuda_crypto.h"
#include "kudelman_version.h"

static kuda_hash_t *drivers = NULL;

#define ERROR_SIZE 1024

#define CLEANUP_CAST (kuda_status_t (*)(void*))

#define KUDA_TYPEDEF_STRUCT(type, incompletion) \
struct type { \
   incompletion \
   void *unk[]; \
};

KUDA_TYPEDEF_STRUCT(kuda_crypto_t,
    kuda_pool_t *pool;
    kuda_crypto_driver_t *provider;
)

KUDA_TYPEDEF_STRUCT(kuda_crypto_key_t,
    kuda_pool_t *pool;
    kuda_crypto_driver_t *provider;
    const kuda_crypto_t *f;
)

KUDA_TYPEDEF_STRUCT(kuda_crypto_block_t,
    kuda_pool_t *pool;
    kuda_crypto_driver_t *provider;
    const kuda_crypto_t *f;
)

typedef struct kuda_crypto_clear_t {
    void *buffer;
    kuda_size_t size;
} kuda_crypto_clear_t;

#if !KUDELMAN_DSO_BUILD
#define DRIVER_LOAD(name,driver_name,pool,params,rv,result) \
    {   \
        extern const kuda_crypto_driver_t driver_name; \
        kuda_hash_set(drivers,name,KUDA_HASH_KEY_STRING,&driver_name); \
        if (driver_name.init) {     \
            rv = driver_name.init(pool, params, result); \
        }  \
        *driver = &driver_name; \
    }
#endif

static kuda_status_t kuda_crypto_term(void *ptr)
{
    /* set drivers to NULL so init can work again */
    drivers = NULL;

    /* Everything else we need is handled by cleanups registered
     * when we created mutexes and loaded DSOs
     */
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_init(kuda_pool_t *pool)
{
    kuda_status_t ret = KUDA_SUCCESS;
    kuda_pool_t *parent;

    if (drivers != NULL) {
        return KUDA_SUCCESS;
    }

    /* Top level pool scope, need process-scope lifetime */
    for (parent = kuda_pool_parent_get(pool);
         parent && parent != pool;
         parent = kuda_pool_parent_get(pool))
        pool = parent;
#if KUDELMAN_DSO_BUILD
    /* deprecate in 2.0 - permit implicit initialization */
    kudelman_dso_init(pool);
#endif
    drivers = kuda_hash_make(pool);

    kuda_pool_cleanup_register(pool, NULL, kuda_crypto_term,
            kuda_pool_cleanup_null);

    return ret;
}

static kuda_status_t crypto_clear(void *ptr)
{
    kuda_crypto_clear_t *clear = (kuda_crypto_clear_t *)ptr;

    kuda_crypto_memzero(clear->buffer, clear->size);
    clear->buffer = NULL;
    clear->size = 0;

    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_clear(kuda_pool_t *pool,
        void *buffer, kuda_size_t size)
{
    kuda_crypto_clear_t *clear = kuda_palloc(pool, sizeof(kuda_crypto_clear_t));

    clear->buffer = buffer;
    clear->size = size;

    kuda_pool_cleanup_register(pool, clear, crypto_clear,
            kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}

#if defined(HAVE_WEAK_SYMBOLS)
void kuda__memzero_explicit(void *buffer, kuda_size_t size);

__attribute__ ((weak))
void kuda__memzero_explicit(void *buffer, kuda_size_t size)
{
    memset(buffer, 0, size);
}
#endif

KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_memzero(void *buffer, kuda_size_t size)
{
#if defined(WIN32)
    SecureZeroMemory(buffer, size);
#elif defined(HAVE_MEMSET_S)
    if (size) {
        return memset_s(buffer, (rsize_t)size, 0, (rsize_t)size);
    }
#elif defined(HAVE_EXPLICIT_BZERO)
    explicit_bzero(buffer, size);
#elif defined(HAVE_WEAK_SYMBOLS)
    kuda__memzero_explicit(buffer, size);
#else
    kuda_size_t i;
    volatile unsigned char *volatile ptr = buffer;
    for (i = 0; i < size; ++i) {
        ptr[i] = 0;
    }
#endif
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(int) kuda_crypto_equals(const void *buf1, const void *buf2,
                                   kuda_size_t size)
{
    const unsigned char *p1 = buf1;
    const unsigned char *p2 = buf2;
    unsigned char diff = 0;
    kuda_size_t i;

    for (i = 0; i < size; ++i) {
        diff |= p1[i] ^ p2[i];
    }

    return 1 & ((diff - 1) >> 8);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_get_driver(
        const kuda_crypto_driver_t **driver, const char *name,
        const char *params, const kudelman_err_t **result, kuda_pool_t *pool)
{
#if KUDELMAN_DSO_BUILD
    char capiname[32];
    char symname[34];
    kuda_dso_handle_t *dso;
    kuda_dso_handle_sym_t symbol;
#endif
    kuda_status_t rv;

    if (result) {
        *result = NULL; /* until further notice */
    }

#if KUDELMAN_DSO_BUILD
    rv = kudelman_dso_mutex_lock();
    if (rv) {
        return rv;
    }
#endif
    *driver = kuda_hash_get(drivers, name, KUDA_HASH_KEY_STRING);
    if (*driver) {
#if KUDELMAN_DSO_BUILD 
        kudelman_dso_mutex_unlock();
#endif
        return KUDA_SUCCESS;
    }

#if KUDELMAN_DSO_BUILD
    /* The driver DSO must have exactly the same lifetime as the
     * drivers hash table; ignore the passed-in pool */
    pool = kuda_hash_pool_get(drivers);

#if defined(NETWARE)
    kuda_snprintf(capiname, sizeof(capiname), "crypto%s.nlm", name);
#elif defined(WIN32) || defined(__CYGWIN__)
    kuda_snprintf(capiname, sizeof(capiname),
            "kuda_crypto_%s-" KUDELMAN_STRINGIFY(KUDELMAN_MAJOR_VERSION) ".dll", name);
#else
    kuda_snprintf(capiname, sizeof(capiname),
            "kuda_crypto_%s-" KUDELMAN_STRINGIFY(KUDELMAN_MAJOR_VERSION) ".so", name);
#endif
    kuda_snprintf(symname, sizeof(symname), "kuda_crypto_%s_driver", name);
    rv = kudelman_dso_load(&dso, &symbol, capiname, symname, pool);
    if (rv == KUDA_SUCCESS || rv == KUDA_EINIT) { /* previously loaded?!? */
        kuda_crypto_driver_t *d = symbol;
        rv = KUDA_SUCCESS;
        if (d->init) {
            rv = d->init(pool, params, result);
        }
        if (KUDA_SUCCESS == rv) {
            *driver = symbol;
            name = kuda_pstrdup(pool, name);
            kuda_hash_set(drivers, name, KUDA_HASH_KEY_STRING, *driver);
        }
    }
    kudelman_dso_mutex_unlock();

    if (KUDA_SUCCESS != rv && result && !*result) {
        char *buffer = kuda_pcalloc(pool, ERROR_SIZE);
        kudelman_err_t *err = kuda_pcalloc(pool, sizeof(kudelman_err_t));
        if (err && buffer) {
            kuda_dso_error(dso, buffer, ERROR_SIZE - 1);
            err->msg = buffer;
            err->reason = kuda_pstrdup(pool, capiname);
            *result = err;
        }
    }

#else /* not builtin and !KUDA_HAS_DSO => not implemented */
    rv = KUDA_ENOTIMPL;

    /* Load statically-linked drivers: */
#if KUDELMAN_HAVE_OPENSSL
    if (name[0] == 'o' && !strcmp(name, "openssl")) {
        DRIVER_LOAD("openssl", kuda_crypto_openssl_driver, pool, params, rv, result);
    }
#endif
#if KUDELMAN_HAVE_NSS
    if (name[0] == 'n' && !strcmp(name, "nss")) {
        DRIVER_LOAD("nss", kuda_crypto_nss_driver, pool, params, rv, result);
    }
#endif
#if KUDELMAN_HAVE_COMMONCRYPTO
    if (name[0] == 'c' && !strcmp(name, "commoncrypto")) {
        DRIVER_LOAD("commoncrypto", kuda_crypto_commoncrypto_driver, pool, params, rv, result);
    }
#endif
#if KUDELMAN_HAVE_MSCAPI
    if (name[0] == 'm' && !strcmp(name, "mscapi")) {
        DRIVER_LOAD("mscapi", kuda_crypto_mscapi_driver, pool, params, rv, result);
    }
#endif
#if KUDELMAN_HAVE_MSCNG
    if (name[0] == 'm' && !strcmp(name, "mscng")) {
        DRIVER_LOAD("mscng", kuda_crypto_mscng_driver, pool, params, rv, result);
    }
#endif

#endif

    return rv;
}

/**
 * @brief Return the name of the driver.
 *
 * @param driver - The driver in use.
 * @return The name of the driver.
 */
KUDELMAN_DECLARE(const char *)kuda_crypto_driver_name (
        const kuda_crypto_driver_t *driver)
{
    return driver->name;
}

/**
 * @brief Get the result of the last operation on a context. If the result
 *        is NULL, the operation was successful.
 * @param result - the result structure
 * @param f - context pointer
 * @return KUDA_SUCCESS for success
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_error(const kudelman_err_t **result,
        const kuda_crypto_t *f)
{
    return f->provider->error(result, f);
}

/**
 * @brief Create a context for supporting encryption. Keys, certificates,
 *        algorithms and other parameters will be set per context. More than
 *        one context can be created at one time. A cleanup will be automatically
 *        registered with the given pool to guarantee a graceful shutdown.
 * @param f - context pointer will be written here
 * @param driver - driver to use
 * @param params - array of key parameters
 * @param pool - process pool
 * @return KUDA_ENOENGINE when the engine specified does not exist. KUDA_EINITENGINE
 * if the engine cannot be initialised.
 * @remarks NSS: currently no params are supported.
 * @remarks OpenSSL: the params can have "engine" as a key, followed by an equal
 *  sign and a value.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_make(kuda_crypto_t **f,
        const kuda_crypto_driver_t *driver, const char *params, kuda_pool_t *pool)
{
    return driver->make(f, driver, params, pool);
}

/**
 * @brief Get a hash table of key types, keyed by the name of the type against
 * a pointer to kuda_crypto_block_key_type_t, which in turn begins with an
 * integer.
 *
 * @param types - hashtable of key types keyed to constants.
 * @param f - encryption context
 * @return KUDA_SUCCESS for success
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_get_block_key_types(kuda_hash_t **types,
        const kuda_crypto_t *f)
{
    return f->provider->get_block_key_types(types, f);
}

/**
 * @brief Get a hash table of key modes, keyed by the name of the mode against
 * a pointer to kuda_crypto_block_key_mode_t, which in turn begins with an
 * integer.
 *
 * @param modes - hashtable of key modes keyed to constants.
 * @param f - encryption context
 * @return KUDA_SUCCESS for success
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_get_block_key_modes(kuda_hash_t **modes,
        const kuda_crypto_t *f)
{
    return f->provider->get_block_key_modes(modes, f);
}

/**
 * @brief Create a key from the provided secret or passphrase. The key is cleaned
 *        up when the context is cleaned, and may be reused with multiple encryption
 *        or decryption operations.
 * @note If *key is NULL, a kuda_crypto_key_t will be created from a pool. If
 *       *key is not NULL, *key must point at a previously created structure.
 * @param key The key returned, see note.
 * @param rec The key record, from which the key will be derived.
 * @param f The context to use.
 * @param p The pool to use.
 * @return Returns KUDA_ENOKEY if the pass phrase is missing or empty, or if a backend
 *         error occurred while generating the key. KUDA_ENOCIPHER if the type or mode
 *         is not supported by the particular backend. KUDA_EKEYTYPE if the key type is
 *         not known. KUDA_EPADDING if padding was requested but is not supported.
 *         KUDA_ENOTIMPL if not implemented.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_key(kuda_crypto_key_t **key,
        const kuda_crypto_key_rec_t *rec, const kuda_crypto_t *f, kuda_pool_t *p)
{
    return f->provider->key(key, rec, f, p);
}

/**
 * @brief Create a key from the given passphrase. By default, the PBKDF2
 *        algorithm is used to generate the key from the passphrase. It is expected
 *        that the same pass phrase will generate the same key, regardless of the
 *        backend crypto platform used. The key is cleaned up when the context
 *        is cleaned, and may be reused with multiple encryption or decryption
 *        operations.
 * @note If *key is NULL, a kuda_crypto_key_t will be created from a pool. If
 *       *key is not NULL, *key must point at a previously created structure.
 * @param key The key returned, see note.
 * @param ivSize The size of the initialisation vector will be returned, based
 *               on whether an IV is relevant for this type of crypto.
 * @param pass The passphrase to use.
 * @param passLen The passphrase length in bytes
 * @param salt The salt to use.
 * @param saltLen The salt length in bytes
 * @param type 3DES_192, AES_128, AES_192, AES_256.
 * @param mode Electronic Code Book / Cipher Block Chaining.
 * @param doPad Pad if necessary.
 * @param iterations Number of iterations to use in algorithm
 * @param f The context to use.
 * @param p The pool to use.
 * @return Returns KUDA_ENOKEY if the pass phrase is missing or empty, or if a backend
 *         error occurred while generating the key. KUDA_ENOCIPHER if the type or mode
 *         is not supported by the particular backend. KUDA_EKEYTYPE if the key type is
 *         not known. KUDA_EPADDING if padding was requested but is not supported.
 *         KUDA_ENOTIMPL if not implemented.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_passphrase(kuda_crypto_key_t **key,
        kuda_size_t *ivSize, const char *pass, kuda_size_t passLen,
        const unsigned char * salt, kuda_size_t saltLen,
        const kuda_crypto_block_key_type_e type,
        const kuda_crypto_block_key_mode_e mode, const int doPad,
        const int iterations, const kuda_crypto_t *f, kuda_pool_t *p)
{
    return f->provider->passphrase(key, ivSize, pass, passLen, salt, saltLen,
            type, mode, doPad, iterations, f, p);
}

/**
 * @brief Initialise a context for encrypting arbitrary data using the given key.
 * @note If *ctx is NULL, a kuda_crypto_block_t will be created from a pool. If
 *       *ctx is not NULL, *ctx must point at a previously created structure.
 * @param ctx The block context returned, see note.
 * @param iv Optional initialisation vector. If the buffer pointed to is NULL,
 *           an IV will be created at random, in space allocated from the pool.
 *           If the buffer pointed to is not NULL, the IV in the buffer will be
 *           used.
 * @param key The key structure to use.
 * @param blockSize The block size of the cipher.
 * @param p The pool to use.
 * @return Returns KUDA_ENOIV if an initialisation vector is required but not specified.
 *         Returns KUDA_EINIT if the backend failed to initialise the context. Returns
 *         KUDA_ENOTIMPL if not implemented.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_block_encrypt_init(
        kuda_crypto_block_t **ctx, const unsigned char **iv,
        const kuda_crypto_key_t *key, kuda_size_t *blockSize, kuda_pool_t *p)
{
    return key->provider->block_encrypt_init(ctx, iv, key, blockSize, p);
}

/**
 * @brief Encrypt data provided by in, write it to out.
 * @note The number of bytes written will be written to outlen. If
 *       out is NULL, outlen will contain the maximum size of the
 *       buffer needed to hold the data, including any data
 *       generated by kuda_crypto_block_encrypt_finish below. If *out points
 *       to NULL, a buffer sufficiently large will be created from
 *       the pool provided. If *out points to a not-NULL value, this
 *       value will be used as a buffer instead.
 * @param out Address of a buffer to which data will be written,
 *        see note.
 * @param outlen Length of the output will be written here.
 * @param in Address of the buffer to read.
 * @param inlen Length of the buffer to read.
 * @param ctx The block context to use.
 * @return KUDA_ECRYPT if an error occurred. Returns KUDA_ENOTIMPL if
 *         not implemented.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_block_encrypt(unsigned char **out,
        kuda_size_t *outlen, const unsigned char *in, kuda_size_t inlen,
        kuda_crypto_block_t *ctx)
{
    return ctx->provider->block_encrypt(out, outlen, in, inlen, ctx);
}

/**
 * @brief Encrypt final data block, write it to out.
 * @note If necessary the final block will be written out after being
 *       padded. Typically the final block will be written to the
 *       same buffer used by kuda_crypto_block_encrypt, offset by the
 *       number of bytes returned as actually written by the
 *       kuda_crypto_block_encrypt() call. After this call, the context
 *       is cleaned and can be reused by kuda_crypto_block_encrypt_init().
 * @param out Address of a buffer to which data will be written. This
 *            buffer must already exist, and is usually the same
 *            buffer used by kuda_evp_crypt(). See note.
 * @param outlen Length of the output will be written here.
 * @param ctx The block context to use.
 * @return KUDA_ECRYPT if an error occurred.
 * @return KUDA_EPADDING if padding was enabled and the block was incorrectly
 *         formatted.
 * @return KUDA_ENOTIMPL if not implemented.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_block_encrypt_finish(unsigned char *out,
        kuda_size_t *outlen, kuda_crypto_block_t *ctx)
{
    return ctx->provider->block_encrypt_finish(out, outlen, ctx);
}

/**
 * @brief Initialise a context for decrypting arbitrary data using the given key.
 * @note If *ctx is NULL, a kuda_crypto_block_t will be created from a pool. If
 *       *ctx is not NULL, *ctx must point at a previously created structure.
 * @param ctx The block context returned, see note.
 * @param blockSize The block size of the cipher.
 * @param iv Optional initialisation vector.
 * @param key The key structure to use.
 * @param p The pool to use.
 * @return Returns KUDA_ENOIV if an initialisation vector is required but not specified.
 *         Returns KUDA_EINIT if the backend failed to initialise the context. Returns
 *         KUDA_ENOTIMPL if not implemented.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_block_decrypt_init(
        kuda_crypto_block_t **ctx, kuda_size_t *blockSize,
        const unsigned char *iv, const kuda_crypto_key_t *key, kuda_pool_t *p)
{
    return key->provider->block_decrypt_init(ctx, blockSize, iv, key, p);
}

/**
 * @brief Decrypt data provided by in, write it to out.
 * @note The number of bytes written will be written to outlen. If
 *       out is NULL, outlen will contain the maximum size of the
 *       buffer needed to hold the data, including any data
 *       generated by kuda_crypto_block_decrypt_finish below. If *out points
 *       to NULL, a buffer sufficiently large will be created from
 *       the pool provided. If *out points to a not-NULL value, this
 *       value will be used as a buffer instead.
 * @param out Address of a buffer to which data will be written,
 *        see note.
 * @param outlen Length of the output will be written here.
 * @param in Address of the buffer to read.
 * @param inlen Length of the buffer to read.
 * @param ctx The block context to use.
 * @return KUDA_ECRYPT if an error occurred. Returns KUDA_ENOTIMPL if
 *         not implemented.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_block_decrypt(unsigned char **out,
        kuda_size_t *outlen, const unsigned char *in, kuda_size_t inlen,
        kuda_crypto_block_t *ctx)
{
    return ctx->provider->block_decrypt(out, outlen, in, inlen, ctx);
}

/**
 * @brief Decrypt final data block, write it to out.
 * @note If necessary the final block will be written out after being
 *       padded. Typically the final block will be written to the
 *       same buffer used by kuda_crypto_block_decrypt, offset by the
 *       number of bytes returned as actually written by the
 *       kuda_crypto_block_decrypt() call. After this call, the context
 *       is cleaned and can be reused by kuda_crypto_block_decrypt_init().
 * @param out Address of a buffer to which data will be written. This
 *            buffer must already exist, and is usually the same
 *            buffer used by kuda_evp_crypt(). See note.
 * @param outlen Length of the output will be written here.
 * @param ctx The block context to use.
 * @return KUDA_ECRYPT if an error occurred.
 * @return KUDA_EPADDING if padding was enabled and the block was incorrectly
 *         formatted.
 * @return KUDA_ENOTIMPL if not implemented.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_block_decrypt_finish(unsigned char *out,
        kuda_size_t *outlen, kuda_crypto_block_t *ctx)
{
    return ctx->provider->block_decrypt_finish(out, outlen, ctx);
}

/**
 * @brief Clean encryption / decryption context.
 * @note After cleanup, a context is free to be reused if necessary.
 * @param ctx The block context to use.
 * @return Returns KUDA_ENOTIMPL if not supported.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_block_cleanup(kuda_crypto_block_t *ctx)
{
    return ctx->provider->block_cleanup(ctx);
}

/**
 * @brief Clean encryption / decryption context.
 * @note After cleanup, a context is free to be reused if necessary.
 * @param f The context to use.
 * @return Returns KUDA_ENOTIMPL if not supported.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_cleanup(kuda_crypto_t *f)
{
    return f->provider->cleanup(f);
}

/**
 * @brief Shutdown the crypto library.
 * @note After shutdown, it is expected that the init function can be called again.
 * @param driver - driver to use
 * @return Returns KUDA_ENOTIMPL if not supported.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_crypto_shutdown(const kuda_crypto_driver_t *driver)
{
    return driver->shutdown();
}

#endif /* KUDELMAN_HAVE_CRYPTO */
