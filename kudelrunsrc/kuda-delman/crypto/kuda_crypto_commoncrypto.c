/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_lib.h"
#include "kudelman.h"
#include "kudelman_errno.h"

#include <ctype.h>
#include <assert.h>
#include <stdlib.h>

#include "kuda_strings.h"
#include "kuda_time.h"
#include "kuda_buckets.h"
#include "kuda_random.h"

#include "kuda_crypto_internal.h"

#if KUDELMAN_HAVE_CRYPTO

#include <CommonCrypto/CommonCrypto.h>

#define LOG_PREFIX "kuda_crypto_commoncrypto: "

struct kuda_crypto_t
{
    kuda_pool_t *pool;
    const kuda_crypto_driver_t *provider;
    kudelman_err_t *result;
    kuda_hash_t *types;
    kuda_hash_t *modes;
    kuda_random_t *rng;
};

struct kuda_crypto_key_t
{
    kuda_pool_t *pool;
    const kuda_crypto_driver_t *provider;
    const kuda_crypto_t *f;
    CCAlgorithm algorithm;
    CCOptions options;
    unsigned char *key;
    int keyLen;
    int ivSize;
    kuda_size_t blockSize;
};

struct kuda_crypto_block_t
{
    kuda_pool_t *pool;
    const kuda_crypto_driver_t *provider;
    const kuda_crypto_t *f;
    const kuda_crypto_key_t *key;
    CCCryptorRef ref;
};

static struct kuda_crypto_block_key_type_t key_types[] =
{
{ KUDA_KEY_3DES_192, 24, 8, 8 },
{ KUDA_KEY_AES_128, 16, 16, 16 },
{ KUDA_KEY_AES_192, 24, 16, 16 },
{ KUDA_KEY_AES_256, 32, 16, 16 } };

static struct kuda_crypto_block_key_mode_t key_modes[] =
{
{ KUDA_MODE_ECB },
{ KUDA_MODE_CBC } };

/**
 * Fetch the most recent error from this driver.
 */
static kuda_status_t crypto_error(const kudelman_err_t **result,
        const kuda_crypto_t *f)
{
    *result = f->result;
    return KUDA_SUCCESS;
}

/**
 * Shutdown the crypto library and release resources.
 */
static kuda_status_t crypto_shutdown(void)
{
    return KUDA_SUCCESS;
}

static kuda_status_t crypto_shutdown_helper(void *data)
{
    return crypto_shutdown();
}

/**
 * Initialise the crypto library and perform one time initialisation.
 */
static kuda_status_t crypto_init(kuda_pool_t *pool, const char *params,
        const kudelman_err_t **result)
{

    kuda_pool_cleanup_register(pool, pool, crypto_shutdown_helper,
            kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}

/**
 * @brief Clean encryption / decryption context.
 * @note After cleanup, a context is free to be reused if necessary.
 * @param ctx The block context to use.
 * @return Returns KUDA_ENOTIMPL if not supported.
 */
static kuda_status_t crypto_block_cleanup(kuda_crypto_block_t *ctx)
{

    if (ctx->ref) {
        CCCryptorRelease(ctx->ref);
        ctx->ref = NULL;
    }

    return KUDA_SUCCESS;

}

static kuda_status_t crypto_block_cleanup_helper(void *data)
{
    kuda_crypto_block_t *block = (kuda_crypto_block_t *) data;
    return crypto_block_cleanup(block);
}

/**
 * @brief Clean encryption / decryption context.
 * @note After cleanup, a context is free to be reused if necessary.
 * @param f The context to use.
 * @return Returns KUDA_ENOTIMPL if not supported.
 */
static kuda_status_t crypto_cleanup(kuda_crypto_t *f)
{

    return KUDA_SUCCESS;

}

static kuda_status_t crypto_cleanup_helper(void *data)
{
    kuda_crypto_t *f = (kuda_crypto_t *) data;
    return crypto_cleanup(f);
}

/**
 * @brief Create a context for supporting encryption. Keys, certificates,
 *        algorithms and other parameters will be set per context. More than
 *        one context can be created at one time. A cleanup will be automatically
 *        registered with the given pool to guarantee a graceful shutdown.
 * @param f - context pointer will be written here
 * @param provider - provider to use
 * @param params - array of key parameters
 * @param pool - process pool
 * @return KUDA_ENOENGINE when the engine specified does not exist. KUDA_EINITENGINE
 * if the engine cannot be initialised.
 */
static kuda_status_t crypto_make(kuda_crypto_t **ff,
        const kuda_crypto_driver_t *provider, const char *params,
        kuda_pool_t *pool)
{
    kuda_crypto_t *f = kuda_pcalloc(pool, sizeof(kuda_crypto_t));
    kuda_status_t rv;

    if (!f) {
        return KUDA_ENOMEM;
    }
    *ff = f;
    f->pool = pool;
    f->provider = provider;

    /* seed the secure random number generator */
    f->rng = kuda_random_standard_new(pool);
    if (!f->rng) {
        return KUDA_ENOMEM;
    }
    do {
        unsigned char seed[8];
        rv = kuda_generate_random_bytes(seed, sizeof(seed));
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
        kuda_random_add_entropy(f->rng, seed, sizeof(seed));
        rv = kuda_random_secure_ready(f->rng);
    } while (rv == KUDA_ENOTENOUGHENTROPY);

    f->result = kuda_pcalloc(pool, sizeof(kudelman_err_t));
    if (!f->result) {
        return KUDA_ENOMEM;
    }

    f->types = kuda_hash_make(pool);
    if (!f->types) {
        return KUDA_ENOMEM;
    }
    kuda_hash_set(f->types, "3des192", KUDA_HASH_KEY_STRING, &(key_types[0]));
    kuda_hash_set(f->types, "aes128", KUDA_HASH_KEY_STRING, &(key_types[1]));
    kuda_hash_set(f->types, "aes192", KUDA_HASH_KEY_STRING, &(key_types[2]));
    kuda_hash_set(f->types, "aes256", KUDA_HASH_KEY_STRING, &(key_types[3]));

    f->modes = kuda_hash_make(pool);
    if (!f->modes) {
        return KUDA_ENOMEM;
    }
    kuda_hash_set(f->modes, "ecb", KUDA_HASH_KEY_STRING, &(key_modes[0]));
    kuda_hash_set(f->modes, "cbc", KUDA_HASH_KEY_STRING, &(key_modes[1]));

    kuda_pool_cleanup_register(pool, f, crypto_cleanup_helper,
            kuda_pool_cleanup_null);

    return KUDA_SUCCESS;

}

/**
 * @brief Get a hash table of key types, keyed by the name of the type against
 * a pointer to kuda_crypto_block_key_type_t.
 *
 * @param types - hashtable of key types keyed to constants.
 * @param f - encryption context
 * @return KUDA_SUCCESS for success
 */
static kuda_status_t crypto_get_block_key_types(kuda_hash_t **types,
        const kuda_crypto_t *f)
{
    *types = f->types;
    return KUDA_SUCCESS;
}

/**
 * @brief Get a hash table of key modes, keyed by the name of the mode against
 * a pointer to kuda_crypto_block_key_mode_t.
 *
 * @param modes - hashtable of key modes keyed to constants.
 * @param f - encryption context
 * @return KUDA_SUCCESS for success
 */
static kuda_status_t crypto_get_block_key_modes(kuda_hash_t **modes,
        const kuda_crypto_t *f)
{
    *modes = f->modes;
    return KUDA_SUCCESS;
}

/*
 * Work out which mechanism to use.
 */
static kuda_status_t crypto_cipher_mechanism(kuda_crypto_key_t *key,
        const kuda_crypto_block_key_type_e type,
        const kuda_crypto_block_key_mode_e mode, const int doPad, kuda_pool_t *p)
{
    /* handle padding */
    key->options = doPad ? kCCOptionPKCS7Padding : 0;

    /* determine the algorithm to be used */
    switch (type) {

    case (KUDA_KEY_3DES_192):

        /* A 3DES key */
        if (mode == KUDA_MODE_CBC) {
            key->algorithm = kCCAlgorithm3DES;
            key->keyLen = kCCKeySize3DES;
            key->ivSize = kCCBlockSize3DES;
            key->blockSize = kCCBlockSize3DES;
        }
        else {
            key->algorithm = kCCAlgorithm3DES;
            key->options += kCCOptionECBMode;
            key->keyLen = kCCKeySize3DES;
            key->ivSize = 0;
            key->blockSize = kCCBlockSize3DES;
        }
        break;

    case (KUDA_KEY_AES_128):

        if (mode == KUDA_MODE_CBC) {
            key->algorithm = kCCAlgorithmAES128;
            key->keyLen = kCCKeySizeAES128;
            key->ivSize = kCCBlockSizeAES128;
            key->blockSize = kCCBlockSizeAES128;
        }
        else {
            key->algorithm = kCCAlgorithmAES128;
            key->options += kCCOptionECBMode;
            key->keyLen = kCCKeySizeAES128;
            key->ivSize = 0;
            key->blockSize = kCCBlockSizeAES128;
        }
        break;

    case (KUDA_KEY_AES_192):

        if (mode == KUDA_MODE_CBC) {
            key->algorithm = kCCAlgorithmAES128;
            key->keyLen = kCCKeySizeAES192;
            key->ivSize = kCCBlockSizeAES128;
            key->blockSize = kCCBlockSizeAES128;
        }
        else {
            key->algorithm = kCCAlgorithmAES128;
            key->options += kCCOptionECBMode;
            key->keyLen = kCCKeySizeAES192;
            key->ivSize = 0;
            key->blockSize = kCCBlockSizeAES128;
        }
        break;

    case (KUDA_KEY_AES_256):

        if (mode == KUDA_MODE_CBC) {
            key->algorithm = kCCAlgorithmAES128;
            key->keyLen = kCCKeySizeAES256;
            key->ivSize = kCCBlockSizeAES128;
            key->blockSize = kCCBlockSizeAES128;
        }
        else {
            key->algorithm = kCCAlgorithmAES128;
            key->options += kCCOptionECBMode;
            key->keyLen = kCCKeySizeAES256;
            key->ivSize = 0;
            key->blockSize = kCCBlockSizeAES128;
        }
        break;

    default:

        /* TODO: Support CAST, Blowfish */

        /* unknown key type, give up */
        return KUDA_EKEYTYPE;

    }

    /* make space for the key */
    key->key = kuda_palloc(p, key->keyLen);
    if (!key->key) {
        return KUDA_ENOMEM;
    }
    kuda_crypto_clear(p, key->key, key->keyLen);

    return KUDA_SUCCESS;
}

/**
 * @brief Create a key from the provided secret or passphrase. The key is cleaned
 *        up when the context is cleaned, and may be reused with multiple encryption
 *        or decryption operations.
 * @note If *key is NULL, a kuda_crypto_key_t will be created from a pool. If
 *       *key is not NULL, *key must point at a previously created structure.
 * @param key The key returned, see note.
 * @param rec The key record, from which the key will be derived.
 * @param f The context to use.
 * @param p The pool to use.
 * @return Returns KUDA_ENOKEY if the pass phrase is missing or empty, or if a backend
 *         error occurred while generating the key. KUDA_ENOCIPHER if the type or mode
 *         is not supported by the particular backend. KUDA_EKEYTYPE if the key type is
 *         not known. KUDA_EPADDING if padding was requested but is not supported.
 *         KUDA_ENOTIMPL if not implemented.
 */
static kuda_status_t crypto_key(kuda_crypto_key_t **k,
        const kuda_crypto_key_rec_t *rec, const kuda_crypto_t *f, kuda_pool_t *p)
{
    kuda_status_t rv;
    kuda_crypto_key_t *key = *k;

    if (!key) {
        *k = key = kuda_pcalloc(p, sizeof *key);
    }
    if (!key) {
        return KUDA_ENOMEM;
    }

    key->f = f;
    key->provider = f->provider;

    /* decide on what cipher mechanism we will be using */
    rv = crypto_cipher_mechanism(key, rec->type, rec->mode, rec->pad, p);
    if (KUDA_SUCCESS != rv) {
        return rv;
    }

    switch (rec->ktype) {

    case KUDA_CRYPTO_KTYPE_PASSPHRASE: {

        /* generate the key */
        if ((f->result->rc = CCKeyDerivationPBKDF(kCCPBKDF2,
                rec->k.passphrase.pass, rec->k.passphrase.passLen,
                rec->k.passphrase.salt, rec->k.passphrase.saltLen,
                kCCPRFHmacAlgSHA1, rec->k.passphrase.iterations, key->key,
                key->keyLen)) == kCCParamError) {
            return KUDA_ENOKEY;
        }

        break;
    }

    case KUDA_CRYPTO_KTYPE_SECRET: {

        /* sanity check - key correct size? */
        if (rec->k.secret.secretLen != key->keyLen) {
            return KUDA_EKEYLENGTH;
        }

        /* copy the key */
        memcpy(key->key, rec->k.secret.secret, rec->k.secret.secretLen);

        break;
    }

    default: {

        return KUDA_ENOKEY;

    }
    }

    return KUDA_SUCCESS;
}

/**
 * @brief Create a key from the given passphrase. By default, the PBKDF2
 *        algorithm is used to generate the key from the passphrase. It is expected
 *        that the same pass phrase will generate the same key, regardless of the
 *        backend crypto platform used. The key is cleaned up when the context
 *        is cleaned, and may be reused with multiple encryption or decryption
 *        operations.
 * @note If *key is NULL, a kuda_crypto_key_t will be created from a pool. If
 *       *key is not NULL, *key must point at a previously created structure.
 * @param key The key returned, see note.
 * @param ivSize The size of the initialisation vector will be returned, based
 *               on whether an IV is relevant for this type of crypto.
 * @param pass The passphrase to use.
 * @param passLen The passphrase length in bytes
 * @param salt The salt to use.
 * @param saltLen The salt length in bytes
 * @param type 3DES_192, AES_128, AES_192, AES_256.
 * @param mode Electronic Code Book / Cipher Block Chaining.
 * @param doPad Pad if necessary.
 * @param iterations Iteration count
 * @param f The context to use.
 * @param p The pool to use.
 * @return Returns KUDA_ENOKEY if the pass phrase is missing or empty, or if a backend
 *         error occurred while generating the key. KUDA_ENOCIPHER if the type or mode
 *         is not supported by the particular backend. KUDA_EKEYTYPE if the key type is
 *         not known. KUDA_EPADDING if padding was requested but is not supported.
 *         KUDA_ENOTIMPL if not implemented.
 */
static kuda_status_t crypto_passphrase(kuda_crypto_key_t **k, kuda_size_t *ivSize,
        const char *pass, kuda_size_t passLen, const unsigned char * salt,
        kuda_size_t saltLen, const kuda_crypto_block_key_type_e type,
        const kuda_crypto_block_key_mode_e mode, const int doPad,
        const int iterations, const kuda_crypto_t *f, kuda_pool_t *p)
{
    kuda_status_t rv;
    kuda_crypto_key_t *key = *k;

    if (!key) {
        *k = key = kuda_pcalloc(p, sizeof *key);
        if (!key) {
            return KUDA_ENOMEM;
        }
    }

    key->f = f;
    key->provider = f->provider;

    /* decide on what cipher mechanism we will be using */
    rv = crypto_cipher_mechanism(key, type, mode, doPad, p);
    if (KUDA_SUCCESS != rv) {
        return rv;
    }

    /* generate the key */
    if ((f->result->rc = CCKeyDerivationPBKDF(kCCPBKDF2, pass, passLen, salt,
            saltLen, kCCPRFHmacAlgSHA1, iterations, key->key, key->keyLen))
            == kCCParamError) {
        return KUDA_ENOKEY;
    }

    if (ivSize) {
        *ivSize = key->ivSize;
    }

    return KUDA_SUCCESS;
}

/**
 * @brief Initialise a context for encrypting arbitrary data using the given key.
 * @note If *ctx is NULL, a kuda_crypto_block_t will be created from a pool. If
 *       *ctx is not NULL, *ctx must point at a previously created structure.
 * @param ctx The block context returned, see note.
 * @param iv Optional initialisation vector. If the buffer pointed to is NULL,
 *           an IV will be created at random, in space allocated from the pool.
 *           If the buffer pointed to is not NULL, the IV in the buffer will be
 *           used.
 * @param key The key structure.
 * @param blockSize The block size of the cipher.
 * @param p The pool to use.
 * @return Returns KUDA_ENOIV if an initialisation vector is required but not specified.
 *         Returns KUDA_EINIT if the backend failed to initialise the context. Returns
 *         KUDA_ENOTIMPL if not implemented.
 */
static kuda_status_t crypto_block_encrypt_init(kuda_crypto_block_t **ctx,
        const unsigned char **iv, const kuda_crypto_key_t *key,
        kuda_size_t *blockSize, kuda_pool_t *p)
{
    unsigned char *usedIv;
    kuda_crypto_block_t *block = *ctx;
    if (!block) {
        *ctx = block = kuda_pcalloc(p, sizeof(kuda_crypto_block_t));
    }
    if (!block) {
        return KUDA_ENOMEM;
    }
    block->f = key->f;
    block->pool = p;
    block->provider = key->provider;
    block->key = key;

    kuda_pool_cleanup_register(p, block, crypto_block_cleanup_helper,
            kuda_pool_cleanup_null);

    /* generate an IV, if necessary */
    usedIv = NULL;
    if (key->ivSize) {
        if (iv == NULL) {
            return KUDA_ENOIV;
        }
        if (*iv == NULL) {
            kuda_status_t status;
            usedIv = kuda_pcalloc(p, key->ivSize);
            if (!usedIv) {
                return KUDA_ENOMEM;
            }
            kuda_crypto_clear(p, usedIv, key->ivSize);
            status = kuda_random_secure_bytes(block->f->rng, usedIv,
                    key->ivSize);
            if (KUDA_SUCCESS != status) {
                return status;
            }
            *iv = usedIv;
        }
        else {
            usedIv = (unsigned char *) *iv;
        }
    }

    /* create a new context for encryption */
    switch ((block->f->result->rc = CCCryptorCreate(kCCEncrypt, key->algorithm,
            key->options, key->key, key->keyLen, usedIv, &block->ref))) {
    case kCCSuccess: {
        break;
    }
    case kCCParamError: {
        return KUDA_EINIT;
    }
    case kCCMemoryFailure: {
        return KUDA_ENOMEM;
    }
    case kCCAlignmentError: {
        return KUDA_EPADDING;
    }
    case kCCUnimplemented: {
        return KUDA_ENOTIMPL;
    }
    default: {
        return KUDA_EINIT;
    }
    }

    if (blockSize) {
        *blockSize = key->blockSize;
    }

    return KUDA_SUCCESS;

}

/**
 * @brief Encrypt data provided by in, write it to out.
 * @note The number of bytes written will be written to outlen. If
 *       out is NULL, outlen will contain the maximum size of the
 *       buffer needed to hold the data, including any data
 *       generated by kuda_crypto_block_encrypt_finish below. If *out points
 *       to NULL, a buffer sufficiently large will be created from
 *       the pool provided. If *out points to a not-NULL value, this
 *       value will be used as a buffer instead.
 * @param out Address of a buffer to which data will be written,
 *        see note.
 * @param outlen Length of the output will be written here.
 * @param in Address of the buffer to read.
 * @param inlen Length of the buffer to read.
 * @param ctx The block context to use.
 * @return KUDA_ECRYPT if an error occurred. Returns KUDA_ENOTIMPL if
 *         not implemented.
 */
static kuda_status_t crypto_block_encrypt(unsigned char **out,
        kuda_size_t *outlen, const unsigned char *in, kuda_size_t inlen,
        kuda_crypto_block_t *ctx)
{
    kuda_size_t outl = *outlen;
    unsigned char *buffer;

    /* are we after the maximum size of the out buffer? */
    if (!out) {
        *outlen = CCCryptorGetOutputLength(ctx->ref, inlen, 1);
        return KUDA_SUCCESS;
    }

    /* must we allocate the output buffer from a pool? */
    if (!*out) {
        outl = CCCryptorGetOutputLength(ctx->ref, inlen, 1);
        buffer = kuda_palloc(ctx->pool, outl);
        if (!buffer) {
            return KUDA_ENOMEM;
        }
        kuda_crypto_clear(ctx->pool, buffer, outl);
        *out = buffer;
    }

    switch ((ctx->f->result->rc = CCCryptorUpdate(ctx->ref, in, inlen, (*out),
            outl, &outl))) {
    case kCCSuccess: {
        break;
    }
    case kCCBufferTooSmall: {
        return KUDA_ENOSPACE;
    }
    default: {
        return KUDA_ECRYPT;
    }
    }
    *outlen = outl;

    return KUDA_SUCCESS;

}

/**
 * @brief Encrypt final data block, write it to out.
 * @note If necessary the final block will be written out after being
 *       padded. Typically the final block will be written to the
 *       same buffer used by kuda_crypto_block_encrypt, offset by the
 *       number of bytes returned as actually written by the
 *       kuda_crypto_block_encrypt() call. After this call, the context
 *       is cleaned and can be reused by kuda_crypto_block_encrypt_init().
 * @param out Address of a buffer to which data will be written. This
 *            buffer must already exist, and is usually the same
 *            buffer used by kuda_evp_crypt(). See note.
 * @param outlen Length of the output will be written here.
 * @param ctx The block context to use.
 * @return KUDA_ECRYPT if an error occurred.
 * @return KUDA_EPADDING if padding was enabled and the block was incorrectly
 *         formatted.
 * @return KUDA_ENOTIMPL if not implemented.
 */
static kuda_status_t crypto_block_encrypt_finish(unsigned char *out,
        kuda_size_t *outlen, kuda_crypto_block_t *ctx)
{
    kuda_size_t len = *outlen;

    ctx->f->result->rc = CCCryptorFinal(ctx->ref, out,
            CCCryptorGetOutputLength(ctx->ref, 0, 1), &len);

    /* always clean up */
    crypto_block_cleanup(ctx);

    switch (ctx->f->result->rc) {
    case kCCSuccess: {
        break;
    }
    case kCCBufferTooSmall: {
        return KUDA_ENOSPACE;
    }
    case kCCAlignmentError: {
        return KUDA_EPADDING;
    }
    case kCCDecodeError: {
        return KUDA_ECRYPT;
    }
    default: {
        return KUDA_ECRYPT;
    }
    }
    *outlen = len;

    return KUDA_SUCCESS;

}

/**
 * @brief Initialise a context for decrypting arbitrary data using the given key.
 * @note If *ctx is NULL, a kuda_crypto_block_t will be created from a pool. If
 *       *ctx is not NULL, *ctx must point at a previously created structure.
 * @param ctx The block context returned, see note.
 * @param blockSize The block size of the cipher.
 * @param iv Optional initialisation vector. If the buffer pointed to is NULL,
 *           an IV will be created at random, in space allocated from the pool.
 *           If the buffer is not NULL, the IV in the buffer will be used.
 * @param key The key structure.
 * @param p The pool to use.
 * @return Returns KUDA_ENOIV if an initialisation vector is required but not specified.
 *         Returns KUDA_EINIT if the backend failed to initialise the context. Returns
 *         KUDA_ENOTIMPL if not implemented.
 */
static kuda_status_t crypto_block_decrypt_init(kuda_crypto_block_t **ctx,
        kuda_size_t *blockSize, const unsigned char *iv,
        const kuda_crypto_key_t *key, kuda_pool_t *p)
{
    kuda_crypto_block_t *block = *ctx;
    if (!block) {
        *ctx = block = kuda_pcalloc(p, sizeof(kuda_crypto_block_t));
    }
    if (!block) {
        return KUDA_ENOMEM;
    }
    block->f = key->f;
    block->pool = p;
    block->provider = key->provider;

    kuda_pool_cleanup_register(p, block, crypto_block_cleanup_helper,
            kuda_pool_cleanup_null);

    /* generate an IV, if necessary */
    if (key->ivSize) {
        if (iv == NULL) {
            return KUDA_ENOIV;
        }
    }

    /* create a new context for decryption */
    switch ((block->f->result->rc = CCCryptorCreate(kCCDecrypt, key->algorithm,
            key->options, key->key, key->keyLen, iv, &block->ref))) {
    case kCCSuccess: {
        break;
    }
    case kCCParamError: {
        return KUDA_EINIT;
    }
    case kCCMemoryFailure: {
        return KUDA_ENOMEM;
    }
    case kCCAlignmentError: {
        return KUDA_EPADDING;
    }
    case kCCUnimplemented: {
        return KUDA_ENOTIMPL;
    }
    default: {
        return KUDA_EINIT;
    }
    }

    if (blockSize) {
        *blockSize = key->blockSize;
    }

    return KUDA_SUCCESS;

}

/**
 * @brief Decrypt data provided by in, write it to out.
 * @note The number of bytes written will be written to outlen. If
 *       out is NULL, outlen will contain the maximum size of the
 *       buffer needed to hold the data, including any data
 *       generated by kuda_crypto_block_decrypt_finish below. If *out points
 *       to NULL, a buffer sufficiently large will be created from
 *       the pool provided. If *out points to a not-NULL value, this
 *       value will be used as a buffer instead.
 * @param out Address of a buffer to which data will be written,
 *        see note.
 * @param outlen Length of the output will be written here.
 * @param in Address of the buffer to read.
 * @param inlen Length of the buffer to read.
 * @param ctx The block context to use.
 * @return KUDA_ECRYPT if an error occurred. Returns KUDA_ENOTIMPL if
 *         not implemented.
 */
static kuda_status_t crypto_block_decrypt(unsigned char **out,
        kuda_size_t *outlen, const unsigned char *in, kuda_size_t inlen,
        kuda_crypto_block_t *ctx)
{
    kuda_size_t outl = *outlen;
    unsigned char *buffer;

    /* are we after the maximum size of the out buffer? */
    if (!out) {
        *outlen = CCCryptorGetOutputLength(ctx->ref, inlen, 1);
        return KUDA_SUCCESS;
    }

    /* must we allocate the output buffer from a pool? */
    if (!*out) {
        outl = CCCryptorGetOutputLength(ctx->ref, inlen, 1);
        buffer = kuda_palloc(ctx->pool, outl);
        if (!buffer) {
            return KUDA_ENOMEM;
        }
        kuda_crypto_clear(ctx->pool, buffer, outl);
        *out = buffer;
    }

    switch ((ctx->f->result->rc = CCCryptorUpdate(ctx->ref, in, inlen, (*out),
            outl, &outl))) {
    case kCCSuccess: {
        break;
    }
    case kCCBufferTooSmall: {
        return KUDA_ENOSPACE;
    }
    default: {
        return KUDA_ECRYPT;
    }
    }
    *outlen = outl;

    return KUDA_SUCCESS;

}

/**
 * @brief Decrypt final data block, write it to out.
 * @note If necessary the final block will be written out after being
 *       padded. Typically the final block will be written to the
 *       same buffer used by kuda_crypto_block_decrypt, offset by the
 *       number of bytes returned as actually written by the
 *       kuda_crypto_block_decrypt() call. After this call, the context
 *       is cleaned and can be reused by kuda_crypto_block_decrypt_init().
 * @param out Address of a buffer to which data will be written. This
 *            buffer must already exist, and is usually the same
 *            buffer used by kuda_evp_crypt(). See note.
 * @param outlen Length of the output will be written here.
 * @param ctx The block context to use.
 * @return KUDA_ECRYPT if an error occurred.
 * @return KUDA_EPADDING if padding was enabled and the block was incorrectly
 *         formatted.
 * @return KUDA_ENOTIMPL if not implemented.
 */
static kuda_status_t crypto_block_decrypt_finish(unsigned char *out,
        kuda_size_t *outlen, kuda_crypto_block_t *ctx)
{
    kuda_size_t len = *outlen;

    ctx->f->result->rc = CCCryptorFinal(ctx->ref, out,
            CCCryptorGetOutputLength(ctx->ref, 0, 1), &len);

    /* always clean up */
    crypto_block_cleanup(ctx);

    switch (ctx->f->result->rc) {
    case kCCSuccess: {
        break;
    }
    case kCCBufferTooSmall: {
        return KUDA_ENOSPACE;
    }
    case kCCAlignmentError: {
        return KUDA_EPADDING;
    }
    case kCCDecodeError: {
        return KUDA_ECRYPT;
    }
    default: {
        return KUDA_ECRYPT;
    }
    }
    *outlen = len;

    return KUDA_SUCCESS;

}

/**
 * OSX Common Crypto cAPI.
 */
KUDELMAN_CAPI_DECLARE_DATA const kuda_crypto_driver_t kuda_crypto_commoncrypto_driver =
{
        "commoncrypto", crypto_init, crypto_make, crypto_get_block_key_types,
        crypto_get_block_key_modes, crypto_passphrase,
        crypto_block_encrypt_init, crypto_block_encrypt,
        crypto_block_encrypt_finish, crypto_block_decrypt_init,
        crypto_block_decrypt, crypto_block_decrypt_finish, crypto_block_cleanup,
        crypto_cleanup, crypto_shutdown, crypto_error, crypto_key
};

#endif
