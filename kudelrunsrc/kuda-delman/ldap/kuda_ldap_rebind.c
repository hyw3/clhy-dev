/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*  kuda_ldap_rebind.c -- LDAP rebind callbacks for referrals
 *
 *  The LDAP SDK allows a callback to be set to enable rebinding
 *  for referral processing.
 *
 */

#include "kuda.h"
#include "kudelman.h"
#include "kudelman_config.h"

#if KUDELMAN_DSO_BUILD
#define KUDELMAN_DSO_LDAP_BUILD
#endif

#include "kuda_ldap.h"
#include "kuda_errno.h"
#include "kuda_strings.h"
#include "kuda_ldap_rebind.h"

#include "stdio.h"

#if KUDA_HAS_LDAP

/* Used to store information about connections for use in the referral rebind callback. */
struct kuda_ldap_rebind_entry {
    kuda_pool_t *pool;
    LDAP *index;
    const char *bindDN;
    const char *bindPW;
    struct kuda_ldap_rebind_entry *next;
};
typedef struct kuda_ldap_rebind_entry kuda_ldap_rebind_entry_t;


#ifdef NETWARE
#include "kuda_private.h"
#define get_apd                 APP_DATA* apd = (APP_DATA*)get_app_data(gLibId);
#define kuda_ldap_xref_lock      ((kuda_thread_mutex_t *)(apd->gs_ldap_xref_lock))
#define xref_head               ((kuda_ldap_rebind_entry_t *)(apd->gs_xref_head))
#else
#if KUDA_HAS_THREADS
static kuda_thread_mutex_t *kuda_ldap_xref_lock = NULL;
#endif
static kuda_ldap_rebind_entry_t *xref_head = NULL;
#endif

static int kuda_ldap_rebind_set_callback(LDAP *ld);
static kuda_status_t kuda_ldap_rebind_remove_helper(void *data);

static kuda_status_t kuda_ldap_pool_cleanup_set_null(void *data_)
{
    void **ptr = (void **)data_;
    *ptr = NULL;
    return KUDA_SUCCESS;
}


/* Kuda Delman Runtime routine used to create the xref_lock. */
KUDELMAN_DECLARE_LDAP(kuda_status_t) kuda_ldap_rebind_init(kuda_pool_t *pool)
{
    kuda_status_t retcode = KUDA_SUCCESS;

#ifdef NETWARE
    get_apd
#endif

#if KUDA_HAS_THREADS
    /* run after kuda_thread_mutex_create cleanup */
    kuda_pool_cleanup_register(pool, &kuda_ldap_xref_lock, kuda_ldap_pool_cleanup_set_null,
                              kuda_pool_cleanup_null);

    if (kuda_ldap_xref_lock == NULL) {
        retcode = kuda_thread_mutex_create(&kuda_ldap_xref_lock, KUDA_THREAD_MUTEX_DEFAULT, pool);
    }
#endif

    return(retcode);
}


KUDELMAN_DECLARE_LDAP(kuda_status_t) kuda_ldap_rebind_add(kuda_pool_t *pool,
                                                   LDAP *ld, 
                                                   const char *bindDN, 
                                                   const char *bindPW)
{
    kuda_status_t retcode = KUDA_SUCCESS;
    kuda_ldap_rebind_entry_t *new_xref;

#ifdef NETWARE
    get_apd
#endif

    new_xref = (kuda_ldap_rebind_entry_t *)kuda_pcalloc(pool, sizeof(kuda_ldap_rebind_entry_t));
    if (new_xref) {
        new_xref->pool = pool;
        new_xref->index = ld;
        if (bindDN) {
            new_xref->bindDN = kuda_pstrdup(pool, bindDN);
        }
        if (bindPW) {
            new_xref->bindPW = kuda_pstrdup(pool, bindPW);
        }
    
#if KUDA_HAS_THREADS
       retcode = kuda_thread_mutex_lock(kuda_ldap_xref_lock);
       if (retcode != KUDA_SUCCESS) { 
           return retcode;
       }
#endif
    
        new_xref->next = xref_head;
        xref_head = new_xref;
    
#if KUDA_HAS_THREADS
        retcode = kuda_thread_mutex_unlock(kuda_ldap_xref_lock);
        if (retcode != KUDA_SUCCESS) { 
           return retcode;
        }
#endif
    }
    else {
        return(KUDA_ENOMEM);
    }

    retcode = kuda_ldap_rebind_set_callback(ld);
    if (KUDA_SUCCESS != retcode) {
        kuda_ldap_rebind_remove(ld);
        return retcode;
    }

    kuda_pool_cleanup_register(pool, ld,
                              kuda_ldap_rebind_remove_helper,
                              kuda_pool_cleanup_null);

    return(KUDA_SUCCESS);
}


KUDELMAN_DECLARE_LDAP(kuda_status_t) kuda_ldap_rebind_remove(LDAP *ld)
{
    kuda_ldap_rebind_entry_t *tmp_xref, *prev = NULL;
    kuda_status_t retcode = 0;

#ifdef NETWARE
    get_apd
#endif

#if KUDA_HAS_THREADS
    retcode = kuda_thread_mutex_lock(kuda_ldap_xref_lock);
    if (retcode != KUDA_SUCCESS) { 
        return retcode;
    }
#endif
    tmp_xref = xref_head;

    while ((tmp_xref) && (tmp_xref->index != ld)) {
        prev = tmp_xref;
        tmp_xref = tmp_xref->next;
    }

    if (tmp_xref) {
        if (tmp_xref == xref_head) {
            xref_head = xref_head->next;
        }
        else {
            prev->next = tmp_xref->next;
        }

        /* tmp_xref and its contents were pool allocated so they don't need to be freed here. */

        /* remove the cleanup, just in case this was done manually */
        kuda_pool_cleanup_kill(tmp_xref->pool, tmp_xref->index,
                              kuda_ldap_rebind_remove_helper);
    }

#if KUDA_HAS_THREADS
    retcode = kuda_thread_mutex_unlock(kuda_ldap_xref_lock);
    if (retcode != KUDA_SUCCESS) { 
       return retcode;
    }
#endif
    return KUDA_SUCCESS;
}


static kuda_status_t kuda_ldap_rebind_remove_helper(void *data)
{
    LDAP *ld = (LDAP *)data;
    kuda_ldap_rebind_remove(ld);
    return KUDA_SUCCESS;
}

#if KUDA_HAS_TIVOLI_LDAPSDK || KUDA_HAS_OPENLDAP_LDAPSDK || KUDA_HAS_NOVELL_LDAPSDK
static kuda_ldap_rebind_entry_t *kuda_ldap_rebind_lookup(LDAP *ld)
{
    kuda_ldap_rebind_entry_t *tmp_xref, *match = NULL;

#ifdef NETWARE
    get_apd
#endif

#if KUDA_HAS_THREADS
    kuda_thread_mutex_lock(kuda_ldap_xref_lock);
#endif
    tmp_xref = xref_head;

    while (tmp_xref) {
        if (tmp_xref->index == ld) {
            match = tmp_xref;
            tmp_xref = NULL;
        }
        else {
            tmp_xref = tmp_xref->next;
        }
    }

#if KUDA_HAS_THREADS
    kuda_thread_mutex_unlock(kuda_ldap_xref_lock);
#endif

    return (match);
}
#endif

#if KUDA_HAS_TIVOLI_LDAPSDK

/* LDAP_rebindproc() Tivoli LDAP style
 *     Rebind callback function. Called when chasing referrals. See API docs.
 * ON ENTRY:
 *     ld       Pointer to an LDAP control structure. (input only)
 *     binddnp  Pointer to an Application DName used for binding (in *or* out)
 *     passwdp  Pointer to the password associated with the DName (in *or* out)
 *     methodp  Pointer to the Auth method (output only)
 *     freeit   Flag to indicate if this is a lookup or a free request (input only)
 */
static int LDAP_rebindproc(LDAP *ld, char **binddnp, char **passwdp, int *methodp, int freeit)
{
    if (!freeit) {
        kuda_ldap_rebind_entry_t *my_conn;

        *methodp = LDAP_AUTH_SIMPLE;
        my_conn = kuda_ldap_rebind_lookup(ld);

        if ((my_conn) && (my_conn->bindDN != NULL)) {
            *binddnp = strdup(my_conn->bindDN);
            *passwdp = strdup(my_conn->bindPW);
        } else {
            *binddnp = NULL;
            *passwdp = NULL;
        }
    } else {
        if (*binddnp) {
            free(*binddnp);
        }
        if (*passwdp) {
            free(*passwdp);
        }
    }

    return LDAP_SUCCESS;
}

static int kuda_ldap_rebind_set_callback(LDAP *ld)
{
    ldap_set_rebind_proc(ld, (LDKUDAebindProc)LDAP_rebindproc);
    return KUDA_SUCCESS;
}

#elif KUDA_HAS_OPENLDAP_LDAPSDK

/* LDAP_rebindproc() openLDAP V3 style
 * ON ENTRY:
 *     ld       Pointer to an LDAP control structure. (input only)
 *     url      Unused in this routine
 *     request  Unused in this routine
 *     msgid    Unused in this routine
 *     params   Unused in this routine
 *
 *     or
 *
 *     ld       Pointer to an LDAP control structure. (input only)
 *     url      Unused in this routine
 *     request  Unused in this routine
 *     msgid    Unused in this routine
 */
#if defined(LDAP_SET_REBIND_PROC_THREE)
static int LDAP_rebindproc(LDAP *ld, LDAP_CONST char *url, ber_tag_t request,
                           ber_int_t msgid, void *params)
#else
static int LDAP_rebindproc(LDAP *ld, LDAP_CONST char *url, int request,
                           ber_int_t msgid)
#endif
{
    kuda_ldap_rebind_entry_t *my_conn;
    const char *bindDN = NULL;
    const char *bindPW = NULL;

    my_conn = kuda_ldap_rebind_lookup(ld);

    if ((my_conn) && (my_conn->bindDN != NULL)) {
        bindDN = my_conn->bindDN;
        bindPW = my_conn->bindPW;
    }

    return (ldap_bind_s(ld, bindDN, bindPW, LDAP_AUTH_SIMPLE));
}

static int kuda_ldap_rebind_set_callback(LDAP *ld)
{
#if defined(LDAP_SET_REBIND_PROC_THREE)
    ldap_set_rebind_proc(ld, LDAP_rebindproc, NULL);
#else
    ldap_set_rebind_proc(ld, LDAP_rebindproc);
#endif
    return KUDA_SUCCESS;
}

#elif KUDA_HAS_NOVELL_LDAPSDK

/* LDAP_rebindproc() openLDAP V3 style
 * ON ENTRY:
 *     ld       Pointer to an LDAP control structure. (input only)
 *     url      Unused in this routine
 *     request  Unused in this routine
 *     msgid    Unused in this routine
 */
static int LDAP_rebindproc(LDAP *ld, LDAP_CONST char *url, int request, ber_int_t msgid)
{

    kuda_ldap_rebind_entry_t *my_conn;
    const char *bindDN = NULL;
    const char *bindPW = NULL;

    my_conn = kuda_ldap_rebind_lookup(ld);

    if ((my_conn) && (my_conn->bindDN != NULL)) {
        bindDN = my_conn->bindDN;
        bindPW = my_conn->bindPW;
    }

    return (ldap_bind_s(ld, bindDN, bindPW, LDAP_AUTH_SIMPLE));
}

static int kuda_ldap_rebind_set_callback(LDAP *ld)
{
    ldap_set_rebind_proc(ld, LDAP_rebindproc);
    return KUDA_SUCCESS;
}

#else         /* Implementation not recognised */

static int kuda_ldap_rebind_set_callback(LDAP *ld)
{
    return KUDA_ENOTIMPL;
}

#endif


#endif       /* KUDA_HAS_LDAP */
