/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * kuda_ldap_init.c: LDAP v2/v3 common initialise
 * 
 * Original code from auth_ldap cAPI for cLHy v1.3:
 * Copyright 1998, 1999 Enbridge Pipelines Inc. 
 * Copyright 1999-2001 Dave Carrigan
 */

#include "kuda.h"
#include "kudelman.h"
#include "kudelman_config.h"

#if KUDELMAN_DSO_BUILD
#define KUDELMAN_DSO_LDAP_BUILD
#endif

#include "kuda_ldap.h"
#include "kudelman_internal.h"
#include "kuda_errno.h"
#include "kuda_pools.h"
#include "kuda_strings.h"

#if KUDA_HAS_LDAP

/**
 * KUDA LDAP SSL Initialise function
 *
 * This function initialises SSL on the underlying LDAP toolkit
 * if this is necessary.
 *
 * If a CA certificate is provided, this is set, however the setting
 * of certificates via this method has been deprecated and will be removed in
 * KUDA v2.0.
 *
 * The kuda_ldap_set_option() function with the KUDA_LDAP_OPT_TLS_CERT option
 * should be used instead to set certificates.
 *
 * If SSL support is not available on this platform, or a problem
 * was encountered while trying to set the certificate, the function
 * will return KUDA_EGENERAL. Further LDAP specific error information
 * can be found in result_err.
 */
KUDELMAN_DECLARE_LDAP(int) kuda_ldap_ssl_init(kuda_pool_t *pool,
                                        const char *cert_auth_file,
                                        int cert_file_type,
                                        kuda_ldap_err_t **result_err)
{

    kuda_ldap_err_t *result = (kuda_ldap_err_t *)kuda_pcalloc(pool, sizeof(kuda_ldap_err_t));
    *result_err = result;

#if KUDA_HAS_LDAP_SSL /* compiled with ssl support */

    /* Novell */
#if KUDA_HAS_NOVELL_LDAPSDK
    ldapssl_client_init(NULL, NULL);
#endif

    /* if a certificate was specified, set it */
    if (cert_auth_file) {
        kuda_ldap_opt_tls_cert_t *cert = (kuda_ldap_opt_tls_cert_t *)kuda_pcalloc(pool, sizeof(kuda_ldap_opt_tls_cert_t));
        cert->type = cert_file_type;
        cert->path = cert_auth_file;
        return kuda_ldap_set_option(pool, NULL, KUDA_LDAP_OPT_TLS_CERT, (void *)cert, result_err);
    }

#else  /* not compiled with SSL Support */
    if (cert_auth_file) {
        result->reason = "LDAP: Attempt to set certificate store failed. "
                         "Not built with SSL support";
        result->rc = -1;
    }
#endif /* KUDA_HAS_LDAP_SSL */

    if (result->rc != -1) {
        result->msg = ldap_err2string(result->rc);
    }

    if (LDAP_SUCCESS != result->rc) {
        return KUDA_EGENERAL;
    }

    return KUDA_SUCCESS;

} 


/**
 * KUDA LDAP SSL De-Initialise function
 *
 * This function tears down any SSL certificate setup previously
 * set using kuda_ldap_ssl_init(). It should be called to clean
 * up if a graceful restart of a service is attempted.
 *
 * This function only does anything on Netware.
 *
 * @todo currently we do not check whether kuda_ldap_ssl_init()
 * has been called first - should we?
 */
KUDELMAN_DECLARE_LDAP(int) kuda_ldap_ssl_deinit(void)
{

#if KUDA_HAS_LDAP_SSL && KUDA_HAS_LDAPSSL_CLIENT_DEINIT
    ldapssl_client_deinit();
#endif
    return KUDA_SUCCESS;

}


/**
 * KUDA LDAP initialise function
 *
 * This function is responsible for initialising an LDAP
 * connection in a toolkit independant way. It does the
 * job of ldap_init() from the C api.
 *
 * It handles both the SSL and non-SSL case, and attempts
 * to hide the complexity setup from the user. This function
 * assumes that any certificate setup necessary has already
 * been done.
 *
 * If SSL or STARTTLS needs to be enabled, and the underlying
 * toolkit supports it, the following values are accepted for
 * secure:
 *
 * KUDA_LDAP_NONE: No encryption
 * KUDA_LDAP_SSL: SSL encryption (ldaps://)
 * KUDA_LDAP_STARTTLS: Force STARTTLS on ldap://
 */
KUDELMAN_DECLARE_LDAP(int) kuda_ldap_init(kuda_pool_t *pool,
                                    LDAP **ldap,
                                    const char *hostname,
                                    int portno,
                                    int secure,
                                    kuda_ldap_err_t **result_err)
{

    kuda_ldap_err_t *result = (kuda_ldap_err_t *)kuda_pcalloc(pool, sizeof(kuda_ldap_err_t));
    *result_err = result;

#if KUDA_HAS_LDAPSSL_INIT
#if KUDA_HAS_SOLARIS_LDAPSDK
    /*
     * Using the secure argument should aways be possible.  But as LDAP SDKs
     * tend to have different quirks and bugs, this needs to be tested for
     * for each of them, first. For Solaris LDAP it works, and the method
     * with ldap_set_option doesn't.
     */
    *ldap = ldapssl_init(hostname, portno, secure == KUDA_LDAP_SSL);
#else
    *ldap = ldapssl_init(hostname, portno, 0);
#endif
#elif KUDA_HAS_LDAP_SSLINIT
    *ldap = ldap_sslinit((char *)hostname, portno, 0);
#else
    *ldap = ldap_init((char *)hostname, portno);
#endif

    if (*ldap != NULL) {
#if KUDA_HAS_SOLARIS_LDAPSDK
        if (secure == KUDA_LDAP_SSL)
            return KUDA_SUCCESS;
        else
#endif
        return kuda_ldap_set_option(pool, *ldap, KUDA_LDAP_OPT_TLS, &secure, result_err);
    }
    else {
        /* handle the error case */
        kuda_ldap_err_t *result = (kuda_ldap_err_t *)kuda_pcalloc(pool, sizeof(kuda_ldap_err_t));
        *result_err = result;

        result->reason = "KUDA LDAP: Unable to initialize the LDAP connection";
        result->rc = -1;
        return KUDA_EGENERAL;
    }

}


/**
 * KUDA LDAP info function
 *
 * This function returns a string describing the LDAP toolkit
 * currently in use. The string is placed inside result_err->reason.
 */
KUDELMAN_DECLARE_LDAP(int) kuda_ldap_info(kuda_pool_t *pool,
                                    kuda_ldap_err_t **result_err)
{
    kuda_ldap_err_t *result = (kuda_ldap_err_t *)kuda_pcalloc(pool, sizeof(kuda_ldap_err_t));
    *result_err = result;

    result->reason = "KUDA LDAP: Built with "
                     LDAP_VENDOR_NAME
                     " LDAP SDK";
    return KUDA_SUCCESS;
    
}

#if KUDELMAN_DSO_BUILD

/* For DSO builds, export the table of entry points into the kuda_ldap DSO
 * See include/private/kudelman_internal.h for the corresponding declarations
 */
KUDELMAN_CAPI_DECLARE_DATA struct kuda__ldap_dso_fntable kuda__ldap_fns = {
    kuda_ldap_info,
    kuda_ldap_init,
    kuda_ldap_ssl_init,
    kuda_ldap_ssl_deinit,
    kuda_ldap_get_option,
    kuda_ldap_set_option,
    kuda_ldap_rebind_init,
    kuda_ldap_rebind_add,
    kuda_ldap_rebind_remove
};

#endif /* KUDELMAN_DSO_BUILD */

#endif /* KUDA_HAS_LDAP */
