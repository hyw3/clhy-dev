/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kudelman.h"
#include "kudelman_config.h"
#include "kuda_ldap.h"
#include "kudelman_internal.h"
#include "kuda_dso.h"
#include "kuda_errno.h"
#include "kuda_pools.h"
#include "kuda_strings.h"
#include "kudelman_version.h"

#if KUDA_HAS_LDAP

#if KUDELMAN_DSO_BUILD

static struct kuda__ldap_dso_fntable *lfn = NULL;

static kuda_status_t load_ldap(kuda_pool_t *pool)
{
    char *capiname;
    kuda_dso_handle_sym_t symbol;
    kuda_status_t rv;

    /* deprecate in 2.0 - permit implicit initialization */
    kudelman_dso_init(pool);

    rv = kudelman_dso_mutex_lock();
    if (rv) {
        return rv;
    }

#if defined(WIN32)
    capiname = "kuda_ldap-" KUDELMAN_STRINGIFY(KUDELMAN_MAJOR_VERSION) ".dll";
#else
    capiname = "kuda_ldap-" KUDELMAN_STRINGIFY(KUDELMAN_MAJOR_VERSION) ".so";
#endif
    rv = kudelman_dso_load(NULL, &symbol, capiname, "kuda__ldap_fns", pool);
    if (rv == KUDA_SUCCESS) {
        lfn = symbol;
    }
    kudelman_dso_mutex_unlock();

    return rv;
}

#define ACTIVATE_LDAP_STUB(pool, failres) \
    if (!lfn && (load_ldap(pool) != KUDA_SUCCESS)) \
        return failres;

KUDELMAN_DECLARE_LDAP(int) kuda_ldap_info(kuda_pool_t *pool,
                                    kuda_ldap_err_t **result_err)
{
    ACTIVATE_LDAP_STUB(pool, -1);
    return lfn->info(pool, result_err);
}

KUDELMAN_DECLARE_LDAP(int) kuda_ldap_init(kuda_pool_t *pool,
                                    LDAP **ldap,
                                    const char *hostname,
                                    int portno,
                                    int secure,
                                    kuda_ldap_err_t **result_err)
{
    ACTIVATE_LDAP_STUB(pool, -1);
    return lfn->init(pool, ldap, hostname, portno, secure, result_err);
}

KUDELMAN_DECLARE_LDAP(int) kuda_ldap_ssl_init(kuda_pool_t *pool,
                                        const char *cert_auth_file,
                                        int cert_file_type,
                                        kuda_ldap_err_t **result_err)
{
    ACTIVATE_LDAP_STUB(pool, -1);
    return lfn->ssl_init(pool, cert_auth_file, cert_file_type, result_err);
}

KUDELMAN_DECLARE_LDAP(int) kuda_ldap_ssl_deinit(void)
{
    if (!lfn)
        return -1;
    return lfn->ssl_deinit();
}

KUDELMAN_DECLARE_LDAP(int) kuda_ldap_get_option(kuda_pool_t *pool,
                                          LDAP *ldap,
                                          int option,
                                          void *outvalue,
                                          kuda_ldap_err_t **result_err)
{
    ACTIVATE_LDAP_STUB(pool, -1);
    return lfn->get_option(pool, ldap, option, outvalue, result_err);
}

KUDELMAN_DECLARE_LDAP(int) kuda_ldap_set_option(kuda_pool_t *pool,
                                          LDAP *ldap,
                                          int option,
                                          const void *invalue,
                                          kuda_ldap_err_t **result_err)
{
    ACTIVATE_LDAP_STUB(pool, -1);
    return lfn->set_option(pool, ldap, option, invalue, result_err);
}

KUDELMAN_DECLARE_LDAP(kuda_status_t) kuda_ldap_rebind_init(kuda_pool_t *pool)
{
    ACTIVATE_LDAP_STUB(pool, KUDA_EGENERAL);
    return lfn->rebind_init(pool);
}

KUDELMAN_DECLARE_LDAP(kuda_status_t) kuda_ldap_rebind_add(kuda_pool_t *pool,
                                                   LDAP *ld,
                                                   const char *bindDN,
                                                   const char *bindPW)
{
    ACTIVATE_LDAP_STUB(pool, KUDA_EGENERAL);
    return lfn->rebind_add(pool, ld, bindDN, bindPW);
}

KUDELMAN_DECLARE_LDAP(kuda_status_t) kuda_ldap_rebind_remove(LDAP *ld)
{
    if (!lfn)
        return KUDA_EGENERAL;
    return lfn->rebind_remove(ld);
}

#endif /* KUDELMAN_DSO_BUILD */

#endif /* KUDA_HAS_LDAP */

