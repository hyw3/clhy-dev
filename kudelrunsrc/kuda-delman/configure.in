dnl
dnl Process this file with autoconf to produce a configure script
dnl

AC_PREREQ(2.59)
AC_INIT(export_vars.sh.in)

AC_CONFIG_HEADER(include/private/kudelman_config.h)
AC_CONFIG_AUX_DIR(build)

sinclude(build/kudelman-conf.m4)
sinclude(build/kudelman-iconv.m4)
sinclude(build/kudelman-hints.m4)
sinclude(build/kuda_common.m4)
sinclude(build/find_kuda.m4)
sinclude(build/crypto.m4)
sinclude(build/dbm.m4)
sinclude(build/dbd.m4)
sinclude(build/dso.m4)

dnl Generate ./plv.upgrade for patch level upgrade and keep run options of configure
dnl 
KUDA_PATCHLEVEL_UPGRADE(plv.upgrade)

dnl # Some initial steps for configuration.  We setup the default directory
dnl # and which files are to be configured.

dnl Absolute source/build directory
abs_srcdir=`(cd $srcdir && pwd)`
abs_builddir=`pwd`

if test "$abs_builddir" != "$abs_srcdir"; then
  USE_VPATH=1
  KUDELMAN_CONFIG_LOCATION=build
else
  KUDELMAN_CONFIG_LOCATION=source
fi

AC_SUBST(KUDELMAN_CONFIG_LOCATION)

AC_CANONICAL_SYSTEM

AC_PROG_INSTALL

# Use -no-install or -no-fast-install to link the test 
# programs on all platforms but Darwin, where it would cause
# the programs to be linked against installed versions of
# libkuda instead of those just built.
case $host in
    *-apple-darwin*)
        LT_NO_INSTALL=""
        ;;
    *-mingw*)
        LT_NO_INSTALL="-no-fast-install"
        ;;
    *)
        LT_NO_INSTALL="-no-install"
        ;;
esac
AC_SUBST(LT_NO_INSTALL)

dnl
dnl compute the top directory of the build
dnl note: this is needed for LIBTOOL and exporting the bundled Expat
dnl
top_builddir="$abs_builddir"
AC_SUBST(top_builddir)
AC_SUBST(abs_srcdir)
AC_SUBST(abs_builddir)

dnl Initialize mkdir -p functionality.
KUDA_MKDIR_P_CHECK($abs_srcdir/build/mkdir.sh)

dnl get our version information
get_version="$abs_srcdir/build/get-version.sh"
version_hdr="$abs_srcdir/include/kudelman_version.h"
KUDADELMAN_MAJOR_VERSION="`$get_version major $version_hdr KUDELMAN`"
KUDADELMAN_DOTTED_VERSION="`$get_version all $version_hdr KUDELMAN`"

KUDELMAN_LTVERSION="-version-info `$get_version libtool $version_hdr KUDELMAN`"

AC_SUBST(KUDADELMAN_DOTTED_VERSION)
AC_SUBST(KUDADELMAN_MAJOR_VERSION)
AC_SUBST(KUDELMAN_LTVERSION)

echo "Kuda-Delman Version: ${KUDADELMAN_DOTTED_VERSION}"

dnl Enable the layout handling code, then reparse the prefix-style
dnl arguments due to autoconf being a PITA.
KUDA_ENABLE_LAYOUT(kuda-delman)
KUDA_PARSE_ARGUMENTS

dnl load platforms-specific hints for kuda-delman
KUDELMAN_PRELOAD

dnl
dnl set up the compilation flags and stuff
dnl

KUDADELMAN_INCLUDES=""
KUDADELMAN_PRIV_INCLUDES="-I$top_builddir/include -I$top_builddir/include/private"
if test -n "$USE_VPATH"; then
    KUDADELMAN_PRIV_INCLUDES="$KUDADELMAN_PRIV_INCLUDES -I$abs_srcdir/include/private -I$abs_srcdir/include"
fi

dnl
dnl Find the KUDA includes directory and (possibly) the source (base) dir.
dnl
KUDELMAN_FIND_KUDA

dnl
dnl even though we use kuda_rules.mk for building kuda-delman, we need
dnl to grab CC and CPP ahead of time so that kuda-delman config tests
dnl use the same compiler as KUDA; we need the same compiler options
dnl and feature test macros as well
dnl
KUDA_SETIFNULL(CC, `$kuda_config --cc`)
KUDA_SETIFNULL(CPP, `$kuda_config --cpp`)
KUDA_ADDTO(CFLAGS, `$kuda_config --cflags`)
KUDA_ADDTO(CPPFLAGS, `$kuda_config --cppflags`)
kuda_shlibpath_var=`$kuda_config --shlib-path-var`
AC_SUBST(kuda_shlibpath_var)

dnl
dnl  Find the KUDA-ICONV directory.
dnl
AC_ARG_WITH(kuda-iconv,
            [  --with-kuda-iconv=DIR    relative path to kuda-iconv source],
  [ kudelman_kudaiconv_dir="$withval"
    if test "$kudelman_kudaiconv_dir" != "no"; then
      if test -d "$kudelman_kudaiconv_dir"; then
        KUDA_SUBDIR_CONFIG("$kudelman_kudaiconv_dir",
                          [$clhy_kuda_flags \
                             --prefix=$prefix \
                             --exec-prefix=$exec_prefix \
                             --libdir=$libdir \
                             --includedir=$includedir \
                             --bindir=$bindir \
                             --datadir=$datadir \
                             --with-installbuilddir=$installbuilddir],
                          [--enable-layout=*|\'--enable-layout=*])
        KUDADELMAN_EXPORT_LIBS="$abs_srcdir/$kudelman_kudaiconv_dir/lib/libkudaiconv.la \
                             $KUDADELMAN_EXPORT_LIBS"
        KUDADELMAN_INCLUDES="-I$abs_srcdir/$kudelman_kudaiconv_dir/include \
                          $KUDADELMAN_INCLUDES"
        KUDA_ICONV_DIR="$kudelman_kudaiconv_dir"
      else
       KUDA_ICONV_DIR=""
      fi
    else
      KUDA_ICONV_DIR=""
    fi
  ])
AC_SUBST(KUDA_ICONV_DIR)

dnl Find LDAP library
dnl Determine what DBM backend type to use.
dnl Find Expat
dnl Find an iconv library
KUDELMAN_CHECK_CRYPTO
KUDELMAN_FIND_LDAP
KUDELMAN_CHECK_DBM
KUDELMAN_CHECK_DBD
KUDELMAN_CHECK_DBD_MYSQL
KUDELMAN_CHECK_DBD_SQLITE3
KUDELMAN_CHECK_DBD_SQLITE2
KUDELMAN_CHECK_DBD_ORACLE
KUDELMAN_CHECK_DBD_ODBC
KUDELMAN_FIND_EXPAT
KUDELMAN_FIND_ICONV

dnl Enable DSO build; must be last:
KUDELMAN_CHECK_UTIL_DSO

AC_SEARCH_LIBS(crypt, crypt ufc)
AC_MSG_CHECKING(if system crypt() function is threadsafe)
if test "x$kudelman_crypt_threadsafe" = "x1"; then
  AC_DEFINE(KUDELMAN_CRYPT_THREADSAFE, 1, [Define if the system crypt() function is threadsafe])
  msg="yes"
else
  msg="no"
fi
AC_MSG_RESULT([$msg])

AC_CHECK_FUNCS(crypt_r, [ crypt_r="1" ], [ crypt_r="0" ])
if test "$crypt_r" = "1"; then
  KUDELMAN_CHECK_CRYPT_R_STYLE
fi

AC_CACHE_CHECK([whether the compiler handles weak symbols], [kudelman_cv_weak_symbols],
[AC_TRY_RUN([
__attribute__ ((weak))
int weak_noop(void)
{
    return 0;
}
int main()
{
    return weak_noop();
}], [kudelman_cv_weak_symbols=yes], [kudelman_cv_weak_symbols=no], [kudelman_cv_weak_symbols=no])])

if test "$kudelman_cv_weak_symbols" = "yes"; then
    AC_DEFINE(HAVE_WEAK_SYMBOLS, 1, [Define if compiler handles weak symbols])
fi

AC_CACHE_CHECK([for memset_s support], [kudelman_cv_memset_s],
[AC_TRY_RUN([
#ifdef HAVE_STRING_H
#define __STDC_WANT_LIB_EXT1__ 1
#include <string.h>
#endif

int main(int argc, const char **argv)
{
    char buf[1] = {1};
    return memset_s(buf, sizeof buf, 0, sizeof buf) != 0 || *buf != '\0';
}], [kudelman_cv_memset_s=yes], [kudelman_cv_memset_s=no], [kudelman_cv_memset_s=no])])

if test "$kudelman_cv_memset_s" = "yes"; then
   AC_DEFINE([HAVE_MEMSET_S], 1, [Define if memset_s function is supported])
fi

AC_CACHE_CHECK([for explicit_bzero support], [kudelman_cv_explicit_bzero],
[AC_TRY_RUN([
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif

int main(int argc, const char **argv)
{
    char buf[1] = {1};
    explicit_bzero(buf, sizeof buf);
    return *buf != '\0';
}], [kudelman_cv_explicit_bzero=yes], [kudelman_cv_explicit_bzero=no], [kudelman_cv_explicit_bzero=no])])

if test "$kudelman_cv_explicit_bzero" = "yes"; then
   AC_DEFINE([HAVE_EXPLICIT_BZERO], 1, [Define if explicit_bzero function is supported])
fi

so_ext=$KUDA_SO_EXT
lib_target=$KUDA_LIB_TARGET
AC_SUBST(so_ext)
AC_SUBST(lib_target)

KUDADELMAN_LIBNAME="kudadelman${libsuffix}"
AC_SUBST(KUDADELMAN_LIBNAME)

# Set up destination directory for DSOs.
KUDELMAN_DSO_LIBDIR="\${libdir}/kuda-delman-${KUDADELMAN_MAJOR_VERSION}"
# Set KUDELMAN_HAVE_CAPIS appropriately for the Makefile
if test -n "$KUDELMAN_CAPIS"; then 
   KUDELMAN_HAVE_CAPIS=yes
else
   KUDELMAN_HAVE_CAPIS=no
fi
# Define expanded libdir for kudelman_config.h
KUDA_EXPAND_VAR(abs_dso_libdir, $KUDELMAN_DSO_LIBDIR)
AC_DEFINE_UNQUOTED([KUDELMAN_DSO_LIBDIR], ["$abs_dso_libdir"],
                   [Define to be absolute path to DSO directory])
AC_SUBST(KUDELMAN_HAVE_CAPIS)
AC_SUBST(KUDELMAN_DSO_LIBDIR)
AC_SUBST(KUDELMAN_CAPIS)
AC_SUBST(EXTRA_OBJECTS)

dnl
dnl Prep all the flags and stuff for compilation and export to other builds
dnl
KUDA_ADDTO(KUDADELMAN_LIBS, [$KUDA_LIBS])

AC_SUBST(KUDADELMAN_EXPORT_LIBS)
AC_SUBST(KUDADELMAN_PRIV_INCLUDES)
AC_SUBST(KUDADELMAN_INCLUDES)
AC_SUBST(KUDADELMAN_LDFLAGS)
AC_SUBST(KUDADELMAN_LIBS)
AC_SUBST(LDFLAGS)

dnl copy kuda's rules.mk into our build directory.
if test ! -d ./build; then
   $mkdir_p build
fi
dnl
dnl MinGW: If KUDA is shared, KUDA_DECLARE_EXPORT will be defined in the
dnl        internal CPPFLAGS, but Kuda-Delman needs KUDELMAN_DECLARE_EXPORT instead.
dnl        If KUDA is static, KUDA_DECLARE_STATIC will be defined in the
dnl        internal CPPFLAGS, but Kuda-Delman needs KUDELMAN_DECLARE_STATIC too.
dnl
case $host in
    *-mingw*)
        sed -e 's/-DKUDA_DECLARE_EXPORT/-DKUDELMAN_DECLARE_EXPORT/' \
            -e 's/-DKUDA_DECLARE_STATIC/-DKUDELMAN_DECLARE_STATIC -DKUDA_DECLARE_STATIC/' \
            < $KUDA_BUILD_DIR/kuda_rules.mk > $abs_builddir/build/rules.mk
        ;;
    *)
        cp $KUDA_BUILD_DIR/kuda_rules.mk $abs_builddir/build/rules.mk
        ;;
esac

dnl
dnl BSD/PLATFORM (BSDi) needs to use a different include syntax in the Makefiles
dnl
case "$host_alias" in
*bsdi* | BSD/PLATFORM)
    # Check whether they've installed GNU make
    if make --version > /dev/null 2>&1; then
        INCLUDE_RULES="include $abs_builddir/build/rules.mk"
        INCLUDE_OUTPUTS="include $abs_srcdir/build-outputs.mk"
    else
        INCLUDE_RULES=".include \"$abs_builddir/build/rules.mk\""
        INCLUDE_OUTPUTS=".include \"$abs_srcdir/build-outputs.mk\""
    fi
    ;;
*)
    INCLUDE_RULES="include $abs_builddir/build/rules.mk"
    INCLUDE_OUTPUTS="include $abs_srcdir/build-outputs.mk"
    ;;
esac
AC_SUBST(INCLUDE_RULES)
AC_SUBST(INCLUDE_OUTPUTS)

for d in include include/private; do
    test -d $top_builddir/$d || mkdir $top_builddir/$d
done

AC_CONFIG_FILES([Makefile export_vars.sh
                 build/pkg/pkginfo kuda-delman.pc
                 kudelman-$KUDADELMAN_MAJOR_VERSION-config:kudelman-config.in
                 include/private/kudelman_select_dbm.h
                 include/kuda_ldap.h
                 include/kudelman.h include/kudelman_want.h])

AC_CONFIG_COMMANDS([default], [
chmod +x kudelman-$KUDADELMAN_MAJOR_VERSION-config
],[
KUDADELMAN_MAJOR_VERSION=$KUDADELMAN_MAJOR_VERSION
])

if test -d $srcdir/test; then
    AC_CONFIG_FILES([test/Makefile])
fi

AC_OUTPUT
