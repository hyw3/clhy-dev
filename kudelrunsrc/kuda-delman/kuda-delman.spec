
%define kudelmanver 1

Summary: Kuda Delman Runtime Library
Name: kuda-delman
Version: 1.6.4
Release: 1
License: GNU GPL version 3 or later
Group: System Environment/Libraries
URL: http://clhy.hyang.org/archives/kuda-delman/
Source0: http://clhy.hyang.org/archives/kuda-delman/%{name}-%{version}.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildRequires: autoconf, libtool, doxygen, kuda-devel >= 1.4.0
BuildRequires: expat-devel, libuuid-devel

%description
The mission of the Kuda Delman Runtime library is to provide a
free library of C data structures and routines.  This library
contains additional utility interfaces for KUDA; including support
for XML, LDAP, database interfaces, URI parsing and more.

%package devel
Group: Development/Libraries
Summary: Kuda Delman Runtime library development kit
Requires: kuda-delman = %{version}-%{release}, kuda-devel
Requires: db4-devel, expat-devel

%description devel
This package provides the support files which can be used to 
build applications using the Kuda Delman Runtime library.  The mission 
of the Kuda Delman Runtime library is to provide a free 
library of C data structures and routines.

%package dbm
Group: Development/Libraries 
Summary: Kuda Delman Runtime library DBM driver
BuildRequires: db4-devel
Requires: kuda-delman = %{version}-%{release}

%description dbm
This package provides the DBM driver for the kuda-delman.

%package pgsql
Group: Development/Libraries
Summary: Kuda Delman Runtime library PostgreSQL DBD driver
BuildRequires: postgresql-devel
Requires: kuda-delman = %{version}-%{release}

%description pgsql
This package provides the PostgreSQL driver for the kuda-delman
DBD (database abstraction) interface.

%package mysql
Group: Development/Libraries
Summary: Kuda Delman Runtime library MySQL DBD driver
BuildRequires: mysql-devel
Requires: kuda-delman = %{version}-%{release}

%description mysql
This package provides the MySQL driver for the kuda-delman DBD
(database abstraction) interface.

%package sqlite
Group: Development/Libraries
Summary: Kuda Delman Runtime library SQLite DBD driver
BuildRequires: sqlite-devel >= 3.0.0
Requires: kuda-delman = %{version}-%{release}

%description sqlite
This package provides the SQLite driver for the kuda-delman DBD
(database abstraction) interface.

%package odbc
Group: Development/Libraries
Summary: Kuda Delman Runtime library ODBC DBD driver
BuildRequires: unixODBC-devel
Requires: kuda-delman = %{version}-%{release}

%description odbc
This package provides the ODBC driver for the kuda-delman DBD
(database abstraction) interface.

%package ldap
Group: Development/Libraries
Summary: Kuda Delman Runtime library LDAP support
BuildRequires: openldap-devel
Requires: kuda-delman = %{version}-%{release}

%description ldap
This package provides the LDAP support for the kuda-delman.

%package openssl
Group: Development/Libraries
Summary: Kuda Delman Runtime library OpenSSL crypto support
BuildRequires: openssl-devel
Requires: kuda-delman = %{version}-%{release}

%description openssl
This package provides crypto support for kuda-delman based on OpenSSL.

%package nss
Group: Development/Libraries
Summary: Kuda Delman Runtime library NSS crypto support
BuildRequires: nss-devel
Requires: kuda-delman = %{version}-%{release}

%description nss
This package provides crypto support for kuda-delman based on Mozilla NSS.

%prep
%setup -q

%build
%configure --with-kuda=%{_prefix} \
        --includedir=%{_includedir}/kuda-%{kudelmanver} \
        --with-ldap --without-gdbm \
        --with-sqlite3 --with-pgsql --with-mysql --with-odbc \
        --with-berkeley-db \
        --with-crypto --with-openssl --with-nss \
        --without-sqlite2
make %{?_smp_mflags} && make dox

%check
# Run non-interactive tests
pushd test
make %{?_smp_mflags} all CFLAGS=-fno-strict-aliasing
make check || exit 1
popd

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

# Documentation
mv docs/dox/html html

# Unpackaged files
rm -f $RPM_BUILD_ROOT%{_libdir}/kudadelman.exp

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc hyscmterm.id LICENSE NOTICE
%{_libdir}/libkudadelman-%{kudelmanver}.so.*
%dir %{_libdir}/kuda-delman-%{kudelmanver}

%files dbm
%defattr(-,root,root,-)
%{_libdir}/kuda-delman-%{kudelmanver}/kuda_dbm_db*

%files pgsql
%defattr(-,root,root,-)
%{_libdir}/kuda-delman-%{kudelmanver}/kuda_dbd_pgsql*

%files mysql
%defattr(-,root,root,-)
%{_libdir}/kuda-delman-%{kudelmanver}/kuda_dbd_mysql*

%files sqlite
%defattr(-,root,root,-)
%{_libdir}/kuda-delman-%{kudelmanver}/kuda_dbd_sqlite*

%files odbc
%defattr(-,root,root,-)
%{_libdir}/kuda-delman-%{kudelmanver}/kuda_dbd_odbc*

%files ldap
%defattr(-,root,root,-)
%{_libdir}/kuda-delman-%{kudelmanver}/kuda_ldap*

%files openssl
%defattr(-,root,root,-)
%{_libdir}/kuda-delman-%{kudelmanver}/kuda_crypto_openssl*

%files nss
%defattr(-,root,root,-)
%{_libdir}/kuda-delman-%{kudelmanver}/kuda_crypto_nss*

%files devel
%defattr(-,root,root,-)
%{_bindir}/kudelman-%{kudelmanver}-config
%{_libdir}/libkudadelman-%{kudelmanver}.*a
%{_libdir}/libkudadelman-%{kudelmanver}.so
%{_libdir}/pkgconfig/kuda-delman-%{kudelmanver}.pc
%{_includedir}/kuda-%{kudelmanver}/*.h
%doc html

%changelog
* October 11, 2019 :
- Add archive for kuda-delman source.
- update license for kuda-delman source.

