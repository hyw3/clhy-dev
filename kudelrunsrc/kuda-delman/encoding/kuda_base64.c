/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* 
 * base64 encoder/decoder used for cLBench and kuda_sha1.c
 *
 */

#include "kuda_base64.h"
#if KUDA_CHARSET_EBCDIC
#include "kuda_xlate.h"
#endif				/* KUDA_CHARSET_EBCDIC */

/* aaaack but it's fast and const should make it shared text page. */
static const unsigned char pr2six[256] =
{
#if !KUDA_CHARSET_EBCDIC
    /* ASCII table */
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 62, 64, 64, 64, 63,
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 64, 64, 64, 64, 64, 64,
    64,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 64, 64, 64, 64, 64,
    64, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64
#else /*KUDA_CHARSET_EBCDIC*/
    /* EBCDIC table */
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 62, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 63, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 26, 27, 28, 29, 30, 31, 32, 33, 34, 64, 64, 64, 64, 64, 64,
    64, 35, 36, 37, 38, 39, 40, 41, 42, 43, 64, 64, 64, 64, 64, 64,
    64, 64, 44, 45, 46, 47, 48, 49, 50, 51, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64,  0,  1,  2,  3,  4,  5,  6,  7,  8, 64, 64, 64, 64, 64, 64,
    64,  9, 10, 11, 12, 13, 14, 15, 16, 17, 64, 64, 64, 64, 64, 64,
    64, 64, 18, 19, 20, 21, 22, 23, 24, 25, 64, 64, 64, 64, 64, 64,
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 64, 64, 64, 64, 64, 64
#endif /*KUDA_CHARSET_EBCDIC*/
};

#if KUDA_CHARSET_EBCDIC
static kuda_xlate_t *xlate_to_ebcdic;
static unsigned char platform_toascii[256];

KUDELMAN_DECLARE(kuda_status_t) kuda_base64init_ebcdic(kuda_xlate_t *to_ascii,
                                             kuda_xlate_t *to_ebcdic)
{
    int i;
    kuda_size_t inbytes_left, outbytes_left;
    kuda_status_t rv;
    int onoff;
    
    /* Only single-byte conversion is supported.
     */
    rv = kuda_xlate_sb_get(to_ascii, &onoff);
    if (rv) {
        return rv;
    }
    if (!onoff) { /* If conversion is not single-byte-only */
        return KUDA_EINVAL;
    }
    rv = kuda_xlate_sb_get(to_ebcdic, &onoff);
    if (rv) {
        return rv;
    }
    if (!onoff) { /* If conversion is not single-byte-only */
        return KUDA_EINVAL;
    }
    xlate_to_ebcdic = to_ebcdic;
    for (i = 0; i < sizeof(platform_toascii); i++) {
        platform_toascii[i] = i;
    }
    inbytes_left = outbytes_left = sizeof(platform_toascii);
    kuda_xlate_conv_buffer(to_ascii, platform_toascii, &inbytes_left,
                          platform_toascii, &outbytes_left);

    return KUDA_SUCCESS;
}
#endif /*KUDA_CHARSET_EBCDIC*/

KUDELMAN_DECLARE(int) kuda_base64_decode_len(const char *bufcoded)
{
    int nbytesdecoded;
    register const unsigned char *bufin;
    register kuda_size_t nprbytes;

    bufin = (const unsigned char *) bufcoded;
    while (pr2six[*(bufin++)] <= 63);

    nprbytes = (bufin - (const unsigned char *) bufcoded) - 1;
    nbytesdecoded = (((int)nprbytes + 3) / 4) * 3;

    return nbytesdecoded + 1;
}

KUDELMAN_DECLARE(int) kuda_base64_decode(char *bufplain, const char *bufcoded)
{
#if KUDA_CHARSET_EBCDIC
    kuda_size_t inbytes_left, outbytes_left;
#endif				/* KUDA_CHARSET_EBCDIC */
    int len;
    
    len = kuda_base64_decode_binary((unsigned char *) bufplain, bufcoded);
#if KUDA_CHARSET_EBCDIC
    inbytes_left = outbytes_left = len;
    kuda_xlate_conv_buffer(xlate_to_ebcdic, bufplain, &inbytes_left,
                          bufplain, &outbytes_left);
#endif				/* KUDA_CHARSET_EBCDIC */
    bufplain[len] = '\0';
    return len;
}

/* This is the same as kuda_base64_decode() except on EBCDIC machines, where
 * the conversion of the output to ebcdic is left out.
 */
KUDELMAN_DECLARE(int) kuda_base64_decode_binary(unsigned char *bufplain,
				   const char *bufcoded)
{
    int nbytesdecoded;
    register const unsigned char *bufin;
    register unsigned char *bufout;
    register kuda_size_t nprbytes;

    bufin = (const unsigned char *) bufcoded;
    while (pr2six[*(bufin++)] <= 63);
    nprbytes = (bufin - (const unsigned char *) bufcoded) - 1;
    nbytesdecoded = (((int)nprbytes + 3) / 4) * 3;

    bufout = (unsigned char *) bufplain;
    bufin = (const unsigned char *) bufcoded;

    while (nprbytes > 4) {
	*(bufout++) =
	    (unsigned char) (pr2six[*bufin] << 2 | pr2six[bufin[1]] >> 4);
	*(bufout++) =
	    (unsigned char) (pr2six[bufin[1]] << 4 | pr2six[bufin[2]] >> 2);
	*(bufout++) =
	    (unsigned char) (pr2six[bufin[2]] << 6 | pr2six[bufin[3]]);
	bufin += 4;
	nprbytes -= 4;
    }

    /* Note: (nprbytes == 1) would be an error, so just ingore that case */
    if (nprbytes > 1) {
	*(bufout++) =
	    (unsigned char) (pr2six[*bufin] << 2 | pr2six[bufin[1]] >> 4);
    }
    if (nprbytes > 2) {
	*(bufout++) =
	    (unsigned char) (pr2six[bufin[1]] << 4 | pr2six[bufin[2]] >> 2);
    }
    if (nprbytes > 3) {
	*(bufout++) =
	    (unsigned char) (pr2six[bufin[2]] << 6 | pr2six[bufin[3]]);
    }

    nbytesdecoded -= (4 - (int)nprbytes) & 3;
    return nbytesdecoded;
}

static const char basis_64[] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

KUDELMAN_DECLARE(int) kuda_base64_encode_len(int len)
{
    return ((len + 2) / 3 * 4) + 1;
}

KUDELMAN_DECLARE(int) kuda_base64_encode(char *encoded, const char *string, int len)
{
#if !KUDA_CHARSET_EBCDIC
    return kuda_base64_encode_binary(encoded, (const unsigned char *) string, len);
#else /* KUDA_CHARSET_EBCDIC */
    int i;
    char *p;

    p = encoded;
    for (i = 0; i < len - 2; i += 3) {
	*p++ = basis_64[(platform_toascii[string[i]] >> 2) & 0x3F];
	*p++ = basis_64[((platform_toascii[string[i]] & 0x3) << 4) |
	                ((int) (platform_toascii[string[i + 1]] & 0xF0) >> 4)];
	*p++ = basis_64[((platform_toascii[string[i + 1]] & 0xF) << 2) |
	                ((int) (platform_toascii[string[i + 2]] & 0xC0) >> 6)];
	*p++ = basis_64[platform_toascii[string[i + 2]] & 0x3F];
    }
    if (i < len) {
	*p++ = basis_64[(platform_toascii[string[i]] >> 2) & 0x3F];
	if (i == (len - 1)) {
	    *p++ = basis_64[((platform_toascii[string[i]] & 0x3) << 4)];
	    *p++ = '=';
	}
	else {
	    *p++ = basis_64[((platform_toascii[string[i]] & 0x3) << 4) |
	                    ((int) (platform_toascii[string[i + 1]] & 0xF0) >> 4)];
	    *p++ = basis_64[((platform_toascii[string[i + 1]] & 0xF) << 2)];
	}
	*p++ = '=';
    }

    *p++ = '\0';
    return p - encoded;
#endif				/* KUDA_CHARSET_EBCDIC */
}

/* This is the same as kuda_base64_encode() except on EBCDIC machines, where
 * the conversion of the input to ascii is left out.
 */
KUDELMAN_DECLARE(int) kuda_base64_encode_binary(char *encoded,
                                      const unsigned char *string, int len)
{
    int i;
    char *p;

    p = encoded;
    for (i = 0; i < len - 2; i += 3) {
	*p++ = basis_64[(string[i] >> 2) & 0x3F];
	*p++ = basis_64[((string[i] & 0x3) << 4) |
	                ((int) (string[i + 1] & 0xF0) >> 4)];
	*p++ = basis_64[((string[i + 1] & 0xF) << 2) |
	                ((int) (string[i + 2] & 0xC0) >> 6)];
	*p++ = basis_64[string[i + 2] & 0x3F];
    }
    if (i < len) {
	*p++ = basis_64[(string[i] >> 2) & 0x3F];
	if (i == (len - 1)) {
	    *p++ = basis_64[((string[i] & 0x3) << 4)];
	    *p++ = '=';
	}
	else {
	    *p++ = basis_64[((string[i] & 0x3) << 4) |
	                    ((int) (string[i + 1] & 0xF0) >> 4)];
	    *p++ = basis_64[((string[i + 1] & 0xF) << 2)];
	}
	*p++ = '=';
    }

    *p++ = '\0';
    return (int)(p - encoded);
}
