# Microsoft Developer Studio Project File - Name="prekudadelman" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=prekudadelman - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "prekudadelman.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "prekudadelman.mak" CFG="prekudadelman - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "prekudadelman - Win32 Release" (based on "Win32 (x86) External Target")
!MESSAGE "prekudadelman - Win32 Debug" (based on "Win32 (x86) External Target")
!MESSAGE "prekudadelman - x64 Release" (based on "Win32 (x86) External Target")
!MESSAGE "prekudadelman - x64 Debug" (based on "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "prekudadelman - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prekudadelman.exe"
# PROP BASE Bsc_Name "prekudadelman.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "prekudadelman - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prekudadelman.exe"
# PROP BASE Bsc_Name "prekudadelman.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "prekudadelman - x64 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prekudadelman.exe"
# PROP BASE Bsc_Name "prekudadelman.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "prekudadelman - x64 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /nologo /f NUL"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "prekudadelman.exe"
# PROP BASE Bsc_Name "prekudadelman.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /nologo /f NUL"
# PROP Rebuild_Opt "/a"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "prekudadelman - Win32 Release"
# Name "prekudadelman - Win32 Debug"
# Name "prekudadelman - x64 Release"
# Name "prekudadelman - x64 Debug"

!IF  "$(CFG)" == "prekudadelman - Win32 Release"

!ELSEIF  "$(CFG)" == "prekudadelman - Win32 Debug"

!ELSEIF  "$(CFG)" == "prekudadelman - x64 Release"

!ELSEIF  "$(CFG)" == "prekudadelman - x64 Debug"

!ENDIF 

# End Target
# End Project
