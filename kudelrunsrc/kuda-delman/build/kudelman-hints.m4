dnl -------------------------------------------------------- -*- autoconf -*-
dnl Licensed to the Hyang Language Foundation (HLF) under one or more
dnl contributor license agreements.  See the NOTICE file distributed with
dnl this work for additional information regarding copyright ownership.
dnl The HLF licenses this file to You under the GNU GPL Version 3 or later
dnl (the "License"); you may not use this file except in compliance with
dnl the License.  You may obtain a copy of the License at
dnl
dnl     http://clhy.hyang.org/license.hyss
dnl
dnl Unless required by applicable law or agreed to in writing, software
dnl distributed under the License is distributed on an "AS IS" BASIS,
dnl WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
dnl See the License for the specific language governing permissions and
dnl limitations under the License.

dnl -----------------------------------------------------------------
dnl kudelman-hints.m4: kuda-delman's autoconf macros for platform-specific hints
dnl
dnl  We preload various configure settings depending
dnl  on previously obtained platform knowledge.
dnl  We allow all settings to be overridden from
dnl  the command-line.

dnl
dnl KUDELMAN_PRELOAD
dnl
dnl  Preload various build parameters based on outside knowledge.
dnl
AC_DEFUN([KUDELMAN_PRELOAD], [
if test "x$kudelman_preload_done" != "xyes" ; then
    kudelman_preload_done="yes"

    echo "Applying kuda-delman hints file rules for $host"

    case "$host" in
    *-dec-osf*)
        KUDA_SETIFNULL(kudelman_crypt_threadsafe, [1])
        ;;
    *-hp-hpux11.*)
        KUDA_SETIFNULL(kudelman_crypt_threadsafe, [1])
        ;;
    *-ibm-aix4*|*-ibm-aix5.1*)
        KUDA_SETIFNULL(kudelman_iconv_inbuf_const, [1])
        ;;
    *-ibm-os390)
        KUDA_SETIFNULL(kudelman_crypt_threadsafe, [1])
        ;;
    *-solaris2*)
        KUDA_SETIFNULL(kudelman_iconv_inbuf_const, [1])
        KUDA_SETIFNULL(kudelman_crypt_threadsafe, [1])
        AC_SEARCH_LIBS(fdatasync, [rt posix4])
        ;;
    *-sco3.2v5*)
	KUDA_SETIFNULL(kudelman_db_xtra_libs, [-lsocket])
	;;
    esac

fi
])


