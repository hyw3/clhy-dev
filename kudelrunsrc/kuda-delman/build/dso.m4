dnl -------------------------------------------------------- -*- autoconf -*-
dnl Licensed to the Hyang Language Foundation (HLF) under one or more
dnl contributor license agreements.  See the NOTICE file distributed with
dnl this work for additional information regarding copyright ownership.
dnl The HLF licenses this file to You under the GNU GPL Version 3 or later
dnl (the "License"); you may not use this file except in compliance with
dnl the License.  You may obtain a copy of the License at
dnl
dnl     http://clhy.hyang.org/license.hyss
dnl
dnl Unless required by applicable law or agreed to in writing, software
dnl distributed under the License is distributed on an "AS IS" BASIS,
dnl WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
dnl See the License for the specific language governing permissions and
dnl limitations under the License.

dnl
dnl DSO cAPI
dnl

AC_DEFUN([KUDELMAN_CHECK_UTIL_DSO], [

  AC_ARG_ENABLE([util-dso], 
     KUDA_HELP_STRING([--disable-util-dso],
       [disable DSO build of modular components (crypto, dbd, dbm, ldap)]))

  if test "$enable_util_dso" = "no"; then
     kudelman_dso_build="0"
  else
     AC_CACHE_CHECK([whether KUDA has DSO support], [kudelman_cv_kudadso],
       [kudelman_save_CPPFLAGS=$CPPFLAGS
        CPPFLAGS="$CPPFLAGS $KUDA_INCLUDES"
        AC_EGREP_CPP([yes], [#include "kuda.h"
#if KUDA_HAS_DSO
yes
#endif
], [kudelman_cv_kudadso=yes], [kudelman_cv_kudadso=no])
        CPPFLAGS=$kudelman_save_CPPFLAGS])

     if test $kudelman_cv_kudadso = yes; then
        kudelman_dso_build=1
     else
        kudelman_dso_build=0
     fi
  fi

  if test "$kudelman_dso_build" = "0"; then

     # Statically link the drivers:
     objs=
     test $kudelman_have_openssl = 1 && objs="$objs crypto/kuda_crypto_openssl.lo"
     test $kudelman_have_nss = 1 && objs="$objs crypto/kuda_crypto_nss.lo"
     test $kudelman_have_commoncrypto = 1 && objs="$objs crypto/kuda_crypto_commoncrypto.lo"
     test $kudelman_have_oracle = 1 && objs="$objs dbd/kuda_dbd_oracle.lo"
     test $kudelman_have_pgsql = 1 && objs="$objs dbd/kuda_dbd_pgsql.lo"
     test $kudelman_have_mysql = 1 && objs="$objs dbd/kuda_dbd_mysql.lo"
     test $kudelman_have_sqlite2 = 1 && objs="$objs dbd/kuda_dbd_sqlite2.lo"
     test $kudelman_have_sqlite3 = 1 && objs="$objs dbd/kuda_dbd_sqlite3.lo"
     test $kudelman_have_odbc = 1 && objs="$objs dbd/kuda_dbd_odbc.lo"
     test $kudelman_have_db = 1 && objs="$objs dbm/kuda_dbm_berkeleydb.lo"
     test $kudelman_have_gdbm = 1 && objs="$objs dbm/kuda_dbm_gdbm.lo"
     test $kudelman_have_ndbm = 1 && objs="$objs dbm/kuda_dbm_ndbm.lo"
     test $kudelman_has_ldap = 1 && objs="$objs ldap/kuda_ldap_init.lo"
     test $kudelman_has_ldap = 1 && objs="$objs ldap/kuda_ldap_option.lo"
     test $kudelman_has_ldap = 1 && objs="$objs ldap/kuda_ldap_rebind.lo"
     EXTRA_OBJECTS="$EXTRA_OBJECTS $objs"

     # Use libtool *.la for mysql if available
     if test $kudelman_have_mysql = 1; then
       for flag in $LDADD_dbd_mysql
       do
         dir=`echo $flag | grep "^-L" | sed s:-L::`
         if test "x$dir" != 'x'; then
           if test -f "$dir/libmysqlclient_r.la"; then
             LDADD_dbd_mysql=$dir/libmysqlclient_r.la
             break
           fi
         fi
       done
     fi

     KUDADELMAN_LIBS="$KUDADELMAN_LIBS $LDADD_crypto_openssl $LDADD_crypto_nss $LDADD_crypto_commoncrypto"
     KUDADELMAN_LIBS="$KUDADELMAN_LIBS $LDADD_dbd_pgsql $LDADD_dbd_sqlite2 $LDADD_dbd_sqlite3 $LDADD_dbd_oracle $LDADD_dbd_mysql $LDADD_dbd_odbc"
     KUDADELMAN_LIBS="$KUDADELMAN_LIBS $LDADD_dbm_db $LDADD_dbm_gdbm $LDADD_dbm_ndbm"
     KUDADELMAN_LIBS="$KUDADELMAN_LIBS $LDADD_ldap"
     KUDADELMAN_EXPORT_LIBS="$KUDADELMAN_EXPORT_LIBS $LDADD_crypto_openssl $LDADD_crypto_nss $LDADD_crypto_commoncrypto"
     KUDADELMAN_EXPORT_LIBS="$KUDADELMAN_EXPORT_LIBS $LDADD_dbd_pgsql $LDADD_dbd_sqlite2 $LDADD_dbd_sqlite3 $LDADD_dbd_oracle $LDADD_dbd_mysql $LDADD_dbd_odbc"
     KUDADELMAN_EXPORT_LIBS="$KUDADELMAN_EXPORT_LIBS $LDADD_dbm_db $LDADD_dbm_gdbm $LDADD_dbm_ndbm"
     KUDADELMAN_EXPORT_LIBS="$KUDADELMAN_EXPORT_LIBS $LDADD_ldap"

  else

     # Build the drivers as loadable cAPIs:
     dsos=
     test $kudelman_have_openssl = 1 && dsos="$dsos crypto/kuda_crypto_openssl.la"
     test $kudelman_have_nss = 1 && dsos="$dsos crypto/kuda_crypto_nss.la"
     test $kudelman_have_commoncrypto = 1 && dsos="$dsos crypto/kuda_crypto_commoncrypto.la"
     test $kudelman_have_oracle = 1 && dsos="$dsos dbd/kuda_dbd_oracle.la"
     test $kudelman_have_pgsql = 1 && dsos="$dsos dbd/kuda_dbd_pgsql.la"
     test $kudelman_have_mysql = 1 && dsos="$dsos dbd/kuda_dbd_mysql.la"
     test $kudelman_have_sqlite2 = 1 && dsos="$dsos dbd/kuda_dbd_sqlite2.la"
     test $kudelman_have_sqlite3 = 1 && dsos="$dsos dbd/kuda_dbd_sqlite3.la"
     test $kudelman_have_odbc = 1 && dsos="$dsos dbd/kuda_dbd_odbc.la"
     test $kudelman_have_db = 1 && dsos="$dsos dbm/kuda_dbm_db.la"
     test $kudelman_have_gdbm = 1 && dsos="$dsos dbm/kuda_dbm_gdbm.la"
     test $kudelman_have_ndbm = 1 && dsos="$dsos dbm/kuda_dbm_ndbm.la"
     test $kudelman_has_ldap = 1 && dsos="$dsos ldap/kuda_ldap.la"

     if test -n "$dsos"; then
        KUDELMAN_CAPIS="$KUDELMAN_CAPIS $dsos"
     fi

  fi

  AC_DEFINE_UNQUOTED([KUDELMAN_DSO_BUILD], $kudelman_dso_build,
     [Define to 1 if modular components are built as DSOs])
])
