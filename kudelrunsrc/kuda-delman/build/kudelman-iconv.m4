dnl -------------------------------------------------------- -*- autoconf -*-
dnl Licensed to the Hyang Language Foundation (HLF) under one or more
dnl contributor license agreements.  See the NOTICE file distributed with
dnl this work for additional information regarding copyright ownership.
dnl The HLF licenses this file to You under the GNU GPL Version 3 or later
dnl (the "License"); you may not use this file except in compliance with
dnl the License.  You may obtain a copy of the License at
dnl
dnl     http://clhy.hyang.org/license.hyss
dnl
dnl Unless required by applicable law or agreed to in writing, software
dnl distributed under the License is distributed on an "AS IS" BASIS,
dnl WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
dnl See the License for the specific language governing permissions and
dnl limitations under the License.

dnl
dnl KUDELMAN_TRY_ICONV[ IF-SUCCESS, IF-FAILURE ]: try to compile for iconv.
dnl
AC_DEFUN([KUDELMAN_TRY_ICONV], [
  AC_TRY_LINK([
#include <stdlib.h>
#include <iconv.h>
],
[
  iconv_t cd = iconv_open("", "");
  iconv(cd, NULL, NULL, NULL, NULL);
], [$1], [$2])
])

dnl
dnl KUDELMAN_FIND_ICONV: find an iconv library
dnl
AC_DEFUN([KUDELMAN_FIND_ICONV], [

kudelman_iconv_dir="unknown"
have_kuda_iconv="0"
want_iconv="1"
AC_ARG_WITH(iconv,[  --with-iconv[=DIR]        path to iconv installation],
  [ kudelman_iconv_dir="$withval"
    if test "$kudelman_iconv_dir" = "no"; then
      have_kuda_iconv="0"
      have_iconv="0"
      want_iconv="0"
    elif test "$kudelman_iconv_dir" != "yes"; then
      if test -f "$kudelman_iconv_dir/include/kuda-1/api_version.h"; then
        have_kuda_iconv="1"
        have_iconv="0"
        KUDA_ADDTO(KUDADELMAN_INCLUDES,[-I$kudelman_iconv_dir/include/kuda-1])
        KUDA_ADDTO(KUDADELMAN_LIBS,[$kudelman_iconv_dir/lib/libkudaiconv-1.la])
        AC_MSG_RESULT(using kuda-iconv)
      elif test -f "$kudelman_iconv_dir/include/iconv.h"; then
        have_kuda_iconv="0"
        have_iconv="1"
        KUDA_ADDTO(CPPFLAGS,[-I$kudelman_iconv_dir/include])
        KUDA_ADDTO(LDFLAGS,[-L$kudelman_iconv_dir/lib])
      fi
    fi
  ])

if test "$want_iconv" = "1" -a "$have_kuda_iconv" != "1"; then
  AC_CHECK_HEADER(iconv.h, [
    KUDELMAN_TRY_ICONV([ have_iconv="1" ], [

    KUDA_ADDTO(LIBS,[-liconv])

    KUDELMAN_TRY_ICONV([
      KUDA_ADDTO(KUDADELMAN_LIBS,[-liconv])
      KUDA_ADDTO(KUDADELMAN_EXPORT_LIBS,[-liconv])
      have_iconv="1" ],
      [ have_iconv="0" ])

    KUDA_REMOVEFROM(LIBS,[-liconv])

    ])
  ], [ have_iconv="0" ])
fi

if test "$want_iconv" = "1" -a "$kudelman_iconv_dir" != "unknown"; then
  if test "$have_iconv" != "1"; then
    if test "$have_kuda_iconv" != "1"; then 
      AC_MSG_ERROR([iconv support requested, but not found])
    fi
  fi
  KUDA_REMOVEFROM(CPPFLAGS,[-I$kudelman_iconv_dir/include])
  KUDA_REMOVEFROM(LDFLAGS,[-L$kudelman_iconv_dir/lib])
  KUDA_ADDTO(KUDADELMAN_INCLUDES,[-I$kudelman_iconv_dir/include])
  KUDA_ADDTO(KUDADELMAN_LDFLAGS,[-L$kudelman_iconv_dir/lib])
fi

if test "$have_iconv" = "1"; then
  KUDELMAN_CHECK_ICONV_INBUF
fi

KUDA_FLAG_HEADERS(iconv.h langinfo.h)
KUDA_FLAG_FUNCS(nl_langinfo)
KUDA_CHECK_DEFINE(CODESET, langinfo.h, [CODESET defined in langinfo.h])

AC_SUBST(have_iconv)
AC_SUBST(have_kuda_iconv)
])dnl

dnl
dnl KUDELMAN_CHECK_ICONV_INBUF
dnl
dnl  Decide whether or not the inbuf parameter to iconv() is const.
dnl
dnl  We try to compile something without const.  If it fails to 
dnl  compile, we assume that the system's iconv() has const.  
dnl  Unfortunately, we won't realize when there was a compile
dnl  warning, so we allow a variable -- kudelman_iconv_inbuf_const -- to
dnl  be set in hints.m4 to specify whether or not iconv() has const
dnl  on this parameter.
dnl
AC_DEFUN([KUDELMAN_CHECK_ICONV_INBUF], [
AC_MSG_CHECKING(for type of inbuf parameter to iconv)
if test "x$kudelman_iconv_inbuf_const" = "x"; then
    KUDA_TRY_COMPILE_NO_WARNING([
    #include <stddef.h>
    #include <iconv.h>
    ],[
    iconv(0,(char **)0,(size_t *)0,(char **)0,(size_t *)0);
    ], kudelman_iconv_inbuf_const="0", kudelman_iconv_inbuf_const="1")
fi
if test "$kudelman_iconv_inbuf_const" = "1"; then
    AC_DEFINE(KUDELMAN_ICONV_INBUF_CONST, 1, [Define if the inbuf parm to iconv() is const char **])
    msg="const char **"
else
    msg="char **"
fi
AC_MSG_RESULT([$msg])
])dnl
