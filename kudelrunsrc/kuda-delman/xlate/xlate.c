/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kudelman.h"
#include "kudelman_config.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_xlate.h"

/* If no implementation is available, don't generate code here since
 * kuda_xlate.h emitted macros which return KUDA_ENOTIMPL.
 */

#if KUDA_HAS_XLATE

#ifdef HAVE_STDDEF_H
#include <stddef.h> /* for NULL */
#endif
#if KUDA_HAVE_STRING_H
#include <string.h>
#endif
#if KUDA_HAVE_STRINGS_H
#include <strings.h>
#endif
#ifdef HAVE_ICONV_H
#include <iconv.h>
#endif
#if KUDELMAN_HAVE_KUDA_ICONV
#include <kuda_iconv.h>
#endif

#if defined(KUDELMAN_ICONV_INBUF_CONST) || KUDELMAN_HAVE_KUDA_ICONV
#define ICONV_INBUF_TYPE const char **
#else
#define ICONV_INBUF_TYPE char **
#endif

#ifndef min
#define min(x,y) ((x) <= (y) ? (x) : (y))
#endif

struct kuda_xlate_t {
    kuda_pool_t *pool;
    char *frompage;
    char *topage;
    char *sbcs_table;
#if KUDELMAN_HAVE_ICONV
    iconv_t ich;
#elif KUDELMAN_HAVE_KUDA_ICONV
    kuda_iconv_t ich;
#endif
};


static const char *handle_special_names(const char *page, kuda_pool_t *pool)
{
    if (page == KUDA_DEFAULT_CHARSET) {
        return kuda_platform_default_encoding(pool);
    }
    else if (page == KUDA_LOCALE_CHARSET) {
        return kuda_platform_locale_encoding(pool);
    }
    else {
        return page;
    }
}

static kuda_status_t kuda_xlate_cleanup(void *convset)
{
    kuda_xlate_t *old = convset;

#if KUDELMAN_HAVE_KUDA_ICONV
    if (old->ich != (kuda_iconv_t)-1) {
        return kuda_iconv_close(old->ich, old->pool);
    }

#elif KUDELMAN_HAVE_ICONV
    if (old->ich != (iconv_t)-1) {
        if (iconv_close(old->ich)) {
            int rv = errno;

            /* Sometimes, iconv is not good about setting errno. */
            return rv ? rv : KUDA_EINVAL;
        }
    }
#endif

    return KUDA_SUCCESS;
}

#if KUDELMAN_HAVE_ICONV
static void check_sbcs(kuda_xlate_t *convset)
{
    char inbuf[256], outbuf[256];
    char *inbufptr = inbuf;
    char *outbufptr = outbuf;
    kuda_size_t inbytes_left, outbytes_left;
    int i;
    kuda_size_t translated;

    for (i = 0; i < sizeof(inbuf); i++) {
        inbuf[i] = i;
    }

    inbytes_left = outbytes_left = sizeof(inbuf);
    translated = iconv(convset->ich, (ICONV_INBUF_TYPE)&inbufptr,
                       &inbytes_left, &outbufptr, &outbytes_left);

    if (translated != (kuda_size_t)-1
        && inbytes_left == 0
        && outbytes_left == 0) {
        /* hurray... this is simple translation; save the table,
         * close the iconv descriptor
         */

        convset->sbcs_table = kuda_palloc(convset->pool, sizeof(outbuf));
        memcpy(convset->sbcs_table, outbuf, sizeof(outbuf));
        iconv_close(convset->ich);
        convset->ich = (iconv_t)-1;

        /* TODO: add the table to the cache */
    }
    else {
        /* reset the iconv descriptor, since it's now in an undefined
         * state. */
        iconv_close(convset->ich);
        convset->ich = iconv_open(convset->topage, convset->frompage);
    }
}
#elif KUDELMAN_HAVE_KUDA_ICONV
static void check_sbcs(kuda_xlate_t *convset)
{
    char inbuf[256], outbuf[256];
    char *inbufptr = inbuf;
    char *outbufptr = outbuf;
    kuda_size_t inbytes_left, outbytes_left;
    int i;
    kuda_size_t translated;
    kuda_status_t rv;

    for (i = 0; i < sizeof(inbuf); i++) {
        inbuf[i] = i;
    }

    inbytes_left = outbytes_left = sizeof(inbuf);
    rv = kuda_iconv(convset->ich, (ICONV_INBUF_TYPE)&inbufptr,
                   &inbytes_left, &outbufptr, &outbytes_left,
                   &translated);

    if ((rv == KUDA_SUCCESS)
        && (translated != (kuda_size_t)-1)
        && inbytes_left == 0
        && outbytes_left == 0) {
        /* hurray... this is simple translation; save the table,
         * close the iconv descriptor
         */

        convset->sbcs_table = kuda_palloc(convset->pool, sizeof(outbuf));
        memcpy(convset->sbcs_table, outbuf, sizeof(outbuf));
        kuda_iconv_close(convset->ich, convset->pool);
        convset->ich = (kuda_iconv_t)-1;

        /* TODO: add the table to the cache */
    }
    else {
        /* reset the iconv descriptor, since it's now in an undefined
         * state. */
        kuda_iconv_close(convset->ich, convset->pool);
        rv = kuda_iconv_open(convset->topage, convset->frompage, 
                            convset->pool, &convset->ich);
    }
}
#endif /* KUDELMAN_HAVE_KUDA_ICONV */

static void make_identity_table(kuda_xlate_t *convset)
{
  int i;

  convset->sbcs_table = kuda_palloc(convset->pool, 256);
  for (i = 0; i < 256; i++)
      convset->sbcs_table[i] = i;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_xlate_open(kuda_xlate_t **convset,
                                         const char *topage,
                                         const char *frompage,
                                         kuda_pool_t *pool)
{
    kuda_status_t rv;
    kuda_xlate_t *new;
    int found = 0;

    *convset = NULL;

    topage = handle_special_names(topage, pool);
    frompage = handle_special_names(frompage, pool);

    new = (kuda_xlate_t *)kuda_pcalloc(pool, sizeof(kuda_xlate_t));
    if (!new) {
        return KUDA_ENOMEM;
    }

    new->pool = pool;
    new->topage = kuda_pstrdup(pool, topage);
    new->frompage = kuda_pstrdup(pool, frompage);
    if (!new->topage || !new->frompage) {
        return KUDA_ENOMEM;
    }

#ifdef TODO
    /* search cache of codepage pairs; we may be able to avoid the
     * expensive iconv_open()
     */

    set found to non-zero if found in the cache
#endif

    if ((! found) && (strcmp(topage, frompage) == 0)) {
        /* to and from are the same */
        found = 1;
        make_identity_table(new);
    }

#if KUDELMAN_HAVE_KUDA_ICONV
    if (!found) {
        rv = kuda_iconv_open(topage, frompage, pool, &new->ich);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
        found = 1;
        check_sbcs(new);
    } else
        new->ich = (kuda_iconv_t)-1;

#elif KUDELMAN_HAVE_ICONV
    if (!found) {
        new->ich = iconv_open(topage, frompage);
        if (new->ich == (iconv_t)-1) {
            int rv = errno;
            /* Sometimes, iconv is not good about setting errno. */
            return rv ? rv : KUDA_EINVAL;
        }
        found = 1;
        check_sbcs(new);
    } else
        new->ich = (iconv_t)-1;
#endif /* KUDELMAN_HAVE_ICONV */

    if (found) {
        *convset = new;
        kuda_pool_cleanup_register(pool, (void *)new, kuda_xlate_cleanup,
                            kuda_pool_cleanup_null);
        rv = KUDA_SUCCESS;
    }
    else {
        rv = KUDA_EINVAL; /* iconv() would return EINVAL if it
                                couldn't handle the pair */
    }

    return rv;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_xlate_sb_get(kuda_xlate_t *convset, int *onoff)
{
    *onoff = convset->sbcs_table != NULL;
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_xlate_conv_buffer(kuda_xlate_t *convset,
                                                const char *inbuf,
                                                kuda_size_t *inbytes_left,
                                                char *outbuf,
                                                kuda_size_t *outbytes_left)
{
    kuda_status_t status = KUDA_SUCCESS;

#if KUDELMAN_HAVE_KUDA_ICONV
    if (convset->ich != (kuda_iconv_t)-1) {
        const char *inbufptr = inbuf;
        kuda_size_t translated;
        char *outbufptr = outbuf;
        status = kuda_iconv(convset->ich, &inbufptr, inbytes_left,
                           &outbufptr, outbytes_left, &translated);

        /* If everything went fine but we ran out of buffer, don't
         * report it as an error.  Caller needs to look at the two
         * bytes-left values anyway.
         *
         * There are three expected cases where rc is -1.  In each of
         * these cases, *inbytes_left != 0.
         * a) the non-error condition where we ran out of output
         *    buffer
         * b) the non-error condition where we ran out of input (i.e.,
         *    the last input character is incomplete)
         * c) the error condition where the input is invalid
         */
        switch (status) {

            case KUDA_BADARG:  /* out of space on output */
                status = 0; /* change table lookup code below if you
                               make this an error */
                break;

            case KUDA_EINVAL: /* input character not complete (yet) */
                status = KUDA_INCOMPLETE;
                break;

            case KUDA_BADCH: /* bad input byte */
                status = KUDA_EINVAL;
                break;

             /* Sometimes, iconv is not good about setting errno. */
            case 0:
                if (inbytes_left && *inbytes_left)
                    status = KUDA_INCOMPLETE;
                break;

            default:
                break;
        }
    }
    else

#elif KUDELMAN_HAVE_ICONV
    if (convset->ich != (iconv_t)-1) {
        const char *inbufptr = inbuf;
        char *outbufptr = outbuf;
        kuda_size_t translated;
        translated = iconv(convset->ich, (ICONV_INBUF_TYPE)&inbufptr,
                           inbytes_left, &outbufptr, outbytes_left);

        /* If everything went fine but we ran out of buffer, don't
         * report it as an error.  Caller needs to look at the two
         * bytes-left values anyway.
         *
         * There are three expected cases where rc is -1.  In each of
         * these cases, *inbytes_left != 0.
         * a) the non-error condition where we ran out of output
         *    buffer
         * b) the non-error condition where we ran out of input (i.e.,
         *    the last input character is incomplete)
         * c) the error condition where the input is invalid
         */
        if (translated == (kuda_size_t)-1) {
            int rv = errno;
            switch (rv) {

            case E2BIG:  /* out of space on output */
                status = 0; /* change table lookup code below if you
                               make this an error */
                break;

            case EINVAL: /* input character not complete (yet) */
                status = KUDA_INCOMPLETE;
                break;

            case EILSEQ: /* bad input byte */
                status = KUDA_EINVAL;
                break;

             /* Sometimes, iconv is not good about setting errno. */
            case 0:
                status = KUDA_INCOMPLETE;
                break;

            default:
                status = rv;
                break;
            }
        }
    }
    else
#endif

    if (inbuf) {
        kuda_size_t to_convert = min(*inbytes_left, *outbytes_left);
        kuda_size_t converted = to_convert;
        char *table = convset->sbcs_table;

        while (to_convert) {
            *outbuf = table[(unsigned char)*inbuf];
            ++outbuf;
            ++inbuf;
            --to_convert;
        }
        *inbytes_left -= converted;
        *outbytes_left -= converted;
    }

    return status;
}

KUDELMAN_DECLARE(kuda_int32_t) kuda_xlate_conv_byte(kuda_xlate_t *convset,
                                             unsigned char inchar)
{
    if (convset->sbcs_table) {
        return convset->sbcs_table[inchar];
    }
    else {
        return -1;
    }
}

KUDELMAN_DECLARE(kuda_status_t) kuda_xlate_close(kuda_xlate_t *convset)
{
    return kuda_pool_cleanup_run(convset->pool, convset, kuda_xlate_cleanup);
}

#else /* !KUDA_HAS_XLATE */

KUDELMAN_DECLARE(kuda_status_t) kuda_xlate_open(kuda_xlate_t **convset,
                                         const char *topage,
                                         const char *frompage,
                                         kuda_pool_t *pool)
{
    return KUDA_ENOTIMPL;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_xlate_sb_get(kuda_xlate_t *convset, int *onoff)
{
    return KUDA_ENOTIMPL;
}

KUDELMAN_DECLARE(kuda_int32_t) kuda_xlate_conv_byte(kuda_xlate_t *convset,
                                             unsigned char inchar)
{
    return (-1);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_xlate_conv_buffer(kuda_xlate_t *convset,
                                                const char *inbuf,
                                                kuda_size_t *inbytes_left,
                                                char *outbuf,
                                                kuda_size_t *outbytes_left)
{
    return KUDA_ENOTIMPL;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_xlate_close(kuda_xlate_t *convset)
{
    return KUDA_ENOTIMPL;
}

#endif /* KUDA_HAS_XLATE */
