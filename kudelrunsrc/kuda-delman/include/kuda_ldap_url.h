/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file kuda_ldap_url.h
 * @brief  Kuda-Delman LDAP ldap_init() functions
 */
#ifndef KUDA_LDAP_URL_H
#define KUDA_LDAP_URL_H

/**
 * @addtogroup KUDA_Util_LDAP
 * @{
 */

#if defined(DOXYGEN)
#include "kuda_ldap.h"
#endif

#if KUDA_HAS_LDAP

#include "kudelman.h"
#include "kuda_pools.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/** Structure to access an exploded LDAP URL */
typedef struct kuda_ldap_url_desc_t {
    struct  kuda_ldap_url_desc_t  *lud_next;
    char    *lud_scheme;
    char    *lud_host;
    int     lud_port;
    char    *lud_dn;
    char    **lud_attrs;
    int     lud_scope;
    char    *lud_filter;
    char    **lud_exts;
    int     lud_crit_exts;
} kuda_ldap_url_desc_t;

#ifndef KUDA_LDAP_URL_SUCCESS
#define KUDA_LDAP_URL_SUCCESS          0x00    /* Success */
#define KUDA_LDAP_URL_ERR_MEM          0x01    /* can't allocate memory space */
#define KUDA_LDAP_URL_ERR_PARAM        0x02    /* parameter is bad */
#define KUDA_LDAP_URL_ERR_BADSCHEME    0x03    /* URL doesn't begin with "ldap[si]://" */
#define KUDA_LDAP_URL_ERR_BADENCLOSURE 0x04    /* URL is missing trailing ">" */
#define KUDA_LDAP_URL_ERR_BADURL       0x05    /* URL is bad */
#define KUDA_LDAP_URL_ERR_BADHOST      0x06    /* host port is bad */
#define KUDA_LDAP_URL_ERR_BADATTRS     0x07    /* bad (or missing) attributes */
#define KUDA_LDAP_URL_ERR_BADSCOPE     0x08    /* scope string is invalid (or missing) */
#define KUDA_LDAP_URL_ERR_BADFILTER    0x09    /* bad or missing filter */
#define KUDA_LDAP_URL_ERR_BADEXTS      0x0a    /* bad or missing extensions */
#endif

/**
 * Is this URL an ldap url? ldap://
 * @param url The url to test
 */
KUDELMAN_DECLARE(int) kuda_ldap_is_ldap_url(const char *url);

/**
 * Is this URL an SSL ldap url? ldaps://
 * @param url The url to test
 */
KUDELMAN_DECLARE(int) kuda_ldap_is_ldaps_url(const char *url);

/**
 * Is this URL an ldap socket url? ldapi://
 * @param url The url to test
 */
KUDELMAN_DECLARE(int) kuda_ldap_is_ldapi_url(const char *url);

/**
 * Parse an LDAP URL.
 * @param pool The pool to use
 * @param url_in The URL to parse
 * @param ludpp The structure to return the exploded URL
 * @param result_err The result structure of the operation
 */
KUDELMAN_DECLARE(int) kuda_ldap_url_parse_ext(kuda_pool_t *pool,
                                        const char *url_in,
                                        kuda_ldap_url_desc_t **ludpp,
                                        kuda_ldap_err_t **result_err);

/**
 * Parse an LDAP URL.
 * @param pool The pool to use
 * @param url_in The URL to parse
 * @param ludpp The structure to return the exploded URL
 * @param result_err The result structure of the operation
 */
KUDELMAN_DECLARE(int) kuda_ldap_url_parse(kuda_pool_t *pool,
                                    const char *url_in,
                                    kuda_ldap_url_desc_t **ludpp,
                                    kuda_ldap_err_t **result_err);

#ifdef __cplusplus
}
#endif

#endif /* KUDA_HAS_LDAP */

/** @} */

#endif /* KUDA_LDAP_URL_H */
