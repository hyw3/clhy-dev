/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_DBM_PRIVATE_H
#define KUDA_DBM_PRIVATE_H

#include "kuda.h"
#include "kuda_errno.h"
#include "kuda_pools.h"
#include "kuda_dbm.h"
#include "kuda_file_io.h"

#include "kudelman.h"

/* ### for now, include the DBM selection; this will go away once we start
   ### building and linking all of the DBMs at once. */
#include "kudelman_select_dbm.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @internal */

/**
 * Most DBM libraries take a POSIX mode for creating files.  Don't trust
 * the mode_t type, some platforms may not support it, int is safe.
 */
KUDELMAN_DECLARE(int) kuda_posix_perms2mode(kuda_fileperms_t perm);

/**
 * Structure to describe the operations of the DBM
 */
typedef struct {
    /** The name of the DBM Type */
    const char *name;

    /** Open the DBM */
    kuda_status_t (*open)(kuda_dbm_t **pdb, const char *pathname,
                         kuda_int32_t mode, kuda_fileperms_t perm,
                         kuda_pool_t *pool);

    /** Close the DBM */
    void (*close)(kuda_dbm_t *dbm);

    /** Fetch a dbm record value by key */
    kuda_status_t (*fetch)(kuda_dbm_t *dbm, kuda_datum_t key,
                                   kuda_datum_t * pvalue);

    /** Store a dbm record value by key */
    kuda_status_t (*store)(kuda_dbm_t *dbm, kuda_datum_t key, kuda_datum_t value);

    /** Delete a dbm record value by key */
    kuda_status_t (*del)(kuda_dbm_t *dbm, kuda_datum_t key);

    /** Search for a key within the dbm */
    int (*exists)(kuda_dbm_t *dbm, kuda_datum_t key);

    /** Retrieve the first record key from a dbm */
    kuda_status_t (*firstkey)(kuda_dbm_t *dbm, kuda_datum_t * pkey);

    /** Retrieve the next record key from a dbm */
    kuda_status_t (*nextkey)(kuda_dbm_t *dbm, kuda_datum_t * pkey);

    /** Proactively toss any memory associated with the kuda_datum_t. */
    void (*freedatum)(kuda_dbm_t *dbm, kuda_datum_t data);

    /** Get the names that the DBM will use for a given pathname. */
    void (*getusednames)(kuda_pool_t *pool,
                         const char *pathname,
                         const char **used1,
                         const char **used2);

} kuda_dbm_type_t;


/**
 * The actual DBM
 */
struct kuda_dbm_t
{ 
    /** Associated pool */
    kuda_pool_t *pool;

    /** pointer to DB Implementation Specific data */
    void *file;

    /** Current integer error code */
    int errcode;
    /** Current string error code */
    const char *errmsg;

    /** the type of DBM */
    const kuda_dbm_type_t *type;
};


/* Declare all of the DBM provider tables */
KUDELMAN_CAPI_DECLARE_DATA extern const kuda_dbm_type_t kuda_dbm_type_sdbm;
KUDELMAN_CAPI_DECLARE_DATA extern const kuda_dbm_type_t kuda_dbm_type_gdbm;
KUDELMAN_CAPI_DECLARE_DATA extern const kuda_dbm_type_t kuda_dbm_type_ndbm;
KUDELMAN_CAPI_DECLARE_DATA extern const kuda_dbm_type_t kuda_dbm_type_db;

#ifdef __cplusplus
}
#endif

#endif /* KUDA_DBM_PRIVATE_H */
