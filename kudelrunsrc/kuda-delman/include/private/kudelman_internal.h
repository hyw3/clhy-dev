/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_dso.h"
#include "kudelman.h"

#ifndef KUDELMAN_INTERNAL_H
#define KUDELMAN_INTERNAL_H

#if KUDELMAN_DSO_BUILD

#ifdef __cplusplus
extern "C" {
#endif

/* For modular dso loading, an internal interlock to allow us to
 * continue to initialize cAPIs by multiple threads, the caller
 * of kudelman_dso_load must lock first, and not unlock until any init
 * finalization is complete.
 */
kuda_status_t kudelman_dso_init(kuda_pool_t *pool);

kuda_status_t kudelman_dso_mutex_lock(void);
kuda_status_t kudelman_dso_mutex_unlock(void);

kuda_status_t kudelman_dso_load(kuda_dso_handle_t **dso, kuda_dso_handle_sym_t *dsoptr, const char *cAPI,
                          const char *capisym, kuda_pool_t *pool);

#if KUDA_HAS_LDAP

/* For LDAP internal builds, wrap our LDAP namespace */

struct kuda__ldap_dso_fntable {
    int (*info)(kuda_pool_t *pool, kuda_ldap_err_t **result_err);
    int (*init)(kuda_pool_t *pool, LDAP **ldap, const char *hostname,
                int portno, int secure, kuda_ldap_err_t **result_err);
    int (*ssl_init)(kuda_pool_t *pool, const char *cert_auth_file,
                    int cert_file_type, kuda_ldap_err_t **result_err);
    int (*ssl_deinit)(void);
    int (*get_option)(kuda_pool_t *pool, LDAP *ldap, int option,
                      void *outvalue, kuda_ldap_err_t **result_err);
    int (*set_option)(kuda_pool_t *pool, LDAP *ldap, int option,
                      const void *invalue, kuda_ldap_err_t **result_err);
    kuda_status_t (*rebind_init)(kuda_pool_t *pool);
    kuda_status_t (*rebind_add)(kuda_pool_t *pool, LDAP *ld,
                               const char *bindDN, const char *bindPW);
    kuda_status_t (*rebind_remove)(LDAP *ld);
};

#endif /* KUDA_HAS_LDAP */

#ifdef __cplusplus
}
#endif

#endif /* KUDELMAN_DSO_BUILD */

#endif /* KUDELMAN_INTERNAL_H */

