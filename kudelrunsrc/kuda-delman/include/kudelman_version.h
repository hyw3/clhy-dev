/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDELMAN_VERSION_H
#define KUDELMAN_VERSION_H

/**
 * @file kudelman_version.h
 * @brief Kuda-Delman Versioning Interface
 * 
 * Kuda-Delman's Version
 *
 * There are several different mechanisms for accessing the version. There
 * is a string form, and a set of numbers; in addition, there are constants
 * which can be compiled into your application, and you can query the library
 * being used for its actual version.
 *
 * Note that it is possible for an application to detect that it has been
 * compiled against a different version of KUDELMAN by use of the compile-time
 * constants and the use of the run-time query function.
 *
 * This file governs the Kuda Delman Runtime versioning system,
 * as also specified at kuda-delman.spec file.
 * 
 */


#define KUDELMAN_COPYRIGHT "Copyright (c) 2019 The Hyang Language Foundation " \
                      "Distributed under GNU GPL version 3 or later."

/* The numeric compile-time version constants. These constants are the
 * authoritative version numbers for KUDELMAN. 
 */

/** major version 
 * Major API changes that could cause compatibility problems for older
 * programs such as structure size changes.  No binary compatibility is
 * possible across a change in the major version.
 */
#define KUDELMAN_MAJOR_VERSION       1

/** minor version
 * Minor API changes that do not cause binary compatibility problems.
 * Reset to 0 when upgrading KUDELMAN_MAJOR_VERSION
 */
#define KUDELMAN_MINOR_VERSION       6

/** patch level 
 * The Patch Level never includes API changes, simply bug fixes.
 * Reset to 0 when upgrading KUDA_MINOR_VERSION
 */
#define KUDELMAN_PATCH_VERSION       4

/** 
 * The symbol KUDELMAN_IS_DEV_VERSION is only defined for internal,
 * "development" copies of KUDELMAN.  It is undefined for released versions
 * of KUDELMAN.
 */
/* #undef KUDELMAN_IS_DEV_VERSION */


#if defined(KUDELMAN_IS_DEV_VERSION) || defined(DOXYGEN)
/** Internal: string form of the "is dev" flag */
#ifndef KUDELMAN_IS_DEV_STRING
#define KUDELMAN_IS_DEV_STRING "-dev"
#endif
#else
#define KUDELMAN_IS_DEV_STRING ""
#endif


#ifndef KUDELMAN_STRINGIFY
/** Properly quote a value as a string in the C preprocessor */
#define KUDELMAN_STRINGIFY(n) KUDELMAN_STRINGIFY_HELPER(n)
/** Helper macro for KUDELMAN_STRINGIFY */
#define KUDELMAN_STRINGIFY_HELPER(n) #n
#endif

/** The formatted string of KUDELMAN's version */
#define KUDELMAN_VERSION_STRING \
     KUDELMAN_STRINGIFY(KUDELMAN_MAJOR_VERSION) "." \
     KUDELMAN_STRINGIFY(KUDELMAN_MINOR_VERSION) "." \
     KUDELMAN_STRINGIFY(KUDELMAN_PATCH_VERSION) \
     KUDELMAN_IS_DEV_STRING

/** An alternative formatted string of KUDA's version */
/* macro for Win32 .rc files using numeric csv representation */
#define KUDELMAN_VERSION_STRING_CSV KUDELMAN_MAJOR_VERSION ##, \
                             ##KUDELMAN_MINOR_VERSION ##, \
                             ##KUDELMAN_PATCH_VERSION


#ifndef KUDELMAN_VERSION_ONLY

/* The C language API to access the version at run time, 
 * as opposed to compile time.  KUDELMAN_VERSION_ONLY may be defined 
 * externally when preprocessing kuda_version.h to obtain strictly 
 * the C Preprocessor macro declarations.
 */

#include "kuda_version.h"

#include "kudelman.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Return Kuda-Delman's version information information in a numeric form.
 *
 *  @param pvsn Pointer to a version structure for returning the version
 *              information.
 */
KUDELMAN_DECLARE(void) kudelman_version(kuda_version_t *pvsn);

/** Return KUDELMAN's version information as a string. */
KUDELMAN_DECLARE(const char *) kudelman_version_string(void);

#ifdef __cplusplus
}
#endif

#endif /* ndef KUDELMAN_VERSION_ONLY */

#endif /* ndef KUDELMAN_VERSION_H */
