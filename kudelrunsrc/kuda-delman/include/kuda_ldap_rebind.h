/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The KUDA LDAP rebind functions provide an implementation of
 * a rebind procedure that can be used to allow clients to chase referrals,
 * using the same credentials used to log in originally.
 *
 * Use of this implementation is optional.
 *
 * @file kuda_ldap_rebind.h
 * @brief cLHy LDAP library
 */

#ifndef KUDELMAN_LDAP_REBIND_H
#define KUDELMAN_LDAP_REBIND_H

/**
 * @addtogroup KUDA_Util_LDAP
 * @{
 **/

#if defined(DOXYGEN)
#include "kuda_ldap.h"
#endif

/*
 * Handle the case when LDAP is enabled
 */
#if KUDA_HAS_LDAP

/**
 * KUDA LDAP initialize rebind lock
 *
 * This function creates the lock for controlling access to the xref list..
 * @param pool Pool to use when creating the xref_lock.
 */
KUDELMAN_DECLARE_LDAP(kuda_status_t) kuda_ldap_rebind_init(kuda_pool_t *pool);


/**
 * KUDA LDAP rebind_add function
 *
 * This function creates a cross reference entry for the specified ldap
 * connection. The rebind callback function will look up this ldap 
 * connection so it can retrieve the bindDN and bindPW for use in any 
 * binds while referrals are being chased.
 *
 * This function will add the callback to the LDAP handle passed in.
 *
 * A cleanup is registered within the pool provided to remove this
 * entry when the pool is removed. Alternatively kuda_ldap_rebind_remove()
 * can be called to explicitly remove the entry at will.
 *
 * @param pool The pool to use
 * @param ld The LDAP connectionhandle
 * @param bindDN The bind DN to be used for any binds while chasing 
 *               referrals on this ldap connection.
 * @param bindPW The bind Password to be used for any binds while 
 *               chasing referrals on this ldap connection.
 */
KUDELMAN_DECLARE_LDAP(kuda_status_t) kuda_ldap_rebind_add(kuda_pool_t *pool,
                                                   LDAP *ld,
                                                   const char *bindDN,
                                                   const char *bindPW);

/**
 * KUDA LDAP rebind_remove function
 *
 * This function removes the rebind cross reference entry for the
 * specified ldap connection.
 *
 * If not explicitly removed, this function will be called automatically
 * when the pool is cleaned up.
 *
 * @param ld The LDAP connectionhandle
 */
KUDELMAN_DECLARE_LDAP(kuda_status_t) kuda_ldap_rebind_remove(LDAP *ld);

#endif /* KUDA_HAS_LDAP */

/** @} */

#endif /* KUDELMAN_LDAP_REBIND_H */

