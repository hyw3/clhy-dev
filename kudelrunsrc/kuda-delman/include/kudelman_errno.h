/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDELMAN_ERRNO_H
#define KUDELMAN_ERRNO_H

/**
 * @file kudelman_errno.h
 * @brief Kuda-Delman Error Codes
 */

#include "kuda.h"
#include "kuda_errno.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup kudelman_errno Error Codes
 * @ingroup KUDA_Util
 * @{
 */

/**
 * @defgroup KUDA_Util_Error KUDA_Util Error Values
 * <PRE>
 * <b>KUDELMAN ERROR VALUES</b>
 * KUDA_ENOKEY         The key provided was empty or NULL
 * KUDA_ENOIV          The initialisation vector provided was NULL
 * KUDA_EKEYTYPE       The key type was not recognised
 * KUDA_ENOSPACE       The buffer supplied was not big enough
 * KUDA_ECRYPT         An error occurred while encrypting or decrypting
 * KUDA_EPADDING       Padding was not supported
 * KUDA_EKEYLENGTH     The key length was incorrect
 * KUDA_ENOCIPHER      The cipher provided was not recognised
 * KUDA_ENODIGEST      The digest provided was not recognised
 * KUDA_ENOENGINE      The engine provided was not recognised
 * KUDA_EINITENGINE    The engine could not be initialised
 * KUDA_EREINIT        Underlying crypto has already been initialised
 * </PRE>
 *
 * <PRE>
 * <b>KUDA STATUS VALUES</b>
 * KUDA_INCHILD        Program is currently executing in the child
 * </PRE>
 * @{
 */
/** @see KUDA_STATUS_IS_ENOKEY */
#define KUDA_ENOKEY           (KUDA_UTIL_START_STATUS + 1)
/** @see KUDA_STATUS_IS_ENOIV */
#define KUDA_ENOIV            (KUDA_UTIL_START_STATUS + 2)
/** @see KUDA_STATUS_IS_EKEYTYPE */
#define KUDA_EKEYTYPE         (KUDA_UTIL_START_STATUS + 3)
/** @see KUDA_STATUS_IS_ENOSPACE */
#define KUDA_ENOSPACE         (KUDA_UTIL_START_STATUS + 4)
/** @see KUDA_STATUS_IS_ECRYPT */
#define KUDA_ECRYPT           (KUDA_UTIL_START_STATUS + 5)
/** @see KUDA_STATUS_IS_EPADDING */
#define KUDA_EPADDING         (KUDA_UTIL_START_STATUS + 6)
/** @see KUDA_STATUS_IS_EKEYLENGTH */
#define KUDA_EKEYLENGTH       (KUDA_UTIL_START_STATUS + 7)
/** @see KUDA_STATUS_IS_ENOCIPHER */
#define KUDA_ENOCIPHER        (KUDA_UTIL_START_STATUS + 8)
/** @see KUDA_STATUS_IS_ENODIGEST */
#define KUDA_ENODIGEST        (KUDA_UTIL_START_STATUS + 9)
/** @see KUDA_STATUS_IS_ENOENGINE */
#define KUDA_ENOENGINE        (KUDA_UTIL_START_STATUS + 10)
/** @see KUDA_STATUS_IS_EINITENGINE */
#define KUDA_EINITENGINE      (KUDA_UTIL_START_STATUS + 11)
/** @see KUDA_STATUS_IS_EREINIT */
#define KUDA_EREINIT          (KUDA_UTIL_START_STATUS + 12)
/** @} */

/**
 * @defgroup KUDELMAN_STATUS_IS Status Value Tests
 * @warning For any particular error condition, more than one of these tests
 *      may match. This is because platform-specific error codes may not
 *      always match the semantics of the POSIX codes these tests (and the
 *      corresponding KUDA error codes) are named after. A notable example
 *      are the KUDA_STATUS_IS_ENOENT and KUDA_STATUS_IS_ENOTDIR tests on
 *      Win32 platforms. The programmer should always be aware of this and
 *      adjust the order of the tests accordingly.
 * @{
 */

/** @} */

/**
 * @addtogroup KUDA_Util_Error
 * @{
 */
/**
 * The key was empty or not provided
 */
#define KUDA_STATUS_IS_ENOKEY(s)        ((s) == KUDA_ENOKEY)
/**
 * The initialisation vector was not provided
 */
#define KUDA_STATUS_IS_ENOIV(s)        ((s) == KUDA_ENOIV)
/**
 * The key type was not recognised
 */
#define KUDA_STATUS_IS_EKEYTYPE(s)        ((s) == KUDA_EKEYTYPE)
/**
 * The buffer provided was not big enough
 */
#define KUDA_STATUS_IS_ENOSPACE(s)        ((s) == KUDA_ENOSPACE)
/**
 * An error occurred while encrypting or decrypting
 */
#define KUDA_STATUS_IS_ECRYPT(s)        ((s) == KUDA_ECRYPT)
/**
 * An error occurred while padding
 */
#define KUDA_STATUS_IS_EPADDING(s)        ((s) == KUDA_EPADDING)
/**
 * An error occurred with the key length
 */
#define KUDA_STATUS_IS_EKEYLENGTH(s)        ((s) == KUDA_EKEYLENGTH)
/**
 * The cipher provided was not recognised
 */
#define KUDA_STATUS_IS_ENOCIPHER(s)        ((s) == KUDA_ENOCIPHER)
/**
 * The digest provided was not recognised
 */
#define KUDA_STATUS_IS_ENODIGEST(s)        ((s) == KUDA_ENODIGEST)
/**
 * The engine provided was not recognised
 */
#define KUDA_STATUS_IS_ENOENGINE(s)        ((s) == KUDA_ENOENGINE)
/**
 * The engine could not be initialised
 */
#define KUDA_STATUS_IS_EINITENGINE(s)        ((s) == KUDA_EINITENGINE)
/**
 * Crypto has already been initialised
 */
#define KUDA_STATUS_IS_EREINIT(s)        ((s) == KUDA_EREINIT)
/** @} */

/**
 * This structure allows the underlying API error codes to be returned
 * along with plain text error messages that explain to us mere mortals
 * what really happened.
 */
typedef struct kudelman_err_t {
    const char *reason;
    const char *msg;
    int rc;
} kudelman_err_t;

/** @} */

#ifdef __cplusplus
}
#endif

#endif  /* ! KUDELMAN_ERRNO_H */
