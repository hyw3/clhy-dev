/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file kuda_anylock.h
 * @brief Kuda-Delman transparent any lock flavor wrapper
 */
#ifndef KUDA_ANYLOCK_H
#define KUDA_ANYLOCK_H

#include "kuda_proc_mutex.h"
#include "kuda_thread_mutex.h"
#include "kuda_thread_rwlock.h"

/** Structure that may contain any KUDA lock type */
typedef struct kuda_anylock_t {
    /** Indicates what type of lock is in lock */
    enum tm_lock {
        kuda_anylock_none,           /**< None */
        kuda_anylock_procmutex,      /**< Process-based */
        kuda_anylock_threadmutex,    /**< Thread-based */
        kuda_anylock_readlock,       /**< Read lock */
        kuda_anylock_writelock       /**< Write lock */
    } type;
    /** Union of all possible KUDA locks */
    union kuda_anylock_u_t {
        kuda_proc_mutex_t *pm;       /**< Process mutex */
#if KUDA_HAS_THREADS
        kuda_thread_mutex_t *tm;     /**< Thread mutex */
        kuda_thread_rwlock_t *rw;    /**< Read-write lock */
#endif
    } lock;
} kuda_anylock_t;

#if KUDA_HAS_THREADS

/** Lock an kuda_anylock_t structure */
#define KUDA_ANYLOCK_LOCK(lck)                \
    (((lck)->type == kuda_anylock_none)         \
      ? KUDA_SUCCESS                              \
      : (((lck)->type == kuda_anylock_threadmutex)  \
          ? kuda_thread_mutex_lock((lck)->lock.tm)    \
          : (((lck)->type == kuda_anylock_procmutex)    \
              ? kuda_proc_mutex_lock((lck)->lock.pm)      \
              : (((lck)->type == kuda_anylock_readlock)     \
                  ? kuda_thread_rwlock_rdlock((lck)->lock.rw) \
                  : (((lck)->type == kuda_anylock_writelock)    \
                      ? kuda_thread_rwlock_wrlock((lck)->lock.rw) \
                      : KUDA_EINVAL)))))

#else /* KUDA_HAS_THREADS */

#define KUDA_ANYLOCK_LOCK(lck)                \
    (((lck)->type == kuda_anylock_none)         \
      ? KUDA_SUCCESS                              \
          : (((lck)->type == kuda_anylock_procmutex)    \
              ? kuda_proc_mutex_lock((lck)->lock.pm)      \
                      : KUDA_EINVAL))

#endif /* KUDA_HAS_THREADS */

#if KUDA_HAS_THREADS

/** Try to lock an kuda_anylock_t structure */
#define KUDA_ANYLOCK_TRYLOCK(lck)                \
    (((lck)->type == kuda_anylock_none)            \
      ? KUDA_SUCCESS                                 \
      : (((lck)->type == kuda_anylock_threadmutex)     \
          ? kuda_thread_mutex_trylock((lck)->lock.tm)    \
          : (((lck)->type == kuda_anylock_procmutex)       \
              ? kuda_proc_mutex_trylock((lck)->lock.pm)      \
              : (((lck)->type == kuda_anylock_readlock)        \
                  ? kuda_thread_rwlock_tryrdlock((lck)->lock.rw) \
                  : (((lck)->type == kuda_anylock_writelock)       \
                      ? kuda_thread_rwlock_trywrlock((lck)->lock.rw) \
                          : KUDA_EINVAL)))))

#else /* KUDA_HAS_THREADS */

#define KUDA_ANYLOCK_TRYLOCK(lck)                \
    (((lck)->type == kuda_anylock_none)            \
      ? KUDA_SUCCESS                                 \
          : (((lck)->type == kuda_anylock_procmutex)       \
              ? kuda_proc_mutex_trylock((lck)->lock.pm)      \
                          : KUDA_EINVAL))

#endif /* KUDA_HAS_THREADS */

#if KUDA_HAS_THREADS

/** Unlock an kuda_anylock_t structure */
#define KUDA_ANYLOCK_UNLOCK(lck)              \
    (((lck)->type == kuda_anylock_none)         \
      ? KUDA_SUCCESS                              \
      : (((lck)->type == kuda_anylock_threadmutex)  \
          ? kuda_thread_mutex_unlock((lck)->lock.tm)  \
          : (((lck)->type == kuda_anylock_procmutex)    \
              ? kuda_proc_mutex_unlock((lck)->lock.pm)    \
              : ((((lck)->type == kuda_anylock_readlock) || \
                  ((lck)->type == kuda_anylock_writelock))    \
                  ? kuda_thread_rwlock_unlock((lck)->lock.rw)   \
                      : KUDA_EINVAL))))

#else /* KUDA_HAS_THREADS */

#define KUDA_ANYLOCK_UNLOCK(lck)              \
    (((lck)->type == kuda_anylock_none)         \
      ? KUDA_SUCCESS                              \
          : (((lck)->type == kuda_anylock_procmutex)    \
              ? kuda_proc_mutex_unlock((lck)->lock.pm)    \
                      : KUDA_EINVAL))

#endif /* KUDA_HAS_THREADS */

#endif /* !KUDA_ANYLOCK_H */
