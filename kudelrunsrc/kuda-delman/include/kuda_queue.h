/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_QUEUE_H
#define KUDA_QUEUE_H

/**
 * @file kuda_queue.h
 * @brief Thread Safe FIFO bounded queue
 * @note Since most implementations of the queue are backed by a condition
 * variable implementation, it isn't available on systems without threads.
 * Although condition variables are sometimes available without threads.
 */

#include "kudelman.h"
#include "kuda_errno.h"
#include "kuda_pools.h"

#if KUDA_HAS_THREADS

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup KUDA_Util_FIFO Thread Safe FIFO bounded queue
 * @ingroup KUDA_Util
 * @{
 */

/**
 * opaque structure
 */
typedef struct kuda_queue_t kuda_queue_t;

/** 
 * create a FIFO queue
 * @param queue The new queue
 * @param queue_capacity maximum size of the queue
 * @param a pool to allocate queue from
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_queue_create(kuda_queue_t **queue, 
                                           unsigned int queue_capacity, 
                                           kuda_pool_t *a);

/**
 * push/add an object to the queue, blocking if the queue is already full
 *
 * @param queue the queue
 * @param data the data
 * @returns KUDA_EINTR the blocking was interrupted (try again)
 * @returns KUDA_EOF the queue has been terminated
 * @returns KUDA_SUCCESS on a successful push
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_queue_push(kuda_queue_t *queue, void *data);

/**
 * pop/get an object from the queue, blocking if the queue is already empty
 *
 * @param queue the queue
 * @param data the data
 * @returns KUDA_EINTR the blocking was interrupted (try again)
 * @returns KUDA_EOF if the queue has been terminated
 * @returns KUDA_SUCCESS on a successful pop
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_queue_pop(kuda_queue_t *queue, void **data);

/**
 * push/add an object to the queue, returning immediately if the queue is full
 *
 * @param queue the queue
 * @param data the data
 * @returns KUDA_EINTR the blocking operation was interrupted (try again)
 * @returns KUDA_EAGAIN the queue is full
 * @returns KUDA_EOF the queue has been terminated
 * @returns KUDA_SUCCESS on a successful push
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_queue_trypush(kuda_queue_t *queue, void *data);

/**
 * pop/get an object to the queue, returning immediately if the queue is empty
 *
 * @param queue the queue
 * @param data the data
 * @returns KUDA_EINTR the blocking operation was interrupted (try again)
 * @returns KUDA_EAGAIN the queue is empty
 * @returns KUDA_EOF the queue has been terminated
 * @returns KUDA_SUCCESS on a successful pop
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_queue_trypop(kuda_queue_t *queue, void **data);

/**
 * returns the size of the queue.
 *
 * @warning this is not threadsafe, and is intended for reporting/monitoring
 * of the queue.
 * @param queue the queue
 * @returns the size of the queue
 */
KUDELMAN_DECLARE(unsigned int) kuda_queue_size(kuda_queue_t *queue);

/**
 * interrupt all the threads blocking on this queue.
 *
 * @param queue the queue
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_queue_interrupt_all(kuda_queue_t *queue);

/**
 * terminate the queue, sending an interrupt to all the
 * blocking threads
 *
 * @param queue the queue
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_queue_term(kuda_queue_t *queue);

#ifdef __cplusplus
}
#endif

/** @} */

#endif /* KUDA_HAS_THREADS */

#endif /* KUDAQUEUE_H */
