/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_DATE_H
#define KUDA_DATE_H

/**
 * @file kuda_date.h
 * @brief Kuda-Delman date routines
 */

/**
 * @defgroup KUDA_Util_Date Date routines
 * @ingroup KUDA_Util
 * @{
 */

/*
 * kuda_date.h: prototypes for date parsing utility routines
 */

#include "kudelman.h"
#include "kuda_time.h"

#ifdef __cplusplus
extern "C" {
#endif

/** A bad date. */
#define KUDA_DATE_BAD ((kuda_time_t)0)

/**
 * Compare a string to a mask
 * @param data The string to compare
 * @param mask Mask characters (arbitrary maximum is 256 characters):
 * <PRE>
 *   '\@' - uppercase letter
 *   '\$' - lowercase letter
 *   '\&' - hex digit
 *   '#' - digit
 *   '~' - digit or space
 *   '*' - swallow remaining characters
 * </PRE>
 * @remark The mask tests for an exact match for any other character
 * @return 1 if the string matches, 0 otherwise
 */
KUDELMAN_DECLARE(int) kuda_date_checkmask(const char *data, const char *mask);

/**
 * Parses an HTTP date in one of three standard forms:
 * <PRE>
 *     Sun, 06 Nov 1994 08:49:37 GMT  ; RFC 822, updated by RFC 1123
 *     Sunday, 06-Nov-94 08:49:37 GMT ; RFC 850, obsoleted by RFC 1036
 *     Sun Nov  6 08:49:37 1994       ; ANSI C's asctime() format
 * </PRE>
 * @param date The date in one of the three formats above
 * @return the kuda_time_t number of microseconds since 1 Jan 1970 GMT, or
 *         0 if this would be out of range or if the date is invalid.
 */
KUDELMAN_DECLARE(kuda_time_t) kuda_date_parse_http(const char *date);

/**
 * Parses a string resembling an RFC 822 date.  This is meant to be
 * leinent in its parsing of dates.  Hence, this will parse a wider 
 * range of dates than kuda_date_parse_http.
 *
 * The prominent mailer (or poster, if mailer is unknown) that has
 * been seen in the wild is included for the unknown formats.
 * <PRE>
 *     Sun, 06 Nov 1994 08:49:37 GMT  ; RFC 822, updated by RFC 1123
 *     Sunday, 06-Nov-94 08:49:37 GMT ; RFC 850, obsoleted by RFC 1036
 *     Sun Nov  6 08:49:37 1994       ; ANSI C's asctime() format
 *     Sun, 6 Nov 1994 08:49:37 GMT   ; RFC 822, updated by RFC 1123
 *     Sun, 06 Nov 94 08:49:37 GMT    ; RFC 822
 *     Sun, 6 Nov 94 08:49:37 GMT     ; RFC 822
 *     Sun, 06 Nov 94 08:49 GMT       ; Unknown [drtr\@ast.cam.ac.uk] 
 *     Sun, 6 Nov 94 08:49 GMT        ; Unknown [drtr\@ast.cam.ac.uk]
 *     Sun, 06 Nov 94 8:49:37 GMT     ; Unknown [Elm 70.85]
 *     Sun, 6 Nov 94 8:49:37 GMT      ; Unknown [Elm 70.85] 
 * </PRE>
 *
 * @param date The date in one of the formats above
 * @return the kuda_time_t number of microseconds since 1 Jan 1970 GMT, or
 *         0 if this would be out of range or if the date is invalid.
 */
KUDELMAN_DECLARE(kuda_time_t) kuda_date_parse_rfc(const char *date);

/** @} */
#ifdef __cplusplus
}
#endif

#endif	/* !KUDA_DATE_H */
