/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_STRMATCH_H
#define KUDA_STRMATCH_H
/**
 * @file kuda_strmatch.h
 * @brief Kuda-Delman string matching routines
 */

#include "kudelman.h"
#include "kuda_pools.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup KUDA_Util_StrMatch String matching routines
 * @ingroup KUDA_Util
 * @{
 */

/** @see kuda_strmatch_pattern */
typedef struct kuda_strmatch_pattern kuda_strmatch_pattern;

/**
 * Precompiled search pattern
 */
struct kuda_strmatch_pattern {
    /** Function called to compare */
    const char *(*compare)(const kuda_strmatch_pattern *this_pattern,
                           const char *s, kuda_size_t slen);
    const char *pattern;    /**< Current pattern */
    kuda_size_t length;      /**< Current length */
    void *context;          /**< hook to add precomputed metadata */
};

#if defined(DOXYGEN)
/**
 * Search for a precompiled pattern within a string
 * @param pattern The pattern
 * @param s The string in which to search for the pattern
 * @param slen The length of s (excluding null terminator)
 * @return A pointer to the first instance of the pattern in s, or
 *         NULL if not found
 */
KUDELMAN_DECLARE(const char *) kuda_strmatch(const kuda_strmatch_pattern *pattern,
                                       const char *s, kuda_size_t slen);
#else
#define kuda_strmatch(pattern, s, slen) (*((pattern)->compare))((pattern), (s), (slen))
#endif

/**
 * Precompile a pattern for matching using the Boyer-Moore-Horspool algorithm
 * @param p The pool from which to allocate the pattern
 * @param s The pattern string
 * @param case_sensitive Whether the matching should be case-sensitive
 * @return a pointer to the compiled pattern, or NULL if compilation fails
 */
KUDELMAN_DECLARE(const kuda_strmatch_pattern *) kuda_strmatch_precompile(kuda_pool_t *p, const char *s, int case_sensitive);

/** @} */
#ifdef __cplusplus
}
#endif

#endif	/* !KUDA_STRMATCH_H */
