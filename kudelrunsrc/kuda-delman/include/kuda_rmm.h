/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_RMM_H
#define KUDA_RMM_H
/** 
 * @file kuda_rmm.h
 * @brief Kuda-Delman Relocatable Memory Management Routines
 */
/**
 * @defgroup KUDA_Util_RMM Relocatable Memory Management Routines
 * @ingroup KUDA_Util
 * @{
 */

#include "kuda.h"
#include "kuda_pools.h"
#include "kuda_errno.h"
#include "kudelman.h"
#include "kuda_anylock.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/** Structure to access Relocatable, Managed Memory */
typedef struct kuda_rmm_t kuda_rmm_t;

/** Fundamental allocation unit, within a specific kuda_rmm_t */
typedef kuda_size_t   kuda_rmm_off_t;

/**
 * Initialize a relocatable memory block to be managed by the kuda_rmm API.
 * @param rmm The relocatable memory block
 * @param lock An kuda_anylock_t of the appropriate type of lock, or NULL
 *             if no locking is required.
 * @param membuf The block of relocatable memory to be managed
 * @param memsize The size of relocatable memory block to be managed
 * @param cont The pool to use for local storage and management
 * @remark Both @param membuf and @param memsize must be aligned
 * (for instance using KUDA_ALIGN_DEFAULT).
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_rmm_init(kuda_rmm_t **rmm, kuda_anylock_t *lock,
                                       void *membuf, kuda_size_t memsize, 
                                       kuda_pool_t *cont);

/**
 * Destroy a managed memory block.
 * @param rmm The relocatable memory block to destroy
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_rmm_destroy(kuda_rmm_t *rmm);

/**
 * Attach to a relocatable memory block already managed by the kuda_rmm API.
 * @param rmm The relocatable memory block
 * @param lock An kuda_anylock_t of the appropriate type of lock
 * @param membuf The block of relocatable memory already under management
 * @param cont The pool to use for local storage and management
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_rmm_attach(kuda_rmm_t **rmm, kuda_anylock_t *lock,
                                         void *membuf, kuda_pool_t *cont);

/**
 * Detach from the managed block of memory.
 * @param rmm The relocatable memory block to detach from
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_rmm_detach(kuda_rmm_t *rmm);

/**
 * Allocate memory from the block of relocatable memory.
 * @param rmm The relocatable memory block
 * @param reqsize How much memory to allocate
 */
KUDELMAN_DECLARE(kuda_rmm_off_t) kuda_rmm_malloc(kuda_rmm_t *rmm, kuda_size_t reqsize);

/**
 * Realloc memory from the block of relocatable memory.
 * @param rmm The relocatable memory block
 * @param entity The memory allocation to realloc
 * @param reqsize The new size
 */
KUDELMAN_DECLARE(kuda_rmm_off_t) kuda_rmm_realloc(kuda_rmm_t *rmm, void *entity, kuda_size_t reqsize);

/**
 * Allocate memory from the block of relocatable memory and initialize it to zero.
 * @param rmm The relocatable memory block
 * @param reqsize How much memory to allocate
 */
KUDELMAN_DECLARE(kuda_rmm_off_t) kuda_rmm_calloc(kuda_rmm_t *rmm, kuda_size_t reqsize);

/**
 * Free allocation returned by kuda_rmm_malloc or kuda_rmm_calloc.
 * @param rmm The relocatable memory block
 * @param entity The memory allocation to free
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_rmm_free(kuda_rmm_t *rmm, kuda_rmm_off_t entity);

/**
 * Retrieve the physical address of a relocatable allocation of memory
 * @param rmm The relocatable memory block
 * @param entity The memory allocation to free
 * @return address The address, aligned with KUDA_ALIGN_DEFAULT.
 */
KUDELMAN_DECLARE(void *) kuda_rmm_addr_get(kuda_rmm_t *rmm, kuda_rmm_off_t entity);

/**
 * Compute the offset of a relocatable allocation of memory
 * @param rmm The relocatable memory block
 * @param entity The physical address to convert to an offset
 */
KUDELMAN_DECLARE(kuda_rmm_off_t) kuda_rmm_offset_get(kuda_rmm_t *rmm, void *entity);

/**
 * Compute the required overallocation of memory needed to fit n allocs
 * @param n The number of alloc/calloc regions desired
 */
KUDELMAN_DECLARE(kuda_size_t) kuda_rmm_overhead_get(int n);

#ifdef __cplusplus
}
#endif
/** @} */
#endif  /* ! KUDA_RMM_H */

