/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @file kuda_xml.h
 * @brief Kuda-Delman XML Library
 */
#ifndef KUDA_XML_H
#define KUDA_XML_H

/**
 * @defgroup KUDA_Util_XML XML 
 * @ingroup KUDA_Util
 * @{
 */
#include "kuda_pools.h"
#include "kuda_tables.h"
#include "kuda_file_io.h"

#include "kudelman.h"
#if KUDA_CHARSET_EBCDIC
#include "kuda_xlate.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @package cLHy XML library
 */

/* -------------------------------------------------------------------- */

/* ### these will need to move at some point to a more logical spot */

/** @see kuda_text */
typedef struct kuda_text kuda_text;

/** Structure to keep a linked list of pieces of text */
struct kuda_text {
    /** The current piece of text */
    const char *text;
    /** a pointer to the next piece of text */
    struct kuda_text *next;
};

/** @see kuda_text_header */
typedef struct kuda_text_header kuda_text_header;

/** A list of pieces of text */
struct kuda_text_header {
    /** The first piece of text in the list */
    kuda_text *first;
    /** The last piece of text in the list */
    kuda_text *last;
};

/**
 * Append a piece of text to the end of a list
 * @param p The pool to allocate out of
 * @param hdr The text header to append to
 * @param text The new text to append
 */
KUDELMAN_DECLARE(void) kuda_text_append(kuda_pool_t *p, kuda_text_header *hdr,
                                  const char *text);


/* --------------------------------------------------------------------
**
** XML PARSING
*/

/*
** Qualified namespace values
**
** KUDA_XML_NS_DAV_ID
**    We always insert the "DAV:" namespace URI at the head of the
**    namespace array. This means that it will always be at ID==0,
**    making it much easier to test for.
**
** KUDA_XML_NS_NONE
**    This special ID is used for two situations:
**
**    1) The namespace prefix begins with "xml" (and we do not know
**       what it means). Namespace prefixes with "xml" (any case) as
**       their first three characters are reserved by the XML Namespaces
**       specification for future use. capi_dav will pass these through
**       unchanged. When this identifier is used, the prefix is LEFT in
**       the element/attribute name. Downstream processing should not
**       prepend another prefix.
**
**    2) The element/attribute does not have a namespace.
**
**       a) No prefix was used, and a default namespace has not been
**          defined.
**       b) No prefix was used, and the default namespace was specified
**          to mean "no namespace". This is done with a namespace
**          declaration of:  xmlns=""
**          (this declaration is typically used to override a previous
**          specification for the default namespace)
**
**       In these cases, we need to record that the elem/attr has no
**       namespace so that we will not attempt to prepend a prefix.
**       All namespaces that are used will have a prefix assigned to
**       them -- capi_dav will never set or use the default namespace
**       when generating XML. This means that "no prefix" will always
**       mean "no namespace".
**
**    In both cases, the XML generation will avoid prepending a prefix.
**    For the first case, this means the original prefix/name will be
**    inserted into the output stream. For the latter case, it means
**    the name will have no prefix, and since we never define a default
**    namespace, this means it will have no namespace.
**
** Note: currently, capi_dav understands the "xmlns" prefix and the
**     "xml:lang" attribute. These are handled specially (they aren't
**     left within the XML tree), so the KUDA_XML_NS_NONE value won't ever
**     really apply to these values.
*/
#define KUDA_XML_NS_DAV_ID	0	/**< namespace ID for "DAV:" */
#define KUDA_XML_NS_NONE		-10	/**< no namespace for this elem/attr */

#define KUDA_XML_NS_ERROR_BASE	-100	/**< used only during processing */
/** Is this namespace an error? */
#define KUDA_XML_NS_IS_ERROR(e)	((e) <= KUDA_XML_NS_ERROR_BASE)

/** @see kuda_xml_attr */
typedef struct kuda_xml_attr kuda_xml_attr;
/** @see kuda_xml_elem */
typedef struct kuda_xml_elem kuda_xml_elem;
/** @see kuda_xml_doc */
typedef struct kuda_xml_doc kuda_xml_doc;

/** kuda_xml_attr: holds a parsed XML attribute */
struct kuda_xml_attr {
    /** attribute name */
    const char *name;
    /** index into namespace array */
    int ns;

    /** attribute value */
    const char *value;

    /** next attribute */
    struct kuda_xml_attr *next;
};

/** kuda_xml_elem: holds a parsed XML element */
struct kuda_xml_elem {
    /** element name */
    const char *name;
    /** index into namespace array */
    int ns;
    /** xml:lang for attrs/contents */
    const char *lang;

    /** cdata right after start tag */
    kuda_text_header first_cdata;
    /** cdata after MY end tag */
    kuda_text_header following_cdata;

    /** parent element */
    struct kuda_xml_elem *parent;	
    /** next (sibling) element */
    struct kuda_xml_elem *next;	
    /** first child element */
    struct kuda_xml_elem *first_child;
    /** first attribute */
    struct kuda_xml_attr *attr;		

    /* used only during parsing */
    /** last child element */
    struct kuda_xml_elem *last_child;
    /** namespaces scoped by this elem */
    struct kuda_xml_ns_scope *ns_scope;

    /* used by cAPIs during request processing */
    /** Place for cAPIs to store private data */
    void *priv;
};

/** Is this XML element empty? */
#define KUDA_XML_ELEM_IS_EMPTY(e) ((e)->first_child == NULL && \
                                  (e)->first_cdata.first == NULL)

/** kuda_xml_doc: holds a parsed XML document */
struct kuda_xml_doc {
    /** root element */
    kuda_xml_elem *root;	
    /** array of namespaces used */
    kuda_array_header_t *namespaces;
};

/** Opaque XML parser structure */
typedef struct kuda_xml_parser kuda_xml_parser;

/**
 * Create an XML parser
 * @param pool The pool for allocating the parser and the parse results.
 * @return The new parser.
 */
KUDELMAN_DECLARE(kuda_xml_parser *) kuda_xml_parser_create(kuda_pool_t *pool);

/**
 * Parse a File, producing a xml_doc
 * @param p      The pool for allocating the parse results.
 * @param parser A pointer to *parser (needed so calling function can get
 *               errors), will be set to NULL on successful completion.
 * @param ppdoc  A pointer to *kuda_xml_doc (which has the parsed results in it)
 * @param xmlfd  A file to read from.
 * @param buffer_length Buffer length which would be suitable 
 * @return Any errors found during parsing.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_xml_parse_file(kuda_pool_t *p,
                                             kuda_xml_parser **parser,
                                             kuda_xml_doc **ppdoc,
                                             kuda_file_t *xmlfd,
                                             kuda_size_t buffer_length);


/**
 * Feed input into the parser
 * @param parser The XML parser for parsing this data.
 * @param data The data to parse.
 * @param len The length of the data.
 * @return Any errors found during parsing.
 * @remark Use kuda_xml_parser_geterror() to get more error information.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_xml_parser_feed(kuda_xml_parser *parser,
                                              const char *data,
                                              kuda_size_t len);

/**
 * Terminate the parsing and return the result
 * @param parser The XML parser for parsing this data.
 * @param pdoc The resulting parse information. May be NULL to simply
 *             terminate the parsing without fetching the info.
 * @return Any errors found during the final stage of parsing.
 * @remark Use kuda_xml_parser_geterror() to get more error information.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_xml_parser_done(kuda_xml_parser *parser,
                                              kuda_xml_doc **pdoc);

/**
 * Fetch additional error information from the parser.
 * @param parser The XML parser to query for errors.
 * @param errbuf A buffer for storing error text.
 * @param errbufsize The length of the error text buffer.
 * @return The error buffer
 */
KUDELMAN_DECLARE(char *) kuda_xml_parser_geterror(kuda_xml_parser *parser,
                                            char *errbuf,
                                            kuda_size_t errbufsize);


/**
 * Converts an XML element tree to flat text 
 * @param p The pool to allocate out of
 * @param elem The XML element to convert
 * @param style How to covert the XML.  One of:
 * <PRE>
 *     KUDA_XML_X2T_FULL                start tag, contents, end tag 
 *     KUDA_XML_X2T_INNER               contents only 
 *     KUDA_XML_X2T_LANG_INNER          xml:lang + inner contents 
 *     KUDA_XML_X2T_FULL_NS_LANG        FULL + ns defns + xml:lang 
 *     KUDA_XML_X2T_PARSED              original prefixes
 * </PRE>
 * @param namespaces The namespace of the current XML element
 * @param ns_map Namespace mapping
 * @param pbuf Buffer to put the converted text into
 * @param psize Size of the converted text
 */
KUDELMAN_DECLARE(void) kuda_xml_to_text(kuda_pool_t *p, const kuda_xml_elem *elem,
                                  int style, kuda_array_header_t *namespaces,
                                  int *ns_map, const char **pbuf,
                                  kuda_size_t *psize);

/* style argument values: */
#define KUDA_XML_X2T_FULL         0	/**< start tag, contents, end tag */
#define KUDA_XML_X2T_INNER        1	/**< contents only */
#define KUDA_XML_X2T_LANG_INNER   2	/**< xml:lang + inner contents */
#define KUDA_XML_X2T_FULL_NS_LANG 3	/**< FULL + ns defns + xml:lang */
#define KUDA_XML_X2T_PARSED       4	/**< original prefixes */

/**
 * empty XML element
 * @param p The pool to allocate out of
 * @param elem The XML element to empty
 * @return the string that was stored in the XML element
 */
KUDELMAN_DECLARE(const char *) kuda_xml_empty_elem(kuda_pool_t *p,
                                             const kuda_xml_elem *elem);

/**
 * quote an XML string
 * Replace '\<', '\>', and '\&' with '\&lt;', '\&gt;', and '\&amp;'.
 * @param p The pool to allocate out of
 * @param s The string to quote
 * @param quotes If quotes is true, then replace '&quot;' with '\&quot;'.
 * @return The quoted string
 * @note If the string does not contain special characters, it is not
 * duplicated into the pool and the original string is returned.
 */
KUDELMAN_DECLARE(const char *) kuda_xml_quote_string(kuda_pool_t *p, const char *s,
                                               int quotes);

/**
 * Quote an XML element
 * @param p The pool to allocate out of
 * @param elem The element to quote
 */
KUDELMAN_DECLARE(void) kuda_xml_quote_elem(kuda_pool_t *p, kuda_xml_elem *elem);

/* manage an array of unique URIs: kuda_xml_insert_uri() and KUDA_XML_URI_ITEM() */

/**
 * return the URI's (existing) index, or insert it and return a new index 
 * @param uri_array array to insert into
 * @param uri The uri to insert
 * @return int The uri's index
 */
KUDELMAN_DECLARE(int) kuda_xml_insert_uri(kuda_array_header_t *uri_array,
                                    const char *uri);

/** Get the URI item for this XML element */
#define KUDA_XML_GET_URI_ITEM(ary, i) (((const char * const *)(ary)->elts)[i])

#if KUDA_CHARSET_EBCDIC
/**
 * Convert parsed tree in EBCDIC 
 * @param p The pool to allocate out of
 * @param pdoc The kuda_xml_doc to convert.
 * @param xlate The translation handle to use.
 * @return Any errors found during conversion.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_xml_parser_convert_doc(kuda_pool_t *p,
                                                     kuda_xml_doc *pdoc,
                                                     kuda_xlate_t *convset);
#endif

#ifdef __cplusplus
}
#endif
/** @} */
#endif /* KUDA_XML_H */
