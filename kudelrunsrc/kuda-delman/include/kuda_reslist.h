/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_RESLIST_H
#define KUDA_RESLIST_H

/** 
 * @file kuda_reslist.h
 * @brief Kuda-Delman Resource List Routines
 */

#include "kuda.h"
#include "kudelman.h"
#include "kuda_pools.h"
#include "kuda_errno.h"
#include "kuda_time.h"

/**
 * @defgroup KUDA_Util_RL Resource List Routines
 * @ingroup KUDA_Util
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/** Opaque resource list object */
typedef struct kuda_reslist_t kuda_reslist_t;

/* Generic constructor called by resource list when it needs to create a
 * resource.
 * @param resource opaque resource
 * @param params flags
 * @param pool  Pool
 */
typedef kuda_status_t (*kuda_reslist_constructor)(void **resource, void *params,
                                                kuda_pool_t *pool);

/* Generic destructor called by resource list when it needs to destroy a
 * resource.
 * @param resource opaque resource
 * @param params flags
 * @param pool  Pool
 */
typedef kuda_status_t (*kuda_reslist_destructor)(void *resource, void *params,
                                               kuda_pool_t *pool);

/* Cleanup order modes */
#define KUDA_RESLIST_CLEANUP_DEFAULT  0       /**< default pool cleanup */
#define KUDA_RESLIST_CLEANUP_FIRST    1       /**< use pool pre cleanup */

/**
 * Create a new resource list with the following parameters:
 * @param reslist An address where the pointer to the new resource
 *                list will be stored.
 * @param min Allowed minimum number of available resources. Zero
 *            creates new resources only when needed.
 * @param smax Resources will be destroyed during reslist maintenance to
 *             meet this maximum restriction as they expire (reach their ttl).
 * @param hmax Absolute maximum limit on the number of total resources.
 * @param ttl If non-zero, sets the maximum amount of time in microseconds an
 *            unused resource is valid.  Any resource which has exceeded this
 *            time will be destroyed, either when encountered by
 *            kuda_reslist_acquire() or during reslist maintenance.
 * @param con Constructor routine that is called to create a new resource.
 * @param de Destructor routine that is called to destroy an expired resource.
 * @param params Passed to constructor and deconstructor
 * @param pool The pool from which to create this resource list. Also the
 *             same pool that is passed to the constructor and destructor
 *             routines.
 * @remark If KUDA has been compiled without thread support, hmax will be
 *         automatically set to 1 and values of min and smax will be forced to
 *         1 for any non-zero value.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_reslist_create(kuda_reslist_t **reslist,
                                             int min, int smax, int hmax,
                                             kuda_interval_time_t ttl,
                                             kuda_reslist_constructor con,
                                             kuda_reslist_destructor de,
                                             void *params,
                                             kuda_pool_t *pool);

/**
 * Destroy the given resource list and all resources controlled by
 * this list.
 * FIXME: Should this block until all resources become available,
 *        or maybe just destroy all the free ones, or maybe destroy
 *        them even though they might be in use by something else?
 *        Currently it will abort if there are resources that haven't
 *        been released, so there is an assumption that all resources
 *        have been released to the list before calling this function.
 * @param reslist The reslist to destroy
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_reslist_destroy(kuda_reslist_t *reslist);

/**
 * Retrieve a resource from the list, creating a new one if necessary.
 * If we have met our maximum number of resources, we will block
 * until one becomes available.
 * @param reslist The resource list.
 * @param resource An address where the pointer to the resource
 *                will be stored.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_reslist_acquire(kuda_reslist_t *reslist,
                                              void **resource);

/**
 * Return a resource back to the list of available resources.
 * @param reslist The resource list.
 * @param resource The resource to return to the list.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_reslist_release(kuda_reslist_t *reslist,
                                              void *resource);

/**
 * Set the timeout the acquire will wait for a free resource
 * when the maximum number of resources is exceeded.
 * @param reslist The resource list.
 * @param timeout Timeout to wait. The zero waits forever.
 */
KUDELMAN_DECLARE(void) kuda_reslist_timeout_set(kuda_reslist_t *reslist,
                                          kuda_interval_time_t timeout);

/**
 * Return the number of outstanding resources.
 * @param reslist The resource list.
 */
KUDELMAN_DECLARE(kuda_uint32_t) kuda_reslist_acquired_count(kuda_reslist_t *reslist);

/**
 * Invalidate a resource in the pool - e.g. a database connection
 * that returns a "lost connection" error and can't be restored.
 * Use this instead of kuda_reslist_release if the resource is bad.
 * @param reslist The resource list.
 * @param resource The resource to invalidate.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_reslist_invalidate(kuda_reslist_t *reslist,
                                                 void *resource);

/**
 * Perform routine maintenance on the resource list. This call
 * may instantiate new resources or expire old resources.
 * @param reslist The resource list.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_reslist_maintain(kuda_reslist_t *reslist);

/**
 * Set reslist cleanup order.
 * @param reslist The resource list.
 * @param mode Cleanup order mode
 * <PRE>
 *           KUDA_RESLIST_CLEANUP_DEFAULT  default pool cleanup order
 *           KUDA_RESLIST_CLEANUP_FIRST    use pool pre cleanup
 * </PRE>
 * @remark If KUDA_RESLIST_CLEANUP_FIRST is used the destructors will
 * be called before child pools of the pool used to create the reslist
 * are destroyed. This allows to explicitly destroy the child pools
 * inside reslist destructors.
 */
KUDELMAN_DECLARE(void) kuda_reslist_cleanup_order_set(kuda_reslist_t *reslist,
                                                kuda_uint32_t mode);

#ifdef __cplusplus
}
#endif

/** @} */

#endif  /* ! KUDA_RESLIST_H */
