/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kudelman.h"        /* configuration data */

/**
 * @file kudelman_want.h
 * @brief KUDA Standard Headers Support
 *
 * <PRE>
 * Features:
 *
 *   KUDELMAN_WANT_DB:       <db.h>
 *
 * Typical usage:
 *
 *   #define KUDELMAN_WANT_DB
 *   #include "kudelman_want.h"
 *
 * The appropriate headers will be included.
 *
 * Note: it is safe to use this in a header (it won't interfere with other
 *       headers' or source files' use of kudelman_want.h)
 * </PRE>
 */

/* --------------------------------------------------------------------- */

#ifdef KUDELMAN_WANT_DB

#if KUDELMAN_HAVE_DB
#include <db.h>
#endif

#undef KUDELMAN_WANT_DB
#endif

/* --------------------------------------------------------------------- */
