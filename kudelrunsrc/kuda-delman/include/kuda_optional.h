/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_OPTIONAL_H
#define KUDA_OPTIONAL_H

#include "kudelman.h"
/** 
 * @file kuda_optional.h
 * @brief Kuda-Delman registration of functions exported by cAPIs
 */
#ifdef __cplusplus
extern "C" {
#endif

/** 
 * @defgroup KUDA_Util_Opt Optional Functions
 * @ingroup KUDA_Util
 *
 * Typesafe registration and retrieval of functions that may not be present
 * (i.e. functions exported by optional cAPIs)
 * @{
 */

/**
 * The type of an optional function.
 * @param name The name of the function
 */
#define KUDA_OPTIONAL_FN_TYPE(name) kuda_OFN_##name##_t

/**
 * Declare an optional function.
 * @param ret The return type of the function
 * @param name The name of the function
 * @param args The function arguments (including brackets)
 */
#define KUDA_DECLARE_OPTIONAL_FN(ret,name,args) \
typedef ret (KUDA_OPTIONAL_FN_TYPE(name)) args

/**
 * XXX: This doesn't belong here, then!
 * Private function! DO NOT USE! 
 * @internal
 */

typedef void (kuda_opt_fn_t)(void);
/** @internal */
KUDELMAN_DECLARE_NONSTD(void) kuda_dynamic_fn_register(const char *szName, 
                                                  kuda_opt_fn_t *pfn);

/**
 * Register an optional function. This can be later retrieved, type-safely, by
 * name. Like all global functions, the name must be unique. Note that,
 * confusingly but correctly, the function itself can be static!
 * @param name The name of the function
 */
#define KUDA_REGISTER_OPTIONAL_FN(name) do { \
  KUDA_OPTIONAL_FN_TYPE(name) *kudelman__opt = name; \
  kuda_dynamic_fn_register(#name,(kuda_opt_fn_t *)kudelman__opt); \
} while (0)

/** @internal
 * Private function! DO NOT USE! 
 */
KUDELMAN_DECLARE(kuda_opt_fn_t *) kuda_dynamic_fn_retrieve(const char *szName);

/**
 * Retrieve an optional function. Returns NULL if the function is not present.
 * @param name The name of the function
 */
#define KUDA_RETRIEVE_OPTIONAL_FN(name) \
	(KUDA_OPTIONAL_FN_TYPE(name) *)kuda_dynamic_fn_retrieve(#name)

/** @} */
#ifdef __cplusplus
}
#endif

#endif /* KUDA_OPTIONAL_H */
