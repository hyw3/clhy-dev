/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file kuda_redis.h
 * @brief Client interface for redis
 * @remark To use this interface you must have a separate redis
 * for more information.
 */

#ifndef KUDA_REDIS_H
#define KUDA_REDIS_H

#include "kuda.h"
#include "kuda_pools.h"
#include "kuda_time.h"
#include "kuda_strings.h"
#include "kuda_network_io.h"
#include "kuda_ring.h"
#include "kuda_buckets.h"
#include "kuda_reslist.h"
#include "kuda_hash.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef RC_DEFAULT_SERVER_PORT
#define RC_DEFAULT_SERVER_PORT 6379
#endif

#ifndef RC_DEFAULT_SERVER_MIN
#define RC_DEFAULT_SERVER_MIN 0
#endif

#ifndef RC_DEFAULT_SERVER_SMAX
#define RC_DEFAULT_SERVER_SMAX 1
#endif

#ifndef RC_DEFAULT_SERVER_TTL
#define RC_DEFAULT_SERVER_TTL 600
#endif

/**
 * @defgroup KUDA_Util_RC Redis Client Routines
 * @ingroup KUDA_Util
 * @{
 */

/** Specifies the status of a redis server */
typedef enum
{
    KUDA_RC_SERVER_LIVE, /**< Server is alive and responding to requests */
    KUDA_RC_SERVER_DEAD  /**< Server is not responding to requests */
} kuda_redis_server_status_t;

/** Opaque redis client connection object */
typedef struct kuda_redis_conn_t kuda_redis_conn_t;

/** Redis Server Info Object */
typedef struct kuda_redis_server_t kuda_redis_server_t;
struct kuda_redis_server_t
{
    const char *host; /**< Hostname of this Server */
    kuda_port_t port; /**< Port of this Server */
    kuda_redis_server_status_t status; /**< @see kuda_redis_server_status_t */
#if KUDA_HAS_THREADS || defined(DOXYGEN)
    kuda_reslist_t *conns; /**< Resource list of actual client connections */
#else
    kuda_redis_conn_t *conn;
#endif
    kuda_pool_t *p; /** Pool to use for private allocations */
#if KUDA_HAS_THREADS
    kuda_thread_mutex_t *lock;
#endif
    kuda_time_t btime;
    kuda_uint32_t rwto;
    struct
    {
        int major;
        int minor;
        int patch;
        char *number;
    } version;
};

typedef struct kuda_redis_t kuda_redis_t;

/* Custom hash callback function prototype, user for server selection.
* @param baton user selected baton
* @param data data to hash
* @param data_len length of data
*/
typedef kuda_uint32_t (*kuda_redis_hash_func)(void *baton,
                                            const char *data,
                                            const kuda_size_t data_len);
/* Custom Server Select callback function prototype.
* @param baton user selected baton
* @param rc redis instance, use rc->live_servers to select a node
* @param hash hash of the selected key.
*/
typedef kuda_redis_server_t* (*kuda_redis_server_func)(void *baton,
                                                 kuda_redis_t *rc,
                                                 const kuda_uint32_t hash);

/** Container for a set of redis servers */
struct kuda_redis_t
{
    kuda_uint32_t flags; /**< Flags, Not currently used */
    kuda_uint16_t nalloc; /**< Number of Servers Allocated */
    kuda_uint16_t ntotal; /**< Number of Servers Added */
    kuda_redis_server_t **live_servers; /**< Array of Servers */
    kuda_pool_t *p; /** Pool to use for allocations */
    void *hash_baton;
    kuda_redis_hash_func hash_func;
    void *server_baton;
    kuda_redis_server_func server_func;
};

/**
 * Creates a crc32 hash used to split keys between servers
 * @param rc The redis client object to use
 * @param data Data to be hashed
 * @param data_len Length of the data to use
 * @return crc32 hash of data
 * @remark The crc32 hash is not compatible with old redisd clients.
 */
KUDELMAN_DECLARE(kuda_uint32_t) kuda_redis_hash(kuda_redis_t *rc,
                                         const char *data,
                                         const kuda_size_t data_len);

/**
 * Pure CRC32 Hash. Used by some clients.
 */
KUDELMAN_DECLARE(kuda_uint32_t) kuda_redis_hash_crc32(void *baton,
                                               const char *data,
                                               const kuda_size_t data_len);

/**
 * hash compatible with the standard Perl Client.
 */
KUDELMAN_DECLARE(kuda_uint32_t) kuda_redis_hash_default(void *baton,
                                                 const char *data,
                                                 const kuda_size_t data_len);

/**
 * Picks a server based on a hash
 * @param rc The redis client object to use
 * @param hash Hashed value of a Key
 * @return server that controls specified hash
 * @see kuda_redis_hash
 */
KUDELMAN_DECLARE(kuda_redis_server_t *) kuda_redis_find_server_hash(kuda_redis_t *rc,
                                                             const kuda_uint32_t hash);

/**
 * server selection compatible with the standard Perl Client.
 */
KUDELMAN_DECLARE(kuda_redis_server_t *) kuda_redis_find_server_hash_default(void *baton,
                                                                      kuda_redis_t *rc,
                                                                      const kuda_uint32_t hash);

/**
 * Adds a server to a client object
 * @param rc The redis client object to use
 * @param server Server to add
 * @remark Adding servers is not thread safe, and should be done once at startup.
 * @warning Changing servers after startup may cause keys to go to
 * different servers.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_add_server(kuda_redis_t *rc,
                                               kuda_redis_server_t *server);


/**
 * Finds a Server object based on a hostname/port pair
 * @param rc The redis client object to use
 * @param host Hostname of the server
 * @param port Port of the server
 * @return Server with matching Hostname and Port, or NULL if none was found.
 */
KUDELMAN_DECLARE(kuda_redis_server_t *) kuda_redis_find_server(kuda_redis_t *rc,
                                                        const char *host,
                                                        kuda_port_t port);

/**
 * Enables a Server for use again
 * @param rc The redis client object to use
 * @param rs Server to Activate
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_enable_server(kuda_redis_t *rc,
                                                  kuda_redis_server_t *rs);


/**
 * Disable a Server
 * @param rc The redis client object to use
 * @param rs Server to Disable
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_disable_server(kuda_redis_t *rc,
                                                   kuda_redis_server_t *rs);

/**
 * Creates a new Server Object
 * @param p Pool to use
 * @param host hostname of the server
 * @param port port of the server
 * @param min  minimum number of client sockets to open
 * @param smax soft maximum number of client connections to open
 * @param max  hard maximum number of client connections
 * @param ttl  time to live in microseconds of a client connection
 * @param rwto r/w timeout value in seconds of a client connection
 * @param ns   location of the new server object
 * @see kuda_reslist_create
 * @remark min, smax, and max are only used when KUDA_HAS_THREADS
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_server_create(kuda_pool_t *p,
                                                  const char *host,
                                                  kuda_port_t port,
                                                  kuda_uint32_t min,
                                                  kuda_uint32_t smax,
                                                  kuda_uint32_t max,
                                                  kuda_uint32_t ttl,
                                                  kuda_uint32_t rwto,
                                                  kuda_redis_server_t **ns);
/**
 * Creates a new redisd client object
 * @param p Pool to use
 * @param max_servers maximum number of servers
 * @param flags Not currently used
 * @param rc   location of the new redis client object
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_create(kuda_pool_t *p,
                                           kuda_uint16_t max_servers,
                                           kuda_uint32_t flags,
                                           kuda_redis_t **rc);

/**
 * Gets a value from the server, allocating the value out of p
 * @param rc client to use
 * @param p Pool to use
 * @param key null terminated string containing the key
 * @param baton location of the allocated value
 * @param len   length of data at baton
 * @param flags any flags set by the client for this key
 * @return 
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_getp(kuda_redis_t *rc,
                                         kuda_pool_t *p,
                                         const char* key,
                                         char **baton,
                                         kuda_size_t *len,
                                         kuda_uint16_t *flags);

/**
 * Sets a value by key on the server
 * @param rc client to use
 * @param key   null terminated string containing the key
 * @param baton data to store on the server
 * @param data_size   length of data at baton
 * @param flags any flags set by the client for this key
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_set(kuda_redis_t *rc,
                                        const char *key,
                                        char *baton,
                                        const kuda_size_t data_size,
                                        kuda_uint16_t flags);

/**
 * Sets a value by key on the server
 * @param rc client to use
 * @param key   null terminated string containing the key
 * @param baton data to store on the server
 * @param data_size   length of data at baton
 * @param timeout time in seconds for the data to live on the server
 * @param flags any flags set by the client for this key
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_setex(kuda_redis_t *rc,
                                          const char *key,
                                          char *baton,
                                          const kuda_size_t data_size,
                                          kuda_uint32_t timeout,
                                          kuda_uint16_t flags);

/**
 * Deletes a key from a server
 * @param rc client to use
 * @param key   null terminated string containing the key
 * @param timeout time for the delete to stop other clients from adding
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_delete(kuda_redis_t *rc,
                                           const char *key,
                                           kuda_uint32_t timeout);

/**
 * Query a server's version
 * @param rs    server to query
 * @param p     Pool to allocate answer from
 * @param baton location to store server version string
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_version(kuda_redis_server_t *rs,
                                            kuda_pool_t *p,
                                            char **baton);

/**
 * Query a server's INFO
 * @param rs    server to query
 * @param p     Pool to allocate answer from
 * @param baton location to store server INFO response string
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_info(kuda_redis_server_t *rs,
                                         kuda_pool_t *p,
                                         char **baton);

/**
 * Increments a value
 * @param rc client to use
 * @param key   null terminated string containing the key
 * @param inc     number to increment by
 * @param new_value    new value after incrementing
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_incr(kuda_redis_t *rc,
                                         const char *key,
                                         kuda_int32_t inc,
                                         kuda_uint32_t *new_value);
/**
 * Decrements a value
 * @param rc client to use
 * @param key   null terminated string containing the key
 * @param inc     number to decrement by
 * @param new_value    new value after decrementing
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_decr(kuda_redis_t *rc,
                                         const char *key,
                                         kuda_int32_t inc,
                                         kuda_uint32_t *new_value);


/**
 * Pings the server
 * @param rs Server to ping
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_ping(kuda_redis_server_t *rs);

/**
 * Gets multiple values from the server, allocating the values out of p
 * @param rc client to use
 * @param temp_pool Pool used for temporary allocations. May be cleared inside this
 *        call.
 * @param data_pool Pool used to allocate data for the returned values.
 * @param values hash of kuda_redis_value_t keyed by strings, contains the
 *        result of the multiget call.
 * @return
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_multgetp(kuda_redis_t *rc,
                                             kuda_pool_t *temp_pool,
                                             kuda_pool_t *data_pool,
                                             kuda_hash_t *values);

typedef enum
{
    KUDA_RS_SERVER_MASTER, /**< Server is a master */
    KUDA_RS_SERVER_SLAVE,  /**< Server is a slave */
    KUDA_RS_SERVER_UNKNOWN  /**< Server role is unknown */
} kuda_redis_server_role_t;

typedef struct
{
/* # Server */
    /** Major version number of this server */
    kuda_uint32_t major;
    /** Minor version number of this server */
    kuda_uint32_t minor;
    /** Patch version number of this server */
    kuda_uint32_t patch;
    /** Process id of this server process */
    kuda_uint32_t process_id;
    /** Number of seconds this server has been running */
    kuda_uint32_t uptime_in_seconds;
    /** Bitsize of the arch on the current machine */
    kuda_uint32_t arch_bits;

/* # Clients */
    /** Number of connected clients */
    kuda_uint32_t connected_clients;
    /** Number of blocked clients */
    kuda_uint32_t blocked_clients;

/* # Memory */
    /** Max memory of this server */
    kuda_uint64_t maxmemory;
    /** Amount of used memory */
    kuda_uint64_t used_memory;
    /** Total memory available on this server */
    kuda_uint64_t total_system_memory;

/* # Stats */
    /** Total connections received */
    kuda_uint64_t total_connections_received;
    /** Total commands processed */
    kuda_uint64_t total_commands_processed;
    /** Total commands rejected */
    kuda_uint64_t rejected_connections;
    /** Total net input bytes */
    kuda_uint64_t total_net_input_bytes;
    /** Total net output bytes */
    kuda_uint64_t total_net_output_bytes;
    /** Keyspace hits */
    kuda_uint64_t keyspace_hits;
    /** Keyspace misses */
    kuda_uint64_t keyspace_misses;

/* # Replication */
    /** Role */
    kuda_redis_server_role_t role;
    /** Number of connected slave */
    kuda_uint32_t connected_slaves;

/* # CPU */
    /** Accumulated CPU user time for this process */
    kuda_uint32_t used_cpu_sys;
    /** Accumulated CPU system time for this process */
    kuda_uint32_t used_cpu_user;

/* # Cluster */
    /** Is cluster enabled */
    kuda_uint32_t cluster_enabled;
} kuda_redis_stats_t;

/**
 * Query a server for statistics
 * @param rs    server to query
 * @param p     Pool to allocate answer from
 * @param stats location of the new statistics structure
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_redis_stats(kuda_redis_server_t *rs,
                                          kuda_pool_t *p,
                                          kuda_redis_stats_t **stats);

/** @} */

#ifdef __cplusplus
}
#endif

#endif /* KUDA_REDIS_H */
