/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * kudelman.h is generated from kudelman.h.in by configure -- do not edit kudelman.h
 */
/* @file kudelman.h
 * @brief Kuda-Delman main file
 */
/**
 * @defgroup KUDA_Util Kuda Delman Utility Functions
 * @{
 */


#ifndef KUDELMAN_H
#define KUDELMAN_H

/**
 * KUDELMAN_DECLARE_EXPORT is defined when building the Kuda-Delman dynamic library,
 * so that all public symbols are exported.
 *
 * KUDELMAN_DECLARE_STATIC is defined when including the Kuda-Delman public headers,
 * to provide static linkage when the dynamic library may be unavailable.
 *
 * KUDELMAN_DECLARE_STATIC and KUDELMAN_DECLARE_EXPORT are left undefined when
 * including the Kuda-Delman public headers, to import and link the symbols from 
 * the dynamic Kuda-Delman library and assure appropriate indirection and calling
 * conventions at compile time.
 */

#if defined(DOXYGEN) || !defined(WIN32)
/**
 * The public Kuda-Delman functions are declared with KUDELMAN_DECLARE(), so they may
 * use the most appropriate calling convention.  Public KUDA functions with 
 * variable arguments must use KUDELMAN_DECLARE_NONSTD().
 *
 * @fn KUDELMAN_DECLARE(rettype) kuda_func(args);
 */
#define KUDELMAN_DECLARE(type)            type
/**
 * The public Kuda-Delman functions using variable arguments are declared with 
 * KUDELMAN_DECLARE_NONSTD(), as they must use the C language calling convention.
 *
 * @fn KUDELMAN_DECLARE_NONSTD(rettype) kuda_func(args, ...);
 */
#define KUDELMAN_DECLARE_NONSTD(type)     type
/**
 * The public Kuda-Delman variables are declared with KUDELMAN_DECLARE_DATA.
 * This assures the appropriate indirection is invoked at compile time.
 *
 * @fn KUDELMAN_DECLARE_DATA type kuda_variable;
 * @note KUDELMAN_DECLARE_DATA extern type kuda_variable; syntax is required for
 * declarations within headers to properly import the variable.
 */
#define KUDELMAN_DECLARE_DATA
#elif defined(KUDELMAN_DECLARE_STATIC)
#define KUDELMAN_DECLARE(type)            type __stdcall
#define KUDELMAN_DECLARE_NONSTD(type)     type __cdecl
#define KUDELMAN_DECLARE_DATA
#elif defined(KUDELMAN_DECLARE_EXPORT)
#define KUDELMAN_DECLARE(type)            __declspec(dllexport) type __stdcall
#define KUDELMAN_DECLARE_NONSTD(type)     __declspec(dllexport) type __cdecl
#define KUDELMAN_DECLARE_DATA             __declspec(dllexport)
#else
#define KUDELMAN_DECLARE(type)            __declspec(dllimport) type __stdcall
#define KUDELMAN_DECLARE_NONSTD(type)     __declspec(dllimport) type __cdecl
#define KUDELMAN_DECLARE_DATA             __declspec(dllimport)
#endif

#if !defined(WIN32) || defined(KUDELMAN_CAPI_DECLARE_STATIC)
/**
 * Declare a dso cAPI's exported cAPI structure as KUDELMAN_CAPI_DECLARE_DATA.
 *
 * Unless KUDELMAN_CAPI_DECLARE_STATIC is defined at compile time, symbols 
 * declared with KUDELMAN_CAPI_DECLARE_DATA are always exported.
 * @code
 * cAPI KUDELMAN_CAPI_DECLARE_DATA capi_tag
 * @endcode
 */
#define KUDELMAN_CAPI_DECLARE_DATA
#else
#define KUDELMAN_CAPI_DECLARE_DATA           __declspec(dllexport)
#endif

/*
 * we always have SDBM (it's in our codebase)
 */
#define KUDELMAN_HAVE_SDBM   @kudelman_have_sdbm@
#define KUDELMAN_HAVE_GDBM   @kudelman_have_gdbm@
#define KUDELMAN_HAVE_NDBM   @kudelman_have_ndbm@
#define KUDELMAN_HAVE_DB     @kudelman_have_db@

#if KUDELMAN_HAVE_DB
#define KUDELMAN_HAVE_DB_VERSION    @kudelman_db_version@
#endif

#define KUDELMAN_HAVE_PGSQL         @kudelman_have_pgsql@
#define KUDELMAN_HAVE_MYSQL         @kudelman_have_mysql@
#define KUDELMAN_HAVE_SQLITE3       @kudelman_have_sqlite3@
#define KUDELMAN_HAVE_SQLITE2       @kudelman_have_sqlite2@
#define KUDELMAN_HAVE_ORACLE        @kudelman_have_oracle@
#define KUDELMAN_HAVE_ODBC          @kudelman_have_odbc@

#define KUDELMAN_HAVE_CRYPTO        @kudelman_have_crypto@
#define KUDELMAN_HAVE_OPENSSL       @kudelman_have_openssl@
#define KUDELMAN_HAVE_NSS           @kudelman_have_nss@
#define KUDELMAN_HAVE_COMMONCRYPTO  @kudelman_have_commoncrypto@

#define KUDELMAN_HAVE_KUDA_ICONV     @have_kuda_iconv@
#define KUDELMAN_HAVE_ICONV         @have_iconv@
#define KUDA_HAS_XLATE          (KUDELMAN_HAVE_KUDA_ICONV || KUDELMAN_HAVE_ICONV)

#endif /* KUDELMAN_H */
/** @} */
