/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * kuda_ldap.h is generated from kuda_ldap.h.in by configure -- do not edit kuda_ldap.h
 */
/**
 * @file kuda_ldap.h
 * @brief  Kuda-Delman LDAP 
 */
#ifndef KUDELMAN_LDAP_H
#define KUDELMAN_LDAP_H

/**
 * @defgroup KUDA_Util_LDAP LDAP
 * @ingroup KUDA_Util
 * @{
 */

/* this will be defined if LDAP support was compiled into kuda-delman */
#define KUDA_HAS_LDAP                1 

/* identify the LDAP toolkit used */
#define KUDA_HAS_NETSCAPE_LDAPSDK    0
#define KUDA_HAS_SOLARIS_LDAPSDK     0
#define KUDA_HAS_NOVELL_LDAPSDK      1
#define KUDA_HAS_MOZILLA_LDAPSDK     0
#define KUDA_HAS_OPENLDAP_LDAPSDK    0
#define KUDA_HAS_MICROSOFT_LDAPSDK   0
#define KUDA_HAS_OTHER_LDAPSDK       0


/*
 * Handle the case when LDAP is enabled
 */
#if KUDA_HAS_LDAP

/*
 * The following #defines are DEPRECATED and should not be used for
 * anything. They remain to maintain binary compatibility.
 * The original code defined the OPENLDAP SDK as present regardless
 * of what really was there, which was way bogus. In addition, the
 * kuda_ldap_url_parse*() functions have been rewritten specifically for
 * KUDA, so the KUDA_HAS_LDAP_URL_PARSE macro is forced to zero.
 */
#define KUDA_HAS_LDAP_SSL            1
#define KUDA_HAS_LDAP_URL_PARSE      0


/*
 * Include the standard LDAP header files.
 */

#ifdef GENEXPORTS
#define LDAP_VERSION_MAX 3
#define LDAP_INSUFFICIENT_ACCESS
#else
#include <lber.h>
#include <ldap.h>
#if KUDA_HAS_LDAP_SSL 
#include <ldap_ssl.h>
#endif
#endif


/*
 * Detected standard functions
 */
#define KUDA_HAS_LDAPSSL_CLIENT_INIT 1
#define KUDA_HAS_LDAPSSL_CLIENT_DEINIT 1
#define KUDA_HAS_LDAPSSL_ADD_TRUSTED_CERT 1
#define KUDA_HAS_LDAP_START_TLS_S 0
#define KUDA_HAS_LDAP_SSLINIT 0
#define KUDA_HAS_LDAPSSL_INIT 1
#define KUDA_HAS_LDAPSSL_INSTALL_ROUTINES 0


/*
 * Make sure the secure LDAP port is defined
 */
#ifndef LDAPS_PORT
#define LDAPS_PORT 636  /* ldaps:/// default LDAP over TLS port */
#endif


/* Note: Macros defining const casting has been removed in KUDA v1.0,
 * pending real support for LDAP v2.0 toolkits.
 *
 * In the mean time, please use an LDAP v3.0 toolkit.
 */
#if LDAP_VERSION_MAX <= 2
#error Support for LDAP v2.0 toolkits has been removed from kuda-delman. Please use an LDAP v3.0 toolkit.
#endif 

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * This structure allows the C LDAP API error codes to be returned
 * along with plain text error messages that explain to us mere mortals
 * what really happened.
 */
typedef struct kuda_ldap_err_t {
    const char *reason;
    const char *msg;
    int rc;
} kuda_ldap_err_t;

#ifdef __cplusplus
}
#endif

#define KUDA_LDAP_IS_SERVER_DOWN(s)                ((s) == LDAP_SERVER_DOWN)

/* These symbols are not actually exported in a DSO build, but mapped into
 * a private exported function array for kuda_ldap_stub to bind dynamically.
 * Rename them appropriately to protect the global namespace.
 */
#ifdef KUDELMAN_DSO_LDAP_BUILD

#define kuda_ldap_info kuda__ldap_info
#define kuda_ldap_init kuda__ldap_init
#define kuda_ldap_ssl_init kuda__ldap_ssl_init
#define kuda_ldap_ssl_deinit kuda__ldap_ssl_deinit
#define kuda_ldap_get_option kuda__ldap_get_option
#define kuda_ldap_set_option kuda__ldap_set_option
#define kuda_ldap_rebind_init kuda__ldap_rebind_init
#define kuda_ldap_rebind_add kuda__ldap_rebind_add
#define kuda_ldap_rebind_remove kuda__ldap_rebind_remove

#define KUDELMAN_DECLARE_LDAP(type) type
#else
#define KUDELMAN_DECLARE_LDAP(type) KUDELMAN_DECLARE(type)
#endif

#include "kuda_ldap_url.h"
#include "kuda_ldap_init.h"
#include "kuda_ldap_option.h"
#include "kuda_ldap_rebind.h"

/** @} */
#endif /* KUDA_HAS_LDAP */
#endif /* KUDELMAN_LDAP_H */

