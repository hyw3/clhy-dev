/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * sdbm - ndbm work-alike hashed database library
 * based on Per-Ake Larson's Dynamic Hashing algorithms. BIT 18 (1978).
 * author: oz@nexus.yorku.ca
 * status: ex-public domain
 */

#ifndef KUDA_SDBM_H
#define KUDA_SDBM_H

#include "kudelman.h"
#include "kuda_errno.h"
#include "kuda_file_io.h"   /* for kuda_fileperms_t */

/** 
 * @file kuda_sdbm.h
 * @brief kuda-delman SDBM library
 */
/**
 * @defgroup KUDA_Util_DBM_SDBM SDBM library
 * @ingroup KUDA_Util_DBM
 * @{
 */

/**
 * Structure for referencing an sdbm
 */
typedef struct kuda_sdbm_t kuda_sdbm_t;

/**
 * Structure for referencing the datum record within an sdbm
 */
typedef struct {
    /** pointer to the data stored/retrieved */
    char *dptr;
    /** size of data */
    /* kuda_ssize_t for release 2.0??? */
    int dsize;
} kuda_sdbm_datum_t;

/* The extensions used for the database files */
/** SDBM Directory file extension */
#define KUDA_SDBM_DIRFEXT	".dir"
/** SDBM page file extension */
#define KUDA_SDBM_PAGFEXT	".pag"

/* flags to sdbm_store */
#define KUDA_SDBM_INSERT     0   /**< Insert */
#define KUDA_SDBM_REPLACE    1   /**< Replace */
#define KUDA_SDBM_INSERTDUP  2   /**< Insert with duplicates */

/**
 * Open an sdbm database by file name
 * @param db The newly opened database
 * @param name The sdbm file to open
 * @param mode The flag values (KUDA_READ and KUDA_BINARY flags are implicit)
 * <PRE>
 *           KUDA_WRITE          open for read-write access
 *           KUDA_CREATE         create the sdbm if it does not exist
 *           KUDA_TRUNCATE       empty the contents of the sdbm
 *           KUDA_EXCL           fail for KUDA_CREATE if the file exists
 *           KUDA_DELONCLOSE     delete the sdbm when closed
 *           KUDA_SHARELOCK      support locking across process/machines
 * </PRE>
 * @param perms Permissions to apply to if created
 * @param p The pool to use when creating the sdbm
 * @remark The sdbm name is not a true file name, as sdbm appends suffixes 
 * for seperate data and index files.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_open(kuda_sdbm_t **db, const char *name, 
                                        kuda_int32_t mode, 
                                        kuda_fileperms_t perms, kuda_pool_t *p);

/**
 * Close an sdbm file previously opened by kuda_sdbm_open
 * @param db The database to close
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_close(kuda_sdbm_t *db);

/**
 * Lock an sdbm database for concurency of multiple operations
 * @param db The database to lock
 * @param type The lock type
 * <PRE>
 *           KUDA_FLOCK_SHARED
 *           KUDA_FLOCK_EXCLUSIVE
 * </PRE>
 * @remark Calls to kuda_sdbm_lock may be nested.  All kuda_sdbm functions
 * perform implicit locking.  Since an KUDA_FLOCK_SHARED lock cannot be 
 * portably promoted to an KUDA_FLOCK_EXCLUSIVE lock, kuda_sdbm_store and 
 * kuda_sdbm_delete calls will fail if an KUDA_FLOCK_SHARED lock is held.
 * The kuda_sdbm_lock call requires the database to be opened with the
 * KUDA_SHARELOCK mode value.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_lock(kuda_sdbm_t *db, int type);

/**
 * Release an sdbm lock previously aquired by kuda_sdbm_lock
 * @param db The database to unlock
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_unlock(kuda_sdbm_t *db);

/**
 * Fetch an sdbm record value by key
 * @param db The database 
 * @param value The value datum retrieved for this record
 * @param key The key datum to find this record
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_fetch(kuda_sdbm_t *db, 
                                         kuda_sdbm_datum_t *value, 
                                         kuda_sdbm_datum_t key);

/**
 * Store an sdbm record value by key
 * @param db The database 
 * @param key The key datum to store this record by
 * @param value The value datum to store in this record
 * @param opt The method used to store the record
 * <PRE>
 *           KUDA_SDBM_INSERT     return an error if the record exists
 *           KUDA_SDBM_REPLACE    overwrite any existing record for key
 * </PRE>
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_store(kuda_sdbm_t *db, kuda_sdbm_datum_t key,
                                         kuda_sdbm_datum_t value, int opt);

/**
 * Delete an sdbm record value by key
 * @param db The database 
 * @param key The key datum of the record to delete
 * @remark It is not an error to delete a non-existent record.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_delete(kuda_sdbm_t *db, 
                                          const kuda_sdbm_datum_t key);

/**
 * Retrieve the first record key from a dbm
 * @param db The database 
 * @param key The key datum of the first record
 * @remark The keys returned are not ordered.  To traverse the list of keys
 * for an sdbm opened with KUDA_SHARELOCK, the caller must use kuda_sdbm_lock
 * prior to retrieving the first record, and hold the lock until after the
 * last call to kuda_sdbm_nextkey.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_firstkey(kuda_sdbm_t *db, kuda_sdbm_datum_t *key);

/**
 * Retrieve the next record key from an sdbm
 * @param db The database 
 * @param key The key datum of the next record
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_nextkey(kuda_sdbm_t *db, kuda_sdbm_datum_t *key);

/**
 * Returns true if the sdbm database opened for read-only access
 * @param db The database to test
 */
KUDELMAN_DECLARE(int) kuda_sdbm_rdonly(kuda_sdbm_t *db);
/** @} */
#endif /* KUDA_SDBM_H */
