/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_XLATE_H
#define KUDA_XLATE_H

#include "kudelman.h"
#include "kuda_pools.h"
#include "kuda_errno.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @file kuda_xlate.h
 * @brief KUDA I18N translation library
 */

/**
 * @defgroup KUDA_XLATE I18N translation library
 * @ingroup KUDA
 * @{
 */
/** Opaque translation buffer */
typedef struct kuda_xlate_t            kuda_xlate_t;

/**
 * Set up for converting text from one charset to another.
 * @param convset The handle to be filled in by this function
 * @param topage The name of the target charset
 * @param frompage The name of the source charset
 * @param pool The pool to use
 * @remark
 *  Specify KUDA_DEFAULT_CHARSET for one of the charset
 *  names to indicate the charset of the source code at
 *  compile time.  This is useful if there are literal
 *  strings in the source code which must be translated
 *  according to the charset of the source code.
 *  KUDA_DEFAULT_CHARSET is not useful if the source code
 *  of the caller was not encoded in the same charset as
 *  KUDA at compile time.
 *
 * @remark
 *  Specify KUDA_LOCALE_CHARSET for one of the charset
 *  names to indicate the charset of the current locale.
 *
 * @remark
 *  Return KUDA_EINVAL if unable to procure a convset, or KUDA_ENOTIMPL
 *  if charset transcoding is not available in this instance of
 *  kuda-delman at all (i.e., KUDA_HAS_XLATE is undefined).
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_xlate_open(kuda_xlate_t **convset, 
                                         const char *topage, 
                                         const char *frompage, 
                                         kuda_pool_t *pool);

/** 
 * This is to indicate the charset of the sourcecode at compile time
 * names to indicate the charset of the source code at
 * compile time.  This is useful if there are literal
 * strings in the source code which must be translated
 * according to the charset of the source code.
 */
#define KUDA_DEFAULT_CHARSET (const char *)0
/**
 * To indicate charset names of the current locale 
 */
#define KUDA_LOCALE_CHARSET (const char *)1

/**
 * Find out whether or not the specified conversion is single-byte-only.
 * @param convset The handle allocated by kuda_xlate_open, specifying the 
 *                parameters of conversion
 * @param onoff Output: whether or not the conversion is single-byte-only
 * @remark
 *  Return KUDA_ENOTIMPL if charset transcoding is not available
 *  in this instance of kuda-delman (i.e., KUDA_HAS_XLATE is undefined).
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_xlate_sb_get(kuda_xlate_t *convset, int *onoff);

/**
 * Convert a buffer of text from one codepage to another.
 * @param convset The handle allocated by kuda_xlate_open, specifying 
 *                the parameters of conversion
 * @param inbuf The address of the source buffer
 * @param inbytes_left Input: the amount of input data to be translated
 *                     Output: the amount of input data not yet translated    
 * @param outbuf The address of the destination buffer
 * @param outbytes_left Input: the size of the output buffer
 *                      Output: the amount of the output buffer not yet used
 * @remark
 * Returns KUDA_ENOTIMPL if charset transcoding is not available
 * in this instance of kuda-delman (i.e., KUDA_HAS_XLATE is undefined).
 * Returns KUDA_INCOMPLETE if the input buffer ends in an incomplete
 * multi-byte character.
 *
 * To correctly terminate the output buffer for some multi-byte
 * character set encodings, a final call must be made to this function
 * after the complete input string has been converted, passing
 * the inbuf and inbytes_left parameters as NULL.  (Note that this
 * mode only works from version 1.1.0 onwards)
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_xlate_conv_buffer(kuda_xlate_t *convset, 
                                                const char *inbuf, 
                                                kuda_size_t *inbytes_left, 
                                                char *outbuf,
                                                kuda_size_t *outbytes_left);

/* @see kuda_file_io.h the comment in kuda_file_io.h about this hack */
#ifdef KUDA_NOT_DONE_YET
/**
 * The purpose of kuda_xlate_conv_char is to translate one character
 * at a time.  This needs to be written carefully so that it works
 * with double-byte character sets. 
 * @param convset The handle allocated by kuda_xlate_open, specifying the
 *                parameters of conversion
 * @param inchar The character to convert
 * @param outchar The converted character
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_xlate_conv_char(kuda_xlate_t *convset, 
                                              char inchar, char outchar);
#endif

/**
 * Convert a single-byte character from one charset to another.
 * @param convset The handle allocated by kuda_xlate_open, specifying the 
 *                parameters of conversion
 * @param inchar The single-byte character to convert.
 * @warning This only works when converting between single-byte character sets.
 *          -1 will be returned if the conversion can't be performed.
 */
KUDELMAN_DECLARE(kuda_int32_t) kuda_xlate_conv_byte(kuda_xlate_t *convset, 
                                             unsigned char inchar);

/**
 * Close a codepage translation handle.
 * @param convset The codepage translation handle to close
 * @remark
 *  Return KUDA_ENOTIMPL if charset transcoding is not available
 *  in this instance of kuda-delman (i.e., KUDA_HAS_XLATE is undefined).
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_xlate_close(kuda_xlate_t *convset);

/** @} */
#ifdef __cplusplus
}
#endif

#endif  /* ! KUDA_XLATE_H */
