/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @file kuda_optional_hooks.h
 * @brief cLHy optional hook functions
 */


#ifndef KUDA_OPTIONAL_HOOK_H
#define KUDA_OPTIONAL_HOOK_H

#include "kuda_tables.h"

#ifdef __cplusplus
extern "C" {
#endif
/** 
 * @defgroup KUDA_Util_OPT_HOOK Optional Hook Functions
 * @ingroup KUDA_Util_Hook
 * @{
 */
/**
 * Function to implement the KUDA_OPTIONAL_HOOK Macro
 * @internal
 * @see KUDA_OPTIONAL_HOOK
 *
 * @param szName The name of the hook
 * @param pfn A pointer to a function that will be called
 * @param aszPre a NULL-terminated array of strings that name cAPIs whose hooks should precede this one
 * @param aszSucc a NULL-terminated array of strings that name cAPIs whose hooks should succeed this one
 * @param nOrder an integer determining order before honouring aszPre and aszSucc (for example HOOK_MIDDLE)
 */


KUDELMAN_DECLARE(void) kuda_optional_hook_add(const char *szName,void (*pfn)(void),
					const char * const *aszPre,
					const char * const *aszSucc,
					int nOrder);

/**
 * Hook to an optional hook.
 *
 * @param ns The namespace prefix of the hook functions
 * @param name The name of the hook
 * @param pfn A pointer to a function that will be called
 * @param aszPre a NULL-terminated array of strings that name cAPIs whose hooks should precede this one
 * @param aszSucc a NULL-terminated array of strings that name cAPIs whose hooks should succeed this one
 * @param nOrder an integer determining order before honouring aszPre and aszSucc (for example HOOK_MIDDLE)
 */

#define KUDA_OPTIONAL_HOOK(ns,name,pfn,aszPre,aszSucc,nOrder) do { \
  ns##_HOOK_##name##_t *kudelman__hook = pfn; \
  kuda_optional_hook_add(#name,(void (*)(void))kudelman__hook,aszPre, aszSucc, nOrder); \
} while (0)

/**
 * @internal
 * @param szName - the name of the function
 * @return the hook structure for a given hook
 */
KUDELMAN_DECLARE(kuda_array_header_t *) kuda_optional_hook_get(const char *szName);

/**
 * Implement an optional hook that runs until one of the functions
 * returns something other than OK or DECLINE.
 *
 * @param ns The namespace prefix of the hook functions
 * @param link The linkage declaration prefix of the hook
 * @param ret The type of the return value of the hook
 * @param ret The type of the return value of the hook
 * @param name The name of the hook
 * @param args_decl The declaration of the arguments for the hook
 * @param args_use The names for the arguments for the hook
 * @param ok Success value
 * @param decline Decline value
 */
#define KUDA_IMPLEMENT_OPTIONAL_HOOK_RUN_ALL(ns,link,ret,name,args_decl,args_use,ok,decline) \
link##_DECLARE(ret) ns##_run_##name args_decl \
    { \
    ns##_LINK_##name##_t *pHook; \
    int n; \
    ret rv; \
    kuda_array_header_t *pHookArray=kuda_optional_hook_get(#name); \
\
    if(!pHookArray) \
	return ok; \
\
    pHook=(ns##_LINK_##name##_t *)pHookArray->elts; \
    for(n=0 ; n < pHookArray->nelts ; ++n) \
	{ \
	rv=(pHook[n].pFunc)args_use; \
\
	if(rv != ok && rv != decline) \
	    return rv; \
	} \
    return ok; \
    }

/** @} */
#ifdef __cplusplus
}
#endif

#endif /* KUDA_OPTIONAL_HOOK_H */
