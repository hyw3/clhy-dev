/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file kuda_uuid.h
 * @brief KUDA UUID library
 */
#ifndef KUDA_UUID_H
#define KUDA_UUID_H

#include "kudelman.h"
#include "kuda_errno.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * @defgroup KUDA_UUID UUID Handling
 * @ingroup KUDA
 * @{
 */

/**
 * we represent a UUID as a block of 16 bytes.
 */

typedef struct {
    unsigned char data[16]; /**< the actual UUID */
} kuda_uuid_t;

/** UUIDs are formatted as: 00112233-4455-6677-8899-AABBCCDDEEFF */
#define KUDA_UUID_FORMATTED_LENGTH 36


/**
 * Generate and return a (new) UUID
 * @param uuid The resulting UUID
 */ 
KUDELMAN_DECLARE(void) kuda_uuid_get(kuda_uuid_t *uuid);

/**
 * Format a UUID into a string, following the standard format
 * @param buffer The buffer to place the formatted UUID string into. It must
 *               be at least KUDA_UUID_FORMATTED_LENGTH + 1 bytes long to hold
 *               the formatted UUID and a null terminator
 * @param uuid The UUID to format
 */ 
KUDELMAN_DECLARE(void) kuda_uuid_format(char *buffer, const kuda_uuid_t *uuid);

/**
 * Parse a standard-format string into a UUID
 * @param uuid The resulting UUID
 * @param uuid_str The formatted UUID
 */ 
KUDELMAN_DECLARE(kuda_status_t) kuda_uuid_parse(kuda_uuid_t *uuid, const char *uuid_str);

/** @} */
#ifdef __cplusplus
}
#endif

#endif /* KUDA_UUID_H */
