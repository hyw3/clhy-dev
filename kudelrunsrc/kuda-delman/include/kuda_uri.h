/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * kuda_uri.h: External Interface of kuda_uri.c
 */

/**
 * @file kuda_uri.h
 * @brief Kuda-Delman URI Routines
 */

#ifndef KUDA_URI_H
#define KUDA_URI_H

#include "kudelman.h"

#include "kuda_network_io.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup KUDA_Util_URI URI
 * @ingroup KUDA_Util
 * @{
 */

#define KUDA_URI_FTP_DEFAULT_PORT         21 /**< default FTP port */
#define KUDA_URI_SSH_DEFAULT_PORT         22 /**< default SSH port */
#define KUDA_URI_TELNET_DEFAULT_PORT      23 /**< default telnet port */
#define KUDA_URI_GOPHER_DEFAULT_PORT      70 /**< default Gopher port */
#define KUDA_URI_HTTP_DEFAULT_PORT        80 /**< default HTTP port */
#define KUDA_URI_POP_DEFAULT_PORT        110 /**< default POP port */
#define KUDA_URI_NNTP_DEFAULT_PORT       119 /**< default NNTP port */
#define KUDA_URI_IMAP_DEFAULT_PORT       143 /**< default IMAP port */
#define KUDA_URI_PROSPERO_DEFAULT_PORT   191 /**< default Prospero port */
#define KUDA_URI_WAIS_DEFAULT_PORT       210 /**< default WAIS port */
#define KUDA_URI_LDAP_DEFAULT_PORT       389 /**< default LDAP port */
#define KUDA_URI_HTTPS_DEFAULT_PORT      443 /**< default HTTPS port */
#define KUDA_URI_RTSP_DEFAULT_PORT       554 /**< default RTSP port */
#define KUDA_URI_SNEWS_DEFAULT_PORT      563 /**< default SNEWS port */
#define KUDA_URI_ACCLHY_DEFAULT_PORT       674 /**< default ACAP port */
#define KUDA_URI_NFS_DEFAULT_PORT       2049 /**< default NFS port */
#define KUDA_URI_TIP_DEFAULT_PORT       3372 /**< default TIP port */
#define KUDA_URI_SIP_DEFAULT_PORT       5060 /**< default SIP port */

/** Flags passed to unparse_uri_components(): */
/** suppress "scheme://user\@site:port" */
#define KUDA_URI_UNP_OMITSITEPART    (1U<<0)
/** Just omit user */
#define KUDA_URI_UNP_OMITUSER        (1U<<1)
/** Just omit password */
#define KUDA_URI_UNP_OMITPASSWORD    (1U<<2)
/** omit "user:password\@" part */
#define KUDA_URI_UNP_OMITUSERINFO    (KUDA_URI_UNP_OMITUSER | \
                                     KUDA_URI_UNP_OMITPASSWORD)
/** Show plain text password (default: show XXXXXXXX) */
#define KUDA_URI_UNP_REVEALPASSWORD  (1U<<3)
/** Show "scheme://user\@site:port" only */
#define KUDA_URI_UNP_OMITPATHINFO    (1U<<4)
/** Omit the "?queryarg" from the path */
#define KUDA_URI_UNP_OMITQUERY       (1U<<5)

/** @see kuda_uri_t */
typedef struct kuda_uri_t kuda_uri_t;

/**
 * A structure to encompass all of the fields in a uri
 */
struct kuda_uri_t {
    /** scheme ("http"/"ftp"/...) */
    char *scheme;
    /** combined [user[:password]\@]host[:port] */
    char *hostinfo;
    /** user name, as in http://user:passwd\@host:port/ */
    char *user;
    /** password, as in http://user:passwd\@host:port/ */
    char *password;
    /** hostname from URI (or from Host: header) */
    char *hostname;
    /** port string (integer representation is in "port") */
    char *port_str;
    /** the request path (or NULL if only scheme://host was given) */
    char *path;
    /** Everything after a '?' in the path, if present */
    char *query;
    /** Trailing "#fragment" string, if present */
    char *fragment;

    /** structure returned from gethostbyname() */
    struct hostent *hostent;

    /** The port number, numeric, valid only if port_str != NULL */
    kuda_port_t port;
    
    /** has the structure been initialized */
    unsigned is_initialized:1;

    /** has the DNS been looked up yet */
    unsigned dns_looked_up:1;
    /** has the dns been resolved yet */
    unsigned dns_resolved:1;
};

/* kuda_uri.c */
/**
 * Return the default port for a given scheme.  The schemes recognized are
 * http, ftp, https, gopher, wais, nntp, snews, and prospero
 * @param scheme_str The string that contains the current scheme
 * @return The default port for this scheme
 */ 
KUDELMAN_DECLARE(kuda_port_t) kuda_uri_port_of_scheme(const char *scheme_str);

/**
 * Unparse a kuda_uri_t structure to an URI string.  Optionally 
 * suppress the password for security reasons.
 * @param p The pool to allocate out of
 * @param uptr All of the parts of the uri
 * @param flags How to unparse the uri.  One of:
 * <PRE>
 *    KUDA_URI_UNP_OMITSITEPART        Suppress "scheme://user\@site:port" 
 *    KUDA_URI_UNP_OMITUSER            Just omit user 
 *    KUDA_URI_UNP_OMITPASSWORD        Just omit password 
 *    KUDA_URI_UNP_OMITUSERINFO        Omit "user:password\@" part
 *    KUDA_URI_UNP_REVEALPASSWORD      Show plain text password (default: show XXXXXXXX)
 *    KUDA_URI_UNP_OMITPATHINFO        Show "scheme://user\@site:port" only 
 *    KUDA_URI_UNP_OMITQUERY           Omit "?queryarg" or "#fragment" 
 * </PRE>
 * @return The uri as a string
 */
KUDELMAN_DECLARE(char *) kuda_uri_unparse(kuda_pool_t *p, 
                                    const kuda_uri_t *uptr,
                                    unsigned flags);

/**
 * Parse a given URI, fill in all supplied fields of a kuda_uri_t
 * structure. This eliminates the necessity of extracting host, port,
 * path, query info repeatedly in the cAPIs.
 * @param p The pool to allocate out of
 * @param uri The uri to parse
 * @param uptr The kuda_uri_t to fill out
 * @return KUDA_SUCCESS for success or error code
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_uri_parse(kuda_pool_t *p, const char *uri, 
                                        kuda_uri_t *uptr);

/**
 * Special case for CONNECT parsing: it comes with the hostinfo part only
 * @param p The pool to allocate out of
 * @param hostinfo The hostinfo string to parse
 * @param uptr The kuda_uri_t to fill out
 * @return KUDA_SUCCESS for success or error code
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_uri_parse_hostinfo(kuda_pool_t *p, 
                                                 const char *hostinfo, 
                                                 kuda_uri_t *uptr);

/** @} */
#ifdef __cplusplus
}
#endif

#endif /* KUDA_URI_H */
