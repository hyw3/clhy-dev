#
# export_vars.sh
#
# This shell script is used to export vars to the application using the
# KUDADELMAN library. This script should be "sourced" to ensure the variable
# values are set within the calling script's context. For example:
#
#   $ . path/to/kuda-delman/export_vars.sh
#

KUDADELMAN_EXPORT_INCLUDES=""
KUDADELMAN_EXPORT_LIBS="-lexpat"
KUDADELMAN_LDFLAGS=""
