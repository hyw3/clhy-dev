/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_general.h" /* for KUDA_STRINGIFY */

#include "kudelman.h"
#include "kudelman_version.h"

KUDELMAN_DECLARE(void) kudelman_version(kuda_version_t *pvsn)
{
    pvsn->major = KUDELMAN_MAJOR_VERSION;
    pvsn->minor = KUDELMAN_MINOR_VERSION;
    pvsn->patch = KUDELMAN_PATCH_VERSION;
#ifdef KUDELMAN_IS_DEV_VERSION
    pvsn->is_dev = 1;
#else
    pvsn->is_dev = 0;
#endif
}

KUDELMAN_DECLARE(const char *) kudelman_version_string(void)
{
    return KUDELMAN_VERSION_STRING;
}
