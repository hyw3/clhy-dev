/*
 * The cLHy Server
 * 
 * Copyright (C) The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use or modify this file
 * except in compliance with the License.  You may obtain a copy of
 * the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */

#include <assert.h>
#include "kuda_thread_pool.h"
#include "kuda_ring.h"
#include "kuda_thread_cond.h"
#include "kuda_portable.h"

#if KUDA_HAS_THREADS

#define TASK_PRIORITY_SEGS 4
#define TASK_PRIORITY_SEG(x) (((x)->dispatch.priority & 0xFF) / 64)

typedef struct kuda_thread_pool_task
{
    KUDA_RING_ENTRY(kuda_thread_pool_task) link;
    kuda_thread_start_t func;
    void *param;
    void *owner;
    union
    {
        kuda_byte_t priority;
        kuda_time_t time;
    } dispatch;
} kuda_thread_pool_task_t;

KUDA_RING_HEAD(kuda_thread_pool_tasks, kuda_thread_pool_task);

struct kuda_thread_list_elt
{
    KUDA_RING_ENTRY(kuda_thread_list_elt) link;
    kuda_thread_t *thd;
    volatile void *current_owner;
    volatile enum { TH_RUN, TH_STOP, TH_PROBATION } state;
};

KUDA_RING_HEAD(kuda_thread_list, kuda_thread_list_elt);

struct kuda_thread_pool
{
    kuda_pool_t *pool;
    volatile kuda_size_t thd_max;
    volatile kuda_size_t idle_max;
    volatile kuda_interval_time_t idle_wait;
    volatile kuda_size_t thd_cnt;
    volatile kuda_size_t idle_cnt;
    volatile kuda_size_t task_cnt;
    volatile kuda_size_t scheduled_task_cnt;
    volatile kuda_size_t threshold;
    volatile kuda_size_t tasks_run;
    volatile kuda_size_t tasks_high;
    volatile kuda_size_t thd_high;
    volatile kuda_size_t thd_timed_out;
    struct kuda_thread_pool_tasks *tasks;
    struct kuda_thread_pool_tasks *scheduled_tasks;
    struct kuda_thread_list *busy_thds;
    struct kuda_thread_list *idle_thds;
    kuda_thread_mutex_t *lock;
    kuda_thread_cond_t *cond;
    volatile int terminated;
    struct kuda_thread_pool_tasks *recycled_tasks;
    struct kuda_thread_list *recycled_thds;
    kuda_thread_pool_task_t *task_idx[TASK_PRIORITY_SEGS];
};

static kuda_status_t thread_pool_construct(kuda_thread_pool_t * me,
                                          kuda_size_t init_threads,
                                          kuda_size_t max_threads)
{
    kuda_status_t rv;
    int i;

    me->thd_max = max_threads;
    me->idle_max = init_threads;
    me->threshold = init_threads / 2;
    rv = kuda_thread_mutex_create(&me->lock, KUDA_THREAD_MUTEX_NESTED,
                                 me->pool);
    if (KUDA_SUCCESS != rv) {
        return rv;
    }
    rv = kuda_thread_cond_create(&me->cond, me->pool);
    if (KUDA_SUCCESS != rv) {
        kuda_thread_mutex_destroy(me->lock);
        return rv;
    }
    me->tasks = kuda_palloc(me->pool, sizeof(*me->tasks));
    if (!me->tasks) {
        goto CATCH_ENOMEM;
    }
    KUDA_RING_INIT(me->tasks, kuda_thread_pool_task, link);
    me->scheduled_tasks = kuda_palloc(me->pool, sizeof(*me->scheduled_tasks));
    if (!me->scheduled_tasks) {
        goto CATCH_ENOMEM;
    }
    KUDA_RING_INIT(me->scheduled_tasks, kuda_thread_pool_task, link);
    me->recycled_tasks = kuda_palloc(me->pool, sizeof(*me->recycled_tasks));
    if (!me->recycled_tasks) {
        goto CATCH_ENOMEM;
    }
    KUDA_RING_INIT(me->recycled_tasks, kuda_thread_pool_task, link);
    me->busy_thds = kuda_palloc(me->pool, sizeof(*me->busy_thds));
    if (!me->busy_thds) {
        goto CATCH_ENOMEM;
    }
    KUDA_RING_INIT(me->busy_thds, kuda_thread_list_elt, link);
    me->idle_thds = kuda_palloc(me->pool, sizeof(*me->idle_thds));
    if (!me->idle_thds) {
        goto CATCH_ENOMEM;
    }
    KUDA_RING_INIT(me->idle_thds, kuda_thread_list_elt, link);
    me->recycled_thds = kuda_palloc(me->pool, sizeof(*me->recycled_thds));
    if (!me->recycled_thds) {
        goto CATCH_ENOMEM;
    }
    KUDA_RING_INIT(me->recycled_thds, kuda_thread_list_elt, link);
    me->thd_cnt = me->idle_cnt = me->task_cnt = me->scheduled_task_cnt = 0;
    me->tasks_run = me->tasks_high = me->thd_high = me->thd_timed_out = 0;
    me->idle_wait = 0;
    me->terminated = 0;
    for (i = 0; i < TASK_PRIORITY_SEGS; i++) {
        me->task_idx[i] = NULL;
    }
    goto FINAL_EXIT;
  CATCH_ENOMEM:
    rv = KUDA_ENOMEM;
    kuda_thread_mutex_destroy(me->lock);
    kuda_thread_cond_destroy(me->cond);
  FINAL_EXIT:
    return rv;
}

/*
 * NOTE: This function is not thread safe by itself. Caller should hold the lock
 */
static kuda_thread_pool_task_t *pop_task(kuda_thread_pool_t * me)
{
    kuda_thread_pool_task_t *task = NULL;
    int seg;

    /* check for scheduled tasks */
    if (me->scheduled_task_cnt > 0) {
        task = KUDA_RING_FIRST(me->scheduled_tasks);
        assert(task != NULL);
        assert(task !=
               KUDA_RING_SENTINEL(me->scheduled_tasks, kuda_thread_pool_task,
                                 link));
        /* if it's time */
        if (task->dispatch.time <= kuda_time_now()) {
            --me->scheduled_task_cnt;
            KUDA_RING_REMOVE(task, link);
            return task;
        }
    }
    /* check for normal tasks if we're not returning a scheduled task */
    if (me->task_cnt == 0) {
        return NULL;
    }

    task = KUDA_RING_FIRST(me->tasks);
    assert(task != NULL);
    assert(task != KUDA_RING_SENTINEL(me->tasks, kuda_thread_pool_task, link));
    --me->task_cnt;
    seg = TASK_PRIORITY_SEG(task);
    if (task == me->task_idx[seg]) {
        me->task_idx[seg] = KUDA_RING_NEXT(task, link);
        if (me->task_idx[seg] == KUDA_RING_SENTINEL(me->tasks,
                                                   kuda_thread_pool_task, link)
            || TASK_PRIORITY_SEG(me->task_idx[seg]) != seg) {
            me->task_idx[seg] = NULL;
        }
    }
    KUDA_RING_REMOVE(task, link);
    return task;
}

static kuda_interval_time_t waiting_time(kuda_thread_pool_t * me)
{
    kuda_thread_pool_task_t *task = NULL;

    task = KUDA_RING_FIRST(me->scheduled_tasks);
    assert(task != NULL);
    assert(task !=
           KUDA_RING_SENTINEL(me->scheduled_tasks, kuda_thread_pool_task,
                             link));
    return task->dispatch.time - kuda_time_now();
}

/*
 * NOTE: This function is not thread safe by itself. Caller should hold the lock
 */
static struct kuda_thread_list_elt *elt_new(kuda_thread_pool_t * me,
                                           kuda_thread_t * t)
{
    struct kuda_thread_list_elt *elt;

    if (KUDA_RING_EMPTY(me->recycled_thds, kuda_thread_list_elt, link)) {
        elt = kuda_pcalloc(me->pool, sizeof(*elt));
        if (NULL == elt) {
            return NULL;
        }
    }
    else {
        elt = KUDA_RING_FIRST(me->recycled_thds);
        KUDA_RING_REMOVE(elt, link);
    }

    KUDA_RING_ELEM_INIT(elt, link);
    elt->thd = t;
    elt->current_owner = NULL;
    elt->state = TH_RUN;
    return elt;
}

/*
 * The worker thread function. Take a task from the queue and perform it if
 * there is any. Otherwise, put itself into the idle thread list and waiting
 * for signal to wake up.
 * The thread terminate directly by detach and exit when it is asked to stop
 * after finishing a task. Otherwise, the thread should be in idle thread list
 * and should be joined.
 */
static void *KUDA_THREAD_FUNC thread_pool_func(kuda_thread_t * t, void *param)
{
    kuda_thread_pool_t *me = param;
    kuda_thread_pool_task_t *task = NULL;
    kuda_interval_time_t wait;
    struct kuda_thread_list_elt *elt;

    kuda_thread_mutex_lock(me->lock);
    elt = elt_new(me, t);
    if (!elt) {
        kuda_thread_mutex_unlock(me->lock);
        kuda_thread_exit(t, KUDA_ENOMEM);
    }

    while (!me->terminated && elt->state != TH_STOP) {
        /* Test if not new element, it is awakened from idle */
        if (KUDA_RING_NEXT(elt, link) != elt) {
            --me->idle_cnt;
            KUDA_RING_REMOVE(elt, link);
        }

        KUDA_RING_INSERT_TAIL(me->busy_thds, elt, kuda_thread_list_elt, link);
        task = pop_task(me);
        while (NULL != task && !me->terminated) {
            ++me->tasks_run;
            elt->current_owner = task->owner;
            kuda_thread_mutex_unlock(me->lock);
            kuda_thread_data_set(task, "kuda_thread_pool_task", NULL, t);
            task->func(t, task->param);
            kuda_thread_mutex_lock(me->lock);
            KUDA_RING_INSERT_TAIL(me->recycled_tasks, task,
                                 kuda_thread_pool_task, link);
            elt->current_owner = NULL;
            if (TH_STOP == elt->state) {
                break;
            }
            task = pop_task(me);
        }
        assert(NULL == elt->current_owner);
        if (TH_STOP != elt->state)
            KUDA_RING_REMOVE(elt, link);

        /* Test if a busy thread been asked to stop, which is not joinable */
        if ((me->idle_cnt >= me->idle_max
             && !(me->scheduled_task_cnt && 0 >= me->idle_max)
             && !me->idle_wait)
            || me->terminated || elt->state != TH_RUN) {
            --me->thd_cnt;
            if ((TH_PROBATION == elt->state) && me->idle_wait)
                ++me->thd_timed_out;
            KUDA_RING_INSERT_TAIL(me->recycled_thds, elt,
                                 kuda_thread_list_elt, link);
            kuda_thread_mutex_unlock(me->lock);
            kuda_thread_detach(t);
            kuda_thread_exit(t, KUDA_SUCCESS);
            return NULL;        /* should not be here, safe net */
        }

        /* busy thread become idle */
        ++me->idle_cnt;
        KUDA_RING_INSERT_TAIL(me->idle_thds, elt, kuda_thread_list_elt, link);

        /* 
         * If there is a scheduled task, always scheduled to perform that task.
         * Since there is no guarantee that current idle threads are scheduled
         * for next scheduled task.
         */
        if (me->scheduled_task_cnt)
            wait = waiting_time(me);
        else if (me->idle_cnt > me->idle_max) {
            wait = me->idle_wait;
            elt->state = TH_PROBATION;
        }
        else
            wait = -1;

        if (wait >= 0) {
            kuda_thread_cond_timedwait(me->cond, me->lock, wait);
        }
        else {
            kuda_thread_cond_wait(me->cond, me->lock);
        }
    }

    /* idle thread been asked to stop, will be joined */
    --me->thd_cnt;
    kuda_thread_mutex_unlock(me->lock);
    kuda_thread_exit(t, KUDA_SUCCESS);
    return NULL;                /* should not be here, safe net */
}

static kuda_status_t thread_pool_cleanup(void *me)
{
    kuda_thread_pool_t *_myself = me;

    _myself->terminated = 1;
    kuda_thread_pool_idle_max_set(_myself, 0);
    while (_myself->thd_cnt) {
        kuda_sleep(20 * 1000);   /* spin lock with 20 ms */
    }
    kuda_thread_mutex_destroy(_myself->lock);
    kuda_thread_cond_destroy(_myself->cond);
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_thread_pool_create(kuda_thread_pool_t ** me,
                                                 kuda_size_t init_threads,
                                                 kuda_size_t max_threads,
                                                 kuda_pool_t * pool)
{
    kuda_thread_t *t;
    kuda_status_t rv = KUDA_SUCCESS;
    kuda_thread_pool_t *tp;

    *me = NULL;
    tp = kuda_pcalloc(pool, sizeof(kuda_thread_pool_t));

    /*
     * This pool will be used by different threads. As we cannot ensure that
     * our caller won't use the pool without acquiring the mutex, we must
     * create a new sub pool.
     */
    rv = kuda_pool_create(&tp->pool, pool);
    if (KUDA_SUCCESS != rv)
        return rv;
    rv = thread_pool_construct(tp, init_threads, max_threads);
    if (KUDA_SUCCESS != rv)
        return rv;
    kuda_pool_pre_cleanup_register(tp->pool, tp, thread_pool_cleanup);

    while (init_threads) {
        /* Grab the mutex as kuda_thread_create() and thread_pool_func() will 
         * allocate from (*me)->pool. This is dangerous if there are multiple 
         * initial threads to create.
         */
        kuda_thread_mutex_lock(tp->lock);
        rv = kuda_thread_create(&t, NULL, thread_pool_func, tp, tp->pool);
        kuda_thread_mutex_unlock(tp->lock);
        if (KUDA_SUCCESS != rv) {
            break;
        }
        tp->thd_cnt++;
        if (tp->thd_cnt > tp->thd_high) {
            tp->thd_high = tp->thd_cnt;
        }
        --init_threads;
    }

    if (rv == KUDA_SUCCESS) {
        *me = tp;
    }

    return rv;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_thread_pool_destroy(kuda_thread_pool_t * me)
{
    kuda_pool_destroy(me->pool);
    return KUDA_SUCCESS;
}

/*
 * NOTE: This function is not thread safe by itself. Caller should hold the lock
 */
static kuda_thread_pool_task_t *task_new(kuda_thread_pool_t * me,
                                        kuda_thread_start_t func,
                                        void *param, kuda_byte_t priority,
                                        void *owner, kuda_time_t time)
{
    kuda_thread_pool_task_t *t;

    if (KUDA_RING_EMPTY(me->recycled_tasks, kuda_thread_pool_task, link)) {
        t = kuda_pcalloc(me->pool, sizeof(*t));
        if (NULL == t) {
            return NULL;
        }
    }
    else {
        t = KUDA_RING_FIRST(me->recycled_tasks);
        KUDA_RING_REMOVE(t, link);
    }

    KUDA_RING_ELEM_INIT(t, link);
    t->func = func;
    t->param = param;
    t->owner = owner;
    if (time > 0) {
        t->dispatch.time = kuda_time_now() + time;
    }
    else {
        t->dispatch.priority = priority;
    }
    return t;
}

/*
 * Test it the task is the only one within the priority segment. 
 * If it is not, return the first element with same or lower priority. 
 * Otherwise, add the task into the queue and return NULL.
 *
 * NOTE: This function is not thread safe by itself. Caller should hold the lock
 */
static kuda_thread_pool_task_t *add_if_empty(kuda_thread_pool_t * me,
                                            kuda_thread_pool_task_t * const t)
{
    int seg;
    int next;
    kuda_thread_pool_task_t *t_next;

    seg = TASK_PRIORITY_SEG(t);
    if (me->task_idx[seg]) {
        assert(KUDA_RING_SENTINEL(me->tasks, kuda_thread_pool_task, link) !=
               me->task_idx[seg]);
        t_next = me->task_idx[seg];
        while (t_next->dispatch.priority > t->dispatch.priority) {
            t_next = KUDA_RING_NEXT(t_next, link);
            if (KUDA_RING_SENTINEL(me->tasks, kuda_thread_pool_task, link) ==
                t_next) {
                return t_next;
            }
        }
        return t_next;
    }

    for (next = seg - 1; next >= 0; next--) {
        if (me->task_idx[next]) {
            KUDA_RING_INSERT_BEFORE(me->task_idx[next], t, link);
            break;
        }
    }
    if (0 > next) {
        KUDA_RING_INSERT_TAIL(me->tasks, t, kuda_thread_pool_task, link);
    }
    me->task_idx[seg] = t;
    return NULL;
}

/*
*   schedule a task to run in "time" microseconds. Find the spot in the ring where
*   the time fits. Adjust the short_time so the thread wakes up when the time is reached.
*/
static kuda_status_t schedule_task(kuda_thread_pool_t *me,
                                  kuda_thread_start_t func, void *param,
                                  void *owner, kuda_interval_time_t time)
{
    kuda_thread_pool_task_t *t;
    kuda_thread_pool_task_t *t_loc;
    kuda_thread_t *thd;
    kuda_status_t rv = KUDA_SUCCESS;
    kuda_thread_mutex_lock(me->lock);

    t = task_new(me, func, param, 0, owner, time);
    if (NULL == t) {
        kuda_thread_mutex_unlock(me->lock);
        return KUDA_ENOMEM;
    }
    t_loc = KUDA_RING_FIRST(me->scheduled_tasks);
    while (NULL != t_loc) {
        /* if the time is less than the entry insert ahead of it */
        if (t->dispatch.time < t_loc->dispatch.time) {
            ++me->scheduled_task_cnt;
            KUDA_RING_INSERT_BEFORE(t_loc, t, link);
            break;
        }
        else {
            t_loc = KUDA_RING_NEXT(t_loc, link);
            if (t_loc ==
                KUDA_RING_SENTINEL(me->scheduled_tasks, kuda_thread_pool_task,
                                  link)) {
                ++me->scheduled_task_cnt;
                KUDA_RING_INSERT_TAIL(me->scheduled_tasks, t,
                                     kuda_thread_pool_task, link);
                break;
            }
        }
    }
    /* there should be at least one thread for scheduled tasks */
    if (0 == me->thd_cnt) {
        rv = kuda_thread_create(&thd, NULL, thread_pool_func, me, me->pool);
        if (KUDA_SUCCESS == rv) {
            ++me->thd_cnt;
            if (me->thd_cnt > me->thd_high)
                me->thd_high = me->thd_cnt;
        }
    }
    kuda_thread_cond_signal(me->cond);
    kuda_thread_mutex_unlock(me->lock);
    return rv;
}

static kuda_status_t add_task(kuda_thread_pool_t *me, kuda_thread_start_t func,
                             void *param, kuda_byte_t priority, int push,
                             void *owner)
{
    kuda_thread_pool_task_t *t;
    kuda_thread_pool_task_t *t_loc;
    kuda_thread_t *thd;
    kuda_status_t rv = KUDA_SUCCESS;

    kuda_thread_mutex_lock(me->lock);

    t = task_new(me, func, param, priority, owner, 0);
    if (NULL == t) {
        kuda_thread_mutex_unlock(me->lock);
        return KUDA_ENOMEM;
    }

    t_loc = add_if_empty(me, t);
    if (NULL == t_loc) {
        goto FINAL_EXIT;
    }

    if (push) {
        while (KUDA_RING_SENTINEL(me->tasks, kuda_thread_pool_task, link) !=
               t_loc && t_loc->dispatch.priority >= t->dispatch.priority) {
            t_loc = KUDA_RING_NEXT(t_loc, link);
        }
    }
    KUDA_RING_INSERT_BEFORE(t_loc, t, link);
    if (!push) {
        if (t_loc == me->task_idx[TASK_PRIORITY_SEG(t)]) {
            me->task_idx[TASK_PRIORITY_SEG(t)] = t;
        }
    }

  FINAL_EXIT:
    me->task_cnt++;
    if (me->task_cnt > me->tasks_high)
        me->tasks_high = me->task_cnt;
    if (0 == me->thd_cnt || (0 == me->idle_cnt && me->thd_cnt < me->thd_max &&
                             me->task_cnt > me->threshold)) {
        rv = kuda_thread_create(&thd, NULL, thread_pool_func, me, me->pool);
        if (KUDA_SUCCESS == rv) {
            ++me->thd_cnt;
            if (me->thd_cnt > me->thd_high)
                me->thd_high = me->thd_cnt;
        }
    }

    kuda_thread_cond_signal(me->cond);
    kuda_thread_mutex_unlock(me->lock);

    return rv;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_thread_pool_push(kuda_thread_pool_t *me,
                                               kuda_thread_start_t func,
                                               void *param,
                                               kuda_byte_t priority,
                                               void *owner)
{
    return add_task(me, func, param, priority, 1, owner);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_thread_pool_schedule(kuda_thread_pool_t *me,
                                                   kuda_thread_start_t func,
                                                   void *param,
                                                   kuda_interval_time_t time,
                                                   void *owner)
{
    return schedule_task(me, func, param, owner, time);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_thread_pool_top(kuda_thread_pool_t *me,
                                              kuda_thread_start_t func,
                                              void *param,
                                              kuda_byte_t priority,
                                              void *owner)
{
    return add_task(me, func, param, priority, 0, owner);
}

static kuda_status_t remove_scheduled_tasks(kuda_thread_pool_t *me,
                                           void *owner)
{
    kuda_thread_pool_task_t *t_loc;
    kuda_thread_pool_task_t *next;

    t_loc = KUDA_RING_FIRST(me->scheduled_tasks);
    while (t_loc !=
           KUDA_RING_SENTINEL(me->scheduled_tasks, kuda_thread_pool_task,
                             link)) {
        next = KUDA_RING_NEXT(t_loc, link);
        /* if this is the owner remove it */
        if (t_loc->owner == owner) {
            --me->scheduled_task_cnt;
            KUDA_RING_REMOVE(t_loc, link);
        }
        t_loc = next;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t remove_tasks(kuda_thread_pool_t *me, void *owner)
{
    kuda_thread_pool_task_t *t_loc;
    kuda_thread_pool_task_t *next;
    int seg;

    t_loc = KUDA_RING_FIRST(me->tasks);
    while (t_loc != KUDA_RING_SENTINEL(me->tasks, kuda_thread_pool_task, link)) {
        next = KUDA_RING_NEXT(t_loc, link);
        if (t_loc->owner == owner) {
            --me->task_cnt;
            seg = TASK_PRIORITY_SEG(t_loc);
            if (t_loc == me->task_idx[seg]) {
                me->task_idx[seg] = KUDA_RING_NEXT(t_loc, link);
                if (me->task_idx[seg] == KUDA_RING_SENTINEL(me->tasks,
                                                           kuda_thread_pool_task,
                                                           link)
                    || TASK_PRIORITY_SEG(me->task_idx[seg]) != seg) {
                    me->task_idx[seg] = NULL;
                }
            }
            KUDA_RING_REMOVE(t_loc, link);
        }
        t_loc = next;
    }
    return KUDA_SUCCESS;
}

static void wait_on_busy_threads(kuda_thread_pool_t *me, void *owner)
{
#ifndef NDEBUG
    kuda_platform_thread_t *platform_thread;
#endif
    struct kuda_thread_list_elt *elt;
    kuda_thread_mutex_lock(me->lock);
    elt = KUDA_RING_FIRST(me->busy_thds);
    while (elt != KUDA_RING_SENTINEL(me->busy_thds, kuda_thread_list_elt, link)) {
        if (elt->current_owner != owner) {
            elt = KUDA_RING_NEXT(elt, link);
            continue;
        }
#ifndef NDEBUG
        /* make sure the thread is not the one calling tasks_cancel */
        kuda_platform_thread_get(&platform_thread, elt->thd);
#ifdef WIN32
        /* hack for kuda win32 bug */
        assert(!kuda_platform_thread_equal(kuda_platform_thread_current(), platform_thread));
#else
        assert(!kuda_platform_thread_equal(kuda_platform_thread_current(), *platform_thread));
#endif
#endif
        while (elt->current_owner == owner) {
            kuda_thread_mutex_unlock(me->lock);
            kuda_sleep(200 * 1000);
            kuda_thread_mutex_lock(me->lock);
        }
        elt = KUDA_RING_FIRST(me->busy_thds);
    }
    kuda_thread_mutex_unlock(me->lock);
    return;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_thread_pool_tasks_cancel(kuda_thread_pool_t *me,
                                                       void *owner)
{
    kuda_status_t rv = KUDA_SUCCESS;

    kuda_thread_mutex_lock(me->lock);
    if (me->task_cnt > 0) {
        rv = remove_tasks(me, owner);
    }
    if (me->scheduled_task_cnt > 0) {
        rv = remove_scheduled_tasks(me, owner);
    }
    kuda_thread_mutex_unlock(me->lock);
    wait_on_busy_threads(me, owner);

    return rv;
}

KUDELMAN_DECLARE(kuda_size_t) kuda_thread_pool_tasks_count(kuda_thread_pool_t *me)
{
    return me->task_cnt;
}

KUDELMAN_DECLARE(kuda_size_t)
    kuda_thread_pool_scheduled_tasks_count(kuda_thread_pool_t *me)
{
    return me->scheduled_task_cnt;
}

KUDELMAN_DECLARE(kuda_size_t) kuda_thread_pool_threads_count(kuda_thread_pool_t *me)
{
    return me->thd_cnt;
}

KUDELMAN_DECLARE(kuda_size_t) kuda_thread_pool_busy_count(kuda_thread_pool_t *me)
{
    return me->thd_cnt - me->idle_cnt;
}

KUDELMAN_DECLARE(kuda_size_t) kuda_thread_pool_idle_count(kuda_thread_pool_t *me)
{
    return me->idle_cnt;
}

KUDELMAN_DECLARE(kuda_size_t)
    kuda_thread_pool_tasks_run_count(kuda_thread_pool_t * me)
{
    return me->tasks_run;
}

KUDELMAN_DECLARE(kuda_size_t)
    kuda_thread_pool_tasks_high_count(kuda_thread_pool_t * me)
{
    return me->tasks_high;
}

KUDELMAN_DECLARE(kuda_size_t)
    kuda_thread_pool_threads_high_count(kuda_thread_pool_t * me)
{
    return me->thd_high;
}

KUDELMAN_DECLARE(kuda_size_t)
    kuda_thread_pool_threads_idle_timeout_count(kuda_thread_pool_t * me)
{
    return me->thd_timed_out;
}


KUDELMAN_DECLARE(kuda_size_t) kuda_thread_pool_idle_max_get(kuda_thread_pool_t *me)
{
    return me->idle_max;
}

KUDELMAN_DECLARE(kuda_interval_time_t)
    kuda_thread_pool_idle_wait_get(kuda_thread_pool_t * me)
{
    return me->idle_wait;
}

/*
 * This function stop extra idle threads to the cnt.
 * @return the number of threads stopped
 * NOTE: There could be busy threads become idle during this function
 */
static struct kuda_thread_list_elt *trim_threads(kuda_thread_pool_t *me,
                                                kuda_size_t *cnt, int idle)
{
    struct kuda_thread_list *thds;
    kuda_size_t n, n_dbg, i;
    struct kuda_thread_list_elt *head, *tail, *elt;

    kuda_thread_mutex_lock(me->lock);
    if (idle) {
        thds = me->idle_thds;
        n = me->idle_cnt;
    }
    else {
        thds = me->busy_thds;
        n = me->thd_cnt - me->idle_cnt;
    }
    if (n <= *cnt) {
        kuda_thread_mutex_unlock(me->lock);
        *cnt = 0;
        return NULL;
    }
    n -= *cnt;

    head = KUDA_RING_FIRST(thds);
    for (i = 0; i < *cnt; i++) {
        head = KUDA_RING_NEXT(head, link);
    }
    tail = KUDA_RING_LAST(thds);
    if (idle) {
        KUDA_RING_UNSPLICE(head, tail, link);
        me->idle_cnt = *cnt;
    }

    n_dbg = 0;
    for (elt = head; elt != tail; elt = KUDA_RING_NEXT(elt, link)) {
        elt->state = TH_STOP;
        n_dbg++;
    }
    elt->state = TH_STOP;
    n_dbg++;
    assert(n == n_dbg);
    *cnt = n;

    kuda_thread_mutex_unlock(me->lock);

    KUDA_RING_PREV(head, link) = NULL;
    KUDA_RING_NEXT(tail, link) = NULL;
    return head;
}

static kuda_size_t trim_idle_threads(kuda_thread_pool_t *me, kuda_size_t cnt)
{
    kuda_size_t n_dbg;
    struct kuda_thread_list_elt *elt, *head, *tail;
    kuda_status_t rv;

    elt = trim_threads(me, &cnt, 1);

    kuda_thread_mutex_lock(me->lock);
    kuda_thread_cond_broadcast(me->cond);
    kuda_thread_mutex_unlock(me->lock);

    n_dbg = 0;
    if (NULL != (head = elt)) {
        while (elt) {
            tail = elt;
            kuda_thread_join(&rv, elt->thd);
            elt = KUDA_RING_NEXT(elt, link);
            ++n_dbg;
        }
        kuda_thread_mutex_lock(me->lock);
        KUDA_RING_SPLICE_TAIL(me->recycled_thds, head, tail,
                             kuda_thread_list_elt, link);
        kuda_thread_mutex_unlock(me->lock);
    }
    assert(cnt == n_dbg);

    return cnt;
}

/* don't join on busy threads for performance reasons, who knows how long will
 * the task takes to perform
 */
static kuda_size_t trim_busy_threads(kuda_thread_pool_t *me, kuda_size_t cnt)
{
    trim_threads(me, &cnt, 0);
    return cnt;
}

KUDELMAN_DECLARE(kuda_size_t) kuda_thread_pool_idle_max_set(kuda_thread_pool_t *me,
                                                     kuda_size_t cnt)
{
    me->idle_max = cnt;
    cnt = trim_idle_threads(me, cnt);
    return cnt;
}

KUDELMAN_DECLARE(kuda_interval_time_t)
    kuda_thread_pool_idle_wait_set(kuda_thread_pool_t * me,
                                  kuda_interval_time_t timeout)
{
    kuda_interval_time_t oldtime;

    oldtime = me->idle_wait;
    me->idle_wait = timeout;

    return oldtime;
}

KUDELMAN_DECLARE(kuda_size_t) kuda_thread_pool_thread_max_get(kuda_thread_pool_t *me)
{
    return me->thd_max;
}

/*
 * This function stop extra working threads to the new limit.
 * NOTE: There could be busy threads become idle during this function
 */
KUDELMAN_DECLARE(kuda_size_t) kuda_thread_pool_thread_max_set(kuda_thread_pool_t *me,
                                                       kuda_size_t cnt)
{
    unsigned int n;

    me->thd_max = cnt;
    if (0 == cnt || me->thd_cnt <= cnt) {
        return 0;
    }

    n = me->thd_cnt - cnt;
    if (n >= me->idle_cnt) {
        trim_busy_threads(me, n - me->idle_cnt);
        trim_idle_threads(me, 0);
    }
    else {
        trim_idle_threads(me, me->idle_cnt - n);
    }
    return n;
}

KUDELMAN_DECLARE(kuda_size_t) kuda_thread_pool_threshold_get(kuda_thread_pool_t *me)
{
    return me->threshold;
}

KUDELMAN_DECLARE(kuda_size_t) kuda_thread_pool_threshold_set(kuda_thread_pool_t *me,
                                                      kuda_size_t val)
{
    kuda_size_t ov;

    ov = me->threshold;
    me->threshold = val;
    return ov;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_thread_pool_task_owner_get(kuda_thread_t *thd,
                                                         void **owner)
{
    kuda_status_t rv;
    kuda_thread_pool_task_t *task;
    void *data;

    rv = kuda_thread_data_get(&data, "kuda_thread_pool_task", thd);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    task = data;
    if (!task) {
        *owner = NULL;
        return KUDA_BADARG;
    }

    *owner = task->owner;
    return KUDA_SUCCESS;
}

#endif /* KUDA_HAS_THREADS */

/* vim: set ts=4 sw=4 et cin tw=80: */
