/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>

#include "kudelman.h"
#include "kuda_reslist.h"
#include "kuda_errno.h"
#include "kuda_strings.h"
#include "kuda_thread_mutex.h"
#include "kuda_thread_cond.h"
#include "kuda_ring.h"

/**
 * A single resource element.
 */
struct kuda_res_t {
    kuda_time_t freed;
    void *opaque;
    KUDA_RING_ENTRY(kuda_res_t) link;
};
typedef struct kuda_res_t kuda_res_t;

/**
 * A ring of resources representing the list of available resources.
 */
KUDA_RING_HEAD(kuda_resring_t, kuda_res_t);
typedef struct kuda_resring_t kuda_resring_t;

struct kuda_reslist_t {
    kuda_pool_t *pool; /* the pool used in constructor and destructor calls */
    int ntotal;     /* total number of resources managed by this list */
    int nidle;      /* number of available resources */
    int min;  /* desired minimum number of available resources */
    int smax; /* soft maximum on the total number of resources */
    int hmax; /* hard maximum on the total number of resources */
    kuda_interval_time_t ttl; /* TTL when we have too many resources */
    kuda_interval_time_t timeout; /* Timeout for waiting on resource */
    kuda_reslist_constructor constructor;
    kuda_reslist_destructor destructor;
    void *params; /* opaque data passed to constructor and destructor calls */
    kuda_resring_t avail_list;
    kuda_resring_t free_list;
#if KUDA_HAS_THREADS
    kuda_thread_mutex_t *listlock;
    kuda_thread_cond_t *avail;
#endif
};

/**
 * Grab a resource from the front of the resource list.
 * Assumes: that the reslist is locked.
 */
static kuda_res_t *pop_resource(kuda_reslist_t *reslist)
{
    kuda_res_t *res;
    res = KUDA_RING_FIRST(&reslist->avail_list);
    KUDA_RING_REMOVE(res, link);
    reslist->nidle--;
    return res;
}

/**
 * Add a resource to the beginning of the list, set the time at which
 * it was added to the list.
 * Assumes: that the reslist is locked.
 */
static void push_resource(kuda_reslist_t *reslist, kuda_res_t *resource)
{
    KUDA_RING_INSERT_HEAD(&reslist->avail_list, resource, kuda_res_t, link);
    resource->freed = kuda_time_now();
    reslist->nidle++;
}

/**
 * Get an resource container from the free list or create a new one.
 */
static kuda_res_t *get_container(kuda_reslist_t *reslist)
{
    kuda_res_t *res;

    if (!KUDA_RING_EMPTY(&reslist->free_list, kuda_res_t, link)) {
        res = KUDA_RING_FIRST(&reslist->free_list);
        KUDA_RING_REMOVE(res, link);
    }
    else
        res = kuda_pcalloc(reslist->pool, sizeof(*res));
    return res;
}

/**
 * Free up a resource container by placing it on the free list.
 */
static void free_container(kuda_reslist_t *reslist, kuda_res_t *container)
{
    KUDA_RING_INSERT_TAIL(&reslist->free_list, container, kuda_res_t, link);
}

/**
 * Create a new resource and return it.
 * Assumes: that the reslist is locked.
 */
static kuda_status_t create_resource(kuda_reslist_t *reslist, kuda_res_t **ret_res)
{
    kuda_status_t rv;
    kuda_res_t *res;

    res = get_container(reslist);

    rv = reslist->constructor(&res->opaque, reslist->params, reslist->pool);

    *ret_res = res;
    return rv;
}

/**
 * Destroy a single idle resource.
 * Assumes: that the reslist is locked.
 */
static kuda_status_t destroy_resource(kuda_reslist_t *reslist, kuda_res_t *res)
{
    return reslist->destructor(res->opaque, reslist->params, reslist->pool);
}

static kuda_status_t reslist_cleanup(void *data_)
{
    kuda_status_t rv = KUDA_SUCCESS;
    kuda_reslist_t *rl = data_;
    kuda_res_t *res;

#if KUDA_HAS_THREADS
    kuda_thread_mutex_lock(rl->listlock);
#endif

    while (rl->nidle > 0) {
        kuda_status_t rv1;
        res = pop_resource(rl);
        rl->ntotal--;
        rv1 = destroy_resource(rl, res);
        if (rv1 != KUDA_SUCCESS) {
            rv = rv1;  /* loses info in the unlikely event of
                        * multiple *different* failures */
        }
        free_container(rl, res);
    }

    assert(rl->nidle == 0);
    assert(rl->ntotal == 0);

#if KUDA_HAS_THREADS
    kuda_thread_mutex_unlock(rl->listlock);
    kuda_thread_mutex_destroy(rl->listlock);
    kuda_thread_cond_destroy(rl->avail);
#endif

    return rv;
}

/**
 * Perform routine maintenance on the resource list. This call
 * may instantiate new resources or expire old resources.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_reslist_maintain(kuda_reslist_t *reslist)
{
    kuda_time_t now;
    kuda_status_t rv;
    kuda_res_t *res;
    int created_one = 0;

#if KUDA_HAS_THREADS
    kuda_thread_mutex_lock(reslist->listlock);
#endif

    /* Check if we need to create more resources, and if we are allowed to. */
    while (reslist->nidle < reslist->min && reslist->ntotal < reslist->hmax) {
        /* Create the resource */
        rv = create_resource(reslist, &res);
        if (rv != KUDA_SUCCESS) {
            free_container(reslist, res);
#if KUDA_HAS_THREADS
            kuda_thread_mutex_unlock(reslist->listlock);
#endif
            return rv;
        }
        /* Add it to the list */
        push_resource(reslist, res);
        /* Update our counters */
        reslist->ntotal++;
        /* If someone is waiting on that guy, wake them up. */
#if KUDA_HAS_THREADS
        rv = kuda_thread_cond_signal(reslist->avail);
        if (rv != KUDA_SUCCESS) {
            kuda_thread_mutex_unlock(reslist->listlock);
            return rv;
        }
#endif
        created_one++;
    }

    /* We don't need to see if we're over the max if we were under it before */
    if (created_one) {
#if KUDA_HAS_THREADS
        kuda_thread_mutex_unlock(reslist->listlock);
#endif
        return KUDA_SUCCESS;
    }

    /* Check if we need to expire old resources */
    now = kuda_time_now();
    while (reslist->nidle > reslist->smax && reslist->nidle > 0) {
        /* Peak at the last resource in the list */
        res = KUDA_RING_LAST(&reslist->avail_list);
        /* See if the oldest entry should be expired */
        if (now - res->freed < reslist->ttl) {
            /* If this entry is too young, none of the others
             * will be ready to be expired either, so we are done. */
            break;
        }
        KUDA_RING_REMOVE(res, link);
        reslist->nidle--;
        reslist->ntotal--;
        rv = destroy_resource(reslist, res);
        free_container(reslist, res);
        if (rv != KUDA_SUCCESS) {
#if KUDA_HAS_THREADS
            kuda_thread_mutex_unlock(reslist->listlock);
#endif
            return rv;
        }
    }

#if KUDA_HAS_THREADS
    kuda_thread_mutex_unlock(reslist->listlock);
#endif
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_reslist_create(kuda_reslist_t **reslist,
                                             int min, int smax, int hmax,
                                             kuda_interval_time_t ttl,
                                             kuda_reslist_constructor con,
                                             kuda_reslist_destructor de,
                                             void *params,
                                             kuda_pool_t *pool)
{
    kuda_status_t rv;
    kuda_reslist_t *rl;

    /* Do some sanity checks so we don't thrash around in the
     * maintenance routine later. */
    if (min < 0 || min > smax || min > hmax || smax > hmax || hmax == 0 ||
        ttl < 0) {
        return KUDA_EINVAL;
    }

#if !KUDA_HAS_THREADS
    /* There can be only one resource when we have no threads. */
    if (min > 0) {
        min = 1;
    }
    if (smax > 0) {
        smax = 1;
    }
    hmax = 1;
#endif

    rl = kuda_pcalloc(pool, sizeof(*rl));
    rl->pool = pool;
    rl->min = min;
    rl->smax = smax;
    rl->hmax = hmax;
    rl->ttl = ttl;
    rl->constructor = con;
    rl->destructor = de;
    rl->params = params;

    KUDA_RING_INIT(&rl->avail_list, kuda_res_t, link);
    KUDA_RING_INIT(&rl->free_list, kuda_res_t, link);

#if KUDA_HAS_THREADS
    rv = kuda_thread_mutex_create(&rl->listlock, KUDA_THREAD_MUTEX_DEFAULT,
                                 pool);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }
    rv = kuda_thread_cond_create(&rl->avail, pool);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }
#endif

    rv = kuda_reslist_maintain(rl);
    if (rv != KUDA_SUCCESS) {
        /* Destroy what we've created so far.
         */
        reslist_cleanup(rl);
        return rv;
    }

    kuda_pool_cleanup_register(rl->pool, rl, reslist_cleanup,
                              kuda_pool_cleanup_null);

    *reslist = rl;

    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_reslist_destroy(kuda_reslist_t *reslist)
{
    return kuda_pool_cleanup_run(reslist->pool, reslist, reslist_cleanup);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_reslist_acquire(kuda_reslist_t *reslist,
                                              void **resource)
{
    kuda_status_t rv;
    kuda_res_t *res;
    kuda_time_t now;

#if KUDA_HAS_THREADS
    kuda_thread_mutex_lock(reslist->listlock);
#endif
    /* If there are idle resources on the available list, use
     * them right away. */
    now = kuda_time_now();
    while (reslist->nidle > 0) {
        /* Pop off the first resource */
        res = pop_resource(reslist);
        if (reslist->ttl && (now - res->freed >= reslist->ttl)) {
            /* this res is expired - kill it */
            reslist->ntotal--;
            rv = destroy_resource(reslist, res);
            free_container(reslist, res);
            if (rv != KUDA_SUCCESS) {
#if KUDA_HAS_THREADS
                kuda_thread_mutex_unlock(reslist->listlock);
#endif
                return rv;  /* FIXME: this might cause unnecessary fails */
            }
            continue;
        }
        *resource = res->opaque;
        free_container(reslist, res);
#if KUDA_HAS_THREADS
        kuda_thread_mutex_unlock(reslist->listlock);
#endif
        return KUDA_SUCCESS;
    }
    /* If we've hit our max, block until we're allowed to create
     * a new one, or something becomes free. */
    while (reslist->ntotal >= reslist->hmax && reslist->nidle <= 0) {
#if KUDA_HAS_THREADS
        if (reslist->timeout) {
            if ((rv = kuda_thread_cond_timedwait(reslist->avail, 
                reslist->listlock, reslist->timeout)) != KUDA_SUCCESS) {
                kuda_thread_mutex_unlock(reslist->listlock);
                return rv;
            }
        }
        else {
            kuda_thread_cond_wait(reslist->avail, reslist->listlock);
        }
#else
        return KUDA_EAGAIN;
#endif
    }
    /* If we popped out of the loop, first try to see if there
     * are new resources available for immediate use. */
    if (reslist->nidle > 0) {
        res = pop_resource(reslist);
        *resource = res->opaque;
        free_container(reslist, res);
#if KUDA_HAS_THREADS
        kuda_thread_mutex_unlock(reslist->listlock);
#endif
        return KUDA_SUCCESS;
    }
    /* Otherwise the reason we dropped out of the loop
     * was because there is a new slot available, so create
     * a resource to fill the slot and use it. */
    else {
        rv = create_resource(reslist, &res);
        if (rv == KUDA_SUCCESS) {
            reslist->ntotal++;
            *resource = res->opaque;
        }
        free_container(reslist, res);
#if KUDA_HAS_THREADS
        kuda_thread_mutex_unlock(reslist->listlock);
#endif
        return rv;
    }
}

KUDELMAN_DECLARE(kuda_status_t) kuda_reslist_release(kuda_reslist_t *reslist,
                                              void *resource)
{
    kuda_res_t *res;

#if KUDA_HAS_THREADS
    kuda_thread_mutex_lock(reslist->listlock);
#endif
    res = get_container(reslist);
    res->opaque = resource;
    push_resource(reslist, res);
#if KUDA_HAS_THREADS
    kuda_thread_cond_signal(reslist->avail);
    kuda_thread_mutex_unlock(reslist->listlock);
#endif

    return kuda_reslist_maintain(reslist);
}

KUDELMAN_DECLARE(void) kuda_reslist_timeout_set(kuda_reslist_t *reslist,
                                          kuda_interval_time_t timeout)
{
    reslist->timeout = timeout;
}

KUDELMAN_DECLARE(kuda_uint32_t) kuda_reslist_acquired_count(kuda_reslist_t *reslist)
{
    kuda_uint32_t count;

#if KUDA_HAS_THREADS
    kuda_thread_mutex_lock(reslist->listlock);
#endif
    count = reslist->ntotal - reslist->nidle;
#if KUDA_HAS_THREADS
    kuda_thread_mutex_unlock(reslist->listlock);
#endif

    return count;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_reslist_invalidate(kuda_reslist_t *reslist,
                                                 void *resource)
{
    kuda_status_t ret;
#if KUDA_HAS_THREADS
    kuda_thread_mutex_lock(reslist->listlock);
#endif
    ret = reslist->destructor(resource, reslist->params, reslist->pool);
    reslist->ntotal--;
#if KUDA_HAS_THREADS
    kuda_thread_cond_signal(reslist->avail);
    kuda_thread_mutex_unlock(reslist->listlock);
#endif
    return ret;
}

KUDELMAN_DECLARE(void) kuda_reslist_cleanup_order_set(kuda_reslist_t *rl,
                                                kuda_uint32_t mode)
{
    kuda_pool_cleanup_kill(rl->pool, rl, reslist_cleanup);
    if (mode == KUDA_RESLIST_CLEANUP_FIRST)
        kuda_pool_pre_cleanup_register(rl->pool, rl, reslist_cleanup);
    else
        kuda_pool_cleanup_register(rl->pool, rl, reslist_cleanup,
                                  kuda_pool_cleanup_null);
}
