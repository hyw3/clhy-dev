/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ctype.h>
#include <stdio.h>

#include "kudelman_config.h"
#include "kudelman.h"

#include "kuda_pools.h"
#include "kuda_tables.h"
#include "kuda_dso.h"
#include "kuda_strings.h"
#include "kuda_hash.h"
#include "kuda_file_io.h"
#include "kuda_env.h"
#include "kuda_atomic.h"

#include "kudelman_internal.h"
#include "kudelman_version.h"

#if KUDELMAN_DSO_BUILD

#if KUDA_HAS_THREADS
static kuda_thread_mutex_t* mutex = NULL;
#endif
static kuda_hash_t *dsos = NULL;
static kuda_uint32_t initialised = 0, in_init = 1;

#if KUDA_HAS_THREADS
kuda_status_t kudelman_dso_mutex_lock()
{
    return kuda_thread_mutex_lock(mutex);
}
kuda_status_t kudelman_dso_mutex_unlock()
{
    return kuda_thread_mutex_unlock(mutex);
}
#else
kuda_status_t kudelman_dso_mutex_lock() {
    return KUDA_SUCCESS;
}
kuda_status_t kudelman_dso_mutex_unlock() {
    return KUDA_SUCCESS;
}
#endif

static kuda_status_t kudelman_dso_term(void *ptr)
{
    /* set statics to NULL so init can work again */
    dsos = NULL;
#if KUDA_HAS_THREADS
    mutex = NULL;
#endif

    /* Everything else we need is handled by cleanups registered
     * when we created mutexes and loaded DSOs
     */
    return KUDA_SUCCESS;
}

kuda_status_t kudelman_dso_init(kuda_pool_t *pool)
{
    kuda_status_t ret = KUDA_SUCCESS;
    kuda_pool_t *parent;

    if (kuda_atomic_inc32(&initialised)) {
        kuda_atomic_set32(&initialised, 1); /* prevent wrap-around */

        while (kuda_atomic_read32(&in_init)) /* wait until we get fully inited */
            ;

        return KUDA_SUCCESS;
    }

    /* Top level pool scope, need process-scope lifetime */
    for (parent = kuda_pool_parent_get(pool);
         parent && parent != pool;
         parent = kuda_pool_parent_get(pool))
        pool = parent;

    dsos = kuda_hash_make(pool);

#if KUDA_HAS_THREADS
    ret = kuda_thread_mutex_create(&mutex, KUDA_THREAD_MUTEX_DEFAULT, pool);
    /* This already registers a pool cleanup */
#endif

    kuda_pool_cleanup_register(pool, NULL, kudelman_dso_term,
                              kuda_pool_cleanup_null);

    kuda_atomic_dec32(&in_init);

    return ret;
}

kuda_status_t kudelman_dso_load(kuda_dso_handle_t **dlhandleptr,
                          kuda_dso_handle_sym_t *dsoptr,
                          const char *cAPI,
                          const char *capisym,
                          kuda_pool_t *pool)
{
    kuda_dso_handle_t *dlhandle = NULL;
    char *pathlist;
    char path[KUDA_PATH_MAX + 1];
    kuda_array_header_t *paths;
    kuda_pool_t *global;
    kuda_status_t rv = KUDA_EDSOOPEN;
    char *eos = NULL;
    int i;

    *dsoptr = kuda_hash_get(dsos, cAPI, KUDA_HASH_KEY_STRING);
    if (*dsoptr) {
        return KUDA_EINIT;
    }

    /* The driver DSO must have exactly the same lifetime as the
     * drivers hash table; ignore the passed-in pool */
    global = kuda_hash_pool_get(dsos);

    /* Retrieve our path search list or prepare for a single search */
    if ((kuda_env_get(&pathlist, KUDA_DSOPATH, pool) != KUDA_SUCCESS)
          || (kuda_filepath_list_split(&paths, pathlist, pool) != KUDA_SUCCESS))
        paths = kuda_array_make(pool, 1, sizeof(char*));

#if defined(KUDELMAN_DSO_LIBDIR)
    /* Always search our prefix path, but on some platforms such as
     * win32 this may be left undefined
     */
    (*((char **)kuda_array_push(paths))) = KUDELMAN_DSO_LIBDIR;
#endif

    for (i = 0; i < paths->nelts; ++i)
    {
#if defined(WIN32)
        /* Use win32 dso search semantics and attempt to
         * load the relative lib on the first pass.
         */
        if (!eos) {
            eos = path;
            --i;
        }
        else
#endif
        {
            eos = kuda_cpystrn(path, ((char**)paths->elts)[i], sizeof(path));
            if ((eos > path) && (eos - path < sizeof(path) - 1))
                *(eos++) = '/';
        }
        kuda_cpystrn(eos, cAPI, sizeof(path) - (eos - path));

        rv = kuda_dso_load(&dlhandle, path, global);
        if (dlhandleptr) {
            *dlhandleptr = dlhandle;
        }
        if (rv == KUDA_SUCCESS) { /* KUDA_EDSOOPEN */
            break;
        }
#if defined(KUDELMAN_DSO_LIBDIR)
        else if (i < paths->nelts - 1) {
#else
        else {   /* No KUDELMAN_DSO_LIBDIR to skip */
#endif
             /* try with kuda-delman-KUDELMAN_MAJOR_VERSION appended */
            eos = kuda_cpystrn(eos,
                              "kuda-delman-" KUDELMAN_STRINGIFY(KUDELMAN_MAJOR_VERSION) "/",
                              sizeof(path) - (eos - path));

            kuda_cpystrn(eos, cAPI, sizeof(path) - (eos - path));

            rv = kuda_dso_load(&dlhandle, path, global);
            if (dlhandleptr) {
                *dlhandleptr = dlhandle;
            }
            if (rv == KUDA_SUCCESS) { /* KUDA_EDSOOPEN */
                break;
            }
        }
    }

    if (rv != KUDA_SUCCESS) /* KUDA_ESYMNOTFOUND */
        return rv;

    rv = kuda_dso_sym(dsoptr, dlhandle, capisym);
    if (rv != KUDA_SUCCESS) { /* KUDA_ESYMNOTFOUND */
        kuda_dso_unload(dlhandle);
    }
    else {
        cAPI = kuda_pstrdup(global, cAPI);
        kuda_hash_set(dsos, cAPI, KUDA_HASH_KEY_STRING, *dsoptr);
    }
    return rv;
}

#endif /* KUDELMAN_DSO_BUILD */

