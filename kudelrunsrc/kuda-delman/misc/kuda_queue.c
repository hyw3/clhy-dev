/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"

#if KUDA_HAVE_STDIO_H
#include <stdio.h>
#endif
#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "kudelman.h"
#include "kuda_portable.h"
#include "kuda_thread_mutex.h"
#include "kuda_thread_cond.h"
#include "kuda_errno.h"
#include "kuda_queue.h"

#if KUDA_HAS_THREADS
/* 
 * define this to get debug messages
 *
#define QUEUE_DEBUG
 */

struct kuda_queue_t {
    void              **data;
    unsigned int        nelts; /**< # elements */
    unsigned int        in;    /**< next empty location */
    unsigned int        out;   /**< next filled location */
    unsigned int        bounds;/**< max size of queue */
    unsigned int        full_waiters;
    unsigned int        empty_waiters;
    kuda_thread_mutex_t *one_big_mutex;
    kuda_thread_cond_t  *not_empty;
    kuda_thread_cond_t  *not_full;
    int                 terminated;
};

#ifdef QUEUE_DEBUG
static void Q_DBG(char*msg, kuda_queue_t *q) {
    fprintf(stderr, "%ld\t#%d in %d out %d\t%s\n", 
                    kuda_platform_thread_current(),
                    q->nelts, q->in, q->out,
                    msg
                    );
}
#else
#define Q_DBG(x,y) 
#endif

/**
 * Detects when the kuda_queue_t is full. This utility function is expected
 * to be called from within critical sections, and is not threadsafe.
 */
#define kuda_queue_full(queue) ((queue)->nelts == (queue)->bounds)

/**
 * Detects when the kuda_queue_t is empty. This utility function is expected
 * to be called from within critical sections, and is not threadsafe.
 */
#define kuda_queue_empty(queue) ((queue)->nelts == 0)

/**
 * Callback routine that is called to destroy this
 * kuda_queue_t when its pool is destroyed.
 */
static kuda_status_t queue_destroy(void *data) 
{
    kuda_queue_t *queue = data;

    /* Ignore errors here, we can't do anything about them anyway. */

    kuda_thread_cond_destroy(queue->not_empty);
    kuda_thread_cond_destroy(queue->not_full);
    kuda_thread_mutex_destroy(queue->one_big_mutex);

    return KUDA_SUCCESS;
}

/**
 * Initialize the kuda_queue_t.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_queue_create(kuda_queue_t **q, 
                                           unsigned int queue_capacity, 
                                           kuda_pool_t *a)
{
    kuda_status_t rv;
    kuda_queue_t *queue;
    queue = kuda_palloc(a, sizeof(kuda_queue_t));
    *q = queue;

    /* nested doesn't work ;( */
    rv = kuda_thread_mutex_create(&queue->one_big_mutex,
                                 KUDA_THREAD_MUTEX_UNNESTED,
                                 a);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    rv = kuda_thread_cond_create(&queue->not_empty, a);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    rv = kuda_thread_cond_create(&queue->not_full, a);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    /* Set all the data in the queue to NULL */
    queue->data = kuda_pcalloc(a, queue_capacity * sizeof(void*));
    queue->bounds = queue_capacity;
    queue->nelts = 0;
    queue->in = 0;
    queue->out = 0;
    queue->terminated = 0;
    queue->full_waiters = 0;
    queue->empty_waiters = 0;

    kuda_pool_cleanup_register(a, queue, queue_destroy, kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}

/**
 * Push new data onto the queue. Blocks if the queue is full. Once
 * the push operation has completed, it signals other threads waiting
 * in kuda_queue_pop() that they may continue consuming sockets.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_queue_push(kuda_queue_t *queue, void *data)
{
    kuda_status_t rv;

    if (queue->terminated) {
        return KUDA_EOF; /* no more elements ever again */
    }

    rv = kuda_thread_mutex_lock(queue->one_big_mutex);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    if (kuda_queue_full(queue)) {
        if (!queue->terminated) {
            queue->full_waiters++;
            rv = kuda_thread_cond_wait(queue->not_full, queue->one_big_mutex);
            queue->full_waiters--;
            if (rv != KUDA_SUCCESS) {
                kuda_thread_mutex_unlock(queue->one_big_mutex);
                return rv;
            }
        }
        /* If we wake up and it's still empty, then we were interrupted */
        if (kuda_queue_full(queue)) {
            Q_DBG("queue full (intr)", queue);
            rv = kuda_thread_mutex_unlock(queue->one_big_mutex);
            if (rv != KUDA_SUCCESS) {
                return rv;
            }
            if (queue->terminated) {
                return KUDA_EOF; /* no more elements ever again */
            }
            else {
                return KUDA_EINTR;
            }
        }
    }

    queue->data[queue->in] = data;
    queue->in++;
    if (queue->in >= queue->bounds)
        queue->in -= queue->bounds;
    queue->nelts++;

    if (queue->empty_waiters) {
        Q_DBG("sig !empty", queue);
        rv = kuda_thread_cond_signal(queue->not_empty);
        if (rv != KUDA_SUCCESS) {
            kuda_thread_mutex_unlock(queue->one_big_mutex);
            return rv;
        }
    }

    rv = kuda_thread_mutex_unlock(queue->one_big_mutex);
    return rv;
}

/**
 * Push new data onto the queue. If the queue is full, return KUDA_EAGAIN. If
 * the push operation completes successfully, it signals other threads
 * waiting in kuda_queue_pop() that they may continue consuming sockets.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_queue_trypush(kuda_queue_t *queue, void *data)
{
    kuda_status_t rv;

    if (queue->terminated) {
        return KUDA_EOF; /* no more elements ever again */
    }

    rv = kuda_thread_mutex_lock(queue->one_big_mutex);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    if (kuda_queue_full(queue)) {
        rv = kuda_thread_mutex_unlock(queue->one_big_mutex);
        return KUDA_EAGAIN;
    }
    
    queue->data[queue->in] = data;
    queue->in++;
    if (queue->in >= queue->bounds)
        queue->in -= queue->bounds;
    queue->nelts++;

    if (queue->empty_waiters) {
        Q_DBG("sig !empty", queue);
        rv  = kuda_thread_cond_signal(queue->not_empty);
        if (rv != KUDA_SUCCESS) {
            kuda_thread_mutex_unlock(queue->one_big_mutex);
            return rv;
        }
    }

    rv = kuda_thread_mutex_unlock(queue->one_big_mutex);
    return rv;
}

/**
 * not thread safe
 */
KUDELMAN_DECLARE(unsigned int) kuda_queue_size(kuda_queue_t *queue) {
    return queue->nelts;
}

/**
 * Retrieves the next item from the queue. If there are no
 * items available, it will block until one becomes available.
 * Once retrieved, the item is placed into the address specified by
 * 'data'.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_queue_pop(kuda_queue_t *queue, void **data)
{
    kuda_status_t rv;

    if (queue->terminated) {
        return KUDA_EOF; /* no more elements ever again */
    }

    rv = kuda_thread_mutex_lock(queue->one_big_mutex);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    /* Keep waiting until we wake up and find that the queue is not empty. */
    if (kuda_queue_empty(queue)) {
        if (!queue->terminated) {
            queue->empty_waiters++;
            rv = kuda_thread_cond_wait(queue->not_empty, queue->one_big_mutex);
            queue->empty_waiters--;
            if (rv != KUDA_SUCCESS) {
                kuda_thread_mutex_unlock(queue->one_big_mutex);
                return rv;
            }
        }
        /* If we wake up and it's still empty, then we were interrupted */
        if (kuda_queue_empty(queue)) {
            Q_DBG("queue empty (intr)", queue);
            rv = kuda_thread_mutex_unlock(queue->one_big_mutex);
            if (rv != KUDA_SUCCESS) {
                return rv;
            }
            if (queue->terminated) {
                return KUDA_EOF; /* no more elements ever again */
            }
            else {
                return KUDA_EINTR;
            }
        }
    } 

    *data = queue->data[queue->out];
    queue->nelts--;

    queue->out++;
    if (queue->out >= queue->bounds)
        queue->out -= queue->bounds;
    if (queue->full_waiters) {
        Q_DBG("signal !full", queue);
        rv = kuda_thread_cond_signal(queue->not_full);
        if (rv != KUDA_SUCCESS) {
            kuda_thread_mutex_unlock(queue->one_big_mutex);
            return rv;
        }
    }

    rv = kuda_thread_mutex_unlock(queue->one_big_mutex);
    return rv;
}

/**
 * Retrieves the next item from the queue. If there are no
 * items available, return KUDA_EAGAIN.  Once retrieved,
 * the item is placed into the address specified by 'data'.
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_queue_trypop(kuda_queue_t *queue, void **data)
{
    kuda_status_t rv;

    if (queue->terminated) {
        return KUDA_EOF; /* no more elements ever again */
    }

    rv = kuda_thread_mutex_lock(queue->one_big_mutex);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    if (kuda_queue_empty(queue)) {
        rv = kuda_thread_mutex_unlock(queue->one_big_mutex);
        return KUDA_EAGAIN;
    } 

    *data = queue->data[queue->out];
    queue->nelts--;

    queue->out++;
    if (queue->out >= queue->bounds)
        queue->out -= queue->bounds;
    if (queue->full_waiters) {
        Q_DBG("signal !full", queue);
        rv = kuda_thread_cond_signal(queue->not_full);
        if (rv != KUDA_SUCCESS) {
            kuda_thread_mutex_unlock(queue->one_big_mutex);
            return rv;
        }
    }

    rv = kuda_thread_mutex_unlock(queue->one_big_mutex);
    return rv;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_queue_interrupt_all(kuda_queue_t *queue)
{
    kuda_status_t rv;
    Q_DBG("intr all", queue);    
    if ((rv = kuda_thread_mutex_lock(queue->one_big_mutex)) != KUDA_SUCCESS) {
        return rv;
    }
    kuda_thread_cond_broadcast(queue->not_empty);
    kuda_thread_cond_broadcast(queue->not_full);

    if ((rv = kuda_thread_mutex_unlock(queue->one_big_mutex)) != KUDA_SUCCESS) {
        return rv;
    }

    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_queue_term(kuda_queue_t *queue)
{
    kuda_status_t rv;

    if ((rv = kuda_thread_mutex_lock(queue->one_big_mutex)) != KUDA_SUCCESS) {
        return rv;
    }

    /* we must hold one_big_mutex when setting this... otherwise,
     * we could end up setting it and waking everybody up just after a 
     * would-be popper checks it but right before they block
     */
    queue->terminated = 1;
    if ((rv = kuda_thread_mutex_unlock(queue->one_big_mutex)) != KUDA_SUCCESS) {
        return rv;
    }
    return kuda_queue_interrupt_all(queue);
}

#endif /* KUDA_HAS_THREADS */
