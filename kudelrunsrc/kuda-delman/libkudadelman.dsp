# Microsoft Developer Studio Project File - Name="libkudadelman" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=libkudadelman - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "libkudadelman.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libkudadelman.mak" CFG="libkudadelman - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libkudadelman - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libkudadelman - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libkudadelman - x64 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libkudadelman - x64 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FD /c
# ADD CPP /nologo /MD /W3 /Zi /O2 /Oy- /I "./include" /I "../kuda/include" /I "./include/private" /I "./xml/expat/lib" /I "../kuda-iconv/include" /I "./dbm/sdbm" /D "NDEBUG" /D "KUDA_DECLARE_EXPORT" /D "KUDELMAN_DECLARE_EXPORT" /D "KUDELMAN_USE_SDBM" $(XML_OPTIONS) /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\" /Fd"$(INTDIR)\libkudadelman_src" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG" /d "KUDELMAN_VERSION_ONLY" /I "./include" /I "../kuda/include"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib /nologo /base:"0x6EE60000" /subsystem:windows /dll /incremental:no /debug /opt:ref
# ADD LINK32 $(XML_PARSER).lib kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib /nologo /base:"0x6EE60000" /subsystem:windows /dll /incremental:no /debug /out:"Release\libkudadelman-1.dll" /pdb:"Release\libkudadelman-1.pdb" /implib:"Release\libkudadelman-1.lib" /MACHINE:X86 /opt:ref
# Begin Special Build Tool
TargetPath=Release\libkudadelman-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FD /EHsc /c
# ADD CPP /nologo /MDd /W3 /Zi /Od /I "./include" /I "../kuda/include" /I "./include/private" /I "./xml/expat/lib" /I "../kuda-iconv/include" /I "./dbm/sdbm" /D "_DEBUG" /D "KUDA_DECLARE_EXPORT" /D "KUDELMAN_DECLARE_EXPORT" /D "KUDELMAN_USE_SDBM" $(XML_OPTIONS) /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\" /Fd"$(INTDIR)\libkudadelman_src" /FD /EHsc /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG" /d "KUDELMAN_VERSION_ONLY" /I "./include" /I "../kuda/include"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib /nologo /base:"0x6EE60000" /subsystem:windows /dll /incremental:no /debug
# ADD LINK32 $(XML_PARSER).lib kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib /nologo /base:"0x6EE60000" /subsystem:windows /dll /incremental:no /debug /out:"Debug\libkudadelman-1.dll" /pdb:"Debug\libkudadelman-1.pdb" /implib:"Debug\libkudadelman-1.lib" /MACHINE:X86
# Begin Special Build Tool
TargetPath=Debug\libkudadelman-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "x64\Release"
# PROP BASE Intermediate_Dir "x64\Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "x64\Release"
# PROP Intermediate_Dir "x64\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FD /c
# ADD CPP /nologo /MD /W3 /Zi /O2 /Oy- /I "./include" /I "../kuda/include" /I "./include/private" /I "./xml/expat/lib" /I "../kuda-iconv/include" /I "./dbm/sdbm" /D "NDEBUG" /D "KUDA_DECLARE_EXPORT" /D "KUDELMAN_DECLARE_EXPORT" /D "KUDELMAN_USE_SDBM" $(XML_OPTIONS) /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\" /Fd"$(INTDIR)\libkudadelman_src" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG" /d "KUDELMAN_VERSION_ONLY" /I "./include" /I "../kuda/include"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib /nologo /base:"0x6EE60000" /subsystem:windows /dll /incremental:no /debug /opt:ref
# ADD LINK32 $(XML_PARSER).lib kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib /nologo /base:"0x6EE60000" /subsystem:windows /dll /incremental:no /debug /out:"x64\Release\libkudadelman-1.dll" /pdb:"x64\Release\libkudadelman-1.pdb" /implib:"x64\Release\libkudadelman-1.lib" /MACHINE:X64 /opt:ref
# Begin Special Build Tool
TargetPath=x64\Release\libkudadelman-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "x64\Debug"
# PROP BASE Intermediate_Dir "x64\Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "x64\Debug"
# PROP Intermediate_Dir "x64\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FD /EHsc /c
# ADD CPP /nologo /MDd /W3 /Zi /Od /I "./include" /I "../kuda/include" /I "./include/private" /I "./xml/expat/lib" /I "../kuda-iconv/include" /I "./dbm/sdbm" /D "_DEBUG" /D "KUDA_DECLARE_EXPORT" /D "KUDELMAN_DECLARE_EXPORT" /D "KUDELMAN_USE_SDBM" $(XML_OPTIONS) /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\" /Fd"$(INTDIR)\libkudadelman_src" /FD /EHsc /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG" /d "KUDELMAN_VERSION_ONLY" /I "./include" /I "../kuda/include"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib /nologo /base:"0x6EE60000" /subsystem:windows /dll /incremental:no /debug
# ADD LINK32 $(XML_PARSER).lib kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib /nologo /base:"0x6EE60000" /subsystem:windows /dll /incremental:no /debug /out:"x64\Debug\libkudadelman-1.dll" /pdb:"x64\Debug\libkudadelman-1.pdb" /implib:"x64\Debug\libkudadelman-1.lib" /MACHINE:X64
# Begin Special Build Tool
TargetPath=x64\Debug\libkudadelman-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "libkudadelman - Win32 Release"
# Name "libkudadelman - Win32 Debug"
# Name "libkudadelman - x64 Release"
# Name "libkudadelman - x64 Debug"
# Begin Group "Source Files"

# PROP Default_Filter ""
# Begin Group "buckets"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\buckets\kuda_brigade.c
# End Source File
# Begin Source File

SOURCE=.\buckets\kuda_buckets.c
# End Source File
# Begin Source File

SOURCE=.\buckets\kuda_buckets_alloc.c
# End Source File
# Begin Source File

SOURCE=.\buckets\kuda_buckets_eos.c
# End Source File
# Begin Source File

SOURCE=.\buckets\kuda_buckets_file.c
# End Source File
# Begin Source File

SOURCE=.\buckets\kuda_buckets_flush.c
# End Source File
# Begin Source File

SOURCE=.\buckets\kuda_buckets_heap.c
# End Source File
# Begin Source File

SOURCE=.\buckets\kuda_buckets_mmap.c
# End Source File
# Begin Source File

SOURCE=.\buckets\kuda_buckets_pipe.c
# End Source File
# Begin Source File

SOURCE=.\buckets\kuda_buckets_pool.c
# End Source File
# Begin Source File

SOURCE=.\buckets\kuda_buckets_refcount.c
# End Source File
# Begin Source File

SOURCE=.\buckets\kuda_buckets_simple.c
# End Source File
# Begin Source File

SOURCE=.\buckets\kuda_buckets_socket.c
# End Source File
# End Group
# Begin Group "crypto"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\crypto\kuda_crypto.c
# End Source File
# Begin Source File

SOURCE=.\crypto\kuda_md4.c
# End Source File
# Begin Source File

SOURCE=.\crypto\kuda_md5.c
# End Source File
# Begin Source File

SOURCE=.\crypto\kuda_passwd.c
# End Source File
# Begin Source File

SOURCE=.\crypto\kuda_sha1.c
# End Source File
# Begin Source File

SOURCE=.\crypto\kuda_siphash.c
# End Source File
# Begin Source File

SOURCE=.\crypto\crypt_blowfish.c
# End Source File
# Begin Source File

SOURCE=.\crypto\crypt_blowfish.h
# End Source File
# Begin Source File

SOURCE=.\crypto\getuuid.c
# End Source File
# Begin Source File

SOURCE=.\crypto\uuid.c
# End Source File
# End Group
# Begin Group "dbd"
# PROP Default_Filter ""
# Begin Source File

SOURCE=.\dbd\kuda_dbd.c
# End Source File
# Begin Source File

SOURCE=.\dbd\kuda_dbd_mysql.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\dbd\kuda_dbd_odbc.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\dbd\kuda_dbd_oracle.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\dbd\kuda_dbd_pgsql.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\dbd\kuda_dbd_sqlite2.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\dbd\kuda_dbd_sqlite3.c
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Group "dbm"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\dbm\kuda_dbm.c
# End Source File
# Begin Source File

SOURCE=.\dbm\kuda_dbm_berkeleydb.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\dbm\kuda_dbm_gdbm.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\dbm\kuda_dbm_sdbm.c
# End Source File
# End Group
# Begin Group "encoding"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\encoding\kuda_base64.c
# End Source File
# End Group
# Begin Group "hooks"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\hooks\kuda_hooks.c
# End Source File
# End Group
# Begin Group "ldap"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ldap\kuda_ldap_init.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\ldap\kuda_ldap_option.c
# PROP Exclude_From_Build 1
# End Source File 
# Begin Source File

SOURCE=.\ldap\kuda_ldap_rebind.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\ldap\kuda_ldap_stub.c
# End Source File
# Begin Source File

SOURCE=.\ldap\kuda_ldap_url.c
# End Source File
# End Group
# Begin Group "memcache"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\memcache\kuda_memcache.c
# End Source File
# End Group
# Begin Group "misc"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\misc\kuda_date.c
# End Source File
# Begin Source File

SOURCE=.\misc\kudelman_dso.c
# End Source File
# Begin Source File

SOURCE=.\misc\kuda_queue.c
# End Source File
# Begin Source File

SOURCE=.\misc\kuda_reslist.c
# End Source File
# Begin Source File

SOURCE=.\misc\kuda_rmm.c
# End Source File
# Begin Source File

SOURCE=.\misc\kuda_thread_pool.c
# End Source File
# Begin Source File

SOURCE=.\misc\kudelman_version.c
# End Source File
# End Group
# Begin Group "redis"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\redis\kuda_redis.c
# End Source File
# End Group
# Begin Group "sdbm"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\dbm\sdbm\sdbm.c
# End Source File
# Begin Source File

SOURCE=.\dbm\sdbm\sdbm_hash.c
# End Source File
# Begin Source File

SOURCE=.\dbm\sdbm\sdbm_lock.c
# End Source File
# Begin Source File

SOURCE=.\dbm\sdbm\sdbm_pair.c
# End Source File
# Begin Source File

SOURCE=.\dbm\sdbm\sdbm_pair.h
# End Source File
# Begin Source File

SOURCE=.\dbm\sdbm\sdbm_private.h
# End Source File
# Begin Source File

SOURCE=.\dbm\sdbm\sdbm_tune.h
# End Source File
# End Group
# Begin Group "strmatch"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\strmatch\kuda_strmatch.c
# End Source File
# End Group
# Begin Group "uri"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\uri\kuda_uri.c
# End Source File
# End Group
# Begin Group "xlate"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\xlate\xlate.c
# End Source File
# End Group
# Begin Group "xml"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\xml\kuda_xml.c
# End Source File
# End Group
# End Group
# Begin Group "Generated Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\include\kuda_ldap.h.in
# End Source File
# Begin Source File

SOURCE=.\include\kuda_ldap.hnw
# End Source File
# Begin Source File

SOURCE=.\include\kuda_ldap.hw

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

# Begin Custom Build - Creating kuda_ldap.h from kuda_ldap.hw
InputPath=.\include\kuda_ldap.hw

".\include\kuda_ldap.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kuda_ldap.hw > .\include\kuda_ldap.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

# Begin Custom Build - Creating kuda_ldap.h from kuda_ldap.hw
InputPath=.\include\kuda_ldap.hw

".\include\kuda_ldap.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kuda_ldap.hw > .\include\kuda_ldap.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

# Begin Custom Build - Creating kuda_ldap.h from kuda_ldap.hw
InputPath=.\include\kuda_ldap.hw

".\include\kuda_ldap.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kuda_ldap.hw > .\include\kuda_ldap.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

# Begin Custom Build - Creating kuda_ldap.h from kuda_ldap.hw
InputPath=.\include\kuda_ldap.hw

".\include\kuda_ldap.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kuda_ldap.hw > .\include\kuda_ldap.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\include\kudelman.h.in
# End Source File
# Begin Source File

SOURCE=.\include\kudelman.hnw
# End Source File
# Begin Source File

SOURCE=.\include\kudelman.hw

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

# Begin Custom Build - Creating kudelman.h from kudelman.hw
InputPath=.\include\kudelman.hw

".\include\kudelman.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kudelman.hw > .\include\kudelman.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

# Begin Custom Build - Creating kudelman.h from kudelman.hw
InputPath=.\include\kudelman.hw

".\include\kudelman.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kudelman.hw > .\include\kudelman.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

# Begin Custom Build - Creating kudelman.h from kudelman.hw
InputPath=.\include\kudelman.hw

".\include\kudelman.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kudelman.hw > .\include\kudelman.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

# Begin Custom Build - Creating kudelman.h from kudelman.hw
InputPath=.\include\kudelman.hw

".\include\kudelman.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kudelman.hw > .\include\kudelman.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\include\private\kudelman_config.h.in
# End Source File
# Begin Source File

SOURCE=.\include\private\kudelman_config.hw

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

# Begin Custom Build - Creating kudelman_config.h from kudelman_config.hw
InputPath=.\include\private\kudelman_config.hw

".\include\private\kudelman_config.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\private\kudelman_config.hw > .\include\private\kudelman_config.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

# Begin Custom Build - Creating kudelman_config.h from kudelman_config.hw
InputPath=.\include\private\kudelman_config.hw

".\include\private\kudelman_config.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\private\kudelman_config.hw > .\include\private\kudelman_config.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

# Begin Custom Build - Creating kudelman_config.h from kudelman_config.hw
InputPath=.\include\private\kudelman_config.hw

".\include\private\kudelman_config.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\private\kudelman_config.hw > .\include\private\kudelman_config.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

# Begin Custom Build - Creating kudelman_config.h from kudelman_config.hw
InputPath=.\include\private\kudelman_config.hw

".\include\private\kudelman_config.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\private\kudelman_config.hw > .\include\private\kudelman_config.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\include\private\kudelman_select_dbm.h.in
# End Source File
# Begin Source File

SOURCE=.\include\private\kudelman_select_dbm.hw

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

# Begin Custom Build - Creating kudelman_select_dbm.h from kudelman_select_dbm.hw
InputPath=.\include\private\kudelman_select_dbm.hw

".\include\private\kudelman_select_dbm.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\private\kudelman_select_dbm.hw > .\include\private\kudelman_select_dbm.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

# Begin Custom Build - Creating kudelman_select_dbm.h from kudelman_select_dbm.hw
InputPath=.\include\private\kudelman_select_dbm.hw

".\include\private\kudelman_select_dbm.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\private\kudelman_select_dbm.hw > .\include\private\kudelman_select_dbm.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

# Begin Custom Build - Creating kudelman_select_dbm.h from kudelman_select_dbm.hw
InputPath=.\include\private\kudelman_select_dbm.hw

".\include\private\kudelman_select_dbm.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\private\kudelman_select_dbm.hw > .\include\private\kudelman_select_dbm.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

# Begin Custom Build - Creating kudelman_select_dbm.h from kudelman_select_dbm.hw
InputPath=.\include\private\kudelman_select_dbm.hw

".\include\private\kudelman_select_dbm.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\private\kudelman_select_dbm.hw > .\include\private\kudelman_select_dbm.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\include\kudelman_want.h.in
# End Source File
# Begin Source File

SOURCE=.\include\kudelman_want.hnw
# End Source File
# Begin Source File

SOURCE=.\include\kudelman_want.hw

!IF  "$(CFG)" == "libkudadelman - Win32 Release"

# Begin Custom Build - Creating kudelman_want.h from kudelman_want.hw
InputPath=.\include\kudelman_want.hw

".\include\kudelman_want.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kudelman_want.hw > .\include\kudelman_want.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - Win32 Debug"

# Begin Custom Build - Creating kudelman_want.h from kudelman_want.hw
InputPath=.\include\kudelman_want.hw

".\include\kudelman_want.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kudelman_want.hw > .\include\kudelman_want.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Release"

# Begin Custom Build - Creating kudelman_want.h from kudelman_want.hw
InputPath=.\include\kudelman_want.hw

".\include\kudelman_want.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kudelman_want.hw > .\include\kudelman_want.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libkudadelman - x64 Debug"

# Begin Custom Build - Creating kudelman_want.h from kudelman_want.hw
InputPath=.\include\kudelman_want.hw

".\include\kudelman_want.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\include\kudelman_want.hw > .\include\kudelman_want.h

# End Custom Build

!ENDIF 

# End Source File
# End Group
# Begin Group "Public Header Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\include\kuda_anylock.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_base64.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_buckets.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_date.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_dbm.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_hooks.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_ldap_url.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_md4.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_md5.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_memcache.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_optional.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_optional_hooks.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_queue.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_reslist.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_rmm.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_sdbm.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_sha1.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_siphash.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_strmatch.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_thread_pool.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_uri.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_uuid.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_xlate.h
# End Source File
# Begin Source File

SOURCE=.\include\kuda_xml.h
# End Source File
# Begin Source File

SOURCE=.\include\kudelman_version.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\libkudadelman.rc
# End Source File
# End Target
# End Project
