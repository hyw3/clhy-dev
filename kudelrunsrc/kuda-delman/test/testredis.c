/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testutil.h"
#include "kuda.h"
#include "kudelman.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_hash.h"
#include "kuda_redis.h"
#include "kuda_network_io.h"

#include <stdio.h>
#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>		/* for exit() */
#endif

#define HOST "localhost"
#define PORT 6379

/* the total number of items to use for set/get testing */
#define TDATA_SIZE 3000

/* some smaller subset of TDATA_SIZE used for multiget testing */
#define TDATA_SET 100

/* our custom hash function just returns this all the time */
#define HASH_FUNC_RESULT 510

/* all keys will be prefixed with this */
static const char prefix[] = "testredis";

/* text for values we store */
static const char txt[] =
"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Duis at"
"lacus in ligula hendrerit consectetuer. Vestibulum tristique odio"
"iaculis leo. In massa arcu, ultricies a, laoreet nec, hendrerit non,"
"neque. Nulla sagittis sapien ac risus. Morbi ligula dolor, vestibulum"
"nec, viverra id, placerat dapibus, arcu. Curabitur egestas feugiat"
"tellus. Donec dignissim. Nunc ante. Curabitur id lorem. In mollis"
"tortor sit amet eros auctor dapibus. Proin nulla sem, tristique in,"
"convallis id, iaculis feugiat cras amet.";

/*
 * this datatype is for our custom server determination function. this might
 * be useful if you don't want to rely on simply hashing keys to determine
 * where a key belongs, but instead want to write something fancy, or use some
 * other kind of configuration data, i.e. a hash plus some data about a 
 * namespace, or whatever. see my_server_func, and test_redis_user_funcs
 * for the examples.
 */
typedef struct {
  const char *someval;
  kuda_uint32_t which_server;
} my_hash_server_baton;


/* this could do something fancy and return some hash result. 
 * for simplicity, just return the same value, so we can test it later on.
 * if you wanted to use some external hashing library or functions for
 * consistent hashing, for example, this would be a good place to do it.
 */
static kuda_uint32_t my_hash_func(void *baton, const char *data,
                                 kuda_size_t data_len)
{

  return HASH_FUNC_RESULT;
}

/*
 * a fancy function to determine which server to use given some kind of data
 * and a hash value. this example actually ignores the hash value itself
 * and pulls some number from the *baton, which is a struct that has some 
 * kind of meaningful stuff in it.
 */
static kuda_redis_server_t *my_server_func(void *baton,
                                             kuda_redis_t *mc,
                                             const kuda_uint32_t hash)
{
  kuda_redis_server_t *ms = NULL;
  my_hash_server_baton *mhsb = (my_hash_server_baton *)baton;

  if(mc->ntotal == 0) {
    return NULL;
  } 

  if(mc->ntotal < mhsb->which_server) {
    return NULL;
  }

  ms = mc->live_servers[mhsb->which_server - 1];

  return ms;
}

static kuda_uint16_t firsttime = 0;
static int randval(kuda_uint32_t high)
{
    kuda_uint32_t i = 0;
    double d = 0;

    if (firsttime == 0) {
	srand((unsigned) (getpid()));
	firsttime = 1;
    }

    d = (double) rand() / ((double) RAND_MAX + 1);
    i = (int) (d * (high - 0 + 1));

    return i > 0 ? i : 1;
}

/*
 * general test to make sure we can create the redis struct and add
 * some servers, but not more than we tell it we can add
 */

static void test_redis_create(abts_case * tc, void *data)
{
  kuda_pool_t *pool = p;
  kuda_status_t rv;
  kuda_redis_t *redis;
  kuda_redis_server_t *server, *s;
  kuda_uint32_t max_servers = 10;
  kuda_uint32_t i;
  kuda_uint32_t hash;

  rv = kuda_redis_create(pool, max_servers, 0, &redis);
  ABTS_ASSERT(tc, "redis create failed", rv == KUDA_SUCCESS);
  
  for (i = 1; i <= max_servers; i++) {
    kuda_port_t port;
    
    port = PORT + i;
    rv =
      kuda_redis_server_create(pool, HOST, PORT + i, 0, 1, 1, 60, 60, &server);
    ABTS_ASSERT(tc, "server create failed", rv == KUDA_SUCCESS);
    
    rv = kuda_redis_add_server(redis, server);
    ABTS_ASSERT(tc, "server add failed", rv == KUDA_SUCCESS);
    
    s = kuda_redis_find_server(redis, HOST, port);
    ABTS_PTR_EQUAL(tc, server, s);
    
    rv = kuda_redis_disable_server(redis, s);
    ABTS_ASSERT(tc, "server disable failed", rv == KUDA_SUCCESS);
    
    rv = kuda_redis_enable_server(redis, s);
    ABTS_ASSERT(tc, "server enable failed", rv == KUDA_SUCCESS);
    
    hash = kuda_redis_hash(redis, prefix, strlen(prefix));
    ABTS_ASSERT(tc, "hash failed", hash > 0);
    
    s = kuda_redis_find_server_hash(redis, hash);
    ABTS_PTR_NOTNULL(tc, s);
  }

  rv = kuda_redis_server_create(pool, HOST, PORT, 0, 1, 1, 60, 60, &server);
  ABTS_ASSERT(tc, "server create failed", rv == KUDA_SUCCESS);
  
  rv = kuda_redis_add_server(redis, server);
  ABTS_ASSERT(tc, "server add should have failed", rv != KUDA_SUCCESS);
  
}

/* install our own custom hashing and server selection routines. */

static int create_test_hash(kuda_pool_t *p, kuda_hash_t *h)
{
  int i;
  
  for (i = 0; i < TDATA_SIZE; i++) {
    char *k, *v;
    
    k = kuda_pstrcat(p, prefix, kuda_itoa(p, i), NULL);
    v = kuda_pstrndup(p, txt, randval((kuda_uint32_t)strlen(txt)));
    
    kuda_hash_set(h, k, KUDA_HASH_KEY_STRING, v);
  }

  return i;
}

static void test_redis_user_funcs(abts_case * tc, void *data)
{
  kuda_pool_t *pool = p;
  kuda_status_t rv;
  kuda_redis_t *redis;
  kuda_redis_server_t *found;
  kuda_uint32_t max_servers = 10;
  kuda_uint32_t hres;
  kuda_uint32_t i;
  my_hash_server_baton *baton = 
    kuda_pcalloc(pool, sizeof(my_hash_server_baton));

  rv = kuda_redis_create(pool, max_servers, 0, &redis);
  ABTS_ASSERT(tc, "redis create failed", rv == KUDA_SUCCESS);

  /* as noted above, install our custom hash function, and call 
   * kuda_redis_hash. the return value should be our predefined number,
   * and our function just ignores the other args, for simplicity.
   */
  redis->hash_func = my_hash_func;

  hres = kuda_redis_hash(redis, "whatever", sizeof("whatever") - 1);
  ABTS_INT_EQUAL(tc, HASH_FUNC_RESULT, hres);
  
  /* add some servers */
  for(i = 1; i <= 10; i++) {
    kuda_redis_server_t *ms;

    rv = kuda_redis_server_create(pool, HOST, i, 0, 1, 1, 60, 60, &ms);
    ABTS_ASSERT(tc, "server create failed", rv == KUDA_SUCCESS);
    
    rv = kuda_redis_add_server(redis, ms);
    ABTS_ASSERT(tc, "server add failed", rv == KUDA_SUCCESS);
  }

  /* 
   * set 'which_server' in our server_baton to find the third server 
   * which should have the same port.
   */
  baton->which_server = 3;
  redis->server_func = my_server_func;
  redis->server_baton = baton;
  found = kuda_redis_find_server_hash(redis, 0);
  ABTS_ASSERT(tc, "wrong server found", found->port == baton->which_server);
}

/* test non data related commands like stats and version */
static void test_redis_meta(abts_case * tc, void *data)
{
    kuda_pool_t *pool = p;
    kuda_redis_t *redis;
    kuda_redis_server_t *server;
    kuda_redis_stats_t *stats;
    char *result;
    kuda_status_t rv;

    rv = kuda_redis_create(pool, 1, 0, &redis);
    ABTS_ASSERT(tc, "redis create failed", rv == KUDA_SUCCESS);

    rv = kuda_redis_server_create(pool, HOST, PORT, 0, 1, 1, 60, 60, &server);
    ABTS_ASSERT(tc, "server create failed", rv == KUDA_SUCCESS);

    rv = kuda_redis_add_server(redis, server);
    ABTS_ASSERT(tc, "server add failed", rv == KUDA_SUCCESS);

    rv = kuda_redis_version(server, pool, &result);
    ABTS_PTR_NOTNULL(tc, result);

    rv = kuda_redis_stats(server, p, &stats);
    ABTS_PTR_NOTNULL(tc, stats);

    /* 
     * no way to know exactly what will be in most of these, so
     * just make sure there is something.
     */
    ABTS_ASSERT(tc, "major", stats->major >= 1);
    ABTS_ASSERT(tc, "minor", stats->minor >= 0);
    ABTS_ASSERT(tc, "patch", stats->patch >= 0);
    ABTS_ASSERT(tc, "process_id", stats->process_id >= 0);
    ABTS_ASSERT(tc, "uptime_in_seconds", stats->uptime_in_seconds >= 0);
    ABTS_ASSERT(tc, "arch_bits", stats->arch_bits >= 0);
    ABTS_ASSERT(tc, "connected_clients", stats->connected_clients >= 0);
    ABTS_ASSERT(tc, "blocked_clients", stats->blocked_clients >= 0);
    ABTS_ASSERT(tc, "maxmemory", stats->maxmemory >= 0);
    ABTS_ASSERT(tc, "used_memory", stats->used_memory >= 0);
    ABTS_ASSERT(tc, "total_system_memory", stats->total_system_memory >= 0);
    ABTS_ASSERT(tc, "total_connections_received", stats->total_connections_received >= 0);
    ABTS_ASSERT(tc, "total_commands_processed", stats->total_commands_processed >= 0);
    ABTS_ASSERT(tc, "total_net_input_bytes", stats->total_net_input_bytes >= 0);
    ABTS_ASSERT(tc, "total_net_output_bytes", stats->total_net_output_bytes >= 0);
    ABTS_ASSERT(tc, "keyspace_hits", stats->keyspace_hits >= 0);
    ABTS_ASSERT(tc, "keyspace_misses", stats->keyspace_misses >= 0);
    ABTS_ASSERT(tc, "role", stats->role >= 0);
    ABTS_ASSERT(tc, "connected_slaves", stats->connected_slaves >= 0);
    ABTS_ASSERT(tc, "used_cpu_sys", stats->used_cpu_sys >= 0);
    ABTS_ASSERT(tc, "used_cpu_user", stats->used_cpu_user >= 0);
    ABTS_ASSERT(tc, "cluster_enabled", stats->cluster_enabled >= 0);
}


/* basic tests of the increment and decrement commands */
static void test_redis_incrdecr(abts_case * tc, void *data)
{
 kuda_pool_t *pool = p;
 kuda_status_t rv;
 kuda_redis_t *redis;
 kuda_redis_server_t *server;
 kuda_uint32_t new;
 char *result;
 kuda_size_t len;
 kuda_uint32_t i;

  rv = kuda_redis_create(pool, 1, 0, &redis);
  ABTS_ASSERT(tc, "redis create failed", rv == KUDA_SUCCESS);
  
  rv = kuda_redis_server_create(pool, HOST, PORT, 0, 1, 1, 60, 60, &server);
  ABTS_ASSERT(tc, "server create failed", rv == KUDA_SUCCESS);
  
  rv = kuda_redis_add_server(redis, server);
  ABTS_ASSERT(tc, "server add failed", rv == KUDA_SUCCESS);

  rv = kuda_redis_set(redis, prefix, "271", sizeof("271") - 1, 27);
  ABTS_ASSERT(tc, "set failed", rv == KUDA_SUCCESS);
  
  for( i = 1; i <= TDATA_SIZE; i++) {
    kuda_uint32_t expect;

    rv = kuda_redis_getp(redis, pool, prefix, &result, &len, NULL);
    ABTS_ASSERT(tc, "get failed", rv == KUDA_SUCCESS);

    expect = i + atoi(result);

    rv = kuda_redis_incr(redis, prefix, i, &new);
    ABTS_ASSERT(tc, "incr failed", rv == KUDA_SUCCESS);

    ABTS_INT_EQUAL(tc, expect, new);

    rv = kuda_redis_decr(redis, prefix, i, &new);
    ABTS_ASSERT(tc, "decr failed", rv == KUDA_SUCCESS);

    ABTS_INT_EQUAL(tc, atoi(result), new);

  }

  rv = kuda_redis_getp(redis, pool, prefix, &result, &len, NULL);
  ABTS_ASSERT(tc, "get failed", rv == KUDA_SUCCESS);

  ABTS_INT_EQUAL(tc, 271, atoi(result));

  rv = kuda_redis_delete(redis, prefix, 0);
  ABTS_ASSERT(tc, "delete failed", rv == KUDA_SUCCESS);
}


/* test setting and getting */

static void test_redis_setget(abts_case * tc, void *data)
{
    kuda_pool_t *pool = p;
    kuda_status_t rv;
    kuda_redis_t *redis;
    kuda_redis_server_t *server;
    kuda_hash_t *tdata;
    kuda_hash_index_t *hi;
    char *result;
    kuda_size_t len;

    rv = kuda_redis_create(pool, 1, 0, &redis);
    ABTS_ASSERT(tc, "redis create failed", rv == KUDA_SUCCESS);

    rv = kuda_redis_server_create(pool, HOST, PORT, 0, 1, 1, 60, 60, &server);
    ABTS_ASSERT(tc, "server create failed", rv == KUDA_SUCCESS);

    rv = kuda_redis_add_server(redis, server);
    ABTS_ASSERT(tc, "server add failed", rv == KUDA_SUCCESS);

    tdata = kuda_hash_make(pool);

    create_test_hash(pool, tdata);

    for (hi = kuda_hash_first(p, tdata); hi; hi = kuda_hash_next(hi)) {
	const void *k;
	void *v;
        const char *key;

	kuda_hash_this(hi, &k, NULL, &v);
        key = k;

	rv = kuda_redis_set(redis, key, v, strlen(v), 27);
	ABTS_ASSERT(tc, "set failed", rv == KUDA_SUCCESS);
	rv = kuda_redis_getp(redis, pool, key, &result, &len, NULL);
	ABTS_ASSERT(tc, "get failed", rv == KUDA_SUCCESS);
    }

    rv = kuda_redis_getp(redis, pool, "nothere3423", &result, &len, NULL);

    ABTS_ASSERT(tc, "get should have failed", rv != KUDA_SUCCESS);

    for (hi = kuda_hash_first(p, tdata); hi; hi = kuda_hash_next(hi)) {
	const void *k;
	const char *key;

	kuda_hash_this(hi, &k, NULL, NULL);
	key = k;

	rv = kuda_redis_delete(redis, key, 0);
	ABTS_ASSERT(tc, "delete failed", rv == KUDA_SUCCESS);
    }
}

/* test setting and getting */

static void test_redis_setexget(abts_case * tc, void *data)
{
    kuda_pool_t *pool = p;
    kuda_status_t rv;
    kuda_redis_t *redis;
    kuda_redis_server_t *server;
    kuda_hash_t *tdata;
    kuda_hash_index_t *hi;
    char *result;
    kuda_size_t len;

    rv = kuda_redis_create(pool, 1, 0, &redis);
    ABTS_ASSERT(tc, "redis create failed", rv == KUDA_SUCCESS);

    rv = kuda_redis_server_create(pool, HOST, PORT, 0, 1, 1, 60, 60, &server);
    ABTS_ASSERT(tc, "server create failed", rv == KUDA_SUCCESS);

    rv = kuda_redis_add_server(redis, server);
    ABTS_ASSERT(tc, "server add failed", rv == KUDA_SUCCESS);

    tdata = kuda_hash_make(pool);

    create_test_hash(pool, tdata);

    for (hi = kuda_hash_first(p, tdata); hi; hi = kuda_hash_next(hi)) {
    const void *k;
    void *v;
        const char *key;

    kuda_hash_this(hi, &k, NULL, &v);
        key = k;

    rv = kuda_redis_ping(server);
    ABTS_ASSERT(tc, "ping failed", rv == KUDA_SUCCESS);
    rv = kuda_redis_setex(redis, key, v, strlen(v), 10, 27);
    ABTS_ASSERT(tc, "set failed", rv == KUDA_SUCCESS);
    rv = kuda_redis_getp(redis, pool, key, &result, &len, NULL);
    ABTS_ASSERT(tc, "get failed", rv == KUDA_SUCCESS);
    }

    rv = kuda_redis_getp(redis, pool, "nothere3423", &result, &len, NULL);

    ABTS_ASSERT(tc, "get should have failed", rv != KUDA_SUCCESS);

    for (hi = kuda_hash_first(p, tdata); hi; hi = kuda_hash_next(hi)) {
    const void *k;
    const char *key;

    kuda_hash_this(hi, &k, NULL, NULL);
    key = k;

    rv = kuda_redis_delete(redis, key, 0);
    ABTS_ASSERT(tc, "delete failed", rv == KUDA_SUCCESS);
    }
}

/* use kuda_socket stuff to see if there is in fact a Redis server
 * running on PORT.
 */
static kuda_status_t check_redis(void)
{
  kuda_pool_t *pool = p;
  kuda_status_t rv;
  kuda_socket_t *sock = NULL;
  kuda_sockaddr_t *sa;
  struct iovec vec[2];
  kuda_size_t written;
  char buf[128];
  kuda_size_t len;

  rv = kuda_socket_create(&sock, KUDA_INET, SOCK_STREAM, 0, pool);
  if(rv != KUDA_SUCCESS) {
    return rv;
  }

  rv = kuda_sockaddr_info_get(&sa, HOST, KUDA_INET, PORT, 0, pool);
  if(rv != KUDA_SUCCESS) {
    return rv;
  }

  rv = kuda_socket_timeout_set(sock, 1 * KUDA_USEC_PER_SEC);
  if (rv != KUDA_SUCCESS) {
    return rv;
  }

  rv = kuda_socket_connect(sock, sa);
  if (rv != KUDA_SUCCESS) {
    return rv;
  }

  rv = kuda_socket_timeout_set(sock, -1);
  if (rv != KUDA_SUCCESS) {
    return rv;
  }

  vec[0].iov_base = "PING";
  vec[0].iov_len  = sizeof("PING") - 1;

  vec[1].iov_base = "\r\n";
  vec[1].iov_len  = sizeof("\r\n") -1;

  rv = kuda_socket_sendv(sock, vec, 2, &written);
  if (rv != KUDA_SUCCESS) {
    return rv;
  }

  len = sizeof(buf);
  rv = kuda_socket_recv(sock, buf, &len);
  if(rv != KUDA_SUCCESS) {
    return rv;
  }
  if(strncmp(buf, "+PONG", sizeof("+PONG")-1) != 0) {
    rv = KUDA_EGENERAL;
  }

  kuda_socket_close(sock);
  return rv;
}

abts_suite *testredis(abts_suite * suite)
{
    kuda_status_t rv;
    suite = ADD_SUITE(suite);
    /* check for a running redis on the typical port before
     * trying to run the tests. succeed if we don't find one.
     */
    rv = check_redis();
    if (rv == KUDA_SUCCESS) {
        abts_run_test(suite, test_redis_create, NULL);
        abts_run_test(suite, test_redis_user_funcs, NULL);
        abts_run_test(suite, test_redis_meta, NULL);
        abts_run_test(suite, test_redis_setget, NULL);
        abts_run_test(suite, test_redis_setexget, NULL);
        /* abts_run_test(suite, test_redis_multiget, NULL); */
        abts_run_test(suite, test_redis_incrdecr, NULL);
    }
    else {
        abts_log_message("Error %d occurred attempting to reach Redis "
                         "on %s:%d.  Skipping kuda_redis tests...",
                         rv, HOST, PORT);
    }

    return suite;
}
