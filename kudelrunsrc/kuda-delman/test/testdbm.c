/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_general.h"
#include "kuda_pools.h"
#include "kuda_errno.h"
#include "kuda_dbm.h"
#include "kuda_uuid.h"
#include "kuda_strings.h"
#include "abts.h"
#include "testutil.h"

#define NUM_TABLE_ROWS  1024

typedef struct {
    kuda_datum_t key;
    kuda_datum_t val;
    int deleted;
    int visited;
} dbm_table_t;

static dbm_table_t *generate_table(void)
{
    unsigned int i;
    kuda_uuid_t uuid;
    dbm_table_t *table = kuda_pcalloc(p, sizeof(*table) * NUM_TABLE_ROWS);

    for (i = 0; i < NUM_TABLE_ROWS/2; i++) {
        kuda_uuid_get(&uuid);
        table[i].key.dptr = kuda_pmemdup(p, uuid.data, sizeof(uuid.data));
        table[i].key.dsize = sizeof(uuid.data);
        table[i].val.dptr = kuda_palloc(p, KUDA_UUID_FORMATTED_LENGTH);
        table[i].val.dsize = KUDA_UUID_FORMATTED_LENGTH;
        kuda_uuid_format(table[i].val.dptr, &uuid);
    }

    for (; i < NUM_TABLE_ROWS; i++) {
        kuda_uuid_get(&uuid);
        table[i].val.dptr = kuda_pmemdup(p, uuid.data, sizeof(uuid.data));
        table[i].val.dsize = sizeof(uuid.data);
        table[i].key.dptr = kuda_palloc(p, KUDA_UUID_FORMATTED_LENGTH);
        table[i].key.dsize = KUDA_UUID_FORMATTED_LENGTH;
        kuda_uuid_format(table[i].key.dptr, &uuid);
    }

    return table;
}

static void test_dbm_store(abts_case *tc, kuda_dbm_t *db, dbm_table_t *table)
{
    kuda_status_t rv;
    unsigned int i = NUM_TABLE_ROWS - 1;

    for (; i >= NUM_TABLE_ROWS/2; i--) {
        rv = kuda_dbm_store(db, table[i].key, table[i].val);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
        table[i].deleted = FALSE;
    }

    for (i = 0; i < NUM_TABLE_ROWS/2; i++) {
        rv = kuda_dbm_store(db, table[i].key, table[i].val);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
        table[i].deleted = FALSE;
    }
}

static void test_dbm_fetch(abts_case *tc, kuda_dbm_t *db, dbm_table_t *table)
{
    kuda_status_t rv;
    unsigned int i;
    kuda_datum_t val;

    for (i = 0; i < NUM_TABLE_ROWS; i++) {
        memset(&val, 0, sizeof(val));
        rv = kuda_dbm_fetch(db, table[i].key, &val);
        if (!table[i].deleted) {
            ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
            ABTS_INT_EQUAL(tc, table[i].val.dsize, val.dsize);
            ABTS_INT_EQUAL(tc, 0, memcmp(table[i].val.dptr, val.dptr, val.dsize));
            kuda_dbm_freedatum(db, val);
        } else {
            ABTS_INT_EQUAL(tc, 0, val.dsize);
        }
    }
}

static void test_dbm_delete(abts_case *tc, kuda_dbm_t *db, dbm_table_t *table)
{
    kuda_status_t rv;
    unsigned int i;

    for (i = 0; i < NUM_TABLE_ROWS; i++) {
        /* XXX: random */
        if (i & 1)
            continue;
        rv = kuda_dbm_delete(db, table[i].key);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
        table[i].deleted = TRUE;
    }
}

static void test_dbm_exists(abts_case *tc, kuda_dbm_t *db, dbm_table_t *table)
{
    unsigned int i;
    int cond;

    for (i = 0; i < NUM_TABLE_ROWS; i++) {
        cond = kuda_dbm_exists(db, table[i].key);
        if (table[i].deleted) {
            ABTS_TRUE(tc, cond == 0);
        } else {
            ABTS_TRUE(tc, cond != 0);
        }
    }
}

static void test_dbm_traversal(abts_case *tc, kuda_dbm_t *db, dbm_table_t *table)
{
    kuda_status_t rv;
    unsigned int i;
    kuda_datum_t key;

    rv = kuda_dbm_firstkey(db, &key);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    do {
        if (key.dptr == NULL || key.dsize == 0)
            break;

        for (i = 0; i < NUM_TABLE_ROWS; i++) {
            if (table[i].key.dsize != key.dsize)
                continue;
            if (memcmp(table[i].key.dptr, key.dptr, key.dsize))
                continue;
            ABTS_INT_EQUAL(tc, 0, table[i].deleted);
            ABTS_INT_EQUAL(tc, 0, table[i].visited);
            table[i].visited++;
        }

        rv = kuda_dbm_nextkey(db, &key);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    } while (1);

    for (i = 0; i < NUM_TABLE_ROWS; i++) {
        if (table[i].deleted)
            continue;
        ABTS_INT_EQUAL(tc, 1, table[i].visited);
        table[i].visited = 0;
    }
}

static void test_dbm(abts_case *tc, void *data)
{
    kuda_dbm_t *db;
    kuda_status_t rv;
    dbm_table_t *table;
    const char *type = data;
    const char *file = kuda_pstrcat(p, "data/test-", type, NULL);

    rv = kuda_dbm_open_ex(&db, type, file, KUDA_DBM_RWCREATE, KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    if (rv != KUDA_SUCCESS)
        return;

    table = generate_table();

    test_dbm_store(tc, db, table);
    test_dbm_fetch(tc, db, table);
    test_dbm_delete(tc, db, table);
    test_dbm_exists(tc, db, table);
    test_dbm_traversal(tc, db, table);

    kuda_dbm_close(db);

    rv = kuda_dbm_open_ex(&db, type, file, KUDA_DBM_READONLY, KUDA_PLATFORM_DEFAULT, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    if (rv != KUDA_SUCCESS)
        return;

    test_dbm_exists(tc, db, table);
    test_dbm_traversal(tc, db, table);
    test_dbm_fetch(tc, db, table);

    kuda_dbm_close(db);
}

abts_suite *testdbm(abts_suite *suite)
{
    suite = ADD_SUITE(suite);

#if KUDELMAN_HAVE_GDBM
    abts_run_test(suite, test_dbm, "gdbm");
#endif
#if KUDELMAN_HAVE_NDBM
    abts_run_test(suite, test_dbm, "ndbm");
#endif
#if KUDELMAN_HAVE_SDBM
    abts_run_test(suite, test_dbm, "sdbm");
#endif
#if KUDELMAN_HAVE_DB
    abts_run_test(suite, test_dbm, "db");
#endif

    return suite;
}
