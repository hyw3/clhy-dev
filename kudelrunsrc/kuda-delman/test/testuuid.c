/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testutil.h"
#include "kuda_general.h"
#include "kuda_uuid.h"

static void test_uuid_parse(abts_case *tc, void *data)
{
    kuda_uuid_t uuid;
    kuda_uuid_t uuid2;
    char buf[KUDA_UUID_FORMATTED_LENGTH + 1];

    kuda_uuid_get(&uuid);
    kuda_uuid_format(buf, &uuid);

    kuda_uuid_parse(&uuid2, buf);
    ABTS_ASSERT(tc, "parse produced a different UUID",
             memcmp(&uuid, &uuid2, sizeof(uuid)) == 0);
}

static void test_gen2(abts_case *tc, void *data)
{
    kuda_uuid_t uuid;
    kuda_uuid_t uuid2;

    /* generate two of them quickly */
    kuda_uuid_get(&uuid);
    kuda_uuid_get(&uuid2);

    ABTS_ASSERT(tc, "generated the same UUID twice",
             memcmp(&uuid, &uuid2, sizeof(uuid)) != 0);
}

abts_suite *testuuid(abts_suite *suite)
{
    suite = ADD_SUITE(suite);

    abts_run_test(suite, test_uuid_parse, NULL);
    abts_run_test(suite, test_gen2, NULL);

    return suite;
}
