/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kudelman.h"
#include "kuda_queue.h"
#include "kuda_thread_pool.h"
#include "kuda_time.h"
#include "abts.h"
#include "testutil.h"

#if KUDA_HAS_THREADS

#define NUMBER_CONSUMERS    3
#define CONSUMER_ACTIVITY   4
#define NUMBER_PRODUCERS    4
#define PRODUCER_ACTIVITY   5
#define QUEUE_SIZE          100

static kuda_queue_t *queue;

static void * KUDA_THREAD_FUNC consumer(kuda_thread_t *thd, void *data)
{
    long sleeprate;
    abts_case *tc = data;
    kuda_status_t rv;
    void *v;

    sleeprate = 1000000/CONSUMER_ACTIVITY;
    kuda_sleep((rand() % 4) * 1000000); /* sleep random seconds */

    while (1)
    {
        rv = kuda_queue_pop(queue, &v);

        if (rv == KUDA_EINTR)
            continue;

        if (rv == KUDA_EOF)
            break;

        ABTS_TRUE(tc, v == NULL);
        ABTS_TRUE(tc, rv == KUDA_SUCCESS);

        kuda_sleep(sleeprate); /* sleep this long to acheive our rate */
    }

    return NULL;
}

static void * KUDA_THREAD_FUNC producer(kuda_thread_t *thd, void *data)
{
    long sleeprate;
    abts_case *tc = data;
    kuda_status_t rv;

    sleeprate = 1000000/PRODUCER_ACTIVITY;
    kuda_sleep((rand() % 4) * 1000000); /* sleep random seconds */

    while (1)
    {
        rv = kuda_queue_push(queue, NULL);

        if (rv == KUDA_EINTR)
            continue;

        if (rv == KUDA_EOF)
            break;

        ABTS_TRUE(tc, rv == KUDA_SUCCESS);

        kuda_sleep(sleeprate); /* sleep this long to acheive our rate */
    }

    return NULL;
}

static void test_queue_producer_consumer(abts_case *tc, void *data)
{
    unsigned int i;
    kuda_status_t rv;
    kuda_thread_pool_t *thrp;

    /* XXX: non-portable */
    srand((unsigned int)kuda_time_now());

    rv = kuda_queue_create(&queue, QUEUE_SIZE, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_thread_pool_create(&thrp, 0, NUMBER_CONSUMERS + NUMBER_PRODUCERS, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    for (i = 0; i < NUMBER_CONSUMERS; i++) {
        rv = kuda_thread_pool_push(thrp, consumer, tc, 0, NULL);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    }

    for (i = 0; i < NUMBER_PRODUCERS; i++) {
        rv = kuda_thread_pool_push(thrp, producer, tc, 0, NULL);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    }

    kuda_sleep(5000000); /* sleep 5 seconds */

    rv = kuda_queue_term(queue);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_thread_pool_destroy(thrp);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

#endif /* KUDA_HAS_THREADS */

abts_suite *testqueue(abts_suite *suite)
{
    suite = ADD_SUITE(suite);

#if KUDA_HAS_THREADS
    abts_run_test(suite, test_queue_producer_consumer, NULL);
#endif /* KUDA_HAS_THREADS */

    return suite;
}
