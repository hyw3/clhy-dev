/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testutil.h"
#include "kuda.h"
#include "kudelman.h"
#include "kuda_pools.h"
#include "kuda_dbd.h"
#include "kuda_strings.h"

static void test_dbd_init(abts_case *tc, void *data)
{
    kuda_pool_t *pool = p;
    kuda_status_t rv;

    rv = kuda_dbd_init(pool);
    ABTS_ASSERT(tc, "failed to init kuda_dbd", rv == KUDA_SUCCESS);
}

#if KUDELMAN_HAVE_SQLITE2 || KUDELMAN_HAVE_SQLITE3
static void test_statement(abts_case *tc, kuda_dbd_t* handle,
                           const kuda_dbd_driver_t* driver, const char* sql)
{
    int nrows;
    kuda_status_t rv;

    rv = kuda_dbd_query(driver, handle, &nrows, sql);

    ABTS_ASSERT(tc, sql, rv == KUDA_SUCCESS);
}

static void create_table(abts_case *tc, kuda_dbd_t* handle,
                         const kuda_dbd_driver_t* driver)
{
    const char *sql = "CREATE TABLE kuda_dbd_test ("
                             "col1 varchar(40) not null,"
                             "col2 varchar(40),"
                             "col3 integer)";

    test_statement(tc, handle, driver, sql);
}

static void drop_table(abts_case *tc, kuda_dbd_t* handle,
                       const kuda_dbd_driver_t* driver)
{
    const char *sql = "DROP TABLE kuda_dbd_test";
    test_statement(tc, handle, driver, sql);
}

static void delete_rows(abts_case *tc, kuda_dbd_t* handle,
                        const kuda_dbd_driver_t* driver)
{
    const char *sql = "DELETE FROM kuda_dbd_test";
    test_statement(tc, handle, driver, sql);
}


static void insert_data(abts_case *tc, kuda_dbd_t* handle,
                        const kuda_dbd_driver_t* driver, int count)
{
    kuda_pool_t* pool = p;
    const char* sql = "INSERT INTO kuda_dbd_test VALUES('%d', '%d', %d)";
    char* sqf = NULL;
    int i;
    int nrows;
    kuda_status_t rv;

    for (i=0; i<count; i++) {
        sqf = kuda_psprintf(pool, sql, i, i, i);
        rv = kuda_dbd_query(driver, handle, &nrows, sqf);
        ABTS_ASSERT(tc, sqf, rv == KUDA_SUCCESS);
        ABTS_ASSERT(tc, sqf, 1 == nrows);
    }
}

static void select_rows(abts_case *tc, kuda_dbd_t* handle,
                        const kuda_dbd_driver_t* driver, int count)
{
    kuda_status_t rv;
    kuda_pool_t* pool = p;
    kuda_pool_t* tpool;
    const char* sql = "SELECT * FROM kuda_dbd_test ORDER BY col1";
    kuda_dbd_results_t *res = NULL;
    kuda_dbd_row_t *row = NULL;
    int i;

    rv = kuda_dbd_select(driver, pool, handle, &res, sql, 0);
    ABTS_ASSERT(tc, sql, rv == KUDA_SUCCESS);
    ABTS_PTR_NOTNULL(tc, res);

    kuda_pool_create(&tpool, pool);
    i = count;
    while (i > 0) {
        row = NULL;
        rv = kuda_dbd_get_row(driver, pool, res, &row, -1);
        ABTS_ASSERT(tc, sql, rv == KUDA_SUCCESS);
        ABTS_PTR_NOTNULL(tc, row);
        kuda_pool_clear(tpool);
        i--;
    }
    ABTS_ASSERT(tc, "Missing Rows!", i == 0);

    res = NULL;
    i = count;

    rv = kuda_dbd_select(driver, pool, handle, &res, sql, 1);
    ABTS_ASSERT(tc, sql, rv == KUDA_SUCCESS);
    ABTS_PTR_NOTNULL(tc, res);

    rv = kuda_dbd_num_tuples(driver, res);
    ABTS_ASSERT(tc, "invalid row count", rv == count);

    while (i > 0) {
        row = NULL;
        rv = kuda_dbd_get_row(driver, pool, res, &row, i);
        ABTS_ASSERT(tc, sql, rv == KUDA_SUCCESS);
        ABTS_PTR_NOTNULL(tc, row);
        kuda_pool_clear(tpool);
        i--;
    }
    ABTS_ASSERT(tc, "Missing Rows!", i == 0);
    rv = kuda_dbd_get_row(driver, pool, res, &row, count+100);
    ABTS_ASSERT(tc, "If we overseek, get_row should return -1", rv == -1);
}

static void test_escape(abts_case *tc, kuda_dbd_t *handle,
                        const kuda_dbd_driver_t *driver)
{
  const char *escaped = kuda_dbd_escape(driver, p, "foo'bar", handle);

  ABTS_STR_EQUAL(tc, "foo''bar", escaped);
}

static void test_dbd_generic(abts_case *tc, kuda_dbd_t* handle,
                             const kuda_dbd_driver_t* driver)
{
    void* native;
    kuda_pool_t *pool = p;
    kuda_status_t rv;

    native = kuda_dbd_native_handle(driver, handle);
    ABTS_PTR_NOTNULL(tc, native);

    rv = kuda_dbd_check_conn(driver, pool, handle);

    create_table(tc, handle, driver);
    select_rows(tc, handle, driver, 0);
    insert_data(tc, handle, driver, 5);
    select_rows(tc, handle, driver, 5);
    delete_rows(tc, handle, driver);
    select_rows(tc, handle, driver, 0);
    drop_table(tc, handle, driver);

    test_escape(tc, handle, driver);

    rv = kuda_dbd_close(driver, handle);
    ABTS_ASSERT(tc, "failed to close database", rv == KUDA_SUCCESS);
}
#endif

#if KUDELMAN_HAVE_SQLITE2
static void test_dbd_sqlite2(abts_case *tc, void *data)
{
    kuda_pool_t *pool = p;
    kuda_status_t rv;
    const kuda_dbd_driver_t* driver = NULL;
    kuda_dbd_t* handle = NULL;

    rv = kuda_dbd_get_driver(pool, "sqlite2", &driver);
    ABTS_ASSERT(tc, "failed to fetch sqlite2 driver", rv == KUDA_SUCCESS);
    ABTS_PTR_NOTNULL(tc, driver);
    if (!driver) {
    	return;
    }

    ABTS_STR_EQUAL(tc, "sqlite2", kuda_dbd_name(driver));

    rv = kuda_dbd_open(driver, pool, "data/sqlite2.db:600", &handle);
    ABTS_ASSERT(tc, "failed to open sqlite2 atabase", rv == KUDA_SUCCESS);
    ABTS_PTR_NOTNULL(tc, handle);
    if (!handle) {
    	return;
    }

    test_dbd_generic(tc, handle, driver);
}
#endif

#if KUDELMAN_HAVE_SQLITE3
static void test_dbd_sqlite3(abts_case *tc, void *data)
{
    kuda_pool_t *pool = p;
    kuda_status_t rv;
    const kuda_dbd_driver_t* driver = NULL;
    kuda_dbd_t* handle = NULL;

    rv = kuda_dbd_get_driver(pool, "sqlite3", &driver);
    ABTS_ASSERT(tc, "failed to fetch sqlite3 driver", rv == KUDA_SUCCESS);
    ABTS_PTR_NOTNULL(tc, driver);
    if (!driver) {
    	return;
    }

    ABTS_STR_EQUAL(tc, "sqlite3", kuda_dbd_name(driver));

    rv = kuda_dbd_open(driver, pool, "data/sqlite3.db", &handle);
    ABTS_ASSERT(tc, "failed to open sqlite3 database", rv == KUDA_SUCCESS);
    ABTS_PTR_NOTNULL(tc, handle);
    if (!handle) {
    	return;
    }

    test_dbd_generic(tc, handle, driver);
}
#endif

abts_suite *testdbd(abts_suite *suite)
{
    suite = ADD_SUITE(suite);


    abts_run_test(suite, test_dbd_init, NULL);

#if KUDELMAN_HAVE_SQLITE2
    abts_run_test(suite, test_dbd_sqlite2, NULL);
#endif

#if KUDELMAN_HAVE_SQLITE3
    abts_run_test(suite, test_dbd_sqlite3, NULL);
#endif
    return suite;
}
