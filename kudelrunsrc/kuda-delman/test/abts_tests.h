/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef KUDA_TEST_INCLUDES
#define KUDA_TEST_INCLUDES

#include "abts.h"
#include "testutil.h"

const struct testlist {
    abts_suite *(*func)(abts_suite *suite);
} alltests[] = {
    {teststrmatch},
    {testuri},
    {testuuid},
    {testbuckets},
    {testpass},
    {testmd4},
    {testmd5},
    {testcrypto},
    {testldap},
    {testdbd},
    {testdate},
    {testmemcache},
    {testredis},
    {testxml},
    {testxlate},
    {testrmm},
    {testdbm},
    {testqueue},
    {testreslist},
    {testsiphash}
};

#endif /* KUDA_TEST_INCLUDES */
