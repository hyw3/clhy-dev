/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_general.h"
#include "kuda_xml.h"
#include "abts.h"
#include "testutil.h"

static kuda_status_t create_dummy_file_error(abts_case *tc, kuda_pool_t *p,
                                            kuda_file_t **fd)
{
    int i;
    kuda_status_t rv;
    kuda_off_t off = 0L;
    char template[] = "data/testxmldummyerrorXXXXXX";

    rv = kuda_file_mktemp(fd, template, KUDA_FOPEN_CREATE | KUDA_FOPEN_TRUNCATE | KUDA_FOPEN_DELONCLOSE |
                         KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_EXCL, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    if (rv != KUDA_SUCCESS)
        return rv;

    rv = kuda_file_puts("<?xml version=\"1.0\" ?>\n<maryx>"
                       "<had a=\"little\"/><lamb/>\n", *fd);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    for (i = 0; i < 5000; i++) {
        rv = kuda_file_puts("<hmm roast=\"lamb\" "
                           "for=\"dinner\">yummy</hmm>\n", *fd);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    }

    rv = kuda_file_puts("</mary>\n", *fd);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_seek(*fd, KUDA_SET, &off);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    return rv;
}

static kuda_status_t create_dummy_file(abts_case *tc, kuda_pool_t *p,
                                      kuda_file_t **fd)
{
    int i;
    kuda_status_t rv;
    kuda_off_t off = 0L;
    char template[] = "data/testxmldummyXXXXXX";

    rv = kuda_file_mktemp(fd, template, KUDA_FOPEN_CREATE | KUDA_FOPEN_TRUNCATE | KUDA_FOPEN_DELONCLOSE |
                         KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_EXCL, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    if (rv != KUDA_SUCCESS)
        return rv;

    rv = kuda_file_puts("<?xml version=\"1.0\" ?>\n<mary>\n", *fd);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    for (i = 0; i < 5000; i++) {
        rv = kuda_file_puts("<hmm roast=\"lamb\" "
                           "for=\"dinner &lt;&gt;&#x3D;\">yummy</hmm>\n", *fd);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    }

    rv = kuda_file_puts("</mary>\n", *fd);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = kuda_file_seek(*fd, KUDA_SET, &off);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    return rv;
}

static void dump_xml(abts_case *tc, kuda_xml_elem *e, int level)
{
    kuda_xml_attr *a;
    kuda_xml_elem *ec;

    if (level == 0) {
        ABTS_STR_EQUAL(tc, "mary", e->name);
    } else {
        ABTS_STR_EQUAL(tc, "hmm", e->name);
    }

    if (e->attr) {
        a = e->attr;
        ABTS_PTR_NOTNULL(tc, a);
        ABTS_STR_EQUAL(tc, "for", a->name);
        ABTS_STR_EQUAL(tc, "dinner <>=", a->value);
        a = a->next;
        ABTS_PTR_NOTNULL(tc, a);
        ABTS_STR_EQUAL(tc, "roast", a->name);
        ABTS_STR_EQUAL(tc, "lamb", a->value);
    }
    if (e->first_child) {
        ec = e->first_child;
        while (ec) {
            dump_xml(tc, ec, level + 1);
            ec = ec->next;
        }
    }
}

static void test_xml_parser(abts_case *tc, void *data)
{
    kuda_file_t *fd;
    kuda_xml_parser *parser;
    kuda_xml_doc *doc;
    kuda_status_t rv;

    rv = create_dummy_file(tc, p, &fd);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    if (rv != KUDA_SUCCESS)
        return;

    rv = kuda_xml_parse_file(p, &parser, &doc, fd, 2000);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    dump_xml(tc, doc->root, 0);

    rv = kuda_file_close(fd);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    rv = create_dummy_file_error(tc, p, &fd);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    if (rv != KUDA_SUCCESS)
        return;

    rv = kuda_xml_parse_file(p, &parser, &doc, fd, 2000);
    ABTS_TRUE(tc, rv != KUDA_SUCCESS);
}

static void test_billion_laughs(abts_case *tc, void *data)
{
    kuda_file_t *fd;
    kuda_xml_parser *parser;
    kuda_xml_doc *doc;
    kuda_status_t rv;

    rv = kuda_file_open(&fd, "data/billion-laughs.xml", 
                       KUDA_FOPEN_READ, 0, p);
    kuda_assert_success(tc, "open billion-laughs.xml", rv);

    /* Don't test for return value; if it returns, chances are the bug
     * is fixed or the machine has insane amounts of RAM. */
    kuda_xml_parse_file(p, &parser, &doc, fd, 2000);

    kuda_file_close(fd);
}

static void test_CVE_2009_3720_alpha(abts_case *tc, void *data)
{
    kuda_xml_parser *xp;
    kuda_xml_doc *doc;
    kuda_status_t rv;

    xp = kuda_xml_parser_create(p);
    
    rv = kuda_xml_parser_feed(xp, "\0\r\n", 3);
    if (rv == KUDA_SUCCESS)
        kuda_xml_parser_done(xp, &doc);
}

static void test_CVE_2009_3720_beta(abts_case *tc, void *data)
{
    kuda_xml_parser *xp;
    kuda_xml_doc *doc;
    kuda_status_t rv;

    xp = kuda_xml_parser_create(p);
    
    rv = kuda_xml_parser_feed(xp, "<?xml version\xc2\x85='1.0'?>\r\n", 25);
    if (rv == KUDA_SUCCESS)
        kuda_xml_parser_done(xp, &doc);
}

abts_suite *testxml(abts_suite *suite)
{
    suite = ADD_SUITE(suite);

    abts_run_test(suite, test_xml_parser, NULL);
    abts_run_test(suite, test_billion_laughs, NULL);
    abts_run_test(suite, test_CVE_2009_3720_alpha, NULL);
    abts_run_test(suite, test_CVE_2009_3720_beta, NULL);

    return suite;
}
