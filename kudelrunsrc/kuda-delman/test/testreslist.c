/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>

#include "kuda_general.h"
#include "kudelman.h"
#include "kuda_reslist.h"
#include "kuda_thread_pool.h"

#if KUDA_HAVE_TIME_H
#include <time.h>
#endif /* KUDA_HAVE_TIME_H */

#include "abts.h"
#include "testutil.h"

#if KUDA_HAS_THREADS

#define RESLIST_MIN   3
#define RESLIST_SMAX 10
#define RESLIST_HMAX 20
#define RESLIST_TTL  KUDA_TIME_C(35000) /* 35 ms */
#define CONSUMER_THREADS 25
#define CONSUMER_ITERATIONS 250
#define CONSTRUCT_SLEEP_TIME  KUDA_TIME_C(25000) /* 25 ms */
#define DESTRUCT_SLEEP_TIME   KUDA_TIME_C(10000) /* 10 ms */
#define WORK_DELAY_SLEEP_TIME KUDA_TIME_C(15000) /* 15 ms */

typedef struct {
    kuda_interval_time_t sleep_upon_construct;
    kuda_interval_time_t sleep_upon_destruct;
    int c_count;
    int d_count;
} my_parameters_t;

typedef struct {
    int id;
} my_resource_t;

/* Linear congruential generator */
static kuda_uint32_t lgc(kuda_uint32_t a)
{
    kuda_uint64_t z = a;
    z *= 279470273;
    z %= KUDA_UINT64_C(4294967291);
    return (kuda_uint32_t)z;
}

static kuda_status_t my_constructor(void **resource, void *params,
                                   kuda_pool_t *pool)
{
    my_resource_t *res;
    my_parameters_t *my_params = params;

    /* Create some resource */
    res = kuda_palloc(pool, sizeof(*res));
    res->id = my_params->c_count++;

    /* Sleep for awhile, to simulate construction overhead. */
    kuda_sleep(my_params->sleep_upon_construct);

    /* Set the resource so it can be managed by the reslist */
    *resource = res;
    return KUDA_SUCCESS;
}

static kuda_status_t my_destructor(void *resource, void *params,
                                  kuda_pool_t *pool)
{
    my_resource_t *res = resource;
    my_parameters_t *my_params = params;
    res->id = my_params->d_count++;

    kuda_sleep(my_params->sleep_upon_destruct);

    return KUDA_SUCCESS;
}

typedef struct {
    int tid;
    abts_case *tc;
    kuda_reslist_t *reslist;
    kuda_interval_time_t work_delay_sleep;
} my_thread_info_t;

/* MAX_UINT * .95 = 2**32 * .95 = 4080218931u */
#define PERCENT95th 4080218931u

static void * KUDA_THREAD_FUNC resource_consuming_thread(kuda_thread_t *thd,
                                                        void *data)
{
    int i;
    kuda_uint32_t chance;
    void *vp;
    kuda_status_t rv;
    my_resource_t *res;
    my_thread_info_t *thread_info = data;
    kuda_reslist_t *rl = thread_info->reslist;

#if KUDA_HAS_RANDOM
    kuda_generate_random_bytes((void*)&chance, sizeof(chance));
#else
    chance = (kuda_uint32_t)(kuda_time_now() % KUDA_TIME_C(4294967291));
#endif

    for (i = 0; i < CONSUMER_ITERATIONS; i++) {
        rv = kuda_reslist_acquire(rl, &vp);
        ABTS_INT_EQUAL(thread_info->tc, KUDA_SUCCESS, rv);
        res = vp;
        kuda_sleep(thread_info->work_delay_sleep);

        /* simulate a 5% chance of the resource being bad */
        chance = lgc(chance);
        if ( chance < PERCENT95th ) {
            rv = kuda_reslist_release(rl, res);
            ABTS_INT_EQUAL(thread_info->tc, KUDA_SUCCESS, rv);
        } else {
            rv = kuda_reslist_invalidate(rl, res);
            ABTS_INT_EQUAL(thread_info->tc, KUDA_SUCCESS, rv);
        }
    }

    return KUDA_SUCCESS;
}

static void test_timeout(abts_case *tc, kuda_reslist_t *rl)
{
    kuda_status_t rv;
    my_resource_t *resources[RESLIST_HMAX];
    void *vp;
    int i;

    kuda_reslist_timeout_set(rl, 1000);

    /* deplete all possible resources from the resource list
     * so that the next call will block until timeout is reached
     * (since there are no other threads to make a resource
     * available)
     */

    for (i = 0; i < RESLIST_HMAX; i++) {
        rv = kuda_reslist_acquire(rl, (void**)&resources[i]);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    }

    /* next call will block until timeout is reached */
    rv = kuda_reslist_acquire(rl, &vp);
    ABTS_TRUE(tc, KUDA_STATUS_IS_TIMEUP(rv));

    /* release the resources; otherwise the destroy operation
     * will blow
     */
    for (i = 0; i < RESLIST_HMAX; i++) {
        rv = kuda_reslist_release(rl, resources[i]);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    }
}

static void test_shrinking(abts_case *tc, kuda_reslist_t *rl)
{
    kuda_status_t rv;
    my_resource_t *resources[RESLIST_HMAX];
    my_resource_t *res;
    void *vp;
    int i;
    int sleep_time = RESLIST_TTL / RESLIST_HMAX;

    /* deplete all possible resources from the resource list */
    for (i = 0; i < RESLIST_HMAX; i++) {
        rv = kuda_reslist_acquire(rl, (void**)&resources[i]);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    }

    /* Free all resources above RESLIST_SMAX - 1 */
    for (i = RESLIST_SMAX - 1; i < RESLIST_HMAX; i++) {
        rv = kuda_reslist_release(rl, resources[i]);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    }

    for (i = 0; i < RESLIST_HMAX; i++) {
        rv = kuda_reslist_acquire(rl, &vp);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
        res = vp;
        kuda_sleep(sleep_time);
        rv = kuda_reslist_release(rl, res);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    }
    kuda_sleep(sleep_time);

    /*
     * Now free the remaining elements. This should trigger the shrinking of
     * the list
     */
    for (i = 0; i < RESLIST_SMAX - 1; i++) {
        rv = kuda_reslist_release(rl, resources[i]);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    }
}

static void test_reslist(abts_case *tc, void *data)
{
    int i;
    kuda_status_t rv;
    kuda_reslist_t *rl;
    my_parameters_t *params;
    kuda_thread_pool_t *thrp;
    my_thread_info_t thread_info[CONSUMER_THREADS];

    rv = kuda_thread_pool_create(&thrp, CONSUMER_THREADS/2, CONSUMER_THREADS, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    /* Create some parameters that will be passed into each
     * constructor and destructor call. */
    params = kuda_pcalloc(p, sizeof(*params));
    params->sleep_upon_construct = CONSTRUCT_SLEEP_TIME;
    params->sleep_upon_destruct = DESTRUCT_SLEEP_TIME;

    /* We're going to want 10 blocks of data from our target rmm. */
    rv = kuda_reslist_create(&rl, RESLIST_MIN, RESLIST_SMAX, RESLIST_HMAX,
                            RESLIST_TTL, my_constructor, my_destructor,
                            params, p);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    for (i = 0; i < CONSUMER_THREADS; i++) {
        thread_info[i].tid = i;
        thread_info[i].tc = tc;
        thread_info[i].reslist = rl;
        thread_info[i].work_delay_sleep = WORK_DELAY_SLEEP_TIME;
        rv = kuda_thread_pool_push(thrp, resource_consuming_thread,
                                  &thread_info[i], 0, NULL);
        ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
    }

    rv = kuda_thread_pool_destroy(thrp);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);

    test_timeout(tc, rl);

    test_shrinking(tc, rl);
    ABTS_INT_EQUAL(tc, RESLIST_SMAX, params->c_count - params->d_count);

    rv = kuda_reslist_destroy(rl);
    ABTS_INT_EQUAL(tc, KUDA_SUCCESS, rv);
}

#endif /* KUDA_HAS_THREADS */

abts_suite *testreslist(abts_suite *suite)
{
    suite = ADD_SUITE(suite);

#if KUDA_HAS_THREADS
    abts_run_test(suite, test_reslist, NULL);
#endif

    return suite;
}
