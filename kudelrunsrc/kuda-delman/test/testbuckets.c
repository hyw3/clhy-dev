/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "abts.h"
#include "testutil.h"
#include "kuda_buckets.h"
#include "kuda_strings.h"

static void test_create(abts_case *tc, void *data)
{
    kuda_bucket_alloc_t *ba;
    kuda_bucket_brigade *bb;

    ba = kuda_bucket_alloc_create(p);
    bb = kuda_brigade_create(p, ba);

    ABTS_ASSERT(tc, "new brigade not NULL", bb != NULL);
    ABTS_ASSERT(tc, "new brigade is empty", KUDA_BRIGADE_EMPTY(bb));

    kuda_brigade_destroy(bb);
    kuda_bucket_alloc_destroy(ba);
}

static void test_simple(abts_case *tc, void *data)
{
    kuda_bucket_alloc_t *ba;
    kuda_bucket_brigade *bb;
    kuda_bucket *fb, *tb;
    
    ba = kuda_bucket_alloc_create(p);
    bb = kuda_brigade_create(p, ba);
    
    fb = KUDA_BRIGADE_FIRST(bb);
    ABTS_ASSERT(tc, "first bucket of empty brigade is sentinel",
                fb == KUDA_BRIGADE_SENTINEL(bb));

    fb = kuda_bucket_flush_create(ba);
    KUDA_BRIGADE_INSERT_HEAD(bb, fb);

    ABTS_ASSERT(tc, "first bucket of brigade is flush",
                KUDA_BRIGADE_FIRST(bb) == fb);

    ABTS_ASSERT(tc, "bucket after flush is sentinel",
                KUDA_BUCKET_NEXT(fb) == KUDA_BRIGADE_SENTINEL(bb));

    tb = kuda_bucket_transient_create("aaa", 3, ba);
    KUDA_BUCKET_INSERT_BEFORE(fb, tb);

    ABTS_ASSERT(tc, "bucket before flush now transient",
                KUDA_BUCKET_PREV(fb) == tb);
    ABTS_ASSERT(tc, "bucket after transient is flush",
                KUDA_BUCKET_NEXT(tb) == fb);
    ABTS_ASSERT(tc, "bucket before transient is sentinel",
                KUDA_BUCKET_PREV(tb) == KUDA_BRIGADE_SENTINEL(bb));

    kuda_brigade_cleanup(bb);

    ABTS_ASSERT(tc, "cleaned up brigade was empty", KUDA_BRIGADE_EMPTY(bb));

    kuda_brigade_destroy(bb);
    kuda_bucket_alloc_destroy(ba);
}

static kuda_bucket_brigade *make_simple_brigade(kuda_bucket_alloc_t *ba,
                                               const char *first, 
                                               const char *second)
{
    kuda_bucket_brigade *bb = kuda_brigade_create(p, ba);
    kuda_bucket *e;
 
    e = kuda_bucket_transient_create(first, strlen(first), ba);
    KUDA_BRIGADE_INSERT_TAIL(bb, e);

    e = kuda_bucket_transient_create(second, strlen(second), ba);
    KUDA_BRIGADE_INSERT_TAIL(bb, e);

    return bb;
}

/* tests that 'bb' flattens to string 'expect'. */
static void flatten_match(abts_case *tc, const char *ctx,
                          kuda_bucket_brigade *bb,
                          const char *expect)
{
    kuda_size_t elen = strlen(expect);
    char *buf = malloc(elen);
    kuda_size_t len = elen;
    char msg[200];

    sprintf(msg, "%s: flatten brigade", ctx);
    kuda_assert_success(tc, msg, kuda_brigade_flatten(bb, buf, &len));
    sprintf(msg, "%s: length match (%ld not %ld)", ctx,
            (long)len, (long)elen);
    ABTS_ASSERT(tc, msg, len == elen);
    sprintf(msg, "%s: result match", msg);
    ABTS_STR_NEQUAL(tc, expect, buf, len);
    free(buf);
}

static void test_flatten(abts_case *tc, void *data)
{
    kuda_bucket_alloc_t *ba = kuda_bucket_alloc_create(p);
    kuda_bucket_brigade *bb;

    bb = make_simple_brigade(ba, "hello, ", "world");

    flatten_match(tc, "flatten brigade", bb, "hello, world");

    kuda_brigade_destroy(bb);
    kuda_bucket_alloc_destroy(ba);    
}

static int count_buckets(kuda_bucket_brigade *bb)
{
    kuda_bucket *e;
    int count = 0;

    for (e = KUDA_BRIGADE_FIRST(bb); 
         e != KUDA_BRIGADE_SENTINEL(bb);
         e = KUDA_BUCKET_NEXT(e)) {
        count++;
    }
    
    return count;
}

static void test_split(abts_case *tc, void *data)
{
    kuda_bucket_alloc_t *ba = kuda_bucket_alloc_create(p);
    kuda_bucket_brigade *bb, *bb2;
    kuda_bucket *e;

    bb = make_simple_brigade(ba, "hello, ", "world");

    /* split at the "world" bucket */
    e = KUDA_BRIGADE_LAST(bb);
    bb2 = kuda_brigade_split(bb, e);

    ABTS_ASSERT(tc, "split brigade contains one bucket",
                count_buckets(bb2) == 1);
    ABTS_ASSERT(tc, "original brigade contains one bucket",
                count_buckets(bb) == 1);

    flatten_match(tc, "match original brigade", bb, "hello, ");
    flatten_match(tc, "match split brigade", bb2, "world");

    kuda_brigade_destroy(bb2);
    kuda_brigade_destroy(bb);
    kuda_bucket_alloc_destroy(ba);
}

#define COUNT 3000
#define THESTR "hello"

static void test_bwrite(abts_case *tc, void *data)
{
    kuda_bucket_alloc_t *ba = kuda_bucket_alloc_create(p);
    kuda_bucket_brigade *bb = kuda_brigade_create(p, ba);
    kuda_off_t length;
    int n;

    for (n = 0; n < COUNT; n++) {
        kuda_assert_success(tc, "brigade_write", 
                           kuda_brigade_write(bb, NULL, NULL,
                                             THESTR, sizeof THESTR));
    }

    kuda_assert_success(tc, "determine brigade length",
                       kuda_brigade_length(bb, 1, &length));

    ABTS_ASSERT(tc, "brigade has correct length",
                length == (COUNT * sizeof THESTR));
    
    kuda_brigade_destroy(bb);
    kuda_bucket_alloc_destroy(ba);
}

static void test_splitline(abts_case *tc, void *data)
{
    kuda_bucket_alloc_t *ba = kuda_bucket_alloc_create(p);
    kuda_bucket_brigade *bin, *bout;

    bin = make_simple_brigade(ba, "blah blah blah-",
                              "end of line.\nfoo foo foo");
    bout = kuda_brigade_create(p, ba);

    kuda_assert_success(tc, "split line",
                       kuda_brigade_split_line(bout, bin,
                                              KUDA_BLOCK_READ, 100));

    flatten_match(tc, "split line", bout, "blah blah blah-end of line.\n");
    flatten_match(tc, "remainder", bin, "foo foo foo");

    kuda_brigade_destroy(bout);
    kuda_brigade_destroy(bin);
    kuda_bucket_alloc_destroy(ba);
}

/* Test that bucket E has content EDATA of length ELEN. */
static void test_bucket_content(abts_case *tc,
                                kuda_bucket *e,
                                const char *edata,
                                kuda_size_t elen)
{
    const char *adata;
    kuda_size_t alen;

    kuda_assert_success(tc, "read from bucket",
                       kuda_bucket_read(e, &adata, &alen, 
                                       KUDA_BLOCK_READ));

    ABTS_ASSERT(tc, "read expected length", alen == elen);
    ABTS_STR_NEQUAL(tc, edata, adata, elen);
}

static void test_splits(abts_case *tc, void *ctx)
{
    kuda_bucket_alloc_t *ba = kuda_bucket_alloc_create(p);
    kuda_bucket_brigade *bb;
    kuda_bucket *e;
    char *str = "alphabeta";
    int n;

    bb = kuda_brigade_create(p, ba);

    KUDA_BRIGADE_INSERT_TAIL(bb,
                            kuda_bucket_immortal_create(str, 9, ba));
    KUDA_BRIGADE_INSERT_TAIL(bb, 
                            kuda_bucket_transient_create(str, 9, ba));
    KUDA_BRIGADE_INSERT_TAIL(bb, 
                            kuda_bucket_heap_create(strdup(str), 9, free, ba));
    KUDA_BRIGADE_INSERT_TAIL(bb, 
                            kuda_bucket_pool_create(kuda_pstrdup(p, str), 9, p, 
                                                   ba));

    ABTS_ASSERT(tc, "four buckets inserted", count_buckets(bb) == 4);
    
    /* now split each of the buckets after byte 5 */
    for (n = 0, e = KUDA_BRIGADE_FIRST(bb); n < 4; n++) {
        ABTS_ASSERT(tc, "reached end of brigade", 
                    e != KUDA_BRIGADE_SENTINEL(bb));
        ABTS_ASSERT(tc, "split bucket OK",
                    kuda_bucket_split(e, 5) == KUDA_SUCCESS);
        e = KUDA_BUCKET_NEXT(e);
        ABTS_ASSERT(tc, "split OK", e != KUDA_BRIGADE_SENTINEL(bb));
        e = KUDA_BUCKET_NEXT(e);
    }
    
    ABTS_ASSERT(tc, "four buckets split into eight", 
                count_buckets(bb) == 8);

    for (n = 0, e = KUDA_BRIGADE_FIRST(bb); n < 4; n++) {
        const char *data;
        kuda_size_t len;
        
        kuda_assert_success(tc, "read alpha from bucket",
                           kuda_bucket_read(e, &data, &len, KUDA_BLOCK_READ));
        ABTS_ASSERT(tc, "read 5 bytes", len == 5);
        ABTS_STR_NEQUAL(tc, "alpha", data, 5);

        e = KUDA_BUCKET_NEXT(e);

        kuda_assert_success(tc, "read beta from bucket",
                           kuda_bucket_read(e, &data, &len, KUDA_BLOCK_READ));
        ABTS_ASSERT(tc, "read 4 bytes", len == 4);
        ABTS_STR_NEQUAL(tc, "beta", data, 5);

        e = KUDA_BUCKET_NEXT(e);
    }

    /* now delete the "alpha" buckets */
    for (n = 0, e = KUDA_BRIGADE_FIRST(bb); n < 4; n++) {
        kuda_bucket *f;

        ABTS_ASSERT(tc, "reached end of brigade",
                    e != KUDA_BRIGADE_SENTINEL(bb));
        f = KUDA_BUCKET_NEXT(e);
        kuda_bucket_delete(e);
        e = KUDA_BUCKET_NEXT(f);
    }    
    
    ABTS_ASSERT(tc, "eight buckets reduced to four", 
                count_buckets(bb) == 4);

    flatten_match(tc, "flatten beta brigade", bb,
                  "beta" "beta" "beta" "beta");

    kuda_brigade_destroy(bb);
    kuda_bucket_alloc_destroy(ba);
}

#define TIF_FNAME "testfile.txt"

static void test_insertfile(abts_case *tc, void *ctx)
{
    kuda_bucket_alloc_t *ba = kuda_bucket_alloc_create(p);
    kuda_bucket_brigade *bb;
    const kuda_off_t bignum = (KUDA_INT64_C(2) << 32) + 424242;
    kuda_off_t count;
    kuda_file_t *f;
    kuda_bucket *e;

    ABTS_ASSERT(tc, "open test file",
                kuda_file_open(&f, TIF_FNAME,
                              KUDA_FOPEN_WRITE | KUDA_FOPEN_TRUNCATE
                            | KUDA_FOPEN_CREATE | KUDA_FOPEN_SPARSE,
                              KUDA_PLATFORM_DEFAULT, p) == KUDA_SUCCESS);

    if (kuda_file_trunc(f, bignum)) {
        kuda_file_close(f);
        kuda_file_remove(TIF_FNAME, p);
        ABTS_NOT_IMPL(tc, "Skipped: could not create large file");
        return;
    }
    
    bb = kuda_brigade_create(p, ba);

    e = kuda_brigade_insert_file(bb, f, 0, bignum, p);
    
    ABTS_ASSERT(tc, "inserted file was not at end of brigade",
                e == KUDA_BRIGADE_LAST(bb));

    /* check that the total size of inserted buckets is equal to the
     * total size of the file. */
    count = 0;

    for (e = KUDA_BRIGADE_FIRST(bb);
         e != KUDA_BRIGADE_SENTINEL(bb);
         e = KUDA_BUCKET_NEXT(e)) {
        ABTS_ASSERT(tc, "bucket size sane", e->length != (kuda_size_t)-1);
        count += e->length;
    }

    ABTS_ASSERT(tc, "total size of buckets incorrect", count == bignum);

    kuda_brigade_destroy(bb);

    /* Truncate the file to zero size before close() so that we don't
     * actually write out the large file if we are on a non-sparse file
     * system - like Mac OS X's HFS.  Otherwise, pity the poor user who
     * has to wait for the 8GB file to be written to disk.
     */
    kuda_file_trunc(f, 0);

    kuda_file_close(f);
    kuda_bucket_alloc_destroy(ba);
    kuda_file_remove(TIF_FNAME, p);
}

/* Make a test file named FNAME, and write CONTENTS to it. */
static kuda_file_t *make_test_file(abts_case *tc, const char *fname,
                                  const char *contents)
{
    kuda_file_t *f;

    ABTS_ASSERT(tc, "create test file",
                kuda_file_open(&f, fname,
                              KUDA_FOPEN_READ|KUDA_FOPEN_WRITE|KUDA_FOPEN_TRUNCATE|KUDA_FOPEN_CREATE,
                              KUDA_PLATFORM_DEFAULT, p) == KUDA_SUCCESS);
    
    ABTS_ASSERT(tc, "write test file contents",
                kuda_file_puts(contents, f) == KUDA_SUCCESS);

    return f;
}

static void test_manyfile(abts_case *tc, void *data)
{
    kuda_bucket_alloc_t *ba = kuda_bucket_alloc_create(p);
    kuda_bucket_brigade *bb = kuda_brigade_create(p, ba);
    kuda_file_t *f;

    f = make_test_file(tc, "manyfile.bin",
                       "world" "hello" "brave" " ,\n");

    kuda_brigade_insert_file(bb, f, 5, 5, p);
    kuda_brigade_insert_file(bb, f, 16, 1, p);
    kuda_brigade_insert_file(bb, f, 15, 1, p);
    kuda_brigade_insert_file(bb, f, 10, 5, p);
    kuda_brigade_insert_file(bb, f, 15, 1, p);
    kuda_brigade_insert_file(bb, f, 0, 5, p);
    kuda_brigade_insert_file(bb, f, 17, 1, p);

    /* can you tell what it is yet? */
    flatten_match(tc, "file seek test", bb,
                  "hello, brave world\n");

    kuda_file_close(f);
    kuda_brigade_destroy(bb);
    kuda_bucket_alloc_destroy(ba);
}

/* Regression test for PR 34708, where a file bucket will keep
 * duplicating itself on being read() when EOF is reached
 * prematurely. */
static void test_truncfile(abts_case *tc, void *data)
{
    kuda_bucket_alloc_t *ba = kuda_bucket_alloc_create(p);
    kuda_bucket_brigade *bb = kuda_brigade_create(p, ba);
    kuda_file_t *f = make_test_file(tc, "testfile.txt", "hello");
    kuda_bucket *e;
    const char *buf;
    kuda_size_t len;

    kuda_brigade_insert_file(bb, f, 0, 5, p);

    kuda_file_trunc(f, 0);

    e = KUDA_BRIGADE_FIRST(bb);

    ABTS_ASSERT(tc, "single bucket in brigade",
                KUDA_BUCKET_NEXT(e) == KUDA_BRIGADE_SENTINEL(bb));

    kuda_bucket_file_enable_mmap(e, 0);

    ABTS_ASSERT(tc, "read gave KUDA_EOF",
                kuda_bucket_read(e, &buf, &len, KUDA_BLOCK_READ) == KUDA_EOF);

    ABTS_ASSERT(tc, "read length 0", len == 0);
    
    ABTS_ASSERT(tc, "still a single bucket in brigade",
                KUDA_BUCKET_NEXT(e) == KUDA_BRIGADE_SENTINEL(bb));

    kuda_file_close(f);
    kuda_brigade_destroy(bb);
    kuda_bucket_alloc_destroy(ba);
}

static const char hello[] = "hello, world";

static void test_partition(abts_case *tc, void *data)
{
    kuda_bucket_alloc_t *ba = kuda_bucket_alloc_create(p);
    kuda_bucket_brigade *bb = kuda_brigade_create(p, ba);
    kuda_bucket *e;

    e = kuda_bucket_immortal_create(hello, strlen(hello), ba);
    KUDA_BRIGADE_INSERT_HEAD(bb, e);

    kuda_assert_success(tc, "partition brigade",
                       kuda_brigade_partition(bb, 5, &e));

    test_bucket_content(tc, KUDA_BRIGADE_FIRST(bb),
                        "hello", 5);

    test_bucket_content(tc, KUDA_BRIGADE_LAST(bb),
                        ", world", 7);

    ABTS_ASSERT(tc, "partition returns KUDA_INCOMPLETE",
                kuda_brigade_partition(bb, 8192, &e));

    ABTS_ASSERT(tc, "KUDA_INCOMPLETE partition returned sentinel",
                e == KUDA_BRIGADE_SENTINEL(bb));

    kuda_brigade_destroy(bb);
    kuda_bucket_alloc_destroy(ba);
}

static void test_write_split(abts_case *tc, void *data)
{
    kuda_bucket_alloc_t *ba = kuda_bucket_alloc_create(p);
    kuda_bucket_brigade *bb1 = kuda_brigade_create(p, ba);
    kuda_bucket_brigade *bb2;
    kuda_bucket *e;

    e = kuda_bucket_heap_create(hello, strlen(hello), NULL, ba);
    KUDA_BRIGADE_INSERT_HEAD(bb1, e);
    kuda_bucket_split(e, strlen("hello, "));
    bb2 = kuda_brigade_split(bb1, KUDA_BRIGADE_LAST(bb1));
    kuda_brigade_write(bb1, NULL, NULL, "foo", strlen("foo"));
    test_bucket_content(tc, KUDA_BRIGADE_FIRST(bb2), "world", 5);

    kuda_brigade_destroy(bb1);
    kuda_brigade_destroy(bb2);
    kuda_bucket_alloc_destroy(ba);
}

static void test_write_putstrs(abts_case *tc, void *data)
{
    kuda_bucket_alloc_t *ba = kuda_bucket_alloc_create(p);
    kuda_bucket_brigade *bb = kuda_brigade_create(p, ba);
    kuda_bucket *e;
    char buf[30];
    kuda_size_t len = sizeof(buf);
    const char *expect = "123456789abcdefghij";

    e = kuda_bucket_heap_create("1", 1, NULL, ba);
    KUDA_BRIGADE_INSERT_HEAD(bb, e);

    kuda_brigade_putstrs(bb, NULL, NULL, "2", "34", "567", "8", "9a", "bcd",
                        "e", "f", "gh", "i", NULL);
    kuda_brigade_putstrs(bb, NULL, NULL, "j", NULL);
    kuda_assert_success(tc, "kuda_brigade_flatten",
                       kuda_brigade_flatten(bb, buf, &len));
    ABTS_STR_NEQUAL(tc, expect, buf, strlen(expect));

    kuda_brigade_destroy(bb);
    kuda_bucket_alloc_destroy(ba);
}

abts_suite *testbuckets(abts_suite *suite)
{
    suite = ADD_SUITE(suite);

    abts_run_test(suite, test_create, NULL);
    abts_run_test(suite, test_simple, NULL);
    abts_run_test(suite, test_flatten, NULL);
    abts_run_test(suite, test_split, NULL);
    abts_run_test(suite, test_bwrite, NULL);
    abts_run_test(suite, test_splitline, NULL);
    abts_run_test(suite, test_splits, NULL);
    abts_run_test(suite, test_insertfile, NULL);
    abts_run_test(suite, test_manyfile, NULL);
    abts_run_test(suite, test_truncfile, NULL);
    abts_run_test(suite, test_partition, NULL);
    abts_run_test(suite, test_write_split, NULL);
    abts_run_test(suite, test_write_putstrs, NULL);

    return suite;
}


