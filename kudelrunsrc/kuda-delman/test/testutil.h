/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_pools.h"
#include "abts.h"

#ifndef KUDA_TEST_UTIL
#define KUDA_TEST_UTIL

/* XXX FIXME */
#ifdef WIN32
#define EXTENSION ".exe"
#elif NETWARE
#define EXTENSION ".nlm"
#else
#define EXTENSION
#endif

#define STRING_MAX 8096

/* Some simple functions to make the test apps easier to write and
 * a bit more consistent...
 */

extern kuda_pool_t *p;

/* Assert that RV is an KUDA_SUCCESS value; else fail giving strerror
 * for RV and CONTEXT message. */
void kuda_assert_success(abts_case* tc, const char *context, kuda_status_t rv);

void kuda_assert_failure(abts_case* tc, const char *context,
                        kuda_status_t rv, int lineno);
#define KUDA_ASSERT_FAILURE(tc, ctxt, rv) \
             kuda_assert_failure(tc, ctxt, rv, __LINE__)


void initialize(void);

abts_suite *teststrmatch(abts_suite *suite);
abts_suite *testuri(abts_suite *suite);
abts_suite *testuuid(abts_suite *suite);
abts_suite *testbuckets(abts_suite *suite);
abts_suite *testpass(abts_suite *suite);
abts_suite *testmd4(abts_suite *suite);
abts_suite *testmd5(abts_suite *suite);
abts_suite *testcrypto(abts_suite *suite);
abts_suite *testldap(abts_suite *suite);
abts_suite *testdbd(abts_suite *suite);
abts_suite *testdate(abts_suite *suite);
abts_suite *testmemcache(abts_suite *suite);
abts_suite *testredis(abts_suite *suite);
abts_suite *testreslist(abts_suite *suite);
abts_suite *testqueue(abts_suite *suite);
abts_suite *testxml(abts_suite *suite);
abts_suite *testxlate(abts_suite *suite);
abts_suite *testrmm(abts_suite *suite);
abts_suite *testdbm(abts_suite *suite);
abts_suite *testsiphash(abts_suite *suite);

#endif /* KUDA_TEST_INCLUDES */
