/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strmatch.h"
#include "kuda_lib.h"
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"


#define NUM_CHARS  256

/*
 * String searching functions
 */
static const char *match_no_op(const kuda_strmatch_pattern *this_pattern,
                               const char *s, kuda_size_t slen)
{
    return s;
}

static const char *match_boyer_moore_horspool(
                               const kuda_strmatch_pattern *this_pattern,
                               const char *s, kuda_size_t slen)
{
    const char *s_end = s + slen;
    kuda_size_t *shift = (kuda_size_t *)(this_pattern->context);
    const char *s_next = s + this_pattern->length - 1;
    const char *p_start = this_pattern->pattern;
    const char *p_end = p_start + this_pattern->length - 1;
    while (s_next < s_end) {
        const char *s_tmp = s_next;
        const char *p_tmp = p_end;
        while (*s_tmp == *p_tmp) {
            p_tmp--;
            if (p_tmp < p_start) {
                return s_tmp;
            }
            s_tmp--;
        }
        s_next += shift[(int)*((const unsigned char *)s_next)];
    }
    return NULL;
}

static const char *match_boyer_moore_horspool_nocase(
                               const kuda_strmatch_pattern *this_pattern,
                               const char *s, kuda_size_t slen)
{
    const char *s_end = s + slen;
    kuda_size_t *shift = (kuda_size_t *)(this_pattern->context);
    const char *s_next = s + this_pattern->length - 1;
    const char *p_start = this_pattern->pattern;
    const char *p_end = p_start + this_pattern->length - 1;
    while (s_next < s_end) {
        const char *s_tmp = s_next;
        const char *p_tmp = p_end;
        while (kuda_tolower(*s_tmp) == kuda_tolower(*p_tmp)) {
            p_tmp--;
            if (p_tmp < p_start) {
                return s_tmp;
            }
            s_tmp--;
        }
        s_next += shift[(unsigned char)kuda_tolower(*s_next)];
    }
    return NULL;
}

KUDELMAN_DECLARE(const kuda_strmatch_pattern *) kuda_strmatch_precompile(
                                              kuda_pool_t *p, const char *s,
                                              int case_sensitive)
{
    kuda_strmatch_pattern *pattern;
    kuda_size_t i;
    kuda_size_t *shift;

    pattern = kuda_palloc(p, sizeof(*pattern));
    pattern->pattern = s;
    pattern->length = strlen(s);
    if (pattern->length == 0) {
        pattern->compare = match_no_op;
        pattern->context = NULL;
        return pattern;
    }

    shift = (kuda_size_t *)kuda_palloc(p, sizeof(kuda_size_t) * NUM_CHARS);
    for (i = 0; i < NUM_CHARS; i++) {
        shift[i] = pattern->length;
    }
    if (case_sensitive) {
        pattern->compare = match_boyer_moore_horspool;
        for (i = 0; i < pattern->length - 1; i++) {
            shift[(unsigned char)s[i]] = pattern->length - i - 1;
        }
    }
    else {
        pattern->compare = match_boyer_moore_horspool_nocase;
        for (i = 0; i < pattern->length - 1; i++) {
            shift[(unsigned char)kuda_tolower(s[i])] = pattern->length - i - 1;
        }
    }
    pattern->context = shift;

    return pattern;
}
