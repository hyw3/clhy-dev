# Microsoft Developer Studio Generated NMAKE File, Based on kuda_dbd_sqlite2.dsp
!IF "$(CFG)" == ""
CFG=kuda_dbd_sqlite2 - Win32 Release
!MESSAGE No configuration specified. Defaulting to kuda_dbd_sqlite2 - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "kuda_dbd_sqlite2 - Win32 Release" && "$(CFG)" != "kuda_dbd_sqlite2 - Win32 Debug" && "$(CFG)" != "kuda_dbd_sqlite2 - x64 Release" && "$(CFG)" != "kuda_dbd_sqlite2 - x64 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "kuda_dbd_sqlite2.mak" CFG="kuda_dbd_sqlite2 - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "kuda_dbd_sqlite2 - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "kuda_dbd_sqlite2 - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "kuda_dbd_sqlite2 - x64 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "kuda_dbd_sqlite2 - x64 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "kuda_dbd_sqlite2 - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\kuda_dbd_sqlite2-1.dll" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libkudadelman - Win32 Release" "libkuda - Win32 Release" "$(OUTDIR)\kuda_dbd_sqlite2-1.dll" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudadelman - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\kuda_dbd_sqlite2-1.res"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2.obj"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2_src.idb"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2_src.pdb"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.dll"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.exp"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.lib"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../include" /I "../../kuda/include" /I "../include/private" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "KUDELMAN_DSO_CAPI_BUILD" /D KUDELMAN_HAVE_SQLITE2=1 /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\kuda_dbd_sqlite2_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL" 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\kuda_dbd_sqlite2-1.res" /i "../include" /i "../../kuda/include" /d DLL_NAME="kuda_dbd_sqlite2" /d "NDEBUG" /d "KUDELMAN_VERSION_ONLY" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\kuda_dbd_sqlite2.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib sqlite2.lib /nologo /base:"0x6EF10000" /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\kuda_dbd_sqlite2-1.pdb" /debug /out:"$(OUTDIR)\kuda_dbd_sqlite2-1.dll" /implib:"$(OUTDIR)\kuda_dbd_sqlite2-1.lib" /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\kuda_dbd_sqlite2.obj" \
	"$(INTDIR)\kuda_dbd_sqlite2-1.res" \
	"..\..\kuda\Release\libkuda-1.lib" \
	"..\Release\libkudadelman-1.lib"

"$(OUTDIR)\kuda_dbd_sqlite2-1.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\kuda_dbd_sqlite2-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\kuda_dbd_sqlite2-1.dll"
   if exist .\Release\kuda_dbd_sqlite2-1.dll.manifest mt.exe -manifest .\Release\kuda_dbd_sqlite2-1.dll.manifest -outputresource:.\Release\kuda_dbd_sqlite2-1.dll;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "kuda_dbd_sqlite2 - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\kuda_dbd_sqlite2-1.dll" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libkudadelman - Win32 Debug" "libkuda - Win32 Debug" "$(OUTDIR)\kuda_dbd_sqlite2-1.dll" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudadelman - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\kuda_dbd_sqlite2-1.res"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2.obj"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2_src.idb"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2_src.pdb"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.dll"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.exp"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.lib"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../include" /I "../../kuda/include" /I "../include/private" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "KUDELMAN_DSO_CAPI_BUILD" /D KUDELMAN_HAVE_SQLITE2=1 /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\kuda_dbd_sqlite2_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL" 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\kuda_dbd_sqlite2-1.res" /i "../include" /i "../../kuda/include" /d DLL_NAME="kuda_dbd_sqlite2" /d "_DEBUG" /d "KUDELMAN_VERSION_ONLY" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\kuda_dbd_sqlite2.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib sqlite2.lib /nologo /base:"0x6EF10000" /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\kuda_dbd_sqlite2-1.pdb" /debug /out:"$(OUTDIR)\kuda_dbd_sqlite2-1.dll" /implib:"$(OUTDIR)\kuda_dbd_sqlite2-1.lib" 
LINK32_OBJS= \
	"$(INTDIR)\kuda_dbd_sqlite2.obj" \
	"$(INTDIR)\kuda_dbd_sqlite2-1.res" \
	"..\..\kuda\Debug\libkuda-1.lib" \
	"..\Debug\libkudadelman-1.lib"

"$(OUTDIR)\kuda_dbd_sqlite2-1.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\kuda_dbd_sqlite2-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\kuda_dbd_sqlite2-1.dll"
   if exist .\Debug\kuda_dbd_sqlite2-1.dll.manifest mt.exe -manifest .\Debug\kuda_dbd_sqlite2-1.dll.manifest -outputresource:.\Debug\kuda_dbd_sqlite2-1.dll;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "kuda_dbd_sqlite2 - x64 Release"

OUTDIR=.\x64\Release
INTDIR=.\x64\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\x64\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\kuda_dbd_sqlite2-1.dll" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libkudadelman - x64 Release" "libkuda - x64 Release" "$(OUTDIR)\kuda_dbd_sqlite2-1.dll" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - x64 ReleaseCLEAN" "libkudadelman - x64 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\kuda_dbd_sqlite2-1.res"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2.obj"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2_src.idb"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2_src.pdb"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.dll"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.exp"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.lib"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../include" /I "../../kuda/include" /I "../include/private" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "KUDELMAN_DSO_CAPI_BUILD" /D KUDELMAN_HAVE_SQLITE2=1 /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\kuda_dbd_sqlite2_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL" 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\kuda_dbd_sqlite2-1.res" /i "../include" /i "../../kuda/include" /d DLL_NAME="kuda_dbd_sqlite2" /d "NDEBUG" /d "KUDELMAN_VERSION_ONLY" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\kuda_dbd_sqlite2.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib sqlite2.lib /nologo /base:"0x6EF10000" /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\kuda_dbd_sqlite2-1.pdb" /debug /out:"$(OUTDIR)\kuda_dbd_sqlite2-1.dll" /implib:"$(OUTDIR)\kuda_dbd_sqlite2-1.lib" /MACHINE:X64 /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\kuda_dbd_sqlite2.obj" \
	"$(INTDIR)\kuda_dbd_sqlite2-1.res" \
	"..\..\kuda\x64\Release\libkuda-1.lib" \
	"..\x64\Release\libkudadelman-1.lib"

"$(OUTDIR)\kuda_dbd_sqlite2-1.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\x64\Release\kuda_dbd_sqlite2-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\x64\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\kuda_dbd_sqlite2-1.dll"
   if exist .\x64\Release\kuda_dbd_sqlite2-1.dll.manifest mt.exe -manifest .\x64\Release\kuda_dbd_sqlite2-1.dll.manifest -outputresource:.\x64\Release\kuda_dbd_sqlite2-1.dll;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "kuda_dbd_sqlite2 - x64 Debug"

OUTDIR=.\x64\Debug
INTDIR=.\x64\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\x64\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\kuda_dbd_sqlite2-1.dll" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libkudadelman - x64 Debug" "libkuda - x64 Debug" "$(OUTDIR)\kuda_dbd_sqlite2-1.dll" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - x64 DebugCLEAN" "libkudadelman - x64 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\kuda_dbd_sqlite2-1.res"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2.obj"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2_src.idb"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2_src.pdb"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.dll"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.exp"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.lib"
	-@erase "$(OUTDIR)\kuda_dbd_sqlite2-1.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../include" /I "../../kuda/include" /I "../include/private" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "KUDELMAN_DSO_CAPI_BUILD" /D KUDELMAN_HAVE_SQLITE2=1 /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\kuda_dbd_sqlite2_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL" 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\kuda_dbd_sqlite2-1.res" /i "../include" /i "../../kuda/include" /d DLL_NAME="kuda_dbd_sqlite2" /d "_DEBUG" /d "KUDELMAN_VERSION_ONLY" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\kuda_dbd_sqlite2.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib advapi32.lib ws2_32.lib mswsock.lib ole32.lib sqlite2.lib /nologo /base:"0x6EF10000" /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\kuda_dbd_sqlite2-1.pdb" /debug /out:"$(OUTDIR)\kuda_dbd_sqlite2-1.dll" /implib:"$(OUTDIR)\kuda_dbd_sqlite2-1.lib" /MACHINE:X64 
LINK32_OBJS= \
	"$(INTDIR)\kuda_dbd_sqlite2.obj" \
	"$(INTDIR)\kuda_dbd_sqlite2-1.res" \
	"..\..\kuda\x64\Debug\libkuda-1.lib" \
	"..\x64\Debug\libkudadelman-1.lib"

"$(OUTDIR)\kuda_dbd_sqlite2-1.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\x64\Debug\kuda_dbd_sqlite2-1.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\x64\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\kuda_dbd_sqlite2-1.dll"
   if exist .\x64\Debug\kuda_dbd_sqlite2-1.dll.manifest mt.exe -manifest .\x64\Debug\kuda_dbd_sqlite2-1.dll.manifest -outputresource:.\x64\Debug\kuda_dbd_sqlite2-1.dll;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("kuda_dbd_sqlite2.dep")
!INCLUDE "kuda_dbd_sqlite2.dep"
!ELSE 
!MESSAGE Warning: cannot find "kuda_dbd_sqlite2.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "kuda_dbd_sqlite2 - Win32 Release" || "$(CFG)" == "kuda_dbd_sqlite2 - Win32 Debug" || "$(CFG)" == "kuda_dbd_sqlite2 - x64 Release" || "$(CFG)" == "kuda_dbd_sqlite2 - x64 Debug"
SOURCE=.\kuda_dbd_sqlite2.c

"$(INTDIR)\kuda_dbd_sqlite2.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "kuda_dbd_sqlite2 - Win32 Release"

"libkuda - Win32 Release" : 
   cd ".\..\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\kuda-delman\dbd"

"libkuda - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\kuda-delman\dbd"

!ELSEIF  "$(CFG)" == "kuda_dbd_sqlite2 - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd ".\..\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\kuda-delman\dbd"

"libkuda - Win32 DebugCLEAN" : 
   cd ".\..\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\kuda-delman\dbd"

!ELSEIF  "$(CFG)" == "kuda_dbd_sqlite2 - x64 Release"

"libkuda - x64 Release" : 
   cd ".\..\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - x64 Release" 
   cd "..\kuda-delman\dbd"

"libkuda - x64 ReleaseCLEAN" : 
   cd ".\..\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - x64 Release" RECURSE=1 CLEAN 
   cd "..\kuda-delman\dbd"

!ELSEIF  "$(CFG)" == "kuda_dbd_sqlite2 - x64 Debug"

"libkuda - x64 Debug" : 
   cd ".\..\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - x64 Debug" 
   cd "..\kuda-delman\dbd"

"libkuda - x64 DebugCLEAN" : 
   cd ".\..\..\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - x64 Debug" RECURSE=1 CLEAN 
   cd "..\kuda-delman\dbd"

!ENDIF 

!IF  "$(CFG)" == "kuda_dbd_sqlite2 - Win32 Release"

"libkudadelman - Win32 Release" : 
   cd ".\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" 
   cd ".\dbd"

"libkudadelman - Win32 ReleaseCLEAN" : 
   cd ".\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd ".\dbd"

!ELSEIF  "$(CFG)" == "kuda_dbd_sqlite2 - Win32 Debug"

"libkudadelman - Win32 Debug" : 
   cd ".\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" 
   cd ".\dbd"

"libkudadelman - Win32 DebugCLEAN" : 
   cd ".\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd ".\dbd"

!ELSEIF  "$(CFG)" == "kuda_dbd_sqlite2 - x64 Release"

"libkudadelman - x64 Release" : 
   cd ".\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - x64 Release" 
   cd ".\dbd"

"libkudadelman - x64 ReleaseCLEAN" : 
   cd ".\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - x64 Release" RECURSE=1 CLEAN 
   cd ".\dbd"

!ELSEIF  "$(CFG)" == "kuda_dbd_sqlite2 - x64 Debug"

"libkudadelman - x64 Debug" : 
   cd ".\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - x64 Debug" 
   cd ".\dbd"

"libkudadelman - x64 DebugCLEAN" : 
   cd ".\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - x64 Debug" RECURSE=1 CLEAN 
   cd ".\dbd"

!ENDIF 

SOURCE=..\libkudadelman.rc

!IF  "$(CFG)" == "kuda_dbd_sqlite2 - Win32 Release"


"$(INTDIR)\kuda_dbd_sqlite2-1.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\kuda_dbd_sqlite2-1.res" /i "../include" /i "../../kuda/include" /i "\wwhy-1.6.0\kudelrunsrc\kuda-delman" /d DLL_NAME="kuda_dbd_sqlite2" /d "NDEBUG" /d "KUDELMAN_VERSION_ONLY" $(SOURCE)


!ELSEIF  "$(CFG)" == "kuda_dbd_sqlite2 - Win32 Debug"


"$(INTDIR)\kuda_dbd_sqlite2-1.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\kuda_dbd_sqlite2-1.res" /i "../include" /i "../../kuda/include" /i "\wwhy-1.6.0\kudelrunsrc\kuda-delman" /d DLL_NAME="kuda_dbd_sqlite2" /d "_DEBUG" /d "KUDELMAN_VERSION_ONLY" $(SOURCE)


!ELSEIF  "$(CFG)" == "kuda_dbd_sqlite2 - x64 Release"


"$(INTDIR)\kuda_dbd_sqlite2-1.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\kuda_dbd_sqlite2-1.res" /i "../include" /i "../../kuda/include" /i "\wwhy-1.6.0\kudelrunsrc\kuda-delman" /d DLL_NAME="kuda_dbd_sqlite2" /d "NDEBUG" /d "KUDELMAN_VERSION_ONLY" $(SOURCE)


!ELSEIF  "$(CFG)" == "kuda_dbd_sqlite2 - x64 Debug"


"$(INTDIR)\kuda_dbd_sqlite2-1.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\kuda_dbd_sqlite2-1.res" /i "../include" /i "../../kuda/include" /i "\wwhy-1.6.0\kudelrunsrc\kuda-delman" /d DLL_NAME="kuda_dbd_sqlite2" /d "_DEBUG" /d "KUDELMAN_VERSION_ONLY" $(SOURCE)


!ENDIF 


!ENDIF 

