/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kudelman.h"

#if KUDELMAN_HAVE_SQLITE2

#include <ctype.h>
#include <stdlib.h>

#include <sqlite.h>

#include "kuda_strings.h"
#include "kuda_time.h"
#include "kuda_buckets.h"

#include "kuda_dbd_internal.h"

struct kuda_dbd_transaction_t {
    int mode;
    int errnum;
    kuda_dbd_t *handle;
};

struct kuda_dbd_t {
    sqlite *conn;
    char *errmsg;
    kuda_dbd_transaction_t *trans;
};

struct kuda_dbd_results_t {
    int random;
    sqlite *handle;
    char **res;
    size_t ntuples;
    size_t sz;
    size_t index;
    kuda_pool_t *pool;
};

struct kuda_dbd_row_t {
    int n;
    char **data;
    kuda_dbd_results_t *res;
};

struct kuda_dbd_prepared_t {
    const char *name;
    int prepared;
};

#define FREE_ERROR_MSG(dbd) \
	do { \
		if(dbd && dbd->errmsg) { \
			free(dbd->errmsg); \
			dbd->errmsg = NULL; \
		} \
	} while(0);

static kuda_status_t free_table(void *data)
{
    sqlite_free_table(data); 
    return KUDA_SUCCESS;
}

static int dbd_sqlite_select(kuda_pool_t * pool, kuda_dbd_t * sql,
                             kuda_dbd_results_t ** results, const char *query,
                             int seek)
{
    char **result;
    int ret = 0;
    int tuples = 0;
    int fields = 0;

    if (sql->trans && sql->trans->errnum) {
        return sql->trans->errnum;
    }

    FREE_ERROR_MSG(sql);

    ret = sqlite_get_table(sql->conn, query, &result, &tuples, &fields,
                          &sql->errmsg);

    if (ret == SQLITE_OK) {
        if (!*results) {
            *results = kuda_pcalloc(pool, sizeof(kuda_dbd_results_t));
        }

        (*results)->res = result;
        (*results)->ntuples = tuples;
        (*results)->sz = fields;
        (*results)->random = seek;
        (*results)->pool = pool;

        if (tuples > 0)
            kuda_pool_cleanup_register(pool, result, free_table,
                                      kuda_pool_cleanup_null);

        ret = 0;
    }
    else {
        if (TXN_NOTICE_ERRORS(sql->trans)) {
            sql->trans->errnum = ret;
        }
    }

    return ret;
}

static const char *dbd_sqlite_get_name(const kuda_dbd_results_t *res, int n)
{
    if ((n < 0) || (n >= res->sz)) {
        return NULL;
    }

    return res->res[n];
}

static int dbd_sqlite_get_row(kuda_pool_t * pool, kuda_dbd_results_t * res,
                              kuda_dbd_row_t ** rowp, int rownum)
{
    kuda_dbd_row_t *row = *rowp;
    int sequential = ((rownum >= 0) && res->random) ? 0 : 1;

    if (row == NULL) {
        row = kuda_palloc(pool, sizeof(kuda_dbd_row_t));
        *rowp = row;
        row->res = res;
        row->n = sequential ? 0 : rownum - 1;
    }
    else {
        if (sequential) {
            ++row->n;
        }
        else {
            row->n = rownum - 1;
        }
    }

    if (row->n >= res->ntuples) {
        *rowp = NULL;
        kuda_pool_cleanup_run(res->pool, res->res, free_table);
        res->res = NULL;
        return -1;
    }

    /* Pointer magic explanation:
     *      The sqlite result is an array such that the first res->sz elements are 
     *      the column names and each tuple follows afterwards 
     *      ex: (from the sqlite2 documentation)
     SELECT employee_name, login, host FROM users WHERE login LIKE *        'd%';

     nrow = 2
     ncolumn = 3
     result[0] = "employee_name"
     result[1] = "login"
     result[2] = "host"
     result[3] = "dummy"
     result[4] = "No such user"
     result[5] = 0
     result[6] = "D. Richard Hipp"
     result[7] = "drh"
     result[8] = "zadok"
     */

    row->data = res->res + res->sz + (res->sz * row->n);

    return 0;
}

static const char *dbd_sqlite_get_entry(const kuda_dbd_row_t * row, int n)
{
    if ((n < 0) || (n >= row->res->sz)) {
      return NULL;
    }

    return row->data[n];
}

static kuda_status_t dbd_sqlite_datum_get(const kuda_dbd_row_t *row, int n,
                                         kuda_dbd_type_e type, void *data)
{
    if ((n < 0) || (n >= row->res->sz)) {
      return KUDA_EGENERAL;
    }

    if (row->data[n] == NULL) {
        return KUDA_ENOENT;
    }

    switch (type) {
    case KUDA_DBD_TYPE_TINY:
        *(char*)data = atoi(row->data[n]);
        break;
    case KUDA_DBD_TYPE_UTINY:
        *(unsigned char*)data = atoi(row->data[n]);
        break;
    case KUDA_DBD_TYPE_SHORT:
        *(short*)data = atoi(row->data[n]);
        break;
    case KUDA_DBD_TYPE_USHORT:
        *(unsigned short*)data = atoi(row->data[n]);
        break;
    case KUDA_DBD_TYPE_INT:
        *(int*)data = atoi(row->data[n]);
        break;
    case KUDA_DBD_TYPE_UINT:
        *(unsigned int*)data = atoi(row->data[n]);
        break;
    case KUDA_DBD_TYPE_LONG:
        *(long*)data = atol(row->data[n]);
        break;
    case KUDA_DBD_TYPE_ULONG:
        *(unsigned long*)data = atol(row->data[n]);
        break;
    case KUDA_DBD_TYPE_LONGLONG:
        *(kuda_int64_t*)data = kuda_atoi64(row->data[n]);
        break;
    case KUDA_DBD_TYPE_ULONGLONG:
        *(kuda_uint64_t*)data = kuda_atoi64(row->data[n]);
        break;
    case KUDA_DBD_TYPE_FLOAT:
        *(float*)data = atof(row->data[n]);
        break;
    case KUDA_DBD_TYPE_DOUBLE:
        *(double*)data = atof(row->data[n]);
        break;
    case KUDA_DBD_TYPE_STRING:
    case KUDA_DBD_TYPE_TEXT:
    case KUDA_DBD_TYPE_TIME:
    case KUDA_DBD_TYPE_DATE:
    case KUDA_DBD_TYPE_DATETIME:
    case KUDA_DBD_TYPE_TIMESTAMP:
    case KUDA_DBD_TYPE_ZTIMESTAMP:
        *(char**)data = row->data[n];
        break;
    case KUDA_DBD_TYPE_BLOB:
    case KUDA_DBD_TYPE_CLOB:
        {
        kuda_bucket *e;
        kuda_bucket_brigade *b = (kuda_bucket_brigade*)data;

        e = kuda_bucket_pool_create(row->data[n],strlen(row->data[n]),
                                   row->res->pool, b->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(b, e);
        }
        break;
    case KUDA_DBD_TYPE_NULL:
        *(void**)data = NULL;
        break;
    default:
        return KUDA_EGENERAL;
    }

    return KUDA_SUCCESS;
}

static const char *dbd_sqlite_error(kuda_dbd_t * sql, int n)
{
    return sql->errmsg;
}

static int dbd_sqlite_query(kuda_dbd_t * sql, int *nrows, const char *query)
{
    char **result;
    int ret;
    int tuples = 0;
    int fields = 0;

    if (sql->trans && sql->trans->errnum) {
        return sql->trans->errnum;
    }

    FREE_ERROR_MSG(sql);

    ret =
        sqlite_get_table(sql->conn, query, &result, &tuples, &fields,
                         &sql->errmsg);
    if (ret == SQLITE_OK) {
        *nrows = sqlite_changes(sql->conn);

        if (tuples > 0)
            free(result);

        ret = 0;
    }

    if (TXN_NOTICE_ERRORS(sql->trans)) {
        sql->trans->errnum = ret;
    }

    return ret;
}

static kuda_status_t free_mem(void *data)
{
    sqlite_freemem(data);
    return KUDA_SUCCESS;
}

static const char *dbd_sqlite_escape(kuda_pool_t * pool, const char *arg,
                                     kuda_dbd_t * sql)
{
    char *ret = sqlite_mprintf("%q", arg);
    kuda_pool_cleanup_register(pool, ret, free_mem, kuda_pool_cleanup_null);
    return ret;
}

static int dbd_sqlite_prepare(kuda_pool_t * pool, kuda_dbd_t * sql,
                              const char *query, const char *label,
                              int nargs, int nvals, kuda_dbd_type_e *types,
                              kuda_dbd_prepared_t ** statement)
{
    return KUDA_ENOTIMPL;
}

static int dbd_sqlite_pquery(kuda_pool_t * pool, kuda_dbd_t * sql,
                             int *nrows, kuda_dbd_prepared_t * statement,
                             const char **values)
{
    return KUDA_ENOTIMPL;
}

static int dbd_sqlite_pvquery(kuda_pool_t * pool, kuda_dbd_t * sql,
                              int *nrows, kuda_dbd_prepared_t * statement,
                              va_list args)
{
    return KUDA_ENOTIMPL;
}

static int dbd_sqlite_pselect(kuda_pool_t * pool, kuda_dbd_t * sql,
                              kuda_dbd_results_t ** results,
                              kuda_dbd_prepared_t * statement,
                              int seek, const char **values)
{
    return KUDA_ENOTIMPL;
}

static int dbd_sqlite_pvselect(kuda_pool_t * pool, kuda_dbd_t * sql,
                               kuda_dbd_results_t ** results,
                               kuda_dbd_prepared_t * statement, int seek,
                               va_list args)
{
    return KUDA_ENOTIMPL;
}

static int dbd_sqlite_pbquery(kuda_pool_t * pool, kuda_dbd_t * sql,
                              int *nrows, kuda_dbd_prepared_t * statement,
                              const void **values)
{
    return KUDA_ENOTIMPL;
}

static int dbd_sqlite_pvbquery(kuda_pool_t * pool, kuda_dbd_t * sql,
                               int *nrows, kuda_dbd_prepared_t * statement,
                               va_list args)
{
    return KUDA_ENOTIMPL;
}

static int dbd_sqlite_pbselect(kuda_pool_t * pool, kuda_dbd_t * sql,
                               kuda_dbd_results_t ** results,
                               kuda_dbd_prepared_t * statement,
                               int seek, const void **values)
{
    return KUDA_ENOTIMPL;
}

static int dbd_sqlite_pvbselect(kuda_pool_t * pool, kuda_dbd_t * sql,
                                kuda_dbd_results_t ** results,
                                kuda_dbd_prepared_t * statement, int seek,
                                va_list args)
{
    return KUDA_ENOTIMPL;
}

static int dbd_sqlite_start_transaction(kuda_pool_t * pool, kuda_dbd_t * handle,
                                        kuda_dbd_transaction_t ** trans)
{
    int ret, rows;

    ret = dbd_sqlite_query(handle, &rows, "BEGIN TRANSACTION");
    if (ret == 0) {
        if (!*trans) {
            *trans = kuda_pcalloc(pool, sizeof(kuda_dbd_transaction_t));
        }
        (*trans)->handle = handle;
        handle->trans = *trans;
    }
    else {
        ret = -1;
    }
    return ret;
}

static int dbd_sqlite_end_transaction(kuda_dbd_transaction_t * trans)
{
    int rows;
    int ret = -1;               /* no transaction is an error cond */

    if (trans) {
        /* rollback on error or explicit rollback request */
        if (trans->errnum || TXN_DO_ROLLBACK(trans)) {
            trans->errnum = 0;
            ret =
                dbd_sqlite_query(trans->handle, &rows,
                                 "ROLLBACK TRANSACTION");
        }
        else {
            ret =
                dbd_sqlite_query(trans->handle, &rows, "COMMIT TRANSACTION");
        }
        trans->handle->trans = NULL;
    }

    return ret;
}

static int dbd_sqlite_transaction_mode_get(kuda_dbd_transaction_t *trans)
{
    if (!trans)
        return KUDA_DBD_TRANSACTION_COMMIT;

    return trans->mode;
}

static int dbd_sqlite_transaction_mode_set(kuda_dbd_transaction_t *trans,
                                           int mode)
{
    if (!trans)
        return KUDA_DBD_TRANSACTION_COMMIT;

    return trans->mode = (mode & TXN_MODE_BITS);
}

static kuda_status_t error_free(void *data)
{
    free(data);
    return KUDA_SUCCESS;
}

static kuda_dbd_t *dbd_sqlite_open(kuda_pool_t * pool, const char *params_,
                                  const char **error)
{
    kuda_dbd_t *sql;
    sqlite *conn = NULL;
    char *perm;
    int iperms = 600;
    char* params = kuda_pstrdup(pool, params_);
    /* params = "[filename]:[permissions]"
     *    example: "shopping.db:600"
     */

    perm = strstr(params, ":");
    if (perm) {
        *(perm++) = '\x00';     /* split the filename and permissions */

        if (strlen(perm) > 0)
            iperms = atoi(perm);
    }

    if (error) {
        *error = NULL;

        conn = sqlite_open(params, iperms, (char **)error);

        if (*error) {
            kuda_pool_cleanup_register(pool, *error, error_free,
                                      kuda_pool_cleanup_null);
        }
    }
    else {
        conn = sqlite_open(params, iperms, NULL);
    }

    sql = kuda_pcalloc(pool, sizeof(*sql));
    sql->conn = conn;

    return sql;
}

static kuda_status_t dbd_sqlite_close(kuda_dbd_t * handle)
{
    if (handle->conn) {
        sqlite_close(handle->conn);
        handle->conn = NULL;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t dbd_sqlite_check_conn(kuda_pool_t * pool,
                                          kuda_dbd_t * handle)
{
    if (handle->conn == NULL)
        return -1;
    return KUDA_SUCCESS;
}

static int dbd_sqlite_select_db(kuda_pool_t * pool, kuda_dbd_t * handle,
                                const char *name)
{
    return KUDA_ENOTIMPL;
}

static void *dbd_sqlite_native(kuda_dbd_t * handle)
{
    return handle->conn;
}

static int dbd_sqlite_num_cols(kuda_dbd_results_t * res)
{
    return res->sz;
}

static int dbd_sqlite_num_tuples(kuda_dbd_results_t * res)
{
    return res->ntuples;
}

KUDELMAN_CAPI_DECLARE_DATA const kuda_dbd_driver_t kuda_dbd_sqlite2_driver = {
    "sqlite2",
    NULL,
    dbd_sqlite_native,
    dbd_sqlite_open,
    dbd_sqlite_check_conn,
    dbd_sqlite_close,
    dbd_sqlite_select_db,
    dbd_sqlite_start_transaction,
    dbd_sqlite_end_transaction,
    dbd_sqlite_query,
    dbd_sqlite_select,
    dbd_sqlite_num_cols,
    dbd_sqlite_num_tuples,
    dbd_sqlite_get_row,
    dbd_sqlite_get_entry,
    dbd_sqlite_error,
    dbd_sqlite_escape,
    dbd_sqlite_prepare,
    dbd_sqlite_pvquery,
    dbd_sqlite_pvselect,
    dbd_sqlite_pquery,
    dbd_sqlite_pselect,
    dbd_sqlite_get_name,
    dbd_sqlite_transaction_mode_get,
    dbd_sqlite_transaction_mode_set,
    NULL,
    dbd_sqlite_pvbquery,
    dbd_sqlite_pvbselect,
    dbd_sqlite_pbquery,
    dbd_sqlite_pbselect,
    dbd_sqlite_datum_get
};
#endif
