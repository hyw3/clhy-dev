/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ctype.h>
#include <stdio.h>

#include "kudelman_config.h"
#include "kudelman.h"

#include "kuda_pools.h"
#include "kuda_dso.h"
#include "kuda_strings.h"
#include "kuda_hash.h"
#include "kuda_thread_mutex.h"
#include "kuda_lib.h"
#include "kuda_atomic.h"

#include "kudelman_internal.h"
#include "kuda_dbd_internal.h"
#include "kuda_dbd.h"
#include "kudelman_version.h"

static kuda_hash_t *drivers = NULL;
static kuda_uint32_t initialised = 0, in_init = 1;

#define CLEANUP_CAST (kuda_status_t (*)(void*))

#if KUDA_HAS_THREADS
/* deprecated, but required for existing providers.  Existing and new
 * providers should be refactored to use a provider-specific mutex so
 * that different providers do not block one another.
 * In KUDA 1.3 this is no longer used for dso cAPI loading, and
 * kudelman_dso_mutex_[un]lock is used instead.
 * In KUDA 2.0 this should become entirely local to libkudadelman-2.so and
 * no longer be exported.
 */
static kuda_thread_mutex_t* mutex = NULL;
KUDELMAN_DECLARE(kuda_status_t) kuda_dbd_mutex_lock()
{
    return kuda_thread_mutex_lock(mutex);
}
KUDELMAN_DECLARE(kuda_status_t) kuda_dbd_mutex_unlock()
{
    return kuda_thread_mutex_unlock(mutex);
}
#else
KUDELMAN_DECLARE(kuda_status_t) kuda_dbd_mutex_lock() {
    return KUDA_SUCCESS;
}
KUDELMAN_DECLARE(kuda_status_t) kuda_dbd_mutex_unlock() {
    return KUDA_SUCCESS;
}
#endif

#if !KUDELMAN_DSO_BUILD
#define DRIVER_LOAD(name,driver,pool) \
    {   \
        extern const kuda_dbd_driver_t driver; \
        kuda_hash_set(drivers,name,KUDA_HASH_KEY_STRING,&driver); \
        if (driver.init) {     \
            driver.init(pool); \
        }  \
    }
#endif

static kuda_status_t kuda_dbd_term(void *ptr)
{
    /* set drivers to NULL so init can work again */
    drivers = NULL;

    /* Everything else we need is handled by cleanups registered
     * when we created mutexes and loaded DSOs
     */
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_dbd_init(kuda_pool_t *pool)
{
    kuda_status_t ret = KUDA_SUCCESS;
    kuda_pool_t *parent;

    if (kuda_atomic_inc32(&initialised)) {
        kuda_atomic_set32(&initialised, 1); /* prevent wrap-around */

        while (kuda_atomic_read32(&in_init)) /* wait until we get fully inited */
            ;

        return KUDA_SUCCESS;
    }

    /* Top level pool scope, need process-scope lifetime */
    for (parent = kuda_pool_parent_get(pool);
        parent && parent != pool;
        parent = kuda_pool_parent_get(pool))
       pool = parent;
#if KUDELMAN_DSO_BUILD
    /* deprecate in 2.0 - permit implicit initialization */
    kudelman_dso_init(pool);
#endif

    drivers = kuda_hash_make(pool);

#if KUDA_HAS_THREADS
    ret = kuda_thread_mutex_create(&mutex, KUDA_THREAD_MUTEX_DEFAULT, pool);
    /* This already registers a pool cleanup */
#endif

#if !KUDELMAN_DSO_BUILD

    /* Load statically-linked drivers: */
#if KUDELMAN_HAVE_MYSQL
    DRIVER_LOAD("mysql", kuda_dbd_mysql_driver, pool);
#endif
#if KUDELMAN_HAVE_PGSQL
    DRIVER_LOAD("pgsql", kuda_dbd_pgsql_driver, pool);
#endif
#if KUDELMAN_HAVE_SQLITE3
    DRIVER_LOAD("sqlite3", kuda_dbd_sqlite3_driver, pool);
#endif
#if KUDELMAN_HAVE_SQLITE2
    DRIVER_LOAD("sqlite2", kuda_dbd_sqlite2_driver, pool);
#endif
#if KUDELMAN_HAVE_ORACLE
    DRIVER_LOAD("oracle", kuda_dbd_oracle_driver, pool);
#endif
#if KUDELMAN_HAVE_ODBC
    DRIVER_LOAD("odbc", kuda_dbd_odbc_driver, pool);
#endif
#if KUDELMAN_HAVE_SOME_OTHER_BACKEND
    DRIVER_LOAD("firebird", kuda_dbd_other_driver, pool);
#endif
#endif /* KUDELMAN_DSO_BUILD */

    kuda_pool_cleanup_register(pool, NULL, kuda_dbd_term,
                              kuda_pool_cleanup_null);

    kuda_atomic_dec32(&in_init);

    return ret;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_dbd_get_driver(kuda_pool_t *pool, const char *name,
                                             const kuda_dbd_driver_t **driver)
{
#if KUDELMAN_DSO_BUILD
    char capiname[32];
    char symname[34];
    kuda_dso_handle_sym_t symbol;
#endif
    kuda_status_t rv;

#if KUDELMAN_DSO_BUILD
    rv = kudelman_dso_mutex_lock();
    if (rv) {
        return rv;
    }
#endif
    *driver = kuda_hash_get(drivers, name, KUDA_HASH_KEY_STRING);
    if (*driver) {
#if KUDELMAN_DSO_BUILD
        kudelman_dso_mutex_unlock();
#endif
        return KUDA_SUCCESS;
    }

#if KUDELMAN_DSO_BUILD
    /* The driver DSO must have exactly the same lifetime as the
     * drivers hash table; ignore the passed-in pool */
    pool = kuda_hash_pool_get(drivers);

#if defined(NETWARE)
    kuda_snprintf(capiname, sizeof(capiname), "dbd%s.nlm", name);
#elif defined(WIN32) || defined(__CYGWIN__)
    kuda_snprintf(capiname, sizeof(capiname),
                 "kuda_dbd_%s-" KUDELMAN_STRINGIFY(KUDELMAN_MAJOR_VERSION) ".dll", name);
#else
    kuda_snprintf(capiname, sizeof(capiname),
                 "kuda_dbd_%s-" KUDELMAN_STRINGIFY(KUDELMAN_MAJOR_VERSION) ".so", name);
#endif
    kuda_snprintf(symname, sizeof(symname), "kuda_dbd_%s_driver", name);
    rv = kudelman_dso_load(NULL, &symbol, capiname, symname, pool);
    if (rv == KUDA_SUCCESS || rv == KUDA_EINIT) { /* previously loaded?!? */
        *driver = symbol;
        name = kuda_pstrdup(pool, name);
        kuda_hash_set(drivers, name, KUDA_HASH_KEY_STRING, *driver);
        rv = KUDA_SUCCESS;
        if ((*driver)->init) {
            (*driver)->init(pool);
        }
    }
    kudelman_dso_mutex_unlock();

#else /* not builtin and !KUDELMAN_DSO_BUILD => not implemented */
    rv = KUDA_ENOTIMPL;
#endif

    return rv;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_dbd_open_ex(const kuda_dbd_driver_t *driver,
                                          kuda_pool_t *pool, const char *params,
                                          kuda_dbd_t **handle,
                                          const char **error)
{
    kuda_status_t rv;
    *handle = (driver->open)(pool, params, error);
    if (*handle == NULL) {
        return KUDA_EGENERAL;
    }
    rv = kuda_dbd_check_conn(driver, pool, *handle);
    if ((rv != KUDA_SUCCESS) && (rv != KUDA_ENOTIMPL)) {
        /* XXX: rv is KUDA error code, but kuda_dbd_error() takes int! */
        if (error) {
            *error = kuda_dbd_error(driver, *handle, rv);
        }
        kuda_dbd_close(driver, *handle);
        return KUDA_EGENERAL;
    }
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_dbd_open(const kuda_dbd_driver_t *driver,
                                       kuda_pool_t *pool, const char *params,
                                       kuda_dbd_t **handle)
{
    return kuda_dbd_open_ex(driver,pool,params,handle,NULL);
}

KUDELMAN_DECLARE(int) kuda_dbd_transaction_start(const kuda_dbd_driver_t *driver,
                                           kuda_pool_t *pool, kuda_dbd_t *handle,
                                           kuda_dbd_transaction_t **trans)
{
    int ret = driver->start_transaction(pool, handle, trans);
    if (*trans) {
        kuda_pool_cleanup_register(pool, *trans,
                                  CLEANUP_CAST driver->end_transaction,
                                  kuda_pool_cleanup_null);
    }
    return ret;
}

KUDELMAN_DECLARE(int) kuda_dbd_transaction_end(const kuda_dbd_driver_t *driver,
                                         kuda_pool_t *pool,
                                         kuda_dbd_transaction_t *trans)
{
    kuda_pool_cleanup_kill(pool, trans, CLEANUP_CAST driver->end_transaction);
    return driver->end_transaction(trans);
}

KUDELMAN_DECLARE(int) kuda_dbd_transaction_mode_get(const kuda_dbd_driver_t *driver,
                                              kuda_dbd_transaction_t *trans)
{
    return driver->transaction_mode_get(trans);
}

KUDELMAN_DECLARE(int) kuda_dbd_transaction_mode_set(const kuda_dbd_driver_t *driver,
                                              kuda_dbd_transaction_t *trans,
                                              int mode)
{
    return driver->transaction_mode_set(trans, mode);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_dbd_close(const kuda_dbd_driver_t *driver,
                                        kuda_dbd_t *handle)
{
    return driver->close(handle);
}

KUDELMAN_DECLARE(const char*) kuda_dbd_name(const kuda_dbd_driver_t *driver)
{
    return driver->name;
}

KUDELMAN_DECLARE(void*) kuda_dbd_native_handle(const kuda_dbd_driver_t *driver,
                                         kuda_dbd_t *handle)
{
    return driver->native_handle(handle);
}

KUDELMAN_DECLARE(int) kuda_dbd_check_conn(const kuda_dbd_driver_t *driver,
                                    kuda_pool_t *pool,
                                    kuda_dbd_t *handle)
{
    return driver->check_conn(pool, handle);
}

KUDELMAN_DECLARE(int) kuda_dbd_set_dbname(const kuda_dbd_driver_t *driver,
                                    kuda_pool_t *pool,
                                    kuda_dbd_t *handle, const char *name)
{
    return driver->set_dbname(pool,handle,name);
}

KUDELMAN_DECLARE(int) kuda_dbd_query(const kuda_dbd_driver_t *driver,
                               kuda_dbd_t *handle,
                               int *nrows, const char *statement)
{
    return driver->query(handle,nrows,statement);
}

KUDELMAN_DECLARE(int) kuda_dbd_select(const kuda_dbd_driver_t *driver,
                                kuda_pool_t *pool,
                                kuda_dbd_t *handle, kuda_dbd_results_t **res,
                                const char *statement, int random)
{
    return driver->select(pool,handle,res,statement,random);
}

KUDELMAN_DECLARE(int) kuda_dbd_num_cols(const kuda_dbd_driver_t *driver,
                                  kuda_dbd_results_t *res)
{
    return driver->num_cols(res);
}

KUDELMAN_DECLARE(int) kuda_dbd_num_tuples(const kuda_dbd_driver_t *driver,
                                    kuda_dbd_results_t *res)
{
    return driver->num_tuples(res);
}

KUDELMAN_DECLARE(int) kuda_dbd_get_row(const kuda_dbd_driver_t *driver,
                                 kuda_pool_t *pool,
                                 kuda_dbd_results_t *res, kuda_dbd_row_t **row,
                                 int rownum)
{
    return driver->get_row(pool,res,row,rownum);
}

KUDELMAN_DECLARE(const char*) kuda_dbd_get_entry(const kuda_dbd_driver_t *driver,
                                           kuda_dbd_row_t *row, int col)
{
    return driver->get_entry(row,col);
}

KUDELMAN_DECLARE(const char*) kuda_dbd_get_name(const kuda_dbd_driver_t *driver,
                                          kuda_dbd_results_t *res, int col)
{
    return driver->get_name(res,col);
}

KUDELMAN_DECLARE(const char*) kuda_dbd_error(const kuda_dbd_driver_t *driver,
                                       kuda_dbd_t *handle, int errnum)
{
    return driver->error(handle,errnum);
}

KUDELMAN_DECLARE(const char*) kuda_dbd_escape(const kuda_dbd_driver_t *driver,
                                        kuda_pool_t *pool, const char *string,
                                        kuda_dbd_t *handle)
{
    return driver->escape(pool,string,handle);
}

KUDELMAN_DECLARE(int) kuda_dbd_prepare(const kuda_dbd_driver_t *driver,
                                 kuda_pool_t *pool,
                                 kuda_dbd_t *handle, const char *query,
                                 const char *label,
                                 kuda_dbd_prepared_t **statement)
{
    size_t qlen;
    int i, nargs = 0, nvals = 0;
    char *p, *pq;
    const char *q;
    kuda_dbd_type_e *t;

    if (!driver->pformat) {
        return KUDA_ENOTIMPL;
    }

    /* find the number of parameters in the query */
    for (q = query; *q; q++) {
        if (q[0] == '%') {
            if (kuda_isalpha(q[1])) {
                nargs++;
            } else if (q[1] == '%') {
                q++;
            }
        }
    }
    nvals = nargs;

    qlen = strlen(query) +
           nargs * (strlen(driver->pformat) + sizeof(nargs) * 3 + 2) + 1;
    pq = kuda_palloc(pool, qlen);
    t = kuda_pcalloc(pool, sizeof(*t) * nargs);

    for (p = pq, q = query, i = 0; *q; q++) {
        if (q[0] == '%') {
            if (kuda_isalpha(q[1])) {
                switch (q[1]) {
                case 'd': t[i] = KUDA_DBD_TYPE_INT;   break;
                case 'u': t[i] = KUDA_DBD_TYPE_UINT;  break;
                case 'f': t[i] = KUDA_DBD_TYPE_FLOAT; break;
                case 'h':
                    switch (q[2]) {
                    case 'h':
                        switch (q[3]){
                        case 'd': t[i] = KUDA_DBD_TYPE_TINY;  q += 2; break;
                        case 'u': t[i] = KUDA_DBD_TYPE_UTINY; q += 2; break;
                        }
                        break;
                    case 'd': t[i] = KUDA_DBD_TYPE_SHORT;  q++; break;
                    case 'u': t[i] = KUDA_DBD_TYPE_USHORT; q++; break;
                    }
                    break;
                case 'l':
                    switch (q[2]) {
                    case 'l':
                        switch (q[3]){
                        case 'd': t[i] = KUDA_DBD_TYPE_LONGLONG;  q += 2; break;
                        case 'u': t[i] = KUDA_DBD_TYPE_ULONGLONG; q += 2; break;
                        }
                        break;
                    case 'd': t[i] = KUDA_DBD_TYPE_LONG;   q++; break;
                    case 'u': t[i] = KUDA_DBD_TYPE_ULONG;  q++; break;
                    case 'f': t[i] = KUDA_DBD_TYPE_DOUBLE; q++; break;
                    }
                    break;
                case 'p':
                    if (q[2] == 'D') {
                        switch (q[3]) {
                        case 't': t[i] = KUDA_DBD_TYPE_TEXT;       q += 2; break;
                        case 'i': t[i] = KUDA_DBD_TYPE_TIME;       q += 2; break;
                        case 'd': t[i] = KUDA_DBD_TYPE_DATE;       q += 2; break;
                        case 'a': t[i] = KUDA_DBD_TYPE_DATETIME;   q += 2; break;
                        case 's': t[i] = KUDA_DBD_TYPE_TIMESTAMP;  q += 2; break;
                        case 'z': t[i] = KUDA_DBD_TYPE_ZTIMESTAMP; q += 2; break;
                        case 'b': t[i] = KUDA_DBD_TYPE_BLOB;       q += 2; break;
                        case 'c': t[i] = KUDA_DBD_TYPE_CLOB;       q += 2; break;
                        case 'n': t[i] = KUDA_DBD_TYPE_NULL;       q += 2; break;
                        }
                    }
                    break;
                }
                q++;

                switch (t[i]) {
                case KUDA_DBD_TYPE_NONE: /* by default, we expect strings */
                    t[i] = KUDA_DBD_TYPE_STRING;
                    break;
                case KUDA_DBD_TYPE_BLOB:
                case KUDA_DBD_TYPE_CLOB: /* three (3) more values passed in */
                    nvals += 3;
                    break;
                default:
                    break;
                }

                /* insert database specific parameter reference */
                p += kuda_snprintf(p, qlen - (p - pq), driver->pformat, ++i);
            } else if (q[1] == '%') { /* reduce %% to % */
                *p++ = *q++;
            } else {
                *p++ = *q;
            }
        } else {
            *p++ = *q;
        }
    }
    *p = '\0';

    return driver->prepare(pool,handle,pq,label,nargs,nvals,t,statement);
}

KUDELMAN_DECLARE(int) kuda_dbd_pquery(const kuda_dbd_driver_t *driver,
                                kuda_pool_t *pool,
                                kuda_dbd_t *handle, int *nrows,
                                kuda_dbd_prepared_t *statement,
                                int nargs, const char **args)
{
    return driver->pquery(pool,handle,nrows,statement,args);
}

KUDELMAN_DECLARE(int) kuda_dbd_pselect(const kuda_dbd_driver_t *driver,
                                 kuda_pool_t *pool,
                                 kuda_dbd_t *handle, kuda_dbd_results_t **res,
                                 kuda_dbd_prepared_t *statement, int random,
                                 int nargs, const char **args)
{
    return driver->pselect(pool,handle,res,statement,random,args);
}

KUDELMAN_DECLARE_NONSTD(int) kuda_dbd_pvquery(const kuda_dbd_driver_t *driver,
                                        kuda_pool_t *pool,
                                        kuda_dbd_t *handle, int *nrows,
                                        kuda_dbd_prepared_t *statement, ...)
{
    int ret;
    va_list args;
    va_start(args, statement);
    ret = driver->pvquery(pool,handle,nrows,statement,args);
    va_end(args);
    return ret;
}

KUDELMAN_DECLARE_NONSTD(int) kuda_dbd_pvselect(const kuda_dbd_driver_t *driver,
                                         kuda_pool_t *pool, kuda_dbd_t *handle,
                                         kuda_dbd_results_t **res,
                                         kuda_dbd_prepared_t *statement,
                                         int random, ...)
{
    int ret;
    va_list args;
    va_start(args, random);
    ret = driver->pvselect(pool,handle,res,statement,random,args);
    va_end(args);
    return ret;
}

KUDELMAN_DECLARE(int) kuda_dbd_pbquery(const kuda_dbd_driver_t *driver,
                                 kuda_pool_t *pool,
                                 kuda_dbd_t *handle, int *nrows,
                                 kuda_dbd_prepared_t *statement,
                                 const void **args)
{
    return driver->pbquery(pool,handle,nrows,statement,args);
}

KUDELMAN_DECLARE(int) kuda_dbd_pbselect(const kuda_dbd_driver_t *driver,
                                  kuda_pool_t *pool,
                                  kuda_dbd_t *handle, kuda_dbd_results_t **res,
                                  kuda_dbd_prepared_t *statement, int random,
                                  const void **args)
{
    return driver->pbselect(pool,handle,res,statement,random,args);
}

KUDELMAN_DECLARE_NONSTD(int) kuda_dbd_pvbquery(const kuda_dbd_driver_t *driver,
                                         kuda_pool_t *pool,
                                         kuda_dbd_t *handle, int *nrows,
                                         kuda_dbd_prepared_t *statement, ...)
{
    int ret;
    va_list args;
    va_start(args, statement);
    ret = driver->pvbquery(pool,handle,nrows,statement,args);
    va_end(args);
    return ret;
}

KUDELMAN_DECLARE_NONSTD(int) kuda_dbd_pvbselect(const kuda_dbd_driver_t *driver,
                                          kuda_pool_t *pool, kuda_dbd_t *handle,
                                          kuda_dbd_results_t **res,
                                          kuda_dbd_prepared_t *statement,
                                          int random, ...)
{
    int ret;
    va_list args;
    va_start(args, random);
    ret = driver->pvbselect(pool,handle,res,statement,random,args);
    va_end(args);
    return ret;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_dbd_datum_get(const kuda_dbd_driver_t *driver,
                                            kuda_dbd_row_t *row, int col,
                                            kuda_dbd_type_e type, void *data)
{
    return driver->datum_get(row,col,type,data);
}
