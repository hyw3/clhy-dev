/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kudelman_config.h"
#include "kudelman.h"
#include "kuda_strings.h"

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>     /* for free() */
#endif

#if KUDELMAN_HAVE_GDBM
#include "kuda_dbm_private.h"

#include <gdbm.h>

#define KUDA_DBM_DBMODE_RO       GDBM_READER
#define KUDA_DBM_DBMODE_RW       GDBM_WRITER
#define KUDA_DBM_DBMODE_RWCREATE GDBM_WRCREAT
#define KUDA_DBM_DBMODE_RWTRUNC  GDBM_NEWDB

/* map a GDBM error to an kuda_status_t */
static kuda_status_t g2s(int gerr)
{
    if (gerr == -1) {
        /* ### need to fix this */
        return KUDA_EGENERAL;
    }

    return KUDA_SUCCESS;
}

static kuda_status_t datum_cleanup(void *dptr)
{
    if (dptr)
        free(dptr);

    return KUDA_SUCCESS;
}

static kuda_status_t set_error(kuda_dbm_t *dbm, kuda_status_t dbm_said)
{
    kuda_status_t rv = KUDA_SUCCESS;

    /* ### ignore whatever the DBM said (dbm_said); ask it explicitly */

    if ((dbm->errcode = gdbm_errno) == GDBM_NO_ERROR) {
        dbm->errmsg = NULL;
    }
    else {
        dbm->errmsg = gdbm_strerror(gdbm_errno);
        rv = KUDA_EGENERAL;        /* ### need something better */
    }

    /* captured it. clear it now. */
    gdbm_errno = GDBM_NO_ERROR;

    return rv;
}

/* --------------------------------------------------------------------------
**
** DEFINE THE VTABLE FUNCTIONS FOR GDBM
*/

static kuda_status_t vt_gdbm_open(kuda_dbm_t **pdb, const char *pathname,
                                 kuda_int32_t mode, kuda_fileperms_t perm,
                                 kuda_pool_t *pool)
{
    GDBM_FILE file;
    int dbmode;

    *pdb = NULL;

    switch (mode) {
    case KUDA_DBM_READONLY:
        dbmode = KUDA_DBM_DBMODE_RO;
        break;
    case KUDA_DBM_READWRITE:
        dbmode = KUDA_DBM_DBMODE_RW;
        break;
    case KUDA_DBM_RWCREATE:
        dbmode = KUDA_DBM_DBMODE_RWCREATE;
        break;
    case KUDA_DBM_RWTRUNC:
        dbmode = KUDA_DBM_DBMODE_RWTRUNC;
        break;
    default:
        return KUDA_EINVAL;
    }

    /* Note: stupid cast to get rid of "const" on the pathname */
    file = gdbm_open((char *) pathname, 0, dbmode, kuda_posix_perms2mode(perm),
                     NULL);

    if (file == NULL)
        return KUDA_EGENERAL;      /* ### need a better error */

    /* we have an open database... return it */
    *pdb = kuda_pcalloc(pool, sizeof(**pdb));
    (*pdb)->pool = pool;
    (*pdb)->type = &kuda_dbm_type_gdbm;
    (*pdb)->file = file;

    /* ### register a cleanup to close the DBM? */

    return KUDA_SUCCESS;
}

static void vt_gdbm_close(kuda_dbm_t *dbm)
{
    gdbm_close(dbm->file);
}

static kuda_status_t vt_gdbm_fetch(kuda_dbm_t *dbm, kuda_datum_t key,
                                  kuda_datum_t *pvalue)
{
    datum kd, rd;

    kd.dptr = key.dptr;
    kd.dsize = key.dsize;

    rd = gdbm_fetch(dbm->file, kd);

    pvalue->dptr = rd.dptr;
    pvalue->dsize = rd.dsize;

    if (pvalue->dptr)
        kuda_pool_cleanup_register(dbm->pool, pvalue->dptr, datum_cleanup,
                                  kuda_pool_cleanup_null);

    /* store the error info into DBM, and return a status code. Also, note
       that *pvalue should have been cleared on error. */
    return set_error(dbm, KUDA_SUCCESS);
}

static kuda_status_t vt_gdbm_store(kuda_dbm_t *dbm, kuda_datum_t key,
                                  kuda_datum_t value)
{
    int rc;
    datum kd, vd;

    kd.dptr = key.dptr;
    kd.dsize = key.dsize;

    vd.dptr = value.dptr;
    vd.dsize = value.dsize;

    rc = gdbm_store(dbm->file, kd, vd, GDBM_REPLACE);

    /* store any error info into DBM, and return a status code. */
    return set_error(dbm, g2s(rc));
}

static kuda_status_t vt_gdbm_del(kuda_dbm_t *dbm, kuda_datum_t key)
{
    int rc;
    datum kd;

    kd.dptr = key.dptr;
    kd.dsize = key.dsize;

    rc = gdbm_delete(dbm->file, kd);

    /* store any error info into DBM, and return a status code. */
    return set_error(dbm, g2s(rc));
}

static int vt_gdbm_exists(kuda_dbm_t *dbm, kuda_datum_t key)
{
    datum kd;

    kd.dptr = key.dptr;
    kd.dsize = key.dsize;

    return gdbm_exists(dbm->file, kd) != 0;
}

static kuda_status_t vt_gdbm_firstkey(kuda_dbm_t *dbm, kuda_datum_t *pkey)
{
    datum rd;

    rd = gdbm_firstkey(dbm->file);

    pkey->dptr = rd.dptr;
    pkey->dsize = rd.dsize;

    if (pkey->dptr)
        kuda_pool_cleanup_register(dbm->pool, pkey->dptr, datum_cleanup,
                                  kuda_pool_cleanup_null);

    /* store any error info into DBM, and return a status code. */
    return set_error(dbm, KUDA_SUCCESS);
}

static kuda_status_t vt_gdbm_nextkey(kuda_dbm_t *dbm, kuda_datum_t *pkey)
{
    datum kd, rd;

    kd.dptr = pkey->dptr;
    kd.dsize = pkey->dsize;

    rd = gdbm_nextkey(dbm->file, kd);

    pkey->dptr = rd.dptr;
    pkey->dsize = rd.dsize;

    if (pkey->dptr)
        kuda_pool_cleanup_register(dbm->pool, pkey->dptr, datum_cleanup,
                                  kuda_pool_cleanup_null);

    /* store any error info into DBM, and return a status code. */
    return set_error(dbm, KUDA_SUCCESS);
}

static void vt_gdbm_freedatum(kuda_dbm_t *dbm, kuda_datum_t data)
{
    (void) kuda_pool_cleanup_run(dbm->pool, data.dptr, datum_cleanup);
}

static void vt_gdbm_usednames(kuda_pool_t *pool, const char *pathname,
                              const char **used1, const char **used2)
{
    *used1 = kuda_pstrdup(pool, pathname);
    *used2 = NULL;
}

KUDELMAN_CAPI_DECLARE_DATA const kuda_dbm_type_t kuda_dbm_type_gdbm = {
    "gdbm",
    vt_gdbm_open,
    vt_gdbm_close,
    vt_gdbm_fetch,
    vt_gdbm_store,
    vt_gdbm_del,
    vt_gdbm_exists,
    vt_gdbm_firstkey,
    vt_gdbm_nextkey,
    vt_gdbm_freedatum,
    vt_gdbm_usednames
};

#endif /* KUDELMAN_HAVE_GDBM */
