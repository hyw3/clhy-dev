/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strings.h"
#define KUDA_WANT_MEMFUNC
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#include "kudelman_config.h"
#include "kudelman.h"

#if KUDELMAN_HAVE_SDBM

#include "kuda_dbm_private.h"
#include "kuda_sdbm.h"

#define KUDA_DBM_DBMODE_RO       (KUDA_FOPEN_READ | KUDA_FOPEN_BUFFERED)
#define KUDA_DBM_DBMODE_RW       (KUDA_FOPEN_READ | KUDA_FOPEN_WRITE)
#define KUDA_DBM_DBMODE_RWCREATE (KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE)
#define KUDA_DBM_DBMODE_RWTRUNC  (KUDA_FOPEN_READ | KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE | \
                                 KUDA_FOPEN_TRUNCATE)

static kuda_status_t set_error(kuda_dbm_t *dbm, kuda_status_t dbm_said)
{
    dbm->errcode = dbm_said;

    if (dbm_said != KUDA_SUCCESS) {
        dbm->errmsg = kuda_psprintf(dbm->pool, "%pm", &dbm_said);
    } else {
        dbm->errmsg = NULL;
    }

    return dbm_said;
}

/* --------------------------------------------------------------------------
**
** DEFINE THE VTABLE FUNCTIONS FOR SDBM
*/

static kuda_status_t vt_sdbm_open(kuda_dbm_t **pdb, const char *pathname,
                                 kuda_int32_t mode, kuda_fileperms_t perm,
                                 kuda_pool_t *pool)
{
    kuda_sdbm_t *file;
    int dbmode;

    *pdb = NULL;

    switch (mode) {
    case KUDA_DBM_READONLY:
        dbmode = KUDA_DBM_DBMODE_RO;
        break;
    case KUDA_DBM_READWRITE:
        dbmode = KUDA_DBM_DBMODE_RW;
        break;
    case KUDA_DBM_RWCREATE:
        dbmode = KUDA_DBM_DBMODE_RWCREATE;
        break;
    case KUDA_DBM_RWTRUNC:
        dbmode = KUDA_DBM_DBMODE_RWTRUNC;
        break;
    default:
        return KUDA_EINVAL;
    }

    {
        kuda_status_t rv;

        rv = kuda_sdbm_open(&file, pathname, dbmode, perm, pool);
        if (rv != KUDA_SUCCESS)
            return rv;
    }

    /* we have an open database... return it */
    *pdb = kuda_pcalloc(pool, sizeof(**pdb));
    (*pdb)->pool = pool;
    (*pdb)->type = &kuda_dbm_type_sdbm;
    (*pdb)->file = file;

    /* ### register a cleanup to close the DBM? */

    return KUDA_SUCCESS;
}

static void vt_sdbm_close(kuda_dbm_t *dbm)
{
    kuda_sdbm_close(dbm->file);
}

static kuda_status_t vt_sdbm_fetch(kuda_dbm_t *dbm, kuda_datum_t key,
                                  kuda_datum_t *pvalue)
{
    kuda_status_t rv;
    kuda_sdbm_datum_t kd, rd;

    kd.dptr = key.dptr;
    kd.dsize = (int)key.dsize;

    rv = kuda_sdbm_fetch(dbm->file, &rd, kd);

    pvalue->dptr = rd.dptr;
    pvalue->dsize = rd.dsize;

    /* store the error info into DBM, and return a status code. Also, note
       that *pvalue should have been cleared on error. */
    return set_error(dbm, rv);
}

static kuda_status_t vt_sdbm_store(kuda_dbm_t *dbm, kuda_datum_t key,
                                  kuda_datum_t value)
{
    kuda_status_t rv;
    kuda_sdbm_datum_t kd, vd;

    kd.dptr = key.dptr;
    kd.dsize = (int)key.dsize;

    vd.dptr = value.dptr;
    vd.dsize = (int)value.dsize;

    rv = kuda_sdbm_store(dbm->file, kd, vd, KUDA_SDBM_REPLACE);

    /* store any error info into DBM, and return a status code. */
    return set_error(dbm, rv);
}

static kuda_status_t vt_sdbm_del(kuda_dbm_t *dbm, kuda_datum_t key)
{
    kuda_status_t rv;
    kuda_sdbm_datum_t kd;

    kd.dptr = key.dptr;
    kd.dsize = (int)key.dsize;

    rv = kuda_sdbm_delete(dbm->file, kd);

    /* store any error info into DBM, and return a status code. */
    return set_error(dbm, rv);
}

static int vt_sdbm_exists(kuda_dbm_t *dbm, kuda_datum_t key)
{
    int exists;
    kuda_sdbm_datum_t vd, kd;

    kd.dptr = key.dptr;
    kd.dsize = (int)key.dsize;

    if (kuda_sdbm_fetch(dbm->file, &vd, kd) != KUDA_SUCCESS)
        exists = 0;
    else
        exists = vd.dptr != NULL;

    return exists;
}

static kuda_status_t vt_sdbm_firstkey(kuda_dbm_t *dbm, kuda_datum_t *pkey)
{
    kuda_status_t rv;
    kuda_sdbm_datum_t rd;

    rv = kuda_sdbm_firstkey(dbm->file, &rd);

    pkey->dptr = rd.dptr;
    pkey->dsize = rd.dsize;

    /* store any error info into DBM, and return a status code. */
    return set_error(dbm, rv);
}

static kuda_status_t vt_sdbm_nextkey(kuda_dbm_t *dbm, kuda_datum_t *pkey)
{
    kuda_sdbm_datum_t rd;

    (void)kuda_sdbm_nextkey(dbm->file, &rd);

    pkey->dptr = rd.dptr;
    pkey->dsize = rd.dsize;

    /* store any error info into DBM, and return a status code. */
    return set_error(dbm, KUDA_SUCCESS);
}

static void vt_sdbm_freedatum(kuda_dbm_t *dbm, kuda_datum_t data)
{
}

static void vt_sdbm_usednames(kuda_pool_t *pool, const char *pathname,
                              const char **used1, const char **used2)
{
    *used1 = kuda_pstrcat(pool, pathname, KUDA_SDBM_DIRFEXT, NULL);
    *used2 = kuda_pstrcat(pool, pathname, KUDA_SDBM_PAGFEXT, NULL);
}

KUDELMAN_CAPI_DECLARE_DATA const kuda_dbm_type_t kuda_dbm_type_sdbm = {
    "sdbm",
    vt_sdbm_open,
    vt_sdbm_close,
    vt_sdbm_fetch,
    vt_sdbm_store,
    vt_sdbm_del,
    vt_sdbm_exists,
    vt_sdbm_firstkey,
    vt_sdbm_nextkey,
    vt_sdbm_freedatum,
    vt_sdbm_usednames
};

#endif /* KUDELMAN_HAVE_SDBM */
