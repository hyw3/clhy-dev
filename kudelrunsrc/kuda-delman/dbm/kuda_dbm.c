/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_dso.h"
#include "kuda_hash.h"
#include "kuda_errno.h"
#include "kuda_pools.h"
#include "kuda_strings.h"
#define KUDA_WANT_MEMFUNC
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"
#include "kuda_general.h"
#include "kuda_atomic.h"

#include "kudelman_config.h"
#include "kudelman.h"
#include "kudelman_internal.h"
#include "kudelman_version.h"
#include "kuda_dbm_private.h"
#include "kudelman_select_dbm.h"
#include "kuda_dbm.h"
#include "kuda_dbm_private.h"

/* ### note: the setting of DBM_VTABLE will go away once we have multiple
   ### DBMs in here. 
   ### Well, that day is here.  So, do we remove DBM_VTABLE and the old
   ### API entirely?  Oh, what to do.  We need an KUDELMAN_DEFAULT_DBM #define.
   ### Sounds like a job for autoconf. */

#if KUDELMAN_USE_DB
#define DBM_VTABLE kuda_dbm_type_db
#define DBM_NAME   "db"
#elif KUDELMAN_USE_GDBM
#define DBM_VTABLE kuda_dbm_type_gdbm
#define DBM_NAME   "gdbm"
#elif KUDELMAN_USE_NDBM
#define DBM_VTABLE kuda_dbm_type_ndbm
#define DBM_NAME   "ndbm"
#elif KUDELMAN_USE_SDBM
#define DBM_VTABLE kuda_dbm_type_sdbm
#define DBM_NAME   "sdbm"
#else /* Not in the USE_xDBM list above */
#error a DBM implementation was not specified
#endif

#if KUDELMAN_DSO_BUILD

static kuda_hash_t *drivers = NULL;
static kuda_uint32_t initialised = 0, in_init = 1;

static kuda_status_t dbm_term(void *ptr)
{
    /* set drivers to NULL so init can work again */
    drivers = NULL;

    /* Everything else we need is handled by cleanups registered
     * when we created mutexes and loaded DSOs
     */
    return KUDA_SUCCESS;
}

#endif /* KUDELMAN_DSO_BUILD */

static kuda_status_t dbm_open_type(kuda_dbm_type_t const* * vtable,
                                  const char *type, 
                                  kuda_pool_t *pool)
{
#if !KUDELMAN_DSO_BUILD

    *vtable = NULL;
    if (!strcasecmp(type, "default"))     *vtable = &DBM_VTABLE;
#if KUDELMAN_HAVE_DB
    else if (!strcasecmp(type, "db"))     *vtable = &kuda_dbm_type_db;
#endif
    else if (*type && !strcasecmp(type + 1, "dbm")) {
#if KUDELMAN_HAVE_GDBM
        if (*type == 'G' || *type == 'g') *vtable = &kuda_dbm_type_gdbm;
#endif
#if KUDELMAN_HAVE_NDBM
        if (*type == 'N' || *type == 'n') *vtable = &kuda_dbm_type_ndbm;
#endif
#if KUDELMAN_HAVE_SDBM
        if (*type == 'S' || *type == 's') *vtable = &kuda_dbm_type_sdbm;
#endif
        /* avoid empty block */ ;
    }
    if (*vtable)
        return KUDA_SUCCESS;
    return KUDA_ENOTIMPL;

#else /* KUDELMAN_DSO_BUILD */

    char capiname[32];
    char symname[34];
    kuda_dso_handle_sym_t symbol;
    kuda_status_t rv;
    int usertype = 0;

    if (!strcasecmp(type, "default"))        type = DBM_NAME;
    else if (!strcasecmp(type, "db"))        type = "db";
    else if (*type && !strcasecmp(type + 1, "dbm")) {
        if      (*type == 'G' || *type == 'g') type = "gdbm"; 
        else if (*type == 'N' || *type == 'n') type = "ndbm"; 
        else if (*type == 'S' || *type == 's') type = "sdbm"; 
    }
    else usertype = 1;

    if (kuda_atomic_inc32(&initialised)) {
        kuda_atomic_set32(&initialised, 1); /* prevent wrap-around */

        while (kuda_atomic_read32(&in_init)) /* wait until we get fully inited */
            ;
    }
    else {
        kuda_pool_t *parent;

        /* Top level pool scope, need process-scope lifetime */
        for (parent = kuda_pool_parent_get(pool);
             parent && parent != pool;
             parent = kuda_pool_parent_get(pool))
            pool = parent;

        /* deprecate in 2.0 - permit implicit initialization */
        kudelman_dso_init(pool);

        drivers = kuda_hash_make(pool);
        kuda_hash_set(drivers, "sdbm", KUDA_HASH_KEY_STRING, &kuda_dbm_type_sdbm);

        kuda_pool_cleanup_register(pool, NULL, dbm_term,
                                  kuda_pool_cleanup_null);

        kuda_atomic_dec32(&in_init);
    }

    rv = kudelman_dso_mutex_lock();
    if (rv) {
        *vtable = NULL;
        return rv;
    }

    *vtable = kuda_hash_get(drivers, type, KUDA_HASH_KEY_STRING);
    if (*vtable) {
        kudelman_dso_mutex_unlock();
        return KUDA_SUCCESS;
    }

    /* The driver DSO must have exactly the same lifetime as the
     * drivers hash table; ignore the passed-in pool */
    pool = kuda_hash_pool_get(drivers);

#if defined(NETWARE)
    kuda_snprintf(capiname, sizeof(capiname), "dbm%s.nlm", type);
#elif defined(WIN32) || defined (__CYGWIN__)
    kuda_snprintf(capiname, sizeof(capiname),
                 "kuda_dbm_%s-" KUDELMAN_STRINGIFY(KUDELMAN_MAJOR_VERSION) ".dll", type);
#else
    kuda_snprintf(capiname, sizeof(capiname),
                 "kuda_dbm_%s-" KUDELMAN_STRINGIFY(KUDELMAN_MAJOR_VERSION) ".so", type);
#endif
    kuda_snprintf(symname, sizeof(symname), "kuda_dbm_type_%s", type);

    rv = kudelman_dso_load(NULL, &symbol, capiname, symname, pool);
    if (rv == KUDA_SUCCESS || rv == KUDA_EINIT) { /* previously loaded?!? */
        *vtable = symbol;
        if (usertype)
            type = kuda_pstrdup(pool, type);
        kuda_hash_set(drivers, type, KUDA_HASH_KEY_STRING, *vtable);
        rv = KUDA_SUCCESS;
    }
    else
        *vtable = NULL;

    kudelman_dso_mutex_unlock();
    return rv;

#endif /* KUDELMAN_DSO_BUILD */
}

KUDELMAN_DECLARE(kuda_status_t) kuda_dbm_open_ex(kuda_dbm_t **pdb, const char *type, 
                                          const char *pathname, 
                                          kuda_int32_t mode,
                                          kuda_fileperms_t perm,
                                          kuda_pool_t *pool)
{
    kuda_dbm_type_t const* vtable = NULL;
    kuda_status_t rv = dbm_open_type(&vtable, type, pool);

    if (rv == KUDA_SUCCESS) {
        rv = (vtable->open)(pdb, pathname, mode, perm, pool);
    }
    return rv;
} 

KUDELMAN_DECLARE(kuda_status_t) kuda_dbm_open(kuda_dbm_t **pdb, const char *pathname, 
                                       kuda_int32_t mode, kuda_fileperms_t perm,
                                       kuda_pool_t *pool)
{
    return kuda_dbm_open_ex(pdb, DBM_NAME, pathname, mode, perm, pool);
}

KUDELMAN_DECLARE(void) kuda_dbm_close(kuda_dbm_t *dbm)
{
    (*dbm->type->close)(dbm);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_dbm_fetch(kuda_dbm_t *dbm, kuda_datum_t key,
                                        kuda_datum_t *pvalue)
{
    return (*dbm->type->fetch)(dbm, key, pvalue);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_dbm_store(kuda_dbm_t *dbm, kuda_datum_t key,
                                        kuda_datum_t value)
{
    return (*dbm->type->store)(dbm, key, value);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_dbm_delete(kuda_dbm_t *dbm, kuda_datum_t key)
{
    return (*dbm->type->del)(dbm, key);
}

KUDELMAN_DECLARE(int) kuda_dbm_exists(kuda_dbm_t *dbm, kuda_datum_t key)
{
    return (*dbm->type->exists)(dbm, key);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_dbm_firstkey(kuda_dbm_t *dbm, kuda_datum_t *pkey)
{
    return (*dbm->type->firstkey)(dbm, pkey);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_dbm_nextkey(kuda_dbm_t *dbm, kuda_datum_t *pkey)
{
    return (*dbm->type->nextkey)(dbm, pkey);
}

KUDELMAN_DECLARE(void) kuda_dbm_freedatum(kuda_dbm_t *dbm, kuda_datum_t data)
{
    (*dbm->type->freedatum)(dbm, data);
}

KUDELMAN_DECLARE(char *) kuda_dbm_geterror(kuda_dbm_t *dbm, int *errcode,
                                     char *errbuf, kuda_size_t errbufsize)
{
    if (errcode != NULL)
        *errcode = dbm->errcode;

    /* assert: errbufsize > 0 */

    if (dbm->errmsg == NULL)
        *errbuf = '\0';
    else
        (void) kuda_cpystrn(errbuf, dbm->errmsg, errbufsize);
    return errbuf;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_dbm_get_usednames_ex(kuda_pool_t *p, 
                                                   const char *type, 
                                                   const char *pathname,
                                                   const char **used1,
                                                   const char **used2)
{
    kuda_dbm_type_t const* vtable;
    kuda_status_t rv = dbm_open_type(&vtable, type, p);

    if (rv == KUDA_SUCCESS) {
        (vtable->getusednames)(p, pathname, used1, used2);
        return KUDA_SUCCESS;
    }
    return rv;
} 

KUDELMAN_DECLARE(void) kuda_dbm_get_usednames(kuda_pool_t *p,
                                        const char *pathname,
                                        const char **used1,
                                        const char **used2)
{
    kuda_dbm_get_usednames_ex(p, DBM_NAME, pathname, used1, used2); 
}

/* Most DBM libraries take a POSIX mode for creating files.  Don't trust
 * the mode_t type, some platforms may not support it, int is safe.
 */
KUDELMAN_DECLARE(int) kuda_posix_perms2mode(kuda_fileperms_t perm)
{
    int mode = 0;

    mode |= 0700 & (perm >> 2); /* User  is off-by-2 bits */
    mode |= 0070 & (perm >> 1); /* Group is off-by-1 bit */
    mode |= 0007 & (perm);      /* World maps 1 for 1 */
    return mode;
}
