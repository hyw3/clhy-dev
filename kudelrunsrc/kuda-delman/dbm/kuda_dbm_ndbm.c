/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strings.h"

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>     /* for free() */
#endif

#include "kudelman_config.h"
#include "kudelman.h"

#if KUDELMAN_HAVE_NDBM
#include "kuda_dbm_private.h"

#include <ndbm.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define KUDA_DBM_DBMODE_RO       O_RDONLY
#define KUDA_DBM_DBMODE_RW       O_RDWR
#define KUDA_DBM_DBMODE_RWCREATE (O_RDWR|O_CREAT)
#define KUDA_DBM_DBMODE_RWTRUNC  (O_RDWR|O_CREAT|O_TRUNC)

/* map a NDBM error to an kuda_status_t */
static kuda_status_t ndbm2s(int ndbmerr)
{
    if (ndbmerr == -1) {
        /* ### need to fix this */
        return KUDA_EGENERAL;
    }

    return KUDA_SUCCESS;
}

static kuda_status_t set_error(kuda_dbm_t *dbm, kuda_status_t dbm_said)
{
    kuda_status_t rv = KUDA_SUCCESS;

    /* ### ignore whatever the DBM said (dbm_said); ask it explicitly */

    dbm->errmsg = NULL;
    if (dbm_error((DBM*)dbm->file)) {
        dbm->errmsg = NULL;
        rv = KUDA_EGENERAL;        /* ### need something better */
    }

    /* captured it. clear it now. */
    dbm_clearerr((DBM*)dbm->file);

    return rv;
}

/* --------------------------------------------------------------------------
**
** DEFINE THE VTABLE FUNCTIONS FOR NDBM
*/

static kuda_status_t vt_ndbm_open(kuda_dbm_t **pdb, const char *pathname,
                                 kuda_int32_t mode, kuda_fileperms_t perm,
                                 kuda_pool_t *pool)
{
    DBM *file;
    int dbmode;

    *pdb = NULL;

    switch (mode) {
    case KUDA_DBM_READONLY:
        dbmode = KUDA_DBM_DBMODE_RO;
        break;
    case KUDA_DBM_READWRITE:
        dbmode = KUDA_DBM_DBMODE_RW;
        break;
    case KUDA_DBM_RWCREATE:
        dbmode = KUDA_DBM_DBMODE_RWCREATE;
        break;
    case KUDA_DBM_RWTRUNC:
        dbmode = KUDA_DBM_DBMODE_RWTRUNC;
        break;
    default:
        return KUDA_EINVAL;
    }

    {
        file = dbm_open(pathname, dbmode, kuda_posix_perms2mode(perm));
        if (file == NULL)
            return KUDA_EGENERAL;      /* ### need a better error */
    }

    /* we have an open database... return it */
    *pdb = kuda_pcalloc(pool, sizeof(**pdb));
    (*pdb)->pool = pool;
    (*pdb)->type = &kuda_dbm_type_ndbm;
    (*pdb)->file = file;

    /* ### register a cleanup to close the DBM? */

    return KUDA_SUCCESS;
}

static void vt_ndbm_close(kuda_dbm_t *dbm)
{
    dbm_close(dbm->file);
}

static kuda_status_t vt_ndbm_fetch(kuda_dbm_t *dbm, kuda_datum_t key,
                                  kuda_datum_t *pvalue)
{
    datum kd, rd;

    kd.dptr = key.dptr;
    kd.dsize = key.dsize;

    rd = dbm_fetch(dbm->file, kd);

    pvalue->dptr = rd.dptr;
    pvalue->dsize = rd.dsize;

    /* store the error info into DBM, and return a status code. Also, note
       that *pvalue should have been cleared on error. */
    return set_error(dbm, KUDA_SUCCESS);
}

static kuda_status_t vt_ndbm_store(kuda_dbm_t *dbm, kuda_datum_t key,
                                  kuda_datum_t value)
{
    int rc;
    datum kd, vd;

    kd.dptr = key.dptr;
    kd.dsize = key.dsize;

    vd.dptr = value.dptr;
    vd.dsize = value.dsize;

    rc = dbm_store(dbm->file, kd, vd, DBM_REPLACE);

    /* store any error info into DBM, and return a status code. */
    return set_error(dbm, ndbm2s(rc));
}

static kuda_status_t vt_ndbm_del(kuda_dbm_t *dbm, kuda_datum_t key)
{
    int rc;
    datum kd;

    kd.dptr = key.dptr;
    kd.dsize = key.dsize;

    rc = dbm_delete(dbm->file, kd);

    /* store any error info into DBM, and return a status code. */
    return set_error(dbm, ndbm2s(rc));
}

static int vt_ndbm_exists(kuda_dbm_t *dbm, kuda_datum_t key)
{
    datum kd, rd;

    kd.dptr = key.dptr;
    kd.dsize = key.dsize;

    rd = dbm_fetch(dbm->file, kd);

    return rd.dptr != NULL;
}

static kuda_status_t vt_ndbm_firstkey(kuda_dbm_t *dbm, kuda_datum_t *pkey)
{
    datum rd;

    rd = dbm_firstkey(dbm->file);

    pkey->dptr = rd.dptr;
    pkey->dsize = rd.dsize;

    /* store any error info into DBM, and return a status code. */
    return set_error(dbm, KUDA_SUCCESS);
}

static kuda_status_t vt_ndbm_nextkey(kuda_dbm_t *dbm, kuda_datum_t *pkey)
{
    datum kd, rd;

    kd.dptr = pkey->dptr;
    kd.dsize = pkey->dsize;

    rd = dbm_nextkey(dbm->file);

    pkey->dptr = rd.dptr;
    pkey->dsize = rd.dsize;

    /* store any error info into DBM, and return a status code. */
    return set_error(dbm, KUDA_SUCCESS);
}

static void vt_ndbm_freedatum(kuda_dbm_t *dbm, kuda_datum_t data)
{
  /* nothing to do */
}

static void vt_ndbm_usednames(kuda_pool_t *pool, const char *pathname,
                              const char **used1, const char **used2)
{
    *used1 = kuda_pstrdup(pool, pathname);
    *used2 = NULL;
}

KUDELMAN_CAPI_DECLARE_DATA const kuda_dbm_type_t kuda_dbm_type_ndbm = {
    "ndbm",
    vt_ndbm_open,
    vt_ndbm_close,
    vt_ndbm_fetch,
    vt_ndbm_store,
    vt_ndbm_del,
    vt_ndbm_exists,
    vt_ndbm_firstkey,
    vt_ndbm_nextkey,
    vt_ndbm_freedatum,
    vt_ndbm_usednames
};

#endif /* KUDELMAN_HAVE_NDBM  */
