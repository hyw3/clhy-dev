/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * sdbm - ndbm work-alike hashed database library
 * based on Per-Aake Larson's Dynamic Hashing algorithms. BIT 18 (1978).
 * author: oz@nexus.yorku.ca
 * ex-public domain, ported to KUDA for cLHy 1
 * core routines
 */

#include "kuda.h"
#include "kuda_file_io.h"
#include "kuda_strings.h"
#include "kuda_errno.h"
#include "kuda_sdbm.h"

#include "sdbm_tune.h"
#include "sdbm_pair.h"
#include "sdbm_private.h"

#include <string.h>     /* for memset() */
#include <stdlib.h>     /* for malloc() and free() */

/*
 * forward
 */
static int getdbit (kuda_sdbm_t *, long);
static kuda_status_t setdbit(kuda_sdbm_t *, long);
static kuda_status_t getpage(kuda_sdbm_t *db, long, int, int);
static kuda_status_t getnext(kuda_sdbm_datum_t *key, kuda_sdbm_t *db);
static kuda_status_t makroom(kuda_sdbm_t *, long, int);

/*
 * useful macros
 */
#define bad(x)		((x).dptr == NULL || (x).dsize <= 0)
#define exhash(item)	sdbm_hash((item).dptr, (item).dsize)

#define OFF_PAG(off)	(kuda_off_t) (off) * PBLKSIZ
#define OFF_DIR(off)	(kuda_off_t) (off) * DBLKSIZ

static const long masks[] = {
        000000000000, 000000000001, 000000000003, 000000000007,
        000000000017, 000000000037, 000000000077, 000000000177,
        000000000377, 000000000777, 000000001777, 000000003777,
        000000007777, 000000017777, 000000037777, 000000077777,
        000000177777, 000000377777, 000000777777, 000001777777,
        000003777777, 000007777777, 000017777777, 000037777777,
        000077777777, 000177777777, 000377777777, 000777777777,
        001777777777, 003777777777, 007777777777, 017777777777
};

const kuda_sdbm_datum_t sdbm_nullitem = { NULL, 0 };

static kuda_status_t database_cleanup(void *data)
{
    kuda_sdbm_t *db = data;

    /*
     * Can't rely on kuda_sdbm_unlock, since it will merely
     * decrement the refcnt if several locks are held.
     */
    if (db->flags & (SDBM_SHARED_LOCK | SDBM_EXCLUSIVE_LOCK))
        (void) kuda_file_unlock(db->dirf);
    (void) kuda_file_close(db->dirf);
    (void) kuda_file_close(db->pagf);
    free(db);

    return KUDA_SUCCESS;
}

static kuda_status_t prep(kuda_sdbm_t **pdb, const char *dirname, const char *pagname,
                         kuda_int32_t flags, kuda_fileperms_t perms, kuda_pool_t *p)
{
    kuda_sdbm_t *db;
    kuda_status_t status;

    *pdb = NULL;

    db = malloc(sizeof(*db));
    memset(db, 0, sizeof(*db));
    db->pagbno = -1L;

    db->pool = p;

    /*
     * adjust user flags so that WRONLY becomes RDWR, 
     * as required by this package. Also set our internal
     * flag for RDONLY if needed.
     */
    if (!(flags & KUDA_FOPEN_WRITE)) {
        db->flags |= SDBM_RDONLY;
    }

    /*
     * adjust the file open flags so that we handle locking
     * on our own (don't rely on any locking behavior within
     * an kuda_file_t, in case it's ever introduced, and set
     * our own flag.
     */
    if (flags & KUDA_FOPEN_SHARELOCK) {
        db->flags |= SDBM_SHARED;
        flags &= ~KUDA_FOPEN_SHARELOCK;
    }

    flags |= KUDA_FOPEN_BINARY | KUDA_FOPEN_READ;

    /*
     * open the files in sequence, and stat the dirfile.
     * If we fail anywhere, undo everything, return NULL.
     */

    if ((status = kuda_file_open(&db->dirf, dirname, flags, perms, p))
                != KUDA_SUCCESS)
        goto error;

    if ((status = kuda_file_open(&db->pagf, pagname, flags, perms, p))
                != KUDA_SUCCESS)
        goto error;

    if ((status = kuda_sdbm_lock(db, (db->flags & SDBM_RDONLY) 
                                        ? KUDA_FLOCK_SHARED
                                        : KUDA_FLOCK_EXCLUSIVE))
                != KUDA_SUCCESS)
        goto error;

    /* kuda_pcalloc zeroed the buffers
     * kuda_sdbm_lock stated the dirf->size and invalidated the cache
     */

    /*
     * if we are opened in SHARED mode, unlock ourself 
     */
    if (db->flags & SDBM_SHARED)
        if ((status = kuda_sdbm_unlock(db)) != KUDA_SUCCESS)
            goto error;

    /* make sure that we close the database at some point */
    kuda_pool_cleanup_register(p, db, database_cleanup, kuda_pool_cleanup_null);

    /* Done! */
    *pdb = db;
    return KUDA_SUCCESS;

error:
    if (db->dirf && db->pagf)
        (void) kuda_sdbm_unlock(db);
    if (db->dirf != NULL)
        (void) kuda_file_close(db->dirf);
    if (db->pagf != NULL) {
        (void) kuda_file_close(db->pagf);
    }
    free(db);
    return status;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_open(kuda_sdbm_t **db, const char *file, 
                                        kuda_int32_t flags, 
                                        kuda_fileperms_t perms, kuda_pool_t *p)
{
    char *dirname = kuda_pstrcat(p, file, KUDA_SDBM_DIRFEXT, NULL);
    char *pagname = kuda_pstrcat(p, file, KUDA_SDBM_PAGFEXT, NULL);
    
    return prep(db, dirname, pagname, flags, perms, p);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_close(kuda_sdbm_t *db)
{
    return kuda_pool_cleanup_run(db->pool, db, database_cleanup);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_fetch(kuda_sdbm_t *db, kuda_sdbm_datum_t *val,
                                         kuda_sdbm_datum_t key)
{
    kuda_status_t status;
    
    if (db == NULL || bad(key))
        return KUDA_EINVAL;

    if ((status = kuda_sdbm_lock(db, KUDA_FLOCK_SHARED)) != KUDA_SUCCESS)
        return status;

    if ((status = getpage(db, exhash(key), 0, 1)) == KUDA_SUCCESS) {
        *val = getpair(db->pagbuf, key);
        /* ### do we want a not-found result? */
    }

    (void) kuda_sdbm_unlock(db);

    return status;
}

static kuda_status_t write_page(kuda_sdbm_t *db, const char *buf, long pagno)
{
    kuda_status_t status;
    kuda_off_t off = OFF_PAG(pagno);
    
    if ((status = kuda_file_seek(db->pagf, KUDA_SET, &off)) == KUDA_SUCCESS)
        status = kuda_file_write_full(db->pagf, buf, PBLKSIZ, NULL);

    return status;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_delete(kuda_sdbm_t *db, 
                                          const kuda_sdbm_datum_t key)
{
    kuda_status_t status;
    
    if (db == NULL || bad(key))
        return KUDA_EINVAL;
    if (kuda_sdbm_rdonly(db))
        return KUDA_EINVAL;
    
    if ((status = kuda_sdbm_lock(db, KUDA_FLOCK_EXCLUSIVE)) != KUDA_SUCCESS)
        return status;

    if ((status = getpage(db, exhash(key), 0, 1)) == KUDA_SUCCESS) {
        if (!delpair(db->pagbuf, key))
            /* ### should we define some KUDADELMAN codes? */
            status = KUDA_EGENERAL;
        else
            status = write_page(db, db->pagbuf, db->pagbno);
    }

    (void) kuda_sdbm_unlock(db);

    return status;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_store(kuda_sdbm_t *db, kuda_sdbm_datum_t key,
                                         kuda_sdbm_datum_t val, int flags)
{
    int need;
    register long hash;
    kuda_status_t status;
    
    if (db == NULL || bad(key))
        return KUDA_EINVAL;
    if (kuda_sdbm_rdonly(db))
        return KUDA_EINVAL;
    need = key.dsize + val.dsize;
    /*
     * is the pair too big (or too small) for this database ??
     */
    if (need < 0 || need > PAIRMAX)
        return KUDA_EINVAL;

    if ((status = kuda_sdbm_lock(db, KUDA_FLOCK_EXCLUSIVE)) != KUDA_SUCCESS)
        return status;

    if ((status = getpage(db, (hash = exhash(key)), 0, 1)) == KUDA_SUCCESS) {

        /*
         * if we need to replace, delete the key/data pair
         * first. If it is not there, ignore.
         */
        if (flags == KUDA_SDBM_REPLACE)
            (void) delpair(db->pagbuf, key);
        else if (!(flags & KUDA_SDBM_INSERTDUP) && duppair(db->pagbuf, key)) {
            status = KUDA_EEXIST;
            goto error;
        }
        /*
         * if we do not have enough room, we have to split.
         */
        if (!fitpair(db->pagbuf, need))
            if ((status = makroom(db, hash, need)) != KUDA_SUCCESS)
                goto error;
        /*
         * we have enough room or split is successful. insert the key,
         * and update the page file.
         */
        (void) putpair(db->pagbuf, key, val);

        status = write_page(db, db->pagbuf, db->pagbno);
    }

error:
    (void) kuda_sdbm_unlock(db);    

    return status;
}

/*
 * makroom - make room by splitting the overfull page
 * this routine will attempt to make room for SPLTMAX times before
 * giving up.
 */
static kuda_status_t makroom(kuda_sdbm_t *db, long hash, int need)
{
    long newp;
    char twin[PBLKSIZ];
    char *pag = db->pagbuf;
    char *new = twin;
    register int smax = SPLTMAX;
    kuda_status_t status;

    do {
        /*
         * split the current page
         */
        (void) splpage(pag, new, db->hmask + 1);
        /*
         * address of the new page
         */
        newp = (hash & db->hmask) | (db->hmask + 1);

        /*
         * write delay, read avoidence/cache shuffle:
         * select the page for incoming pair: if key is to go to the new page,
         * write out the previous one, and copy the new one over, thus making
         * it the current page. If not, simply write the new page, and we are
         * still looking at the page of interest. current page is not updated
         * here, as sdbm_store will do so, after it inserts the incoming pair.
         */
        if (hash & (db->hmask + 1)) {
            if ((status = write_page(db, db->pagbuf, db->pagbno)) 
                        != KUDA_SUCCESS)
                return status;
                    
            db->pagbno = newp;
            (void) memcpy(pag, new, PBLKSIZ);
        }
        else {
            if ((status = write_page(db, new, newp)) != KUDA_SUCCESS)
                return status;
        }

        if ((status = setdbit(db, db->curbit)) != KUDA_SUCCESS)
            return status;
        /*
         * see if we have enough room now
         */
        if (fitpair(pag, need))
            return KUDA_SUCCESS;
        /*
         * try again... update curbit and hmask as getpage would have
         * done. because of our update of the current page, we do not
         * need to read in anything. BUT we have to write the current
         * [deferred] page out, as the window of failure is too great.
         */
        db->curbit = 2 * db->curbit
                   + ((hash & (db->hmask + 1)) ? 2 : 1);
        db->hmask |= db->hmask + 1;
            
        if ((status = write_page(db, db->pagbuf, db->pagbno))
                    != KUDA_SUCCESS)
            return status;
            
    } while (--smax);

    /*
     * if we are here, this is real bad news. After SPLTMAX splits,
     * we still cannot fit the key. say goodnight.
     */
#if 0
    (void) write(2, "sdbm: cannot insert after SPLTMAX attempts.\n", 44);
#endif
    /* ### ENOSPC not really appropriate but better than nothing */
    return KUDA_ENOSPC;

}

/* Reads 'len' bytes from file 'f' at offset 'off' into buf.
 * 'off' is given relative to the start of the file.
 * If 'create' is asked and EOF is returned while reading, this is taken
 * as success (i.e. a cleared buffer is returned).
 */
static kuda_status_t read_from(kuda_file_t *f, void *buf, 
                              kuda_off_t off, kuda_size_t len,
                              int create)
{
    kuda_status_t status;

    if ((status = kuda_file_seek(f, KUDA_SET, &off)) != KUDA_SUCCESS ||
        ((status = kuda_file_read_full(f, buf, len, NULL)) != KUDA_SUCCESS)) {
        /* if EOF is reached, pretend we read all zero's */
        if (status == KUDA_EOF && create) {
            memset(buf, 0, len);
            status = KUDA_SUCCESS;
        }
    }

    return status;
}

/*
 * the following two routines will break if
 * deletions aren't taken into account. (ndbm bug)
 */
KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_firstkey(kuda_sdbm_t *db, 
                                            kuda_sdbm_datum_t *key)
{
    kuda_status_t status;
    
    if ((status = kuda_sdbm_lock(db, KUDA_FLOCK_SHARED)) != KUDA_SUCCESS)
        return status;

    /*
     * start at page 0
     */
    if ((status = getpage(db, 0, 1, 1)) == KUDA_SUCCESS) {
        db->blkptr = 0;
        db->keyptr = 0;
        status = getnext(key, db);
    }

    (void) kuda_sdbm_unlock(db);

    return status;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_nextkey(kuda_sdbm_t *db, 
                                           kuda_sdbm_datum_t *key)
{
    kuda_status_t status;
    
    if ((status = kuda_sdbm_lock(db, KUDA_FLOCK_SHARED)) != KUDA_SUCCESS)
        return status;

    status = getnext(key, db);

    (void) kuda_sdbm_unlock(db);

    return status;
}

/*
 * all important binary tree traversal
 */
static kuda_status_t getpage(kuda_sdbm_t *db, long hash, int by_num, int create)
{
    kuda_status_t status;
    register long pagb;

    if (by_num) {
        pagb = hash;
    }
    else {
        register int hbit = 0;
        register long dbit = 0;

        while (dbit < db->maxbno && getdbit(db, dbit))
            dbit = 2 * dbit + ((hash & (1 << hbit++)) ? 2 : 1);
        debug(("dbit: %d...", dbit));

        db->curbit = dbit;
        db->hmask = masks[hbit];

        pagb = hash & db->hmask;
    }

    /*
     * see if the block we need is already in memory.
     * note: this lookaside cache has about 10% hit rate.
     */
    if (pagb != db->pagbno) { 
        /*
         * note: here, we assume a "hole" is read as 0s.
         * if not, must zero pagbuf first.
         * ### joe: this assumption was surely never correct? but
         * ### we make it so in read_from anyway.
         */
        if ((status = read_from(db->pagf, db->pagbuf,
                                OFF_PAG(pagb), PBLKSIZ,
                                create)) != KUDA_SUCCESS)
            return status;

        if (!chkpage(db->pagbuf))
            return KUDA_ENOSPC; /* ### better error? */

        db->pagbno = pagb;

        debug(("pag read: %d\n", pagb));
    }
    return KUDA_SUCCESS;
}

static int getdbit(kuda_sdbm_t *db, long dbit)
{
    register long c;
    register long dirb;

    c = dbit / BYTESIZ;
    dirb = c / DBLKSIZ;

    if (dirb != db->dirbno) {
        if (read_from(db->dirf, db->dirbuf,
                      OFF_DIR(dirb), DBLKSIZ,
                      1) != KUDA_SUCCESS)
            return 0;

        db->dirbno = dirb;

        debug(("dir read: %d\n", dirb));
    }

    return db->dirbuf[c % DBLKSIZ] & (1 << dbit % BYTESIZ);
}

static kuda_status_t setdbit(kuda_sdbm_t *db, long dbit)
{
    register long c;
    register long dirb;
    kuda_status_t status;
    kuda_off_t off;

    c = dbit / BYTESIZ;
    dirb = c / DBLKSIZ;

    if (dirb != db->dirbno) {
        if ((status = read_from(db->dirf, db->dirbuf,
                                OFF_DIR(dirb), DBLKSIZ,
                                1)) != KUDA_SUCCESS)
            return status;

        db->dirbno = dirb;
        
        debug(("dir read: %d\n", dirb));
    }

    db->dirbuf[c % DBLKSIZ] |= (1 << dbit % BYTESIZ);

    if (dbit >= db->maxbno)
        db->maxbno += DBLKSIZ * BYTESIZ;

    off = OFF_DIR(dirb);
    if ((status = kuda_file_seek(db->dirf, KUDA_SET, &off)) == KUDA_SUCCESS)
        status = kuda_file_write_full(db->dirf, db->dirbuf, DBLKSIZ, NULL);

    return status;
}

/*
* getnext - get the next key in the page, and if done with
* the page, try the next page in sequence
*/
static kuda_status_t getnext(kuda_sdbm_datum_t *key, kuda_sdbm_t *db)
{
    kuda_status_t status;
    for (;;) {
        db->keyptr++;
        *key = getnkey(db->pagbuf, db->keyptr);
        if (key->dptr != NULL)
            return KUDA_SUCCESS;
        /*
         * we either run out, or there is nothing on this page..
         * try the next one... If we lost our position on the
         * file, we will have to seek.
         */
        db->blkptr++;
        db->keyptr = 0;

        /* ### EOF acceptable here too? */
        if ((status = getpage(db, db->blkptr, 1, 0)) != KUDA_SUCCESS)
            return status;
    }

    /* NOTREACHED */
}


KUDELMAN_DECLARE(int) kuda_sdbm_rdonly(kuda_sdbm_t *db)
{
    /* ### Should we return true if the first lock is a share lock,
     *     to reflect that kuda_sdbm_store and kuda_sdbm_delete will fail?
     */
    return (db->flags & SDBM_RDONLY) != 0;
}

