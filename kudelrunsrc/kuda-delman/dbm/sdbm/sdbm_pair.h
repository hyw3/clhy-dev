/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SDBM_PAIR_H
#define SDBM_PAIR_H

/* Mini EMBED (pair.c) */
#define chkpage kudelman__sdbm_chkpage
#define delpair kudelman__sdbm_delpair
#define duppair kudelman__sdbm_duppair
#define fitpair kudelman__sdbm_fitpair
#define getnkey kudelman__sdbm_getnkey
#define getpair kudelman__sdbm_getpair
#define putpair kudelman__sdbm_putpair
#define splpage kudelman__sdbm_splpage

int fitpair(char *, int);
void  putpair(char *, kuda_sdbm_datum_t, kuda_sdbm_datum_t);
kuda_sdbm_datum_t getpair(char *, kuda_sdbm_datum_t);
int  delpair(char *, kuda_sdbm_datum_t);
int  chkpage (char *);
kuda_sdbm_datum_t getnkey(char *, int);
void splpage(char *, char *, long);
int duppair(char *, kuda_sdbm_datum_t);

#endif /* SDBM_PAIR_H */

