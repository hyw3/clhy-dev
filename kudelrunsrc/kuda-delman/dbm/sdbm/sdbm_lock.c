/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_file_info.h"
#include "kuda_file_io.h"
#include "kuda_sdbm.h"

#include "sdbm_private.h"
#include "sdbm_tune.h"

/* NOTE: this function may block until it acquires the lock */
KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_lock(kuda_sdbm_t *db, int type)
{
    kuda_status_t status;
    int lock_type = type & KUDA_FLOCK_TYPEMASK;

    if (!(lock_type == KUDA_FLOCK_SHARED || lock_type == KUDA_FLOCK_EXCLUSIVE))
        return KUDA_EINVAL;

    if (db->flags & SDBM_EXCLUSIVE_LOCK) {
        ++db->lckcnt;
        return KUDA_SUCCESS;
    }
    else if (db->flags & SDBM_SHARED_LOCK) {
        /*
         * Cannot promote a shared lock to an exlusive lock
         * in a cross-platform compatibile manner.
         */
        if (type == KUDA_FLOCK_EXCLUSIVE)
            return KUDA_EINVAL;
        ++db->lckcnt;
        return KUDA_SUCCESS;
    }
    /*
     * zero size: either a fresh database, or one with a single,
     * unsplit data page: dirpage is all zeros.
     */
    if ((status = kuda_file_lock(db->dirf, type)) == KUDA_SUCCESS) 
    {
        kuda_finfo_t finfo;
        if ((status = kuda_file_info_get(&finfo, KUDA_FINFO_SIZE, db->dirf))
                != KUDA_SUCCESS) {
            (void) kuda_file_unlock(db->dirf);
            return status;
        }

        SDBM_INVALIDATE_CACHE(db, finfo);

        ++db->lckcnt;
        if (type == KUDA_FLOCK_SHARED)
            db->flags |= SDBM_SHARED_LOCK;
        else if (type == KUDA_FLOCK_EXCLUSIVE)
            db->flags |= SDBM_EXCLUSIVE_LOCK;
    }
    return status;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_sdbm_unlock(kuda_sdbm_t *db)
{
    if (!(db->flags & (SDBM_SHARED_LOCK | SDBM_EXCLUSIVE_LOCK)))
        return KUDA_EINVAL;
    if (--db->lckcnt > 0)
        return KUDA_SUCCESS;
    db->flags &= ~(SDBM_SHARED_LOCK | SDBM_EXCLUSIVE_LOCK);
    return kuda_file_unlock(db->dirf);
}
