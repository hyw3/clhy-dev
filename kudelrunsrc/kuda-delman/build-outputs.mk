# DO NOT EDIT. AUTOMATICALLY GENERATED.

buckets/kuda_brigade.lo: buckets/kuda_brigade.c .make.dirs include/kuda_buckets.h
buckets/kuda_buckets.lo: buckets/kuda_buckets.c .make.dirs include/kuda_buckets.h
buckets/kuda_buckets_alloc.lo: buckets/kuda_buckets_alloc.c .make.dirs include/kuda_buckets.h
buckets/kuda_buckets_eos.lo: buckets/kuda_buckets_eos.c .make.dirs include/kuda_buckets.h
buckets/kuda_buckets_file.lo: buckets/kuda_buckets_file.c .make.dirs include/kuda_buckets.h
buckets/kuda_buckets_flush.lo: buckets/kuda_buckets_flush.c .make.dirs include/kuda_buckets.h
buckets/kuda_buckets_heap.lo: buckets/kuda_buckets_heap.c .make.dirs include/kuda_buckets.h
buckets/kuda_buckets_mmap.lo: buckets/kuda_buckets_mmap.c .make.dirs include/kuda_buckets.h
buckets/kuda_buckets_pipe.lo: buckets/kuda_buckets_pipe.c .make.dirs include/kuda_buckets.h
buckets/kuda_buckets_pool.lo: buckets/kuda_buckets_pool.c .make.dirs include/kuda_buckets.h
buckets/kuda_buckets_refcount.lo: buckets/kuda_buckets_refcount.c .make.dirs include/kuda_buckets.h
buckets/kuda_buckets_simple.lo: buckets/kuda_buckets_simple.c .make.dirs include/kuda_buckets.h
buckets/kuda_buckets_socket.lo: buckets/kuda_buckets_socket.c .make.dirs include/kuda_buckets.h
crypto/kuda_crypto.lo: crypto/kuda_crypto.c .make.dirs include/kuda_crypto.h include/kudelman_errno.h include/kudelman_version.h include/private/kuda_crypto_internal.h include/private/kudelman_internal.h
crypto/kuda_md4.lo: crypto/kuda_md4.c .make.dirs include/kuda_md4.h include/kuda_xlate.h
crypto/kuda_md5.lo: crypto/kuda_md5.c .make.dirs include/kuda_md5.h include/kuda_xlate.h
crypto/kuda_passwd.lo: crypto/kuda_passwd.c .make.dirs include/kuda_md5.h include/kuda_sha1.h include/kuda_xlate.h
crypto/kuda_sha1.lo: crypto/kuda_sha1.c .make.dirs include/kuda_base64.h include/kuda_sha1.h include/kuda_xlate.h
crypto/kuda_siphash.lo: crypto/kuda_siphash.c .make.dirs include/kuda_siphash.h
crypto/crypt_blowfish.lo: crypto/crypt_blowfish.c .make.dirs 
crypto/getuuid.lo: crypto/getuuid.c .make.dirs include/kuda_md5.h include/kuda_uuid.h include/kuda_xlate.h
crypto/uuid.lo: crypto/uuid.c .make.dirs include/kuda_uuid.h
dbd/kuda_dbd.lo: dbd/kuda_dbd.c .make.dirs include/kuda_dbd.h include/kudelman_version.h include/private/kuda_dbd_internal.h include/private/kudelman_internal.h
dbm/kuda_dbm.lo: dbm/kuda_dbm.c .make.dirs include/kuda_dbm.h include/kudelman_version.h include/private/kuda_dbm_private.h include/private/kudelman_internal.h
dbm/kuda_dbm_sdbm.lo: dbm/kuda_dbm_sdbm.c .make.dirs include/kuda_dbm.h include/kuda_sdbm.h include/private/kuda_dbm_private.h
dbm/sdbm/sdbm.lo: dbm/sdbm/sdbm.c .make.dirs include/kuda_sdbm.h
dbm/sdbm/sdbm_hash.lo: dbm/sdbm/sdbm_hash.c .make.dirs include/kuda_sdbm.h
dbm/sdbm/sdbm_lock.lo: dbm/sdbm/sdbm_lock.c .make.dirs include/kuda_sdbm.h
dbm/sdbm/sdbm_pair.lo: dbm/sdbm/sdbm_pair.c .make.dirs include/kuda_sdbm.h
encoding/kuda_base64.lo: encoding/kuda_base64.c .make.dirs include/kuda_base64.h include/kuda_xlate.h
hooks/kuda_hooks.lo: hooks/kuda_hooks.c .make.dirs include/kuda_hooks.h include/kuda_optional.h include/kuda_optional_hooks.h
ldap/kuda_ldap_stub.lo: ldap/kuda_ldap_stub.c .make.dirs include/kudelman_version.h include/private/kudelman_internal.h
ldap/kuda_ldap_url.lo: ldap/kuda_ldap_url.c .make.dirs 
memcache/kuda_memcache.lo: memcache/kuda_memcache.c .make.dirs include/kuda_buckets.h include/kuda_memcache.h include/kuda_reslist.h
misc/kuda_date.lo: misc/kuda_date.c .make.dirs include/kuda_date.h
misc/kuda_queue.lo: misc/kuda_queue.c .make.dirs include/kuda_queue.h
misc/kuda_reslist.lo: misc/kuda_reslist.c .make.dirs include/kuda_reslist.h
misc/kuda_rmm.lo: misc/kuda_rmm.c .make.dirs include/kuda_anylock.h include/kuda_rmm.h
misc/kuda_thread_pool.lo: misc/kuda_thread_pool.c .make.dirs include/kuda_thread_pool.h
misc/kudelman_dso.lo: misc/kudelman_dso.c .make.dirs include/kudelman_version.h include/private/kudelman_internal.h
misc/kudelman_version.lo: misc/kudelman_version.c .make.dirs include/kudelman_version.h
redis/kuda_redis.lo: redis/kuda_redis.c .make.dirs include/kuda_buckets.h include/kuda_redis.h include/kuda_reslist.h
strmatch/kuda_strmatch.lo: strmatch/kuda_strmatch.c .make.dirs include/kuda_strmatch.h
uri/kuda_uri.lo: uri/kuda_uri.c .make.dirs include/kuda_uri.h
xlate/xlate.lo: xlate/xlate.c .make.dirs include/kuda_xlate.h
xml/kuda_xml.lo: xml/kuda_xml.c .make.dirs include/kuda_xlate.h include/kuda_xml.h

OBJECTS_all = buckets/kuda_brigade.lo buckets/kuda_buckets.lo buckets/kuda_buckets_alloc.lo buckets/kuda_buckets_eos.lo buckets/kuda_buckets_file.lo buckets/kuda_buckets_flush.lo buckets/kuda_buckets_heap.lo buckets/kuda_buckets_mmap.lo buckets/kuda_buckets_pipe.lo buckets/kuda_buckets_pool.lo buckets/kuda_buckets_refcount.lo buckets/kuda_buckets_simple.lo buckets/kuda_buckets_socket.lo crypto/kuda_crypto.lo crypto/kuda_md4.lo crypto/kuda_md5.lo crypto/kuda_passwd.lo crypto/kuda_sha1.lo crypto/kuda_siphash.lo crypto/crypt_blowfish.lo crypto/getuuid.lo crypto/uuid.lo dbd/kuda_dbd.lo dbm/kuda_dbm.lo dbm/kuda_dbm_sdbm.lo dbm/sdbm/sdbm.lo dbm/sdbm/sdbm_hash.lo dbm/sdbm/sdbm_lock.lo dbm/sdbm/sdbm_pair.lo encoding/kuda_base64.lo hooks/kuda_hooks.lo ldap/kuda_ldap_stub.lo ldap/kuda_ldap_url.lo memcache/kuda_memcache.lo misc/kuda_date.lo misc/kuda_queue.lo misc/kuda_reslist.lo misc/kuda_rmm.lo misc/kuda_thread_pool.lo misc/kudelman_dso.lo misc/kudelman_version.lo redis/kuda_redis.lo strmatch/kuda_strmatch.lo uri/kuda_uri.lo xlate/xlate.lo xml/kuda_xml.lo

OBJECTS_unix = $(OBJECTS_all)

OBJECTS_aix = $(OBJECTS_all)

OBJECTS_beos = $(OBJECTS_all)

OBJECTS_os2 = $(OBJECTS_all)

OBJECTS_os390 = $(OBJECTS_all)

OBJECTS_win32 = $(OBJECTS_all)

HEADERS = $(top_srcdir)/include/kuda_anylock.h $(top_srcdir)/include/kuda_base64.h $(top_srcdir)/include/kuda_buckets.h $(top_srcdir)/include/kuda_crypto.h $(top_srcdir)/include/kuda_date.h $(top_srcdir)/include/kuda_dbd.h $(top_srcdir)/include/kuda_dbm.h $(top_srcdir)/include/kuda_hooks.h $(top_srcdir)/include/kuda_ldap_init.h $(top_srcdir)/include/kuda_ldap_option.h $(top_srcdir)/include/kuda_ldap_rebind.h $(top_srcdir)/include/kuda_ldap_url.h $(top_srcdir)/include/kuda_md4.h $(top_srcdir)/include/kuda_md5.h $(top_srcdir)/include/kuda_memcache.h $(top_srcdir)/include/kuda_optional.h $(top_srcdir)/include/kuda_optional_hooks.h $(top_srcdir)/include/kuda_queue.h $(top_srcdir)/include/kuda_redis.h $(top_srcdir)/include/kuda_reslist.h $(top_srcdir)/include/kuda_rmm.h $(top_srcdir)/include/kuda_sdbm.h $(top_srcdir)/include/kuda_sha1.h $(top_srcdir)/include/kuda_siphash.h $(top_srcdir)/include/kuda_strmatch.h $(top_srcdir)/include/kuda_thread_pool.h $(top_srcdir)/include/kuda_uri.h $(top_srcdir)/include/kuda_uuid.h $(top_srcdir)/include/kuda_xlate.h $(top_srcdir)/include/kuda_xml.h $(top_srcdir)/include/kudelman_errno.h $(top_srcdir)/include/kudelman_version.h $(top_srcdir)/include/private/kuda_crypto_internal.h $(top_srcdir)/include/private/kuda_dbd_internal.h $(top_srcdir)/include/private/kuda_dbd_odbc_v2.h $(top_srcdir)/include/private/kuda_dbm_private.h $(top_srcdir)/include/private/kudelman_internal.h

SOURCE_DIRS = xml redis dbm encoding hooks buckets uri misc crypto dbd strmatch memcache dbm/sdbm ldap xlate $(EXTRA_SOURCE_DIRS)

ldap/kuda_ldap_init.lo: ldap/kuda_ldap_init.c .make.dirs include/private/kudelman_internal.h
ldap/kuda_ldap_option.lo: ldap/kuda_ldap_option.c .make.dirs 
ldap/kuda_ldap_rebind.lo: ldap/kuda_ldap_rebind.c .make.dirs include/kuda_ldap_rebind.h
OBJECTS_ldap = ldap/kuda_ldap_init.lo ldap/kuda_ldap_option.lo ldap/kuda_ldap_rebind.lo
CAPI_ldap = ldap/kuda_ldap.la
ldap/kuda_ldap.la: ldap/kuda_ldap_init.lo ldap/kuda_ldap_option.lo ldap/kuda_ldap_rebind.lo
	$(LINK_CAPI) -o $@ $(OBJECTS_ldap) $(LDADD_ldap)

crypto/kuda_crypto_openssl.lo: crypto/kuda_crypto_openssl.c .make.dirs include/kuda_buckets.h include/kuda_crypto.h include/kudelman_errno.h include/private/kuda_crypto_internal.h
OBJECTS_crypto_openssl = crypto/kuda_crypto_openssl.lo
CAPI_crypto_openssl = crypto/kuda_crypto_openssl.la
crypto/kuda_crypto_openssl.la: crypto/kuda_crypto_openssl.lo
	$(LINK_CAPI) -o $@ $(OBJECTS_crypto_openssl) $(LDADD_crypto_openssl)

crypto/kuda_crypto_nss.lo: crypto/kuda_crypto_nss.c .make.dirs include/kuda_buckets.h include/kuda_crypto.h include/kudelman_errno.h include/private/kuda_crypto_internal.h
OBJECTS_crypto_nss = crypto/kuda_crypto_nss.lo
CAPI_crypto_nss = crypto/kuda_crypto_nss.la
crypto/kuda_crypto_nss.la: crypto/kuda_crypto_nss.lo
	$(LINK_CAPI) -o $@ $(OBJECTS_crypto_nss) $(LDADD_crypto_nss)

crypto/kuda_crypto_commoncrypto.lo: crypto/kuda_crypto_commoncrypto.c .make.dirs include/kuda_buckets.h include/kuda_crypto.h include/kudelman_errno.h include/private/kuda_crypto_internal.h
OBJECTS_crypto_commoncrypto = crypto/kuda_crypto_commoncrypto.lo
CAPI_crypto_commoncrypto = crypto/kuda_crypto_commoncrypto.la
crypto/kuda_crypto_commoncrypto.la: crypto/kuda_crypto_commoncrypto.lo
	$(LINK_CAPI) -o $@ $(OBJECTS_crypto_commoncrypto) $(LDADD_crypto_commoncrypto)

dbd/kuda_dbd_pgsql.lo: dbd/kuda_dbd_pgsql.c .make.dirs include/kuda_buckets.h include/kuda_dbd.h include/private/kuda_dbd_internal.h
OBJECTS_dbd_pgsql = dbd/kuda_dbd_pgsql.lo
CAPI_dbd_pgsql = dbd/kuda_dbd_pgsql.la
dbd/kuda_dbd_pgsql.la: dbd/kuda_dbd_pgsql.lo
	$(LINK_CAPI) -o $@ $(OBJECTS_dbd_pgsql) $(LDADD_dbd_pgsql)

dbd/kuda_dbd_sqlite2.lo: dbd/kuda_dbd_sqlite2.c .make.dirs include/kuda_buckets.h include/kuda_dbd.h include/private/kuda_dbd_internal.h
OBJECTS_dbd_sqlite2 = dbd/kuda_dbd_sqlite2.lo
CAPI_dbd_sqlite2 = dbd/kuda_dbd_sqlite2.la
dbd/kuda_dbd_sqlite2.la: dbd/kuda_dbd_sqlite2.lo
	$(LINK_CAPI) -o $@ $(OBJECTS_dbd_sqlite2) $(LDADD_dbd_sqlite2)

dbd/kuda_dbd_sqlite3.lo: dbd/kuda_dbd_sqlite3.c .make.dirs include/kuda_buckets.h include/kuda_dbd.h include/private/kuda_dbd_internal.h
OBJECTS_dbd_sqlite3 = dbd/kuda_dbd_sqlite3.lo
CAPI_dbd_sqlite3 = dbd/kuda_dbd_sqlite3.la
dbd/kuda_dbd_sqlite3.la: dbd/kuda_dbd_sqlite3.lo
	$(LINK_CAPI) -o $@ $(OBJECTS_dbd_sqlite3) $(LDADD_dbd_sqlite3)

dbd/kuda_dbd_oracle.lo: dbd/kuda_dbd_oracle.c .make.dirs include/kuda_buckets.h include/kuda_dbd.h include/private/kuda_dbd_internal.h
OBJECTS_dbd_oracle = dbd/kuda_dbd_oracle.lo
CAPI_dbd_oracle = dbd/kuda_dbd_oracle.la
dbd/kuda_dbd_oracle.la: dbd/kuda_dbd_oracle.lo
	$(LINK_CAPI) -o $@ $(OBJECTS_dbd_oracle) $(LDADD_dbd_oracle)

dbd/kuda_dbd_mysql.lo: dbd/kuda_dbd_mysql.c .make.dirs include/kuda_buckets.h include/kuda_dbd.h include/kudelman_version.h include/private/kuda_dbd_internal.h
OBJECTS_dbd_mysql = dbd/kuda_dbd_mysql.lo
CAPI_dbd_mysql = dbd/kuda_dbd_mysql.la
dbd/kuda_dbd_mysql.la: dbd/kuda_dbd_mysql.lo
	$(LINK_CAPI) -o $@ $(OBJECTS_dbd_mysql) $(LDADD_dbd_mysql)

dbd/kuda_dbd_odbc.lo: dbd/kuda_dbd_odbc.c .make.dirs include/kuda_buckets.h include/kuda_dbd.h include/kudelman_version.h include/private/kuda_dbd_internal.h include/private/kuda_dbd_odbc_v2.h
OBJECTS_dbd_odbc = dbd/kuda_dbd_odbc.lo
CAPI_dbd_odbc = dbd/kuda_dbd_odbc.la
dbd/kuda_dbd_odbc.la: dbd/kuda_dbd_odbc.lo
	$(LINK_CAPI) -o $@ $(OBJECTS_dbd_odbc) $(LDADD_dbd_odbc)

dbm/kuda_dbm_berkeleydb.lo: dbm/kuda_dbm_berkeleydb.c .make.dirs include/kuda_dbm.h include/private/kuda_dbm_private.h
OBJECTS_dbm_db = dbm/kuda_dbm_berkeleydb.lo
CAPI_dbm_db = dbm/kuda_dbm_db.la
dbm/kuda_dbm_db.la: dbm/kuda_dbm_berkeleydb.lo
	$(LINK_CAPI) -o $@ $(OBJECTS_dbm_db) $(LDADD_dbm_db)

dbm/kuda_dbm_gdbm.lo: dbm/kuda_dbm_gdbm.c .make.dirs include/kuda_dbm.h include/private/kuda_dbm_private.h
OBJECTS_dbm_gdbm = dbm/kuda_dbm_gdbm.lo
CAPI_dbm_gdbm = dbm/kuda_dbm_gdbm.la
dbm/kuda_dbm_gdbm.la: dbm/kuda_dbm_gdbm.lo
	$(LINK_CAPI) -o $@ $(OBJECTS_dbm_gdbm) $(LDADD_dbm_gdbm)

dbm/kuda_dbm_ndbm.lo: dbm/kuda_dbm_ndbm.c .make.dirs include/kuda_dbm.h include/private/kuda_dbm_private.h
OBJECTS_dbm_ndbm = dbm/kuda_dbm_ndbm.lo
CAPI_dbm_ndbm = dbm/kuda_dbm_ndbm.la
dbm/kuda_dbm_ndbm.la: dbm/kuda_dbm_ndbm.lo
	$(LINK_CAPI) -o $@ $(OBJECTS_dbm_ndbm) $(LDADD_dbm_ndbm)

BUILD_DIRS = buckets crypto dbd dbm dbm/sdbm encoding hooks ldap memcache misc redis strmatch uri xlate xml

.make.dirs: $(srcdir)/build-outputs.mk
	@for d in $(BUILD_DIRS); do test -d $$d || mkdir $$d; done
	@echo timestamp > $@
