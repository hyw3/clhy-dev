/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include "kuda_buckets.h"
#include "kuda_allocator.h"
#include "kuda_version.h"

#define ALLOC_AMT (8192 - KUDA_MEMNODE_T_SIZE)

typedef struct node_header_t {
    kuda_size_t size;
    kuda_bucket_alloc_t *alloc;
    kuda_memnode_t *memnode;
    struct node_header_t *next;
} node_header_t;

#define SIZEOF_NODE_HEADER_T  KUDA_ALIGN_DEFAULT(sizeof(node_header_t))
#define SMALL_NODE_SIZE       (KUDA_BUCKET_ALLOC_SIZE + SIZEOF_NODE_HEADER_T)

/** A list of free memory from which new buckets or private bucket
 *  structures can be allocated.
 */
struct kuda_bucket_alloc_t {
    kuda_pool_t *pool;
    kuda_allocator_t *allocator;
    node_header_t *freelist;
    kuda_memnode_t *blocks;
};

static kuda_status_t alloc_cleanup(void *data)
{
    kuda_bucket_alloc_t *list = data;

    kuda_allocator_free(list->allocator, list->blocks);

#if KUDA_POOL_DEBUG
    if (list->pool && list->allocator != kuda_pool_allocator_get(list->pool)) {
        kuda_allocator_destroy(list->allocator);
    }
#endif

    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE_NONSTD(kuda_bucket_alloc_t *) kuda_bucket_alloc_create(kuda_pool_t *p)
{
    kuda_allocator_t *allocator = kuda_pool_allocator_get(p);
    kuda_bucket_alloc_t *list;

#if KUDA_POOL_DEBUG
    /* may be NULL for debug mode. */
    if (allocator == NULL) {
        if (kuda_allocator_create(&allocator) != KUDA_SUCCESS) {
            kuda_abortfunc_t fn = kuda_pool_abort_get(p);
            if (fn)
                (fn)(KUDA_ENOMEM);
            abort();
        }
    }
#endif
    list = kuda_bucket_alloc_create_ex(allocator);
    if (list == NULL) {
            kuda_abortfunc_t fn = kuda_pool_abort_get(p);
            if (fn)
                (fn)(KUDA_ENOMEM);
            abort();
    }
    list->pool = p;
    kuda_pool_cleanup_register(list->pool, list, alloc_cleanup,
                              kuda_pool_cleanup_null);

    return list;
}

KUDELMAN_DECLARE_NONSTD(kuda_bucket_alloc_t *) kuda_bucket_alloc_create_ex(
                                             kuda_allocator_t *allocator)
{
    kuda_bucket_alloc_t *list;
    kuda_memnode_t *block;

    block = kuda_allocator_alloc(allocator, ALLOC_AMT);
    if (!block) {
        return NULL;
    }
    list = (kuda_bucket_alloc_t *)block->first_avail;
    list->pool = NULL;
    list->allocator = allocator;
    list->freelist = NULL;
    list->blocks = block;
    block->first_avail += KUDA_ALIGN_DEFAULT(sizeof(*list));

    return list;
}

KUDELMAN_DECLARE_NONSTD(void) kuda_bucket_alloc_destroy(kuda_bucket_alloc_t *list)
{
    if (list->pool) {
        kuda_pool_cleanup_kill(list->pool, list, alloc_cleanup);
    }

    kuda_allocator_free(list->allocator, list->blocks);

#if KUDA_POOL_DEBUG
    if (list->pool && list->allocator != kuda_pool_allocator_get(list->pool)) {
        kuda_allocator_destroy(list->allocator);
    }
#endif
}

KUDELMAN_DECLARE_NONSTD(kuda_size_t) kuda_bucket_alloc_aligned_floor(kuda_bucket_alloc_t *list,
                                                              kuda_size_t size)
{
    if (size <= SMALL_NODE_SIZE) {
        size = SMALL_NODE_SIZE;
    }
    else {
#if KUDA_VERSION_AT_LEAST(1,6,0)
        if (size < KUDA_MEMNODE_T_SIZE) {
            size = kuda_allocator_align(list->allocator, 0);
        }
        else {
            size = kuda_allocator_align(list->allocator,
                                       size - KUDA_MEMNODE_T_SIZE);
        }
#else
        /* Assumes the minimum (default) allocator's boundary of 4K and
         * minimum (immutable before KUDA-1.6.x) allocation size of 8K,
         * hence possibly (yet unlikely) under-estimating the floor...
         */
        size = KUDA_ALIGN(size, 4096);
        if (size < 8192) {
            size = 8192;
        }
#endif
        size -= KUDA_MEMNODE_T_SIZE;
    }
    size -= SIZEOF_NODE_HEADER_T;
    return size;
}

KUDELMAN_DECLARE_NONSTD(void *) kuda_bucket_alloc(kuda_size_t size, 
                                            kuda_bucket_alloc_t *list)
{
    node_header_t *node;
    kuda_memnode_t *active = list->blocks;
    char *endp;

    size += SIZEOF_NODE_HEADER_T;
    if (size <= SMALL_NODE_SIZE) {
        if (list->freelist) {
            node = list->freelist;
            list->freelist = node->next;
        }
        else {
            endp = active->first_avail + SMALL_NODE_SIZE;
            if (endp >= active->endp) {
                list->blocks = kuda_allocator_alloc(list->allocator, ALLOC_AMT);
                if (!list->blocks) {
                    list->blocks = active;
                    return NULL;
                }
                list->blocks->next = active;
                active = list->blocks;
                endp = active->first_avail + SMALL_NODE_SIZE;
            }
            node = (node_header_t *)active->first_avail;
            node->alloc = list;
            node->memnode = active;
            node->size = SMALL_NODE_SIZE;
            active->first_avail = endp;
        }
    }
    else {
        kuda_memnode_t *memnode = kuda_allocator_alloc(list->allocator, size);
        if (!memnode) {
            return NULL;
        }
        node = (node_header_t *)memnode->first_avail;
        node->alloc = list;
        node->memnode = memnode;
        node->size = size;
    }
    return ((char *)node) + SIZEOF_NODE_HEADER_T;
}

#ifdef KUDA_BUCKET_DEBUG
#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif
static void check_not_already_free(node_header_t *node)
{
    kuda_bucket_alloc_t *list = node->alloc;
    node_header_t *curr = list->freelist;

    while (curr) {
        if (node == curr) {
            abort();
        }
        curr = curr->next;
    }
}
#else
#define check_not_already_free(node)
#endif

KUDELMAN_DECLARE_NONSTD(void) kuda_bucket_free(void *mem)
{
    node_header_t *node = (node_header_t *)((char *)mem - SIZEOF_NODE_HEADER_T);
    kuda_bucket_alloc_t *list = node->alloc;

    if (node->size == SMALL_NODE_SIZE) {
        check_not_already_free(node);
        node->next = list->freelist;
        list->freelist = node;
    }
    else {
        kuda_allocator_free(list->allocator, node->memnode);
    }
}
