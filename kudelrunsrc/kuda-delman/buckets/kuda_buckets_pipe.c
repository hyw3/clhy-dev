/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_buckets.h"

static kuda_status_t pipe_bucket_read(kuda_bucket *a, const char **str,
                                     kuda_size_t *len, kuda_read_type_e block)
{
    kuda_file_t *p = a->data;
    char *buf;
    kuda_status_t rv;
    kuda_interval_time_t timeout;

    if (block == KUDA_NONBLOCK_READ) {
        kuda_file_pipe_timeout_get(p, &timeout);
        kuda_file_pipe_timeout_set(p, 0);
    }

    *str = NULL;
    *len = KUDA_BUCKET_BUFF_SIZE;
    buf = kuda_bucket_alloc(*len, a->list); /* XXX: check for failure? */

    rv = kuda_file_read(p, buf, len);

    if (block == KUDA_NONBLOCK_READ) {
        kuda_file_pipe_timeout_set(p, timeout);
    }

    if (rv != KUDA_SUCCESS && rv != KUDA_EOF) {
        kuda_bucket_free(buf);
        return rv;
    }
    /*
     * If there's more to read we have to keep the rest of the pipe
     * for later.  Otherwise, we'll close the pipe.
     * XXX: Note that more complicated bucket types that 
     * refer to data not in memory and must therefore have a read()
     * function similar to this one should be wary of copying this
     * code because if they have a destroy function they probably
     * want to migrate the bucket's subordinate structure from the
     * old bucket to a raw new one and adjust it as appropriate,
     * rather than destroying the old one and creating a completely
     * new bucket.
     */
    if (*len > 0) {
        kuda_bucket_heap *h;
        /* Change the current bucket to refer to what we read */
        a = kuda_bucket_heap_make(a, buf, *len, kuda_bucket_free);
        h = a->data;
        h->alloc_len = KUDA_BUCKET_BUFF_SIZE; /* note the real buffer size */
        *str = buf;
        KUDA_BUCKET_INSERT_AFTER(a, kuda_bucket_pipe_create(p, a->list));
    }
    else {
        kuda_bucket_free(buf);
        a = kuda_bucket_immortal_make(a, "", 0);
        *str = a->data;
        if (rv == KUDA_EOF) {
            kuda_file_close(p);
        }
    }
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_pipe_make(kuda_bucket *b, kuda_file_t *p)
{
    /*
     * A pipe is closed when the end is reached in pipe_bucket_read().  If
     * the pipe isn't read to the end (e.g., error path), the pipe will be
     * closed when its pool goes away.
     *
     * Note that typically the pipe is allocated from the request pool
     * so it will disappear when the request is finished. However the
     * core filter may decide to set aside the tail end of a CGI
     * response if the connection is pipelined. This turns out not to
     * be a problem because the core will have read to the end of the
     * stream so the bucket(s) that it sets aside will be the heap
     * buckets created by pipe_bucket_read() above.
     */
    b->type        = &kuda_bucket_type_pipe;
    b->length      = (kuda_size_t)(-1);
    b->start       = -1;
    b->data        = p;
    
    return b;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_pipe_create(kuda_file_t *p,
                                                 kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    return kuda_bucket_pipe_make(b, p);
}

KUDELMAN_DECLARE_DATA const kuda_bucket_type_t kuda_bucket_type_pipe = {
    "PIPE", 5, KUDA_BUCKET_DATA, 
    kuda_bucket_destroy_noop,
    pipe_bucket_read,
    kuda_bucket_setaside_notimpl,
    kuda_bucket_split_notimpl,
    kuda_bucket_copy_notimpl
};
