/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_pools.h"
#include "kuda_tables.h"
#include "kuda_buckets.h"
#include "kuda_errno.h"
#define KUDA_WANT_MEMFUNC
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#if KUDA_HAVE_SYS_UIO_H
#include <sys/uio.h>
#endif

static kuda_status_t brigade_cleanup(void *data) 
{
    return kuda_brigade_cleanup(data);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_cleanup(void *data)
{
    kuda_bucket_brigade *b = data;
    kuda_bucket *e;

    while (!KUDA_BRIGADE_EMPTY(b)) {
        e = KUDA_BRIGADE_FIRST(b);
        kuda_bucket_delete(e);
    }
    /* We don't need to free(bb) because it's allocated from a pool. */
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_destroy(kuda_bucket_brigade *b)
{
    kuda_pool_cleanup_kill(b->p, b, brigade_cleanup);
    return kuda_brigade_cleanup(b);
}

KUDELMAN_DECLARE(kuda_bucket_brigade *) kuda_brigade_create(kuda_pool_t *p,
                                                     kuda_bucket_alloc_t *list)
{
    kuda_bucket_brigade *b;

    b = kuda_palloc(p, sizeof(*b));
    b->p = p;
    b->bucket_alloc = list;

    KUDA_RING_INIT(&b->list, kuda_bucket, link);

    kuda_pool_cleanup_register(b->p, b, brigade_cleanup, kuda_pool_cleanup_null);
    return b;
}

KUDELMAN_DECLARE(kuda_bucket_brigade *) kuda_brigade_split_ex(kuda_bucket_brigade *b,
                                                       kuda_bucket *e,
                                                       kuda_bucket_brigade *a)
{
    kuda_bucket *f;

    if (!a) {
        a = kuda_brigade_create(b->p, b->bucket_alloc);
    }
    else if (!KUDA_BRIGADE_EMPTY(a)) {
        kuda_brigade_cleanup(a);
    }
    /* Return an empty brigade if there is nothing left in 
     * the first brigade to split off 
     */
    if (e != KUDA_BRIGADE_SENTINEL(b)) {
        f = KUDA_RING_LAST(&b->list);
        KUDA_RING_UNSPLICE(e, f, link);
        KUDA_RING_SPLICE_HEAD(&a->list, e, f, kuda_bucket, link);
    }

    KUDA_BRIGADE_CHECK_CONSISTENCY(a);
    KUDA_BRIGADE_CHECK_CONSISTENCY(b);

    return a;
}

KUDELMAN_DECLARE(kuda_bucket_brigade *) kuda_brigade_split(kuda_bucket_brigade *b,
                                                    kuda_bucket *e)
{
    return kuda_brigade_split_ex(b, e, NULL);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_partition(kuda_bucket_brigade *b,
                                                kuda_off_t point,
                                                kuda_bucket **after_point)
{
    kuda_bucket *e;
    const char *s;
    kuda_size_t len;
    kuda_uint64_t point64;
    kuda_status_t rv;

    if (point < 0) {
        /* this could cause weird (not necessarily SEGV) things to happen */
        return KUDA_EINVAL;
    }
    if (point == 0) {
        *after_point = KUDA_BRIGADE_FIRST(b);
        return KUDA_SUCCESS;
    }

    /*
     * Try to reduce the following casting mess: We know that point will be
     * larger equal 0 now and forever and thus that point (kuda_off_t) and
     * kuda_size_t will fit into kuda_uint64_t in any case.
     */
    point64 = (kuda_uint64_t)point;

    KUDA_BRIGADE_CHECK_CONSISTENCY(b);

    for (e = KUDA_BRIGADE_FIRST(b);
         e != KUDA_BRIGADE_SENTINEL(b);
         e = KUDA_BUCKET_NEXT(e))
    {
        /* For an unknown length bucket, while 'point64' is beyond the possible
         * size contained in kuda_size_t, read and continue...
         */
        if ((e->length == (kuda_size_t)(-1))
            && (point64 > (kuda_uint64_t)KUDA_SIZE_MAX)) {
            /* point64 is too far out to simply split this bucket,
             * we must fix this bucket's size and keep going... */
            rv = kuda_bucket_read(e, &s, &len, KUDA_BLOCK_READ);
            if (rv != KUDA_SUCCESS) {
                *after_point = e;
                return rv;
            }
        }
        else if ((point64 < (kuda_uint64_t)e->length)
                 || (e->length == (kuda_size_t)(-1))) {
            /* We already consumed buckets where point64 is beyond
             * our interest ( point64 > KUDA_SIZE_MAX ), above.
             * Here point falls between 0 and KUDA_SIZE_MAX
             * and is within this bucket, or this bucket's len
             * is undefined, so now we are ready to split it.
             * First try to split the bucket natively... */
            if ((rv = kuda_bucket_split(e, (kuda_size_t)point64)) 
                    != KUDA_ENOTIMPL) {
                *after_point = KUDA_BUCKET_NEXT(e);
                return rv;
            }

            /* if the bucket cannot be split, we must read from it,
             * changing its type to one that can be split */
            rv = kuda_bucket_read(e, &s, &len, KUDA_BLOCK_READ);
            if (rv != KUDA_SUCCESS) {
                *after_point = e;
                return rv;
            }

            /* this assumes that len == e->length, which is okay because e
             * might have been morphed by the kuda_bucket_read() above, but
             * if it was, the length would have been adjusted appropriately */
            if (point64 < (kuda_uint64_t)e->length) {
                rv = kuda_bucket_split(e, (kuda_size_t)point64);
                *after_point = KUDA_BUCKET_NEXT(e);
                return rv;
            }
        }
        if (point64 == (kuda_uint64_t)e->length) {
            *after_point = KUDA_BUCKET_NEXT(e);
            return KUDA_SUCCESS;
        }
        point64 -= (kuda_uint64_t)e->length;
    }
    *after_point = KUDA_BRIGADE_SENTINEL(b); 
    return KUDA_INCOMPLETE;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_length(kuda_bucket_brigade *bb,
                                             int read_all, kuda_off_t *length)
{
    kuda_off_t total = 0;
    kuda_bucket *bkt;
    kuda_status_t status = KUDA_SUCCESS;

    for (bkt = KUDA_BRIGADE_FIRST(bb);
         bkt != KUDA_BRIGADE_SENTINEL(bb);
         bkt = KUDA_BUCKET_NEXT(bkt))
    {
        if (bkt->length == (kuda_size_t)(-1)) {
            const char *ignore;
            kuda_size_t len;

            if (!read_all) {
                total = -1;
                break;
            }

            if ((status = kuda_bucket_read(bkt, &ignore, &len,
                                          KUDA_BLOCK_READ)) != KUDA_SUCCESS) {
                break;
            }
        }

        total += bkt->length;
    }

    *length = total;
    return status;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_flatten(kuda_bucket_brigade *bb,
                                              char *c, kuda_size_t *len)
{
    kuda_size_t actual = 0;
    kuda_bucket *b;
 
    for (b = KUDA_BRIGADE_FIRST(bb);
         b != KUDA_BRIGADE_SENTINEL(bb);
         b = KUDA_BUCKET_NEXT(b))
    {
        const char *str;
        kuda_size_t str_len;
        kuda_status_t status;

        status = kuda_bucket_read(b, &str, &str_len, KUDA_BLOCK_READ);
        if (status != KUDA_SUCCESS) {
            return status;
        }

        /* If we would overflow. */
        if (str_len + actual > *len) {
            str_len = *len - actual;
        }

        /* XXX: It appears that overflow of the final bucket
         * is DISCARDED without any warning to the caller.
         *
         * No, we only copy the data up to their requested size.  -- jre
         */
        memcpy(c, str, str_len);

        c += str_len;
        actual += str_len;

        /* This could probably be actual == *len, but be safe from stray
         * photons. */
        if (actual >= *len) {
            break;
        }
    }

    *len = actual;
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_pflatten(kuda_bucket_brigade *bb,
                                               char **c,
                                               kuda_size_t *len,
                                               kuda_pool_t *pool)
{
    kuda_off_t actual;
    kuda_size_t total;
    kuda_status_t rv;

    kuda_brigade_length(bb, 1, &actual);
    
    /* XXX: This is dangerous beyond belief.  At least in the
     * kuda_brigade_flatten case, the user explicitly stated their
     * buffer length - so we don't up and palloc 4GB for a single
     * file bucket.  This API must grow a useful max boundry,
     * either compiled-in or preset via the *len value.
     *
     * Shouldn't both fn's grow an additional return value for 
     * the case that the brigade couldn't be flattened into the
     * provided or allocated buffer (such as KUDA_EMOREDATA?)
     * Not a failure, simply an advisory result.
     */
    total = (kuda_size_t)actual;

    *c = kuda_palloc(pool, total);
    
    rv = kuda_brigade_flatten(bb, *c, &total);

    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    *len = total;
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_split_line(kuda_bucket_brigade *bbOut,
                                                 kuda_bucket_brigade *bbIn,
                                                 kuda_read_type_e block,
                                                 kuda_off_t maxbytes)
{
    kuda_off_t readbytes = 0;

    while (!KUDA_BRIGADE_EMPTY(bbIn)) {
        const char *pos;
        const char *str;
        kuda_size_t len;
        kuda_status_t rv;
        kuda_bucket *e;

        e = KUDA_BRIGADE_FIRST(bbIn);
        rv = kuda_bucket_read(e, &str, &len, block);

        if (rv != KUDA_SUCCESS) {
            return rv;
        }

        pos = memchr(str, KUDA_ASCII_LF, len);
        /* We found a match. */
        if (pos != NULL) {
            kuda_bucket_split(e, pos - str + 1);
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(bbOut, e);
            return KUDA_SUCCESS;
        }
        KUDA_BUCKET_REMOVE(e);
        if (KUDA_BUCKET_IS_METADATA(e) || len > KUDA_BUCKET_BUFF_SIZE/4) {
            KUDA_BRIGADE_INSERT_TAIL(bbOut, e);
        }
        else {
            if (len > 0) {
                rv = kuda_brigade_write(bbOut, NULL, NULL, str, len);
                if (rv != KUDA_SUCCESS) {
                    return rv;
                }
            }
            kuda_bucket_destroy(e);
        }
        readbytes += len;
        /* We didn't find an KUDA_ASCII_LF within the maximum line length. */
        if (readbytes >= maxbytes) {
            break;
        }
    }

    return KUDA_SUCCESS;
}


KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_to_iovec(kuda_bucket_brigade *b, 
                                               struct iovec *vec, int *nvec)
{
    int left = *nvec;
    kuda_bucket *e;
    struct iovec *orig;
    kuda_size_t iov_len;
    const char *iov_base;
    kuda_status_t rv;

    orig = vec;

    for (e = KUDA_BRIGADE_FIRST(b);
         e != KUDA_BRIGADE_SENTINEL(b);
         e = KUDA_BUCKET_NEXT(e))
    {
        if (left-- == 0)
            break;

        rv = kuda_bucket_read(e, &iov_base, &iov_len, KUDA_NONBLOCK_READ);
        if (rv != KUDA_SUCCESS)
            return rv;
        /* Set indirectly since types differ: */
        vec->iov_len = iov_len;
        vec->iov_base = (void *)iov_base;
        ++vec;
    }

    *nvec = (int)(vec - orig);
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_vputstrs(kuda_bucket_brigade *b, 
                                               kuda_brigade_flush flush,
                                               void *ctx,
                                               va_list va)
{
#define MAX_VECS    8
    struct iovec vec[MAX_VECS];
    kuda_size_t i = 0;

    for (;;) {
        char *str = va_arg(va, char *);
        kuda_status_t rv;

        if (str == NULL)
            break;

        vec[i].iov_base = str;
        vec[i].iov_len = strlen(str);
        i++;

        if (i == MAX_VECS) {
            rv = kuda_brigade_writev(b, flush, ctx, vec, i);
            if (rv != KUDA_SUCCESS)
                return rv;
            i = 0;
        }
    }
    if (i != 0)
       return kuda_brigade_writev(b, flush, ctx, vec, i);

    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_putc(kuda_bucket_brigade *b,
                                           kuda_brigade_flush flush, void *ctx,
                                           const char c)
{
    return kuda_brigade_write(b, flush, ctx, &c, 1);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_write(kuda_bucket_brigade *b,
                                            kuda_brigade_flush flush,
                                            void *ctx, 
                                            const char *str, kuda_size_t nbyte)
{
    kuda_bucket *e = KUDA_BRIGADE_LAST(b);
    kuda_size_t remaining = KUDA_BUCKET_BUFF_SIZE;
    char *buf = NULL;

    /*
     * If the last bucket is a heap bucket and its buffer is not shared with
     * another bucket, we may write into that bucket.
     */
    if (!KUDA_BRIGADE_EMPTY(b) && KUDA_BUCKET_IS_HEAP(e)
        && ((kuda_bucket_heap *)(e->data))->refcount.refcount == 1) {
        kuda_bucket_heap *h = e->data;

        /* HEAP bucket start offsets are always in-memory, safe to cast */
        remaining = h->alloc_len - (e->length + (kuda_size_t)e->start);
        buf = h->base + e->start + e->length;
    }

    if (nbyte > remaining) {
        /* either a buffer bucket exists but is full, 
         * or no buffer bucket exists and the data is too big
         * to buffer.  In either case, we should flush.  */
        if (flush) {
            e = kuda_bucket_transient_create(str, nbyte, b->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(b, e);
            return flush(b, ctx);
        }
        else {
            e = kuda_bucket_heap_create(str, nbyte, NULL, b->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(b, e);
            return KUDA_SUCCESS;
        }
    }
    else if (!buf) {
        /* we don't have a buffer, but the data is small enough
         * that we don't mind making a new buffer */
        buf = kuda_bucket_alloc(KUDA_BUCKET_BUFF_SIZE, b->bucket_alloc);
        e = kuda_bucket_heap_create(buf, KUDA_BUCKET_BUFF_SIZE,
                                   kuda_bucket_free, b->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(b, e);
        e->length = 0;   /* We are writing into the brigade, and
                          * allocating more memory than we need.  This
                          * ensures that the bucket thinks it is empty just
                          * after we create it.  We'll fix the length
                          * once we put data in it below.
                          */
    }

    /* there is a sufficiently big buffer bucket available now */
    memcpy(buf, str, nbyte);
    e->length += nbyte;

    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_writev(kuda_bucket_brigade *b,
                                             kuda_brigade_flush flush,
                                             void *ctx,
                                             const struct iovec *vec,
                                             kuda_size_t nvec)
{
    kuda_bucket *e;
    kuda_size_t total_len;
    kuda_size_t i;
    char *buf;

    /* Compute the total length of the data to be written.
     */
    total_len = 0;
    for (i = 0; i < nvec; i++) {
       total_len += vec[i].iov_len;
    }

    /* If the data to be written is very large, try to convert
     * the iovec to transient buckets rather than copying.
     */
    if (total_len > KUDA_BUCKET_BUFF_SIZE) {
        if (flush) {
            for (i = 0; i < nvec; i++) {
                e = kuda_bucket_transient_create(vec[i].iov_base,
                                                vec[i].iov_len,
                                                b->bucket_alloc);
                KUDA_BRIGADE_INSERT_TAIL(b, e);
            }
            return flush(b, ctx);
        }
        else {
            for (i = 0; i < nvec; i++) {
                e = kuda_bucket_heap_create((const char *) vec[i].iov_base,
                                           vec[i].iov_len, NULL,
                                           b->bucket_alloc);
                KUDA_BRIGADE_INSERT_TAIL(b, e);
            }
            return KUDA_SUCCESS;
        }
    }

    i = 0;

    /* If there is a heap bucket at the end of the brigade
     * already, and its refcount is 1, copy into the existing bucket.
     */
    e = KUDA_BRIGADE_LAST(b);
    if (!KUDA_BRIGADE_EMPTY(b) && KUDA_BUCKET_IS_HEAP(e)
        && ((kuda_bucket_heap *)(e->data))->refcount.refcount == 1) {
        kuda_bucket_heap *h = e->data;
        kuda_size_t remaining = h->alloc_len -
            (e->length + (kuda_size_t)e->start);
        buf = h->base + e->start + e->length;

        if (remaining >= total_len) {
            /* Simple case: all the data will fit in the
             * existing heap bucket
             */
            for (; i < nvec; i++) {
                kuda_size_t len = vec[i].iov_len;
                memcpy(buf, (const void *) vec[i].iov_base, len);
                buf += len;
            }
            e->length += total_len;
            return KUDA_SUCCESS;
        }
        else {
            /* More complicated case: not all of the data
             * will fit in the existing heap bucket.  The
             * total data size is <= KUDA_BUCKET_BUFF_SIZE,
             * so we'll need only one additional bucket.
             */
            const char *start_buf = buf;
            for (; i < nvec; i++) {
                kuda_size_t len = vec[i].iov_len;
                if (len > remaining) {
                    break;
                }
                memcpy(buf, (const void *) vec[i].iov_base, len);
                buf += len;
                remaining -= len;
            }
            e->length += (buf - start_buf);
            total_len -= (buf - start_buf);

            if (flush) {
                kuda_status_t rv = flush(b, ctx);
                if (rv != KUDA_SUCCESS) {
                    return rv;
                }
            }

            /* Now fall through into the case below to
             * allocate another heap bucket and copy the
             * rest of the array.  (Note that i is not
             * reset to zero here; it holds the index
             * of the first vector element to be
             * written to the new bucket.)
             */
        }
    }

    /* Allocate a new heap bucket, and copy the data into it.
     * The checks above ensure that the amount of data to be
     * written here is no larger than KUDA_BUCKET_BUFF_SIZE.
     */
    buf = kuda_bucket_alloc(KUDA_BUCKET_BUFF_SIZE, b->bucket_alloc);
    e = kuda_bucket_heap_create(buf, KUDA_BUCKET_BUFF_SIZE,
                               kuda_bucket_free, b->bucket_alloc);
    for (; i < nvec; i++) {
        kuda_size_t len = vec[i].iov_len;
        memcpy(buf, (const void *) vec[i].iov_base, len);
        buf += len;
    }
    e->length = total_len;
    KUDA_BRIGADE_INSERT_TAIL(b, e);

    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_puts(kuda_bucket_brigade *bb,
                                           kuda_brigade_flush flush, void *ctx,
                                           const char *str)
{
    return kuda_brigade_write(bb, flush, ctx, str, strlen(str));
}

KUDELMAN_DECLARE_NONSTD(kuda_status_t) kuda_brigade_putstrs(kuda_bucket_brigade *b, 
                                                     kuda_brigade_flush flush,
                                                     void *ctx, ...)
{
    va_list va;
    kuda_status_t rv;

    va_start(va, ctx);
    rv = kuda_brigade_vputstrs(b, flush, ctx, va);
    va_end(va);
    return rv;
}

KUDELMAN_DECLARE_NONSTD(kuda_status_t) kuda_brigade_printf(kuda_bucket_brigade *b, 
                                                    kuda_brigade_flush flush,
                                                    void *ctx, 
                                                    const char *fmt, ...)
{
    va_list clhy;
    kuda_status_t rv;

    va_start(clhy, fmt);
    rv = kuda_brigade_vprintf(b, flush, ctx, fmt, clhy);
    va_end(clhy);
    return rv;
}

struct brigade_vprintf_data_t {
    kuda_vformatter_buff_t vbuff;

    kuda_bucket_brigade *b;  /* associated brigade */
    kuda_brigade_flush *flusher; /* flushing function */
    void *ctx;

    char *cbuff; /* buffer to flush from */
};

static kuda_status_t brigade_flush(kuda_vformatter_buff_t *buff)
{
    /* callback function passed to clhy_vformatter to be
     * called when vformatter needs to buff and
     * buff.curpos > buff.endpos
     */

    /* "downcast," have really passed a brigade_vprintf_data_t* */
    struct brigade_vprintf_data_t *vd = (struct brigade_vprintf_data_t*)buff;
    kuda_status_t res = KUDA_SUCCESS;

    res = kuda_brigade_write(vd->b, *vd->flusher, vd->ctx, vd->cbuff,
                          KUDA_BUCKET_BUFF_SIZE);

    if(res != KUDA_SUCCESS) {
      return -1;
    }

    vd->vbuff.curpos = vd->cbuff;
    vd->vbuff.endpos = vd->cbuff + KUDA_BUCKET_BUFF_SIZE;

    return res;
}

KUDELMAN_DECLARE(kuda_status_t) kuda_brigade_vprintf(kuda_bucket_brigade *b,
                                              kuda_brigade_flush flush,
                                              void *ctx,
                                              const char *fmt, va_list va)
{
    /* the cast, in order of appearance */
    struct brigade_vprintf_data_t vd;
    char buf[KUDA_BUCKET_BUFF_SIZE];
    int written;

    vd.vbuff.curpos = buf;
    vd.vbuff.endpos = buf + KUDA_BUCKET_BUFF_SIZE;
    vd.b = b;
    vd.flusher = &flush;
    vd.ctx = ctx;
    vd.cbuff = buf;

    written = kuda_vformatter(brigade_flush, &vd.vbuff, fmt, va);

    if (written == -1) {
      return -1;
    }

    /* write out what remains in the buffer */
    return kuda_brigade_write(b, flush, ctx, buf, vd.vbuff.curpos - buf);
}

/* A "safe" maximum bucket size, 1Gb */
#define MAX_BUCKET_SIZE (0x40000000)

KUDELMAN_DECLARE(kuda_bucket *) kuda_brigade_insert_file(kuda_bucket_brigade *bb,
                                                  kuda_file_t *f,
                                                  kuda_off_t start,
                                                  kuda_off_t length,
                                                  kuda_pool_t *p)
{
    kuda_bucket *e;

    if (sizeof(kuda_off_t) == sizeof(kuda_size_t) || length < MAX_BUCKET_SIZE) {
        e = kuda_bucket_file_create(f, start, (kuda_size_t)length, p, 
                                   bb->bucket_alloc);
    }
    else {
        /* Several buckets are needed. */        
        e = kuda_bucket_file_create(f, start, MAX_BUCKET_SIZE, p, 
                                   bb->bucket_alloc);

        while (length > MAX_BUCKET_SIZE) {
            kuda_bucket *ce;
            kuda_bucket_copy(e, &ce);
            KUDA_BRIGADE_INSERT_TAIL(bb, ce);
            e->start += MAX_BUCKET_SIZE;
            length -= MAX_BUCKET_SIZE;
        }
        e->length = (kuda_size_t)length; /* Resize just the last bucket */
    }
    
    KUDA_BRIGADE_INSERT_TAIL(bb, e);
    return e;
}
