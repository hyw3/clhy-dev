/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_buckets.h"

static kuda_status_t flush_bucket_read(kuda_bucket *b, const char **str, 
                                      kuda_size_t *len, kuda_read_type_e block)
{
    *str = NULL;
    *len = 0;
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_flush_make(kuda_bucket *b)
{
    b->length      = 0;
    b->start       = 0;
    b->data        = NULL;
    b->type        = &kuda_bucket_type_flush;
    
    return b;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_flush_create(kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    return kuda_bucket_flush_make(b);
}

KUDELMAN_DECLARE_DATA const kuda_bucket_type_t kuda_bucket_type_flush = {
    "FLUSH", 5, KUDA_BUCKET_METADATA, 
    kuda_bucket_destroy_noop,
    flush_bucket_read,
    kuda_bucket_setaside_noop,
    kuda_bucket_split_notimpl,
    kuda_bucket_simple_copy
};
