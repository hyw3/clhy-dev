/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_general.h"
#include "kuda_file_io.h"
#include "kuda_buckets.h"

#if KUDA_HAS_MMAP
#include "kuda_mmap.h"

/* mmap support for static files based on ideas from John Heidemann's
 * patch against 1.0.5.  See
 * <http://www.isi.edu/~johnh/SOFTWARE/CLHYKUDEL/index.html>.
 */

#endif /* KUDA_HAS_MMAP */

static void file_bucket_destroy(void *data)
{
    kuda_bucket_file *f = data;

    if (kuda_bucket_shared_destroy(f)) {
        /* no need to close the file here; it will get
         * done automatically when the pool gets cleaned up */
        kuda_bucket_free(f);
    }
}

#if KUDA_HAS_MMAP
static int file_make_mmap(kuda_bucket *e, kuda_size_t filelength,
                           kuda_off_t fileoffset, kuda_pool_t *p)
{
    kuda_bucket_file *a = e->data;
    kuda_mmap_t *mm;

    if (!a->can_mmap) {
        return 0;
    }

    if (filelength > KUDA_MMAP_LIMIT) {
        if (kuda_mmap_create(&mm, a->fd, fileoffset, KUDA_MMAP_LIMIT,
                            KUDA_MMAP_READ, p) != KUDA_SUCCESS)
        {
            return 0;
        }
        kuda_bucket_split(e, KUDA_MMAP_LIMIT);
        filelength = KUDA_MMAP_LIMIT;
    }
    else if ((filelength < KUDA_MMAP_THRESHOLD) ||
             (kuda_mmap_create(&mm, a->fd, fileoffset, filelength,
                              KUDA_MMAP_READ, p) != KUDA_SUCCESS))
    {
        return 0;
    }
    kuda_bucket_mmap_make(e, mm, 0, filelength);
    file_bucket_destroy(a);
    return 1;
}
#endif

static kuda_status_t file_bucket_read(kuda_bucket *e, const char **str,
                                     kuda_size_t *len, kuda_read_type_e block)
{
    kuda_bucket_file *a = e->data;
    kuda_file_t *f = a->fd;
    kuda_bucket *b = NULL;
    char *buf;
    kuda_status_t rv;
    kuda_size_t filelength = e->length;  /* bytes remaining in file past offset */
    kuda_off_t fileoffset = e->start;
#if KUDA_HAS_THREADS && !KUDA_HAS_XTHREAD_FILES
    kuda_int32_t flags;
#endif

#if KUDA_HAS_MMAP
    if (file_make_mmap(e, filelength, fileoffset, a->readpool)) {
        return kuda_bucket_read(e, str, len, block);
    }
#endif

#if KUDA_HAS_THREADS && !KUDA_HAS_XTHREAD_FILES
    if ((flags = kuda_file_flags_get(f)) & KUDA_FOPEN_XTHREAD) {
        /* this file descriptor is shared across multiple threads and
         * this PLATFORM doesn't support that natively, so as a workaround
         * we must reopen the file into a->readpool */
        const char *fname;
        kuda_file_name_get(&fname, f);

        rv = kuda_file_open(&f, fname, (flags & ~KUDA_FOPEN_XTHREAD), 0, a->readpool);
        if (rv != KUDA_SUCCESS)
            return rv;

        a->fd = f;
    }
#endif

    *str = NULL;  /* in case we die prematurely */
    *len = (filelength > a->read_size) ? a->read_size : filelength;
    buf = kuda_bucket_alloc(*len, e->list);

    /* Handle offset ... */
    rv = kuda_file_seek(f, KUDA_SET, &fileoffset);
    if (rv != KUDA_SUCCESS) {
        kuda_bucket_free(buf);
        return rv;
    }
    rv = kuda_file_read(f, buf, len);
    if (rv != KUDA_SUCCESS && rv != KUDA_EOF) {
        kuda_bucket_free(buf);
        return rv;
    }
    filelength -= *len;
    /*
     * Change the current bucket to refer to what we read,
     * even if we read nothing because we hit EOF.
     */
    kuda_bucket_heap_make(e, buf, *len, kuda_bucket_free);

    /* If we have more to read from the file, then create another bucket */
    if (filelength > 0 && rv != KUDA_EOF) {
        /* for efficiency, we can just build a new kuda_bucket struct
         * to wrap around the existing file bucket */
        b = kuda_bucket_alloc(sizeof(*b), e->list);
        b->start  = fileoffset + (*len);
        b->length = filelength;
        b->data   = a;
        b->type   = &kuda_bucket_type_file;
        b->free   = kuda_bucket_free;
        b->list   = e->list;
        KUDA_BUCKET_INSERT_AFTER(e, b);
    }
    else {
        file_bucket_destroy(a);
    }

    *str = buf;
    return rv;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_file_make(kuda_bucket *b, kuda_file_t *fd,
                                               kuda_off_t offset,
                                               kuda_size_t len, kuda_pool_t *p)
{
    kuda_bucket_file *f;

    f = kuda_bucket_alloc(sizeof(*f), b->list);
    f->fd = fd;
    f->readpool = p;
#if KUDA_HAS_MMAP
    f->can_mmap = 1;
#endif
    f->read_size = KUDA_BUCKET_BUFF_SIZE;

    b = kuda_bucket_shared_make(b, f, offset, len);
    b->type = &kuda_bucket_type_file;

    return b;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_file_create(kuda_file_t *fd,
                                                 kuda_off_t offset,
                                                 kuda_size_t len, kuda_pool_t *p,
                                                 kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    return kuda_bucket_file_make(b, fd, offset, len, p);
}

KUDELMAN_DECLARE(kuda_status_t) kuda_bucket_file_enable_mmap(kuda_bucket *e,
                                                      int enabled)
{
#if KUDA_HAS_MMAP
    kuda_bucket_file *a = e->data;
    a->can_mmap = enabled;
    return KUDA_SUCCESS;
#else
    return KUDA_ENOTIMPL;
#endif /* KUDA_HAS_MMAP */
}

KUDELMAN_DECLARE(kuda_status_t) kuda_bucket_file_set_buf_size(kuda_bucket *e,
                                                       kuda_size_t size)
{
    kuda_bucket_file *a = e->data;

    if (size <= KUDA_BUCKET_BUFF_SIZE) {
        a->read_size = KUDA_BUCKET_BUFF_SIZE;
    }
    else {
        kuda_size_t floor = kuda_bucket_alloc_aligned_floor(e->list, size);
        a->read_size = (size < floor) ? size : floor;
    }

    return KUDA_SUCCESS;
}

static kuda_status_t file_bucket_setaside(kuda_bucket *data, kuda_pool_t *reqpool)
{
    kuda_bucket_file *a = data->data;
    kuda_file_t *fd = NULL;
    kuda_file_t *f = a->fd;
    kuda_pool_t *curpool = kuda_file_pool_get(f);

    if (kuda_pool_is_ancestor(curpool, reqpool)) {
        return KUDA_SUCCESS;
    }

    if (!kuda_pool_is_ancestor(a->readpool, reqpool)) {
        a->readpool = reqpool;
    }

    kuda_file_setaside(&fd, f, reqpool);
    a->fd = fd;
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE_DATA const kuda_bucket_type_t kuda_bucket_type_file = {
    "FILE", 5, KUDA_BUCKET_DATA,
    file_bucket_destroy,
    file_bucket_read,
    file_bucket_setaside,
    kuda_bucket_shared_split,
    kuda_bucket_shared_copy
};
