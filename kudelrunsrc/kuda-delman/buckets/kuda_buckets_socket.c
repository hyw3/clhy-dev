/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_buckets.h"

static kuda_status_t socket_bucket_read(kuda_bucket *a, const char **str,
                                       kuda_size_t *len, kuda_read_type_e block)
{
    kuda_socket_t *p = a->data;
    char *buf;
    kuda_status_t rv;
    kuda_interval_time_t timeout;

    if (block == KUDA_NONBLOCK_READ) {
        kuda_socket_timeout_get(p, &timeout);
        kuda_socket_timeout_set(p, 0);
    }

    *str = NULL;
    *len = KUDA_BUCKET_BUFF_SIZE;
    buf = kuda_bucket_alloc(*len, a->list); /* XXX: check for failure? */

    rv = kuda_socket_recv(p, buf, len);

    if (block == KUDA_NONBLOCK_READ) {
        kuda_socket_timeout_set(p, timeout);
    }

    if (rv != KUDA_SUCCESS && rv != KUDA_EOF) {
        kuda_bucket_free(buf);
        return rv;
    }
    /*
     * If there's more to read we have to keep the rest of the socket
     * for later. XXX: Note that more complicated bucket types that
     * refer to data not in memory and must therefore have a read()
     * function similar to this one should be wary of copying this
     * code because if they have a destroy function they probably
     * want to migrate the bucket's subordinate structure from the
     * old bucket to a raw new one and adjust it as appropriate,
     * rather than destroying the old one and creating a completely
     * new bucket.
     *
     * Even if there is nothing more to read, don't close the socket here
     * as we have to use it to send any response :)  We could shut it 
     * down for reading, but there is no benefit to doing so.
     */
    if (*len > 0) {
        kuda_bucket_heap *h;
        /* Change the current bucket to refer to what we read */
        a = kuda_bucket_heap_make(a, buf, *len, kuda_bucket_free);
        h = a->data;
        h->alloc_len = KUDA_BUCKET_BUFF_SIZE; /* note the real buffer size */
        *str = buf;
        KUDA_BUCKET_INSERT_AFTER(a, kuda_bucket_socket_create(p, a->list));
    }
    else {
        kuda_bucket_free(buf);
        a = kuda_bucket_immortal_make(a, "", 0);
        *str = a->data;
    }
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_socket_make(kuda_bucket *b, kuda_socket_t *p)
{
    /*
     * XXX: We rely on a cleanup on some pool or other to actually
     * destroy the socket. We should probably explicitly call kuda to
     * destroy it instead.
     *
     * Note that typically the socket is allocated from the connection pool
     * so it will disappear when the connection is finished. 
     */
    b->type        = &kuda_bucket_type_socket;
    b->length      = (kuda_size_t)(-1);
    b->start       = -1;
    b->data        = p;

    return b;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_socket_create(kuda_socket_t *p,
                                                   kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    return kuda_bucket_socket_make(b, p);
}

KUDELMAN_DECLARE_DATA const kuda_bucket_type_t kuda_bucket_type_socket = {
    "SOCKET", 5, KUDA_BUCKET_DATA,
    kuda_bucket_destroy_noop,
    socket_bucket_read,
    kuda_bucket_setaside_notimpl, 
    kuda_bucket_split_notimpl,
    kuda_bucket_copy_notimpl
};
