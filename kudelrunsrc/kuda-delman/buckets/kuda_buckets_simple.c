/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_buckets.h"

KUDELMAN_DECLARE_NONSTD(kuda_status_t) kuda_bucket_simple_copy(kuda_bucket *a,
                                                        kuda_bucket **b)
{
    *b = kuda_bucket_alloc(sizeof(**b), a->list); /* XXX: check for failure? */
    **b = *a;

    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE_NONSTD(kuda_status_t) kuda_bucket_simple_split(kuda_bucket *a,
                                                         kuda_size_t point)
{
    kuda_bucket *b;

    if (point > a->length) {
        return KUDA_EINVAL;
    }

    kuda_bucket_simple_copy(a, &b);

    a->length  = point;
    b->length -= point;
    b->start  += point;

    KUDA_BUCKET_INSERT_AFTER(a, b);

    return KUDA_SUCCESS;
}

static kuda_status_t simple_bucket_read(kuda_bucket *b, const char **str, 
                                       kuda_size_t *len, kuda_read_type_e block)
{
    *str = (char *)b->data + b->start;
    *len = b->length;
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_immortal_make(kuda_bucket *b,
                                                   const char *buf,
                                                   kuda_size_t length)
{
    b->data   = (char *)buf;
    b->length = length;
    b->start  = 0;
    b->type   = &kuda_bucket_type_immortal;

    return b;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_immortal_create(const char *buf,
                                                     kuda_size_t length,
                                                     kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    return kuda_bucket_immortal_make(b, buf, length);
}

/*
 * XXX: This function could do with some tweaking to reduce memory
 * usage in various cases, e.g. share buffers in the heap between all
 * the buckets that are set aside, or even spool set-aside data to
 * disk if it gets too voluminous (but if it does then that's probably
 * a bug elsewhere). There should probably be a kuda_brigade_setaside()
 * function that co-ordinates the action of all the bucket setaside
 * functions to improve memory efficiency.
 */
static kuda_status_t transient_bucket_setaside(kuda_bucket *b, kuda_pool_t *pool)
{
    b = kuda_bucket_heap_make(b, (char *)b->data + b->start, b->length, NULL);
    if (b == NULL) {
        return KUDA_ENOMEM;
    }
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_transient_make(kuda_bucket *b,
                                                    const char *buf,
                                                    kuda_size_t length)
{
    b->data   = (char *)buf;
    b->length = length;
    b->start  = 0;
    b->type   = &kuda_bucket_type_transient;
    return b;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_transient_create(const char *buf,
                                                      kuda_size_t length,
                                                      kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    return kuda_bucket_transient_make(b, buf, length);
}

const kuda_bucket_type_t kuda_bucket_type_immortal = {
    "IMMORTAL", 5, KUDA_BUCKET_DATA,
    kuda_bucket_destroy_noop,
    simple_bucket_read,
    kuda_bucket_setaside_noop,
    kuda_bucket_simple_split,
    kuda_bucket_simple_copy
};

KUDELMAN_DECLARE_DATA const kuda_bucket_type_t kuda_bucket_type_transient = {
    "TRANSIENT", 5, KUDA_BUCKET_DATA,
    kuda_bucket_destroy_noop, 
    simple_bucket_read,
    transient_bucket_setaside,
    kuda_bucket_simple_split,
    kuda_bucket_simple_copy
};
