/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_buckets.h"
#define KUDA_WANT_MEMFUNC
#include "kuda_want.h"

static kuda_status_t pool_bucket_cleanup(void *data)
{
    kuda_bucket_pool *p = data;

    /*
     * If the pool gets cleaned up, we have to copy the data out
     * of the pool and onto the heap.  But the kuda_buckets out there
     * that point to this pool bucket need to be notified such that
     * they can morph themselves into a regular heap bucket the next
     * time they try to read.  To avoid having to manipulate
     * reference counts and b->data pointers, the kuda_bucket_pool
     * actually _contains_ an kuda_bucket_heap as its first element,
     * so the two share their kuda_bucket_refcount member, and you
     * can typecast a pool bucket struct to make it look like a
     * regular old heap bucket struct.
     */
    p->heap.base = kuda_bucket_alloc(p->heap.alloc_len, p->list);
    memcpy(p->heap.base, p->base, p->heap.alloc_len);
    p->base = NULL;
    p->pool = NULL;

    return KUDA_SUCCESS;
}

static kuda_status_t pool_bucket_read(kuda_bucket *b, const char **str, 
                                     kuda_size_t *len, kuda_read_type_e block)
{
    kuda_bucket_pool *p = b->data;
    const char *base = p->base;

    if (p->pool == NULL) {
        /*
         * pool has been cleaned up... masquerade as a heap bucket from now
         * on. subsequent bucket operations will use the heap bucket code.
         */
        b->type = &kuda_bucket_type_heap;
        base = p->heap.base;
    }
    *str = base + b->start;
    *len = b->length;
    return KUDA_SUCCESS;
}

static void pool_bucket_destroy(void *data)
{
    kuda_bucket_pool *p = data;

    /* If the pool is cleaned up before the last reference goes
     * away, the data is really now on the heap; heap_destroy() takes
     * over.  free() in heap_destroy() thinks it's freeing
     * an kuda_bucket_heap, when in reality it's freeing the whole
     * kuda_bucket_pool for us.
     */
    if (p->pool) {
        /* the shared resource is still in the pool
         * because the pool has not been cleaned up yet
         */
        if (kuda_bucket_shared_destroy(p)) {
            kuda_pool_cleanup_kill(p->pool, p, pool_bucket_cleanup);
            kuda_bucket_free(p);
        }
    }
    else {
        /* the shared resource is no longer in the pool, it's
         * on the heap, but this reference still thinks it's a pool
         * bucket.  we should just go ahead and pass control to
         * heap_destroy() for it since it doesn't know any better.
         */
        kuda_bucket_type_heap.destroy(p);
    }
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_pool_make(kuda_bucket *b,
                      const char *buf, kuda_size_t length, kuda_pool_t *pool)
{
    kuda_bucket_pool *p;

    p = kuda_bucket_alloc(sizeof(*p), b->list);

    /* XXX: we lose the const qualifier here which indicates
     * there's something screwy with the API...
     */
    /* XXX: why is this?  buf is const, p->base is const... what's
     * the problem?  --jcw */
    p->base = (char *) buf;
    p->pool = pool;
    p->list = b->list;

    b = kuda_bucket_shared_make(b, p, 0, length);
    b->type = &kuda_bucket_type_pool;

    /* pre-initialize heap bucket member */
    p->heap.alloc_len = length;
    p->heap.base      = NULL;
    p->heap.free_func = kuda_bucket_free;

    kuda_pool_cleanup_register(p->pool, p, pool_bucket_cleanup,
                              kuda_pool_cleanup_null);
    return b;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_pool_create(const char *buf,
                                                 kuda_size_t length,
                                                 kuda_pool_t *pool,
                                                 kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    return kuda_bucket_pool_make(b, buf, length, pool);
}

KUDELMAN_DECLARE_DATA const kuda_bucket_type_t kuda_bucket_type_pool = {
    "POOL", 5, KUDA_BUCKET_DATA,
    pool_bucket_destroy,
    pool_bucket_read,
    kuda_bucket_setaside_noop, /* don't need to setaside thanks to the cleanup*/
    kuda_bucket_shared_split,
    kuda_bucket_shared_copy
};
