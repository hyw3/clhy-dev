/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_buckets.h"
#define KUDA_WANT_MEMFUNC
#include "kuda_want.h"

static kuda_status_t heap_bucket_read(kuda_bucket *b, const char **str, 
                                     kuda_size_t *len, kuda_read_type_e block)
{
    kuda_bucket_heap *h = b->data;

    *str = h->base + b->start;
    *len = b->length;
    return KUDA_SUCCESS;
}

static void heap_bucket_destroy(void *data)
{
    kuda_bucket_heap *h = data;

    if (kuda_bucket_shared_destroy(h)) {
        (*h->free_func)(h->base);
        kuda_bucket_free(h);
    }
}

/* Warning: if you change this function, be sure to
 * change kuda_bucket_pool_make() too! */
KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_heap_make(kuda_bucket *b, const char *buf,
                                               kuda_size_t length,
                                               void (*free_func)(void *data))
{
    kuda_bucket_heap *h;

    h = kuda_bucket_alloc(sizeof(*h), b->list);

    if (!free_func) {
        h->alloc_len = length;
        h->base = kuda_bucket_alloc(h->alloc_len, b->list);
        if (h->base == NULL) {
            kuda_bucket_free(h);
            return NULL;
        }
        h->free_func = kuda_bucket_free;
        memcpy(h->base, buf, length);
    }
    else {
        /* XXX: we lose the const qualifier here which indicates
         * there's something screwy with the API...
         */
        h->base = (char *) buf;
        h->alloc_len = length;
        h->free_func = free_func;
    }

    b = kuda_bucket_shared_make(b, h, 0, length);
    b->type = &kuda_bucket_type_heap;

    return b;
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_heap_create(const char *buf,
                                                 kuda_size_t length,
                                                 void (*free_func)(void *data),
                                                 kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    return kuda_bucket_heap_make(b, buf, length, free_func);
}

KUDELMAN_DECLARE_DATA const kuda_bucket_type_t kuda_bucket_type_heap = {
    "HEAP", 5, KUDA_BUCKET_DATA,
    heap_bucket_destroy,
    heap_bucket_read,
    kuda_bucket_setaside_noop,
    kuda_bucket_shared_split,
    kuda_bucket_shared_copy
};
