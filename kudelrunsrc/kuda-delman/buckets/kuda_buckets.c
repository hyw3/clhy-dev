/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_buckets.h"

KUDELMAN_DECLARE_NONSTD(kuda_status_t) kuda_bucket_setaside_noop(kuda_bucket *data,
                                                          kuda_pool_t *pool)
{
    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE_NONSTD(kuda_status_t) kuda_bucket_setaside_notimpl(kuda_bucket *data,
                                                             kuda_pool_t *pool)
{
    return KUDA_ENOTIMPL;
}

KUDELMAN_DECLARE_NONSTD(kuda_status_t) kuda_bucket_split_notimpl(kuda_bucket *data,
                                                          kuda_size_t point)
{
    return KUDA_ENOTIMPL;
}

KUDELMAN_DECLARE_NONSTD(kuda_status_t) kuda_bucket_copy_notimpl(kuda_bucket *e,
                                                         kuda_bucket **c)
{
    return KUDA_ENOTIMPL;
}

KUDELMAN_DECLARE_NONSTD(void) kuda_bucket_destroy_noop(void *data)
{
    return;
}
