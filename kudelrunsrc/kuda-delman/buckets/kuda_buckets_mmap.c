/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_buckets.h"

#if KUDA_HAS_MMAP

static kuda_status_t mmap_bucket_read(kuda_bucket *b, const char **str, 
                                     kuda_size_t *length, kuda_read_type_e block)
{
    kuda_bucket_mmap *m = b->data;
    kuda_status_t ok;
    void *addr;
   
    if (!m->mmap) {
        /* the kuda_mmap_t was already cleaned up out from under us */
        return KUDA_EINVAL;
    }

    ok = kuda_mmap_offset(&addr, m->mmap, b->start);
    if (ok != KUDA_SUCCESS) {
        return ok;
    }
    *str = addr;
    *length = b->length;
    return KUDA_SUCCESS;
}

static kuda_status_t mmap_bucket_cleanup(void *data)
{
    /* the kuda_mmap_t is about to disappear out from under us, so we
     * have no choice but to pretend it doesn't exist anymore.  the
     * refcount is now useless because there's nothing to refer to
     * anymore.  so the only valid action on any remaining referrer
     * is to delete it.  no more reads, no more anything. */
    kuda_bucket_mmap *m = data;

    m->mmap = NULL;
    return KUDA_SUCCESS;
}

static void mmap_bucket_destroy(void *data)
{
    kuda_bucket_mmap *m = data;

    if (kuda_bucket_shared_destroy(m)) {
        if (m->mmap) {
            kuda_pool_cleanup_kill(m->mmap->cntxt, m, mmap_bucket_cleanup);
            kuda_mmap_delete(m->mmap);
        }
        kuda_bucket_free(m);
    }
}

/*
 * XXX: are the start and length arguments useful?
 */
KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_mmap_make(kuda_bucket *b, kuda_mmap_t *mm, 
                                               kuda_off_t start, 
                                               kuda_size_t length)
{
    kuda_bucket_mmap *m;

    m = kuda_bucket_alloc(sizeof(*m), b->list);
    m->mmap = mm;

    kuda_pool_cleanup_register(mm->cntxt, m, mmap_bucket_cleanup,
                              kuda_pool_cleanup_null);

    b = kuda_bucket_shared_make(b, m, start, length);
    b->type = &kuda_bucket_type_mmap;

    return b;
}


KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_mmap_create(kuda_mmap_t *mm, 
                                                 kuda_off_t start, 
                                                 kuda_size_t length,
                                                 kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    return kuda_bucket_mmap_make(b, mm, start, length);
}

static kuda_status_t mmap_bucket_setaside(kuda_bucket *b, kuda_pool_t *p)
{
    kuda_bucket_mmap *m = b->data;
    kuda_mmap_t *mm = m->mmap;
    kuda_mmap_t *new_mm;
    kuda_status_t ok;

    if (!mm) {
        /* the kuda_mmap_t was already cleaned up out from under us */
        return KUDA_EINVAL;
    }

    /* shortcut if possible */
    if (kuda_pool_is_ancestor(mm->cntxt, p)) {
        return KUDA_SUCCESS;
    }

    /* duplicate kuda_mmap_t into new pool */
    ok = kuda_mmap_dup(&new_mm, mm, p);
    if (ok != KUDA_SUCCESS) {
        return ok;
    }

    /* decrement refcount on old kuda_bucket_mmap */
    mmap_bucket_destroy(m);

    /* create new kuda_bucket_mmap pointing to new kuda_mmap_t */
    kuda_bucket_mmap_make(b, new_mm, b->start, b->length);

    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE_DATA const kuda_bucket_type_t kuda_bucket_type_mmap = {
    "MMAP", 5, KUDA_BUCKET_DATA,
    mmap_bucket_destroy,
    mmap_bucket_read,
    mmap_bucket_setaside,
    kuda_bucket_shared_split,
    kuda_bucket_shared_copy
};

#endif
