/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_buckets.h"

KUDELMAN_DECLARE_NONSTD(kuda_status_t) kuda_bucket_shared_split(kuda_bucket *a,
                                                         kuda_size_t point)
{
    kuda_bucket_refcount *r = a->data;
    kuda_status_t rv;

    if ((rv = kuda_bucket_simple_split(a, point)) != KUDA_SUCCESS) {
        return rv;
    }
    r->refcount++;

    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE_NONSTD(kuda_status_t) kuda_bucket_shared_copy(kuda_bucket *a,
                                                        kuda_bucket **b)
{
    kuda_bucket_refcount *r = a->data;

    kuda_bucket_simple_copy(a, b);
    r->refcount++;

    return KUDA_SUCCESS;
}

KUDELMAN_DECLARE(int) kuda_bucket_shared_destroy(void *data)
{
    kuda_bucket_refcount *r = data;
    r->refcount--;
    return (r->refcount == 0);
}

KUDELMAN_DECLARE(kuda_bucket *) kuda_bucket_shared_make(kuda_bucket *b, void *data,
                                                 kuda_off_t start,
                                                 kuda_size_t length)
{
    kuda_bucket_refcount *r = data;

    b->data   = r;
    b->start  = start;
    b->length = length;
    /* caller initializes the type field */
    r->refcount = 1;

    return b;
}
