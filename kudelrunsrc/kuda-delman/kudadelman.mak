# Microsoft Developer Studio Generated NMAKE File, Based on kudadelman.dsp
!IF "$(CFG)" == ""
CFG=kudadelman - Win32 Release
!MESSAGE No configuration specified. Defaulting to kudadelman - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "kudadelman - Win32 Release" && "$(CFG)" != "kudadelman - Win32 Debug" && "$(CFG)" != "kudadelman - x64 Release" && "$(CFG)" != "kudadelman - x64 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "kudadelman.mak" CFG="kudadelman - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "kudadelman - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "kudadelman - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "kudadelman - x64 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "kudadelman - x64 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF "$(_HAVE_OSSL110)" == "1"
SSLINC=/I ../openssl/include
!ELSE 
SSLINC=/I ../openssl/inc32
!ENDIF 

!IF  "$(CFG)" == "kudadelman - Win32 Release"

OUTDIR=.\LibR
INTDIR=.\LibR
# Begin Custom Macros
OutDir=.\LibR
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\kudadelman-1.lib"

!ELSE 

ALL : "kudaiconv - Win32 Release" "$(OUTDIR)\kudadelman-1.lib"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kudaiconv - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\kuda_base64.obj"
	-@erase "$(INTDIR)\kuda_brigade.obj"
	-@erase "$(INTDIR)\kuda_buckets.obj"
	-@erase "$(INTDIR)\kuda_buckets_alloc.obj"
	-@erase "$(INTDIR)\kuda_buckets_eos.obj"
	-@erase "$(INTDIR)\kuda_buckets_file.obj"
	-@erase "$(INTDIR)\kuda_buckets_flush.obj"
	-@erase "$(INTDIR)\kuda_buckets_heap.obj"
	-@erase "$(INTDIR)\kuda_buckets_mmap.obj"
	-@erase "$(INTDIR)\kuda_buckets_pipe.obj"
	-@erase "$(INTDIR)\kuda_buckets_pool.obj"
	-@erase "$(INTDIR)\kuda_buckets_refcount.obj"
	-@erase "$(INTDIR)\kuda_buckets_simple.obj"
	-@erase "$(INTDIR)\kuda_buckets_socket.obj"
	-@erase "$(INTDIR)\kuda_crypto.obj"
	-@erase "$(INTDIR)\kuda_date.obj"
	-@erase "$(INTDIR)\kuda_dbd.obj"
	-@erase "$(INTDIR)\kuda_dbd_mysql.obj"
	-@erase "$(INTDIR)\kuda_dbd_odbc.obj"
	-@erase "$(INTDIR)\kuda_dbd_oracle.obj"
	-@erase "$(INTDIR)\kuda_dbd_pgsql.obj"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2.obj"
	-@erase "$(INTDIR)\kuda_dbd_sqlite3.obj"
	-@erase "$(INTDIR)\kuda_dbm.obj"
	-@erase "$(INTDIR)\kuda_dbm_berkeleydb.obj"
	-@erase "$(INTDIR)\kuda_dbm_gdbm.obj"
	-@erase "$(INTDIR)\kuda_dbm_sdbm.obj"
	-@erase "$(INTDIR)\kuda_hooks.obj"
	-@erase "$(INTDIR)\kuda_ldap_init.obj"
	-@erase "$(INTDIR)\kuda_ldap_option.obj"
	-@erase "$(INTDIR)\kuda_ldap_rebind.obj"
	-@erase "$(INTDIR)\kuda_ldap_stub.obj"
	-@erase "$(INTDIR)\kuda_ldap_url.obj"
	-@erase "$(INTDIR)\kuda_md4.obj"
	-@erase "$(INTDIR)\kuda_md5.obj"
	-@erase "$(INTDIR)\kuda_memcache.obj"
	-@erase "$(INTDIR)\kuda_passwd.obj"
	-@erase "$(INTDIR)\kuda_queue.obj"
	-@erase "$(INTDIR)\kuda_redis.obj"
	-@erase "$(INTDIR)\kuda_reslist.obj"
	-@erase "$(INTDIR)\kuda_rmm.obj"
	-@erase "$(INTDIR)\kuda_sha1.obj"
	-@erase "$(INTDIR)\kuda_siphash.obj"
	-@erase "$(INTDIR)\kuda_strmatch.obj"
	-@erase "$(INTDIR)\kuda_thread_pool.obj"
	-@erase "$(INTDIR)\kuda_uri.obj"
	-@erase "$(INTDIR)\kuda_xml.obj"
	-@erase "$(INTDIR)\kudadelman-1.idb"
	-@erase "$(INTDIR)\kudadelman-1.pdb"
	-@erase "$(INTDIR)\kudelman_dso.obj"
	-@erase "$(INTDIR)\kudelman_version.obj"
	-@erase "$(INTDIR)\crypt_blowfish.obj"
	-@erase "$(INTDIR)\getuuid.obj"
	-@erase "$(INTDIR)\sdbm.obj"
	-@erase "$(INTDIR)\sdbm_hash.obj"
	-@erase "$(INTDIR)\sdbm_lock.obj"
	-@erase "$(INTDIR)\sdbm_pair.obj"
	-@erase "$(INTDIR)\uuid.obj"
	-@erase "$(INTDIR)\xlate.obj"
	-@erase "$(OUTDIR)\kudadelman-1.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "./include" /I "../kuda/include" /I "./include/private" /I "../kuda-iconv/include" /I "./dbm/sdbm" /I "./xml/expat/lib" $(SSLINC) /D "NDEBUG" /D "KUDA_DECLARE_STATIC" /D "KUDELMAN_DECLARE_STATIC" /D "API_DECLARE_STATIC" /D "KUDELMAN_USE_SDBM" /D "HAVE_SQL_H" /D "XML_STATIC" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(OUTDIR)\kudadelman-1" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\kudadelman.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"$(OUTDIR)\kudadelman-1.lib" 
LIB32_OBJS= \
	"$(INTDIR)\kuda_brigade.obj" \
	"$(INTDIR)\kuda_buckets.obj" \
	"$(INTDIR)\kuda_buckets_alloc.obj" \
	"$(INTDIR)\kuda_buckets_eos.obj" \
	"$(INTDIR)\kuda_buckets_file.obj" \
	"$(INTDIR)\kuda_buckets_flush.obj" \
	"$(INTDIR)\kuda_buckets_heap.obj" \
	"$(INTDIR)\kuda_buckets_mmap.obj" \
	"$(INTDIR)\kuda_buckets_pipe.obj" \
	"$(INTDIR)\kuda_buckets_pool.obj" \
	"$(INTDIR)\kuda_buckets_refcount.obj" \
	"$(INTDIR)\kuda_buckets_simple.obj" \
	"$(INTDIR)\kuda_buckets_socket.obj" \
	"$(INTDIR)\kuda_crypto.obj" \
	"$(INTDIR)\kuda_md4.obj" \
	"$(INTDIR)\kuda_md5.obj" \
	"$(INTDIR)\kuda_passwd.obj" \
	"$(INTDIR)\kuda_sha1.obj" \
	"$(INTDIR)\kuda_siphash.obj" \
	"$(INTDIR)\crypt_blowfish.obj" \
	"$(INTDIR)\getuuid.obj" \
	"$(INTDIR)\uuid.obj" \
	"$(INTDIR)\kuda_dbd.obj" \
	"$(INTDIR)\kuda_dbd_mysql.obj" \
	"$(INTDIR)\kuda_dbd_odbc.obj" \
	"$(INTDIR)\kuda_dbd_oracle.obj" \
	"$(INTDIR)\kuda_dbd_pgsql.obj" \
	"$(INTDIR)\kuda_dbd_sqlite2.obj" \
	"$(INTDIR)\kuda_dbd_sqlite3.obj" \
	"$(INTDIR)\kuda_dbm.obj" \
	"$(INTDIR)\kuda_dbm_berkeleydb.obj" \
	"$(INTDIR)\kuda_dbm_gdbm.obj" \
	"$(INTDIR)\kuda_dbm_sdbm.obj" \
	"$(INTDIR)\kuda_base64.obj" \
	"$(INTDIR)\kuda_hooks.obj" \
	"$(INTDIR)\kuda_ldap_init.obj" \
	"$(INTDIR)\kuda_ldap_option.obj" \
	"$(INTDIR)\kuda_ldap_rebind.obj" \
	"$(INTDIR)\kuda_ldap_stub.obj" \
	"$(INTDIR)\kuda_ldap_url.obj" \
	"$(INTDIR)\kuda_memcache.obj" \
	"$(INTDIR)\kuda_date.obj" \
	"$(INTDIR)\kudelman_dso.obj" \
	"$(INTDIR)\kuda_queue.obj" \
	"$(INTDIR)\kuda_redis.obj" \
	"$(INTDIR)\kuda_reslist.obj" \
	"$(INTDIR)\kuda_rmm.obj" \
	"$(INTDIR)\kuda_thread_pool.obj" \
	"$(INTDIR)\kudelman_version.obj" \
	"$(INTDIR)\sdbm.obj" \
	"$(INTDIR)\sdbm_hash.obj" \
	"$(INTDIR)\sdbm_lock.obj" \
	"$(INTDIR)\sdbm_pair.obj" \
	"$(INTDIR)\kuda_strmatch.obj" \
	"$(INTDIR)\kuda_uri.obj" \
	"$(INTDIR)\xlate.obj" \
	"$(INTDIR)\kuda_xml.obj" \
	"..\kuda-iconv\LibR\kudaiconv-1.lib"

"$(OUTDIR)\kudadelman-1.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "kudadelman - Win32 Debug"

OUTDIR=.\LibD
INTDIR=.\LibD
# Begin Custom Macros
OutDir=.\LibD
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : ".\include\private\kudelman_select_dbm.h" ".\include\private\kudelman_config.h" ".\include\kudelman_want.h" ".\include\kudelman.h" ".\include\kuda_ldap.h" "$(OUTDIR)\kudadelman-1.lib"

!ELSE 

ALL : "kudaiconv - Win32 Debug" ".\include\private\kudelman_select_dbm.h" ".\include\private\kudelman_config.h" ".\include\kudelman_want.h" ".\include\kudelman.h" ".\include\kuda_ldap.h" "$(OUTDIR)\kudadelman-1.lib"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kudaiconv - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\kuda_base64.obj"
	-@erase "$(INTDIR)\kuda_brigade.obj"
	-@erase "$(INTDIR)\kuda_buckets.obj"
	-@erase "$(INTDIR)\kuda_buckets_alloc.obj"
	-@erase "$(INTDIR)\kuda_buckets_eos.obj"
	-@erase "$(INTDIR)\kuda_buckets_file.obj"
	-@erase "$(INTDIR)\kuda_buckets_flush.obj"
	-@erase "$(INTDIR)\kuda_buckets_heap.obj"
	-@erase "$(INTDIR)\kuda_buckets_mmap.obj"
	-@erase "$(INTDIR)\kuda_buckets_pipe.obj"
	-@erase "$(INTDIR)\kuda_buckets_pool.obj"
	-@erase "$(INTDIR)\kuda_buckets_refcount.obj"
	-@erase "$(INTDIR)\kuda_buckets_simple.obj"
	-@erase "$(INTDIR)\kuda_buckets_socket.obj"
	-@erase "$(INTDIR)\kuda_crypto.obj"
	-@erase "$(INTDIR)\kuda_date.obj"
	-@erase "$(INTDIR)\kuda_dbd.obj"
	-@erase "$(INTDIR)\kuda_dbd_mysql.obj"
	-@erase "$(INTDIR)\kuda_dbd_odbc.obj"
	-@erase "$(INTDIR)\kuda_dbd_oracle.obj"
	-@erase "$(INTDIR)\kuda_dbd_pgsql.obj"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2.obj"
	-@erase "$(INTDIR)\kuda_dbd_sqlite3.obj"
	-@erase "$(INTDIR)\kuda_dbm.obj"
	-@erase "$(INTDIR)\kuda_dbm_berkeleydb.obj"
	-@erase "$(INTDIR)\kuda_dbm_gdbm.obj"
	-@erase "$(INTDIR)\kuda_dbm_sdbm.obj"
	-@erase "$(INTDIR)\kuda_hooks.obj"
	-@erase "$(INTDIR)\kuda_ldap_init.obj"
	-@erase "$(INTDIR)\kuda_ldap_option.obj"
	-@erase "$(INTDIR)\kuda_ldap_rebind.obj"
	-@erase "$(INTDIR)\kuda_ldap_stub.obj"
	-@erase "$(INTDIR)\kuda_ldap_url.obj"
	-@erase "$(INTDIR)\kuda_md4.obj"
	-@erase "$(INTDIR)\kuda_md5.obj"
	-@erase "$(INTDIR)\kuda_memcache.obj"
	-@erase "$(INTDIR)\kuda_passwd.obj"
	-@erase "$(INTDIR)\kuda_queue.obj"
	-@erase "$(INTDIR)\kuda_redis.obj"
	-@erase "$(INTDIR)\kuda_reslist.obj"
	-@erase "$(INTDIR)\kuda_rmm.obj"
	-@erase "$(INTDIR)\kuda_sha1.obj"
	-@erase "$(INTDIR)\kuda_siphash.obj"
	-@erase "$(INTDIR)\kuda_strmatch.obj"
	-@erase "$(INTDIR)\kuda_thread_pool.obj"
	-@erase "$(INTDIR)\kuda_uri.obj"
	-@erase "$(INTDIR)\kuda_xml.obj"
	-@erase "$(INTDIR)\kudadelman-1.idb"
	-@erase "$(INTDIR)\kudadelman-1.pdb"
	-@erase "$(INTDIR)\kudelman_dso.obj"
	-@erase "$(INTDIR)\kudelman_version.obj"
	-@erase "$(INTDIR)\crypt_blowfish.obj"
	-@erase "$(INTDIR)\getuuid.obj"
	-@erase "$(INTDIR)\sdbm.obj"
	-@erase "$(INTDIR)\sdbm_hash.obj"
	-@erase "$(INTDIR)\sdbm_lock.obj"
	-@erase "$(INTDIR)\sdbm_pair.obj"
	-@erase "$(INTDIR)\uuid.obj"
	-@erase "$(INTDIR)\xlate.obj"
	-@erase "$(OUTDIR)\kudadelman-1.lib"
	-@erase ".\include\kuda_ldap.h"
	-@erase ".\include\kudelman.h"
	-@erase ".\include\kudelman_want.h"
	-@erase ".\include\private\kudelman_config.h"
	-@erase ".\include\private\kudelman_select_dbm.h"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "./include" /I "../kuda/include" /I "./include/private" /I "../kuda-iconv/include" /I "./dbm/sdbm" /I "./xml/expat/lib"  $(SSLINC)/D "_DEBUG" /D "KUDA_DECLARE_STATIC" /D "KUDELMAN_DECLARE_STATIC" /D "API_DECLARE_STATIC" /D "KUDELMAN_USE_SDBM" /D "HAVE_SQL_H" /D "XML_STATIC" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(OUTDIR)\kudadelman-1" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\kudadelman.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"$(OUTDIR)\kudadelman-1.lib" 
LIB32_OBJS= \
	"$(INTDIR)\kuda_brigade.obj" \
	"$(INTDIR)\kuda_buckets.obj" \
	"$(INTDIR)\kuda_buckets_alloc.obj" \
	"$(INTDIR)\kuda_buckets_eos.obj" \
	"$(INTDIR)\kuda_buckets_file.obj" \
	"$(INTDIR)\kuda_buckets_flush.obj" \
	"$(INTDIR)\kuda_buckets_heap.obj" \
	"$(INTDIR)\kuda_buckets_mmap.obj" \
	"$(INTDIR)\kuda_buckets_pipe.obj" \
	"$(INTDIR)\kuda_buckets_pool.obj" \
	"$(INTDIR)\kuda_buckets_refcount.obj" \
	"$(INTDIR)\kuda_buckets_simple.obj" \
	"$(INTDIR)\kuda_buckets_socket.obj" \
	"$(INTDIR)\kuda_crypto.obj" \
	"$(INTDIR)\kuda_md4.obj" \
	"$(INTDIR)\kuda_md5.obj" \
	"$(INTDIR)\kuda_passwd.obj" \
	"$(INTDIR)\kuda_sha1.obj" \
	"$(INTDIR)\kuda_siphash.obj" \
	"$(INTDIR)\crypt_blowfish.obj" \
	"$(INTDIR)\getuuid.obj" \
	"$(INTDIR)\uuid.obj" \
	"$(INTDIR)\kuda_dbd.obj" \
	"$(INTDIR)\kuda_dbd_mysql.obj" \
	"$(INTDIR)\kuda_dbd_odbc.obj" \
	"$(INTDIR)\kuda_dbd_oracle.obj" \
	"$(INTDIR)\kuda_dbd_pgsql.obj" \
	"$(INTDIR)\kuda_dbd_sqlite2.obj" \
	"$(INTDIR)\kuda_dbd_sqlite3.obj" \
	"$(INTDIR)\kuda_dbm.obj" \
	"$(INTDIR)\kuda_dbm_berkeleydb.obj" \
	"$(INTDIR)\kuda_dbm_gdbm.obj" \
	"$(INTDIR)\kuda_dbm_sdbm.obj" \
	"$(INTDIR)\kuda_base64.obj" \
	"$(INTDIR)\kuda_hooks.obj" \
	"$(INTDIR)\kuda_ldap_init.obj" \
	"$(INTDIR)\kuda_ldap_option.obj" \
	"$(INTDIR)\kuda_ldap_rebind.obj" \
	"$(INTDIR)\kuda_ldap_stub.obj" \
	"$(INTDIR)\kuda_ldap_url.obj" \
	"$(INTDIR)\kuda_memcache.obj" \
	"$(INTDIR)\kuda_date.obj" \
	"$(INTDIR)\kudelman_dso.obj" \
	"$(INTDIR)\kuda_queue.obj" \
	"$(INTDIR)\kuda_redis.obj" \
	"$(INTDIR)\kuda_reslist.obj" \
	"$(INTDIR)\kuda_rmm.obj" \
	"$(INTDIR)\kuda_thread_pool.obj" \
	"$(INTDIR)\kudelman_version.obj" \
	"$(INTDIR)\sdbm.obj" \
	"$(INTDIR)\sdbm_hash.obj" \
	"$(INTDIR)\sdbm_lock.obj" \
	"$(INTDIR)\sdbm_pair.obj" \
	"$(INTDIR)\kuda_strmatch.obj" \
	"$(INTDIR)\kuda_uri.obj" \
	"$(INTDIR)\xlate.obj" \
	"$(INTDIR)\kuda_xml.obj" \
	"..\kuda-iconv\LibD\kudaiconv-1.lib"

"$(OUTDIR)\kudadelman-1.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "kudadelman - x64 Release"

OUTDIR=.\x64\LibR
INTDIR=.\x64\LibR
# Begin Custom Macros
OutDir=.\x64\LibR
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : ".\include\private\kudelman_select_dbm.h" ".\include\private\kudelman_config.h" ".\include\kudelman_want.h" ".\include\kudelman.h" ".\include\kuda_ldap.h" "$(OUTDIR)\kudadelman-1.lib"

!ELSE 

ALL : "kudaiconv - x64 Release" ".\include\private\kudelman_select_dbm.h" ".\include\private\kudelman_config.h" ".\include\kudelman_want.h" ".\include\kudelman.h" ".\include\kuda_ldap.h" "$(OUTDIR)\kudadelman-1.lib"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kudaiconv - x64 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\kuda_base64.obj"
	-@erase "$(INTDIR)\kuda_brigade.obj"
	-@erase "$(INTDIR)\kuda_buckets.obj"
	-@erase "$(INTDIR)\kuda_buckets_alloc.obj"
	-@erase "$(INTDIR)\kuda_buckets_eos.obj"
	-@erase "$(INTDIR)\kuda_buckets_file.obj"
	-@erase "$(INTDIR)\kuda_buckets_flush.obj"
	-@erase "$(INTDIR)\kuda_buckets_heap.obj"
	-@erase "$(INTDIR)\kuda_buckets_mmap.obj"
	-@erase "$(INTDIR)\kuda_buckets_pipe.obj"
	-@erase "$(INTDIR)\kuda_buckets_pool.obj"
	-@erase "$(INTDIR)\kuda_buckets_refcount.obj"
	-@erase "$(INTDIR)\kuda_buckets_simple.obj"
	-@erase "$(INTDIR)\kuda_buckets_socket.obj"
	-@erase "$(INTDIR)\kuda_crypto.obj"
	-@erase "$(INTDIR)\kuda_date.obj"
	-@erase "$(INTDIR)\kuda_dbd.obj"
	-@erase "$(INTDIR)\kuda_dbd_mysql.obj"
	-@erase "$(INTDIR)\kuda_dbd_odbc.obj"
	-@erase "$(INTDIR)\kuda_dbd_oracle.obj"
	-@erase "$(INTDIR)\kuda_dbd_pgsql.obj"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2.obj"
	-@erase "$(INTDIR)\kuda_dbd_sqlite3.obj"
	-@erase "$(INTDIR)\kuda_dbm.obj"
	-@erase "$(INTDIR)\kuda_dbm_berkeleydb.obj"
	-@erase "$(INTDIR)\kuda_dbm_gdbm.obj"
	-@erase "$(INTDIR)\kuda_dbm_sdbm.obj"
	-@erase "$(INTDIR)\kuda_hooks.obj"
	-@erase "$(INTDIR)\kuda_ldap_init.obj"
	-@erase "$(INTDIR)\kuda_ldap_option.obj"
	-@erase "$(INTDIR)\kuda_ldap_rebind.obj"
	-@erase "$(INTDIR)\kuda_ldap_stub.obj"
	-@erase "$(INTDIR)\kuda_ldap_url.obj"
	-@erase "$(INTDIR)\kuda_md4.obj"
	-@erase "$(INTDIR)\kuda_md5.obj"
	-@erase "$(INTDIR)\kuda_memcache.obj"
	-@erase "$(INTDIR)\kuda_passwd.obj"
	-@erase "$(INTDIR)\kuda_queue.obj"
	-@erase "$(INTDIR)\kuda_redis.obj"
	-@erase "$(INTDIR)\kuda_reslist.obj"
	-@erase "$(INTDIR)\kuda_rmm.obj"
	-@erase "$(INTDIR)\kuda_sha1.obj"
	-@erase "$(INTDIR)\kuda_siphash.obj"
	-@erase "$(INTDIR)\kuda_strmatch.obj"
	-@erase "$(INTDIR)\kuda_thread_pool.obj"
	-@erase "$(INTDIR)\kuda_uri.obj"
	-@erase "$(INTDIR)\kuda_xml.obj"
	-@erase "$(INTDIR)\kudadelman-1.idb"
	-@erase "$(INTDIR)\kudadelman-1.pdb"
	-@erase "$(INTDIR)\kudelman_dso.obj"
	-@erase "$(INTDIR)\kudelman_version.obj"
	-@erase "$(INTDIR)\crypt_blowfish.obj"
	-@erase "$(INTDIR)\getuuid.obj"
	-@erase "$(INTDIR)\sdbm.obj"
	-@erase "$(INTDIR)\sdbm_hash.obj"
	-@erase "$(INTDIR)\sdbm_lock.obj"
	-@erase "$(INTDIR)\sdbm_pair.obj"
	-@erase "$(INTDIR)\uuid.obj"
	-@erase "$(INTDIR)\xlate.obj"
	-@erase "$(OUTDIR)\kudadelman-1.lib"
	-@erase ".\include\kuda_ldap.h"
	-@erase ".\include\kudelman.h"
	-@erase ".\include\kudelman_want.h"
	-@erase ".\include\private\kudelman_config.h"
	-@erase ".\include\private\kudelman_select_dbm.h"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "./include" /I "../kuda/include" /I "./include/private" /I "../kuda-iconv/include" /I "./dbm/sdbm" /I "./xml/expat/lib" $(SSLINC) /D "NDEBUG" /D "KUDA_DECLARE_STATIC" /D "KUDELMAN_DECLARE_STATIC" /D "API_DECLARE_STATIC" /D "KUDELMAN_USE_SDBM" /D "HAVE_SQL_H" /D "XML_STATIC" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(OUTDIR)\kudadelman-1" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\kudadelman.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"$(OUTDIR)\kudadelman-1.lib" 
LIB32_OBJS= \
	"$(INTDIR)\kuda_brigade.obj" \
	"$(INTDIR)\kuda_buckets.obj" \
	"$(INTDIR)\kuda_buckets_alloc.obj" \
	"$(INTDIR)\kuda_buckets_eos.obj" \
	"$(INTDIR)\kuda_buckets_file.obj" \
	"$(INTDIR)\kuda_buckets_flush.obj" \
	"$(INTDIR)\kuda_buckets_heap.obj" \
	"$(INTDIR)\kuda_buckets_mmap.obj" \
	"$(INTDIR)\kuda_buckets_pipe.obj" \
	"$(INTDIR)\kuda_buckets_pool.obj" \
	"$(INTDIR)\kuda_buckets_refcount.obj" \
	"$(INTDIR)\kuda_buckets_simple.obj" \
	"$(INTDIR)\kuda_buckets_socket.obj" \
	"$(INTDIR)\kuda_crypto.obj" \
	"$(INTDIR)\kuda_md4.obj" \
	"$(INTDIR)\kuda_md5.obj" \
	"$(INTDIR)\kuda_passwd.obj" \
	"$(INTDIR)\kuda_sha1.obj" \
	"$(INTDIR)\kuda_siphash.obj" \
	"$(INTDIR)\crypt_blowfish.obj" \
	"$(INTDIR)\getuuid.obj" \
	"$(INTDIR)\uuid.obj" \
	"$(INTDIR)\kuda_dbd.obj" \
	"$(INTDIR)\kuda_dbd_mysql.obj" \
	"$(INTDIR)\kuda_dbd_odbc.obj" \
	"$(INTDIR)\kuda_dbd_oracle.obj" \
	"$(INTDIR)\kuda_dbd_pgsql.obj" \
	"$(INTDIR)\kuda_dbd_sqlite2.obj" \
	"$(INTDIR)\kuda_dbd_sqlite3.obj" \
	"$(INTDIR)\kuda_dbm.obj" \
	"$(INTDIR)\kuda_dbm_berkeleydb.obj" \
	"$(INTDIR)\kuda_dbm_gdbm.obj" \
	"$(INTDIR)\kuda_dbm_sdbm.obj" \
	"$(INTDIR)\kuda_base64.obj" \
	"$(INTDIR)\kuda_hooks.obj" \
	"$(INTDIR)\kuda_ldap_init.obj" \
	"$(INTDIR)\kuda_ldap_option.obj" \
	"$(INTDIR)\kuda_ldap_rebind.obj" \
	"$(INTDIR)\kuda_ldap_stub.obj" \
	"$(INTDIR)\kuda_ldap_url.obj" \
	"$(INTDIR)\kuda_memcache.obj" \
	"$(INTDIR)\kuda_date.obj" \
	"$(INTDIR)\kudelman_dso.obj" \
	"$(INTDIR)\kuda_queue.obj" \
	"$(INTDIR)\kuda_redis.obj" \
	"$(INTDIR)\kuda_reslist.obj" \
	"$(INTDIR)\kuda_rmm.obj" \
	"$(INTDIR)\kuda_thread_pool.obj" \
	"$(INTDIR)\kudelman_version.obj" \
	"$(INTDIR)\sdbm.obj" \
	"$(INTDIR)\sdbm_hash.obj" \
	"$(INTDIR)\sdbm_lock.obj" \
	"$(INTDIR)\sdbm_pair.obj" \
	"$(INTDIR)\kuda_strmatch.obj" \
	"$(INTDIR)\kuda_uri.obj" \
	"$(INTDIR)\xlate.obj" \
	"$(INTDIR)\kuda_xml.obj" \
	"..\kuda-iconv\x64\LibR\kudaiconv-1.lib"

"$(OUTDIR)\kudadelman-1.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "kudadelman - x64 Debug"

OUTDIR=.\x64\LibD
INTDIR=.\x64\LibD
# Begin Custom Macros
OutDir=.\x64\LibD
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : ".\include\private\kudelman_select_dbm.h" ".\include\private\kudelman_config.h" ".\include\kudelman_want.h" ".\include\kudelman.h" ".\include\kuda_ldap.h" "$(OUTDIR)\kudadelman-1.lib"

!ELSE 

ALL : "kudaiconv - x64 Debug" ".\include\private\kudelman_select_dbm.h" ".\include\private\kudelman_config.h" ".\include\kudelman_want.h" ".\include\kudelman.h" ".\include\kuda_ldap.h" "$(OUTDIR)\kudadelman-1.lib"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kudaiconv - x64 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\kuda_base64.obj"
	-@erase "$(INTDIR)\kuda_brigade.obj"
	-@erase "$(INTDIR)\kuda_buckets.obj"
	-@erase "$(INTDIR)\kuda_buckets_alloc.obj"
	-@erase "$(INTDIR)\kuda_buckets_eos.obj"
	-@erase "$(INTDIR)\kuda_buckets_file.obj"
	-@erase "$(INTDIR)\kuda_buckets_flush.obj"
	-@erase "$(INTDIR)\kuda_buckets_heap.obj"
	-@erase "$(INTDIR)\kuda_buckets_mmap.obj"
	-@erase "$(INTDIR)\kuda_buckets_pipe.obj"
	-@erase "$(INTDIR)\kuda_buckets_pool.obj"
	-@erase "$(INTDIR)\kuda_buckets_refcount.obj"
	-@erase "$(INTDIR)\kuda_buckets_simple.obj"
	-@erase "$(INTDIR)\kuda_buckets_socket.obj"
	-@erase "$(INTDIR)\kuda_crypto.obj"
	-@erase "$(INTDIR)\kuda_date.obj"
	-@erase "$(INTDIR)\kuda_dbd.obj"
	-@erase "$(INTDIR)\kuda_dbd_mysql.obj"
	-@erase "$(INTDIR)\kuda_dbd_odbc.obj"
	-@erase "$(INTDIR)\kuda_dbd_oracle.obj"
	-@erase "$(INTDIR)\kuda_dbd_pgsql.obj"
	-@erase "$(INTDIR)\kuda_dbd_sqlite2.obj"
	-@erase "$(INTDIR)\kuda_dbd_sqlite3.obj"
	-@erase "$(INTDIR)\kuda_dbm.obj"
	-@erase "$(INTDIR)\kuda_dbm_berkeleydb.obj"
	-@erase "$(INTDIR)\kuda_dbm_gdbm.obj"
	-@erase "$(INTDIR)\kuda_dbm_sdbm.obj"
	-@erase "$(INTDIR)\kuda_hooks.obj"
	-@erase "$(INTDIR)\kuda_ldap_init.obj"
	-@erase "$(INTDIR)\kuda_ldap_option.obj"
	-@erase "$(INTDIR)\kuda_ldap_rebind.obj"
	-@erase "$(INTDIR)\kuda_ldap_stub.obj"
	-@erase "$(INTDIR)\kuda_ldap_url.obj"
	-@erase "$(INTDIR)\kuda_md4.obj"
	-@erase "$(INTDIR)\kuda_md5.obj"
	-@erase "$(INTDIR)\kuda_memcache.obj"
	-@erase "$(INTDIR)\kuda_passwd.obj"
	-@erase "$(INTDIR)\kuda_queue.obj"
	-@erase "$(INTDIR)\kuda_redis.obj"
	-@erase "$(INTDIR)\kuda_reslist.obj"
	-@erase "$(INTDIR)\kuda_rmm.obj"
	-@erase "$(INTDIR)\kuda_sha1.obj"
	-@erase "$(INTDIR)\kuda_siphash.obj"
	-@erase "$(INTDIR)\kuda_strmatch.obj"
	-@erase "$(INTDIR)\kuda_thread_pool.obj"
	-@erase "$(INTDIR)\kuda_uri.obj"
	-@erase "$(INTDIR)\kuda_xml.obj"
	-@erase "$(INTDIR)\kudadelman-1.idb"
	-@erase "$(INTDIR)\kudadelman-1.pdb"
	-@erase "$(INTDIR)\kudelman_dso.obj"
	-@erase "$(INTDIR)\kudelman_version.obj"
	-@erase "$(INTDIR)\crypt_blowfish.obj"
	-@erase "$(INTDIR)\getuuid.obj"
	-@erase "$(INTDIR)\sdbm.obj"
	-@erase "$(INTDIR)\sdbm_hash.obj"
	-@erase "$(INTDIR)\sdbm_lock.obj"
	-@erase "$(INTDIR)\sdbm_pair.obj"
	-@erase "$(INTDIR)\uuid.obj"
	-@erase "$(INTDIR)\xlate.obj"
	-@erase "$(OUTDIR)\kudadelman-1.lib"
	-@erase ".\include\kuda_ldap.h"
	-@erase ".\include\kudelman.h"
	-@erase ".\include\kudelman_want.h"
	-@erase ".\include\private\kudelman_config.h"
	-@erase ".\include\private\kudelman_select_dbm.h"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "./include" /I "../kuda/include" /I "./include/private" /I "../kuda-iconv/include" /I "./dbm/sdbm" /I "./xml/expat/lib" $(SSLINC) /D "_DEBUG" /D "KUDA_DECLARE_STATIC" /D "KUDELMAN_DECLARE_STATIC" /D "API_DECLARE_STATIC" /D "KUDELMAN_USE_SDBM" /D "HAVE_SQL_H" /D "XML_STATIC" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(OUTDIR)\kudadelman-1" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\kudadelman.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"$(OUTDIR)\kudadelman-1.lib" 
LIB32_OBJS= \
	"$(INTDIR)\kuda_brigade.obj" \
	"$(INTDIR)\kuda_buckets.obj" \
	"$(INTDIR)\kuda_buckets_alloc.obj" \
	"$(INTDIR)\kuda_buckets_eos.obj" \
	"$(INTDIR)\kuda_buckets_file.obj" \
	"$(INTDIR)\kuda_buckets_flush.obj" \
	"$(INTDIR)\kuda_buckets_heap.obj" \
	"$(INTDIR)\kuda_buckets_mmap.obj" \
	"$(INTDIR)\kuda_buckets_pipe.obj" \
	"$(INTDIR)\kuda_buckets_pool.obj" \
	"$(INTDIR)\kuda_buckets_refcount.obj" \
	"$(INTDIR)\kuda_buckets_simple.obj" \
	"$(INTDIR)\kuda_buckets_socket.obj" \
	"$(INTDIR)\kuda_crypto.obj" \
	"$(INTDIR)\kuda_md4.obj" \
	"$(INTDIR)\kuda_md5.obj" \
	"$(INTDIR)\kuda_passwd.obj" \
	"$(INTDIR)\kuda_sha1.obj" \
	"$(INTDIR)\kuda_siphash.obj" \
	"$(INTDIR)\crypt_blowfish.obj" \
	"$(INTDIR)\getuuid.obj" \
	"$(INTDIR)\uuid.obj" \
	"$(INTDIR)\kuda_dbd.obj" \
	"$(INTDIR)\kuda_dbd_mysql.obj" \
	"$(INTDIR)\kuda_dbd_odbc.obj" \
	"$(INTDIR)\kuda_dbd_oracle.obj" \
	"$(INTDIR)\kuda_dbd_pgsql.obj" \
	"$(INTDIR)\kuda_dbd_sqlite2.obj" \
	"$(INTDIR)\kuda_dbd_sqlite3.obj" \
	"$(INTDIR)\kuda_dbm.obj" \
	"$(INTDIR)\kuda_dbm_berkeleydb.obj" \
	"$(INTDIR)\kuda_dbm_gdbm.obj" \
	"$(INTDIR)\kuda_dbm_sdbm.obj" \
	"$(INTDIR)\kuda_base64.obj" \
	"$(INTDIR)\kuda_hooks.obj" \
	"$(INTDIR)\kuda_ldap_init.obj" \
	"$(INTDIR)\kuda_ldap_option.obj" \
	"$(INTDIR)\kuda_ldap_rebind.obj" \
	"$(INTDIR)\kuda_ldap_stub.obj" \
	"$(INTDIR)\kuda_ldap_url.obj" \
	"$(INTDIR)\kuda_memcache.obj" \
	"$(INTDIR)\kuda_date.obj" \
	"$(INTDIR)\kudelman_dso.obj" \
	"$(INTDIR)\kuda_queue.obj" \
	"$(INTDIR)\kuda_redis.obj" \
	"$(INTDIR)\kuda_reslist.obj" \
	"$(INTDIR)\kuda_rmm.obj" \
	"$(INTDIR)\kuda_thread_pool.obj" \
	"$(INTDIR)\kudelman_version.obj" \
	"$(INTDIR)\sdbm.obj" \
	"$(INTDIR)\sdbm_hash.obj" \
	"$(INTDIR)\sdbm_lock.obj" \
	"$(INTDIR)\sdbm_pair.obj" \
	"$(INTDIR)\kuda_strmatch.obj" \
	"$(INTDIR)\kuda_uri.obj" \
	"$(INTDIR)\xlate.obj" \
	"$(INTDIR)\kuda_xml.obj" \
	"..\kuda-iconv\x64\LibD\kudaiconv-1.lib"

"$(OUTDIR)\kudadelman-1.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("kudadelman.dep")
!INCLUDE "kudadelman.dep"
!ELSE 
!MESSAGE Warning: cannot find "kudadelman.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "kudadelman - Win32 Release" || "$(CFG)" == "kudadelman - Win32 Debug" || "$(CFG)" == "kudadelman - x64 Release" || "$(CFG)" == "kudadelman - x64 Debug"
SOURCE=.\buckets\kuda_brigade.c

"$(INTDIR)\kuda_brigade.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets.c

"$(INTDIR)\kuda_buckets.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_alloc.c

"$(INTDIR)\kuda_buckets_alloc.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_eos.c

"$(INTDIR)\kuda_buckets_eos.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_file.c

"$(INTDIR)\kuda_buckets_file.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_flush.c

"$(INTDIR)\kuda_buckets_flush.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_heap.c

"$(INTDIR)\kuda_buckets_heap.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_mmap.c

"$(INTDIR)\kuda_buckets_mmap.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_pipe.c

"$(INTDIR)\kuda_buckets_pipe.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_pool.c

"$(INTDIR)\kuda_buckets_pool.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_refcount.c

"$(INTDIR)\kuda_buckets_refcount.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_simple.c

"$(INTDIR)\kuda_buckets_simple.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\buckets\kuda_buckets_socket.c

"$(INTDIR)\kuda_buckets_socket.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\kuda_crypto.c

"$(INTDIR)\kuda_crypto.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\kuda_md4.c

"$(INTDIR)\kuda_md4.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\kuda_md5.c

"$(INTDIR)\kuda_md5.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\kuda_passwd.c

"$(INTDIR)\kuda_passwd.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\kuda_sha1.c

"$(INTDIR)\kuda_sha1.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\kuda_siphash.c

"$(INTDIR)\kuda_siphash.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\crypt_blowfish.c

"$(INTDIR)\crypt_blowfish.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\getuuid.c

"$(INTDIR)\getuuid.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\crypto\uuid.c

"$(INTDIR)\uuid.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbd\kuda_dbd.c

"$(INTDIR)\kuda_dbd.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbd\kuda_dbd_mysql.c

"$(INTDIR)\kuda_dbd_mysql.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h" ".\include\private\kudelman_config.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbd\kuda_dbd_odbc.c

"$(INTDIR)\kuda_dbd_odbc.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h" ".\include\private\kudelman_config.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbd\kuda_dbd_oracle.c

"$(INTDIR)\kuda_dbd_oracle.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbd\kuda_dbd_pgsql.c

"$(INTDIR)\kuda_dbd_pgsql.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h" ".\include\private\kudelman_config.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbd\kuda_dbd_sqlite2.c

"$(INTDIR)\kuda_dbd_sqlite2.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbd\kuda_dbd_sqlite3.c

"$(INTDIR)\kuda_dbd_sqlite3.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbm\kuda_dbm.c

"$(INTDIR)\kuda_dbm.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h" ".\include\private\kudelman_select_dbm.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbm\kuda_dbm_berkeleydb.c

"$(INTDIR)\kuda_dbm_berkeleydb.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman_want.h" ".\include\private\kudelman_config.h" ".\include\kudelman.h" ".\include\private\kudelman_select_dbm.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbm\kuda_dbm_gdbm.c

"$(INTDIR)\kuda_dbm_gdbm.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h" ".\include\private\kudelman_select_dbm.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbm\kuda_dbm_sdbm.c

"$(INTDIR)\kuda_dbm_sdbm.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h" ".\include\private\kudelman_select_dbm.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\encoding\kuda_base64.c

"$(INTDIR)\kuda_base64.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\hooks\kuda_hooks.c

"$(INTDIR)\kuda_hooks.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\ldap\kuda_ldap_init.c

"$(INTDIR)\kuda_ldap_init.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h" ".\include\private\kudelman_config.h" ".\include\kuda_ldap.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\ldap\kuda_ldap_option.c

"$(INTDIR)\kuda_ldap_option.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h" ".\include\private\kudelman_config.h" ".\include\kuda_ldap.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\ldap\kuda_ldap_rebind.c

"$(INTDIR)\kuda_ldap_rebind.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h" ".\include\private\kudelman_config.h" ".\include\kuda_ldap.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\ldap\kuda_ldap_stub.c

"$(INTDIR)\kuda_ldap_stub.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h" ".\include\private\kudelman_config.h" ".\include\kuda_ldap.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\ldap\kuda_ldap_url.c

"$(INTDIR)\kuda_ldap_url.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h" ".\include\kuda_ldap.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\memcache\kuda_memcache.c

"$(INTDIR)\kuda_memcache.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kuda_date.c

"$(INTDIR)\kuda_date.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kuda_queue.c

"$(INTDIR)\kuda_queue.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kuda_reslist.c

"$(INTDIR)\kuda_reslist.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kuda_rmm.c

"$(INTDIR)\kuda_rmm.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kuda_thread_pool.c

"$(INTDIR)\kuda_thread_pool.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kudelman_dso.c

"$(INTDIR)\kudelman_dso.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\misc\kudelman_version.c

"$(INTDIR)\kudelman_version.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\redis\kuda_redis.c

"$(INTDIR)\kuda_redis.obj" : $(SOURCE) "$(INTDIR)" ".\include\kuda_redis.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbm\sdbm\sdbm.c

"$(INTDIR)\sdbm.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbm\sdbm\sdbm_hash.c

"$(INTDIR)\sdbm_hash.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbm\sdbm\sdbm_lock.c

"$(INTDIR)\sdbm_lock.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\dbm\sdbm\sdbm_pair.c

"$(INTDIR)\sdbm_pair.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\strmatch\kuda_strmatch.c

"$(INTDIR)\kuda_strmatch.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\uri\kuda_uri.c

"$(INTDIR)\kuda_uri.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\xlate\xlate.c

"$(INTDIR)\xlate.obj" : $(SOURCE) "$(INTDIR)" ".\include\kudelman.h" ".\include\private\kudelman_config.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\xml\kuda_xml.c

"$(INTDIR)\kuda_xml.obj" : $(SOURCE) "$(INTDIR)" ".\include\private\kudelman_config.h" ".\include\kudelman.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\include\kuda_ldap.hw

!IF  "$(CFG)" == "kudadelman - Win32 Release"

InputPath=.\include\kuda_ldap.hw

".\include\kuda_ldap.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kuda_ldap.hw > .\include\kuda_ldap.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - Win32 Debug"

InputPath=.\include\kuda_ldap.hw

".\include\kuda_ldap.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kuda_ldap.hw > .\include\kuda_ldap.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - x64 Release"

InputPath=.\include\kuda_ldap.hw

".\include\kuda_ldap.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kuda_ldap.hw > .\include\kuda_ldap.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - x64 Debug"

InputPath=.\include\kuda_ldap.hw

".\include\kuda_ldap.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kuda_ldap.hw > .\include\kuda_ldap.h
<< 
	

!ENDIF 

SOURCE=.\include\kudelman.hw

!IF  "$(CFG)" == "kudadelman - Win32 Release"

InputPath=.\include\kudelman.hw

".\include\kudelman.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman.hw > .\include\kudelman.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - Win32 Debug"

InputPath=.\include\kudelman.hw

".\include\kudelman.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman.hw > .\include\kudelman.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - x64 Release"

InputPath=.\include\kudelman.hw

".\include\kudelman.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman.hw > .\include\kudelman.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - x64 Debug"

InputPath=.\include\kudelman.hw

".\include\kudelman.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman.hw > .\include\kudelman.h
<< 
	

!ENDIF 

SOURCE=.\include\private\kudelman_config.hw

!IF  "$(CFG)" == "kudadelman - Win32 Release"

InputPath=.\include\private\kudelman_config.hw

".\include\private\kudelman_config.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_config.hw > .\include\private\kudelman_config.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - Win32 Debug"

InputPath=.\include\private\kudelman_config.hw

".\include\private\kudelman_config.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_config.hw > .\include\private\kudelman_config.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - x64 Release"

InputPath=.\include\private\kudelman_config.hw

".\include\private\kudelman_config.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_config.hw > .\include\private\kudelman_config.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - x64 Debug"

InputPath=.\include\private\kudelman_config.hw

".\include\private\kudelman_config.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_config.hw > .\include\private\kudelman_config.h
<< 
	

!ENDIF 

SOURCE=.\include\private\kudelman_select_dbm.hw

!IF  "$(CFG)" == "kudadelman - Win32 Release"

InputPath=.\include\private\kudelman_select_dbm.hw

".\include\private\kudelman_select_dbm.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_select_dbm.hw > .\include\private\kudelman_select_dbm.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - Win32 Debug"

InputPath=.\include\private\kudelman_select_dbm.hw

".\include\private\kudelman_select_dbm.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_select_dbm.hw > .\include\private\kudelman_select_dbm.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - x64 Release"

InputPath=.\include\private\kudelman_select_dbm.hw

".\include\private\kudelman_select_dbm.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_select_dbm.hw > .\include\private\kudelman_select_dbm.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - x64 Debug"

InputPath=.\include\private\kudelman_select_dbm.hw

".\include\private\kudelman_select_dbm.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\private\kudelman_select_dbm.hw > .\include\private\kudelman_select_dbm.h
<< 
	

!ENDIF 

SOURCE=.\include\kudelman_want.hw

!IF  "$(CFG)" == "kudadelman - Win32 Release"

InputPath=.\include\kudelman_want.hw

".\include\kudelman_want.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman_want.hw > .\include\kudelman_want.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - Win32 Debug"

InputPath=.\include\kudelman_want.hw

".\include\kudelman_want.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman_want.hw > .\include\kudelman_want.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - x64 Release"

InputPath=.\include\kudelman_want.hw

".\include\kudelman_want.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman_want.hw > .\include\kudelman_want.h
<< 
	

!ELSEIF  "$(CFG)" == "kudadelman - x64 Debug"

InputPath=.\include\kudelman_want.hw

".\include\kudelman_want.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\include\kudelman_want.hw > .\include\kudelman_want.h
<< 
	

!ENDIF 

!IF  "$(CFG)" == "kudadelman - Win32 Release"

"kudaiconv - Win32 Release" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudaiconv.mak" CFG="kudaiconv - Win32 Release" 
   cd "..\kuda-delman"

"kudaiconv - Win32 ReleaseCLEAN" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudaiconv.mak" CFG="kudaiconv - Win32 Release" RECURSE=1 CLEAN 
   cd "..\kuda-delman"

!ELSEIF  "$(CFG)" == "kudadelman - Win32 Debug"

"kudaiconv - Win32 Debug" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudaiconv.mak" CFG="kudaiconv - Win32 Debug" 
   cd "..\kuda-delman"

"kudaiconv - Win32 DebugCLEAN" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudaiconv.mak" CFG="kudaiconv - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\kuda-delman"

!ELSEIF  "$(CFG)" == "kudadelman - x64 Release"

"kudaiconv - x64 Release" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudaiconv.mak" CFG="kudaiconv - x64 Release" 
   cd "..\kuda-delman"

"kudaiconv - x64 ReleaseCLEAN" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudaiconv.mak" CFG="kudaiconv - x64 Release" RECURSE=1 CLEAN 
   cd "..\kuda-delman"

!ELSEIF  "$(CFG)" == "kudadelman - x64 Debug"

"kudaiconv - x64 Debug" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudaiconv.mak" CFG="kudaiconv - x64 Debug" 
   cd "..\kuda-delman"

"kudaiconv - x64 DebugCLEAN" : 
   cd ".\..\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudaiconv.mak" CFG="kudaiconv - x64 Debug" RECURSE=1 CLEAN 
   cd "..\kuda-delman"

!ENDIF 


!ENDIF 

