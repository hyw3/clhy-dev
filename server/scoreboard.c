/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_lib.h"

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#include "clhy_config.h"
#include "wwhy.h"
#include "http_log.h"
#include "http_main.h"
#include "http_core.h"
#include "http_config.h"
#include "http_protocol.h"
#include "clhy_core.h"

#include "scoreboard.h"

/* we know core's capi_index is 0 */
#undef CLHYLOG_CAPI_INDEX
#define CLHYLOG_CAPI_INDEX CLHY_CORE_CAPI_INDEX

CLHY_DECLARE_DATA scoreboard *clhy_scoreboard_image = NULL;
CLHY_DECLARE_DATA const char *clhy_scoreboard_fname = NULL;
static clhy_scoreboard_e scoreboard_type;

const char * clhy_set_scoreboard(cmd_parms *cmd, void *dummy,
                               const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    clhy_scoreboard_fname = arg;
    return NULL;
}

/* Default to false when capi_status is not loaded */
CLHY_DECLARE_DATA int clhy_extended_status = 0;

const char *clhy_set_extended_status(cmd_parms *cmd, void *dummy, int arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }
    clhy_extended_status = arg;
    return NULL;
}

CLHY_DECLARE_DATA int clhy_capi_status_reqtail = 0;

const char *clhy_set_reqtail(cmd_parms *cmd, void *dummy, int arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }
    clhy_capi_status_reqtail = arg;
    return NULL;
}

#if KUDA_HAS_SHARED_MEMORY

#include "kuda_shm.h"

#ifndef WIN32
static /* but must be exported to clmp_winnt */
#endif
        kuda_shm_t *clhy_scoreboard_shm = NULL;

#endif

KUDA_HOOK_STRUCT(
    KUDA_HOOK_LINK(pre_clmp)
)

CLHY_IMPLEMENT_HOOK_RUN_ALL(int,pre_clmp,
                          (kuda_pool_t *p, clhy_scoreboard_e sb_type),
                          (p, sb_type),OK,DECLINED)

static KUDA_OPTIONAL_FN_TYPE(clhy_logio_get_last_bytes)
                                *pfn_clhy_logio_get_last_bytes;

struct clhy_sb_handle_t {
    int child_num;
    int thread_num;
};

static int server_limit, thread_limit;
static kuda_size_t scoreboard_size;

/*
 * ToDo:
 * This function should be renamed to cleanup_shared
 * and it should handle cleaning up a scoreboard shared
 * between processes using any form of IPC (file, shared memory
 * segment, etc.). Leave it as is now because it is being used
 * by various cLMPs.
 */
static kuda_status_t clhy_cleanup_shared_mem(void *d)
{
#if KUDA_HAS_SHARED_MEMORY
    free(clhy_scoreboard_image);
    clhy_scoreboard_image = NULL;
    kuda_shm_destroy(clhy_scoreboard_shm);
#endif
    return KUDA_SUCCESS;
}

#define SIZE_OF_scoreboard    KUDA_ALIGN_DEFAULT(sizeof(scoreboard))
#define SIZE_OF_global_score  KUDA_ALIGN_DEFAULT(sizeof(global_score))
#define SIZE_OF_process_score KUDA_ALIGN_DEFAULT(sizeof(process_score))
#define SIZE_OF_worker_score  KUDA_ALIGN_DEFAULT(sizeof(worker_score))

CLHY_DECLARE(int) clhy_calc_scoreboard_size(void)
{
    clhy_clmp_query(CLHY_CLMPQ_HARD_LIMIT_THREADS, &thread_limit);
    clhy_clmp_query(CLHY_CLMPQ_HARD_LIMIT_DAEMONS, &server_limit);

    scoreboard_size  = SIZE_OF_global_score;
    scoreboard_size += SIZE_OF_process_score * server_limit;
    scoreboard_size += SIZE_OF_worker_score * server_limit * thread_limit;

    return scoreboard_size;
}

CLHY_DECLARE(void) clhy_init_scoreboard(void *shared_score)
{
    char *more_storage;
    int i;

    pfn_clhy_logio_get_last_bytes = KUDA_RETRIEVE_OPTIONAL_FN(clhy_logio_get_last_bytes);
    if (!shared_score) {
        return;
    }
    
    clhy_calc_scoreboard_size();
    clhy_scoreboard_image =
        clhy_calloc(1, SIZE_OF_scoreboard + server_limit * sizeof(worker_score *));
    more_storage = shared_score;
    clhy_scoreboard_image->global = (global_score *)more_storage;
    more_storage += SIZE_OF_global_score;
    clhy_scoreboard_image->parent = (process_score *)more_storage;
    more_storage += SIZE_OF_process_score * server_limit;
    clhy_scoreboard_image->servers =
        (worker_score **)((char*)clhy_scoreboard_image + SIZE_OF_scoreboard);
    for (i = 0; i < server_limit; i++) {
        clhy_scoreboard_image->servers[i] = (worker_score *)more_storage;
        more_storage += thread_limit * SIZE_OF_worker_score;
    }
    clhy_assert(more_storage == (char*)shared_score + scoreboard_size);
    clhy_scoreboard_image->global->server_limit = server_limit;
    clhy_scoreboard_image->global->thread_limit = thread_limit;
}

/**
 * Create a name-based scoreboard in the given pool using the
 * given filename.
 */
static kuda_status_t create_namebased_scoreboard(kuda_pool_t *pool,
                                                const char *fname)
{
#if KUDA_HAS_SHARED_MEMORY
    kuda_status_t rv;

    /* The shared memory file must not exist before we create the
     * segment. */
    kuda_shm_remove(fname, pool); /* ignore errors */

    rv = kuda_shm_create(&clhy_scoreboard_shm, scoreboard_size, fname, pool);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, clhy_server_conf, CLHYLOGNO(00001)
                     "unable to create or access scoreboard \"%s\" "
                     "(name-based shared memory failure)", fname);
        return rv;
    }
#endif /* KUDA_HAS_SHARED_MEMORY */
    return KUDA_SUCCESS;
}

/* ToDo: This function should be made to handle setting up
 * a scoreboard shared between processes using any IPC technique,
 * not just a shared memory segment
 */
static kuda_status_t open_scoreboard(kuda_pool_t *pconf)
{
#if KUDA_HAS_SHARED_MEMORY
    kuda_status_t rv;
    char *fname = NULL;
    kuda_pool_t *global_pool;

    /* We don't want to have to recreate the scoreboard after
     * restarts, so we'll create a global pool and never clean it.
     */
    rv = kuda_pool_create(&global_pool, NULL);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, clhy_server_conf, CLHYLOGNO(00002)
                     "Fatal error: unable to create global pool "
                     "for use by the scoreboard");
        return rv;
    }

    /* The config says to create a name-based shmem */
    if (clhy_scoreboard_fname) {
        /* make sure it's an absolute pathname */
        fname = clhy_server_root_relative(pconf, clhy_scoreboard_fname);
        if (!fname) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, KUDA_EBADPATH, clhy_server_conf, CLHYLOGNO(00003)
                         "Fatal error: Invalid Scoreboard path %s",
                         clhy_scoreboard_fname);
            return KUDA_EBADPATH;
        }
        return create_namebased_scoreboard(global_pool, fname);
    }
    else { /* config didn't specify, we get to choose shmem type */
        rv = kuda_shm_create(&clhy_scoreboard_shm, scoreboard_size, NULL,
                            global_pool); /* anonymous shared memory */
        if ((rv != KUDA_SUCCESS) && (rv != KUDA_ENOTIMPL)) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, clhy_server_conf, CLHYLOGNO(00004)
                         "Unable to create or access scoreboard "
                         "(anonymous shared memory failure)");
            return rv;
        }
        /* Make up a filename and do name-based shmem */
        else if (rv == KUDA_ENOTIMPL) {
            /* Make sure it's an absolute pathname */
            clhy_scoreboard_fname = DEFAULT_SCOREBOARD;
            fname = clhy_server_root_relative(pconf, clhy_scoreboard_fname);

            return create_namebased_scoreboard(global_pool, fname);
        }
    }
#endif /* KUDA_HAS_SHARED_MEMORY */
    return KUDA_SUCCESS;
}

/* If detach is non-zero, this is a separate child process,
 * if zero, it is a forked child.
 */
CLHY_DECLARE(kuda_status_t) clhy_reopen_scoreboard(kuda_pool_t *p, kuda_shm_t **shm,
                                              int detached)
{
#if KUDA_HAS_SHARED_MEMORY
    if (!detached) {
        return KUDA_SUCCESS;
    }
    if (kuda_shm_size_get(clhy_scoreboard_shm) < scoreboard_size) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, clhy_server_conf, CLHYLOGNO(00005)
                     "Fatal error: shared scoreboard too small for child!");
        kuda_shm_detach(clhy_scoreboard_shm);
        clhy_scoreboard_shm = NULL;
        return KUDA_EINVAL;
    }
    /* everything will be cleared shortly */
    if (*shm) {
        *shm = clhy_scoreboard_shm;
    }
#endif
    return KUDA_SUCCESS;
}

kuda_status_t clhy_cleanup_scoreboard(void *d)
{
    if (clhy_scoreboard_image == NULL) {
        return KUDA_SUCCESS;
    }
    if (scoreboard_type == SB_SHARED) {
        clhy_cleanup_shared_mem(NULL);
    }
    else {
        free(clhy_scoreboard_image->global);
        free(clhy_scoreboard_image);
        clhy_scoreboard_image = NULL;
    }
    return KUDA_SUCCESS;
}

/* Create or reinit an existing scoreboard. The cLMP can control whether
 * the scoreboard is shared across multiple processes or not
 */
int clhy_create_scoreboard(kuda_pool_t *p, clhy_scoreboard_e sb_type)
{
    int i;
#if KUDA_HAS_SHARED_MEMORY
    kuda_status_t rv;
#endif

    if (clhy_scoreboard_image) {
        clhy_scoreboard_image->global->restart_time = kuda_time_now();
        memset(clhy_scoreboard_image->parent, 0,
               SIZE_OF_process_score * server_limit);
        for (i = 0; i < server_limit; i++) {
            memset(clhy_scoreboard_image->servers[i], 0,
                   SIZE_OF_worker_score * thread_limit);
        }
        clhy_init_scoreboard(NULL);
        return OK;
    }

    clhy_calc_scoreboard_size();
#if KUDA_HAS_SHARED_MEMORY
    if (sb_type == SB_SHARED) {
        void *sb_shared;
        rv = open_scoreboard(p);
        if (rv || !(sb_shared = kuda_shm_baseaddr_get(clhy_scoreboard_shm))) {
            return HTTP_INTERNAL_SERVER_ERROR;
        }
        memset(sb_shared, 0, scoreboard_size);
        clhy_init_scoreboard(sb_shared);
    }
    else
#endif
    {
        /* A simple malloc will suffice */
        void *sb_mem = clhy_calloc(1, scoreboard_size);
        clhy_init_scoreboard(sb_mem);
    }

    scoreboard_type = sb_type;
    clhy_scoreboard_image->global->running_generation = 0;
    clhy_scoreboard_image->global->restart_time = kuda_time_now();

    kuda_pool_cleanup_register(p, NULL, clhy_cleanup_scoreboard, kuda_pool_cleanup_null);

    return OK;
}

/* Routines called to deal with the scoreboard image
 * --- note that we do *not* need write locks, since update_child_status
 * only updates a *single* record in place, and only one process writes to
 * a given scoreboard slot at a time (either the child process owning that
 * slot, or the parent, noting that the child has died).
 *
 * As a final note --- setting the score entry to getpid() is always safe,
 * since when the parent is writing an entry, it's only noting SERVER_DEAD
 * anyway.
 */

CLHY_DECLARE(int) clhy_exists_scoreboard_image(void)
{
    return (clhy_scoreboard_image ? 1 : 0);
}

CLHY_DECLARE(void) clhy_set_conn_count(clhy_sb_handle_t *sb, request_rec *r, 
                                   unsigned short conn_count)
{
    worker_score *ws;

    if (!sb)
        return;

    ws = &clhy_scoreboard_image->servers[sb->child_num][sb->thread_num];
    ws->conn_count = conn_count;
}

CLHY_DECLARE(void) clhy_increment_counts(clhy_sb_handle_t *sb, request_rec *r)
{
    worker_score *ws;
    kuda_off_t bytes;

    if (!sb)
        return;

    ws = &clhy_scoreboard_image->servers[sb->child_num][sb->thread_num];
    if (pfn_clhy_logio_get_last_bytes != NULL) {
        bytes = pfn_clhy_logio_get_last_bytes(r->connection);
    }
    else if (r->method_number == M_GET && r->method[0] == 'H') {
        bytes = 0;
    }
    else {
        bytes = r->bytes_sent;
    }

#ifdef HAVE_TIMES
    times(&ws->times);
#endif
    ws->access_count++;
    ws->my_access_count++;
    ws->conn_count++;
    ws->bytes_served += bytes;
    ws->my_bytes_served += bytes;
    ws->conn_bytes += bytes;
}

CLHY_DECLARE(int) clhy_find_child_by_pid(kuda_proc_t *pid)
{
    int i;
    int max_daemons_limit = 0;

    clhy_clmp_query(CLHY_CLMPQ_MAX_DAEMON_USED, &max_daemons_limit);

    for (i = 0; i < max_daemons_limit; ++i) {
        if (clhy_scoreboard_image->parent[i].pid == pid->pid) {
            return i;
        }
    }

    return -1;
}

CLHY_DECLARE(void) clhy_update_sb_handle(clhy_sb_handle_t *sbh,
                                     int child_num, int thread_num)
{
    sbh->child_num = child_num;
    sbh->thread_num = thread_num;
}

CLHY_DECLARE(void) clhy_create_sb_handle(clhy_sb_handle_t **new_sbh, kuda_pool_t *p,
                                     int child_num, int thread_num)
{
    *new_sbh = (clhy_sb_handle_t *)kuda_palloc(p, sizeof(clhy_sb_handle_t));
    clhy_update_sb_handle(*new_sbh, child_num, thread_num);
}

static void copy_request(char *rbuf, kuda_size_t rbuflen, request_rec *r)
{
    char *p;

    if (r->the_request == NULL) {
        kuda_cpystrn(rbuf, "NULL", rbuflen);
        return; /* short circuit below */
    }

    if (r->parsed_uri.password == NULL) {
        p = r->the_request;
    }
    else {
        /* Don't reveal the password in the server-status view */
        p = kuda_pstrcat(r->pool, r->method, " ",
                        kuda_uri_unparse(r->pool, &r->parsed_uri,
                        KUDA_URI_UNP_OMITPASSWORD),
                        r->assbackwards ? NULL : " ", r->protocol, NULL);
    }

    /* now figure out if we copy over the 1st rbuflen chars or the last */
    if (!clhy_capi_status_reqtail) {
        kuda_cpystrn(rbuf, p, rbuflen);
    }
    else {
        kuda_size_t slen = strlen(p);
        if (slen < rbuflen) {
            /* it all fits anyway */
            kuda_cpystrn(rbuf, p, rbuflen);
        }
        else {
            kuda_cpystrn(rbuf, p+(slen-rbuflen+1), rbuflen);
        }
    }
}

static int update_child_status_internal(int child_num,
                                        int thread_num,
                                        int status,
                                        conn_rec *c,
                                        server_rec *s,
                                        request_rec *r,
                                        const char *descr)
{
    int old_status;
    worker_score *ws;
    int clmp_generation;

    ws = &clhy_scoreboard_image->servers[child_num][thread_num];
    old_status = ws->status;
    ws->status = status;
    
    if (status == SERVER_READY
        && old_status == SERVER_STARTING) {
        process_score *ps = &clhy_scoreboard_image->parent[child_num];
        ws->thread_num = child_num * thread_limit + thread_num;
        clhy_clmp_query(CLHY_CLMPQ_GENERATION, &clmp_generation);
        ps->generation = clmp_generation;
    }

    if (clhy_extended_status) {
        const char *val;
        
        if (status == SERVER_READY || status == SERVER_DEAD) {
            /*
             * Reset individual counters
             */
            if (status == SERVER_DEAD) {
                ws->my_access_count = 0L;
                ws->my_bytes_served = 0L;
#ifdef HAVE_TIMES
                ws->times.tms_utime = 0;
                ws->times.tms_stime = 0;
                ws->times.tms_cutime = 0;
                ws->times.tms_cstime = 0;
#endif
            }
            ws->conn_count = 0;
            ws->conn_bytes = 0;
            ws->last_used = kuda_time_now();
        }

        if (descr) {
            kuda_cpystrn(ws->request, descr, sizeof(ws->request));
        }
        else if (r) {
            copy_request(ws->request, sizeof(ws->request), r);
        }
        else if (c) {
            ws->request[0]='\0';
        }

        if (r && r->useragent_ip) {
            if (!(val = clhy_get_useragent_host(r, REMOTE_NOLOOKUP, NULL))) {
                kuda_cpystrn(ws->client, r->useragent_ip, sizeof(ws->client)); /* DEPRECATE */
                kuda_cpystrn(ws->client64, r->useragent_ip, sizeof(ws->client64));
            }
            else {
                kuda_cpystrn(ws->client, val, sizeof(ws->client)); /* DEPRECATE */
                kuda_cpystrn(ws->client64, val, sizeof(ws->client64));
            }
        }
        else if (c) {
            if (!(val = clhy_get_remote_host(c, c->base_server->lookup_defaults,
                                           REMOTE_NOLOOKUP, NULL))) {
                kuda_cpystrn(ws->client, c->client_ip, sizeof(ws->client)); /* DEPRECATE */
                kuda_cpystrn(ws->client64, c->client_ip, sizeof(ws->client64));
            }
            else {
                kuda_cpystrn(ws->client, val, sizeof(ws->client)); /* DEPRECATE */
                kuda_cpystrn(ws->client64, val, sizeof(ws->client64));
            }
        }

        if (s) {
            if (c) {
                kuda_snprintf(ws->vhost, sizeof(ws->vhost), "%s:%d",
                             s->server_hostname, c->local_addr->port);
            }
            else {
                kuda_cpystrn(ws->vhost, s->server_hostname, sizeof(ws->vhost));
            }
        }
        else if (c) {
            ws->vhost[0]='\0';
        }

        if (c) {
            val = clhy_get_protocol(c);
            kuda_cpystrn(ws->protocol, val, sizeof(ws->protocol));
        }
    }

    return old_status;
}

CLHY_DECLARE(int) clhy_update_child_status_from_indexes(int child_num,
                                                    int thread_num,
                                                    int status,
                                                    request_rec *r)
{
    if (child_num < 0) {
        return -1;
    }

    return update_child_status_internal(child_num, thread_num, status,
                                        r ? r->connection : NULL,
                                        r ? r->server : NULL,
                                        r, NULL);
}

CLHY_DECLARE(int) clhy_update_child_status(clhy_sb_handle_t *sbh, int status,
                                      request_rec *r)
{
    if (!sbh || (sbh->child_num < 0))
        return -1;

    return update_child_status_internal(sbh->child_num, sbh->thread_num,
                                        status,
                                        r ? r->connection : NULL,
                                        r ? r->server : NULL,
                                        r, NULL);
}

CLHY_DECLARE(int) clhy_update_child_status_from_conn(clhy_sb_handle_t *sbh, int status,
                                                 conn_rec *c)
{
    if (!sbh || (sbh->child_num < 0))
        return -1;

    return update_child_status_internal(sbh->child_num, sbh->thread_num,
                                        status, c, NULL, NULL, NULL);
}

CLHY_DECLARE(int) clhy_update_child_status_from_server(clhy_sb_handle_t *sbh, int status, 
                                                   conn_rec *c, server_rec *s)
{
    if (!sbh || (sbh->child_num < 0))
        return -1;

    return update_child_status_internal(sbh->child_num, sbh->thread_num,
                                        status, c, s, NULL, NULL);
}

CLHY_DECLARE(int) clhy_update_child_status_descr(clhy_sb_handle_t *sbh, int status, const char *descr)
{
    if (!sbh || (sbh->child_num < 0))
        return -1;

    return update_child_status_internal(sbh->child_num, sbh->thread_num,
                                        status, NULL, NULL, NULL, descr);
}

CLHY_DECLARE(void) clhy_time_process_request(clhy_sb_handle_t *sbh, int status)
{
    worker_score *ws;

    if (!sbh)
        return;

    if (sbh->child_num < 0) {
        return;
    }

    ws = &clhy_scoreboard_image->servers[sbh->child_num][sbh->thread_num];

    if (status == START_PREQUEST) {
        ws->start_time = ws->last_used = kuda_time_now();
    }
    else if (status == STOP_PREQUEST) {
        ws->stop_time = ws->last_used = kuda_time_now();
        if (clhy_extended_status) {
            ws->duration += ws->stop_time - ws->start_time;
        }
    }
}

CLHY_DECLARE(int) clhy_update_global_status()
{
#ifdef HAVE_TIMES
    if (clhy_scoreboard_image == NULL) {
        return KUDA_SUCCESS;
    }
    times(&clhy_scoreboard_image->global->times);
#endif
    return KUDA_SUCCESS;
}

CLHY_DECLARE(worker_score *) clhy_get_scoreboard_worker_from_indexes(int x, int y)
{
    if (((x < 0) || (x >= server_limit)) ||
        ((y < 0) || (y >= thread_limit))) {
        return(NULL); /* Out of range */
    }
    return &clhy_scoreboard_image->servers[x][y];
}

CLHY_DECLARE(worker_score *) clhy_get_scoreboard_worker(clhy_sb_handle_t *sbh)
{
    if (!sbh)
        return NULL;

    return clhy_get_scoreboard_worker_from_indexes(sbh->child_num,
                                                 sbh->thread_num);
}

CLHY_DECLARE(void) clhy_copy_scoreboard_worker(worker_score *dest, 
                                           int child_num,
                                           int thread_num)
{
    worker_score *ws = clhy_get_scoreboard_worker_from_indexes(child_num, thread_num);

    memcpy(dest, ws, sizeof *ws);

    /* For extra safety, NUL-terminate the strings returned, though it
     * should be true those last bytes are always zero anyway. */
    dest->client[sizeof(dest->client) - 1] = '\0';
    dest->client64[sizeof(dest->client64) - 1] = '\0';
    dest->request[sizeof(dest->request) - 1] = '\0';
    dest->vhost[sizeof(dest->vhost) - 1] = '\0';
    dest->protocol[sizeof(dest->protocol) - 1] = '\0';
}

CLHY_DECLARE(process_score *) clhy_get_scoreboard_process(int x)
{
    if ((x < 0) || (x >= server_limit)) {
        return(NULL); /* Out of range */
    }
    return &clhy_scoreboard_image->parent[x];
}

CLHY_DECLARE(global_score *) clhy_get_scoreboard_global()
{
    return clhy_scoreboard_image->global;
}
