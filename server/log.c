/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * http_log.c: Dealing with the logs and errors
 *
 * Rob McCool
 *
 */

#include "kuda.h"
#include "kuda_general.h"        /* for signal stuff */
#include "kuda_strings.h"
#include "kuda_errno.h"
#include "kuda_thread_proc.h"
#include "kuda_lib.h"
#include "kuda_signal.h"
#include "kuda_portable.h"
#include "kuda_base64.h"

#define KUDA_WANT_STDIO
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#if KUDA_HAVE_STDARG_H
#include <stdarg.h>
#endif
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#if KUDA_HAVE_PROCESS_H
#include <process.h>            /* for getpid() on Win32 */
#endif

#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_main.h"
#include "util_time.h"
#include "clhy_core.h"
#include "clhy_listen.h"

#if HAVE_GETTID
#include <sys/syscall.h>
#include <sys/types.h>
#endif

/* we know core's capi_index is 0 */
#undef CLHYLOG_CAPI_INDEX
#define CLHYLOG_CAPI_INDEX CLHY_CORE_CAPI_INDEX

typedef struct {
    const char *t_name;
    int t_val;
} TRANS;

KUDA_HOOK_STRUCT(
    KUDA_HOOK_LINK(error_log)
    KUDA_HOOK_LINK(generate_log_id)
)

int CLHY_DECLARE_DATA clhy_default_loglevel = DEFAULT_LOGLEVEL;

#ifdef HAVE_SYSLOG

static const TRANS facilities[] = {
    {"auth",    LOG_AUTH},
#ifdef LOG_AUTHPRIV
    {"authpriv",LOG_AUTHPRIV},
#endif
#ifdef LOG_CRON
    {"cron",    LOG_CRON},
#endif
#ifdef LOG_DAEMON
    {"daemon",  LOG_DAEMON},
#endif
#ifdef LOG_FTP
    {"ftp", LOG_FTP},
#endif
#ifdef LOG_KERN
    {"kern",    LOG_KERN},
#endif
#ifdef LOG_LPR
    {"lpr", LOG_LPR},
#endif
#ifdef LOG_MAIL
    {"mail",    LOG_MAIL},
#endif
#ifdef LOG_NEWS
    {"news",    LOG_NEWS},
#endif
#ifdef LOG_SYSLOG
    {"syslog",  LOG_SYSLOG},
#endif
#ifdef LOG_USER
    {"user",    LOG_USER},
#endif
#ifdef LOG_UUCP
    {"uucp",    LOG_UUCP},
#endif
#ifdef LOG_LOCAL0
    {"local0",  LOG_LOCAL0},
#endif
#ifdef LOG_LOCAL1
    {"local1",  LOG_LOCAL1},
#endif
#ifdef LOG_LOCAL2
    {"local2",  LOG_LOCAL2},
#endif
#ifdef LOG_LOCAL3
    {"local3",  LOG_LOCAL3},
#endif
#ifdef LOG_LOCAL4
    {"local4",  LOG_LOCAL4},
#endif
#ifdef LOG_LOCAL5
    {"local5",  LOG_LOCAL5},
#endif
#ifdef LOG_LOCAL6
    {"local6",  LOG_LOCAL6},
#endif
#ifdef LOG_LOCAL7
    {"local7",  LOG_LOCAL7},
#endif
    {NULL,      -1},
};
#endif

static const TRANS priorities[] = {
    {"emerg",   CLHYLOG_EMERG},
    {"alert",   CLHYLOG_ALERT},
    {"crit",    CLHYLOG_CRIT},
    {"error",   CLHYLOG_ERR},
    {"warn",    CLHYLOG_WARNING},
    {"notice",  CLHYLOG_NOTICE},
    {"info",    CLHYLOG_INFO},
    {"debug",   CLHYLOG_DEBUG},
    {"trace1",  CLHYLOG_TRACE1},
    {"trace2",  CLHYLOG_TRACE2},
    {"trace3",  CLHYLOG_TRACE3},
    {"trace4",  CLHYLOG_TRACE4},
    {"trace5",  CLHYLOG_TRACE5},
    {"trace6",  CLHYLOG_TRACE6},
    {"trace7",  CLHYLOG_TRACE7},
    {"trace8",  CLHYLOG_TRACE8},
    {NULL,      -1},
};

static kuda_pool_t *stderr_pool = NULL;

static kuda_file_t *stderr_log = NULL;

/* track pipe handles to close in child process */
typedef struct read_handle_t {
    struct read_handle_t *next;
    kuda_file_t *handle;
} read_handle_t;

static read_handle_t *read_handles;

/**
 * @brief The piped logging structure.
 *
 * Piped logs are used to move functionality out of the main server.
 * For example, log rotation is done with piped logs.
 */
struct piped_log {
    /** The pool to use for the piped log */
    kuda_pool_t *p;
    /** The pipe between the server and the logging process */
    kuda_file_t *read_fd, *write_fd;
#ifdef CLHY_HAVE_RELIABLE_PIPED_LOGS
    /** The name of the program the logging process is running */
    char *program;
    /** The pid of the logging process */
    kuda_proc_t *pid;
    /** How to reinvoke program when it must be replaced */
    kuda_cmdtype_e cmdtype;
#endif
};

CLHY_DECLARE(kuda_file_t *) clhy_piped_log_read_fd(piped_log *pl)
{
    return pl->read_fd;
}

CLHY_DECLARE(kuda_file_t *) clhy_piped_log_write_fd(piped_log *pl)
{
    return pl->write_fd;
}

/* remember to close this handle in the child process
 *
 * On Win32 this makes zero sense, because we don't
 * take the parent process's child procs.
 * If the win32 parent instead passed each and every
 * logger write handle from itself down to the child,
 * and the parent manages all aspects of keeping the
 * reliable pipe log children alive, this would still
 * make no sense :)  Cripple it on Win32.
 */
static void close_handle_in_child(kuda_pool_t *p, kuda_file_t *f)
{
#ifndef WIN32
    read_handle_t *new_handle;

    new_handle = kuda_pcalloc(p, sizeof(read_handle_t));
    new_handle->next = read_handles;
    new_handle->handle = f;
    read_handles = new_handle;
#endif
}

void clhy_logs_child_init(kuda_pool_t *p, server_rec *s)
{
    read_handle_t *cur = read_handles;

    while (cur) {
        kuda_file_close(cur->handle);
        cur = cur->next;
    }
}

CLHY_DECLARE(void) clhy_open_stderr_log(kuda_pool_t *p)
{
    kuda_file_open_stderr(&stderr_log, p);
}

CLHY_DECLARE(kuda_status_t) clhy_replace_stderr_log(kuda_pool_t *p,
                                               const char *fname)
{
    kuda_file_t *stderr_file;
    kuda_status_t rc;
    char *filename = clhy_server_root_relative(p, fname);
    if (!filename) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP|CLHYLOG_CRIT,
                     KUDA_EBADPATH, NULL, CLHYLOGNO(00085) "Invalid -E error log file %s",
                     fname);
        return KUDA_EBADPATH;
    }
    if ((rc = kuda_file_open(&stderr_file, filename,
                            KUDA_APPEND | KUDA_WRITE | KUDA_CREATE | KUDA_LARGEFILE,
                            KUDA_PLATFORM_DEFAULT, p)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, rc, NULL, CLHYLOGNO(00086)
                     "%s: could not open error log file %s.",
                     clhy_server_argv0, fname);
        return rc;
    }
    if (!stderr_pool) {
        /* This is safe provided we revert it when we are finished.
         * We don't manager the callers pool!
         */
        stderr_pool = p;
    }
    if ((rc = kuda_file_open_stderr(&stderr_log, stderr_pool))
            == KUDA_SUCCESS) {
        kuda_file_flush(stderr_log);
        if ((rc = kuda_file_dup2(stderr_log, stderr_file, stderr_pool))
                == KUDA_SUCCESS) {
            kuda_file_close(stderr_file);
            /*
             * You might ponder why stderr_pool should survive?
             * The trouble is, stderr_pool may have s_main->error_log,
             * so we aren't in a position to destory stderr_pool until
             * the next recycle.  There's also an apparent bug which
             * is not; if some folk decided to call this function before
             * the core open error logs hook, this pool won't survive.
             * Neither does the stderr logger, so this isn't a problem.
             */
        }
    }
    /* Revert, see above */
    if (stderr_pool == p)
        stderr_pool = NULL;

    if (rc != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rc, NULL, CLHYLOGNO(00087)
                     "unable to replace stderr with error log file");
    }
    return rc;
}

static void log_child_errfn(kuda_pool_t *pool, kuda_status_t err,
                            const char *description)
{
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, err, NULL, CLHYLOGNO(00088)
                 "%s", description);
}

/* Create a child process running PROGNAME with a pipe connected to
 * the childs stdin.  The write-end of the pipe will be placed in
 * *FPIN on successful return.  If dummy_stderr is non-zero, the
 * stderr for the child will be the same as the stdout of the parent.
 * Otherwise the child will inherit the stderr from the parent. */
static int log_child(kuda_pool_t *p, const char *progname,
                     kuda_file_t **fpin, kuda_cmdtype_e cmdtype,
                     int dummy_stderr)
{
    /* Child process code for 'ErrorLog "|..."';
     * may want a common framework for this, since I expect it will
     * be common for other foo-loggers to want this sort of thing...
     */
    kuda_status_t rc;
    kuda_procattr_t *procattr;
    kuda_proc_t *procnew;
    kuda_file_t *errfile;

    if (((rc = kuda_procattr_create(&procattr, p)) == KUDA_SUCCESS)
        && ((rc = kuda_procattr_dir_set(procattr,
                                       clhy_server_root)) == KUDA_SUCCESS)
        && ((rc = kuda_procattr_cmdtype_set(procattr, cmdtype)) == KUDA_SUCCESS)
        && ((rc = kuda_procattr_io_set(procattr,
                                      KUDA_FULL_BLOCK,
                                      KUDA_NO_PIPE,
                                      KUDA_NO_PIPE)) == KUDA_SUCCESS)
        && ((rc = kuda_procattr_error_check_set(procattr, 1)) == KUDA_SUCCESS)
        && ((rc = kuda_procattr_child_errfn_set(procattr, log_child_errfn))
                == KUDA_SUCCESS)) {
        char **args;

        kuda_tokenize_to_argv(progname, &args, p);
        procnew = (kuda_proc_t *)kuda_pcalloc(p, sizeof(*procnew));

        if (dummy_stderr) {
            if ((rc = kuda_file_open_stdout(&errfile, p)) == KUDA_SUCCESS)
                rc = kuda_procattr_child_err_set(procattr, errfile, NULL);
        }

        rc = kuda_proc_create(procnew, args[0], (const char * const *)args,
                             NULL, procattr, p);

        if (rc == KUDA_SUCCESS) {
            kuda_pool_note_subprocess(p, procnew, KUDA_KILL_AFTER_TIMEOUT);
            (*fpin) = procnew->in;
            /* read handle to pipe not kept open, so no need to call
             * close_handle_in_child()
             */
        }
    }

    return rc;
}

/* Open the error log for the given server_rec.  If IS_MAIN is
 * non-zero, s is the main server. */
static int open_error_log(server_rec *s, int is_main, kuda_pool_t *p)
{
    const char *fname;
    int rc;

    if (*s->error_fname == '|') {
        kuda_file_t *dummy = NULL;
        kuda_cmdtype_e cmdtype = KUDA_PROGRAM_ENV;
        fname = s->error_fname + 1;

        /* In 2.4 favor PROGRAM_ENV, accept "||prog" syntax for compatibility
         * and "|$cmd" to override the default.
         * Any 2.2 backport would continue to favor SHELLCMD_ENV so there
         * accept "||prog" to override, and "|$cmd" to ease conversion.
         */
        if (*fname == '|')
            ++fname;
        if (*fname == '$') {
            cmdtype = KUDA_SHELLCMD_ENV;
            ++fname;
        }

        /* Spawn a new child logger.  If this is the main server_rec,
         * the new child must use a dummy stderr since the current
         * stderr might be a pipe to the old logger.  Otherwise, the
         * child inherits the parents stderr. */
        rc = log_child(p, fname, &dummy, cmdtype, is_main);
        if (rc != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, rc, NULL, CLHYLOGNO(00089)
                         "Couldn't start ErrorLog process '%s'.",
                         s->error_fname + 1);
            return DONE;
        }

        s->error_log = dummy;
    }

#ifdef HAVE_SYSLOG
    else if (!strncasecmp(s->error_fname, "syslog", 6)) {
        if ((fname = strchr(s->error_fname, ':'))) {
            const TRANS *fac;

            fname++;
            for (fac = facilities; fac->t_name; fac++) {
                if (!strcasecmp(fname, fac->t_name)) {
                    openlog(clhy_server_argv0, LOG_NDELAY|LOG_CONS|LOG_PID,
                            fac->t_val);
                    s->error_log = NULL;
                    return OK;
                }
            }
        }
        else {
            openlog(clhy_server_argv0, LOG_NDELAY|LOG_CONS|LOG_PID, LOG_LOCAL7);
        }

        s->error_log = NULL;
    }
#endif
    else {
        fname = clhy_server_root_relative(p, s->error_fname);
        if (!fname) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, KUDA_EBADPATH, NULL, CLHYLOGNO(00090)
                         "%s: Invalid error log path %s.",
                         clhy_server_argv0, s->error_fname);
            return DONE;
        }
        if ((rc = kuda_file_open(&s->error_log, fname,
                               KUDA_APPEND | KUDA_WRITE | KUDA_CREATE | KUDA_LARGEFILE,
                               KUDA_PLATFORM_DEFAULT, p)) != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, rc, NULL, CLHYLOGNO(00091)
                         "%s: could not open error log file %s.",
                         clhy_server_argv0, fname);
            return DONE;
        }
    }

    return OK;
}

int clhy_open_logs(kuda_pool_t *pconf, kuda_pool_t *p /* plog */,
                 kuda_pool_t *ptemp, server_rec *s_main)
{
    kuda_pool_t *stderr_p;
    server_rec *virt, *q;
    int replace_stderr;


    /* Register to throw away the read_handles list when we
     * cleanup plog.  Upon fork() for the clhy children,
     * this read_handles list is closed so only the parent
     * can relaunch a lost log child.  These read handles
     * are always closed on exec.
     * We won't care what happens to our stderr log child
     * between log phases, so we don't mind losing stderr's
     * read_handle a little bit early.
     */
    kuda_pool_cleanup_register(p, &read_handles, clhy_pool_cleanup_set_null,
                              kuda_pool_cleanup_null);

    /* HERE we need a stdout log that outlives plog.
     * We *presume* the parent of plog is a process
     * or global pool which spans server restarts.
     * Create our stderr_pool as a child of the plog's
     * parent pool.
     */
    kuda_pool_create(&stderr_p, kuda_pool_parent_get(p));
    kuda_pool_tag(stderr_p, "stderr_pool");

    if (open_error_log(s_main, 1, stderr_p) != OK) {
        return DONE;
    }

    replace_stderr = 1;
    if (s_main->error_log) {
        kuda_status_t rv;

        /* Replace existing stderr with new log. */
        kuda_file_flush(s_main->error_log);
        rv = kuda_file_dup2(stderr_log, s_main->error_log, stderr_p);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s_main, CLHYLOGNO(00092)
                         "unable to replace stderr with error_log");
        }
        else {
            /* We are done with stderr_pool, close it, killing
             * the previous generation's stderr logger
             */
            if (stderr_pool)
                kuda_pool_destroy(stderr_pool);
            stderr_pool = stderr_p;
            replace_stderr = 0;
            /*
             * Now that we have dup'ed s_main->error_log to stderr_log
             * close it and set s_main->error_log to stderr_log. This avoids
             * this fd being inherited by the next piped logger who would
             * keep open the writing end of the pipe that this one uses
             * as stdin. This in turn would prevent the piped logger from
             * exiting.
             */
            kuda_file_close(s_main->error_log);
            s_main->error_log = stderr_log;
        }
    }
    /* note that stderr may still need to be replaced with something
     * because it points to the old error log, or back to the tty
     * of the submitter.
     * XXX: This is BS - /dev/null is non-portable
     *      errno-as-kuda_status_t is also non-portable
     */

#ifdef WIN32
#define NULL_DEVICE "nul"
#else
#define NULL_DEVICE "/dev/null"
#endif

    if (replace_stderr && freopen(NULL_DEVICE, "w", stderr) == NULL) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, errno, s_main, CLHYLOGNO(00093)
                     "unable to replace stderr with %s", NULL_DEVICE);
    }

    for (virt = s_main->next; virt; virt = virt->next) {
        if (virt->error_fname) {
            for (q=s_main; q != virt; q = q->next) {
                if (q->error_fname != NULL
                    && strcmp(q->error_fname, virt->error_fname) == 0) {
                    break;
                }
            }

            if (q == virt) {
                if (open_error_log(virt, 0, p) != OK) {
                    return DONE;
                }
            }
            else {
                virt->error_log = q->error_log;
            }
        }
        else {
            virt->error_log = s_main->error_log;
        }
    }
    return OK;
}

CLHY_DECLARE(void) clhy_error_log2stderr(server_rec *s) {
    kuda_file_t *errfile = NULL;

    kuda_file_open_stderr(&errfile, s->process->pool);
    if (s->error_log != NULL) {
        kuda_file_dup2(s->error_log, errfile, s->process->pool);
    }
}

static int cpystrn(char *buf, const char *arg, int buflen)
{
    char *end;
    if (!arg)
        return 0;
    end = kuda_cpystrn(buf, arg, buflen);
    return end - buf;
}


static int log_remote_address(const clhy_errorlog_info *info, const char *arg,
                              char *buf, int buflen)
{
    if (info->r && !(arg && *arg == 'c'))
        return kuda_snprintf(buf, buflen, "%s:%d", info->r->useragent_ip,
                            info->r->useragent_addr ? info->r->useragent_addr->port : 0);
    else if (info->c)
        return kuda_snprintf(buf, buflen, "%s:%d", info->c->client_ip,
                            info->c->client_addr ? info->c->client_addr->port : 0);
    else
        return 0;
}

static int log_local_address(const clhy_errorlog_info *info, const char *arg,
                             char *buf, int buflen)
{
    if (info->c)
        return kuda_snprintf(buf, buflen, "%s:%d", info->c->local_ip,
                            info->c->local_addr->port);
    else
        return 0;
}

static int log_pid(const clhy_errorlog_info *info, const char *arg,
                   char *buf, int buflen)
{
    pid_t pid = getpid();
    return kuda_snprintf(buf, buflen, "%" KUDA_PID_T_FMT, pid);
}

static int log_tid(const clhy_errorlog_info *info, const char *arg,
                   char *buf, int buflen)
{
#if KUDA_HAS_THREADS
    int result;
#endif
#if HAVE_GETTID
    if (arg && *arg == 'g') {
        pid_t tid = syscall(SYS_gettid);
        if (tid == -1)
            return 0;
        return kuda_snprintf(buf, buflen, "%"KUDA_PID_T_FMT, tid);
    }
#endif
#if KUDA_HAS_THREADS
    if (clhy_clmp_query(CLHY_CLMPQ_IS_THREADED, &result) == KUDA_SUCCESS
        && result != CLHY_CLMPQ_NOT_SUPPORTED)
    {
        kuda_platform_thread_t tid = kuda_platform_thread_current();
        return kuda_snprintf(buf, buflen, "%pT", &tid);
    }
#endif
    return 0;
}

static int log_ctime(const clhy_errorlog_info *info, const char *arg,
                     char *buf, int buflen)
{
    int time_len = buflen;
    int option = CLHY_CTIME_OPTION_NONE;

    while (arg && *arg) {
        switch (*arg) {
            case 'u':   option |= CLHY_CTIME_OPTION_USEC;
                        break;
            case 'c':   option |= CLHY_CTIME_OPTION_COMPACT;
                        break;
        }
        arg++;
    }

    clhy_recent_ctime_ex(buf, kuda_time_now(), option, &time_len);

    /* clhy_recent_ctime_ex includes the trailing \0 in time_len */
    return time_len - 1;
}

static int log_loglevel(const clhy_errorlog_info *info, const char *arg,
                        char *buf, int buflen)
{
    if (info->level < 0)
        return 0;
    else
        return cpystrn(buf, priorities[info->level].t_name, buflen);
}

static int log_log_id(const clhy_errorlog_info *info, const char *arg,
                      char *buf, int buflen)
{
    /*
     * C: log conn log_id if available,
     * c: log conn log id if available and not a once-per-request log line
     * else: log request log id if available
     */
    if (arg && !strcasecmp(arg, "c")) {
        if (info->c && (*arg != 'C' || !info->r)) {
            return cpystrn(buf, info->c->log_id, buflen);
        }
    }
    else if (info->rmain) {
        return cpystrn(buf, info->rmain->log_id, buflen);
    }
    return 0;
}

static int log_keepalives(const clhy_errorlog_info *info, const char *arg,
                          char *buf, int buflen)
{
    if (!info->c)
        return 0;

    return kuda_snprintf(buf, buflen, "%d", info->c->keepalives);
}

static int log_capi_name(const clhy_errorlog_info *info, const char *arg,
                           char *buf, int buflen)
{
    return cpystrn(buf, clhy_find_capi_short_name(info->capi_index), buflen);
}

static int log_file_line(const clhy_errorlog_info *info, const char *arg,
                         char *buf, int buflen)
{
    if (info->file == NULL) {
        return 0;
    }
    else {
        const char *file = info->file;
#if defined(_OSD_POSIX) || defined(WIN32) || defined(__MVS__)
        char tmp[256];
        char *e = strrchr(file, '/');
#ifdef WIN32
        if (!e) {
            e = strrchr(file, '\\');
        }
#endif

        /* In OSD/POSIX, the compiler returns for __FILE__
         * a string like: __FILE__="*POSIX(/usr/include/stdio.h)"
         * (it even returns an absolute path for sources in
         * the current directory). Here we try to strip this
         * down to the basename.
         */
        if (e != NULL && e[1] != '\0') {
            kuda_snprintf(tmp, sizeof(tmp), "%s", &e[1]);
            e = &tmp[strlen(tmp)-1];
            if (*e == ')') {
                *e = '\0';
            }
            file = tmp;
        }
#else /* _OSD_POSIX || WIN32 */
        const char *p;
        /* On Unix, __FILE__ may be an absolute path in a
         * VPATH build. */
        if (file[0] == '/' && (p = clhy_strrchr_c(file, '/')) != NULL) {
            file = p + 1;
        }
#endif /*_OSD_POSIX || WIN32 */
        return kuda_snprintf(buf, buflen, "%s(%d)", file, info->line);
    }
}

static int log_kuda_status(const clhy_errorlog_info *info, const char *arg,
                          char *buf, int buflen)
{
    kuda_status_t status = info->status;
    int len;
    if (!status)
        return 0;

    if (status < KUDA_PLATFORM_START_EAIERR) {
        len = kuda_snprintf(buf, buflen, "(%d)", status);
    }
    else if (status < KUDA_PLATFORM_START_SYSERR) {
        len = kuda_snprintf(buf, buflen, "(EAI %d)",
                           status - KUDA_PLATFORM_START_EAIERR);
    }
    else if (status < 100000 + KUDA_PLATFORM_START_SYSERR) {
        len = kuda_snprintf(buf, buflen, "(PLATFORM %d)",
                           status - KUDA_PLATFORM_START_SYSERR);
    }
    else {
        len = kuda_snprintf(buf, buflen, "(platforms 0x%08x)",
                           status - KUDA_PLATFORM_START_SYSERR);
    }
    kuda_strerror(status, buf + len, buflen - len);
    len += strlen(buf + len);
    return len;
}

static int log_server_name(const clhy_errorlog_info *info, const char *arg,
                           char *buf, int buflen)
{
    if (info->r)
        return cpystrn(buf, clhy_get_server_name((request_rec *)info->r), buflen);

    return 0;
}

static int log_virtual_host(const clhy_errorlog_info *info, const char *arg,
                            char *buf, int buflen)
{
    if (info->s)
        return cpystrn(buf, info->s->server_hostname, buflen);

    return 0;
}


static int log_table_entry(const kuda_table_t *table, const char *name,
                           char *buf, int buflen)
{
#ifndef CLHY_UNSAFE_ERROR_LOG_UNESCAPED
    const char *value;
    char scratch[MAX_STRING_LEN];

    if ((value = kuda_table_get(table, name)) != NULL) {
        clhy_escape_errorlog_item(scratch, value, MAX_STRING_LEN);
        return cpystrn(buf, scratch, buflen);
    }

    return 0;
#else
    return cpystrn(buf, kuda_table_get(table, name), buflen);
#endif
}

static int log_header(const clhy_errorlog_info *info, const char *arg,
                      char *buf, int buflen)
{
    if (info->r)
        return log_table_entry(info->r->headers_in, arg, buf, buflen);

    return 0;
}

static int log_note(const clhy_errorlog_info *info, const char *arg,
                      char *buf, int buflen)
{
    /* XXX: maybe escaping the entry is not necessary for notes? */
    if (info->r)
        return log_table_entry(info->r->notes, arg, buf, buflen);

    return 0;
}

static int log_env_var(const clhy_errorlog_info *info, const char *arg,
                      char *buf, int buflen)
{
    if (info->r)
        return log_table_entry(info->r->subprocess_env, arg, buf, buflen);

    return 0;
}

static int core_generate_log_id(const conn_rec *c, const request_rec *r,
                                 const char **idstring)
{
    kuda_uint64_t id, tmp;
    pid_t pid;
    int len;
    char *encoded;

    if (r && r->request_time) {
        id = (kuda_uint64_t)r->request_time;
    }
    else {
        id = (kuda_uint64_t)kuda_time_now();
    }

    pid = getpid();
    if (sizeof(pid_t) > 2) {
        tmp = pid;
        tmp = tmp << 40;
        id ^= tmp;
        pid = pid >> 24;
        tmp = pid;
        tmp = tmp << 56;
        id ^= tmp;
    }
    else {
        tmp = pid;
        tmp = tmp << 40;
        id ^= tmp;
    }
#if KUDA_HAS_THREADS
    {
        kuda_uintptr_t tmp2 = (kuda_uintptr_t)c->current_thread;
        tmp = tmp2;
        tmp = tmp << 32;
        id ^= tmp;
    }
#endif

    len = kuda_base64_encode_len(sizeof(id)); /* includes trailing \0 */
    encoded = kuda_palloc(r ? r->pool : c->pool, len);
    kuda_base64_encode(encoded, (char *)&id, sizeof(id));

    /* Skip the last char, it is always '=' */
    encoded[len - 2] = '\0';

    *idstring = encoded;

    return OK;
}

static void add_log_id(const conn_rec *c, const request_rec *r)
{
    const char **id;
    /* need to cast const away */
    if (r) {
        id = &((request_rec *)r)->log_id;
    }
    else {
        id = &((conn_rec *)c)->log_id;
    }

    clhy_run_generate_log_id(c, r, id);
}

CLHY_DECLARE(void) clhy_register_log_hooks(kuda_pool_t *p)
{
    clhy_hook_generate_log_id(core_generate_log_id, NULL, NULL,
                            KUDA_HOOK_REALLY_LAST);

    clhy_register_errorlog_handler(p, "a", log_remote_address, 0);
    clhy_register_errorlog_handler(p, "A", log_local_address, 0);
    clhy_register_errorlog_handler(p, "e", log_env_var, 0);
    clhy_register_errorlog_handler(p, "E", log_kuda_status, 0);
    clhy_register_errorlog_handler(p, "F", log_file_line, 0);
    clhy_register_errorlog_handler(p, "i", log_header, 0);
    clhy_register_errorlog_handler(p, "k", log_keepalives, 0);
    clhy_register_errorlog_handler(p, "l", log_loglevel, 0);
    clhy_register_errorlog_handler(p, "L", log_log_id, 0);
    clhy_register_errorlog_handler(p, "m", log_capi_name, 0);
    clhy_register_errorlog_handler(p, "n", log_note, 0);
    clhy_register_errorlog_handler(p, "P", log_pid, 0);
    clhy_register_errorlog_handler(p, "t", log_ctime, 0);
    clhy_register_errorlog_handler(p, "T", log_tid, 0);
    clhy_register_errorlog_handler(p, "v", log_virtual_host, 0);
    clhy_register_errorlog_handler(p, "V", log_server_name, 0);
}

/*
 * This is used if no error log format is defined and during startup.
 * It automatically omits the timestamp if logging to syslog.
 */
static int do_errorlog_default(const clhy_errorlog_info *info, char *buf,
                               int buflen, int *errstr_start, int *errstr_end,
                               const char *errstr_fmt, va_list args)
{
    int len = 0;
    int field_start = 0;
    int item_len;
#ifndef CLHY_UNSAFE_ERROR_LOG_UNESCAPED
    char scratch[MAX_STRING_LEN];
#endif

    if (!info->using_syslog && !info->startup) {
        buf[len++] = '[';
        len += log_ctime(info, "u", buf + len, buflen - len);
        buf[len++] = ']';
        buf[len++] = ' ';
    }

    if (!info->startup) {
        buf[len++] = '[';
        len += log_capi_name(info, NULL, buf + len, buflen - len);
        buf[len++] = ':';
        len += log_loglevel(info, NULL, buf + len, buflen - len);
        len += cpystrn(buf + len, "] [pid ", buflen - len);

        len += log_pid(info, NULL, buf + len, buflen - len);
#if KUDA_HAS_THREADS
        field_start = len;
        len += cpystrn(buf + len, ":tid ", buflen - len);
        item_len = log_tid(info, NULL, buf + len, buflen - len);
        if (!item_len)
            len = field_start;
        else
            len += item_len;
#endif
        buf[len++] = ']';
        buf[len++] = ' ';
    }

    if (info->level >= CLHYLOG_DEBUG) {
        item_len = log_file_line(info, NULL, buf + len, buflen - len);
        if (item_len) {
            len += item_len;
            len += cpystrn(buf + len, ": ", buflen - len);
        }
    }

    if (info->status) {
        item_len = log_kuda_status(info, NULL, buf + len, buflen - len);
        if (item_len) {
            len += item_len;
            len += cpystrn(buf + len, ": ", buflen - len);
        }
    }

    /*
     * useragent_ip/client_ip can be client or backend server. If we have
     * a scoreboard handle, it is likely a client.
     */
    if (info->r) {
        len += kuda_snprintf(buf + len, buflen - len,
                            info->r->connection->sbh ? "[client %s:%d] " : "[remote %s:%d] ",
                            info->r->useragent_ip,
                            info->r->useragent_addr ? info->r->useragent_addr->port : 0);
    }
    else if (info->c) {
        len += kuda_snprintf(buf + len, buflen - len,
                            info->c->sbh ? "[client %s:%d] " : "[remote %s:%d] ",
                            info->c->client_ip,
                            info->c->client_addr ? info->c->client_addr->port : 0);
    }

    /* the actual error message */
    *errstr_start = len;
#ifndef CLHY_UNSAFE_ERROR_LOG_UNESCAPED
    if (kuda_vsnprintf(scratch, MAX_STRING_LEN, errstr_fmt, args)) {
        len += clhy_escape_errorlog_item(buf + len, scratch,
                                       buflen - len);

    }
#else
    len += kuda_vsnprintf(buf + len, buflen - len, errstr_fmt, args);
#endif
    *errstr_end = len;

    field_start = len;
    len += cpystrn(buf + len, ", referer: ", buflen - len);
    item_len = log_header(info, "Referer", buf + len, buflen - len);
    if (item_len)
        len += item_len;
    else
        len = field_start;

    return len;
}

static int do_errorlog_format(kuda_array_header_t *fmt, clhy_errorlog_info *info,
                              char *buf, int buflen, int *errstr_start,
                              int *errstr_end, const char *err_fmt, va_list args)
{
#ifndef CLHY_UNSAFE_ERROR_LOG_UNESCAPED
    char scratch[MAX_STRING_LEN];
#endif
    int i;
    int len = 0;
    int field_start = 0;
    int skipping = 0;
    clhy_errorlog_format_item *items = (clhy_errorlog_format_item *)fmt->elts;

    CLHY_DEBUG_ASSERT(fmt->nelts > 0);
    for (i = 0; i < fmt->nelts; ++i) {
        clhy_errorlog_format_item *item = &items[i];
        if (item->flags & CLHY_ERRORLOG_FLAG_FIELD_SEP) {
            if (skipping) {
                skipping = 0;
            }
            else {
                field_start = len;
            }
        }

        if (item->flags & CLHY_ERRORLOG_FLAG_MESSAGE) {
            /* the actual error message */
            *errstr_start = len;
#ifndef CLHY_UNSAFE_ERROR_LOG_UNESCAPED
            if (kuda_vsnprintf(scratch, MAX_STRING_LEN, err_fmt, args)) {
                len += clhy_escape_errorlog_item(buf + len, scratch,
                                               buflen - len);

            }
#else
            len += kuda_vsnprintf(buf + len, buflen - len, err_fmt, args);
#endif
            *errstr_end = len;
        }
        else if (skipping) {
            continue;
        }
        else if (info->level != -1 && (int)item->min_loglevel > info->level) {
            len = field_start;
            skipping = 1;
        }
        else {
            int item_len = (*item->func)(info, item->arg, buf + len,
                                         buflen - len);
            if (!item_len) {
                if (item->flags & CLHY_ERRORLOG_FLAG_REQUIRED) {
                    /* required item is empty. skip whole line */
                    buf[0] = '\0';
                    return 0;
                }
                else if (item->flags & CLHY_ERRORLOG_FLAG_NULL_AS_HYPHEN) {
                    buf[len++] = '-';
                }
                else {
                    len = field_start;
                    skipping = 1;
                }
            }
            else {
                len += item_len;
            }
        }
    }
    return len;
}

static void write_logline(char *errstr, kuda_size_t len, kuda_file_t *logf,
                          int level)
{
    /* NULL if we are logging to syslog */
    if (logf) {
        /* Truncate for the terminator (as kuda_snprintf does) */
        if (len > MAX_STRING_LEN - sizeof(KUDA_EOL_STR)) {
            len = MAX_STRING_LEN - sizeof(KUDA_EOL_STR);
        }
        strcpy(errstr + len, KUDA_EOL_STR);
        kuda_file_puts(errstr, logf);
        kuda_file_flush(logf);
    }
#ifdef HAVE_SYSLOG
    else {
        syslog(level < LOG_PRIMASK ? level : CLHYLOG_DEBUG, "%.*s",
               (int)len, errstr);
    }
#endif
}

static void log_error_core(const char *file, int line, int capi_index,
                           int level,
                           kuda_status_t status, const server_rec *s,
                           const conn_rec *c,
                           const request_rec *r, kuda_pool_t *pool,
                           const char *fmt, va_list args)
{
    char errstr[MAX_STRING_LEN];
    kuda_file_t *logf = NULL;
    int level_and_mask = level & CLHYLOG_LEVELMASK;
    const request_rec *rmain = NULL;
    core_server_config *sconf = NULL;
    clhy_errorlog_info info;

    /* do we need to log once-per-req or once-per-conn info? */
    int log_conn_info = 0, log_req_info = 0;
    kuda_array_header_t **lines = NULL;
    int done = 0;
    int line_number = 0;

    if (r) {
        CLHY_DEBUG_ASSERT(r->connection != NULL);
        c = r->connection;
    }

    if (s == NULL) {
        /*
         * If we are doing stderr logging (startup), don't log messages that are
         * above the default server log level unless it is a startup/shutdown
         * notice
         */
#ifndef DEBUG
        if ((level_and_mask != CLHYLOG_NOTICE)
            && (level_and_mask > clhy_default_loglevel)) {
            return;
        }
#endif

        logf = stderr_log;
    }
    else {
        int configured_level = r ? clhy_get_request_capi_loglevel(r, capi_index)        :
                               c ? clhy_get_conn_server_capi_loglevel(c, s, capi_index) :
                                   clhy_get_server_capi_loglevel(s, capi_index);
        if (s->error_log) {
            /*
             * If we are doing normal logging, don't log messages that are
             * above the cAPI's log level unless it is a startup/shutdown notice
             */
            if ((level_and_mask != CLHYLOG_NOTICE)
                && (level_and_mask > configured_level)) {
                return;
            }

            logf = s->error_log;
        }
        else {
            /*
             * If we are doing syslog logging, don't log messages that are
             * above the cAPI's log level (including a startup/shutdown notice)
             */
            if (level_and_mask > configured_level) {
                return;
            }
        }

        /* the faked server_rec from capi_cgid does not have s->capi_config */
        if (s->capi_config) {
            sconf = clhy_get_core_capi_config(s->capi_config);
            if (c && !c->log_id) {
                add_log_id(c, NULL);
                if (sconf->error_log_conn && sconf->error_log_conn->nelts > 0)
                    log_conn_info = 1;
            }
            if (r) {
                if (r->main)
                    rmain = r->main;
                else
                    rmain = r;

                if (!rmain->log_id) {
                    /* XXX: do we need separate log ids for subrequests? */
                    if (sconf->error_log_req && sconf->error_log_req->nelts > 0)
                        log_req_info = 1;
                    /*
                     * XXX: potential optimization: only create log id if %L is
                     * XXX: actually used
                     */
                    add_log_id(c, rmain);
                }
            }
        }
    }

    info.s             = s;
    info.c             = c;
    info.pool          = pool;
    info.file          = NULL;
    info.line          = 0;
    info.status        = 0;
    info.using_syslog  = (logf == NULL);
    info.startup       = ((level & CLHYLOG_STARTUP) == CLHYLOG_STARTUP);
    info.format        = fmt;

    while (!done) {
        kuda_array_header_t *log_format;
        int len = 0, errstr_start = 0, errstr_end = 0;
        /* XXX: potential optimization: format common prefixes only once */
        if (log_conn_info) {
            /* once-per-connection info */
            if (line_number == 0) {
                lines = (kuda_array_header_t **)sconf->error_log_conn->elts;
                info.r = NULL;
                info.rmain = NULL;
                info.level = -1;
                info.capi_index = CLHYLOG_NO_CAPI;
            }

            log_format = lines[line_number++];

            if (line_number == sconf->error_log_conn->nelts) {
                /* this is the last line of once-per-connection info */
                line_number = 0;
                log_conn_info = 0;
            }
        }
        else if (log_req_info) {
            /* once-per-request info */
            if (line_number == 0) {
                lines = (kuda_array_header_t **)sconf->error_log_req->elts;
                info.r = rmain;
                info.rmain = rmain;
                info.level = -1;
                info.capi_index = CLHYLOG_NO_CAPI;
            }

            log_format = lines[line_number++];

            if (line_number == sconf->error_log_req->nelts) {
                /* this is the last line of once-per-request info */
                line_number = 0;
                log_req_info = 0;
            }
        }
        else {
            /* the actual error message */
            info.r            = r;
            info.rmain        = rmain;
            info.level        = level_and_mask;
            info.capi_index = capi_index;
            info.file         = file;
            info.line         = line;
            info.status       = status;
            log_format = sconf ? sconf->error_log_format : NULL;
            done = 1;
        }

        /*
         * prepare and log one line
         */

        if (log_format && !info.startup) {
            len += do_errorlog_format(log_format, &info, errstr + len,
                                      MAX_STRING_LEN - len,
                                      &errstr_start, &errstr_end, fmt, args);
        }
        else {
            len += do_errorlog_default(&info, errstr + len, MAX_STRING_LEN - len,
                                       &errstr_start, &errstr_end, fmt, args);
        }

        if (!*errstr) {
            /*
             * Don't log empty lines. This can happen with once-per-conn/req
             * info if an item with CLHY_ERRORLOG_FLAG_REQUIRED is NULL.
             */
            continue;
        }
        write_logline(errstr, len, logf, level_and_mask);

        if (done) {
            /*
             * We don't call the error_log hook for per-request/per-conn
             * lines, and we only pass the actual log message, not the
             * prefix and suffix.
             */
            errstr[errstr_end] = '\0';
            clhy_run_error_log(&info, errstr + errstr_start);
        }

        *errstr = '\0';
    }
}

/* For internal calls to log_error_core with self-composed arg lists */
static void log_error_va_glue(const char *file, int line, int capi_index,
                              int level, kuda_status_t status,
                              const server_rec *s, const conn_rec *c,
                              const request_rec *r, kuda_pool_t *pool,
                              const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    log_error_core(file, line, capi_index, level, status, s, c, r, pool,
                   fmt, args);
    va_end(args);
}

CLHY_DECLARE(void) clhy_log_error_(const char *file, int line, int capi_index,
                               int level, kuda_status_t status,
                               const server_rec *s, const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    log_error_core(file, line, capi_index, level, status, s, NULL, NULL,
                   NULL, fmt, args);
    va_end(args);
}

CLHY_DECLARE(void) clhy_log_perror_(const char *file, int line, int capi_index,
                                int level, kuda_status_t status, kuda_pool_t *p,
                                const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    log_error_core(file, line, capi_index, level, status, NULL, NULL, NULL,
                   p, fmt, args);
    va_end(args);
}

CLHY_DECLARE(void) clhy_log_rerror_(const char *file, int line, int capi_index,
                                int level, kuda_status_t status,
                                const request_rec *r, const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    log_error_core(file, line, capi_index, level, status, r->server, NULL, r,
                   NULL, fmt, args);

    /*
     * IF CLHYLOG_TOCLIENT is set,
     * AND the error level is 'warning' or more severe,
     * AND there isn't already error text associated with this request,
     * THEN make the message text available to ErrorDocument and
     * other error processors.
     */
    va_end(args);
    va_start(args,fmt);
    if ((level & CLHYLOG_TOCLIENT)
        && ((level & CLHYLOG_LEVELMASK) <= CLHYLOG_WARNING)
        && (kuda_table_get(r->notes, "error-notes") == NULL)) {
        kuda_table_setn(r->notes, "error-notes",
                       clhy_escape_html(r->pool, kuda_pvsprintf(r->pool, fmt,
                                                             args)));
    }
    va_end(args);
}

CLHY_DECLARE(void) clhy_log_cserror_(const char *file, int line, int capi_index,
                                 int level, kuda_status_t status,
                                 const conn_rec *c, const server_rec *s,
                                 const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    log_error_core(file, line, capi_index, level, status, s, c,
                   NULL, NULL, fmt, args);
    va_end(args);
}

CLHY_DECLARE(void) clhy_log_cerror_(const char *file, int line, int capi_index,
                                int level, kuda_status_t status,
                                const conn_rec *c, const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    log_error_core(file, line, capi_index, level, status, c->base_server, c,
                   NULL, NULL, fmt, args);
    va_end(args);
}

#define BYTES_LOGGED_PER_LINE 16
#define LOG_BYTES_BUFFER_SIZE (BYTES_LOGGED_PER_LINE * 3 + 2)

static void fmt_data(unsigned char *buf, const void *vdata, kuda_size_t len, kuda_size_t *off)
{
    const unsigned char *data = (const unsigned char *)vdata;
    unsigned char *chars;
    unsigned char *hex;
    kuda_size_t this_time = 0;

    memset(buf, ' ', LOG_BYTES_BUFFER_SIZE - 1);
    buf[LOG_BYTES_BUFFER_SIZE - 1] = '\0';
    
    chars = buf; /* start character dump here */
    hex   = buf + BYTES_LOGGED_PER_LINE + 1; /* start hex dump here */
    while (*off < len && this_time < BYTES_LOGGED_PER_LINE) {
        unsigned char c = data[*off];

        if (kuda_isprint(c)
            && c != '\\') {  /* backslash will be escaped later, which throws
                              * off the formatting
                              */
            *chars = c;
        }
        else {
            *chars = '.';
        }

        if ((c >> 4) >= 10) {
            *hex = 'a' + ((c >> 4) - 10);
        }
        else {
            *hex = '0' + (c >> 4);
        }

        if ((c & 0x0F) >= 10) {
            *(hex + 1) = 'a' + ((c & 0x0F) - 10);
        }
        else {
            *(hex + 1) = '0' + (c & 0x0F);
        }

        chars += 1;
        hex += 2;
        *off += 1;
        ++this_time;
    }
}

static void log_data_core(const char *file, int line, int capi_index,
                          int level, const server_rec *s,
                          const conn_rec *c, const request_rec *r,
                          const char *label, const void *data, kuda_size_t len,
                          unsigned int flags)
{
    unsigned char buf[LOG_BYTES_BUFFER_SIZE];
    kuda_size_t off;
    char prefix[20];

    if (!(flags & CLHY_LOG_DATA_SHOW_OFFSET)) {
        prefix[0] = '\0';
    }

    if (len > 0xffff) { /* bug in caller? */
        len = 0xffff;
    }

    if (label) {
        log_error_va_glue(file, line, capi_index, level, KUDA_SUCCESS, s,
                          c, r, NULL, "%s (%" KUDA_SIZE_T_FMT " bytes)",
                          label, len);
    }

    off = 0;
    while (off < len) {
        if (flags & CLHY_LOG_DATA_SHOW_OFFSET) {
            kuda_snprintf(prefix, sizeof prefix, "%04x: ", (unsigned int)off);
        }
        fmt_data(buf, data, len, &off);
        log_error_va_glue(file, line, capi_index, level, KUDA_SUCCESS, s,
                          c, r, NULL, "%s%s", prefix, buf);
    }
}

CLHY_DECLARE(void) clhy_log_data_(const char *file, int line, 
                              int capi_index, int level,
                              const server_rec *s, const char *label,
                              const void *data, kuda_size_t len,
                              unsigned int flags)
{
    log_data_core(file, line, capi_index, level, s, NULL, NULL, label,
                  data, len, flags);
}

CLHY_DECLARE(void) clhy_log_rdata_(const char *file, int line,
                               int capi_index, int level,
                               const request_rec *r, const char *label,
                               const void *data, kuda_size_t len,
                               unsigned int flags)
{
    log_data_core(file, line, capi_index, level, r->server, NULL, r, label,
                  data, len, flags);
}

CLHY_DECLARE(void) clhy_log_cdata_(const char *file, int line,
                               int capi_index, int level,
                               const conn_rec *c, const char *label,
                               const void *data, kuda_size_t len,
                               unsigned int flags)
{
    log_data_core(file, line, capi_index, level, c->base_server, c, NULL,
                  label, data, len, flags);
}

CLHY_DECLARE(void) clhy_log_csdata_(const char *file, int line, int capi_index,
                                int level, const conn_rec *c, const server_rec *s,
                                const char *label, const void *data,
                                kuda_size_t len, unsigned int flags)
{
    log_data_core(file, line, capi_index, level, s, c, NULL, label, data,
                  len, flags);
}

CLHY_DECLARE(void) clhy_log_command_line(kuda_pool_t *plog, server_rec *s)
{
    int i;
    process_rec *process = s->process;
    char *result;
    int len_needed = 0;

    /* Piece together the command line from the pieces
     * in process->argv, with spaces in between.
     */
    for (i = 0; i < process->argc; i++) {
        len_needed += strlen(process->argv[i]) + 1;
    }

    result = (char *) kuda_palloc(plog, len_needed);
    *result = '\0';

    for (i = 0; i < process->argc; i++) {
        strcat(result, process->argv[i]);
        if ((i+1)< process->argc) {
            strcat(result, " ");
        }
    }
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, s, CLHYLOGNO(00094)
                 "Command line: '%s'", result);
}

/* grab bag function to log commonly logged and shared info */
CLHY_DECLARE(void) clhy_log_core_common(server_rec *s)
{
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG , 0, s, CLHYLOGNO(02639)
                 "Using SO_REUSEPORT: %s (%d)",
                 clhy_have_so_reuseport ? "yes" : "no",
                 clhy_num_listen_buckets);
}

CLHY_DECLARE(void) clhy_remove_pid(kuda_pool_t *p, const char *rel_fname)
{
    kuda_status_t rv;
    const char *fname = clhy_server_root_relative(p, rel_fname);

    if (fname != NULL) {
        rv = kuda_file_remove(fname, p);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, clhy_server_conf, CLHYLOGNO(00095)
                         "failed to remove PID file %s", fname);
        }
        else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, clhy_server_conf, CLHYLOGNO(00096)
                         "removed PID file %s (pid=%" KUDA_PID_T_FMT ")",
                         fname, getpid());
        }
    }
}

CLHY_DECLARE(void) clhy_log_pid(kuda_pool_t *p, const char *filename)
{
    kuda_file_t *pid_file = NULL;
    kuda_finfo_t finfo;
    static pid_t saved_pid = -1;
    pid_t mypid;
    kuda_status_t rv;
    const char *fname;

    if (!filename) {
        return;
    }

    fname = clhy_server_root_relative(p, filename);
    if (!fname) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP|CLHYLOG_CRIT, KUDA_EBADPATH,
                     NULL, CLHYLOGNO(00097) "Invalid PID file path %s, ignoring.", filename);
        return;
    }

    mypid = getpid();
    if (mypid != saved_pid
        && kuda_stat(&finfo, fname, KUDA_FINFO_MTIME, p) == KUDA_SUCCESS) {
        /* CLHY_SIG_GRACEFUL and HUP call this on each restart.
         * Only warn on first time through for this pid.
         *
         * XXX: Could just write first time through too, although
         *      that may screw up scripts written to do something
         *      based on the last modification time of the pid file.
         */
        clhy_log_perror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, p, CLHYLOGNO(00098)
                      "pid file %s overwritten -- Unclean "
                      "shutdown of previous cLHy run?",
                      fname);
    }

    if ((rv = kuda_file_open(&pid_file, fname,
                            KUDA_WRITE | KUDA_CREATE | KUDA_TRUNCATE,
                            KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD | KUDA_WREAD, p))
        != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, NULL, CLHYLOGNO(00099)
                     "could not create %s", fname);
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, NULL, CLHYLOGNO(00100)
                     "%s: could not log pid to file %s",
                     clhy_server_argv0, fname);
        exit(1);
    }
    kuda_file_printf(pid_file, "%" KUDA_PID_T_FMT KUDA_EOL_STR, mypid);
    kuda_file_close(pid_file);
    saved_pid = mypid;
}

CLHY_DECLARE(kuda_status_t) clhy_read_pid(kuda_pool_t *p, const char *filename,
                                     pid_t *mypid)
{
    const kuda_size_t BUFFER_SIZE = sizeof(long) * 3 + 2; /* see kuda_ltoa */
    kuda_file_t *pid_file = NULL;
    kuda_status_t rv;
    const char *fname;
    char *buf, *endptr;
    kuda_size_t bytes_read;

    if (!filename) {
        return KUDA_EGENERAL;
    }

    fname = clhy_server_root_relative(p, filename);
    if (!fname) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP|CLHYLOG_CRIT, KUDA_EBADPATH,
                     NULL, CLHYLOGNO(00101) "Invalid PID file path %s, ignoring.", filename);
        return KUDA_EGENERAL;
    }

    rv = kuda_file_open(&pid_file, fname, KUDA_READ, KUDA_PLATFORM_DEFAULT, p);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    buf = kuda_palloc(p, BUFFER_SIZE);

    rv = kuda_file_read_full(pid_file, buf, BUFFER_SIZE - 1, &bytes_read);
    if (rv != KUDA_SUCCESS && rv != KUDA_EOF) {
        return rv;
    }

    /* If we fill the buffer, we're probably reading a corrupt pid file.
     * To be nice, let's also ensure the first char is a digit. */
    if (bytes_read == 0 || bytes_read == BUFFER_SIZE - 1 || !kuda_isdigit(*buf)) {
        return KUDA_EGENERAL;
    }

    buf[bytes_read] = '\0';
    *mypid = strtol(buf, &endptr, 10);

    kuda_file_close(pid_file);
    return KUDA_SUCCESS;
}

CLHY_DECLARE(void) clhy_log_assert(const char *szExp, const char *szFile,
                               int nLine)
{
    char time_str[KUDA_CTIME_LEN];

    kuda_ctime(time_str, kuda_time_now());
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, NULL, CLHYLOGNO(00102)
                 "[%s] file %s, line %d, assertion \"%s\" failed",
                 time_str, szFile, nLine, szExp);
#if defined(WIN32)
    DebugBreak();
#else
    /* unix assert does an abort leading to a core dump */
    abort();
#endif
}

/* piped log support */

#ifdef CLHY_HAVE_RELIABLE_PIPED_LOGS
/* forward declaration */
static void piped_log_maintenance(int reason, void *data, kuda_wait_t status);

/* Spawn the piped logger process pl->program. */
static kuda_status_t piped_log_spawn(piped_log *pl)
{
    kuda_procattr_t *procattr;
    kuda_proc_t *procnew = NULL;
    kuda_status_t status;

    if (((status = kuda_procattr_create(&procattr, pl->p)) != KUDA_SUCCESS) ||
        ((status = kuda_procattr_dir_set(procattr, clhy_server_root))
         != KUDA_SUCCESS) ||
        ((status = kuda_procattr_cmdtype_set(procattr, pl->cmdtype))
         != KUDA_SUCCESS) ||
        ((status = kuda_procattr_child_in_set(procattr,
                                             pl->read_fd,
                                             pl->write_fd))
         != KUDA_SUCCESS) ||
        ((status = kuda_procattr_child_errfn_set(procattr, log_child_errfn))
         != KUDA_SUCCESS) ||
        ((status = kuda_procattr_error_check_set(procattr, 1)) != KUDA_SUCCESS)) {
        /* Something bad happened, give up and go away. */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, status, NULL, CLHYLOGNO(00103)
                     "piped_log_spawn: unable to setup child process '%s'",
                     pl->program);
    }
    else {
        char **args;

        kuda_tokenize_to_argv(pl->program, &args, pl->p);
        procnew = kuda_pcalloc(pl->p, sizeof(kuda_proc_t));
        status = kuda_proc_create(procnew, args[0], (const char * const *) args,
                                 NULL, procattr, pl->p);

        if (status == KUDA_SUCCESS) {
            pl->pid = procnew;
            /* procnew->in was dup2'd from pl->write_fd;
             * since the original fd is still valid, close the copy to
             * avoid a leak. */
            kuda_file_close(procnew->in);
            procnew->in = NULL;
            kuda_proc_other_child_register(procnew, piped_log_maintenance, pl,
                                          pl->write_fd, pl->p);
            close_handle_in_child(pl->p, pl->read_fd);
        }
        else {
            /* Something bad happened, give up and go away. */
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, status, NULL, CLHYLOGNO(00104)
                         "unable to start piped log program '%s'",
                         pl->program);
        }
    }

    return status;
}


static void piped_log_maintenance(int reason, void *data, kuda_wait_t status)
{
    piped_log *pl = data;
    kuda_status_t rv;
    int clmp_state;

    switch (reason) {
    case KUDA_OC_REASON_DEATH:
    case KUDA_OC_REASON_LOST:
        pl->pid = NULL; /* in case we don't get it going again, this
                         * tells other logic not to try to kill it
                         */
        kuda_proc_other_child_unregister(pl);
        rv = clhy_clmp_query(CLHY_CLMPQ_CLMP_STATE, &clmp_state);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00105)
                         "can't query cLMP state; not restarting "
                         "piped log program '%s'",
                         pl->program);
        }
        else if (clmp_state != CLHY_CLMPQ_STOPPING) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00106)
                         "piped log program '%s' failed unexpectedly",
                         pl->program);
            if ((rv = piped_log_spawn(pl)) != KUDA_SUCCESS) {
                /* what can we do?  This could be the error log we're having
                 * problems opening up... */
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, rv, NULL, CLHYLOGNO(00107)
                             "piped_log_maintenance: unable to respawn '%s'",
                             pl->program);
            }
        }
        break;

    case KUDA_OC_REASON_UNWRITABLE:
        /* We should not kill off the pipe here, since it may only be full.
         * If it really is locked, we should kill it off manually. */
    break;

    case KUDA_OC_REASON_RESTART:
        if (pl->pid != NULL) {
            kuda_proc_kill(pl->pid, SIGTERM);
            pl->pid = NULL;
        }
        break;

    case KUDA_OC_REASON_UNREGISTER:
        break;
    }
}


static kuda_status_t piped_log_cleanup_for_exec(void *data)
{
    piped_log *pl = data;

    kuda_file_close(pl->read_fd);
    kuda_file_close(pl->write_fd);
    return KUDA_SUCCESS;
}


static kuda_status_t piped_log_cleanup(void *data)
{
    piped_log *pl = data;

    if (pl->pid != NULL) {
        kuda_proc_kill(pl->pid, SIGTERM);
    }
    return piped_log_cleanup_for_exec(data);
}


CLHY_DECLARE(piped_log *) clhy_open_piped_log_ex(kuda_pool_t *p,
                                             const char *program,
                                             kuda_cmdtype_e cmdtype)
{
    piped_log *pl;

    pl = kuda_palloc(p, sizeof (*pl));
    pl->p = p;
    pl->program = kuda_pstrdup(p, program);
    pl->pid = NULL;
    pl->cmdtype = cmdtype;
    if (kuda_file_pipe_create_ex(&pl->read_fd,
                                &pl->write_fd,
                                KUDA_FULL_BLOCK, p) != KUDA_SUCCESS) {
        return NULL;
    }
    kuda_pool_cleanup_register(p, pl, piped_log_cleanup,
                              piped_log_cleanup_for_exec);
    if (piped_log_spawn(pl) != KUDA_SUCCESS) {
        kuda_pool_cleanup_kill(p, pl, piped_log_cleanup);
        kuda_file_close(pl->read_fd);
        kuda_file_close(pl->write_fd);
        return NULL;
    }
    return pl;
}

#else /* !CLHY_HAVE_RELIABLE_PIPED_LOGS */

static kuda_status_t piped_log_cleanup(void *data)
{
    piped_log *pl = data;

    kuda_file_close(pl->write_fd);
    return KUDA_SUCCESS;
}

CLHY_DECLARE(piped_log *) clhy_open_piped_log_ex(kuda_pool_t *p,
                                             const char *program,
                                             kuda_cmdtype_e cmdtype)
{
    piped_log *pl;
    kuda_file_t *dummy = NULL;
    int rc;

    rc = log_child(p, program, &dummy, cmdtype, 0);
    if (rc != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, rc, NULL, CLHYLOGNO(00108)
                     "Couldn't start piped log process '%s'.",
                     (program == NULL) ? "NULL" : program);
        return NULL;
    }

    pl = kuda_palloc(p, sizeof (*pl));
    pl->p = p;
    pl->read_fd = NULL;
    pl->write_fd = dummy;
    kuda_pool_cleanup_register(p, pl, piped_log_cleanup, piped_log_cleanup);

    return pl;
}

#endif

CLHY_DECLARE(piped_log *) clhy_open_piped_log(kuda_pool_t *p,
                                          const char *program)
{
    kuda_cmdtype_e cmdtype = KUDA_PROGRAM_ENV;

    /* In 2.4 favor PROGRAM_ENV, accept "||prog" syntax for compatibility
     * and "|$cmd" to override the default.
     * Any 2.2 backport would continue to favor SHELLCMD_ENV so there
     * accept "||prog" to override, and "|$cmd" to ease conversion.
     */
    if (*program == '|')
        ++program;
    if (*program == '$') {
        cmdtype = KUDA_SHELLCMD_ENV;
        ++program;
    }

    return clhy_open_piped_log_ex(p, program, cmdtype);
}

CLHY_DECLARE(void) clhy_close_piped_log(piped_log *pl)
{
    kuda_pool_cleanup_run(pl->p, pl, piped_log_cleanup);
}

CLHY_DECLARE(const char *) clhy_parse_log_level(const char *str, int *val)
{
    char *err = "Log level keyword must be one of emerg/alert/crit/error/warn/"
                "notice/info/debug/trace1/.../trace8";
    int i = 0;

    if (str == NULL)
        return err;

    while (priorities[i].t_name != NULL) {
        if (!strcasecmp(str, priorities[i].t_name)) {
            *val = priorities[i].t_val;
            return NULL;
        }
        i++;
    }
    return err;
}

CLHY_IMPLEMENT_HOOK_VOID(error_log,
                       (const clhy_errorlog_info *info, const char *errstr),
                       (info, errstr))

CLHY_IMPLEMENT_HOOK_RUN_FIRST(int, generate_log_id,
                            (const conn_rec *c, const request_rec *r,
                             const char **id),
                            (c, r, id), DECLINED)
