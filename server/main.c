/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_getopt.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "kuda_md5.h"
#include "kuda_time.h"
#include "kuda_version.h"
#include "kudelman_version.h"

#define KUDA_WANT_STDIO
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#include "clhy_config.h"
#include "wwhy.h"
#include "http_main.h"
#include "http_log.h"
#include "http_config.h"
#include "http_core.h"
#include "capi_core.h"
#include "http_request.h"
#include "http_vhost.h"
#include "kuda_uri.h"
#include "util_ebcdic.h"
#include "clhy_core.h"

#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif

/* WARNING: Win32 binds http_main.c dynamically to the server. Please place
 *          extern functions and global data in another appropriate cAPI.
 *
 * Most significant main() global data can be found in http_config.c
 */

static void show_clmp_settings(void)
{
    int clmp_query_info;
    kuda_status_t retval;

    printf("Server cLMP:     %s\n", clhy_show_clmp());

    retval = clhy_clmp_query(CLHY_CLMPQ_IS_THREADED, &clmp_query_info);

    if (retval == KUDA_SUCCESS) {
        printf("  threaded:     ");

        if (clmp_query_info == CLHY_CLMPQ_DYNAMIC) {
            printf("yes (variable thread count)\n");
        }
        else if (clmp_query_info == CLHY_CLMPQ_STATIC) {
            printf("yes (fixed thread count)\n");
        }
        else {
            printf("no\n");
        }
    }

    retval = clhy_clmp_query(CLHY_CLMPQ_IS_FORKED, &clmp_query_info);

    if (retval == KUDA_SUCCESS) {
        printf("    forked:     ");

        if (clmp_query_info == CLHY_CLMPQ_DYNAMIC) {
            printf("yes (variable process count)\n");
        }
        else if (clmp_query_info == CLHY_CLMPQ_STATIC) {
            printf("yes (fixed process count)\n");
        }
        else {
            printf("no\n");
        }
    }
}

static void show_compile_settings(void)
{
    printf("Server version: %s\n", clhy_get_server_description());
    printf("Server built:   %s\n", clhy_get_server_built());
    printf("Server's cAPI Magic Number: %u:%u\n",
           CAPI_MAGIC_NUMBER_MAJOR, CAPI_MAGIC_NUMBER_MINOR);
#if KUDA_MAJOR_VERSION >= 2
    printf("Server loaded:  KUDA %s\n", kuda_version_string());
    printf("Compiled using: KUDA %s\n", KUDA_VERSION_STRING);
#else
    printf("Server loaded:  KUDA %s, Kuda-Delman %s\n",
           kuda_version_string(), kudelman_version_string());
    printf("Compiled using: KUDA %s, Kuda-Delman %s\n",
           KUDA_VERSION_STRING, KUDELMAN_VERSION_STRING);
#endif
    /* sizeof(foo) is long on some platforms so we might as well
     * make it long everywhere to keep the printf format
     * consistent
     */
    printf("Architecture:   %ld-bit\n", 8 * (long)sizeof(void *));

    show_clmp_settings();

    printf("Server compiled with....\n");
#ifdef BIG_SECURITY_HOLE
    printf(" -D BIG_SECURITY_HOLE\n");
#endif

#ifdef SECURITY_HOLE_PASS_AUTHORIZATION
    printf(" -D SECURITY_HOLE_PASS_AUTHORIZATION\n");
#endif

#ifdef PLATFORM
    printf(" -D PLATFORM=\"" PLATFORM "\"\n");
#endif

#ifdef HAVE_SHMGET
    printf(" -D HAVE_SHMGET\n");
#endif

#if KUDA_FILE_BASED_SHM
    printf(" -D KUDA_FILE_BASED_SHM\n");
#endif

#if KUDA_HAS_SENDFILE
    printf(" -D KUDA_HAS_SENDFILE\n");
#endif

#if KUDA_HAS_MMAP
    printf(" -D KUDA_HAS_MMAP\n");
#endif

#ifdef NO_WRITEV
    printf(" -D NO_WRITEV\n");
#endif

#ifdef NO_LINGCLOSE
    printf(" -D NO_LINGCLOSE\n");
#endif

#if KUDA_HAVE_IPV6
    printf(" -D KUDA_HAVE_IPV6 (IPv4-mapped addresses ");
#ifdef CLHY_ENABLE_V4_MAPPED
    printf("enabled)\n");
#else
    printf("disabled)\n");
#endif
#endif

#if KUDA_USE_FLOCK_SERIALIZE
    printf(" -D KUDA_USE_FLOCK_SERIALIZE\n");
#endif

#if KUDA_USE_SYSVSEM_SERIALIZE
    printf(" -D KUDA_USE_SYSVSEM_SERIALIZE\n");
#endif

#if KUDA_USE_POSIXSEM_SERIALIZE
    printf(" -D KUDA_USE_POSIXSEM_SERIALIZE\n");
#endif

#if KUDA_USE_FCNTL_SERIALIZE
    printf(" -D KUDA_USE_FCNTL_SERIALIZE\n");
#endif

#if KUDA_USE_PROC_PTHREAD_SERIALIZE
    printf(" -D KUDA_USE_PROC_PTHREAD_SERIALIZE\n");
#endif

#if KUDA_USE_PTHREAD_SERIALIZE
    printf(" -D KUDA_USE_PTHREAD_SERIALIZE\n");
#endif

#if KUDA_PROCESS_LOCK_IS_GLOBAL
    printf(" -D KUDA_PROCESS_LOCK_IS_GLOBAL\n");
#endif

#ifdef SINGLE_LISTEN_UNSERIALIZED_ACCEPT
    printf(" -D SINGLE_LISTEN_UNSERIALIZED_ACCEPT\n");
#endif

#if KUDA_HAS_OTHER_CHILD
    printf(" -D KUDA_HAS_OTHER_CHILD\n");
#endif

#ifdef CLHY_HAVE_RELIABLE_PIPED_LOGS
    printf(" -D CLHY_HAVE_RELIABLE_PIPED_LOGS\n");
#endif

#ifdef BUFFERED_LOGS
    printf(" -D BUFFERED_LOGS\n");
#ifdef PIPE_BUF
    printf(" -D PIPE_BUF=%ld\n",(long)PIPE_BUF);
#endif
#endif

    printf(" -D DYNAMIC_CAPI_LIMIT=%ld\n",(long)DYNAMIC_CAPI_LIMIT);

#if KUDA_CHARSET_EBCDIC
    printf(" -D KUDA_CHARSET_EBCDIC\n");
#endif

#ifdef NEED_HASHBANG_EMUL
    printf(" -D NEED_HASHBANG_EMUL\n");
#endif

/* This list displays the compiled in default paths: */
#ifdef WWHY_ROOT
    printf(" -D WWHY_ROOT=\"" WWHY_ROOT "\"\n");
#endif

#ifdef SUEXEC_BIN
    printf(" -D SUEXEC_BIN=\"" SUEXEC_BIN "\"\n");
#endif

#ifdef DEFAULT_PIDLOG
    printf(" -D DEFAULT_PIDLOG=\"" DEFAULT_PIDLOG "\"\n");
#endif

#ifdef DEFAULT_SCOREBOARD
    printf(" -D DEFAULT_SCOREBOARD=\"" DEFAULT_SCOREBOARD "\"\n");
#endif

#ifdef DEFAULT_ERRORLOG
    printf(" -D DEFAULT_ERRORLOG=\"" DEFAULT_ERRORLOG "\"\n");
#endif

#ifdef CLHY_TYPES_CONFIG_FILE
    printf(" -D CLHY_TYPES_CONFIG_FILE=\"" CLHY_TYPES_CONFIG_FILE "\"\n");
#endif

#ifdef SERVER_CONFIG_FILE
    printf(" -D SERVER_CONFIG_FILE=\"" SERVER_CONFIG_FILE "\"\n");
#endif
}

#define TASK_SWITCH_SLEEP 10000

static void destroy_and_exit_process(process_rec *process,
                                     int process_exit_value)
{
    /*
     * Sleep for TASK_SWITCH_SLEEP micro seconds to cause a task switch on
     * PLATFORM layer and thus give possibly started piped loggers a chance to
     * process their input. Otherwise it is possible that they get killed
     * by us before they can do so. In this case maybe valueable log messages
     * might get lost.
     */
    kuda_sleep(TASK_SWITCH_SLEEP);
    clhy_main_state = CLHY_SQ_MS_EXITING;
    kuda_pool_destroy(process->pool); /* and destroy all descendent pools */
    kuda_terminate();
    exit(process_exit_value);
}

/* KUDA callback invoked if allocation fails. */
static int abort_on_oom(int retcode)
{
    clhy_abort_on_oom();
    return retcode; /* unreachable, hopefully. */
}

static process_rec *init_process(int *argc, const char * const * *argv)
{
    process_rec *process;
    kuda_pool_t *cntx;
    kuda_status_t stat;
    const char *failed = "kuda_app_initialize()";

    stat = kuda_app_initialize(argc, argv, NULL);
    if (stat == KUDA_SUCCESS) {
        failed = "kuda_pool_create()";
        stat = kuda_pool_create(&cntx, NULL);
    }

    if (stat != KUDA_SUCCESS) {
        /* For all intents and purposes, this is impossibly unlikely,
         * but KUDA doesn't exist yet, we can't use it for reporting
         * these earliest two failures;
         *
         * XXX: Note the kuda_ctime() and kuda_time_now() calls.  These
         * work, today, against an uninitialized KUDA, but in the future
         * (if they relied on global pools or mutexes, for example) then
         * the datestamp logic will need to be replaced.
         */
        char ctimebuff[KUDA_CTIME_LEN];
        kuda_ctime(ctimebuff, kuda_time_now());
        fprintf(stderr, "[%s] [crit] (%d) %s: %s failed "
                        "to initial context, exiting\n",
                        ctimebuff, stat, (*argv)[0], failed);
        kuda_terminate();
        exit(1);
    }

    kuda_pool_abort_set(abort_on_oom, cntx);
    kuda_pool_tag(cntx, "process");
    clhy_open_stderr_log(cntx);

    /* Now we have initialized kuda and our logger, no more
     * exceptional error reporting required for the lifetime
     * of this server process.
     */

    process = kuda_palloc(cntx, sizeof(process_rec));
    process->pool = cntx;

    kuda_pool_create(&process->pconf, process->pool);
    kuda_pool_tag(process->pconf, "pconf");
    process->argc = *argc;
    process->argv = *argv;
    process->short_name = kuda_filepath_name_get((*argv)[0]);
    return process;
}

static void usage(process_rec *process)
{
    const char *bin = process->argv[0];
    int pad_len = strlen(bin);

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "Usage: %s [-D name] [-d directory] [-f file]", bin);

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "       %*s [-C \"directive\"] [-c \"directive\"]", pad_len, " ");

#ifdef WIN32
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "       %*s [-w] [-k start|restart|stop|shutdown] [-n service_name]",
                 pad_len, " ");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "       %*s [-k install|config|uninstall] [-n service_name]",
                 pad_len, " ");
#else
/* XXX not all cLMPs support signalling the server in general or graceful-stop
 * in particular
 */
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "       %*s [-k start|restart|graceful|graceful-stop|stop]",
                 pad_len, " ");
#endif
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "       %*s [-v] [-V] [-h] [-l] [-L] [-t] [-T] [-S] [-X]",
                 pad_len, " ");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "Options:");

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -D name            : define a name for use in "
                 "<IfDefine name> directives");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -d directory       : specify an alternate initial "
                 "ServerRoot");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -f file            : specify an alternate ServerConfigFile");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -C \"directive\"     : process directive before reading "
                 "config files");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -c \"directive\"     : process directive after reading "
                 "config files");

#ifdef NETWARE
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -n name            : set screen name");
#endif
#ifdef WIN32
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -n name            : set service name and use its "
                 "ServerConfigFile and ServerRoot");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -k start           : tell cLHy to start");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -k restart         : tell running cLHy to do a graceful "
                 "restart");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -k stop|shutdown   : tell running cLHy to shutdown");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -k install         : install an cLHy service");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -k config          : change startup Options of an cLHy "
                 "service");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -k uninstall       : uninstall an cLHy service");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -w                 : hold open the console window on error");
#endif

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -e level           : show startup errors of level "
                 "(see LogLevel)");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -E file            : log startup errors to file");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -v                 : show version number");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -V                 : show compile settings");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -h                 : list available command line options "
                 "(this page)");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -l                 : list compiled in cAPIs");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -L                 : list available configuration "
                 "directives");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -t -D DUMP_VHOSTS  : show parsed vhost settings");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -t -D DUMP_RUN_CFG : show parsed run settings");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -S                 : a synonym for -t -D DUMP_VHOSTS -D DUMP_RUN_CFG");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -t -D DUMP_CAPIS   : show list of activated cAPIs ");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -M                 : a synonym for -t -D DUMP_CAPIS");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -t -D DUMP_INCLUDES: show all included configuration files");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -t                 : run syntax check for config files");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -T                 : start without DocumentRoot(s) check");
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL,
                 "  -X                 : debug mode (only one worker, do not detach)");

    destroy_and_exit_process(process, 1);
}

int main(int argc, const char * const argv[])
{
    char c;
    int showcompile = 0, showdirectives = 0;
    const char *confname = SERVER_CONFIG_FILE;
    const char *def_server_root = WWHY_ROOT;
    const char *temp_error_log = NULL;
    const char *error;
    process_rec *process;
    kuda_pool_t *pconf;
    kuda_pool_t *plog; /* Pool of log streams, reset _after_ each read of conf */
    kuda_pool_t *ptemp; /* Pool for temporary config stuff, reset often */
    kuda_pool_t *pcommands; /* Pool for -D, -C and -c switches */
    kuda_getopt_t *opt;
    kuda_status_t rv;
    cAPI **mod;
    const char *opt_arg;
    KUDA_OPTIONAL_FN_TYPE(clhy_signal_server) *signal_server;
    int rc = OK;

    CLHY_MONCONTROL(0); /* turn off profiling of startup */

    process = init_process(&argc, &argv);
    clhy_pglobal = process->pool;
    pconf = process->pconf;
    clhy_server_argv0 = process->short_name;
    clhy_init_rng(clhy_pglobal);

    /* Set up the OOM callback in the global pool, so all pools should
     * by default inherit it. */
    kuda_pool_abort_set(abort_on_oom, kuda_pool_parent_get(process->pool));

#if KUDA_CHARSET_EBCDIC
    if (clhy_init_ebcdic(clhy_pglobal) != KUDA_SUCCESS) {
        destroy_and_exit_process(process, 1);
    }
#endif

    kuda_pool_create(&pcommands, clhy_pglobal);
    kuda_pool_tag(pcommands, "pcommands");
    clhy_server_pre_read_config  = kuda_array_make(pcommands, 1, sizeof(char *));
    clhy_server_post_read_config = kuda_array_make(pcommands, 1, sizeof(char *));
    clhy_server_config_defines   = kuda_array_make(pcommands, 1, sizeof(char *));

    error = clhy_setup_prelinked_capis(process);
    if (error) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP|CLHYLOG_EMERG, 0, NULL, CLHYLOGNO(00012)
                     "%s: %s", clhy_server_argv0, error);
        destroy_and_exit_process(process, 1);
    }

    clhy_run_rewrite_args(process);

    /* Maintain CLHY_SERVER_BASEARGS list in http_main.h to allow the cLMP
     * to safely pass on our args from its rewrite_args() handler.
     */
    kuda_getopt_init(&opt, pcommands, process->argc, process->argv);

    while ((rv = kuda_getopt(opt, CLHY_SERVER_BASEARGS, &c, &opt_arg))
            == KUDA_SUCCESS) {
        char **new;

        switch (c) {
        case 'c':
            new = (char **)kuda_array_push(clhy_server_post_read_config);
            *new = kuda_pstrdup(pcommands, opt_arg);
            break;

        case 'C':
            new = (char **)kuda_array_push(clhy_server_pre_read_config);
            *new = kuda_pstrdup(pcommands, opt_arg);
            break;

        case 'd':
            def_server_root = opt_arg;
            break;

        case 'D':
            new = (char **)kuda_array_push(clhy_server_config_defines);
            *new = kuda_pstrdup(pcommands, opt_arg);
            /* Setting -D DUMP_VHOSTS should work like setting -S */
            if (strcmp(opt_arg, "DUMP_VHOSTS") == 0)
                clhy_run_mode = CLHY_SQ_RM_CONFIG_DUMP;
            /* Setting -D DUMP_RUN_CFG should work like setting -S */
            else if (strcmp(opt_arg, "DUMP_RUN_CFG") == 0)
                clhy_run_mode = CLHY_SQ_RM_CONFIG_DUMP;
            /* Setting -D DUMP_CAPIS is equivalent to setting -M */
            else if (strcmp(opt_arg, "DUMP_CAPIS") == 0)
                clhy_run_mode = CLHY_SQ_RM_CONFIG_DUMP;
            /* Setting -D DUMP_INCLUDES is a type of configuration dump */
            else if (strcmp(opt_arg, "DUMP_INCLUDES") == 0)
                clhy_run_mode = CLHY_SQ_RM_CONFIG_DUMP;
            break;

        case 'e':
            if (clhy_parse_log_level(opt_arg, &clhy_default_loglevel) != NULL)
                usage(process);
            break;

        case 'E':
            temp_error_log = kuda_pstrdup(process->pool, opt_arg);
            break;

        case 'X':
            new = (char **)kuda_array_push(clhy_server_config_defines);
            *new = "DEBUG";
            break;

        case 'f':
            confname = opt_arg;
            break;

        case 'v':
            printf("Server version: %s\n", clhy_get_server_description());
            printf("Server built:   %s\n", clhy_get_server_built());
            destroy_and_exit_process(process, 0);

        case 'l':
            clhy_show_capis();
            destroy_and_exit_process(process, 0);

        case 'L':
            clhy_run_mode = CLHY_SQ_RM_CONFIG_DUMP;
            showdirectives = 1;
            break;

        case 't':
            if (clhy_run_mode == CLHY_SQ_RM_UNKNOWN)
                clhy_run_mode = CLHY_SQ_RM_CONFIG_TEST;
            break;

       case 'T':
           clhy_document_root_check = 0;
           break;

        case 'S':
            clhy_run_mode = CLHY_SQ_RM_CONFIG_DUMP;
            new = (char **)kuda_array_push(clhy_server_config_defines);
            *new = "DUMP_VHOSTS";
            new = (char **)kuda_array_push(clhy_server_config_defines);
            *new = "DUMP_RUN_CFG";
            break;

        case 'M':
            clhy_run_mode = CLHY_SQ_RM_CONFIG_DUMP;
            new = (char **)kuda_array_push(clhy_server_config_defines);
            *new = "DUMP_CAPIS";
            break;

        case 'V':
            if (strcmp(clhy_show_clmp(), "")) {
                show_compile_settings();
                destroy_and_exit_process(process, 0);
            }
            else {
                showcompile = 1;
                clhy_run_mode = CLHY_SQ_RM_CONFIG_DUMP;
            }
            break;

        case 'h':
        case '?':
            usage(process);
        }
    }

    if (clhy_run_mode == CLHY_SQ_RM_UNKNOWN)
        clhy_run_mode = CLHY_SQ_RM_NORMAL;

    /* bad cmdline option?  then we die */
    if (rv != KUDA_EOF || opt->ind < opt->argc) {
        usage(process);
    }

    clhy_main_state = CLHY_SQ_MS_CREATE_PRE_CONFIG;
    kuda_pool_create(&plog, clhy_pglobal);
    kuda_pool_tag(plog, "plog");
    kuda_pool_create(&ptemp, pconf);
    kuda_pool_tag(ptemp, "ptemp");

    /* Note that we preflight the config file once
     * before reading it _again_ in the main loop.
     * This allows things, log files configuration
     * for example, to settle down.
     */

    clhy_server_root = def_server_root;
    if (temp_error_log) {
        clhy_replace_stderr_log(process->pool, temp_error_log);
    }
    clhy_server_conf = clhy_read_config(process, ptemp, confname, &clhy_conftree);
    if (!clhy_server_conf) {
        destroy_and_exit_process(process, 1);
    }
    kuda_pool_cleanup_register(pconf, &clhy_server_conf, clhy_pool_cleanup_set_null,
                              kuda_pool_cleanup_null);
    /* sort hooks here to make sure pre_config hooks are sorted properly */
    kuda_hook_sort_all();

    if (clhy_run_pre_config(pconf, plog, ptemp) != OK) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP |CLHYLOG_ERR, 0,
                     NULL, CLHYLOGNO(00013) "Pre-configuration failed");
        destroy_and_exit_process(process, 1);
    }

    rv = clhy_process_config_tree(clhy_server_conf, clhy_conftree,
                                process->pconf, ptemp);
    if (rv == OK) {
        clhy_fixup_virtual_hosts(pconf, clhy_server_conf);
        clhy_fini_vhost_config(pconf, clhy_server_conf);
        /*
         * Sort hooks again because clhy_process_config_tree may have add cAPIs
         * and hence hooks. This happens with capi_perl and cAPIs written in
         * perl.
         */
        kuda_hook_sort_all();

        if (clhy_run_check_config(pconf, plog, ptemp, clhy_server_conf) != OK) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP |CLHYLOG_ERR, 0,
                         NULL, CLHYLOGNO(00014) "Configuration check failed");
            destroy_and_exit_process(process, 1);
        }

        if (clhy_run_mode != CLHY_SQ_RM_NORMAL) {
            if (showcompile) {
                show_compile_settings();
            }
            else if (showdirectives) {
                clhy_show_directives();
                destroy_and_exit_process(process, 0);
            }
            else {
                clhy_run_test_config(pconf, clhy_server_conf);
                if (clhy_run_mode == CLHY_SQ_RM_CONFIG_TEST)
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, "Syntax OK");
            }
            destroy_and_exit_process(process, 0);
        }
    }

    /* If our config failed, deal with that here. */
    if (rv != OK) {
        destroy_and_exit_process(process, 1);
    }

    signal_server = KUDA_RETRIEVE_OPTIONAL_FN(clhy_signal_server);
    if (signal_server) {
        int exit_status;

        if (signal_server(&exit_status, pconf) != 0) {
            destroy_and_exit_process(process, exit_status);
        }
    }

    kuda_pool_clear(plog);

    if ( clhy_run_open_logs(pconf, plog, ptemp, clhy_server_conf) != OK) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP |CLHYLOG_ERR,
                     0, NULL, CLHYLOGNO(00015) "Unable to open logs");
        destroy_and_exit_process(process, 1);
    }

    if ( clhy_run_post_config(pconf, plog, ptemp, clhy_server_conf) != OK) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP |CLHYLOG_ERR, 0,
                     NULL, CLHYLOGNO(00016) "Configuration Failed");
        destroy_and_exit_process(process, 1);
    }

    kuda_pool_destroy(ptemp);

    do {
        clhy_main_state = CLHY_SQ_MS_DESTROY_CONFIG;
        kuda_hook_deregister_all();
        kuda_pool_clear(pconf);
        clhy_clear_auth_internal();

        clhy_main_state = CLHY_SQ_MS_CREATE_CONFIG;
        clhy_config_generation++;
        for (mod = clhy_prelinked_capis; *mod != NULL; mod++) {
            clhy_register_hooks(*mod, pconf);
        }

        /* This is a hack until we finish the code so that it only reads
         * the config file once and just operates on the tree already in
         * memory.  rbb
         */
        clhy_conftree = NULL;
        kuda_pool_create(&ptemp, pconf);
        kuda_pool_tag(ptemp, "ptemp");
        clhy_server_root = def_server_root;
        clhy_server_conf = clhy_read_config(process, ptemp, confname, &clhy_conftree);
        if (!clhy_server_conf) {
            destroy_and_exit_process(process, 1);
        }
        kuda_pool_cleanup_register(pconf, &clhy_server_conf,
                                  clhy_pool_cleanup_set_null, kuda_pool_cleanup_null);
        /* sort hooks here to make sure pre_config hooks are sorted properly */
        kuda_hook_sort_all();

        if (clhy_run_pre_config(pconf, plog, ptemp) != OK) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, 0, NULL,
                         CLHYLOGNO(00017) "Pre-configuration failed, exiting");
            destroy_and_exit_process(process, 1);
        }

        if (clhy_process_config_tree(clhy_server_conf, clhy_conftree, process->pconf,
                                   ptemp) != OK) {
            destroy_and_exit_process(process, 1);
        }
        clhy_fixup_virtual_hosts(pconf, clhy_server_conf);
        clhy_fini_vhost_config(pconf, clhy_server_conf);
        /*
         * Sort hooks again because clhy_process_config_tree may have add cAPIs
         * and hence hooks. This happens with capi_perl and cAPIs written in
         * perl.
         */
        kuda_hook_sort_all();

        if (clhy_run_check_config(pconf, plog, ptemp, clhy_server_conf) != OK) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, 0, NULL,
                         CLHYLOGNO(00018) "Configuration check failed, exiting");
            destroy_and_exit_process(process, 1);
        }

        kuda_pool_clear(plog);
        if (clhy_run_open_logs(pconf, plog, ptemp, clhy_server_conf) != OK) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, 0, NULL,
                         CLHYLOGNO(00019) "Unable to open logs, exiting");
            destroy_and_exit_process(process, 1);
        }

        if (clhy_run_post_config(pconf, plog, ptemp, clhy_server_conf) != OK) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, 0, NULL,
                         CLHYLOGNO(00020) "Configuration Failed, exiting");
            destroy_and_exit_process(process, 1);
        }

        kuda_pool_destroy(ptemp);
        kuda_pool_lock(pconf, 1);

        clhy_run_optional_fn_retrieve();

        clhy_main_state = CLHY_SQ_MS_RUN_CLMP;
        rc = clhy_run_clcore(pconf, plog, clhy_server_conf);

        kuda_pool_lock(pconf, 0);

    } while (rc == OK);

    if (rc == DONE) {
        rc = OK;
    }
    else if (rc != OK) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, 0, NULL, CLHYLOGNO(02818)
                     "cLMP run failed, exiting");
    }
    destroy_and_exit_process(process, rc);

    /* NOTREACHED */
    return !OK;
}

#ifdef CLHY_USING_AUTOCONF
/* This ugly little hack pulls any function referenced in exports.c into
 * the web server.  exports.c is generated during the build, and it
 * has all of the KUDA functions specified by the kuda/kuda.exports and
 * kuda-delman/kudadelman.exports files.
 */
const void *clhy_suck_in_KUDA(void);
const void *clhy_suck_in_KUDA(void)
{
    extern const void *clhy_ugly_hack;

    return clhy_ugly_hack;
}
#endif
