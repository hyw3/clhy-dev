/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_request.h"
#include "http_protocol.h"
#include "scoreboard.h"

static kuda_status_t eor_bucket_cleanup(void *data)
{
    kuda_bucket *b = (kuda_bucket *)data;
    request_rec *r = (request_rec *)b->data;

    if (r != NULL) {
        /*
         * If eor_bucket_destroy is called after us, this prevents
         * eor_bucket_destroy from trying to destroy the pool again.
         */
        b->data = NULL;
        /* Update child status and log the transaction */
        clhy_update_child_status(r->connection->sbh, SERVER_BUSY_LOG, r);
        clhy_run_log_transaction(r);
        if (clhy_extended_status) {
            clhy_increment_counts(r->connection->sbh, r);
        }
    }
    return KUDA_SUCCESS;
}

static kuda_status_t eor_bucket_read(kuda_bucket *b, const char **str,
                                    kuda_size_t *len, kuda_read_type_e block)
{
    *str = NULL;
    *len = 0;
    return KUDA_SUCCESS;
}

CLHY_DECLARE(kuda_bucket *) clhy_bucket_eor_make(kuda_bucket *b, request_rec *r)
{
    b->length      = 0;
    b->start       = 0;
    b->data        = r;
    b->type        = &clhy_bucket_type_eor;

    return b;
}

CLHY_DECLARE(kuda_bucket *) clhy_bucket_eor_create(kuda_bucket_alloc_t *list,
                                              request_rec *r)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    if (r) {
        /*
         * Register a cleanup for the request pool as the eor bucket could
         * have been allocated from a different pool then the request pool
         * e.g. the parent pool of the request pool. In this case
         * eor_bucket_destroy might be called at a point of time when the
         * request pool had been already destroyed.
         * We need to use a pre-cleanup here because a cAPI may create a
         * sub-pool which is still needed during the log_transaction hook.
         */
        kuda_pool_pre_cleanup_register(r->pool, (void *)b, eor_bucket_cleanup);
    }
    return clhy_bucket_eor_make(b, r);
}

static void eor_bucket_destroy(void *data)
{
    request_rec *r = (request_rec *)data;

    if (r) {
        /* eor_bucket_cleanup will be called when the pool gets destroyed */
        kuda_pool_destroy(r->pool);
    }
}

CLHY_DECLARE_DATA const kuda_bucket_type_t clhy_bucket_type_eor = {
    "EOR", 5, KUDA_BUCKET_METADATA,
    eor_bucket_destroy,
    eor_bucket_read,
    kuda_bucket_setaside_noop,
    kuda_bucket_split_notimpl,
    kuda_bucket_simple_copy
};

