/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "http_protocol.h"
#include "kuda_buckets.h"
#include "kuda_strings.h"
#if KUDA_HAVE_STRINGS_H
#include <strings.h>
#endif

static kuda_status_t error_bucket_read(kuda_bucket *b, const char **str,
                                      kuda_size_t *len, kuda_read_type_e block)
{
    *str = NULL;
    *len = 0;
    return KUDA_SUCCESS;
}

static void error_bucket_destroy(void *data)
{
    clhy_bucket_error *h = data;

    if (kuda_bucket_shared_destroy(h)) {
        kuda_bucket_free(h);
    }
}

CLHY_DECLARE(kuda_bucket *) clhy_bucket_error_make(kuda_bucket *b, int error,
                                              const char *buf, kuda_pool_t *p)
{
    clhy_bucket_error *h;

    h = kuda_bucket_alloc(sizeof(*h), b->list);
    h->status = error;
    h->data = kuda_pstrdup(p, buf);

    b = kuda_bucket_shared_make(b, h, 0, 0);
    b->type = &clhy_bucket_type_error;
    return b;
}

CLHY_DECLARE(kuda_bucket *) clhy_bucket_error_create(int error, const char *buf,
                                                kuda_pool_t *p,
                                                kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    if (!clhy_is_HTTP_VALID_RESPONSE(error)) {
        error = HTTP_INTERNAL_SERVER_ERROR;
    }
    return clhy_bucket_error_make(b, error, buf, p);
}

CLHY_DECLARE_DATA const kuda_bucket_type_t clhy_bucket_type_error = {
    "ERROR", 5, KUDA_BUCKET_METADATA,
    error_bucket_destroy,
    error_bucket_read,
    kuda_bucket_setaside_notimpl,
    kuda_bucket_split_notimpl,
    kuda_bucket_shared_copy
};
