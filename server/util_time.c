/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "util_time.h"


/* Number of characters needed to format the microsecond part of a timestamp.
 * Microseconds have 6 digits plus one separator character makes 7.
 *   */
#define CLHY_CTIME_USEC_LENGTH      7

/* Length of ISO 8601 date/time */
#define CLHY_CTIME_COMPACT_LEN      20


/* Cache for exploded values of recent timestamps
 */

struct exploded_time_cache_element {
    kuda_int64_t t;
    kuda_time_exp_t xt;
    kuda_int64_t t_validate; /* please see comments in cached_explode() */
};

/* the "+ 1" is for the current second: */
#define TIME_CACHE_SIZE (CLHY_TIME_RECENT_THRESHOLD + 1)

/* Note that CLHY_TIME_RECENT_THRESHOLD is defined to
 * be a power of two minus one in util_time.h, so that
 * we can replace a modulo operation with a bitwise AND
 * when hashing items into a cache of size
 * CLHY_TIME_RECENT_THRESHOLD+1
 */
#define TIME_CACHE_MASK (CLHY_TIME_RECENT_THRESHOLD)

static struct exploded_time_cache_element exploded_cache_localtime[TIME_CACHE_SIZE];
static struct exploded_time_cache_element exploded_cache_gmt[TIME_CACHE_SIZE];


static kuda_status_t cached_explode(kuda_time_exp_t *xt, kuda_time_t t,
                                   struct exploded_time_cache_element *cache,
                                   int use_gmt)
{
    kuda_int64_t seconds = kuda_time_sec(t);
    struct exploded_time_cache_element *cache_element =
        &(cache[seconds & TIME_CACHE_MASK]);
    struct exploded_time_cache_element cache_element_snapshot;

    /* The cache is implemented as a ring buffer.  Each second,
     * it uses a different element in the buffer.  The timestamp
     * in the element indicates whether the element contains the
     * exploded time for the current second (vs the time
     * 'now - CLHY_TIME_RECENT_THRESHOLD' seconds ago).  If the
     * cached value is for the current time, we use it.  Otherwise,
     * we compute the kuda_time_exp_t and store it in this
     * cache element. Note that the timestamp in the cache
     * element is updated only after the exploded time.  Thus
     * if two threads hit this cache element simultaneously
     * at the start of a new second, they'll both explode the
     * time and store it.  I.e., the writers will collide, but
     * they'll be writing the same value.
     */
    if (cache_element->t >= seconds) {
        /* There is an intentional race condition in this design:
         * in a multithreaded app, one thread might be reading
         * from this cache_element to resolve a timestamp from
         * TIME_CACHE_SIZE seconds ago at the same time that
         * another thread is copying the exploded form of the
         * current time into the same cache_element.  (I.e., the
         * first thread might hit this element of the ring buffer
         * just as the element is being recycled.)  This can
         * also happen at the start of a new second, if a
         * reader accesses the cache_element after a writer
         * has updated cache_element.t but before the writer
         * has finished updating the whole cache_element.
         *
         * Rather than trying to prevent this race condition
         * with locks, we allow it to happen and then detect
         * and correct it.  The detection works like this:
         *   Step 1: Take a "snapshot" of the cache element by
         *           copying it into a temporary buffer.
         *   Step 2: Check whether the snapshot contains consistent
         *           data: the timestamps at the start and end of
         *           the cache_element should both match the 'seconds'
         *           value that we computed from the input time.
         *           If these three don't match, then the snapshot
         *           shows the cache_element in the middle of an
         *           update, and its contents are invalid.
         *   Step 3: If the snapshot is valid, use it.  Otherwise,
         *           just give up on the cache and explode the
         *           input time.
         */
        memcpy(&cache_element_snapshot, cache_element,
               sizeof(struct exploded_time_cache_element));
        if ((seconds != cache_element_snapshot.t) ||
            (seconds != cache_element_snapshot.t_validate)) {
            /* Invalid snapshot */
            if (use_gmt) {
                return kuda_time_exp_gmt(xt, t);
            }
            else {
                return kuda_time_exp_lt(xt, t);
            }
        }
        else {
            /* Valid snapshot */
            memcpy(xt, &(cache_element_snapshot.xt),
                   sizeof(kuda_time_exp_t));
        }
    }
    else {
        kuda_status_t r;
        if (use_gmt) {
            r = kuda_time_exp_gmt(xt, t);
        }
        else {
            r = kuda_time_exp_lt(xt, t);
        }
        if (r != KUDA_SUCCESS) {
            return r;
        }
        cache_element->t = seconds;
        memcpy(&(cache_element->xt), xt, sizeof(kuda_time_exp_t));
        cache_element->t_validate = seconds;
    }
    xt->tm_usec = (int)kuda_time_usec(t);
    return KUDA_SUCCESS;
}


CLHY_DECLARE(kuda_status_t) clhy_explode_recent_localtime(kuda_time_exp_t * tm,
                                                     kuda_time_t t)
{
    return cached_explode(tm, t, exploded_cache_localtime, 0);
}

CLHY_DECLARE(kuda_status_t) clhy_explode_recent_gmt(kuda_time_exp_t * tm,
                                               kuda_time_t t)
{
    return cached_explode(tm, t, exploded_cache_gmt, 1);
}

CLHY_DECLARE(kuda_status_t) clhy_recent_ctime(char *date_str, kuda_time_t t)
{
    int len = KUDA_CTIME_LEN;
    return clhy_recent_ctime_ex(date_str, t, CLHY_CTIME_OPTION_NONE, &len);
}

CLHY_DECLARE(kuda_status_t) clhy_recent_ctime_ex(char *date_str, kuda_time_t t,
                                            int option, int *len)
{
    /* ### This code is a clone of kuda_ctime(), except that it
     * uses clhy_explode_recent_localtime() instead of kuda_time_exp_lt().
     */
    kuda_time_exp_t xt;
    const char *s;
    int real_year;
    int needed;


    /* Calculate the needed buffer length */
    if (option & CLHY_CTIME_OPTION_COMPACT)
        needed = CLHY_CTIME_COMPACT_LEN;
    else
        needed = KUDA_CTIME_LEN;

    if (option & CLHY_CTIME_OPTION_USEC) {
        needed += CLHY_CTIME_USEC_LENGTH;
    }

    /* Check the provided buffer length */
    if (len && *len >= needed) {
        *len = needed;
    }
    else {
        if (len != NULL) {
            *len = 0;
        }
        return KUDA_ENOMEM;
    }

    /* example without options: "Wed Jun 30 21:49:08 1993" */
    /*                           123456789012345678901234  */
    /* example for compact format: "1993-06-30 21:49:08" */
    /*                              1234567890123456789  */

    clhy_explode_recent_localtime(&xt, t);
    real_year = 1900 + xt.tm_year;
    if (option & CLHY_CTIME_OPTION_COMPACT) {
        int real_month = xt.tm_mon + 1;
        *date_str++ = real_year / 1000 + '0';
        *date_str++ = real_year % 1000 / 100 + '0';
        *date_str++ = real_year % 100 / 10 + '0';
        *date_str++ = real_year % 10 + '0';
        *date_str++ = '-';
        *date_str++ = real_month / 10 + '0';
        *date_str++ = real_month % 10 + '0';
        *date_str++ = '-';
    }
    else {
        s = &kuda_day_snames[xt.tm_wday][0];
        *date_str++ = *s++;
        *date_str++ = *s++;
        *date_str++ = *s++;
        *date_str++ = ' ';
        s = &kuda_month_snames[xt.tm_mon][0];
        *date_str++ = *s++;
        *date_str++ = *s++;
        *date_str++ = *s++;
        *date_str++ = ' ';
    }
    *date_str++ = xt.tm_mday / 10 + '0';
    *date_str++ = xt.tm_mday % 10 + '0';
    *date_str++ = ' ';
    *date_str++ = xt.tm_hour / 10 + '0';
    *date_str++ = xt.tm_hour % 10 + '0';
    *date_str++ = ':';
    *date_str++ = xt.tm_min / 10 + '0';
    *date_str++ = xt.tm_min % 10 + '0';
    *date_str++ = ':';
    *date_str++ = xt.tm_sec / 10 + '0';
    *date_str++ = xt.tm_sec % 10 + '0';
    if (option & CLHY_CTIME_OPTION_USEC) {
        int div;
        int usec = (int)xt.tm_usec;
        *date_str++ = '.';
        for (div=100000; div>0; div=div/10) {
            *date_str++ = usec / div + '0';
            usec = usec % div;
        }
    }
    if (!(option & CLHY_CTIME_OPTION_COMPACT)) {
        *date_str++ = ' ';
        *date_str++ = real_year / 1000 + '0';
        *date_str++ = real_year % 1000 / 100 + '0';
        *date_str++ = real_year % 100 / 10 + '0';
        *date_str++ = real_year % 10 + '0';
    }
    *date_str++ = 0;

    return KUDA_SUCCESS;
}

CLHY_DECLARE(kuda_status_t) clhy_recent_rfc822_date(char *date_str, kuda_time_t t)
{
    /* ### This code is a clone of kuda_rfc822_date(), except that it
     * uses clhy_explode_recent_gmt() instead of kuda_time_exp_gmt().
     */
    kuda_time_exp_t xt;
    const char *s;
    int real_year;

    clhy_explode_recent_gmt(&xt, t);

    /* example: "Sat, 08 Jan 2000 18:31:41 GMT" */
    /*           12345678901234567890123456789  */

    s = &kuda_day_snames[xt.tm_wday][0];
    *date_str++ = *s++;
    *date_str++ = *s++;
    *date_str++ = *s++;
    *date_str++ = ',';
    *date_str++ = ' ';
    *date_str++ = xt.tm_mday / 10 + '0';
    *date_str++ = xt.tm_mday % 10 + '0';
    *date_str++ = ' ';
    s = &kuda_month_snames[xt.tm_mon][0];
    *date_str++ = *s++;
    *date_str++ = *s++;
    *date_str++ = *s++;
    *date_str++ = ' ';
    real_year = 1900 + xt.tm_year;
    /* This routine isn't y10k ready. */
    *date_str++ = real_year / 1000 + '0';
    *date_str++ = real_year % 1000 / 100 + '0';
    *date_str++ = real_year % 100 / 10 + '0';
    *date_str++ = real_year % 10 + '0';
    *date_str++ = ' ';
    *date_str++ = xt.tm_hour / 10 + '0';
    *date_str++ = xt.tm_hour % 10 + '0';
    *date_str++ = ':';
    *date_str++ = xt.tm_min / 10 + '0';
    *date_str++ = xt.tm_min % 10 + '0';
    *date_str++ = ':';
    *date_str++ = xt.tm_sec / 10 + '0';
    *date_str++ = xt.tm_sec % 10 + '0';
    *date_str++ = ' ';
    *date_str++ = 'G';
    *date_str++ = 'M';
    *date_str++ = 'T';
    *date_str++ = 0;
    return KUDA_SUCCESS;
}
