/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* The purpose of this file is to store the code that MOST cLMP's will need
 * this does not mean a function only goes into this file if every cLMP needs
 * it.  It means that if a function is needed by more than one cLMP, and
 * future maintenance would be served by making the code common, then the
 * function belongs here.
 *
 * This is going in src/main because it is not platform specific, it is
 * specific to multi-process servers, but NOT to Unix.  Which is why it
 * does not belong in src/platforms/unix
 */

#include "kuda.h"
#include "kuda_thread_proc.h"
#include "kuda_signal.h"
#include "kuda_strings.h"
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"
#include "kuda_getopt.h"
#include "kuda_optional.h"
#include "kuda_allocator.h"

#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_main.h"
#include "core_common.h"
#include "capi_core.h"
#include "clhy_core.h"
#include "clhy_listen.h"
#include "util_mutex.h"

#include "scoreboard.h"

#ifdef HAVE_PWD_H
#include <pwd.h>
#endif
#ifdef HAVE_GRP_H
#include <grp.h>
#endif
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif

/* we know core's capi_index is 0 */
#undef CLHYLOG_CAPI_INDEX
#define CLHYLOG_CAPI_INDEX CLHY_CORE_CAPI_INDEX

#define DEFAULT_HOOK_LINKS \
    KUDA_HOOK_LINK(monitor) \
    KUDA_HOOK_LINK(drop_privileges) \
    KUDA_HOOK_LINK(clcore) \
    KUDA_HOOK_LINK(clmp_query) \
    KUDA_HOOK_LINK(clmp_register_timed_callback) \
    KUDA_HOOK_LINK(clmp_get_name) \
    KUDA_HOOK_LINK(end_generation) \
    KUDA_HOOK_LINK(child_status) \
    KUDA_HOOK_LINK(suspend_connection) \
    KUDA_HOOK_LINK(resume_connection)

#if CLHY_ENABLE_EXCEPTION_HOOK
KUDA_HOOK_STRUCT(
    KUDA_HOOK_LINK(fatal_exception)
    DEFAULT_HOOK_LINKS
)
CLHY_IMPLEMENT_HOOK_RUN_ALL(int, fatal_exception,
                          (clhy_exception_info_t *ei), (ei), OK, DECLINED)
#else
KUDA_HOOK_STRUCT(
    DEFAULT_HOOK_LINKS
)
#endif
CLHY_IMPLEMENT_HOOK_RUN_ALL(int, monitor,
                          (kuda_pool_t *p, server_rec *s), (p, s), OK, DECLINED)
CLHY_IMPLEMENT_HOOK_RUN_ALL(int, drop_privileges,
                          (kuda_pool_t * pchild, server_rec * s),
                          (pchild, s), OK, DECLINED)
CLHY_IMPLEMENT_HOOK_RUN_FIRST(int, clcore,
                            (kuda_pool_t *pconf, kuda_pool_t *plog, server_rec *s),
                            (pconf, plog, s), DECLINED)
CLHY_IMPLEMENT_HOOK_RUN_FIRST(int, clmp_query,
                            (int query_code, int *result, kuda_status_t *_rv),
                            (query_code, result, _rv), DECLINED)
CLHY_IMPLEMENT_HOOK_RUN_FIRST(kuda_status_t, clmp_register_timed_callback,
                            (kuda_time_t t, clhy_clmp_callback_fn_t *cbfn, void *baton),
                            (t, cbfn, baton), KUDA_ENOTIMPL)
CLHY_IMPLEMENT_HOOK_VOID(end_generation,
                       (server_rec *s, clhy_generation_t gen),
                       (s, gen))
CLHY_IMPLEMENT_HOOK_VOID(child_status,
                       (server_rec *s, pid_t pid, clhy_generation_t gen, int slot, clmp_child_status status),
                       (s,pid,gen,slot,status))
CLHY_IMPLEMENT_HOOK_VOID(suspend_connection,
                       (conn_rec *c, request_rec *r),
                       (c, r))
CLHY_IMPLEMENT_HOOK_VOID(resume_connection,
                       (conn_rec *c, request_rec *r),
                       (c, r))

/* hooks with no args are implemented last, after disabling KUDA hook probes */
#if defined(KUDA_HOOK_PROBES_ENABLED)
#undef KUDA_HOOK_PROBES_ENABLED
#undef KUDA_HOOK_PROBE_ENTRY
#define KUDA_HOOK_PROBE_ENTRY(ud,ns,name,args)
#undef KUDA_HOOK_PROBE_RETURN
#define KUDA_HOOK_PROBE_RETURN(ud,ns,name,rv,args)
#undef KUDA_HOOK_PROBE_INVOKE
#define KUDA_HOOK_PROBE_INVOKE(ud,ns,name,src,args)
#undef KUDA_HOOK_PROBE_COMPLETE
#define KUDA_HOOK_PROBE_COMPLETE(ud,ns,name,src,rv,args)
#undef KUDA_HOOK_INT_DCL_UD
#define KUDA_HOOK_INT_DCL_UD
#endif
CLHY_IMPLEMENT_HOOK_RUN_FIRST(const char *, clmp_get_name,
                            (void),
                            (), NULL)

typedef struct clmp_gen_info_t {
    KUDA_RING_ENTRY(clmp_gen_info_t) link;
    int gen;          /* which gen? */
    int active;       /* number of active processes */
    int done;         /* gen finished? (whether or not active processes) */
} clmp_gen_info_t;

KUDA_RING_HEAD(clmp_gen_info_head_t, clmp_gen_info_t);
static struct clmp_gen_info_head_t *geninfo, *unused_geninfo;
static int gen_head_init; /* yuck */

/* variables representing config directives implemented here */
CLHY_DECLARE_DATA const char *clhy_pid_fname;
CLHY_DECLARE_DATA int clhy_max_requests_per_child;
CLHY_DECLARE_DATA char clhy_coredump_dir[MAX_STRING_LEN];
CLHY_DECLARE_DATA int clhy_coredumpdir_configured;
CLHY_DECLARE_DATA int clhy_graceful_shutdown_timeout;
CLHY_DECLARE_DATA kuda_uint32_t clhy_max_mem_free;
CLHY_DECLARE_DATA kuda_size_t clhy_thread_stacksize;

#define ALLOCATOR_MAX_FREE_DEFAULT (2048*1024)

/* Set defaults for config directives implemented here.  This is
 * called from core's pre-config hook, so cLMPs which need to override
 * one of these should run their pre-config hook after that of core.
 */
void core_common_pre_config(kuda_pool_t *pconf)
{
    clhy_pid_fname = DEFAULT_PIDLOG;
    clhy_max_requests_per_child = 0; /* unlimited */
    kuda_cpystrn(clhy_coredump_dir, clhy_server_root, sizeof(clhy_coredump_dir));
    clhy_coredumpdir_configured = 0;
    clhy_graceful_shutdown_timeout = 0; /* unlimited */
    clhy_max_mem_free = ALLOCATOR_MAX_FREE_DEFAULT;
    clhy_thread_stacksize = 0; /* use system default */
}

/* number of calls to wait_or_timeout between writable probes */
#ifndef INTERVAL_OF_WRITABLE_PROBES
#define INTERVAL_OF_WRITABLE_PROBES 10
#endif
static int wait_or_timeout_counter;

CLHY_DECLARE(void) clhy_wait_or_timeout(kuda_exit_why_e *status, int *exitcode,
                                    kuda_proc_t *ret, kuda_pool_t *p,
                                    server_rec *s)
{
    kuda_status_t rv;

    ++wait_or_timeout_counter;
    if (wait_or_timeout_counter == INTERVAL_OF_WRITABLE_PROBES) {
        wait_or_timeout_counter = 0;
        clhy_run_monitor(p, s);
    }

    rv = kuda_proc_wait_all_procs(ret, exitcode, status, KUDA_NOWAIT, p);
    if (KUDA_STATUS_IS_EINTR(rv)) {
        ret->pid = -1;
        return;
    }

    if (KUDA_STATUS_IS_CHILD_DONE(rv)) {
        return;
    }

    kuda_sleep(kuda_time_from_sec(1));
    ret->pid = -1;
}

#if defined(TCP_NODELAY)
void clhy_sock_disable_nagle(kuda_socket_t *s)
{
    /* The Nagle algorithm says that we should delay sending partial
     * packets in hopes of getting more data.  We don't want to do
     * this; we are not telnet.  There are bad interactions between
     * persistent connections and Nagle's algorithm that have very severe
     * performance penalties.  (Failing to disable Nagle is not much of a
     * problem with simple HTTP.)
     *
     * In spite of these problems, failure here is not a shooting offense.
     */
    kuda_status_t status = kuda_socket_opt_set(s, KUDA_TCP_NODELAY, 1);

    if (status != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, status, clhy_server_conf, CLHYLOGNO(00542)
                     "kuda_socket_opt_set: (TCP_NODELAY)");
    }
}
#endif

#ifdef HAVE_GETPWNAM
CLHY_DECLARE(uid_t) clhy_uname2id(const char *name)
{
    struct passwd *ent;

    if (name[0] == '#')
        return (atoi(&name[1]));

    if (!(ent = getpwnam(name))) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00543)
                     "%s: bad user name %s", clhy_server_argv0, name);
        exit(1);
    }

    return (ent->pw_uid);
}
#endif

#ifdef HAVE_GETGRNAM
CLHY_DECLARE(gid_t) clhy_gname2id(const char *name)
{
    struct group *ent;

    if (name[0] == '#')
        return (atoi(&name[1]));

    if (!(ent = getgrnam(name))) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00544)
                     "%s: bad group name %s", clhy_server_argv0, name);
        exit(1);
    }

    return (ent->gr_gid);
}
#endif

#ifndef HAVE_INITGROUPS
int initgroups(const char *name, gid_t basegid)
{
#if defined(_OSD_POSIX) || defined(OS2) || defined(WIN32) || defined(NETWARE)
    return 0;
#else
    gid_t groups[NGROUPS_MAX];
    struct group *g;
    int index = 0;

    setgrent();

    groups[index++] = basegid;

    while (index < NGROUPS_MAX && ((g = getgrent()) != NULL)) {
        if (g->gr_gid != basegid) {
            char **names;

            for (names = g->gr_mem; *names != NULL; ++names) {
                if (!strcmp(*names, name))
                    groups[index++] = g->gr_gid;
            }
        }
    }

    endgrent();

    return setgroups(index, groups);
#endif
}
#endif /* def HAVE_INITGROUPS */

/* standard cLMP configuration handling */

const char *clhy_clmp_set_pidfile(cmd_parms *cmd, void *dummy,
                               const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    if (cmd->server->is_virtual) {
        return "PidFile directive not allowed in <VirtualHost>";
    }

    clhy_pid_fname = arg;
    return NULL;
}

void clhy_clmp_dump_pidfile(kuda_pool_t *p, kuda_file_t *out)
{
    kuda_file_printf(out, "PidFile: \"%s\"\n",
                    clhy_server_root_relative(p, clhy_pid_fname));
}

const char *clhy_clmp_set_max_requests(cmd_parms *cmd, void *dummy,
                                    const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    if (!strcasecmp(cmd->cmd->name, "MaxRequestsPerChild")) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, NULL, CLHYLOGNO(00545)
                     "MaxRequestsPerChild is deprecated, use "
                     "MaxConnectionsPerChild instead.");
    }

    clhy_max_requests_per_child = atoi(arg);

    return NULL;
}

const char *clhy_clmp_set_coredumpdir(cmd_parms *cmd, void *dummy,
                                   const char *arg)
{
    kuda_finfo_t finfo;
    const char *fname;
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    fname = clhy_server_root_relative(cmd->temp_pool, arg);
    if (!fname) {
        return kuda_pstrcat(cmd->pool, "Invalid CoreDumpDirectory path ",
                           arg, NULL);
    }
    if (kuda_stat(&finfo, fname, KUDA_FINFO_TYPE, cmd->pool) != KUDA_SUCCESS) {
        return kuda_pstrcat(cmd->pool, "CoreDumpDirectory ", fname,
                           " does not exist", NULL);
    }
    if (finfo.filetype != KUDA_DIR) {
        return kuda_pstrcat(cmd->pool, "CoreDumpDirectory ", fname,
                           " is not a directory", NULL);
    }
    kuda_cpystrn(clhy_coredump_dir, fname, sizeof(clhy_coredump_dir));
    clhy_coredumpdir_configured = 1;
    return NULL;
}

CLHY_DECLARE(const char *)clhy_clmp_set_graceful_shutdown(cmd_parms *cmd,
                                                     void *dummy,
                                                     const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }
    clhy_graceful_shutdown_timeout = atoi(arg);
    return NULL;
}

const char *clhy_clmp_set_max_mem_free(cmd_parms *cmd, void *dummy,
                                    const char *arg)
{
    long value;
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    errno = 0;
    value = strtol(arg, NULL, 10);
    if (value < 0 || errno == ERANGE)
        return kuda_pstrcat(cmd->pool, "Invalid MaxMemFree value: ",
                           arg, NULL);

    clhy_max_mem_free = (kuda_uint32_t)value * 1024;

    return NULL;
}

const char *clhy_clmp_set_thread_stacksize(cmd_parms *cmd, void *dummy,
                                        const char *arg)
{
    long value;
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    errno = 0;
    value = strtol(arg, NULL, 10);
    if (value < 0 || errno == ERANGE)
        return kuda_pstrcat(cmd->pool, "Invalid ThreadStackSize value: ",
                           arg, NULL);

    clhy_thread_stacksize = (kuda_size_t)value;

    return NULL;
}

CLHY_DECLARE(kuda_status_t) clhy_clmp_query(int query_code, int *result)
{
    kuda_status_t rv;

    if (clhy_run_clmp_query(query_code, result, &rv) == DECLINED) {
        rv = KUDA_EGENERAL;
    }

    return rv;
}

static void end_gen(clmp_gen_info_t *gi)
{
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE4, 0, clhy_server_conf,
                 "end of generation %d", gi->gen);
    clhy_run_end_generation(clhy_server_conf, gi->gen);
    KUDA_RING_REMOVE(gi, link);
    KUDA_RING_INSERT_HEAD(unused_geninfo, gi, clmp_gen_info_t, link);
}

kuda_status_t clhy_clmp_end_gen_helper(void *unused) /* cleanup on pconf */
{
    int gen = clhy_config_generation - 1; /* differs from cLMP generation */
    clmp_gen_info_t *cur;

    if (geninfo == NULL) {
        /* initial pconf teardown, cLMP hasn't run */
        return KUDA_SUCCESS;
    }

    cur = KUDA_RING_FIRST(geninfo);
    while (cur != KUDA_RING_SENTINEL(geninfo, clmp_gen_info_t, link) &&
           cur->gen != gen) {
        cur = KUDA_RING_NEXT(cur, link);
    }

    if (cur == KUDA_RING_SENTINEL(geninfo, clmp_gen_info_t, link)) {
        /* last child of generation already exited */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE4, 0, clhy_server_conf,
                     "no record of generation %d", gen);
    }
    else {
        cur->done = 1;
        if (cur->active == 0) {
            end_gen(cur);
        }
    }

    return KUDA_SUCCESS;
}

/* core's child-status hook
 * tracks number of remaining children per generation and
 * runs the end-generation hook when the last child of
 * a generation exits
 */
void clhy_core_child_status(server_rec *s, pid_t pid,
                          clhy_generation_t gen, int slot,
                          clmp_child_status status)
{
    clmp_gen_info_t *cur;
    const char *status_msg = "unknown status";

    if (!gen_head_init) { /* where to run this? */
        gen_head_init = 1;
        geninfo = kuda_pcalloc(s->process->pool, sizeof *geninfo);
        unused_geninfo = kuda_pcalloc(s->process->pool, sizeof *unused_geninfo);
        KUDA_RING_INIT(geninfo, clmp_gen_info_t, link);
        KUDA_RING_INIT(unused_geninfo, clmp_gen_info_t, link);
    }

    cur = KUDA_RING_FIRST(geninfo);
    while (cur != KUDA_RING_SENTINEL(geninfo, clmp_gen_info_t, link) &&
           cur->gen != gen) {
        cur = KUDA_RING_NEXT(cur, link);
    }

    switch(status) {
    case CLMP_CHILD_STARTED:
        status_msg = "started";
        if (cur == KUDA_RING_SENTINEL(geninfo, clmp_gen_info_t, link)) {
            /* first child for this generation */
            if (!KUDA_RING_EMPTY(unused_geninfo, clmp_gen_info_t, link)) {
                cur = KUDA_RING_FIRST(unused_geninfo);
                KUDA_RING_REMOVE(cur, link);
                cur->active = cur->done = 0;
            }
            else {
                cur = kuda_pcalloc(s->process->pool, sizeof *cur);
            }
            cur->gen = gen;
            KUDA_RING_ELEM_INIT(cur, link);
            KUDA_RING_INSERT_HEAD(geninfo, cur, clmp_gen_info_t, link);
        }
        clhy_random_parent_after_fork();
        ++cur->active;
        break;
    case CLMP_CHILD_EXITED:
        status_msg = "exited";
        if (cur == KUDA_RING_SENTINEL(geninfo, clmp_gen_info_t, link)) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00546)
                         "no record of generation %d of exiting child %" KUDA_PID_T_FMT,
                         gen, pid);
        }
        else {
            --cur->active;
            if (!cur->active && cur->done) { /* no children, server has stopped/restarted */
                end_gen(cur);
            }
        }
        break;
    case CLMP_CHILD_LOST_SLOT:
        status_msg = "lost slot";
        /* we don't track by slot, so it doesn't matter */
        break;
    }
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE4, 0, s,
                 "clcore child %" KUDA_PID_T_FMT " (gen %d/slot %d) %s",
                 pid, gen, slot, status_msg);
}

CLHY_DECLARE(kuda_status_t) clhy_clmp_register_timed_callback(kuda_time_t t, clhy_clmp_callback_fn_t *cbfn, void *baton)
{
    return clhy_run_clmp_register_timed_callback(t, cbfn, baton);
}

CLHY_DECLARE(const char *)clhy_show_clmp(void)
{
    const char *name = clhy_run_clmp_get_name();

    if (!name) {
        name = "";
    }

    return name;
}

CLHY_DECLARE(const char *)clhy_check_clcore(void)
{
    static const char *last_clmp_name = NULL;

    if (!_hooks.link_clcore || _hooks.link_clcore->nelts == 0)
        return "No cLHy core API (cLMP) loaded.";
    else if (_hooks.link_clcore->nelts > 1)
        return "More than one cLHy core API (cLMP) loaded.";

    if (last_clmp_name) {
        if (strcmp(last_clmp_name, clhy_show_clmp())) {
            return "The cLHy core API (cLMP) cannot be changed during restart.";
        }
    }
    else {
        last_clmp_name = kuda_pstrdup(clhy_pglobal, clhy_show_clmp());
    }

    return NULL;
}
