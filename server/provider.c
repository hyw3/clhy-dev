/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_pools.h"
#include "kuda_hash.h"
#include "kuda_tables.h"
#include "kuda_strings.h"

#include "clhy_provider.h"

static kuda_hash_t *global_providers = NULL;
static kuda_hash_t *global_providers_names = NULL;


static kuda_status_t cleanup_global_providers(void *ctx)
{
    global_providers = NULL;
    global_providers_names = NULL;
    return KUDA_SUCCESS;
}

CLHY_DECLARE(kuda_status_t) clhy_register_provider(kuda_pool_t *pool,
                                              const char *provider_group,
                                              const char *provider_name,
                                              const char *provider_version,
                                              const void *provider)
{
    kuda_hash_t *provider_group_hash, *provider_version_hash;

    if (global_providers == NULL) {
        global_providers = kuda_hash_make(pool);
        global_providers_names = kuda_hash_make(pool);
        kuda_pool_cleanup_register(pool, NULL, cleanup_global_providers,
                                  kuda_pool_cleanup_null);
    }

    /* First, deal with storing the provider away */
    provider_group_hash = kuda_hash_get(global_providers, provider_group,
                                       KUDA_HASH_KEY_STRING);

    if (!provider_group_hash) {
        provider_group_hash = kuda_hash_make(pool);
        kuda_hash_set(global_providers, provider_group, KUDA_HASH_KEY_STRING,
                     provider_group_hash);

    }

    provider_version_hash = kuda_hash_get(provider_group_hash, provider_name,
                                         KUDA_HASH_KEY_STRING);

    if (!provider_version_hash) {
        provider_version_hash = kuda_hash_make(pool);
        kuda_hash_set(provider_group_hash, provider_name, KUDA_HASH_KEY_STRING,
                     provider_version_hash);

    }

    /* just set it. no biggy if it was there before. */
    kuda_hash_set(provider_version_hash, provider_version, KUDA_HASH_KEY_STRING,
                 provider);

    /* Now, tuck away the provider names in an easy-to-get format */
    provider_group_hash = kuda_hash_get(global_providers_names, provider_group,
                                       KUDA_HASH_KEY_STRING);

    if (!provider_group_hash) {
        provider_group_hash = kuda_hash_make(pool);
        kuda_hash_set(global_providers_names, provider_group, KUDA_HASH_KEY_STRING,
                     provider_group_hash);

    }

    provider_version_hash = kuda_hash_get(provider_group_hash, provider_version,
                                         KUDA_HASH_KEY_STRING);

    if (!provider_version_hash) {
        provider_version_hash = kuda_hash_make(pool);
        kuda_hash_set(provider_group_hash, provider_version, KUDA_HASH_KEY_STRING,
                     provider_version_hash);

    }

    /* just set it. no biggy if it was there before. */
    kuda_hash_set(provider_version_hash, provider_name, KUDA_HASH_KEY_STRING,
                 provider_name);

    return KUDA_SUCCESS;
}

CLHY_DECLARE(void *) clhy_lookup_provider(const char *provider_group,
                                      const char *provider_name,
                                      const char *provider_version)
{
    kuda_hash_t *provider_group_hash, *provider_name_hash;

    if (global_providers == NULL) {
        return NULL;
    }

    provider_group_hash = kuda_hash_get(global_providers, provider_group,
                                       KUDA_HASH_KEY_STRING);

    if (provider_group_hash == NULL) {
        return NULL;
    }

    provider_name_hash = kuda_hash_get(provider_group_hash, provider_name,
                                      KUDA_HASH_KEY_STRING);

    if (provider_name_hash == NULL) {
        return NULL;
    }

    return kuda_hash_get(provider_name_hash, provider_version,
                        KUDA_HASH_KEY_STRING);
}

CLHY_DECLARE(kuda_array_header_t *) clhy_list_provider_names(kuda_pool_t *pool,
                                              const char *provider_group,
                                              const char *provider_version)
{
    kuda_array_header_t *ret = kuda_array_make(pool, 10, sizeof(clhy_list_provider_names_t));
    clhy_list_provider_names_t *entry;
    kuda_hash_t *provider_group_hash, *h;
    kuda_hash_index_t *hi;
    char *val;

    if (global_providers_names == NULL) {
        return ret;
    }

    provider_group_hash = kuda_hash_get(global_providers_names, provider_group,
                                       KUDA_HASH_KEY_STRING);

    if (provider_group_hash == NULL) {
        return ret;
    }

    h = kuda_hash_get(provider_group_hash, provider_version,
                                      KUDA_HASH_KEY_STRING);

    if (h == NULL) {
        return ret;
    }

    for (hi = kuda_hash_first(pool, h); hi; hi = kuda_hash_next(hi)) {
        kuda_hash_this(hi, NULL, NULL, (void *)&val);
        entry = kuda_array_push(ret);
        entry->provider_name = kuda_pstrdup(pool, val);
    }
    return ret;
}

CLHY_DECLARE(kuda_array_header_t *) clhy_list_provider_groups(kuda_pool_t *pool)
{
    kuda_array_header_t *ret = kuda_array_make(pool, 10, sizeof(clhy_list_provider_groups_t));
    clhy_list_provider_groups_t *entry;
    kuda_hash_t *provider_group_hash;
    kuda_hash_index_t *groups_hi, *vers_hi;
    char *group, *version;

    if (global_providers_names == NULL)
        return ret;

    for (groups_hi = kuda_hash_first(pool, global_providers_names);
         groups_hi;
         groups_hi = kuda_hash_next(groups_hi))
    {
        kuda_hash_this(groups_hi, (void *)&group, NULL, (void *)&provider_group_hash);
        if (provider_group_hash == NULL)
            continue;
        for (vers_hi = kuda_hash_first(pool, provider_group_hash);
             vers_hi;
             vers_hi = kuda_hash_next(vers_hi))
        {
            kuda_hash_this(vers_hi, (void *)&version, NULL, NULL);

            entry = kuda_array_push(ret);
            entry->provider_group = group;
            entry->provider_version = version;
        }
    }
    return ret;
}
