/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * http_config.c: once was auxiliary functions for reading wwhy's config
 * file and converting filenames into a namespace
 *
 * Rob McCool
 *
 * Wall-to-wall rewrite for cLHy... commands which are part of the
 * server core can now be found next door in "http_core.c".  Now contains
 * general command loop, and functions which do bookkeeping for the new
 * cLHy config stuff (cAPIs and configuration vectors).
 *
 * rst
 *
 */

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_file_io.h"
#include "kuda_fnmatch.h"

#define KUDA_WANT_STDIO
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_protocol.h"
#include "http_core.h"
#include "http_log.h"      /* for errors in parse_htaccess */
#include "http_request.h"  /* for default_handler (see invoke_handler) */
#include "http_main.h"
#include "http_vhost.h"
#include "util_cfgtree.h"
#include "util_varbuf.h"
#include "core_common.h"

#define CLHYLOG_UNSET   (CLHYLOG_NO_CAPI - 1)
/* we know core's capi_index is 0 */
#undef CLHYLOG_CAPI_INDEX
#define CLHYLOG_CAPI_INDEX CLHY_CORE_CAPI_INDEX

CLHY_DECLARE_DATA const char *clhy_server_argv0 = NULL;
CLHY_DECLARE_DATA const char *clhy_server_root = NULL;
CLHY_DECLARE_DATA const char *clhy_runtime_dir = NULL;
CLHY_DECLARE_DATA server_rec *clhy_server_conf = NULL;
CLHY_DECLARE_DATA kuda_pool_t *clhy_pglobal = NULL;

CLHY_DECLARE_DATA kuda_array_header_t *clhy_server_pre_read_config = NULL;
CLHY_DECLARE_DATA kuda_array_header_t *clhy_server_post_read_config = NULL;
CLHY_DECLARE_DATA kuda_array_header_t *clhy_server_config_defines = NULL;

CLHY_DECLARE_DATA clhy_directive_t *clhy_conftree = NULL;

KUDA_HOOK_STRUCT(
           KUDA_HOOK_LINK(header_parser)
           KUDA_HOOK_LINK(pre_config)
           KUDA_HOOK_LINK(check_config)
           KUDA_HOOK_LINK(post_config)
           KUDA_HOOK_LINK(open_logs)
           KUDA_HOOK_LINK(child_init)
           KUDA_HOOK_LINK(handler)
           KUDA_HOOK_LINK(quick_handler)
           KUDA_HOOK_LINK(optional_fn_retrieve)
           KUDA_HOOK_LINK(test_config)
           KUDA_HOOK_LINK(open_htaccess)
)

CLHY_IMPLEMENT_HOOK_RUN_ALL(int, header_parser,
                          (request_rec *r), (r), OK, DECLINED)

CLHY_IMPLEMENT_HOOK_RUN_ALL(int, pre_config,
                          (kuda_pool_t *pconf, kuda_pool_t *plog,
                           kuda_pool_t *ptemp),
                          (pconf, plog, ptemp), OK, DECLINED)

CLHY_IMPLEMENT_HOOK_RUN_ALL(int, check_config,
                          (kuda_pool_t *pconf, kuda_pool_t *plog,
                           kuda_pool_t *ptemp, server_rec *s),
                          (pconf, plog, ptemp, s), OK, DECLINED)

CLHY_IMPLEMENT_HOOK_VOID(test_config,
                       (kuda_pool_t *pconf, server_rec *s),
                       (pconf, s))

CLHY_IMPLEMENT_HOOK_RUN_ALL(int, post_config,
                          (kuda_pool_t *pconf, kuda_pool_t *plog,
                           kuda_pool_t *ptemp, server_rec *s),
                          (pconf, plog, ptemp, s), OK, DECLINED)

/* During the course of debugging I expanded this macro out, so
 * rather than remove all the useful information there is in the
 * following lines, I'm going to leave it here in case anyone
 * else finds it useful.
 *
 * Ben has looked at it and thinks it correct :)
 *
CLHY_DECLARE(int) clhy_hook_post_config(clhy_HOOK_post_config_t *pf,
                                    const char * const *aszPre,
                                    const char * const *aszSucc,
                                    int nOrder)
{
    clhy_LINK_post_config_t *pHook;

    if (!_hooks.link_post_config) {
        _hooks.link_post_config = kuda_array_make(kuda_hook_global_pool, 1,
                                                 sizeof(clhy_LINK_post_config_t));
        kuda_hook_sort_register("post_config", &_hooks.link_post_config);
    }

    pHook = kuda_array_push(_hooks.link_post_config);
    pHook->pFunc = pf;
    pHook->aszPredecessors = aszPre;
    pHook->aszSuccessors = aszSucc;
    pHook->nOrder = nOrder;
    pHook->szName = kuda_hook_debug_current;

    if (kuda_hook_debug_enabled)
        kuda_hook_debug_show("post_config", aszPre, aszSucc);
}

CLHY_DECLARE(kuda_array_header_t *) clhy_hook_get_post_config(void)
{
    return _hooks.link_post_config;
}

CLHY_DECLARE(int) clhy_run_post_config(kuda_pool_t *pconf,
                                   kuda_pool_t *plog,
                                   kuda_pool_t *ptemp,
                                   server_rec *s)
{
    clhy_LINK_post_config_t *pHook;
    int n;

    if (!_hooks.link_post_config)
        return;

    pHook = (clhy_LINK_post_config_t *)_hooks.link_post_config->elts;
    for (n = 0; n < _hooks.link_post_config->nelts; ++n)
        pHook[n].pFunc (pconf, plog, ptemp, s);
}
 */

CLHY_IMPLEMENT_HOOK_RUN_ALL(int, open_logs,
                          (kuda_pool_t *pconf, kuda_pool_t *plog,
                           kuda_pool_t *ptemp, server_rec *s),
                          (pconf, plog, ptemp, s), OK, DECLINED)

CLHY_IMPLEMENT_HOOK_VOID(child_init,
                       (kuda_pool_t *pchild, server_rec *s),
                       (pchild, s))

CLHY_IMPLEMENT_HOOK_RUN_FIRST(int, handler, (request_rec *r),
                            (r), DECLINED)

CLHY_IMPLEMENT_HOOK_RUN_FIRST(int, quick_handler, (request_rec *r, int lookup),
                            (r, lookup), DECLINED)

CLHY_IMPLEMENT_HOOK_RUN_FIRST(kuda_status_t, open_htaccess,
                            (request_rec *r, const char *dir_name, const char *access_name,
                             clhy_configfile_t **conffile, const char **full_name),
                            (r, dir_name, access_name, conffile, full_name),
                            CLHY_DECLINED)

/* hooks with no args are implemented last, after disabling KUDA hook probes */
#if defined(KUDA_HOOK_PROBES_ENABLED)
#undef KUDA_HOOK_PROBES_ENABLED
#undef KUDA_HOOK_PROBE_ENTRY
#define KUDA_HOOK_PROBE_ENTRY(ud,ns,name,args)
#undef KUDA_HOOK_PROBE_RETURN
#define KUDA_HOOK_PROBE_RETURN(ud,ns,name,rv,args)
#undef KUDA_HOOK_PROBE_INVOKE
#define KUDA_HOOK_PROBE_INVOKE(ud,ns,name,src,args)
#undef KUDA_HOOK_PROBE_COMPLETE
#define KUDA_HOOK_PROBE_COMPLETE(ud,ns,name,src,rv,args)
#undef KUDA_HOOK_INT_DCL_UD
#define KUDA_HOOK_INT_DCL_UD
#endif
CLHY_IMPLEMENT_HOOK_VOID(optional_fn_retrieve, (void), ())

/****************************************************************
 *
 * We begin with the functions which deal with the linked list
 * of cAPIs which control just about all of the server operation.
 */

/* total_capis is the number of cAPIs that have been linked
 * into the server.
 */
static int total_capis = 0;

/* dynamic_capis is the number of cAPIs that have been added
 * after the pre-loaded ones have been set up. It shouldn't be larger
 * than DYNAMIC_CAPI_LIMIT.
 */
static int dynamic_capis = 0;

/* The maximum possible value for total_capis, i.e. number of static
 * cAPIs plus DYNAMIC_CAPI_LIMIT.
 */
static int max_capis = 0;

/* The number of elements we need to alloc for config vectors. Before loading
 * of dynamic cAPIs, we must be liberal and set this to max_capis. After
 * loading of dynamic cAPIs, we can trim it down to total_capis. On
 * restart, reset to max_capis.
 */
static int conf_vector_length = 0;

static int reserved_capi_slots = 0;

CLHY_DECLARE_DATA cAPI *clhy_top_capi = NULL;
CLHY_DECLARE_DATA cAPI **clhy_activated_capis=NULL;

static kuda_hash_t *clhy_config_hash = NULL;

/* a list of the cAPI symbol names with the trailing "_capi"removed */
static char **clhy_capi_short_names = NULL;

typedef int (*handler_func)(request_rec *);
typedef void *(*dir_maker_func)(kuda_pool_t *, char *);
typedef void *(*merger_func)(kuda_pool_t *, void *, void *);

/* A list of the merge_dir_config functions of all loaded cAPIs, sorted
 * by capi_index.
 * Using this list in clhy_merge_per_dir_configs() is faster than following
 * the cAPI->next linked list because of better memory locality (resulting
 * in better cache usage).
 */
static merger_func *merger_func_cache;

/* maximum nesting level for config directories */
#ifndef CLHY_MAX_INCLUDE_DIR_DEPTH
#define CLHY_MAX_INCLUDE_DIR_DEPTH (128)
#endif

/* Dealing with config vectors.  These are associated with per-directory,
 * per-server, and per-request configuration, and have a void* pointer for
 * each cAPIs.  The nature of the structure pointed to is private to the
 * cAPI in question... the core doesn't (and can't) know.  However, there
 * are defined interfaces which allow it to create instances of its private
 * per-directory and per-server structures, and to merge the per-directory
 * structures of a directory and its subdirectory (producing a new one in
 * which the defaults applying to the base directory have been properly
 * overridden).
 */

static clhy_conf_vector_t *create_empty_config(kuda_pool_t *p)
{
    void *conf_vector = kuda_pcalloc(p, sizeof(void *) * conf_vector_length);
    return conf_vector;
}

static clhy_conf_vector_t *create_default_per_dir_config(kuda_pool_t *p)
{
    void **conf_vector = kuda_pcalloc(p, sizeof(void *) * conf_vector_length);
    cAPI *capip;

    for (capip = clhy_top_capi; capip; capip = capip->next) {
        dir_maker_func df = capip->create_dir_config;

        if (df)
            conf_vector[capip->capi_index] = (*df)(p, NULL);
    }

    return (clhy_conf_vector_t *)conf_vector;
}

CLHY_CORE_DECLARE(clhy_conf_vector_t *) clhy_merge_per_dir_configs(kuda_pool_t *p,
                                           clhy_conf_vector_t *base,
                                           clhy_conf_vector_t *new_conf)
{
    void **conf_vector = kuda_palloc(p, sizeof(void *) * conf_vector_length);
    void **base_vector = (void **)base;
    void **new_vector = (void **)new_conf;
    int i;

    for (i = 0; i < total_capis; i++) {
        if (!new_vector[i]) {
            conf_vector[i] = base_vector[i];
        }
        else {
            const merger_func df = merger_func_cache[i];
            if (df && base_vector[i]) {
                conf_vector[i] = (*df)(p, base_vector[i], new_vector[i]);
            }
            else
                conf_vector[i] = new_vector[i];
        }
    }

    return (clhy_conf_vector_t *)conf_vector;
}

static clhy_conf_vector_t *create_server_config(kuda_pool_t *p, server_rec *s)
{
    void **conf_vector = kuda_pcalloc(p, sizeof(void *) * conf_vector_length);
    cAPI *capip;

    for (capip = clhy_top_capi; capip; capip = capip->next) {
        if (capip->create_server_config)
            conf_vector[capip->capi_index] = (*capip->create_server_config)(p, s);
    }

    return (clhy_conf_vector_t *)conf_vector;
}

static void merge_server_configs(kuda_pool_t *p, clhy_conf_vector_t *base,
                                 server_rec *virt)
{
    /* Can reuse the 'virt' vector for the spine of it, since we don't
     * have to deal with the moral equivalent of .htaccess files here...
     */

    void **base_vector = (void **)base;
    void **virt_vector = (void **)virt->capi_config;
    cAPI *capip;

    for (capip = clhy_top_capi; capip; capip = capip->next) {
        merger_func df = capip->merge_server_config;
        int i = capip->capi_index;

        if (!virt_vector[i]) {
            if (df && capip->create_server_config
                   && (clhy_get_capi_flags(capip) &
                       CLHY_CAPI_FLAG_ALWAYS_MERGE)) {
                virt_vector[i] = (*capip->create_server_config)(p, virt);
            }
            else {
                virt_vector[i] = base_vector[i];
                df = NULL;
            }
        }
        if (df) {
            virt_vector[i] = (*df)(p, base_vector[i], virt_vector[i]);
        }
    }
}

CLHY_CORE_DECLARE(clhy_conf_vector_t *) clhy_create_request_config(kuda_pool_t *p)
{
    return create_empty_config(p);
}

CLHY_CORE_DECLARE(clhy_conf_vector_t *) clhy_create_conn_config(kuda_pool_t *p)
{
    return create_empty_config(p);
}

CLHY_CORE_DECLARE(clhy_conf_vector_t *) clhy_create_per_dir_config(kuda_pool_t *p)
{
    return create_empty_config(p);
}

/* Invoke the filter_init_func for all filters with FILTERS where f->r
 * matches R.  Restricting to a matching R avoids re-running init
 * functions for filters configured for r->main where r is a
 * subrequest.  */
static int invoke_filter_init(request_rec *r, clhy_filter_t *filters)
{
    while (filters) {
        if (filters->frec->filter_init_func && filters->r == r) {
            int result = filters->frec->filter_init_func(filters);
            if (result != OK) {
                return result;
            }
        }
        filters = filters->next;
    }
    return OK;
}

CLHY_CORE_DECLARE(int) clhy_invoke_handler(request_rec *r)
{
    const char *handler;
    const char *p;
    int result;
    const char *old_handler = r->handler;
    const char *ignore;

    /*
     * The new insert_filter stage makes the most sense here.  We only use
     * it when we are going to run the request, so we must insert filters
     * if any are available.  Since the goal of this phase is to allow all
     * cAPIs to insert a filter if they want to, this filter returns
     * void.  I just can't see any way that this filter can reasonably
     * fail, either your cAPIs inserts something or it doesn't.  rbb
     */
    clhy_run_insert_filter(r);

    /* Before continuing, allow each filter that is in the two chains to
     * run their init function to let them do any magic before we could
     * start generating data.
     */
    result = invoke_filter_init(r, r->input_filters);
    if (result != OK) {
        return result;
    }
    result = invoke_filter_init(r, r->output_filters);
    if (result != OK) {
        return result;
    }

    if (!r->handler) {
        if (r->content_type) {
            handler = r->content_type;
            if ((p=clhy_strchr_c(handler, ';')) != NULL) {
                char *new_handler = (char *)kuda_pmemdup(r->pool, handler,
                                                        p - handler + 1);
                char *p2 = new_handler + (p - handler);
                handler = new_handler;

                /* exclude media type arguments */
                while (p2 > handler && p2[-1] == ' ')
                    --p2; /* strip trailing spaces */

                *p2='\0';
            }
        }
        else {
            handler = CLHY_DEFAULT_HANDLER_NAME;
        }

        r->handler = handler;
    }

    result = clhy_run_handler(r);

    r->handler = old_handler;

    if (result == DECLINED && r->handler && r->filename) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(00523)
            "handler \"%s\" not found for: %s", r->handler, r->filename);
    }
    if ((result != OK) && (result != DONE) && (result != DECLINED) && (result != SUSPENDED)
        && (result != CLHY_FILTER_ERROR) /* clhy_die() knows about this specifically */
        && !clhy_is_HTTP_VALID_RESPONSE(result)) {
        /* If a cAPI is deliberately returning something else
         * (request_rec in non-HTTP or proprietary extension?)
         * let it set a note to allow it explicitly.
         * Otherwise, a return code that is neither reserved nor HTTP
         * is a bug, as in PR#31759.
         */
        ignore = kuda_table_get(r->notes, "HTTP_IGNORE_RANGE");
        if (!ignore) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00524)
                          "Handler for %s returned invalid result code %d",
                          r->handler, result);
            result = HTTP_INTERNAL_SERVER_ERROR;
        }
    }

    return result == DECLINED ? HTTP_INTERNAL_SERVER_ERROR : result;
}

CLHY_DECLARE(int) clhy_method_is_limited(cmd_parms *cmd, const char *method)
{
    int methnum;

    methnum = clhy_method_number_of(method);

    /*
     * A method number either hardcoded into clhy or
     * added by a cAPI and registered.
     */
    if (methnum != M_INVALID) {
        return (cmd->limited & (CLHY_METHOD_BIT << methnum)) ? 1 : 0;
    }

    return 0; /* not found */
}

CLHY_DECLARE(void) clhy_register_hooks(cAPI *m, kuda_pool_t *p)
{
    if (m->register_hooks) {
        if (getenv("SHOW_HOOKS")) {
            printf("Registering hooks for %s\n", m->name);
            kuda_hook_debug_enabled = 1;
        }

        kuda_hook_debug_current = m->name;
        m->register_hooks(p);
    }
}

static void clhy_add_capi_commands(cAPI *m, kuda_pool_t *p);

typedef struct clhy_capi_list_struct clhy_capi_list;
struct clhy_capi_list_struct {
    struct clhy_capi_list_struct *next;
    cAPI *m;
    const command_rec *cmd;
};

static void rebuild_conf_hash(kuda_pool_t *p, int add_prelinked)
{
    cAPI **m;

    clhy_config_hash = kuda_hash_make(p);

    kuda_pool_cleanup_register(p, &clhy_config_hash, clhy_pool_cleanup_set_null,
                              kuda_pool_cleanup_null);
    if (add_prelinked) {
        for (m = clhy_prelinked_capis; *m != NULL; m++) {
            clhy_add_capi_commands(*m, p);
        }
    }
}

static void clhy_add_capi_commands(cAPI *m, kuda_pool_t *p)
{
    kuda_pool_t *tpool;
    clhy_capi_list *mln;
    const command_rec *cmd;
    char *dir;

    cmd = m->cmds;

    if (clhy_config_hash == NULL) {
        rebuild_conf_hash(p, 0);
    }

    tpool = kuda_hash_pool_get(clhy_config_hash);

    while (cmd && cmd->name) {
        mln = kuda_palloc(tpool, sizeof(clhy_capi_list));
        mln->cmd = cmd;
        mln->m = m;
        dir = kuda_pstrdup(tpool, cmd->name);

        clhy_str_tolower(dir);

        mln->next = kuda_hash_get(clhy_config_hash, dir, KUDA_HASH_KEY_STRING);
        kuda_hash_set(clhy_config_hash, dir, KUDA_HASH_KEY_STRING, mln);
        ++cmd;
    }
}


/* One-time setup for precompiled cAPIs --- NOT to be done on restart */

CLHY_DECLARE(const char *) clhy_add_capi(cAPI *m, kuda_pool_t *p,
                                       const char *sym_name)
{
    clhy_capi_symbol_t *sym = clhy_prelinked_capi_symbols;

    /* This could be called from a ActivatecAPI wwhy.conf command,
     * after the file has been linked and the cAPI structure within it
     * teased out...
     */

    if (m->version != CAPI_MAGIC_NUMBER_MAJOR) {
        return kuda_psprintf(p, "cAPI \"%s\" is not compatible with this "
                            "version of cLHy (found %d, need %d). Please "
                            "contact the vendor for the correct version.",
                            m->name, m->version, CAPI_MAGIC_NUMBER_MAJOR);
    }

    if (m->capi_index == -1) {
        if (dynamic_capis >= DYNAMIC_CAPI_LIMIT) {
            return kuda_psprintf(p, "cAPI \"%s\" could not be loaded, "
                                "because the dynamic cAPI limit was "
                                "reached. Please increase "
                                "DYNAMIC_CAPI_LIMIT and recompile.", m->name);
        }
        /*
         * If this fails some cAPI forgot to call clhy_reserve_capi_slots*.
         */
        clhy_assert(total_capis < conf_vector_length);

        m->capi_index = total_capis++;
        dynamic_capis++;

    }
    else if (!sym_name) {
        while (sym->capip != NULL) {
            if (sym->capip == m) {
                sym_name = sym->name;
                break;
            }
            sym++;
        }
    }

    if (m->next == NULL) {
        m->next = clhy_top_capi;
        clhy_top_capi = m;
    }

    if (sym_name) {
        int len = strlen(sym_name);
        int slen = strlen("_capi");
        if (len > slen && !strcmp(sym_name + len - slen, "_capi")) {
            len -= slen;
        }

        clhy_capi_short_names[m->capi_index] = clhy_malloc(len + 1);
        memcpy(clhy_capi_short_names[m->capi_index], sym_name, len);
        clhy_capi_short_names[m->capi_index][len] = '\0';
        merger_func_cache[m->capi_index] = m->merge_dir_config;
    }


    /* Some C compilers put a complete path into __FILE__, but we want
     * only the filename (e.g. capi_includes.c). So check for path
     * components (Unix and DOS), and remove them.
     */

    if (clhy_strrchr_c(m->name, '/'))
        m->name = 1 + clhy_strrchr_c(m->name, '/');

    if (clhy_strrchr_c(m->name, '\\'))
        m->name = 1 + clhy_strrchr_c(m->name, '\\');

#ifdef _OSD_POSIX




    /* We cannot fix the string in-place, because it's const */
    if (m->name[strlen(m->name)-1] == ')') {
        char *tmp = clhy_malloc(strlen(m->name)); /* FIXME: memory leak, albeit a small one */
        memcpy(tmp, m->name, strlen(m->name)-1);
        tmp[strlen(m->name)-1] = '\0';
        m->name = tmp;
    }
#endif /*_OSD_POSIX*/

    clhy_add_capi_commands(m, p);
    /*  FIXME: is this the right place to call this?
     *  It doesn't appear to be
     */
    clhy_register_hooks(m, p);

    return NULL;
}

/*
 * remove_capi undoes what add_capi did. There are some caveats:
 * when the cAPI is removed, its slot is lost so all the current
 * per-dir and per-server configurations are invalid. So we should
 * only ever call this function when you are invalidating almost
 * all our current data. I.e. when doing a restart.
 */

CLHY_DECLARE(void) clhy_remove_capi(cAPI *m)
{
    cAPI *capip;

    capip = clhy_top_capi;
    if (capip == m) {
        /* We are the top cAPI, special case */
        clhy_top_capi = capip->next;
        m->next = NULL;
    }
    else {
        /* Not the top cAPI, find use. When found capip will
         * point to the cAPI _before_ us in the list
         */

        while (capip && capip->next != m) {
            capip = capip->next;
        }

        if (!capip) {
            /* Uh-oh, this cAPI doesn't exist */
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, NULL, CLHYLOGNO(00525)
                         "Cannot remove cAPI %s: not found in cAPI list",
                         m->name);
            return;
        }

        /* Eliminate us from the cAPI list */
        capip->next = capip->next->next;
    }

    free(clhy_capi_short_names[m->capi_index]);
    clhy_capi_short_names[m->capi_index] = NULL;
    merger_func_cache[m->capi_index] = NULL;

    m->capi_index = -1; /* simulate being unloaded, should
                           * be unnecessary */
    dynamic_capis--;
    total_capis--;
}

CLHY_DECLARE(const char *) clhy_add_activated_capi(cAPI *mod, kuda_pool_t *p,
                                              const char *short_name)
{
    cAPI **m;
    const char *error;

    /*
     *  Add cAPI pointer to top of chained cAPI list
     */
    error = clhy_add_capi(mod, p, short_name);
    if (error) {
        return error;
    }

    /*
     *  And cAPI pointer to list of loaded cAPIs
     *
     *  Notes: 1. clhy_add_capi() would already complain if no more space
     *            exists for adding a dynamically loaded cAPI
     *         2. clhy_add_capi() accepts double inclusion, so we have
     *            to accept this, too.
     */
    for (m = clhy_activated_capis; *m != NULL; m++)
        ;
    *m++ = mod;
    *m = NULL;

    return NULL;
}

CLHY_DECLARE(void) clhy_remove_activated_capi(cAPI *mod)
{
    cAPI **m;
    cAPI **m2;
    int done;

    /*
     *  Remove cAPI pointer from chained cAPI list
     */
    clhy_remove_capi(mod);

    /*
     *  Remove cAPI pointer from list of loaded cAPIs
     *
     *  Note: 1. We cannot determine if the cAPI was successfully
     *           removed by clhy_remove_capi().
     *        2. We have not to complain explicitly when the cAPI
     *           is not found because clhy_remove_capi() did it
     *           for us already.
     */
    for (m = m2 = clhy_activated_capis, done = 0; *m2 != NULL; m2++) {
        if (*m2 == mod && done == 0)
            done = 1;
        else
            *m++ = *m2;
    }

    *m = NULL;
}

CLHY_DECLARE(const char *) clhy_setup_prelinked_capis(process_rec *process)
{
    cAPI **m;
    cAPI **m2;
    const char *error;

    kuda_hook_global_pool=process->pconf;

    rebuild_conf_hash(process->pconf, 0);

    /*
     *  Initialise total_capis variable and cAPI indices
     */
    total_capis = 0;
    for (m = clhy_preactivated_capis; *m != NULL; m++)
        (*m)->capi_index = total_capis++;

    max_capis = total_capis + DYNAMIC_CAPI_LIMIT + 1;
    conf_vector_length = max_capis;

    /*
     *  Initialise list of loaded cAPIs and short names
     */
    clhy_activated_capis = (cAPI **)kuda_palloc(process->pool,
        sizeof(cAPI *) * conf_vector_length);
    if (!clhy_capi_short_names)
        clhy_capi_short_names = clhy_calloc(sizeof(char *), conf_vector_length);

    if (!merger_func_cache)
        merger_func_cache = clhy_calloc(sizeof(merger_func), conf_vector_length);

    if (clhy_activated_capis == NULL || clhy_capi_short_names == NULL
        || merger_func_cache == NULL)
        return "Ouch! Out of memory in clhy_setup_prelinked_capis()!";

    for (m = clhy_preactivated_capis, m2 = clhy_activated_capis; *m != NULL; )
        *m2++ = *m++;

    *m2 = NULL;

    /*
     *   Initialize chain of linked (=activate) cAPIs
     */
    for (m = clhy_prelinked_capis; *m != NULL; m++) {
        error = clhy_add_capi(*m, process->pconf, NULL);
        if (error) {
            return error;
        }
    }

    kuda_hook_sort_all();

    return NULL;
}

CLHY_DECLARE(const char *) clhy_find_capi_name(cAPI *m)
{
    return m->name;
}

CLHY_DECLARE(const char *) clhy_find_capi_short_name(int capi_index)
{
        if (capi_index < 0 || capi_index >= conf_vector_length)
                return NULL;
        return clhy_capi_short_names[capi_index];
}

CLHY_DECLARE(cAPI *) clhy_find_linked_capi(const char *name)
{
    cAPI *capip;

    for (capip = clhy_top_capi; capip; capip = capip->next) {
        if (strcmp(capip->name, name) == 0)
            return capip;
    }

    return NULL;
}

/*****************************************************************
 *
 * Resource, access, and .htaccess config files now parsed by a common
 * command loop.
 *
 * Let's begin with the basics; parsing the line and
 * invoking the function...
 */

#define CLHY_MAX_ARGC 64

static const char *invoke_cmd(const command_rec *cmd, cmd_parms *parms,
                              void *mconfig, const char *args)
{
    int override_list_ok = 0;
    char *w, *w2, *w3;
    const char *errmsg = NULL;

    /* Have we been provided a list of acceptable directives? */
    if (parms->override_list != NULL) { 
         if (kuda_table_get(parms->override_list, cmd->name) != NULL) { 
              override_list_ok = 1;
         }
    }

    if ((parms->override & cmd->req_override) == 0 && !override_list_ok) {
        if (parms->override & NONFATAL_OVERRIDE) {
            clhy_log_perror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, parms->temp_pool,
                          CLHYLOGNO(02295)
                          "%s in .htaccess forbidden by AllowOverride",
                          cmd->name);
            return NULL;
        }
        else if (parms->directive && parms->directive->parent) {
            return kuda_pstrcat(parms->pool, cmd->name, " not allowed in ",
                               parms->directive->parent->directive, ">",
                               " context", NULL);
        }
        else {
            return kuda_pstrcat(parms->pool, cmd->name,
                               " not allowed here", NULL);
        }
    }

    parms->info = cmd->cmd_data;
    parms->cmd = cmd;

    switch (cmd->args_how) {
    case RAW_ARGS:
#ifdef RESOLVE_ENV_PER_TOKEN
        args = clhy_resolve_env(parms->pool,args);
#endif
        return cmd->CLHY_RAW_ARGS(parms, mconfig, args);

    case TAKE_ARGV:
        {
            char *argv[CLHY_MAX_ARGC];
            int argc = 0;

            do {
                w = clhy_getword_conf(parms->pool, &args);
                if (*w == '\0' && *args == '\0') {
                    break;
                }
                argv[argc] = w;
                argc++;
            } while (argc < CLHY_MAX_ARGC && *args != '\0');

            return cmd->CLHY_TAKE_ARGV(parms, mconfig, argc, argv);
        }

    case NO_ARGS:
        if (*args != 0)
            return kuda_pstrcat(parms->pool, cmd->name, " takes no arguments",
                               NULL);

        return cmd->CLHY_NO_ARGS(parms, mconfig);

    case TAKE1:
        w = clhy_getword_conf(parms->pool, &args);

        if (*w == '\0' || *args != 0)
            return kuda_pstrcat(parms->pool, cmd->name, " takes one argument",
                               cmd->errmsg ? ", " : NULL, cmd->errmsg, NULL);

        return cmd->CLHY_TAKE1(parms, mconfig, w);

    case TAKE2:
        w = clhy_getword_conf(parms->pool, &args);
        w2 = clhy_getword_conf(parms->pool, &args);

        if (*w == '\0' || *w2 == '\0' || *args != 0)
            return kuda_pstrcat(parms->pool, cmd->name, " takes two arguments",
                               cmd->errmsg ? ", " : NULL, cmd->errmsg, NULL);

        return cmd->CLHY_TAKE2(parms, mconfig, w, w2);

    case TAKE12:
        w = clhy_getword_conf(parms->pool, &args);
        w2 = clhy_getword_conf(parms->pool, &args);

        if (*w == '\0' || *args != 0)
            return kuda_pstrcat(parms->pool, cmd->name, " takes 1-2 arguments",
                               cmd->errmsg ? ", " : NULL, cmd->errmsg, NULL);

        return cmd->CLHY_TAKE2(parms, mconfig, w, *w2 ? w2 : NULL);

    case TAKE3:
        w = clhy_getword_conf(parms->pool, &args);
        w2 = clhy_getword_conf(parms->pool, &args);
        w3 = clhy_getword_conf(parms->pool, &args);

        if (*w == '\0' || *w2 == '\0' || *w3 == '\0' || *args != 0)
            return kuda_pstrcat(parms->pool, cmd->name, " takes three arguments",
                               cmd->errmsg ? ", " : NULL, cmd->errmsg, NULL);

        return cmd->CLHY_TAKE3(parms, mconfig, w, w2, w3);

    case TAKE23:
        w = clhy_getword_conf(parms->pool, &args);
        w2 = clhy_getword_conf(parms->pool, &args);
        w3 = *args ? clhy_getword_conf(parms->pool, &args) : NULL;

        if (*w == '\0' || *w2 == '\0' || *args != 0)
            return kuda_pstrcat(parms->pool, cmd->name,
                               " takes two or three arguments",
                               cmd->errmsg ? ", " : NULL, cmd->errmsg, NULL);

        return cmd->CLHY_TAKE3(parms, mconfig, w, w2, w3);

    case TAKE123:
        w = clhy_getword_conf(parms->pool, &args);
        w2 = *args ? clhy_getword_conf(parms->pool, &args) : NULL;
        w3 = *args ? clhy_getword_conf(parms->pool, &args) : NULL;

        if (*w == '\0' || *args != 0)
            return kuda_pstrcat(parms->pool, cmd->name,
                               " takes one, two or three arguments",
                               cmd->errmsg ? ", " : NULL, cmd->errmsg, NULL);

        return cmd->CLHY_TAKE3(parms, mconfig, w, w2, w3);

    case TAKE13:
        w = clhy_getword_conf(parms->pool, &args);
        w2 = *args ? clhy_getword_conf(parms->pool, &args) : NULL;
        w3 = *args ? clhy_getword_conf(parms->pool, &args) : NULL;

        if (*w == '\0' || (w2 && *w2 && !w3) || *args != 0)
            return kuda_pstrcat(parms->pool, cmd->name,
                               " takes one or three arguments",
                               cmd->errmsg ? ", " : NULL, cmd->errmsg, NULL);

        return cmd->CLHY_TAKE3(parms, mconfig, w, w2, w3);

    case ITERATE:
        w = clhy_getword_conf(parms->pool, &args);
        
        if (*w == '\0')
            return kuda_pstrcat(parms->pool, cmd->name,
                               " requires at least one argument",
                               cmd->errmsg ? ", " : NULL, cmd->errmsg, NULL);

        while (*w != '\0') {
            errmsg = cmd->CLHY_TAKE1(parms, mconfig, w);

            if (errmsg && strcmp(errmsg, DECLINE_CMD) != 0)
                return errmsg;

            w = clhy_getword_conf(parms->pool, &args);
        }

        return errmsg;

    case ITERATE2:
        w = clhy_getword_conf(parms->pool, &args);

        if (*w == '\0' || *args == 0)
            return kuda_pstrcat(parms->pool, cmd->name,
                               " requires at least two arguments",
                               cmd->errmsg ? ", " : NULL, cmd->errmsg, NULL);

        while (*(w2 = clhy_getword_conf(parms->pool, &args)) != '\0') {

            errmsg = cmd->CLHY_TAKE2(parms, mconfig, w, w2);

            if (errmsg && strcmp(errmsg, DECLINE_CMD) != 0)
                return errmsg;
        }

        return errmsg;

    case FLAG:
        /*
         * This is safe to use temp_pool here, because the 'flag' itself is not
         * forwarded as-is
         */
        w = clhy_getword_conf(parms->temp_pool, &args);

        if (*w == '\0' || (strcasecmp(w, "on") && strcasecmp(w, "off")))
            return kuda_pstrcat(parms->pool, cmd->name, " must be On or Off",
                               NULL);

        return cmd->CLHY_FLAG(parms, mconfig, strcasecmp(w, "off") != 0);

    default:
        return kuda_pstrcat(parms->pool, cmd->name,
                           " is improperly configured internally (server bug)",
                           NULL);
    }
}

CLHY_CORE_DECLARE(const command_rec *) clhy_find_command(const char *name,
                                                     const command_rec *cmds)
{
    while (cmds->name) {
        if (!strcasecmp(name, cmds->name))
            return cmds;

        ++cmds;
    }

    return NULL;
}

CLHY_CORE_DECLARE(const command_rec *) clhy_find_command_in_capis(
                                          const char *cmd_name, cAPI **mod)
{
    const command_rec *cmdp;
    cAPI *capip;

    for (capip = *mod; capip; capip = capip->next) {
        if (capip->cmds && (cmdp = clhy_find_command(cmd_name, capip->cmds))) {
            *mod = capip;
            return cmdp;
        }
    }

    return NULL;
}

CLHY_CORE_DECLARE(void *) clhy_set_config_vectors(server_rec *server,
                                              clhy_conf_vector_t *section_vector,
                                              const char *section,
                                              cAPI *mod, kuda_pool_t *pconf)
{
    void *section_config = clhy_get_capi_config(section_vector, mod);
    void *server_config = clhy_get_capi_config(server->capi_config, mod);

    if (!section_config && mod->create_dir_config) {
        /* ### need to fix the create_dir_config functions' prototype... */
        section_config = (*mod->create_dir_config)(pconf, (char *)section);
        clhy_set_capi_config(section_vector, mod, section_config);
    }

    if (!server_config && mod->create_server_config) {
        server_config = (*mod->create_server_config)(pconf, server);
        clhy_set_capi_config(server->capi_config, mod, server_config);
    }

    return section_config;
}

static const char *execute_now(char *cmd_line, const char *args,
                               cmd_parms *parms,
                               kuda_pool_t *p, kuda_pool_t *ptemp,
                               clhy_directive_t **sub_tree,
                               clhy_directive_t *parent);

static const char *clhy_build_config_sub(kuda_pool_t *p, kuda_pool_t *temp_pool,
                                       const char *l, cmd_parms *parms,
                                       clhy_directive_t **current,
                                       clhy_directive_t **curr_parent,
                                       clhy_directive_t **conftree)
{
    const char *retval = NULL;
    const char *args;
    char *cmd_name;
    clhy_directive_t *newdir;
    cAPI *mod = clhy_top_capi;
    const command_rec *cmd;

    if (*l == '#' || *l == '\0')
        return NULL;

#if RESOLVE_ENV_PER_TOKEN
    args = l;
#else
    args = clhy_resolve_env(temp_pool, l);
#endif

    /* The first word is the name of a directive.  We can safely use the
     * 'temp_pool' for it.  If it matches the name of a known directive, we
     * can reference the string within the cAPI if needed.  Otherwise, we
     * can still make a copy in the 'p' pool. */
    cmd_name = clhy_getword_conf(temp_pool, &args);
    if (*cmd_name == '\0') {
        /* Note: this branch should not occur. An empty line should have
         * triggered the exit further above.
         */
        return NULL;
    }

    if (cmd_name[1] != '/') {
        char *lastc = cmd_name + strlen(cmd_name) - 1;
        if (*lastc == '>') {
            *lastc = '\0' ;
        }
        if (cmd_name[0] == '<' && *args == '\0') {
            args = ">";
        }
    }

    newdir = kuda_pcalloc(p, sizeof(clhy_directive_t));
    newdir->filename = parms->config_file->name;
    newdir->line_num = parms->config_file->line_number;
    newdir->args = kuda_pstrdup(p, args);

    if ((cmd = clhy_find_command_in_capis(cmd_name, &mod)) != NULL) {
        newdir->directive = cmd->name;
        
        if (cmd->req_override & EXEC_ON_READ) {
            clhy_directive_t *sub_tree = NULL;

            parms->err_directive = newdir;
            retval = execute_now(cmd_name, args, parms, p, temp_pool,
                                 &sub_tree, *curr_parent);
            if (*current) {
                (*current)->next = sub_tree;
            }
            else {
                *current = sub_tree;
                if (*curr_parent) {
                    (*curr_parent)->first_child = (*current);
                }
                if (*current) {
                    (*current)->parent = (*curr_parent);
                }
            }
            if (*current) {
                if (!*conftree) {
                    /* Before walking *current to the end of the list,
                     * set the head to *current.
                     */
                    *conftree = *current;
                }
                while ((*current)->next != NULL) {
                    (*current) = (*current)->next;
                    (*current)->parent = (*curr_parent);
                }
            }
            return retval;
        }
    }
    else {
        /* No known directive found?  Make a copy of what we have parsed. */
        newdir->directive = kuda_pstrdup(p, cmd_name);
    }


    if (cmd_name[0] == '<') {
        if (cmd_name[1] != '/') {
            (*current) = clhy_add_node(curr_parent, *current, newdir, 1);
        }
        else if (*curr_parent == NULL) {
            parms->err_directive = newdir;
            return kuda_pstrcat(p, cmd_name,
                               " without matching <", cmd_name + 2,
                               " section", NULL);
        }
        else {
            char *bracket = cmd_name + strlen(cmd_name) - 1;

            if (*bracket != '>') {
                parms->err_directive = newdir;
                return kuda_pstrcat(p, cmd_name,
                                   "> directive missing closing '>'", NULL);
            }

            *bracket = '\0';

            if (strcasecmp(cmd_name + 2,
                           (*curr_parent)->directive + 1) != 0) {
                parms->err_directive = newdir;
                return kuda_pstrcat(p, "Expected </",
                                   (*curr_parent)->directive + 1, "> but saw ",
                                   cmd_name, ">", NULL);
            }

            *bracket = '>';

            /* done with this section; move up a level */
            *current = *curr_parent;
            *curr_parent = (*current)->parent;
        }
    }
    else {
        *current = clhy_add_node(curr_parent, *current, newdir, 0);
    }

    return retval;
}

#define VARBUF_INIT_LEN 200
#define VARBUF_MAX_LEN  (16*1024*1024)

CLHY_DECLARE(const char *) clhy_build_cont_config(kuda_pool_t *p,
                                              kuda_pool_t *temp_pool,
                                              cmd_parms *parms,
                                              clhy_directive_t **current,
                                              clhy_directive_t **curr_parent,
                                              char *orig_directive)
{
    char *bracket;
    const char *retval;
    clhy_directive_t *sub_tree = NULL;
    kuda_status_t rc;
    struct clhy_varbuf vb;
    kuda_size_t max_len = VARBUF_MAX_LEN;
    if (p == temp_pool)
        max_len = HUGE_STRING_LEN; /* lower limit for .htaccess */

    bracket = kuda_pstrcat(temp_pool, orig_directive + 1, ">", NULL);
    clhy_varbuf_init(temp_pool, &vb, VARBUF_INIT_LEN);

    while ((rc = clhy_varbuf_cfg_getline(&vb, parms->config_file, max_len))
           == KUDA_SUCCESS) {
        if (!memcmp(vb.buf, "</", 2)
            && (strcasecmp(vb.buf + 2, bracket) == 0)
            && (*curr_parent == NULL)) {
            break;
        }
        retval = clhy_build_config_sub(p, temp_pool, vb.buf, parms, current,
                                     curr_parent, &sub_tree);
        if (retval != NULL)
            return retval;

        if (sub_tree == NULL) {
            sub_tree = *curr_parent;
        }

        if (sub_tree == NULL) {
            sub_tree = *current;
        }
    }
    clhy_varbuf_free(&vb);
    if (rc != KUDA_EOF && rc != KUDA_SUCCESS)
        return clhy_pcfg_strerror(temp_pool, parms->config_file, rc);

    *current = sub_tree;
    return NULL;
}

static const char *clhy_walk_config_sub(const clhy_directive_t *current,
                                      cmd_parms *parms,
                                      clhy_conf_vector_t *section_vector)
{
    const command_rec *cmd;
    clhy_capi_list *ml;
    char *dir = kuda_pstrdup(parms->temp_pool, current->directive);

    clhy_str_tolower(dir);

    ml = kuda_hash_get(clhy_config_hash, dir, KUDA_HASH_KEY_STRING);

    if (ml == NULL) {
        parms->err_directive = current;
        if (parms->override & NONFATAL_UNKNOWN) {
            clhy_log_perror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, parms->temp_pool,
                          CLHYLOGNO(02296) "Unknown directive %s "
                          "perhaps misspelled or defined by a cAPI "
                          "not included in the server configuration", dir);
            return NULL;
        }
        else {
            return kuda_pstrcat(parms->pool, "Invalid command '",
                               current->directive,
                               "', perhaps misspelled or defined by a cAPI "
                               "not included in the server configuration",
                               NULL);
        }
    }

    for ( ; ml != NULL; ml = ml->next) {
        void *dir_config = clhy_set_config_vectors(parms->server,
                                                 section_vector,
                                                 parms->path,
                                                 ml->m,
                                                 parms->pool);
        const char *retval;
        cmd = ml->cmd;

        /* Once was enough? */
        if (cmd->req_override & EXEC_ON_READ) {
            continue;
        }

        retval = invoke_cmd(cmd, parms, dir_config, current->args);

        if (retval != NULL && strcmp(retval, DECLINE_CMD) != 0) {
            /* If the directive in error has already been set, don't
             * replace it.  Otherwise, an error inside a container
             * will be reported as occurring on the first line of the
             * container.
             */
            if (!parms->err_directive) {
                parms->err_directive = current;
            }
            return retval;
        }
    }

    return NULL;
}

CLHY_DECLARE(const char *) clhy_walk_config(clhy_directive_t *current,
                                        cmd_parms *parms,
                                        clhy_conf_vector_t *section_vector)
{
    clhy_conf_vector_t *oldconfig = parms->context;

    parms->context = section_vector;

    /* scan through all directives, executing each one */
    for (; current != NULL; current = current->next) {
        const char *errmsg;

        parms->directive = current;

        /* actually parse the command and execute the correct function */
        errmsg = clhy_walk_config_sub(current, parms, section_vector);
        if (errmsg != NULL) {
            /* restore the context (just in case) */
            parms->context = oldconfig;
            return errmsg;
        }
    }

    parms->context = oldconfig;
    return NULL;
}

CLHY_DECLARE(const char *) clhy_build_config(cmd_parms *parms,
                                         kuda_pool_t *p, kuda_pool_t *temp_pool,
                                         clhy_directive_t **conftree)
{
    clhy_directive_t *current = *conftree;
    clhy_directive_t *curr_parent = NULL;
    const char *errmsg;
    clhy_directive_t **last_ptr = NULL;
    kuda_status_t rc;
    struct clhy_varbuf vb;
    kuda_size_t max_len = VARBUF_MAX_LEN;
    if (p == temp_pool)
        max_len = HUGE_STRING_LEN; /* lower limit for .htaccess */

    clhy_varbuf_init(temp_pool, &vb, VARBUF_INIT_LEN);

    if (current != NULL) {
        /* If we have to traverse the whole tree again for every included
         * config file, the required time grows as O(n^2) with the number of
         * files. This can be a significant delay for large configurations.
         * Therefore we cache a pointer to the last node.
         */
        last_ptr = &(current->last);

        if (last_ptr && *last_ptr) {
            current = *last_ptr;
        }

        while (current->next) {
            current = current->next;
        }

        if (last_ptr) {
            /* update cached pointer to last node */
            *last_ptr = current;
        }
    }

    while ((rc = clhy_varbuf_cfg_getline(&vb, parms->config_file, max_len))
           == KUDA_SUCCESS) {
        errmsg = clhy_build_config_sub(p, temp_pool, vb.buf, parms,
                                     &current, &curr_parent, conftree);
        if (errmsg != NULL)
            return errmsg;

        if (*conftree == NULL && curr_parent != NULL) {
            *conftree = curr_parent;
        }

        if (*conftree == NULL && current != NULL) {
            *conftree = current;
        }
    }
    clhy_varbuf_free(&vb);
    if (rc != KUDA_EOF && rc != KUDA_SUCCESS)
        return clhy_pcfg_strerror(temp_pool, parms->config_file, rc);

    if (curr_parent != NULL) {
        errmsg = "";

        while (curr_parent != NULL) {
            errmsg = kuda_psprintf(p, "%s%s%s:%u: %s> was not closed.",
                                  errmsg,
                                  *errmsg == '\0' ? "" : KUDA_EOL_STR,
                                  curr_parent->filename,
                                  curr_parent->line_num,
                                  curr_parent->directive);

            parms->err_directive = curr_parent;
            curr_parent = curr_parent->parent;
        }

        return errmsg;
    }

    return NULL;
}

/*
 * Generic command functions...
 */

CLHY_DECLARE_NONSTD(const char *) clhy_set_string_slot(cmd_parms *cmd,
                                                   void *struct_ptr,
                                                   const char *arg)
{
    int offset = (int)(long)cmd->info;

    *(const char **)((char *)struct_ptr + offset) = arg;

    return NULL;
}

CLHY_DECLARE_NONSTD(const char *) clhy_set_int_slot(cmd_parms *cmd,
                                                void *struct_ptr,
                                                const char *arg)
{
    char *endptr;
    char *error_str = NULL;
    int offset = (int)(long)cmd->info;

    *(int *)((char*)struct_ptr + offset) = strtol(arg, &endptr, 10);

    if ((*arg == '\0') || (*endptr != '\0')) {
        error_str = kuda_psprintf(cmd->pool,
                     "Invalid value for directive %s, expected integer",
                     cmd->directive->directive);
    }

    return error_str;
}

CLHY_DECLARE_NONSTD(const char *) clhy_set_string_slot_lower(cmd_parms *cmd,
                                                         void *struct_ptr,
                                                         const char *arg_)
{
    char *arg = kuda_pstrdup(cmd->pool,arg_);
    int offset = (int)(long)cmd->info;

    clhy_str_tolower(arg);
    *(char **)((char *)struct_ptr + offset) = arg;

    return NULL;
}

CLHY_DECLARE_NONSTD(const char *) clhy_set_flag_slot(cmd_parms *cmd,
                                                 void *struct_ptr_v, int arg)
{
    int offset = (int)(long)cmd->info;
    char *struct_ptr = (char *)struct_ptr_v;

    *(int *)(struct_ptr + offset) = arg ? 1 : 0;

    return NULL;
}

CLHY_DECLARE_NONSTD(const char *) clhy_set_flag_slot_char(cmd_parms *cmd,
                                                      void *struct_ptr_v, int arg)
{
    int offset = (int)(long)cmd->info;
    char *struct_ptr = (char *)struct_ptr_v;

    *(struct_ptr + offset) = arg ? 1 : 0;

    return NULL;
}


CLHY_DECLARE_NONSTD(const char *) clhy_set_file_slot(cmd_parms *cmd, void *struct_ptr,
                                                 const char *arg)
{
    /* Prepend server_root to relative arg.
     * This allows most args to be independent of server_root,
     * so the server can be moved or mirrored with less pain.
     */
    const char *path;
    int offset = (int)(long)cmd->info;

    path = clhy_server_root_relative(cmd->pool, arg);

    if (!path) {
        return kuda_pstrcat(cmd->pool, cmd->cmd->name, ": Invalid file path '",
                           arg, "'", NULL);
    }

    *(const char **) ((char*)struct_ptr + offset) = path;

    return NULL;
}

CLHY_DECLARE_NONSTD(const char *) clhy_set_deprecated(cmd_parms *cmd,
                                                  void *struct_ptr,
                                                  const char *arg)
{
    return cmd->cmd->errmsg;
}

CLHY_DECLARE(void) clhy_reset_capi_loglevels(struct clhy_logconf *l, int val)
{
    if (l->capi_levels)
        memset(l->capi_levels, val, conf_vector_length);
}

CLHY_DECLARE(void) clhy_set_capi_loglevel(kuda_pool_t *pool, struct clhy_logconf *l,
                                        int index, int level)
{
    if (!l->capi_levels) {
        l->capi_levels = kuda_palloc(pool, conf_vector_length);
        if (l->level == CLHYLOG_UNSET) {
                clhy_reset_capi_loglevels(l, CLHYLOG_UNSET);
        }
        else {
                clhy_reset_capi_loglevels(l, CLHYLOG_NO_CAPI);
        }
    }

    l->capi_levels[index] = level;
}

/*****************************************************************
 *
 * Reading whole config files...
 */

static cmd_parms default_parms =
{NULL, 0, 0, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

CLHY_DECLARE(char *) clhy_server_root_relative(kuda_pool_t *p, const char *file)
{
    char *newpath = NULL;
    kuda_status_t rv;
    rv = kuda_filepath_merge(&newpath, clhy_server_root, file,
                            KUDA_FILEPATH_TRUENAME, p);
    if (newpath && (rv == KUDA_SUCCESS || KUDA_STATUS_IS_EPATHWILD(rv)
                                      || KUDA_STATUS_IS_ENOENT(rv)
                                      || KUDA_STATUS_IS_ENOTDIR(rv))) {
        return newpath;
    }
    else {
        return NULL;
    }
}

CLHY_DECLARE(char *) clhy_runtime_dir_relative(kuda_pool_t *p, const char *file)
{
    char *newpath = NULL;
    kuda_status_t rv;
    const char *runtime_dir = clhy_runtime_dir ? clhy_runtime_dir : clhy_server_root_relative(p, DEFAULT_REL_RUNTIMEDIR);

    rv = kuda_filepath_merge(&newpath, runtime_dir, file,
                            KUDA_FILEPATH_TRUENAME, p);
    if (newpath && (rv == KUDA_SUCCESS || KUDA_STATUS_IS_EPATHWILD(rv)
                                      || KUDA_STATUS_IS_ENOENT(rv)
                                      || KUDA_STATUS_IS_ENOTDIR(rv))) {
        return newpath;
    }
    else {
        return NULL;
    }
}


CLHY_DECLARE(const char *) clhy_soak_end_container(cmd_parms *cmd, char *directive)
{
    struct clhy_varbuf vb;
    const char *args;
    char *cmd_name;
    kuda_status_t rc;
    kuda_size_t max_len = VARBUF_MAX_LEN;
    if (cmd->pool == cmd->temp_pool)
        max_len = HUGE_STRING_LEN; /* lower limit for .htaccess */

    clhy_varbuf_init(cmd->temp_pool, &vb, VARBUF_INIT_LEN);

    while ((rc = clhy_varbuf_cfg_getline(&vb, cmd->config_file, max_len))
           == KUDA_SUCCESS) {
        args = vb.buf;

        cmd_name = clhy_getword_conf(cmd->temp_pool, &args);
        if (cmd_name[0] == '<') {
            if (cmd_name[1] == '/') {
                cmd_name[strlen(cmd_name) - 1] = '\0';

                if (strcasecmp(cmd_name + 2, directive + 1) != 0) {
                    return kuda_pstrcat(cmd->pool, "Expected </",
                                       directive + 1, "> but saw ",
                                       cmd_name, ">", NULL);
                }

                clhy_varbuf_free(&vb);
                return NULL; /* found end of container */
            }
            else {
                const char *msg;

                if (*args == '\0' && cmd_name[strlen(cmd_name) - 1] == '>') {
                    cmd_name[strlen(cmd_name) - 1] = '\0';
                }

                if ((msg = clhy_soak_end_container(cmd, cmd_name)) != NULL) {
                    return msg;
                }
            }
        }
    }
    if (rc != KUDA_EOF && rc != KUDA_SUCCESS)
        return clhy_pcfg_strerror(cmd->temp_pool, cmd->config_file, rc);

    return kuda_pstrcat(cmd->pool, "Expected </",
                       directive + 1, "> before end of configuration",
                       NULL);
}

static const char *execute_now(char *cmd_line, const char *args,
                               cmd_parms *parms,
                               kuda_pool_t *p, kuda_pool_t *ptemp,
                               clhy_directive_t **sub_tree,
                               clhy_directive_t *parent)
{
    const command_rec *cmd;
    clhy_capi_list *ml;
    char *dir = kuda_pstrdup(parms->temp_pool, cmd_line);

    clhy_str_tolower(dir);

    ml = kuda_hash_get(clhy_config_hash, dir, KUDA_HASH_KEY_STRING);

    if (ml == NULL) {
        return kuda_pstrcat(parms->pool, "Invalid command '",
                           cmd_line,
                           "', perhaps misspelled or defined by a cAPI "
                           "not included in the server configuration",
                           NULL);
    }

    for ( ; ml != NULL; ml = ml->next) {
        const char *retval;
        cmd = ml->cmd;

        retval = invoke_cmd(cmd, parms, sub_tree, args);

        if (retval != NULL) {
            return retval;
        }
    }

    return NULL;
}

/* This structure and the following functions are needed for the
 * table-based config file reading. They are passed to the
 * cfg_open_custom() routine.
 */

/* Structure to be passed to cfg_open_custom(): it contains an
 * index which is incremented from 0 to nelts on each call to
 * cfg_getline() (which in turn calls arr_elts_getstr())
 * and an kuda_array_header_t pointer for the string array.
 */
typedef struct {
    kuda_array_header_t *array;
    int curr_idx;
} arr_elts_param_t;


/* arr_elts_getstr() returns the next line from the string array. */
static kuda_status_t arr_elts_getstr(void *buf, kuda_size_t bufsiz, void *param)
{
    arr_elts_param_t *arr_param = (arr_elts_param_t *)param;
    const char *elt;

    /* End of array reached? */
    if (++arr_param->curr_idx > arr_param->array->nelts)
        return KUDA_EOF;

    /* return the line */
    elt = ((const char **)arr_param->array->elts)[arr_param->curr_idx - 1];
    if (kuda_cpystrn(buf, elt, bufsiz) - (char *)buf >= bufsiz - 1)
        return KUDA_ENOSPC;
    return KUDA_SUCCESS;
}


/* arr_elts_close(): dummy close routine (makes sure no more lines can be read) */
static kuda_status_t arr_elts_close(void *param)
{
    arr_elts_param_t *arr_param = (arr_elts_param_t *)param;

    arr_param->curr_idx = arr_param->array->nelts;

    return KUDA_SUCCESS;
}

static const char *process_command_config(server_rec *s,
                                          kuda_array_header_t *arr,
                                          clhy_directive_t **conftree,
                                          kuda_pool_t *p,
                                          kuda_pool_t *ptemp)
{
    const char *errmsg;
    cmd_parms parms;
    arr_elts_param_t arr_parms;

    arr_parms.curr_idx = 0;
    arr_parms.array = arr;

    if (clhy_config_hash == NULL) {
        rebuild_conf_hash(s->process->pconf, 1);
    }

    parms = default_parms;
    parms.pool = p;
    parms.temp_pool = ptemp;
    parms.server = s;
    parms.override = (RSRC_CONF | OR_ALL) & ~(OR_AUTHCFG | OR_LIMIT);
    parms.override_opts = OPT_ALL | OPT_SYM_OWNER | OPT_MULTI;

    parms.config_file = clhy_pcfg_open_custom(p, "-c/-C directives",
                                            &arr_parms, NULL,
                                            arr_elts_getstr, arr_elts_close);

    errmsg = clhy_build_config(&parms, p, ptemp, conftree);
    clhy_cfg_closefile(parms.config_file);

    if (errmsg) {
        return kuda_pstrcat(p, "Syntax error in -C/-c directive: ", errmsg,
                           NULL);
    }

    return NULL;
}

/**
 * Used by -D DUMP_INCLUDES to output the config file "tree".
 */
static void dump_config_name(const char *fname, kuda_pool_t *p)
{
    unsigned i, recursion, line_number;
    void *data;
    kuda_file_t *out = NULL;

    kuda_file_open_stdout(&out, p);

    /* clhy_include_sentinel is defined by the core Include directive; use it to
     * figure out how deep in the stack we are.
     */
    kuda_pool_userdata_get(&data, "clhy_include_sentinel", p);

    if (data) {
        recursion = *(unsigned *)data;
    } else {
        recursion = 0;
    }

    /* Indent once for each level. */
    for (i = 0; i < (recursion + 1); ++i) {
        kuda_file_printf(out, "  ");
    }

    /* clhy_include_lineno is similarly defined to tell us where in the last
     * config file we were.
     */
    kuda_pool_userdata_get(&data, "clhy_include_lineno", p);

    if (data) {
        line_number = *(unsigned *)data;
    } else {
        line_number = 0;
    }

    /* Print the line number and the name of the parsed file. */
    if (line_number > 0) {
        kuda_file_printf(out, "(%u)", line_number);
    } else {
        kuda_file_printf(out, "(*)");
    }

    kuda_file_printf(out, " %s\n", fname);
}

CLHY_DECLARE(const char *) clhy_process_resource_config(server_rec *s,
                                                    const char *fname,
                                                    clhy_directive_t **conftree,
                                                    kuda_pool_t *p,
                                                    kuda_pool_t *ptemp)
{
    clhy_configfile_t *cfp;
    cmd_parms parms;
    kuda_status_t rv;
    const char *error;

    parms = default_parms;
    parms.pool = p;
    parms.temp_pool = ptemp;
    parms.server = s;
    parms.override = (RSRC_CONF | OR_ALL) & ~(OR_AUTHCFG | OR_LIMIT);
    parms.override_opts = OPT_ALL | OPT_SYM_OWNER | OPT_MULTI;

    rv = clhy_pcfg_openfile(&cfp, p, fname);
    if (rv != KUDA_SUCCESS) {
        return kuda_psprintf(p, "Could not open configuration file %s: %pm",
                            fname, &rv);
    }

    if (clhy_exists_config_define("DUMP_INCLUDES")) {
        dump_config_name(fname, p);
    }

    parms.config_file = cfp;
    error = clhy_build_config(&parms, p, ptemp, conftree);
    clhy_cfg_closefile(cfp);

    if (error) {
        if (parms.err_directive)
            return kuda_psprintf(p, "Syntax error on line %d of %s: %s",
                                parms.err_directive->line_num,
                                parms.err_directive->filename, error);
        else
            return error;
    }

    return NULL;
}

typedef struct {
    server_rec *s;
    clhy_directive_t **conftree;
} configs;

static const char *process_resource_config_cb(clhy_dir_match_t *w, const char *fname)
{
    configs *cfgs = w->ctx;
    return clhy_process_resource_config(cfgs->s, fname, cfgs->conftree, w->p, w->ptemp);
}

CLHY_DECLARE(const char *) clhy_process_fnmatch_configs(server_rec *s,
                                                    const char *fname,
                                                    clhy_directive_t **conftree,
                                                    kuda_pool_t *p,
                                                    kuda_pool_t *ptemp,
                                                    int optional)
{
    configs cfgs;
    clhy_dir_match_t w;

    cfgs.s = s;
    cfgs.conftree = conftree;

    w.prefix = "Include/IncludeOptional: ";
    w.p = p;
    w.ptemp = ptemp;
    w.flags = (optional ? CLHY_DIR_FLAG_OPTIONAL : CLHY_DIR_FLAG_NONE) | CLHY_DIR_FLAG_RECURSIVE;
    w.cb = process_resource_config_cb;
    w.ctx = &cfgs;
    w.depth = 0;

    /* don't require conf/wwhy.conf if we have a -C or -c switch */
    if ((clhy_server_pre_read_config->nelts
        || clhy_server_post_read_config->nelts)
        && !(strcmp(fname, clhy_server_root_relative(ptemp, SERVER_CONFIG_FILE)))) {
        kuda_finfo_t finfo;

        if (kuda_stat(&finfo, fname, KUDA_FINFO_LINK | KUDA_FINFO_TYPE, ptemp) != KUDA_SUCCESS)
            return NULL;
    }

    if (!kuda_fnmatch_test(fname)) {
        return clhy_dir_nofnmatch(&w, fname);
    }
    else {
        kuda_status_t status;
        const char *rootpath, *filepath = fname;

        /* locate the start of the directories proper */
        status = kuda_filepath_root(&rootpath, &filepath, KUDA_FILEPATH_TRUENAME, ptemp);

        /* we allow KUDA_SUCCESS and KUDA_EINCOMPLETE */
        if (KUDA_ERELATIVE == status) {
            return kuda_pstrcat(p, "Include must have an absolute path, ", fname, NULL);
        }
        else if (KUDA_EBADPATH == status) {
            return kuda_pstrcat(p, "Include has a bad path, ", fname, NULL);
        }

        /* walk the filepath */
        return clhy_dir_fnmatch(&w, rootpath, filepath);
    }
}

CLHY_DECLARE(int) clhy_process_config_tree(server_rec *s,
                                       clhy_directive_t *conftree,
                                       kuda_pool_t *p,
                                       kuda_pool_t *ptemp)
{
    const char *errmsg;
    cmd_parms parms;

    parms = default_parms;
    parms.pool = p;
    parms.temp_pool = ptemp;
    parms.server = s;
    parms.override = (RSRC_CONF | OR_ALL) & ~(OR_AUTHCFG | OR_LIMIT);
    parms.override_opts = OPT_ALL | OPT_SYM_OWNER | OPT_MULTI;
    parms.limited = -1;

    errmsg = clhy_walk_config(conftree, &parms, s->lookup_defaults);
    if (errmsg) {
        if (parms.err_directive)
            clhy_log_perror(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, p, CLHYLOGNO(00526)
                          "Syntax error on line %d of %s:",
                          parms.err_directive->line_num,
                          parms.err_directive->filename);
        clhy_log_perror(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, p, "%s", errmsg);
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    return OK;
}

kuda_status_t clhy_open_htaccess(request_rec *r, const char *dir_name,
                              const char *access_name,
                              clhy_configfile_t **conffile,
                              const char **full_name)
{
    *full_name = clhy_make_full_path(r->pool, dir_name, access_name);
    return clhy_pcfg_openfile(conffile, r->pool, *full_name);
}

CLHY_CORE_DECLARE(int) clhy_parse_htaccess(clhy_conf_vector_t **result,
                                       request_rec *r, int override,
                                       int override_opts, kuda_table_t *override_list,
                                       const char *d, const char *access_names)
{
    clhy_configfile_t *f = NULL;
    cmd_parms parms;
    const char *filename;
    const struct htaccess_result *cache;
    struct htaccess_result *new;
    clhy_conf_vector_t *dc = NULL;
    kuda_status_t status;

    /* firstly, search cache */
    for (cache = r->htaccess; cache != NULL; cache = cache->next) {
        if (cache->override == override && strcmp(cache->dir, d) == 0) {
            *result = cache->htaccess;
            return OK;
        }
    }

    parms = default_parms;
    parms.override = override;
    parms.override_opts = override_opts;
    parms.override_list = override_list;
    parms.pool = r->pool;
    parms.temp_pool = r->pool;
    parms.server = r->server;
    parms.path = kuda_pstrdup(r->pool, d);

    /* loop through the access names and find the first one */
    while (access_names[0]) {
        const char *access_name = clhy_getword_conf(r->pool, &access_names);

        filename = NULL;
        status = clhy_run_open_htaccess(r, d, access_name, &f, &filename);
        if (status == KUDA_SUCCESS) {
            const char *errmsg;
            clhy_directive_t *temptree = NULL;

            dc = clhy_create_per_dir_config(r->pool);

            parms.config_file = f;
            errmsg = clhy_build_config(&parms, r->pool, r->pool, &temptree);
            if (errmsg == NULL)
                errmsg = clhy_walk_config(temptree, &parms, dc);

            clhy_cfg_closefile(f);

            if (errmsg) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ALERT, 0, r,
                              "%s: %s", filename, errmsg);
                return HTTP_INTERNAL_SERVER_ERROR;
            }

            *result = dc;
            break;
        }
        else {
            if (!KUDA_STATUS_IS_ENOENT(status)
                && !KUDA_STATUS_IS_ENOTDIR(status)) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_CRIT, status, r, CLHYLOGNO(00529)
                              "%s pcfg_openfile: unable to check htaccess file, "
                              "ensure it is readable and that '%s' "
                              "is executable",
                              filename, d);
                kuda_table_setn(r->notes, "error-notes",
                               "Server unable to read htaccess file, denying "
                               "access to be safe");
                return HTTP_FORBIDDEN;
            }
        }
    }

    /* cache it */
    new = kuda_palloc(r->pool, sizeof(struct htaccess_result));
    new->dir = parms.path;
    new->override = override;
    new->override_opts = override_opts;
    new->htaccess = dc;

    /* add to head of list */
    new->next = r->htaccess;
    r->htaccess = new;

    return OK;
}

CLHY_CORE_DECLARE(const char *) clhy_init_virtual_host(kuda_pool_t *p,
                                                   const char *hostname,
                                                   server_rec *main_server,
                                                   server_rec **ps)
{
    server_rec *s = (server_rec *) kuda_pcalloc(p, sizeof(server_rec));

    /* TODO: this crap belongs in http_core */
    s->process = main_server->process;
    s->server_admin = NULL;
    s->server_hostname = NULL;
    s->server_scheme = NULL;
    s->error_fname = NULL;
    s->timeout = 0;
    s->keep_alive_timeout = 0;
    s->keep_alive = -1;
    s->keep_alive_max = -1;
    s->error_log = main_server->error_log;
    s->log.level = CLHYLOG_UNSET;
    s->log.capi_levels = NULL;
    /* useful default, otherwise we get a port of 0 on redirects */
    s->port = main_server->port;
    s->next = NULL;

    s->is_virtual = 1;
    s->names = kuda_array_make(p, 4, sizeof(char **));
    s->wild_names = kuda_array_make(p, 4, sizeof(char **));

    s->capi_config = create_empty_config(p);
    s->lookup_defaults = clhy_create_per_dir_config(p);

    s->limit_req_line = main_server->limit_req_line;
    s->limit_req_fieldsize = main_server->limit_req_fieldsize;
    s->limit_req_fields = main_server->limit_req_fields;

    *ps = s;

    return clhy_parse_vhost_addrs(p, hostname, s);
}

CLHY_DECLARE(struct clhy_logconf *) clhy_new_log_config(kuda_pool_t *p,
                                                  const struct clhy_logconf *old)
{
    struct clhy_logconf *l = kuda_pcalloc(p, sizeof(struct clhy_logconf));
    if (old) {
        l->level = old->level;
        if (old->capi_levels) {
            l->capi_levels =
                kuda_pmemdup(p, old->capi_levels, conf_vector_length);
        }
    }
    else {
        l->level = CLHYLOG_UNSET;
    }
    return l;
}

CLHY_DECLARE(void) clhy_merge_log_config(const struct clhy_logconf *old_conf,
                                     struct clhy_logconf *new_conf)
{
    if (new_conf->level != CLHYLOG_UNSET) {
        /* Setting the main loglevel resets all per-cAPI log levels.
         * I.e. if new->level has been set, we must ignore old->capi_levels.
         */
        return;
    }

    new_conf->level = old_conf->level;
    if (new_conf->capi_levels == NULL) {
        new_conf->capi_levels = old_conf->capi_levels;
    }
    else if (old_conf->capi_levels != NULL) {
        int i;
        for (i = 0; i < conf_vector_length; i++) {
            if (new_conf->capi_levels[i] == CLHYLOG_UNSET)
                new_conf->capi_levels[i] = old_conf->capi_levels[i];
        }
    }
}

CLHY_DECLARE(void) clhy_fixup_virtual_hosts(kuda_pool_t *p, server_rec *main_server)
{
    server_rec *virt;
    core_dir_config *dconf =
        clhy_get_core_capi_config(main_server->lookup_defaults);
    dconf->log = &main_server->log;

    for (virt = main_server->next; virt; virt = virt->next) {
        merge_server_configs(p, main_server->capi_config, virt);

        virt->lookup_defaults =
            clhy_merge_per_dir_configs(p, main_server->lookup_defaults,
                                     virt->lookup_defaults);

        if (virt->server_admin == NULL)
            virt->server_admin = main_server->server_admin;

        if (virt->timeout == 0)
            virt->timeout = main_server->timeout;

        if (virt->keep_alive_timeout == 0)
            virt->keep_alive_timeout = main_server->keep_alive_timeout;

        if (virt->keep_alive == -1)
            virt->keep_alive = main_server->keep_alive;

        if (virt->keep_alive_max == -1)
            virt->keep_alive_max = main_server->keep_alive_max;

        clhy_merge_log_config(&main_server->log, &virt->log);

        dconf = clhy_get_core_capi_config(virt->lookup_defaults);
        dconf->log = &virt->log;

        /* XXX: this is really something that should be dealt with by a
         * post-config api phase
         */
        clhy_core_reorder_directories(p, virt);
    }

    clhy_core_reorder_directories(p, main_server);
}

/*****************************************************************
 *
 * Getting *everything* configured...
 */

static void init_config_globals(kuda_pool_t *p)
{
    /* Global virtual host hash bucket pointers.  Init to null. */
    clhy_init_vhost_config(p);
}

static server_rec *init_server_config(process_rec *process, kuda_pool_t *p)
{
    kuda_status_t rv;
    server_rec *s = (server_rec *) kuda_pcalloc(p, sizeof(server_rec));

    kuda_file_open_stderr(&s->error_log, p);
    s->process = process;
    s->port = 0;
    s->server_admin = DEFAULT_ADMIN;
    s->server_hostname = NULL;
    s->server_scheme = NULL;
    s->error_fname = DEFAULT_ERRORLOG;
    s->log.level = DEFAULT_LOGLEVEL;
    s->log.capi_levels = NULL;
    s->limit_req_line = DEFAULT_LIMIT_REQUEST_LINE;
    s->limit_req_fieldsize = DEFAULT_LIMIT_REQUEST_FIELDSIZE;
    s->limit_req_fields = DEFAULT_LIMIT_REQUEST_FIELDS;
    s->timeout = kuda_time_from_sec(DEFAULT_TIMEOUT);
    s->keep_alive_timeout = kuda_time_from_sec(DEFAULT_KEEPALIVE_TIMEOUT);
    s->keep_alive_max = DEFAULT_KEEPALIVE;
    s->keep_alive = 1;
    s->next = NULL;
    s->addrs = kuda_pcalloc(p, sizeof(server_addr_rec));

    /* NOT virtual host; don't match any real network interface */
    rv = kuda_sockaddr_info_get(&s->addrs->host_addr,
                               NULL, KUDA_UNSPEC, 0, 0, p);
    if (rv != KUDA_SUCCESS) {
        /* should we test here for rv being an EAIERR? */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP|CLHYLOG_CRIT, rv, NULL, CLHYLOGNO(00530)
                     "initialisation: bug or getaddrinfo fail");
        return NULL;
    }

    s->addrs->host_port = 0; /* matches any port */
    s->addrs->virthost = ""; /* must be non-NULL */
    s->names = s->wild_names = NULL;

    s->capi_config = create_server_config(p, s);
    s->lookup_defaults = create_default_per_dir_config(p);

    return s;
}


static kuda_status_t reset_conf_vector_length(void *dummy)
{
    reserved_capi_slots = 0;
    conf_vector_length = max_capis;
    return KUDA_SUCCESS;
}

static int conf_vector_length_pre_config(kuda_pool_t *pconf, kuda_pool_t *plog,
                                         kuda_pool_t *ptemp)
{
    /*
     * We have loaded all cAPIs that are loaded by EXEC_ON_READ directives.
     * From now on we reduce the size of the config vectors to what we need,
     * plus what has been reserved (e.g. by capi_perl) for additional cAPIs
     * loaded later on.
     * If max_capis is too small, clhy_add_capi() will abort.
     */
    if (total_capis + reserved_capi_slots < max_capis) {
        conf_vector_length = total_capis + reserved_capi_slots;
    }
    kuda_pool_cleanup_register(pconf, NULL, reset_conf_vector_length,
                              kuda_pool_cleanup_null);
    return OK;
}


CLHY_CORE_DECLARE(void) clhy_register_config_hooks(kuda_pool_t *p)
{
    clhy_hook_pre_config(conf_vector_length_pre_config, NULL, NULL,
                       KUDA_HOOK_REALLY_LAST);
}

CLHY_DECLARE(server_rec*) clhy_read_config(process_rec *process, kuda_pool_t *ptemp,
                                       const char *filename,
                                       clhy_directive_t **conftree)
{
    const char *confname, *error;
    kuda_pool_t *p = process->pconf;
    server_rec *s = init_server_config(process, p);
    if (s == NULL) {
        return s;
    }

    init_config_globals(p);

    if (clhy_exists_config_define("DUMP_INCLUDES")) {
        kuda_file_t *out = NULL;
        kuda_file_open_stdout(&out, p);

        /* Included files will be dumped as the config is walked; print a
         * header.
         */
        kuda_file_printf(out, "Included configuration files:\n");
    }

    /* All server-wide config files now have the SAME syntax... */
    error = process_command_config(s, clhy_server_pre_read_config, conftree,
                                   p, ptemp);
    if (error) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP|CLHYLOG_CRIT, 0, NULL, "%s: %s",
                     clhy_server_argv0, error);
        return NULL;
    }

    /* process_command_config may change the ServerRoot so
     * compute this config file name afterwards.
     */
    confname = clhy_server_root_relative(p, filename);

    if (!confname) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP|CLHYLOG_CRIT,
                     KUDA_EBADPATH, NULL, CLHYLOGNO(00532) "Invalid config file path %s",
                     filename);
        return NULL;
    }

    error = clhy_process_resource_config(s, confname, conftree, p, ptemp);
    if (error) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP|CLHYLOG_CRIT, 0, NULL,
                     "%s: %s", clhy_server_argv0, error);
        return NULL;
    }

    error = clhy_check_clcore();
    if (error) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP|CLHYLOG_CRIT, 0, NULL, CLHYLOGNO(00534)
                     "%s: Configuration error: %s", clhy_server_argv0, error);
        return NULL;
    }

    error = process_command_config(s, clhy_server_post_read_config, conftree,
                                   p, ptemp);

    if (error) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP|CLHYLOG_CRIT, 0, NULL, "%s: %s",
                     clhy_server_argv0, error);
        return NULL;
    }

    return s;
}

CLHY_DECLARE(void) clhy_single_capi_configure(kuda_pool_t *p, server_rec *s,
                                            cAPI *m)
{
    if (m->create_server_config)
        clhy_set_capi_config(s->capi_config, m,
                             (*m->create_server_config)(p, s));

    if (m->create_dir_config)
        clhy_set_capi_config(s->lookup_defaults, m,
                             (*m->create_dir_config)(p, NULL));
}

CLHY_DECLARE(void) clhy_run_rewrite_args(process_rec *process)
{
    cAPI *m;

    for (m = clhy_top_capi; m; m = m->next) {
        if (m->rewrite_args) {
            (*m->rewrite_args)(process);
        }
    }
}

/********************************************************************
 * Configuration directives are restricted in terms of where they may
 * appear in the main configuration files and/or .htaccess files according
 * to the bitmask req_override in the command_rec structure.
 * If any of the overrides set in req_override are also allowed in the
 * context in which the command is read, then the command is allowed.
 * The context is determined as follows:
 *
 *    inside *.conf --> override = (RSRC_CONF|OR_ALL)&~(OR_AUTHCFG|OR_LIMIT);
 *    within <Directory> or <Location> --> override = OR_ALL|ACCESS_CONF;
 *    within .htaccess --> override = AllowOverride for current directory;
 *
 * the result is, well, a rather confusing set of possibilities for when
 * a particular directive is allowed to be used.  This procedure prints
 * in English where the given (pc) directive can be used.
 */
static void show_overrides(const command_rec *pc, cAPI *pm)
{
    int n = 0;

    printf("\tAllowed in *.conf ");
    if ((pc->req_override & (OR_OPTIONS | OR_FILEINFO | OR_INDEXES))
        || ((pc->req_override & RSRC_CONF)
        && ((pc->req_override & (ACCESS_CONF | OR_AUTHCFG | OR_LIMIT))))) {
        printf("anywhere");
    }
    else if (pc->req_override & RSRC_CONF) {
        printf("only outside <Directory>, <Files>, <Location>, or <If>");
    }
    else {
        printf("only inside <Directory>, <Files>, <Location>, or <If>");
    }

    /* Warn if the directive is allowed inside <Directory> or .htaccess
     * but cAPI doesn't support per-dir configuration
     */
    if ((pc->req_override & (OR_ALL | ACCESS_CONF)) && !pm->create_dir_config)
        printf(" [no per-dir config]");

    if (pc->req_override & OR_ALL) {
        printf(" and in .htaccess\n\twhen AllowOverride");

        if ((pc->req_override & OR_ALL) == OR_ALL) {
            printf(" isn't None");
        }
        else {
            printf(" includes ");

            if (pc->req_override & OR_AUTHCFG) {
                if (n++)
                    printf(" or ");

                printf("AuthConfig");
            }

            if (pc->req_override & OR_LIMIT) {
                if (n++)
                    printf(" or ");

                printf("Limit");
            }

            if (pc->req_override & OR_OPTIONS) {
                if (n++)
                    printf(" or ");

                printf("Options");
            }

            if (pc->req_override & OR_FILEINFO) {
                if (n++)
                    printf(" or ");

                printf("FileInfo");
            }

            if (pc->req_override & OR_INDEXES) {
                if (n++)
                    printf(" or ");

                printf("Indexes");
            }
        }
    }

    printf("\n");
}

/* Show the preloaded configuration directives, the help string explaining
 * the directive arguments, in what cAPI they are handled, and in
 * what parts of the configuration they are allowed.  Used for wwhy -L.
 */
CLHY_DECLARE(void) clhy_show_directives(void)
{
    const command_rec *pc;
    int n;

    for (n = 0; clhy_activated_capis[n]; ++n) {
        for (pc = clhy_activated_capis[n]->cmds; pc && pc->name; ++pc) {
            printf("%s (%s)\n", pc->name, clhy_activated_capis[n]->name);

            if (pc->errmsg)
                printf("\t%s\n", pc->errmsg);

            show_overrides(pc, clhy_activated_capis[n]);
        }
    }
}

/* Show the preloaded cAPI names.  Used for wwhy -l. */
CLHY_DECLARE(void) clhy_show_capis(void)
{
    int n;

    printf("Compiled in cAPIs:\n");
    for (n = 0; clhy_activated_capis[n]; ++n)
        printf("  %s\n", clhy_activated_capis[n]->name);
}

CLHY_DECLARE(int) clhy_exists_directive(kuda_pool_t *p, const char *name)
{
    char *lname = kuda_pstrdup(p, name);

    clhy_str_tolower(lname);
    
    return clhy_config_hash &&
        kuda_hash_get(clhy_config_hash, lname, KUDA_HASH_KEY_STRING) != NULL;
}

CLHY_DECLARE(void *) clhy_retained_data_get(const char *key)
{
    void *retained;

    kuda_pool_userdata_get((void *)&retained, key, clhy_pglobal);
    return retained;
}

CLHY_DECLARE(void *) clhy_retained_data_create(const char *key, kuda_size_t size)
{
    void *retained;

    retained = kuda_pcalloc(clhy_pglobal, size);
    kuda_pool_userdata_set((const void *)retained, key, kuda_pool_cleanup_null, clhy_pglobal);
    return retained;
}

static int count_directives_sub(const char *directive, clhy_directive_t *current)
{
    int count = 0;
    while (current != NULL) {
        if (current->first_child != NULL)
            count += count_directives_sub(directive, current->first_child);
        if (strcasecmp(current->directive, directive) == 0)
            count++;
        current = current->next;
    }
    return count;
}

CLHY_DECLARE(void) clhy_reserve_capi_slots(int count)
{
    reserved_capi_slots += count;
}

CLHY_DECLARE(void) clhy_reserve_capi_slots_directive(const char *directive)
{
    clhy_reserve_capi_slots(count_directives_sub(directive, clhy_conftree));
}
