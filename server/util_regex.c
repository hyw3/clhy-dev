/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_lib.h"
#include "kuda_pools.h"
#include "kuda_strings.h"
#include "clhy_config.h"
#include "clhy_regex.h"
#include "wwhy.h"

static kuda_status_t rxplus_cleanup(void *preg)
{
    clhy_regfree((clhy_regex_t *) preg);
    return KUDA_SUCCESS;
}

CLHY_DECLARE(clhy_rxplus_t*) clhy_rxplus_compile(kuda_pool_t *pool,
                                           const char *pattern)
{
    /* perl style patterns
     * add support for more as and when wanted
     * substitute: s/rx/subs/
     * match: m/rx/ or just /rx/
     */

    /* allow any nonalnum delimiter as first or second char.
     * If we ever use this with non-string pattern we'll need an extra check
     */
    const char *endp = 0;
    const char *str = pattern;
    const char *rxstr;
    clhy_rxplus_t *ret = kuda_pcalloc(pool, sizeof(clhy_rxplus_t));
    char delim = 0;
    enum { SUBSTITUTE = 's', MATCH = 'm'} action = MATCH;

    if (!kuda_isalnum(pattern[0])) {
        delim = *str++;
    }
    else if (pattern[0] == 's' && !kuda_isalnum(pattern[1])) {
        action = SUBSTITUTE;
        delim = pattern[1];
        str += 2;
    }
    else if (pattern[0] == 'm' && !kuda_isalnum(pattern[1])) {
        delim = pattern[1];
        str += 2;
    }
    /* TODO: support perl's after/before */
    /* FIXME: fix these simplminded delims */

    /* we think there's a delimiter.  Allow for it not to be if unmatched */
    if (delim) {
        endp = clhy_strchr_c(str, delim);
    }
    if (!endp) { /* there's no delim or flags */
        if (clhy_regcomp(&ret->rx, pattern, 0) == 0) {
            kuda_pool_cleanup_register(pool, &ret->rx, rxplus_cleanup,
                                      kuda_pool_cleanup_null);
            return ret;
        }
        else {
            return NULL;
        }
    }

    /* We have a delimiter.  Use it to extract the regexp */
    rxstr = kuda_pstrmemdup(pool, str, endp-str);

    /* If it's a substitution, we need the replacement string
     * TODO: possible future enhancement - support other parsing
     * in the replacement string.
     */
    if (action == SUBSTITUTE) {
        str = endp+1;
        if (!*str || (endp = clhy_strchr_c(str, delim), !endp)) {
            /* missing replacement string is an error */
            return NULL;
        }
        ret->subs = kuda_pstrmemdup(pool, str, endp-str);
    }

    /* anything after the current delimiter is flags */
    while (*++endp) {
        switch (*endp) {
        case 'i': ret->flags |= CLHY_REG_ICASE; break;
        case 'm': ret->flags |= CLHY_REG_NEWLINE; break;
        case 'n': ret->flags |= CLHY_REG_NOMEM; break;
        case 'g': ret->flags |= CLHY_REG_MULTI; break;
        case 's': ret->flags |= CLHY_REG_DOTALL; break;
        case '^': ret->flags |= CLHY_REG_NOTBOL; break;
        case '$': ret->flags |= CLHY_REG_NOTEOL; break;
        default: break; /* we should probably be stricter here */
        }
    }
    if (clhy_regcomp(&ret->rx, rxstr, ret->flags) == 0) {
        kuda_pool_cleanup_register(pool, &ret->rx, rxplus_cleanup,
                                  kuda_pool_cleanup_null);
    }
    else {
        return NULL;
    }
    if (!(ret->flags & CLHY_REG_NOMEM)) {
        /* count size of memory required, starting at 1 for the whole-match
         * Simpleminded should be fine 'cos regcomp already checked syntax
         */
        ret->nmatch = 1;
        while (*rxstr) {
            switch (*rxstr++) {
            case '\\':  /* next char is escaped - skip it */
                if (*rxstr != 0) {
                    ++rxstr;
                }
                break;
            case '(':   /* unescaped bracket implies memory */
                ++ret->nmatch;
                break;
            default:
                break;
            }
        }
        ret->pmatch = kuda_palloc(pool, ret->nmatch*sizeof(clhy_regmatch_t));
    }
    return ret;
}

CLHY_DECLARE(int) clhy_rxplus_exec(kuda_pool_t *pool, clhy_rxplus_t *rx,
                               const char *pattern, char **newpattern)
{
    int ret = 1;
    int startl, oldl, newl, diffsz;
    const char *remainder;
    char *subs;
/* snrf process_regexp from capi_headers */
    if (clhy_regexec(&rx->rx, pattern, rx->nmatch, rx->pmatch, rx->flags) != 0) {
        rx->match = NULL;
        return 0; /* no match, nothing to do */
    }
    rx->match = pattern;
    if (rx->subs) {
        *newpattern = clhy_pregsub(pool, rx->subs, pattern,
                                 rx->nmatch, rx->pmatch);
        if (!*newpattern) {
            return 0; /* FIXME - should we do more to handle error? */
        }
        startl = rx->pmatch[0].rm_so;
        oldl = rx->pmatch[0].rm_eo - startl;
        newl = strlen(*newpattern);
        diffsz = newl - oldl;
        remainder = pattern + startl + oldl;
        if (rx->flags & CLHY_REG_MULTI) {
            /* recurse to do any further matches */
            ret += clhy_rxplus_exec(pool, rx, remainder, &subs);
            if (ret > 1) {
                /* a further substitution happened */
                diffsz += strlen(subs) - strlen(remainder);
                remainder = subs;
            }
        }
        subs  = kuda_palloc(pool, strlen(pattern) + 1 + diffsz);
        memcpy(subs, pattern, startl);
        memcpy(subs+startl, *newpattern, newl);
        strcpy(subs+startl+newl, remainder);
        *newpattern = subs;
    }
    return ret;
}
#ifdef DOXYGEN
CLHY_DECLARE(int) clhy_rxplus_nmatch(clhy_rxplus_t *rx)
{
    return (rx->match != NULL) ? rx->nmatch : 0;
}
#endif

/* If this blows up on you, see the notes in the header/apidoc
 * rx->match is a pointer and it's your responsibility to ensure
 * it hasn't gone out-of-scope since the last clhy_rxplus_exec
 */
CLHY_DECLARE(void) clhy_rxplus_match(clhy_rxplus_t *rx, int n, int *len,
                                 const char **match)
{
    if (n >= 0 && n < clhy_rxplus_nmatch(rx)) {
        *match = rx->match + rx->pmatch[n].rm_so;
        *len = rx->pmatch[n].rm_eo - rx->pmatch[n].rm_so;
    }
    else {
        *len = -1;
        *match = NULL;
    }
}
CLHY_DECLARE(char*) clhy_rxplus_pmatch(kuda_pool_t *pool, clhy_rxplus_t *rx, int n)
{
    int len;
    const char *match;
    clhy_rxplus_match(rx, n, &len, &match);
    return kuda_pstrndup(pool, match, len);
}
