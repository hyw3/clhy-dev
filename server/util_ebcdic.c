/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "clhy_config.h"

#if KUDA_CHARSET_EBCDIC

#include "kuda_strings.h"
#include "wwhy.h"
#include "http_log.h"
#include "http_core.h"
#include "util_ebcdic.h"

/* we know core's capi_index is 0 */
#undef CLHYLOG_CAPI_INDEX
#define CLHYLOG_CAPI_INDEX CLHY_CORE_CAPI_INDEX

kuda_status_t clhy_init_ebcdic(kuda_pool_t *pool)
{
    kuda_status_t rv;

    rv = kuda_xlate_open(&clhy_hdrs_to_ascii, "ISO-8859-1", KUDA_DEFAULT_CHARSET, pool);
    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, NULL, CLHYLOGNO(00040)
                     "kuda_xlate_open() failed");
        return rv;
    }

    rv = kuda_xlate_open(&clhy_hdrs_from_ascii, KUDA_DEFAULT_CHARSET, "ISO-8859-1", pool);
    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, NULL, CLHYLOGNO(00041)
                     "kuda_xlate_open() failed");
        return rv;
    }

    rv = kuda_MD5InitEBCDIC(clhy_hdrs_to_ascii);
    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, NULL, CLHYLOGNO(00042)
                     "kuda_MD5InitEBCDIC() failed");
        return rv;
    }

    rv = kuda_base64init_ebcdic(clhy_hdrs_to_ascii, clhy_hdrs_from_ascii);
    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, NULL, CLHYLOGNO(00043)
                     "kuda_base64init_ebcdic() failed");
        return rv;
    }

    rv = kuda_SHA1InitEBCDIC(clhy_hdrs_to_ascii);
    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, NULL, CLHYLOGNO(00044)
                     "kuda_SHA1InitEBCDIC() failed");
        return rv;
    }

    return KUDA_SUCCESS;
}

void clhy_xlate_proto_to_ascii(char *buffer, kuda_size_t len)
{
    kuda_size_t inbytes_left, outbytes_left;

    inbytes_left = outbytes_left = len;
    kuda_xlate_conv_buffer(clhy_hdrs_to_ascii, buffer, &inbytes_left,
                          buffer, &outbytes_left);
}

void clhy_xlate_proto_from_ascii(char *buffer, kuda_size_t len)
{
    kuda_size_t inbytes_left, outbytes_left;

    inbytes_left = outbytes_left = len;
    kuda_xlate_conv_buffer(clhy_hdrs_from_ascii, buffer, &inbytes_left,
                          buffer, &outbytes_left);
}

int clhy_rvputs_proto_in_ascii(request_rec *r, ...)
{
    va_list va;
    const char *s;
    char *ascii_s;
    kuda_size_t len;
    kuda_size_t written = 0;

    va_start(va, r);
    while (1) {
        s = va_arg(va, const char *);
        if (s == NULL)
            break;
        len = strlen(s);
        ascii_s = kuda_pstrmemdup(r->pool, s, len);
        clhy_xlate_proto_to_ascii(ascii_s, len);
        if (clhy_rputs(ascii_s, r) < 0) {
            va_end(va);
            return -1;
        }
        written += len;
    }
    va_end(va);

    return written;
}
#endif /* KUDA_CHARSET_EBCDIC */
