/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "util_cfgtree.h"
#include <stdlib.h>

clhy_directive_t *clhy_add_node(clhy_directive_t **parent, clhy_directive_t *current,
                            clhy_directive_t *toadd, int child)
{
    if (current == NULL) {
        /* we just started a new parent */
        if (*parent != NULL) {
            (*parent)->first_child = toadd;
            toadd->parent = *parent;
        }
        if (child) {
            /* First item in config file or container is a container */
            *parent = toadd;
            return NULL;
        }
        return toadd;
    }
    current->next = toadd;
    toadd->parent = *parent;
    if (child) {
        /* switch parents, navigate into child */
        *parent = toadd;
        return NULL;
    }
    return toadd;
}


