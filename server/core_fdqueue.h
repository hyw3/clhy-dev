/* The cLHy Server
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  server/core_fdqueue.h
 * @brief fd queue declarations
 *
 * @addtogroup CLHYKUDEL_CLMP_EVENT
 * @{
 */

#ifndef CLMP_FDQUEUE_H
#define CLMP_FDQUEUE_H

#include <kuda.h>

/* This code is not CLHY_DECLARE()ed/exported, and used by CLMPs event/worker
 * only (for now), not worth thinking about w/o threads either...
 */
#if KUDA_HAS_THREADS

#include "clhy_core.h"

#include <kuda_ring.h>
#include <kuda_pools.h>
#include <kuda_thread_mutex.h>
#include <kuda_thread_cond.h>
#include <kuda_network_io.h>

struct fd_queue_info_t; /* opaque */
struct fd_queue_elem_t; /* opaque */
typedef struct fd_queue_info_t fd_queue_info_t;
typedef struct fd_queue_elem_t fd_queue_elem_t;

CLHY_DECLARE(kuda_status_t) clhy_queue_info_create(fd_queue_info_t **queue_info,
                                              kuda_pool_t *pool, int max_idlers,
                                              int max_recycled_pools);
CLHY_DECLARE(kuda_status_t) clhy_queue_info_set_idle(fd_queue_info_t *queue_info,
                                                kuda_pool_t *pool_to_recycle);
CLHY_DECLARE(kuda_status_t) clhy_queue_info_try_get_idler(fd_queue_info_t *queue_info);
CLHY_DECLARE(kuda_status_t) clhy_queue_info_wait_for_idler(fd_queue_info_t *queue_info,
                                                      int *had_to_block);
CLHY_DECLARE(kuda_uint32_t) clhy_queue_info_num_idlers(fd_queue_info_t *queue_info);
CLHY_DECLARE(kuda_status_t) clhy_queue_info_term(fd_queue_info_t *queue_info);

CLHY_DECLARE(void) clhy_queue_info_pop_pool(fd_queue_info_t *queue_info,
                                        kuda_pool_t **recycled_pool);
CLHY_DECLARE(void) clhy_queue_info_push_pool(fd_queue_info_t *queue_info,
                                         kuda_pool_t *pool_to_recycle);
CLHY_DECLARE(void) clhy_queue_info_free_idle_pools(fd_queue_info_t *queue_info);

struct timer_event_t
{
    KUDA_RING_ENTRY(timer_event_t) link;
    kuda_time_t when;
    clhy_clmp_callback_fn_t *cbfunc;
    void *baton;
    int canceled;
    kuda_array_header_t *remove;
};
typedef struct timer_event_t timer_event_t;

struct fd_queue_t
{
    KUDA_RING_HEAD(timers_t, timer_event_t) timers;
    fd_queue_elem_t *data;
    unsigned int nelts;
    unsigned int bounds;
    unsigned int in;
    unsigned int out;
    kuda_thread_mutex_t *one_big_mutex;
    kuda_thread_cond_t *not_empty;
    int terminated;
};
typedef struct fd_queue_t fd_queue_t;

CLHY_DECLARE(kuda_status_t) clhy_queue_create(fd_queue_t **pqueue,
                                         int capacity, kuda_pool_t *p);
CLHY_DECLARE(kuda_status_t) clhy_queue_push_socket(fd_queue_t *queue,
                                              kuda_socket_t *sd, void *sd_baton,
                                              kuda_pool_t *p);
CLHY_DECLARE(kuda_status_t) clhy_queue_push_timer(fd_queue_t *queue,
                                             timer_event_t *te);
CLHY_DECLARE(kuda_status_t) clhy_queue_pop_something(fd_queue_t *queue,
                                                kuda_socket_t **sd, void **sd_baton,
                                                kuda_pool_t **p, timer_event_t **te);
#define                  clhy_queue_pop_socket(q_, s_, p_) \
                            clhy_queue_pop_something((q_), (s_), NULL, (p_), NULL)

CLHY_DECLARE(kuda_status_t) clhy_queue_interrupt_all(fd_queue_t *queue);
CLHY_DECLARE(kuda_status_t) clhy_queue_interrupt_one(fd_queue_t *queue);
CLHY_DECLARE(kuda_status_t) clhy_queue_term(fd_queue_t *queue);

#endif /* KUDA_HAS_THREADS */

#endif /* CLMP_FDQUEUE_H */
/** @} */
