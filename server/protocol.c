/* The cLHy Server
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * protocol.c --- routines which directly communicate with the client.
 *
 * Code originally by Rob McCool; much redone by Robert S. Thau
 * and the Hyang Language Foundation.
 */

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_buckets.h"
#include "kuda_lib.h"
#include "kuda_signal.h"
#include "kuda_strmatch.h"

#define KUDA_WANT_STDIO          /* for sscanf */
#define KUDA_WANT_STRFUNC
#define KUDA_WANT_MEMFUNC
#include "kuda_want.h"

#include "util_filter.h"
#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_protocol.h"
#include "http_main.h"
#include "http_request.h"
#include "http_vhost.h"
#include "http_log.h"           /* For errors detected in basic auth common
                                 * support code... */
#include "capi_core.h"
#include "util_charset.h"
#include "util_ebcdic.h"
#include "scoreboard.h"

#if KUDA_HAVE_STDARG_H
#include <stdarg.h>
#endif
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif

/* we know core's capi_index is 0 */
#undef CLHYLOG_CAPI_INDEX
#define CLHYLOG_CAPI_INDEX CLHY_CORE_CAPI_INDEX

KUDA_HOOK_STRUCT(
    KUDA_HOOK_LINK(pre_read_request)
    KUDA_HOOK_LINK(post_read_request)
    KUDA_HOOK_LINK(log_transaction)
    KUDA_HOOK_LINK(http_scheme)
    KUDA_HOOK_LINK(default_port)
    KUDA_HOOK_LINK(note_auth_failure)
    KUDA_HOOK_LINK(protocol_propose)
    KUDA_HOOK_LINK(protocol_switch)
    KUDA_HOOK_LINK(protocol_get)
)

CLHY_DECLARE_DATA clhy_filter_rec_t *clhy_old_write_func = NULL;


/* Patterns to match in clhy_make_content_type() */
static const char *needcset[] = {
    "text/plain",
    "text/html",
    NULL
};
static const kuda_strmatch_pattern **needcset_patterns;
static const kuda_strmatch_pattern *charset_pattern;

CLHY_DECLARE(void) clhy_setup_make_content_type(kuda_pool_t *pool)
{
    int i;
    for (i = 0; needcset[i]; i++) {
        continue;
    }
    needcset_patterns = (const kuda_strmatch_pattern **)
        kuda_palloc(pool, (i + 1) * sizeof(kuda_strmatch_pattern *));
    for (i = 0; needcset[i]; i++) {
        needcset_patterns[i] = kuda_strmatch_precompile(pool, needcset[i], 0);
    }
    needcset_patterns[i] = NULL;
    charset_pattern = kuda_strmatch_precompile(pool, "charset=", 0);
}

/*
 * Builds the content-type that should be sent to the client from the
 * content-type specified.  The following rules are followed:
 *    - if type is NULL or "", return NULL (do not set content-type).
 *    - if charset adding is disabled, stop processing and return type.
 *    - then, if there are no parameters on type, add the default charset
 *    - return type
 */
CLHY_DECLARE(const char *)clhy_make_content_type(request_rec *r, const char *type)
{
    const kuda_strmatch_pattern **pcset;
    core_dir_config *conf =
        (core_dir_config *)clhy_get_core_capi_config(r->per_dir_config);
    core_request_config *request_conf;
    kuda_size_t type_len;

    if (!type || *type == '\0') {
        return NULL;
    }

    if (conf->add_default_charset != ADD_DEFAULT_CHARSET_ON) {
        return type;
    }

    request_conf = clhy_get_core_capi_config(r->request_config);
    if (request_conf->suppress_charset) {
        return type;
    }

    type_len = strlen(type);

    if (kuda_strmatch(charset_pattern, type, type_len) != NULL) {
        /* already has parameter, do nothing */
        /* XXX we don't check the validity */
        ;
    }
    else {
        /* see if it makes sense to add the charset. At present,
         * we only add it if the Content-type is one of needcset[]
         */
        for (pcset = needcset_patterns; *pcset ; pcset++) {
            if (kuda_strmatch(*pcset, type, type_len) != NULL) {
                struct iovec concat[3];
                concat[0].iov_base = (void *)type;
                concat[0].iov_len = type_len;
                concat[1].iov_base = (void *)"; charset=";
                concat[1].iov_len = sizeof("; charset=") - 1;
                concat[2].iov_base = (void *)(conf->add_default_charset_name);
                concat[2].iov_len = strlen(conf->add_default_charset_name);
                type = kuda_pstrcatv(r->pool, concat, 3, NULL);
                break;
            }
        }
    }

    return type;
}

CLHY_DECLARE(void) clhy_set_content_length(request_rec *r, kuda_off_t clength)
{
    r->clength = clength;
    kuda_table_setn(r->headers_out, "Content-Length",
                   kuda_off_t_toa(r->pool, clength));
}

/*
 * Return the latest rational time from a request/mtime (modification time)
 * pair.  We return the mtime unless it's in the future, in which case we
 * return the current time.  We use the request time as a reference in order
 * to limit the number of calls to time().  We don't check for futurosity
 * unless the mtime is at least as new as the reference.
 */
CLHY_DECLARE(kuda_time_t) clhy_rationalize_mtime(request_rec *r, kuda_time_t mtime)
{
    kuda_time_t now;

    /* For all static responses, it's almost certain that the file was
     * last modified before the beginning of the request.  So there's
     * no reason to call time(NULL) again.  But if the response has been
     * created on demand, then it might be newer than the time the request
     * started.  In this event we really have to call time(NULL) again
     * so that we can give the clients the most accurate Last-Modified.  If we
     * were given a time in the future, we return the current time - the
     * Last-Modified can't be in the future.
     */
    now = (mtime < r->request_time) ? r->request_time : kuda_time_now();
    return (mtime > now) ? now : mtime;
}

/* Get a line of protocol input, including any continuation lines
 * caused by MIME folding (or broken clients) if fold != 0, and place it
 * in the buffer s, of size n bytes, without the ending newline.
 * 
 * Pulls from r->proto_input_filters instead of r->input_filters for
 * stricter protocol adherence and better input filter behavior during
 * chunked trailer processing (for http).
 *
 * If s is NULL, clhy_rgetline_core will allocate necessary memory from r->pool.
 *
 * Returns KUDA_SUCCESS if there are no problems and sets *read to be
 * the full length of s.
 *
 * KUDA_ENOSPC is returned if there is not enough buffer space.
 * Other errors may be returned on other errors.
 *
 * The [CR]LF are *not* returned in the buffer.  Therefore, a *read of 0
 * indicates that an empty line was read.
 *
 * Notes: Because the buffer uses 1 char for NUL, the most we can return is
 *        (n - 1) actual characters.
 *
 *        If no LF is detected on the last line due to a dropped connection
 *        or a full buffer, that's considered an error.
 */
CLHY_DECLARE(kuda_status_t) clhy_rgetline_core(char **s, kuda_size_t n,
                                          kuda_size_t *read, request_rec *r,
                                          int flags, kuda_bucket_brigade *bb)
{
    kuda_status_t rv;
    kuda_bucket *e;
    kuda_size_t bytes_handled = 0, current_alloc = 0;
    char *pos, *last_char = *s;
    int do_alloc = (*s == NULL), saw_eos = 0;
    int fold = flags & CLHY_GETLINE_FOLD;
    int crlf = flags & CLHY_GETLINE_CRLF;
    int nospc_eol = flags & CLHY_GETLINE_NOSPC_EOL;
    int saw_eol = 0, saw_nospc = 0;

    if (!n) {
        /* Needs room for NUL byte at least */
        *read = 0;
        return KUDA_BADARG;
    }

    /*
     * Initialize last_char as otherwise a random value will be compared
     * against KUDA_ASCII_LF at the end of the loop if bb only contains
     * zero-length buckets.
     */
    if (last_char)
        *last_char = '\0';

    do {
        kuda_brigade_cleanup(bb);
        rv = clhy_get_brigade(r->proto_input_filters, bb, CLHY_MODE_GETLINE,
                            KUDA_BLOCK_READ, 0);
        if (rv != KUDA_SUCCESS) {
            goto cleanup;
        }

        /* Something horribly wrong happened.  Someone didn't block! 
         * (this also happens at the end of each keepalive connection)
         */
        if (KUDA_BRIGADE_EMPTY(bb)) {
            rv = KUDA_EGENERAL;
            goto cleanup;
        }

        for (e = KUDA_BRIGADE_FIRST(bb);
             e != KUDA_BRIGADE_SENTINEL(bb);
             e = KUDA_BUCKET_NEXT(e))
        {
            const char *str;
            kuda_size_t len;

            /* If we see an EOS, don't bother doing anything more. */
            if (KUDA_BUCKET_IS_EOS(e)) {
                saw_eos = 1;
                break;
            }

            rv = kuda_bucket_read(e, &str, &len, KUDA_BLOCK_READ);
            if (rv != KUDA_SUCCESS) {
                goto cleanup;
            }

            if (len == 0) {
                /* no use attempting a zero-byte alloc (hurts when
                 * using --with-efence --enable-pool-debug) or
                 * doing any of the other logic either
                 */
                continue;
            }

            /* Would this overrun our buffer?  If so, we'll die. */
            if (n < bytes_handled + len) {
                /* Before we die, let's fill the buffer up to its limit (i.e.
                 * fall through with the remaining length, if any), setting
                 * saw_eol on LF to stop the outer loop appropriately; we may
                 * come back here once the buffer is filled (no LF seen), and
                 * either be done at that time or continue to wait for LF here
                 * if nospc_eol is set.
                 *
                 * But there is also a corner cases which we want to address,
                 * namely if the buffer is overrun by the final LF only (i.e.
                 * the CR fits in); this is not really an overrun since we'll
                 * strip the CR finally (and use it for NUL byte), but anyway
                 * we have to handle the case so that it's not returned to the
                 * caller as part of the truncated line (it's not!). This is
                 * easier to consider that LF is out of counting and thus fall
                 * through with no error (saw_eol is set to 2 so that we later
                 * ignore LF handling already done here), while folding and
                 * nospc_eol logics continue to work (or fail) appropriately.
                 */
                saw_eol = (str[len - 1] == KUDA_ASCII_LF);
                if (/* First time around */
                    saw_eol && !saw_nospc
                    /*  Single LF completing the buffered CR, */
                    && ((len == 1 && ((*s)[bytes_handled - 1] == KUDA_ASCII_CR))
                    /*  or trailing CRLF overuns by LF only */
                        || (len > 1 && str[len - 2] == KUDA_ASCII_CR
                            && n - bytes_handled + 1 == len))) {
                    /* In both cases *last_char is (to be) the CR stripped by
                     * later 'bytes_handled = last_char - *s'.
                     */
                    saw_eol = 2;
                }
                else {
                    /* In any other case we'd lose data. */
                    rv = KUDA_ENOSPC;
                    saw_nospc = 1;
                }
                len = n - bytes_handled;
                if (!len) {
                    if (saw_eol) {
                        break;
                    }
                    if (nospc_eol) {
                        continue;
                    }
                    goto cleanup;
                }
            }

            /* Do we have to handle the allocation ourselves? */
            if (do_alloc) {
                /* We'll assume the common case where one bucket is enough. */
                if (!*s) {
                    current_alloc = len;
                    *s = kuda_palloc(r->pool, current_alloc + 1);
                }
                else if (bytes_handled + len > current_alloc) {
                    /* Increase the buffer size */
                    kuda_size_t new_size = current_alloc * 2;
                    char *new_buffer;

                    if (bytes_handled + len > new_size) {
                        new_size = (bytes_handled + len) * 2;
                    }

                    new_buffer = kuda_palloc(r->pool, new_size + 1);

                    /* Copy what we already had. */
                    memcpy(new_buffer, *s, bytes_handled);
                    current_alloc = new_size;
                    *s = new_buffer;
                }
            }

            /* Just copy the rest of the data to the end of the old buffer. */
            pos = *s + bytes_handled;
            memcpy(pos, str, len);
            last_char = pos + len - 1;

            /* We've now processed that new data - update accordingly. */
            bytes_handled += len;
        }

        /* If we got a full line of input, stop reading */
        if (last_char && (*last_char == KUDA_ASCII_LF)) {
            saw_eol = 1;
        }
    } while (!saw_eol);

    if (rv != KUDA_SUCCESS) {
        /* End of line after KUDA_ENOSPC above */
        goto cleanup;
    }

    /* Now terminate the string at the end of the line;
     * if the last-but-one character is a CR, terminate there.
     * LF is handled above (not accounted) when saw_eol == 2,
     * the last char is CR to terminate at still.
     */
    if (saw_eol < 2) {
        if (last_char > *s && last_char[-1] == KUDA_ASCII_CR) {
            last_char--;
        }
        else if (crlf) {
            rv = KUDA_EINVAL;
            goto cleanup;
        }
    }
    bytes_handled = last_char - *s;

    /* If we're folding, we have more work to do.
     *
     * Note that if an EOS was seen, we know we can't have another line.
     */
    if (fold && bytes_handled && !saw_eos) {
        for (;;) {
            const char *str;
            kuda_size_t len;
            char c;

            /* Clear the temp brigade for this filter read. */
            kuda_brigade_cleanup(bb);

            /* We only care about the first byte. */
            rv = clhy_get_brigade(r->proto_input_filters, bb, CLHY_MODE_SPECULATIVE,
                                KUDA_BLOCK_READ, 1);
            if (rv != KUDA_SUCCESS) {
                goto cleanup;
            }

            if (KUDA_BRIGADE_EMPTY(bb)) {
                break;
            }

            e = KUDA_BRIGADE_FIRST(bb);

            /* If we see an EOS, don't bother doing anything more. */
            if (KUDA_BUCKET_IS_EOS(e)) {
                break;
            }

            rv = kuda_bucket_read(e, &str, &len, KUDA_BLOCK_READ);
            if (rv != KUDA_SUCCESS) {
                kuda_brigade_cleanup(bb);
                goto cleanup;
            }

            /* Found one, so call ourselves again to get the next line.
             *
             * FIXME: If the folding line is completely blank, should we
             * stop folding?  Does that require also looking at the next
             * char?
             */
            /* When we call destroy, the buckets are deleted, so save that
             * one character we need.  This simplifies our execution paths
             * at the cost of one character read.
             */
            c = *str;
            if (c == KUDA_ASCII_BLANK || c == KUDA_ASCII_TAB) {
                /* Do we have enough space? We may be full now. */
                if (bytes_handled >= n) {
                    rv = KUDA_ENOSPC;
                    goto cleanup;
                }
                else {
                    kuda_size_t next_size, next_len;
                    char *tmp;

                    /* If we're doing the allocations for them, we have to
                     * give ourselves a NULL and copy it on return.
                     */
                    if (do_alloc) {
                        tmp = NULL;
                    }
                    else {
                        tmp = last_char;
                    }

                    next_size = n - bytes_handled;

                    rv = clhy_rgetline_core(&tmp, next_size, &next_len, r,
                                          flags & ~CLHY_GETLINE_FOLD, bb);
                    if (rv != KUDA_SUCCESS) {
                        goto cleanup;
                    }

                    if (do_alloc && next_len > 0) {
                        char *new_buffer;
                        kuda_size_t new_size = bytes_handled + next_len + 1;

                        /* we need to alloc an extra byte for a null */
                        new_buffer = kuda_palloc(r->pool, new_size);

                        /* Copy what we already had. */
                        memcpy(new_buffer, *s, bytes_handled);

                        /* copy the new line, including the trailing null */
                        memcpy(new_buffer + bytes_handled, tmp, next_len);
                        *s = new_buffer;
                    }

                    last_char += next_len;
                    bytes_handled += next_len;
                }
            }
            else { /* next character is not tab or space */
                break;
            }
        }
    }

cleanup:
    if (bytes_handled >= n) {
        bytes_handled = n - 1;
    }

    *read = bytes_handled;
    if (*s) {
        /* ensure the string is NUL terminated */
        (*s)[*read] = '\0';

        /* PR#43039: We shouldn't accept NULL bytes within the line */
        bytes_handled = strlen(*s);
        if (bytes_handled < *read) {
            *read = bytes_handled;
            if (rv == KUDA_SUCCESS) {
                rv = KUDA_EINVAL;
            }
        }
    }
    return rv;
}

#if KUDA_CHARSET_EBCDIC
CLHY_DECLARE(kuda_status_t) clhy_rgetline(char **s, kuda_size_t n,
                                     kuda_size_t *read, request_rec *r,
                                     int fold, kuda_bucket_brigade *bb)
{
    /* on ASCII boxes, clhy_rgetline is a macro which simply invokes
     * clhy_rgetline_core with the same parms
     *
     * on EBCDIC boxes, each complete http protocol input line needs to be
     * translated into the code page used by the compiler.  Since
     * clhy_rgetline_core uses recursion, we do the translation in a wrapper
     * function to ensure that each input character gets translated only once.
     */
    kuda_status_t rv;

    rv = clhy_rgetline_core(s, n, read, r, fold, bb);
    if (rv == KUDA_SUCCESS || KUDA_STATUS_IS_ENOSPC(rv)) {
        clhy_xlate_proto_from_ascii(*s, *read);
    }
    return rv;
}
#endif

CLHY_DECLARE(int) clhy_getline(char *s, int n, request_rec *r, int flags)
{
    char *tmp_s = s;
    kuda_status_t rv;
    kuda_size_t len;
    kuda_bucket_brigade *tmp_bb;

    if (n < 1) {
        /* Can't work since we always NUL terminate */
        return -1;
    }

    tmp_bb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
    rv = clhy_rgetline(&tmp_s, n, &len, r, flags, tmp_bb);
    kuda_brigade_destroy(tmp_bb);

    /* Map the out-of-space condition to the old API. */
    if (rv == KUDA_ENOSPC) {
        return n;
    }

    /* Anything else is just bad. */
    if (rv != KUDA_SUCCESS) {
        return -1;
    }

    return (int)len;
}

/* parse_uri: break apart the uri
 * Side Effects:
 * - sets r->args to rest after '?' (or NULL if no '?')
 * - sets r->uri to request uri (without r->args part)
 * - sets r->hostname (if not set already) from request (scheme://host:port)
 */
CLHY_CORE_DECLARE(void) clhy_parse_uri(request_rec *r, const char *uri)
{
    int status = HTTP_OK;

    r->unparsed_uri = kuda_pstrdup(r->pool, uri);

    /* 
     * Note :
     *
     * This is not in fact a URI, it's a path.  That matters in the
     * case of a leading double-slash.  We need to resolve the issue
     * by normalizing that out before treating it as a URI.
     */
    while ((uri[0] == '/') && (uri[1] == '/')) {
        ++uri ;
    }
    if (r->method_number == M_CONNECT) {
        status = kuda_uri_parse_hostinfo(r->pool, uri, &r->parsed_uri);
    }
    else {
        status = kuda_uri_parse(r->pool, uri, &r->parsed_uri);
    }

    if (status == KUDA_SUCCESS) {
        /* if it has a scheme we may need to do absoluteURI vhost stuff */
        if (r->parsed_uri.scheme
            && !strcasecmp(r->parsed_uri.scheme, clhy_http_scheme(r))) {
            r->hostname = r->parsed_uri.hostname;
        }
        else if (r->method_number == M_CONNECT) {
            r->hostname = r->parsed_uri.hostname;
        }

        r->args = r->parsed_uri.query;
        r->uri = r->parsed_uri.path ? r->parsed_uri.path
                 : kuda_pstrdup(r->pool, "/");

#if defined(OS2) || defined(WIN32)
        /* Handle path translations for OS/2 and plug security hole.
         * This will prevent "http://www.wherever.com/..\..\/" from
         * returning a directory for the root drive.
         */
        {
            char *x;

            for (x = r->uri; (x = strchr(x, '\\')) != NULL; )
                *x = '/';
        }
#endif /* OS2 || WIN32 */
    }
    else {
        r->args = NULL;
        r->hostname = NULL;
        r->status = HTTP_BAD_REQUEST;             /* set error status */
        r->uri = kuda_pstrdup(r->pool, uri);
    }
}

/* get the length of the field name for logging, but no more than 80 bytes */
#define LOG_NAME_MAX_LEN 80
static int field_name_len(const char *field)
{
    const char *end = clhy_strchr_c(field, ':');
    if (end == NULL || end - field > LOG_NAME_MAX_LEN)
        return LOG_NAME_MAX_LEN;
    return end - field;
}

static int read_request_line(request_rec *r, kuda_bucket_brigade *bb)
{
    enum {
        rrl_none, rrl_badmethod, rrl_badwhitespace, rrl_excesswhitespace,
        rrl_missinguri, rrl_baduri, rrl_badprotocol, rrl_trailingtext,
        rrl_badmethod09, rrl_reject09
    } deferred_error = rrl_none;
    char *ll;
    char *uri;
    kuda_size_t len;
    int num_blank_lines = DEFAULT_LIMIT_BLANK_LINES;
    core_server_config *conf = clhy_get_core_capi_config(r->server->capi_config);
    int strict = (conf->http_conformance != CLHY_HTTP_CONFORMANCE_UNSAFE);

    /* Read past empty lines until we get a real request line,
     * a read error, the connection closes (EOF), or we timeout.
     *
     * We skip empty lines because browsers have to tack a CRLF on to the end
     * of POSTs to support old CERN webservers.  But note that we may not
     * have flushed any previous response completely to the client yet.
     * We delay the flush as long as possible so that we can improve
     * performance for clients that are pipelining requests.  If a request
     * is pipelined then we won't block during the (implicit) read() below.
     * If the requests aren't pipelined, then the client is still waiting
     * for the final buffer flush from us, and we will block in the implicit
     * read().  B_SAFEREAD ensures that the BUFF layer flushes if it will
     * have to block during a read.
     */

    do {
        kuda_status_t rv;

        /* ensure clhy_rgetline allocates memory each time thru the loop
         * if there are empty lines
         */
        r->the_request = NULL;
        rv = clhy_rgetline(&(r->the_request), (kuda_size_t)(r->server->limit_req_line + 2),
                         &len, r, strict ? CLHY_GETLINE_CRLF : 0, bb);

        if (rv != KUDA_SUCCESS) {
            r->request_time = kuda_time_now();

            /* clhy_rgetline returns KUDA_ENOSPC if it fills up the
             * buffer before finding the end-of-line.  This is only going to
             * happen if it exceeds the configured limit for a request-line.
             */
            if (KUDA_STATUS_IS_ENOSPC(rv)) {
                r->status = HTTP_REQUEST_URI_TOO_LARGE;
            }
            else if (KUDA_STATUS_IS_TIMEUP(rv)) {
                r->status = HTTP_REQUEST_TIME_OUT;
            }
            else if (KUDA_STATUS_IS_EINVAL(rv)) {
                r->status = HTTP_BAD_REQUEST;
            }
            r->proto_num = HTTP_VERSION(1,0);
            r->protocol  = kuda_pstrdup(r->pool, "HTTP/1.0");
            return 0;
        }
    } while ((len <= 0) && (--num_blank_lines >= 0));

    if (CLHYLOGrtrace5(r)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE5, 0, r,
                      "Request received from client: %s",
                      clhy_escape_logitem(r->pool, r->the_request));
    }

    r->request_time = kuda_time_now();

    r->method = r->the_request;

    /* If there is whitespace before a method, skip it and mark in error */
    if (kuda_isspace(*r->method)) {
        deferred_error = rrl_badwhitespace; 
        for ( ; kuda_isspace(*r->method); ++r->method)
            ; 
    }

    /* Scan the method up to the next whitespace, ensure it contains only
     * valid http-token characters, otherwise mark in error
     */
    if (strict) {
        ll = (char*) clhy_scan_http_token(r->method);
    }
    else {
        ll = (char*) clhy_scan_vchar_obstext(r->method);
    }

    if (((ll == r->method) || (*ll && !kuda_isspace(*ll)))
            && deferred_error == rrl_none) {
        deferred_error = rrl_badmethod;
        ll = strpbrk(ll, "\t\n\v\f\r ");
    }

    /* Verify method terminated with a single SP, or mark as specific error */
    if (!ll) {
        if (deferred_error == rrl_none)
            deferred_error = rrl_missinguri;
        r->protocol = uri = "";
        len = 0;
        goto rrl_done;
    }
    else if (strict && ll[0] && kuda_isspace(ll[1])
             && deferred_error == rrl_none) {
        deferred_error = rrl_excesswhitespace; 
    }

    /* Advance uri pointer over leading whitespace, NUL terminate the method
     * If non-SP whitespace is encountered, mark as specific error
     */
    for (uri = ll; kuda_isspace(*uri); ++uri) 
        if (*uri != ' ' && deferred_error == rrl_none)
            deferred_error = rrl_badwhitespace; 
    *ll = '\0';

    if (!*uri && deferred_error == rrl_none)
        deferred_error = rrl_missinguri;

    /* Scan the URI up to the next whitespace, ensure it contains no raw
     * control characters, otherwise mark in error
     */
    ll = (char*) clhy_scan_vchar_obstext(uri);
    if (ll == uri || (*ll && !kuda_isspace(*ll))) {
        deferred_error = rrl_baduri;
        ll = strpbrk(ll, "\t\n\v\f\r ");
    }

    /* Verify URI terminated with a single SP, or mark as specific error */
    if (!ll) {
        r->protocol = "";
        len = 0;
        goto rrl_done;
    }
    else if (strict && ll[0] && kuda_isspace(ll[1])
             && deferred_error == rrl_none) {
        deferred_error = rrl_excesswhitespace; 
    }

    /* Advance protocol pointer over leading whitespace, NUL terminate the uri
     * If non-SP whitespace is encountered, mark as specific error
     */
    for (r->protocol = ll; kuda_isspace(*r->protocol); ++r->protocol) 
        if (*r->protocol != ' ' && deferred_error == rrl_none)
            deferred_error = rrl_badwhitespace; 
    *ll = '\0';

    /* Scan the protocol up to the next whitespace, validation comes later */
    if (!(ll = (char*) clhy_scan_vchar_obstext(r->protocol))) {
        len = strlen(r->protocol);
        goto rrl_done;
    }
    len = ll - r->protocol;

    /* Advance over trailing whitespace, if found mark in error,
     * determine if trailing text is found, unconditionally mark in error,
     * finally NUL terminate the protocol string
     */
    if (*ll && !kuda_isspace(*ll)) {
        deferred_error = rrl_badprotocol;
    }
    else if (strict && *ll) {
        deferred_error = rrl_excesswhitespace;
    }
    else {
        for ( ; kuda_isspace(*ll); ++ll)
            if (*ll != ' ' && deferred_error == rrl_none)
                deferred_error = rrl_badwhitespace; 
        if (*ll && deferred_error == rrl_none)
            deferred_error = rrl_trailingtext;
    }
    *((char *)r->protocol + len) = '\0';

rrl_done:
    /* For internal integrity and palloc efficiency, reconstruct the_request
     * in one palloc, using only single SP characters, per spec.
     */
    r->the_request = kuda_pstrcat(r->pool, r->method, *uri ? " " : NULL, uri,
                                 *r->protocol ? " " : NULL, r->protocol, NULL);

    if (len == 8
            && r->protocol[0] == 'H' && r->protocol[1] == 'T'
            && r->protocol[2] == 'T' && r->protocol[3] == 'P'
            && r->protocol[4] == '/' && kuda_isdigit(r->protocol[5])
            && r->protocol[6] == '.' && kuda_isdigit(r->protocol[7])
            && r->protocol[5] != '0') {
        r->assbackwards = 0;
        r->proto_num = HTTP_VERSION(r->protocol[5] - '0', r->protocol[7] - '0');
    }
    else if (len == 8
                 && (r->protocol[0] == 'H' || r->protocol[0] == 'h')
                 && (r->protocol[1] == 'T' || r->protocol[1] == 't')
                 && (r->protocol[2] == 'T' || r->protocol[2] == 't')
                 && (r->protocol[3] == 'P' || r->protocol[3] == 'p')
                 && r->protocol[4] == '/' && kuda_isdigit(r->protocol[5])
                 && r->protocol[6] == '.' && kuda_isdigit(r->protocol[7])
                 && r->protocol[5] != '0') {
        r->assbackwards = 0;
        r->proto_num = HTTP_VERSION(r->protocol[5] - '0', r->protocol[7] - '0');
        if (strict && deferred_error == rrl_none)
            deferred_error = rrl_badprotocol;
        else
            memcpy((char*)r->protocol, "HTTP", 4);
    }
    else if (r->protocol[0]) {
        r->proto_num = HTTP_VERSION(0, 9);
        /* Defer setting the r->protocol string till error msg is composed */
        if (deferred_error == rrl_none)
            deferred_error = rrl_badprotocol;
    }
    else {
        r->assbackwards = 1;
        r->protocol  = kuda_pstrdup(r->pool, "HTTP/0.9");
        r->proto_num = HTTP_VERSION(0, 9);
    }

    /* Determine the method_number and parse the uri prior to invoking error
     * handling, such that these fields are available for substitution
     */
    r->method_number = clhy_method_number_of(r->method);
    if (r->method_number == M_GET && r->method[0] == 'H')
        r->header_only = 1;

    clhy_parse_uri(r, uri);

    /* With the request understood, we can consider HTTP/0.9 specific errors */
    if (r->proto_num == HTTP_VERSION(0, 9) && deferred_error == rrl_none) {
        if (conf->http09_enable == CLHY_HTTP09_DISABLE)
            deferred_error = rrl_reject09;
        else if (strict && (r->method_number != M_GET || r->header_only))
            deferred_error = rrl_badmethod09;
    }

    /* Now that the method, uri and protocol are all processed,
     * we can safely resume any deferred error reporting
     */
    if (deferred_error != rrl_none) {
        if (deferred_error == rrl_badmethod)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03445)
                          "HTTP Request Line; Invalid method token: '%.*s'",
                          field_name_len(r->method), r->method);
        else if (deferred_error == rrl_badmethod09)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03444)
                          "HTTP Request Line; Invalid method token: '%.*s'"
                          " (only GET is allowed for HTTP/0.9 requests)",
                          field_name_len(r->method), r->method);
        else if (deferred_error == rrl_missinguri)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03446)
                          "HTTP Request Line; Missing URI");
        else if (deferred_error == rrl_baduri)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03454)
                          "HTTP Request Line; URI incorrectly encoded: '%.*s'",
                          field_name_len(r->unparsed_uri), r->unparsed_uri);
        else if (deferred_error == rrl_badwhitespace)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03447)
                          "HTTP Request Line; Invalid whitespace");
        else if (deferred_error == rrl_excesswhitespace)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03448)
                          "HTTP Request Line; Excess whitespace "
                          "(disallowed by HttpProtocolOptions Strict");
        else if (deferred_error == rrl_trailingtext)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03449)
                          "HTTP Request Line; Extraneous text found '%.*s' "
                          "(perhaps whitespace was injected?)",
                          field_name_len(ll), ll);
        else if (deferred_error == rrl_reject09)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02401)
                          "HTTP Request Line; Rejected HTTP/0.9 request");
        else if (deferred_error == rrl_badprotocol)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02418)
                          "HTTP Request Line; Unrecognized protocol '%.*s' "
                          "(perhaps whitespace was injected?)",
                          field_name_len(r->protocol), r->protocol);
        r->status = HTTP_BAD_REQUEST;
        goto rrl_failed;
    }

    if (conf->http_methods == CLHY_HTTP_METHODS_REGISTERED
            && r->method_number == M_INVALID) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02423)
                      "HTTP Request Line; Unrecognized HTTP method: '%.*s' "
                      "(disallowed by RegisteredMethods)",
                      field_name_len(r->method), r->method);
        r->status = HTTP_NOT_IMPLEMENTED;
        /* This can't happen in an HTTP/0.9 request, we verified GET above */
        return 0;
    }

    if (r->status != HTTP_OK) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03450)
                      "HTTP Request Line; Unable to parse URI: '%.*s'",
                      field_name_len(r->uri), r->uri);
        goto rrl_failed;
    }

    if (strict) {
        if (r->parsed_uri.fragment) {
            /* RFC3986 3.5: no fragment */
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02421)
                          "HTTP Request Line; URI must not contain a fragment");
            r->status = HTTP_BAD_REQUEST;
            goto rrl_failed;
        }
        if (r->parsed_uri.user || r->parsed_uri.password) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02422)
                          "HTTP Request Line; URI must not contain a "
                          "username/password");
            r->status = HTTP_BAD_REQUEST;
            goto rrl_failed;
        }
    }

    return 1;

rrl_failed:
    if (r->proto_num == HTTP_VERSION(0, 9)) {
        /* Send all parsing and protocol error response with 1.x behavior,
         * and reserve 505 errors for actual HTTP protocols presented.
         * As called out in RFC7230 3.5, any errors parsing the protocol
         * from the request line are nearly always misencoded HTTP/1.x
         * requests. Only a valid 0.9 request with no parsing errors
         * at all may be treated as a simple request, if allowed.
         */
        r->assbackwards = 0;
        r->connection->keepalive = CLHY_CONN_CLOSE;
        r->proto_num = HTTP_VERSION(1, 0);
        r->protocol  = kuda_pstrdup(r->pool, "HTTP/1.0");
    }
    return 0;
}

static int table_do_fn_check_lengths(void *r_, const char *key,
                                     const char *value)
{
    request_rec *r = r_;
    if (value == NULL || r->server->limit_req_fieldsize >= strlen(value) )
        return 1;

    r->status = HTTP_BAD_REQUEST;
    kuda_table_setn(r->notes, "error-notes",
                   "Size of a request header field exceeds server limit.");
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(00560) "Request "
                  "header exceeds LimitRequestFieldSize after merging: %.*s",
                  field_name_len(key), key);
    return 0;
}

CLHY_DECLARE(void) clhy_get_mime_headers_core(request_rec *r, kuda_bucket_brigade *bb)
{
    char *last_field = NULL;
    kuda_size_t last_len = 0;
    kuda_size_t alloc_len = 0;
    char *field;
    char *value;
    kuda_size_t len;
    int fields_read = 0;
    char *tmp_field;
    core_server_config *conf = clhy_get_core_capi_config(r->server->capi_config);
    int strict = (conf->http_conformance != CLHY_HTTP_CONFORMANCE_UNSAFE);

    /*
     * Read header lines until we get the empty separator line, a read error,
     * the connection closes (EOF), reach the server limit, or we timeout.
     */
    while(1) {
        kuda_status_t rv;

        field = NULL;
        rv = clhy_rgetline(&field, r->server->limit_req_fieldsize + 2,
                         &len, r, strict ? CLHY_GETLINE_CRLF : 0, bb);

        if (rv != KUDA_SUCCESS) {
            if (KUDA_STATUS_IS_TIMEUP(rv)) {
                r->status = HTTP_REQUEST_TIME_OUT;
            }
            else {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, r, 
                              "Failed to read request header line %s", field);
                r->status = HTTP_BAD_REQUEST;
            }

            /* clhy_rgetline returns KUDA_ENOSPC if it fills up the buffer before
             * finding the end-of-line.  This is only going to happen if it
             * exceeds the configured limit for a field size.
             */
            if (rv == KUDA_ENOSPC) {
                kuda_table_setn(r->notes, "error-notes",
                               "Size of a request header field "
                               "exceeds server limit.");
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(00561)
                              "Request header exceeds LimitRequestFieldSize%s"
                              "%.*s",
                              (field && *field) ? ": " : "",
                              (field) ? field_name_len(field) : 0,
                              (field) ? field : "");
            }
            return;
        }

        /* For all header values, and all obs-fold lines, the presence of
         * additional whitespace is a no-op, so collapse trailing whitespace
         * to save buffer allocation and optimize copy operations.
         * Do not remove the last single whitespace under any condition.
         */
        while (len > 1 && (field[len-1] == '\t' || field[len-1] == ' ')) {
            field[--len] = '\0';
        } 

        if (*field == '\t' || *field == ' ') {

            /* Append any newly-read obs-fold line onto the preceding
             * last_field line we are processing
             */
            kuda_size_t fold_len;

            if (last_field == NULL) {
                r->status = HTTP_BAD_REQUEST;
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03442)
                              "Line folding encountered before first"
                              " header line");
                return;
            }

            if (field[1] == '\0') {
                r->status = HTTP_BAD_REQUEST;
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03443)
                              "Empty folded line encountered");
                return;
            }

            /* Leading whitespace on an obs-fold line can be
             * similarly discarded */
            while (field[1] == '\t' || field[1] == ' ') {
                ++field; --len;
            }

            /* This line is a continuation of the preceding line(s),
             * so append it to the line that we've set aside.
             * Note: this uses a power-of-two allocator to avoid
             * doing O(n) allocs and using O(n^2) space for
             * continuations that span many many lines.
             */
            fold_len = last_len + len + 1; /* trailing null */

            if (fold_len >= (kuda_size_t)(r->server->limit_req_fieldsize)) {
                r->status = HTTP_BAD_REQUEST;
                /* report what we have accumulated so far before the
                 * overflow (last_field) as the field with the problem
                 */
                kuda_table_setn(r->notes, "error-notes",
                               "Size of a request header field "
                               "exceeds server limit.");
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(00562)
                              "Request header exceeds LimitRequestFieldSize "
                              "after folding: %.*s",
                              field_name_len(last_field), last_field);
                return;
            }

            if (fold_len > alloc_len) {
                char *fold_buf;
                alloc_len += alloc_len;
                if (fold_len > alloc_len) {
                    alloc_len = fold_len;
                }
                fold_buf = (char *)kuda_palloc(r->pool, alloc_len);
                memcpy(fold_buf, last_field, last_len);
                last_field = fold_buf;
            }
            memcpy(last_field + last_len, field, len +1); /* +1 for nul */
            /* Replace obs-fold w/ SP per RFC 7230 3.2.4 */
            last_field[last_len] = ' ';
            last_len += len;

            /* We've appended this obs-fold line to last_len, proceed to
             * read the next input line
             */
            continue;
        }
        else if (last_field != NULL) {

            /* Process the previous last_field header line with all obs-folded
             * segments already concatenated (this is not operating on the
             * most recently read input line).
             */

            if (r->server->limit_req_fields
                    && (++fields_read > r->server->limit_req_fields)) {
                r->status = HTTP_BAD_REQUEST;
                kuda_table_setn(r->notes, "error-notes",
                               "The number of request header fields "
                               "exceeds this server's limit.");
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(00563)
                              "Number of request headers exceeds "
                              "LimitRequestFields");
                return;
            }

            if (!strict)
            {
                /* Not Strict ('Unsafe' mode), using the legacy parser */

                if (!(value = strchr(last_field, ':'))) { /* Find ':' or */
                    r->status = HTTP_BAD_REQUEST;   /* abort bad request */
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00564)
                                  "Request header field is missing ':' "
                                  "separator: %.*s", (int)LOG_NAME_MAX_LEN,
                                  last_field);
                    return;
                }

                if (value == last_field) {
                    r->status = HTTP_BAD_REQUEST;
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03453)
                                  "Request header field name was empty");
                    return;
                }

                *value++ = '\0'; /* NUL-terminate at colon */

                if (strpbrk(last_field, "\t\n\v\f\r ")) {
                    r->status = HTTP_BAD_REQUEST;
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03452)
                                  "Request header field name presented"
                                  " invalid whitespace");
                    return;
                }

                while (*value == ' ' || *value == '\t') {
                     ++value;            /* Skip to start of value   */
                }

                if (strpbrk(value, "\n\v\f\r")) {
                    r->status = HTTP_BAD_REQUEST;
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03451)
                                  "Request header field value presented"
                                  " bad whitespace");
                    return;
                }
            }
            else /* Using strict RFC7230 parsing */
            {
                /* Ensure valid token chars before ':' per RFC 7230 3.2.4 */
                value = (char *)clhy_scan_http_token(last_field);
                if ((value == last_field) || *value != ':') {
                    r->status = HTTP_BAD_REQUEST;
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02426)
                                  "Request header field name is malformed: "
                                  "%.*s", (int)LOG_NAME_MAX_LEN, last_field);
                    return;
                }

                *value++ = '\0'; /* NUL-terminate last_field name at ':' */

                while (*value == ' ' || *value == '\t') {
                    ++value;     /* Skip LWS of value */
                }

                /* Find invalid, non-HT ctrl char, or the trailing NULL */
                tmp_field = (char *)clhy_scan_http_field_content(value);

                /* Reject value for all garbage input (CTRLs excluding HT)
                 * e.g. only VCHAR / SP / HT / obs-text are allowed per
                 * RFC7230 3.2.6 - leave all more explicit rule enforcement
                 * for specific header handler logic later in the cycle
                 */
                if (*tmp_field != '\0') {
                    r->status = HTTP_BAD_REQUEST;
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02427)
                                  "Request header value is malformed: "
                                  "%.*s", (int)LOG_NAME_MAX_LEN, value);
                    return;
                }
            }

            kuda_table_addn(r->headers_in, last_field, value);

            /* This last_field header is now stored in headers_in,
             * resume processing of the current input line.
             */
        }

        /* Found the terminating empty end-of-headers line, stop. */
        if (len == 0) {
            break;
        }

        /* Keep track of this new header line so that we can extend it across
         * any obs-fold or parse it on the next loop iteration. We referenced
         * our previously allocated buffer in r->headers_in,
         * so allocate a fresh buffer if required.
         */
        alloc_len = 0;
        last_field = field;
        last_len = len;
    }

    /* Combine multiple message-header fields with the same
     * field-name, following RFC 2616, 4.2.
     */
    kuda_table_compress(r->headers_in, KUDA_OVERLAP_TABLES_MERGE);

    /* enforce LimitRequestFieldSize for merged headers */
    kuda_table_do(table_do_fn_check_lengths, r, r->headers_in, NULL);
}

CLHY_DECLARE(void) clhy_get_mime_headers(request_rec *r)
{
    kuda_bucket_brigade *tmp_bb;
    tmp_bb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
    clhy_get_mime_headers_core(r, tmp_bb);
    kuda_brigade_destroy(tmp_bb);
}

request_rec *clhy_read_request(conn_rec *conn)
{
    request_rec *r;
    kuda_pool_t *p;
    const char *expect;
    int access_status;
    kuda_bucket_brigade *tmp_bb;
    kuda_socket_t *csd;
    kuda_interval_time_t cur_timeout;


    kuda_pool_create(&p, conn->pool);
    kuda_pool_tag(p, "request");
    r = kuda_pcalloc(p, sizeof(request_rec));
    CLHY_READ_REQUEST_ENTRY((intptr_t)r, (uintptr_t)conn);
    r->pool            = p;
    r->connection      = conn;
    r->server          = conn->base_server;

    r->user            = NULL;
    r->clhy_auth_type    = NULL;

    r->allowed_methods = clhy_make_method_list(p, 2);

    r->headers_in      = kuda_table_make(r->pool, 25);
    r->trailers_in     = kuda_table_make(r->pool, 5);
    r->subprocess_env  = kuda_table_make(r->pool, 25);
    r->headers_out     = kuda_table_make(r->pool, 12);
    r->err_headers_out = kuda_table_make(r->pool, 5);
    r->trailers_out    = kuda_table_make(r->pool, 5);
    r->notes           = kuda_table_make(r->pool, 5);

    r->request_config  = clhy_create_request_config(r->pool);
    /* Must be set before we run create request hook */

    r->proto_output_filters = conn->output_filters;
    r->output_filters  = r->proto_output_filters;
    r->proto_input_filters = conn->input_filters;
    r->input_filters   = r->proto_input_filters;
    clhy_run_create_request(r);
    r->per_dir_config  = r->server->lookup_defaults;

    r->sent_bodyct     = 0;                      /* bytect isn't for body */

    r->read_length     = 0;
    r->read_body       = REQUEST_NO_BODY;

    r->status          = HTTP_OK;  /* Until further notice */
    r->the_request     = NULL;

    /* Begin by presuming any capi can make its own path_info assumptions,
     * until some capi interjects and changes the value.
     */
    r->used_path_info = CLHY_REQ_DEFAULT_PATH_INFO;

    r->useragent_addr = conn->client_addr;
    r->useragent_ip = conn->client_ip;

    tmp_bb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);

    clhy_run_pre_read_request(r, conn);

    /* Get the request... */
    if (!read_request_line(r, tmp_bb)) {
        switch (r->status) {
        case HTTP_REQUEST_URI_TOO_LARGE:
        case HTTP_BAD_REQUEST:
        case HTTP_VERSION_NOT_SUPPORTED:
        case HTTP_NOT_IMPLEMENTED:
            if (r->status == HTTP_REQUEST_URI_TOO_LARGE) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(00565)
                              "request failed: client's request-line exceeds LimitRequestLine (longer than %d)",
                              r->server->limit_req_line);
            }
            else if (r->method == NULL) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00566)
                              "request failed: malformed request line");
            }
            access_status = r->status;
            r->status = HTTP_OK;
            clhy_die(access_status, r);
            clhy_update_child_status(conn->sbh, SERVER_BUSY_LOG, r);
            clhy_run_log_transaction(r);
            r = NULL;
            kuda_brigade_destroy(tmp_bb);
            goto traceout;
        case HTTP_REQUEST_TIME_OUT:
            clhy_update_child_status(conn->sbh, SERVER_BUSY_LOG, NULL);
            if (!r->connection->keepalives)
                clhy_run_log_transaction(r);
            kuda_brigade_destroy(tmp_bb);
            goto traceout;
        default:
            kuda_brigade_destroy(tmp_bb);
            r = NULL;
            goto traceout;
        }
    }

    /* We may have been in keep_alive_timeout mode, so toggle back
     * to the normal timeout mode as we fetch the header lines,
     * as necessary.
     */
    csd = clhy_get_conn_socket(conn);
    kuda_socket_timeout_get(csd, &cur_timeout);
    if (cur_timeout != conn->base_server->timeout) {
        kuda_socket_timeout_set(csd, conn->base_server->timeout);
        cur_timeout = conn->base_server->timeout;
    }

    if (!r->assbackwards) {
        const char *tenc;

        clhy_get_mime_headers_core(r, tmp_bb);
        if (r->status != HTTP_OK) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00567)
                          "request failed: error reading the headers");
            clhy_send_error_response(r, 0);
            clhy_update_child_status(conn->sbh, SERVER_BUSY_LOG, r);
            clhy_run_log_transaction(r);
            kuda_brigade_destroy(tmp_bb);
            goto traceout;
        }

        tenc = kuda_table_get(r->headers_in, "Transfer-Encoding");
        if (tenc) {
            /* http://tools.ietf.org/html/draft-ietf-httpbis-p1-messaging-23
             * Section 3.3.3.3: "If a Transfer-Encoding header field is
             * present in a request and the chunked transfer coding is not
             * the final encoding ...; the server MUST respond with the 400
             * (Bad Request) status code and then close the connection".
             */
            if (!(strcasecmp(tenc, "chunked") == 0 /* fast path */
                    || clhy_find_last_token(r->pool, tenc, "chunked"))) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02539)
                              "client sent unknown Transfer-Encoding "
                              "(%s): %s", tenc, r->uri);
                r->status = HTTP_BAD_REQUEST;
                conn->keepalive = CLHY_CONN_CLOSE;
                clhy_send_error_response(r, 0);
                clhy_update_child_status(conn->sbh, SERVER_BUSY_LOG, r);
                clhy_run_log_transaction(r);
                kuda_brigade_destroy(tmp_bb);
                goto traceout;
            }

            /* http://tools.ietf.org/html/draft-ietf-httpbis-p1-messaging-23
             * Section 3.3.3.3: "If a message is received with both a
             * Transfer-Encoding and a Content-Length header field, the
             * Transfer-Encoding overrides the Content-Length. ... A sender
             * MUST remove the received Content-Length field".
             */
            kuda_table_unset(r->headers_in, "Content-Length");
        }
    }

    kuda_brigade_destroy(tmp_bb);

    /* update what we think the virtual host is based on the headers we've
     * now read. may update status.
     */
    clhy_update_vhost_from_headers(r);
    access_status = r->status;

    /* Toggle to the Host:-based vhost's timeout mode to fetch the
     * request body and send the response body, if needed.
     */
    if (cur_timeout != r->server->timeout) {
        kuda_socket_timeout_set(csd, r->server->timeout);
        cur_timeout = r->server->timeout;
    }

    /* we may have switched to another server */
    r->per_dir_config = r->server->lookup_defaults;

    if ((!r->hostname && (r->proto_num >= HTTP_VERSION(1, 1)))
        || ((r->proto_num == HTTP_VERSION(1, 1))
            && !kuda_table_get(r->headers_in, "Host"))) {
        /*
         * Client sent us an HTTP/1.1 or later request without telling us the
         * hostname, either with a full URL or a Host: header. We therefore
         * need to (as per the 1.1 spec) send an error.  As a special case,
         * HTTP/1.1 mentions twice (S9, S14.23) that a request MUST contain
         * a Host: header, and the server MUST respond with 400 if it doesn't.
         */
        access_status = HTTP_BAD_REQUEST;
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00569)
                      "client sent HTTP/1.1 request without hostname "
                      "(see RFC2616 section 14.23): %s", r->uri);
    }

    /*
     * Add the HTTP_IN filter here to ensure that clhy_discard_request_body
     * called by clhy_die and by clhy_send_error_response works correctly on
     * status codes that do not cause the connection to be dropped and
     * in situations where the connection should be kept alive.
     */

    clhy_add_input_filter_handle(clhy_http_input_filter_handle,
                               NULL, r, r->connection);

    if (access_status != HTTP_OK
        || (access_status = clhy_run_post_read_request(r))) {
        clhy_die(access_status, r);
        clhy_update_child_status(conn->sbh, SERVER_BUSY_LOG, r);
        clhy_run_log_transaction(r);
        r = NULL;
        goto traceout;
    }

    if (((expect = kuda_table_get(r->headers_in, "Expect")) != NULL)
        && (expect[0] != '\0')) {
        /*
         * The Expect header field was added to HTTP/1.1 after RFC 2068
         * as a means to signal when a 100 response is desired and,
         * unfortunately, to signal a poor man's mandatory extension that
         * the server must understand or return 417 Expectation Failed.
         */
        if (strcasecmp(expect, "100-continue") == 0) {
            r->expecting_100 = 1;
        }
        else {
            r->status = HTTP_EXPECTATION_FAILED;
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(00570)
                          "client sent an unrecognized expectation value of "
                          "Expect: %s", expect);
            clhy_send_error_response(r, 0);
            clhy_update_child_status(conn->sbh, SERVER_BUSY_LOG, r);
            clhy_run_log_transaction(r);
            goto traceout;
        }
    }

    CLHY_READ_REQUEST_SUCCESS((uintptr_t)r, (char *)r->method, (char *)r->uri, (char *)r->server->defn_name, r->status);
    return r;
    traceout:
    CLHY_READ_REQUEST_FAILURE((uintptr_t)r);
    return r;
}

/* if a request with a body creates a subrequest, remove original request's
 * input headers which pertain to the body which has already been read.
 * out-of-line helper function for clhy_set_sub_req_protocol.
 */

static void strip_headers_request_body(request_rec *rnew)
{
    kuda_table_unset(rnew->headers_in, "Content-Encoding");
    kuda_table_unset(rnew->headers_in, "Content-Language");
    kuda_table_unset(rnew->headers_in, "Content-Length");
    kuda_table_unset(rnew->headers_in, "Content-Location");
    kuda_table_unset(rnew->headers_in, "Content-MD5");
    kuda_table_unset(rnew->headers_in, "Content-Range");
    kuda_table_unset(rnew->headers_in, "Content-Type");
    kuda_table_unset(rnew->headers_in, "Expires");
    kuda_table_unset(rnew->headers_in, "Last-Modified");
    kuda_table_unset(rnew->headers_in, "Transfer-Encoding");
}

/*
 * A couple of other functions which initialize some of the fields of
 * a request structure, as appropriate for adjuncts of one kind or another
 * to a request in progress.  Best here, rather than elsewhere, since
 * *someone* has to set the protocol-specific fields...
 */

CLHY_DECLARE(void) clhy_set_sub_req_protocol(request_rec *rnew,
                                         const request_rec *r)
{
    rnew->the_request     = r->the_request;  /* Keep original request-line */

    rnew->assbackwards    = 1;   /* Don't send headers from this. */
    rnew->no_local_copy   = 1;   /* Don't try to send HTTP_NOT_MODIFIED for a
                                  * fragment. */
    rnew->method          = "GET";
    rnew->method_number   = M_GET;
    rnew->protocol        = "INCLUDED";

    rnew->status          = HTTP_OK;

    rnew->headers_in      = kuda_table_copy(rnew->pool, r->headers_in);
    rnew->trailers_in     = kuda_table_copy(rnew->pool, r->trailers_in);

    /* did the original request have a body?  (e.g. POST w/SSI tags)
     * if so, make sure the subrequest doesn't inherit body headers
     */
    if (!r->kept_body && (kuda_table_get(r->headers_in, "Content-Length")
        || kuda_table_get(r->headers_in, "Transfer-Encoding"))) {
        strip_headers_request_body(rnew);
    }
    rnew->subprocess_env  = kuda_table_copy(rnew->pool, r->subprocess_env);
    rnew->headers_out     = kuda_table_make(rnew->pool, 5);
    rnew->err_headers_out = kuda_table_make(rnew->pool, 5);
    rnew->trailers_out    = kuda_table_make(rnew->pool, 5);
    rnew->notes           = kuda_table_make(rnew->pool, 5);

    rnew->expecting_100   = r->expecting_100;
    rnew->read_length     = r->read_length;
    rnew->read_body       = REQUEST_NO_BODY;

    rnew->main = (request_rec *) r;
}

static void end_output_stream(request_rec *r)
{
    conn_rec *c = r->connection;
    kuda_bucket_brigade *bb;
    kuda_bucket *b;

    bb = kuda_brigade_create(r->pool, c->bucket_alloc);
    b = kuda_bucket_eos_create(c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bb, b);
    clhy_pass_brigade(r->output_filters, bb);
}

CLHY_DECLARE(void) clhy_finalize_sub_req_protocol(request_rec *sub)
{
    /* tell the filter chain there is no more content coming */
    if (!sub->eos_sent) {
        end_output_stream(sub);
    }
}

/* finalize_request_protocol is called at completion of sending the
 * response.  Its sole purpose is to send the terminating protocol
 * information for any wrappers around the response message body
 * (i.e., transfer encodings).  It should have been named finalize_response.
 */
CLHY_DECLARE(void) clhy_finalize_request_protocol(request_rec *r)
{
    (void) clhy_discard_request_body(r);

    /* tell the filter chain there is no more content coming */
    if (!r->eos_sent) {
        end_output_stream(r);
    }
}

/*
 * Support for the Basic authentication protocol, and a bit for Digest.
 */
CLHY_DECLARE(void) clhy_note_auth_failure(request_rec *r)
{
    const char *type = clhy_auth_type(r);
    if (type) {
        clhy_run_note_auth_failure(r, type);
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00571)
                      "need AuthType to note auth failure: %s", r->uri);
    }
}

CLHY_DECLARE(void) clhy_note_basic_auth_failure(request_rec *r)
{
    clhy_note_auth_failure(r);
}

CLHY_DECLARE(void) clhy_note_digest_auth_failure(request_rec *r)
{
    clhy_note_auth_failure(r);
}

CLHY_DECLARE(int) clhy_get_basic_auth_pw(request_rec *r, const char **pw)
{
    const char *auth_line = kuda_table_get(r->headers_in,
                                          (PROXYREQ_PROXY == r->proxyreq)
                                              ? "Proxy-Authorization"
                                              : "Authorization");
    const char *t;

    if (!(t = clhy_auth_type(r)) || strcasecmp(t, "Basic"))
        return DECLINED;

    if (!clhy_auth_name(r)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00572) 
                      "need AuthName: %s", r->uri);
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    if (!auth_line) {
        clhy_note_auth_failure(r);
        return HTTP_UNAUTHORIZED;
    }

    if (strcasecmp(clhy_getword(r->pool, &auth_line, ' '), "Basic")) {
        /* Client tried to authenticate using wrong auth scheme */
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(00573)
                      "client used wrong authentication scheme: %s", r->uri);
        clhy_note_auth_failure(r);
        return HTTP_UNAUTHORIZED;
    }

    while (*auth_line == ' ' || *auth_line == '\t') {
        auth_line++;
    }

    t = clhy_pbase64decode(r->pool, auth_line);
    r->user = clhy_getword_nulls (r->pool, &t, ':');
    kuda_table_setn(r->notes, CLHY_GET_BASIC_AUTH_PW_NOTE, "1");
    r->clhy_auth_type = "Basic";

    *pw = t;

    return OK;
}

CLHY_DECLARE(kuda_status_t) clhy_get_basic_auth_components(const request_rec *r,
                                                      const char **username,
                                                      const char **password)
{
    const char *auth_header;
    const char *credentials;
    const char *decoded;
    const char *user;

    auth_header = (PROXYREQ_PROXY == r->proxyreq) ? "Proxy-Authorization"
                                                  : "Authorization";
    credentials = kuda_table_get(r->headers_in, auth_header);

    if (!credentials) {
        /* No auth header. */
        return KUDA_EINVAL;
    }

    if (clhy_cstr_casecmp(clhy_getword(r->pool, &credentials, ' '), "Basic")) {
        /* These aren't Basic credentials. */
        return KUDA_EINVAL;
    }

    while (*credentials == ' ' || *credentials == '\t') {
        credentials++;
    }

    /* XXX Our base64 decoding functions don't actually error out if the string
     * we give it isn't base64; they'll just silently stop and hand us whatever
     * they've parsed up to that point.
     *
     * Since this function is supposed to be a drop-in replacement for the
     * deprecated clhy_get_basic_auth_pw(), don't fix this for 2.4.x.
     */
    decoded = clhy_pbase64decode(r->pool, credentials);
    user = clhy_getword_nulls(r->pool, &decoded, ':');

    if (username) {
        *username = user;
    }
    if (password) {
        *password = decoded;
    }

    return KUDA_SUCCESS;
}

struct content_length_ctx {
    int data_sent;  /* true if the C-L filter has already sent at
                     * least one bucket on to the next output filter
                     * for this request
                     */
    kuda_bucket_brigade *tmpbb;
};

/* This filter computes the content length, but it also computes the number
 * of bytes sent to the client.  This means that this filter will always run
 * through all of the buckets in all brigades
 */
CLHY_CORE_DECLARE_NONSTD(kuda_status_t) clhy_content_length_filter(
    clhy_filter_t *f,
    kuda_bucket_brigade *b)
{
    request_rec *r = f->r;
    struct content_length_ctx *ctx;
    kuda_bucket *e;
    int eos = 0;
    kuda_read_type_e eblock = KUDA_NONBLOCK_READ;

    ctx = f->ctx;
    if (!ctx) {
        f->ctx = ctx = kuda_palloc(r->pool, sizeof(*ctx));
        ctx->data_sent = 0;
        ctx->tmpbb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
    }

    /* Loop through the brigade to count the length. To avoid
     * arbitrary memory consumption with morphing bucket types, this
     * loop will stop and pass on the brigade when necessary. */
    e = KUDA_BRIGADE_FIRST(b);
    while (e != KUDA_BRIGADE_SENTINEL(b)) {
        kuda_status_t rv;

        if (KUDA_BUCKET_IS_EOS(e)) {
            eos = 1;
            break;
        }
        /* For a flush bucket, fall through to pass the brigade and
         * flush now. */
        else if (KUDA_BUCKET_IS_FLUSH(e)) {
            e = KUDA_BUCKET_NEXT(e);
        }
        /* For metadata bucket types other than FLUSH, loop. */
        else if (KUDA_BUCKET_IS_METADATA(e)) {
            e = KUDA_BUCKET_NEXT(e);
            continue;
        }
        /* For determinate length data buckets, count the length and
         * continue. */
        else if (e->length != (kuda_size_t)-1) {
            r->bytes_sent += e->length;
            e = KUDA_BUCKET_NEXT(e);
            continue;
        }
        /* For indeterminate length data buckets, perform one read. */
        else /* e->length == (kuda_size_t)-1 */ {
            kuda_size_t len;
            const char *ignored;
        
            rv = kuda_bucket_read(e, &ignored, &len, eblock);
            if ((rv != KUDA_SUCCESS) && !KUDA_STATUS_IS_EAGAIN(rv)) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(00574)
                              "clhy_content_length_filter: "
                              "kuda_bucket_read() failed");
                return rv;
            }
            if (rv == KUDA_SUCCESS) {
                eblock = KUDA_NONBLOCK_READ;
                e = KUDA_BUCKET_NEXT(e);
                r->bytes_sent += len;
            }
            else if (KUDA_STATUS_IS_EAGAIN(rv)) {
                kuda_bucket *flush;

                /* Next read must block. */
                eblock = KUDA_BLOCK_READ;

                /* Ensure the last bucket to pass down is a flush if
                 * the next read will block. */
                flush = kuda_bucket_flush_create(f->c->bucket_alloc);
                KUDA_BUCKET_INSERT_BEFORE(e, flush);
            }
        }

        /* Optimization: if the next bucket is EOS (directly after a
         * bucket morphed to the heap, or a flush), short-cut to
         * handle EOS straight away - allowing C-L to be determined
         * for content which is already entirely in memory. */
        if (e != KUDA_BRIGADE_SENTINEL(b) && KUDA_BUCKET_IS_EOS(e)) {
            continue;
        }

        /* On reaching here, pass on everything in the brigade up to
         * this point. */
        kuda_brigade_split_ex(b, e, ctx->tmpbb);
        
        rv = clhy_pass_brigade(f->next, b);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
        else if (f->c->aborted) {
            return KUDA_ECONNABORTED;
        }
        kuda_brigade_cleanup(b);
        KUDA_BRIGADE_CONCAT(b, ctx->tmpbb);
        e = KUDA_BRIGADE_FIRST(b);
        
        ctx->data_sent = 1;
    }

    /* If we've now seen the entire response and it's otherwise
     * okay to set the C-L in the response header, then do so now.
     *
     * We can only set a C-L in the response header if we haven't already
     * sent any buckets on to the next output filter for this request.
     */
    if (ctx->data_sent == 0 && eos &&
        /* don't whack the C-L if it has already been set for a HEAD
         * by something like proxy.  the brigade only has an EOS bucket
         * in this case, making r->bytes_sent zero.
         *
         * if r->bytes_sent > 0 we have a (temporary) body whose length may
         * have been changed by a filter.  the C-L header might not have been
         * updated so we do it here.  long term it would be cleaner to have
         * such filters update or remove the C-L header, and just use it
         * if present.
         */
        !((r->header_only || CLHY_STATUS_IS_HEADER_ONLY(r->status)) && r->bytes_sent == 0 &&
            kuda_table_get(r->headers_out, "Content-Length"))) {
        clhy_set_content_length(r, r->bytes_sent);
    }

    ctx->data_sent = 1;
    return clhy_pass_brigade(f->next, b);
}

/*
 * Send the body of a response to the client.
 */
CLHY_DECLARE(kuda_status_t) clhy_send_fd(kuda_file_t *fd, request_rec *r,
                                    kuda_off_t offset, kuda_size_t len,
                                    kuda_size_t *nbytes)
{
    conn_rec *c = r->connection;
    kuda_bucket_brigade *bb = NULL;
    kuda_status_t rv;

    bb = kuda_brigade_create(r->pool, c->bucket_alloc);

    kuda_brigade_insert_file(bb, fd, offset, len, r->pool);

    rv = clhy_pass_brigade(r->output_filters, bb);
    if (rv != KUDA_SUCCESS) {
        *nbytes = 0; /* no way to tell how many were actually sent */
    }
    else {
        *nbytes = len;
    }

    return rv;
}

#if KUDA_HAS_MMAP
/* send data from an in-memory buffer */
CLHY_DECLARE(kuda_size_t) clhy_send_mmap(kuda_mmap_t *mm,
                                    request_rec *r,
                                    kuda_size_t offset,
                                    kuda_size_t length)
{
    conn_rec *c = r->connection;
    kuda_bucket_brigade *bb = NULL;
    kuda_bucket *b;

    bb = kuda_brigade_create(r->pool, c->bucket_alloc);
    b = kuda_bucket_mmap_create(mm, offset, length, c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bb, b);
    clhy_pass_brigade(r->output_filters, bb);

    return mm->size; /* XXX - change API to report kuda_status_t? */
}
#endif /* KUDA_HAS_MMAP */

typedef struct {
    kuda_bucket_brigade *bb;
    kuda_bucket_brigade *tmpbb;
} old_write_filter_ctx;

CLHY_CORE_DECLARE_NONSTD(kuda_status_t) clhy_old_write_filter(
    clhy_filter_t *f, kuda_bucket_brigade *bb)
{
    old_write_filter_ctx *ctx = f->ctx;

    CLHY_DEBUG_ASSERT(ctx);

    if (ctx->bb != NULL) {
        /* whatever is coming down the pipe (we don't care), we
         * can simply insert our buffered data at the front and
         * pass the whole bundle down the chain.
         */
        KUDA_BRIGADE_PREPEND(bb, ctx->bb);
    }

    return clhy_pass_brigade(f->next, bb);
}

static clhy_filter_t *insert_old_write_filter(request_rec *r)
{
    clhy_filter_t *f;
    old_write_filter_ctx *ctx;

    /* future optimization: record some flags in the request_rec to
     * say whether we've added our filter, and whether it is first.
     */

    /* this will typically exit on the first test */
    for (f = r->output_filters; f != NULL; f = f->next) {
        if (clhy_old_write_func == f->frec)
            break;
    }

    if (f == NULL) {
        /* our filter hasn't been added yet */
        ctx = kuda_pcalloc(r->pool, sizeof(*ctx));
        ctx->tmpbb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);

        clhy_add_output_filter("OLD_WRITE", ctx, r, r->connection);
        f = r->output_filters;
    }

    return f;
}

static kuda_status_t buffer_output(request_rec *r,
                                  const char *str, kuda_size_t len)
{
    conn_rec *c = r->connection;
    clhy_filter_t *f;
    old_write_filter_ctx *ctx;

    if (len == 0)
        return KUDA_SUCCESS;

    f = insert_old_write_filter(r);
    ctx = f->ctx;

    /* if the first filter is not our buffering filter, then we have to
     * deliver the content through the normal filter chain
     */
    if (f != r->output_filters) {
        kuda_status_t rv;
        kuda_bucket *b = kuda_bucket_transient_create(str, len, c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(ctx->tmpbb, b);

        rv = clhy_pass_brigade(r->output_filters, ctx->tmpbb);
        kuda_brigade_cleanup(ctx->tmpbb);
        return rv;
    }

    if (ctx->bb == NULL) {
        ctx->bb = kuda_brigade_create(r->pool, c->bucket_alloc);
    }

    return clhy_fwrite(f->next, ctx->bb, str, len);
}

CLHY_DECLARE(int) clhy_rputc(int c, request_rec *r)
{
    char c2 = (char)c;

    if (r->connection->aborted) {
        return -1;
    }

    if (buffer_output(r, &c2, 1) != KUDA_SUCCESS)
        return -1;

    return c;
}

CLHY_DECLARE(int) clhy_rwrite(const void *buf, int nbyte, request_rec *r)
{
    if (r->connection->aborted)
        return -1;

    if (buffer_output(r, buf, nbyte) != KUDA_SUCCESS)
        return -1;

    return nbyte;
}

struct clhy_vrprintf_data {
    kuda_vformatter_buff_t vbuff;
    request_rec *r;
    char *buff;
};

/* Flush callback for kuda_vformatter; returns -1 on error. */
static int r_flush(kuda_vformatter_buff_t *buff)
{
    /* callback function passed to clhy_vformatter to be called when
     * vformatter needs to write into buff and buff.curpos > buff.endpos */

    /* clhy_vrprintf_data passed as a kuda_vformatter_buff_t, which is then
     * "downcast" to an clhy_vrprintf_data */
    struct clhy_vrprintf_data *vd = (struct clhy_vrprintf_data*)buff;

    if (vd->r->connection->aborted)
        return -1;

    /* r_flush is called when vbuff is completely full */
    if (buffer_output(vd->r, vd->buff, CLHY_IOBUFSIZE)) {
        return -1;
    }

    /* reset the buffer position */
    vd->vbuff.curpos = vd->buff;
    vd->vbuff.endpos = vd->buff + CLHY_IOBUFSIZE;

    return 0;
}

CLHY_DECLARE(int) clhy_vrprintf(request_rec *r, const char *fmt, va_list va)
{
    kuda_size_t written;
    struct clhy_vrprintf_data vd;
    char vrprintf_buf[CLHY_IOBUFSIZE];

    vd.vbuff.curpos = vrprintf_buf;
    vd.vbuff.endpos = vrprintf_buf + CLHY_IOBUFSIZE;
    vd.r = r;
    vd.buff = vrprintf_buf;

    if (r->connection->aborted)
        return -1;

    written = kuda_vformatter(r_flush, &vd.vbuff, fmt, va);

    if (written != -1) {
        int n = vd.vbuff.curpos - vrprintf_buf;

        /* last call to buffer_output, to finish clearing the buffer */
        if (buffer_output(r, vrprintf_buf,n) != KUDA_SUCCESS)
            return -1;

        written += n;
    }

    return written;
}

CLHY_DECLARE_NONSTD(int) clhy_rprintf(request_rec *r, const char *fmt, ...)
{
    va_list va;
    int n;

    if (r->connection->aborted)
        return -1;

    va_start(va, fmt);
    n = clhy_vrprintf(r, fmt, va);
    va_end(va);

    return n;
}

CLHY_DECLARE_NONSTD(int) clhy_rvputs(request_rec *r, ...)
{
    va_list va;
    const char *s;
    kuda_size_t len;
    kuda_size_t written = 0;

    if (r->connection->aborted)
        return -1;

    /* ### TODO: if the total output is large, put all the strings
     * ### into a single brigade, rather than flushing each time we
     * ### fill the buffer
     */
    va_start(va, r);
    while (1) {
        s = va_arg(va, const char *);
        if (s == NULL)
            break;

        len = strlen(s);
        if (buffer_output(r, s, len) != KUDA_SUCCESS) {
            return -1;
        }

        written += len;
    }
    va_end(va);

    return written;
}

CLHY_DECLARE(int) clhy_rflush(request_rec *r)
{
    conn_rec *c = r->connection;
    kuda_bucket *b;
    clhy_filter_t *f;
    old_write_filter_ctx *ctx;
    kuda_status_t rv;

    f = insert_old_write_filter(r);
    ctx = f->ctx;

    b = kuda_bucket_flush_create(c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(ctx->tmpbb, b);

    rv = clhy_pass_brigade(r->output_filters, ctx->tmpbb);
    kuda_brigade_cleanup(ctx->tmpbb);
    if (rv != KUDA_SUCCESS)
        return -1;

    return 0;
}

/*
 * This function sets the Last-Modified output header field to the value
 * of the mtime field in the request structure - rationalized to keep it from
 * being in the future.
 */
CLHY_DECLARE(void) clhy_set_last_modified(request_rec *r)
{
    if (!r->assbackwards) {
        kuda_time_t capi_time = clhy_rationalize_mtime(r, r->mtime);
        char *datestr = kuda_palloc(r->pool, KUDA_RFC822_DATE_LEN);

        kuda_rfc822_date(datestr, capi_time);
        kuda_table_setn(r->headers_out, "Last-Modified", datestr);
    }
}

typedef struct hdr_ptr {
    clhy_filter_t *f;
    kuda_bucket_brigade *bb;
} hdr_ptr;
 
#if KUDA_CHARSET_EBCDIC
static int send_header(void *data, const char *key, const char *val)
{
    char *header_line = NULL;
    hdr_ptr *hdr = (hdr_ptr*)data;

    header_line = kuda_pstrcat(hdr->bb->p, key, ": ", val, CRLF, NULL);
    clhy_xlate_proto_to_ascii(header_line, strlen(header_line));
    clhy_fputs(hdr->f, hdr->bb, header_line);
    return 1;
}
#else
static int send_header(void *data, const char *key, const char *val)
{
     clhy_fputstrs(((hdr_ptr*)data)->f, ((hdr_ptr*)data)->bb,
                 key, ": ", val, CRLF, NULL);
     return 1;
 }
#endif

CLHY_DECLARE(void) clhy_send_interim_response(request_rec *r, int send_headers)
{
    hdr_ptr x;
    char *status_line = NULL;
    request_rec *rr;

    if (r->proto_num < HTTP_VERSION(1,1)) {
        /* don't send interim response to HTTP/1.0 Client */
        return;
    }
    if (!clhy_is_HTTP_INFO(r->status)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00575)
                      "Status is %d - not sending interim response", r->status);
        return;
    }
    if (r->status == HTTP_CONTINUE) {
        if (!r->expecting_100) {
            /*
             * Don't send 100-Continue when there was no Expect: 100-continue
             * in the request headers. For origin servers this is a SHOULD NOT
             * for proxies it is a MUST NOT according to RFC 2616 8.2.3
             */
            return;
        }

        /* if we send an interim response, we're no longer in a state of
         * expecting one.  Also, this could feasibly be in a subrequest,
         * so we need to propagate the fact that we responded.
         */
        for (rr = r; rr != NULL; rr = rr->main) {
            rr->expecting_100 = 0;
        }
    }

    status_line = kuda_pstrcat(r->pool, CLHY_SERVER_PROTOCOL, " ", r->status_line, CRLF, NULL);
    clhy_xlate_proto_to_ascii(status_line, strlen(status_line));

    x.f = r->connection->output_filters;
    x.bb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);

    clhy_fputs(x.f, x.bb, status_line);
    if (send_headers) {
        kuda_table_do(send_header, &x, r->headers_out, NULL);
        kuda_table_clear(r->headers_out);
    }
    clhy_fputs(x.f, x.bb, CRLF_ASCII);
    clhy_fflush(x.f, x.bb);
    kuda_brigade_destroy(x.bb);
}

/*
 * Compare two protocol identifier. Result is similar to strcmp():
 * 0 gives same precedence, >0 means proto1 is preferred.
 */
static int protocol_cmp(const kuda_array_header_t *preferences,
                        const char *proto1,
                        const char *proto2)
{
    if (preferences && preferences->nelts > 0) {
        int index1 = clhy_array_str_index(preferences, proto1, 0);
        int index2 = clhy_array_str_index(preferences, proto2, 0);
        if (index2 > index1) {
            return (index1 >= 0) ? 1 : -1;
        }
        else if (index1 > index2) {
            return (index2 >= 0) ? -1 : 1;
        }
    }
    /* both have the same index (mabye -1 or no pref configured) and we compare
     * the names so that spdy3 gets precedence over spdy2. That makes
     * the outcome at least deterministic. */
    return strcmp(proto1, proto2);
}

CLHY_DECLARE(const char *) clhy_get_protocol(conn_rec *c)
{
    const char *protocol = clhy_run_protocol_get(c);
    return protocol? protocol : CLHY_PROTOCOL_HTTP1;
}

CLHY_DECLARE(kuda_status_t) clhy_get_protocol_upgrades(conn_rec *c, request_rec *r, 
                                                  server_rec *s, int report_all, 
                                                  const kuda_array_header_t **pupgrades)
{
    kuda_pool_t *pool = r? r->pool : c->pool;
    core_server_config *conf;
    const char *existing;
    kuda_array_header_t *upgrades = NULL;

    if (!s) {
        s = (r? r->server : c->base_server);
    }
    conf = clhy_get_core_capi_config(s->capi_config);
    
    if (conf->protocols->nelts > 0) {
        existing = clhy_get_protocol(c);
        if (conf->protocols->nelts > 1 
            || !clhy_array_str_contains(conf->protocols, existing)) {
            int i;
            
            /* possibly more than one choice or one, but not the
             * existing. (TODO: maybe 426 and Upgrade then?) */
            upgrades = kuda_array_make(pool, conf->protocols->nelts + 1, 
                                      sizeof(char *));
            for (i = 0; i < conf->protocols->nelts; i++) {
                const char *p = KUDA_ARRAY_IDX(conf->protocols, i, char *);
                if (strcmp(existing, p)) {
                    /* not the one we have and possible, add in this order */
                    KUDA_ARRAY_PUSH(upgrades, const char*) = p;
                }
                else if (!report_all) {
                    break;
                }
            }
        }
    }
    
    *pupgrades = upgrades;
    return KUDA_SUCCESS;
}

CLHY_DECLARE(const char *) clhy_select_protocol(conn_rec *c, request_rec *r, 
                                            server_rec *s,
                                            const kuda_array_header_t *choices)
{
    kuda_pool_t *pool = r? r->pool : c->pool;
    core_server_config *conf;
    const char *protocol = NULL, *existing;
    kuda_array_header_t *proposals;

    if (!s) {
        s = (r? r->server : c->base_server);
    }
    conf = clhy_get_core_capi_config(s->capi_config);
    
    if (CLHYLOGcdebug(c)) {
        const char *p = kuda_array_pstrcat(pool, conf->protocols, ',');
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, c, CLHYLOGNO(03155) 
                      "select protocol from %s, choices=%s for server %s", 
                      p, kuda_array_pstrcat(pool, choices, ','),
                      s->server_hostname);
    }

    if (conf->protocols->nelts <= 0) {
        /* nothing configured, by default, we only allow http/1.1 here.
         * For now...
         */
        if (clhy_array_str_contains(choices, CLHY_PROTOCOL_HTTP1)) {
            return CLHY_PROTOCOL_HTTP1;
        }
        else {
            return NULL;
        }
    }

    proposals = kuda_array_make(pool, choices->nelts + 1, sizeof(char *));
    clhy_run_protocol_propose(c, r, s, choices, proposals);

    /* If the existing protocol has not been proposed, but is a choice,
     * add it to the proposals implicitly.
     */
    existing = clhy_get_protocol(c);
    if (!clhy_array_str_contains(proposals, existing)
        && clhy_array_str_contains(choices, existing)) {
        KUDA_ARRAY_PUSH(proposals, const char*) = existing;
    }

    if (proposals->nelts > 0) {
        int i;
        const kuda_array_header_t *prefs = NULL;

        /* Default for protocols_honor_order is 'on' or != 0 */
        if (conf->protocols_honor_order == 0 && choices->nelts > 0) {
            prefs = choices;
        }
        else {
            prefs = conf->protocols;
        }

        /* Select the most preferred protocol */
        if (CLHYLOGcdebug(c)) {
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, c, CLHYLOGNO(03156) 
                          "select protocol, proposals=%s preferences=%s configured=%s", 
                          kuda_array_pstrcat(pool, proposals, ','),
                          kuda_array_pstrcat(pool, prefs, ','),
                          kuda_array_pstrcat(pool, conf->protocols, ','));
        }
        for (i = 0; i < proposals->nelts; ++i) {
            const char *p = KUDA_ARRAY_IDX(proposals, i, const char *);
            if (!clhy_array_str_contains(conf->protocols, p)) {
                /* not a configured protocol here */
                continue;
            }
            else if (!protocol 
                     || (protocol_cmp(prefs, protocol, p) < 0)) {
                /* none selected yet or this one has preference */
                protocol = p;
            }
        }
    }
    if (CLHYLOGcdebug(c)) {
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, c, CLHYLOGNO(03157)
                      "selected protocol=%s", 
                      protocol? protocol : "(none)");
    }

    return protocol;
}

CLHY_DECLARE(kuda_status_t) clhy_switch_protocol(conn_rec *c, request_rec *r, 
                                            server_rec *s,
                                            const char *protocol)
{
    const char *current = clhy_get_protocol(c);
    int rc;
    
    if (!strcmp(current, protocol)) {
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, c, CLHYLOGNO(02906)
                      "already at it, protocol_switch to %s", 
                      protocol);
        return KUDA_SUCCESS;
    }
    
    rc = clhy_run_protocol_switch(c, r, s, protocol);
    switch (rc) {
        case DECLINED:
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, c, CLHYLOGNO(02907)
                          "no implementation for protocol_switch to %s", 
                          protocol);
            return KUDA_ENOTIMPL;
        case OK:
        case DONE:
            return KUDA_SUCCESS;
        default:
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, c, CLHYLOGNO(02905)
                          "unexpected return code %d from protocol_switch to %s"
                          , rc, protocol);
            return KUDA_EOF;
    }    
}

CLHY_DECLARE(int) clhy_is_allowed_protocol(conn_rec *c, request_rec *r,
                                       server_rec *s, const char *protocol)
{
    core_server_config *conf;

    if (!s) {
        s = (r? r->server : c->base_server);
    }
    conf = clhy_get_core_capi_config(s->capi_config);
    
    if (conf->protocols->nelts > 0) {
        return clhy_array_str_contains(conf->protocols, protocol);
    }
    return !strcmp(CLHY_PROTOCOL_HTTP1, protocol);
}


CLHY_IMPLEMENT_HOOK_VOID(pre_read_request,
                       (request_rec *r, conn_rec *c),
                       (r, c))
CLHY_IMPLEMENT_HOOK_RUN_ALL(int,post_read_request,
                          (request_rec *r), (r), OK, DECLINED)
CLHY_IMPLEMENT_HOOK_RUN_ALL(int,log_transaction,
                          (request_rec *r), (r), OK, DECLINED)
CLHY_IMPLEMENT_HOOK_RUN_FIRST(const char *,http_scheme,
                            (const request_rec *r), (r), NULL)
CLHY_IMPLEMENT_HOOK_RUN_FIRST(unsigned short,default_port,
                            (const request_rec *r), (r), 0)
CLHY_IMPLEMENT_HOOK_RUN_FIRST(int, note_auth_failure,
                            (request_rec *r, const char *auth_type),
                            (r, auth_type), DECLINED)
CLHY_IMPLEMENT_HOOK_RUN_ALL(int,protocol_propose,
                          (conn_rec *c, request_rec *r, server_rec *s,
                           const kuda_array_header_t *offers,
                           kuda_array_header_t *proposals), 
                          (c, r, s, offers, proposals), OK, DECLINED)
CLHY_IMPLEMENT_HOOK_RUN_FIRST(int,protocol_switch,
                            (conn_rec *c, request_rec *r, server_rec *s,
                             const char *protocol), 
                            (c, r, s, protocol), DECLINED)
CLHY_IMPLEMENT_HOOK_RUN_FIRST(const char *,protocol_get,
                            (const conn_rec *c), (c), NULL)
