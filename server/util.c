/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * util.c: string utility things
 *
 * 3/21/93 Rob McCool
 * 1995-96 Many changes by the Hyang Language Foundation
 *
 */

/* Debugging aid:
 * #define DEBUG            to trace all cfg_open*()/cfg_closefile() calls
 * #define DEBUG_CFG_LINES  to trace every line read from the config files
 */

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_lib.h"

#define KUDA_WANT_STDIO
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#if KUDA_HAVE_PROCESS_H
#include <process.h>            /* for getpid() on Win32 */
#endif
#if KUDA_HAVE_NETDB_H
#include <netdb.h>              /* for gethostbyname() */
#endif

#include "clhy_config.h"
#include "kuda_base64.h"
#include "kuda_fnmatch.h"
#include "wwhy.h"
#include "http_main.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_config.h"
#include "http_core.h"
#include "util_ebcdic.h"
#include "util_varbuf.h"

#ifdef HAVE_PWD_H
#include <pwd.h>
#endif
#ifdef HAVE_GRP_H
#include <grp.h>
#endif
#ifdef HAVE_SYS_LOADAVG_H
#include <sys/loadavg.h>
#endif

#include "clhy_core.h"

/* A bunch of functions in util.c scan strings looking for certain characters.
 * To make that more efficient we encode a lookup table.  The test_char_table
 * is generated automatically by gen_test_char.c.
 */
#include "test_char.h"

/* we assume the folks using this ensure 0 <= c < 256... which means
 * you need a cast to (unsigned char) first, you can't just plug a
 * char in here and get it to work, because if char is signed then it
 * will first be sign extended.
 */
#define TEST_CHAR(c, f)        (test_char_table[(unsigned char)(c)] & (f))

/* Win32/NetWare/OS2 need to check for both forward and back slashes
 * in clhy_getparents() and clhy_escape_url.
 */
#ifdef CASE_BLIND_FILESYSTEM
#define IS_SLASH(s) ((s == '/') || (s == '\\'))
#define SLASHES "/\\"
#else
#define IS_SLASH(s) (s == '/')
#define SLASHES "/"
#endif

/* we know core's capi_index is 0 */
#undef CLHYLOG_CAPI_INDEX
#define CLHYLOG_CAPI_INDEX CLHY_CORE_CAPI_INDEX

/* maximum nesting level for config directories */
#ifndef CLHY_MAX_FNMATCH_DIR_DEPTH
#define CLHY_MAX_FNMATCH_DIR_DEPTH (128)
#endif

/*
 * Examine a field value (such as a media-/content-type) string and return
 * it sans any parameters; e.g., strip off any ';charset=foo' and the like.
 */
CLHY_DECLARE(char *) clhy_field_noparam(kuda_pool_t *p, const char *intype)
{
    const char *semi;

    if (intype == NULL) return NULL;

    semi = clhy_strchr_c(intype, ';');
    if (semi == NULL) {
        return kuda_pstrdup(p, intype);
    }
    else {
        while ((semi > intype) && kuda_isspace(semi[-1])) {
            semi--;
        }
        return kuda_pstrmemdup(p, intype, semi - intype);
    }
}

CLHY_DECLARE(char *) clhy_ht_time(kuda_pool_t *p, kuda_time_t t, const char *fmt,
                              int gmt)
{
    kuda_size_t retcode;
    char ts[MAX_STRING_LEN];
    char tf[MAX_STRING_LEN];
    kuda_time_exp_t xt;

    if (gmt) {
        const char *f;
        char *strp;

        kuda_time_exp_gmt(&xt, t);
        /* Convert %Z to "GMT" and %z to "+0000";
         * on hosts that do not have a time zone string in struct tm,
         * strftime must assume its argument is local time.
         */
        for(strp = tf, f = fmt; strp < tf + sizeof(tf) - 6 && (*strp = *f)
            ; f++, strp++) {
            if (*f != '%') continue;
            switch (f[1]) {
            case '%':
                *++strp = *++f;
                break;
            case 'Z':
                *strp++ = 'G';
                *strp++ = 'M';
                *strp = 'T';
                f++;
                break;
            case 'z': /* common extension */
                *strp++ = '+';
                *strp++ = '0';
                *strp++ = '0';
                *strp++ = '0';
                *strp = '0';
                f++;
                break;
            }
        }
        *strp = '\0';
        fmt = tf;
    }
    else {
        kuda_time_exp_lt(&xt, t);
    }

    /* check return code? */
    kuda_strftime(ts, &retcode, MAX_STRING_LEN, fmt, &xt);
    ts[MAX_STRING_LEN - 1] = '\0';
    return kuda_pstrdup(p, ts);
}

/* Roy owes Rob beer. */
/* Rob owes Roy dinner. */

/* These legacy comments would make a lot more sense if Roy hadn't
 * replaced the old later_than() routine with util_date.c.
 *
 * Well, okay, they still wouldn't make any sense.
 */

/* Match = 0, NoMatch = 1, Abort = -1
 * Based loosely on sections of wildmat.c by Rich Salz
 * Hmmm... shouldn't this really go component by component?
 */
CLHY_DECLARE(int) clhy_strcmp_match(const char *str, const char *expected)
{
    int x, y;

    for (x = 0, y = 0; expected[y]; ++y, ++x) {
        if ((!str[x]) && (expected[y] != '*'))
            return -1;
        if (expected[y] == '*') {
            while (expected[++y] == '*');
            if (!expected[y])
                return 0;
            while (str[x]) {
                int ret;
                if ((ret = clhy_strcmp_match(&str[x++], &expected[y])) != 1)
                    return ret;
            }
            return -1;
        }
        else if ((expected[y] != '?') && (str[x] != expected[y]))
            return 1;
    }
    return (str[x] != '\0');
}

CLHY_DECLARE(int) clhy_strcasecmp_match(const char *str, const char *expected)
{
    int x, y;

    for (x = 0, y = 0; expected[y]; ++y, ++x) {
        if (!str[x] && expected[y] != '*')
            return -1;
        if (expected[y] == '*') {
            while (expected[++y] == '*');
            if (!expected[y])
                return 0;
            while (str[x]) {
                int ret;
                if ((ret = clhy_strcasecmp_match(&str[x++], &expected[y])) != 1)
                    return ret;
            }
            return -1;
        }
        else if (expected[y] != '?'
                 && kuda_tolower(str[x]) != kuda_tolower(expected[y]))
            return 1;
    }
    return (str[x] != '\0');
}

/* We actually compare the canonical root to this root, (but we don't
 * waste time checking the case), since every use of this function in
 * wwhy tests if the path is 'proper', meaning we've already passed
 * it through kuda_filepath_merge, or we haven't.
 */
CLHY_DECLARE(int) clhy_platform_is_path_absolute(kuda_pool_t *p, const char *dir)
{
    const char *newpath;
    const char *ourdir = dir;
    if (kuda_filepath_root(&newpath, &dir, 0, p) != KUDA_SUCCESS
            || strncmp(newpath, ourdir, strlen(newpath)) != 0) {
        return 0;
    }
    return 1;
}

CLHY_DECLARE(int) clhy_is_matchexp(const char *str)
{
    int x;

    for (x = 0; str[x]; x++)
        if ((str[x] == '*') || (str[x] == '?'))
            return 1;
    return 0;
}

/*
 * Here's a pool-based interface to the POSIX-esque clhy_regcomp().
 * Note that we return clhy_regex_t instead of being passed one.
 * The reason is that if you use an already-used clhy_regex_t structure,
 * the memory that you've already allocated gets forgotten, and
 * regfree() doesn't clear it. So we don't allow it.
 */

static kuda_status_t regex_cleanup(void *preg)
{
    clhy_regfree((clhy_regex_t *) preg);
    return KUDA_SUCCESS;
}

CLHY_DECLARE(clhy_regex_t *) clhy_pregcomp(kuda_pool_t *p, const char *pattern,
                                     int cflags)
{
    clhy_regex_t *preg = kuda_palloc(p, sizeof *preg);
    int err = clhy_regcomp(preg, pattern, cflags);
    if (err) {
        if (err == CLHY_REG_ESPACE)
            clhy_abort_on_oom();
        return NULL;
    }

    kuda_pool_cleanup_register(p, (void *) preg, regex_cleanup,
                              kuda_pool_cleanup_null);

    return preg;
}

CLHY_DECLARE(void) clhy_pregfree(kuda_pool_t *p, clhy_regex_t *reg)
{
    clhy_regfree(reg);
    kuda_pool_cleanup_kill(p, (void *) reg, regex_cleanup);
}

/*
 * Similar to standard strstr() but we ignore case in this version.
 * Based on the strstr() implementation further below.
 */
CLHY_DECLARE(char *) clhy_strcasestr(const char *s1, const char *s2)
{
    char *p1, *p2;
    if (*s2 == '\0') {
        /* an empty s2 */
        return((char *)s1);
    }
    while(1) {
        for ( ; (*s1 != '\0') && (kuda_tolower(*s1) != kuda_tolower(*s2)); s1++);
        if (*s1 == '\0') {
            return(NULL);
        }
        /* found first character of s2, see if the rest matches */
        p1 = (char *)s1;
        p2 = (char *)s2;
        for (++p1, ++p2; kuda_tolower(*p1) == kuda_tolower(*p2); ++p1, ++p2) {
            if (*p1 == '\0') {
                /* both strings ended together */
                return((char *)s1);
            }
        }
        if (*p2 == '\0') {
            /* second string ended, a match */
            break;
        }
        /* didn't find a match here, try starting at next character in s1 */
        s1++;
    }
    return((char *)s1);
}

/*
 * Returns an offsetted pointer in bigstring immediately after
 * prefix. Returns bigstring if bigstring doesn't start with
 * prefix or if prefix is longer than bigstring while still matching.
 * NOTE: pointer returned is relative to bigstring, so we
 * can use standard pointer comparisons in the calling function
 * (eg: test if clhy_stripprefix(a,b) == a)
 */
CLHY_DECLARE(const char *) clhy_stripprefix(const char *bigstring,
                                        const char *prefix)
{
    const char *p1;

    if (*prefix == '\0')
        return bigstring;

    p1 = bigstring;
    while (*p1 && *prefix) {
        if (*p1++ != *prefix++)
            return bigstring;
    }
    if (*prefix == '\0')
        return p1;

    /* hit the end of bigstring! */
    return bigstring;
}

/* This function substitutes for $0-$9, filling in regular expression
 * submatches. Pass it the same nmatch and pmatch arguments that you
 * passed clhy_regexec(). pmatch should not be greater than the maximum number
 * of subexpressions - i.e. one more than the re_nsub member of clhy_regex_t.
 *
 * nmatch must be <=CLHY_MAX_REG_MATCH (10).
 *
 * input should be the string with the $-expressions, source should be the
 * string that was matched against.
 *
 * It returns the substituted string, or NULL if a vbuf is used.
 * On errors, returns the orig string.
 *
 * Parts of this code are based on Henry Spencer's regsub(), from his
 * AT&T V8 regexp package.
 */

static kuda_status_t regsub_core(kuda_pool_t *p, char **result,
                                struct clhy_varbuf *vb, const char *input,
                                const char *source, kuda_size_t nmatch,
                                clhy_regmatch_t pmatch[], kuda_size_t maxlen)
{
    const char *src = input;
    char *dst;
    char c;
    kuda_size_t no;
    kuda_size_t len = 0;

    CLHY_DEBUG_ASSERT((result && p && !vb) || (vb && !p && !result));
    if (!source || nmatch>CLHY_MAX_REG_MATCH)
        return KUDA_EINVAL;
    if (!nmatch) {
        len = strlen(src);
        if (maxlen > 0 && len >= maxlen)
            return KUDA_ENOMEM;
        if (!vb) {
            *result = kuda_pstrmemdup(p, src, len);
            return KUDA_SUCCESS;
        }
        else {
            clhy_varbuf_strmemcat(vb, src, len);
            return KUDA_SUCCESS;
        }
    }

    /* First pass, find the size */
    while ((c = *src++) != '\0') {
        if (c == '$' && kuda_isdigit(*src))
            no = *src++ - '0';
        else
            no = CLHY_MAX_REG_MATCH;

        if (no >= CLHY_MAX_REG_MATCH) {  /* Ordinary character. */
            if (c == '\\' && *src)
                src++;
            len++;
        }
        else if (no < nmatch && pmatch[no].rm_so < pmatch[no].rm_eo) {
            if (KUDA_SIZE_MAX - len <= pmatch[no].rm_eo - pmatch[no].rm_so)
                return KUDA_ENOMEM;
            len += pmatch[no].rm_eo - pmatch[no].rm_so;
        }

    }

    if (len >= maxlen && maxlen > 0)
        return KUDA_ENOMEM;

    if (!vb) {
        *result = dst = kuda_palloc(p, len + 1);
    }
    else {
        if (vb->strlen == CLHY_VARBUF_UNKNOWN)
            vb->strlen = strlen(vb->buf);
        clhy_varbuf_grow(vb, vb->strlen + len);
        dst = vb->buf + vb->strlen;
        vb->strlen += len;
    }

    /* Now actually fill in the string */

    src = input;

    while ((c = *src++) != '\0') {
        if (c == '$' && kuda_isdigit(*src))
            no = *src++ - '0';
        else
            no = CLHY_MAX_REG_MATCH;

        if (no >= CLHY_MAX_REG_MATCH) {  /* Ordinary character. */
            if (c == '\\' && *src)
                c = *src++;
            *dst++ = c;
        }
        else if (no < nmatch && pmatch[no].rm_so < pmatch[no].rm_eo) {
            len = pmatch[no].rm_eo - pmatch[no].rm_so;
            memcpy(dst, source + pmatch[no].rm_so, len);
            dst += len;
        }

    }
    *dst = '\0';

    return KUDA_SUCCESS;
}

#ifndef CLHY_PREGSUB_MAXLEN
#define CLHY_PREGSUB_MAXLEN   (HUGE_STRING_LEN * 8)
#endif
CLHY_DECLARE(char *) clhy_pregsub(kuda_pool_t *p, const char *input,
                              const char *source, kuda_size_t nmatch,
                              clhy_regmatch_t pmatch[])
{
    char *result;
    kuda_status_t rc = regsub_core(p, &result, NULL, input, source, nmatch,
                                  pmatch, CLHY_PREGSUB_MAXLEN);
    if (rc != KUDA_SUCCESS)
        result = NULL;
    return result;
}

CLHY_DECLARE(kuda_status_t) clhy_pregsub_ex(kuda_pool_t *p, char **result,
                                       const char *input, const char *source,
                                       kuda_size_t nmatch, clhy_regmatch_t pmatch[],
                                       kuda_size_t maxlen)
{
    kuda_status_t rc = regsub_core(p, result, NULL, input, source, nmatch,
                                  pmatch, maxlen);
    if (rc != KUDA_SUCCESS)
        *result = NULL;
    return rc;
}

/*
 * Parse .. so we don't compromise security
 */
CLHY_DECLARE(void) clhy_getparents(char *name)
{
    char *next;
    int l, w, first_dot;

    /* Four paseses, as per RFC 1808 */
    /* a) remove ./ path segments */
    for (next = name; *next && (*next != '.'); next++) {
    }

    l = w = first_dot = next - name;
    while (name[l] != '\0') {
        if (name[l] == '.' && IS_SLASH(name[l + 1])
            && (l == 0 || IS_SLASH(name[l - 1])))
            l += 2;
        else
            name[w++] = name[l++];
    }

    /* b) remove trailing . path, segment */
    if (w == 1 && name[0] == '.')
        w--;
    else if (w > 1 && name[w - 1] == '.' && IS_SLASH(name[w - 2]))
        w--;
    name[w] = '\0';

    /* c) remove all xx/../ segments. (including leading ../ and /../) */
    l = first_dot;

    while (name[l] != '\0') {
        if (name[l] == '.' && name[l + 1] == '.' && IS_SLASH(name[l + 2])
            && (l == 0 || IS_SLASH(name[l - 1]))) {
            int m = l + 3, n;

            l = l - 2;
            if (l >= 0) {
                while (l >= 0 && !IS_SLASH(name[l]))
                    l--;
                l++;
            }
            else
                l = 0;
            n = l;
            while ((name[n] = name[m]))
                (++n, ++m);
        }
        else
            ++l;
    }

    /* d) remove trailing xx/.. segment. */
    if (l == 2 && name[0] == '.' && name[1] == '.')
        name[0] = '\0';
    else if (l > 2 && name[l - 1] == '.' && name[l - 2] == '.'
             && IS_SLASH(name[l - 3])) {
        l = l - 4;
        if (l >= 0) {
            while (l >= 0 && !IS_SLASH(name[l]))
                l--;
            l++;
        }
        else
            l = 0;
        name[l] = '\0';
    }
}

CLHY_DECLARE(void) clhy_no2slash_ex(char *name, int is_fs_path)
{

    char *d, *s;

    if (!*name) {
        return;
    }

    s = d = name;

#ifdef HAVE_UNC_PATHS
    /* Check for UNC names.  Leave leading two slashes. */
    if (is_fs_path && s[0] == '/' && s[1] == '/')
        *d++ = *s++;
#endif

    while (*s) {
        if ((*d++ = *s) == '/') {
            do {
                ++s;
            } while (*s == '/');
        }
        else {
            ++s;
        }
    }
    *d = '\0';
}

CLHY_DECLARE(void) clhy_no2slash(char *name)
{
    clhy_no2slash_ex(name, 1);
}

/*
 * copy at most n leading directories of s into d
 * d should be at least as large as s plus 1 extra byte
 * assumes n > 0
 * the return value is the ever useful pointer to the trailing \0 of d
 *
 * MODIFIED FOR HAVE_DRIVE_LETTERS and NETWARE environments,
 * so that if n == 0, "/" is returned in d with n == 1
 * and s == "e:/test.html", "e:/" is returned in d
 * *** See also clhy_directory_walk in server/request.c
 *
 * examples:
 *    /a/b, 0  ==> /  (true for all platforms)
 *    /a/b, 1  ==> /
 *    /a/b, 2  ==> /a/
 *    /a/b, 3  ==> /a/b/
 *    /a/b, 4  ==> /a/b/
 *
 *    c:/a/b 0 ==> /
 *    c:/a/b 1 ==> c:/
 *    c:/a/b 2 ==> c:/a/
 *    c:/a/b 3 ==> c:/a/b
 *    c:/a/b 4 ==> c:/a/b
 */
CLHY_DECLARE(char *) clhy_make_dirstr_prefix(char *d, const char *s, int n)
{
    if (n < 1) {
        *d = '/';
        *++d = '\0';
        return (d);
    }

    for (;;) {
        if (*s == '\0' || (*s == '/' && (--n) == 0)) {
            *d = '/';
            break;
        }
        *d++ = *s++;
    }
    *++d = 0;
    return (d);
}


/*
 * return the parent directory name including trailing / of the file s
 */
CLHY_DECLARE(char *) clhy_make_dirstr_parent(kuda_pool_t *p, const char *s)
{
    const char *last_slash = clhy_strrchr_c(s, '/');
    char *d;
    int l;

    if (last_slash == NULL) {
        return kuda_pstrdup(p, "");
    }
    l = (last_slash - s) + 1;
    d = kuda_pstrmemdup(p, s, l);

    return (d);
}


CLHY_DECLARE(int) clhy_count_dirs(const char *path)
{
    int x, n;

    for (x = 0, n = 0; path[x]; x++)
        if (path[x] == '/')
            n++;
    return n;
}

CLHY_DECLARE(char *) clhy_getword_nc(kuda_pool_t *atrans, char **line, char stop)
{
    return clhy_getword(atrans, (const char **) line, stop);
}

CLHY_DECLARE(char *) clhy_getword(kuda_pool_t *atrans, const char **line, char stop)
{
    const char *pos = *line;
    int len;
    char *res;

    while ((*pos != stop) && *pos) {
        ++pos;
    }

    len = pos - *line;
    res = kuda_pstrmemdup(atrans, *line, len);

    if (stop) {
        while (*pos == stop) {
            ++pos;
        }
    }
    *line = pos;

    return res;
}

CLHY_DECLARE(char *) clhy_getword_white_nc(kuda_pool_t *atrans, char **line)
{
    return clhy_getword_white(atrans, (const char **) line);
}

CLHY_DECLARE(char *) clhy_getword_white(kuda_pool_t *atrans, const char **line)
{
    const char *pos = *line;
    int len;
    char *res;

    while (!kuda_isspace(*pos) && *pos) {
        ++pos;
    }

    len = pos - *line;
    res = kuda_pstrmemdup(atrans, *line, len);

    while (kuda_isspace(*pos)) {
        ++pos;
    }

    *line = pos;

    return res;
}

CLHY_DECLARE(char *) clhy_getword_nulls_nc(kuda_pool_t *atrans, char **line,
                                       char stop)
{
    return clhy_getword_nulls(atrans, (const char **) line, stop);
}

CLHY_DECLARE(char *) clhy_getword_nulls(kuda_pool_t *atrans, const char **line,
                                    char stop)
{
    const char *pos = clhy_strchr_c(*line, stop);
    char *res;

    if (!pos) {
        kuda_size_t len = strlen(*line);
        res = kuda_pstrmemdup(atrans, *line, len);
        *line += len;
        return res;
    }

    res = kuda_pstrmemdup(atrans, *line, pos - *line);

    ++pos;

    *line = pos;

    return res;
}

/* Get a word, (new) config-file style --- quoted strings and backslashes
 * all honored
 */

static char *substring_conf(kuda_pool_t *p, const char *start, int len,
                            char quote)
{
    char *result = kuda_palloc(p, len + 1);
    char *resp = result;
    int i;

    for (i = 0; i < len; ++i) {
        if (start[i] == '\\' && (start[i + 1] == '\\'
                                 || (quote && start[i + 1] == quote)))
            *resp++ = start[++i];
        else
            *resp++ = start[i];
    }

    *resp++ = '\0';
#if RESOLVE_ENV_PER_TOKEN
    return (char *)clhy_resolve_env(p,result);
#else
    return result;
#endif
}

CLHY_DECLARE(char *) clhy_getword_conf_nc(kuda_pool_t *p, char **line)
{
    return clhy_getword_conf(p, (const char **) line);
}

CLHY_DECLARE(char *) clhy_getword_conf(kuda_pool_t *p, const char **line)
{
    const char *str = *line, *strend;
    char *res;
    char quote;

    while (kuda_isspace(*str))
        ++str;

    if (!*str) {
        *line = str;
        return "";
    }

    if ((quote = *str) == '"' || quote == '\'') {
        strend = str + 1;
        while (*strend && *strend != quote) {
            if (*strend == '\\' && strend[1] &&
                (strend[1] == quote || strend[1] == '\\')) {
                strend += 2;
            }
            else {
                ++strend;
            }
        }
        res = substring_conf(p, str + 1, strend - str - 1, quote);

        if (*strend == quote)
            ++strend;
    }
    else {
        strend = str;
        while (*strend && !kuda_isspace(*strend))
            ++strend;

        res = substring_conf(p, str, strend - str, 0);
    }

    while (kuda_isspace(*strend))
        ++strend;
    *line = strend;
    return res;
}

CLHY_DECLARE(char *) clhy_getword_conf2_nc(kuda_pool_t *p, char **line)
{
    return clhy_getword_conf2(p, (const char **) line);
}

CLHY_DECLARE(char *) clhy_getword_conf2(kuda_pool_t *p, const char **line)
{
    const char *str = *line, *strend;
    char *res;
    char quote;
    int count = 1;

    while (kuda_isspace(*str))
        ++str;

    if (!*str) {
        *line = str;
        return "";
    }

    if ((quote = *str) == '"' || quote == '\'')
        return clhy_getword_conf(p, line);

    if (quote == '{') {
        strend = str + 1;
        while (*strend) {
            if (*strend == '}' && !--count)
                break;
            if (*strend == '{')
                ++count;
            if (*strend == '\\' && strend[1] && strend[1] == '\\') {
                ++strend;
            }
            ++strend;
        }
        res = substring_conf(p, str + 1, strend - str - 1, 0);

        if (*strend == '}')
            ++strend;
    }
    else {
        strend = str;
        while (*strend && !kuda_isspace(*strend))
            ++strend;

        res = substring_conf(p, str, strend - str, 0);
    }

    while (kuda_isspace(*strend))
        ++strend;
    *line = strend;
    return res;
}

CLHY_DECLARE(int) clhy_cfg_closefile(clhy_configfile_t *cfp)
{
#ifdef DEBUG
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, NULL, CLHYLOGNO(00551)
        "Done with config file %s", cfp->name);
#endif
    return (cfp->close == NULL) ? 0 : cfp->close(cfp->param);
}

/* we can't use kuda_file_* directly because of linking issues on Windows */
static kuda_status_t cfg_close(void *param)
{
    return kuda_file_close(param);
}

static kuda_status_t cfg_getch(char *ch, void *param)
{
    return kuda_file_getc(ch, param);
}

static kuda_status_t cfg_getstr(void *buf, kuda_size_t bufsiz, void *param)
{
    return kuda_file_gets(buf, bufsiz, param);
}

/* Open a clhy_configfile_t as FILE, return open clhy_configfile_t struct pointer */
CLHY_DECLARE(kuda_status_t) clhy_pcfg_openfile(clhy_configfile_t **ret_cfg,
                                          kuda_pool_t *p, const char *name)
{
    clhy_configfile_t *new_cfg;
    kuda_file_t *file = NULL;
    kuda_finfo_t finfo;
    kuda_status_t status;
#ifdef DEBUG
    char buf[120];
#endif

    if (name == NULL) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, NULL, CLHYLOGNO(00552)
               "Internal error: pcfg_openfile() called with NULL filename");
        return KUDA_EBADF;
    }

    status = kuda_file_open(&file, name, KUDA_READ | KUDA_BUFFERED,
                           KUDA_PLATFORM_DEFAULT, p);
#ifdef DEBUG
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, NULL, CLHYLOGNO(00553)
                "Opening config file %s (%s)",
                name, (status != KUDA_SUCCESS) ?
                kuda_strerror(status, buf, sizeof(buf)) : "successful");
#endif
    if (status != KUDA_SUCCESS)
        return status;

    status = kuda_file_info_get(&finfo, KUDA_FINFO_TYPE, file);
    if (status != KUDA_SUCCESS)
        return status;

    if (finfo.filetype != KUDA_REG &&
#if defined(WIN32) || defined(OS2) || defined(NETWARE)
        strcasecmp(kuda_filepath_name_get(name), "nul") != 0) {
#else
        strcmp(name, "/dev/null") != 0) {
#endif /* WIN32 || OS2 */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, NULL, CLHYLOGNO(00554)
                     "Access to file %s denied by server: not a regular file",
                     name);
        kuda_file_close(file);
        return KUDA_EBADF;
    }

#ifdef WIN32
    /* Some twisted character [no pun intended] at MS decided that a
     * zero width joiner as the lead wide character would be ideal for
     * describing Unicode text files.  This was further convoluted to
     * another MSism that the same character mapped into utf-8, EF BB BF
     * would signify utf-8 text files.
     *
     * Since MS configuration files are all protecting utf-8 encoded
     * Unicode path, file and resource names, we already have the correct
     * WinNT encoding.  But at least eat the stupid three bytes up front.
     */
    {
        unsigned char buf[4];
        kuda_size_t len = 3;
        status = kuda_file_read(file, buf, &len);
        if ((status != KUDA_SUCCESS) || (len < 3)
              || memcmp(buf, "\xEF\xBB\xBF", 3) != 0) {
            kuda_off_t zero = 0;
            kuda_file_seek(file, KUDA_SET, &zero);
        }
    }
#endif

    new_cfg = kuda_palloc(p, sizeof(*new_cfg));
    new_cfg->param = file;
    new_cfg->name = kuda_pstrdup(p, name);
    new_cfg->getch = cfg_getch;
    new_cfg->getstr = cfg_getstr;
    new_cfg->close = cfg_close;
    new_cfg->line_number = 0;
    *ret_cfg = new_cfg;
    return KUDA_SUCCESS;
}


/* Allocate a clhy_configfile_t handle with user defined functions and params */
CLHY_DECLARE(clhy_configfile_t *) clhy_pcfg_open_custom(
            kuda_pool_t *p, const char *descr, void *param,
            kuda_status_t (*getc_func) (char *ch, void *param),
            kuda_status_t (*gets_func) (void *buf, kuda_size_t bufsize, void *param),
            kuda_status_t (*close_func) (void *param))
{
    clhy_configfile_t *new_cfg = kuda_palloc(p, sizeof(*new_cfg));
    new_cfg->param = param;
    new_cfg->name = descr;
    new_cfg->getch = getc_func;
    new_cfg->getstr = gets_func;
    new_cfg->close = close_func;
    new_cfg->line_number = 0;
    return new_cfg;
}

/* Read one character from a configfile_t */
CLHY_DECLARE(kuda_status_t) clhy_cfg_getc(char *ch, clhy_configfile_t *cfp)
{
    kuda_status_t rc = cfp->getch(ch, cfp->param);
    if (rc == KUDA_SUCCESS && *ch == LF)
        ++cfp->line_number;
    return rc;
}

CLHY_DECLARE(const char *) clhy_pcfg_strerror(kuda_pool_t *p, clhy_configfile_t *cfp,
                                          kuda_status_t rc)
{
    if (rc == KUDA_SUCCESS)
        return NULL;

    if (rc == KUDA_ENOSPC)
        return kuda_psprintf(p, "Error reading %s at line %d: Line too long",
                            cfp->name, cfp->line_number);

    return kuda_psprintf(p, "Error reading %s at line %d: %pm",
                        cfp->name, cfp->line_number, &rc);
}

/* Read one line from open clhy_configfile_t, strip LF, increase line number */
/* If custom handler does not define a getstr() function, read char by char */
static kuda_status_t clhy_cfg_getline_core(char *buf, kuda_size_t bufsize,
                                        kuda_size_t offset, clhy_configfile_t *cfp)
{
    kuda_status_t rc;
    /* If a "get string" function is defined, use it */
    if (cfp->getstr != NULL) {
        char *cp;
        char *cbuf = buf + offset;
        kuda_size_t cbufsize = bufsize - offset;

        while (1) {
            ++cfp->line_number;
            rc = cfp->getstr(cbuf, cbufsize, cfp->param);
            if (rc == KUDA_EOF) {
                if (cbuf != buf + offset) {
                    *cbuf = '\0';
                    break;
                }
                else {
                    return KUDA_EOF;
                }
            }
            if (rc != KUDA_SUCCESS) {
                return rc;
            }

            /*
             *  check for line continuation,
             *  i.e. match [^\\]\\[\r]\n only
             */
            cp = cbuf;
            cp += strlen(cp);
            if (cp > buf && cp[-1] == LF) {
                cp--;
                if (cp > buf && cp[-1] == CR)
                    cp--;
                if (cp > buf && cp[-1] == '\\') {
                    cp--;
                    /*
                     * line continuation requested -
                     * then remove backslash and continue
                     */
                    cbufsize -= (cp-cbuf);
                    cbuf = cp;
                    continue;
                }
            }
            else if (cp - buf >= bufsize - 1) {
                return KUDA_ENOSPC;
            }
            break;
        }
    } else {
        /* No "get string" function defined; read character by character */
        kuda_size_t i = offset;

        if (bufsize < 2) {
            /* too small, assume caller is crazy */
            return KUDA_EINVAL;
        }
        buf[offset] = '\0';

        while (1) {
            char c;
            rc = cfp->getch(&c, cfp->param);
            if (rc == KUDA_EOF) {
                if (i > offset)
                    break;
                else
                    return KUDA_EOF;
            }
            if (rc != KUDA_SUCCESS)
                return rc;
            if (c == LF) {
                ++cfp->line_number;
                /* check for line continuation */
                if (i > 0 && buf[i-1] == '\\') {
                    i--;
                    continue;
                }
                else {
                    break;
                }
            }
            buf[i] = c;
            ++i;
            if (i >= bufsize - 1) {
                return KUDA_ENOSPC;
            }
        }
        buf[i] = '\0';
    }
    return KUDA_SUCCESS;
}

static int cfg_trim_line(char *buf)
{
    char *start, *end;
    /*
     * Leading and trailing white space is eliminated completely
     */
    start = buf;
    while (kuda_isspace(*start))
        ++start;
    /* blast trailing whitespace */
    end = &start[strlen(start)];
    while (--end >= start && kuda_isspace(*end))
        *end = '\0';
    /* Zap leading whitespace by shifting */
    if (start != buf)
        memmove(buf, start, end - start + 2);
#ifdef DEBUG_CFG_LINES
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, NULL, CLHYLOGNO(00555) "Read config: '%s'", buf);
#endif
    return end - start + 1;
}

/* Read one line from open clhy_configfile_t, strip LF, increase line number */
/* If custom handler does not define a getstr() function, read char by char */
CLHY_DECLARE(kuda_status_t) clhy_cfg_getline(char *buf, kuda_size_t bufsize,
                                        clhy_configfile_t *cfp)
{
    kuda_status_t rc = clhy_cfg_getline_core(buf, bufsize, 0, cfp);
    if (rc == KUDA_SUCCESS)
        cfg_trim_line(buf);
    return rc;
}

CLHY_DECLARE(kuda_status_t) clhy_varbuf_cfg_getline(struct clhy_varbuf *vb,
                                               clhy_configfile_t *cfp,
                                               kuda_size_t max_len)
{
    kuda_status_t rc;
    kuda_size_t new_len;
    vb->strlen = 0;
    *vb->buf = '\0';

    if (vb->strlen == CLHY_VARBUF_UNKNOWN)
        vb->strlen = strlen(vb->buf);
    if (vb->avail - vb->strlen < 3) {
        new_len = vb->avail * 2;
        if (new_len > max_len)
            new_len = max_len;
        else if (new_len < 3)
            new_len = 3;
        clhy_varbuf_grow(vb, new_len);
    }

    for (;;) {
        rc = clhy_cfg_getline_core(vb->buf, vb->avail, vb->strlen, cfp);
        if (rc == KUDA_ENOSPC || rc == KUDA_SUCCESS)
            vb->strlen += strlen(vb->buf + vb->strlen);
        if (rc != KUDA_ENOSPC)
            break;
        if (vb->avail >= max_len)
            return KUDA_ENOSPC;
        new_len = vb->avail * 2;
        if (new_len > max_len)
            new_len = max_len;
        clhy_varbuf_grow(vb, new_len);
        --cfp->line_number;
    }
    if (vb->strlen > max_len)
        return KUDA_ENOSPC;
    if (rc == KUDA_SUCCESS)
        vb->strlen = cfg_trim_line(vb->buf);
    return rc;
}

/* Size an HTTP header field list item, as separated by a comma.
 * The return value is a pointer to the beginning of the non-empty list item
 * within the original string (or NULL if there is none) and the address
 * of field is shifted to the next non-comma, non-whitespace character.
 * len is the length of the item excluding any beginning whitespace.
 */
CLHY_DECLARE(const char *) clhy_size_list_item(const char **field, int *len)
{
    const unsigned char *ptr = (const unsigned char *)*field;
    const unsigned char *token;
    int in_qpair, in_qstr, in_com;

    /* Find first non-comma, non-whitespace byte */

    while (*ptr == ',' || kuda_isspace(*ptr))
        ++ptr;

    token = ptr;

    /* Find the end of this item, skipping over dead bits */

    for (in_qpair = in_qstr = in_com = 0;
         *ptr && (in_qpair || in_qstr || in_com || *ptr != ',');
         ++ptr) {

        if (in_qpair) {
            in_qpair = 0;
        }
        else {
            switch (*ptr) {
                case '\\': in_qpair = 1;      /* quoted-pair         */
                           break;
                case '"' : if (!in_com)       /* quoted string delim */
                               in_qstr = !in_qstr;
                           break;
                case '(' : if (!in_qstr)      /* comment (may nest)  */
                               ++in_com;
                           break;
                case ')' : if (in_com)        /* end comment         */
                               --in_com;
                           break;
                default  : break;
            }
        }
    }

    if ((*len = (ptr - token)) == 0) {
        *field = (const char *)ptr;
        return NULL;
    }

    /* Advance field pointer to the next non-comma, non-white byte */

    while (*ptr == ',' || kuda_isspace(*ptr))
        ++ptr;

    *field = (const char *)ptr;
    return (const char *)token;
}

/* Retrieve an HTTP header field list item, as separated by a comma,
 * while stripping insignificant whitespace and lowercasing anything not in
 * a quoted string or comment.  The return value is a new string containing
 * the converted list item (or NULL if none) and the address pointed to by
 * field is shifted to the next non-comma, non-whitespace.
 */
CLHY_DECLARE(char *) clhy_get_list_item(kuda_pool_t *p, const char **field)
{
    const char *tok_start;
    const unsigned char *ptr;
    unsigned char *pos;
    char *token;
    int addspace = 0, in_qpair = 0, in_qstr = 0, in_com = 0, tok_len = 0;

    /* Find the beginning and maximum length of the list item so that
     * we can allocate a buffer for the new string and reset the field.
     */
    if ((tok_start = clhy_size_list_item(field, &tok_len)) == NULL) {
        return NULL;
    }
    token = kuda_palloc(p, tok_len + 1);

    /* Scan the token again, but this time copy only the good bytes.
     * We skip extra whitespace and any whitespace around a '=', '/',
     * or ';' and lowercase normal characters not within a comment,
     * quoted-string or quoted-pair.
     */
    for (ptr = (const unsigned char *)tok_start, pos = (unsigned char *)token;
         *ptr && (in_qpair || in_qstr || in_com || *ptr != ',');
         ++ptr) {

        if (in_qpair) {
            in_qpair = 0;
            *pos++ = *ptr;
        }
        else {
            switch (*ptr) {
                case '\\': in_qpair = 1;
                           if (addspace == 1)
                               *pos++ = ' ';
                           *pos++ = *ptr;
                           addspace = 0;
                           break;
                case '"' : if (!in_com)
                               in_qstr = !in_qstr;
                           if (addspace == 1)
                               *pos++ = ' ';
                           *pos++ = *ptr;
                           addspace = 0;
                           break;
                case '(' : if (!in_qstr)
                               ++in_com;
                           if (addspace == 1)
                               *pos++ = ' ';
                           *pos++ = *ptr;
                           addspace = 0;
                           break;
                case ')' : if (in_com)
                               --in_com;
                           *pos++ = *ptr;
                           addspace = 0;
                           break;
                case ' ' :
                case '\t': if (addspace)
                               break;
                           if (in_com || in_qstr)
                               *pos++ = *ptr;
                           else
                               addspace = 1;
                           break;
                case '=' :
                case '/' :
                case ';' : if (!(in_com || in_qstr))
                               addspace = -1;
                           *pos++ = *ptr;
                           break;
                default  : if (addspace == 1)
                               *pos++ = ' ';
                           *pos++ = (in_com || in_qstr) ? *ptr
                                                        : kuda_tolower(*ptr);
                           addspace = 0;
                           break;
            }
        }
    }
    *pos = '\0';

    return token;
}

typedef enum clhy_etag_e {
    CLHY_ETAG_NONE,
    CLHY_ETAG_WEAK,
    CLHY_ETAG_STRONG
} clhy_etag_e;

/* Find an item in canonical form (lowercase, no extra spaces) within
 * an HTTP field value list.  Returns 1 if found, 0 if not found.
 * This would be much more efficient if we stored header fields as
 * an array of list items as they are received instead of a plain string.
 */
static int find_list_item(kuda_pool_t *p, const char *line,
                                  const char *tok, clhy_etag_e type)
{
    const unsigned char *pos;
    const unsigned char *ptr = (const unsigned char *)line;
    int good = 0, addspace = 0, in_qpair = 0, in_qstr = 0, in_com = 0;

    if (!line || !tok) {
        return 0;
    }
    if (type == CLHY_ETAG_STRONG && *tok != '\"') {
        return 0;
    }
    if (type == CLHY_ETAG_WEAK) {
        if (*tok == 'W' && (*(tok+1)) == '/' && (*(tok+2)) == '\"') {
            tok += 2;
        }
        else if (*tok != '\"') {
            return 0;
        }
    }

    do {  /* loop for each item in line's list */

        /* Find first non-comma, non-whitespace byte */
        while (*ptr == ',' || kuda_isspace(*ptr)) {
            ++ptr;
        }

        /* Account for strong or weak Etags, depending on our search */
        if (type == CLHY_ETAG_STRONG && *ptr != '\"') {
            break;
        }
        if (type == CLHY_ETAG_WEAK) {
            if (*ptr == 'W' && (*(ptr+1)) == '/' && (*(ptr+2)) == '\"') {
                ptr += 2;
            }
            else if (*ptr != '\"') {
                break;
            }
        }

        if (*ptr)
            good = 1;  /* until proven otherwise for this item */
        else
            break;     /* no items left and nothing good found */

        /* We skip extra whitespace and any whitespace around a '=', '/',
         * or ';' and lowercase normal characters not within a comment,
         * quoted-string or quoted-pair.
         */
        for (pos = (const unsigned char *)tok;
             *ptr && (in_qpair || in_qstr || in_com || *ptr != ',');
             ++ptr) {

            if (in_qpair) {
                in_qpair = 0;
                if (good)
                    good = (*pos++ == *ptr);
            }
            else {
                switch (*ptr) {
                    case '\\': in_qpair = 1;
                               if (addspace == 1)
                                   good = good && (*pos++ == ' ');
                               good = good && (*pos++ == *ptr);
                               addspace = 0;
                               break;
                    case '"' : if (!in_com)
                                   in_qstr = !in_qstr;
                               if (addspace == 1)
                                   good = good && (*pos++ == ' ');
                               good = good && (*pos++ == *ptr);
                               addspace = 0;
                               break;
                    case '(' : if (!in_qstr)
                                   ++in_com;
                               if (addspace == 1)
                                   good = good && (*pos++ == ' ');
                               good = good && (*pos++ == *ptr);
                               addspace = 0;
                               break;
                    case ')' : if (in_com)
                                   --in_com;
                               good = good && (*pos++ == *ptr);
                               addspace = 0;
                               break;
                    case ' ' :
                    case '\t': if (addspace || !good)
                                   break;
                               if (in_com || in_qstr)
                                   good = (*pos++ == *ptr);
                               else
                                   addspace = 1;
                               break;
                    case '=' :
                    case '/' :
                    case ';' : if (!(in_com || in_qstr))
                                   addspace = -1;
                               good = good && (*pos++ == *ptr);
                               break;
                    default  : if (!good)
                                   break;
                               if (addspace == 1)
                                   good = (*pos++ == ' ');
                               if (in_com || in_qstr)
                                   good = good && (*pos++ == *ptr);
                               else
                                   good = good
                                       && (kuda_tolower(*pos++) == kuda_tolower(*ptr));
                               addspace = 0;
                               break;
                }
            }
        }
        if (good && *pos)
            good = 0;          /* not good if only a prefix was matched */

    } while (*ptr && !good);

    return good;
}

/* Find an item in canonical form (lowercase, no extra spaces) within
 * an HTTP field value list.  Returns 1 if found, 0 if not found.
 * This would be much more efficient if we stored header fields as
 * an array of list items as they are received instead of a plain string.
 */
CLHY_DECLARE(int) clhy_find_list_item(kuda_pool_t *p, const char *line,
                                  const char *tok)
{
    return find_list_item(p, line, tok, CLHY_ETAG_NONE);
}

/* Find a strong Etag in canonical form (lowercase, no extra spaces) within
 * an HTTP field value list.  Returns 1 if found, 0 if not found.
 */
CLHY_DECLARE(int) clhy_find_etag_strong(kuda_pool_t *p, const char *line,
                                    const char *tok)
{
    return find_list_item(p, line, tok, CLHY_ETAG_STRONG);
}

/* Find a weak ETag in canonical form (lowercase, no extra spaces) within
 * an HTTP field value list.  Returns 1 if found, 0 if not found.
 */
CLHY_DECLARE(int) clhy_find_etag_weak(kuda_pool_t *p, const char *line,
                                  const char *tok)
{
    return find_list_item(p, line, tok, CLHY_ETAG_WEAK);
}

/* Grab a list of tokens of the format 1#token (from RFC7230) */
CLHY_DECLARE(const char *) clhy_parse_token_list_strict(kuda_pool_t *p,
                                                const char *str_in,
                                                kuda_array_header_t **tokens,
                                                int skip_invalid)
{
    int in_leading_space = 1;
    int in_trailing_space = 0;
    int string_end = 0;
    const char *tok_begin;
    const char *cur;

    if (!str_in) {
        return NULL;
    }

    tok_begin = cur = str_in;

    while (!string_end) {
        const unsigned char c = (unsigned char)*cur;

        if (!TEST_CHAR(c, T_HTTP_TOKEN_STOP)) {
            /* Non-separator character; we are finished with leading
             * whitespace. We must never have encountered any trailing
             * whitespace before the delimiter (comma) */
            in_leading_space = 0;
            if (in_trailing_space) {
                return "Encountered illegal whitespace in token";
            }
        }
        else if (c == ' ' || c == '\t') {
            /* "Linear whitespace" only includes ASCII CRLF, space, and tab;
             * we can't get a CRLF since headers are split on them already,
             * so only look for a space or a tab */
            if (in_leading_space) {
                /* We're still in leading whitespace */
                ++tok_begin;
            }
            else {
                /* We must be in trailing whitespace */
                ++in_trailing_space;
            }
        }
        else if (c == ',' || c == '\0') {
            if (!in_leading_space) {
                /* If we're out of the leading space, we know we've read some
                 * characters of a token */
                if (*tokens == NULL) {
                    *tokens = kuda_array_make(p, 4, sizeof(char *));
                }
                KUDA_ARRAY_PUSH(*tokens, char *) =
                    kuda_pstrmemdup((*tokens)->pool, tok_begin,
                                   (cur - tok_begin) - in_trailing_space);
            }
            /* We're allowed to have null elements, just don't add them to the
             * array */

            tok_begin = cur + 1;
            in_leading_space = 1;
            in_trailing_space = 0;
            string_end = (c == '\0');
        }
        else {
            /* Encountered illegal separator char */
            if (skip_invalid) {
                /* Skip to the next separator */
                const char *temp;
                temp = clhy_strchr_c(cur, ',');
                if(!temp) {
                    temp = clhy_strchr_c(cur, '\0');
                }

                /* Act like we haven't seen a token so we reset */
                cur = temp - 1;
                in_leading_space = 1;
                in_trailing_space = 0;
            }
            else {
                return kuda_psprintf(p, "Encountered illegal separator "
                                    "'\\x%.2x'", (unsigned int)c);
            }
        }

        ++cur;
    }

    return NULL;
}

/* Scan a string for HTTP VCHAR/obs-text characters including HT and SP
 * (as used in header values, for example, in RFC 7230 section 3.2)
 * returning the pointer to the first non-HT ASCII ctrl character.
 */
CLHY_DECLARE(const char *) clhy_scan_http_field_content(const char *ptr)
{
    for ( ; !TEST_CHAR(*ptr, T_HTTP_CTRLS); ++ptr) ;

    return ptr;
}

/* Scan a string for HTTP token characters, returning the pointer to
 * the first non-token character.
 */
CLHY_DECLARE(const char *) clhy_scan_http_token(const char *ptr)
{
    for ( ; !TEST_CHAR(*ptr, T_HTTP_TOKEN_STOP); ++ptr) ;

    return ptr;
}

/* Scan a string for visible ASCII (0x21-0x7E) or obstext (0x80+)
 * and return a pointer to the first ctrl/space character encountered.
 */
CLHY_DECLARE(const char *) clhy_scan_vchar_obstext(const char *ptr)
{
    for ( ; TEST_CHAR(*ptr, T_VCHAR_OBSTEXT); ++ptr) ;

    return ptr;
}

/* Retrieve a token, spacing over it and returning a pointer to
 * the first non-white byte afterwards.  Note that these tokens
 * are delimited by semis and commas; and can also be delimited
 * by whitespace at the caller's option.
 */

CLHY_DECLARE(char *) clhy_get_token(kuda_pool_t *p, const char **accept_line,
                                int accept_white)
{
    const char *ptr = *accept_line;
    const char *tok_start;
    char *token;

    /* Find first non-white byte */

    while (kuda_isspace(*ptr))
        ++ptr;

    tok_start = ptr;

    /* find token end, skipping over quoted strings.
     * (comments are already gone).
     */

    while (*ptr && (accept_white || !kuda_isspace(*ptr))
           && *ptr != ';' && *ptr != ',') {
        if (*ptr++ == '"')
            while (*ptr)
                if (*ptr++ == '"')
                    break;
    }

    token = kuda_pstrmemdup(p, tok_start, ptr - tok_start);

    /* Advance accept_line pointer to the next non-white byte */

    while (kuda_isspace(*ptr))
        ++ptr;

    *accept_line = ptr;
    return token;
}


/* find http tokens, see the definition of token from RFC2068 */
CLHY_DECLARE(int) clhy_find_token(kuda_pool_t *p, const char *line, const char *tok)
{
    const unsigned char *start_token;
    const unsigned char *s;

    if (!line)
        return 0;

    s = (const unsigned char *)line;
    for (;;) {
        /* find start of token, skip all stop characters */
        while (*s && TEST_CHAR(*s, T_HTTP_TOKEN_STOP)) {
            ++s;
        }
        if (!*s) {
            return 0;
        }
        start_token = s;
        /* find end of the token */
        while (*s && !TEST_CHAR(*s, T_HTTP_TOKEN_STOP)) {
            ++s;
        }
        if (!strncasecmp((const char *)start_token, (const char *)tok,
                         s - start_token)) {
            return 1;
        }
        if (!*s) {
            return 0;
        }
    }
}


CLHY_DECLARE(int) clhy_find_last_token(kuda_pool_t *p, const char *line,
                                   const char *tok)
{
    int llen, tlen, lidx;

    if (!line)
        return 0;

    llen = strlen(line);
    tlen = strlen(tok);
    lidx = llen - tlen;

    if (lidx < 0 ||
        (lidx > 0 && !(kuda_isspace(line[lidx - 1]) || line[lidx - 1] == ',')))
        return 0;

    return (strncasecmp(&line[lidx], tok, tlen) == 0);
}

CLHY_DECLARE(char *) clhy_escape_shell_cmd(kuda_pool_t *p, const char *str)
{
    char *cmd;
    unsigned char *d;
    const unsigned char *s;

    cmd = kuda_palloc(p, 2 * strlen(str) + 1);        /* Be safe */
    d = (unsigned char *)cmd;
    s = (const unsigned char *)str;
    for (; *s; ++s) {

#if defined(OS2) || defined(WIN32)
        /*
         * Newlines to Win32/OS2 CreateProcess() are ill advised.
         * Convert them to spaces since they are effectively white
         * space to most applications
         */
        if (*s == '\r' || *s == '\n') {
             *d++ = ' ';
             continue;
         }
#endif

        if (TEST_CHAR(*s, T_ESCAPE_SHELL_CMD)) {
            *d++ = '\\';
        }
        *d++ = *s;
    }
    *d = '\0';

    return cmd;
}

static char x2c(const char *what)
{
    char digit;

#if !KUDA_CHARSET_EBCDIC
    digit = ((what[0] >= 'A') ? ((what[0] & 0xdf) - 'A') + 10
             : (what[0] - '0'));
    digit *= 16;
    digit += (what[1] >= 'A' ? ((what[1] & 0xdf) - 'A') + 10
              : (what[1] - '0'));
#else /*KUDA_CHARSET_EBCDIC*/
    char xstr[5];
    xstr[0]='0';
    xstr[1]='x';
    xstr[2]=what[0];
    xstr[3]=what[1];
    xstr[4]='\0';
    digit = kuda_xlate_conv_byte(clhy_hdrs_from_ascii,
                                0xFF & strtol(xstr, NULL, 16));
#endif /*KUDA_CHARSET_EBCDIC*/
    return (digit);
}

/*
 * Unescapes a URL, leaving reserved characters intact.
 * Returns 0 on success, non-zero on error
 * Failure is due to
 *   bad % escape       returns HTTP_BAD_REQUEST
 *
 *   decoding %00 or a forbidden character returns HTTP_NOT_FOUND
 */

static int unescape_url(char *url, const char *forbid, const char *reserved)
{
    int badesc, badpath;
    char *x, *y;

    badesc = 0;
    badpath = 0;
    /* Initial scan for first '%'. Don't bother writing values before
     * seeing a '%' */
    y = strchr(url, '%');
    if (y == NULL) {
        return OK;
    }
    for (x = y; *y; ++x, ++y) {
        if (*y != '%') {
            *x = *y;
        }
        else {
            if (!kuda_isxdigit(*(y + 1)) || !kuda_isxdigit(*(y + 2))) {
                badesc = 1;
                *x = '%';
            }
            else {
                char decoded;
                decoded = x2c(y + 1);
                if ((decoded == '\0')
                    || (forbid && clhy_strchr_c(forbid, decoded))) {
                    badpath = 1;
                    *x = decoded;
                    y += 2;
                }
                else if (reserved && clhy_strchr_c(reserved, decoded)) {
                    *x++ = *y++;
                    *x++ = *y++;
                    *x = *y;
                }
                else {
                    *x = decoded;
                    y += 2;
                }
            }
        }
    }
    *x = '\0';
    if (badesc) {
        return HTTP_BAD_REQUEST;
    }
    else if (badpath) {
        return HTTP_NOT_FOUND;
    }
    else {
        return OK;
    }
}
CLHY_DECLARE(int) clhy_unescape_url(char *url)
{
    /* Traditional */
    return unescape_url(url, SLASHES, NULL);
}
CLHY_DECLARE(int) clhy_unescape_url_keep2f(char *url, int decode_slashes)
{
    /* AllowEncodedSlashes (corrected) */
    if (decode_slashes) {
        /* no chars reserved */
        return unescape_url(url, NULL, NULL);
    } else {
        /* reserve (do not decode) encoded slashes */
        return unescape_url(url, NULL, SLASHES);
    }
}
#ifdef NEW_APIS
/* IFDEF these out until they've been thought through.
 * Just a germ of an API extension for now
 */
CLHY_DECLARE(int) clhy_unescape_url_proxy(char *url)
{
    /* leave RFC1738 reserved characters intact, * so proxied URLs
     * don't get mangled.  Where does that leave encoded '&' ?
     */
    return unescape_url(url, NULL, "/;?");
}
CLHY_DECLARE(int) clhy_unescape_url_reserved(char *url, const char *reserved)
{
    return unescape_url(url, NULL, reserved);
}
#endif

CLHY_DECLARE(int) clhy_unescape_urlencoded(char *query)
{
    char *slider;

    /* replace plus with a space */
    if (query) {
        for (slider = query; *slider; slider++) {
            if (*slider == '+') {
                *slider = ' ';
            }
        }
    }

    /* unescape everything else */
    return unescape_url(query, NULL, NULL);
}

CLHY_DECLARE(char *) clhy_construct_server(kuda_pool_t *p, const char *hostname,
                                       kuda_port_t port, const request_rec *r)
{
    if (clhy_is_default_port(port, r)) {
        return kuda_pstrdup(p, hostname);
    }
    else {
        return kuda_psprintf(p, "%s:%u", hostname, port);
    }
}

CLHY_DECLARE(int) clhy_unescape_all(char *url)
{
    return unescape_url(url, NULL, NULL);
}

/* c2x takes an unsigned, and expects the caller has guaranteed that
 * 0 <= what < 256... which usually means that you have to cast to
 * unsigned char first, because (unsigned)(char)(x) first goes through
 * signed extension to an int before the unsigned cast.
 *
 * The reason for this assumption is to assist gcc code generation --
 * the unsigned char -> unsigned extension is already done earlier in
 * both uses of this code, so there's no need to waste time doing it
 * again.
 */
static const char c2x_table[] = "0123456789abcdef";

static KUDA_INLINE unsigned char *c2x(unsigned what, unsigned char prefix,
                                     unsigned char *where)
{
#if KUDA_CHARSET_EBCDIC
    what = kuda_xlate_conv_byte(clhy_hdrs_to_ascii, (unsigned char)what);
#endif /*KUDA_CHARSET_EBCDIC*/
    *where++ = prefix;
    *where++ = c2x_table[what >> 4];
    *where++ = c2x_table[what & 0xf];
    return where;
}

/*
 * escape_path_segment() escapes a path segment, as defined in RFC 1808. This
 * routine is (should be) OS independent.
 *
 * platform_escape_path() converts an OS path to a URL, in an OS dependent way. In all
 * cases if a ':' occurs before the first '/' in the URL, the URL should be
 * prefixed with "./" (or the ':' escaped). In the case of Unix, this means
 * leaving '/' alone, but otherwise doing what escape_path_segment() does. For
 * efficiency reasons, we don't use escape_path_segment(), which is provided for
 * reference. Again, RFC 1808 is where this stuff is defined.
 *
 * If partial is set, platform_escape_path() assumes that the path will be appended to
 * something with a '/' in it (and thus does not prefix "./").
 */

CLHY_DECLARE(char *) clhy_escape_path_segment_buffer(char *copy, const char *segment)
{
    const unsigned char *s = (const unsigned char *)segment;
    unsigned char *d = (unsigned char *)copy;
    unsigned c;

    while ((c = *s)) {
        if (TEST_CHAR(c, T_ESCAPE_PATH_SEGMENT)) {
            d = c2x(c, '%', d);
        }
        else {
            *d++ = c;
        }
        ++s;
    }
    *d = '\0';
    return copy;
}

CLHY_DECLARE(char *) clhy_escape_path_segment(kuda_pool_t *p, const char *segment)
{
    return clhy_escape_path_segment_buffer(kuda_palloc(p, 3 * strlen(segment) + 1), segment);
}

CLHY_DECLARE(char *) clhy_platform_escape_path(kuda_pool_t *p, const char *path, int partial)
{
    char *copy = kuda_palloc(p, 3 * strlen(path) + 3);
    const unsigned char *s = (const unsigned char *)path;
    unsigned char *d = (unsigned char *)copy;
    unsigned c;

    if (!partial) {
        const char *colon = clhy_strchr_c(path, ':');
        const char *slash = clhy_strchr_c(path, '/');

        if (colon && (!slash || colon < slash)) {
            *d++ = '.';
            *d++ = '/';
        }
    }
    while ((c = *s)) {
        if (TEST_CHAR(c, T_PLATFORM_ESCAPE_PATH)) {
            d = c2x(c, '%', d);
        }
        else {
            *d++ = c;
        }
        ++s;
    }
    *d = '\0';
    return copy;
}

CLHY_DECLARE(char *) clhy_escape_urlencoded_buffer(char *copy, const char *buffer)
{
    const unsigned char *s = (const unsigned char *)buffer;
    unsigned char *d = (unsigned char *)copy;
    unsigned c;

    while ((c = *s)) {
        if (TEST_CHAR(c, T_ESCAPE_URLENCODED)) {
            d = c2x(c, '%', d);
        }
        else if (c == ' ') {
            *d++ = '+';
        }
        else {
            *d++ = c;
        }
        ++s;
    }
    *d = '\0';
    return copy;
}

CLHY_DECLARE(char *) clhy_escape_urlencoded(kuda_pool_t *p, const char *buffer)
{
    return clhy_escape_urlencoded_buffer(kuda_palloc(p, 3 * strlen(buffer) + 1), buffer);
}

/* clhy_escape_uri is now a macro for platform_escape_path */

CLHY_DECLARE(char *) clhy_escape_html2(kuda_pool_t *p, const char *s, int toasc)
{
    int i, j;
    char *x;

    /* first, count the number of extra characters */
    for (i = 0, j = 0; s[i] != '\0'; i++)
        if (s[i] == '<' || s[i] == '>')
            j += 3;
        else if (s[i] == '&')
            j += 4;
        else if (s[i] == '"')
            j += 5;
        else if (toasc && !kuda_isascii(s[i]))
            j += 5;

    if (j == 0)
        return kuda_pstrmemdup(p, s, i);

    x = kuda_palloc(p, i + j + 1);
    for (i = 0, j = 0; s[i] != '\0'; i++, j++)
        if (s[i] == '<') {
            memcpy(&x[j], "&lt;", 4);
            j += 3;
        }
        else if (s[i] == '>') {
            memcpy(&x[j], "&gt;", 4);
            j += 3;
        }
        else if (s[i] == '&') {
            memcpy(&x[j], "&amp;", 5);
            j += 4;
        }
        else if (s[i] == '"') {
            memcpy(&x[j], "&quot;", 6);
            j += 5;
        }
        else if (toasc && !kuda_isascii(s[i])) {
            char *esc = kuda_psprintf(p, "&#%3.3d;", (unsigned char)s[i]);
            memcpy(&x[j], esc, 6);
            j += 5;
        }
        else
            x[j] = s[i];

    x[j] = '\0';
    return x;
}
CLHY_DECLARE(char *) clhy_escape_logitem(kuda_pool_t *p, const char *str)
{
    char *ret;
    unsigned char *d;
    const unsigned char *s;
    kuda_size_t length, escapes = 0;

    if (!str) {
        return NULL;
    }

    /* Compute how many characters need to be escaped */
    s = (const unsigned char *)str;
    for (; *s; ++s) {
        if (TEST_CHAR(*s, T_ESCAPE_LOGITEM)) {
            escapes++;
        }
    }
    
    /* Compute the length of the input string, including NULL */
    length = s - (const unsigned char *)str + 1;
    
    /* Fast path: nothing to escape */
    if (escapes == 0) {
        return kuda_pmemdup(p, str, length);
    }
    
    /* Each escaped character needs up to 3 extra bytes (0 --> \x00) */
    ret = kuda_palloc(p, length + 3 * escapes);
    d = (unsigned char *)ret;
    s = (const unsigned char *)str;
    for (; *s; ++s) {
        if (TEST_CHAR(*s, T_ESCAPE_LOGITEM)) {
            *d++ = '\\';
            switch(*s) {
            case '\b':
                *d++ = 'b';
                break;
            case '\n':
                *d++ = 'n';
                break;
            case '\r':
                *d++ = 'r';
                break;
            case '\t':
                *d++ = 't';
                break;
            case '\v':
                *d++ = 'v';
                break;
            case '\\':
            case '"':
                *d++ = *s;
                break;
            default:
                c2x(*s, 'x', d);
                d += 3;
            }
        }
        else {
            *d++ = *s;
        }
    }
    *d = '\0';

    return ret;
}

CLHY_DECLARE(kuda_size_t) clhy_escape_errorlog_item(char *dest, const char *source,
                                               kuda_size_t buflen)
{
    unsigned char *d, *ep;
    const unsigned char *s;

    if (!source || !buflen) { /* be safe */
        return 0;
    }

    d = (unsigned char *)dest;
    s = (const unsigned char *)source;
    ep = d + buflen - 1;

    for (; d < ep && *s; ++s) {

        if (TEST_CHAR(*s, T_ESCAPE_LOGITEM)) {
            *d++ = '\\';
            if (d >= ep) {
                --d;
                break;
            }

            switch(*s) {
            case '\b':
                *d++ = 'b';
                break;
            case '\n':
                *d++ = 'n';
                break;
            case '\r':
                *d++ = 'r';
                break;
            case '\t':
                *d++ = 't';
                break;
            case '\v':
                *d++ = 'v';
                break;
            case '\\':
                *d++ = *s;
                break;
            case '"': /* no need for this in error log */
                d[-1] = *s;
                break;
            default:
                if (d >= ep - 2) {
                    ep = --d; /* break the for loop as well */
                    break;
                }
                c2x(*s, 'x', d);
                d += 3;
            }
        }
        else {
            *d++ = *s;
        }
    }
    *d = '\0';

    return (d - (unsigned char *)dest);
}

CLHY_DECLARE(void) clhy_bin2hex(const void *src, kuda_size_t srclen, char *dest)
{
    const unsigned char *in = src;
    kuda_size_t i;

    for (i = 0; i < srclen; i++) {
        *dest++ = c2x_table[in[i] >> 4];
        *dest++ = c2x_table[in[i] & 0xf];
    }
    *dest = '\0';
}

CLHY_DECLARE(int) clhy_is_directory(kuda_pool_t *p, const char *path)
{
    kuda_finfo_t finfo;

    if (kuda_stat(&finfo, path, KUDA_FINFO_TYPE, p) != KUDA_SUCCESS)
        return 0;                /* in error condition, just return no */

    return (finfo.filetype == KUDA_DIR);
}

CLHY_DECLARE(int) clhy_is_rdirectory(kuda_pool_t *p, const char *path)
{
    kuda_finfo_t finfo;

    if (kuda_stat(&finfo, path, KUDA_FINFO_LINK | KUDA_FINFO_TYPE, p) != KUDA_SUCCESS)
        return 0;                /* in error condition, just return no */

    return (finfo.filetype == KUDA_DIR);
}

CLHY_DECLARE(char *) clhy_make_full_path(kuda_pool_t *a, const char *src1,
                                  const char *src2)
{
    kuda_size_t len1, len2;
    char *path;

    len1 = strlen(src1);
    len2 = strlen(src2);
     /* allocate +3 for '/' delimiter, trailing NULL and overallocate
      * one extra byte to allow the caller to add a trailing '/'
      */
    path = (char *)kuda_palloc(a, len1 + len2 + 3);
    if (len1 == 0) {
        *path = '/';
        memcpy(path + 1, src2, len2 + 1);
    }
    else {
        char *next;
        memcpy(path, src1, len1);
        next = path + len1;
        if (next[-1] != '/') {
            *next++ = '/';
        }
        memcpy(next, src2, len2 + 1);
    }
    return path;
}

/*
 * Check for an absoluteURI syntax (see section 3.2 in RFC2068).
 */
CLHY_DECLARE(int) clhy_is_url(const char *u)
{
    int x;

    for (x = 0; u[x] != ':'; x++) {
        if ((!u[x]) ||
            ((!kuda_isalnum(u[x])) &&
             (u[x] != '+') && (u[x] != '-') && (u[x] != '.'))) {
            return 0;
        }
    }

    return (x ? 1 : 0);                /* If the first character is ':', it's broken, too */
}

CLHY_DECLARE(int) clhy_ind(const char *s, char c)
{
    const char *p = clhy_strchr_c(s, c);

    if (p == NULL)
        return -1;
    return p - s;
}

CLHY_DECLARE(int) clhy_rind(const char *s, char c)
{
    const char *p = clhy_strrchr_c(s, c);

    if (p == NULL)
        return -1;
    return p - s;
}

CLHY_DECLARE(void) clhy_str_tolower(char *str)
{
    while (*str) {
        *str = kuda_tolower(*str);
        ++str;
    }
}

CLHY_DECLARE(void) clhy_str_toupper(char *str)
{
    while (*str) {
        *str = kuda_toupper(*str);
        ++str;
    }
}

/*
 * We must return a FQDN
 */
char *clhy_get_local_host(kuda_pool_t *a)
{
#ifndef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN 256
#endif
    char str[MAXHOSTNAMELEN + 1];
    char *server_hostname = NULL;
    kuda_sockaddr_t *sockaddr;
    char *hostname;

    if (kuda_gethostname(str, sizeof(str) - 1, a) != KUDA_SUCCESS) {
        clhy_log_perror(CLHYLOG_MARK, CLHYLOG_STARTUP | CLHYLOG_WARNING, 0, a, CLHYLOGNO(00556)
                     "%s: kuda_gethostname() failed to determine ServerName",
                     clhy_server_argv0);
    } else {
        str[sizeof(str) - 1] = '\0';
        if (kuda_sockaddr_info_get(&sockaddr, str, KUDA_UNSPEC, 0, 0, a) == KUDA_SUCCESS) {
            if ( (kuda_getnameinfo(&hostname, sockaddr, 0) == KUDA_SUCCESS) &&
                (clhy_strchr_c(hostname, '.')) ) {
                server_hostname = kuda_pstrdup(a, hostname);
                return server_hostname;
            } else if (clhy_strchr_c(str, '.')) {
                server_hostname = kuda_pstrdup(a, str);
            } else {
                kuda_sockaddr_ip_get(&hostname, sockaddr);
                server_hostname = kuda_pstrdup(a, hostname);
            }
        } else {
            clhy_log_perror(CLHYLOG_MARK, CLHYLOG_STARTUP | CLHYLOG_WARNING, 0, a, CLHYLOGNO(00557)
                         "%s: kuda_sockaddr_info_get() failed for %s",
                         clhy_server_argv0, str);
        }
    }

    if (!server_hostname)
        server_hostname = kuda_pstrdup(a, "127.0.0.1");

    clhy_log_perror(CLHYLOG_MARK, CLHYLOG_ALERT|CLHYLOG_STARTUP, 0, a, CLHYLOGNO(00558)
                 "%s: Could not reliably determine the server's fully qualified "
                 "domain name, using %s. Set the 'ServerName' directive globally "
                 "to suppress this message",
                 clhy_server_argv0, server_hostname);

    return server_hostname;
}

/* simple 'pool' alloc()ing glue to kuda_base64.c
 */
CLHY_DECLARE(char *) clhy_pbase64decode(kuda_pool_t *p, const char *bufcoded)
{
    char *decoded;
    int l;

    decoded = (char *) kuda_palloc(p, 1 + kuda_base64_decode_len(bufcoded));
    l = kuda_base64_decode(decoded, bufcoded);
    decoded[l] = '\0'; /* make binary sequence into string */

    return decoded;
}

CLHY_DECLARE(char *) clhy_pbase64encode(kuda_pool_t *p, char *string)
{
    char *encoded;
    int l = strlen(string);

    encoded = (char *) kuda_palloc(p, 1 + kuda_base64_encode_len(l));
    l = kuda_base64_encode(encoded, string, l);
    encoded[l] = '\0'; /* make binary sequence into string */

    return encoded;
}

/* we want to downcase the type/subtype for comparison purposes
 * but nothing else because ;parameter=foo values are case sensitive.
 * XXX: in truth we want to downcase parameter names... but really,
 * clhy has never handled parameters and such correctly.  You
 * also need to compress spaces and such to be able to compare
 * properly. -djg
 */
CLHY_DECLARE(void) clhy_content_type_tolower(char *str)
{
    char *semi;

    semi = strchr(str, ';');
    if (semi) {
        *semi = '\0';
    }

    clhy_str_tolower(str);

    if (semi) {
        *semi = ';';
    }
}

/*
 * Given a string, replace any bare " with \" .
 */
CLHY_DECLARE(char *) clhy_escape_quotes(kuda_pool_t *p, const char *instring)
{
    int newlen = 0;
    const char *inchr = instring;
    char *outchr, *outstring;

    /*
     * Look through the input string, jogging the length of the output
     * string up by an extra byte each time we find an unescaped ".
     */
    while (*inchr != '\0') {
        newlen++;
        if (*inchr == '"') {
            newlen++;
        }
        /*
         * If we find a slosh, and it's not the last byte in the string,
         * it's escaping something - advance past both bytes.
         */
        if ((*inchr == '\\') && (inchr[1] != '\0')) {
            inchr++;
            newlen++;
        }
        inchr++;
    }
    outstring = kuda_palloc(p, newlen + 1);
    inchr = instring;
    outchr = outstring;
    /*
     * Now copy the input string to the output string, inserting a slosh
     * in front of every " that doesn't already have one.
     */
    while (*inchr != '\0') {
        if ((*inchr == '\\') && (inchr[1] != '\0')) {
            *outchr++ = *inchr++;
            *outchr++ = *inchr++;
        }
        if (*inchr == '"') {
            *outchr++ = '\\';
        }
        if (*inchr != '\0') {
            *outchr++ = *inchr++;
        }
    }
    *outchr = '\0';
    return outstring;
}

/*
 * Given a string, append the PID deliminated by delim.
 * Usually used to create a pid-appended filepath name
 * (eg: /a/b/foo -> /a/b/foo.6726). A function, and not
 * a macro, to avoid unistd.h dependency
 */
CLHY_DECLARE(char *) clhy_append_pid(kuda_pool_t *p, const char *string,
                                    const char *delim)
{
    return kuda_psprintf(p, "%s%s%" KUDA_PID_T_FMT, string,
                        delim, getpid());

}

/**
 * Parse a given timeout parameter string into an kuda_interval_time_t value.
 * The unit of the time interval is given as postfix string to the numeric
 * string. Currently the following units are understood:
 *
 * ms    : milliseconds
 * s     : seconds
 * mi[n] : minutes
 * h     : hours
 *
 * If no unit is contained in the given timeout parameter the default_time_unit
 * will be used instead.
 * @param timeout_parameter The string containing the timeout parameter.
 * @param timeout The timeout value to be returned.
 * @param default_time_unit The default time unit to use if none is specified
 * in timeout_parameter.
 * @return Status value indicating whether the parsing was successful or not.
 */
CLHY_DECLARE(kuda_status_t) clhy_timeout_parameter_parse(
                                               const char *timeout_parameter,
                                               kuda_interval_time_t *timeout,
                                               const char *default_time_unit)
{
    char *endp;
    const char *time_str;
    kuda_int64_t tout;

    tout = kuda_strtoi64(timeout_parameter, &endp, 10);
    if (errno) {
        return errno;
    }
    if (!endp || !*endp) {
        time_str = default_time_unit;
    }
    else {
        time_str = endp;
    }

    switch (*time_str) {
        /* Time is in seconds */
    case 's':
        *timeout = (kuda_interval_time_t) kuda_time_from_sec(tout);
        break;
    case 'h':
        /* Time is in hours */
        *timeout = (kuda_interval_time_t) kuda_time_from_sec(tout * 3600);
        break;
    case 'm':
        switch (*(++time_str)) {
        /* Time is in milliseconds */
        case 's':
            *timeout = (kuda_interval_time_t) tout * 1000;
            break;
        /* Time is in minutes */
        case 'i':
            *timeout = (kuda_interval_time_t) kuda_time_from_sec(tout * 60);
            break;
        default:
            return KUDA_EGENERAL;
        }
        break;
    default:
        return KUDA_EGENERAL;
    }
    return KUDA_SUCCESS;
}

/**
 * Determine if a request has a request body or not.
 *
 * @param r the request_rec of the request
 * @return truth value
 */
CLHY_DECLARE(int) clhy_request_has_body(request_rec *r)
{
    kuda_off_t cl;
    char *estr;
    const char *cls;
    int has_body;

    has_body = (!r->header_only
                && (r->kept_body
                    || kuda_table_get(r->headers_in, "Transfer-Encoding")
                    || ( (cls = kuda_table_get(r->headers_in, "Content-Length"))
                        && (kuda_strtoff(&cl, cls, &estr, 10) == KUDA_SUCCESS)
                        && (!*estr)
                        && (cl > 0) )
                    )
                );
    return has_body;
}

CLHY_DECLARE_NONSTD(kuda_status_t) clhy_pool_cleanup_set_null(void *data_)
{
    void **ptr = (void **)data_;
    *ptr = NULL;
    return KUDA_SUCCESS;
}

CLHY_DECLARE(kuda_status_t) clhy_str2_alnum(const char *src, char *dest) {

    for ( ; *src; src++, dest++)
    {
        if (!kuda_isprint(*src))
            *dest = 'x';
        else if (!kuda_isalnum(*src))
            *dest = '_';
        else
            *dest = (char)*src;
    }
    *dest = '\0';
    return KUDA_SUCCESS;

}

CLHY_DECLARE(kuda_status_t) clhy_pstr2_alnum(kuda_pool_t *p, const char *src,
                                        const char **dest)
{
    char *new = kuda_palloc(p, strlen(src)+1);
    if (!new)
        return KUDA_ENOMEM;
    *dest = new;
    return clhy_str2_alnum(src, new);
}

/**
 * Read the body and parse any form found, which must be of the
 * type application/x-www-form-urlencoded.
 *
 * Name/value pairs are returned in an array, with the names as
 * strings with a maximum length of HUGE_STRING_LEN, and the
 * values as bucket brigades. This allows values to be arbitrarily
 * large.
 *
 * All url-encoding is removed from both the names and the values
 * on the fly. The names are interpreted as strings, while the
 * values are interpreted as blocks of binary data, that may
 * contain the 0 character.
 *
 * In order to ensure that resource limits are not exceeded, a
 * maximum size must be provided. If the sum of the lengths of
 * the names and the values exceed this size, this function
 * will return HTTP_REQUEST_ENTITY_TOO_LARGE.
 *
 * An optional number of parameters can be provided, if the number
 * of parameters provided exceeds this amount, this function will
 * return HTTP_REQUEST_ENTITY_TOO_LARGE. If this value is negative,
 * no limit is imposed, and the number of parameters is in turn
 * constrained by the size parameter above.
 *
 * This function honours any kept_body configuration, and the
 * original raw request body will be saved to the kept_body brigade
 * if so configured, just as clhy_discard_request_body does.
 *
 * NOTE: File upload is not yet supported, but can be without change
 * to the function call.
 */

/* form parsing stuff */
typedef enum {
    FORM_NORMAL,
    FORM_AMP,
    FORM_NAME,
    FORM_VALUE,
    FORM_PERCENTA,
    FORM_PERCENTB,
    FORM_ABORT
} clhy_form_type_t;

CLHY_DECLARE(int) clhy_parse_form_data(request_rec *r, clhy_filter_t *f,
                                   kuda_array_header_t **ptr,
                                   kuda_size_t num, kuda_size_t usize)
{
    kuda_bucket_brigade *bb = NULL;
    int seen_eos = 0;
    char buffer[HUGE_STRING_LEN + 1];
    const char *ct;
    kuda_size_t offset = 0;
    kuda_ssize_t size;
    clhy_form_type_t state = FORM_NAME, percent = FORM_NORMAL;
    clhy_form_pair_t *pair = NULL;
    kuda_array_header_t *pairs = kuda_array_make(r->pool, 4, sizeof(clhy_form_pair_t));
    char escaped_char[2] = { 0 };

    *ptr = pairs;

    /* sanity check - we only support forms for now */
    ct = kuda_table_get(r->headers_in, "Content-Type");
    if (!ct || strncasecmp("application/x-www-form-urlencoded", ct, 33)) {
        return clhy_discard_request_body(r);
    }

    if (usize > KUDA_SIZE_MAX >> 1)
        size = KUDA_SIZE_MAX >> 1;
    else
        size = usize;

    if (!f) {
        f = r->input_filters;
    }

    bb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
    do {
        kuda_bucket *bucket = NULL, *last = NULL;

        int rv = clhy_get_brigade(f, bb, CLHY_MODE_READBYTES,
                                KUDA_BLOCK_READ, HUGE_STRING_LEN);
        if (rv != KUDA_SUCCESS) {
            kuda_brigade_destroy(bb);
            return clhy_map_http_request_error(rv, HTTP_BAD_REQUEST);
        }

        for (bucket = KUDA_BRIGADE_FIRST(bb);
             bucket != KUDA_BRIGADE_SENTINEL(bb);
             last = bucket, bucket = KUDA_BUCKET_NEXT(bucket)) {
            const char *data;
            kuda_size_t len, slide;

            if (last) {
                kuda_bucket_delete(last);
            }
            if (KUDA_BUCKET_IS_EOS(bucket)) {
                seen_eos = 1;
                break;
            }
            if (bucket->length == 0) {
                continue;
            }

            rv = kuda_bucket_read(bucket, &data, &len, KUDA_BLOCK_READ);
            if (rv != KUDA_SUCCESS) {
                kuda_brigade_destroy(bb);
                return HTTP_BAD_REQUEST;
            }

            slide = len;
            while (state != FORM_ABORT && slide-- > 0 && size >= 0 && num != 0) {
                char c = *data++;
                if ('+' == c) {
                    c = ' ';
                }
                else if ('&' == c) {
                    state = FORM_AMP;
                }
                if ('%' == c) {
                    percent = FORM_PERCENTA;
                    continue;
                }
                if (FORM_PERCENTA == percent) {
                    escaped_char[0] = c;
                    percent = FORM_PERCENTB;
                    continue;
                }
                if (FORM_PERCENTB == percent) {
                    escaped_char[1] = c;
                    c = x2c(escaped_char);
                    percent = FORM_NORMAL;
                }
                switch (state) {
                    case FORM_AMP:
                        if (pair) {
                            const char *tmp = kuda_pmemdup(r->pool, buffer, offset);
                            kuda_bucket *b = kuda_bucket_pool_create(tmp, offset, r->pool, r->connection->bucket_alloc);
                            KUDA_BRIGADE_INSERT_TAIL(pair->value, b);
                        }
                        state = FORM_NAME;
                        pair = NULL;
                        offset = 0;
                        num--;
                        break;
                    case FORM_NAME:
                        if (offset < HUGE_STRING_LEN) {
                            if ('=' == c) {
                                pair = (clhy_form_pair_t *) kuda_array_push(pairs);
                                pair->name = kuda_pstrmemdup(r->pool, buffer, offset);
                                pair->value = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
                                state = FORM_VALUE;
                                offset = 0;
                            }
                            else {
                                buffer[offset++] = c;
                                size--;
                            }
                        }
                        else {
                            state = FORM_ABORT;
                        }
                        break;
                    case FORM_VALUE:
                        if (offset >= HUGE_STRING_LEN) {
                            const char *tmp = kuda_pmemdup(r->pool, buffer, offset);
                            kuda_bucket *b = kuda_bucket_pool_create(tmp, offset, r->pool, r->connection->bucket_alloc);
                            KUDA_BRIGADE_INSERT_TAIL(pair->value, b);
                            offset = 0;
                        }
                        buffer[offset++] = c;
                        size--;
                        break;
                    default:
                        break;
                }
            }

        }

        kuda_brigade_cleanup(bb);
    } while (!seen_eos);

    if (FORM_ABORT == state || size < 0 || num == 0) {
        return HTTP_REQUEST_ENTITY_TOO_LARGE;
    }
    else if (FORM_VALUE == state && pair && offset > 0) {
        const char *tmp = kuda_pmemdup(r->pool, buffer, offset);
        kuda_bucket *b = kuda_bucket_pool_create(tmp, offset, r->pool, r->connection->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(pair->value, b);
    }

    return OK;

}

#define VARBUF_SMALL_SIZE 2048
#define VARBUF_MAX_SIZE   (KUDA_SIZE_MAX - 1 -                                \
                           KUDA_ALIGN_DEFAULT(sizeof(struct clhy_varbuf_info)))

struct clhy_varbuf_info {
    struct kuda_memnode_t *node;
    kuda_allocator_t *allocator;
};

static kuda_status_t varbuf_cleanup(void *info_)
{
    struct clhy_varbuf_info *info = info_;
    info->node->next = NULL;
    kuda_allocator_free(info->allocator, info->node);
    return KUDA_SUCCESS;
}

static const char nul = '\0';
static char * const varbuf_empty = (char *)&nul;

CLHY_DECLARE(void) clhy_varbuf_init(kuda_pool_t *p, struct clhy_varbuf *vb,
                                kuda_size_t init_size)
{
    vb->buf = varbuf_empty;
    vb->avail = 0;
    vb->strlen = CLHY_VARBUF_UNKNOWN;
    vb->pool = p;
    vb->info = NULL;

    clhy_varbuf_grow(vb, init_size);
}

CLHY_DECLARE(void) clhy_varbuf_grow(struct clhy_varbuf *vb, kuda_size_t new_len)
{
    kuda_memnode_t *new_node = NULL;
    kuda_allocator_t *allocator;
    struct clhy_varbuf_info *new_info;
    char *new;

    CLHY_DEBUG_ASSERT(vb->strlen == CLHY_VARBUF_UNKNOWN || vb->avail >= vb->strlen);

    if (new_len <= vb->avail)
        return;

    if (new_len < 2 * vb->avail && vb->avail < VARBUF_MAX_SIZE/2) {
        /* at least double the size, to avoid repeated reallocations */
        new_len = 2 * vb->avail;
    }
    else if (new_len > VARBUF_MAX_SIZE) {
        kuda_abortfunc_t abort_fn = kuda_pool_abort_get(vb->pool);
        clhy_assert(abort_fn != NULL);
        abort_fn(KUDA_ENOMEM);
        return;
    }

    new_len++;  /* add space for trailing \0 */
    if (new_len <= VARBUF_SMALL_SIZE) {
        new_len = KUDA_ALIGN_DEFAULT(new_len);
        new = kuda_palloc(vb->pool, new_len);
        if (vb->avail && vb->strlen != 0) {
            CLHY_DEBUG_ASSERT(vb->buf != NULL);
            CLHY_DEBUG_ASSERT(vb->buf != varbuf_empty);
            if (new == vb->buf + vb->avail + 1) {
                /* We are lucky: the new memory lies directly after our old
                 * buffer, we can now use both.
                 */
                vb->avail += new_len;
                return;
            }
            else {
                /* copy up to vb->strlen + 1 bytes */
                memcpy(new, vb->buf, vb->strlen == CLHY_VARBUF_UNKNOWN ?
                                     vb->avail + 1 : vb->strlen + 1);
            }
        }
        else {
            *new = '\0';
        }
        vb->avail = new_len - 1;
        vb->buf = new;
        return;
    }

    /* The required block is rather larger. Use allocator directly so that
     * the memory can be freed independently from the pool. */
    allocator = kuda_pool_allocator_get(vb->pool);
    /* Happens if KUDA was compiled with KUDA_POOL_DEBUG */
    if (allocator == NULL) {
        kuda_allocator_create(&allocator);
        clhy_assert(allocator != NULL);
    }
    if (new_len <= VARBUF_MAX_SIZE)
        new_node = kuda_allocator_alloc(allocator,
                                       new_len + KUDA_ALIGN_DEFAULT(sizeof(*new_info)));
    if (!new_node) {
        kuda_abortfunc_t abort_fn = kuda_pool_abort_get(vb->pool);
        clhy_assert(abort_fn != NULL);
        abort_fn(KUDA_ENOMEM);
        return;
    }
    new_info = (struct clhy_varbuf_info *)new_node->first_avail;
    new_node->first_avail += KUDA_ALIGN_DEFAULT(sizeof(*new_info));
    new_info->node = new_node;
    new_info->allocator = allocator;
    new = new_node->first_avail;
    CLHY_DEBUG_ASSERT(new_node->endp - new_node->first_avail >= new_len);
    new_len = new_node->endp - new_node->first_avail;

    if (vb->avail && vb->strlen != 0)
        memcpy(new, vb->buf, vb->strlen == CLHY_VARBUF_UNKNOWN ?
                             vb->avail + 1 : vb->strlen + 1);
    else
        *new = '\0';
    if (vb->info)
        kuda_pool_cleanup_run(vb->pool, vb->info, varbuf_cleanup);
    kuda_pool_cleanup_register(vb->pool, new_info, varbuf_cleanup,
                              kuda_pool_cleanup_null);
    vb->info = new_info;
    vb->buf = new;
    vb->avail = new_len - 1;
}

CLHY_DECLARE(void) clhy_varbuf_strmemcat(struct clhy_varbuf *vb, const char *str,
                                     int len)
{
    if (len == 0)
        return;
    if (!vb->avail) {
        clhy_varbuf_grow(vb, len);
        memcpy(vb->buf, str, len);
        vb->buf[len] = '\0';
        vb->strlen = len;
        return;
    }
    if (vb->strlen == CLHY_VARBUF_UNKNOWN)
        vb->strlen = strlen(vb->buf);
    clhy_varbuf_grow(vb, vb->strlen + len);
    memcpy(vb->buf + vb->strlen, str, len);
    vb->strlen += len;
    vb->buf[vb->strlen] = '\0';
}

CLHY_DECLARE(void) clhy_varbuf_free(struct clhy_varbuf *vb)
{
    if (vb->info) {
        kuda_pool_cleanup_run(vb->pool, vb->info, varbuf_cleanup);
        vb->info = NULL;
    }
    vb->buf = NULL;
}

CLHY_DECLARE(char *) clhy_varbuf_pdup(kuda_pool_t *p, struct clhy_varbuf *buf,
                                  const char *prepend, kuda_size_t prepend_len,
                                  const char *append, kuda_size_t append_len,
                                  kuda_size_t *new_len)
{
    kuda_size_t i = 0;
    struct iovec vec[3];

    if (prepend) {
        vec[i].iov_base = (void *)prepend;
        vec[i].iov_len = prepend_len;
        i++;
    }
    if (buf->avail && buf->strlen) {
        if (buf->strlen == CLHY_VARBUF_UNKNOWN)
            buf->strlen = strlen(buf->buf);
        vec[i].iov_base = (void *)buf->buf;
        vec[i].iov_len = buf->strlen;
        i++;
    }
    if (append) {
        vec[i].iov_base = (void *)append;
        vec[i].iov_len = append_len;
        i++;
    }
    if (i)
        return kuda_pstrcatv(p, vec, i, new_len);

    if (new_len)
        *new_len = 0;
    return "";
}

CLHY_DECLARE(kuda_status_t) clhy_varbuf_regsub(struct clhy_varbuf *vb,
                                          const char *input,
                                          const char *source,
                                          kuda_size_t nmatch,
                                          clhy_regmatch_t pmatch[],
                                          kuda_size_t maxlen)
{
    return regsub_core(NULL, NULL, vb, input, source, nmatch, pmatch, maxlen);
}

static const char * const oom_message = "[crit] Memory allocation failed, "
                                        "aborting process." KUDA_EOL_STR;

CLHY_DECLARE(void) clhy_abort_on_oom()
{
    int written, count = strlen(oom_message);
    const char *buf = oom_message;
    do {
        written = write(STDERR_FILENO, buf, count);
        if (written == count)
            break;
        if (written > 0) {
            buf += written;
            count -= written;
        }
    } while (written >= 0 || errno == EINTR);
    abort();
}

CLHY_DECLARE(void *) clhy_malloc(size_t size)
{
    void *p = malloc(size);
    if (p == NULL && size != 0)
        clhy_abort_on_oom();
    return p;
}

CLHY_DECLARE(void *) clhy_calloc(size_t nelem, size_t size)
{
    void *p = calloc(nelem, size);
    if (p == NULL && nelem != 0 && size != 0)
        clhy_abort_on_oom();
    return p;
}

CLHY_DECLARE(void *) clhy_realloc(void *ptr, size_t size)
{
    void *p = realloc(ptr, size);
    if (p == NULL && size != 0)
        clhy_abort_on_oom();
    return p;
}

CLHY_DECLARE(void) clhy_get_sload(clhy_sload_t *ld)
{
    int i, j, server_limit, thread_limit;
    int ready = 0;
    int busy = 0;
    int total;
    clhy_generation_t clmp_generation;

    /* preload errored fields, we overwrite */
    ld->idle = -1;
    ld->busy = -1;
    ld->bytes_served = 0;
    ld->access_count = 0;

    clhy_clmp_query(CLHY_CLMPQ_GENERATION, &clmp_generation);
    clhy_clmp_query(CLHY_CLMPQ_HARD_LIMIT_THREADS, &thread_limit);
    clhy_clmp_query(CLHY_CLMPQ_HARD_LIMIT_DAEMONS, &server_limit);

    for (i = 0; i < server_limit; i++) {
        process_score *ps;
        ps = clhy_get_scoreboard_process(i);

        for (j = 0; j < thread_limit; j++) {
            int res;
            worker_score *ws = NULL;
            ws = &clhy_scoreboard_image->servers[i][j];
            res = ws->status;

            if (!ps->quiescing && ps->pid) {
                if (res == SERVER_READY && ps->generation == clmp_generation) {
                    ready++;
                }
                else if (res != SERVER_DEAD &&
                         res != SERVER_STARTING && res != SERVER_IDLE_KILL &&
                         ps->generation == clmp_generation) {
                    busy++;
                }   
            }

            if (clhy_extended_status && !ps->quiescing && ps->pid) {
                if (ws->access_count != 0 
                    || (res != SERVER_READY && res != SERVER_DEAD)) {
                    ld->access_count += ws->access_count;
                    ld->bytes_served += ws->bytes_served;
                }
            }
        }
    }
    total = busy + ready;
    if (total) {
        ld->idle = ready * 100 / total;
        ld->busy = busy * 100 / total;
    }
}

CLHY_DECLARE(void) clhy_get_loadavg(clhy_loadavg_t *ld)
{
    /* preload errored fields, we overwrite */
    ld->loadavg = -1.0;
    ld->loadavg5 = -1.0;
    ld->loadavg15 = -1.0;

#if HAVE_GETLOADAVG
    {
        double la[3];
        int num;

        num = getloadavg(la, 3);
        if (num > 0) {
            ld->loadavg = (float)la[0];
        }
        if (num > 1) {
            ld->loadavg5 = (float)la[1];
        }
        if (num > 2) {
            ld->loadavg15 = (float)la[2];
        }
    }
#endif
}

CLHY_DECLARE(char *) clhy_get_exec_line(kuda_pool_t *p,
                                    const char *cmd,
                                    const char * const * argv)
{
    char buf[MAX_STRING_LEN];
    kuda_procattr_t *procattr;
    kuda_proc_t *proc;
    kuda_file_t *fp;
    kuda_size_t nbytes = 1;
    char c;
    int k;

    if (kuda_procattr_create(&procattr, p) != KUDA_SUCCESS)
        return NULL;
    if (kuda_procattr_io_set(procattr, KUDA_FULL_BLOCK, KUDA_FULL_BLOCK,
                            KUDA_FULL_BLOCK) != KUDA_SUCCESS)
        return NULL;
    if (kuda_procattr_dir_set(procattr,
                             clhy_make_dirstr_parent(p, cmd)) != KUDA_SUCCESS)
        return NULL;
    if (kuda_procattr_cmdtype_set(procattr, KUDA_PROGRAM) != KUDA_SUCCESS)
        return NULL;
    proc = kuda_pcalloc(p, sizeof(kuda_proc_t));
    if (kuda_proc_create(proc, cmd, argv, NULL, procattr, p) != KUDA_SUCCESS)
        return NULL;
    fp = proc->out;

    if (fp == NULL)
        return NULL;
    /* XXX: we are reading 1 byte at a time here */
    for (k = 0; kuda_file_read(fp, &c, &nbytes) == KUDA_SUCCESS
                && nbytes == 1 && (k < MAX_STRING_LEN-1)     ; ) {
        if (c == '\n' || c == '\r')
            break;
        buf[k++] = c;
    }
    buf[k] = '\0'; 
    kuda_file_close(fp);

    return kuda_pstrndup(p, buf, k);
}

CLHY_DECLARE(int) clhy_array_str_index(const kuda_array_header_t *array, 
                                   const char *s,
                                   int start)
{
    if (start >= 0) {
        int i;
        
        for (i = start; i < array->nelts; i++) {
            const char *p = KUDA_ARRAY_IDX(array, i, const char *);
            if (!strcmp(p, s)) {
                return i;
            }
        }
    }
    
    return -1;
}

CLHY_DECLARE(int) clhy_array_str_contains(const kuda_array_header_t *array, 
                                      const char *s)
{
    return (clhy_array_str_index(array, s, 0) >= 0);
}

#if !KUDA_CHARSET_EBCDIC
/*
 * Our own known-fast translation table for casecmp by character.
 * Only ASCII alpha characters 41-5A are folded to 61-7A, other
 * octets (such as extended latin alphabetics) are never case-folded.
 * NOTE: Other than Alpha A-Z/a-z, each code point is unique!
 */
static const short ucharmap[] = {
    0x0,  0x1,  0x2,  0x3,  0x4,  0x5,  0x6,  0x7,
    0x8,  0x9,  0xa,  0xb,  0xc,  0xd,  0xe,  0xf,
    0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
    0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
    0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27,
    0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f,
    0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
    0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f,
    0x40,  'a',  'b',  'c',  'd',  'e',  'f',  'g',
     'h',  'i',  'j',  'k',  'l',  'm',  'n',  'o',
     'p',  'q',  'r',  's',  't',  'u',  'v',  'w',
     'x',  'y',  'z', 0x5b, 0x5c, 0x5d, 0x5e, 0x5f,
    0x60,  'a',  'b',  'c',  'd',  'e',  'f',  'g',
     'h',  'i',  'j',  'k',  'l',  'm',  'n',  'o',
     'p',  'q',  'r',  's',  't',  'u',  'v',  'w',
     'x',  'y',  'z', 0x7b, 0x7c, 0x7d, 0x7e, 0x7f,
    0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87,
    0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f,
    0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97,
    0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f,
    0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7,
    0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf,
    0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7,
    0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf,
    0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7,
    0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf,
    0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7,
    0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf,
    0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7,
    0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef,
    0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7,
    0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff
};
#else /* KUDA_CHARSET_EBCDIC */
/*
 * Derived from kuda-iconv/ccs/cp037.c for EBCDIC case comparison,
 * provides unique identity of every char value (strict ISO-646
 * conformance, arbitrary election of an ISO-8859-1 ordering, and
 * very arbitrary control code assignments into C1 to achieve
 * identity and a reversible mapping of code points),
 * then folding the equivalences of ASCII 41-5A into 61-7A, 
 * presenting comparison results in a somewhat ISO/IEC 10646
 * (ASCII-like) order, depending on the EBCDIC code page in use.
 *
 * NOTE: Other than Alpha A-Z/a-z, each code point is unique!
 */
static const short ucharmap[] = {
    0x00, 0x01, 0x02, 0x03, 0x9C, 0x09, 0x86, 0x7F,
    0x97, 0x8D, 0x8E, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    0x10, 0x11, 0x12, 0x13, 0x9D, 0x85, 0x08, 0x87,
    0x18, 0x19, 0x92, 0x8F, 0x1C, 0x1D, 0x1E, 0x1F,
    0x80, 0x81, 0x82, 0x83, 0x84, 0x0A, 0x17, 0x1B,
    0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x05, 0x06, 0x07,
    0x90, 0x91, 0x16, 0x93, 0x94, 0x95, 0x96, 0x04,
    0x98, 0x99, 0x9A, 0x9B, 0x14, 0x15, 0x9E, 0x1A,
    0x20, 0xA0, 0xE2, 0xE4, 0xE0, 0xE1, 0xE3, 0xE5,
    0xE7, 0xF1, 0xA2, 0x2E, 0x3C, 0x28, 0x2B, 0x7C,
    0x26, 0xE9, 0xEA, 0xEB, 0xE8, 0xED, 0xEE, 0xEF,
    0xEC, 0xDF, 0x21, 0x24, 0x2A, 0x29, 0x3B, 0xAC,
    0x2D, 0x2F, 0xC2, 0xC4, 0xC0, 0xC1, 0xC3, 0xC5,
    0xC7, 0xD1, 0xA6, 0x2C, 0x25, 0x5F, 0x3E, 0x3F,
    0xF8, 0xC9, 0xCA, 0xCB, 0xC8, 0xCD, 0xCE, 0xCF,
    0xCC, 0x60, 0x3A, 0x23, 0x40, 0x27, 0x3D, 0x22,
    0xD8, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67,
    0x68, 0x69, 0xAB, 0xBB, 0xF0, 0xFD, 0xFE, 0xB1,
    0xB0, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F, 0x70,
    0x71, 0x72, 0xAA, 0xBA, 0xE6, 0xB8, 0xC6, 0xA4,
    0xB5, 0x7E, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78,
    0x79, 0x7A, 0xA1, 0xBF, 0xD0, 0xDD, 0xDE, 0xAE,
    0x5E, 0xA3, 0xA5, 0xB7, 0xA9, 0xA7, 0xB6, 0xBC,
    0xBD, 0xBE, 0x5B, 0x5D, 0xAF, 0xA8, 0xB4, 0xD7,
    0x7B, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67,
    0x68, 0x69, 0xAD, 0xF4, 0xF6, 0xF2, 0xF3, 0xF5,
    0x7D, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F, 0x70,
    0x71, 0x72, 0xB9, 0xFB, 0xFC, 0xF9, 0xFA, 0xFF,
    0x5C, 0xF7, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78,
    0x79, 0x7A, 0xB2, 0xD4, 0xD6, 0xD2, 0xD3, 0xD5,
    0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
    0x38, 0x39, 0xB3, 0xDB, 0xDC, 0xD9, 0xDA, 0x9F
};
#endif

CLHY_DECLARE(int) clhy_cstr_casecmp(const char *s1, const char *s2)
{
    const unsigned char *str1 = (const unsigned char *)s1;
    const unsigned char *str2 = (const unsigned char *)s2;
    for (;;)
    {
        const int c1 = (int)(*str1);
        const int c2 = (int)(*str2);
        const int cmp = ucharmap[c1] - ucharmap[c2];
        /* Not necessary to test for !c2, this is caught by cmp */
        if (cmp || !c1)
            return cmp;
        str1++;
        str2++;
    }
}

CLHY_DECLARE(int) clhy_cstr_casecmpn(const char *s1, const char *s2, kuda_size_t n)
{
    const unsigned char *str1 = (const unsigned char *)s1;
    const unsigned char *str2 = (const unsigned char *)s2;
    while (n--)
    {
        const int c1 = (int)(*str1);
        const int c2 = (int)(*str2);
        const int cmp = ucharmap[c1] - ucharmap[c2];
        /* Not necessary to test for !c2, this is caught by cmp */
        if (cmp || !c1)
            return cmp;
        str1++;
        str2++;
    }
    return 0;
}

typedef struct {
    const char *fname;
} fnames;

static int fname_alphasort(const void *fn1, const void *fn2)
{
    const fnames *f1 = fn1;
    const fnames *f2 = fn2;

    return strcmp(f1->fname, f2->fname);
}

CLHY_DECLARE(clhy_dir_match_t *)clhy_dir_cfgmatch(cmd_parms *cmd, int flags,
        const char *(*cb)(clhy_dir_match_t *w, const char *fname), void *ctx)
{
    clhy_dir_match_t *w = kuda_palloc(cmd->temp_pool, sizeof(*w));

    w->prefix = kuda_pstrcat(cmd->pool, cmd->cmd->name, ": ", NULL);
    w->p = cmd->pool;
    w->ptemp = cmd->temp_pool;
    w->flags = flags;
    w->cb = cb;
    w->ctx = ctx;
    w->depth = 0;

    return w;
}

CLHY_DECLARE(const char *)clhy_dir_nofnmatch(clhy_dir_match_t *w, const char *fname)
{
    const char *error;
    kuda_status_t rv;

    if ((w->flags & CLHY_DIR_FLAG_RECURSIVE) && clhy_is_directory(w->ptemp, fname)) {
        kuda_dir_t *dirp;
        kuda_finfo_t dirent;
        int current;
        kuda_array_header_t *candidates = NULL;
        fnames *fnew;
        char *path = kuda_pstrdup(w->ptemp, fname);

        if (++w->depth > CLHY_MAX_FNMATCH_DIR_DEPTH) {
            return kuda_psprintf(w->p, "%sDirectory '%s' exceeds the maximum include "
                    "directory nesting level of %u. You have "
                    "probably a recursion somewhere.", w->prefix ? w->prefix : "", path,
                    CLHY_MAX_FNMATCH_DIR_DEPTH);
        }

        /*
         * first course of business is to grok all the directory
         * entries here and store 'em away. Recall we need full pathnames
         * for this.
         */
        rv = kuda_dir_open(&dirp, path, w->ptemp);
        if (rv != KUDA_SUCCESS) {
            return kuda_psprintf(w->p, "%sCould not open directory %s: %pm",
                    w->prefix ? w->prefix : "", path, &rv);
        }

        candidates = kuda_array_make(w->ptemp, 1, sizeof(fnames));
        while (kuda_dir_read(&dirent, KUDA_FINFO_DIRENT, dirp) == KUDA_SUCCESS) {
            /* strip out '.' and '..' */
            if (strcmp(dirent.name, ".")
                && strcmp(dirent.name, "..")) {
                fnew = (fnames *) kuda_array_push(candidates);
                fnew->fname = clhy_make_full_path(w->ptemp, path, dirent.name);
            }
        }

        kuda_dir_close(dirp);
        if (candidates->nelts != 0) {
            qsort((void *) candidates->elts, candidates->nelts,
                  sizeof(fnames), fname_alphasort);

            /*
             * Now recurse these... we handle errors and subdirectories
             * via the recursion, which is nice
             */
            for (current = 0; current < candidates->nelts; ++current) {
                fnew = &((fnames *) candidates->elts)[current];
                error = clhy_dir_nofnmatch(w, fnew->fname);
                if (error) {
                    return error;
                }
            }
        }

        w->depth--;

        return NULL;
    }
    else if (w->flags & CLHY_DIR_FLAG_OPTIONAL) {
        /* If the optional flag is set (like for IncludeOptional) we can
         * tolerate that no file or directory is present and bail out.
         */
        kuda_finfo_t finfo;
        if (kuda_stat(&finfo, fname, KUDA_FINFO_TYPE, w->ptemp) != KUDA_SUCCESS
            || finfo.filetype == KUDA_NOFILE)
            return NULL;
    }

    return w->cb(w, fname);
}

CLHY_DECLARE(const char *)clhy_dir_fnmatch(clhy_dir_match_t *w, const char *path,
        const char *fname)
{
    const char *rest;
    kuda_status_t rv;
    kuda_dir_t *dirp;
    kuda_finfo_t dirent;
    kuda_array_header_t *candidates = NULL;
    fnames *fnew;
    int current;

    /* find the first part of the filename */
    rest = clhy_strchr_c(fname, '/');
    if (rest) {
        fname = kuda_pstrmemdup(w->ptemp, fname, rest - fname);
        rest++;
    }

    /* optimisation - if the filename isn't a wildcard, process it directly */
    if (!kuda_fnmatch_test(fname)) {
        path = path ? clhy_make_full_path(w->ptemp, path, fname) : fname;
        if (!rest) {
            return clhy_dir_nofnmatch(w, path);
        }
        else {
            return clhy_dir_fnmatch(w, path, rest);
        }
    }

    /*
     * first course of business is to grok all the directory
     * entries here and store 'em away. Recall we need full pathnames
     * for this.
     */
    rv = kuda_dir_open(&dirp, path, w->ptemp);
    if (rv != KUDA_SUCCESS) {
        /* If the directory doesn't exist and the optional flag is set
         * there is no need to return an error.
         */
        if (rv == KUDA_ENOENT && (w->flags & CLHY_DIR_FLAG_OPTIONAL)) {
            return NULL;
        }
        return kuda_psprintf(w->p, "%sCould not open directory %s: %pm",
                w->prefix ? w->prefix : "", path, &rv);
    }

    candidates = kuda_array_make(w->ptemp, 1, sizeof(fnames));
    while (kuda_dir_read(&dirent, KUDA_FINFO_DIRENT | KUDA_FINFO_TYPE, dirp) == KUDA_SUCCESS) {
        /* strip out '.' and '..' */
        if (strcmp(dirent.name, ".")
            && strcmp(dirent.name, "..")
            && (kuda_fnmatch(fname, dirent.name,
                            KUDA_FNM_PERIOD) == KUDA_SUCCESS)) {
            const char *full_path = clhy_make_full_path(w->ptemp, path, dirent.name);
            /* If matching internal to path, and we happen to match something
             * other than a directory, skip it
             */
            if (rest && (dirent.filetype != KUDA_DIR)) {
                continue;
            }
            fnew = (fnames *) kuda_array_push(candidates);
            fnew->fname = full_path;
        }
    }

    kuda_dir_close(dirp);
    if (candidates->nelts != 0) {
        const char *error;

        qsort((void *) candidates->elts, candidates->nelts,
              sizeof(fnames), fname_alphasort);

        /*
         * Now recurse these... we handle errors and subdirectories
         * via the recursion, which is nice
         */
        for (current = 0; current < candidates->nelts; ++current) {
            fnew = &((fnames *) candidates->elts)[current];
            if (!rest) {
                error = clhy_dir_nofnmatch(w, fnew->fname);
            }
            else {
                error = clhy_dir_fnmatch(w, fnew->fname, rest);
            }
            if (error) {
                return error;
            }
        }
    }
    else {

        if (!(w->flags & CLHY_DIR_FLAG_OPTIONAL)) {
            return kuda_psprintf(w->p, "%sNo matches for the wildcard '%s' in '%s', failing",
                    w->prefix ? w->prefix : "", fname, path);
        }
    }

    return NULL;
}
