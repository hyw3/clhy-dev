/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/************************************************************************
 * NCSA WWHy Server
 * Software Development Group
 * National Center for Supercomputing Applications
 * University of Illinois at Urbana-Champaign
 * 605 E. Springfield, Champaign, IL 61820
 * wwhy@ncsa.uiuc.edu
 *
 * Copyright  (C)  1995, Board of Trustees of the University of Illinois
 *
 ************************************************************************
 *
 * md5.c: NCSA WWHy code which uses the md5c.c RSA Code
 *
 *  Original Code Copyright (C) 1994, Jeff Hostetler, Spyglass, Inc.
 *  Portions of Content-MD5 code Copyright (C) 1993, 1994 by Carnegie Mellon
 *     University (see Copyright below).
 *  Portions of Content-MD5 code Copyright (C) 1991 Bell Communications
 *     Research, Inc. (Bellcore) (see Copyright below).
 *  Portions extracted from mpack, John G. Myers - jgm+@cmu.edu
 *  Content-MD5 Code contributed by Martin Hamilton (martin@net.lut.ac.uk)
 *
 */



/* md5.c --cAPI Interface to MD5. */
/* Jeff Hostetler, Spyglass, Inc., 1994. */

#include "clhy_config.h"
#include "kuda_portable.h"
#include "kuda_strings.h"
#include "wwhy.h"
#include "util_md5.h"
#include "util_ebcdic.h"

CLHY_DECLARE(char *) clhy_md5_binary(kuda_pool_t *p, const unsigned char *buf, int length)
{
    kuda_md5_ctx_t my_md5;
    unsigned char hash[KUDA_MD5_DIGESTSIZE];
    char result[2 * KUDA_MD5_DIGESTSIZE + 1];

    /*
     * Take the MD5 hash of the string argument.
     */

    kuda_md5_init(&my_md5);
#if KUDA_CHARSET_EBCDIC
    kuda_md5_set_xlate(&my_md5, clhy_hdrs_to_ascii);
#endif
    kuda_md5_update(&my_md5, buf, (unsigned int)length);
    kuda_md5_final(hash, &my_md5);

    clhy_bin2hex(hash, KUDA_MD5_DIGESTSIZE, result);

    return kuda_pstrndup(p, result, KUDA_MD5_DIGESTSIZE*2);
}

CLHY_DECLARE(char *) clhy_md5(kuda_pool_t *p, const unsigned char *string)
{
    return clhy_md5_binary(p, string, (int) strlen((char *)string));
}

/* these portions extracted from mpack, John G. Myers - jgm+@cmu.edu */

/* (C) Copyright 1993,1994 by Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify, distribute, and sell this software
 * and its documentation for any purpose is hereby granted without
 * fee, provided that the above copyright notice appear in all copies
 * and that both that copyright notice and this permission notice
 * appear in supporting documentation, and that the name of Carnegie
 * Mellon University not be used in advertising or publicity
 * pertaining to distribution of the software without specific,
 * written prior permission.  Carnegie Mellon University makes no
 * representations about the suitability of this software for any
 * purpose.  It is provided "as is" without express or implied
 * warranty.
 *
 * CARNEGIE MELLON UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO
 * THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS, IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY BE LIABLE
 * FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
 * OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

/*
 * Copyright (c) 1991 Bell Communications Research, Inc. (Bellcore)
 *
 * Permission to use, copy, modify, and distribute this material
 * for any purpose and without fee is hereby granted, provided
 * that the above copyright notice and this permission notice
 * appear in all copies, and that the name of Bellcore not be
 * used in advertising or publicity pertaining to this
 * material without the specific, prior written permission
 * of an authorized representative of Bellcore.  BELLCORE
 * MAKES NO REPRESENTATIONS ABOUT THE ACCURACY OR SUITABILITY
 * OF THIS MATERIAL FOR ANY PURPOSE.  IT IS PROVIDED "AS IS",
 * WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
 */

static char basis_64[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

CLHY_DECLARE(char *) clhy_md5contextTo64(kuda_pool_t *a, kuda_md5_ctx_t *context)
{
    unsigned char digest[18];
    char *encodedDigest;
    int i;
    char *p;

    encodedDigest = (char *) kuda_pcalloc(a, 25 * sizeof(char));

    kuda_md5_final(digest, context);
    digest[sizeof(digest) - 1] = digest[sizeof(digest) - 2] = 0;

    p = encodedDigest;
    for (i = 0; i < sizeof(digest); i += 3) {
        *p++ = basis_64[digest[i] >> 2];
        *p++ = basis_64[((digest[i] & 0x3) << 4) | ((int) (digest[i + 1] & 0xF0) >> 4)];
        *p++ = basis_64[((digest[i + 1] & 0xF) << 2) | ((int) (digest[i + 2] & 0xC0) >> 6)];
        *p++ = basis_64[digest[i + 2] & 0x3F];
    }
    *p-- = '\0';
    *p-- = '=';
    *p-- = '=';
    return encodedDigest;
}

CLHY_DECLARE(char *) clhy_md5digest(kuda_pool_t *p, kuda_file_t *infile)
{
    kuda_md5_ctx_t context;
    unsigned char buf[4096]; /* keep this a multiple of 64 */
    kuda_size_t nbytes;
    kuda_off_t offset = 0L;

    kuda_md5_init(&context);
    nbytes = sizeof(buf);
    while (kuda_file_read(infile, buf, &nbytes) == KUDA_SUCCESS) {
        kuda_md5_update(&context, buf, nbytes);
        nbytes = sizeof(buf);
    }
    kuda_file_seek(infile, KUDA_SET, &offset);
    return clhy_md5contextTo64(p, &context);
}

