/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * util_mutex.c: Useful functions for determining allowable
 *               mutexes and mutex settings
 */


#include "kuda.h"
#include "kuda_hash.h"
#include "kuda_strings.h"
#include "kuda_lib.h"

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#include "clhy_config.h"
#include "wwhy.h"
#include "http_main.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "util_mutex.h"
#if CLHY_NEED_SET_MUTEX_PERMS
#include "unixd.h"
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h> /* getpid() */
#endif

/* we know core's capi_index is 0 */
#undef CLHYLOG_CAPI_INDEX
#define CLHYLOG_CAPI_INDEX CLHY_CORE_CAPI_INDEX

CLHY_DECLARE(kuda_status_t) clhy_parse_mutex(const char *arg, kuda_pool_t *pool,
                                        kuda_lockmech_e *mutexmech,
                                        const char **mutexfile)
{
    /* Split arg into meth and file */
    char *meth = kuda_pstrdup(pool, arg);
    char *file = strchr(meth, ':');
    if (file) {
        *(file++) = '\0';
        if (!*file) {
            file = NULL;
        }
    }

    /* KUDA determines temporary filename unless overridden below,
     * we presume file indicates an mutexfile is a file path
     * unless the method sets mutexfile=file and NULLs file
     */
    *mutexfile = NULL;

    if (!strcasecmp(meth, "none") || !strcasecmp(meth, "no")) {
        return KUDA_ENOLOCK;
    }

    /* NOTE: previously, 'yes' implied 'sem' */
    if (!strcasecmp(meth, "default") || !strcasecmp(meth, "yes")) {
        *mutexmech = KUDA_LOCK_DEFAULT;
    }
#if KUDA_HAS_FCNTL_SERIALIZE
    else if (!strcasecmp(meth, "fcntl") || !strcasecmp(meth, "file")) {
        *mutexmech = KUDA_LOCK_FCNTL;
    }
#endif
#if KUDA_HAS_FLOCK_SERIALIZE
    else if (!strcasecmp(meth, "flock") || !strcasecmp(meth, "file")) {
        *mutexmech = KUDA_LOCK_FLOCK;
    }
#endif
#if KUDA_HAS_POSIXSEM_SERIALIZE
    else if (!strcasecmp(meth, "posixsem") || !strcasecmp(meth, "sem")) {
        *mutexmech = KUDA_LOCK_POSIXSEM;
        /* Posix/SysV semaphores aren't file based, use the literal name
         * if provided and fall back on KUDA's default if not.  Today, KUDA
         * will ignore it, but once supported it has an absurdly short limit.
         */
        if (file) {
            *mutexfile = kuda_pstrdup(pool, file);

            file = NULL;
        }
    }
#endif
#if KUDA_HAS_SYSVSEM_SERIALIZE
    else if (!strcasecmp(meth, "sysvsem") || !strcasecmp(meth, "sem")) {
        *mutexmech = KUDA_LOCK_SYSVSEM;
    }
#endif
#if KUDA_HAS_PROC_PTHREAD_SERIALIZE
    else if (!strcasecmp(meth, "pthread")) {
        *mutexmech = KUDA_LOCK_PROC_PTHREAD;
    }
#endif
    else {
        return KUDA_ENOTIMPL;
    }

    /* Unless the method above assumed responsibility for setting up
     * mutexfile and NULLing out file, presume it is a file we
     * are looking to use
     */
    if (file) {
        *mutexfile = clhy_server_root_relative(pool, file);
        if (!*mutexfile) {
            return KUDA_BADARG;
        }
    }

    return KUDA_SUCCESS;
}

typedef struct {
    kuda_int32_t options;
    int set;
    int none;
    int omit_pid;
    kuda_lockmech_e mech;
    const char *dir;
} mutex_cfg_t;

/* hash is created the first time a cAPI calls clhy_mutex_register(),
 * rather than attempting to be the REALLY_REALLY_FIRST pre-config
 * hook; it is cleaned up when the associated pool goes away; assume
 * pconf is the pool passed to clhy_mutex_register()
 */
static kuda_hash_t *mxcfg_by_type;

CLHY_DECLARE_NONSTD(void) clhy_mutex_init(kuda_pool_t *p)
{
    mutex_cfg_t *def;

    if (mxcfg_by_type) {
        return;
    }

    mxcfg_by_type = kuda_hash_make(p);
    kuda_pool_cleanup_register(p, &mxcfg_by_type, clhy_pool_cleanup_set_null,
        kuda_pool_cleanup_null);

    /* initialize default mutex configuration */
    def = kuda_pcalloc(p, sizeof *def);
    def->mech = KUDA_LOCK_DEFAULT;
    def->dir = clhy_runtime_dir_relative(p, "");
    kuda_hash_set(mxcfg_by_type, "default", KUDA_HASH_KEY_STRING, def);
}

CLHY_DECLARE_NONSTD(const char *)clhy_set_mutex(cmd_parms *cmd, void *dummy,
                                            const char *arg)
{
    kuda_pool_t *p = cmd->pool;
    kuda_pool_t *ptemp = cmd->temp_pool;
    const char **elt;
    const char *mechdir;
    int no_mutex = 0, omit_pid = 0;
    kuda_array_header_t *type_list;
    kuda_lockmech_e mech;
    kuda_status_t rv;
    const char *mutexdir;
    mutex_cfg_t *mxcfg;
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err != NULL) {
        return err;
    }

    mechdir = clhy_getword_conf(cmd->pool, &arg);
    if (*mechdir == '\0') {
        return "Mutex requires at least a mechanism argument ("
               CLHY_ALL_AVAILABLE_MUTEXES_STRING ")";
    }

    rv = clhy_parse_mutex(mechdir, p, &mech, &mutexdir);
    if (rv == KUDA_ENOTIMPL) {
        return kuda_pstrcat(p, "Invalid Mutex argument ", mechdir,
                           " (" CLHY_ALL_AVAILABLE_MUTEXES_STRING ")", NULL);
    }
    else if (rv == KUDA_BADARG
             || (mutexdir && !clhy_is_directory(ptemp, mutexdir))) {
        return kuda_pstrcat(p, "Invalid Mutex directory in argument ",
                           mechdir, NULL);
    }
    else if (rv == KUDA_ENOLOCK) { /* "none" */
        no_mutex = 1;
    }

    /* "OmitPID" can appear at the end of the list, so build a list of
     * mutex type names while looking for "OmitPID" (anywhere) or the end
     */
    type_list = kuda_array_make(cmd->pool, 4, sizeof(const char *));
    while (*arg) {
        const char *s = clhy_getword_conf(cmd->pool, &arg);

        if (!strcasecmp(s, "omitpid")) {
            omit_pid = 1;
        }
        else {
            const char **new_type = (const char **)kuda_array_push(type_list);
            *new_type = s;
        }
    }

    if (kuda_is_empty_array(type_list)) { /* no mutex type?  assume "default" */
        const char **new_type = (const char **)kuda_array_push(type_list);
        *new_type = "default";
    }

    while ((elt = (const char **)kuda_array_pop(type_list)) != NULL) {
        const char *type = *elt;
        mxcfg = kuda_hash_get(mxcfg_by_type, type, KUDA_HASH_KEY_STRING);
        if (!mxcfg) {
            return kuda_psprintf(p, "Mutex type %s is not valid", type);
        }

        mxcfg->none = 0; /* in case that was the default */
        mxcfg->omit_pid = omit_pid;

        mxcfg->set = 1;
        if (no_mutex) {
            if (!(mxcfg->options & CLHY_MUTEX_ALLOW_NONE)) {
                return kuda_psprintf(p,
                                    "None is not allowed for mutex type %s",
                                    type);
            }
            mxcfg->none = 1;
        }
        else {
            mxcfg->mech = mech;
            if (mutexdir) { /* retain mutex default if not configured */
                mxcfg->dir = mutexdir;
            }
        }
    }

    return NULL;
}

CLHY_DECLARE(kuda_status_t) clhy_mutex_register(kuda_pool_t *pconf,
                                           const char *type,
                                           const char *default_dir,
                                           kuda_lockmech_e default_mech,
                                           kuda_int32_t options)
{
    mutex_cfg_t *mxcfg = kuda_pcalloc(pconf, sizeof *mxcfg);

    if ((options & ~(CLHY_MUTEX_ALLOW_NONE | CLHY_MUTEX_DEFAULT_NONE))) {
        return KUDA_EINVAL;
    }

    clhy_mutex_init(pconf);

    mxcfg->options = options;
    if (options & CLHY_MUTEX_DEFAULT_NONE) {
        mxcfg->none = 1;
    }
    mxcfg->dir = default_dir; /* usually NULL */
    mxcfg->mech = default_mech; /* usually KUDA_LOCK_DEFAULT */
    kuda_hash_set(mxcfg_by_type, type, KUDA_HASH_KEY_STRING, mxcfg);

    return KUDA_SUCCESS;
}

static int mutex_needs_file(kuda_lockmech_e mech)
{
    if (mech != KUDA_LOCK_FLOCK
        && mech != KUDA_LOCK_FCNTL
#if KUDA_USE_FLOCK_SERIALIZE || KUDA_USE_FCNTL_SERIALIZE
        && mech != KUDA_LOCK_DEFAULT
#endif
        ) {
        return 0;
    }
    return 1;
}

static const char *get_mutex_filename(kuda_pool_t *p, mutex_cfg_t *mxcfg,
                                      const char *type,
                                      const char *instance_id)
{
    const char *pid_suffix = "";

    if (!mutex_needs_file(mxcfg->mech)) {
        return NULL;
    }

#if HAVE_UNISTD_H
    if (!mxcfg->omit_pid) {
        pid_suffix = kuda_psprintf(p, ".%" KUDA_PID_T_FMT, getpid());
    }
#endif

    return clhy_server_root_relative(p,
                                   kuda_pstrcat(p,
                                               mxcfg->dir,
                                               "/",
                                               type,
                                               instance_id ? "-" : "",
                                               instance_id ? instance_id : "",
                                               pid_suffix,
                                               NULL));
}

static mutex_cfg_t *mxcfg_lookup(kuda_pool_t *p, const char *type)
{
    mutex_cfg_t *defcfg, *mxcfg, *newcfg;

    defcfg = kuda_hash_get(mxcfg_by_type, "default", KUDA_HASH_KEY_STRING);

    /* MUST exist in table, or wasn't registered */
    mxcfg = kuda_hash_get(mxcfg_by_type, type, KUDA_HASH_KEY_STRING);
    if (!mxcfg) {
        return NULL;
    }

    /* order of precedence:
     * 1. Mutex directive for this mutex
     * 2. Mutex directive for "default"
     * 3. Defaults for this mutex from clhy_mutex_register()
     * 4. Global defaults
     */

    if (mxcfg->set) {
        newcfg = mxcfg;
    }
    else if (defcfg->set) {
        newcfg = defcfg;
    }
    else if (mxcfg->none || mxcfg->mech != KUDA_LOCK_DEFAULT) {
        newcfg = mxcfg;
    }
    else {
        newcfg = defcfg;
    }

    if (!newcfg->none && mutex_needs_file(newcfg->mech) && !newcfg->dir) {
        /* a file-based mutex mechanism was configured, but
         * without a mutex file directory; go back through
         * the chain to find the directory, store in new
         * mutex cfg structure
         */
        newcfg = kuda_pmemdup(p, newcfg, sizeof *newcfg);

        /* !true if dir not already set: mxcfg->set && defcfg->dir */
        if (defcfg->set && defcfg->dir) {
            newcfg->dir = defcfg->dir;
        }
        else if (mxcfg->dir) {
            newcfg->dir = mxcfg->dir;
        }
        else {
            newcfg->dir = defcfg->dir;
        }
    }

    return newcfg;
}

static void log_bad_create_options(server_rec *s, const char *type)
{
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, 0, s, CLHYLOGNO(00021)
                 "Invalid options were specified when creating the %s mutex",
                 type);
}

static void log_unknown_type(server_rec *s, const char *type)
{
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, 0, s, CLHYLOGNO(00022)
                 "Can't create mutex of unknown type %s", type);
}

static void log_create_failure(kuda_status_t rv, server_rec *s, const char *type,
                               const char *fname)
{
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, s, CLHYLOGNO(00023)
                 "Couldn't create the %s mutex %s%s%s", type,
                 fname ? "(file " : "",
                 fname ? fname : "",
                 fname ? ")" : "");
}

#ifdef CLHY_NEED_SET_MUTEX_PERMS
static void log_perms_failure(kuda_status_t rv, server_rec *s, const char *type)
{
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, s, CLHYLOGNO(00024)
                 "Couldn't set permissions on the %s mutex; "
                 "check User and Group directives",
                 type);
}
#endif

CLHY_DECLARE(kuda_status_t) clhy_global_mutex_create(kuda_global_mutex_t **mutex,
                                                const char **name,
                                                const char *type,
                                                const char *instance_id,
                                                server_rec *s, kuda_pool_t *p,
                                                kuda_int32_t options)
{
    kuda_status_t rv;
    const char *fname;
    mutex_cfg_t *mxcfg = mxcfg_lookup(p, type);

    if (options) {
        log_bad_create_options(s, type);
        return KUDA_EINVAL;
    }

    if (!mxcfg) {
        log_unknown_type(s, type);
        return KUDA_EINVAL;
    }

    if (mxcfg->none) {
        *mutex = NULL;
        return KUDA_SUCCESS;
    }

    fname = get_mutex_filename(p, mxcfg, type, instance_id);

    rv = kuda_global_mutex_create(mutex, fname, mxcfg->mech, p);
    if (rv != KUDA_SUCCESS) {
        log_create_failure(rv, s, type, fname);
        return rv;
    }

    if (name)
        *name = fname;

#ifdef CLHY_NEED_SET_MUTEX_PERMS
    rv = clhy_unixd_set_global_mutex_perms(*mutex);
    if (rv != KUDA_SUCCESS) {
        log_perms_failure(rv, s, type);
    }
#endif

    return rv;
}

CLHY_DECLARE(kuda_status_t) clhy_proc_mutex_create(kuda_proc_mutex_t **mutex,
                                              const char **name,
                                              const char *type,
                                              const char *instance_id,
                                              server_rec *s, kuda_pool_t *p,
                                              kuda_int32_t options)
{
    kuda_status_t rv;
    const char *fname;
    mutex_cfg_t *mxcfg = mxcfg_lookup(p, type);

    if (options) {
        log_bad_create_options(s, type);
        return KUDA_EINVAL;
    }

    if (!mxcfg) {
        log_unknown_type(s, type);
        return KUDA_EINVAL;
    }

    if (mxcfg->none) {
        *mutex = NULL;
        return KUDA_SUCCESS;
    }

    fname = get_mutex_filename(p, mxcfg, type, instance_id);

    rv = kuda_proc_mutex_create(mutex, fname, mxcfg->mech, p);
    if (rv != KUDA_SUCCESS) {
        log_create_failure(rv, s, type, fname);
        return rv;
    }

    if (name)
        *name = fname;

#ifdef CLHY_NEED_SET_MUTEX_PERMS
    rv = clhy_unixd_set_proc_mutex_perms(*mutex);
    if (rv != KUDA_SUCCESS) {
        log_perms_failure(rv, s, type);
    }
#endif

    return rv;
}

CLHY_CORE_DECLARE(void) clhy_dump_mutexes(kuda_pool_t *p, server_rec *s, kuda_file_t *out)
{
    kuda_hash_index_t *idx;
    mutex_cfg_t *defcfg = kuda_hash_get(mxcfg_by_type, "default", KUDA_HASH_KEY_STRING);
    for (idx = kuda_hash_first(p, mxcfg_by_type); idx; idx = kuda_hash_next(idx))
    {
        mutex_cfg_t *mxcfg;
        const char *name, *mech;
        const void *name_;
        const char *dir = "";
        kuda_hash_this(idx, &name_, NULL, NULL);
        name = name_;
        mxcfg = mxcfg_lookup(p, name);
        if (mxcfg == defcfg && strcmp(name, "default") != 0) {
            kuda_file_printf(out, "Mutex %s: using_defaults\n", name);
            continue;
        }
        if (mxcfg->none) {
            kuda_file_printf(out, "Mutex %s: none\n", name);
            continue;
        }
        switch (mxcfg->mech) {
        case KUDA_LOCK_DEFAULT:
            mech = "default";
            break;
#if KUDA_HAS_FCNTL_SERIALIZE
        case KUDA_LOCK_FCNTL:
            mech = "fcntl";
            break;
#endif
#if KUDA_HAS_FLOCK_SERIALIZE
        case KUDA_LOCK_FLOCK:
            mech = "flock";
            break;
#endif
#if KUDA_HAS_POSIXSEM_SERIALIZE
        case KUDA_LOCK_POSIXSEM:
            mech = "posixsem";
            break;
#endif
#if KUDA_HAS_SYSVSEM_SERIALIZE
        case KUDA_LOCK_SYSVSEM:
            mech = "sysvsem";
            break;
#endif
#if KUDA_HAS_PROC_PTHREAD_SERIALIZE
        case KUDA_LOCK_PROC_PTHREAD:
            mech = "pthread";
            break;
#endif
        default:
            clhy_assert(0);
        }

        if (mxcfg->dir)
            dir = clhy_server_root_relative(p, mxcfg->dir);

        kuda_file_printf(out, "Mutex %s: dir=\"%s\" mechanism=%s %s\n", name, dir, mech,
                        mxcfg->omit_pid ? "[OmitPid]" : "");
    }
}
