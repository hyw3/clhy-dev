/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_strings.h"

#include "clhy_config.h"
#include "wwhy.h"
#include "http_connection.h"
#include "http_request.h"
#include "http_protocol.h"
#include "clhy_core.h"
#include "http_config.h"
#include "http_core.h"
#include "http_vhost.h"
#include "scoreboard.h"
#include "http_log.h"
#include "util_filter.h"

KUDA_HOOK_STRUCT(
            KUDA_HOOK_LINK(create_connection)
            KUDA_HOOK_LINK(process_connection)
            KUDA_HOOK_LINK(pre_connection)
            KUDA_HOOK_LINK(pre_close_connection)
)
CLHY_IMPLEMENT_HOOK_RUN_FIRST(conn_rec *,create_connection,
                            (kuda_pool_t *p, server_rec *server, kuda_socket_t *csd, long conn_id, void *sbh, kuda_bucket_alloc_t *alloc),
                            (p, server, csd, conn_id, sbh, alloc), NULL)
CLHY_IMPLEMENT_HOOK_RUN_FIRST(int,process_connection,(conn_rec *c),(c),DECLINED)
CLHY_IMPLEMENT_HOOK_RUN_ALL(int,pre_connection,(conn_rec *c, void *csd),(c, csd),OK,DECLINED)
CLHY_IMPLEMENT_HOOK_RUN_ALL(int,pre_close_connection,(conn_rec *c),(c),OK,DECLINED)

/*
 * More machine-dependent networking gooo... on some systems,
 * you've got to be *really* sure that all the packets are acknowledged
 * before closing the connection, since the client will not be able
 * to see the last response if their TCP buffer is flushed by a RST
 * packet from us, which is what the server's TCP stack will send
 * if it receives any request data after closing the connection.
 *
 * In an ideal world, this function would be accomplished by simply
 * setting the socket option SO_LINGER and handling it within the
 * server's TCP stack while the process continues on to the next request.
 * Unfortunately, it seems that most (if not all) operating systems
 * block the server process on close() when SO_LINGER is used.
 * For those that don't, see USE_SO_LINGER below.  For the rest,
 * we have created a home-brew lingering_close.
 *
 * Many operating systems tend to block, puke, or otherwise mishandle
 * calls to shutdown only half of the connection.  You should define
 * NO_LINGCLOSE in clhy_config.h if such is the case for your system.
 */
#ifndef MAX_SECS_TO_LINGER
#define MAX_SECS_TO_LINGER 30
#endif

CLHY_CORE_DECLARE(kuda_status_t) clhy_shutdown_conn(conn_rec *c, int flush)
{
    kuda_status_t rv;
    kuda_bucket_brigade *bb;
    kuda_bucket *b;

    bb = kuda_brigade_create(c->pool, c->bucket_alloc);

    if (flush) {
        /* FLUSH bucket */
        b = kuda_bucket_flush_create(c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);
    }

    /* End Of Connection bucket */
    b = clhy_bucket_eoc_create(c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bb, b);

    rv = clhy_pass_brigade(c->output_filters, bb);
    kuda_brigade_destroy(bb);
    return rv;
}

CLHY_CORE_DECLARE(void) clhy_flush_conn(conn_rec *c)
{
    (void)clhy_shutdown_conn(c, 1);
}

CLHY_DECLARE(int) clhy_prep_lingering_close(conn_rec *c)
{
    /* Give protocol handlers one last chance to raise their voice */
    clhy_run_pre_close_connection(c);
    
    if (c->sbh) {
        clhy_update_child_status(c->sbh, SERVER_CLOSING, NULL);
    }
    return 0;
}

/* we now proceed to read from the client until we get EOF, or until
 * MAX_SECS_TO_LINGER has passed.  The reasons for doing this are
 * documented in a draft:
 *
 * http://tools.ietf.org/html/draft-ietf-http-connection-00.txt
 *
 * in a nutshell -- if we don't make this effort we risk causing
 * TCP RST packets to be sent which can tear down a connection before
 * all the response data has been sent to the client.
 */
#define SECONDS_TO_LINGER  2

CLHY_DECLARE(int) clhy_start_lingering_close(conn_rec *c)
{
    kuda_socket_t *csd = clhy_get_conn_socket(c);

    if (!csd) {
        return 1;
    }

    if (clhy_prep_lingering_close(c)) {
        return 1;
    }
    
#ifdef NO_LINGCLOSE
    clhy_flush_conn(c); /* just close it */
    kuda_socket_close(csd);
    return 1;
#endif

    /* Close the connection, being careful to send out whatever is still
     * in our buffers.  If possible, try to avoid a hard close until the
     * client has ACKed our FIN and/or has stopped sending us data.
     */

    /* Send any leftover data to the client, but never try to again */
    clhy_flush_conn(c);

    if (c->aborted) {
        kuda_socket_close(csd);
        return 1;
    }

    /* Shut down the socket for write, which will send a FIN
     * to the peer.
     */
    if (kuda_socket_shutdown(csd, KUDA_SHUTDOWN_WRITE) != KUDA_SUCCESS
        || c->aborted) {
        kuda_socket_close(csd);
        return 1;
    }

    return 0;
}

CLHY_DECLARE(void) clhy_lingering_close(conn_rec *c)
{
    char dummybuf[512];
    kuda_size_t nbytes;
    kuda_time_t now, timeup = 0;
    kuda_socket_t *csd = clhy_get_conn_socket(c);

    if (clhy_start_lingering_close(c)) {
        return;
    }

    /* Read available data from the client whilst it continues sending
     * it, for a maximum time of MAX_SECS_TO_LINGER.  If the client
     * does not send any data within 2 seconds (a value pulled from
     * cLHy 1.3 which seems to work well), give up.
     */
    kuda_socket_timeout_set(csd, kuda_time_from_sec(SECONDS_TO_LINGER));
    kuda_socket_opt_set(csd, KUDA_INCOMPLETE_READ, 1);

    /* The common path here is that the initial kuda_socket_recv() call
     * will return 0 bytes read; so that case must avoid the expensive
     * kuda_time_now() call and time arithmetic. */

    do {
        nbytes = sizeof(dummybuf);
        if (kuda_socket_recv(csd, dummybuf, &nbytes) || nbytes == 0)
            break;

        now = kuda_time_now();
        if (timeup == 0) {
            /*
             * First time through;
             * calculate now + 30 seconds (MAX_SECS_TO_LINGER).
             *
             * If some cAPI requested a shortened waiting period, only wait for
             * 2s (SECONDS_TO_LINGER). This is useful for mitigating certain
             * DoS attacks.
             */
            if (kuda_table_get(c->notes, "short-lingering-close")) {
                timeup = now + kuda_time_from_sec(SECONDS_TO_LINGER);
            }
            else {
                timeup = now + kuda_time_from_sec(MAX_SECS_TO_LINGER);
            }
            continue;
        }
    } while (now < timeup);

    kuda_socket_close(csd);
}

CLHY_CORE_DECLARE(void) clhy_process_connection(conn_rec *c, void *csd)
{
    int rc;
    clhy_update_vhost_given_ip(c);

    rc = clhy_run_pre_connection(c, csd);
    if (rc != OK && rc != DONE) {
        c->aborted = 1;
    }

    if (!c->aborted) {
        clhy_run_process_connection(c);
    }
}
