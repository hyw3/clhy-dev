/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"

/* Possibly get rid of the macros we defined in wwhy.h */
#if defined(strchr)
#undef strchr
#endif

#if defined (strrchr)
#undef strrchr
#endif

#if defined (strstr)
#undef strstr
#endif


#if defined(clhy_strchr)
#undef clhy_strchr
CLHY_DECLARE(char *) clhy_strchr(char *s, int c);
#endif

CLHY_DECLARE(char *) clhy_strchr(char *s, int c)
{
    return strchr(s,c);
}

#if defined(clhy_strchr_c)
#undef clhy_strchr_c
CLHY_DECLARE(const char *) clhy_strchr_c(const char *s, int c);
#endif

CLHY_DECLARE(const char *) clhy_strchr_c(const char *s, int c)
{
    return strchr(s,c);
}

#if defined(clhy_strrchr)
#undef clhy_strrchr
CLHY_DECLARE(char *) clhy_strrchr(char *s, int c);
#endif

CLHY_DECLARE(char *) clhy_strrchr(char *s, int c)
{
    return strrchr(s,c);
}

#if defined(clhy_strrchr_c)
#undef clhy_strrchr_c
CLHY_DECLARE(const char *) clhy_strrchr_c(const char *s, int c);
#endif

CLHY_DECLARE(const char *) clhy_strrchr_c(const char *s, int c)
{
    return strrchr(s,c);
}

#if defined(clhy_strstr)
#undef clhy_strstr
CLHY_DECLARE(char *) clhy_strstr(char *s, const char *c);
#endif

CLHY_DECLARE(char *) clhy_strstr(char *s, const char *c)
{
    return strstr(s,c);
}

#if defined(clhy_strstr_c)
#undef clhy_strstr_c
CLHY_DECLARE(const char *) clhy_strstr_c(const char *s, const char *c);
#endif

CLHY_DECLARE(const char *) clhy_strstr_c(const char *s, const char *c)
{
    return strstr(s,c);
}

#if defined(clhy_get_capi_config)
#undef clhy_get_capi_config
CLHY_DECLARE(void *) clhy_get_capi_config(const clhy_conf_vector_t *cv,
                                        const cAPI *m);
#endif

CLHY_DECLARE(void *) clhy_get_capi_config(const clhy_conf_vector_t *cv,
                                        const cAPI *m)
{
    return ((void **)cv)[m->capi_index];
}

CLHY_DECLARE(int) clhy_get_capi_flags(const cAPI *m)
{
    if (m->version < CLHY_CAPI_FLAGS_CAPIMN_MAJOR
            || (m->version == CLHY_CAPI_FLAGS_CAPIMN_MAJOR
                && (m->minor_version < CLHY_CAPI_FLAGS_CAPIMN_MINOR))) {
        return 0;
    }

    return m->flags;
}

#if defined(clhy_get_core_capi_config)
#undef clhy_get_core_capi_config
CLHY_DECLARE(void *) clhy_get_core_capi_config(const clhy_conf_vector_t *cv);
#endif

CLHY_DECLARE(void *) clhy_get_core_capi_config(const clhy_conf_vector_t *cv)
{
    return ((void **)cv)[CLHY_CORE_CAPI_INDEX];
}


#if defined(clhy_get_server_capi_loglevel)
#undef clhy_get_server_capi_loglevel
CLHY_DECLARE(int) clhy_get_server_capi_loglevel(const server_rec *s, int capi_index);
#endif

CLHY_DECLARE(int) clhy_get_server_capi_loglevel(const server_rec *s, int capi_index)
{
    if (capi_index < 0 || s->log.capi_levels == NULL ||
        s->log.capi_levels[capi_index] < 0)
    {
        return s->log.level;
    }

    return s->log.capi_levels[capi_index];
}

#if defined(clhy_get_conn_capi_loglevel)
#undef clhy_get_conn_capi_loglevel
CLHY_DECLARE(int) clhy_get_conn_capi_loglevel(const conn_rec *c, int capi_index);
#endif

CLHY_DECLARE(int) clhy_get_conn_capi_loglevel(const conn_rec *c, int capi_index)
{
    const struct clhy_logconf *l = (c)->log ? (c)->log : &(c)->base_server->log;
    if (capi_index < 0 || l->capi_levels == NULL ||
        l->capi_levels[capi_index] < 0)
    {
        return l->level;
    }

    return l->capi_levels[capi_index];
}

#if defined(clhy_get_conn_server_capi_loglevel)
#undef clhy_get_conn_server_capi_loglevel
CLHY_DECLARE(int) clhy_get_conn_server_capi_loglevel(const conn_rec *c,
                                                   const server_rec *s,
                                                   int capi_index);
#endif

CLHY_DECLARE(int) clhy_get_conn_server_capi_loglevel(const conn_rec *c,
                                                   const server_rec *s,
                                                   int capi_index)
{
    const struct clhy_logconf *l = (c->log && c->log != &c->base_server->log) ?
                                 c->log : &s->log;
    if (capi_index < 0 || l->capi_levels == NULL ||
        l->capi_levels[capi_index] < 0)
    {
        return l->level;
    }

    return l->capi_levels[capi_index];
}

#if defined(clhy_get_request_capi_loglevel)
#undef clhy_get_request_capi_loglevel
CLHY_DECLARE(int) clhy_get_request_capi_loglevel(const request_rec *c, int capi_index);
#endif

CLHY_DECLARE(int) clhy_get_request_capi_loglevel(const request_rec *r, int capi_index)
{
    const struct clhy_logconf *l = r->log             ? r->log             :
                                 r->connection->log ? r->connection->log :
                                 &r->server->log;
    if (capi_index < 0 || l->capi_levels == NULL ||
        l->capi_levels[capi_index] < 0)
    {
        return l->level;
    }

    return l->capi_levels[capi_index];
}

/**
 * Generic accessors for other cAPIs to set at their own cAPI-specific
 * data
 * @param conf_vector The vector in which the cAPIs configuration is stored.
 *        usually r->per_dir_config or s->capi_config
 * @param m The cAPI to set the data for.
 * @param val The cAPI-specific data to set
 * @fn void clhy_set_capi_config(clhy_conf_vector_t *cv, const cAPI *m, void *val)
 */
#if defined(clhy_set_capi_config)
#undef clhy_set_capi_config
CLHY_DECLARE(void) clhy_set_capi_config(clhy_conf_vector_t *cv, const cAPI *m,
                                      void *val);
#endif

CLHY_DECLARE(void) clhy_set_capi_config(clhy_conf_vector_t *cv, const cAPI *m,
                                      void *val)
{
    ((void **)cv)[m->capi_index] = val;
}


#if defined(clhy_set_core_capi_config)
#undef clhy_set_core_capi_config
CLHY_DECLARE(void) clhy_set_core_capi_config(clhy_conf_vector_t *cv, void *val);
#endif

CLHY_DECLARE(void) clhy_set_core_capi_config(clhy_conf_vector_t *cv, void *val)
{
    ((void **)cv)[CLHY_CORE_CAPI_INDEX] = val;
}
