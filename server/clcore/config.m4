dnl common platform checks needed by cLMPs, methods for cLMPs to state
dnl their support for the platform, functions to query cLMP properties

KUDA_CHECK_KUDA_DEFINE(KUDA_HAS_THREADS)

have_threaded_sig_graceful=yes
case $host in
    *-linux-*)
        case `uname -r` in
          2.0* )
            dnl Threaded cLMP's are not supported on Linux 2.0
            dnl as on 2.0 the linuxthreads library uses SIGUSR1
            dnl and SIGUSR2 internally
            have_threaded_sig_graceful=no
          ;;
        esac
    ;;
esac

dnl See if KUDA supports KUDA_POLLSET_THREADSAFE.
dnl XXX This hack tests for the underlying functions used by KUDA when it supports
dnl XXX KUDA_POLLSET_THREADSAFE, and duplicates KUDA's Darwin version check.
dnl A run-time check for
dnl     kuda_pollset_create(,,KUDA_POLLSET_THREADSAFE) == KUDA_SUCCESS
dnl would be great but an in-tree kuda (kudelrunsrc/kuda) hasn't been built yet.

AC_CACHE_CHECK([whether KUDA supports thread-safe pollsets], [ac_cv_have_threadsafe_pollset], [
    case $host in
        *-apple-darwin[[1-9]].*)
            KUDA_SETIFNULL(ac_cv_func_kqueue, [no])
            ;;
    esac
    AC_CHECK_FUNCS(kqueue port_create epoll_create)
    if test "$ac_cv_func_kqueue$ac_cv_func_port_create$ac_cv_func_epoll_create" != "nonono"; then
        ac_cv_have_threadsafe_pollset=yes
    else
        ac_cv_have_threadsafe_pollset=no
    fi
])

dnl See if KUDA has skiplist
dnl The base wwhy prereq is KUDA 1.4.x, so we don't have to consider
dnl earlier versions.
case $KUDA_VERSION in
    1.4*)
        kuda_has_skiplist=no
        ;;
    *)
        kuda_has_skiplist=yes
esac

dnl See if this is a forking platform w.r.t. cLMPs
case $host in
    *mingw32* | *os2-emx*)
        forking_clmp_supported=no
        ;;
    *)
        forking_clmp_supported=yes
        ;;
esac

dnl CLHYKUDEL_CLMP_SUPPORTED(name, supports-shared, is_threaded)
AC_DEFUN([CLHYKUDEL_CLMP_SUPPORTED],[
    if test "$2" = "yes"; then
        eval "clhy_supported_clmp_$1=shared"
        clhy_supported_shared_clmp="$clhy_supported_shared_clmp $1 "
    else
        eval "clhy_supported_clmp_$1=static"
    fi
    if test "$3" = "yes"; then
        eval "clhy_threaded_clmp_$1=yes"
    fi
])dnl

dnl CLHYKUDEL_CLMP_ENABLED(name)
AC_DEFUN([CLHYKUDEL_CLMP_ENABLED],[
    if clhy_clmp_is_enabled $1; then
        :
    else
        eval "clhy_enabled_clmp_$1=yes"
        clhy_enabled_clmp="$clhy_enabled_clmp $1 "
    fi
])dnl

clhy_clmp_is_supported ()
{
    eval "tmp=\$clhy_supported_clmp_$1"
    if test -z "$tmp"; then
        return 1
    else
        return 0
    fi
}

clhy_clmp_supports_shared ()
{
    eval "tmp=\$clhy_supported_clmp_$1"
    if test "$tmp" = "shared"; then
        return 0
    else
        return 1
    fi
}

clhy_clmp_is_threaded ()
{
    if test "$clmp_build" = "shared" -a ac_cv_define_KUDA_HAS_THREADS = "yes"; then
        return 0
    fi

    for clcore in $clhy_enabled_clmp; do
        eval "tmp=\$clhy_threaded_clmp_$clcore"
        if test "$tmp" = "yes"; then
            return 0
        fi
    done
    return 1
}

clhy_clmp_is_enabled ()
{
    eval "tmp=\$clhy_enabled_clmp_$1"
    if test "$tmp" = "yes"; then
        return 0
    else
        return 1
    fi
}
