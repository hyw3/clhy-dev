AC_MSG_CHECKING(which cLMP to use by default)
AC_ARG_WITH(clcore,
CLHYKUDEL_HELP_STRING(--with-clmp=CLMP,Choose the process model for cLHy to use by default.
                          CLMP={event|worker|prefork|winnt}
                          This will be statically linked as the only available CLMP unless
                          --enable-clmp-shared is also specified.
),[
    default_clmp=$withval
    AC_MSG_RESULT($withval);
],[
    dnl Order of preference for default cLMP: 
    dnl   The Windows and OS2 cLMPs are used on those platforms.
    dnl   Everywhere else: event, worker, prefork
    if clhy_clmp_is_supported "winnt"; then
        default_clmp=winnt
        AC_MSG_RESULT(winnt)
    elif clhy_clmp_is_supported "os2threaded"; then
        default_clmp=os2threaded
        AC_MSG_RESULT(os2threaded)
    elif clhy_clmp_is_supported "event"; then
        default_clmp=event
        AC_MSG_RESULT(event)
    elif clhy_clmp_is_supported "worker"; then
        default_clmp=worker
        AC_MSG_RESULT(worker - event is not supported)
    else
        default_clmp=prefork
        AC_MSG_RESULT(prefork - event and worker are not supported)
    fi
])

CLHYKUDEL_CLMP_ENABLED($default_clmp)

AC_ARG_ENABLE(clmp-shared,
CLHYKUDEL_HELP_STRING(--enable-clmp-shared=CLMP-LIST,Space-separated list of cLHy core APIs (cLMPs) to enable for dynamic loading.  CLMP-LIST=list | "all"),[
    if test "$enableval" = "no"; then
        clmp_build=static
    else
        clmp_build=shared
dnl     Build just the default cLMP if --enable-clmp-shared has no argument.
        if test "$enableval" = "yes"; then
            enableval=$default_clmp
        fi
        for i in $enableval; do
            if test "$i" = "all"; then
                for j in $clhy_supported_shared_clmp; do
                    eval "enable_clmp_$j=shared"
                    CLHYKUDEL_CLMP_ENABLED($j)
                done
            else
                i=`echo $i | sed 's/-/_/g'`
                if clhy_clmp_supports_shared $i; then
                    eval "enable_clmp_$i=shared"
                    CLHYKUDEL_CLMP_ENABLED($i)
                else
                    AC_MSG_ERROR([CLMP $i does not support dynamic loading.])
                fi
            fi
        done
    fi
], [clmp_build=static])

for i in $clhy_enabled_clmp; do
    if clhy_clmp_is_supported $i; then
        :
    else
        AC_MSG_ERROR([CLMP $i is not supported on this platform.])
    fi
done

if test $clmp_build = "shared"; then
    eval "tmp=\$enable_clmp_$default_clmp"
    if test "$tmp" != "shared"; then
        AC_MSG_ERROR([The default cLMP ($default_clmp) must be included in --enable-clmp-shared.  Use --with-clmp to change the default cLMP.])
    fi
fi

CLHYKUDEL_FAST_OUTPUT(server/clcore/Makefile)

if test $clmp_build = "shared"; then
    CLMP_LIB=""
else
    CLMP_LIB=server/clcore/$default_clmp/lib${default_clmp}.la
    CAPILIST="$CAPILIST clmp_${default_clmp}"
fi

CLMP_SUBDIRS=$clhy_enabled_clmp
CLHYKUDEL_SUBST(CLMP_SUBDIRS)
CLHYKUDEL_SUBST(CLMP_LIB)
