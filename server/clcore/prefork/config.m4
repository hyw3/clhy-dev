AC_MSG_CHECKING(if prefork cLMP supports this platform)
if test $forking_clmp_supported != yes; then
    AC_MSG_RESULT(no - This is not a forking platform)
else
    AC_MSG_RESULT(yes)
    CLHYKUDEL_CLMP_SUPPORTED(prefork, yes, no)
fi
