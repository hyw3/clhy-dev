/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_portable.h"
#include "kuda_strings.h"
#include "kuda_thread_proc.h"
#include "kuda_signal.h"

#define KUDA_WANT_STDIO
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#include "clhy_config.h"
#include "wwhy.h"
#include "core_default.h"
#include "http_main.h"
#include "http_log.h"
#include "http_config.h"
#include "http_core.h"          /* for get_remote_host */
#include "http_connection.h"
#include "scoreboard.h"
#include "clhy_core.h"
#include "util_mutex.h"
#include "unixd.h"
#include "core_common.h"
#include "clhy_listen.h"
#include "clhy_capimn.h"
#include "kuda_poll.h"

#include <stdlib.h>

#ifdef HAVE_TIME_H
#include <time.h>
#endif
#ifdef HAVE_SYS_PROCESSOR_H
#include <sys/processor.h> /* for bindprocessor() */
#endif

#include <signal.h>
#include <sys/times.h>

/* Limit on the total --- clients will be locked out if more servers than
 * this are needed.  It is intended solely to keep the server from crashing
 * when things get out of hand.
 *
 * We keep a hard maximum number of servers, for two reasons --- first off,
 * in case something goes seriously wrong, we want to stop the fork bomb
 * short of actually crashing the machine we're running on by filling some
 * kernel table.  Secondly, it keeps the size of the scoreboard file small
 * enough that we can read the whole thing without worrying too much about
 * the overhead.
 */
#ifndef DEFAULT_SERVER_LIMIT
#define DEFAULT_SERVER_LIMIT 256
#endif

/* Admin can't tune ServerLimit beyond MAX_SERVER_LIMIT.  We want
 * some sort of compile-time limit to help catch typos.
 */
#ifndef MAX_SERVER_LIMIT
#define MAX_SERVER_LIMIT 200000
#endif

#ifndef HARD_THREAD_LIMIT
#define HARD_THREAD_LIMIT 1
#endif

/* config globals */

static int clhy_daemons_to_start=0;
static int clhy_daemons_min_free=0;
static int clhy_daemons_max_free=0;
static int clhy_daemons_limit=0;      /* MaxRequestWorkers */
static int server_limit = 0;

/* data retained by prefork across load/unload of the cAPI
 * allocated on first call to pre-config hook; located on
 * subsequent calls to pre-config hook
 */
typedef struct prefork_retained_data {
    clhy_unixd_clmp_retained_data *clmp;

    int first_server_limit;
    int maxclients_reported;
    /*
     * The max child slot ever assigned, preserved across restarts.  Necessary
     * to deal with MaxRequestWorkers changes across CLHY_SIG_GRACEFUL restarts.  We
     * use this value to optimize routines that have to scan the entire scoreboard.
     */
    int max_daemons_limit;
    /*
     * idle_spawn_rate is the number of children that will be spawned on the
     * next maintenance cycle if there aren't enough idle servers.  It is
     * doubled up to MAX_SPAWN_RATE, and reset only when a cycle goes by
     * without the need to spawn.
     */
    int idle_spawn_rate;
#ifndef MAX_SPAWN_RATE
#define MAX_SPAWN_RATE  (32)
#endif
    int hold_off_on_exponential_spawning;
} prefork_retained_data;
static prefork_retained_data *retained;

typedef struct prefork_child_bucket {
    clhy_pod_t *pod;
    clhy_listen_rec *listeners;
    kuda_proc_mutex_t *mutex;
} prefork_child_bucket;
static prefork_child_bucket *all_buckets, /* All listeners buckets */
                            *my_bucket;   /* Current child bucket */

#define CLMP_CHILD_PID(i) (clhy_scoreboard_image->parent[i].pid)

/* one_process --- debugging mode variable; can be set from the command line
 * with the -X flag.  If set, this gets you the child_main loop running
 * in the process which originally started up (no detach, no make_child),
 * which is a pretty nice debugging environment.  (You'll get a SIGHUP
 * early in standalone_main; just continue through.  This is the server
 * trying to kill off any child processes which it might have lying
 * around --- cLHy doesn't keep track of their pids, it just sends
 * SIGHUP to the process group, ignoring it in the root process.
 * Continue through and you'll be fine.).
 */

static int one_process = 0;

static kuda_pool_t *pconf;               /* Pool for config stuff */
static kuda_pool_t *pchild;              /* Pool for wwhy child stuff */

static pid_t clhy_my_pid; /* it seems silly to call getpid all the time */
static pid_t parent_pid;
static int my_child_num;

#ifdef GPROF
/*
 * change directory for gprof to plop the gmon.out file
 * configure in wwhy.conf:
 * GprofDir $RuntimeDir/   -> $ServerRoot/$RuntimeDir/gmon.out
 * GprofDir $RuntimeDir/%  -> $ServerRoot/$RuntimeDir/gprof.$pid/gmon.out
 */
static void chdir_for_gprof(void)
{
    core_server_config *sconf =
        clhy_get_core_capi_config(clhy_server_conf->capi_config);
    char *dir = sconf->gprof_dir;
    const char *use_dir;

    if(dir) {
        kuda_status_t res;
        char *buf = NULL ;
        int len = strlen(sconf->gprof_dir) - 1;
        if(*(dir + len) == '%') {
            dir[len] = '\0';
            buf = clhy_append_pid(pconf, dir, "gprof.");
        }
        use_dir = clhy_server_root_relative(pconf, buf ? buf : dir);
        res = kuda_dir_make(use_dir,
                           KUDA_UREAD | KUDA_UWRITE | KUDA_UEXECUTE |
                           KUDA_GREAD | KUDA_GEXECUTE |
                           KUDA_WREAD | KUDA_WEXECUTE, pconf);
        if(res != KUDA_SUCCESS && !KUDA_STATUS_IS_EEXIST(res)) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, res, clhy_server_conf, CLHYLOGNO(00142)
                         "gprof: error creating directory %s", dir);
        }
    }
    else {
        use_dir = clhy_runtime_dir_relative(pconf, "");
    }

    chdir(use_dir);
}
#else
#define chdir_for_gprof()
#endif

static void prefork_note_child_killed(int childnum, pid_t pid,
                                      clhy_generation_t gen)
{
    CLHY_DEBUG_ASSERT(childnum != -1); /* no scoreboard squatting with this cLMP */
    clhy_run_child_status(clhy_server_conf,
                        clhy_scoreboard_image->parent[childnum].pid,
                        clhy_scoreboard_image->parent[childnum].generation,
                        childnum, CLMP_CHILD_EXITED);
    clhy_scoreboard_image->parent[childnum].pid = 0;
}

static void prefork_note_child_started(int slot, pid_t pid)
{
    clhy_generation_t gen = retained->clmp->my_generation;
    clhy_scoreboard_image->parent[slot].pid = pid;
    clhy_scoreboard_image->parent[slot].generation = gen;
    clhy_run_child_status(clhy_server_conf, pid, gen, slot, CLMP_CHILD_STARTED);
}

/* a clean exit from a child with proper cleanup */
static void clean_child_exit(int code) __attribute__ ((noreturn));
static void clean_child_exit(int code)
{
    retained->clmp->clmp_state = CLHY_CLMPQ_STOPPING;

    kuda_signal(SIGHUP, SIG_IGN);
    kuda_signal(SIGTERM, SIG_IGN);

    if (pchild) {
        kuda_pool_destroy(pchild);
    }

    if (one_process) {
        prefork_note_child_killed(/* slot */ 0, 0, 0);
    }

    clhy_clmp_pod_close(my_bucket->pod);
    chdir_for_gprof();
    exit(code);
}

static kuda_status_t accept_mutex_on(void)
{
    kuda_status_t rv = kuda_proc_mutex_lock(my_bucket->mutex);
    if (rv != KUDA_SUCCESS) {
        const char *msg = "couldn't grab the accept mutex";

        if (retained->clmp->my_generation !=
            clhy_scoreboard_image->global->running_generation) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, clhy_server_conf, CLHYLOGNO(00143) "%s", msg);
            clean_child_exit(0);
        }
        else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, clhy_server_conf, CLHYLOGNO(00144) "%s", msg);
            exit(APEXIT_CHILDFATAL);
        }
    }
    return KUDA_SUCCESS;
}

static kuda_status_t accept_mutex_off(void)
{
    kuda_status_t rv = kuda_proc_mutex_unlock(my_bucket->mutex);
    if (rv != KUDA_SUCCESS) {
        const char *msg = "couldn't release the accept mutex";

        if (retained->clmp->my_generation !=
            clhy_scoreboard_image->global->running_generation) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, clhy_server_conf, CLHYLOGNO(00145) "%s", msg);
            /* don't exit here... we have a connection to
             * process, after which point we'll see that the
             * generation changed and we'll exit cleanly
             */
        }
        else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, clhy_server_conf, CLHYLOGNO(00146) "%s", msg);
            exit(APEXIT_CHILDFATAL);
        }
    }
    return KUDA_SUCCESS;
}

/* On some architectures it's safe to do unserialized accept()s in the single
 * Listen case.  But it's never safe to do it in the case where there's
 * multiple Listen statements.  Define SINGLE_LISTEN_UNSERIALIZED_ACCEPT
 * when it's safe in the single Listen case.
 */
#ifdef SINGLE_LISTEN_UNSERIALIZED_ACCEPT
#define SAFE_ACCEPT(stmt) (clhy_listeners->next ? (stmt) : KUDA_SUCCESS)
#else
#define SAFE_ACCEPT(stmt) (stmt)
#endif

static int prefork_query(int query_code, int *result, kuda_status_t *rv)
{
    *rv = KUDA_SUCCESS;
    switch(query_code){
    case CLHY_CLMPQ_MAX_DAEMON_USED:
        *result = clhy_daemons_limit;
        break;
    case CLHY_CLMPQ_IS_THREADED:
        *result = CLHY_CLMPQ_NOT_SUPPORTED;
        break;
    case CLHY_CLMPQ_IS_FORKED:
        *result = CLHY_CLMPQ_DYNAMIC;
        break;
    case CLHY_CLMPQ_HARD_LIMIT_DAEMONS:
        *result = server_limit;
        break;
    case CLHY_CLMPQ_HARD_LIMIT_THREADS:
        *result = HARD_THREAD_LIMIT;
        break;
    case CLHY_CLMPQ_MAX_THREADS:
        *result = 1;
        break;
    case CLHY_CLMPQ_MIN_SPARE_DAEMONS:
        *result = clhy_daemons_min_free;
        break;
    case CLHY_CLMPQ_MIN_SPARE_THREADS:
        *result = 0;
        break;
    case CLHY_CLMPQ_MAX_SPARE_DAEMONS:
        *result = clhy_daemons_max_free;
        break;
    case CLHY_CLMPQ_MAX_SPARE_THREADS:
        *result = 0;
        break;
    case CLHY_CLMPQ_MAX_REQUESTS_DAEMON:
        *result = clhy_max_requests_per_child;
        break;
    case CLHY_CLMPQ_MAX_DAEMONS:
        *result = clhy_daemons_limit;
        break;
    case CLHY_CLMPQ_CLMP_STATE:
        *result = retained->clmp->clmp_state;
        break;
    case CLHY_CLMPQ_GENERATION:
        *result = retained->clmp->my_generation;
        break;
    default:
        *rv = KUDA_ENOTIMPL;
        break;
    }
    return OK;
}

static const char *prefork_get_name(void)
{
    return "prefork";
}

/*****************************************************************
 * Connection structures and accounting...
 */

static void just_die(int sig)
{
    clean_child_exit(0);
}

/* volatile because it's updated from a signal handler */
static int volatile die_now = 0;

static void stop_listening(int sig)
{
    retained->clmp->clmp_state = CLHY_CLMPQ_STOPPING;
    clhy_close_listeners_ex(my_bucket->listeners);

    /* For a graceful stop, we want the child to exit when done */
    die_now = 1;
}

/*****************************************************************
 * Child process main loop.
 * The following vars are static to avoid getting clobbered by longjmp();
 * they are really private to child_main.
 */

static int requests_this_child;
static int num_listensocks = 0;

static void child_main(int child_num_arg, int child_bucket)
{
#if KUDA_HAS_THREADS
    kuda_thread_t *thd = NULL;
    kuda_platform_thread_t osthd;
#endif
    kuda_pool_t *ptrans;
    kuda_allocator_t *allocator;
    kuda_status_t status;
    int i;
    clhy_listen_rec *lr;
    kuda_pollset_t *pollset;
    clhy_sb_handle_t *sbh;
    kuda_bucket_alloc_t *bucket_alloc;
    int last_poll_idx = 0;
    const char *lockfile;

    /* for benefit of any hooks that run as this child initializes */
    retained->clmp->clmp_state = CLHY_CLMPQ_STARTING;

    my_child_num = child_num_arg;
    clhy_my_pid = getpid();
    requests_this_child = 0;

    clhy_fatal_signal_child_setup(clhy_server_conf);

    /* Get a sub context for global allocations in this child, so that
     * we can have cleanups occur when the child exits.
     */
    kuda_allocator_create(&allocator);
    kuda_allocator_max_free_set(allocator, clhy_max_mem_free);
    kuda_pool_create_ex(&pchild, pconf, NULL, allocator);
    kuda_allocator_owner_set(allocator, pchild);
    kuda_pool_tag(pchild, "pchild");

#if KUDA_HAS_THREADS
    osthd = kuda_platform_thread_current();
    kuda_platform_thread_put(&thd, &osthd, pchild);
#endif

    kuda_pool_create(&ptrans, pchild);
    kuda_pool_tag(ptrans, "transaction");

    /* close unused listeners and pods */
    for (i = 0; i < retained->clmp->num_buckets; i++) {
        if (i != child_bucket) {
            clhy_close_listeners_ex(all_buckets[i].listeners);
            clhy_clmp_pod_close(all_buckets[i].pod);
        }
    }

    /* needs to be done before we switch UIDs so we have permissions */
    clhy_reopen_scoreboard(pchild, NULL, 0);
    status = SAFE_ACCEPT(kuda_proc_mutex_child_init(&my_bucket->mutex,
                                    kuda_proc_mutex_lockfile(my_bucket->mutex),
                                    pchild));
    if (status != KUDA_SUCCESS) {
        lockfile = kuda_proc_mutex_lockfile(my_bucket->mutex);
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, status, clhy_server_conf, CLHYLOGNO(00155)
                     "Couldn't initialize cross-process lock in child "
                     "(%s) (%s)",
                     lockfile ? lockfile : "none",
                     kuda_proc_mutex_name(my_bucket->mutex));
        clean_child_exit(APEXIT_CHILDFATAL);
    }

    if (clhy_run_drop_privileges(pchild, clhy_server_conf)) {
        clean_child_exit(APEXIT_CHILDFATAL);
    }

    clhy_run_child_init(pchild, clhy_server_conf);

    clhy_create_sb_handle(&sbh, pchild, my_child_num, 0);

    (void) clhy_update_child_status(sbh, SERVER_READY, (request_rec *) NULL);

    /* Set up the pollfd array */
    status = kuda_pollset_create(&pollset, num_listensocks, pchild,
                                KUDA_POLLSET_NOCOPY);
    if (status != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, status, clhy_server_conf, CLHYLOGNO(00156)
                     "Couldn't create pollset in child; check system or user limits");
        clean_child_exit(APEXIT_CHILDSICK); /* assume temporary resource issue */
    }

    for (lr = my_bucket->listeners, i = num_listensocks; i--; lr = lr->next) {
        kuda_pollfd_t *pfd = kuda_pcalloc(pchild, sizeof *pfd);

        pfd->desc_type = KUDA_POLL_SOCKET;
        pfd->desc.s = lr->sd;
        pfd->reqevents = KUDA_POLLIN;
        pfd->client_data = lr;

        status = kuda_pollset_add(pollset, pfd);
        if (status != KUDA_SUCCESS) {
            /* If the child processed a SIGWINCH before setting up the
             * pollset, this error path is expected and harmless,
             * since the listener fd was already closed; so don't
             * pollute the logs in that case. */
            if (!die_now) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, status, clhy_server_conf, CLHYLOGNO(00157)
                             "Couldn't add listener to pollset; check system or user limits");
                clean_child_exit(APEXIT_CHILDSICK);
            }
            clean_child_exit(0);
        }

        lr->accept_func = clhy_unixd_accept;
    }

    retained->clmp->clmp_state = CLHY_CLMPQ_RUNNING;

    bucket_alloc = kuda_bucket_alloc_create(pchild);

    /* die_now is set when CLHY_SIG_GRACEFUL is received in the child;
     * {shutdown,restart}_pending are set when a signal is received while
     * running in single process mode.
     */
    while (!die_now
           && !retained->clmp->shutdown_pending
           && !retained->clmp->restart_pending) {
        conn_rec *current_conn;
        void *csd;

        /*
         * (Re)initialize this child to a pre-connection state.
         */

        kuda_pool_clear(ptrans);

        if ((clhy_max_requests_per_child > 0
             && requests_this_child++ >= clhy_max_requests_per_child)) {
            clean_child_exit(0);
        }

        (void) clhy_update_child_status(sbh, SERVER_READY, (request_rec *) NULL);

        /*
         * Wait for an acceptable connection to arrive.
         */

        /* Lock around "accept", if necessary */
        SAFE_ACCEPT(accept_mutex_on());

        if (num_listensocks == 1) {
            /* There is only one listener record, so refer to that one. */
            lr = my_bucket->listeners;
        }
        else {
            /* multiple listening sockets - need to poll */
            for (;;) {
                kuda_int32_t numdesc;
                const kuda_pollfd_t *pdesc;

                /* check for termination first so we don't sleep for a while in
                 * poll if already signalled
                 */
                if (die_now         /* in graceful stop/restart */
                        || retained->clmp->shutdown_pending
                        || retained->clmp->restart_pending) {
                    SAFE_ACCEPT(accept_mutex_off());
                    clean_child_exit(0);
                }

                /* timeout == 10 seconds to avoid a hang at graceful restart/stop
                 * caused by the closing of sockets by the signal handler
                 */
                status = kuda_pollset_poll(pollset, kuda_time_from_sec(10),
                                          &numdesc, &pdesc);
                if (status != KUDA_SUCCESS) {
                    if (KUDA_STATUS_IS_TIMEUP(status) ||
                        KUDA_STATUS_IS_EINTR(status)) {
                        continue;
                    }
                    /* Single Unix documents select as returning errnos
                     * EBADF, EINTR, and EINVAL... and in none of those
                     * cases does it make sense to continue.  In fact
                     * on Linux 2.0.x we seem to end up with EFAULT
                     * occasionally, and we'd loop forever due to it.
                     */
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, status,
                                 clhy_server_conf, CLHYLOGNO(00158) "kuda_pollset_poll: (listen)");
                    SAFE_ACCEPT(accept_mutex_off());
                    clean_child_exit(APEXIT_CHILDSICK);
                }

                /* We can always use pdesc[0], but sockets at position N
                 * could end up completely starved of attention in a very
                 * busy server. Therefore, we round-robin across the
                 * returned set of descriptors. While it is possible that
                 * the returned set of descriptors might flip around and
                 * continue to starve some sockets, we happen to know the
                 * internal pollset implementation retains ordering
                 * stability of the sockets. Thus, the round-robin should
                 * ensure that a socket will eventually be serviced.
                 */
                if (last_poll_idx >= numdesc)
                    last_poll_idx = 0;

                /* Grab a listener record from the client_data of the poll
                 * descriptor, and advance our saved index to round-robin
                 * the next fetch.
                 *
                 * ### hmm... this descriptor might have POLLERR rather
                 * ### than POLLIN
                 */
                lr = pdesc[last_poll_idx++].client_data;
                goto got_fd;
            }
        }
    got_fd:
        /* if we accept() something we don't want to die, so we have to
         * defer the exit
         */
        status = lr->accept_func(&csd, lr, ptrans);

        SAFE_ACCEPT(accept_mutex_off());      /* unlock after "accept" */

        if (status == KUDA_EGENERAL) {
            /* resource shortage or should-not-occur occurred */
            clean_child_exit(APEXIT_CHILDSICK);
        }
        else if (status != KUDA_SUCCESS) {
            continue;
        }

        /*
         * We now have a connection, so set it up with the appropriate
         * socket options, file descriptors, and read/write buffers.
         */

        current_conn = clhy_run_create_connection(ptrans, clhy_server_conf, csd, my_child_num, sbh, bucket_alloc);
        if (current_conn) {
#if KUDA_HAS_THREADS
            current_conn->current_thread = thd;
#endif
            clhy_process_connection(current_conn, csd);
            clhy_lingering_close(current_conn);
        }

        /* Check the pod and the generation number after processing a
         * connection so that we'll go away if a graceful restart occurred
         * while we were processing the connection or we are the lucky
         * idle server process that gets to die.
         */
        if (clhy_clmp_pod_check(my_bucket->pod) == KUDA_SUCCESS) { /* selected as idle? */
            die_now = 1;
        }
        else if (retained->clmp->my_generation !=
                 clhy_scoreboard_image->global->running_generation) { /* restart? */
            /* yeah, this could be non-graceful restart, in which case the
             * parent will kill us soon enough, but why bother checking?
             */
            die_now = 1;
        }
    }
    kuda_pool_clear(ptrans); /* kludge to avoid crash in kuda reslist cleanup code */
    clean_child_exit(0);
}


static int make_child(server_rec *s, int slot)
{
    int bucket = slot % retained->clmp->num_buckets;
    int pid;

    if (slot + 1 > retained->max_daemons_limit) {
        retained->max_daemons_limit = slot + 1;
    }

    if (one_process) {
        my_bucket = &all_buckets[0];

        prefork_note_child_started(slot, getpid());
        child_main(slot, 0);
        /* NOTREACHED */
        clhy_assert(0);
        return -1;
    }

    (void) clhy_update_child_status_from_indexes(slot, 0, SERVER_STARTING,
                                               (request_rec *) NULL);

#ifdef _OSD_POSIX
    /* BS2000 requires a "special" version of fork() before a setuid() call */
    if ((pid = platform_fork(clhy_unixd_config.user_name)) == -1) {
#else
    if ((pid = fork()) == -1) {
#endif
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, errno, s, CLHYLOGNO(00159) "fork: Unable to fork new process");

        /* fork didn't succeed. Fix the scoreboard or else
         * it will say SERVER_STARTING forever and ever
         */
        (void) clhy_update_child_status_from_indexes(slot, 0, SERVER_DEAD,
                                                   (request_rec *) NULL);

        /* In case system resources are maxxed out, we don't want
         * cLHy running away with the CPU trying to fork over and
         * over and over again.
         */
        sleep(10);

        return -1;
    }

    if (!pid) {
        my_bucket = &all_buckets[bucket];

#ifdef HAVE_BINDPROCESSOR
        /* by default AIX binds to a single processor
         * this bit unbinds children which will then bind to another cpu
         */
        int status = bindprocessor(BINDPROCESS, (int)getpid(),
                                   PROCESSOR_CLASS_ANY);
        if (status != OK) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, errno,
                         clhy_server_conf, CLHYLOGNO(00160) "processor unbind failed");
        }
#endif
        RAISE_SIGSTOP(MAKE_CHILD);
        CLHY_MONCONTROL(1);
        /* Disable the parent's signal handlers and set up proper handling in
         * the child.
         */
        kuda_signal(SIGHUP, just_die);
        kuda_signal(SIGTERM, just_die);
        /* Ignore SIGINT in child. This fixes race-condition in signals
         * handling when wwhy is runnning on foreground and user hits ctrl+c.
         * In this case, SIGINT is sent to all children followed by SIGTERM
         * from the main process, which interrupts the SIGINT handler and
         * leads to inconsistency.
         */
        kuda_signal(SIGINT, SIG_IGN);
        /* The child process just closes listeners on CLHY_SIG_GRACEFUL.
         * The pod is used for signalling the graceful restart.
         */
        kuda_signal(CLHY_SIG_GRACEFUL, stop_listening);
        child_main(slot, bucket);
    }

    prefork_note_child_started(slot, pid);

    return 0;
}


/* start up a bunch of children */
static void startup_children(int number_to_start)
{
    int i;

    for (i = 0; number_to_start && i < clhy_daemons_limit; ++i) {
        if (clhy_scoreboard_image->servers[i][0].status != SERVER_DEAD) {
            continue;
        }
        if (make_child(clhy_server_conf, i) < 0) {
            break;
        }
        --number_to_start;
    }
}

static void perform_idle_server_maintenance(kuda_pool_t *p)
{
    int i;
    int idle_count;
    worker_score *ws;
    int free_length;
    int free_slots[MAX_SPAWN_RATE];
    int last_non_dead;
    int total_non_dead;

    /* initialize the free_list */
    free_length = 0;

    idle_count = 0;
    last_non_dead = -1;
    total_non_dead = 0;

    for (i = 0; i < clhy_daemons_limit; ++i) {
        int status;

        if (i >= retained->max_daemons_limit && free_length == retained->idle_spawn_rate)
            break;
        ws = &clhy_scoreboard_image->servers[i][0];
        status = ws->status;
        if (status == SERVER_DEAD) {
            /* try to keep children numbers as low as possible */
            if (free_length < retained->idle_spawn_rate) {
                free_slots[free_length] = i;
                ++free_length;
            }
        }
        else {
            /* We consider a starting server as idle because we started it
             * at least a cycle ago, and if it still hasn't finished starting
             * then we're just going to swamp things worse by forking more.
             * So we hopefully won't need to fork more if we count it.
             * This depends on the ordering of SERVER_READY and SERVER_STARTING.
             */
            if (status <= SERVER_READY) {
                ++ idle_count;
            }

            ++total_non_dead;
            last_non_dead = i;
        }
    }
    retained->max_daemons_limit = last_non_dead + 1;
    if (idle_count > clhy_daemons_max_free) {
        static int bucket_kill_child_record = -1;
        /* kill off one child... we use the pod because that'll cause it to
         * shut down gracefully, in case it happened to pick up a request
         * while we were counting
         */
        bucket_kill_child_record = (bucket_kill_child_record + 1) % retained->clmp->num_buckets;
        clhy_clmp_pod_signal(all_buckets[bucket_kill_child_record].pod);
        retained->idle_spawn_rate = 1;
    }
    else if (idle_count < clhy_daemons_min_free) {
        /* terminate the free list */
        if (free_length == 0) {
            /* only report this condition once */
            if (!retained->maxclients_reported) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, clhy_server_conf, CLHYLOGNO(00161)
                            "server reached MaxRequestWorkers setting, consider"
                            " raising the MaxRequestWorkers setting");
                retained->maxclients_reported = 1;
            }
            retained->idle_spawn_rate = 1;
        }
        else {
            if (retained->idle_spawn_rate >= 8) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, clhy_server_conf, CLHYLOGNO(00162)
                    "server seems busy, (you may need "
                    "to increase StartServers, or Min/MaxSpareServers), "
                    "spawning %d children, there are %d idle, and "
                    "%d total children", retained->idle_spawn_rate,
                    idle_count, total_non_dead);
            }
            for (i = 0; i < free_length; ++i) {
                make_child(clhy_server_conf, free_slots[i]);
            }
            /* the next time around we want to spawn twice as many if this
             * wasn't good enough, but not if we've just done a graceful
             */
            if (retained->hold_off_on_exponential_spawning) {
                --retained->hold_off_on_exponential_spawning;
            }
            else if (retained->idle_spawn_rate < MAX_SPAWN_RATE) {
                retained->idle_spawn_rate *= 2;
            }
        }
    }
    else {
        retained->idle_spawn_rate = 1;
    }
}

/*****************************************************************
 * Executive routines.
 */

static int prefork_run(kuda_pool_t *_pconf, kuda_pool_t *plog, server_rec *s)
{
    int index;
    int remaining_children_to_start;
    int i;

    clhy_log_pid(pconf, clhy_pid_fname);

    if (!retained->clmp->was_graceful) {
        if (clhy_run_pre_clmp(s->process->pool, SB_SHARED) != OK) {
            retained->clmp->clmp_state = CLHY_CLMPQ_STOPPING;
            return !OK;
        }
        /* fix the generation number in the global score; we just got a new,
         * cleared scoreboard
         */
        clhy_scoreboard_image->global->running_generation = retained->clmp->my_generation;
    }

    clhy_unixd_clmp_set_signals(pconf, one_process);

    if (one_process) {
        CLHY_MONCONTROL(1);
        make_child(clhy_server_conf, 0);
        /* NOTREACHED */
        clhy_assert(0);
        return !OK;
    }

    /* Don't thrash since num_buckets depends on the
     * system and the number of online CPU cores...
     */
    if (clhy_daemons_limit < retained->clmp->num_buckets)
        clhy_daemons_limit = retained->clmp->num_buckets;
    if (clhy_daemons_to_start < retained->clmp->num_buckets)
        clhy_daemons_to_start = retained->clmp->num_buckets;
    if (clhy_daemons_min_free < retained->clmp->num_buckets)
        clhy_daemons_min_free = retained->clmp->num_buckets;
    if (clhy_daemons_max_free < clhy_daemons_min_free + retained->clmp->num_buckets)
        clhy_daemons_max_free = clhy_daemons_min_free + retained->clmp->num_buckets;

    /* If we're doing a graceful_restart then we're going to see a lot
     * of children exiting immediately when we get into the main loop
     * below (because we just sent them CLHY_SIG_GRACEFUL).  This happens pretty
     * rapidly... and for each one that exits we'll start a new one until
     * we reach at least daemons_min_free.  But we may be permitted to
     * start more than that, so we'll just keep track of how many we're
     * supposed to start up without the 1 second penalty between each fork.
     */
    remaining_children_to_start = clhy_daemons_to_start;
    if (remaining_children_to_start > clhy_daemons_limit) {
        remaining_children_to_start = clhy_daemons_limit;
    }
    if (!retained->clmp->was_graceful) {
        startup_children(remaining_children_to_start);
        remaining_children_to_start = 0;
    }
    else {
        /* give the system some time to recover before kicking into
         * exponential mode
         */
        retained->hold_off_on_exponential_spawning = 10;
    }

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00163)
                "%s configured -- resuming normal operations",
                clhy_get_server_description());
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, clhy_server_conf, CLHYLOGNO(00164)
                "Server built: %s", clhy_get_server_built());
    clhy_log_command_line(plog, s);
    clhy_log_core_common(s);
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(00165)
                "Accept mutex: %s (default: %s)",
                (all_buckets[0].mutex)
                    ? kuda_proc_mutex_name(all_buckets[0].mutex)
                    : "none",
                kuda_proc_mutex_defname());

    retained->clmp->clmp_state = CLHY_CLMPQ_RUNNING;

    while (!retained->clmp->restart_pending && !retained->clmp->shutdown_pending) {
        int child_slot;
        kuda_exit_why_e exitwhy;
        int status, processed_status;
        /* this is a memory leak, but I'll fix it later. */
        kuda_proc_t pid;

        clhy_wait_or_timeout(&exitwhy, &status, &pid, pconf, clhy_server_conf);

        /* XXX: if it takes longer than 1 second for all our children
         * to start up and get into IDLE state then we may spawn an
         * extra child
         */
        if (pid.pid != -1) {
            processed_status = clhy_process_child_status(&pid, exitwhy, status);
            child_slot = clhy_find_child_by_pid(&pid);
            if (processed_status == APEXIT_CHILDFATAL) {
                /* fix race condition found in PR 39311
                 * A child created at the same time as a graceful happens 
                 * can find the lock missing and create a fatal error.
                 * It is not fatal for the last generation to be in this state.
                 */
                if (child_slot < 0
                    || clhy_get_scoreboard_process(child_slot)->generation
                       == retained->clmp->my_generation) {
                    retained->clmp->clmp_state = CLHY_CLMPQ_STOPPING;
                    return !OK;
                }
                else {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, clhy_server_conf, CLHYLOGNO(00166)
                                 "Ignoring fatal error in child of previous "
                                 "generation (pid %ld).",
                                 (long)pid.pid);
                }
            }

            /* non-fatal death... note that it's gone in the scoreboard. */
            if (child_slot >= 0) {
                (void) clhy_update_child_status_from_indexes(child_slot, 0, SERVER_DEAD,
                                                           (request_rec *) NULL);
                prefork_note_child_killed(child_slot, 0, 0);
                if (processed_status == APEXIT_CHILDSICK) {
                    /* child detected a resource shortage (E[NM]FILE, ENOBUFS, etc)
                     * cut the fork rate to the minimum
                     */
                    retained->idle_spawn_rate = 1;
                }
                else if (remaining_children_to_start
                    && child_slot < clhy_daemons_limit) {
                    /* we're still doing a 1-for-1 replacement of dead
                     * children with new children
                     */
                    make_child(clhy_server_conf, child_slot);
                    --remaining_children_to_start;
                }
#if KUDA_HAS_OTHER_CHILD
            }
            else if (kuda_proc_other_child_alert(&pid, KUDA_OC_REASON_DEATH, status) == KUDA_SUCCESS) {
                /* handled */
#endif
            }
            else if (retained->clmp->was_graceful) {
                /* Great, we've probably just lost a slot in the
                 * scoreboard.  Somehow we don't know about this
                 * child.
                 */
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING,
                            0, clhy_server_conf, CLHYLOGNO(00167)
                            "long lost child came home! (pid %ld)", (long)pid.pid);
            }
            /* Don't perform idle maintenance when a child dies,
             * only do it when there's a timeout.  Remember only a
             * finite number of children can die, and it's pretty
             * pathological for a lot to die suddenly.
             */
            continue;
        }
        else if (remaining_children_to_start) {
            /* we hit a 1 second timeout in which none of the previous
             * generation of children needed to be reaped... so assume
             * they're all done, and pick up the slack if any is left.
             */
            startup_children(remaining_children_to_start);
            remaining_children_to_start = 0;
            /* In any event we really shouldn't do the code below because
             * few of the servers we just started are in the IDLE state
             * yet, so we'd mistakenly create an extra server.
             */
            continue;
        }

        perform_idle_server_maintenance(pconf);
    }

    retained->clmp->clmp_state = CLHY_CLMPQ_STOPPING;

    if (retained->clmp->shutdown_pending && retained->clmp->is_ungraceful) {
        /* Time to shut down:
         * Kill child processes, tell them to call child_exit, etc...
         */
        if (clhy_unixd_killpg(getpgrp(), SIGTERM) < 0) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, errno, clhy_server_conf, CLHYLOGNO(00168) "killpg SIGTERM");
        }
        clhy_reclaim_child_processes(1, /* Start with SIGTERM */
                                   prefork_note_child_killed);

        /* cleanup pid file on normal shutdown */
        clhy_remove_pid(pconf, clhy_pid_fname);
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00169)
                    "caught SIGTERM, shutting down");

        return DONE;
    }

    if (retained->clmp->shutdown_pending) {
        /* Time to perform a graceful shut down:
         * Reap the inactive children, and ask the active ones
         * to close their listeners, then wait until they are
         * all done to exit.
         */
        int active_children;
        kuda_time_t cutoff = 0;

        /* Stop listening */
        clhy_close_listeners();

        /* kill off the idle ones */
        for (i = 0; i < retained->clmp->num_buckets; i++) {
            clhy_clmp_pod_killpg(all_buckets[i].pod, retained->max_daemons_limit);
        }

        /* Send SIGUSR1 to the active children */
        active_children = 0;
        for (index = 0; index < clhy_daemons_limit; ++index) {
            if (clhy_scoreboard_image->servers[index][0].status != SERVER_DEAD) {
                /* Ask each child to close its listeners. */
                clhy_clmp_safe_kill(CLMP_CHILD_PID(index), CLHY_SIG_GRACEFUL);
                active_children++;
            }
        }

        /* Allow each child which actually finished to exit */
        clhy_relieve_child_processes(prefork_note_child_killed);

        /* cleanup pid file */
        clhy_remove_pid(pconf, clhy_pid_fname);
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00170)
           "caught " CLHY_SIG_GRACEFUL_STOP_STRING ", shutting down gracefully");

        if (clhy_graceful_shutdown_timeout) {
            cutoff = kuda_time_now() +
                     kuda_time_from_sec(clhy_graceful_shutdown_timeout);
        }

        /* Don't really exit until each child has finished */
        retained->clmp->shutdown_pending = 0;
        do {
            /* Pause for a second */
            sleep(1);

            /* Relieve any children which have now exited */
            clhy_relieve_child_processes(prefork_note_child_killed);

            active_children = 0;
            for (index = 0; index < clhy_daemons_limit; ++index) {
                if (clhy_clmp_safe_kill(CLMP_CHILD_PID(index), 0) == KUDA_SUCCESS) {
                    active_children = 1;
                    /* Having just one child is enough to stay around */
                    break;
                }
            }
        } while (!retained->clmp->shutdown_pending && active_children &&
                 (!clhy_graceful_shutdown_timeout || kuda_time_now() < cutoff));

        /* We might be here because we received SIGTERM, either
         * way, try and make sure that all of our processes are
         * really dead.
         */
        clhy_unixd_killpg(getpgrp(), SIGTERM);

        return DONE;
    }

    /* we've been told to restart */
    if (one_process) {
        /* not worth thinking about */
        return DONE;
    }

    /* advance to the next generation */
    /* XXX: we really need to make sure this new generation number isn't in
     * use by any of the children.
     */
    ++retained->clmp->my_generation;
    clhy_scoreboard_image->global->running_generation = retained->clmp->my_generation;

    if (!retained->clmp->is_ungraceful) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00171)
                    "Graceful restart requested, doing restart");

        /* kill off the idle ones */
        for (i = 0; i < retained->clmp->num_buckets; i++) {
            clhy_clmp_pod_killpg(all_buckets[i].pod, retained->max_daemons_limit);
        }

        /* This is mostly for debugging... so that we know what is still
         * gracefully dealing with existing request.  This will break
         * in a very nasty way if we ever have the scoreboard totally
         * file-based (no shared memory)
         */
        for (index = 0; index < clhy_daemons_limit; ++index) {
            if (clhy_scoreboard_image->servers[index][0].status != SERVER_DEAD) {
                clhy_scoreboard_image->servers[index][0].status = SERVER_GRACEFUL;
                /* Ask each child to close its listeners.
                 *
                 * NOTE: we use the scoreboard, because if we send SIGUSR1
                 * to every process in the group, this may include CGI's,
                 * piped loggers, etc. They almost certainly won't handle
                 * it gracefully.
                 */
                clhy_clmp_safe_kill(clhy_scoreboard_image->parent[index].pid, CLHY_SIG_GRACEFUL);
            }
        }
    }
    else {
        /* Kill 'em off */
        if (clhy_unixd_killpg(getpgrp(), SIGHUP) < 0) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, errno, clhy_server_conf, CLHYLOGNO(00172) "killpg SIGHUP");
        }
        clhy_reclaim_child_processes(0, /* Not when just starting up */
                                   prefork_note_child_killed);
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00173)
                    "SIGHUP received.  Attempting to restart");
    }

    return OK;
}

/* This really should be a post_config hook, but the error log is already
 * redirected by that point, so we need to do this in the open_logs phase.
 */
static int prefork_open_logs(kuda_pool_t *p, kuda_pool_t *plog, kuda_pool_t *ptemp, server_rec *s)
{
    int startup = 0;
    int level_flags = 0;
    clhy_listen_rec **listen_buckets;
    kuda_status_t rv;
    char id[16];
    int i;

    pconf = p;

    /* the reverse of pre_config, we want this only the first time around */
    if (retained->clmp->capi_loads == 1) {
        startup = 1;
        level_flags |= CLHYLOG_STARTUP;
    }

    if ((num_listensocks = clhy_setup_listeners(clhy_server_conf)) < 1) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT | level_flags, 0,
                     (startup ? NULL : s),
                     "no listening sockets available, shutting down");
        return !OK;
    }

    if (one_process) {
        retained->clmp->num_buckets = 1;
    }
    else if (!retained->clmp->was_graceful) {
        /* Preserve the number of buckets on graceful restarts. */
        retained->clmp->num_buckets = 0;
    }
    if ((rv = clhy_duplicate_listeners(pconf, clhy_server_conf,
                                     &listen_buckets, &retained->clmp->num_buckets))) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT | level_flags, rv,
                     (startup ? NULL : s),
                     "could not duplicate listeners");
        return !OK;
    }
    all_buckets = kuda_pcalloc(pconf, retained->clmp->num_buckets *
                                     sizeof(prefork_child_bucket));
    for (i = 0; i < retained->clmp->num_buckets; i++) {
        if ((rv = clhy_clmp_pod_open(pconf, &all_buckets[i].pod))) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT | level_flags, rv,
                         (startup ? NULL : s),
                         "could not open pipe-of-death");
            return !OK;
        }
        /* Initialize cross-process accept lock (safe accept needed only) */
        if ((rv = SAFE_ACCEPT((kuda_snprintf(id, sizeof id, "%i", i),
                               clhy_proc_mutex_create(&all_buckets[i].mutex,
                                                    NULL, CLHY_ACCEPT_MUTEX_TYPE,
                                                    id, s, pconf, 0))))) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT | level_flags, rv,
                         (startup ? NULL : s),
                         "could not create accept mutex");
            return !OK;
        }
        all_buckets[i].listeners = listen_buckets[i];
    }

    return OK;
}

static int prefork_pre_config(kuda_pool_t *p, kuda_pool_t *plog, kuda_pool_t *ptemp)
{
    int no_detach, debug, foreground;
    kuda_status_t rv;
    const char *userdata_key = "clmp_prefork_capi";

    debug = clhy_exists_config_define("DEBUG");

    if (debug) {
        foreground = one_process = 1;
        no_detach = 0;
    }
    else
    {
        no_detach = clhy_exists_config_define("NO_DETACH");
        one_process = clhy_exists_config_define("ONE_PROCESS");
        foreground = clhy_exists_config_define("FOREGROUND");
    }

    clhy_mutex_register(p, CLHY_ACCEPT_MUTEX_TYPE, NULL, KUDA_LOCK_DEFAULT, 0);

    retained = clhy_retained_data_get(userdata_key);
    if (!retained) {
        retained = clhy_retained_data_create(userdata_key, sizeof(*retained));
        retained->clmp = clhy_unixd_clmp_get_retained_data();
        retained->max_daemons_limit = -1;
        retained->idle_spawn_rate = 1;
    }
    retained->clmp->clmp_state = CLHY_CLMPQ_STARTING;
    if (retained->clmp->baton != retained) {
        retained->clmp->was_graceful = 0;
        retained->clmp->baton = retained;
    }
    ++retained->clmp->capi_loads;

    /* sigh, want this only the second time around */
    if (retained->clmp->capi_loads == 2) {
        if (!one_process && !foreground) {
            /* before we detach, setup crash handlers to log to errorlog */
            clhy_fatal_signal_setup(clhy_server_conf, pconf);
            rv = kuda_proc_detach(no_detach ? KUDA_PROC_DETACH_FOREGROUND
                                           : KUDA_PROC_DETACH_DAEMONIZE);
            if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, NULL, CLHYLOGNO(00174)
                             "kuda_proc_detach failed");
                return HTTP_INTERNAL_SERVER_ERROR;
            }
        }
    }

    parent_pid = clhy_my_pid = getpid();

    clhy_listen_pre_config();
    clhy_daemons_to_start = DEFAULT_START_DAEMON;
    clhy_daemons_min_free = DEFAULT_MIN_FREE_DAEMON;
    clhy_daemons_max_free = DEFAULT_MAX_FREE_DAEMON;
    server_limit = DEFAULT_SERVER_LIMIT;
    clhy_daemons_limit = server_limit;
    clhy_extended_status = 0;

    return OK;
}

static int prefork_check_config(kuda_pool_t *p, kuda_pool_t *plog,
                                kuda_pool_t *ptemp, server_rec *s)
{
    int startup = 0;

    /* the reverse of pre_config, we want this only the first time around */
    if (retained->clmp->capi_loads == 1) {
        startup = 1;
    }

    if (server_limit > MAX_SERVER_LIMIT) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00175)
                         "WARNING: ServerLimit of %d exceeds compile-time "
                         "limit of %d servers, decreasing to %d.",
                         server_limit, MAX_SERVER_LIMIT, MAX_SERVER_LIMIT);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00176)
                         "ServerLimit of %d exceeds compile-time limit "
                         "of %d, decreasing to match",
                         server_limit, MAX_SERVER_LIMIT);
        }
        server_limit = MAX_SERVER_LIMIT;
    }
    else if (server_limit < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00177)
                         "WARNING: ServerLimit of %d not allowed, "
                         "increasing to 1.", server_limit);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00178)
                         "ServerLimit of %d not allowed, increasing to 1",
                         server_limit);
        }
        server_limit = 1;
    }

    /* you cannot change ServerLimit across a restart; ignore
     * any such attempts
     */
    if (!retained->first_server_limit) {
        retained->first_server_limit = server_limit;
    }
    else if (server_limit != retained->first_server_limit) {
        /* don't need a startup console version here */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00179)
                     "changing ServerLimit to %d from original value of %d "
                     "not allowed during restart",
                     server_limit, retained->first_server_limit);
        server_limit = retained->first_server_limit;
    }

    if (clhy_daemons_limit > server_limit) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00180)
                         "WARNING: MaxRequestWorkers of %d exceeds ServerLimit "
                         "value of %d servers, decreasing MaxRequestWorkers to %d. "
                         "To increase, please see the ServerLimit directive.",
                         clhy_daemons_limit, server_limit, server_limit);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00181)
                         "MaxRequestWorkers of %d exceeds ServerLimit value "
                         "of %d, decreasing to match",
                         clhy_daemons_limit, server_limit);
        }
        clhy_daemons_limit = server_limit;
    }
    else if (clhy_daemons_limit < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00182)
                         "WARNING: MaxRequestWorkers of %d not allowed, "
                         "increasing to 1.", clhy_daemons_limit);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00183)
                         "MaxRequestWorkers of %d not allowed, increasing to 1",
                         clhy_daemons_limit);
        }
        clhy_daemons_limit = 1;
    }

    /* clhy_daemons_to_start > clhy_daemons_limit checked in prefork_run() */
    if (clhy_daemons_to_start < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00184)
                         "WARNING: StartServers of %d not allowed, "
                         "increasing to 1.", clhy_daemons_to_start);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00185)
                         "StartServers of %d not allowed, increasing to 1",
                         clhy_daemons_to_start);
        }
        clhy_daemons_to_start = 1;
    }

    if (clhy_daemons_min_free < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00186)
                         "WARNING: MinSpareServers of %d not allowed, "
                         "increasing to 1 to avoid almost certain server failure. "
                         "Please read the documentation.", clhy_daemons_min_free);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00187)
                         "MinSpareServers of %d not allowed, increasing to 1",
                         clhy_daemons_min_free);
        }
        clhy_daemons_min_free = 1;
    }

    /* clhy_daemons_max_free < clhy_daemons_min_free + 1 checked in prefork_run() */

    return OK;
}

static void prefork_hooks(kuda_pool_t *p)
{
    /* Our open_logs hook function must run before the core's, or stderr
     * will be redirected to a file, and the messages won't print to the
     * console.
     */
    static const char *const aszSucc[] = {"core.c", NULL};

    clhy_hook_open_logs(prefork_open_logs, NULL, aszSucc, KUDA_HOOK_REALLY_FIRST);
    /* we need to set the cLMP state before other pre-config hooks use cLMP query
     * to retrieve it, so register as REALLY_FIRST
     */
    clhy_hook_pre_config(prefork_pre_config, NULL, NULL, KUDA_HOOK_REALLY_FIRST);
    clhy_hook_check_config(prefork_check_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clcore(prefork_run, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clmp_query(prefork_query, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clmp_get_name(prefork_get_name, NULL, NULL, KUDA_HOOK_MIDDLE);
}

static const char *set_daemons_to_start(cmd_parms *cmd, void *dummy, const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    clhy_daemons_to_start = atoi(arg);
    return NULL;
}

static const char *set_min_free_servers(cmd_parms *cmd, void *dummy, const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    clhy_daemons_min_free = atoi(arg);
    return NULL;
}

static const char *set_max_free_servers(cmd_parms *cmd, void *dummy, const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    clhy_daemons_max_free = atoi(arg);
    return NULL;
}

static const char *set_max_clients (cmd_parms *cmd, void *dummy, const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }
    if (!strcasecmp(cmd->cmd->name, "MaxClients")) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, NULL, CLHYLOGNO(00188)
                     "MaxClients is deprecated, use MaxRequestWorkers "
                     "instead.");
    }
    clhy_daemons_limit = atoi(arg);
    return NULL;
}

static const char *set_server_limit (cmd_parms *cmd, void *dummy, const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    server_limit = atoi(arg);
    return NULL;
}

static const command_rec prefork_cmds[] = {
LISTEN_COMMANDS,
CLHY_INIT_TAKE1("StartServers", set_daemons_to_start, NULL, RSRC_CONF,
              "Number of child processes launched at server startup"),
CLHY_INIT_TAKE1("MinSpareServers", set_min_free_servers, NULL, RSRC_CONF,
              "Minimum number of idle children, to handle request spikes"),
CLHY_INIT_TAKE1("MaxSpareServers", set_max_free_servers, NULL, RSRC_CONF,
              "Maximum number of idle children"),
CLHY_INIT_TAKE1("MaxClients", set_max_clients, NULL, RSRC_CONF,
              "Deprecated name of MaxRequestWorkers"),
CLHY_INIT_TAKE1("MaxRequestWorkers", set_max_clients, NULL, RSRC_CONF,
              "Maximum number of children alive at the same time"),
CLHY_INIT_TAKE1("ServerLimit", set_server_limit, NULL, RSRC_CONF,
              "Maximum value of MaxRequestWorkers for this run of cLHy"),
CLHY_GRACEFUL_SHUTDOWN_TIMEOUT_COMMAND,
{ NULL }
};

CLHY_DECLARE_CAPI(clmp_prefork) = {
    CLMP16_CAPI_STUFF,
    NULL,                       /* hook to run before clhy parses args */
    NULL,                       /* create per-directory config structure */
    NULL,                       /* merge per-directory config structures */
    NULL,                       /* create per-server config structure */
    NULL,                       /* merge per-server config structures */
    prefork_cmds,               /* command kuda_table_t */
    prefork_hooks,              /* register hooks */
};
