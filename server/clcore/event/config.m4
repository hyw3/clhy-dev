AC_MSG_CHECKING(if event cLMP supports this platform)
if test $forking_clmp_supported != yes; then
    AC_MSG_RESULT(no - This is not a forking platform)
elif test $ac_cv_define_KUDA_HAS_THREADS != yes; then
    AC_MSG_RESULT(no - KUDA does not support threads)
elif test $have_threaded_sig_graceful != yes; then
    AC_MSG_RESULT(no - SIG_GRACEFUL cannot be used with a threaded cLMP)
elif test $ac_cv_have_threadsafe_pollset != yes; then
    AC_MSG_RESULT(no - KUDA_POLLSET_THREADSAFE is not supported)
elif test $kuda_has_skiplist != yes; then
    AC_MSG_RESULT(no - KUDA skiplist is not available, need KUDA 1.5.x or later)
else
    AC_MSG_RESULT(yes)
    CLHYKUDEL_CLMP_SUPPORTED(event, yes, yes)
fi
