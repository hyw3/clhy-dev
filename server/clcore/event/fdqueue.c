/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "fdqueue.h"
#include "kuda_atomic.h"

static const kuda_uint32_t zero_pt = KUDA_UINT32_MAX/2;

struct recycled_pool
{
    kuda_pool_t *pool;
    struct recycled_pool *next;
};

struct fd_queue_info_t
{
    kuda_uint32_t idlers;     /**
                              * >= zero_pt: number of idle worker threads
                              * < zero_pt:  number of threads blocked waiting
                              *             for an idle worker
                              */
    kuda_thread_mutex_t *idlers_mutex;
    kuda_thread_cond_t *wait_for_idler;
    int terminated;
    int max_idlers;
    int max_recycled_pools;
    kuda_uint32_t recycled_pools_count;
    struct recycled_pool *recycled_pools;
};

static kuda_status_t queue_info_cleanup(void *data_)
{
    fd_queue_info_t *qi = data_;
    kuda_thread_cond_destroy(qi->wait_for_idler);
    kuda_thread_mutex_destroy(qi->idlers_mutex);

    /* Clean up any pools in the recycled list */
    for (;;) {
        struct recycled_pool *first_pool = qi->recycled_pools;
        if (first_pool == NULL) {
            break;
        }
        if (kuda_atomic_casptr
            ((void*) &(qi->recycled_pools), first_pool->next,
             first_pool) == first_pool) {
            kuda_pool_destroy(first_pool->pool);
        }
    }

    return KUDA_SUCCESS;
}

kuda_status_t clhy_queue_info_create(fd_queue_info_t ** queue_info,
                                  kuda_pool_t * pool, int max_idlers,
                                  int max_recycled_pools)
{
    kuda_status_t rv;
    fd_queue_info_t *qi;

    qi = kuda_pcalloc(pool, sizeof(*qi));

    rv = kuda_thread_mutex_create(&qi->idlers_mutex, KUDA_THREAD_MUTEX_DEFAULT,
                                 pool);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }
    rv = kuda_thread_cond_create(&qi->wait_for_idler, pool);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }
    qi->recycled_pools = NULL;
    qi->max_recycled_pools = max_recycled_pools;
    qi->max_idlers = max_idlers;
    qi->idlers = zero_pt;
    kuda_pool_cleanup_register(pool, qi, queue_info_cleanup,
                              kuda_pool_cleanup_null);

    *queue_info = qi;

    return KUDA_SUCCESS;
}

kuda_status_t clhy_queue_info_set_idle(fd_queue_info_t * queue_info,
                                    kuda_pool_t * pool_to_recycle)
{
    kuda_status_t rv;
    kuda_int32_t prev_idlers;

    clhy_push_pool(queue_info, pool_to_recycle);

    /* Atomically increment the count of idle workers */
    prev_idlers = kuda_atomic_inc32(&(queue_info->idlers)) - zero_pt;

    /* If other threads are waiting on a worker, wake one up */
    if (prev_idlers < 0) {
        rv = kuda_thread_mutex_lock(queue_info->idlers_mutex);
        if (rv != KUDA_SUCCESS) {
            CLHY_DEBUG_ASSERT(0);
            return rv;
        }
        rv = kuda_thread_cond_signal(queue_info->wait_for_idler);
        if (rv != KUDA_SUCCESS) {
            kuda_thread_mutex_unlock(queue_info->idlers_mutex);
            return rv;
        }
        rv = kuda_thread_mutex_unlock(queue_info->idlers_mutex);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
    }

    return KUDA_SUCCESS;
}

kuda_status_t clhy_queue_info_try_get_idler(fd_queue_info_t * queue_info)
{
    kuda_int32_t new_idlers;
    new_idlers = kuda_atomic_add32(&(queue_info->idlers), -1) - zero_pt;
    if (--new_idlers <= 0) {
        kuda_atomic_inc32(&(queue_info->idlers));    /* back out dec */
        return KUDA_EAGAIN;
    }
    return KUDA_SUCCESS;
}

kuda_status_t clhy_queue_info_wait_for_idler(fd_queue_info_t * queue_info,
                                          int *had_to_block)
{
    kuda_status_t rv;
    kuda_int32_t prev_idlers;

    /* Atomically decrement the idle worker count, saving the old value */
    /* See TODO in clhy_queue_info_set_idle() */
    prev_idlers = kuda_atomic_add32(&(queue_info->idlers), -1) - zero_pt;

    /* Block if there weren't any idle workers */
    if (prev_idlers <= 0) {
        rv = kuda_thread_mutex_lock(queue_info->idlers_mutex);
        if (rv != KUDA_SUCCESS) {
            CLHY_DEBUG_ASSERT(0);
            /* See TODO in clhy_queue_info_set_idle() */
            kuda_atomic_inc32(&(queue_info->idlers));    /* back out dec */
            return rv;
        }
        /* Re-check the idle worker count to guard against a
         * race condition.  Now that we're in the mutex-protected
         * region, one of two things may have happened:
         *   - If the idle worker count is still negative, the
         *     workers are all still busy, so it's safe to
         *     block on a condition variable.
         *   - If the idle worker count is non-negative, then a
         *     worker has become idle since the first check
         *     of queue_info->idlers above.  It's possible
         *     that the worker has also signaled the condition
         *     variable--and if so, the listener missed it
         *     because it wasn't yet blocked on the condition
         *     variable.  But if the idle worker count is
         *     now non-negative, it's safe for this function to
         *     return immediately.
         *
         *     A "negative value" (relative to zero_pt) in
         *     queue_info->idlers tells how many
         *     threads are waiting on an idle worker.
         */
        if (queue_info->idlers < zero_pt) {
            *had_to_block = 1;
            rv = kuda_thread_cond_wait(queue_info->wait_for_idler,
                                      queue_info->idlers_mutex);
            if (rv != KUDA_SUCCESS) {
                kuda_status_t rv2;
                CLHY_DEBUG_ASSERT(0);
                rv2 = kuda_thread_mutex_unlock(queue_info->idlers_mutex);
                if (rv2 != KUDA_SUCCESS) {
                    return rv2;
                }
                return rv;
            }
        }
        rv = kuda_thread_mutex_unlock(queue_info->idlers_mutex);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
    }

    if (queue_info->terminated) {
        return KUDA_EOF;
    }
    else {
        return KUDA_SUCCESS;
    }
}

kuda_uint32_t clhy_queue_info_get_idlers(fd_queue_info_t * queue_info)
{
    kuda_int32_t val;
    val = (kuda_int32_t)kuda_atomic_read32(&queue_info->idlers) - zero_pt;
    if (val < 0)
        return 0;
    return val;
}

void clhy_push_pool(fd_queue_info_t * queue_info,
                                    kuda_pool_t * pool_to_recycle)
{
    struct recycled_pool *new_recycle;
    /* If we have been given a pool to recycle, atomically link
     * it into the queue_info's list of recycled pools
     */
    if (!pool_to_recycle)
        return;

    if (queue_info->max_recycled_pools >= 0) {
        kuda_uint32_t cnt = kuda_atomic_read32(&queue_info->recycled_pools_count);
        if (cnt >= queue_info->max_recycled_pools) {
            kuda_pool_destroy(pool_to_recycle);
            return;
        }
        kuda_atomic_inc32(&queue_info->recycled_pools_count);
    }

    kuda_pool_clear(pool_to_recycle);
    new_recycle = (struct recycled_pool *) kuda_palloc(pool_to_recycle,
                                                      sizeof (*new_recycle));
    new_recycle->pool = pool_to_recycle;
    for (;;) {
        /*
         * Save queue_info->recycled_pool in local variable next because
         * new_recycle->next can be changed after kuda_atomic_casptr
         * function call. For gory details see PR 44402.
         */
        struct recycled_pool *next = queue_info->recycled_pools;
        new_recycle->next = next;
        if (kuda_atomic_casptr((void*) &(queue_info->recycled_pools),
                              new_recycle, next) == next)
            break;
    }
}

void clhy_pop_pool(kuda_pool_t ** recycled_pool, fd_queue_info_t * queue_info)
{
    /* Atomically pop a pool from the recycled list */

    /* This function is safe only as long as it is single threaded because
     * it reaches into the queue and accesses "next" which can change.
     * We are OK today because it is only called from the listener thread.
     * cas-based pushes do not have the same limitation - any number can
     * happen concurrently with a single cas-based pop.
     */

    *recycled_pool = NULL;


    /* Atomically pop a pool from the recycled list */
    for (;;) {
        struct recycled_pool *first_pool = queue_info->recycled_pools;
        if (first_pool == NULL) {
            break;
        }
        if (kuda_atomic_casptr
            ((void*) &(queue_info->recycled_pools),
             first_pool->next, first_pool) == first_pool) {
            *recycled_pool = first_pool->pool;
            if (queue_info->max_recycled_pools >= 0)
                kuda_atomic_dec32(&queue_info->recycled_pools_count);
            break;
        }
    }
}

void clhy_free_idle_pools(fd_queue_info_t *queue_info)
{
    kuda_pool_t *p;

    queue_info->max_recycled_pools = 0;
    do {
        clhy_pop_pool(&p, queue_info);
        if (p != NULL)
            kuda_pool_destroy(p);
    } while (p != NULL);
}


kuda_status_t clhy_queue_info_term(fd_queue_info_t * queue_info)
{
    kuda_status_t rv;
    rv = kuda_thread_mutex_lock(queue_info->idlers_mutex);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }
    queue_info->terminated = 1;
    kuda_thread_cond_broadcast(queue_info->wait_for_idler);
    return kuda_thread_mutex_unlock(queue_info->idlers_mutex);
}

/**
 * Detects when the fd_queue_t is full. This utility function is expected
 * to be called from within critical sections, and is not threadsafe.
 */
#define clhy_queue_full(queue) ((queue)->nelts == (queue)->bounds)

/**
 * Detects when the fd_queue_t is empty. This utility function is expected
 * to be called from within critical sections, and is not threadsafe.
 */
#define clhy_queue_empty(queue) ((queue)->nelts == 0 && KUDA_RING_EMPTY(&queue->timers ,timer_event_t, link))

/**
 * Callback routine that is called to destroy this
 * fd_queue_t when its pool is destroyed.
 */
static kuda_status_t clhy_queue_destroy(void *data)
{
    fd_queue_t *queue = data;

    /* Ignore errors here, we can't do anything about them anyway.
     * XXX: We should at least try to signal an error here, it is
     * indicative of a programmer error. -aaron */
    kuda_thread_cond_destroy(queue->not_empty);
    kuda_thread_mutex_destroy(queue->one_big_mutex);

    return KUDA_SUCCESS;
}

/**
 * Initialize the fd_queue_t.
 */
kuda_status_t clhy_queue_init(fd_queue_t * queue, int queue_capacity,
                           kuda_pool_t * a)
{
    int i;
    kuda_status_t rv;

    if ((rv = kuda_thread_mutex_create(&queue->one_big_mutex,
                                      KUDA_THREAD_MUTEX_DEFAULT,
                                      a)) != KUDA_SUCCESS) {
        return rv;
    }
    if ((rv = kuda_thread_cond_create(&queue->not_empty, a)) != KUDA_SUCCESS) {
        return rv;
    }

    KUDA_RING_INIT(&queue->timers, timer_event_t, link);

    queue->data = kuda_palloc(a, queue_capacity * sizeof(fd_queue_elem_t));
    queue->bounds = queue_capacity;
    queue->nelts = 0;
    queue->in = 0;
    queue->out = 0;

    /* Set all the sockets in the queue to NULL */
    for (i = 0; i < queue_capacity; ++i)
        queue->data[i].sd = NULL;

    kuda_pool_cleanup_register(a, queue, clhy_queue_destroy,
                              kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}

/**
 * Push a new socket onto the queue.
 *
 * precondition: clhy_queue_info_wait_for_idler has already been called
 *               to reserve an idle worker thread
 */
kuda_status_t clhy_queue_push(fd_queue_t * queue, kuda_socket_t * sd,
                           event_conn_state_t * ecs, kuda_pool_t * p)
{
    fd_queue_elem_t *elem;
    kuda_status_t rv;

    if ((rv = kuda_thread_mutex_lock(queue->one_big_mutex)) != KUDA_SUCCESS) {
        return rv;
    }

    CLHY_DEBUG_ASSERT(!queue->terminated);
    CLHY_DEBUG_ASSERT(!clhy_queue_full(queue));

    elem = &queue->data[queue->in];
    queue->in++;
    if (queue->in >= queue->bounds)
        queue->in -= queue->bounds;
    elem->sd = sd;
    elem->ecs = ecs;
    elem->p = p;
    queue->nelts++;

    kuda_thread_cond_signal(queue->not_empty);

    if ((rv = kuda_thread_mutex_unlock(queue->one_big_mutex)) != KUDA_SUCCESS) {
        return rv;
    }

    return KUDA_SUCCESS;
}

kuda_status_t clhy_queue_push_timer(fd_queue_t * queue, timer_event_t *te)
{
    kuda_status_t rv;

    if ((rv = kuda_thread_mutex_lock(queue->one_big_mutex)) != KUDA_SUCCESS) {
        return rv;
    }

    CLHY_DEBUG_ASSERT(!queue->terminated);

    KUDA_RING_INSERT_TAIL(&queue->timers, te, timer_event_t, link);

    kuda_thread_cond_signal(queue->not_empty);

    if ((rv = kuda_thread_mutex_unlock(queue->one_big_mutex)) != KUDA_SUCCESS) {
        return rv;
    }

    return KUDA_SUCCESS;
}

/**
 * Retrieves the next available socket from the queue. If there are no
 * sockets available, it will block until one becomes available.
 * Once retrieved, the socket is placed into the address specified by
 * 'sd'.
 */
kuda_status_t clhy_queue_pop_something(fd_queue_t * queue, kuda_socket_t ** sd,
                                    event_conn_state_t ** ecs, kuda_pool_t ** p,
                                    timer_event_t ** te_out)
{
    fd_queue_elem_t *elem;
    kuda_status_t rv;

    if ((rv = kuda_thread_mutex_lock(queue->one_big_mutex)) != KUDA_SUCCESS) {
        return rv;
    }

    /* Keep waiting until we wake up and find that the queue is not empty. */
    if (clhy_queue_empty(queue)) {
        if (!queue->terminated) {
            kuda_thread_cond_wait(queue->not_empty, queue->one_big_mutex);
        }
        /* If we wake up and it's still empty, then we were interrupted */
        if (clhy_queue_empty(queue)) {
            rv = kuda_thread_mutex_unlock(queue->one_big_mutex);
            if (rv != KUDA_SUCCESS) {
                return rv;
            }
            if (queue->terminated) {
                return KUDA_EOF; /* no more elements ever again */
            }
            else {
                return KUDA_EINTR;
            }
        }
    }

    *te_out = NULL;

    if (!KUDA_RING_EMPTY(&queue->timers, timer_event_t, link)) {
        *te_out = KUDA_RING_FIRST(&queue->timers);
        KUDA_RING_REMOVE(*te_out, link);
    }
    else {
        elem = &queue->data[queue->out];
        queue->out++;
        if (queue->out >= queue->bounds)
            queue->out -= queue->bounds;
        queue->nelts--;
        *sd = elem->sd;
        *ecs = elem->ecs;
        *p = elem->p;
#ifdef CLHY_DEBUG
        elem->sd = NULL;
        elem->p = NULL;
#endif /* CLHY_DEBUG */
    }

    rv = kuda_thread_mutex_unlock(queue->one_big_mutex);
    return rv;
}

static kuda_status_t queue_interrupt(fd_queue_t * queue, int all)
{
    kuda_status_t rv;

    if ((rv = kuda_thread_mutex_lock(queue->one_big_mutex)) != KUDA_SUCCESS) {
        return rv;
    }
    if (all)
        kuda_thread_cond_broadcast(queue->not_empty);
    else
        kuda_thread_cond_signal(queue->not_empty);
    return kuda_thread_mutex_unlock(queue->one_big_mutex);
}

kuda_status_t clhy_queue_interrupt_all(fd_queue_t * queue)
{
    return queue_interrupt(queue, 1);
}

kuda_status_t clhy_queue_interrupt_one(fd_queue_t * queue)
{
    return queue_interrupt(queue, 0);
}

kuda_status_t clhy_queue_term(fd_queue_t * queue)
{
    kuda_status_t rv;

    if ((rv = kuda_thread_mutex_lock(queue->one_big_mutex)) != KUDA_SUCCESS) {
        return rv;
    }
    /* we must hold one_big_mutex when setting this... otherwise,
     * we could end up setting it and waking everybody up just after a
     * would-be popper checks it but right before they block
     */
    queue->terminated = 1;
    if ((rv = kuda_thread_mutex_unlock(queue->one_big_mutex)) != KUDA_SUCCESS) {
        return rv;
    }
    return clhy_queue_interrupt_all(queue);
}
