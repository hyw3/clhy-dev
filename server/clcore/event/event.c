/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This CLMP tries to fix the 'keep alive problem' in HTTP.
 *
 * After a client completes the first request, the client can keep the
 * connection open to send more requests with the same socket.  This can save
 * significant overhead in creating TCP connections.  However, the major
 * disadvantage is that cLHy traditionally keeps an entire child
 * process/thread waiting for data from the client.  To solve this problem,
 * this CLMP has a dedicated thread for handling both the Listening sockets,
 * and all sockets that are in a Keep Alive status.
 *
 * The CLMP assumes the underlying kuda_pollset implementation is somewhat
 * threadsafe.  This currently is only compatible with KQueue and EPoll.  This
 * enables the CLMP to avoid extra high level locking or having to wake up the
 * listener thread when a keep-alive socket needs to be sent to it.
 *
 * This CLMP does not perform well on older platforms that do not have very good
 * threading, like Linux with a 2.4 kernel, but this does not matter, since we
 * require EPoll or KQueue.
 *
 * For FreeBSD, use 5.3.  It is possible to run this CLMP on FreeBSD 5.2.1, if
 * you use libkse (see `man libmap.conf`).
 *
 * For NetBSD, use at least 2.0.
 *
 * For Linux, you should use a 2.6 kernel, and make sure your glibc has epoll
 * support compiled in.
 *
 */

#include "kuda.h"
#include "kuda_portable.h"
#include "kuda_strings.h"
#include "kuda_file_io.h"
#include "kuda_thread_proc.h"
#include "kuda_signal.h"
#include "kuda_thread_mutex.h"
#include "kuda_poll.h"
#include "kuda_ring.h"
#include "kuda_queue.h"
#include "kuda_atomic.h"
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"
#include "kuda_version.h"

#include <stdlib.h>

#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#if KUDA_HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#if KUDA_HAVE_SYS_WAIT_H
#include <sys/wait.h>
#endif
#ifdef HAVE_SYS_PROCESSOR_H
#include <sys/processor.h>      /* for bindprocessor() */
#endif

#if !KUDA_HAS_THREADS
#error The Event CLMP requires kuda threads, but they are unavailable.
#endif

#include "clhy_config.h"
#include "wwhy.h"
#include "http_main.h"
#include "http_log.h"
#include "http_config.h"        /* for read_config */
#include "http_core.h"          /* for get_remote_host */
#include "http_connection.h"
#include "http_protocol.h"
#include "clhy_core.h"
#include "core_common.h"
#include "clhy_listen.h"
#include "scoreboard.h"
#include "core_fdqueue.h"
#include "core_default.h"
#include "http_vhost.h"
#include "unixd.h"
#include "kuda_skiplist.h"

#include <signal.h>
#include <limits.h>             /* for INT_MAX */


/* Limit on the total --- clients will be locked out if more servers than
 * this are needed.  It is intended solely to keep the server from crashing
 * when things get out of hand.
 *
 * We keep a hard maximum number of servers, for two reasons --- first off,
 * in case something goes seriously wrong, we want to stop the fork bomb
 * short of actually crashing the machine we're running on by filling some
 * kernel table.  Secondly, it keeps the size of the scoreboard file small
 * enough that we can read the whole thing without worrying too much about
 * the overhead.
 */
#ifndef DEFAULT_SERVER_LIMIT
#define DEFAULT_SERVER_LIMIT 16
#endif

/* Admin can't tune ServerLimit beyond MAX_SERVER_LIMIT.  We want
 * some sort of compile-time limit to help catch typos.
 */
#ifndef MAX_SERVER_LIMIT
#define MAX_SERVER_LIMIT 20000
#endif

/* Limit on the threads per process.  Clients will be locked out if more than
 * this are needed.
 *
 * We keep this for one reason it keeps the size of the scoreboard file small
 * enough that we can read the whole thing without worrying too much about
 * the overhead.
 */
#ifndef DEFAULT_THREAD_LIMIT
#define DEFAULT_THREAD_LIMIT 64
#endif

/* Admin can't tune ThreadLimit beyond MAX_THREAD_LIMIT.  We want
 * some sort of compile-time limit to help catch typos.
 */
#ifndef MAX_THREAD_LIMIT
#define MAX_THREAD_LIMIT 100000
#endif

#define CLMP_CHILD_PID(i) (clhy_scoreboard_image->parent[i].pid)

#if !KUDA_VERSION_AT_LEAST(1,4,0)
#define kuda_time_from_msec(x) (x * 1000)
#endif

#ifndef MAX_SECS_TO_LINGER
#define MAX_SECS_TO_LINGER 30
#endif
#define SECONDS_TO_LINGER  2

/*
 * Actual definitions of config globals
 */

#ifndef DEFAULT_WORKER_FACTOR
#define DEFAULT_WORKER_FACTOR 2
#endif
#define WORKER_FACTOR_SCALE   16  /* scale factor to allow fractional values */
static unsigned int worker_factor = DEFAULT_WORKER_FACTOR * WORKER_FACTOR_SCALE;
    /* AsyncRequestWorkerFactor * 16 */

static int threads_per_child = 0;           /* ThreadsPerChild */
static int clhy_daemons_to_start = 0;         /* StartServers */
static int min_spare_threads = 0;           /* MinSpareThreads */
static int max_spare_threads = 0;           /* MaxSpareThreads */
static int active_daemons_limit = 0;        /* MaxRequestWorkers / ThreadsPerChild */
static int active_daemons = 0;              /* workers that still active, i.e. are
                                               not shutting down gracefully */
static int max_workers = 0;                 /* MaxRequestWorkers */
static int server_limit = 0;                /* ServerLimit */
static int thread_limit = 0;                /* ThreadLimit */
static int had_healthy_child = 0;
static volatile int dying = 0;
static volatile int workers_may_exit = 0;
static volatile int start_thread_may_exit = 0;
static volatile int listener_may_exit = 0;
static int listener_is_wakeable = 0;        /* Pollset supports KUDA_POLLSET_WAKEABLE */
static int num_listensocks = 0;
static kuda_int32_t conns_this_child;        /* MaxConnectionsPerChild, only access
                                               in listener thread */
static kuda_uint32_t connection_count = 0;   /* Number of open connections */
static kuda_uint32_t lingering_count = 0;    /* Number of connections in lingering close */
static kuda_uint32_t suspended_count = 0;    /* Number of suspended connections */
static kuda_uint32_t clogged_count = 0;      /* Number of threads processing ssl conns */
static kuda_uint32_t threads_shutdown = 0;   /* Number of threads that have shutdown
                                               early during graceful termination */
static int resource_shortage = 0;
static fd_queue_t *worker_queue;
static fd_queue_info_t *worker_queue_info;

static kuda_thread_mutex_t *timeout_mutex;

cAPI CLHY_CAPI_DECLARE_DATA clmp_event_capi;

/* forward declare */
struct event_srv_cfg_s;
typedef struct event_srv_cfg_s event_srv_cfg;

static kuda_pollfd_t *listener_pollfd;

/*
 * The pollset for sockets that are in any of the timeout queues. Currently
 * we use the timeout_mutex to make sure that connections are added/removed
 * atomically to/from both event_pollset and a timeout queue. Otherwise
 * some confusion can happen under high load if timeout queues and pollset
 * get out of sync.
 * XXX: It should be possible to make the lock unnecessary in many or even all
 * XXX: cases.
 */
static kuda_pollset_t *event_pollset;

typedef struct event_conn_state_t event_conn_state_t;

/*
 * The chain of connections to be shutdown by a worker thread (deferred),
 * linked list updated atomically.
 */
static event_conn_state_t *volatile defer_linger_chain;

struct event_conn_state_t {
    /** KUDA_RING of expiration timeouts */
    KUDA_RING_ENTRY(event_conn_state_t) timeout_list;
    /** the time when the entry was queued */
    kuda_time_t queue_timestamp;
    /** connection record this struct refers to */
    conn_rec *c;
    /** request record (if any) this struct refers to */
    request_rec *r;
    /** server config this struct refers to */
    event_srv_cfg *sc;
    /** scoreboard handle for the conn_rec */
    clhy_sb_handle_t *sbh;
    /** is the current conn_rec suspended?  (disassociated with
     * a particular CLMP thread; for suspend_/resume_connection
     * hooks)
     */
    int suspended;
    /** memory pool to allocate from */
    kuda_pool_t *p;
    /** bucket allocator */
    kuda_bucket_alloc_t *bucket_alloc;
    /** poll file descriptor information */
    kuda_pollfd_t pfd;
    /** public parts of the connection state */
    conn_state_t pub;
    /** chaining in defer_linger_chain */
    struct event_conn_state_t *chain;
};

KUDA_RING_HEAD(timeout_head_t, event_conn_state_t);

struct timeout_queue {
    struct timeout_head_t head;
    kuda_interval_time_t timeout;
    kuda_uint32_t count;         /* for this queue */
    kuda_uint32_t *total;        /* for all chained/related queues */
    struct timeout_queue *next; /* chaining */
};
/*
 * Several timeout queues that use different timeouts, so that we always can
 * simply append to the end.
 *   write_completion_q uses vhost's TimeOut
 *   keepalive_q        uses vhost's KeepAliveTimeOut
 *   linger_q           uses MAX_SECS_TO_LINGER
 *   short_linger_q     uses SECONDS_TO_LINGER
 */
static struct timeout_queue *write_completion_q,
                            *keepalive_q,
                            *linger_q,
                            *short_linger_q;
static volatile kuda_time_t  queues_next_expiry;

/* Prevent extra poll/wakeup calls for timeouts close in the future (queues
 * have the granularity of a second anyway).
 * XXX: Wouldn't 0.5s (instead of 0.1s) be "enough"?
 */
#define TIMEOUT_FUDGE_FACTOR kuda_time_from_msec(100)

/*
 * Macros for accessing struct timeout_queue.
 * For TO_QUEUE_APPEND and TO_QUEUE_REMOVE, timeout_mutex must be held.
 */
static void TO_QUEUE_APPEND(struct timeout_queue *q, event_conn_state_t *el)
{
    kuda_time_t q_expiry;
    kuda_time_t next_expiry;

    KUDA_RING_INSERT_TAIL(&q->head, el, event_conn_state_t, timeout_list);
    ++*q->total;
    ++q->count;

    /* Cheaply update the overall queues' next expiry according to the
     * first entry of this queue (oldest), if necessary.
     */
    el = KUDA_RING_FIRST(&q->head);
    q_expiry = el->queue_timestamp + q->timeout;
    next_expiry = queues_next_expiry;
    if (!next_expiry || next_expiry > q_expiry + TIMEOUT_FUDGE_FACTOR) {
        queues_next_expiry = q_expiry;
        /* Unblock the poll()ing listener for it to update its timeout. */
        if (listener_is_wakeable) {
            kuda_pollset_wakeup(event_pollset);
        }
    }
}

static void TO_QUEUE_REMOVE(struct timeout_queue *q, event_conn_state_t *el)
{
    KUDA_RING_REMOVE(el, timeout_list);
    KUDA_RING_ELEM_INIT(el, timeout_list);
    --*q->total;
    --q->count;
}

static struct timeout_queue *TO_QUEUE_MAKE(kuda_pool_t *p, kuda_time_t t,
                                           struct timeout_queue *ref)
{
    struct timeout_queue *q;
                                           
    q = kuda_pcalloc(p, sizeof *q);
    KUDA_RING_INIT(&q->head, event_conn_state_t, timeout_list);
    q->total = (ref) ? ref->total : kuda_pcalloc(p, sizeof *q->total);
    q->timeout = t;

    return q;
}

#define TO_QUEUE_ELEM_INIT(el) \
    KUDA_RING_ELEM_INIT((el), timeout_list)

/* The structure used to pass unique initialization info to each thread */
typedef struct
{
    int pslot;  /* process slot */
    int tslot;  /* worker slot of the thread */
} proc_info;

/* Structure used to pass information to the thread responsible for
 * creating the rest of the threads.
 */
typedef struct
{
    kuda_thread_t **threads;
    kuda_thread_t *listener;
    int child_num_arg;
    kuda_threadattr_t *threadattr;
} thread_starter;

typedef enum
{
    PT_CSD,
    PT_ACCEPT
} poll_type_e;

typedef struct
{
    poll_type_e type;
    void *baton;
} listener_poll_type;

/* data retained by event across load/unload of the cAPI
 * allocated on first call to pre-config hook; located on
 * subsequent calls to pre-config hook
 */
typedef struct event_retained_data {
    clhy_unixd_clmp_retained_data *clmp;

    int first_server_limit;
    int first_thread_limit;
    int sick_child_detected;
    int maxclients_reported;
    int near_maxclients_reported;
    /*
     * The max child slot ever assigned, preserved across restarts.  Necessary
     * to deal with MaxRequestWorkers changes across CLHY_SIG_GRACEFUL restarts.
     * We use this value to optimize routines that have to scan the entire
     * scoreboard.
     */
    int max_daemons_limit;

    /*
     * All running workers, active and shutting down, including those that
     * may be left from before a graceful restart.
     * Not kept up-to-date when shutdown is pending.
     */
    int total_daemons;

    /*
     * idle_spawn_rate is the number of children that will be spawned on the
     * next maintenance cycle if there aren't enough idle servers.  It is
     * maintained per listeners bucket, doubled up to MAX_SPAWN_RATE, and
     * reset only when a cycle goes by without the need to spawn.
     */
    int *idle_spawn_rate;
#ifndef MAX_SPAWN_RATE
#define MAX_SPAWN_RATE        (32)
#endif
    int hold_off_on_exponential_spawning;
} event_retained_data;
static event_retained_data *retained;
 
typedef struct event_child_bucket {
    clhy_pod_t *pod;
    clhy_listen_rec *listeners;
} event_child_bucket;
static event_child_bucket *all_buckets, /* All listeners buckets */
                          *my_bucket;   /* Current child bucket */

struct event_srv_cfg_s {
    struct timeout_queue *wc_q,
                         *ka_q;
};

#define ID_FROM_CHILD_THREAD(c, t)    ((c * thread_limit) + t)

/* The event CLMP respects a couple of runtime flags that can aid
 * in debugging. Setting the -DNO_DETACH flag will prevent the root process
 * from detaching from its controlling terminal. Additionally, setting
 * the -DONE_PROCESS flag (which implies -DNO_DETACH) will get you the
 * child_main loop running in the process which originally started up.
 * This gives you a pretty nice debugging environment.  (You'll get a SIGHUP
 * early in standalone_main; just continue through.  This is the server
 * trying to kill off any child processes which it might have lying
 * around --- cLHy doesn't keep track of their pids, it just sends
 * SIGHUP to the process group, ignoring it in the root process.
 * Continue through and you'll be fine.).
 */

static int one_process = 0;

#ifdef DEBUG_SIGSTOP
int raise_sigstop_flags;
#endif

static kuda_pool_t *pconf;       /* Pool for config stuff */
static kuda_pool_t *pchild;      /* Pool for wwhy child stuff */
static kuda_pool_t *pruntime;    /* Pool for CLMP threads stuff */

static pid_t clhy_my_pid;         /* Linux getpid() doesn't work except in main
                                   thread. Use this instead */
static pid_t parent_pid;
static kuda_platform_thread_t *listener_platform_thread;

static int clhy_child_slot;       /* Current child process slot in scoreboard */

/* The LISTENER_SIGNAL signal will be sent from the main thread to the
 * listener thread to wake it up for graceful termination (what a child
 * process from an old generation does when the admin does "delmanserve
 * graceful").  This signal will be blocked in all threads of a child
 * process except for the listener thread.
 */
#define LISTENER_SIGNAL     SIGHUP

/* An array of socket descriptors in use by each thread used to
 * perform a non-graceful (forced) shutdown of the server.
 */
static kuda_socket_t **worker_sockets;

static volatile kuda_uint32_t listensocks_disabled;

static void disable_listensocks(void)
{
    int i;
    if (kuda_atomic_cas32(&listensocks_disabled, 1, 0) != 0) {
        return;
    }
    if (event_pollset) {
        for (i = 0; i < num_listensocks; i++) {
            kuda_pollset_remove(event_pollset, &listener_pollfd[i]);
        }
    }
    clhy_scoreboard_image->parent[clhy_child_slot].not_accepting = 1;
}

static void enable_listensocks(void)
{
    int i;
    if (listener_may_exit
            || kuda_atomic_cas32(&listensocks_disabled, 0, 1) != 1) {
        return;
    }
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(00457)
                 "Accepting new connections again: "
                 "%u active conns (%u lingering/%u clogged/%u suspended), "
                 "%u idle workers",
                 kuda_atomic_read32(&connection_count),
                 kuda_atomic_read32(&lingering_count),
                 kuda_atomic_read32(&clogged_count),
                 kuda_atomic_read32(&suspended_count),
                 clhy_queue_info_num_idlers(worker_queue_info));
    for (i = 0; i < num_listensocks; i++)
        kuda_pollset_add(event_pollset, &listener_pollfd[i]);
    /*
     * XXX: This is not yet optimal. If many workers suddenly become available,
     * XXX: the parent may kill some processes off too soon.
     */
    clhy_scoreboard_image->parent[clhy_child_slot].not_accepting = 0;
}

static KUDA_INLINE kuda_uint32_t listeners_disabled(void)
{
    return kuda_atomic_read32(&listensocks_disabled);
}

static KUDA_INLINE int connections_above_limit(void)
{
    kuda_uint32_t i_count = clhy_queue_info_num_idlers(worker_queue_info);
    if (i_count > 0) {
        kuda_uint32_t c_count = kuda_atomic_read32(&connection_count);
        kuda_uint32_t l_count = kuda_atomic_read32(&lingering_count);
        if (c_count <= l_count
                /* Off by 'listeners_disabled()' to avoid flip flop */
                || c_count - l_count < (kuda_uint32_t)threads_per_child +
                                       (i_count - listeners_disabled()) *
                                       (worker_factor / WORKER_FACTOR_SCALE)) {
            return 0;
        }
    }
    return 1;
}

static void abort_socket_nonblocking(kuda_socket_t *csd)
{
    kuda_status_t rv;
    kuda_socket_timeout_set(csd, 0);
#if defined(SOL_SOCKET) && defined(SO_LINGER)
    /* This socket is over now, and we don't want to block nor linger
     * anymore, so reset it. A normal close could still linger in the
     * system, while RST is fast, nonblocking, and what the peer will
     * get if it sends us further data anyway.
     */
    {
        kuda_platform_sock_t osd = -1;
        struct linger opt;
        opt.l_onoff = 1;
        opt.l_linger = 0; /* zero timeout is RST */
        kuda_platform_sock_get(&osd, csd);
        setsockopt(osd, SOL_SOCKET, SO_LINGER, (void *)&opt, sizeof opt);
    }
#endif
    rv = kuda_socket_close(csd);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, clhy_server_conf, CLHYLOGNO(00468)
                     "error closing socket");
        CLHY_DEBUG_ASSERT(0);
    }
}

static void close_worker_sockets(void)
{
    int i;
    for (i = 0; i < threads_per_child; i++) {
        kuda_socket_t *csd = worker_sockets[i];
        if (csd) {
            worker_sockets[i] = NULL;
            abort_socket_nonblocking(csd);
        }
    }
    for (;;) {
        event_conn_state_t *cs = defer_linger_chain;
        if (!cs) {
            break;
        }
        if (kuda_atomic_casptr((void *)&defer_linger_chain, cs->chain,
                              cs) != cs) {
            /* Race lost, try again */
            continue;
        }
        cs->chain = NULL;
        abort_socket_nonblocking(cs->pfd.desc.s);
    }
}

static void wakeup_listener(void)
{
    listener_may_exit = 1;
    disable_listensocks();

    /* Unblock the listener if it's poll()ing */
    if (event_pollset && listener_is_wakeable) {
        kuda_pollset_wakeup(event_pollset);
    }

    /* unblock the listener if it's waiting for a worker */
    if (worker_queue_info) {
        clhy_queue_info_term(worker_queue_info);
    }

    if (!listener_platform_thread) {
        /* XXX there is an obscure path that this doesn't handle perfectly:
         *     right after listener thread is created but before
         *     listener_platform_thread is set, the first worker thread hits an
         *     error and starts graceful termination
         */
        return;
    }
    /*
     * we should just be able to "kill(clhy_my_pid, LISTENER_SIGNAL)" on all
     * platforms and wake up the listener thread since it is the only thread
     * with SIGHUP unblocked, but that doesn't work on Linux
     */
#ifdef HAVE_PTHREAD_KILL
    pthread_kill(*listener_platform_thread, LISTENER_SIGNAL);
#else
    kill(clhy_my_pid, LISTENER_SIGNAL);
#endif
}

#define ST_INIT              0
#define ST_GRACEFUL          1
#define ST_UNGRACEFUL        2

static int terminate_mode = ST_INIT;

static void signal_threads(int mode)
{
    if (terminate_mode >= mode) {
        return;
    }
    terminate_mode = mode;
    retained->clmp->clmp_state = CLHY_CLMPQ_STOPPING;

    /* in case we weren't called from the listener thread, wake up the
     * listener thread
     */
    wakeup_listener();

    /* for ungraceful termination, let the workers exit now;
     * for graceful termination, the listener thread will notify the
     * workers to exit once it has stopped accepting new connections
     */
    if (mode == ST_UNGRACEFUL) {
        workers_may_exit = 1;
        clhy_queue_interrupt_all(worker_queue);
        close_worker_sockets(); /* forcefully kill all current connections */
    }
}

static int event_query(int query_code, int *result, kuda_status_t *rv)
{
    *rv = KUDA_SUCCESS;
    switch (query_code) {
    case CLHY_CLMPQ_MAX_DAEMON_USED:
        *result = retained->max_daemons_limit;
        break;
    case CLHY_CLMPQ_IS_THREADED:
        *result = CLHY_CLMPQ_STATIC;
        break;
    case CLHY_CLMPQ_IS_FORKED:
        *result = CLHY_CLMPQ_DYNAMIC;
        break;
    case CLHY_CLMPQ_IS_ASYNC:
        *result = 1;
        break;
    case CLHY_CLMPQ_HARD_LIMIT_DAEMONS:
        *result = server_limit;
        break;
    case CLHY_CLMPQ_HARD_LIMIT_THREADS:
        *result = thread_limit;
        break;
    case CLHY_CLMPQ_MAX_THREADS:
        *result = threads_per_child;
        break;
    case CLHY_CLMPQ_MIN_SPARE_DAEMONS:
        *result = 0;
        break;
    case CLHY_CLMPQ_MIN_SPARE_THREADS:
        *result = min_spare_threads;
        break;
    case CLHY_CLMPQ_MAX_SPARE_DAEMONS:
        *result = 0;
        break;
    case CLHY_CLMPQ_MAX_SPARE_THREADS:
        *result = max_spare_threads;
        break;
    case CLHY_CLMPQ_MAX_REQUESTS_DAEMON:
        *result = clhy_max_requests_per_child;
        break;
    case CLHY_CLMPQ_MAX_DAEMONS:
        *result = active_daemons_limit;
        break;
    case CLHY_CLMPQ_CLMP_STATE:
        *result = retained->clmp->clmp_state;
        break;
    case CLHY_CLMPQ_GENERATION:
        *result = retained->clmp->my_generation;
        break;
    default:
        *rv = KUDA_ENOTIMPL;
        break;
    }
    return OK;
}

static void event_note_child_killed(int childnum, pid_t pid, clhy_generation_t gen)
{
    if (childnum != -1) { /* child had a scoreboard slot? */
        clhy_run_child_status(clhy_server_conf,
                            clhy_scoreboard_image->parent[childnum].pid,
                            clhy_scoreboard_image->parent[childnum].generation,
                            childnum, CLMP_CHILD_EXITED);
        clhy_scoreboard_image->parent[childnum].pid = 0;
    }
    else {
        clhy_run_child_status(clhy_server_conf, pid, gen, -1, CLMP_CHILD_EXITED);
    }
}

static void event_note_child_started(int slot, pid_t pid)
{
    clhy_generation_t gen = retained->clmp->my_generation;
    clhy_scoreboard_image->parent[slot].pid = pid;
    clhy_scoreboard_image->parent[slot].generation = gen;
    clhy_run_child_status(clhy_server_conf, pid, gen, slot, CLMP_CHILD_STARTED);
}

static const char *event_get_name(void)
{
    return "event";
}

/* a clean exit from a child with proper cleanup */
static void clean_child_exit(int code) __attribute__ ((noreturn));
static void clean_child_exit(int code)
{
    retained->clmp->clmp_state = CLHY_CLMPQ_STOPPING;
    if (pchild) {
        kuda_pool_destroy(pchild);
    }

    if (one_process) {
        event_note_child_killed(/* slot */ 0, 0, 0);
    }

    exit(code);
}

static void just_die(int sig)
{
    clean_child_exit(0);
}

/*****************************************************************
 * Connection structures and accounting...
 */

static int child_fatal;

static kuda_status_t decrement_connection_count(void *cs_)
{
    int is_last_connection;
    event_conn_state_t *cs = cs_;
    switch (cs->pub.state) {
        case CONN_STATE_LINGER_NORMAL:
        case CONN_STATE_LINGER_SHORT:
            kuda_atomic_dec32(&lingering_count);
            break;
        case CONN_STATE_SUSPENDED:
            kuda_atomic_dec32(&suspended_count);
            break;
        default:
            break;
    }
    /* Unblock the listener if it's waiting for connection_count = 0,
     * or if the listening sockets were disabled due to limits and can
     * now accept new connections.
     */
    is_last_connection = !kuda_atomic_dec32(&connection_count);
    if (listener_is_wakeable
            && ((is_last_connection && listener_may_exit)
                || (listeners_disabled() && !connections_above_limit()))) {
        kuda_pollset_wakeup(event_pollset);
    }
    return KUDA_SUCCESS;
}

static void notify_suspend(event_conn_state_t *cs)
{
    clhy_run_suspend_connection(cs->c, cs->r);
    cs->c->sbh = NULL;
    cs->suspended = 1;
}

static void notify_resume(event_conn_state_t *cs, int cleanup)
{
    cs->suspended = 0;
    cs->c->sbh = cleanup ? NULL : cs->sbh;
    clhy_run_resume_connection(cs->c, cs->r);
}

/*
 * Close our side of the connection, flushing data to the client first.
 * Pre-condition: cs is not in any timeout queue and not in the pollset,
 *                timeout_mutex is not locked
 * return: 0 if connection is fully closed,
 *         1 if connection is lingering
 * May only be called by worker thread.
 */
static int start_lingering_close_blocking(event_conn_state_t *cs)
{
    kuda_socket_t *csd = cs->pfd.desc.s;

    if (clhy_start_lingering_close(cs->c)) {
        notify_suspend(cs);
        kuda_socket_close(csd);
        clhy_queue_info_push_pool(worker_queue_info, cs->p);
        return DONE;
    }

#ifdef CLHY_DEBUG
    {
        kuda_status_t rv;
        rv = kuda_socket_timeout_set(csd, 0);
        CLHY_DEBUG_ASSERT(rv == KUDA_SUCCESS);
    }
#else
    kuda_socket_timeout_set(csd, 0);
#endif

    cs->queue_timestamp = kuda_time_now();
    /*
     * If some cAPI requested a shortened waiting period, only wait for
     * 2s (SECONDS_TO_LINGER). This is useful for mitigating certain
     * DoS attacks.
     */
    if (kuda_table_get(cs->c->notes, "short-lingering-close")) {
        cs->pub.state = CONN_STATE_LINGER_SHORT;
    }
    else {
        cs->pub.state = CONN_STATE_LINGER_NORMAL;
    }
    kuda_atomic_inc32(&lingering_count);
    notify_suspend(cs);

    return OK;
}

/*
 * Defer flush and close of the connection by adding it to defer_linger_chain,
 * for a worker to grab it and do the job (should that be blocking).
 * Pre-condition: cs is not in any timeout queue and not in the pollset,
 *                timeout_mutex is not locked
 * return: 1 connection is alive (but aside and about to linger)
 * May be called by listener thread.
 */
static int start_lingering_close_nonblocking(event_conn_state_t *cs)
{
    event_conn_state_t *chain;
    for (;;) {
        cs->chain = chain = defer_linger_chain;
        if (kuda_atomic_casptr((void *)&defer_linger_chain, cs,
                              chain) != chain) {
            /* Race lost, try again */
            continue;
        }
        return 1;
    }
}

/*
 * forcibly close a lingering connection after the lingering period has
 * expired
 * Pre-condition: cs is not in any timeout queue and not in the pollset
 * return: irrelevant (need same prototype as start_lingering_close)
 */
static int stop_lingering_close(event_conn_state_t *cs)
{
    kuda_socket_t *csd = clhy_get_conn_socket(cs->c);
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE4, 0, clhy_server_conf,
                 "socket abort in state %i", (int)cs->pub.state);
    abort_socket_nonblocking(csd);
    clhy_queue_info_push_pool(worker_queue_info, cs->p);
    if (dying)
        clhy_queue_interrupt_one(worker_queue);
    return 0;
}

/*
 * This runs before any non-CLMP cleanup code on the connection;
 * if the connection is currently suspended as far as cAPIs
 * know, provide notification of resumption.
 */
static kuda_status_t ptrans_pre_cleanup(void *dummy)
{
    event_conn_state_t *cs = dummy;

    if (cs->suspended) {
        notify_resume(cs, 1);
    }
    return KUDA_SUCCESS;
}

/*
 * event_pre_read_request() and event_request_cleanup() track the
 * current r for a given connection.
 */
static kuda_status_t event_request_cleanup(void *dummy)
{
    conn_rec *c = dummy;
    event_conn_state_t *cs = clhy_get_capi_config(c->conn_config,
                                                  &clmp_event_capi);

    cs->r = NULL;
    return KUDA_SUCCESS;
}

static void event_pre_read_request(request_rec *r, conn_rec *c)
{
    event_conn_state_t *cs = clhy_get_capi_config(c->conn_config,
                                                  &clmp_event_capi);

    cs->r = r;
    cs->sc = clhy_get_capi_config(clhy_server_conf->capi_config,
                                  &clmp_event_capi);
    kuda_pool_cleanup_register(r->pool, c, event_request_cleanup,
                              kuda_pool_cleanup_null);
}

/*
 * event_post_read_request() tracks the current server config for a
 * given request.
 */
static int event_post_read_request(request_rec *r)
{
    conn_rec *c = r->connection;
    event_conn_state_t *cs = clhy_get_capi_config(c->conn_config,
                                                  &clmp_event_capi);

    /* To preserve legacy behaviour (consistent with other CLMPs), use
     * the keepalive timeout from the base server (first on this IP:port)
     * when none is explicitly configured on this server.
     */
    if (r->server->keep_alive_timeout_set) {
        cs->sc = clhy_get_capi_config(r->server->capi_config,
                                      &clmp_event_capi);
    }
    else {
        cs->sc = clhy_get_capi_config(c->base_server->capi_config,
                                      &clmp_event_capi);
    }
    return OK;
}

/* Forward declare */
static void process_lingering_close(event_conn_state_t *cs);

/*
 * process one connection in the worker
 */
static void process_socket(kuda_thread_t *thd, kuda_pool_t * p, kuda_socket_t * sock,
                          event_conn_state_t * cs, int my_child_num,
                          int my_thread_num)
{
    conn_rec *c;
    long conn_id = ID_FROM_CHILD_THREAD(my_child_num, my_thread_num);
    int clogging = 0;
    kuda_status_t rv;
    int rc = OK;

    if (cs == NULL) {           /* This is a new connection */
        listener_poll_type *pt = kuda_pcalloc(p, sizeof(*pt));
        cs = kuda_pcalloc(p, sizeof(event_conn_state_t));
        cs->bucket_alloc = kuda_bucket_alloc_create(p);
        clhy_create_sb_handle(&cs->sbh, p, my_child_num, my_thread_num);
        c = clhy_run_create_connection(p, clhy_server_conf, sock,
                                     conn_id, cs->sbh, cs->bucket_alloc);
        if (!c) {
            clhy_queue_info_push_pool(worker_queue_info, p);
            return;
        }
        kuda_atomic_inc32(&connection_count);
        kuda_pool_cleanup_register(c->pool, cs, decrement_connection_count,
                                  kuda_pool_cleanup_null);
        clhy_set_capi_config(c->conn_config, &clmp_event_capi, cs);
        c->current_thread = thd;
        cs->c = c;
        c->cs = &(cs->pub);
        cs->p = p;
        cs->sc = clhy_get_capi_config(clhy_server_conf->capi_config,
                                      &clmp_event_capi);
        cs->pfd.desc_type = KUDA_POLL_SOCKET;
        cs->pfd.reqevents = KUDA_POLLIN;
        cs->pfd.desc.s = sock;
        pt->type = PT_CSD;
        pt->baton = cs;
        cs->pfd.client_data = pt;
        kuda_pool_pre_cleanup_register(p, cs, ptrans_pre_cleanup);
        TO_QUEUE_ELEM_INIT(cs);

        clhy_update_vhost_given_ip(c);

        rc = clhy_run_pre_connection(c, sock);
        if (rc != OK && rc != DONE) {
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, c, CLHYLOGNO(00469)
                          "process_socket: connection aborted");
            c->aborted = 1;
        }

        /**
         * XXX If the platform does not have a usable way of bundling
         * accept() with a socket readability check, like Win32,
         * and there are measurable delays before the
         * socket is readable due to the first data packet arriving,
         * it might be better to create the cs on the listener thread
         * with the state set to CONN_STATE_CHECK_REQUEST_LINE_READABLE
         *
         * FreeBSD users will want to enable the HTTP accept filter
         * cAPI in their kernel for the highest performance
         * When the accept filter is active, sockets are kept in the
         * kernel until a HTTP request is received.
         */
        cs->pub.state = CONN_STATE_READ_REQUEST_LINE;

        cs->pub.sense = CONN_SENSE_DEFAULT;
        rc = OK;
    }
    else {
        c = cs->c;
        clhy_update_sb_handle(cs->sbh, my_child_num, my_thread_num);
        notify_resume(cs, 0);
        c->current_thread = thd;
        /* Subsequent request on a conn, and thread number is part of ID */
        c->id = conn_id;
    }

    if (c->aborted) {
        /* do lingering close below */
        cs->pub.state = CONN_STATE_LINGER;
    }
    else if (cs->pub.state >= CONN_STATE_LINGER) {
        /* fall through */
    }
    else {
        if (cs->pub.state == CONN_STATE_READ_REQUEST_LINE
            /* If we have an input filter which 'clogs' the input stream,
             * like capi_ssl used to, lets just do the normal read from input
             * filters, like the Worker CLMP does. Filters that need to write
             * where they would otherwise read, or read where they would
             * otherwise write, should set the sense appropriately.
             */
             || c->clogging_input_filters) {
read_request:
            clogging = c->clogging_input_filters;
            if (clogging) {
                kuda_atomic_inc32(&clogged_count);
            }
            rc = clhy_run_process_connection(c);
            if (clogging) {
                kuda_atomic_dec32(&clogged_count);
            }
            if (cs->pub.state > CONN_STATE_LINGER) {
                cs->pub.state = CONN_STATE_LINGER;
            }
            if (rc == DONE) {
                rc = OK;
            }
        }
    }
    /*
     * The process_connection hooks above should set the connection state
     * appropriately upon return, for event CLMP to either:
     * - do lingering close (CONN_STATE_LINGER),
     * - wait for readability of the next request with respect to the keepalive
     *   timeout (state CONN_STATE_CHECK_REQUEST_LINE_READABLE),
     * - wait for read/write-ability of the underlying socket with respect to
     *   its timeout by setting c->clogging_input_filters to 1 and the sense
     *   to CONN_SENSE_WANT_READ/WRITE (state CONN_STATE_WRITE_COMPLETION),
     * - keep flushing the output filters stack in nonblocking mode, and then
     *   if required wait for read/write-ability of the underlying socket with
     *   respect to its own timeout (state CONN_STATE_WRITE_COMPLETION); since
     *   completion at some point may require reads (e.g. SSL_ERROR_WANT_READ),
     *   an output filter can also set the sense to CONN_SENSE_WANT_READ at any
     *   time for event CLMP to do the right thing,
     * - suspend the connection (SUSPENDED) such that it now interracts with
     *   the CLMP through suspend/resume_connection() hooks, and/or registered
     *   poll callbacks (PT_USER), and/or registered timed callbacks triggered
     *   by timer events.
     * If a process_connection hook returns an error or no hook sets the state
     * to one of the above expected value, we forcibly close the connection w/
     * CONN_STATE_LINGER.  This covers the cases where no process_connection
     * hook executes (DECLINED), or one returns OK w/o touching the state (i.e.
     * CONN_STATE_READ_REQUEST_LINE remains after the call) which can happen
     * with third-party cAPIs not updated to work specifically with event CLMP
     * while this was expected to do lingering close unconditionally with
     * worker or prefork CLMPs for instance.
     */
    if (rc != OK || (cs->pub.state >= CONN_STATE_NUM)
                 || (cs->pub.state < CONN_STATE_LINGER
                     && cs->pub.state != CONN_STATE_WRITE_COMPLETION
                     && cs->pub.state != CONN_STATE_CHECK_REQUEST_LINE_READABLE
                     && cs->pub.state != CONN_STATE_SUSPENDED)) {
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, c, CLHYLOGNO(10111)
                      "process_socket: connection processing %s: closing",
                      rc ? kuda_psprintf(c->pool, "returned error %i", rc)
                         : kuda_psprintf(c->pool, "unexpected state %i",
                                                 (int)cs->pub.state));
        cs->pub.state = CONN_STATE_LINGER;
    }

    if (cs->pub.state == CONN_STATE_WRITE_COMPLETION) {
        clhy_filter_t *output_filter = c->output_filters;
        kuda_status_t rv;
        clhy_update_child_status(cs->sbh, SERVER_BUSY_WRITE, NULL);
        while (output_filter->next != NULL) {
            output_filter = output_filter->next;
        }
        rv = output_filter->frec->filter_func.out_func(output_filter, NULL);
        if (rv != KUDA_SUCCESS) {
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, c, CLHYLOGNO(00470)
                          "network write failure in core output filter");
            cs->pub.state = CONN_STATE_LINGER;
        }
        else if (c->data_in_output_filters ||
                 cs->pub.sense == CONN_SENSE_WANT_READ) {
            /* Still in WRITE_COMPLETION_STATE:
             * Set a read/write timeout for this connection, and let the
             * event thread poll for read/writeability.
             */
            cs->queue_timestamp = kuda_time_now();
            notify_suspend(cs);

            if (cs->pub.sense == CONN_SENSE_WANT_READ) {
                cs->pfd.reqevents = KUDA_POLLIN;
            }
            else {
                cs->pfd.reqevents = KUDA_POLLOUT;
            }
            /* POLLHUP/ERR are usually returned event only (ignored here), but
             * some pollset backends may require them in reqevents to do the
             * right thing, so it shouldn't hurt.
             */
            cs->pfd.reqevents |= KUDA_POLLHUP | KUDA_POLLERR;
            cs->pub.sense = CONN_SENSE_DEFAULT;

            kuda_thread_mutex_lock(timeout_mutex);
            TO_QUEUE_APPEND(cs->sc->wc_q, cs);
            rv = kuda_pollset_add(event_pollset, &cs->pfd);
            if (rv != KUDA_SUCCESS && !KUDA_STATUS_IS_EEXIST(rv)) {
                CLHY_DEBUG_ASSERT(0);
                TO_QUEUE_REMOVE(cs->sc->wc_q, cs);
                kuda_thread_mutex_unlock(timeout_mutex);
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, clhy_server_conf, CLHYLOGNO(03465)
                             "process_socket: kuda_pollset_add failure for "
                             "write completion");
                kuda_socket_close(cs->pfd.desc.s);
                clhy_queue_info_push_pool(worker_queue_info, cs->p);
            }
            else {
                kuda_thread_mutex_unlock(timeout_mutex);
            }
            return;
        }
        else if (c->keepalive != CLHY_CONN_KEEPALIVE || c->aborted ||
                 listener_may_exit) {
            cs->pub.state = CONN_STATE_LINGER;
        }
        else if (c->data_in_input_filters) {
            cs->pub.state = CONN_STATE_READ_REQUEST_LINE;
            goto read_request;
        }
        else {
            cs->pub.state = CONN_STATE_CHECK_REQUEST_LINE_READABLE;
        }
    }

    if (cs->pub.state == CONN_STATE_CHECK_REQUEST_LINE_READABLE) {
        clhy_update_child_status(cs->sbh, SERVER_BUSY_KEEPALIVE, NULL);

        /* It greatly simplifies the logic to use a single timeout value per q
         * because the new element can just be added to the end of the list and
         * it will stay sorted in expiration time sequence.  If brand new
         * sockets are sent to the event thread for a readability check, this
         * will be a slight behavior change - they use the non-keepalive
         * timeout today.  With a normal client, the socket will be readable in
         * a few milliseconds anyway.
         */
        cs->queue_timestamp = kuda_time_now();
        notify_suspend(cs);

        /* Add work to pollset. */
        cs->pfd.reqevents = KUDA_POLLIN;
        kuda_thread_mutex_lock(timeout_mutex);
        TO_QUEUE_APPEND(cs->sc->ka_q, cs);
        rv = kuda_pollset_add(event_pollset, &cs->pfd);
        if (rv != KUDA_SUCCESS && !KUDA_STATUS_IS_EEXIST(rv)) {
            CLHY_DEBUG_ASSERT(0);
            TO_QUEUE_REMOVE(cs->sc->ka_q, cs);
            kuda_thread_mutex_unlock(timeout_mutex);
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, clhy_server_conf, CLHYLOGNO(03093)
                         "process_socket: kuda_pollset_add failure for "
                         "keep alive");
            kuda_socket_close(cs->pfd.desc.s);
            clhy_queue_info_push_pool(worker_queue_info, cs->p);
        }
        else {
            kuda_thread_mutex_unlock(timeout_mutex);
        }
        return;
    }

    if (cs->pub.state == CONN_STATE_SUSPENDED) {
        kuda_atomic_inc32(&suspended_count);
        notify_suspend(cs);
        return;
    }

    if (cs->pub.state == CONN_STATE_LINGER) {
        rc = start_lingering_close_blocking(cs);
    }
    if (rc == OK && (cs->pub.state == CONN_STATE_LINGER_NORMAL ||
                     cs->pub.state == CONN_STATE_LINGER_SHORT)) {
        process_lingering_close(cs);
    }
}

/* conns_this_child has gone to zero or below.  See if the admin coded
   "MaxConnectionsPerChild 0", and keep going in that case.  Doing it this way
   simplifies the hot path in worker_thread */
static void check_infinite_requests(void)
{
    if (clhy_max_requests_per_child) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, clhy_server_conf,
                     "Stopping process due to MaxConnectionsPerChild");
        signal_threads(ST_GRACEFUL);
    }
    else {
        /* keep going */
        conns_this_child = KUDA_INT32_MAX;
    }
}

static void close_listeners(int *closed)
{
    if (!*closed) {
        int i;
        clhy_close_listeners_ex(my_bucket->listeners);
        *closed = 1;
        dying = 1;
        clhy_scoreboard_image->parent[clhy_child_slot].quiescing = 1;
        for (i = 0; i < threads_per_child; ++i) {
            clhy_update_child_status_from_indexes(clhy_child_slot, i,
                                                SERVER_GRACEFUL, NULL);
        }
        /* wake up the main thread */
        kill(clhy_my_pid, SIGTERM);

        clhy_queue_info_free_idle_pools(worker_queue_info);
        clhy_queue_interrupt_all(worker_queue);
    }
}

static void unblock_signal(int sig)
{
    sigset_t sig_mask;

    sigemptyset(&sig_mask);
    sigaddset(&sig_mask, sig);
#if defined(SIGPROCMASK_SETS_THREAD_MASK)
    sigprocmask(SIG_UNBLOCK, &sig_mask, NULL);
#else
    pthread_sigmask(SIG_UNBLOCK, &sig_mask, NULL);
#endif
}

static void dummy_signal_handler(int sig)
{
    /* XXX If specifying SIG_IGN is guaranteed to unblock a syscall,
     *     then we don't need this goofy function.
     */
}


static kuda_status_t push_timer2worker(timer_event_t* te)
{
    return clhy_queue_push_timer(worker_queue, te);
}

/*
 * Pre-condition: cs is neither in event_pollset nor a timeout queue
 * this function may only be called by the listener
 */
static kuda_status_t push2worker(event_conn_state_t *cs, kuda_socket_t *csd,
                                kuda_pool_t *ptrans)
{
    kuda_status_t rc;

    if (cs) {
        csd = cs->pfd.desc.s;
        ptrans = cs->p;
    }
    rc = clhy_queue_push_socket(worker_queue, csd, cs, ptrans);
    if (rc != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rc, clhy_server_conf, CLHYLOGNO(00471)
                     "push2worker: clhy_queue_push_socket failed");
        /* trash the connection; we couldn't queue the connected
         * socket to a worker
         */
        if (csd) {
            abort_socket_nonblocking(csd);
        }
        if (ptrans) {
            clhy_queue_info_push_pool(worker_queue_info, ptrans);
        }
        signal_threads(ST_GRACEFUL);
    }

    return rc;
}

/* get_worker:
 *     If *have_idle_worker_p == 0, reserve a worker thread, and set
 *     *have_idle_worker_p = 1.
 *     If *have_idle_worker_p is already 1, will do nothing.
 *     If blocking == 1, block if all workers are currently busy.
 *     If no worker was available immediately, will set *all_busy to 1.
 *     XXX: If there are no workers, we should not block immediately but
 *     XXX: close all keep-alive connections first.
 */
static void get_worker(int *have_idle_worker_p, int blocking, int *all_busy)
{
    kuda_status_t rc;

    if (*have_idle_worker_p) {
        /* already reserved a worker thread - must have hit a
         * transient error on a previous pass
         */
        return;
    }

    if (blocking)
        rc = clhy_queue_info_wait_for_idler(worker_queue_info, all_busy);
    else
        rc = clhy_queue_info_try_get_idler(worker_queue_info);

    if (rc == KUDA_SUCCESS || KUDA_STATUS_IS_EOF(rc)) {
        *have_idle_worker_p = 1;
    }
    else if (!blocking && rc == KUDA_EAGAIN) {
        *all_busy = 1;
    }
    else {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rc, clhy_server_conf, CLHYLOGNO(00472)
                     "clhy_queue_info_wait_for_idler failed.  "
                     "Attempting to shutdown process gracefully");
        signal_threads(ST_GRACEFUL);
    }
}

/* Structures to reuse */
static KUDA_RING_HEAD(timer_free_ring_t, timer_event_t) timer_free_ring;

static kuda_skiplist *timer_skiplist;
static volatile kuda_time_t timers_next_expiry;

/* Same goal as for TIMEOUT_FUDGE_FACTOR (avoid extra poll calls), but applied
 * to timers. Since their timeouts are custom (user defined), we can't be too
 * approximative here (hence using 0.01s).
 */
#define EVENT_FUDGE_FACTOR kuda_time_from_msec(10)

/* The following compare function is used by kuda_skiplist_insert() to keep the
 * elements (timers) sorted and provide O(log n) complexity (this is also true
 * for kuda_skiplist_{find,remove}(), but those are not used in CLMP event where
 * inserted timers are not searched nor removed, but with kuda_skiplist_pop()
 * which does use any compare function).  It is meant to return 0 when a == b,
 * <0 when a < b, and >0 when a > b.  However kuda_skiplist_insert() will not
 * add duplicates (i.e. a == b), and kuda_skiplist_add() is only available in
 * kuda 1.6, yet multiple timers could possibly be created in the same micro-
 * second (duplicates with regard to kuda_time_t); therefore we implement the
 * compare function to return +1 instead of 0 when compared timers are equal,
 * thus duplicates are still added after each other (in order of insertion).
 */
static int timer_comp(void *a, void *b)
{
    kuda_time_t t1 = (kuda_time_t) ((timer_event_t *)a)->when;
    kuda_time_t t2 = (kuda_time_t) ((timer_event_t *)b)->when;
    CLHY_DEBUG_ASSERT(t1);
    CLHY_DEBUG_ASSERT(t2);
    return ((t1 < t2) ? -1 : 1);
}

static kuda_thread_mutex_t *g_timer_skiplist_mtx;

static kuda_status_t event_register_timed_callback(kuda_time_t t,
                                                  clhy_clmp_callback_fn_t *cbfn,
                                                  void *baton)
{
    timer_event_t *te;
    /* oh yeah, and make locking smarter/fine grained. */
    kuda_thread_mutex_lock(g_timer_skiplist_mtx);

    if (!KUDA_RING_EMPTY(&timer_free_ring, timer_event_t, link)) {
        te = KUDA_RING_FIRST(&timer_free_ring);
        KUDA_RING_REMOVE(te, link);
    }
    else {
        te = kuda_skiplist_alloc(timer_skiplist, sizeof(timer_event_t));
        KUDA_RING_ELEM_INIT(te, link);
    }

    te->cbfunc = cbfn;
    te->baton = baton;
    /* XXXXX: optimize */
    te->when = t + kuda_time_now();

    { 
        kuda_time_t next_expiry;

        /* Okay, add sorted by when.. */
        kuda_skiplist_insert(timer_skiplist, te);

        /* Cheaply update the overall timers' next expiry according to
         * this event, if necessary.
         */
        next_expiry = timers_next_expiry;
        if (!next_expiry || next_expiry > te->when + EVENT_FUDGE_FACTOR) {
            timers_next_expiry = te->when;
            /* Unblock the poll()ing listener for it to update its timeout. */
            if (listener_is_wakeable) {
                kuda_pollset_wakeup(event_pollset);
            }
        }
    }

    kuda_thread_mutex_unlock(g_timer_skiplist_mtx);

    return KUDA_SUCCESS;
}


/*
 * Close socket and clean up if remote closed its end while we were in
 * lingering close. Only to be called in the worker thread, and since it's
 * in immediate call stack, we can afford a comfortable buffer size to
 * consume data quickly.
 */
#define LINGERING_BUF_SIZE (32 * 1024)
static void process_lingering_close(event_conn_state_t *cs)
{
    kuda_socket_t *csd = clhy_get_conn_socket(cs->c);
    char dummybuf[LINGERING_BUF_SIZE];
    kuda_size_t nbytes;
    kuda_status_t rv;
    struct timeout_queue *q;

    /* socket is already in non-blocking state */
    do {
        nbytes = sizeof(dummybuf);
        rv = kuda_socket_recv(csd, dummybuf, &nbytes);
    } while (rv == KUDA_SUCCESS);

    if (!KUDA_STATUS_IS_EAGAIN(rv)) {
        rv = kuda_socket_close(csd);
        CLHY_DEBUG_ASSERT(rv == KUDA_SUCCESS);
        clhy_queue_info_push_pool(worker_queue_info, cs->p);
        return;
    }

    /* Re-queue the connection to come back when readable */
    cs->pfd.reqevents = KUDA_POLLIN;
    cs->pub.sense = CONN_SENSE_DEFAULT;
    q = (cs->pub.state == CONN_STATE_LINGER_SHORT) ? short_linger_q : linger_q;
    kuda_thread_mutex_lock(timeout_mutex);
    TO_QUEUE_APPEND(q, cs);
    rv = kuda_pollset_add(event_pollset, &cs->pfd);
    if (rv != KUDA_SUCCESS && !KUDA_STATUS_IS_EEXIST(rv)) {
        CLHY_DEBUG_ASSERT(0);
        TO_QUEUE_REMOVE(q, cs);
        kuda_thread_mutex_unlock(timeout_mutex);
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, clhy_server_conf, CLHYLOGNO(03092)
                     "process_lingering_close: kuda_pollset_add failure");
        rv = kuda_socket_close(cs->pfd.desc.s);
        CLHY_DEBUG_ASSERT(rv == KUDA_SUCCESS);
        clhy_queue_info_push_pool(worker_queue_info, cs->p);
        return;
    }
    kuda_thread_mutex_unlock(timeout_mutex);
}

/* call 'func' for all elements of 'q' with timeout less than 'timeout_time'.
 * Pre-condition: timeout_mutex must already be locked
 * Post-condition: timeout_mutex will be locked again
 */
static void process_timeout_queue(struct timeout_queue *q,
                                  kuda_time_t timeout_time,
                                  int (*func)(event_conn_state_t *))
{
    kuda_uint32_t total = 0, count;
    event_conn_state_t *first, *cs, *last;
    struct timeout_head_t trash;
    struct timeout_queue *qp;
    kuda_status_t rv;

    if (!*q->total) {
        return;
    }

    KUDA_RING_INIT(&trash, event_conn_state_t, timeout_list);
    for (qp = q; qp; qp = qp->next) {
        count = 0;
        cs = first = last = KUDA_RING_FIRST(&qp->head);
        while (cs != KUDA_RING_SENTINEL(&qp->head, event_conn_state_t,
                                       timeout_list)) {
            /* Trash the entry if:
             * - no timeout_time was given (asked for all), or
             * - it expired (according to the queue timeout), or
             * - the system clock skewed in the past: no entry should be
             *   registered above the given timeout_time (~now) + the queue
             *   timeout, we won't keep any here (eg. for centuries).
             *
             * Otherwise stop, no following entry will match thanks to the
             * single timeout per queue (entries are added to the end!).
             * This allows maintenance in O(1).
             */
            if (timeout_time
                    && cs->queue_timestamp + qp->timeout > timeout_time
                    && cs->queue_timestamp < timeout_time + qp->timeout) {
                /* Since this is the next expiring of this queue, update the
                 * overall queues' next expiry if it's later than this one.
                 */
                kuda_time_t q_expiry = cs->queue_timestamp + qp->timeout;
                kuda_time_t next_expiry = queues_next_expiry;
                if (!next_expiry || next_expiry > q_expiry) {
                    queues_next_expiry = q_expiry;
                }
                break;
            }

            last = cs;
            rv = kuda_pollset_remove(event_pollset, &cs->pfd);
            if (rv != KUDA_SUCCESS && !KUDA_STATUS_IS_NOTFOUND(rv)) {
                CLHY_DEBUG_ASSERT(0);
                clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, cs->c, CLHYLOGNO(00473)
                              "kuda_pollset_remove failed");
            }
            cs = KUDA_RING_NEXT(cs, timeout_list);
            count++;
        }
        if (!count)
            continue;

        KUDA_RING_UNSPLICE(first, last, timeout_list);
        KUDA_RING_SPLICE_TAIL(&trash, first, last, event_conn_state_t,
                             timeout_list);
        CLHY_DEBUG_ASSERT(*q->total >= count && qp->count >= count);
        *q->total -= count;
        qp->count -= count;
        total += count;
    }
    if (!total)
        return;

    kuda_thread_mutex_unlock(timeout_mutex);
    first = KUDA_RING_FIRST(&trash);
    do {
        cs = KUDA_RING_NEXT(first, timeout_list);
        TO_QUEUE_ELEM_INIT(first);
        func(first);
        first = cs;
    } while (--total);
    kuda_thread_mutex_lock(timeout_mutex);
}

static void process_keepalive_queue(kuda_time_t timeout_time)
{
    /* If all workers are busy, we kill older keep-alive connections so
     * that they may connect to another process.
     */
    if (!timeout_time) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, clhy_server_conf,
                     "All workers are busy or dying, will close %u "
                     "keep-alive connections", *keepalive_q->total);
    }
    process_timeout_queue(keepalive_q, timeout_time,
                          start_lingering_close_nonblocking);
}

static void * KUDA_THREAD_FUNC listener_thread(kuda_thread_t * thd, void *dummy)
{
    kuda_status_t rc;
    proc_info *ti = dummy;
    int process_slot = ti->pslot;
    struct process_score *ps = clhy_get_scoreboard_process(process_slot);
    int closed = 0;
    int have_idle_worker = 0;
    kuda_time_t last_log;

    last_log = kuda_time_now();
    free(ti);

    /* Unblock the signal used to wake this thread up, and set a handler for
     * it.
     */
    unblock_signal(LISTENER_SIGNAL);
    kuda_signal(LISTENER_SIGNAL, dummy_signal_handler);

    for (;;) {
        timer_event_t *te;
        const kuda_pollfd_t *out_pfd;
        kuda_int32_t num = 0;
        kuda_interval_time_t timeout_interval;
        kuda_time_t now, timeout_time;
        int workers_were_busy = 0;

        if (conns_this_child <= 0)
            check_infinite_requests();

        if (listener_may_exit) {
            close_listeners(&closed);
            if (terminate_mode == ST_UNGRACEFUL
                || kuda_atomic_read32(&connection_count) == 0)
                break;
        }

        now = kuda_time_now();
        if (CLHYLOGtrace6(clhy_server_conf)) {
            /* trace log status every second */
            if (now - last_log > kuda_time_from_sec(1)) {
                last_log = now;
                kuda_thread_mutex_lock(timeout_mutex);
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE6, 0, clhy_server_conf,
                             "connections: %u (clogged: %u write-completion: %d "
                             "keep-alive: %d lingering: %d suspended: %u)",
                             kuda_atomic_read32(&connection_count),
                             kuda_atomic_read32(&clogged_count),
                             *(volatile kuda_uint32_t*)write_completion_q->total,
                             *(volatile kuda_uint32_t*)keepalive_q->total,
                             kuda_atomic_read32(&lingering_count),
                             kuda_atomic_read32(&suspended_count));
                if (dying) {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE6, 0, clhy_server_conf,
                                 "%u/%u workers shutdown",
                                 kuda_atomic_read32(&threads_shutdown),
                                 threads_per_child);
                }
                kuda_thread_mutex_unlock(timeout_mutex);
            }
        }

        /* Start with an infinite poll() timeout and update it according to
         * the next expiring timer or queue entry. If there are none, either
         * the listener is wakeable and it can poll() indefinitely until a wake
         * up occurs, otherwise periodic checks (maintenance, shutdown, ...)
         * must be performed.
         */
        timeout_interval = -1;

        /* Push expired timers to a worker, the first remaining one determines
         * the maximum time to poll() below, if any.
         */
        timeout_time = timers_next_expiry;
        if (timeout_time && timeout_time < now + EVENT_FUDGE_FACTOR) {
            kuda_thread_mutex_lock(g_timer_skiplist_mtx);
            while ((te = kuda_skiplist_peek(timer_skiplist))) {
                if (te->when > now + EVENT_FUDGE_FACTOR) {
                    timers_next_expiry = te->when;
                    timeout_interval = te->when - now;
                    break;
                }
                kuda_skiplist_pop(timer_skiplist, NULL);
                push_timer2worker(te);
            }
            if (!te) {
                timers_next_expiry = 0;
            }
            kuda_thread_mutex_unlock(g_timer_skiplist_mtx);
        }

        /* Same for queues, use their next expiry, if any. */
        timeout_time = queues_next_expiry;
        if (timeout_time
                && (timeout_interval < 0
                    || timeout_time <= now
                    || timeout_interval > timeout_time - now)) {
            timeout_interval = timeout_time > now ? timeout_time - now : 1;
        }

        /* When non-wakeable, don't wait more than 100 ms, in any case. */
#define NON_WAKEABLE_POLL_TIMEOUT kuda_time_from_msec(100)
        if (!listener_is_wakeable
                && (timeout_interval < 0
                    || timeout_interval > NON_WAKEABLE_POLL_TIMEOUT)) {
            timeout_interval = NON_WAKEABLE_POLL_TIMEOUT;
        }

        rc = kuda_pollset_poll(event_pollset, timeout_interval, &num, &out_pfd);
        if (rc != KUDA_SUCCESS) {
            if (KUDA_STATUS_IS_EINTR(rc)) {
                /* Woken up, if we are exiting or listeners are disabled we
                 * must fall through to kill kept-alive connections or test
                 * whether listeners should be re-enabled. Otherwise we only
                 * need to update timeouts (logic is above, so simply restart
                 * the loop).
                 */
                if (!listener_may_exit && !listeners_disabled()) {
                    continue;
                }
                timeout_time = 0;
            }
            else if (!KUDA_STATUS_IS_TIMEUP(rc)) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rc, clhy_server_conf,
                             "kuda_pollset_poll failed.  Attempting to "
                             "shutdown process gracefully");
                signal_threads(ST_GRACEFUL);
            }
            num = 0;
        }

        if (listener_may_exit) {
            close_listeners(&closed);
            if (terminate_mode == ST_UNGRACEFUL
                || kuda_atomic_read32(&connection_count) == 0)
                break;
        }

        for (; num; --num, ++out_pfd) {
            listener_poll_type *pt = (listener_poll_type *) out_pfd->client_data;
            if (pt->type == PT_CSD) {
                /* one of the sockets is readable */
                event_conn_state_t *cs = (event_conn_state_t *) pt->baton;
                struct timeout_queue *remove_from_q = NULL;
                /* don't wait for a worker for a keepalive request or
                 * lingering close processing. */
                int blocking = 0;

                switch (cs->pub.state) {
                case CONN_STATE_WRITE_COMPLETION:
                    remove_from_q = cs->sc->wc_q;
                    blocking = 1;
                    break;

                case CONN_STATE_CHECK_REQUEST_LINE_READABLE:
                    cs->pub.state = CONN_STATE_READ_REQUEST_LINE;
                    remove_from_q = cs->sc->ka_q;
                    break;

                case CONN_STATE_LINGER_NORMAL:
                    remove_from_q = linger_q;
                    break;

                case CONN_STATE_LINGER_SHORT:
                    remove_from_q = short_linger_q;
                    break;

                default:
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rc,
                                 clhy_server_conf, CLHYLOGNO(03096)
                                 "event_loop: unexpected state %d",
                                 cs->pub.state);
                    clhy_assert(0);
                }

                if (remove_from_q) {
                    kuda_thread_mutex_lock(timeout_mutex);
                    TO_QUEUE_REMOVE(remove_from_q, cs);
                    rc = kuda_pollset_remove(event_pollset, &cs->pfd);
                    kuda_thread_mutex_unlock(timeout_mutex);
                    /*
                     * Some of the pollset backends, like KQueue or Epoll
                     * automagically remove the FD if the socket is closed,
                     * therefore, we can accept _SUCCESS or _NOTFOUND,
                     * and we still want to keep going
                     */
                    if (rc != KUDA_SUCCESS && !KUDA_STATUS_IS_NOTFOUND(rc)) {
                        CLHY_DEBUG_ASSERT(0);
                        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rc, clhy_server_conf,
                                     CLHYLOGNO(03094) "pollset remove failed");
                        start_lingering_close_nonblocking(cs);
                        break;
                    }

                    /* If we don't get a worker immediately (nonblocking), we
                     * close the connection; the client can re-connect to a
                     * different process for keepalive, and for lingering close
                     * the connection will be reset so the choice is to favor
                     * incoming/alive connections.
                     */
                    get_worker(&have_idle_worker, blocking,
                               &workers_were_busy);
                    if (!have_idle_worker) {
                        if (remove_from_q == cs->sc->ka_q) {
                            start_lingering_close_nonblocking(cs);
                        }
                        else {
                            stop_lingering_close(cs);
                        }
                    }
                    else if (push2worker(cs, NULL, NULL) == KUDA_SUCCESS) {
                        have_idle_worker = 0;
                    }
                }
            }
            else if (pt->type == PT_ACCEPT && !listeners_disabled()) {
                /* A Listener Socket is ready for an accept() */
                if (workers_were_busy) {
                    disable_listensocks();
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf,
                                 "All workers busy, not accepting new conns "
                                 "in this process");
                }
                else if (connections_above_limit()) {
                    disable_listensocks();
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf,
                                 "Too many open connections (%u), "
                                 "not accepting new conns in this process",
                                 kuda_atomic_read32(&connection_count));
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, clhy_server_conf,
                                 "Idle workers: %u",
                                 clhy_queue_info_num_idlers(worker_queue_info));
                    workers_were_busy = 1;
                }
                else if (!listener_may_exit) {
                    void *csd = NULL;
                    clhy_listen_rec *lr = (clhy_listen_rec *) pt->baton;
                    kuda_pool_t *ptrans;         /* Pool for per-transaction stuff */
                    clhy_queue_info_pop_pool(worker_queue_info, &ptrans);

                    if (ptrans == NULL) {
                        /* create a new transaction pool for each accepted socket */
                        kuda_allocator_t *allocator = NULL;

                        rc = kuda_allocator_create(&allocator);
                        if (rc == KUDA_SUCCESS) {
                            kuda_allocator_max_free_set(allocator,
                                                       clhy_max_mem_free);
                            rc = kuda_pool_create_ex(&ptrans, pconf, NULL,
                                                    allocator);
                            if (rc == KUDA_SUCCESS) {
                                kuda_pool_tag(ptrans, "transaction");
                                kuda_allocator_owner_set(allocator, ptrans);
                            }
                        }
                        if (rc != KUDA_SUCCESS) {
                            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rc,
                                         clhy_server_conf, CLHYLOGNO(03097)
                                         "Failed to create transaction pool");
                            if (allocator) {
                                kuda_allocator_destroy(allocator);
                            }
                            resource_shortage = 1;
                            signal_threads(ST_GRACEFUL);
                            continue;
                        }
                    }

                    get_worker(&have_idle_worker, 1, &workers_were_busy);
                    rc = lr->accept_func(&csd, lr, ptrans);

                    /* later we trash rv and rely on csd to indicate
                     * success/failure
                     */
                    CLHY_DEBUG_ASSERT(rc == KUDA_SUCCESS || !csd);

                    if (rc == KUDA_EGENERAL) {
                        /* E[NM]FILE, ENOMEM, etc */
                        resource_shortage = 1;
                        signal_threads(ST_GRACEFUL);
                    }

                    if (csd != NULL) {
                        conns_this_child--;
                        if (push2worker(NULL, csd, ptrans) == KUDA_SUCCESS) {
                            have_idle_worker = 0;
                        }
                    }
                    else {
                        clhy_queue_info_push_pool(worker_queue_info, ptrans);
                    }
                }
            }               /* if:else on pt->type */
        } /* for processing poll */

        /* XXX possible optimization: stash the current time for use as
         * r->request_time for new requests
         */
        /* We process the timeout queues here only when their overall next
         * expiry (read once above) is over. This happens accurately since
         * adding to the queues (in workers) can only decrease this expiry,
         * while latest ones are only taken into account here (in listener)
         * during queues' processing, with the lock held. This works both
         * with and without wake-ability.
         */
        if (timeout_time && timeout_time < (now = kuda_time_now())) {
            timeout_time = now + TIMEOUT_FUDGE_FACTOR;

            /* handle timed out sockets */
            kuda_thread_mutex_lock(timeout_mutex);

            /* Processing all the queues below will recompute this. */
            queues_next_expiry = 0;

            /* Step 1: keepalive timeouts */
            if (workers_were_busy || dying) {
                process_keepalive_queue(0); /* kill'em all \m/ */
            }
            else {
                process_keepalive_queue(timeout_time);
            }
            /* Step 2: write completion timeouts */
            process_timeout_queue(write_completion_q, timeout_time,
                                  start_lingering_close_nonblocking);
            /* Step 3: (normal) lingering close completion timeouts */
            process_timeout_queue(linger_q, timeout_time,
                                  stop_lingering_close);
            /* Step 4: (short) lingering close completion timeouts */
            process_timeout_queue(short_linger_q, timeout_time,
                                  stop_lingering_close);

            kuda_thread_mutex_unlock(timeout_mutex);

            ps->keep_alive = *(volatile kuda_uint32_t*)keepalive_q->total;
            ps->write_completion = *(volatile kuda_uint32_t*)write_completion_q->total;
            ps->connections = kuda_atomic_read32(&connection_count);
            ps->suspended = kuda_atomic_read32(&suspended_count);
            ps->lingering_close = kuda_atomic_read32(&lingering_count);
        }
        else if ((workers_were_busy || dying)
                 && *(volatile kuda_uint32_t*)keepalive_q->total) {
            kuda_thread_mutex_lock(timeout_mutex);
            process_keepalive_queue(0); /* kill'em all \m/ */
            kuda_thread_mutex_unlock(timeout_mutex);
            ps->keep_alive = 0;
        }

        /* If there are some lingering closes to defer (to a worker), schedule
         * them now. We might wakeup a worker spuriously if another one empties
         * defer_linger_chain in the meantime, but there also may be no active
         * or all busy workers for an undefined time.  In any case a deferred
         * lingering close can't starve if we do that here since the chain is
         * filled only above in the listener and it's emptied only in the
         * worker(s); thus a NULL here means it will stay so while the listener
         * waits (possibly indefinitely) in poll().
         */
        if (defer_linger_chain) {
            get_worker(&have_idle_worker, 0, &workers_were_busy);
            if (have_idle_worker
                    && defer_linger_chain /* re-test */
                    && push2worker(NULL, NULL, NULL) == KUDA_SUCCESS) {
                have_idle_worker = 0;
            }
        }

        if (listeners_disabled()
                && !workers_were_busy
                && !connections_above_limit()) {
            enable_listensocks();
        }
    } /* listener main loop */

    close_listeners(&closed);
    clhy_queue_term(worker_queue);

    kuda_thread_exit(thd, KUDA_SUCCESS);
    return NULL;
}

/*
 * During graceful shutdown, if there are more running worker threads than
 * open connections, exit one worker thread.
 *
 * return 1 if thread should exit, 0 if it should continue running.
 */
static int worker_thread_should_exit_early(void)
{
    for (;;) {
        kuda_uint32_t conns = kuda_atomic_read32(&connection_count);
        kuda_uint32_t dead = kuda_atomic_read32(&threads_shutdown);
        kuda_uint32_t newdead;

        CLHY_DEBUG_ASSERT(dead <= threads_per_child);
        if (conns >= threads_per_child - dead)
            return 0;

        newdead = dead + 1;
        if (kuda_atomic_cas32(&threads_shutdown, newdead, dead) == dead) {
            /*
             * No other thread has exited in the mean time, safe to exit
             * this one.
             */
            return 1;
        }
    }
}

/* XXX For ungraceful termination/restart, we definitely don't want to
 *     wait for active connections to finish but we may want to wait
 *     for idle workers to get out of the queue code and release mutexes,
 *     since those mutexes are cleaned up pretty soon and some systems
 *     may not react favorably (i.e., segfault) if operations are attempted
 *     on cleaned-up mutexes.
 */
static void *KUDA_THREAD_FUNC worker_thread(kuda_thread_t * thd, void *dummy)
{
    proc_info *ti = dummy;
    int process_slot = ti->pslot;
    int thread_slot = ti->tslot;
    kuda_status_t rv;
    int is_idle = 0;

    free(ti);

    clhy_scoreboard_image->servers[process_slot][thread_slot].pid = clhy_my_pid;
    clhy_scoreboard_image->servers[process_slot][thread_slot].tid = kuda_platform_thread_current();
    clhy_scoreboard_image->servers[process_slot][thread_slot].generation = retained->clmp->my_generation;
    clhy_update_child_status_from_indexes(process_slot, thread_slot,
                                        SERVER_STARTING, NULL);

    while (!workers_may_exit) {
        kuda_socket_t *csd = NULL;
        event_conn_state_t *cs;
        timer_event_t *te = NULL;
        kuda_pool_t *ptrans;         /* Pool for per-transaction stuff */

        if (!is_idle) {
            rv = clhy_queue_info_set_idle(worker_queue_info, NULL);
            if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, clhy_server_conf,
                             "clhy_queue_info_set_idle failed. Attempting to "
                             "shutdown process gracefully.");
                signal_threads(ST_GRACEFUL);
                break;
            }
            is_idle = 1;
        }

        clhy_update_child_status_from_indexes(process_slot, thread_slot,
                                            dying ? SERVER_GRACEFUL
                                                  : SERVER_READY, NULL);
      worker_pop:
        if (workers_may_exit) {
            break;
        }
        if (dying && worker_thread_should_exit_early()) {
            break;
        }

        rv = clhy_queue_pop_something(worker_queue, &csd, (void **)&cs,
                                    &ptrans, &te);

        if (rv != KUDA_SUCCESS) {
            /* We get KUDA_EOF during a graceful shutdown once all the
             * connections accepted by this server process have been handled.
             */
            if (KUDA_STATUS_IS_EOF(rv)) {
                break;
            }
            /* We get KUDA_EINTR whenever clhy_queue_pop_*() has been interrupted
             * from an explicit call to clhy_queue_interrupt_all(). This allows
             * us to unblock threads stuck in clhy_queue_pop_*() when a shutdown
             * is pending.
             *
             * If workers_may_exit is set and this is ungraceful termination/
             * restart, we are bound to get an error on some systems (e.g.,
             * AIX, which sanity-checks mutex operations) since the queue
             * may have already been cleaned up.  Don't log the "error" if
             * workers_may_exit is set.
             */
            else if (KUDA_STATUS_IS_EINTR(rv)) {
                goto worker_pop;
            }
            /* We got some other error. */
            else if (!workers_may_exit) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, clhy_server_conf,
                             CLHYLOGNO(03099) "clhy_queue_pop_socket failed");
            }
            continue;
        }
        if (te != NULL) {
            te->cbfunc(te->baton);

            {
                kuda_thread_mutex_lock(g_timer_skiplist_mtx);
                KUDA_RING_INSERT_TAIL(&timer_free_ring, te, timer_event_t, link);
                kuda_thread_mutex_unlock(g_timer_skiplist_mtx);
            }
        }
        else {
            is_idle = 0;
            if (csd != NULL) {
                worker_sockets[thread_slot] = csd;
                process_socket(thd, ptrans, csd, cs, process_slot, thread_slot);
                worker_sockets[thread_slot] = NULL;
            }
        }

        /* If there are deferred lingering closes, handle them now. */
        while (!workers_may_exit) {
            cs = defer_linger_chain;
            if (!cs) {
                break;
            }
            if (kuda_atomic_casptr((void *)&defer_linger_chain, cs->chain,
                                  cs) != cs) {
                /* Race lost, try again */
                continue;
            }
            cs->chain = NULL;

            worker_sockets[thread_slot] = csd = cs->pfd.desc.s;
#ifdef CLHY_DEBUG
            rv = kuda_socket_timeout_set(csd, SECONDS_TO_LINGER);
            CLHY_DEBUG_ASSERT(rv == KUDA_SUCCESS);
#else
            kuda_socket_timeout_set(csd, SECONDS_TO_LINGER);
#endif
            cs->pub.state = CONN_STATE_LINGER;
            process_socket(thd, cs->p, csd, cs, process_slot, thread_slot);
            worker_sockets[thread_slot] = NULL;
        }
    }

    clhy_update_child_status_from_indexes(process_slot, thread_slot,
                                        dying ? SERVER_DEAD
                                              : SERVER_GRACEFUL, NULL);

    kuda_thread_exit(thd, KUDA_SUCCESS);
    return NULL;
}

static int check_signal(int signum)
{
    switch (signum) {
    case SIGTERM:
    case SIGINT:
        return 1;
    }
    return 0;
}

static void create_listener_thread(thread_starter * ts)
{
    int my_child_num = ts->child_num_arg;
    kuda_threadattr_t *thread_attr = ts->threadattr;
    proc_info *my_info;
    kuda_status_t rv;

    my_info = (proc_info *) clhy_malloc(sizeof(proc_info));
    my_info->pslot = my_child_num;
    my_info->tslot = -1;      /* listener thread doesn't have a thread slot */
    rv = kuda_thread_create(&ts->listener, thread_attr, listener_thread,
                           my_info, pruntime);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, rv, clhy_server_conf, CLHYLOGNO(00474)
                     "kuda_thread_create: unable to create listener thread");
        /* let the parent decide how bad this really is */
        clean_child_exit(APEXIT_CHILDSICK);
    }
    kuda_platform_thread_get(&listener_platform_thread, ts->listener);
}

static void setup_threads_runtime(void)
{
    kuda_status_t rv;
    clhy_listen_rec *lr;
    kuda_pool_t *pskip = NULL;
    int max_recycled_pools = -1, i;
    const int good_methods[] = { KUDA_POLLSET_KQUEUE,
                                 KUDA_POLLSET_PORT,
                                 KUDA_POLLSET_EPOLL };
    /* XXX: K-A or lingering close connection included in the async factor */
    const kuda_uint32_t async_factor = worker_factor / WORKER_FACTOR_SCALE;
    const kuda_uint32_t pollset_size = (kuda_uint32_t)num_listensocks +
                                      (kuda_uint32_t)threads_per_child *
                                      (async_factor > 2 ? async_factor : 2);
    int pollset_flags;

    /* Event's skiplist operations will happen concurrently with other cAPIs'
     * runtime so they need their own pool for allocations, and its lifetime
     * should be at least the one of the connections (ptrans). Thus pskip is
     * created as a subpool of pconf like/before ptrans (before so that it's
     * destroyed after). In forked mode pconf is never destroyed so we are good
     * anyway, but in ONE_PROCESS mode this ensures that the skiplist works
     * from connection/ptrans cleanups (even after pchild is destroyed).
     */
    kuda_pool_create(&pskip, pconf);
    kuda_pool_tag(pskip, "clmp_skiplist");
    kuda_thread_mutex_create(&g_timer_skiplist_mtx, KUDA_THREAD_MUTEX_DEFAULT, pskip);
    KUDA_RING_INIT(&timer_free_ring, timer_event_t, link);
    kuda_skiplist_init(&timer_skiplist, pskip);
    kuda_skiplist_set_compare(timer_skiplist, timer_comp, timer_comp);

    /* All threads (listener, workers) and synchronization objects (queues,
     * pollset, mutexes...) created here should have at least the lifetime of
     * the connections they handle (i.e. ptrans). We can't use this thread's
     * self pool because all these objects survive it, nor use pchild or pconf
     * directly because this starter thread races with other cAPIs' runtime,
     * nor finally pchild (or subpool thereof) because it is killed explicitely
     * before pconf (thus connections/ptrans can live longer, which matters in
     * ONE_PROCESS mode). So this leaves us with a subpool of pconf, created
     * before any ptrans hence destroyed after.
     */
    kuda_pool_create(&pruntime, pconf);
    kuda_pool_tag(pruntime, "clmp_runtime");

    /* We must create the fd queues before we start up the listener
     * and worker threads. */
    rv = clhy_queue_create(&worker_queue, threads_per_child, pruntime);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, rv, clhy_server_conf, CLHYLOGNO(03100)
                     "clhy_queue_create() failed");
        clean_child_exit(APEXIT_CHILDFATAL);
    }

    if (clhy_max_mem_free != KUDA_ALLOCATOR_MAX_FREE_UNLIMITED) {
        /* If we want to conserve memory, let's not keep an unlimited number of
         * pools & allocators.
         * XXX: This should probably be a separate config directive
         */
        max_recycled_pools = threads_per_child * 3 / 4 ;
    }
    rv = clhy_queue_info_create(&worker_queue_info, pruntime,
                              threads_per_child, max_recycled_pools);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, rv, clhy_server_conf, CLHYLOGNO(03101)
                     "clhy_queue_info_create() failed");
        clean_child_exit(APEXIT_CHILDFATAL);
    }

    /* Create the timeout mutex and main pollset before the listener
     * thread starts.
     */
    rv = kuda_thread_mutex_create(&timeout_mutex, KUDA_THREAD_MUTEX_DEFAULT,
                                 pruntime);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, clhy_server_conf, CLHYLOGNO(03102)
                     "creation of the timeout mutex failed.");
        clean_child_exit(APEXIT_CHILDFATAL);
    }

    /* Create the main pollset */
    pollset_flags = KUDA_POLLSET_THREADSAFE | KUDA_POLLSET_NOCOPY |
                    KUDA_POLLSET_NODEFAULT | KUDA_POLLSET_WAKEABLE;
    for (i = 0; i < sizeof(good_methods) / sizeof(good_methods[0]); i++) {
        rv = kuda_pollset_create_ex(&event_pollset, pollset_size, pruntime,
                                   pollset_flags, good_methods[i]);
        if (rv == KUDA_SUCCESS) {
            listener_is_wakeable = 1;
            break;
        }
    }
    if (rv != KUDA_SUCCESS) {
        pollset_flags &= ~KUDA_POLLSET_WAKEABLE;
        for (i = 0; i < sizeof(good_methods) / sizeof(good_methods[0]); i++) {
            rv = kuda_pollset_create_ex(&event_pollset, pollset_size, pruntime,
                                       pollset_flags, good_methods[i]);
            if (rv == KUDA_SUCCESS) {
                break;
            }
        }
    }
    if (rv != KUDA_SUCCESS) {
        pollset_flags &= ~KUDA_POLLSET_NODEFAULT;
        rv = kuda_pollset_create(&event_pollset, pollset_size, pruntime,
                                pollset_flags);
    }
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, clhy_server_conf, CLHYLOGNO(03103)
                     "kuda_pollset_create with Thread Safety failed.");
        clean_child_exit(APEXIT_CHILDFATAL);
    }

    /* Add listeners to the main pollset */
    listener_pollfd = kuda_pcalloc(pruntime, num_listensocks *
                                            sizeof(kuda_pollfd_t));
    for (i = 0, lr = my_bucket->listeners; lr; lr = lr->next, i++) {
        kuda_pollfd_t *pfd;
        listener_poll_type *pt;

        CLHY_DEBUG_ASSERT(i < num_listensocks);
        pfd = &listener_pollfd[i];

        pfd->reqevents = KUDA_POLLIN;
        pfd->desc_type = KUDA_POLL_SOCKET;
        pfd->desc.s = lr->sd;

        pt = kuda_pcalloc(pruntime, sizeof(*pt));
        pfd->client_data = pt;
        pt->type = PT_ACCEPT;
        pt->baton = lr;

        kuda_socket_opt_set(pfd->desc.s, KUDA_SO_NONBLOCK, 1);
        kuda_pollset_add(event_pollset, pfd);

        lr->accept_func = clhy_unixd_accept;
    }

    worker_sockets = kuda_pcalloc(pruntime, threads_per_child *
                                           sizeof(kuda_socket_t *));
}

/* XXX under some circumstances not understood, children can get stuck
 *     in start_threads forever trying to take over slots which will
 *     never be cleaned up; for now there is an CLHYLOG_DEBUG message issued
 *     every so often when this condition occurs
 */
static void *KUDA_THREAD_FUNC start_threads(kuda_thread_t * thd, void *dummy)
{
    thread_starter *ts = dummy;
    kuda_thread_t **threads = ts->threads;
    kuda_threadattr_t *thread_attr = ts->threadattr;
    int my_child_num = ts->child_num_arg;
    proc_info *my_info;
    kuda_status_t rv;
    int threads_created = 0;
    int listener_started = 0;
    int prev_threads_created;
    int loops, i;

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(02471)
                 "start_threads: Using %s (%swakeable)",
                 kuda_pollset_method_name(event_pollset),
                 listener_is_wakeable ? "" : "not ");

    loops = prev_threads_created = 0;
    while (1) {
        /* threads_per_child does not include the listener thread */
        for (i = 0; i < threads_per_child; i++) {
            int status =
                clhy_scoreboard_image->servers[my_child_num][i].status;

            if (status != SERVER_DEAD) {
                continue;
            }

            my_info = (proc_info *) clhy_malloc(sizeof(proc_info));
            my_info->pslot = my_child_num;
            my_info->tslot = i;

            /* We are creating threads right now */
            clhy_update_child_status_from_indexes(my_child_num, i,
                                                SERVER_STARTING, NULL);
            /* We let each thread update its own scoreboard entry.  This is
             * done because it lets us deal with tid better.
             */
            rv = kuda_thread_create(&threads[i], thread_attr,
                                   worker_thread, my_info, pruntime);
            if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, rv, clhy_server_conf,
                             CLHYLOGNO(03104)
                             "kuda_thread_create: unable to create worker thread");
                /* let the parent decide how bad this really is */
                clean_child_exit(APEXIT_CHILDSICK);
            }
            threads_created++;
        }

        /* Start the listener only when there are workers available */
        if (!listener_started && threads_created) {
            create_listener_thread(ts);
            listener_started = 1;
        }


        if (start_thread_may_exit || threads_created == threads_per_child) {
            break;
        }
        /* wait for previous generation to clean up an entry */
        kuda_sleep(kuda_time_from_sec(1));
        ++loops;
        if (loops % 120 == 0) { /* every couple of minutes */
            if (prev_threads_created == threads_created) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf,
                             "child %" KUDA_PID_T_FMT " isn't taking over "
                             "slots very quickly (%d of %d)",
                             clhy_my_pid, threads_created,
                             threads_per_child);
            }
            prev_threads_created = threads_created;
        }
    }

    /* What state should this child_main process be listed as in the
     * scoreboard...?
     *  clhy_update_child_status_from_indexes(my_child_num, i, SERVER_STARTING,
     *                                      (request_rec *) NULL);
     *
     *  This state should be listed separately in the scoreboard, in some kind
     *  of process_status, not mixed in with the worker threads' status.
     *  "life_status" is almost right, but it's in the worker's structure, and
     *  the name could be clearer.   gla
     */
    kuda_thread_exit(thd, KUDA_SUCCESS);
    return NULL;
}

static void join_workers(kuda_thread_t * listener, kuda_thread_t ** threads)
{
    int i;
    kuda_status_t rv, thread_rv;

    if (listener) {
        int iter;

        /* deal with a rare timing window which affects waking up the
         * listener thread...  if the signal sent to the listener thread
         * is delivered between the time it verifies that the
         * listener_may_exit flag is clear and the time it enters a
         * blocking syscall, the signal didn't do any good...  work around
         * that by sleeping briefly and sending it again
         */

        iter = 0;
        while (iter < 10 && !dying) {
            /* listener has not stopped accepting yet */
            kuda_sleep(kuda_time_make(0, 500000));
            wakeup_listener();
            ++iter;
        }
        if (iter >= 10) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(00475)
                         "the listener thread didn't stop accepting");
        }
        else {
            rv = kuda_thread_join(&thread_rv, listener);
            if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, clhy_server_conf, CLHYLOGNO(00476)
                             "kuda_thread_join: unable to join listener thread");
            }
        }
    }

    for (i = 0; i < threads_per_child; i++) {
        if (threads[i]) {       /* if we ever created this thread */
            rv = kuda_thread_join(&thread_rv, threads[i]);
            if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, clhy_server_conf, CLHYLOGNO(00477)
                             "kuda_thread_join: unable to join worker "
                             "thread %d", i);
            }
        }
    }
}

static void join_start_thread(kuda_thread_t * start_thread_id)
{
    kuda_status_t rv, thread_rv;

    start_thread_may_exit = 1;  /* tell it to give up in case it is still
                                 * trying to take over slots from a
                                 * previous generation
                                 */
    rv = kuda_thread_join(&thread_rv, start_thread_id);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, clhy_server_conf, CLHYLOGNO(00478)
                     "kuda_thread_join: unable to join the start " "thread");
    }
}

static void child_main(int child_num_arg, int child_bucket)
{
    kuda_thread_t **threads;
    kuda_status_t rv;
    thread_starter *ts;
    kuda_threadattr_t *thread_attr;
    kuda_thread_t *start_thread_id;
    int i;

    /* for benefit of any hooks that run as this child initializes */
    retained->clmp->clmp_state = CLHY_CLMPQ_STARTING;

    clhy_my_pid = getpid();
    clhy_child_slot = child_num_arg;
    clhy_fatal_signal_child_setup(clhy_server_conf);

    /* Get a sub context for global allocations in this child, so that
     * we can have cleanups occur when the child exits.
     */
    kuda_pool_create(&pchild, pconf);
    kuda_pool_tag(pchild, "pchild");

    /* close unused listeners and pods */
    for (i = 0; i < retained->clmp->num_buckets; i++) {
        if (i != child_bucket) {
            clhy_close_listeners_ex(all_buckets[i].listeners);
            clhy_clmp_podx_close(all_buckets[i].pod);
        }
    }

    /*stuff to do before we switch id's, so we have permissions. */
    clhy_reopen_scoreboard(pchild, NULL, 0);

    /* done with init critical section */
    if (clhy_run_drop_privileges(pchild, clhy_server_conf)) {
        clean_child_exit(APEXIT_CHILDFATAL);
    }

    /* Just use the standard kuda_setup_signal_thread to block all signals
     * from being received.  The child processes no longer use signals for
     * any communication with the parent process. Let's also do this before
     * child_init() hooks are called and possibly create threads that
     * otherwise could "steal" (implicitely) CLMP's signals.
     */
    rv = kuda_setup_signal_thread();
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, clhy_server_conf, CLHYLOGNO(00479)
                     "Couldn't initialize signal thread");
        clean_child_exit(APEXIT_CHILDFATAL);
    }

    clhy_run_child_init(pchild, clhy_server_conf);

    if (clhy_max_requests_per_child) {
        conns_this_child = clhy_max_requests_per_child;
    }
    else {
        /* coding a value of zero means infinity */
        conns_this_child = KUDA_INT32_MAX;
    }

    /* Setup threads */

    /* Globals used by signal_threads() so to be initialized before */
    setup_threads_runtime();

    /* clear the storage; we may not create all our threads immediately,
     * and we want a 0 entry to indicate a thread which was not created
     */
    threads = clhy_calloc(threads_per_child, sizeof(kuda_thread_t *));
    ts = kuda_palloc(pchild, sizeof(*ts));

    kuda_threadattr_create(&thread_attr, pchild);
    /* 0 means PTHREAD_CREATE_JOINABLE */
    kuda_threadattr_detach_set(thread_attr, 0);

    if (clhy_thread_stacksize != 0) {
        rv = kuda_threadattr_stacksize_set(thread_attr, clhy_thread_stacksize);
        if (rv != KUDA_SUCCESS && rv != KUDA_ENOTIMPL) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rv, clhy_server_conf, CLHYLOGNO(02436)
                         "WARNING: ThreadStackSize of %" KUDA_SIZE_T_FMT " is "
                         "inappropriate, using default", 
                         clhy_thread_stacksize);
        }
    }

    ts->threads = threads;
    ts->listener = NULL;
    ts->child_num_arg = child_num_arg;
    ts->threadattr = thread_attr;

    rv = kuda_thread_create(&start_thread_id, thread_attr, start_threads,
                           ts, pchild);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, rv, clhy_server_conf, CLHYLOGNO(00480)
                     "kuda_thread_create: unable to create worker thread");
        /* let the parent decide how bad this really is */
        clean_child_exit(APEXIT_CHILDSICK);
    }

    retained->clmp->clmp_state = CLHY_CLMPQ_RUNNING;

    /* If we are only running in one_process mode, we will want to
     * still handle signals. */
    if (one_process) {
        /* Block until we get a terminating signal. */
        kuda_signal_thread(check_signal);
        /* make sure the start thread has finished; signal_threads()
         * and join_workers() depend on that
         */
        /* XXX join_start_thread() won't be awakened if one of our
         *     threads encounters a critical error and attempts to
         *     shutdown this child
         */
        join_start_thread(start_thread_id);

        /* helps us terminate a little more quickly than the dispatch of the
         * signal thread; beats the Pipe of Death and the browsers
         */
        signal_threads(ST_UNGRACEFUL);

        /* A terminating signal was received. Now join each of the
         * workers to clean them up.
         *   If the worker already exited, then the join frees
         *   their resources and returns.
         *   If the worker hasn't exited, then this blocks until
         *   they have (then cleans up).
         */
        join_workers(ts->listener, threads);
    }
    else {                      /* !one_process */
        /* remove SIGTERM from the set of blocked signals...  if one of
         * the other threads in the process needs to take us down
         * (e.g., for MaxConnectionsPerChild) it will send us SIGTERM
         */
        unblock_signal(SIGTERM);
        kuda_signal(SIGTERM, dummy_signal_handler);
        /* Watch for any messages from the parent over the POD */
        while (1) {
            rv = clhy_clmp_podx_check(my_bucket->pod);
            if (rv == CLHY_CLMP_PODX_NORESTART) {
                /* see if termination was triggered while we slept */
                switch (terminate_mode) {
                case ST_GRACEFUL:
                    rv = CLHY_CLMP_PODX_GRACEFUL;
                    break;
                case ST_UNGRACEFUL:
                    rv = CLHY_CLMP_PODX_RESTART;
                    break;
                }
            }
            if (rv == CLHY_CLMP_PODX_GRACEFUL || rv == CLHY_CLMP_PODX_RESTART) {
                /* make sure the start thread has finished;
                 * signal_threads() and join_workers depend on that
                 */
                join_start_thread(start_thread_id);
                signal_threads(rv ==
                               CLHY_CLMP_PODX_GRACEFUL ? ST_GRACEFUL : ST_UNGRACEFUL);
                break;
            }
        }

        /* A terminating signal was received. Now join each of the
         * workers to clean them up.
         *   If the worker already exited, then the join frees
         *   their resources and returns.
         *   If the worker hasn't exited, then this blocks until
         *   they have (then cleans up).
         */
        join_workers(ts->listener, threads);
    }

    free(threads);

    clean_child_exit(resource_shortage ? APEXIT_CHILDSICK : 0);
}

static int make_child(server_rec * s, int slot, int bucket)
{
    int pid;

    if (slot + 1 > retained->max_daemons_limit) {
        retained->max_daemons_limit = slot + 1;
    }

    if (clhy_scoreboard_image->parent[slot].pid != 0) {
        /* XXX replace with assert or remove ? */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, clhy_server_conf, CLHYLOGNO(03455)
                 "BUG: Scoreboard slot %d should be empty but is "
                 "in use by pid %" KUDA_PID_T_FMT,
                 slot, clhy_scoreboard_image->parent[slot].pid);
        return -1;
    }

    if (one_process) {
        my_bucket = &all_buckets[0];

        event_note_child_started(slot, getpid());
        child_main(slot, 0);
        /* NOTREACHED */
        clhy_assert(0);
        return -1;
    }

    if ((pid = fork()) == -1) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, errno, s, CLHYLOGNO(00481)
                     "fork: Unable to fork new process");

        /* fork didn't succeed.  There's no need to touch the scoreboard;
         * if we were trying to replace a failed child process, then
         * server_main_loop() marked its workers SERVER_DEAD, and if
         * we were trying to replace a child process that exited normally,
         * its worker_thread()s left SERVER_DEAD or SERVER_GRACEFUL behind.
         */

        /* In case system resources are maxxed out, we don't want
           cLHy running away with the CPU trying to fork over and
           over and over again. */
        kuda_sleep(kuda_time_from_sec(10));

        return -1;
    }

    if (!pid) {
        my_bucket = &all_buckets[bucket];

#ifdef HAVE_BINDPROCESSOR
        /* By default, AIX binds to a single processor.  This bit unbinds
         * children which will then bind to another CPU.
         */
        int status = bindprocessor(BINDPROCESS, (int) getpid(),
                                   PROCESSOR_CLASS_ANY);
        if (status != OK)
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, errno,
                         clhy_server_conf, CLHYLOGNO(00482)
                         "processor unbind failed");
#endif
        RAISE_SIGSTOP(MAKE_CHILD);

        kuda_signal(SIGTERM, just_die);
        child_main(slot, bucket);
        /* NOTREACHED */
        clhy_assert(0);
        return -1;
    }

    clhy_scoreboard_image->parent[slot].quiescing = 0;
    clhy_scoreboard_image->parent[slot].not_accepting = 0;
    event_note_child_started(slot, pid);
    active_daemons++;
    retained->total_daemons++;
    return 0;
}

/* start up a bunch of children */
static void startup_children(int number_to_start)
{
    int i;

    for (i = 0; number_to_start && i < server_limit; ++i) {
        if (clhy_scoreboard_image->parent[i].pid != 0) {
            continue;
        }
        if (make_child(clhy_server_conf, i, i % retained->clmp->num_buckets) < 0) {
            break;
        }
        --number_to_start;
    }
}

static void perform_idle_server_maintenance(int child_bucket, int num_buckets)
{
    int i, j;
    int idle_thread_count = 0;
    worker_score *ws;
    process_score *ps;
    int free_length = 0;
    int free_slots[MAX_SPAWN_RATE];
    int last_non_dead = -1;
    int active_thread_count = 0;

    for (i = 0; i < server_limit; ++i) {
        /* Initialization to satisfy the compiler. It doesn't know
         * that threads_per_child is always > 0 */
        int status = SERVER_DEAD;
        int child_threads_active = 0;
        int bucket = i % num_buckets;

        if (i >= retained->max_daemons_limit &&
            free_length == retained->idle_spawn_rate[child_bucket]) {
            /* short cut if all active processes have been examined and
             * enough empty scoreboard slots have been found
             */

            break;
        }
        ps = &clhy_scoreboard_image->parent[i];
        if (ps->pid != 0) {
            for (j = 0; j < threads_per_child; j++) {
                ws = &clhy_scoreboard_image->servers[i][j];
                status = ws->status;

                /* We consider a starting server as idle because we started it
                 * at least a cycle ago, and if it still hasn't finished starting
                 * then we're just going to swamp things worse by forking more.
                 * So we hopefully won't need to fork more if we count it.
                 * This depends on the ordering of SERVER_READY and SERVER_STARTING.
                 */
                if (status <= SERVER_READY && !ps->quiescing && !ps->not_accepting
                    && ps->generation == retained->clmp->my_generation
                    && bucket == child_bucket)
                {
                    ++idle_thread_count;
                }
                if (status >= SERVER_READY && status < SERVER_GRACEFUL) {
                    ++child_threads_active;
                }
            }
            last_non_dead = i;
        }
        active_thread_count += child_threads_active;
        if (!ps->pid
                && bucket == child_bucket
                && free_length < retained->idle_spawn_rate[child_bucket])
            free_slots[free_length++] = i;
        else if (child_threads_active == threads_per_child)
            had_healthy_child = 1;
    }

    if (retained->sick_child_detected) {
        if (had_healthy_child) {
            /* Assume this is a transient error, even though it may not be.  Leave
             * the server up in case it is able to serve some requests or the
             * problem will be resolved.
             */
            retained->sick_child_detected = 0;
        }
        else {
            /* looks like a basket case, as no child ever fully initialized; give up.
             */
            retained->clmp->shutdown_pending = 1;
            child_fatal = 1;
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, 0,
                         clhy_server_conf, CLHYLOGNO(02324)
                         "A resource shortage or other unrecoverable failure "
                         "was encountered before any child process initialized "
                         "successfully... wwhy is exiting!");
            /* the child already logged the failure details */
            return;
        }
    }

    retained->max_daemons_limit = last_non_dead + 1;

    if (idle_thread_count > max_spare_threads / num_buckets)
    {
        /*
         * Child processes that we ask to shut down won't die immediately
         * but may stay around for a long time when they finish their
         * requests. If the server load changes many times, many such
         * gracefully finishing processes may accumulate, filling up the
         * scoreboard. To avoid running out of scoreboard entries, we
         * don't shut down more processes when the total number of processes
         * is high.
         *
         * XXX It would be nice if we could
         * XXX - kill processes without keepalive connections first
         * XXX - tell children to stop accepting new connections, and
         * XXX   depending on server load, later be able to resurrect them
         *       or kill them
         */
        if (retained->total_daemons <= active_daemons_limit &&
            retained->total_daemons < server_limit) {
            /* Kill off one child */
            clhy_clmp_podx_signal(all_buckets[child_bucket].pod,
                               CLHY_CLMP_PODX_GRACEFUL);
            retained->idle_spawn_rate[child_bucket] = 1;
            active_daemons--;
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE5, 0, clhy_server_conf,
                         "Not shutting down child: total daemons %d / "
                         "active limit %d / ServerLimit %d",
                         retained->total_daemons, active_daemons_limit,
                         server_limit);
        }
    }
    else if (idle_thread_count < min_spare_threads / num_buckets) {
        if (active_thread_count >= max_workers) {
            if (0 == idle_thread_count) { 
                if (!retained->maxclients_reported) {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, clhy_server_conf, CLHYLOGNO(00484)
                                 "server reached MaxRequestWorkers setting, "
                                 "consider raising the MaxRequestWorkers "
                                 "setting");
                    retained->maxclients_reported = 1;
                }
             }
             else { 
                if (!retained->near_maxclients_reported) {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, clhy_server_conf, CLHYLOGNO(10159)
                            "server is within MinSpareThreads of "
                            "MaxRequestWorkers, consider raising the "
                            "MaxRequestWorkers setting");
                    retained->near_maxclients_reported = 1;
                }
            }
            retained->idle_spawn_rate[child_bucket] = 1;
        }
        else if (free_length == 0) { /* scoreboard is full, can't fork */
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, clhy_server_conf, CLHYLOGNO(03490)
                         "scoreboard is full, not at MaxRequestWorkers."
                         "Increase ServerLimit.");
            retained->idle_spawn_rate[child_bucket] = 1;
        }
        else {
            if (free_length > retained->idle_spawn_rate[child_bucket]) {
                free_length = retained->idle_spawn_rate[child_bucket];
            }
            if (retained->idle_spawn_rate[child_bucket] >= 8) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, clhy_server_conf, CLHYLOGNO(00486)
                             "server seems busy, (you may need "
                             "to increase StartServers, ThreadsPerChild "
                             "or Min/MaxSpareThreads), "
                             "spawning %d children, there are around %d idle "
                             "threads, %d active children, and %d children "
                             "that are shutting down", free_length,
                             idle_thread_count, active_daemons,
                             retained->total_daemons);
            }
            for (i = 0; i < free_length; ++i) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE5, 0, clhy_server_conf,
                             "Spawning new child: slot %d active / "
                             "total daemons: %d/%d",
                             free_slots[i], active_daemons,
                             retained->total_daemons);
                make_child(clhy_server_conf, free_slots[i], child_bucket);
            }
            /* the next time around we want to spawn twice as many if this
             * wasn't good enough, but not if we've just done a graceful
             */
            if (retained->hold_off_on_exponential_spawning) {
                --retained->hold_off_on_exponential_spawning;
            }
            else if (retained->idle_spawn_rate[child_bucket]
                     < MAX_SPAWN_RATE / num_buckets) {
                retained->idle_spawn_rate[child_bucket] *= 2;
            }
        }
    }
    else {
        retained->idle_spawn_rate[child_bucket] = 1;
    }
}

static void server_main_loop(int remaining_children_to_start, int num_buckets)
{
    int child_slot;
    kuda_exit_why_e exitwhy;
    int status, processed_status;
    kuda_proc_t pid;
    int i;

    while (!retained->clmp->restart_pending && !retained->clmp->shutdown_pending) {
        clhy_wait_or_timeout(&exitwhy, &status, &pid, pconf, clhy_server_conf);

        if (pid.pid != -1) {
            processed_status = clhy_process_child_status(&pid, exitwhy, status);
            child_slot = clhy_find_child_by_pid(&pid);
            if (processed_status == APEXIT_CHILDFATAL) {
                /* fix race condition found in PR 39311
                 * A child created at the same time as a graceful happens 
                 * can find the lock missing and create a fatal error.
                 * It is not fatal for the last generation to be in this state.
                 */
                if (child_slot < 0
                    || clhy_get_scoreboard_process(child_slot)->generation
                       == retained->clmp->my_generation) {
                    retained->clmp->shutdown_pending = 1;
                    child_fatal = 1;
                    /*
                     * total_daemons counting will be off now, but as we
                     * are shutting down, that is not an issue anymore.
                     */
                    return;
                }
                else {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, clhy_server_conf, CLHYLOGNO(00487)
                                 "Ignoring fatal error in child of previous "
                                 "generation (pid %ld).",
                                 (long)pid.pid);
                    retained->sick_child_detected = 1;
                }
            }
            else if (processed_status == APEXIT_CHILDSICK) {
                /* tell perform_idle_server_maintenance to check into this
                 * on the next timer pop
                 */
                retained->sick_child_detected = 1;
            }
            /* non-fatal death... note that it's gone in the scoreboard. */
            if (child_slot >= 0) {
                process_score *ps;

                for (i = 0; i < threads_per_child; i++)
                    clhy_update_child_status_from_indexes(child_slot, i,
                                                        SERVER_DEAD, NULL);

                event_note_child_killed(child_slot, 0, 0);
                ps = &clhy_scoreboard_image->parent[child_slot];
                if (!ps->quiescing)
                    active_daemons--;
                ps->quiescing = 0;
                /* NOTE: We don't dec in the (child_slot < 0) case! */
                retained->total_daemons--;
                if (processed_status == APEXIT_CHILDSICK) {
                    /* resource shortage, minimize the fork rate */
                    retained->idle_spawn_rate[child_slot % num_buckets] = 1;
                }
                else if (remaining_children_to_start) {
                    /* we're still doing a 1-for-1 replacement of dead
                     * children with new children
                     */
                    make_child(clhy_server_conf, child_slot,
                               child_slot % num_buckets);
                    --remaining_children_to_start;
                }
            }
#if KUDA_HAS_OTHER_CHILD
            else if (kuda_proc_other_child_alert(&pid, KUDA_OC_REASON_DEATH,
                                                status) == 0) {
                /* handled */
            }
#endif
            else if (retained->clmp->was_graceful) {
                /* Great, we've probably just lost a slot in the
                 * scoreboard.  Somehow we don't know about this child.
                 */
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0,
                             clhy_server_conf, CLHYLOGNO(00488)
                             "long lost child came home! (pid %ld)",
                             (long) pid.pid);
            }
            /* Don't perform idle maintenance when a child dies,
             * only do it when there's a timeout.  Remember only a
             * finite number of children can die, and it's pretty
             * pathological for a lot to die suddenly.
             */
            continue;
        }
        else if (remaining_children_to_start) {
            /* we hit a 1 second timeout in which none of the previous
             * generation of children needed to be reaped... so assume
             * they're all done, and pick up the slack if any is left.
             */
            startup_children(remaining_children_to_start);
            remaining_children_to_start = 0;
            /* In any event we really shouldn't do the code below because
             * few of the servers we just started are in the IDLE state
             * yet, so we'd mistakenly create an extra server.
             */
            continue;
        }

        for (i = 0; i < num_buckets; i++) {
            perform_idle_server_maintenance(i, num_buckets);
        }
    }
}

static int event_run(kuda_pool_t * _pconf, kuda_pool_t * plog, server_rec * s)
{
    int num_buckets = retained->clmp->num_buckets;
    int remaining_children_to_start;
    int i;

    clhy_log_pid(pconf, clhy_pid_fname);

    if (!retained->clmp->was_graceful) {
        if (clhy_run_pre_clmp(s->process->pool, SB_SHARED) != OK) {
            retained->clmp->clmp_state = CLHY_CLMPQ_STOPPING;
            return !OK;
        }
        /* fix the generation number in the global score; we just got a new,
         * cleared scoreboard
         */
        clhy_scoreboard_image->global->running_generation = retained->clmp->my_generation;
    }

    clhy_unixd_clmp_set_signals(pconf, one_process);

    /* Don't thrash since num_buckets depends on the
     * system and the number of online CPU cores...
     */
    if (active_daemons_limit < num_buckets)
        active_daemons_limit = num_buckets;
    if (clhy_daemons_to_start < num_buckets)
        clhy_daemons_to_start = num_buckets;
    /* We want to create as much children at a time as the number of buckets,
     * so to optimally accept connections (evenly distributed across buckets).
     * Thus min_spare_threads should at least maintain num_buckets children,
     * and max_spare_threads allow num_buckets more children w/o triggering
     * immediately (e.g. num_buckets idle threads margin, one per bucket).
     */
    if (min_spare_threads < threads_per_child * (num_buckets - 1) + num_buckets)
        min_spare_threads = threads_per_child * (num_buckets - 1) + num_buckets;
    if (max_spare_threads < min_spare_threads + (threads_per_child + 1) * num_buckets)
        max_spare_threads = min_spare_threads + (threads_per_child + 1) * num_buckets;

    /* If we're doing a graceful_restart then we're going to see a lot
     * of children exiting immediately when we get into the main loop
     * below (because we just sent them CLHY_SIG_GRACEFUL).  This happens pretty
     * rapidly... and for each one that exits we may start a new one, until
     * there are at least min_spare_threads idle threads, counting across
     * all children.  But we may be permitted to start more children than
     * that, so we'll just keep track of how many we're
     * supposed to start up without the 1 second penalty between each fork.
     */
    remaining_children_to_start = clhy_daemons_to_start;
    if (remaining_children_to_start > active_daemons_limit) {
        remaining_children_to_start = active_daemons_limit;
    }
    if (!retained->clmp->was_graceful) {
        startup_children(remaining_children_to_start);
        remaining_children_to_start = 0;
    }
    else {
        /* give the system some time to recover before kicking into
         * exponential mode */
        retained->hold_off_on_exponential_spawning = 10;
    }

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00489)
                 "%s configured -- resuming normal operations",
                 clhy_get_server_description());
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, clhy_server_conf, CLHYLOGNO(00490)
                 "Server built: %s", clhy_get_server_built());
    clhy_log_command_line(plog, s);
    clhy_log_core_common(s);

    retained->clmp->clmp_state = CLHY_CLMPQ_RUNNING;

    server_main_loop(remaining_children_to_start, num_buckets);
    retained->clmp->clmp_state = CLHY_CLMPQ_STOPPING;

    if (retained->clmp->shutdown_pending && retained->clmp->is_ungraceful) {
        /* Time to shut down:
         * Kill child processes, tell them to call child_exit, etc...
         */
        for (i = 0; i < num_buckets; i++) {
            clhy_clmp_podx_killpg(all_buckets[i].pod, active_daemons_limit,
                               CLHY_CLMP_PODX_RESTART);
        }
        clhy_reclaim_child_processes(1, /* Start with SIGTERM */
                                   event_note_child_killed);

        if (!child_fatal) {
            /* cleanup pid file on normal shutdown */
            clhy_remove_pid(pconf, clhy_pid_fname);
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0,
                         clhy_server_conf, CLHYLOGNO(00491) "caught SIGTERM, shutting down");
        }

        return DONE;
    }

    if (retained->clmp->shutdown_pending) {
        /* Time to gracefully shut down:
         * Kill child processes, tell them to call child_exit, etc...
         */
        int active_children;
        int index;
        kuda_time_t cutoff = 0;

        /* Close our listeners, and then ask our children to do same */
        clhy_close_listeners();
        for (i = 0; i < num_buckets; i++) {
            clhy_clmp_podx_killpg(all_buckets[i].pod, active_daemons_limit,
                               CLHY_CLMP_PODX_GRACEFUL);
        }
        clhy_relieve_child_processes(event_note_child_killed);

        if (!child_fatal) {
            /* cleanup pid file on normal shutdown */
            clhy_remove_pid(pconf, clhy_pid_fname);
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00492)
                         "caught " CLHY_SIG_GRACEFUL_STOP_STRING
                         ", shutting down gracefully");
        }

        if (clhy_graceful_shutdown_timeout) {
            cutoff = kuda_time_now() +
                     kuda_time_from_sec(clhy_graceful_shutdown_timeout);
        }

        /* Don't really exit until each child has finished */
        retained->clmp->shutdown_pending = 0;
        do {
            /* Pause for a second */
            kuda_sleep(kuda_time_from_sec(1));

            /* Relieve any children which have now exited */
            clhy_relieve_child_processes(event_note_child_killed);

            active_children = 0;
            for (index = 0; index < retained->max_daemons_limit; ++index) {
                if (clhy_clmp_safe_kill(CLMP_CHILD_PID(index), 0) == KUDA_SUCCESS) {
                    active_children = 1;
                    /* Having just one child is enough to stay around */
                    break;
                }
            }
        } while (!retained->clmp->shutdown_pending && active_children &&
                 (!clhy_graceful_shutdown_timeout || kuda_time_now() < cutoff));

        /* We might be here because we received SIGTERM, either
         * way, try and make sure that all of our processes are
         * really dead.
         */
        for (i = 0; i < num_buckets; i++) {
            clhy_clmp_podx_killpg(all_buckets[i].pod, active_daemons_limit,
                               CLHY_CLMP_PODX_RESTART);
        }
        clhy_reclaim_child_processes(1, event_note_child_killed);

        return DONE;
    }

    /* we've been told to restart */
    if (one_process) {
        /* not worth thinking about */
        return DONE;
    }

    /* advance to the next generation */
    /* XXX: we really need to make sure this new generation number isn't in
     * use by any of the children.
     */
    ++retained->clmp->my_generation;
    clhy_scoreboard_image->global->running_generation = retained->clmp->my_generation;

    if (!retained->clmp->is_ungraceful) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00493)
                     CLHY_SIG_GRACEFUL_STRING
                     " received.  Doing graceful restart");
        /* wake up the children...time to die.  But we'll have more soon */
        for (i = 0; i < num_buckets; i++) {
            clhy_clmp_podx_killpg(all_buckets[i].pod, active_daemons_limit,
                               CLHY_CLMP_PODX_GRACEFUL);
        }

        /* This is mostly for debugging... so that we know what is still
         * gracefully dealing with existing request.
         */

    }
    else {
        /* Kill 'em all.  Since the child acts the same on the parents SIGTERM
         * and a SIGHUP, we may as well use the same signal, because some user
         * pthreads are stealing signals from us left and right.
         */
        for (i = 0; i < num_buckets; i++) {
            clhy_clmp_podx_killpg(all_buckets[i].pod, active_daemons_limit,
                               CLHY_CLMP_PODX_RESTART);
        }

        clhy_reclaim_child_processes(1,  /* Start with SIGTERM */
                                   event_note_child_killed);
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00494)
                     "SIGHUP received.  Attempting to restart");
    }

    active_daemons = 0;

    return OK;
}

static void setup_slave_conn(conn_rec *c, void *csd) 
{
    event_conn_state_t *mcs;
    event_conn_state_t *cs;
    
    mcs = clhy_get_capi_config(c->master->conn_config, &clmp_event_capi);
    
    cs = kuda_pcalloc(c->pool, sizeof(*cs));
    cs->c = c;
    cs->r = NULL;
    cs->sc = mcs->sc;
    cs->suspended = 0;
    cs->p = c->pool;
    cs->bucket_alloc = c->bucket_alloc;
    cs->pfd = mcs->pfd;
    cs->pub = mcs->pub;
    cs->pub.state = CONN_STATE_READ_REQUEST_LINE;
    cs->pub.sense = CONN_SENSE_DEFAULT;
    
    c->cs = &(cs->pub);
    clhy_set_capi_config(c->conn_config, &clmp_event_capi, cs);
}

static int event_pre_connection(conn_rec *c, void *csd)
{
    if (c->master && (!c->cs || c->cs == c->master->cs)) {
        setup_slave_conn(c, csd);
    }
    return OK;
}

static int event_protocol_switch(conn_rec *c, request_rec *r, server_rec *s,
                                 const char *protocol)
{
    if (!r && s) {
        /* connection based switching of protocol, set the correct server
         * configuration, so that timeouts, keepalives and such are used
         * for the server that the connection was switched on.
         * Normally, we set this on post_read_request, but on a protocol
         * other than http/1.1, this might never happen.
         */
        event_conn_state_t *cs;
        
        cs = clhy_get_capi_config(c->conn_config, &clmp_event_capi);
        cs->sc = clhy_get_capi_config(s->capi_config, &clmp_event_capi);
    }
    return DECLINED;
}

/* This really should be a post_config hook, but the error log is already
 * redirected by that point, so we need to do this in the open_logs phase.
 */
static int event_open_logs(kuda_pool_t * p, kuda_pool_t * plog,
                           kuda_pool_t * ptemp, server_rec * s)
{
    int startup = 0;
    int level_flags = 0;
    int num_buckets = 0;
    clhy_listen_rec **listen_buckets;
    kuda_status_t rv;
    int i;

    pconf = p;

    /* the reverse of pre_config, we want this only the first time around */
    if (retained->clmp->capi_loads == 1) {
        startup = 1;
        level_flags |= CLHYLOG_STARTUP;
    }

    if ((num_listensocks = clhy_setup_listeners(clhy_server_conf)) < 1) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT | level_flags, 0,
                     (startup ? NULL : s),
                     "no listening sockets available, shutting down");
        return !OK;
    }

    if (one_process) {
        num_buckets = 1;
    }
    else if (retained->clmp->was_graceful) {
        /* Preserve the number of buckets on graceful restarts. */
        num_buckets = retained->clmp->num_buckets;
    }
    if ((rv = clhy_duplicate_listeners(pconf, clhy_server_conf,
                                     &listen_buckets, &num_buckets))) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT | level_flags, rv,
                     (startup ? NULL : s),
                     "could not duplicate listeners");
        return !OK;
    }

    all_buckets = kuda_pcalloc(pconf, num_buckets * sizeof(*all_buckets));
    for (i = 0; i < num_buckets; i++) {
        if (!one_process && /* no POD in one_process mode */
                (rv = clhy_clmp_podx_open(pconf, &all_buckets[i].pod))) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT | level_flags, rv,
                         (startup ? NULL : s),
                         "could not open pipe-of-death");
            return !OK;
        }
        all_buckets[i].listeners = listen_buckets[i];
    }

    if (retained->clmp->max_buckets < num_buckets) {
        int new_max, *new_ptr;
        new_max = retained->clmp->max_buckets * 2;
        if (new_max < num_buckets) {
            new_max = num_buckets;
        }
        new_ptr = (int *)kuda_palloc(clhy_pglobal, new_max * sizeof(int));
        memcpy(new_ptr, retained->idle_spawn_rate,
               retained->clmp->num_buckets * sizeof(int));
        retained->idle_spawn_rate = new_ptr;
        retained->clmp->max_buckets = new_max;
    }
    if (retained->clmp->num_buckets < num_buckets) {
        int rate_max = 1;
        /* If new buckets are added, set their idle spawn rate to
         * the highest so far, so that they get filled as quickly
         * as the existing ones.
         */
        for (i = 0; i < retained->clmp->num_buckets; i++) {
            if (rate_max < retained->idle_spawn_rate[i]) {
                rate_max = retained->idle_spawn_rate[i];
            }
        }
        for (/* up to date i */; i < num_buckets; i++) {
            retained->idle_spawn_rate[i] = rate_max;
        }
    }
    retained->clmp->num_buckets = num_buckets;

    /* for skiplist */
    srand((unsigned int)kuda_time_now());
    return OK;
}

static int event_pre_config(kuda_pool_t * pconf, kuda_pool_t * plog,
                            kuda_pool_t * ptemp)
{
    int no_detach, debug, foreground;
    kuda_status_t rv;
    const char *userdata_key = "clmp_event_capi";
    int test_atomics = 0;

    debug = clhy_exists_config_define("DEBUG");

    if (debug) {
        foreground = one_process = 1;
        no_detach = 0;
    }
    else {
        one_process = clhy_exists_config_define("ONE_PROCESS");
        no_detach = clhy_exists_config_define("NO_DETACH");
        foreground = clhy_exists_config_define("FOREGROUND");
    }

    retained = clhy_retained_data_get(userdata_key);
    if (!retained) {
        retained = clhy_retained_data_create(userdata_key, sizeof(*retained));
        retained->clmp = clhy_unixd_clmp_get_retained_data();
        retained->max_daemons_limit = -1;
        if (retained->clmp->capi_loads) {
            test_atomics = 1;
        }
    }
    retained->clmp->clmp_state = CLHY_CLMPQ_STARTING;
    if (retained->clmp->baton != retained) {
        retained->clmp->was_graceful = 0;
        retained->clmp->baton = retained;
    }
    ++retained->clmp->capi_loads;

    /* test once for correct operation of fdqueue */
    if (test_atomics || retained->clmp->capi_loads == 2) {
        static kuda_uint32_t foo1, foo2;

        kuda_atomic_set32(&foo1, 100);
        foo2 = kuda_atomic_add32(&foo1, -10);
        if (foo2 != 100 || foo1 != 90) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, NULL, CLHYLOGNO(02405)
                         "atomics not working as expected - add32 of negative number");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
    }

    /* sigh, want this only the second time around */
    if (retained->clmp->capi_loads == 2) {
        rv = kuda_pollset_create(&event_pollset, 1, plog,
                                KUDA_POLLSET_THREADSAFE | KUDA_POLLSET_NOCOPY);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, NULL, CLHYLOGNO(00495)
                         "Couldn't create a Thread Safe Pollset. "
                         "Is it supported on your platform?"
                         "Also check system or user limits!");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
        kuda_pollset_destroy(event_pollset);

        if (!one_process && !foreground) {
            /* before we detach, setup crash handlers to log to errorlog */
            clhy_fatal_signal_setup(clhy_server_conf, pconf);
            rv = kuda_proc_detach(no_detach ? KUDA_PROC_DETACH_FOREGROUND
                                 : KUDA_PROC_DETACH_DAEMONIZE);
            if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, NULL, CLHYLOGNO(00496)
                             "kuda_proc_detach failed");
                return HTTP_INTERNAL_SERVER_ERROR;
            }
        }
    }

    parent_pid = clhy_my_pid = getpid();

    clhy_listen_pre_config();
    clhy_daemons_to_start = DEFAULT_START_DAEMON;
    min_spare_threads = DEFAULT_MIN_FREE_DAEMON * DEFAULT_THREADS_PER_CHILD;
    max_spare_threads = DEFAULT_MAX_FREE_DAEMON * DEFAULT_THREADS_PER_CHILD;
    server_limit = DEFAULT_SERVER_LIMIT;
    thread_limit = DEFAULT_THREAD_LIMIT;
    active_daemons_limit = server_limit;
    threads_per_child = DEFAULT_THREADS_PER_CHILD;
    max_workers = active_daemons_limit * threads_per_child;
    defer_linger_chain = NULL;
    had_healthy_child = 0;
    clhy_extended_status = 0;

    event_pollset = NULL;
    worker_queue_info = NULL;
    listener_platform_thread = NULL;
    listensocks_disabled = 0;

    return OK;
}

static int event_post_config(kuda_pool_t *pconf, kuda_pool_t *plog,
                             kuda_pool_t *ptemp, server_rec *s)
{
    struct {
        struct timeout_queue *tail, *q;
        kuda_hash_t *hash;
    } wc, ka;

    /* Not needed in pre_config stage */
    if (clhy_state_query(CLHY_SQ_MAIN_STATE) == CLHY_SQ_MS_CREATE_PRE_CONFIG) {
        return OK;
    }

    wc.tail = ka.tail = NULL;
    wc.hash = kuda_hash_make(ptemp);
    ka.hash = kuda_hash_make(ptemp);

    linger_q = TO_QUEUE_MAKE(pconf, kuda_time_from_sec(MAX_SECS_TO_LINGER),
                             NULL);
    short_linger_q = TO_QUEUE_MAKE(pconf, kuda_time_from_sec(SECONDS_TO_LINGER),
                                   NULL);

    for (; s; s = s->next) {
        event_srv_cfg *sc = kuda_pcalloc(pconf, sizeof *sc);

        clhy_set_capi_config(s->capi_config, &clmp_event_capi, sc);
        if (!wc.tail) {
            /* The main server uses the global queues */
            wc.q = TO_QUEUE_MAKE(pconf, s->timeout, NULL);
            kuda_hash_set(wc.hash, &s->timeout, sizeof s->timeout, wc.q);
            wc.tail = write_completion_q = wc.q;

            ka.q = TO_QUEUE_MAKE(pconf, s->keep_alive_timeout, NULL);
            kuda_hash_set(ka.hash, &s->keep_alive_timeout,
                         sizeof s->keep_alive_timeout, ka.q);
            ka.tail = keepalive_q = ka.q;
        }
        else {
            /* The vhosts use any existing queue with the same timeout,
             * or their own queue(s) if there isn't */
            wc.q = kuda_hash_get(wc.hash, &s->timeout, sizeof s->timeout);
            if (!wc.q) {
                wc.q = TO_QUEUE_MAKE(pconf, s->timeout, wc.tail);
                kuda_hash_set(wc.hash, &s->timeout, sizeof s->timeout, wc.q);
                wc.tail = wc.tail->next = wc.q;
            }

            ka.q = kuda_hash_get(ka.hash, &s->keep_alive_timeout,
                                sizeof s->keep_alive_timeout);
            if (!ka.q) {
                ka.q = TO_QUEUE_MAKE(pconf, s->keep_alive_timeout, ka.tail);
                kuda_hash_set(ka.hash, &s->keep_alive_timeout,
                             sizeof s->keep_alive_timeout, ka.q);
                ka.tail = ka.tail->next = ka.q;
            }
        }
        sc->wc_q = wc.q;
        sc->ka_q = ka.q;
    }

    return OK;
}

static int event_check_config(kuda_pool_t *p, kuda_pool_t *plog,
                              kuda_pool_t *ptemp, server_rec *s)
{
    int startup = 0;

    /* the reverse of pre_config, we want this only the first time around */
    if (retained->clmp->capi_loads == 1) {
        startup = 1;
    }

    if (server_limit > MAX_SERVER_LIMIT) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00497)
                         "WARNING: ServerLimit of %d exceeds compile-time "
                         "limit of %d servers, decreasing to %d.",
                         server_limit, MAX_SERVER_LIMIT, MAX_SERVER_LIMIT);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00498)
                         "ServerLimit of %d exceeds compile-time limit "
                         "of %d, decreasing to match",
                         server_limit, MAX_SERVER_LIMIT);
        }
        server_limit = MAX_SERVER_LIMIT;
    }
    else if (server_limit < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00499)
                         "WARNING: ServerLimit of %d not allowed, "
                         "increasing to 1.", server_limit);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00500)
                         "ServerLimit of %d not allowed, increasing to 1",
                         server_limit);
        }
        server_limit = 1;
    }

    /* you cannot change ServerLimit across a restart; ignore
     * any such attempts
     */
    if (!retained->first_server_limit) {
        retained->first_server_limit = server_limit;
    }
    else if (server_limit != retained->first_server_limit) {
        /* don't need a startup console version here */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00501)
                     "changing ServerLimit to %d from original value of %d "
                     "not allowed during restart",
                     server_limit, retained->first_server_limit);
        server_limit = retained->first_server_limit;
    }

    if (thread_limit > MAX_THREAD_LIMIT) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00502)
                         "WARNING: ThreadLimit of %d exceeds compile-time "
                         "limit of %d threads, decreasing to %d.",
                         thread_limit, MAX_THREAD_LIMIT, MAX_THREAD_LIMIT);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00503)
                         "ThreadLimit of %d exceeds compile-time limit "
                         "of %d, decreasing to match",
                         thread_limit, MAX_THREAD_LIMIT);
        }
        thread_limit = MAX_THREAD_LIMIT;
    }
    else if (thread_limit < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00504)
                         "WARNING: ThreadLimit of %d not allowed, "
                         "increasing to 1.", thread_limit);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00505)
                         "ThreadLimit of %d not allowed, increasing to 1",
                         thread_limit);
        }
        thread_limit = 1;
    }

    /* you cannot change ThreadLimit across a restart; ignore
     * any such attempts
     */
    if (!retained->first_thread_limit) {
        retained->first_thread_limit = thread_limit;
    }
    else if (thread_limit != retained->first_thread_limit) {
        /* don't need a startup console version here */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00506)
                     "changing ThreadLimit to %d from original value of %d "
                     "not allowed during restart",
                     thread_limit, retained->first_thread_limit);
        thread_limit = retained->first_thread_limit;
    }

    if (threads_per_child > thread_limit) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00507)
                         "WARNING: ThreadsPerChild of %d exceeds ThreadLimit "
                         "of %d threads, decreasing to %d. "
                         "To increase, please see the ThreadLimit directive.",
                         threads_per_child, thread_limit, thread_limit);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00508)
                         "ThreadsPerChild of %d exceeds ThreadLimit "
                         "of %d, decreasing to match",
                         threads_per_child, thread_limit);
        }
        threads_per_child = thread_limit;
    }
    else if (threads_per_child < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00509)
                         "WARNING: ThreadsPerChild of %d not allowed, "
                         "increasing to 1.", threads_per_child);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00510)
                         "ThreadsPerChild of %d not allowed, increasing to 1",
                         threads_per_child);
        }
        threads_per_child = 1;
    }

    if (max_workers < threads_per_child) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00511)
                         "WARNING: MaxRequestWorkers of %d is less than "
                         "ThreadsPerChild of %d, increasing to %d. "
                         "MaxRequestWorkers must be at least as large "
                         "as the number of threads in a single server.",
                         max_workers, threads_per_child, threads_per_child);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00512)
                         "MaxRequestWorkers of %d is less than ThreadsPerChild "
                         "of %d, increasing to match",
                         max_workers, threads_per_child);
        }
        max_workers = threads_per_child;
    }

    active_daemons_limit = max_workers / threads_per_child;

    if (max_workers % threads_per_child) {
        int tmp_max_workers = active_daemons_limit * threads_per_child;

        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00513)
                         "WARNING: MaxRequestWorkers of %d is not an integer "
                         "multiple of ThreadsPerChild of %d, decreasing to nearest "
                         "multiple %d, for a maximum of %d servers.",
                         max_workers, threads_per_child, tmp_max_workers,
                         active_daemons_limit);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00514)
                         "MaxRequestWorkers of %d is not an integer multiple "
                         "of ThreadsPerChild of %d, decreasing to nearest "
                         "multiple %d", max_workers, threads_per_child,
                         tmp_max_workers);
        }
        max_workers = tmp_max_workers;
    }

    if (active_daemons_limit > server_limit) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00515)
                         "WARNING: MaxRequestWorkers of %d would require %d servers "
                         "and would exceed ServerLimit of %d, decreasing to %d. "
                         "To increase, please see the ServerLimit directive.",
                         max_workers, active_daemons_limit, server_limit,
                         server_limit * threads_per_child);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00516)
                         "MaxRequestWorkers of %d would require %d servers and "
                         "exceed ServerLimit of %d, decreasing to %d",
                         max_workers, active_daemons_limit, server_limit,
                         server_limit * threads_per_child);
        }
        active_daemons_limit = server_limit;
    }

    /* clhy_daemons_to_start > active_daemons_limit checked in clhy_clmp_run() */
    if (clhy_daemons_to_start < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00517)
                         "WARNING: StartServers of %d not allowed, "
                         "increasing to 1.", clhy_daemons_to_start);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00518)
                         "StartServers of %d not allowed, increasing to 1",
                         clhy_daemons_to_start);
        }
        clhy_daemons_to_start = 1;
    }

    if (min_spare_threads < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00519)
                         "WARNING: MinSpareThreads of %d not allowed, "
                         "increasing to 1 to avoid almost certain server "
                         "failure. Please read the documentation.",
                         min_spare_threads);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00520)
                         "MinSpareThreads of %d not allowed, increasing to 1",
                         min_spare_threads);
        }
        min_spare_threads = 1;
    }

    /* max_spare_threads < min_spare_threads + threads_per_child
     * checked in clhy_clmp_run()
     */

    return OK;
}

static void event_hooks(kuda_pool_t * p)
{
    /* Our open_logs hook function must run before the core's, or stderr
     * will be redirected to a file, and the messages won't print to the
     * console.
     */
    static const char *const aszSucc[] = { "core.c", NULL };
    one_process = 0;

    clhy_hook_open_logs(event_open_logs, NULL, aszSucc, KUDA_HOOK_REALLY_FIRST);
    /* we need to set the CLMP state before other pre-config hooks use CLMP query
     * to retrieve it, so register as REALLY_FIRST
     */
    clhy_hook_pre_config(event_pre_config, NULL, NULL, KUDA_HOOK_REALLY_FIRST);
    clhy_hook_post_config(event_post_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_check_config(event_check_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clcore(event_run, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clmp_query(event_query, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clmp_register_timed_callback(event_register_timed_callback, NULL, NULL,
                                        KUDA_HOOK_MIDDLE);
    clhy_hook_pre_read_request(event_pre_read_request, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_post_read_request(event_post_read_request, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clmp_get_name(event_get_name, NULL, NULL, KUDA_HOOK_MIDDLE);

    clhy_hook_pre_connection(event_pre_connection, NULL, NULL, KUDA_HOOK_REALLY_FIRST);
    clhy_hook_protocol_switch(event_protocol_switch, NULL, NULL, KUDA_HOOK_REALLY_FIRST);
}

static const char *set_daemons_to_start(cmd_parms *cmd, void *dummy,
                                        const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    clhy_daemons_to_start = atoi(arg);
    return NULL;
}

static const char *set_min_spare_threads(cmd_parms * cmd, void *dummy,
                                         const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    min_spare_threads = atoi(arg);
    return NULL;
}

static const char *set_max_spare_threads(cmd_parms * cmd, void *dummy,
                                         const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    max_spare_threads = atoi(arg);
    return NULL;
}

static const char *set_max_workers(cmd_parms * cmd, void *dummy,
                                   const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }
    if (!strcasecmp(cmd->cmd->name, "MaxClients")) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, NULL, CLHYLOGNO(00521)
                     "MaxClients is deprecated, use MaxRequestWorkers "
                     "instead.");
    }
    max_workers = atoi(arg);
    return NULL;
}

static const char *set_threads_per_child(cmd_parms * cmd, void *dummy,
                                         const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    threads_per_child = atoi(arg);
    return NULL;
}
static const char *set_server_limit (cmd_parms *cmd, void *dummy, const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    server_limit = atoi(arg);
    return NULL;
}

static const char *set_thread_limit(cmd_parms * cmd, void *dummy,
                                    const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    thread_limit = atoi(arg);
    return NULL;
}

static const char *set_worker_factor(cmd_parms * cmd, void *dummy,
                                     const char *arg)
{
    double val;
    char *endptr;
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    val = strtod(arg, &endptr);
    if (*endptr)
        return "error parsing value";

    if (val <= 0)
        return "AsyncRequestWorkerFactor argument must be a positive number";

    worker_factor = val * WORKER_FACTOR_SCALE;
    if (worker_factor < WORKER_FACTOR_SCALE) {
        worker_factor = WORKER_FACTOR_SCALE;
    }
    return NULL;
}


static const command_rec event_cmds[] = {
    LISTEN_COMMANDS,
    CLHY_INIT_TAKE1("StartServers", set_daemons_to_start, NULL, RSRC_CONF,
                  "Number of child processes launched at server startup"),
    CLHY_INIT_TAKE1("ServerLimit", set_server_limit, NULL, RSRC_CONF,
                  "Maximum number of child processes for this run of cLHy"),
    CLHY_INIT_TAKE1("MinSpareThreads", set_min_spare_threads, NULL, RSRC_CONF,
                  "Minimum number of idle threads, to handle request spikes"),
    CLHY_INIT_TAKE1("MaxSpareThreads", set_max_spare_threads, NULL, RSRC_CONF,
                  "Maximum number of idle threads"),
    CLHY_INIT_TAKE1("MaxClients", set_max_workers, NULL, RSRC_CONF,
                  "Deprecated name of MaxRequestWorkers"),
    CLHY_INIT_TAKE1("MaxRequestWorkers", set_max_workers, NULL, RSRC_CONF,
                  "Maximum number of threads alive at the same time"),
    CLHY_INIT_TAKE1("ThreadsPerChild", set_threads_per_child, NULL, RSRC_CONF,
                  "Number of threads each child creates"),
    CLHY_INIT_TAKE1("ThreadLimit", set_thread_limit, NULL, RSRC_CONF,
                  "Maximum number of worker threads per child process for this "
                  "run of cLHy - Upper limit for ThreadsPerChild"),
    CLHY_INIT_TAKE1("AsyncRequestWorkerFactor", set_worker_factor, NULL, RSRC_CONF,
                  "How many additional connects will be accepted per idle "
                  "worker thread"),
    CLHY_GRACEFUL_SHUTDOWN_TIMEOUT_COMMAND,
    {NULL}
};

CLHY_DECLARE_CAPI(clmp_event) = {
    CLMP16_CAPI_STUFF,
    NULL,                       /* hook to run before cLHy parses args */
    NULL,                       /* create per-directory config structure */
    NULL,                       /* merge per-directory config structures */
    NULL,                       /* create per-server config structure */
    NULL,                       /* merge per-server config structures */
    event_cmds,                 /* command kuda_table_t */
    event_hooks                 /* register_hooks */
};
