/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * wwhy.c: simple http daemon for answering WWW file requests
 *
 *
 * 03-21-93  Rob McCool wrote original code (up to NCSA WWHy 1.3)
 *
 * 03-06-95  blong
 *  changed server number for child-alone processes to 0 and changed name
 *   of processes
 *
 * 03-10-95  blong
 *      Added numerous speed hacks proposed by Robert S. Thau (rst@ai.mit.edu)
 *      including set group before fork, and call gettime before to fork
 *      to set up libraries.
 *
 * 04-14-95  rst / rh
 *      Brandon's code snarfed from NCSA 1.4, but tinkered to work with the
 *      cLHy server, and also to have child processes do accept() directly.
 *
 * April-July '95 rst
 *      Extensive rework for cLHy.
 */

#include "kuda.h"
#include "kuda_portable.h"
#include "kuda_strings.h"
#include "kuda_thread_proc.h"
#include "kuda_signal.h"
#include "kuda_tables.h"
#include "kuda_getopt.h"
#include "kuda_thread_mutex.h"

#define KUDA_WANT_STDIO
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#ifndef USE_WINSOCK
#include <sys/select.h>
#endif

#include "clhy_config.h"
#include "wwhy.h"
#include "core_default.h"
#include "http_main.h"
#include "http_log.h"
#include "http_config.h"
#include "http_core.h"             /* for get_remote_host */
#include "http_connection.h"
#include "scoreboard.h"
#include "clhy_core.h"
#include "core_common.h"
#include "clhy_listen.h"
#include "clhy_capimn.h"

#ifdef HAVE_TIME_H
#include <time.h>
#endif

#include <signal.h>

#include <netware.h>
#include <nks/netware.h>
#include <library.h>
#include <screen.h>

int nlmUnloadSignaled(int wait);

/* Limit on the total --- clients will be locked out if more servers than
 * this are needed.  It is intended solely to keep the server from crashing
 * when things get out of hand.
 *
 * We keep a hard maximum number of servers, for two reasons --- first off,
 * in case something goes seriously wrong, we want to stop the fork bomb
 * short of actually crashing the machine we're running on by filling some
 * kernel table.  Secondly, it keeps the size of the scoreboard file small
 * enough that we can read the whole thing without worrying too much about
 * the overhead.
 */
#ifndef HARD_SERVER_LIMIT
#define HARD_SERVER_LIMIT 1
#endif

#define WORKER_DEAD         SERVER_DEAD
#define WORKER_STARTING     SERVER_STARTING
#define WORKER_READY        SERVER_READY
#define WORKER_IDLE_KILL    SERVER_IDLE_KILL

#define CLMP_HARD_LIMITS_FILE "/core_default.h"

/* *Non*-shared http_main globals... */

static int clhy_threads_per_child=0;         /* Worker threads per child */
static int clhy_threads_to_start=0;
static int clhy_threads_min_free=0;
static int clhy_threads_max_free=0;
static int clhy_threads_limit=0;
static int clmp_state = CLHY_CLMPQ_STARTING;

/*
 * The max child slot ever assigned, preserved across restarts.  Necessary
 * to deal with MaxRequestWorkers changes across SIGWINCH restarts.  We use this
 * value to optimize routines that have to scan the entire scoreboard.
 */
static int clhy_max_workers_limit = -1;

int hold_screen_on_exit = 0; /* Indicates whether the screen should be held open */

static fd_set listenfds;
static int listenmaxfd;

static kuda_pool_t *pconf;               /* Pool for config stuff */
static kuda_pool_t *pmain;               /* Pool for wwhy child stuff */

static pid_t clhy_my_pid;  /* it seems silly to call getpid all the time */
static char *clhy_my_addrspace = NULL;

static int die_now = 0;

/* Keep track of the number of worker threads currently active */
static unsigned long worker_thread_count;
static int request_count;

/*  Structure used to register/deregister a console handler with the PLATFORM */
static int InstallConsoleHandler(void);
static void RemoveConsoleHandler(void);
static int CommandLineInterpreter(scr_t screenID, const char *commandLine);
static  CommandParser_t ConsoleHandler = {0, NULL, 0};
#define HANDLEDCOMMAND  0
#define NOTMYCOMMAND    1

static int show_settings = 0;

//#define DBINFO_ON
//#define DBPRINT_ON
#ifdef DBPRINT_ON
#define DBPRINT0(s) printf(s)
#define DBPRINT1(s,v1) printf(s,v1)
#define DBPRINT2(s,v1,v2) printf(s,v1,v2)
#else
#define DBPRINT0(s)
#define DBPRINT1(s,v1)
#define DBPRINT2(s,v1,v2)
#endif

/* volatile just in case */
static int volatile shutdown_pending;
static int volatile restart_pending;
static int volatile is_graceful;
static int volatile wait_to_finish=1;
static clhy_generation_t volatile clhy_my_generation=0;

/* a clean exit from a child with proper cleanup */
static void clean_child_exit(int code, int worker_num, kuda_pool_t *ptrans,
                             kuda_bucket_alloc_t *bucket_alloc) __attribute__ ((noreturn));
static void clean_child_exit(int code, int worker_num, kuda_pool_t *ptrans,
                             kuda_bucket_alloc_t *bucket_alloc)
{
    kuda_bucket_alloc_destroy(bucket_alloc);
    if (!shutdown_pending) {
        kuda_pool_destroy(ptrans);
    }

    atomic_dec (&worker_thread_count);
    if (worker_num >=0)
        clhy_update_child_status_from_indexes(0, worker_num, WORKER_DEAD,
                                            (request_rec *) NULL);
    NXThreadExit((void*)&code);
}

/* proper cleanup when returning from clhy_clmp_run() */
static void clmp_main_cleanup(void)
{
    if (pmain) {
        kuda_pool_destroy(pmain);
    }
}

static int netware_query(int query_code, int *result, kuda_status_t *rv)
{
    *rv = KUDA_SUCCESS;
    switch(query_code){
        case CLHY_CLMPQ_MAX_DAEMON_USED:
            *result = 1;
            break;
        case CLHY_CLMPQ_IS_THREADED:
            *result = CLHY_CLMPQ_DYNAMIC;
            break;
        case CLHY_CLMPQ_IS_FORKED:
            *result = CLHY_CLMPQ_NOT_SUPPORTED;
            break;
        case CLHY_CLMPQ_HARD_LIMIT_DAEMONS:
            *result = HARD_SERVER_LIMIT;
            break;
        case CLHY_CLMPQ_HARD_LIMIT_THREADS:
            *result = HARD_THREAD_LIMIT;
            break;
        case CLHY_CLMPQ_MAX_THREADS:
            *result = clhy_threads_limit;
            break;
        case CLHY_CLMPQ_MIN_SPARE_DAEMONS:
            *result = 0;
            break;
        case CLHY_CLMPQ_MIN_SPARE_THREADS:
            *result = clhy_threads_min_free;
            break;
        case CLHY_CLMPQ_MAX_SPARE_DAEMONS:
            *result = 0;
            break;
        case CLHY_CLMPQ_MAX_SPARE_THREADS:
            *result = clhy_threads_max_free;
            break;
        case CLHY_CLMPQ_MAX_REQUESTS_DAEMON:
            *result = clhy_max_requests_per_child;
            break;
        case CLHY_CLMPQ_MAX_DAEMONS:
            *result = 1;
            break;
        case CLHY_CLMPQ_CLMP_STATE:
            *result = clmp_state;
            break;
        case CLHY_CLMPQ_GENERATION:
            *result = clhy_my_generation;
            break;
        default:
            *rv = KUDA_ENOTIMPL;
            break;
    }
    return OK;
}

static const char *netware_get_name(void)
{
    return "NetWare";
}

/*****************************************************************
 * Connection structures and accounting...
 */

static void clmp_term(void)
{
    RemoveConsoleHandler();
    wait_to_finish = 0;
    NXThreadYield();
}

static void sig_term(int sig)
{
    if (shutdown_pending == 1) {
        /* Um, is this _probably_ not an error, if the user has
         * tried to do a shutdown twice quickly, so we won't
         * worry about reporting it.
         */
        return;
    }
    shutdown_pending = 1;

    DBPRINT0 ("waiting for threads\n");
    while (wait_to_finish) {
        kuda_thread_yield();
    }
    DBPRINT0 ("goodbye\n");
}

/* restart() is the signal handler for SIGHUP and SIGWINCH
 * in the parent process, unless running in ONE_PROCESS mode
 */
static void restart(void)
{
    if (restart_pending == 1) {
        /* Probably not an error - don't bother reporting it */
        return;
    }
    restart_pending = 1;
    is_graceful = 1;
}

static void set_signals(void)
{
    kuda_signal(SIGTERM, sig_term);
    kuda_signal(SIGABRT, sig_term);
}

int nlmUnloadSignaled(int wait)
{
    shutdown_pending = 1;

    if (wait) {
        while (wait_to_finish) {
            NXThreadYield();
        }
    }

    return 0;
}

/*****************************************************************
 * Child process main loop.
 * The following vars are static to avoid getting clobbered by longjmp();
 * they are really private to child_main.
 */


#define MAX_WB_RETRIES  3
#ifdef DBINFO_ON
static int would_block = 0;
static int retry_success = 0;
static int retry_fail = 0;
static int avg_retries = 0;
#endif

/*static */
void worker_main(void *arg)
{
    clhy_listen_rec *lr, *first_lr, *last_lr = NULL;
    kuda_pool_t *ptrans;
    kuda_allocator_t *allocator;
    kuda_bucket_alloc_t *bucket_alloc;
    conn_rec *current_conn;
    kuda_status_t stat = KUDA_EINIT;
    clhy_sb_handle_t *sbh;
    kuda_thread_t *thd = NULL;
    kuda_platform_thread_t osthd;

    int my_worker_num = (int)arg;
    kuda_socket_t *csd = NULL;
    int requests_this_child = 0;
    kuda_socket_t *sd = NULL;
    fd_set main_fds;

    int sockdes;
    int srv;
    struct timeval tv;
    int wouldblock_retry;

    osthd = kuda_platform_thread_current();
    kuda_platform_thread_put(&thd, &osthd, pmain);

    tv.tv_sec = 1;
    tv.tv_usec = 0;

    kuda_allocator_create(&allocator);
    kuda_allocator_max_free_set(allocator, clhy_max_mem_free);

    kuda_pool_create_ex(&ptrans, pmain, NULL, allocator);
    kuda_allocator_owner_set(allocator, ptrans);
    kuda_pool_tag(ptrans, "transaction");

    bucket_alloc = kuda_bucket_alloc_create_ex(allocator);

    atomic_inc (&worker_thread_count);

    while (!die_now) {
        /*
        * (Re)initialize this child to a pre-connection state.
        */
        current_conn = NULL;
        kuda_pool_clear(ptrans);

        if ((clhy_max_requests_per_child > 0
            && requests_this_child++ >= clhy_max_requests_per_child)) {
            DBPRINT1 ("\n**Thread slot %d is shutting down", my_worker_num);
            clean_child_exit(0, my_worker_num, ptrans, bucket_alloc);
        }

        clhy_update_child_status_from_indexes(0, my_worker_num, WORKER_READY,
                                            (request_rec *) NULL);

        /*
        * Wait for an acceptable connection to arrive.
        */

        for (;;) {
            if (shutdown_pending || restart_pending || (clhy_scoreboard_image->servers[0][my_worker_num].status == WORKER_IDLE_KILL)) {
                DBPRINT1 ("\nThread slot %d is shutting down\n", my_worker_num);
                clean_child_exit(0, my_worker_num, ptrans, bucket_alloc);
            }

            /* Check the listen queue on all sockets for requests */
            memcpy(&main_fds, &listenfds, sizeof(fd_set));
            srv = select(listenmaxfd + 1, &main_fds, NULL, NULL, &tv);

            if (srv <= 0) {
                if (srv < 0) {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00217)
                        "select() failed on listen socket");
                    kuda_thread_yield();
                }
                continue;
            }

            /* remember the last_lr we searched last time around so that
            we don't end up starving any particular listening socket */
            if (last_lr == NULL) {
                lr = clhy_listeners;
            }
            else {
                lr = last_lr->next;
                if (!lr)
                    lr = clhy_listeners;
            }
            first_lr = lr;
            do {
                kuda_platform_sock_get(&sockdes, lr->sd);
                if (FD_ISSET(sockdes, &main_fds))
                    goto got_listener;
                lr = lr->next;
                if (!lr)
                    lr = clhy_listeners;
            } while (lr != first_lr);
            /* if we get here, something unexpected happened. Go back
            into the select state and try again.
            */
            continue;
        got_listener:
            last_lr = lr;
            sd = lr->sd;

            wouldblock_retry = MAX_WB_RETRIES;

            while (wouldblock_retry) {
                if ((stat = kuda_socket_accept(&csd, sd, ptrans)) == KUDA_SUCCESS) {
                    break;
                }
                else {
                    /* if the error is a wouldblock then maybe we were too
                        quick try to pull the next request from the listen
                        queue.  Try a few more times then return to our idle
                        listen state. */
                    if (!KUDA_STATUS_IS_EAGAIN(stat)) {
                        break;
                    }

                    if (wouldblock_retry--) {
                        kuda_thread_yield();
                    }
                }
            }

            /* If we got a new socket, set it to non-blocking mode and process
                it.  Otherwise handle the error. */
            if (stat == KUDA_SUCCESS) {
                kuda_socket_opt_set(csd, KUDA_SO_NONBLOCK, 0);
#ifdef DBINFO_ON
                if (wouldblock_retry < MAX_WB_RETRIES) {
                    retry_success++;
                    avg_retries += (MAX_WB_RETRIES-wouldblock_retry);
                }
#endif
                break;       /* We have a socket ready for reading */
            }
            else {
#ifdef DBINFO_ON
                if (KUDA_STATUS_IS_EAGAIN(stat)) {
                        would_block++;
                        retry_fail++;
                }
                else if (
#else
                if (KUDA_STATUS_IS_EAGAIN(stat) ||
#endif
                    KUDA_STATUS_IS_ECONNRESET(stat) ||
                    KUDA_STATUS_IS_ETIMEDOUT(stat) ||
                    KUDA_STATUS_IS_EHOSTUNREACH(stat) ||
                    KUDA_STATUS_IS_ENETUNREACH(stat)) {
                        ;
                }
#ifdef USE_WINSOCK
                else if (KUDA_STATUS_IS_ENETDOWN(stat)) {
                       /*
                        * When the network layer has been shut down, there
                        * is not much use in simply exiting: the parent
                        * would simply re-create us (and we'd fail again).
                        * Use the CHILDFATAL code to tear the server down.
                        * @@@ Martin's idea for possible improvement:
                        * A different approach would be to define
                        * a new APEXIT_NETDOWN exit code, the reception
                        * of which would make the parent shutdown all
                        * children, then idle-loop until it detected that
                        * the network is up again, and restart the children.
                        * Ben Hyde noted that temporary ENETDOWN situations
                        * occur in mobile IP.
                        */
                        clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, stat, clhy_server_conf, CLHYLOGNO(00218)
                            "kuda_socket_accept: giving up.");
                        clean_child_exit(APEXIT_CHILDFATAL, my_worker_num, ptrans,
                                         bucket_alloc);
                }
#endif
                else {
                        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, stat, clhy_server_conf, CLHYLOGNO(00219)
                            "kuda_socket_accept: (client socket)");
                        clean_child_exit(1, my_worker_num, ptrans, bucket_alloc);
                }
            }
        }

        clhy_create_sb_handle(&sbh, ptrans, 0, my_worker_num);
        /*
        * We now have a connection, so set it up with the appropriate
        * socket options, file descriptors, and read/write buffers.
        */
        current_conn = clhy_run_create_connection(ptrans, clhy_server_conf, csd,
                                                my_worker_num, sbh,
                                                bucket_alloc);
        if (current_conn) {
            current_conn->current_thread = thd;
            clhy_process_connection(current_conn, csd);
            clhy_lingering_close(current_conn);
        }
        request_count++;
    }
    clean_child_exit(0, my_worker_num, ptrans, bucket_alloc);
}


static int make_child(server_rec *s, int slot)
{
    int tid;
    int err=0;
    NXContext_t ctx;

    if (slot + 1 > clhy_max_workers_limit) {
        clhy_max_workers_limit = slot + 1;
    }

    clhy_update_child_status_from_indexes(0, slot, WORKER_STARTING,
                                        (request_rec *) NULL);

    if (ctx = NXContextAlloc((void (*)(void *)) worker_main, (void*)slot, NX_PRIO_MED, clhy_thread_stacksize, NX_CTX_NORMAL, &err)) {
        char threadName[32];

        sprintf (threadName, "cLHy_Worker %d", slot);
        NXContextSetName(ctx, threadName);
        err = NXThreadCreate(ctx, NX_THR_BIND_CONTEXT, &tid);
        if (err) {
            NXContextFree (ctx);
        }
    }

    if (err) {
        /* create thread didn't succeed. Fix the scoreboard or else
        * it will say SERVER_STARTING forever and ever
        */
        clhy_update_child_status_from_indexes(0, slot, WORKER_DEAD,
                                            (request_rec *) NULL);

        /* In case system resources are maxxed out, we don't want
        cLHy running away with the CPU trying to fork over and
        over and over again. */
        kuda_thread_yield();

        return -1;
    }

    clhy_scoreboard_image->servers[0][slot].tid = tid;

    return 0;
}


/* start up a bunch of worker threads */
static void startup_workers(int number_to_start)
{
    int i;

    for (i = 0; number_to_start && i < clhy_threads_limit; ++i) {
        if (clhy_scoreboard_image->servers[0][i].status != WORKER_DEAD) {
            continue;
        }
        if (make_child(clhy_server_conf, i) < 0) {
            break;
        }
        --number_to_start;
    }
}


/*
 * idle_spawn_rate is the number of children that will be spawned on the
 * next maintenance cycle if there aren't enough idle servers.  It is
 * doubled up to MAX_SPAWN_RATE, and reset only when a cycle goes by
 * without the need to spawn.
 */
static int idle_spawn_rate = 1;
#ifndef MAX_SPAWN_RATE
#define MAX_SPAWN_RATE (64)
#endif
static int hold_off_on_exponential_spawning;

static void perform_idle_server_maintenance(kuda_pool_t *p)
{
    int i;
    int idle_count;
    worker_score *ws;
    int free_length;
    int free_slots[MAX_SPAWN_RATE];
    int last_non_dead;
    int total_non_dead;

    /* initialize the free_list */
    free_length = 0;

    idle_count = 0;
    last_non_dead = -1;
    total_non_dead = 0;

    for (i = 0; i < clhy_threads_limit; ++i) {
        int status;

        if (i >= clhy_max_workers_limit && free_length == idle_spawn_rate)
            break;
        ws = &clhy_scoreboard_image->servers[0][i];
        status = ws->status;
        if (status == WORKER_DEAD) {
            /* try to keep children numbers as low as possible */
            if (free_length < idle_spawn_rate) {
                free_slots[free_length] = i;
                ++free_length;
            }
        }
        else if (status == WORKER_IDLE_KILL) {
            /* If it is already marked to die, skip it */
            continue;
        }
        else {
            /* We consider a starting server as idle because we started it
            * at least a cycle ago, and if it still hasn't finished starting
            * then we're just going to swamp things worse by forking more.
            * So we hopefully won't need to fork more if we count it.
            * This depends on the ordering of SERVER_READY and SERVER_STARTING.
            */
            if (status <= WORKER_READY) {
                ++ idle_count;
            }

            ++total_non_dead;
            last_non_dead = i;
        }
    }
    DBPRINT2("Total: %d Idle Count: %d  \r", total_non_dead, idle_count);
    clhy_max_workers_limit = last_non_dead + 1;
    if (idle_count > clhy_threads_max_free) {
        /* kill off one child... we use the pod because that'll cause it to
        * shut down gracefully, in case it happened to pick up a request
        * while we were counting
        */
        idle_spawn_rate = 1;
        clhy_update_child_status_from_indexes(0, last_non_dead, WORKER_IDLE_KILL,
                                            (request_rec *) NULL);
        DBPRINT1("\nKilling idle thread: %d\n", last_non_dead);
    }
    else if (idle_count < clhy_threads_min_free) {
        /* terminate the free list */
        if (free_length == 0) {
            /* only report this condition once */
            static int reported = 0;

            if (!reported) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, clhy_server_conf, CLHYLOGNO(00220)
                    "server reached MaxRequestWorkers setting, consider"
                    " raising the MaxRequestWorkers setting");
                reported = 1;
            }
            idle_spawn_rate = 1;
        }
        else {
            if (idle_spawn_rate >= 8) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, clhy_server_conf, CLHYLOGNO(00221)
                    "server seems busy, (you may need "
                    "to increase StartServers, or Min/MaxSpareServers), "
                    "spawning %d children, there are %d idle, and "
                    "%d total children", idle_spawn_rate,
                    idle_count, total_non_dead);
            }
            DBPRINT0("\n");
            for (i = 0; i < free_length; ++i) {
                DBPRINT1("Spawning additional thread slot: %d\n", free_slots[i]);
                make_child(clhy_server_conf, free_slots[i]);
            }
            /* the next time around we want to spawn twice as many if this
            * wasn't good enough, but not if we've just done a graceful
            */
            if (hold_off_on_exponential_spawning) {
                --hold_off_on_exponential_spawning;
            }
            else if (idle_spawn_rate < MAX_SPAWN_RATE) {
                idle_spawn_rate *= 2;
            }
        }
    }
    else {
        idle_spawn_rate = 1;
    }
}

static void display_settings()
{
    int status_array[SERVER_NUM_STATUS];
    int i, status, total=0;
    int reqs = request_count;
#ifdef DBINFO_ON
    int wblock = would_block;

    would_block = 0;
#endif

    request_count = 0;

    ClearScreen (getscreenhandle());
    printf("%s \n", clhy_get_server_description());

    for (i=0;i<SERVER_NUM_STATUS;i++) {
        status_array[i] = 0;
    }

    for (i = 0; i < clhy_threads_limit; ++i) {
        status = (clhy_scoreboard_image->servers[0][i]).status;
        status_array[status]++;
    }

    for (i=0;i<SERVER_NUM_STATUS;i++) {
        switch(i)
        {
        case SERVER_DEAD:
            printf ("Available:\t%d\n", status_array[i]);
            break;
        case SERVER_STARTING:
            printf ("Starting:\t%d\n", status_array[i]);
            break;
        case SERVER_READY:
            printf ("Ready:\t\t%d\n", status_array[i]);
            break;
        case SERVER_BUSY_READ:
            printf ("Busy:\t\t%d\n", status_array[i]);
            break;
        case SERVER_BUSY_WRITE:
            printf ("Busy Write:\t%d\n", status_array[i]);
            break;
        case SERVER_BUSY_KEEPALIVE:
            printf ("Busy Keepalive:\t%d\n", status_array[i]);
            break;
        case SERVER_BUSY_LOG:
            printf ("Busy Log:\t%d\n", status_array[i]);
            break;
        case SERVER_BUSY_DNS:
            printf ("Busy DNS:\t%d\n", status_array[i]);
            break;
        case SERVER_CLOSING:
            printf ("Closing:\t%d\n", status_array[i]);
            break;
        case SERVER_GRACEFUL:
            printf ("Restart:\t%d\n", status_array[i]);
            break;
        case SERVER_IDLE_KILL:
            printf ("Idle Kill:\t%d\n", status_array[i]);
            break;
        default:
            printf ("Unknown Status:\t%d\n", status_array[i]);
            break;
        }
        if (i != SERVER_DEAD)
            total+=status_array[i];
    }
    printf ("Total Running:\t%d\tout of: \t%d\n", total, clhy_threads_limit);
    printf ("Requests per interval:\t%d\n", reqs);

#ifdef DBINFO_ON
    printf ("Would blocks:\t%d\n", wblock);
    printf ("Successful retries:\t%d\n", retry_success);
    printf ("Failed retries:\t%d\n", retry_fail);
    printf ("Avg retries:\t%d\n", retry_success == 0 ? 0 : avg_retries / retry_success);
#endif
}

static void show_server_data()
{
    clhy_listen_rec *lr;
    cAPI **m;

    printf("%s\n", clhy_get_server_description());
    if (clhy_my_addrspace && (clhy_my_addrspace[0] != 'O') && (clhy_my_addrspace[1] != 'S'))
        printf("   Running in address space %s\n", clhy_my_addrspace);


    /* Display listening ports */
    printf("   Listening on port(s):");
    lr = clhy_listeners;
    do {
       printf(" %d", lr->bind_addr->port);
       lr = lr->next;
    } while (lr && lr != clhy_listeners);

    /* Display dynamic cAPIs loaded */
    printf("\n");
    for (m = clhy_activated_capis; *m != NULL; m++) {
        if (((cAPI*)*m)->dynamic_load_handle) {
            printf("   Loaded dynamic cAPI %s\n", ((cAPI*)*m)->name);
        }
    }
}


static int setup_listeners(server_rec *s)
{
    clhy_listen_rec *lr;
    int sockdes;

    if (clhy_setup_listeners(s) < 1 ) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, 0, s, CLHYLOGNO(00222)
            "no listening sockets available, shutting down");
        return -1;
    }

    listenmaxfd = -1;
    FD_ZERO(&listenfds);
    for (lr = clhy_listeners; lr; lr = lr->next) {
        kuda_platform_sock_get(&sockdes, lr->sd);
        FD_SET(sockdes, &listenfds);
        if (sockdes > listenmaxfd) {
            listenmaxfd = sockdes;
        }
    }
    return 0;
}

static int shutdown_listeners()
{
    clhy_listen_rec *lr;

    for (lr = clhy_listeners; lr; lr = lr->next) {
        kuda_socket_close(lr->sd);
    }
    clhy_listeners = NULL;
    return 0;
}

/*****************************************************************
 * Executive routines.
 */

static int netware_run(kuda_pool_t *_pconf, kuda_pool_t *plog, server_rec *s)
{
    kuda_status_t status=0;

    pconf = _pconf;
    clhy_server_conf = s;

    if (setup_listeners(s)) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, status, s, CLHYLOGNO(00223)
            "no listening sockets available, shutting down");
        return !OK;
    }

    restart_pending = shutdown_pending = 0;
    worker_thread_count = 0;

    if (!is_graceful) {
        if (clhy_run_pre_clmp(s->process->pool, SB_NOT_SHARED) != OK) {
            return !OK;
        }
    }

    /* Only set slot 0 since that is all NetWare will ever have. */
    clhy_scoreboard_image->parent[0].pid = getpid();
    clhy_run_child_status(clhy_server_conf,
                        clhy_scoreboard_image->parent[0].pid,
                        clhy_my_generation,
                        0,
                        CLMP_CHILD_STARTED);

    set_signals();

    kuda_pool_create(&pmain, pconf);
    clhy_run_child_init(pmain, clhy_server_conf);

    if (clhy_threads_max_free < clhy_threads_min_free + 1)  /* Don't thrash... */
        clhy_threads_max_free = clhy_threads_min_free + 1;
    request_count = 0;

    startup_workers(clhy_threads_to_start);

     /* Allow the cLHy screen to be closed normally on exit() only if it
        has not been explicitly forced to close on exit(). (ie. the -E flag
        was specified at startup) */
    if (hold_screen_on_exit > 0) {
        hold_screen_on_exit = 0;
    }

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00224)
            "%s configured -- resuming normal operations",
            clhy_get_server_description());
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, clhy_server_conf, CLHYLOGNO(00225)
            "Server built: %s", clhy_get_server_built());
    clhy_log_command_line(plog, s);
    clhy_log_core_common(s);
    show_server_data();

    clmp_state = CLHY_CLMPQ_RUNNING;
    while (!restart_pending && !shutdown_pending) {
        perform_idle_server_maintenance(pconf);
        if (show_settings)
            display_settings();
        kuda_thread_yield();
        kuda_sleep(SCOREBOARD_MAINTENANCE_INTERVAL);
    }
    clmp_state = CLHY_CLMPQ_STOPPING;

    clhy_run_child_status(clhy_server_conf,
                        clhy_scoreboard_image->parent[0].pid,
                        clhy_my_generation,
                        0,
                        CLMP_CHILD_EXITED);

    /* Shutdown the listen sockets so that we don't get stuck in a blocking call.
    shutdown_listeners();*/

    if (shutdown_pending) { /* Got an unload from the console */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00226)
            "caught SIGTERM, shutting down");

        while (worker_thread_count > 0) {
            printf ("\rShutdown pending. Waiting for %lu thread(s) to terminate...",
                    worker_thread_count);
            kuda_thread_yield();
        }

        clmp_main_cleanup();
        return DONE;
    }
    else {  /* the only other way out is a restart */
        /* advance to the next generation */
        /* XXX: we really need to make sure this new generation number isn't in
         * use by any of the children.
         */
        ++clhy_my_generation;
        clhy_scoreboard_image->global->running_generation = clhy_my_generation;

        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00227)
                "Graceful restart requested, doing restart");

        /* Wait for all of the threads to terminate before initiating the restart */
        while (worker_thread_count > 0) {
            printf ("\rRestart pending. Waiting for %lu thread(s) to terminate...",
                    worker_thread_count);
            kuda_thread_yield();
        }
        printf ("\nRestarting...\n");
    }

    clmp_main_cleanup();
    return OK;
}

static int netware_pre_config(kuda_pool_t *p, kuda_pool_t *plog, kuda_pool_t *ptemp)
{
    char *addrname = NULL;

    clmp_state = CLHY_CLMPQ_STARTING;

    is_graceful = 0;
    clhy_my_pid = getpid();
    addrname = getaddressspacename (NULL, NULL);
    if (addrname) {
        clhy_my_addrspace = kuda_pstrdup (p, addrname);
        free (addrname);
    }

#ifndef USE_WINSOCK
    /* The following call has been moved to the capi_nw_ssl pre-config handler */
    clhy_listen_pre_config();
#endif

    clhy_threads_to_start = DEFAULT_START_THREADS;
    clhy_threads_min_free = DEFAULT_MIN_FREE_THREADS;
    clhy_threads_max_free = DEFAULT_MAX_FREE_THREADS;
    clhy_threads_limit = HARD_THREAD_LIMIT;
    clhy_extended_status = 0;

    /* override core's default thread stacksize */
    clhy_thread_stacksize = DEFAULT_THREAD_STACKSIZE;

    return OK;
}

static int netware_check_config(kuda_pool_t *p, kuda_pool_t *plog,
                                kuda_pool_t *ptemp, server_rec *s)
{
    static int restart_num = 0;
    int startup = 0;

    /* we want this only the first time around */
    if (restart_num++ == 0) {
        startup = 1;
    }

    if (clhy_threads_limit > HARD_THREAD_LIMIT) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00228)
                         "WARNING: MaxThreads of %d exceeds compile-time "
                         "limit of %d threads, decreasing to %d. "
                         "To increase, please see the HARD_THREAD_LIMIT "
                         "define in server/clcore/netware%s.",
                         clhy_threads_limit, HARD_THREAD_LIMIT, HARD_THREAD_LIMIT,
                         CLMP_HARD_LIMITS_FILE);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00229)
                         "MaxThreads of %d exceeds compile-time limit "
                         "of %d, decreasing to match",
                         clhy_threads_limit, HARD_THREAD_LIMIT);
        }
        clhy_threads_limit = HARD_THREAD_LIMIT;
    }
    else if (clhy_threads_limit < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00230)
                         "WARNING: MaxThreads of %d not allowed, "
                         "increasing to 1.", clhy_threads_limit);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(02661)
                         "MaxThreads of %d not allowed, increasing to 1",
                         clhy_threads_limit);
        }
        clhy_threads_limit = 1;
    }

    /* clhy_threads_to_start > clhy_threads_limit effectively checked in
     * call to startup_workers(clhy_threads_to_start) in clhy_clmp_run()
     */
    if (clhy_threads_to_start < 0) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00231)
                         "WARNING: StartThreads of %d not allowed, "
                         "increasing to 1.", clhy_threads_to_start);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00232)
                         "StartThreads of %d not allowed, increasing to 1",
                         clhy_threads_to_start);
        }
        clhy_threads_to_start = 1;
    }

    if (clhy_threads_min_free < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00233)
                         "WARNING: MinSpareThreads of %d not allowed, "
                         "increasing to 1 to avoid almost certain server failure. "
                         "Please read the documentation.", clhy_threads_min_free);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00234)
                         "MinSpareThreads of %d not allowed, increasing to 1",
                         clhy_threads_min_free);
        }
        clhy_threads_min_free = 1;
    }

    /* clhy_threads_max_free < clhy_threads_min_free + 1 checked in clhy_clmp_run() */

    return OK;
}

static void netware_clmp_hooks(kuda_pool_t *p)
{
    /* Run the pre-config hook after core's so that it can override the
     * default setting of ThreadStackSize for NetWare.
     */
    static const char * const predecessors[] = {"core.c", NULL};

    clhy_hook_pre_config(netware_pre_config, predecessors, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_check_config(netware_check_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    //clhy_hook_post_config(netware_post_config, NULL, NULL, 0);
    //clhy_hook_child_init(netware_child_init, NULL, NULL, KUDA_HOOK_MIDDLE);
    //clhy_hook_open_logs(netware_open_logs, NULL, aszSucc, KUDA_HOOK_REALLY_FIRST);
    clhy_hook_clcore(netware_run, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clmp_query(netware_query, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clmp_get_name(netware_get_name, NULL, NULL, KUDA_HOOK_MIDDLE);
}

static void netware_rewrite_args(process_rec *process)
{
    char *def_server_root;
    char optbuf[3];
    const char *opt_arg;
    kuda_getopt_t *opt;
    kuda_array_header_t *clmp_new_argv;


    atexit (clmp_term);
    InstallConsoleHandler();

    /* Make sure to hold the cLHy screen open if exit() is called */
    hold_screen_on_exit = 1;

    /* Rewrite process->argv[];
     *
     * add default -d serverroot from the path of this executable
     *
     * The end result will look like:
     *     The -d serverroot default from the running executable
     */
    if (process->argc > 0) {
        char *s = kuda_pstrdup (process->pconf, process->argv[0]);
        if (s) {
            int i, len = strlen(s);

            for (i=len; i; i--) {
                if (s[i] == '\\' || s[i] == '/') {
                    s[i] = '\0';
                    kuda_filepath_merge(&def_server_root, NULL, s,
                        KUDA_FILEPATH_TRUENAME, process->pool);
                    break;
                }
            }
            /* Use process->pool so that the rewritten argv
            * lasts for the lifetime of the server process,
            * because pconf will be destroyed after the
            * initial pre-flight of the config parser.
            */
            clmp_new_argv = kuda_array_make(process->pool, process->argc + 2,
                                  sizeof(const char *));
            *(const char **)kuda_array_push(clmp_new_argv) = process->argv[0];
            *(const char **)kuda_array_push(clmp_new_argv) = "-d";
            *(const char **)kuda_array_push(clmp_new_argv) = def_server_root;

            optbuf[0] = '-';
            optbuf[2] = '\0';
            kuda_getopt_init(&opt, process->pool, process->argc, process->argv);
            while (kuda_getopt(opt, CLHY_SERVER_BASEARGS"n:", optbuf + 1, &opt_arg) == KUDA_SUCCESS) {
                switch (optbuf[1]) {
                case 'n':
                    if (opt_arg) {
                        renamescreen(opt_arg);
                    }
                    break;
                case 'E':
                    /* Don't need to hold the screen open if the output is going to a file */
                    hold_screen_on_exit = -1;
                default:
                    *(const char **)kuda_array_push(clmp_new_argv) =
                        kuda_pstrdup(process->pool, optbuf);

                    if (opt_arg) {
                        *(const char **)kuda_array_push(clmp_new_argv) = opt_arg;
                    }
                    break;
                }
            }
            process->argc = clmp_new_argv->nelts;
            process->argv = (const char * const *) clmp_new_argv->elts;
        }
    }
}

static int CommandLineInterpreter(scr_t screenID, const char *commandLine)
{
    char *szCommand = "CLHYDELMAN ";
    int iCommandLen = 8;
    char szcommandLine[256];
    char *pID;
    screenID = screenID;


    if (commandLine == NULL)
        return NOTMYCOMMAND;
    if (strlen(commandLine) <= strlen(szCommand))
        return NOTMYCOMMAND;

    kuda_cpystrn(szcommandLine, commandLine, sizeof(szcommandLine));

    /*  All added commands begin with "CLHYDELMAN " */

    if (!strnicmp(szCommand, szcommandLine, iCommandLen)) {
        ActivateScreen (getscreenhandle());

        /* If an instance id was not given but the nlm is loaded in
            protected space, then the command belongs to the
            PLATFORM address space instance to pass it on. */
        pID = strstr (szcommandLine, "-p");
        if ((pID == NULL) && nlmisloadedprotected())
            return NOTMYCOMMAND;

        /* If we got an instance id but it doesn't match this
            instance of the nlm, pass it on. */
        if (pID) {
            pID = &pID[2];
            while (*pID && (*pID == ' '))
                pID++;
        }
        if (pID && clhy_my_addrspace && strnicmp(pID, clhy_my_addrspace, strlen(clhy_my_addrspace)))
            return NOTMYCOMMAND;

        /* If we have determined that this command belongs to this
            instance of the nlm, then handle it. */
        if (!strnicmp("RESTART",&szcommandLine[iCommandLen],3)) {
            printf("Restart Requested...\n");
            restart();
        }
        else if (!strnicmp("VERSION",&szcommandLine[iCommandLen],3)) {
            printf("Server version: %s\n", clhy_get_server_description());
            printf("Server built:   %s\n", clhy_get_server_built());
        }
        else if (!strnicmp("CAPIS",&szcommandLine[iCommandLen],3)) {
            clhy_show_capis();
        }
        else if (!strnicmp("DIRECTIVES",&szcommandLine[iCommandLen],3)) {
                clhy_show_directives();
        }
        else if (!strnicmp("SHUTDOWN",&szcommandLine[iCommandLen],3)) {
            printf("Shutdown Requested...\n");
            shutdown_pending = 1;
        }
        else if (!strnicmp("SETTINGS",&szcommandLine[iCommandLen],3)) {
            if (show_settings) {
                show_settings = 0;
                ClearScreen (getscreenhandle());
                show_server_data();
            }
            else {
                show_settings = 1;
                display_settings();
            }
        }
        else {
            show_settings = 0;
            if (strnicmp("HELP",&szcommandLine[iCommandLen],3))
                printf("Unknown CLHYDELMAN command %s\n", &szcommandLine[iCommandLen]);
            printf("Usage: CLHYDELMAN [command] [-p <instance ID>]\n");
            printf("Commands:\n");
            printf("\tDIRECTIVES - Show directives\n");
            printf("\tHELP       - Display this help information\n");
            printf("\tCAPIS      - Show a list of activated cAPIs\n");
            printf("\tRESTART    - Reread the configuration file and restart cLHy\n");
            printf("\tSETTINGS   - Show current thread status\n");
            printf("\tSHUTDOWN   - Shutdown cLHy\n");
            printf("\tVERSION    - Display the server version information\n");
        }

        /*  Tell NetWare we handled the command */
        return HANDLEDCOMMAND;
    }

    /*  Tell NetWare that the command isn't mine */
    return NOTMYCOMMAND;
}

static int InstallConsoleHandler(void)
{
    /*  Our command line handler interfaces the system operator
    with this NLM */

    NX_WRAP_INTERFACE(CommandLineInterpreter, 2, (void*)&(ConsoleHandler.parser));

    ConsoleHandler.rTag = AllocateResourceTag(getnlmhandle(), "Command Line Processor",
        ConsoleCommandSignature);
    if (!ConsoleHandler.rTag)
    {
        printf("Error on allocate resource tag\n");
        return 1;
    }

    RegisterConsoleCommand(&ConsoleHandler);

    /*  The Remove procedure unregisters the console handler */

    return 0;
}

static void RemoveConsoleHandler(void)
{
    UnRegisterConsoleCommand(&ConsoleHandler);
    NX_UNWRAP_INTERFACE(ConsoleHandler.parser);
}

static const char *set_threads_to_start(cmd_parms *cmd, void *dummy, const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    clhy_threads_to_start = atoi(arg);
    return NULL;
}

static const char *set_min_free_threads(cmd_parms *cmd, void *dummy, const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    clhy_threads_min_free = atoi(arg);
    return NULL;
}

static const char *set_max_free_threads(cmd_parms *cmd, void *dummy, const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    clhy_threads_max_free = atoi(arg);
    return NULL;
}

static const char *set_thread_limit (cmd_parms *cmd, void *dummy, const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    clhy_threads_limit = atoi(arg);
    return NULL;
}

static const command_rec netware_clmp_cmds[] = {
LISTEN_COMMANDS,
CLHY_INIT_TAKE1("StartThreads", set_threads_to_start, NULL, RSRC_CONF,
              "Number of worker threads launched at server startup"),
CLHY_INIT_TAKE1("MinSpareThreads", set_min_free_threads, NULL, RSRC_CONF,
              "Minimum number of idle threads, to handle request spikes"),
CLHY_INIT_TAKE1("MaxSpareThreads", set_max_free_threads, NULL, RSRC_CONF,
              "Maximum number of idle threads"),
CLHY_INIT_TAKE1("MaxThreads", set_thread_limit, NULL, RSRC_CONF,
              "Maximum number of worker threads alive at the same time"),
{ NULL }
};

CLHY_DECLARE_CAPI(core_netware) = {
    CLMP16_CAPI_STUFF,
    netware_rewrite_args,   /* hook to run before clhy parses args */
    NULL,                   /* create per-directory config structure */
    NULL,                   /* merge per-directory config structures */
    NULL,                   /* create per-server config structure */
    NULL,                   /* merge per-server config structures */
    netware_clmp_cmds,       /* command kuda_table_t */
    netware_clmp_hooks,      /* register hooks */
};
