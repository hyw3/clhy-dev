/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Multi-process, multi-threaded cLMP for OS2
 *
 * Server consists of
 * - a main, parent process
 * - a small, static number of child processes
 *
 * The parent process's job is to manage the child processes. This involves
 * spawning children as required to ensure there are always clhy_daemons_to_start
 * processes accepting connections.
 *
 * Each child process consists of a pool of worker threads and a
 * main thread that accepts connections & passes them to the workers via
 * a work queue. The worker thread pool is dynamic, managed by a maintanence
 * thread so that the number of idle threads is kept between
 * min_spare_threads & max_spare_threads.
 *
 */

/*
 Todo list
 - Enforce MaxRequestWorkers somehow
*/
#define INCL_NOPMAPI
#define INCL_DOS
#define INCL_DOSERRORS

#include "clhy_config.h"
#include "wwhy.h"
#include "core_default.h"
#include "http_main.h"
#include "http_log.h"
#include "http_config.h"
#include "http_core.h"  /* for get_remote_host */
#include "http_connection.h"
#include "clhy_core.h"
#include "clhy_listen.h"
#include "kuda_portable.h"
#include "core_common.h"
#include "scoreboard.h"
#include "kuda_strings.h"
#include <os2.h>
#include <process.h>

/* We don't need many processes,
 * they're only for redundancy in the event of a crash
 */
#define HARD_SERVER_LIMIT 10

/* Limit on the total number of threads per process
 */
#ifndef HARD_THREAD_LIMIT
#define HARD_THREAD_LIMIT 256
#endif

server_rec *clhy_server_conf;
static kuda_pool_t *pconf = NULL;  /* Pool for config stuff */

/* Config globals */
static int one_process = 0;
static int clhy_daemons_to_start = 0;
static int clhy_thread_limit = 0;
int clhy_min_spare_threads = 0;
int clhy_max_spare_threads = 0;

/* Keep track of a few interesting statistics */
int clhy_max_daemons_limit = -1;

/* volatile just in case */
static int volatile shutdown_pending;
static int volatile restart_pending;
static int volatile is_graceful = 0;
clhy_generation_t volatile clhy_my_generation=0; /* Used by the scoreboard */
static int is_parent_process=TRUE;
HMTX clhy_clmp_accept_mutex = 0;

/* An array of these is stored in a shared memory area for passing
 * sockets from the parent to child processes
 */
typedef struct {
    struct sockaddr_in name;
    kuda_platform_sock_t listen_fd;
} listen_socket_t;

typedef struct {
    HMTX accept_mutex;
    listen_socket_t listeners[1];
} parent_info_t;

static int master_main();
static void spawn_child(int slot);
void clhy_clmp_child_main(kuda_pool_t *pconf);
static void set_signals();


static int os2threaded_run(kuda_pool_t *_pconf, kuda_pool_t *plog, server_rec *s )
{
    char *listener_shm_name;
    parent_info_t *parent_info;
    ULONG rc;
    pconf = _pconf;
    clhy_server_conf = s;
    restart_pending = 0;

    DosSetMaxFH(clhy_thread_limit * 2);
    listener_shm_name = kuda_psprintf(pconf, "/sharemem/wwhy/parent_info.%d", getppid());
    rc = DosGetNamedSharedMem((PPVOID)&parent_info, listener_shm_name, PAG_READ);
    is_parent_process = rc != 0;
    clhy_scoreboard_fname = kuda_psprintf(pconf, "/sharemem/wwhy/scoreboard.%d", is_parent_process ? getpid() : getppid());

    if (rc == 0) {
        /* Child process */
        clhy_listen_rec *lr;
        int num_listeners = 0;

        clhy_clmp_accept_mutex = parent_info->accept_mutex;

        /* Set up a default listener if necessary */
        if (clhy_listeners == NULL) {
            clhy_listen_rec *lr = kuda_pcalloc(s->process->pool, sizeof(clhy_listen_rec));
            clhy_listeners = lr;
            kuda_sockaddr_info_get(&lr->bind_addr, "0.0.0.0", KUDA_UNSPEC,
                                  DEFAULT_HTTP_PORT, 0, s->process->pool);
            kuda_socket_create(&lr->sd, lr->bind_addr->family,
                              SOCK_STREAM, 0, s->process->pool);
        }

        for (lr = clhy_listeners; lr; lr = lr->next) {
            kuda_sockaddr_t *sa;
            kuda_platform_sock_put(&lr->sd, &parent_info->listeners[num_listeners].listen_fd, pconf);
            kuda_socket_addr_get(&sa, KUDA_LOCAL, lr->sd);
            num_listeners++;
        }

        DosFreeMem(parent_info);

        /* Do the work */
        clhy_clmp_child_main(pconf);

        /* Outta here */
        return DONE;
    }
    else {
        /* Parent process */
        int rc;
        is_parent_process = TRUE;

        if (clhy_setup_listeners(clhy_server_conf) < 1) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, 0, s, CLHYLOGNO(00200)
                         "no listening sockets available, shutting down");
            return !OK;
        }

        clhy_log_pid(pconf, clhy_pid_fname);

        rc = master_main();
        ++clhy_my_generation;
        clhy_scoreboard_image->global->running_generation = clhy_my_generation;

        if (rc != OK) {
            clhy_remove_pid(pconf, clhy_pid_fname);
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00201)
                         "caught %s, shutting down",
                         (rc == DONE) ? "SIGTERM" : "error");
            return rc;
        }
    }  /* Parent process */

    return OK; /* Restart */
}



/* Main processing of the parent process
 * returns TRUE if restarting
 */
static int master_main()
{
    server_rec *s = clhy_server_conf;
    clhy_listen_rec *lr;
    parent_info_t *parent_info;
    char *listener_shm_name;
    int listener_num, num_listeners, slot;
    ULONG rc;

    printf("%s \n", clhy_get_server_description());
    set_signals();

    if (clhy_setup_listeners(clhy_server_conf) < 1) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, 0, s, CLHYLOGNO(00202)
                     "no listening sockets available, shutting down");
        return !OK;
    }

    /* Allocate a shared memory block for the array of listeners */
    for (num_listeners = 0, lr = clhy_listeners; lr; lr = lr->next) {
        num_listeners++;
    }

    listener_shm_name = kuda_psprintf(pconf, "/sharemem/wwhy/parent_info.%d", getpid());
    rc = DosAllocSharedMem((PPVOID)&parent_info, listener_shm_name,
                           sizeof(parent_info_t) + num_listeners * sizeof(listen_socket_t),
                           PAG_READ|PAG_WRITE|PAG_COMMIT);

    if (rc) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, KUDA_FROM_PLATFORM_ERROR(rc), s, CLHYLOGNO(00203)
                     "failure allocating shared memory, shutting down");
        return !OK;
    }

    /* Store the listener sockets in the shared memory area for our children to see */
    for (listener_num = 0, lr = clhy_listeners; lr; lr = lr->next, listener_num++) {
        kuda_platform_sock_get(&parent_info->listeners[listener_num].listen_fd, lr->sd);
    }

    /* Create mutex to prevent multiple child processes from detecting
     * a connection with kuda_poll()
     */

    rc = DosCreateMutexSem(NULL, &clhy_clmp_accept_mutex, DC_SEM_SHARED, FALSE);

    if (rc) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, KUDA_FROM_PLATFORM_ERROR(rc), s, CLHYLOGNO(00204)
                     "failure creating accept mutex, shutting down");
        return !OK;
    }

    parent_info->accept_mutex = clhy_clmp_accept_mutex;

    /* Allocate shared memory for scoreboard */
    if (clhy_scoreboard_image == NULL) {
        void *sb_mem;
        rc = DosAllocSharedMem(&sb_mem, clhy_scoreboard_fname,
                               clhy_calc_scoreboard_size(),
                               PAG_COMMIT|PAG_READ|PAG_WRITE);

        if (rc) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, KUDA_FROM_PLATFORM_ERROR(rc), clhy_server_conf, CLHYLOGNO(00205)
                         "unable to allocate shared memory for scoreboard , exiting");
            return !OK;
        }

        clhy_init_scoreboard(sb_mem);
    }

    clhy_scoreboard_image->global->restart_time = kuda_time_now();
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00206)
                "%s configured -- resuming normal operations",
                clhy_get_server_description());
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, clhy_server_conf, CLHYLOGNO(00207)
                "Server built: %s", clhy_get_server_built());
    if (one_process) {
        clhy_scoreboard_image->parent[0].pid = getpid();
        clhy_clmp_child_main(pconf);
        return DONE;
    }

    while (!restart_pending && !shutdown_pending) {
        RESULTCODES proc_rc;
        PID child_pid;
        int active_children = 0;

        /* Count number of active children */
        for (slot=0; slot < HARD_SERVER_LIMIT; slot++) {
            active_children += clhy_scoreboard_image->parent[slot].pid != 0 &&
                !clhy_scoreboard_image->parent[slot].quiescing;
        }

        /* Spawn children if needed */
        for (slot=0; slot < HARD_SERVER_LIMIT && active_children < clhy_daemons_to_start; slot++) {
            if (clhy_scoreboard_image->parent[slot].pid == 0) {
                spawn_child(slot);
                active_children++;
            }
        }

        rc = DosWaitChild(DCWA_PROCESSTREE, DCWW_NOWAIT, &proc_rc, &child_pid, 0);

        if (rc == 0) {
            /* A child has terminated, remove its scoreboard entry & terminate if necessary */
            for (slot=0; clhy_scoreboard_image->parent[slot].pid != child_pid && slot < HARD_SERVER_LIMIT; slot++);

            if (slot < HARD_SERVER_LIMIT) {
                clhy_scoreboard_image->parent[slot].pid = 0;
                clhy_scoreboard_image->parent[slot].quiescing = 0;

                if (proc_rc.codeTerminate == TC_EXIT) {
                    /* Child terminated normally, check its exit code and
                     * terminate server if child indicates a fatal error
                     */
                    if (proc_rc.codeResult == APEXIT_CHILDFATAL)
                        break;
                }
            }
        } else if (rc == ERROR_CHILD_NOT_COMPLETE) {
            /* No child exited, lets sleep for a while.... */
            kuda_sleep(SCOREBOARD_MAINTENANCE_INTERVAL);
        }
    }

    /* Signal children to shut down, either gracefully or immediately */
    for (slot=0; slot<HARD_SERVER_LIMIT; slot++) {
      kill(clhy_scoreboard_image->parent[slot].pid, is_graceful ? SIGHUP : SIGTERM);
    }

    DosFreeMem(parent_info);
    return restart_pending ? OK : DONE;
}



static void spawn_child(int slot)
{
    PPIB ppib;
    PTIB ptib;
    char fail_capi[100];
    char progname[CCHMAXPATH];
    RESULTCODES proc_rc;
    ULONG rc;

    clhy_scoreboard_image->parent[slot].generation = clhy_my_generation;
    DosGetInfoBlocks(&ptib, &ppib);
    DosQuerycAPIName(ppib->pib_hmte, sizeof(progname), progname);
    rc = DosExecPgm(fail_capi, sizeof(fail_capi), EXEC_ASYNCRESULT,
                    ppib->pib_pchcmd, NULL, &proc_rc, progname);

    if (rc) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, KUDA_FROM_PLATFORM_ERROR(rc), clhy_server_conf, CLHYLOGNO(00208)
                     "error spawning child, slot %d", slot);
    }

    if (clhy_max_daemons_limit < slot) {
        clhy_max_daemons_limit = slot;
    }

    clhy_scoreboard_image->parent[slot].pid = proc_rc.codeTerminate;
}



/* Signal handling routines */

static void sig_term(int sig)
{
    shutdown_pending = 1;
    signal(SIGTERM, SIG_DFL);
}



static void sig_restart(int sig)
{
    if (sig == SIGUSR1) {
        is_graceful = 1;
    }

    restart_pending = 1;
}



static void set_signals()
{
    struct sigaction sa;

    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = sig_term;

    if (sigaction(SIGTERM, &sa, NULL) < 0)
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, errno, clhy_server_conf, CLHYLOGNO(00209) "sigaction(SIGTERM)");

    if (sigaction(SIGINT, &sa, NULL) < 0)
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, errno, clhy_server_conf, CLHYLOGNO(00210) "sigaction(SIGINT)");

    sa.sa_handler = sig_restart;

    if (sigaction(SIGHUP, &sa, NULL) < 0)
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, errno, clhy_server_conf, CLHYLOGNO(00211) "sigaction(SIGHUP)");
    if (sigaction(SIGUSR1, &sa, NULL) < 0)
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, errno, clhy_server_conf, CLHYLOGNO(00212) "sigaction(SIGUSR1)");
}



/* Enquiry functions used get cLMP status info */

static kuda_status_t os2threaded_query(int query_code, int *result, kuda_status_t *rv)
{
    *rv = KUDA_SUCCESS;

    switch (query_code) {
        case CLHY_CLMPQ_MAX_DAEMON_USED:
            *result = clhy_max_daemons_limit;
            break;

        case CLHY_CLMPQ_IS_THREADED:
            *result = CLHY_CLMPQ_DYNAMIC;
            break;

        case CLHY_CLMPQ_IS_FORKED:
            *result = CLHY_CLMPQ_NOT_SUPPORTED;
            break;

        case CLHY_CLMPQ_HARD_LIMIT_DAEMONS:
            *result = HARD_SERVER_LIMIT;
            break;

        case CLHY_CLMPQ_HARD_LIMIT_THREADS:
            *result = HARD_THREAD_LIMIT;
            break;

        case CLHY_CLMPQ_MIN_SPARE_DAEMONS:
            *result = 0;
            break;

        case CLHY_CLMPQ_MAX_SPARE_DAEMONS:
            *result = 0;
            break;

        case CLHY_CLMPQ_MAX_REQUESTS_DAEMON:
            *result = clhy_max_requests_per_child;
            break;

        case CLHY_CLMPQ_GENERATION:
            *result = clhy_my_generation;
            break;

        default:
            *rv = KUDA_ENOTIMPL;
            break;
    }

    return OK;
}




static const char *os2threaded_get_name(void)
{
    return "os2threaded";
}




/* Configuration handling stuff */

static int os2threaded_pre_config(kuda_pool_t *pconf, kuda_pool_t *plog, kuda_pool_t *ptemp)
{
    one_process = clhy_exists_config_define("ONE_PROCESS") ||
                  clhy_exists_config_define("DEBUG");
    is_graceful = 0;
    clhy_listen_pre_config();
    clhy_daemons_to_start = DEFAULT_START_DAEMON;
    clhy_thread_limit = HARD_THREAD_LIMIT;
    clhy_extended_status = 0;
    clhy_min_spare_threads = DEFAULT_MIN_SPARE_THREAD;
    clhy_max_spare_threads = DEFAULT_MAX_SPARE_THREAD;
    clhy_sys_privileges_handlers(1);

    return OK;
}



static int os2threaded_check_config(kuda_pool_t *p, kuda_pool_t *plog,
                                 kuda_pool_t *ptemp, server_rec *s)
{
    static int restart_num = 0;
    int startup = 0;

    /* we want this only the first time around */
    if (restart_num++ == 0) {
        startup = 1;
    }

    if (clhy_daemons_to_start < 0) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00213)
                         "WARNING: StartServers of %d not allowed, "
                         "increasing to 1.", clhy_daemons_to_start);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00214)
                         "StartServers of %d not allowed, increasing to 1",
                         clhy_daemons_to_start);
        }
        clhy_daemons_to_start = 1;
    }

    if (clhy_min_spare_threads < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00215)
                         "WARNING: MinSpareThreads of %d not allowed, "
                         "increasing to 1 to avoid almost certain server failure. "
                         "Please read the documentation.", clhy_min_spare_threads);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00216)
                         "MinSpareThreads of %d not allowed, increasing to 1",
                         clhy_min_spare_threads);
        }
        clhy_min_spare_threads = 1;
    }

    return OK;
}



static void os2threaded_hooks(kuda_pool_t *p)
{
    clhy_hook_pre_config(os2threaded_pre_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_check_config(os2threaded_check_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clcore(os2threaded_run, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clmp_query(os2threaded_query, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clmp_get_name(os2threaded_get_name, NULL, NULL, KUDA_HOOK_MIDDLE);
}



static const char *set_daemons_to_start(cmd_parms *cmd, void *dummy, const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err != NULL) {
        return err;
    }

    clhy_daemons_to_start = atoi(arg);
    return NULL;
}



static const char *set_min_spare_threads(cmd_parms *cmd, void *dummy,
                                         const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err != NULL) {
        return err;
    }

    clhy_min_spare_threads = atoi(arg);
    return NULL;
}



static const char *set_max_spare_threads(cmd_parms *cmd, void *dummy,
                                         const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err != NULL) {
        return err;
    }

    clhy_max_spare_threads = atoi(arg);
    return NULL;
}



static const char *ignore_cmd(cmd_parms *cmd, void *dummy, const char *arg)
{
    return NULL;
}



static const command_rec os2threaded_cmds[] = {
LISTEN_COMMANDS,
CLHY_INIT_TAKE1( "StartServers", set_daemons_to_start, NULL, RSRC_CONF,
  "Number of child processes launched at server startup" ),
CLHY_INIT_TAKE1("MinSpareThreads", set_min_spare_threads, NULL, RSRC_CONF,
  "Minimum number of idle children, to handle request spikes"),
CLHY_INIT_TAKE1("MaxSpareThreads", set_max_spare_threads, NULL, RSRC_CONF,
  "Maximum number of idle children"),
CLHY_INIT_TAKE1("User", ignore_cmd, NULL, RSRC_CONF,
  "Not applicable on this platform"),
CLHY_INIT_TAKE1("Group", ignore_cmd, NULL, RSRC_CONF,
  "Not applicable on this platform"),
CLHY_INIT_TAKE1("ScoreBoardFile", ignore_cmd, NULL, RSRC_CONF, \
  "Not applicable on this platform"),
{ NULL }
};

CLHY_DECLARE_CAPI(clmp_os2threaded) = {
    CLMP16_CAPI_STUFF,
    NULL,            /* hook to run before clhy parses args */
    NULL,            /* create per-directory config structure */
    NULL,            /* merge per-directory config structures */
    NULL,            /* create per-server config structure */
    NULL,            /* merge per-server config structures */
    os2threaded_cmds,   /* command kuda_table_t */
    os2threaded_hooks,  /* register_hooks */
};
