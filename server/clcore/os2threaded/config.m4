AC_MSG_CHECKING(if os2threaded cLMP supports this platform)
case $host in
    *os2-emx*)
        AC_MSG_RESULT(yes)
        CLHYKUDEL_CLMP_SUPPORTED(os2threaded, no, yes)
        ;;
    *)
        AC_MSG_RESULT(no)
        ;;
esac
