/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  os2threaded/core_default.h
 * @brief os2 cLMP defaults
 *
 * @defgroup CLHYKUDEL_CLMP_OS2 OS2 cLMP
 * @ingroup CLHYKUDEL_INTERNAL
 * @{
 */

#ifndef CLHYKUDEL_CLMP_DEFAULT_H
#define CLHYKUDEL_CLMP_DEFAULT_H

/* Number of servers processes to spawn off by default
 */
#ifndef DEFAULT_START_DAEMON
#define DEFAULT_START_DAEMON 2
#endif

/* Maximum number of *free* server threads --- more than this, and
 * they will die off.
 */

#ifndef DEFAULT_MAX_SPARE_THREAD
#define DEFAULT_MAX_SPARE_THREAD 10
#endif

/* Minimum --- fewer than this, and more will be created */

#ifndef DEFAULT_MIN_SPARE_THREAD
#define DEFAULT_MIN_SPARE_THREAD 5
#endif

/*
 * Interval, in microseconds, between scoreboard maintenance.
 */
#ifndef SCOREBOARD_MAINTENANCE_INTERVAL
#define SCOREBOARD_MAINTENANCE_INTERVAL 1000000
#endif

#endif /* CLHY_CLMP_DEFAULT_H */
/** @} */
