/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  worker/fdqueue.h
 * @brief fd queue declarations
 *
 * @addtogroup CLHYKUDEL_CLMP_WORKER
 * @{
 */

#ifndef FDQUEUE_H
#define FDQUEUE_H
#include "wwhy.h"
#include <stdlib.h>
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <kuda_thread_mutex.h>
#include <kuda_thread_cond.h>
#include <sys/types.h>
#if KUDA_HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#include <kuda_errno.h>

typedef struct fd_queue_info_t fd_queue_info_t;

kuda_status_t clhy_queue_info_create(fd_queue_info_t **queue_info,
                                  kuda_pool_t *pool, int max_idlers);
kuda_status_t clhy_queue_info_set_idle(fd_queue_info_t *queue_info,
                                    kuda_pool_t *pool_to_recycle);
kuda_status_t clhy_queue_info_wait_for_idler(fd_queue_info_t *queue_info,
                                          kuda_pool_t **recycled_pool);
kuda_status_t clhy_queue_info_term(fd_queue_info_t *queue_info);

struct fd_queue_elem_t {
    kuda_socket_t      *sd;
    kuda_pool_t        *p;
};
typedef struct fd_queue_elem_t fd_queue_elem_t;

struct fd_queue_t {
    fd_queue_elem_t    *data;
    unsigned int       nelts;
    unsigned int       bounds;
    unsigned int       in;
    unsigned int       out;
    kuda_thread_mutex_t *one_big_mutex;
    kuda_thread_cond_t  *not_empty;
    int                 terminated;
};
typedef struct fd_queue_t fd_queue_t;

kuda_status_t clhy_queue_init(fd_queue_t *queue, int queue_capacity, kuda_pool_t *a);
kuda_status_t clhy_queue_push(fd_queue_t *queue, kuda_socket_t *sd, kuda_pool_t *p);
kuda_status_t clhy_queue_pop(fd_queue_t *queue, kuda_socket_t **sd, kuda_pool_t **p);
kuda_status_t clhy_queue_interrupt_all(fd_queue_t *queue);
kuda_status_t clhy_queue_term(fd_queue_t *queue);

#endif /* FDQUEUE_H */
/** @} */
