/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* The purpose of this cLMP is to fix the design flaws in the threaded
 * model.  Because of the way that pthreads and mutex locks interact,
 * it is basically impossible to cleanly gracefully shutdown a child
 * process if multiple threads are all blocked in accept.  This model
 * fixes those problems.
 */

#include "kuda.h"
#include "kuda_portable.h"
#include "kuda_strings.h"
#include "kuda_file_io.h"
#include "kuda_thread_proc.h"
#include "kuda_signal.h"
#include "kuda_thread_mutex.h"
#include "kuda_proc_mutex.h"
#include "kuda_poll.h"

#include <stdlib.h>

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#if KUDA_HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#if KUDA_HAVE_SYS_WAIT_H
#include <sys/wait.h>
#endif
#ifdef HAVE_SYS_PROCESSOR_H
#include <sys/processor.h> /* for bindprocessor() */
#endif

#if !KUDA_HAS_THREADS
#error The Worker cLMP requires kuda threads, but they are unavailable.
#endif

#include "clhy_config.h"
#include "wwhy.h"
#include "http_main.h"
#include "http_log.h"
#include "http_config.h"        /* for read_config */
#include "http_core.h"          /* for get_remote_host */
#include "http_connection.h"
#include "clhy_core.h"
#include "core_common.h"
#include "clhy_listen.h"
#include "scoreboard.h"
#include "core_fdqueue.h"
#include "core_default.h"
#include "util_mutex.h"
#include "unixd.h"

#include <signal.h>
#include <limits.h>             /* for INT_MAX */

/* Limit on the total --- clients will be locked out if more servers than
 * this are needed.  It is intended solely to keep the server from crashing
 * when things get out of hand.
 *
 * We keep a hard maximum number of servers, for two reasons --- first off,
 * in case something goes seriously wrong, we want to stop the fork bomb
 * short of actually crashing the machine we're running on by filling some
 * kernel table.  Secondly, it keeps the size of the scoreboard file small
 * enough that we can read the whole thing without worrying too much about
 * the overhead.
 */
#ifndef DEFAULT_SERVER_LIMIT
#define DEFAULT_SERVER_LIMIT 16
#endif

/* Admin can't tune ServerLimit beyond MAX_SERVER_LIMIT.  We want
 * some sort of compile-time limit to help catch typos.
 */
#ifndef MAX_SERVER_LIMIT
#define MAX_SERVER_LIMIT 20000
#endif

/* Limit on the threads per process.  Clients will be locked out if more than
 * this  * server_limit are needed.
 *
 * We keep this for one reason it keeps the size of the scoreboard file small
 * enough that we can read the whole thing without worrying too much about
 * the overhead.
 */
#ifndef DEFAULT_THREAD_LIMIT
#define DEFAULT_THREAD_LIMIT 64
#endif

/* Admin can't tune ThreadLimit beyond MAX_THREAD_LIMIT.  We want
 * some sort of compile-time limit to help catch typos.
 */
#ifndef MAX_THREAD_LIMIT
#define MAX_THREAD_LIMIT 20000
#endif

/*
 * Actual definitions of config globals
 */

static int threads_per_child = 0;     /* Worker threads per child */
static int clhy_daemons_to_start = 0;
static int min_spare_threads = 0;
static int max_spare_threads = 0;
static int clhy_daemons_limit = 0;
static int max_workers = 0;
static int server_limit = 0;
static int thread_limit = 0;
static int had_healthy_child = 0;
static int dying = 0;
static int workers_may_exit = 0;
static int start_thread_may_exit = 0;
static int listener_may_exit = 0;
static int requests_this_child;
static int num_listensocks = 0;
static int resource_shortage = 0;
static fd_queue_t *worker_queue;
static fd_queue_info_t *worker_queue_info;
static kuda_pollset_t *worker_pollset;


/* data retained by worker across load/unload of the cAPI
 * allocated on first call to pre-config hook; located on
 * subsequent calls to pre-config hook
 */
typedef struct worker_retained_data {
    clhy_unixd_clmp_retained_data *clmp;

    int first_server_limit;
    int first_thread_limit;
    int sick_child_detected;
    int maxclients_reported;
    int near_maxclients_reported;
    /*
     * The max child slot ever assigned, preserved across restarts.  Necessary
     * to deal with MaxRequestWorkers changes across CLHY_SIG_GRACEFUL restarts.
     * We use this value to optimize routines that have to scan the entire
     * scoreboard.
     */
    int max_daemons_limit;
    /*
     * idle_spawn_rate is the number of children that will be spawned on the
     * next maintenance cycle if there aren't enough idle servers.  It is
     * maintained per listeners bucket, doubled up to MAX_SPAWN_RATE, and
     * reset only when a cycle goes by without the need to spawn.
     */
    int *idle_spawn_rate;
#ifndef MAX_SPAWN_RATE
#define MAX_SPAWN_RATE        (32)
#endif
    int hold_off_on_exponential_spawning;
} worker_retained_data;
static worker_retained_data *retained;

typedef struct worker_child_bucket {
    clhy_pod_t *pod;
    clhy_listen_rec *listeners;
    kuda_proc_mutex_t *mutex;
} worker_child_bucket;
static worker_child_bucket *all_buckets, /* All listeners buckets */
                           *my_bucket;   /* Current child bucket */

#define CLMP_CHILD_PID(i) (clhy_scoreboard_image->parent[i].pid)

/* The structure used to pass unique initialization info to each thread */
typedef struct {
    int pid;
    int tid;
    int sd;
} proc_info;

/* Structure used to pass information to the thread responsible for
 * creating the rest of the threads.
 */
typedef struct {
    kuda_thread_t **threads;
    kuda_thread_t *listener;
    int child_num_arg;
    kuda_threadattr_t *threadattr;
} thread_starter;

#define ID_FROM_CHILD_THREAD(c, t)    ((c * thread_limit) + t)

/* The worker cLMP respects a couple of runtime flags that can aid
 * in debugging. Setting the -DNO_DETACH flag will prevent the root process
 * from detaching from its controlling terminal. Additionally, setting
 * the -DONE_PROCESS flag (which implies -DNO_DETACH) will get you the
 * child_main loop running in the process which originally started up.
 * This gives you a pretty nice debugging environment.  (You'll get a SIGHUP
 * early in standalone_main; just continue through.  This is the server
 * trying to kill off any child processes which it might have lying
 * around --- cLHy doesn't keep track of their pids, it just sends
 * SIGHUP to the process group, ignoring it in the root process.
 * Continue through and you'll be fine.).
 */

static int one_process = 0;

#ifdef DEBUG_SIGSTOP
int raise_sigstop_flags;
#endif

static kuda_pool_t *pconf;                 /* Pool for config stuff */
static kuda_pool_t *pchild;                /* Pool for wwhy child stuff */
static kuda_pool_t *pruntime;              /* Pool for cLMP threads stuff */

static pid_t clhy_my_pid; /* Linux getpid() doesn't work except in main
                           thread. Use this instead */
static pid_t parent_pid;
static kuda_platform_thread_t *listener_platform_thread;

#ifdef SINGLE_LISTEN_UNSERIALIZED_ACCEPT
#define SAFE_ACCEPT(stmt) (clhy_listeners->next ? (stmt) : KUDA_SUCCESS)
#else
#define SAFE_ACCEPT(stmt) (stmt)
#endif

/* The LISTENER_SIGNAL signal will be sent from the main thread to the
 * listener thread to wake it up for graceful termination (what a child
 * process from an old generation does when the admin does "delmanserve
 * graceful").  This signal will be blocked in all threads of a child
 * process except for the listener thread.
 */
#define LISTENER_SIGNAL     SIGHUP

/* The WORKER_SIGNAL signal will be sent from the main thread to the
 * worker threads during an ungraceful restart or shutdown.
 * This ensures that on systems (i.e., Linux) where closing the worker
 * socket doesn't awake the worker thread when it is polling on the socket
 * (especially in kuda_wait_for_io_or_timeout() when handling
 * Keep-Alive connections), close_worker_sockets() and join_workers()
 * still function in timely manner and allow ungraceful shutdowns to
 * proceed to completion.  Otherwise join_workers() doesn't return
 * before the main process decides the child process is non-responsive
 * and sends a SIGKILL.
 */
#define WORKER_SIGNAL       CLHY_SIG_GRACEFUL

/* An array of socket descriptors in use by each thread used to
 * perform a non-graceful (forced) shutdown of the server. */
static kuda_socket_t **worker_sockets;

static void close_worker_sockets(void)
{
    int i;
    for (i = 0; i < threads_per_child; i++) {
        if (worker_sockets[i]) {
            kuda_socket_close(worker_sockets[i]);
            worker_sockets[i] = NULL;
        }
    }
}

static void wakeup_listener(void)
{
    listener_may_exit = 1;
    if (!listener_platform_thread) {
        /* XXX there is an obscure path that this doesn't handle perfectly:
         *     right after listener thread is created but before
         *     listener_platform_thread is set, the first worker thread hits an
         *     error and starts graceful termination
         */
        return;
    }

    /* unblock the listener if it's waiting for a worker */
    clhy_queue_info_term(worker_queue_info);

    /*
     * we should just be able to "kill(clhy_my_pid, LISTENER_SIGNAL)" on all
     * platforms and wake up the listener thread since it is the only thread
     * with SIGHUP unblocked, but that doesn't work on Linux
     */
#ifdef HAVE_PTHREAD_KILL
    pthread_kill(*listener_platform_thread, LISTENER_SIGNAL);
#else
    kill(clhy_my_pid, LISTENER_SIGNAL);
#endif
}

#define ST_INIT              0
#define ST_GRACEFUL          1
#define ST_UNGRACEFUL        2

static int terminate_mode = ST_INIT;

static void signal_threads(int mode)
{
    if (terminate_mode == mode) {
        return;
    }
    terminate_mode = mode;
    retained->clmp->clmp_state = CLHY_CLMPQ_STOPPING;

    /* in case we weren't called from the listener thread, wake up the
     * listener thread
     */
    wakeup_listener();

    /* for ungraceful termination, let the workers exit now;
     * for graceful termination, the listener thread will notify the
     * workers to exit once it has stopped accepting new connections
     */
    if (mode == ST_UNGRACEFUL) {
        workers_may_exit = 1;
        clhy_queue_interrupt_all(worker_queue);
        close_worker_sockets(); /* forcefully kill all current connections */
    }
}

static int worker_query(int query_code, int *result, kuda_status_t *rv)
{
    *rv = KUDA_SUCCESS;
    switch (query_code) {
        case CLHY_CLMPQ_MAX_DAEMON_USED:
            *result = retained->max_daemons_limit;
            break;
        case CLHY_CLMPQ_IS_THREADED:
            *result = CLHY_CLMPQ_STATIC;
            break;
        case CLHY_CLMPQ_IS_FORKED:
            *result = CLHY_CLMPQ_DYNAMIC;
            break;
        case CLHY_CLMPQ_HARD_LIMIT_DAEMONS:
            *result = server_limit;
            break;
        case CLHY_CLMPQ_HARD_LIMIT_THREADS:
            *result = thread_limit;
            break;
        case CLHY_CLMPQ_MAX_THREADS:
            *result = threads_per_child;
            break;
        case CLHY_CLMPQ_MIN_SPARE_DAEMONS:
            *result = 0;
            break;
        case CLHY_CLMPQ_MIN_SPARE_THREADS:
            *result = min_spare_threads;
            break;
        case CLHY_CLMPQ_MAX_SPARE_DAEMONS:
            *result = 0;
            break;
        case CLHY_CLMPQ_MAX_SPARE_THREADS:
            *result = max_spare_threads;
            break;
        case CLHY_CLMPQ_MAX_REQUESTS_DAEMON:
            *result = clhy_max_requests_per_child;
            break;
        case CLHY_CLMPQ_MAX_DAEMONS:
            *result = clhy_daemons_limit;
            break;
        case CLHY_CLMPQ_CLMP_STATE:
            *result = retained->clmp->clmp_state;
            break;
        case CLHY_CLMPQ_GENERATION:
            *result = retained->clmp->my_generation;
            break;
        default:
            *rv = KUDA_ENOTIMPL;
            break;
    }
    return OK;
}

static void worker_note_child_killed(int childnum, pid_t pid, clhy_generation_t gen)
{
    if (childnum != -1) { /* child had a scoreboard slot? */
        clhy_run_child_status(clhy_server_conf,
                            clhy_scoreboard_image->parent[childnum].pid,
                            clhy_scoreboard_image->parent[childnum].generation,
                            childnum, CLMP_CHILD_EXITED);
        clhy_scoreboard_image->parent[childnum].pid = 0;
    }
    else {
        clhy_run_child_status(clhy_server_conf, pid, gen, -1, CLMP_CHILD_EXITED);
    }
}

static void worker_note_child_started(int slot, pid_t pid)
{
    clhy_generation_t gen = retained->clmp->my_generation;
    clhy_scoreboard_image->parent[slot].pid = pid;
    clhy_scoreboard_image->parent[slot].generation = gen;
    clhy_run_child_status(clhy_server_conf, pid, gen, slot, CLMP_CHILD_STARTED);
}

static void worker_note_child_lost_slot(int slot, pid_t newpid)
{
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(00263)
                 "pid %" KUDA_PID_T_FMT " taking over scoreboard slot from "
                 "%" KUDA_PID_T_FMT "%s",
                 newpid,
                 clhy_scoreboard_image->parent[slot].pid,
                 clhy_scoreboard_image->parent[slot].quiescing ?
                 " (quiescing)" : "");
    clhy_run_child_status(clhy_server_conf,
                        clhy_scoreboard_image->parent[slot].pid,
                        clhy_scoreboard_image->parent[slot].generation,
                        slot, CLMP_CHILD_LOST_SLOT);
    /* Don't forget about this exiting child process, or we
     * won't be able to kill it if it doesn't exit by the
     * time the server is shut down.
     */
    clhy_register_extra_clmp_process(clhy_scoreboard_image->parent[slot].pid,
                                  clhy_scoreboard_image->parent[slot].generation);
}

static const char *worker_get_name(void)
{
    return "worker";
}

/* a clean exit from a child with proper cleanup */
static void clean_child_exit(int code) __attribute__ ((noreturn));
static void clean_child_exit(int code)
{
    retained->clmp->clmp_state = CLHY_CLMPQ_STOPPING;
    if (pchild) {
        kuda_pool_destroy(pchild);
    }

    if (one_process) {
        worker_note_child_killed(/* slot */ 0, 0, 0);
    }

    exit(code);
}

static void just_die(int sig)
{
    clean_child_exit(0);
}

/*****************************************************************
 * Connection structures and accounting...
 */

static int child_fatal;

/*****************************************************************
 * Here follows a long bunch of generic server bookkeeping stuff...
 */

/*****************************************************************
 * Child process main loop.
 */

static void process_socket(kuda_thread_t *thd, kuda_pool_t *p, kuda_socket_t *sock,
                           int my_child_num,
                           int my_thread_num, kuda_bucket_alloc_t *bucket_alloc)
{
    conn_rec *current_conn;
    long conn_id = ID_FROM_CHILD_THREAD(my_child_num, my_thread_num);
    clhy_sb_handle_t *sbh;

    clhy_create_sb_handle(&sbh, p, my_child_num, my_thread_num);

    current_conn = clhy_run_create_connection(p, clhy_server_conf, sock,
                                            conn_id, sbh, bucket_alloc);
    if (current_conn) {
        current_conn->current_thread = thd;
        clhy_process_connection(current_conn, sock);
        clhy_lingering_close(current_conn);
    }
}

/* requests_this_child has gone to zero or below.  See if the admin coded
   "MaxConnectionsPerChild 0", and keep going in that case.  Doing it this way
   simplifies the hot path in worker_thread */
static void check_infinite_requests(void)
{
    if (clhy_max_requests_per_child) {
        signal_threads(ST_GRACEFUL);
    }
    else {
        requests_this_child = INT_MAX;      /* keep going */
    }
}

static void unblock_signal(int sig)
{
    sigset_t sig_mask;

    sigemptyset(&sig_mask);
    sigaddset(&sig_mask, sig);
#if defined(SIGPROCMASK_SETS_THREAD_MASK)
    sigprocmask(SIG_UNBLOCK, &sig_mask, NULL);
#else
    pthread_sigmask(SIG_UNBLOCK, &sig_mask, NULL);
#endif
}

static void dummy_signal_handler(int sig)
{
    /* XXX If specifying SIG_IGN is guaranteed to unblock a syscall,
     *     then we don't need this goofy function.
     */
}

static void accept_mutex_error(const char *func, kuda_status_t rv, int process_slot)
{
    int level = CLHYLOG_EMERG;

    if (clhy_scoreboard_image->parent[process_slot].generation !=
        clhy_scoreboard_image->global->running_generation) {
        level = CLHYLOG_DEBUG; /* common to get these at restart time */
    }
    else if (requests_this_child == INT_MAX
        || ((requests_this_child == clhy_max_requests_per_child)
            && clhy_max_requests_per_child)) {
        clhy_log_error(CLHYLOG_MARK, level, rv, clhy_server_conf, CLHYLOGNO(00272)
                     "kuda_proc_mutex_%s failed "
                     "before this child process served any requests.",
                     func);
        clean_child_exit(APEXIT_CHILDSICK);
    }
    clhy_log_error(CLHYLOG_MARK, level, rv, clhy_server_conf, CLHYLOGNO(00273)
                 "kuda_proc_mutex_%s failed. Attempting to "
                 "shutdown process gracefully.", func);
    signal_threads(ST_GRACEFUL);
}

static void * KUDA_THREAD_FUNC listener_thread(kuda_thread_t *thd, void * dummy)
{
    proc_info * ti = dummy;
    int process_slot = ti->pid;
    void *csd = NULL;
    kuda_pool_t *ptrans = NULL;            /* Pool for per-transaction stuff */
    kuda_status_t rv;
    clhy_listen_rec *lr = NULL;
    int have_idle_worker = 0;
    int last_poll_idx = 0;

    free(ti);

    /* Unblock the signal used to wake this thread up, and set a handler for
     * it.
     */
    unblock_signal(LISTENER_SIGNAL);
    kuda_signal(LISTENER_SIGNAL, dummy_signal_handler);

    /* TODO: Switch to a system where threads reuse the results from earlier
       poll calls - manoj */
    while (1) {
        /* TODO: requests_this_child should be synchronized - aaron */
        if (requests_this_child <= 0) {
            check_infinite_requests();
        }
        if (listener_may_exit) break;

        if (!have_idle_worker) {
            rv = clhy_queue_info_wait_for_idler(worker_queue_info, NULL);
            if (KUDA_STATUS_IS_EOF(rv)) {
                break; /* we've been signaled to die now */
            }
            else if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, clhy_server_conf,
                             "kuda_queue_info_wait failed. Attempting to "
                             " shutdown process gracefully.");
                signal_threads(ST_GRACEFUL);
                break;
            }
            have_idle_worker = 1;
        }

        /* We've already decremented the idle worker count inside
         * clhy_queue_info_wait_for_idler. */

        if ((rv = SAFE_ACCEPT(kuda_proc_mutex_lock(my_bucket->mutex)))
            != KUDA_SUCCESS) {

            if (!listener_may_exit) {
                accept_mutex_error("lock", rv, process_slot);
            }
            break;                    /* skip the lock release */
        }

        if (!my_bucket->listeners->next) {
            /* Only one listener, so skip the poll */
            lr = my_bucket->listeners;
        }
        else {
            while (!listener_may_exit) {
                kuda_int32_t numdesc;
                const kuda_pollfd_t *pdesc;

                rv = kuda_pollset_poll(worker_pollset, -1, &numdesc, &pdesc);
                if (rv != KUDA_SUCCESS) {
                    if (KUDA_STATUS_IS_EINTR(rv)) {
                        continue;
                    }

                    /* kuda_pollset_poll() will only return errors in catastrophic
                     * circumstances. Let's try exiting gracefully, for now. */
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, clhy_server_conf, CLHYLOGNO(03137)
                                 "kuda_pollset_poll: (listen)");
                    signal_threads(ST_GRACEFUL);
                }

                if (listener_may_exit) break;

                /* We can always use pdesc[0], but sockets at position N
                 * could end up completely starved of attention in a very
                 * busy server. Therefore, we round-robin across the
                 * returned set of descriptors. While it is possible that
                 * the returned set of descriptors might flip around and
                 * continue to starve some sockets, we happen to know the
                 * internal pollset implementation retains ordering
                 * stability of the sockets. Thus, the round-robin should
                 * ensure that a socket will eventually be serviced.
                 */
                if (last_poll_idx >= numdesc)
                    last_poll_idx = 0;

                /* Grab a listener record from the client_data of the poll
                 * descriptor, and advance our saved index to round-robin
                 * the next fetch.
                 *
                 * ### hmm... this descriptor might have POLLERR rather
                 * ### than POLLIN
                 */
                lr = pdesc[last_poll_idx++].client_data;
                break;

            } /* while */

        } /* if/else */

        if (!listener_may_exit) {
            /* the following pops a recycled ptrans pool off a stack */
            clhy_queue_info_pop_pool(worker_queue_info, &ptrans);
            if (ptrans == NULL) {
                /* we can't use a recycled transaction pool this time.
                 * create a new transaction pool */
                kuda_allocator_t *allocator;

                kuda_allocator_create(&allocator);
                kuda_allocator_max_free_set(allocator, clhy_max_mem_free);
                kuda_pool_create_ex(&ptrans, pconf, NULL, allocator);
                kuda_allocator_owner_set(allocator, ptrans);
                kuda_pool_tag(ptrans, "transaction");
            }
            rv = lr->accept_func(&csd, lr, ptrans);
            /* later we trash rv and rely on csd to indicate success/failure */
            CLHY_DEBUG_ASSERT(rv == KUDA_SUCCESS || !csd);

            if (rv == KUDA_EGENERAL) {
                /* E[NM]FILE, ENOMEM, etc */
                resource_shortage = 1;
                signal_threads(ST_GRACEFUL);
            }
            if ((rv = SAFE_ACCEPT(kuda_proc_mutex_unlock(my_bucket->mutex)))
                != KUDA_SUCCESS) {

                if (listener_may_exit) {
                    break;
                }
                accept_mutex_error("unlock", rv, process_slot);
            }
            if (csd != NULL) {
                rv = clhy_queue_push_socket(worker_queue, csd, NULL, ptrans);
                if (rv) {
                    /* trash the connection; we couldn't queue the connected
                     * socket to a worker
                     */
                    kuda_socket_close(csd);
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, clhy_server_conf, CLHYLOGNO(03138)
                                 "clhy_queue_push_socket failed");
                }
                else {
                    have_idle_worker = 0;
                }
            }
        }
        else {
            if ((rv = SAFE_ACCEPT(kuda_proc_mutex_unlock(my_bucket->mutex)))
                != KUDA_SUCCESS) {
                int level = CLHYLOG_EMERG;

                if (clhy_scoreboard_image->parent[process_slot].generation !=
                    clhy_scoreboard_image->global->running_generation) {
                    level = CLHYLOG_DEBUG; /* common to get these at restart time */
                }
                clhy_log_error(CLHYLOG_MARK, level, rv, clhy_server_conf, CLHYLOGNO(00274)
                             "kuda_proc_mutex_unlock failed. Attempting to "
                             "shutdown process gracefully.");
                signal_threads(ST_GRACEFUL);
            }
            break;
        }
    }

    clhy_close_listeners_ex(my_bucket->listeners);
    clhy_queue_info_free_idle_pools(worker_queue_info);
    clhy_queue_term(worker_queue);
    dying = 1;
    clhy_scoreboard_image->parent[process_slot].quiescing = 1;

    /* wake up the main thread */
    kill(clhy_my_pid, SIGTERM);

    kuda_thread_exit(thd, KUDA_SUCCESS);
    return NULL;
}

/* XXX For ungraceful termination/restart, we definitely don't want to
 *     wait for active connections to finish but we may want to wait
 *     for idle workers to get out of the queue code and release mutexes,
 *     since those mutexes are cleaned up pretty soon and some systems
 *     may not react favorably (i.e., segfault) if operations are attempted
 *     on cleaned-up mutexes.
 */
static void * KUDA_THREAD_FUNC worker_thread(kuda_thread_t *thd, void * dummy)
{
    proc_info * ti = dummy;
    int process_slot = ti->pid;
    int thread_slot = ti->tid;
    kuda_socket_t *csd = NULL;
    kuda_bucket_alloc_t *bucket_alloc;
    kuda_pool_t *last_ptrans = NULL;
    kuda_pool_t *ptrans;                /* Pool for per-transaction stuff */
    kuda_status_t rv;
    int is_idle = 0;

    free(ti);

    clhy_scoreboard_image->servers[process_slot][thread_slot].pid = clhy_my_pid;
    clhy_scoreboard_image->servers[process_slot][thread_slot].tid = kuda_platform_thread_current();
    clhy_scoreboard_image->servers[process_slot][thread_slot].generation = retained->clmp->my_generation;
    clhy_update_child_status_from_indexes(process_slot, thread_slot,
                                        SERVER_STARTING, NULL);

#ifdef HAVE_PTHREAD_KILL
    unblock_signal(WORKER_SIGNAL);
    kuda_signal(WORKER_SIGNAL, dummy_signal_handler);
#endif

    while (!workers_may_exit) {
        if (!is_idle) {
            rv = clhy_queue_info_set_idle(worker_queue_info, last_ptrans);
            last_ptrans = NULL;
            if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, clhy_server_conf,
                             "clhy_queue_info_set_idle failed. Attempting to "
                             "shutdown process gracefully.");
                signal_threads(ST_GRACEFUL);
                break;
            }
            is_idle = 1;
        }

        clhy_update_child_status_from_indexes(process_slot, thread_slot,
                                            SERVER_READY, NULL);
worker_pop:
        if (workers_may_exit) {
            break;
        }
        rv = clhy_queue_pop_socket(worker_queue, &csd, &ptrans);

        if (rv != KUDA_SUCCESS) {
            /* We get KUDA_EOF during a graceful shutdown once all the connections
             * accepted by this server process have been handled.
             */
            if (KUDA_STATUS_IS_EOF(rv)) {
                break;
            }
            /* We get KUDA_EINTR whenever clhy_queue_pop_*() has been interrupted
             * from an explicit call to clhy_queue_interrupt_all(). This allows
             * us to unblock threads stuck in clhy_queue_pop_*() when a shutdown
             * is pending.
             *
             * If workers_may_exit is set and this is ungraceful termination/
             * restart, we are bound to get an error on some systems (e.g.,
             * AIX, which sanity-checks mutex operations) since the queue
             * may have already been cleaned up.  Don't log the "error" if
             * workers_may_exit is set.
             */
            else if (KUDA_STATUS_IS_EINTR(rv)) {
                goto worker_pop;
            }
            /* We got some other error. */
            else if (!workers_may_exit) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, clhy_server_conf, CLHYLOGNO(03139)
                             "clhy_queue_pop_socket failed");
            }
            continue;
        }
        is_idle = 0;
        worker_sockets[thread_slot] = csd;
        bucket_alloc = kuda_bucket_alloc_create(ptrans);
        process_socket(thd, ptrans, csd, process_slot, thread_slot, bucket_alloc);
        worker_sockets[thread_slot] = NULL;
        requests_this_child--;
        kuda_pool_clear(ptrans);
        last_ptrans = ptrans;
    }

    clhy_update_child_status_from_indexes(process_slot, thread_slot,
                                        dying ? SERVER_DEAD
                                              : SERVER_GRACEFUL, NULL);

    kuda_thread_exit(thd, KUDA_SUCCESS);
    return NULL;
}

static int check_signal(int signum)
{
    switch (signum) {
    case SIGTERM:
    case SIGINT:
        return 1;
    }
    return 0;
}

static void create_listener_thread(thread_starter *ts)
{
    int my_child_num = ts->child_num_arg;
    kuda_threadattr_t *thread_attr = ts->threadattr;
    proc_info *my_info;
    kuda_status_t rv;

    my_info = (proc_info *)clhy_malloc(sizeof(proc_info));
    my_info->pid = my_child_num;
    my_info->tid = -1; /* listener thread doesn't have a thread slot */
    my_info->sd = 0;
    rv = kuda_thread_create(&ts->listener, thread_attr, listener_thread,
                           my_info, pruntime);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, rv, clhy_server_conf, CLHYLOGNO(00275)
                     "kuda_thread_create: unable to create listener thread");
        /* let the parent decide how bad this really is */
        clean_child_exit(APEXIT_CHILDSICK);
    }
    kuda_platform_thread_get(&listener_platform_thread, ts->listener);
}

static void setup_threads_runtime(void)
{
    clhy_listen_rec *lr;
    kuda_status_t rv;

    /* All threads (listener, workers) and synchronization objects (queues,
     * pollset, mutexes...) created here should have at least the lifetime of
     * the connections they handle (i.e. ptrans). We can't use this thread's
     * self pool because all these objects survive it, nor use pchild or pconf
     * directly because this starter thread races with other cAPIs' runtime,
     * nor finally pchild (or subpool thereof) because it is killed explicitely
     * before pconf (thus connections/ptrans can live longer, which matters in
     * ONE_PROCESS mode). So this leaves us with a subpool of pconf, created
     * before any ptrans hence destroyed after.
     */
    kuda_pool_create(&pruntime, pconf);
    kuda_pool_tag(pruntime, "clmp_runtime");

    /* We must create the fd queues before we start up the listener
     * and worker threads. */
    rv = clhy_queue_create(&worker_queue, threads_per_child, pruntime);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, rv, clhy_server_conf, CLHYLOGNO(03140)
                     "clhy_queue_create() failed");
        clean_child_exit(APEXIT_CHILDFATAL);
    }

    rv = clhy_queue_info_create(&worker_queue_info, pruntime,
                              threads_per_child, -1);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, rv, clhy_server_conf, CLHYLOGNO(03141)
                     "clhy_queue_info_create() failed");
        clean_child_exit(APEXIT_CHILDFATAL);
    }

    /* Create the main pollset */
    rv = kuda_pollset_create(&worker_pollset, num_listensocks, pruntime,
                            KUDA_POLLSET_NOCOPY);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, clhy_server_conf, CLHYLOGNO(03285)
                     "Couldn't create pollset in thread;"
                     " check system or user limits");
        /* let the parent decide how bad this really is */
        clean_child_exit(APEXIT_CHILDSICK);
    }

    for (lr = my_bucket->listeners; lr != NULL; lr = lr->next) {
        kuda_pollfd_t *pfd = kuda_pcalloc(pruntime, sizeof *pfd);

        pfd->desc_type = KUDA_POLL_SOCKET;
        pfd->desc.s = lr->sd;
        pfd->reqevents = KUDA_POLLIN;
        pfd->client_data = lr;

        rv = kuda_pollset_add(worker_pollset, pfd);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, clhy_server_conf, CLHYLOGNO(03286)
                         "Couldn't create add listener to pollset;"
                         " check system or user limits");
            /* let the parent decide how bad this really is */
            clean_child_exit(APEXIT_CHILDSICK);
        }

        lr->accept_func = clhy_unixd_accept;
    }

    worker_sockets = kuda_pcalloc(pruntime, threads_per_child *
                                           sizeof(kuda_socket_t *));
}

/* XXX under some circumstances not understood, children can get stuck
 *     in start_threads forever trying to take over slots which will
 *     never be cleaned up; for now there is an CLHYLOG_DEBUG message issued
 *     every so often when this condition occurs
 */
static void * KUDA_THREAD_FUNC start_threads(kuda_thread_t *thd, void *dummy)
{
    thread_starter *ts = dummy;
    kuda_thread_t **threads = ts->threads;
    kuda_threadattr_t *thread_attr = ts->threadattr;
    int my_child_num = ts->child_num_arg;
    proc_info *my_info;
    kuda_status_t rv;
    int threads_created = 0;
    int listener_started = 0;
    int prev_threads_created;
    int loops, i;

    loops = prev_threads_created = 0;
    while (1) {
        /* threads_per_child does not include the listener thread */
        for (i = 0; i < threads_per_child; i++) {
            int status = clhy_scoreboard_image->servers[my_child_num][i].status;

            if (status != SERVER_GRACEFUL && status != SERVER_DEAD) {
                continue;
            }

            my_info = (proc_info *)clhy_malloc(sizeof(proc_info));
            my_info->pid = my_child_num;
            my_info->tid = i;
            my_info->sd = 0;

            /* We are creating threads right now */
            clhy_update_child_status_from_indexes(my_child_num, i,
                                                SERVER_STARTING, NULL);
            /* We let each thread update its own scoreboard entry.  This is
             * done because it lets us deal with tid better.
             */
            rv = kuda_thread_create(&threads[i], thread_attr,
                                   worker_thread, my_info, pruntime);
            if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, rv, clhy_server_conf, CLHYLOGNO(03142)
                             "kuda_thread_create: unable to create worker thread");
                /* let the parent decide how bad this really is */
                clean_child_exit(APEXIT_CHILDSICK);
            }
            threads_created++;
        }
        /* Start the listener only when there are workers available */
        if (!listener_started && threads_created) {
            create_listener_thread(ts);
            listener_started = 1;
        }
        if (start_thread_may_exit || threads_created == threads_per_child) {
            break;
        }
        /* wait for previous generation to clean up an entry */
        kuda_sleep(kuda_time_from_sec(1));
        ++loops;
        if (loops % 120 == 0) { /* every couple of minutes */
            if (prev_threads_created == threads_created) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf,
                             "child %" KUDA_PID_T_FMT " isn't taking over "
                             "slots very quickly (%d of %d)",
                             clhy_my_pid, threads_created, threads_per_child);
            }
            prev_threads_created = threads_created;
        }
    }

    /* What state should this child_main process be listed as in the
     * scoreboard...?
     *  clhy_update_child_status_from_indexes(my_child_num, i, SERVER_STARTING,
     *                                      (request_rec *) NULL);
     *
     *  This state should be listed separately in the scoreboard, in some kind
     *  of process_status, not mixed in with the worker threads' status.
     *  "life_status" is almost right, but it's in the worker's structure, and
     *  the name could be clearer.   gla
     */
    kuda_thread_exit(thd, KUDA_SUCCESS);
    return NULL;
}

static void join_workers(kuda_thread_t *listener, kuda_thread_t **threads,
                         int mode)
{
    int i;
    kuda_status_t rv, thread_rv;

    if (listener) {
        int iter;

        /* deal with a rare timing window which affects waking up the
         * listener thread...  if the signal sent to the listener thread
         * is delivered between the time it verifies that the
         * listener_may_exit flag is clear and the time it enters a
         * blocking syscall, the signal didn't do any good...  work around
         * that by sleeping briefly and sending it again
         */

        iter = 0;
        while (iter < 10 &&
#ifdef HAVE_PTHREAD_KILL
               pthread_kill(*listener_platform_thread, 0)
#else
               kill(clhy_my_pid, 0)
#endif
               == 0) {
            /* listener not dead yet */
            kuda_sleep(kuda_time_make(0, 500000));
            wakeup_listener();
            ++iter;
        }
        if (iter >= 10) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(00276)
                         "the listener thread didn't exit");
        }
        else {
            rv = kuda_thread_join(&thread_rv, listener);
            if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, clhy_server_conf, CLHYLOGNO(00277)
                             "kuda_thread_join: unable to join listener thread");
            }
        }
    }

    for (i = 0; i < threads_per_child; i++) {
        if (threads[i]) { /* if we ever created this thread */
            if (mode != ST_GRACEFUL) {
#ifdef HAVE_PTHREAD_KILL
                kuda_platform_thread_t *worker_platform_thread;

                kuda_platform_thread_get(&worker_platform_thread, threads[i]);
                pthread_kill(*worker_platform_thread, WORKER_SIGNAL);
#endif
            }

            rv = kuda_thread_join(&thread_rv, threads[i]);
            if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, clhy_server_conf, CLHYLOGNO(00278)
                             "kuda_thread_join: unable to join worker "
                             "thread %d",
                             i);
            }
        }
    }
}

static void join_start_thread(kuda_thread_t *start_thread_id)
{
    kuda_status_t rv, thread_rv;

    start_thread_may_exit = 1; /* tell it to give up in case it is still
                                * trying to take over slots from a
                                * previous generation
                                */
    rv = kuda_thread_join(&thread_rv, start_thread_id);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, clhy_server_conf, CLHYLOGNO(00279)
                     "kuda_thread_join: unable to join the start "
                     "thread");
    }
}

static void child_main(int child_num_arg, int child_bucket)
{
    kuda_thread_t **threads;
    kuda_status_t rv;
    thread_starter *ts;
    kuda_threadattr_t *thread_attr;
    kuda_thread_t *start_thread_id;
    int i;

    /* for benefit of any hooks that run as this child initializes */
    retained->clmp->clmp_state = CLHY_CLMPQ_STARTING;

    clhy_my_pid = getpid();
    clhy_fatal_signal_child_setup(clhy_server_conf);

    /* Get a sub context for global allocations in this child, so that
     * we can have cleanups occur when the child exits.
     */
    kuda_pool_create(&pchild, pconf);
    kuda_pool_tag(pchild, "pchild");

    /* close unused listeners and pods */
    for (i = 0; i < retained->clmp->num_buckets; i++) {
        if (i != child_bucket) {
            clhy_close_listeners_ex(all_buckets[i].listeners);
            clhy_clmp_podx_close(all_buckets[i].pod);
        }
    }

    /*stuff to do before we switch id's, so we have permissions.*/
    clhy_reopen_scoreboard(pchild, NULL, 0);

    rv = SAFE_ACCEPT(kuda_proc_mutex_child_init(&my_bucket->mutex,
                                    kuda_proc_mutex_lockfile(my_bucket->mutex),
                                    pchild));
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, clhy_server_conf, CLHYLOGNO(00280)
                     "Couldn't initialize cross-process lock in child");
        clean_child_exit(APEXIT_CHILDFATAL);
    }

    /* done with init critical section */
    if (clhy_run_drop_privileges(pchild, clhy_server_conf)) {
        clean_child_exit(APEXIT_CHILDFATAL);
    }

    /* Just use the standard kuda_setup_signal_thread to block all signals
     * from being received.  The child processes no longer use signals for
     * any communication with the parent process. Let's also do this before
     * child_init() hooks are called and possibly create threads that
     * otherwise could "steal" (implicitely) CLMP's signals.
     */
    rv = kuda_setup_signal_thread();
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, clhy_server_conf, CLHYLOGNO(00281)
                     "Couldn't initialize signal thread");
        clean_child_exit(APEXIT_CHILDFATAL);
    }

    clhy_run_child_init(pchild, clhy_server_conf);

    if (clhy_max_requests_per_child) {
        requests_this_child = clhy_max_requests_per_child;
    }
    else {
        /* coding a value of zero means infinity */
        requests_this_child = INT_MAX;
    }

    /* Setup threads */

    /* Globals used by signal_threads() so to be initialized before */
    setup_threads_runtime();

    /* clear the storage; we may not create all our threads immediately,
     * and we want a 0 entry to indicate a thread which was not created
     */
    threads = (kuda_thread_t **)clhy_calloc(1,
                                  sizeof(kuda_thread_t *) * threads_per_child);
    ts = (thread_starter *)kuda_palloc(pchild, sizeof(*ts));

    kuda_threadattr_create(&thread_attr, pchild);
    /* 0 means PTHREAD_CREATE_JOINABLE */
    kuda_threadattr_detach_set(thread_attr, 0);

    if (clhy_thread_stacksize != 0) {
        rv = kuda_threadattr_stacksize_set(thread_attr, clhy_thread_stacksize);
        if (rv != KUDA_SUCCESS && rv != KUDA_ENOTIMPL) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rv, clhy_server_conf, CLHYLOGNO(02435)
                         "WARNING: ThreadStackSize of %" KUDA_SIZE_T_FMT " is "
                         "inappropriate, using default", 
                         clhy_thread_stacksize);
        }
    }

    ts->threads = threads;
    ts->listener = NULL;
    ts->child_num_arg = child_num_arg;
    ts->threadattr = thread_attr;

    rv = kuda_thread_create(&start_thread_id, thread_attr, start_threads,
                           ts, pchild);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, rv, clhy_server_conf, CLHYLOGNO(00282)
                     "kuda_thread_create: unable to create worker thread");
        /* let the parent decide how bad this really is */
        clean_child_exit(APEXIT_CHILDSICK);
    }

    retained->clmp->clmp_state = CLHY_CLMPQ_RUNNING;

    /* If we are only running in one_process mode, we will want to
     * still handle signals. */
    if (one_process) {
        /* Block until we get a terminating signal. */
        kuda_signal_thread(check_signal);
        /* make sure the start thread has finished; signal_threads()
         * and join_workers() depend on that
         */
        /* XXX join_start_thread() won't be awakened if one of our
         *     threads encounters a critical error and attempts to
         *     shutdown this child
         */
        join_start_thread(start_thread_id);
        signal_threads(ST_UNGRACEFUL); /* helps us terminate a little more
                           * quickly than the dispatch of the signal thread
                           * beats the Pipe of Death and the browsers
                           */
        /* A terminating signal was received. Now join each of the
         * workers to clean them up.
         *   If the worker already exited, then the join frees
         *   their resources and returns.
         *   If the worker hasn't exited, then this blocks until
         *   they have (then cleans up).
         */
        join_workers(ts->listener, threads, ST_UNGRACEFUL);
    }
    else { /* !one_process */
        /* remove SIGTERM from the set of blocked signals...  if one of
         * the other threads in the process needs to take us down
         * (e.g., for MaxConnectionsPerChild) it will send us SIGTERM
         */
        unblock_signal(SIGTERM);
        kuda_signal(SIGTERM, dummy_signal_handler);
        /* Watch for any messages from the parent over the POD */
        while (1) {
            rv = clhy_clmp_podx_check(my_bucket->pod);
            if (rv == CLHY_CLMP_PODX_NORESTART) {
                /* see if termination was triggered while we slept */
                switch(terminate_mode) {
                case ST_GRACEFUL:
                    rv = CLHY_CLMP_PODX_GRACEFUL;
                    break;
                case ST_UNGRACEFUL:
                    rv = CLHY_CLMP_PODX_RESTART;
                    break;
                }
            }
            if (rv == CLHY_CLMP_PODX_GRACEFUL || rv == CLHY_CLMP_PODX_RESTART) {
                /* make sure the start thread has finished;
                 * signal_threads() and join_workers depend on that
                 */
                join_start_thread(start_thread_id);
                signal_threads(rv == CLHY_CLMP_PODX_GRACEFUL ? ST_GRACEFUL : ST_UNGRACEFUL);
                break;
            }
        }

        /* A terminating signal was received. Now join each of the
         * workers to clean them up.
         *   If the worker already exited, then the join frees
         *   their resources and returns.
         *   If the worker hasn't exited, then this blocks until
         *   they have (then cleans up).
         */
        join_workers(ts->listener, threads,
                     rv == CLHY_CLMP_PODX_GRACEFUL ? ST_GRACEFUL : ST_UNGRACEFUL);
    }

    free(threads);

    clean_child_exit(resource_shortage ? APEXIT_CHILDSICK : 0);
}

static int make_child(server_rec *s, int slot, int bucket)
{
    int pid;

    if (slot + 1 > retained->max_daemons_limit) {
        retained->max_daemons_limit = slot + 1;
    }

    if (one_process) {
        my_bucket = &all_buckets[0];

        worker_note_child_started(slot, getpid());
        child_main(slot, 0);
        /* NOTREACHED */
        clhy_assert(0);
        return -1;
    }

    if ((pid = fork()) == -1) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, errno, s, CLHYLOGNO(00283)
                     "fork: Unable to fork new process");
        /* fork didn't succeed.  There's no need to touch the scoreboard;
         * if we were trying to replace a failed child process, then
         * server_main_loop() marked its workers SERVER_DEAD, and if
         * we were trying to replace a child process that exited normally,
         * its worker_thread()s left SERVER_DEAD or SERVER_GRACEFUL behind.
         */

        /* In case system resources are maxxed out, we don't want
           cLHy running away with the CPU trying to fork over and
           over and over again. */
        kuda_sleep(kuda_time_from_sec(10));

        return -1;
    }

    if (!pid) {
        my_bucket = &all_buckets[bucket];

#ifdef HAVE_BINDPROCESSOR
        /* By default, AIX binds to a single processor.  This bit unbinds
         * children which will then bind to another CPU.
         */
        int status = bindprocessor(BINDPROCESS, (int)getpid(),
                               PROCESSOR_CLASS_ANY);
        if (status != OK)
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, errno,
                         clhy_server_conf, CLHYLOGNO(00284)
                         "processor unbind failed");
#endif
        RAISE_SIGSTOP(MAKE_CHILD);

        kuda_signal(SIGTERM, just_die);
        child_main(slot, bucket);
        /* NOTREACHED */
        clhy_assert(0);
        return -1;
    }

    if (clhy_scoreboard_image->parent[slot].pid != 0) {
        /* This new child process is squatting on the scoreboard
         * entry owned by an exiting child process, which cannot
         * exit until all active requests complete.
         */
        worker_note_child_lost_slot(slot, pid);
    }
    clhy_scoreboard_image->parent[slot].quiescing = 0;
    worker_note_child_started(slot, pid);
    return 0;
}

/* start up a bunch of children */
static void startup_children(int number_to_start)
{
    int i;

    for (i = 0; number_to_start && i < clhy_daemons_limit; ++i) {
        if (clhy_scoreboard_image->parent[i].pid != 0) {
            continue;
        }
        if (make_child(clhy_server_conf, i, i % retained->clmp->num_buckets) < 0) {
            break;
        }
        --number_to_start;
    }
}

static void perform_idle_server_maintenance(int child_bucket, int num_buckets)
{
    int i, j;
    int idle_thread_count;
    worker_score *ws;
    process_score *ps;
    int free_length;
    int totally_free_length = 0;
    int free_slots[MAX_SPAWN_RATE];
    int last_non_dead;
    int total_non_dead;
    int active_thread_count = 0;

    /* initialize the free_list */
    free_length = 0;

    idle_thread_count = 0;
    last_non_dead = -1;
    total_non_dead = 0;

    for (i = 0; i < clhy_daemons_limit; ++i) {
        /* Initialization to satisfy the compiler. It doesn't know
         * that threads_per_child is always > 0 */
        int status = SERVER_DEAD;
        int any_dying_threads = 0;
        int any_dead_threads = 0;
        int all_dead_threads = 1;
        int child_threads_active = 0;
        int bucket = i % num_buckets;

        if (i >= retained->max_daemons_limit &&
            totally_free_length == retained->idle_spawn_rate[child_bucket]) {
            /* short cut if all active processes have been examined and
             * enough empty scoreboard slots have been found
             */
            break;
        }
        ps = &clhy_scoreboard_image->parent[i];
        for (j = 0; j < threads_per_child; j++) {
            ws = &clhy_scoreboard_image->servers[i][j];
            status = ws->status;

            /* XXX any_dying_threads is probably no longer needed    GLA */
            any_dying_threads = any_dying_threads ||
                                (status == SERVER_GRACEFUL);
            any_dead_threads = any_dead_threads || (status == SERVER_DEAD);
            all_dead_threads = all_dead_threads &&
                                   (status == SERVER_DEAD ||
                                    status == SERVER_GRACEFUL);

            /* We consider a starting server as idle because we started it
             * at least a cycle ago, and if it still hasn't finished starting
             * then we're just going to swamp things worse by forking more.
             * So we hopefully won't need to fork more if we count it.
             * This depends on the ordering of SERVER_READY and SERVER_STARTING.
             */
            if (ps->pid != 0) { /* XXX just set all_dead_threads in outer for
                                   loop if no pid?  not much else matters */
                if (status <= SERVER_READY &&
                        !ps->quiescing &&
                        ps->generation == retained->clmp->my_generation &&
                        bucket == child_bucket) {
                    ++idle_thread_count;
                }
                if (status >= SERVER_READY && status < SERVER_GRACEFUL) {
                    ++child_threads_active;
                }
            }
        }
        active_thread_count += child_threads_active;
        if (any_dead_threads
                && bucket == child_bucket
                && totally_free_length < retained->idle_spawn_rate[child_bucket]
                && free_length < MAX_SPAWN_RATE / num_buckets
                && (!ps->pid               /* no process in the slot */
                    || ps->quiescing)) {   /* or at least one is going away */
            if (all_dead_threads) {
                /* great! we prefer these, because the new process can
                 * start more threads sooner.  So prioritize this slot
                 * by putting it ahead of any slots with active threads.
                 *
                 * first, make room by moving a slot that's potentially still
                 * in use to the end of the array
                 */
                free_slots[free_length] = free_slots[totally_free_length];
                free_slots[totally_free_length++] = i;
            }
            else {
                /* slot is still in use - back of the bus
                 */
                free_slots[free_length] = i;
            }
            ++free_length;
        }
        else if (child_threads_active == threads_per_child) {
            had_healthy_child = 1;
        }
        /* XXX if (!ps->quiescing)     is probably more reliable  GLA */
        if (!any_dying_threads) {
            last_non_dead = i;
            ++total_non_dead;
        }
    }

    if (retained->sick_child_detected) {
        if (had_healthy_child) {
            /* Assume this is a transient error, even though it may not be.  Leave
             * the server up in case it is able to serve some requests or the
             * problem will be resolved.
             */
            retained->sick_child_detected = 0;
        }
        else {
            /* looks like a basket case, as no child ever fully initialized; give up.
             */
            retained->clmp->shutdown_pending = 1;
            child_fatal = 1;
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, 0,
                         clhy_server_conf, CLHYLOGNO(02325)
                         "A resource shortage or other unrecoverable failure "
                         "was encountered before any child process initialized "
                         "successfully... wwhy is exiting!");
            /* the child already logged the failure details */
            return;
        }
    }

    retained->max_daemons_limit = last_non_dead + 1;

    if (idle_thread_count > max_spare_threads / num_buckets) {
        /* Kill off one child */
        clhy_clmp_podx_signal(all_buckets[child_bucket].pod,
                           CLHY_CLMP_PODX_GRACEFUL);
        retained->idle_spawn_rate[child_bucket] = 1;
    }
    else if (idle_thread_count < min_spare_threads / num_buckets) {
        /* terminate the free list */
        if (free_length == 0) { /* scoreboard is full, can't fork */

            if (active_thread_count >= clhy_daemons_limit * threads_per_child) {
                /* no threads are "inactive" - starting, stopping, etc. */
                /* have we reached MaxRequestWorkers, or just getting close? */
                if (0 == idle_thread_count) {
                    if (!retained->maxclients_reported) {
                        /* only report this condition once */
                        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, clhy_server_conf, CLHYLOGNO(00286)
                                     "server reached MaxRequestWorkers "
                                     "setting, consider raising the "
                                     "MaxRequestWorkers setting");
                        retained->maxclients_reported = 1;
                    }
                } else {
                    if (!retained->near_maxclients_reported) {
                        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, clhy_server_conf, CLHYLOGNO(00287)
                                     "server is within MinSpareThreads of "
                                     "MaxRequestWorkers, consider raising the "
                                     "MaxRequestWorkers setting");
                        retained->near_maxclients_reported = 1;
                    }
                }
            }
            else {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0,
                             clhy_server_conf, CLHYLOGNO(00288)
                             "scoreboard is full, not at MaxRequestWorkers");
            }
            retained->idle_spawn_rate[child_bucket] = 1;
        }
        else {
            if (free_length > retained->idle_spawn_rate[child_bucket]) {
                free_length = retained->idle_spawn_rate[child_bucket];
            }
            if (retained->idle_spawn_rate[child_bucket] >= 8) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0,
                             clhy_server_conf, CLHYLOGNO(00289)
                             "server seems busy, (you may need "
                             "to increase StartServers, ThreadsPerChild "
                             "or Min/MaxSpareThreads), "
                             "spawning %d children, there are around %d idle "
                             "threads, and %d total children", free_length,
                             idle_thread_count, total_non_dead);
            }
            for (i = 0; i < free_length; ++i) {
                make_child(clhy_server_conf, free_slots[i], child_bucket);
            }
            /* the next time around we want to spawn twice as many if this
             * wasn't good enough, but not if we've just done a graceful
             */
            if (retained->hold_off_on_exponential_spawning) {
                --retained->hold_off_on_exponential_spawning;
            }
            else if (retained->idle_spawn_rate[child_bucket]
                     < MAX_SPAWN_RATE / num_buckets) {
                retained->idle_spawn_rate[child_bucket] *= 2;
            }
        }
    }
    else {
        retained->idle_spawn_rate[child_bucket] = 1;
    }
}

static void server_main_loop(int remaining_children_to_start, int num_buckets)
{
    clhy_generation_t old_gen;
    int child_slot;
    kuda_exit_why_e exitwhy;
    int status, processed_status;
    kuda_proc_t pid;
    int i;

    while (!retained->clmp->restart_pending && !retained->clmp->shutdown_pending) {
        clhy_wait_or_timeout(&exitwhy, &status, &pid, pconf, clhy_server_conf);

        if (pid.pid != -1) {
            processed_status = clhy_process_child_status(&pid, exitwhy, status);
            child_slot = clhy_find_child_by_pid(&pid);
            if (processed_status == APEXIT_CHILDFATAL) {
                /* fix race condition found in PR 39311
                 * A child created at the same time as a graceful happens 
                 * can find the lock missing and create a fatal error.
                 * It is not fatal for the last generation to be in this state.
                 */
                if (child_slot < 0
                    || clhy_get_scoreboard_process(child_slot)->generation
                       == retained->clmp->my_generation) {
                    retained->clmp->shutdown_pending = 1;
                    child_fatal = 1;
                    return;
                }
                else {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, clhy_server_conf, CLHYLOGNO(00290)
                                 "Ignoring fatal error in child of previous "
                                 "generation (pid %ld).",
                                 (long)pid.pid);
                    retained->sick_child_detected = 1;
                }
            }
            else if (processed_status == APEXIT_CHILDSICK) {
                /* tell perform_idle_server_maintenance to check into this
                 * on the next timer pop
                 */
                retained->sick_child_detected = 1;
            }
            /* non-fatal death... note that it's gone in the scoreboard. */
            if (child_slot >= 0) {
                process_score *ps;

                for (i = 0; i < threads_per_child; i++)
                    clhy_update_child_status_from_indexes(child_slot, i,
                                                        SERVER_DEAD, NULL);

                worker_note_child_killed(child_slot, 0, 0);
                ps = &clhy_scoreboard_image->parent[child_slot];
                ps->quiescing = 0;
                if (processed_status == APEXIT_CHILDSICK) {
                    /* resource shortage, minimize the fork rate */
                    retained->idle_spawn_rate[child_slot % num_buckets] = 1;
                }
                else if (remaining_children_to_start
                    && child_slot < clhy_daemons_limit) {
                    /* we're still doing a 1-for-1 replacement of dead
                     * children with new children
                     */
                    make_child(clhy_server_conf, child_slot,
                               child_slot % num_buckets);
                    --remaining_children_to_start;
                }
            }
            else if (clhy_unregister_extra_clmp_process(pid.pid, &old_gen) == 1) {
                worker_note_child_killed(-1, /* already out of the scoreboard */
                                         pid.pid, old_gen);
                if (processed_status == APEXIT_CHILDSICK
                    && old_gen == retained->clmp->my_generation) {
                    /* resource shortage, minimize the fork rate */
                    for (i = 0; i < num_buckets; i++) {
                        retained->idle_spawn_rate[i] = 1;
                    }
                }
#if KUDA_HAS_OTHER_CHILD
            }
            else if (kuda_proc_other_child_alert(&pid, KUDA_OC_REASON_DEATH,
                                                status) == 0) {
                /* handled */
#endif
            }
            else if (retained->clmp->was_graceful) {
                /* Great, we've probably just lost a slot in the
                 * scoreboard.  Somehow we don't know about this child.
                 */
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0,
                             clhy_server_conf, CLHYLOGNO(00291)
                             "long lost child came home! (pid %ld)",
                             (long)pid.pid);
            }
            /* Don't perform idle maintenance when a child dies,
             * only do it when there's a timeout.  Remember only a
             * finite number of children can die, and it's pretty
             * pathological for a lot to die suddenly.
             */
            continue;
        }
        else if (remaining_children_to_start) {
            /* we hit a 1 second timeout in which none of the previous
             * generation of children needed to be reaped... so assume
             * they're all done, and pick up the slack if any is left.
             */
            startup_children(remaining_children_to_start);
            remaining_children_to_start = 0;
            /* In any event we really shouldn't do the code below because
             * few of the servers we just started are in the IDLE state
             * yet, so we'd mistakenly create an extra server.
             */
            continue;
        }

        for (i = 0; i < num_buckets; i++) {
            perform_idle_server_maintenance(i, num_buckets);
        }
    }
}

static int worker_run(kuda_pool_t *_pconf, kuda_pool_t *plog, server_rec *s)
{
    int num_buckets = retained->clmp->num_buckets;
    int remaining_children_to_start;
    int i;

    clhy_log_pid(pconf, clhy_pid_fname);

    if (!retained->clmp->was_graceful) {
        if (clhy_run_pre_clmp(s->process->pool, SB_SHARED) != OK) {
            retained->clmp->clmp_state = CLHY_CLMPQ_STOPPING;
            return !OK;
        }
        /* fix the generation number in the global score; we just got a new,
         * cleared scoreboard
         */
        clhy_scoreboard_image->global->running_generation = retained->clmp->my_generation;
    }

    clhy_unixd_clmp_set_signals(pconf, one_process);

    /* Don't thrash since num_buckets depends on the
     * system and the number of online CPU cores...
     */
    if (clhy_daemons_limit < num_buckets)
        clhy_daemons_limit = num_buckets;
    if (clhy_daemons_to_start < num_buckets)
        clhy_daemons_to_start = num_buckets;
    /* We want to create as much children at a time as the number of buckets,
     * so to optimally accept connections (evenly distributed across buckets).
     * Thus min_spare_threads should at least maintain num_buckets children,
     * and max_spare_threads allow num_buckets more children w/o triggering
     * immediately (e.g. num_buckets idle threads margin, one per bucket).
     */
    if (min_spare_threads < threads_per_child * (num_buckets - 1) + num_buckets)
        min_spare_threads = threads_per_child * (num_buckets - 1) + num_buckets;
    if (max_spare_threads < min_spare_threads + (threads_per_child + 1) * num_buckets)
        max_spare_threads = min_spare_threads + (threads_per_child + 1) * num_buckets;

    /* If we're doing a graceful_restart then we're going to see a lot
     * of children exiting immediately when we get into the main loop
     * below (because we just sent them CLHY_SIG_GRACEFUL).  This happens pretty
     * rapidly... and for each one that exits we may start a new one, until
     * there are at least min_spare_threads idle threads, counting across
     * all children.  But we may be permitted to start more children than
     * that, so we'll just keep track of how many we're
     * supposed to start up without the 1 second penalty between each fork.
     */
    remaining_children_to_start = clhy_daemons_to_start;
    if (remaining_children_to_start > clhy_daemons_limit) {
        remaining_children_to_start = clhy_daemons_limit;
    }
    if (!retained->clmp->was_graceful) {
        startup_children(remaining_children_to_start);
        remaining_children_to_start = 0;
    }
    else {
        /* give the system some time to recover before kicking into
            * exponential mode */
        retained->hold_off_on_exponential_spawning = 10;
    }

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00292)
                "%s configured -- resuming normal operations",
                clhy_get_server_description());
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, clhy_server_conf, CLHYLOGNO(00293)
                "Server built: %s", clhy_get_server_built());
    clhy_log_command_line(plog, s);
    clhy_log_core_common(s);
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(00294)
                "Accept mutex: %s (default: %s)",
                (all_buckets[0].mutex)
                    ? kuda_proc_mutex_name(all_buckets[0].mutex)
                    : "none",
                kuda_proc_mutex_defname());
    retained->clmp->clmp_state = CLHY_CLMPQ_RUNNING;

    server_main_loop(remaining_children_to_start, num_buckets);
    retained->clmp->clmp_state = CLHY_CLMPQ_STOPPING;

    if (retained->clmp->shutdown_pending && retained->clmp->is_ungraceful) {
        /* Time to shut down:
         * Kill child processes, tell them to call child_exit, etc...
         */
        for (i = 0; i < num_buckets; i++) {
            clhy_clmp_podx_killpg(all_buckets[i].pod, clhy_daemons_limit,
                               CLHY_CLMP_PODX_RESTART);
        }
        clhy_reclaim_child_processes(1, /* Start with SIGTERM */
                                   worker_note_child_killed);

        if (!child_fatal) {
            /* cleanup pid file on normal shutdown */
            clhy_remove_pid(pconf, clhy_pid_fname);
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0,
                         clhy_server_conf, CLHYLOGNO(00295) "caught SIGTERM, shutting down");
        }
        return DONE;
    }

    if (retained->clmp->shutdown_pending) {
        /* Time to gracefully shut down:
         * Kill child processes, tell them to call child_exit, etc...
         */
        int active_children;
        int index;
        kuda_time_t cutoff = 0;

        /* Close our listeners, and then ask our children to do same */
        clhy_close_listeners();

        for (i = 0; i < num_buckets; i++) {
            clhy_clmp_podx_killpg(all_buckets[i].pod, clhy_daemons_limit,
                               CLHY_CLMP_PODX_GRACEFUL);
        }
        clhy_relieve_child_processes(worker_note_child_killed);

        if (!child_fatal) {
            /* cleanup pid file on normal shutdown */
            clhy_remove_pid(pconf, clhy_pid_fname);
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00296)
                         "caught " CLHY_SIG_GRACEFUL_STOP_STRING
                         ", shutting down gracefully");
        }

        if (clhy_graceful_shutdown_timeout) {
            cutoff = kuda_time_now() +
                     kuda_time_from_sec(clhy_graceful_shutdown_timeout);
        }

        /* Don't really exit until each child has finished */
        retained->clmp->shutdown_pending = 0;
        do {
            /* Pause for a second */
            kuda_sleep(kuda_time_from_sec(1));

            /* Relieve any children which have now exited */
            clhy_relieve_child_processes(worker_note_child_killed);

            active_children = 0;
            for (index = 0; index < clhy_daemons_limit; ++index) {
                if (clhy_clmp_safe_kill(CLMP_CHILD_PID(index), 0) == KUDA_SUCCESS) {
                    active_children = 1;
                    /* Having just one child is enough to stay around */
                    break;
                }
            }
        } while (!retained->clmp->shutdown_pending && active_children &&
                 (!clhy_graceful_shutdown_timeout || kuda_time_now() < cutoff));

        /* We might be here because we received SIGTERM, either
         * way, try and make sure that all of our processes are
         * really dead.
         */
        for (i = 0; i < num_buckets; i++) {
            clhy_clmp_podx_killpg(all_buckets[i].pod, clhy_daemons_limit,
                               CLHY_CLMP_PODX_RESTART);
        }
        clhy_reclaim_child_processes(1, worker_note_child_killed);

        return DONE;
    }

    /* we've been told to restart */
    if (one_process) {
        /* not worth thinking about */
        return DONE;
    }

    /* advance to the next generation */
    /* XXX: we really need to make sure this new generation number isn't in
     * use by any of the children.
     */
    ++retained->clmp->my_generation;
    clhy_scoreboard_image->global->running_generation = retained->clmp->my_generation;

    if (!retained->clmp->is_ungraceful) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00297)
                     CLHY_SIG_GRACEFUL_STRING " received.  Doing graceful restart");
        /* wake up the children...time to die.  But we'll have more soon */
        for (i = 0; i < num_buckets; i++) {
            clhy_clmp_podx_killpg(all_buckets[i].pod, clhy_daemons_limit,
                               CLHY_CLMP_PODX_GRACEFUL);
        }

        /* This is mostly for debugging... so that we know what is still
         * gracefully dealing with existing request.
         */

    }
    else {
        /* Kill 'em all.  Since the child acts the same on the parents SIGTERM
         * and a SIGHUP, we may as well use the same signal, because some user
         * pthreads are stealing signals from us left and right.
         */
        for (i = 0; i < num_buckets; i++) {
            clhy_clmp_podx_killpg(all_buckets[i].pod, clhy_daemons_limit,
                               CLHY_CLMP_PODX_RESTART);
        }

        clhy_reclaim_child_processes(1, /* Start with SIGTERM */
                                   worker_note_child_killed);
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf, CLHYLOGNO(00298)
                    "SIGHUP received.  Attempting to restart");
    }

    return OK;
}

/* This really should be a post_config hook, but the error log is already
 * redirected by that point, so we need to do this in the open_logs phase.
 */
static int worker_open_logs(kuda_pool_t *p, kuda_pool_t *plog, kuda_pool_t *ptemp, server_rec *s)
{
    int startup = 0;
    int level_flags = 0;
    int num_buckets = 0;
    clhy_listen_rec **listen_buckets;
    kuda_status_t rv;
    char id[16];
    int i;

    pconf = p;

    /* the reverse of pre_config, we want this only the first time around */
    if (retained->clmp->capi_loads == 1) {
        startup = 1;
        level_flags |= CLHYLOG_STARTUP;
    }

    if ((num_listensocks = clhy_setup_listeners(clhy_server_conf)) < 1) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT | level_flags, 0,
                     (startup ? NULL : s),
                     "no listening sockets available, shutting down");
        return !OK;
    }

    if (one_process) {
        num_buckets = 1;
    }
    else if (retained->clmp->was_graceful) {
        /* Preserve the number of buckets on graceful restarts. */
        num_buckets = retained->clmp->num_buckets;
    }
    if ((rv = clhy_duplicate_listeners(pconf, clhy_server_conf,
                                     &listen_buckets, &num_buckets))) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT | level_flags, rv,
                     (startup ? NULL : s),
                     "could not duplicate listeners");
        return !OK;
    }

    all_buckets = kuda_pcalloc(pconf, num_buckets * sizeof(*all_buckets));
    for (i = 0; i < num_buckets; i++) {
        if (!one_process && /* no POD in one_process mode */
                (rv = clhy_clmp_podx_open(pconf, &all_buckets[i].pod))) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT | level_flags, rv,
                         (startup ? NULL : s),
                         "could not open pipe-of-death");
            return !OK;
        }
        /* Initialize cross-process accept lock (safe accept needed only) */
        if ((rv = SAFE_ACCEPT((kuda_snprintf(id, sizeof id, "%i", i),
                               clhy_proc_mutex_create(&all_buckets[i].mutex,
                                                    NULL, CLHY_ACCEPT_MUTEX_TYPE,
                                                    id, s, pconf, 0))))) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT | level_flags, rv,
                         (startup ? NULL : s),
                         "could not create accept mutex");
            return !OK;
        }
        all_buckets[i].listeners = listen_buckets[i];
    }

    if (retained->clmp->max_buckets < num_buckets) {
        int new_max, *new_ptr;
        new_max = retained->clmp->max_buckets * 2;
        if (new_max < num_buckets) {
            new_max = num_buckets;
        }
        new_ptr = (int *)kuda_palloc(clhy_pglobal, new_max * sizeof(int));
        memcpy(new_ptr, retained->idle_spawn_rate,
               retained->clmp->num_buckets * sizeof(int));
        retained->idle_spawn_rate = new_ptr;
        retained->clmp->max_buckets = new_max;
    }
    if (retained->clmp->num_buckets < num_buckets) {
        int rate_max = 1;
        /* If new buckets are added, set their idle spawn rate to
         * the highest so far, so that they get filled as quickly
         * as the existing ones.
         */
        for (i = 0; i < retained->clmp->num_buckets; i++) {
            if (rate_max < retained->idle_spawn_rate[i]) {
                rate_max = retained->idle_spawn_rate[i];
            }
        }
        for (/* up to date i */; i < num_buckets; i++) {
            retained->idle_spawn_rate[i] = rate_max;
        }
    }
    retained->clmp->num_buckets = num_buckets;

    return OK;
}

static int worker_pre_config(kuda_pool_t *pconf, kuda_pool_t *plog,
                             kuda_pool_t *ptemp)
{
    int no_detach, debug, foreground;
    kuda_status_t rv;
    const char *userdata_key = "clmp_worker_capi";

    debug = clhy_exists_config_define("DEBUG");

    if (debug) {
        foreground = one_process = 1;
        no_detach = 0;
    }
    else {
        one_process = clhy_exists_config_define("ONE_PROCESS");
        no_detach = clhy_exists_config_define("NO_DETACH");
        foreground = clhy_exists_config_define("FOREGROUND");
    }

    clhy_mutex_register(pconf, CLHY_ACCEPT_MUTEX_TYPE, NULL, KUDA_LOCK_DEFAULT, 0);

    retained = clhy_retained_data_get(userdata_key);
    if (!retained) {
        retained = clhy_retained_data_create(userdata_key, sizeof(*retained));
        retained->clmp = clhy_unixd_clmp_get_retained_data();
        retained->max_daemons_limit = -1;
    }
    retained->clmp->clmp_state = CLHY_CLMPQ_STARTING;
    if (retained->clmp->baton != retained) {
        retained->clmp->was_graceful = 0;
        retained->clmp->baton = retained;
    }
    ++retained->clmp->capi_loads;

    /* sigh, want this only the second time around */
    if (retained->clmp->capi_loads == 2) {
        if (!one_process && !foreground) {
            /* before we detach, setup crash handlers to log to errorlog */
            clhy_fatal_signal_setup(clhy_server_conf, pconf);
            rv = kuda_proc_detach(no_detach ? KUDA_PROC_DETACH_FOREGROUND
                                           : KUDA_PROC_DETACH_DAEMONIZE);
            if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, NULL, CLHYLOGNO(00299)
                             "kuda_proc_detach failed");
                return HTTP_INTERNAL_SERVER_ERROR;
            }
        }
    }

    parent_pid = clhy_my_pid = getpid();

    clhy_listen_pre_config();
    clhy_daemons_to_start = DEFAULT_START_DAEMON;
    min_spare_threads = DEFAULT_MIN_FREE_DAEMON * DEFAULT_THREADS_PER_CHILD;
    max_spare_threads = DEFAULT_MAX_FREE_DAEMON * DEFAULT_THREADS_PER_CHILD;
    server_limit = DEFAULT_SERVER_LIMIT;
    thread_limit = DEFAULT_THREAD_LIMIT;
    clhy_daemons_limit = server_limit;
    threads_per_child = DEFAULT_THREADS_PER_CHILD;
    max_workers = clhy_daemons_limit * threads_per_child;
    had_healthy_child = 0;
    clhy_extended_status = 0;

    return OK;
}

static int worker_check_config(kuda_pool_t *p, kuda_pool_t *plog,
                               kuda_pool_t *ptemp, server_rec *s)
{
    int startup = 0;

    /* the reverse of pre_config, we want this only the first time around */
    if (retained->clmp->capi_loads == 1) {
        startup = 1;
    }

    if (server_limit > MAX_SERVER_LIMIT) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00300)
                         "WARNING: ServerLimit of %d exceeds compile-time "
                         "limit of %d servers, decreasing to %d.",
                         server_limit, MAX_SERVER_LIMIT, MAX_SERVER_LIMIT);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00301)
                         "ServerLimit of %d exceeds compile-time limit "
                         "of %d, decreasing to match",
                         server_limit, MAX_SERVER_LIMIT);
        }
        server_limit = MAX_SERVER_LIMIT;
    }
    else if (server_limit < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00302)
                         "WARNING: ServerLimit of %d not allowed, "
                         "increasing to 1.", server_limit);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00303)
                         "ServerLimit of %d not allowed, increasing to 1",
                         server_limit);
        }
        server_limit = 1;
    }

    /* you cannot change ServerLimit across a restart; ignore
     * any such attempts
     */
    if (!retained->first_server_limit) {
        retained->first_server_limit = server_limit;
    }
    else if (server_limit != retained->first_server_limit) {
        /* don't need a startup console version here */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00304)
                     "changing ServerLimit to %d from original value of %d "
                     "not allowed during restart",
                     server_limit, retained->first_server_limit);
        server_limit = retained->first_server_limit;
    }

    if (thread_limit > MAX_THREAD_LIMIT) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00305)
                         "WARNING: ThreadLimit of %d exceeds compile-time "
                         "limit of %d threads, decreasing to %d.",
                         thread_limit, MAX_THREAD_LIMIT, MAX_THREAD_LIMIT);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00306)
                         "ThreadLimit of %d exceeds compile-time limit "
                         "of %d, decreasing to match",
                         thread_limit, MAX_THREAD_LIMIT);
        }
        thread_limit = MAX_THREAD_LIMIT;
    }
    else if (thread_limit < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00307)
                         "WARNING: ThreadLimit of %d not allowed, "
                         "increasing to 1.", thread_limit);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00308)
                         "ThreadLimit of %d not allowed, increasing to 1",
                         thread_limit);
        }
        thread_limit = 1;
    }

    /* you cannot change ThreadLimit across a restart; ignore
     * any such attempts
     */
    if (!retained->first_thread_limit) {
        retained->first_thread_limit = thread_limit;
    }
    else if (thread_limit != retained->first_thread_limit) {
        /* don't need a startup console version here */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00309)
                     "changing ThreadLimit to %d from original value of %d "
                     "not allowed during restart",
                     thread_limit, retained->first_thread_limit);
        thread_limit = retained->first_thread_limit;
    }

    if (threads_per_child > thread_limit) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00310)
                         "WARNING: ThreadsPerChild of %d exceeds ThreadLimit "
                         "of %d threads, decreasing to %d. "
                         "To increase, please see the ThreadLimit directive.",
                         threads_per_child, thread_limit, thread_limit);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00311)
                         "ThreadsPerChild of %d exceeds ThreadLimit "
                         "of %d, decreasing to match",
                         threads_per_child, thread_limit);
        }
        threads_per_child = thread_limit;
    }
    else if (threads_per_child < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00312)
                         "WARNING: ThreadsPerChild of %d not allowed, "
                         "increasing to 1.", threads_per_child);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00313)
                         "ThreadsPerChild of %d not allowed, increasing to 1",
                         threads_per_child);
        }
        threads_per_child = 1;
    }

    if (max_workers < threads_per_child) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00314)
                         "WARNING: MaxRequestWorkers of %d is less than "
                         "ThreadsPerChild of %d, increasing to %d. "
                         "MaxRequestWorkers must be at least as large "
                         "as the number of threads in a single server.",
                         max_workers, threads_per_child, threads_per_child);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00315)
                         "MaxRequestWorkers of %d is less than ThreadsPerChild "
                         "of %d, increasing to match",
                         max_workers, threads_per_child);
        }
        max_workers = threads_per_child;
    }

    clhy_daemons_limit = max_workers / threads_per_child;

    if (max_workers % threads_per_child) {
        int tmp_max_workers = clhy_daemons_limit * threads_per_child;

        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00316)
                         "WARNING: MaxRequestWorkers of %d is not an integer "
                         "multiple of ThreadsPerChild of %d, decreasing to nearest "
                         "multiple %d, for a maximum of %d servers.",
                         max_workers, threads_per_child, tmp_max_workers,
                         clhy_daemons_limit);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00317)
                         "MaxRequestWorkers of %d is not an integer multiple of "
                         "ThreadsPerChild of %d, decreasing to nearest "
                         "multiple %d", max_workers, threads_per_child,
                         tmp_max_workers);
        }
        max_workers = tmp_max_workers;
    }

    if (clhy_daemons_limit > server_limit) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00318)
                         "WARNING: MaxRequestWorkers of %d would require %d "
                         "servers and would exceed ServerLimit of %d, decreasing to %d. "
                         "To increase, please see the ServerLimit directive.",
                         max_workers, clhy_daemons_limit, server_limit,
                         server_limit * threads_per_child);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00319)
                         "MaxRequestWorkers of %d would require %d servers and "
                         "exceed ServerLimit of %d, decreasing to %d",
                         max_workers, clhy_daemons_limit, server_limit,
                         server_limit * threads_per_child);
        }
        clhy_daemons_limit = server_limit;
    }

    /* clhy_daemons_to_start > clhy_daemons_limit checked in worker_run() */
    if (clhy_daemons_to_start < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00320)
                         "WARNING: StartServers of %d not allowed, "
                         "increasing to 1.", clhy_daemons_to_start);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00321)
                         "StartServers of %d not allowed, increasing to 1",
                         clhy_daemons_to_start);
        }
        clhy_daemons_to_start = 1;
    }

    if (min_spare_threads < 1) {
        if (startup) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING | CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00322)
                         "WARNING: MinSpareThreads of %d not allowed, "
                         "increasing to 1 to avoid almost certain server failure. "
                         "Please read the documentation.", min_spare_threads);
        } else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(00323)
                         "MinSpareThreads of %d not allowed, increasing to 1",
                         min_spare_threads);
        }
        min_spare_threads = 1;
    }

    /* max_spare_threads < min_spare_threads + threads_per_child
     * checked in worker_run()
     */

    return OK;
}

static void worker_hooks(kuda_pool_t *p)
{
    /* Our open_logs hook function must run before the core's, or stderr
     * will be redirected to a file, and the messages won't print to the
     * console.
     */
    static const char *const aszSucc[] = {"core.c", NULL};
    one_process = 0;

    clhy_hook_open_logs(worker_open_logs, NULL, aszSucc, KUDA_HOOK_REALLY_FIRST);
    /* we need to set the cLMP state before other pre-config hooks use cLMP query
     * to retrieve it, so register as REALLY_FIRST
     */
    clhy_hook_pre_config(worker_pre_config, NULL, NULL, KUDA_HOOK_REALLY_FIRST);
    clhy_hook_check_config(worker_check_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clcore(worker_run, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clmp_query(worker_query, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_clmp_get_name(worker_get_name, NULL, NULL, KUDA_HOOK_MIDDLE);
}

static const char *set_daemons_to_start(cmd_parms *cmd, void *dummy,
                                        const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    clhy_daemons_to_start = atoi(arg);
    return NULL;
}

static const char *set_min_spare_threads(cmd_parms *cmd, void *dummy,
                                         const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    min_spare_threads = atoi(arg);
    return NULL;
}

static const char *set_max_spare_threads(cmd_parms *cmd, void *dummy,
                                         const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    max_spare_threads = atoi(arg);
    return NULL;
}

static const char *set_max_workers (cmd_parms *cmd, void *dummy,
                                     const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }
    if (!strcasecmp(cmd->cmd->name, "MaxClients")) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, NULL, CLHYLOGNO(00324)
                     "MaxClients is deprecated, use MaxRequestWorkers "
                     "instead.");
    }
    max_workers = atoi(arg);
    return NULL;
}

static const char *set_threads_per_child (cmd_parms *cmd, void *dummy,
                                          const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    threads_per_child = atoi(arg);
    return NULL;
}

static const char *set_server_limit (cmd_parms *cmd, void *dummy, const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    server_limit = atoi(arg);
    return NULL;
}

static const char *set_thread_limit (cmd_parms *cmd, void *dummy, const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    thread_limit = atoi(arg);
    return NULL;
}

static const command_rec worker_cmds[] = {
LISTEN_COMMANDS,
CLHY_INIT_TAKE1("StartServers", set_daemons_to_start, NULL, RSRC_CONF,
  "Number of child processes launched at server startup"),
CLHY_INIT_TAKE1("MinSpareThreads", set_min_spare_threads, NULL, RSRC_CONF,
  "Minimum number of idle threads, to handle request spikes"),
CLHY_INIT_TAKE1("MaxSpareThreads", set_max_spare_threads, NULL, RSRC_CONF,
  "Maximum number of idle threads"),
CLHY_INIT_TAKE1("MaxRequestWorkers", set_max_workers, NULL, RSRC_CONF,
  "Maximum number of threads alive at the same time"),
CLHY_INIT_TAKE1("MaxClients", set_max_workers, NULL, RSRC_CONF,
  "Deprecated name of MaxRequestWorkers"),
CLHY_INIT_TAKE1("ThreadsPerChild", set_threads_per_child, NULL, RSRC_CONF,
  "Number of threads each child creates"),
CLHY_INIT_TAKE1("ServerLimit", set_server_limit, NULL, RSRC_CONF,
  "Maximum number of child processes for this run of cLHy"),
CLHY_INIT_TAKE1("ThreadLimit", set_thread_limit, NULL, RSRC_CONF,
  "Maximum number of worker threads per child process for this run of cLHy - Upper limit for ThreadsPerChild"),
CLHY_GRACEFUL_SHUTDOWN_TIMEOUT_COMMAND,
{ NULL }
};

CLHY_DECLARE_CAPI(clmp_worker) = {
    CLMP16_CAPI_STUFF,
    NULL,                       /* hook to run before clhy parses args */
    NULL,                       /* create per-directory config structure */
    NULL,                       /* merge per-directory config structures */
    NULL,                       /* create per-server config structure */
    NULL,                       /* merge per-server config structures */
    worker_cmds,                /* command kuda_table_t */
    worker_hooks                /* register_hooks */
};

