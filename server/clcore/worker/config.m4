AC_MSG_CHECKING(if worker cLMP supports this platform)
if test $forking_clmp_supported != yes; then
    AC_MSG_RESULT(no - This is not a forking platform)
elif test $ac_cv_define_KUDA_HAS_THREADS != yes; then
    AC_MSG_RESULT(no - KUDA does not support threads)
elif test $have_threaded_sig_graceful != yes; then
    AC_MSG_RESULT(no - SIG_GRACEFUL cannot be used with a threaded cLMP)
else
    AC_MSG_RESULT(yes)
    CLHYKUDEL_CLMP_SUPPORTED(worker, yes, yes)
fi
