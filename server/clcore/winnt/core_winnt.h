/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  core_winnt.h
 * @brief WinNT cLMP specific
 *
 * @addtogroup CLHYKUDEL_CLMP_WINNT
 * @{
 */

#ifndef CLHYKUDEL_CLMP_WINNT_H
#define CLHYKUDEL_CLMP_WINNT_H

#include "kuda_proc_mutex.h"
#include "clhy_listen.h"

/* From service.c: */

#define SERVICE_CLHYKUDEL_RESTART 128

#ifndef CLHY_DEFAULT_SERVICE_NAME
#define CLHY_DEFAULT_SERVICE_NAME "cLHy1.4"
#endif

#define SERVICECONFIG "System\\CurrentControlSet\\Services\\%s"
#define SERVICEPARAMS "System\\CurrentControlSet\\Services\\%s\\Parameters"

kuda_status_t clmp_service_set_name(kuda_pool_t *p, const char **display_name,
                                                 const char *set_name);
kuda_status_t clmp_merge_service_args(kuda_pool_t *p, kuda_array_header_t *args,
                                   int fixed_args);

kuda_status_t clmp_service_to_start(const char **display_name, kuda_pool_t *p);
kuda_status_t clmp_service_started(void);
kuda_status_t clmp_service_install(kuda_pool_t *ptemp, int argc,
                                char const* const* argv, int reconfig);
kuda_status_t clmp_service_uninstall(void);

kuda_status_t clmp_service_start(kuda_pool_t *ptemp, int argc,
                              char const* const* argv);

void clmp_signal_service(kuda_pool_t *ptemp, int signal);

void clmp_service_stopping(void);

void clmp_start_console_handler(void);
void clmp_start_child_console_handler(void);

/* From nt_eventlog.c: */

void clmp_nt_eventlog_stderr_open(const char *display_name, kuda_pool_t *p);
void clmp_nt_eventlog_stderr_flush(void);

/* From core_winnt.c: */

extern cAPI CLHY_CAPI_DECLARE_DATA core_winnt_capi;
extern int clhy_threads_per_child;

extern DWORD my_pid;
extern kuda_proc_mutex_t *start_mutex;
extern HANDLE exit_event;

extern int winnt_clmp_state;
extern OSVERSIONINFO osver;
extern DWORD stack_res_flag;

extern void clean_child_exit(int);

typedef enum {
    SIGNAL_PARENT_SHUTDOWN,
    SIGNAL_PARENT_RESTART,
    SIGNAL_PARENT_RESTART_GRACEFUL
} clhy_signal_parent_e;
CLHY_DECLARE(void) clhy_signal_parent(clhy_signal_parent_e type);

void hold_console_open_on_error(void);

/* From child.c: */
void child_main(kuda_pool_t *pconf, DWORD parent_pid);

#endif /* CLHYKUDEL_CLMP_WINNT_H */
/** @} */
