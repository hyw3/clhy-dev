AC_MSG_CHECKING(if WinNT cLMP supports this platform)
case $host in
    *mingw32*)
        AC_MSG_RESULT(yes)
        CLHYKUDEL_CLMP_SUPPORTED(winnt, no, yes)
        ;;
    *)
        AC_MSG_RESULT(no)
        ;;
esac
