/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_xml.h"

#include "wwhy.h"
#include "http_protocol.h"
#include "http_log.h"
#include "http_core.h"

#include "util_charset.h"
#include "util_xml.h"


/* used for reading input blocks */
#define READ_BLOCKSIZE 2048


/* we know core's capi_index is 0 */
#undef CLHYLOG_CAPI_INDEX
#define CLHYLOG_CAPI_INDEX CLHY_CORE_CAPI_INDEX

CLHY_DECLARE(int) clhy_xml_parse_input(request_rec * r, kuda_xml_doc **pdoc)
{
    kuda_xml_parser *parser;
    kuda_bucket_brigade *brigade;
    int seen_eos;
    kuda_status_t status;
    char errbuf[200];
    kuda_size_t total_read = 0;
    kuda_size_t limit_xml_body = clhy_get_limit_xml_body(r);
    int result = HTTP_BAD_REQUEST;

    parser = kuda_xml_parser_create(r->pool);
    brigade = kuda_brigade_create(r->pool, r->connection->bucket_alloc);

    seen_eos = 0;
    total_read = 0;

    do {
        kuda_bucket *bucket;

        /* read the body, stuffing it into the parser */
        status = clhy_get_brigade(r->input_filters, brigade,
                                CLHY_MODE_READBYTES, KUDA_BLOCK_READ,
                                READ_BLOCKSIZE);

        if (status != KUDA_SUCCESS) {
            result = clhy_map_http_request_error(status, HTTP_BAD_REQUEST);
            goto read_error;
        }

        for (bucket = KUDA_BRIGADE_FIRST(brigade);
             bucket != KUDA_BRIGADE_SENTINEL(brigade);
             bucket = KUDA_BUCKET_NEXT(bucket))
        {
            const char *data;
            kuda_size_t len;

            if (KUDA_BUCKET_IS_EOS(bucket)) {
                seen_eos = 1;
                break;
            }

            if (KUDA_BUCKET_IS_METADATA(bucket)) {
                continue;
            }

            status = kuda_bucket_read(bucket, &data, &len, KUDA_BLOCK_READ);
            if (status != KUDA_SUCCESS) {
                goto read_error;
            }

            total_read += len;
            if (limit_xml_body && total_read > limit_xml_body) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00539)
                              "XML request body is larger than the configured "
                              "limit of %lu", (unsigned long)limit_xml_body);
                result = HTTP_REQUEST_ENTITY_TOO_LARGE;
                goto read_error;
            }

            status = kuda_xml_parser_feed(parser, data, len);
            if (status) {
                goto parser_error;
            }
        }

        kuda_brigade_cleanup(brigade);
    } while (!seen_eos);

    kuda_brigade_destroy(brigade);

    /* tell the parser that we're done */
    status = kuda_xml_parser_done(parser, pdoc);
    if (status) {
        /* Some parsers are stupid and return an error on blank documents. */
        if (!total_read) {
            *pdoc = NULL;
            return OK;
        }
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00540)
                      "XML parser error (at end). status=%d", status);
        return HTTP_BAD_REQUEST;
    }

#if KUDA_CHARSET_EBCDIC
    kuda_xml_parser_convert_doc(r->pool, *pdoc, clhy_hdrs_from_ascii);
#endif
    return OK;

  parser_error:
    (void) kuda_xml_parser_geterror(parser, errbuf, sizeof(errbuf));
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00541)
                  "XML Parser Error: %s", errbuf);

    /* FALLTHRU */

  read_error:
    /* make sure the parser is terminated */
    (void) kuda_xml_parser_done(parser, NULL);

    kuda_brigade_destroy(brigade);

    /* cLHy will supply a default error, plus the error log above. */
    return result;
}
