/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"
#include "kuda_lib.h"
#include "kuda_hash.h"
#include "kuda_strings.h"

#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "util_filter.h"

/* NOTE: cLHy's current design doesn't allow a pool to be passed thru,
   so we depend on a global to hold the correct pool
*/
#define FILTER_POOL     kuda_hook_global_pool
#include "clhy_hooks.h"   /* for kuda_hook_global_pool */

/*
** This macro returns true/false if a given filter should be inserted BEFORE
** another filter. This will happen when one of: 1) there isn't another
** filter; 2) that filter has a higher filter type (class); 3) that filter
** corresponds to a different request.
*/
#define INSERT_BEFORE(f, before_this) ((before_this) == NULL \
                           || (before_this)->frec->ftype > (f)->frec->ftype \
                           || (before_this)->r != (f)->r)

/* Trie structure to hold the mapping from registered
 * filter names to filters
 */

/* we know core's capi_index is 0 */
#undef CLHYLOG_CAPI_INDEX
#define CLHYLOG_CAPI_INDEX CLHY_CORE_CAPI_INDEX

typedef struct filter_trie_node filter_trie_node;

typedef struct {
    int c;
    filter_trie_node *child;
} filter_trie_child_ptr;

/* Each trie node has an array of pointers to its children.
 * The array is kept in sorted order so that add_any_filter()
 * can do a binary search
 */
struct filter_trie_node {
    clhy_filter_rec_t *frec;
    filter_trie_child_ptr *children;
    int nchildren;
    int size;
};

#define TRIE_INITIAL_SIZE 4

/* Link a trie node to its parent
 */
static void trie_node_link(kuda_pool_t *p, filter_trie_node *parent,
                           filter_trie_node *child, int c)
{
    int i, j;

    if (parent->nchildren == parent->size) {
        filter_trie_child_ptr *new;
        parent->size *= 2;
        new = (filter_trie_child_ptr *)kuda_palloc(p, parent->size *
                                             sizeof(filter_trie_child_ptr));
        memcpy(new, parent->children, parent->nchildren *
               sizeof(filter_trie_child_ptr));
        parent->children = new;
    }

    for (i = 0; i < parent->nchildren; i++) {
        if (c == parent->children[i].c) {
            return;
        }
        else if (c < parent->children[i].c) {
            break;
        }
    }
    for (j = parent->nchildren; j > i; j--) {
        parent->children[j].c = parent->children[j - 1].c;
        parent->children[j].child = parent->children[j - 1].child;
    }
    parent->children[i].c = c;
    parent->children[i].child = child;

    parent->nchildren++;
}

/* Allocate a new node for a trie.
 * If parent is non-NULL, link the new node under the parent node with
 * key 'c' (or, if an existing child node matches, return that one)
 */
static filter_trie_node *trie_node_alloc(kuda_pool_t *p,
                                         filter_trie_node *parent, char c)
{
    filter_trie_node *new_node;
    if (parent) {
        int i;
        for (i = 0; i < parent->nchildren; i++) {
            if (c == parent->children[i].c) {
                return parent->children[i].child;
            }
            else if (c < parent->children[i].c) {
                break;
            }
        }
        new_node =
            (filter_trie_node *)kuda_palloc(p, sizeof(filter_trie_node));
        trie_node_link(p, parent, new_node, c);
    }
    else { /* No parent node */
        new_node = (filter_trie_node *)kuda_palloc(p,
                                                  sizeof(filter_trie_node));
    }

    new_node->frec = NULL;
    new_node->nchildren = 0;
    new_node->size = TRIE_INITIAL_SIZE;
    new_node->children = (filter_trie_child_ptr *)kuda_palloc(p,
                             new_node->size * sizeof(filter_trie_child_ptr));
    return new_node;
}

static filter_trie_node *registered_output_filters = NULL;
static filter_trie_node *registered_input_filters = NULL;


static kuda_status_t filter_cleanup(void *ctx)
{
    registered_output_filters = NULL;
    registered_input_filters = NULL;
    return KUDA_SUCCESS;
}

static clhy_filter_rec_t *get_filter_handle(const char *name,
                                          const filter_trie_node *filter_set)
{
    if (filter_set) {
        const char *n;
        const filter_trie_node *node;

        node = filter_set;
        for (n = name; *n; n++) {
            int start, end;
            start = 0;
            end = node->nchildren - 1;
            while (end >= start) {
                int middle = (end + start) / 2;
                char ch = node->children[middle].c;
                if (*n == ch) {
                    node = node->children[middle].child;
                    break;
                }
                else if (*n < ch) {
                    end = middle - 1;
                }
                else {
                    start = middle + 1;
                }
            }
            if (end < start) {
                node = NULL;
                break;
            }
        }

        if (node && node->frec) {
            return node->frec;
        }
    }
    return NULL;
}

CLHY_DECLARE(clhy_filter_rec_t *)clhy_get_output_filter_handle(const char *name)
{
    return get_filter_handle(name, registered_output_filters);
}

CLHY_DECLARE(clhy_filter_rec_t *)clhy_get_input_filter_handle(const char *name)
{
    return get_filter_handle(name, registered_input_filters);
}

static clhy_filter_rec_t *register_filter(const char *name,
                            clhy_filter_func filter_func,
                            clhy_init_filter_func filter_init,
                            clhy_filter_type ftype,
                            filter_trie_node **reg_filter_set)
{
    clhy_filter_rec_t *frec;
    char *normalized_name;
    const char *n;
    filter_trie_node *node;

    if (!*reg_filter_set) {
        *reg_filter_set = trie_node_alloc(FILTER_POOL, NULL, 0);
    }

    normalized_name = kuda_pstrdup(FILTER_POOL, name);
    clhy_str_tolower(normalized_name);

    node = *reg_filter_set;
    for (n = normalized_name; *n; n++) {
        filter_trie_node *child = trie_node_alloc(FILTER_POOL, node, *n);
        if (kuda_isalpha(*n)) {
            trie_node_link(FILTER_POOL, node, child, kuda_toupper(*n));
        }
        node = child;
    }
    if (node->frec) {
        frec = node->frec;
    }
    else {
        frec = kuda_pcalloc(FILTER_POOL, sizeof(*frec));
        node->frec = frec;
        frec->name = normalized_name;
    }
    frec->filter_func = filter_func;
    frec->filter_init_func = filter_init;
    frec->ftype = ftype;

    kuda_pool_cleanup_register(FILTER_POOL, NULL, filter_cleanup,
                              kuda_pool_cleanup_null);
    return frec;
}

CLHY_DECLARE(clhy_filter_rec_t *) clhy_register_input_filter(const char *name,
                                          clhy_in_filter_func filter_func,
                                          clhy_init_filter_func filter_init,
                                          clhy_filter_type ftype)
{
    clhy_filter_func f;
    f.in_func = filter_func;
    return register_filter(name, f, filter_init, ftype,
                           &registered_input_filters);
}

CLHY_DECLARE(clhy_filter_rec_t *) clhy_register_output_filter(const char *name,
                                           clhy_out_filter_func filter_func,
                                           clhy_init_filter_func filter_init,
                                           clhy_filter_type ftype)
{
    return clhy_register_output_filter_protocol(name, filter_func,
                                              filter_init, ftype, 0);
}

CLHY_DECLARE(clhy_filter_rec_t *) clhy_register_output_filter_protocol(
                                           const char *name,
                                           clhy_out_filter_func filter_func,
                                           clhy_init_filter_func filter_init,
                                           clhy_filter_type ftype,
                                           unsigned int proto_flags)
{
    clhy_filter_rec_t* ret ;
    clhy_filter_func f;
    f.out_func = filter_func;
    ret = register_filter(name, f, filter_init, ftype,
                          &registered_output_filters);
    ret->proto_flags = proto_flags ;
    return ret ;
}

static clhy_filter_t *add_any_filter_handle(clhy_filter_rec_t *frec, void *ctx,
                                          request_rec *r, conn_rec *c,
                                          clhy_filter_t **r_filters,
                                          clhy_filter_t **p_filters,
                                          clhy_filter_t **c_filters)
{
    kuda_pool_t *p = frec->ftype < CLHY_FTYPE_CONNECTION && r ? r->pool : c->pool;
    clhy_filter_t *f = kuda_palloc(p, sizeof(*f));
    clhy_filter_t **outf;

    if (frec->ftype < CLHY_FTYPE_PROTOCOL) {
        if (r) {
            outf = r_filters;
        }
        else {
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, c, CLHYLOGNO(00080)
                          "a content filter was added without a request: %s", frec->name);
            return NULL;
        }
    }
    else if (frec->ftype < CLHY_FTYPE_CONNECTION) {
        if (r) {
            outf = p_filters;
        }
        else {
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, c, CLHYLOGNO(00081)
                          "a protocol filter was added without a request: %s", frec->name);
            return NULL;
        }
    }
    else {
        outf = c_filters;
    }

    f->frec = frec;
    f->ctx = ctx;
    /* f->r must always be NULL for connection filters */
    f->r = frec->ftype < CLHY_FTYPE_CONNECTION ? r : NULL;
    f->c = c;
    f->next = NULL;

    if (INSERT_BEFORE(f, *outf)) {
        f->next = *outf;

        if (*outf) {
            clhy_filter_t *first = NULL;

            if (r) {
                /* If we are adding our first non-connection filter,
                 * Then don't try to find the right location, it is
                 * automatically first.
                 */
                if (*r_filters != *c_filters) {
                    first = *r_filters;
                    while (first && (first->next != (*outf))) {
                        first = first->next;
                    }
                }
            }
            if (first && first != (*outf)) {
                first->next = f;
            }
        }
        *outf = f;
    }
    else {
        clhy_filter_t *fscan = *outf;
        while (!INSERT_BEFORE(f, fscan->next))
            fscan = fscan->next;

        f->next = fscan->next;
        fscan->next = f;
    }

    if (frec->ftype < CLHY_FTYPE_CONNECTION && (*r_filters == *c_filters)) {
        *r_filters = *p_filters;
    }
    return f;
}

static clhy_filter_t *add_any_filter(const char *name, void *ctx,
                                   request_rec *r, conn_rec *c,
                                   const filter_trie_node *reg_filter_set,
                                   clhy_filter_t **r_filters,
                                   clhy_filter_t **p_filters,
                                   clhy_filter_t **c_filters)
{
    if (reg_filter_set) {
        const char *n;
        const filter_trie_node *node;

        node = reg_filter_set;
        for (n = name; *n; n++) {
            int start, end;
            start = 0;
            end = node->nchildren - 1;
            while (end >= start) {
                int middle = (end + start) / 2;
                char ch = node->children[middle].c;
                if (*n == ch) {
                    node = node->children[middle].child;
                    break;
                }
                else if (*n < ch) {
                    end = middle - 1;
                }
                else {
                    start = middle + 1;
                }
            }
            if (end < start) {
                node = NULL;
                break;
            }
        }

        if (node && node->frec) {
            return add_any_filter_handle(node->frec, ctx, r, c, r_filters,
                                         p_filters, c_filters);
        }
    }

    clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r ? r->connection : c, CLHYLOGNO(00082)
                  "an unknown filter was not added: %s", name);
    return NULL;
}

CLHY_DECLARE(clhy_filter_t *) clhy_add_input_filter(const char *name, void *ctx,
                                              request_rec *r, conn_rec *c)
{
    return add_any_filter(name, ctx, r, c, registered_input_filters,
                          r ? &r->input_filters : NULL,
                          r ? &r->proto_input_filters : NULL, &c->input_filters);
}

CLHY_DECLARE(clhy_filter_t *) clhy_add_input_filter_handle(clhy_filter_rec_t *f,
                                                     void *ctx,
                                                     request_rec *r,
                                                     conn_rec *c)
{
    return add_any_filter_handle(f, ctx, r, c, r ? &r->input_filters : NULL,
                                 r ? &r->proto_input_filters : NULL,
                                 &c->input_filters);
}

CLHY_DECLARE(clhy_filter_t *) clhy_add_output_filter(const char *name, void *ctx,
                                               request_rec *r, conn_rec *c)
{
    return add_any_filter(name, ctx, r, c, registered_output_filters,
                          r ? &r->output_filters : NULL,
                          r ? &r->proto_output_filters : NULL, &c->output_filters);
}

CLHY_DECLARE(clhy_filter_t *) clhy_add_output_filter_handle(clhy_filter_rec_t *f,
                                                      void *ctx,
                                                      request_rec *r,
                                                      conn_rec *c)
{
    return add_any_filter_handle(f, ctx, r, c, r ? &r->output_filters : NULL,
                                 r ? &r->proto_output_filters : NULL,
                                 &c->output_filters);
}

static void remove_any_filter(clhy_filter_t *f, clhy_filter_t **r_filt, clhy_filter_t **p_filt,
                              clhy_filter_t **c_filt)
{
    clhy_filter_t **curr = r_filt ? r_filt : c_filt;
    clhy_filter_t *fscan = *curr;

    if (p_filt && *p_filt == f)
        *p_filt = (*p_filt)->next;

    if (*curr == f) {
        *curr = (*curr)->next;
        return;
    }

    while (fscan->next != f) {
        if (!(fscan = fscan->next)) {
            return;
        }
    }

    fscan->next = f->next;
}

CLHY_DECLARE(void) clhy_remove_input_filter(clhy_filter_t *f)
{
    remove_any_filter(f, f->r ? &f->r->input_filters : NULL,
                      f->r ? &f->r->proto_input_filters : NULL,
                      &f->c->input_filters);
}

CLHY_DECLARE(void) clhy_remove_output_filter(clhy_filter_t *f)
{
    remove_any_filter(f, f->r ? &f->r->output_filters : NULL,
                      f->r ? &f->r->proto_output_filters : NULL,
                      &f->c->output_filters);
}

CLHY_DECLARE(kuda_status_t) clhy_remove_input_filter_byhandle(clhy_filter_t *next,
                                                         const char *handle)
{
    clhy_filter_t *found = NULL;
    clhy_filter_rec_t *filter;

    if (!handle) {
        return KUDA_EINVAL;
    }
    filter = clhy_get_input_filter_handle(handle);
    if (!filter) {
        return KUDA_NOTFOUND;
    }

    while (next) {
        if (next->frec == filter) {
            found = next;
            break;
        }
        next = next->next;
    }
    if (found) {
        clhy_remove_input_filter(found);
        return KUDA_SUCCESS;
    }
    return KUDA_NOTFOUND;
}

CLHY_DECLARE(kuda_status_t) clhy_remove_output_filter_byhandle(clhy_filter_t *next,
                                                          const char *handle)
{
    clhy_filter_t *found = NULL;
    clhy_filter_rec_t *filter;

    if (!handle) {
        return KUDA_EINVAL;
    }
    filter = clhy_get_output_filter_handle(handle);
    if (!filter) {
        return KUDA_NOTFOUND;
    }

    while (next) {
        if (next->frec == filter) {
            found = next;
            break;
        }
        next = next->next;
    }
    if (found) {
        clhy_remove_output_filter(found);
        return KUDA_SUCCESS;
    }
    return KUDA_NOTFOUND;
}


/*
 * Read data from the next filter in the filter stack.  Data should be
 * modified in the bucket brigade that is passed in.  The core allocates the
 * bucket brigade, cAPIs that wish to replace large chunks of data or to
 * save data off to the side should probably create their own temporary
 * brigade especially for that use.
 */
CLHY_DECLARE(kuda_status_t) clhy_get_brigade(clhy_filter_t *next,
                                        kuda_bucket_brigade *bb,
                                        clhy_input_mode_t mode,
                                        kuda_read_type_e block,
                                        kuda_off_t readbytes)
{
    if (next) {
        return next->frec->filter_func.in_func(next, bb, mode, block,
                                               readbytes);
    }
    return CLHY_NOBODY_READ;
}

/* Pass the buckets to the next filter in the filter stack.  If the
 * current filter is a handler, we should get NULL passed in instead of
 * the current filter.  At that point, we can just call the first filter in
 * the stack, or r->output_filters.
 */
CLHY_DECLARE(kuda_status_t) clhy_pass_brigade(clhy_filter_t *next,
                                         kuda_bucket_brigade *bb)
{
    if (next) {
        kuda_bucket *e;
        if ((e = KUDA_BRIGADE_LAST(bb)) && KUDA_BUCKET_IS_EOS(e) && next->r) {
            /* This is only safe because HTTP_HEADER filter is always in
             * the filter stack.   This ensures that there is ALWAYS a
             * request-based filter that we can attach this to.  If the
             * HTTP_FILTER is removed, and another filter is not put in its
             * place, then handlers like capi_cgi, which attach their own
             * EOS bucket to the brigade will be broken, because we will
             * get two EOS buckets on the same request.
             */
            next->r->eos_sent = 1;

            /* remember the eos for internal redirects, too */
            if (next->r->prev) {
                request_rec *prev = next->r->prev;

                while (prev) {
                    prev->eos_sent = 1;
                    prev = prev->prev;
                }
            }
        }
        return next->frec->filter_func.out_func(next, bb);
    }
    return CLHY_NOBODY_WROTE;
}

/* Pass the buckets to the next filter in the filter stack
 * checking return status for filter errors.
 * returns: OK if clhy_pass_brigade returns KUDA_SUCCESS
 *          CLHY_FILTER_ERROR if filter error exists
 *          HTTP_INTERNAL_SERVER_ERROR for all other cases
 *          logged with optional errmsg
 */
CLHY_DECLARE(kuda_status_t) clhy_pass_brigade_fchk(request_rec *r,
                                              kuda_bucket_brigade *bb,
                                              const char *fmt,
                                              ...)
{
    kuda_status_t rv;

    rv = clhy_pass_brigade(r->output_filters, bb);
    if (rv != KUDA_SUCCESS) {
        if (rv != CLHY_FILTER_ERROR) {
            if (!fmt)
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, r, CLHYLOGNO(00083)
                              "clhy_pass_brigade returned %d", rv);
            else {
                va_list clhy;
                const char *res;
                va_start(clhy, fmt);
                res = kuda_pvsprintf(r->pool, fmt, clhy);
                va_end(clhy);
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, r, CLHYLOGNO(03158)
                              "%s", res);
            }
            return HTTP_INTERNAL_SERVER_ERROR;
        }
        return CLHY_FILTER_ERROR;
    }
    return OK;
}

CLHY_DECLARE(kuda_status_t) clhy_save_brigade(clhy_filter_t *f,
                                         kuda_bucket_brigade **saveto,
                                         kuda_bucket_brigade **b, kuda_pool_t *p)
{
    kuda_bucket *e;
    kuda_status_t rv, srv = KUDA_SUCCESS;

    /* If have never stored any data in the filter, then we had better
     * create an empty bucket brigade so that we can concat.
     */
    if (!(*saveto)) {
        *saveto = kuda_brigade_create(p, f->c->bucket_alloc);
    }

    for (e = KUDA_BRIGADE_FIRST(*b);
         e != KUDA_BRIGADE_SENTINEL(*b);
         e = KUDA_BUCKET_NEXT(e))
    {
        rv = kuda_bucket_setaside(e, p);

        /* If the bucket type does not implement setaside, then
         * (hopefully) morph it into a bucket type which does, and set
         * *that* aside... */
        if (rv == KUDA_ENOTIMPL) {
            const char *s;
            kuda_size_t n;

            rv = kuda_bucket_read(e, &s, &n, KUDA_BLOCK_READ);
            if (rv == KUDA_SUCCESS) {
                rv = kuda_bucket_setaside(e, p);
            }
        }

        if (rv != KUDA_SUCCESS) {
            srv = rv;
            /* Return an error but still save the brigade if
             * ->setaside() is really not implemented. */
            if (rv != KUDA_ENOTIMPL) {
                return rv;
            }
        }
    }
    KUDA_BRIGADE_CONCAT(*saveto, *b);
    return srv;
}

CLHY_DECLARE_NONSTD(kuda_status_t) clhy_filter_flush(kuda_bucket_brigade *bb,
                                                void *ctx)
{
    clhy_filter_t *f = ctx;
    kuda_status_t rv;

    rv = clhy_pass_brigade(f, bb);

    /* Before invocation of the flush callback, kuda_brigade_write et
     * al may place transient buckets in the brigade, which will fall
     * out of scope after returning.  Empty the brigade here, to avoid
     * issues with leaving such buckets in the brigade if some filter
     * fails and leaves a non-empty brigade. */
    kuda_brigade_cleanup(bb);

    return rv;
}

CLHY_DECLARE(kuda_status_t) clhy_fflush(clhy_filter_t *f, kuda_bucket_brigade *bb)
{
    kuda_bucket *b;

    b = kuda_bucket_flush_create(f->c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bb, b);
    return clhy_pass_brigade(f, bb);
}

CLHY_DECLARE_NONSTD(kuda_status_t) clhy_fputstrs(clhy_filter_t *f,
                                            kuda_bucket_brigade *bb, ...)
{
    va_list args;
    kuda_status_t rv;

    va_start(args, bb);
    rv = kuda_brigade_vputstrs(bb, clhy_filter_flush, f, args);
    va_end(args);
    return rv;
}

CLHY_DECLARE_NONSTD(kuda_status_t) clhy_fprintf(clhy_filter_t *f,
                                           kuda_bucket_brigade *bb,
                                           const char *fmt,
                                           ...)
{
    va_list args;
    kuda_status_t rv;

    va_start(args, fmt);
    rv = kuda_brigade_vprintf(bb, clhy_filter_flush, f, fmt, args);
    va_end(args);
    return rv;
}
CLHY_DECLARE(void) clhy_filter_protocol(clhy_filter_t *f, unsigned int flags)
{
    f->frec->proto_flags = flags ;
}
