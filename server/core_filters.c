/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  core_filters.c
 * @brief Core input/output network filters.
 */

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_lib.h"
#include "kuda_fnmatch.h"
#include "kuda_hash.h"
#include "kuda_thread_proc.h"    /* for RLIMIT stuff */

#define KUDA_WANT_IOVEC
#define KUDA_WANT_STRFUNC
#define KUDA_WANT_MEMFUNC
#include "kuda_want.h"

#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_protocol.h" /* For index_of_response().  Grump. */
#include "http_request.h"
#include "http_vhost.h"
#include "http_main.h"     /* For the default_handler below... */
#include "http_log.h"
#include "util_md5.h"
#include "http_connection.h"
#include "kuda_buckets.h"
#include "util_filter.h"
#include "util_ebcdic.h"
#include "core_common.h"
#include "scoreboard.h"
#include "capi_core.h"
#include "clhy_listen.h"

#include "capi_so.h" /* for clhy_find_activated_capi_symbol */

#define CLHY_MIN_SENDFILE_BYTES           (256)

/**
 * Remove all zero length buckets from the brigade.
 */
#define BRIGADE_NORMALIZE(b) \
do { \
    kuda_bucket *e = KUDA_BRIGADE_FIRST(b); \
    do {  \
        if (e->length == 0 && !KUDA_BUCKET_IS_METADATA(e)) { \
            kuda_bucket *d; \
            d = KUDA_BUCKET_NEXT(e); \
            kuda_bucket_delete(e); \
            e = d; \
        } \
        else { \
            e = KUDA_BUCKET_NEXT(e); \
        } \
    } while (!KUDA_BRIGADE_EMPTY(b) && (e != KUDA_BRIGADE_SENTINEL(b))); \
} while (0)

/* we know core's capi_index is 0 */
#undef CLHYLOG_CAPI_INDEX
#define CLHYLOG_CAPI_INDEX CLHY_CORE_CAPI_INDEX

struct core_output_filter_ctx {
    kuda_bucket_brigade *buffered_bb;
    kuda_bucket_brigade *tmp_flush_bb;
    kuda_pool_t *deferred_write_pool;
    kuda_size_t bytes_written;
};

struct core_filter_ctx {
    kuda_bucket_brigade *b;
    kuda_bucket_brigade *tmpbb;
};


kuda_status_t clhy_core_input_filter(clhy_filter_t *f, kuda_bucket_brigade *b,
                                  clhy_input_mode_t mode, kuda_read_type_e block,
                                  kuda_off_t readbytes)
{
    kuda_status_t rv;
    core_net_rec *net = f->ctx;
    core_ctx_t *ctx = net->in_ctx;
    const char *str;
    kuda_size_t len;

    if (mode == CLHY_MODE_INIT) {
        /*
         * this mode is for filters that might need to 'initialize'
         * a connection before reading request data from a client.
         * NNTP over SSL for example needs to handshake before the
         * server sends the welcome message.
         * such filters would have changed the mode before this point
         * is reached.  however, protocol cAPIs such as NNTP should
         * not need to know anything about SSL.  given the example, if
         * SSL is not in the filter chain, CLHY_MODE_INIT is a noop.
         */
        return KUDA_SUCCESS;
    }

    if (!ctx)
    {
        net->in_ctx = ctx = kuda_palloc(f->c->pool, sizeof(*ctx));
        ctx->b = kuda_brigade_create(f->c->pool, f->c->bucket_alloc);
        ctx->tmpbb = kuda_brigade_create(f->c->pool, f->c->bucket_alloc);
        /* seed the brigade with the client socket. */
        rv = clhy_run_insert_network_bucket(f->c, ctx->b, net->client_socket);
        if (rv != KUDA_SUCCESS)
            return rv;
    }
    else if (KUDA_BRIGADE_EMPTY(ctx->b)) {
        return KUDA_EOF;
    }

    /* ### This is bad. */
    BRIGADE_NORMALIZE(ctx->b);

    /* check for empty brigade again *AFTER* BRIGADE_NORMALIZE()
     * If we have lost our socket bucket (see above), we are EOF.
     *
     * Ideally, this should be returning SUCCESS with EOS bucket, but
     * some higher-up APIs (spec. read_request_line via clhy_rgetline)
     * want an error code. */
    if (KUDA_BRIGADE_EMPTY(ctx->b)) {
        return KUDA_EOF;
    }

    if (mode == CLHY_MODE_GETLINE) {
        /* we are reading a single LF line, e.g. the HTTP headers */
        rv = kuda_brigade_split_line(b, ctx->b, block, HUGE_STRING_LEN);
        /* We should treat EAGAIN here the same as we do for EOF (brigade is
         * empty).  We do this by returning whatever we have read.  This may
         * or may not be bogus, but is consistent (for now) with EOF logic.
         */
        if (KUDA_STATUS_IS_EAGAIN(rv) && block == KUDA_NONBLOCK_READ) {
            rv = KUDA_SUCCESS;
        }
        return rv;
    }

    /* ### CLHY_MODE_PEEK is a horrific name for this mode because we also
     * eat any CRLFs that we see.  That's not the obvious intention of
     * this mode.  Determine whether anyone actually uses this or not. */
    if (mode == CLHY_MODE_EATCRLF) {
        kuda_bucket *e;
        const char *c;

        /* The purpose of this loop is to ignore any CRLF (or LF) at the end
         * of a request.  Many browsers send extra lines at the end of POST
         * requests.  We use the PEEK method to determine if there is more
         * data on the socket, so that we know if we should delay sending the
         * end of one request until we have served the second request in a
         * pipelined situation.  We don't want to actually delay sending a
         * response if the server finds a CRLF (or LF), becuause that doesn't
         * mean that there is another request, just a blank line.
         */
        while (1) {
            if (KUDA_BRIGADE_EMPTY(ctx->b))
                return KUDA_EOF;

            e = KUDA_BRIGADE_FIRST(ctx->b);

            rv = kuda_bucket_read(e, &str, &len, KUDA_NONBLOCK_READ);

            if (rv != KUDA_SUCCESS)
                return rv;

            c = str;
            while (c < str + len) {
                if (*c == KUDA_ASCII_LF)
                    c++;
                else if (*c == KUDA_ASCII_CR && *(c + 1) == KUDA_ASCII_LF)
                    c += 2;
                else
                    return KUDA_SUCCESS;
            }

            /* If we reach here, we were a bucket just full of CRLFs, so
             * just toss the bucket. */
            /* FIXME: Is this the right thing to do in the core? */
            kuda_bucket_delete(e);
        }
        return KUDA_SUCCESS;
    }

    /* If mode is EXHAUSTIVE, we want to just read everything until the end
     * of the brigade, which in this case means the end of the socket.
     * To do this, we attach the brigade that has currently been setaside to
     * the brigade that was passed down, and send that brigade back.
     *
     * NOTE:  This is VERY dangerous to use, and should only be done with
     * extreme caution.  FWLIW, this would be needed by an cLMP like Perchild;
     * such an cLMP can easily request the socket and all data that has been
     * read, which means that it can pass it to the correct child process.
     */
    if (mode == CLHY_MODE_EXHAUSTIVE) {
        kuda_bucket *e;

        /* Tack on any buckets that were set aside. */
        KUDA_BRIGADE_CONCAT(b, ctx->b);

        /* Since we've just added all potential buckets (which will most
         * likely simply be the socket bucket) we know this is the end,
         * so tack on an EOS too. */
        /* We have read until the brigade was empty, so we know that we
         * must be EOS. */
        e = kuda_bucket_eos_create(f->c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(b, e);
        return KUDA_SUCCESS;
    }

    /* read up to the amount they specified. */
    if (mode == CLHY_MODE_READBYTES || mode == CLHY_MODE_SPECULATIVE) {
        kuda_bucket *e;

        CLHY_DEBUG_ASSERT(readbytes > 0);

        e = KUDA_BRIGADE_FIRST(ctx->b);
        rv = kuda_bucket_read(e, &str, &len, block);

        if (KUDA_STATUS_IS_EAGAIN(rv) && block == KUDA_NONBLOCK_READ) {
            /* getting EAGAIN for a blocking read is an error; for a
             * non-blocking read, return an empty brigade. */
            return KUDA_SUCCESS;
        }
        else if (rv != KUDA_SUCCESS) {
            return rv;
        }
        else if (block == KUDA_BLOCK_READ && len == 0) {
            /* We wanted to read some bytes in blocking mode.  We read
             * 0 bytes.  Hence, we now assume we are EOS.
             *
             * When we are in normal mode, return an EOS bucket to the
             * caller.
             * When we are in speculative mode, leave ctx->b empty, so
             * that the next call returns an EOS bucket.
             */
            kuda_bucket_delete(e);

            if (mode == CLHY_MODE_READBYTES) {
                e = kuda_bucket_eos_create(f->c->bucket_alloc);
                KUDA_BRIGADE_INSERT_TAIL(b, e);
            }
            return KUDA_SUCCESS;
        }

        /* Have we read as much data as we wanted (be greedy)? */
        if (len < readbytes) {
            kuda_size_t bucket_len;

            rv = KUDA_SUCCESS;
            /* We already registered the data in e in len */
            e = KUDA_BUCKET_NEXT(e);
            while ((len < readbytes) && (rv == KUDA_SUCCESS)
                   && (e != KUDA_BRIGADE_SENTINEL(ctx->b))) {
                /* Check for the availability of buckets with known length */
                if (e->length != -1) {
                    len += e->length;
                    e = KUDA_BUCKET_NEXT(e);
                }
                else {
                    /*
                     * Read from bucket, but non blocking. If there isn't any
                     * more data, well than this is fine as well, we will
                     * not wait for more since we already got some and we are
                     * only checking if there isn't more.
                     */
                    rv = kuda_bucket_read(e, &str, &bucket_len,
                                         KUDA_NONBLOCK_READ);
                    if (rv == KUDA_SUCCESS) {
                        len += bucket_len;
                        e = KUDA_BUCKET_NEXT(e);
                    }
                }
            }
        }

        /* We can only return at most what we read. */
        if (len < readbytes) {
            readbytes = len;
        }

        rv = kuda_brigade_partition(ctx->b, readbytes, &e);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }

        /* Must do move before CONCAT */
        ctx->tmpbb = kuda_brigade_split_ex(ctx->b, e, ctx->tmpbb);

        if (mode == CLHY_MODE_READBYTES) {
            KUDA_BRIGADE_CONCAT(b, ctx->b);
        }
        else if (mode == CLHY_MODE_SPECULATIVE) {
            kuda_bucket *copy_bucket;

            for (e = KUDA_BRIGADE_FIRST(ctx->b);
                 e != KUDA_BRIGADE_SENTINEL(ctx->b);
                 e = KUDA_BUCKET_NEXT(e))
            {
                rv = kuda_bucket_copy(e, &copy_bucket);
                if (rv != KUDA_SUCCESS) {
                    return rv;
                }
                KUDA_BRIGADE_INSERT_TAIL(b, copy_bucket);
            }
        }

        /* Take what was originally there and place it back on ctx->b */
        KUDA_BRIGADE_CONCAT(ctx->b, ctx->tmpbb);
    }
    return KUDA_SUCCESS;
}

static void setaside_remaining_output(clhy_filter_t *f,
                                      core_output_filter_ctx_t *ctx,
                                      kuda_bucket_brigade *bb,
                                      conn_rec *c);

static kuda_status_t send_brigade_nonblocking(kuda_socket_t *s,
                                             kuda_bucket_brigade *bb,
                                             kuda_size_t *bytes_written,
                                             conn_rec *c);

static void remove_empty_buckets(kuda_bucket_brigade *bb);

static kuda_status_t send_brigade_blocking(kuda_socket_t *s,
                                          kuda_bucket_brigade *bb,
                                          kuda_size_t *bytes_written,
                                          conn_rec *c);

static kuda_status_t writev_nonblocking(kuda_socket_t *s,
                                       struct iovec *vec, kuda_size_t nvec,
                                       kuda_bucket_brigade *bb,
                                       kuda_size_t *cumulative_bytes_written,
                                       conn_rec *c);

#if KUDA_HAS_SENDFILE
static kuda_status_t sendfile_nonblocking(kuda_socket_t *s,
                                         kuda_bucket *bucket,
                                         kuda_size_t *cumulative_bytes_written,
                                         conn_rec *c);
#endif

/* XXX: Should these be configurable parameters? */
#define THRESHOLD_MIN_WRITE 4096
#define THRESHOLD_MAX_BUFFER 65536
#define MAX_REQUESTS_IN_PIPELINE 5

/* Optional function coming from capi_logio, used for logging of output
 * traffic
 */
extern KUDA_OPTIONAL_FN_TYPE(clhy_logio_add_bytes_out) *clhy__logio_add_bytes_out;

kuda_status_t clhy_core_output_filter(clhy_filter_t *f, kuda_bucket_brigade *new_bb)
{
    conn_rec *c = f->c;
    core_net_rec *net = f->ctx;
    core_output_filter_ctx_t *ctx = net->out_ctx;
    kuda_bucket_brigade *bb = NULL;
    kuda_bucket *bucket, *next, *flush_upto = NULL;
    kuda_size_t bytes_in_brigade, non_file_bytes_in_brigade;
    int eor_buckets_in_brigade, morphing_bucket_in_brigade;
    kuda_status_t rv;

    /* Fail quickly if the connection has already been aborted. */
    if (c->aborted) {
        if (new_bb != NULL) {
            kuda_brigade_cleanup(new_bb);
        }
        return KUDA_ECONNABORTED;
    }

    if (ctx == NULL) {
        ctx = kuda_pcalloc(c->pool, sizeof(*ctx));
        net->out_ctx = (core_output_filter_ctx_t *)ctx;
        /*
         * Need to create tmp brigade with correct lifetime. Passing
         * NULL to kuda_brigade_split_ex would result in a brigade
         * allocated from bb->pool which might be wrong.
         */
        ctx->tmp_flush_bb = kuda_brigade_create(c->pool, c->bucket_alloc);
        /* same for buffered_bb and clhy_save_brigade */
        ctx->buffered_bb = kuda_brigade_create(c->pool, c->bucket_alloc);
    }

    if (new_bb != NULL)
        bb = new_bb;

    if ((ctx->buffered_bb != NULL) &&
        !KUDA_BRIGADE_EMPTY(ctx->buffered_bb)) {
        if (new_bb != NULL) {
            KUDA_BRIGADE_PREPEND(bb, ctx->buffered_bb);
        }
        else {
            bb = ctx->buffered_bb;
        }
        c->data_in_output_filters = 0;
    }
    else if (new_bb == NULL) {
        return KUDA_SUCCESS;
    }

    /* Scan through the brigade and decide whether to attempt a write,
     * and how much to write, based on the following rules:
     *
     *  1) The new_bb is null: Do a nonblocking write of as much as
     *     possible: do a nonblocking write of as much data as possible,
     *     then save the rest in ctx->buffered_bb.  (If new_bb == NULL,
     *     it probably means that the cLMP is doing asynchronous write
     *     completion and has just determined that this connection
     *     is writable.)
     *
     *  2) Determine if and up to which bucket we need to do a blocking
     *     write:
     *
     *  a) The brigade contains a flush bucket: Do a blocking write
     *     of everything up that point.
     *
     *  b) The request is in CONN_STATE_HANDLER state, and the brigade
     *     contains at least THRESHOLD_MAX_BUFFER bytes in non-file
     *     buckets: Do blocking writes until the amount of data in the
     *     buffer is less than THRESHOLD_MAX_BUFFER.  (The point of this
     *     rule is to provide flow control, in case a handler is
     *     streaming out lots of data faster than the data can be
     *     sent to the client.)
     *
     *  c) The request is in CONN_STATE_HANDLER state, and the brigade
     *     contains at least MAX_REQUESTS_IN_PIPELINE EOR buckets:
     *     Do blocking writes until less than MAX_REQUESTS_IN_PIPELINE EOR
     *     buckets are left. (The point of this rule is to prevent too many
     *     FDs being kept open by pipelined requests, possibly allowing a
     *     DoS).
     *
     *  d) The brigade contains a morphing bucket: If there was no other
     *     reason to do a blocking write yet, try reading the bucket. If its
     *     contents fit into memory before THRESHOLD_MAX_BUFFER is reached,
     *     everything is fine. Otherwise we need to do a blocking write the
     *     up to and including the morphing bucket, because clhy_save_brigade()
     *     would read the whole bucket into memory later on.
     *
     *  3) Actually do the blocking write up to the last bucket determined
     *     by rules 2a-d. The point of doing only one flush is to make as
     *     few calls to writev() as possible.
     *
     *  4) If the brigade contains at least THRESHOLD_MIN_WRITE
     *     bytes: Do a nonblocking write of as much data as possible,
     *     then save the rest in ctx->buffered_bb.
     */

    if (new_bb == NULL) {
        rv = send_brigade_nonblocking(net->client_socket, bb,
                                      &(ctx->bytes_written), c);
        if (rv != KUDA_SUCCESS && !KUDA_STATUS_IS_EAGAIN(rv)) {
            /* The client has aborted the connection */
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE1, rv, c,
                          "core_output_filter: writing data to the network");
            kuda_brigade_cleanup(bb);
            c->aborted = 1;
            return rv;
        }
        setaside_remaining_output(f, ctx, bb, c);
        return KUDA_SUCCESS;
    }

    bytes_in_brigade = 0;
    non_file_bytes_in_brigade = 0;
    eor_buckets_in_brigade = 0;
    morphing_bucket_in_brigade = 0;

    for (bucket = KUDA_BRIGADE_FIRST(bb); bucket != KUDA_BRIGADE_SENTINEL(bb);
         bucket = next) {
        next = KUDA_BUCKET_NEXT(bucket);

        if (!KUDA_BUCKET_IS_METADATA(bucket)) {
            if (bucket->length == (kuda_size_t)-1) {
                /*
                 * A setaside of morphing buckets would read everything into
                 * memory. Instead, we will flush everything up to and
                 * including this bucket.
                 */
                morphing_bucket_in_brigade = 1;
            }
            else {
                bytes_in_brigade += bucket->length;
                if (!KUDA_BUCKET_IS_FILE(bucket))
                    non_file_bytes_in_brigade += bucket->length;
            }
        }
        else if (CLHY_BUCKET_IS_EOR(bucket)) {
            eor_buckets_in_brigade++;
        }

        if (KUDA_BUCKET_IS_FLUSH(bucket)
            || non_file_bytes_in_brigade >= THRESHOLD_MAX_BUFFER
            || morphing_bucket_in_brigade
            || eor_buckets_in_brigade > MAX_REQUESTS_IN_PIPELINE) {
            /* this segment of the brigade MUST be sent before returning. */

            if (CLHYLOGctrace6(c)) {
                char *reason = KUDA_BUCKET_IS_FLUSH(bucket) ?
                               "FLUSH bucket" :
                               (non_file_bytes_in_brigade >= THRESHOLD_MAX_BUFFER) ?
                               "THRESHOLD_MAX_BUFFER" :
                               morphing_bucket_in_brigade ? "morphing bucket" :
                               "MAX_REQUESTS_IN_PIPELINE";
                clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE6, 0, c,
                              "core_output_filter: flushing because of %s",
                              reason);
            }
            /*
             * Defer the actual blocking write to avoid doing many writes.
             */
            flush_upto = next;

            bytes_in_brigade = 0;
            non_file_bytes_in_brigade = 0;
            eor_buckets_in_brigade = 0;
            morphing_bucket_in_brigade = 0;
        }
    }

    if (flush_upto != NULL) {
        ctx->tmp_flush_bb = kuda_brigade_split_ex(bb, flush_upto,
                                                 ctx->tmp_flush_bb);
        rv = send_brigade_blocking(net->client_socket, bb,
                                   &(ctx->bytes_written), c);
        if (rv != KUDA_SUCCESS) {
            /* The client has aborted the connection */
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE1, rv, c,
                          "core_output_filter: writing data to the network");
            kuda_brigade_cleanup(bb);
            c->aborted = 1;
            return rv;
        }
        KUDA_BRIGADE_CONCAT(bb, ctx->tmp_flush_bb);
    }

    if (bytes_in_brigade >= THRESHOLD_MIN_WRITE) {
        rv = send_brigade_nonblocking(net->client_socket, bb,
                                      &(ctx->bytes_written), c);
        if ((rv != KUDA_SUCCESS) && (!KUDA_STATUS_IS_EAGAIN(rv))) {
            /* The client has aborted the connection */
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE1, rv, c,
                          "core_output_filter: writing data to the network");
            kuda_brigade_cleanup(bb);
            c->aborted = 1;
            return rv;
        }
    }

    setaside_remaining_output(f, ctx, bb, c);
    return KUDA_SUCCESS;
}

/*
 * This function assumes that either ctx->buffered_bb == NULL, or
 * ctx->buffered_bb is empty, or ctx->buffered_bb == bb
 */
static void setaside_remaining_output(clhy_filter_t *f,
                                      core_output_filter_ctx_t *ctx,
                                      kuda_bucket_brigade *bb,
                                      conn_rec *c)
{
    if (bb == NULL) {
        return;
    }
    remove_empty_buckets(bb);
    if (!KUDA_BRIGADE_EMPTY(bb)) {
        c->data_in_output_filters = 1;
        if (bb != ctx->buffered_bb) {
            if (!ctx->deferred_write_pool) {
                kuda_pool_create(&ctx->deferred_write_pool, c->pool);
                kuda_pool_tag(ctx->deferred_write_pool, "deferred_write");
            }
            clhy_save_brigade(f, &(ctx->buffered_bb), &bb,
                            ctx->deferred_write_pool);
        }
    }
    else if (ctx->deferred_write_pool) {
        /*
         * There are no more requests in the pipeline. We can just clear the
         * pool.
         */
        kuda_pool_clear(ctx->deferred_write_pool);
    }
}

#ifndef KUDA_MAX_IOVEC_SIZE
#define MAX_IOVEC_TO_WRITE 16
#else
#if KUDA_MAX_IOVEC_SIZE > 16
#define MAX_IOVEC_TO_WRITE 16
#else
#define MAX_IOVEC_TO_WRITE KUDA_MAX_IOVEC_SIZE
#endif
#endif

static kuda_status_t send_brigade_nonblocking(kuda_socket_t *s,
                                             kuda_bucket_brigade *bb,
                                             kuda_size_t *bytes_written,
                                             conn_rec *c)
{
    kuda_bucket *bucket, *next;
    kuda_status_t rv;
    struct iovec vec[MAX_IOVEC_TO_WRITE];
    kuda_size_t nvec = 0;

    remove_empty_buckets(bb);

    for (bucket = KUDA_BRIGADE_FIRST(bb);
         bucket != KUDA_BRIGADE_SENTINEL(bb);
         bucket = next) {
        next = KUDA_BUCKET_NEXT(bucket);
#if KUDA_HAS_SENDFILE
        if (KUDA_BUCKET_IS_FILE(bucket)) {
            kuda_bucket_file *file_bucket = (kuda_bucket_file *)(bucket->data);
            kuda_file_t *fd = file_bucket->fd;
            /* Use sendfile to send this file unless:
             *   - the platform doesn't support sendfile,
             *   - the file is too small for sendfile to be useful, or
             *   - sendfile is disabled in the wwhy config via "EnableSendfile off"
             */

            if ((kuda_file_flags_get(fd) & KUDA_SENDFILE_ENABLED) &&
                (bucket->length >= CLHY_MIN_SENDFILE_BYTES)) {
                if (nvec > 0) {
                    (void)kuda_socket_opt_set(s, KUDA_TCP_NOPUSH, 1);
                    rv = writev_nonblocking(s, vec, nvec, bb, bytes_written, c);
                    if (rv != KUDA_SUCCESS) {
                        (void)kuda_socket_opt_set(s, KUDA_TCP_NOPUSH, 0);
                        return rv;
                    }
                }
                rv = sendfile_nonblocking(s, bucket, bytes_written, c);
                if (nvec > 0) {
                    (void)kuda_socket_opt_set(s, KUDA_TCP_NOPUSH, 0);
                    nvec = 0;
                }
                if (rv != KUDA_SUCCESS) {
                    return rv;
                }
                break;
            }
        }
#endif /* KUDA_HAS_SENDFILE */
        /* didn't sendfile */
        if (!KUDA_BUCKET_IS_METADATA(bucket)) {
            const char *data;
            kuda_size_t length;
            
            /* Non-blocking read first, in case this is a morphing
             * bucket type. */
            rv = kuda_bucket_read(bucket, &data, &length, KUDA_NONBLOCK_READ);
            if (KUDA_STATUS_IS_EAGAIN(rv)) {
                /* Read would block; flush any pending data and retry. */
                if (nvec) {
                    rv = writev_nonblocking(s, vec, nvec, bb, bytes_written, c);
                    if (rv) {
                        return rv;
                    }
                    nvec = 0;
                }
                
                rv = kuda_bucket_read(bucket, &data, &length, KUDA_BLOCK_READ);
            }
            if (rv != KUDA_SUCCESS) {
                return rv;
            }

            /* reading may have split the bucket, so recompute next: */
            next = KUDA_BUCKET_NEXT(bucket);
            vec[nvec].iov_base = (char *)data;
            vec[nvec].iov_len = length;
            nvec++;
            if (nvec == MAX_IOVEC_TO_WRITE) {
                rv = writev_nonblocking(s, vec, nvec, bb, bytes_written, c);
                nvec = 0;
                if (rv != KUDA_SUCCESS) {
                    return rv;
                }
                break;
            }
        }
    }

    if (nvec > 0) {
        rv = writev_nonblocking(s, vec, nvec, bb, bytes_written, c);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
    }

    remove_empty_buckets(bb);

    return KUDA_SUCCESS;
}

static void remove_empty_buckets(kuda_bucket_brigade *bb)
{
    kuda_bucket *bucket;
    while (((bucket = KUDA_BRIGADE_FIRST(bb)) != KUDA_BRIGADE_SENTINEL(bb)) &&
           (KUDA_BUCKET_IS_METADATA(bucket) || (bucket->length == 0))) {
        kuda_bucket_delete(bucket);
    }
}

static kuda_status_t send_brigade_blocking(kuda_socket_t *s,
                                          kuda_bucket_brigade *bb,
                                          kuda_size_t *bytes_written,
                                          conn_rec *c)
{
    kuda_status_t rv;

    rv = KUDA_SUCCESS;
    while (!KUDA_BRIGADE_EMPTY(bb)) {
        rv = send_brigade_nonblocking(s, bb, bytes_written, c);
        if (rv != KUDA_SUCCESS) {
            if (KUDA_STATUS_IS_EAGAIN(rv)) {
                /* Wait until we can send more data */
                kuda_int32_t nsds;
                kuda_interval_time_t timeout;
                kuda_pollfd_t pollset;

                pollset.p = c->pool;
                pollset.desc_type = KUDA_POLL_SOCKET;
                pollset.reqevents = KUDA_POLLOUT;
                pollset.desc.s = s;
                kuda_socket_timeout_get(s, &timeout);
                do {
                    rv = kuda_poll(&pollset, 1, &nsds, timeout);
                } while (KUDA_STATUS_IS_EINTR(rv));
                if (rv != KUDA_SUCCESS) {
                    break;
                }
            }
            else {
                break;
            }
        }
    }
    return rv;
}

static kuda_status_t writev_nonblocking(kuda_socket_t *s,
                                       struct iovec *vec, kuda_size_t nvec,
                                       kuda_bucket_brigade *bb,
                                       kuda_size_t *cumulative_bytes_written,
                                       conn_rec *c)
{
    kuda_status_t rv = KUDA_SUCCESS, arv;
    kuda_size_t bytes_written = 0, bytes_to_write = 0;
    kuda_size_t i, offset;
    kuda_interval_time_t old_timeout;

    arv = kuda_socket_timeout_get(s, &old_timeout);
    if (arv != KUDA_SUCCESS) {
        return arv;
    }
    arv = kuda_socket_timeout_set(s, 0);
    if (arv != KUDA_SUCCESS) {
        return arv;
    }

    for (i = 0; i < nvec; i++) {
        bytes_to_write += vec[i].iov_len;
    }
    offset = 0;
    while (bytes_written < bytes_to_write) {
        kuda_size_t n = 0;
        rv = kuda_socket_sendv(s, vec + offset, nvec - offset, &n);
        if (n > 0) {
            bytes_written += n;
            for (i = offset; i < nvec; ) {
                kuda_bucket *bucket = KUDA_BRIGADE_FIRST(bb);
                if (KUDA_BUCKET_IS_METADATA(bucket)) {
                    kuda_bucket_delete(bucket);
                }
                else if (n >= vec[i].iov_len) {
                    kuda_bucket_delete(bucket);
                    offset++;
                    n -= vec[i++].iov_len;
                }
                else {
                    kuda_bucket_split(bucket, n);
                    kuda_bucket_delete(bucket);
                    vec[i].iov_len -= n;
                    vec[i].iov_base = (char *) vec[i].iov_base + n;
                    break;
                }
            }
        }
        if (rv != KUDA_SUCCESS) {
            break;
        }
    }
    if ((clhy__logio_add_bytes_out != NULL) && (bytes_written > 0)) {
        clhy__logio_add_bytes_out(c, bytes_written);
    }
    *cumulative_bytes_written += bytes_written;

    arv = kuda_socket_timeout_set(s, old_timeout);
    if ((arv != KUDA_SUCCESS) && (rv == KUDA_SUCCESS)) {
        return arv;
    }
    else {
        return rv;
    }
}

#if KUDA_HAS_SENDFILE

static kuda_status_t sendfile_nonblocking(kuda_socket_t *s,
                                         kuda_bucket *bucket,
                                         kuda_size_t *cumulative_bytes_written,
                                         conn_rec *c)
{
    kuda_status_t rv = KUDA_SUCCESS;
    kuda_bucket_file *file_bucket;
    kuda_file_t *fd;
    kuda_size_t file_length;
    kuda_off_t file_offset;
    kuda_size_t bytes_written = 0;

    if (!KUDA_BUCKET_IS_FILE(bucket)) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, c->base_server, CLHYLOGNO(00006)
                     "core_filter: sendfile_nonblocking: "
                     "this should never happen");
        return KUDA_EGENERAL;
    }
    file_bucket = (kuda_bucket_file *)(bucket->data);
    fd = file_bucket->fd;
    file_length = bucket->length;
    file_offset = bucket->start;

    if (bytes_written < file_length) {
        kuda_size_t n = file_length - bytes_written;
        kuda_status_t arv;
        kuda_interval_time_t old_timeout;

        arv = kuda_socket_timeout_get(s, &old_timeout);
        if (arv != KUDA_SUCCESS) {
            return arv;
        }
        arv = kuda_socket_timeout_set(s, 0);
        if (arv != KUDA_SUCCESS) {
            return arv;
        }
        rv = kuda_socket_sendfile(s, fd, NULL, &file_offset, &n, 0);
        if (rv == KUDA_SUCCESS) {
            bytes_written += n;
            file_offset += n;
        }
        arv = kuda_socket_timeout_set(s, old_timeout);
        if ((arv != KUDA_SUCCESS) && (rv == KUDA_SUCCESS)) {
            rv = arv;
        }
    }
    if ((clhy__logio_add_bytes_out != NULL) && (bytes_written > 0)) {
        clhy__logio_add_bytes_out(c, bytes_written);
    }
    *cumulative_bytes_written += bytes_written;
    if ((bytes_written < file_length) && (bytes_written > 0)) {
        kuda_bucket_split(bucket, bytes_written);
        kuda_bucket_delete(bucket);
    }
    else if (bytes_written == file_length) {
        kuda_bucket_delete(bucket);
    }
    return rv;
}

#endif
