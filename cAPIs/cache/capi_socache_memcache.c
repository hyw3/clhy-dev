/* The cLHy Server
*  with
* this work for additional information regarding copyright ownership.
* The HLF licenses this file to You under the GNU GPL Version 3 or later
* (the "License"); you may not use this file except in compliance with
* the License.  You may obtain a copy of the License at
*
*     http://clhy.hyang.org/license.hyss
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


#include "wwhy.h"
#include "http_config.h"
#include "http_protocol.h"

#include "kuda.h"
#include "kudelman_version.h"

/* kuda_memcache support requires >= 1.3 */
#if KUDELMAN_MAJOR_VERSION > 1 || \
    (KUDELMAN_MAJOR_VERSION == 1 && KUDELMAN_MINOR_VERSION > 2)
#define HAVE_KUDELMAN_MEMCACHE 1
#endif

#ifdef HAVE_KUDELMAN_MEMCACHE

#include "clhy_socache.h"
#include "clhy_core.h"
#include "http_log.h"
#include "kuda_memcache.h"
#include "kuda_strings.h"
#include "capi_status.h"

/* The underlying kuda_memcache system is thread safe.. */
#define MC_KEY_LEN 254

#ifndef MC_DEFAULT_SERVER_PORT
#define MC_DEFAULT_SERVER_PORT 11211
#endif


#ifndef MC_DEFAULT_SERVER_MIN
#define MC_DEFAULT_SERVER_MIN 0
#endif

#ifndef MC_DEFAULT_SERVER_SMAX
#define MC_DEFAULT_SERVER_SMAX 1
#endif

#ifndef MC_DEFAULT_SERVER_TTL
#define MC_DEFAULT_SERVER_TTL    kuda_time_from_sec(15)
#endif

cAPI CLHY_CAPI_DECLARE_DATA socache_memcache_capi;

typedef struct {
    kuda_uint32_t ttl;
} socache_mc_svr_cfg;

struct clhy_socache_instance_t {
    const char *servers;
    kuda_memcache_t *mc;
    const char *tag;
    kuda_size_t taglen; /* strlen(tag) + 1 */
};

static const char *socache_mc_create(clhy_socache_instance_t **context,
                                     const char *arg,
                                     kuda_pool_t *tmp, kuda_pool_t *p)
{
    clhy_socache_instance_t *ctx;

    *context = ctx = kuda_palloc(p, sizeof *ctx);

    if (!arg || !*arg) {
        return "List of server names required to create memcache socache.";
    }

    ctx->servers = kuda_pstrdup(p, arg);

    return NULL;
}

static kuda_status_t socache_mc_init(clhy_socache_instance_t *ctx,
                                    const char *namespace,
                                    const struct clhy_socache_hints *hints,
                                    server_rec *s, kuda_pool_t *p)
{
    kuda_status_t rv;
    int thread_limit = 0;
    kuda_uint16_t nservers = 0;
    char *cache_config;
    char *split;
    char *tok;

    socache_mc_svr_cfg *sconf = clhy_get_capi_config(s->capi_config,
                                                     &socache_memcache_capi);

    clhy_clmp_query(CLHY_CLMPQ_HARD_LIMIT_THREADS, &thread_limit);

    /* Find all the servers in the first run to get a total count */
    cache_config = kuda_pstrdup(p, ctx->servers);
    split = kuda_strtok(cache_config, ",", &tok);
    while (split) {
        nservers++;
        split = kuda_strtok(NULL,",", &tok);
    }

    rv = kuda_memcache_create(p, nservers, 0, &ctx->mc);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(00785)
                     "Failed to create Memcache Object of '%d' size.",
                     nservers);
        return rv;
    }

    /* Now add each server to the memcache */
    cache_config = kuda_pstrdup(p, ctx->servers);
    split = kuda_strtok(cache_config, ",", &tok);
    while (split) {
        kuda_memcache_server_t *st;
        char *host_str;
        char *scope_id;
        kuda_port_t port;

        rv = kuda_parse_addr_port(&host_str, &scope_id, &port, split, p);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(00786)
                         "Failed to Parse memcache Server: '%s'", split);
            return rv;
        }

        if (host_str == NULL) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(00787)
                         "Failed to Parse Server, "
                         "no hostname specified: '%s'", split);
            return KUDA_EINVAL;
        }

        if (port == 0) {
            port = MC_DEFAULT_SERVER_PORT;
        }

        rv = kuda_memcache_server_create(p,
                                        host_str, port,
                                        MC_DEFAULT_SERVER_MIN,
                                        MC_DEFAULT_SERVER_SMAX,
                                        thread_limit,
                                        sconf->ttl,
                                        &st);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(00788)
                         "Failed to Create memcache Server: %s:%d",
                         host_str, port);
            return rv;
        }

        rv = kuda_memcache_add_server(ctx->mc, st);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(00789)
                         "Failed to Add memcache Server: %s:%d",
                         host_str, port);
            return rv;
        }

        split = kuda_strtok(NULL,",", &tok);
    }

    ctx->tag = kuda_pstrcat(p, namespace, ":", NULL);
    ctx->taglen = strlen(ctx->tag) + 1;

    /* socache API constraint: */
    CLHY_DEBUG_ASSERT(ctx->taglen <= 16);

    return KUDA_SUCCESS;
}

static void socache_mc_destroy(clhy_socache_instance_t *context, server_rec *s)
{
    /* noop. */
}

/* Converts (binary) id into a key prefixed by the predetermined
 * namespace tag; writes output to key buffer.  Returns non-zero if
 * the id won't fit in the key buffer. */
static int socache_mc_id2key(clhy_socache_instance_t *ctx,
                             const unsigned char *id, unsigned int idlen,
                             char *key, kuda_size_t keylen)
{
    char *cp;

    if (idlen * 2 + ctx->taglen >= keylen)
        return 1;

    cp = kuda_cpystrn(key, ctx->tag, ctx->taglen);
    clhy_bin2hex(id, idlen, cp);

    return 0;
}

static kuda_status_t socache_mc_store(clhy_socache_instance_t *ctx, server_rec *s,
                                     const unsigned char *id, unsigned int idlen,
                                     kuda_time_t expiry,
                                     unsigned char *ucaData, unsigned int nData,
                                     kuda_pool_t *p)
{
    char buf[MC_KEY_LEN];
    kuda_status_t rv;

    if (socache_mc_id2key(ctx, id, idlen, buf, sizeof buf)) {
        return KUDA_EINVAL;
    }

    /* memcache needs time in seconds till expiry; fail if this is not
     * positive *before* casting to unsigned (kuda_uint32_t). */
    expiry -= kuda_time_now();
    if (kuda_time_sec(expiry) <= 0) {
        return KUDA_EINVAL;
    }
    rv = kuda_memcache_set(ctx->mc, buf, (char*)ucaData, nData,
                          kuda_time_sec(expiry), 0);

    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(00790)
                     "scache_mc: error setting key '%s' "
                     "with %d bytes of data", buf, nData);
        return rv;
    }

    return KUDA_SUCCESS;
}

static kuda_status_t socache_mc_retrieve(clhy_socache_instance_t *ctx, server_rec *s,
                                        const unsigned char *id, unsigned int idlen,
                                        unsigned char *dest, unsigned int *destlen,
                                        kuda_pool_t *p)
{
    kuda_size_t data_len;
    char buf[MC_KEY_LEN], *data;
    kuda_status_t rv;

    if (socache_mc_id2key(ctx, id, idlen, buf, sizeof buf)) {
        return KUDA_EINVAL;
    }

    /* ### this could do with a subpool, but _getp looks like it will
     * eat memory like it's going out of fashion anyway. */

    rv = kuda_memcache_getp(ctx->mc, p, buf, &data, &data_len, NULL);
    if (rv) {
        if (rv != KUDA_NOTFOUND) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00791)
                         "scache_mc: 'retrieve' FAIL");
        }
        return rv;
    }
    else if (data_len > *destlen) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00792)
                     "scache_mc: 'retrieve' OVERFLOW");
        return KUDA_ENOMEM;
    }

    memcpy(dest, data, data_len);
    *destlen = data_len;

    return KUDA_SUCCESS;
}

static kuda_status_t socache_mc_remove(clhy_socache_instance_t *ctx, server_rec *s,
                                      const unsigned char *id,
                                      unsigned int idlen, kuda_pool_t *p)
{
    char buf[MC_KEY_LEN];
    kuda_status_t rv;

    if (socache_mc_id2key(ctx, id, idlen, buf, sizeof buf)) {
        return KUDA_EINVAL;
    }

    rv = kuda_memcache_delete(ctx->mc, buf, 0);

    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, s, CLHYLOGNO(00793)
                     "scache_mc: error deleting key '%s' ",
                     buf);
    }

    return rv;
}

static void socache_mc_status(clhy_socache_instance_t *ctx, request_rec *r, int flags)
{
    kuda_memcache_t *rc = ctx->mc;
    int i;

    for (i = 0; i < rc->ntotal; i++) {
        kuda_memcache_server_t *ms;
        kuda_memcache_stats_t *stats;
        kuda_status_t rv;
        char *br = (!(flags & CLHY_STATUS_SHORT) ? "<br />" : "");

        ms = rc->live_servers[i];

        clhy_rprintf(r, "Memcached server: %s:%d [%s]%s\n", ms->host, (int)ms->port,
                (ms->status == KUDA_MC_SERVER_LIVE) ? "Up" : "Down",
                br);
        rv = kuda_memcache_stats(ms, r->pool, &stats);
        if (rv != KUDA_SUCCESS)
            continue;
        if (!(flags & CLHY_STATUS_SHORT)) {
            clhy_rprintf(r, "<b>Version:</b> <i>%s</i> [%u bits], PID: <i>%u</i>, Uptime: <i>%u hrs</i> <br />\n",
                    stats->version , stats->pointer_size, stats->pid, stats->uptime/3600);
            clhy_rprintf(r, "<b>Clients::</b> Structures: <i>%u</i>, Total: <i>%u</i>, Current: <i>%u</i> <br />\n",
                    stats->connection_structures, stats->total_connections, stats->curr_connections);
            clhy_rprintf(r, "<b>Storage::</b> Total Items: <i>%u</i>, Current Items: <i>%u</i>, Bytes: <i>%" KUDA_UINT64_T_FMT "</i> <br />\n",
                    stats->total_items, stats->curr_items, stats->bytes);
            clhy_rprintf(r, "<b>CPU::</b> System: <i>%u</i>, User: <i>%u</i> <br />\n",
                    (unsigned)stats->rusage_system, (unsigned)stats->rusage_user );
            clhy_rprintf(r, "<b>Cache::</b> Gets: <i>%u</i>, Sets: <i>%u</i>, Hits: <i>%u</i>, Misses: <i>%u</i> <br />\n",
                    stats->cmd_get, stats->cmd_set, stats->get_hits, stats->get_misses);
            clhy_rprintf(r, "<b>Net::</b> Input bytes: <i>%" KUDA_UINT64_T_FMT "</i>, Output bytes: <i>%" KUDA_UINT64_T_FMT "</i> <br />\n",
                    stats->bytes_read, stats->bytes_written);
            clhy_rprintf(r, "<b>Misc::</b> Evictions: <i>%" KUDA_UINT64_T_FMT "</i>, MaxMem: <i>%u</i>, Threads: <i>%u</i> <br />\n",
                    stats->evictions, stats->limit_maxbytes, stats->threads);
            clhy_rputs("<hr><br />\n", r);
        }
        else {
            clhy_rprintf(r, "Version: %s [%u bits], PID: %u, Uptime: %u hrs %s\n",
                    stats->version , stats->pointer_size, stats->pid, stats->uptime/3600, br);
            clhy_rprintf(r, "Clients:: Structures: %d, Total: %d, Current: %u %s\n",
                    stats->connection_structures, stats->total_connections, stats->curr_connections, br);
            clhy_rprintf(r, "Storage:: Total Items: %u, Current Items: %u, Bytes: %" KUDA_UINT64_T_FMT " %s\n",
                    stats->total_items, stats->curr_items, stats->bytes, br);
            clhy_rprintf(r, "CPU:: System: %u, User: %u %s\n",
                    (unsigned)stats->rusage_system, (unsigned)stats->rusage_user , br);
            clhy_rprintf(r, "Cache:: Gets: %u, Sets: %u, Hits: %u, Misses: %u %s\n",
                    stats->cmd_get, stats->cmd_set, stats->get_hits, stats->get_misses, br);
            clhy_rprintf(r, "Net:: Input bytes: %" KUDA_UINT64_T_FMT ", Output bytes: %" KUDA_UINT64_T_FMT " %s\n",
                    stats->bytes_read, stats->bytes_written, br);
            clhy_rprintf(r, "Misc:: Evictions: %" KUDA_UINT64_T_FMT ", MaxMem: %u, Threads: %u %s\n",
                    stats->evictions, stats->limit_maxbytes, stats->threads, br);
        }
    }

}

static kuda_status_t socache_mc_iterate(clhy_socache_instance_t *instance,
                                       server_rec *s, void *userctx,
                                       clhy_socache_iterator_t *iterator,
                                       kuda_pool_t *pool)
{
    return KUDA_ENOTIMPL;
}

static const clhy_socache_provider_t socache_mc = {
    "memcache",
    0,
    socache_mc_create,
    socache_mc_init,
    socache_mc_destroy,
    socache_mc_store,
    socache_mc_retrieve,
    socache_mc_remove,
    socache_mc_status,
    socache_mc_iterate
};

#endif /* HAVE_KUDELMAN_MEMCACHE */

static void *create_server_config(kuda_pool_t *p, server_rec *s)
{
    socache_mc_svr_cfg *sconf = kuda_pcalloc(p, sizeof(socache_mc_svr_cfg));
    
    sconf->ttl = MC_DEFAULT_SERVER_TTL;

    return sconf;
}

static const char *socache_mc_set_ttl(cmd_parms *cmd, void *dummy,
                                      const char *arg)
{
    kuda_interval_time_t ttl;
    socache_mc_svr_cfg *sconf = clhy_get_capi_config(cmd->server->capi_config,
                                                     &socache_memcache_capi);

    if (clhy_timeout_parameter_parse(arg, &ttl, "s") != KUDA_SUCCESS) {
        return kuda_pstrcat(cmd->pool, cmd->cmd->name,
                           " has wrong format", NULL);
    }

    if ((ttl < kuda_time_from_sec(0)) || (ttl > kuda_time_from_sec(3600))) {
        return kuda_pstrcat(cmd->pool, cmd->cmd->name,
                           " can only be 0 or up to one hour.", NULL);
    }

    /* kuda_memcache_server_create needs a ttl in usec. */
    sconf->ttl = ttl;

    return NULL;
}

static void register_hooks(kuda_pool_t *p)
{
#ifdef HAVE_KUDELMAN_MEMCACHE
    clhy_register_provider(p, CLHY_SOCACHE_PROVIDER_GROUP, "memcache",
                         CLHY_SOCACHE_PROVIDER_VERSION,
                         &socache_mc);
#endif
}

static const command_rec socache_memcache_cmds[] = {
    CLHY_INIT_TAKE1("MemcacheConnTTL", socache_mc_set_ttl, NULL, RSRC_CONF,
                  "TTL used for the connection with the memcache server(s)"),
    { NULL }
};

CLHY_DECLARE_CAPI(socache_memcache) = {
    STANDARD16_CAPI_STUFF,
    NULL,                     /* create per-dir    config structures */
    NULL,                     /* merge  per-dir    config structures */
    create_server_config,     /* create per-server config structures */
    NULL,                     /* merge  per-server config structures */
    socache_memcache_cmds,    /* table of config file commands       */
    register_hooks            /* register hooks                      */
};
