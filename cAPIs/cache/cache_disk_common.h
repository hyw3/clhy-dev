/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file cache_disk_common.h
 * @brief Common Disk Cache vars/structs
 *
 * @defgroup Cache_cache  Cache Functions
 * @ingroup  CAPI_DISK_CACHE
 * @{
 */

#ifndef CACHE_DIST_COMMON_H
#define CACHE_DIST_COMMON_H

#define VARY_FORMAT_VERSION 5
#define DISK_FORMAT_VERSION 6

#define CACHE_HEADER_SUFFIX ".header"
#define CACHE_DATA_SUFFIX   ".data"
#define CACHE_VDIR_SUFFIX   ".vary"

#define CLHY_TEMPFILE_PREFIX "/"
#define CLHY_TEMPFILE_BASE   "aptmp"
#define CLHY_TEMPFILE_SUFFIX "XXXXXX"
#define CLHY_TEMPFILE_BASELEN strlen(CLHY_TEMPFILE_BASE)
#define CLHY_TEMPFILE_NAMELEN strlen(CLHY_TEMPFILE_BASE CLHY_TEMPFILE_SUFFIX)
#define CLHY_TEMPFILE CLHY_TEMPFILE_PREFIX CLHY_TEMPFILE_BASE CLHY_TEMPFILE_SUFFIX

typedef struct {
    /* Indicates the format of the header struct stored on-disk. */
    kuda_uint32_t format;
    /* The HTTP status code returned for this response.  */
    int status;
    /* The size of the entity name that follows. */
    kuda_size_t name_len;
    /* The number of times we've cached this entity. */
    kuda_size_t entity_version;
    /* Miscellaneous time values. */
    kuda_time_t date;
    kuda_time_t expire;
    kuda_time_t request_time;
    kuda_time_t response_time;
    /* The ident of the body file, so we can test the body matches the header */
    kuda_ino_t inode;
    kuda_dev_t device;
    /* Does this cached request have a body? */
    unsigned int has_body:1;
    unsigned int header_only:1;
    /* The parsed cache control header */
    cache_control_t control;
} disk_cache_info_t;

#endif /* CACHE_DIST_COMMON_H */
/** @} */
