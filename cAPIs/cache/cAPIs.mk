capi_file_cache.la: capi_file_cache.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_file_cache.lo $(CAPI_FILE_CACHE_LDADD)
capi_cache.la: capi_cache.slo cache_storage.slo cache_util.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_cache.lo cache_storage.lo cache_util.lo  $(CAPI_CACHE_LDADD)
capi_cache_disk.la: capi_cache_disk.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_cache_disk.lo $(CAPI_CACHE_DISK_LDADD)
capi_cache_socache.la: capi_cache_socache.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_cache_socache.lo $(CAPI_CACHE_SOCACHE_LDADD)
capi_socache_shmcb.la: capi_socache_shmcb.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_socache_shmcb.lo $(CAPI_SOCACHE_SHMCB_LDADD)
capi_socache_dbm.la: capi_socache_dbm.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_socache_dbm.lo $(CAPI_SOCACHE_DBM_LDADD)
capi_socache_memcache.la: capi_socache_memcache.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_socache_memcache.lo $(CAPI_SOCACHE_MEMCACHE_LDADD)
capi_socache_redis.la: capi_socache_redis.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_socache_redis.lo $(CAPI_SOCACHE_REDIS_LDADD)
DISTCLEAN_TARGETS = cAPIs.mk
static = 
shared =  capi_file_cache.la capi_cache.la capi_cache_disk.la capi_cache_socache.la capi_socache_shmcb.la capi_socache_dbm.la capi_socache_memcache.la capi_socache_redis.la
