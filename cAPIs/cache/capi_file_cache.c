/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Author: capi_file_cache by Bill Stoddard <stoddard hyang.org>
 *         Based on capi_mmap_static by Dean Gaudet <dgaudet arctic.org>
 *
 * v0.01: initial implementation
 */

/*
    Documentation:

    Some sites have a set of static files that are really busy, and
    change infrequently (or even on a regular schedule). Save time
    by caching open handles to these files. This cAPI, unlike
    capi_mmap_static, caches open file handles, not file content.
    On systems (like Windows) with heavy system call overhead and
    that have an efficient sendfile implementation, caching file handles
    offers several advantages over caching content. First, the file system
    can manage the memory, allowing infrequently hit cached files to
    be paged out. Second, since caching open handles does not consume
    significant resources, it will be possible to enable an AutoLoadCache
    feature where static files are dynamically loaded in the cache
    as the server runs. On systems that have file change notification,
    this cAPI can be enhanced to automatically garbage collect
    cached files that change on disk.

    This cAPI should work on Unix systems that have sendfile. Place
    cachefile directives into your configuration to direct files to
    be cached.

        cachefile /path/to/file1
        cachefile /path/to/file2
        ...

    These files are only cached when the server is restarted, so if you
    change the list, or if the files are changed, then you'll need to
    restart the server.

    To reiterate that point:  if the files are modified *in place*
    without restarting the server you may end up serving requests that
    are completely bogus.  You should update files by unlinking the old
    copy and putting a new copy in place.

    There's no such thing as inheriting these files across vhosts or
    whatever... place the directives in the main server only.

    Known problems:

    Don't use Alias or RewriteRule to move these files around...  unless
    you feel like paying for an extra stat() on each request.  This is
    a deficiency in the cLHy API that will hopefully be solved some day.
    The file will be served out of the file handle cache, but there will be
    an extra stat() that's a waste.
*/

#include "kuda.h"

#if !(KUDA_HAS_SENDFILE || KUDA_HAS_MMAP)
#error capi_file_cache only works on systems with KUDA_HAS_SENDFILE or KUDA_HAS_MMAP
#endif

#include "kuda_mmap.h"
#include "kuda_strings.h"
#include "kuda_hash.h"
#include "kuda_buckets.h"

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_request.h"
#include "http_core.h"

cAPI CLHY_CAPI_DECLARE_DATA file_cache_capi;

typedef struct {
#if KUDA_HAS_SENDFILE
    kuda_file_t *file;
#endif
    const char *filename;
    kuda_finfo_t finfo;
    int is_mmapped;
#if KUDA_HAS_MMAP
    kuda_mmap_t *mm;
#endif
    char mtimestr[KUDA_RFC822_DATE_LEN];
    char sizestr[21];   /* big enough to hold any 64-bit file size + null */
} a_file;

typedef struct {
    kuda_hash_t *fileht;
} a_server_config;


static void *create_server_config(kuda_pool_t *p, server_rec *s)
{
    a_server_config *sconf = kuda_palloc(p, sizeof(*sconf));

    sconf->fileht = kuda_hash_make(p);
    return sconf;
}

static void cache_the_file(cmd_parms *cmd, const char *filename, int mmap)
{
    a_server_config *sconf;
    a_file *new_file;
    a_file tmp;
    kuda_file_t *fd = NULL;
    kuda_status_t rc;
    const char *fspec;

    fspec = clhy_server_root_relative(cmd->pool, filename);
    if (!fspec) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, KUDA_EBADPATH, cmd->server, CLHYLOGNO(00794)
                     "invalid file path "
                     "%s, skipping", filename);
        return;
    }
    if ((rc = kuda_stat(&tmp.finfo, fspec, KUDA_FINFO_MIN,
                                 cmd->temp_pool)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rc, cmd->server, CLHYLOGNO(00795)
                     "unable to stat(%s), skipping", fspec);
        return;
    }
    if (tmp.finfo.filetype != KUDA_REG) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, cmd->server, CLHYLOGNO(00796)
                     "%s isn't a regular file, skipping", fspec);
        return;
    }
    if (tmp.finfo.size > CLHY_MAX_SENDFILE) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, cmd->server, CLHYLOGNO(00797)
                     "%s is too large to cache, skipping", fspec);
        return;
    }

    rc = kuda_file_open(&fd, fspec, KUDA_READ | KUDA_BINARY | KUDA_XTHREAD,
                       KUDA_PLATFORM_DEFAULT, cmd->pool);
    if (rc != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rc, cmd->server, CLHYLOGNO(00798)
                     "unable to open(%s, O_RDONLY), skipping", fspec);
        return;
    }
    kuda_file_inherit_set(fd);

    /* WooHoo, we have a file to put in the cache */
    new_file = kuda_pcalloc(cmd->pool, sizeof(a_file));
    new_file->finfo = tmp.finfo;

#if KUDA_HAS_MMAP
    if (mmap) {
        /* MMAPFile directive. MMAP'ing the file
         * XXX: KUDA_HAS_LARGE_FILES issue; need to reject this request if
         * size is greater than MAX(kuda_size_t) (perhaps greater than 1M?).
         */
        if ((rc = kuda_mmap_create(&new_file->mm, fd, 0,
                                  (kuda_size_t)new_file->finfo.size,
                                  KUDA_MMAP_READ, cmd->pool)) != KUDA_SUCCESS) {
            kuda_file_close(fd);
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rc, cmd->server, CLHYLOGNO(00799)
                         "unable to mmap %s, skipping", filename);
            return;
        }
        kuda_file_close(fd);
        new_file->is_mmapped = TRUE;
    }
#endif
#if KUDA_HAS_SENDFILE
    if (!mmap) {
        /* CacheFile directive. Caching the file handle */
        new_file->is_mmapped = FALSE;
        new_file->file = fd;
    }
#endif

    new_file->filename = fspec;
    kuda_rfc822_date(new_file->mtimestr, new_file->finfo.mtime);
    kuda_snprintf(new_file->sizestr, sizeof new_file->sizestr, "%" KUDA_OFF_T_FMT, new_file->finfo.size);

    sconf = clhy_get_capi_config(cmd->server->capi_config, &file_cache_capi);
    kuda_hash_set(sconf->fileht, new_file->filename, strlen(new_file->filename), new_file);

}

static const char *cachefilehandle(cmd_parms *cmd, void *dummy, const char *filename)
{
#if KUDA_HAS_SENDFILE
    cache_the_file(cmd, filename, 0);
#else
    /* Sendfile not supported by this PLATFORM */
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, cmd->server, CLHYLOGNO(00800)
                 "unable to cache file: %s. Sendfile is not supported on this PLATFORM", filename);
#endif
    return NULL;
}
static const char *cachefilemmap(cmd_parms *cmd, void *dummy, const char *filename)
{
#if KUDA_HAS_MMAP
    cache_the_file(cmd, filename, 1);
#else
    /* MMAP not supported by this PLATFORM */
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, cmd->server, CLHYLOGNO(00801)
                 "unable to cache file: %s. MMAP is not supported by this PLATFORM", filename);
#endif
    return NULL;
}

static int file_cache_post_config(kuda_pool_t *p, kuda_pool_t *plog,
                                   kuda_pool_t *ptemp, server_rec *s)
{
    /* Hummm, anything to do here? */
    return OK;
}

/* If it's one of ours, fill in r->finfo now to avoid extra stat()... this is a
 * bit of a kludge, because we really want to run after core_translate runs.
 */
static int file_cache_xlat(request_rec *r)
{
    a_server_config *sconf;
    a_file *match;
    int res;

    sconf = clhy_get_capi_config(r->server->capi_config, &file_cache_capi);

    /* we only operate when at least one cachefile directive was used */
    if (!kuda_hash_count(sconf->fileht)) {
        return DECLINED;
    }

    res = clhy_core_translate(r);
    if (res != OK || !r->filename) {
        return res;
    }

    /* search the cache */
    match = (a_file *) kuda_hash_get(sconf->fileht, r->filename, KUDA_HASH_KEY_STRING);
    if (match == NULL)
        return DECLINED;

    /* pass search results to handler */
    clhy_set_capi_config(r->request_config, &file_cache_capi, match);

    /* shortcircuit the get_path_info() stat() calls and stuff */
    r->finfo = match->finfo;
    return OK;
}

static int mmap_handler(request_rec *r, a_file *file)
{
#if KUDA_HAS_MMAP
    conn_rec *c = r->connection;
    kuda_bucket *b;
    kuda_mmap_t *mm;
    kuda_bucket_brigade *bb = kuda_brigade_create(r->pool, c->bucket_alloc);

    kuda_mmap_dup(&mm, file->mm, r->pool);
    b = kuda_bucket_mmap_create(mm, 0, (kuda_size_t)file->finfo.size,
                               c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bb, b);
    b = kuda_bucket_eos_create(c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bb, b);

    if (clhy_pass_brigade(r->output_filters, bb) != KUDA_SUCCESS)
        return CLHY_FILTER_ERROR;
#endif
    return OK;
}

static int sendfile_handler(request_rec *r, a_file *file)
{
#if KUDA_HAS_SENDFILE
    conn_rec *c = r->connection;
    kuda_bucket *b;
    kuda_bucket_brigade *bb = kuda_brigade_create(r->pool, c->bucket_alloc);

    kuda_brigade_insert_file(bb, file->file, 0, file->finfo.size, r->pool);

    b = kuda_bucket_eos_create(c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bb, b);

    if (clhy_pass_brigade(r->output_filters, bb) != KUDA_SUCCESS)
        return CLHY_FILTER_ERROR;
#endif
    return OK;
}

static int file_cache_handler(request_rec *r)
{
    a_file *match;
    int errstatus;
    int rc = OK;

    /* Bail out if r->handler isn't the default value, and doesn't look like a Content-Type
     * XXX: Even though we made the user explicitly list each path to cache?
     */
    if (clhy_strcmp_match(r->handler, "*/*") && !CLHY_IS_DEFAULT_HANDLER_NAME(r->handler)) {
        return DECLINED;
    }

    /* we don't handle anything but GET */
    if (r->method_number != M_GET) return DECLINED;

    /* did xlat phase find the file? */
    match = clhy_get_capi_config(r->request_config, &file_cache_capi);

    if (match == NULL) {
        return DECLINED;
    }

    /* note that we would handle GET on this resource */
    r->allowed |= (CLHY_METHOD_BIT << M_GET);

    /* This handler has no use for a request body (yet), but we still
     * need to read and discard it if the client sent one.
     */
    if ((errstatus = clhy_discard_request_body(r)) != OK)
        return errstatus;

    clhy_update_mtime(r, match->finfo.mtime);

    /* clhy_set_last_modified() always converts the file mtime to a string
     * which is slow.  Accelerate the common case.
     * clhy_set_last_modified(r);
     */
    {
        kuda_time_t capi_time;
        char *datestr;

        capi_time = clhy_rationalize_mtime(r, r->mtime);
        if (capi_time == match->finfo.mtime)
            datestr = match->mtimestr;
        else {
            datestr = kuda_palloc(r->pool, KUDA_RFC822_DATE_LEN);
            kuda_rfc822_date(datestr, capi_time);
        }
        kuda_table_setn(r->headers_out, "Last-Modified", datestr);
    }

    /* clhy_set_content_length() always converts the same number and never
     * returns an error.  Accelerate it.
     */
    r->clength = match->finfo.size;
    kuda_table_setn(r->headers_out, "Content-Length", match->sizestr);

    clhy_set_etag(r);
    if ((errstatus = clhy_meets_conditions(r)) != OK) {
       return errstatus;
    }

    /* Call appropriate handler */
    if (!r->header_only) {
        if (match->is_mmapped == TRUE)
            rc = mmap_handler(r, match);
        else
            rc = sendfile_handler(r, match);
    }

    return rc;
}

static command_rec file_cache_cmds[] =
{
CLHY_INIT_ITERATE("cachefile", cachefilehandle, NULL, RSRC_CONF,
     "A space separated list of files to add to the file handle cache at config time"),
CLHY_INIT_ITERATE("mmapfile", cachefilemmap, NULL, RSRC_CONF,
     "A space separated list of files to mmap at config time"),
    {NULL}
};

static void register_hooks(kuda_pool_t *p)
{
    clhy_hook_handler(file_cache_handler, NULL, NULL, KUDA_HOOK_LAST);
    clhy_hook_post_config(file_cache_post_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_translate_name(file_cache_xlat, NULL, NULL, KUDA_HOOK_MIDDLE);
    /* This trick doesn't work apparently because the translate hooks
       are single shot. If the core_hook returns OK, then our hook is
       not called.
    clhy_hook_translate_name(file_cache_xlat, aszPre, NULL, KUDA_HOOK_MIDDLE);
    */

}

CLHY_DECLARE_CAPI(file_cache) =
{
    STANDARD16_CAPI_STUFF,
    NULL,                     /* create per-directory config structure */
    NULL,                     /* merge per-directory config structures */
    create_server_config,     /* create per-server config structure */
    NULL,                     /* merge per-server config structures */
    file_cache_cmds,          /* command handlers */
    register_hooks            /* register hooks */
};
