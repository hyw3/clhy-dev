/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_log.h"
#include "http_request.h"
#include "http_config.h"
#include "http_protocol.h"
#include "capi_status.h"

#include "kuda_strings.h"
#include "kuda_time.h"

#include "clhy_socache.h"

#include "distcache/dc_client.h"

#if !defined(DISTCACHE_CLIENT_API) || (DISTCACHE_CLIENT_API < 0x0001)
#error "You must compile with a more recent version of the distcache-base package"
#endif

struct clhy_socache_instance_t {
    /* Configured target server: */
    const char *target;
    /* distcache client context: */
    DC_CTX *dc;
};

static const char *socache_dc_create(clhy_socache_instance_t **context,
                                     const char *arg,
                                     kuda_pool_t *tmp, kuda_pool_t *p)
{
    struct clhy_socache_instance_t *ctx;

    ctx = *context = kuda_palloc(p, sizeof *ctx);

    ctx->target = kuda_pstrdup(p, arg);

    return NULL;
}

static kuda_status_t socache_dc_init(clhy_socache_instance_t *ctx,
                                    const char *namespace,
                                    const struct clhy_socache_hints *hints,
                                    server_rec *s, kuda_pool_t *p)
{
#if 0
    /* If a "persistent connection" mode of operation is preferred, you *must*
     * also use the PIDCHECK flag to ensure fork()'d processes don't interlace
     * comms on the same connection as each other. */
#define SESSION_CTX_FLAGS        SESSION_CTX_FLAG_PERSISTENT | \
                                 SESSION_CTX_FLAG_PERSISTENT_PIDCHECK | \
                                 SESSION_CTX_FLAG_PERSISTENT_RETRY | \
                                 SESSION_CTX_FLAG_PERSISTENT_LATE
#else
    /* This mode of operation will open a temporary connection to the 'target'
     * for each cache operation - this makes it safe against fork()
     * automatically. This mode is preferred when running a local proxy (over
     * unix domain sockets) because overhead is negligable and it reduces the
     * performance/stability danger of file-descriptor bloatage. */
#define SESSION_CTX_FLAGS        0
#endif
    ctx->dc = DC_CTX_new(ctx->target, SESSION_CTX_FLAGS);
    if (!ctx->dc) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00738) "distributed scache failed to obtain context");
        return KUDA_EGENERAL;
    }
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, s, CLHYLOGNO(00739) "distributed scache context initialised");

    return KUDA_SUCCESS;
}

static void socache_dc_destroy(clhy_socache_instance_t *ctx, server_rec *s)
{
    if (ctx && ctx->dc) {
        DC_CTX_free(ctx->dc);
        ctx->dc = NULL;
    }
}

static kuda_status_t socache_dc_store(clhy_socache_instance_t *ctx, server_rec *s,
                                     const unsigned char *id, unsigned int idlen,
                                     kuda_time_t expiry,
                                     unsigned char *der, unsigned int der_len,
                                     kuda_pool_t *p)
{
    /* !@#$%^ - why do we deal with *absolute* time anyway???
     * Uhm - because most things expire things at a specific time?
     * Were the API were thought out expiry - r->request_time is a good approximation
     */
    expiry -= kuda_time_now();
    /* Send the serialised session to the distributed cache context */
    if (!DC_CTX_add_session(ctx->dc, id, idlen, der, der_len,
                            kuda_time_msec(expiry))) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00740) "distributed scache 'store' failed");
        return KUDA_EGENERAL;
    }
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00741) "distributed scache 'store' successful");
    return KUDA_SUCCESS;
}

static kuda_status_t socache_dc_retrieve(clhy_socache_instance_t *ctx, server_rec *s,
                                        const unsigned char *id, unsigned int idlen,
                                        unsigned char *dest, unsigned int *destlen,
                                        kuda_pool_t *p)
{
    unsigned int data_len;

    /* Retrieve any corresponding session from the distributed cache context */
    if (!DC_CTX_get_session(ctx->dc, id, idlen, dest, *destlen, &data_len)) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00742) "distributed scache 'retrieve' MISS");
        return KUDA_NOTFOUND;
    }
    if (data_len > *destlen) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00743) "distributed scache 'retrieve' OVERFLOW");
        return KUDA_ENOSPC;
    }
    *destlen = data_len;
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00744) "distributed scache 'retrieve' HIT");
    return KUDA_SUCCESS;
}

static kuda_status_t socache_dc_remove(clhy_socache_instance_t *ctx,
                                      server_rec *s, const unsigned char *id,
                                      unsigned int idlen, kuda_pool_t *p)
{
    /* Remove any corresponding session from the distributed cache context */
    if (!DC_CTX_remove_session(ctx->dc, id, idlen)) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00745) "distributed scache 'remove' MISS");
        return KUDA_NOTFOUND;
    }
    else {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00746) "distributed scache 'remove' HIT");
        return KUDA_SUCCESS;
    }
}

static void socache_dc_status(clhy_socache_instance_t *ctx, request_rec *r, int flags)
{
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00747)
                  "distributed scache 'socache_dc_status'");
    if (!(flags & CLHY_STATUS_SHORT)) {
        clhy_rprintf(r, "cache type: <b>DC (Distributed Cache)</b>, "
                   " target: <b>%s</b><br>", ctx->target);
    }
    else {
        clhy_rputs("CacheType: DC\n", r);
        clhy_rvputs(r, "CacheTarget: ", ctx->target, "\n", NULL);
    }
}

static kuda_status_t socache_dc_iterate(clhy_socache_instance_t *instance,
                                       server_rec *s, void *userctx,
                                       clhy_socache_iterator_t *iterator,
                                       kuda_pool_t *pool)
{
    return KUDA_ENOTIMPL;
}

static const clhy_socache_provider_t socache_dc = {
    "distcache",
    0,
    socache_dc_create,
    socache_dc_init,
    socache_dc_destroy,
    socache_dc_store,
    socache_dc_retrieve,
    socache_dc_remove,
    socache_dc_status,
    socache_dc_iterate
};

static void register_hooks(kuda_pool_t *p)
{
    clhy_register_provider(p, CLHY_SOCACHE_PROVIDER_GROUP, "dc",
                         CLHY_SOCACHE_PROVIDER_VERSION,
                         &socache_dc);
}

CLHY_DECLARE_CAPI(socache_dc) = {
    STANDARD16_CAPI_STUFF,
    NULL, NULL, NULL, NULL, NULL,
    register_hooks
};

