/* The cLHy Server
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * capi_socache_neuro
 * The cAPI to interface NeuroCache client
 *  _   _                       ____           _
 * | \ | | ___ _   _ _ __ ___  / ___|__ _  ___| |__   ___
 * |  \| |/ _ \ | | | '__/ _ \| |   / _` |/ __| '_ \ / _ \
 * | |\  |  __/ |_| | | | (_) | |__| (_| | (__| | | |  __/
 * |_| \_|\___|\__,_|_|  \___/ \____\__,_|\___|_| |_|\___|
 *
 * Written by Hilman P. Alisabana <alisabana@hyang.org>
 *
 * The NeuroCache source package and all the libraries and utilities
 * are available at
 *
 *    http://clhy.hyang.org/archives/neurocache
 *
 * and the source tree is versioned using HySCM and is available at
 *
 *   http://clhy.hyang.org/cgi-bin/neurocache-dev/neurocache
 *
 */

#include "wwhy.h"
#include "http_log.h"
#include "http_request.h"
#include "http_config.h"
#include "http_protocol.h"
#include "capi_status.h"
#include "kuda_strings.h"
#include "kuda_time.h"
#include "clhy_socache.h"

/* The neurocache and neural library API */
#include "neurocache/nsc_client.h"
#include "libneural/neural.h"
#include "libneural/neural_build.h"

/* The following is an API version that will be bumped each time the API change.
 * This version number does not track changes to the functions implemented in the
 * client header library that might affect binary or program compatibility. It merely
 * provides a way for dependant source code to provide pre-processing rules to ensure
 * that source code is being compiled using an acceptable version of NeuroCache.
 */

#if !defined(NEUROCACHE_CLIENT_API) || (NEUROCACHE_CLIENT_API < 0x0001)
#error "Please build NeuroCache package with a more recent version"
#endif

struct clhy_socache_instance_t {
    /* Configured target server */
    const char *target;
    /* The black-box type of neurocache client context */
    NSC_CTX *neuro;
};

static const char *socache_neuro_create(clhy_socache_instance_t **context,
                                        const char *arg,
                                        kuda_pool_t *tmp, kuda_pool_t *p)
{
    struct clhy_socache_instance_t *ctx;
    ctx = *context = kuda_palloc(p, sizeof *ctx);
    ctx->target = kuda_pstrdup(p, arg);
    return NULL;
}

static kuda_status_t socache_neuro_init(clhy_socache_instance_t *ctx,
                                        const char *namespace,
                                        const struct clhy_socache_hints *hints,
                                        server_rec *s, kuda_pool_t *p)
{
#if 0
    /* The PIDCHECK flag must be used if the mode operation is preferred using
     * a single persistent connetion. This can ensure that fork() processes not
     * interlace on the same connection as each other. */
#define SESSION_CTX_FLAGS        SESSION_CTX_FLAG_PERSISTENT | \
                                 SESSION_CTX_FLAG_PERSISTENT_PIDCHECK | \
                                 SESSION_CTX_FLAG_PERSISTENT_RETRY | \
                                 SESSION_CTX_FLAG_PERSISTENT_LATE
#else
#define SESSION_CTX_FLAGS        0
#endif
    ctx->neuro = NSC_CTX_new(ctx->target, SESSION_CTX_FLAGS);
    if (!ctx->neuro) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00738) "NeuroCache failed to obtain context");
        return KUDA_EGENERAL;
    }
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, s, CLHYLOGNO(00739) "NeuroCache context initialised");

    return KUDA_SUCCESS;
}

static void socache_neuro_destroy(clhy_socache_instance_t *ctx, server_rec *s)
{
    if (ctx && ctx->neuro) {
        NSC_CTX_free(ctx->neuro);
        ctx->neuro = NULL;
    }
}

static kuda_status_t socache_neuro_store(clhy_socache_instance_t *ctx, server_rec *s,
                                         const unsigned char *id, unsigned int idlen,
                                         kuda_time_t expiry,
                                         unsigned char *der, unsigned int der_len,
                                         kuda_pool_t *p)
{
    expiry -= kuda_time_now();
    if (!NSC_CTX_add_session(ctx->neuro, id, idlen, der, der_len,
                             kuda_time_msec(expiry))) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00740) "NeuroCache 'store' failed");
        return KUDA_EGENERAL;
    }
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00741) "NeuroCache 'store' successful");
    return KUDA_SUCCESS;
}

static kuda_status_t socache_neuro_retrieve(clhy_socache_instance_t *ctx, server_rec *s,
                                            const unsigned char *id, unsigned int idlen,
                                            unsigned char *dest, unsigned int *destlen,
                                            kuda_pool_t *p)
{
    unsigned int data_len;
    if (!NSC_CTX_get_session(ctx->neuro, id, idlen, dest, *destlen, &data_len)) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00742) "NeuroCache 'retrieve' MISS");
        return KUDA_NOTFOUND;
    }
    if (data_len > *destlen) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00743) "NeuroCache 'retrieve' OVERFLOW");
        return KUDA_ENOSPC;
    }
    *destlen = data_len;
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00744) "NeuroCache 'retrieve' HIT");
    return KUDA_SUCCESS;
}

static kuda_status_t socache_neuro_remove(clhy_socache_instance_t *ctx,
                                          server_rec *s, const unsigned char *id,
                                          unsigned int idlen, kuda_pool_t *p)
{
    if (!NSC_CTX_remove_session(ctx->neuro, id, idlen)) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00745) "NeuroCache 'remove' MISS");
        return KUDA_NOTFOUND;
    }
    else {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00746) "NeuroCache 'remove' HIT");
        return KUDA_SUCCESS;
    }
}

static void socache_neuro_status(clhy_socache_instance_t *ctx, request_rec *r, int flags)
{
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00747)
                  "NeuroCache 'socache_neuro_status'");
    if (!(flags & CLHY_STATUS_SHORT)) {
        clhy_rprintf(r, "cache type: <b>NSC (Neuro Session Cache)</b>, "
                   " target: <b>%s</b><br>", ctx->target);
    }
    else {
        clhy_rputs("CacheType: NSC\n", r);
        clhy_rvputs(r, "CacheTarget: ", ctx->target, "\n", NULL);
    }
}

static kuda_status_t socache_neuro_iterate(clhy_socache_instance_t *instance,
                                           server_rec *s, void *userctx,
                                           clhy_socache_iterator_t *iterator,
                                           kuda_pool_t *pool)
{
    return KUDA_ENOTIMPL;
}

static const clhy_socache_provider_t socache_neuro = {
    "neurocache",
    0,
    socache_neuro_create,
    socache_neuro_init,
    socache_neuro_destroy,
    socache_neuro_store,
    socache_neuro_retrieve,
    socache_neuro_remove,
    socache_neuro_status,
    socache_neuro_iterate
};

static void register_hooks(kuda_pool_t *p)
{
    clhy_register_provider(p, CLHY_SOCACHE_PROVIDER_GROUP, "neuro",
                           CLHY_SOCACHE_PROVIDER_VERSION,
                           &socache_neuro);
}

CLHY_DECLARE_CAPI(socache_neuro) = {
    STANDARD16_CAPI_STUFF,
    NULL, NULL, NULL, NULL, NULL,
    register_hooks
};

