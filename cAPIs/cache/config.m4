dnl cAPIs enabled in this directory by default

dnl CLHYKUDEL_CAPI(name, helptext[, objects[, structname[, default[, config]]]])

CLHYKUDEL_CAPIPATH_INIT(cache)

CLHYKUDEL_CAPI(file_cache, File cache, , , most)

dnl #  list of object files for capi_cache
cache_objs="dnl
capi_cache.lo dnl
cache_storage.lo dnl
cache_util.lo dnl
"
cache_disk_objs="capi_cache_disk.lo"
cache_socache_objs="capi_cache_socache.lo"

case "$host" in
  *os2*)
    # OS2 DLLs must resolve all symbols at build time
    # and we need some from main cache cAPI
    cache_disk_objs="$cache_disk_objs capi_cache.la"
    cache_socache_objs="$cache_socache_objs capi_cache.la"
    ;;
esac

CLHYKUDEL_CAPI(cache, dynamic file caching.  At least one storage management cAPI (e.g. capi_cache_disk) is also necessary., $cache_objs, , most)
CLHYKUDEL_CAPI(cache_disk, disk caching cAPI, $cache_disk_objs, , most, , cache)
CLHYKUDEL_CAPI(cache_socache, shared object caching cAPI, $cache_socache_objs, , most)

dnl
dnl CLHYKUDEL_CHECK_NEUROCACHE
dnl
dnl Detect the NeuroCache installation on cLHy configuration time, and
dnl give preference to "--with-neurocache=<path>" if it was specified.
dnl
AC_DEFUN([CLHYKUDEL_CHECK_NEUROCACHE],[
if test "x$clhy_neurocache_configured" = "x"; then
  dnl initialise the variables we use
  clhy_neurocache_found=""
  clhy_neurocache_base=""
  clhy_neurocache_libs=""
  clhy_neurocache_ldflags=""
  clhy_neurocache_with=""

  dnl Determine the neurocache directory, if any
  AC_MSG_CHECKING([for user-provided neurocache package])
  AC_ARG_WITH(neurocache, CLHYKUDEL_HELP_STRING(--with-neurocache=PATH, NeuroCache installation directory), [
    dnl If --with-neurocache specifies a directory, we use that directory or fail
    if test "x$withval" != "xyes" -a "x$withval" != "x"; then
      dnl This ensures $withval is actually a directory and that it is absolute
      clhy_neurocache_with="yes"
      clhy_neurocache_base="`cd $withval ; pwd`"
    fi
  ])
  if test "x$clhy_neurocache_base" = "x"; then
    AC_MSG_RESULT(none)
  else
    AC_MSG_RESULT($clhy_neurocache_base)
  fi

  dnl Run header and version checks
  saved_CPPFLAGS="$CPPFLAGS"
  saved_LIBS="$LIBS"
  saved_LDFLAGS="$LDFLAGS"

  if test "x$clhy_neurocache_base" != "x"; then
    KUDA_ADDTO(CPPFLAGS, [-I$clhy_neurocache_base/include])
    KUDA_ADDTO(CAPI_INCLUDES, [-I$clhy_neurocache_base/include])
    KUDA_ADDTO(LDFLAGS, [-L$clhy_neurocache_base/lib])
    KUDA_ADDTO(clhy_neurocache_ldflags, [-L$clhy_neurocache_base/lib])
    if test "x$clhy_platform_runtime_link_flag" != "x"; then
      KUDA_ADDTO(LDFLAGS, [$clhy_platform_runtime_link_flag$clhy_neurocache_base/lib])
      KUDA_ADDTO(clhy_neurocache_ldflags, [$clhy_platform_runtime_link_flag$clhy_neurocache_base/lib])
    fi
  fi
  dnl First check for mandatory headers
  AC_CHECK_HEADERS([neurocache/nsc_client.h], [clhy_neurocache_found="yes"], [])
  if test "$clhy_neurocache_found" = "yes"; then
    dnl test for a good version
    AC_MSG_CHECKING(for neurocache version)
    AC_TRY_COMPILE([#include <neurocache/nsc_client.h>],[
#if NEUROCACHE_CLIENT_API != 0x0001
#error "NeuroCache API version is unrecognised"
#endif],
      [],
      [clhy_neurocache_found="no"])
    AC_MSG_RESULT($clhy_neurocache_found)
  fi
  if test "$clhy_neurocache_found" != "yes"; then
    if test "x$clhy_neurocache_with" = "x"; then
      AC_MSG_WARN([...No neurocache detected])
    else
      AC_MSG_ERROR([...No neurocache detected])
    fi
  else
    dnl Run library and function checks
    AC_MSG_CHECKING(for neurocache libraries)
    clhy_neurocache_libs="-lneurocache -lneural"
    KUDA_ADDTO(LIBS, [$clhy_neurocache_libs])

    AC_TRY_LINK(
      [#include <neurocache/nsc_client.h>],
      [NSC_CTX *foo = NSC_CTX_new((const char *)0,0);],
      [],
      [clhy_neurocache_found="no"])
    AC_MSG_RESULT($clhy_neurocache_found)
    if test "$clhy_neurocache_found" != "yes"; then
      if test "x$clhy_neurocache_base" = "x"; then
        AC_MSG_WARN([... Error, NeuroCache libraries were missing or unusable])
      else
        AC_MSG_ERROR([... Error, NeuroCache libraries were missing or unusable])
      fi
    fi
  fi

  dnl restore
  CPPFLAGS="$saved_CPPFLAGS"
  LIBS="$saved_LIBS"
  LDFLAGS="$saved_LDFLAGS"

  dnl Adjust cLHy's configuration based on what we found above.
  if test "$clhy_neurocache_found" = "yes"; then
    KUDA_ADDTO(CAPI_SOCACHE_NEURO_LDADD, [$clhy_neurocache_ldflags $clhy_neurocache_libs])
    AC_DEFINE(HAVE_NEUROCACHE, 1, [Define if neurocache support is enabled])
  else
    enable_socache_neuro=no
  fi
  clhy_neurocache_configured="yes"
fi
])


dnl
dnl CLHYKUDEL_CHECK_DISTCACHE
dnl
dnl Configure for the detected distcache installation, giving
dnl preference to "--with-distcache=<path>" if it was specified.
dnl
AC_DEFUN([CLHYKUDEL_CHECK_DISTCACHE],[
if test "x$clhy_distcache_configured" = "x"; then
  dnl initialise the variables we use
  clhy_distcache_found=""
  clhy_distcache_base=""
  clhy_distcache_libs=""
  clhy_distcache_ldflags=""
  clhy_distcache_with=""

  dnl Determine the distcache base directory, if any
  AC_MSG_CHECKING([for user-provided distcache base])
  AC_ARG_WITH(distcache, CLHYKUDEL_HELP_STRING(--with-distcache=PATH, Distcache installation directory), [
    dnl If --with-distcache specifies a directory, we use that directory or fail
    if test "x$withval" != "xyes" -a "x$withval" != "x"; then
      dnl This ensures $withval is actually a directory and that it is absolute
      clhy_distcache_with="yes"
      clhy_distcache_base="`cd $withval ; pwd`"
    fi
  ])
  if test "x$clhy_distcache_base" = "x"; then
    AC_MSG_RESULT(none)
  else
    AC_MSG_RESULT($clhy_distcache_base)
  fi

  dnl Run header and version checks
  saved_CPPFLAGS="$CPPFLAGS"
  saved_LIBS="$LIBS"
  saved_LDFLAGS="$LDFLAGS"

  if test "x$clhy_distcache_base" != "x"; then
    KUDA_ADDTO(CPPFLAGS, [-I$clhy_distcache_base/include])
    KUDA_ADDTO(CAPI_INCLUDES, [-I$clhy_distcache_base/include])
    KUDA_ADDTO(LDFLAGS, [-L$clhy_distcache_base/lib])
    KUDA_ADDTO(clhy_distcache_ldflags, [-L$clhy_distcache_base/lib])
    if test "x$clhy_platform_runtime_link_flag" != "x"; then
      KUDA_ADDTO(LDFLAGS, [$clhy_platform_runtime_link_flag$clhy_distcache_base/lib])
      KUDA_ADDTO(clhy_distcache_ldflags, [$clhy_platform_runtime_link_flag$clhy_distcache_base/lib])
    fi
  fi
  dnl First check for mandatory headers
  AC_CHECK_HEADERS([distcache/dc_client.h], [clhy_distcache_found="yes"], [])
  if test "$clhy_distcache_found" = "yes"; then
    dnl test for a good version
    AC_MSG_CHECKING(for distcache version)
    AC_TRY_COMPILE([#include <distcache/dc_client.h>],[
#if DISTCACHE_CLIENT_API != 0x0001
#error "distcache API version is unrecognised"
#endif],
      [],
      [clhy_distcache_found="no"])
    AC_MSG_RESULT($clhy_distcache_found)
  fi
  if test "$clhy_distcache_found" != "yes"; then
    if test "x$clhy_distcache_with" = "x"; then
      AC_MSG_WARN([...No distcache detected])
    else
      AC_MSG_ERROR([...No distcache detected])
    fi
  else
    dnl Run library and function checks
    AC_MSG_CHECKING(for distcache libraries)
    clhy_distcache_libs="-ldistcache -lnal"
    KUDA_ADDTO(LIBS, [$clhy_distcache_libs])

    AC_TRY_LINK(
      [#include <distcache/dc_client.h>],
      [DC_CTX *foo = DC_CTX_new((const char *)0,0);],
      [],
      [clhy_distcache_found="no"])
    AC_MSG_RESULT($clhy_distcache_found)
    if test "$clhy_distcache_found" != "yes"; then
      if test "x$clhy_distcache_base" = "x"; then
        AC_MSG_WARN([... Error, distcache libraries were missing or unusable])
      else
        AC_MSG_ERROR([... Error, distcache libraries were missing or unusable])
      fi
    fi
  fi

  dnl restore
  CPPFLAGS="$saved_CPPFLAGS"
  LIBS="$saved_LIBS"
  LDFLAGS="$saved_LDFLAGS"

  dnl Adjust clhy's configuration based on what we found above.
  if test "$clhy_distcache_found" = "yes"; then
    KUDA_ADDTO(CAPI_SOCACHE_DC_LDADD, [$clhy_distcache_ldflags $clhy_distcache_libs])
    AC_DEFINE(HAVE_DISTCACHE, 1, [Define if distcache support is enabled])
  else
    enable_socache_dc=no
  fi
  clhy_distcache_configured="yes"
fi
])

CLHYKUDEL_CAPI(socache_shmcb,  shmcb small object cache provider, , , most)
CLHYKUDEL_CAPI(socache_dbm, dbm small object cache provider, , , most)
CLHYKUDEL_CAPI(socache_memcache, memcache small object cache provider, , , most)
CLHYKUDEL_CAPI(socache_redis, redis small object cache provider, , , most)
CLHYKUDEL_CAPI(socache_neuro, neuro-based session caching provider, , , no, [CLHYKUDEL_CHECK_NEUROCACHE])
CLHYKUDEL_CAPI(socache_dc, distcache small object cache provider, , , no, [
    CLHYKUDEL_CHECK_DISTCACHE
])

KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

CLHYKUDEL_CAPIPATH_FINISH
