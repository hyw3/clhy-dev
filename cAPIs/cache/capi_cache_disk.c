/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_lib.h"
#include "kuda_file_io.h"
#include "kuda_strings.h"
#include "capi_cache.h"
#include "capi_cache_disk.h"
#include "http_config.h"
#include "http_log.h"
#include "http_core.h"
#include "clhy_provider.h"
#include "util_filter.h"
#include "util_script.h"
#include "util_charset.h"

/*
 * capi_cache_disk: Disk Based HTTP 1.1 Cache.
 *
 * Flow to Find the .data file:
 *   Incoming client requests URI /foo/bar/baz
 *   Generate <hash> off of /foo/bar/baz
 *   Open <hash>.header
 *   Read in <hash>.header file (may contain Format #1 or Format #2)
 *   If format #1 (Contains a list of Vary Headers):
 *      Use each header name (from .header) with our request values (headers_in) to
 *      regenerate <hash> using HeaderName+HeaderValue+.../foo/bar/baz
 *      re-read in <hash>.header (must be format #2)
 *   read in <hash>.data
 *
 * Format #1:
 *   kuda_uint32_t format;
 *   kuda_time_t expire;
 *   kuda_array_t vary_headers (delimited by CRLF)
 *
 * Format #2:
 *   disk_cache_info_t (first sizeof(kuda_uint32_t) bytes is the format)
 *   entity name (dobj->name) [length is in disk_cache_info_t->name_len]
 *   r->headers_out (delimited by CRLF)
 *   CRLF
 *   r->headers_in (delimited by CRLF)
 *   CRLF
 */

cAPI CLHY_CAPI_DECLARE_DATA cache_disk_capi;

/* Forward declarations */
static int remove_entity(cache_handle_t *h);
static kuda_status_t store_headers(cache_handle_t *h, request_rec *r, cache_info *i);
static kuda_status_t store_body(cache_handle_t *h, request_rec *r, kuda_bucket_brigade *in,
                               kuda_bucket_brigade *out);
static kuda_status_t recall_headers(cache_handle_t *h, request_rec *r);
static kuda_status_t recall_body(cache_handle_t *h, kuda_pool_t *p, kuda_bucket_brigade *bb);
static kuda_status_t read_array(request_rec *r, kuda_array_header_t* arr,
                               kuda_file_t *file);

/*
 * Local static functions
 */

static char *header_file(kuda_pool_t *p, disk_cache_conf *conf,
                         disk_cache_object_t *dobj, const char *name)
{
    if (!dobj->hashfile) {
        dobj->hashfile = clhy_cache_generate_name(p, conf->dirlevels,
                                                conf->dirlength, name);
    }

    if (dobj->prefix) {
        return kuda_pstrcat(p, dobj->prefix, CACHE_VDIR_SUFFIX "/",
                           dobj->hashfile, CACHE_HEADER_SUFFIX, NULL);
     }
     else {
        return kuda_pstrcat(p, conf->cache_root, "/", dobj->hashfile,
                           CACHE_HEADER_SUFFIX, NULL);
     }
}

static char *data_file(kuda_pool_t *p, disk_cache_conf *conf,
                       disk_cache_object_t *dobj, const char *name)
{
    if (!dobj->hashfile) {
        dobj->hashfile = clhy_cache_generate_name(p, conf->dirlevels,
                                                conf->dirlength, name);
    }

    if (dobj->prefix) {
        return kuda_pstrcat(p, dobj->prefix, CACHE_VDIR_SUFFIX "/",
                           dobj->hashfile, CACHE_DATA_SUFFIX, NULL);
     }
     else {
        return kuda_pstrcat(p, conf->cache_root, "/", dobj->hashfile,
                           CACHE_DATA_SUFFIX, NULL);
     }
}

static kuda_status_t mkdir_structure(disk_cache_conf *conf, const char *file, kuda_pool_t *pool)
{
    kuda_status_t rv;
    char *p;

    for (p = (char*)file + conf->cache_root_len + 1;;) {
        p = strchr(p, '/');
        if (!p)
            break;
        *p = '\0';

        rv = kuda_dir_make(file,
                          KUDA_UREAD|KUDA_UWRITE|KUDA_UEXECUTE, pool);
        if (rv != KUDA_SUCCESS && !KUDA_STATUS_IS_EEXIST(rv)) {
            return rv;
        }
        *p = '/';
        ++p;
    }
    return KUDA_SUCCESS;
}

/* htcacheclean may remove directories underneath us.
 * So, we'll try renaming three times at a cost of 0.002 seconds.
 */
static kuda_status_t safe_file_rename(disk_cache_conf *conf,
                                     const char *src, const char *dest,
                                     kuda_pool_t *pool)
{
    kuda_status_t rv;

    rv = kuda_file_rename(src, dest, pool);

    if (rv != KUDA_SUCCESS) {
        int i;

        for (i = 0; i < 2 && rv != KUDA_SUCCESS; i++) {
            /* 1000 micro-seconds aka 0.001 seconds. */
            kuda_sleep(1000);

            rv = mkdir_structure(conf, dest, pool);
            if (rv != KUDA_SUCCESS)
                continue;

            rv = kuda_file_rename(src, dest, pool);
        }
    }

    return rv;
}

static kuda_status_t file_cache_el_final(disk_cache_conf *conf, disk_cache_file_t *file,
                                        request_rec *r)
{
    kuda_status_t rv = KUDA_SUCCESS;

    /* This assumes that the tempfiles are on the same file system
     * as the cache_root. If not, then we need a file copy/move
     * rather than a rename.
     */

    /* move the file over */
    if (file->tempfd) {

        rv = safe_file_rename(conf, file->tempfile, file->file, file->pool);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, r, CLHYLOGNO(00699)
                    "rename tempfile to file failed:"
                    " %s -> %s", file->tempfile, file->file);
            kuda_file_remove(file->tempfile, file->pool);
        }

        file->tempfd = NULL;
    }

    return rv;
}

static kuda_status_t file_cache_temp_cleanup(void *dummy)
{
    disk_cache_file_t *file = (disk_cache_file_t *)dummy;

    /* clean up the temporary file */
    if (file->tempfd) {
        kuda_file_remove(file->tempfile, file->pool);
        file->tempfd = NULL;
    }
    file->tempfile = NULL;
    file->pool = NULL;

    return KUDA_SUCCESS;
}

static kuda_status_t file_cache_create(disk_cache_conf *conf, disk_cache_file_t *file,
                                      kuda_pool_t *pool)
{
    file->pool = pool;
    file->tempfile = kuda_pstrcat(pool, conf->cache_root, CLHY_TEMPFILE, NULL);

    kuda_pool_cleanup_register(pool, file, file_cache_temp_cleanup, kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}

/* These two functions get and put state information into the data
 * file for an clhy_cache_el, this state information will be read
 * and written transparent to clients of this cAPI
 */
static int file_cache_recall_mydata(kuda_file_t *fd, cache_info *info,
                                    disk_cache_object_t *dobj, request_rec *r)
{
    kuda_status_t rv;
    char *urlbuff;
    kuda_size_t len;

    /* read the data from the cache file */
    len = sizeof(disk_cache_info_t);
    rv = kuda_file_read_full(fd, &dobj->disk_info, len, &len);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    /* Store it away so we can get it later. */
    info->status = dobj->disk_info.status;
    info->date = dobj->disk_info.date;
    info->expire = dobj->disk_info.expire;
    info->request_time = dobj->disk_info.request_time;
    info->response_time = dobj->disk_info.response_time;

    memcpy(&info->control, &dobj->disk_info.control, sizeof(cache_control_t));

    /* Note that we could optimize this by conditionally doing the palloc
     * depending upon the size. */
    urlbuff = kuda_palloc(r->pool, dobj->disk_info.name_len + 1);
    len = dobj->disk_info.name_len;
    rv = kuda_file_read_full(fd, urlbuff, len, &len);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }
    urlbuff[dobj->disk_info.name_len] = '\0';

    /* check that we have the same URL */
    /* Would strncmp be correct? */
    if (strcmp(urlbuff, dobj->name) != 0) {
        return KUDA_EGENERAL;
    }

    return KUDA_SUCCESS;
}

static const char* regen_key(kuda_pool_t *p, kuda_table_t *headers,
                             kuda_array_header_t *varray, const char *oldkey)
{
    struct iovec *iov;
    int i, k;
    int nvec;
    const char *header;
    const char **elts;

    nvec = (varray->nelts * 2) + 1;
    iov = kuda_palloc(p, sizeof(struct iovec) * nvec);
    elts = (const char **) varray->elts;

    /* TODO:
     *    - Handle multiple-value headers better. (sort them?)
     *    - Handle Case in-sensitive Values better.
     *        This isn't the end of the world, since it just lowers the cache
     *        hit rate, but it would be nice to fix.
     *
     * The majority are case insenstive if they are values (encoding etc).
     * Most of rfc2616 is case insensitive on header contents.
     *
     * So the better solution may be to identify headers which should be
     * treated case-sensitive?
     *  HTTP URI's (3.2.3) [host and scheme are insensitive]
     *  HTTP method (5.1.1)
     *  HTTP-date values (3.3.1)
     *  3.7 Media Types [exerpt]
     *     The type, subtype, and parameter attribute names are case-
     *     insensitive. Parameter values might or might not be case-sensitive,
     *     depending on the semantics of the parameter name.
     *  4.20 Except [exerpt]
     *     Comparison of expectation values is case-insensitive for unquoted
     *     tokens (including the 100-continue token), and is case-sensitive for
     *     quoted-string expectation-extensions.
     */

    for (i=0, k=0; i < varray->nelts; i++) {
        header = kuda_table_get(headers, elts[i]);
        if (!header) {
            header = "";
        }
        iov[k].iov_base = (char*) elts[i];
        iov[k].iov_len = strlen(elts[i]);
        k++;
        iov[k].iov_base = (char*) header;
        iov[k].iov_len = strlen(header);
        k++;
    }
    iov[k].iov_base = (char*) oldkey;
    iov[k].iov_len = strlen(oldkey);
    k++;

    return kuda_pstrcatv(p, iov, k, NULL);
}

static int array_alphasort(const void *fn1, const void *fn2)
{
    return strcmp(*(char**)fn1, *(char**)fn2);
}

static void tokens_to_array(kuda_pool_t *p, const char *data,
                            kuda_array_header_t *arr)
{
    char *token;

    while ((token = clhy_get_list_item(p, &data)) != NULL) {
        *((const char **) kuda_array_push(arr)) = token;
    }

    /* Sort it so that "Vary: A, B" and "Vary: B, A" are stored the same. */
    qsort((void *) arr->elts, arr->nelts,
         sizeof(char *), array_alphasort);
}

/*
 * Hook and capi_cache callback functions
 */
static int create_entity(cache_handle_t *h, request_rec *r, const char *key, kuda_off_t len,
                         kuda_bucket_brigade *bb)
{
    disk_cache_dir_conf *dconf = clhy_get_capi_config(r->per_dir_config, &cache_disk_capi);
    disk_cache_conf *conf = clhy_get_capi_config(r->server->capi_config,
                                                 &cache_disk_capi);
    cache_object_t *obj;
    disk_cache_object_t *dobj;
    kuda_pool_t *pool;

    if (conf->cache_root == NULL) {
        return DECLINED;
    }

    /* we don't support caching of range requests (yet) */
    if (r->status == HTTP_PARTIAL_CONTENT) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00700)
                "URL %s partial content response not cached",
                key);
        return DECLINED;
    }

    /* Note, len is -1 if unknown so don't trust it too hard */
    if (len > dconf->maxfs) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00701)
                "URL %s failed the size check "
                "(%" KUDA_OFF_T_FMT " > %" KUDA_OFF_T_FMT ")",
                key, len, dconf->maxfs);
        return DECLINED;
    }
    if (len >= 0 && len < dconf->minfs) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00702)
                "URL %s failed the size check "
                "(%" KUDA_OFF_T_FMT " < %" KUDA_OFF_T_FMT ")",
                key, len, dconf->minfs);
        return DECLINED;
    }

    /* Allocate and initialize cache_object_t and disk_cache_object_t */
    h->cache_obj = obj = kuda_pcalloc(r->pool, sizeof(*obj));
    obj->vobj = dobj = kuda_pcalloc(r->pool, sizeof(*dobj));

    obj->key = kuda_pstrdup(r->pool, key);

    dobj->name = obj->key;
    dobj->prefix = NULL;
    /* Save the cache root */
    dobj->root = kuda_pstrmemdup(r->pool, conf->cache_root, conf->cache_root_len);
    dobj->root_len = conf->cache_root_len;

    kuda_pool_create(&pool, r->pool);
    kuda_pool_tag(pool, "capi_cache (create_entity)");

    file_cache_create(conf, &dobj->hdrs, pool);
    file_cache_create(conf, &dobj->vary, pool);
    file_cache_create(conf, &dobj->data, pool);

    dobj->data.file = data_file(r->pool, conf, dobj, key);
    dobj->hdrs.file = header_file(r->pool, conf, dobj, key);
    dobj->vary.file = header_file(r->pool, conf, dobj, key);

    dobj->disk_info.header_only = r->header_only;

    return OK;
}

static int open_entity(cache_handle_t *h, request_rec *r, const char *key)
{
    kuda_uint32_t format;
    kuda_size_t len;
    const char *nkey;
    kuda_status_t rc;
    static int error_logged = 0;
    disk_cache_conf *conf = clhy_get_capi_config(r->server->capi_config,
                                                 &cache_disk_capi);
#ifdef KUDA_SENDFILE_ENABLED
    core_dir_config *coreconf = clhy_get_core_capi_config(r->per_dir_config);
#endif
    kuda_finfo_t finfo;
    cache_object_t *obj;
    cache_info *info;
    disk_cache_object_t *dobj;
    int flags;
    kuda_pool_t *pool;

    h->cache_obj = NULL;

    /* Look up entity keyed to 'url' */
    if (conf->cache_root == NULL) {
        if (!error_logged) {
            error_logged = 1;
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00703)
                    "Cannot cache files to disk without a CacheRoot specified.");
        }
        return DECLINED;
    }

    /* Create and init the cache object */
    obj = kuda_pcalloc(r->pool, sizeof(cache_object_t));
    dobj = kuda_pcalloc(r->pool, sizeof(disk_cache_object_t));

    info = &(obj->info);

    /* Open the headers file */
    dobj->prefix = NULL;

    /* Save the cache root */
    dobj->root = kuda_pstrmemdup(r->pool, conf->cache_root, conf->cache_root_len);
    dobj->root_len = conf->cache_root_len;

    dobj->vary.file = header_file(r->pool, conf, dobj, key);
    flags = KUDA_READ|KUDA_BINARY|KUDA_BUFFERED;
    rc = kuda_file_open(&dobj->vary.fd, dobj->vary.file, flags, 0, r->pool);
    if (rc != KUDA_SUCCESS) {
        return DECLINED;
    }

    /* read the format from the cache file */
    len = sizeof(format);
    kuda_file_read_full(dobj->vary.fd, &format, len, &len);

    if (format == VARY_FORMAT_VERSION) {
        kuda_array_header_t* varray;
        kuda_time_t expire;

        len = sizeof(expire);
        kuda_file_read_full(dobj->vary.fd, &expire, len, &len);

        varray = kuda_array_make(r->pool, 5, sizeof(char*));
        rc = read_array(r, varray, dobj->vary.fd);
        if (rc != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rc, r, CLHYLOGNO(00704)
                    "Cannot parse vary header file: %s",
                    dobj->vary.file);
            kuda_file_close(dobj->vary.fd);
            return DECLINED;
        }
        kuda_file_close(dobj->vary.fd);

        nkey = regen_key(r->pool, r->headers_in, varray, key);

        dobj->hashfile = NULL;
        dobj->prefix = dobj->vary.file;
        dobj->hdrs.file = header_file(r->pool, conf, dobj, nkey);

        flags = KUDA_READ|KUDA_BINARY|KUDA_BUFFERED;
        rc = kuda_file_open(&dobj->hdrs.fd, dobj->hdrs.file, flags, 0, r->pool);
        if (rc != KUDA_SUCCESS) {
            return DECLINED;
        }
    }
    else if (format != DISK_FORMAT_VERSION) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00705)
                "File '%s' has a version mismatch. File had version: %d.",
                dobj->vary.file, format);
        kuda_file_close(dobj->vary.fd);
        return DECLINED;
    }
    else {
        kuda_off_t offset = 0;

        /* oops, not vary as it turns out */
        dobj->hdrs.fd = dobj->vary.fd;
        dobj->vary.fd = NULL;
        dobj->hdrs.file = dobj->vary.file;

        /* This wasn't a Vary Format file, so we must seek to the
         * start of the file again, so that later reads work.
         */
        kuda_file_seek(dobj->hdrs.fd, KUDA_SET, &offset);
        nkey = key;
    }

    obj->key = nkey;
    dobj->key = nkey;
    dobj->name = key;

    kuda_pool_create(&pool, r->pool);
    kuda_pool_tag(pool, "capi_cache (open_entity)");

    file_cache_create(conf, &dobj->hdrs, pool);
    file_cache_create(conf, &dobj->vary, pool);
    file_cache_create(conf, &dobj->data, pool);

    dobj->data.file = data_file(r->pool, conf, dobj, nkey);

    /* Read the bytes to setup the cache_info fields */
    rc = file_cache_recall_mydata(dobj->hdrs.fd, info, dobj, r);
    if (rc != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rc, r, CLHYLOGNO(00706)
                "Cannot read header file %s", dobj->hdrs.file);
        kuda_file_close(dobj->hdrs.fd);
        return DECLINED;
    }


    /* Is this a cached HEAD request? */
    if (dobj->disk_info.header_only && !r->header_only) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, KUDA_SUCCESS, r, CLHYLOGNO(00707)
                "HEAD request cached, non-HEAD requested, ignoring: %s",
                dobj->hdrs.file);
        kuda_file_close(dobj->hdrs.fd);
        return DECLINED;
    }

    /* Open the data file */
    if (dobj->disk_info.has_body) {
        flags = KUDA_READ | KUDA_BINARY;
#ifdef KUDA_SENDFILE_ENABLED
        /* When we are in the quick handler we don't have the per-directory
         * configuration, so this check only takes the global setting of
         * the EnableSendFile directive into account.
         */
        flags |= CLHY_SENDFILE_ENABLED(coreconf->enable_sendfile);
#endif
        rc = kuda_file_open(&dobj->data.fd, dobj->data.file, flags, 0, r->pool);
        if (rc != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rc, r, CLHYLOGNO(00708)
                    "Cannot open data file %s", dobj->data.file);
            kuda_file_close(dobj->hdrs.fd);
            return DECLINED;
        }

        rc = kuda_file_info_get(&finfo, KUDA_FINFO_SIZE | KUDA_FINFO_IDENT,
                dobj->data.fd);
        if (rc == KUDA_SUCCESS) {
            dobj->file_size = finfo.size;
        }

        /* Atomic check - does the body file belong to the header file? */
        if (dobj->disk_info.inode == finfo.inode &&
                dobj->disk_info.device == finfo.device) {

            /* Initialize the cache_handle callback functions */
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00709)
                    "Recalled cached URL info header %s", dobj->name);

            /* make the configuration stick */
            h->cache_obj = obj;
            obj->vobj = dobj;

            return OK;
        }

    }
    else {

        /* make the configuration stick */
        h->cache_obj = obj;
        obj->vobj = dobj;

        return OK;
    }

    /* Oh dear, no luck matching header to the body */
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00710)
            "Cached URL info header '%s' didn't match body, ignoring this entry",
            dobj->name);

    kuda_file_close(dobj->hdrs.fd);
    return DECLINED;
}

static void close_disk_cache_fd(disk_cache_file_t *file)
{
   if (file->fd != NULL) {
       kuda_file_close(file->fd);
       file->fd = NULL;
   }
   if (file->tempfd != NULL) {
       kuda_file_close(file->tempfd);
       file->tempfd = NULL;
   }
}

static int remove_entity(cache_handle_t *h)
{
    disk_cache_object_t *dobj = (disk_cache_object_t *) h->cache_obj->vobj;

    close_disk_cache_fd(&(dobj->hdrs));
    close_disk_cache_fd(&(dobj->vary));
    close_disk_cache_fd(&(dobj->data));

    /* Null out the cache object pointer so next time we start from scratch  */
    h->cache_obj = NULL;
    return OK;
}

static int remove_url(cache_handle_t *h, request_rec *r)
{
    kuda_status_t rc;
    disk_cache_object_t *dobj;

    /* Get disk cache object from cache handle */
    dobj = (disk_cache_object_t *) h->cache_obj->vobj;
    if (!dobj) {
        return DECLINED;
    }

    /* Delete headers file */
    if (dobj->hdrs.file) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00711)
                "Deleting %s from cache.", dobj->hdrs.file);

        rc = kuda_file_remove(dobj->hdrs.file, r->pool);
        if ((rc != KUDA_SUCCESS) && !KUDA_STATUS_IS_ENOENT(rc)) {
            /* Will only result in an output if wwhy is started with -e debug.
             * For reason see log_error_core for the case s == NULL.
             */
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rc, r, CLHYLOGNO(00712)
                    "Failed to delete headers file %s from cache.",
                    dobj->hdrs.file);
            return DECLINED;
        }
    }

    /* Delete data file */
    if (dobj->data.file) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00713)
                "Deleting %s from cache.", dobj->data.file);

        rc = kuda_file_remove(dobj->data.file, r->pool);
        if ((rc != KUDA_SUCCESS) && !KUDA_STATUS_IS_ENOENT(rc)) {
            /* Will only result in an output if wwhy is started with -e debug.
             * For reason see log_error_core for the case s == NULL.
             */
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rc, r, CLHYLOGNO(00714)
                    "Failed to delete data file %s from cache.",
                    dobj->data.file);
            return DECLINED;
        }
    }

    /* now delete directories as far as possible up to our cache root */
    if (dobj->root) {
        const char *str_to_copy;

        str_to_copy = dobj->hdrs.file ? dobj->hdrs.file : dobj->data.file;
        if (str_to_copy) {
            char *dir, *slash, *q;

            dir = kuda_pstrdup(r->pool, str_to_copy);

            /* remove filename */
            slash = strrchr(dir, '/');
            *slash = '\0';

            /*
             * now walk our way back to the cache root, delete everything
             * in the way as far as possible
             *
             * Note: due to the way we constructed the file names in
             * header_file and data_file, we are guaranteed that the
             * cache_root is suffixed by at least one '/' which will be
             * turned into a terminating null by this loop.  Therefore,
             * we won't either delete or go above our cache root.
             */
            for (q = dir + dobj->root_len; *q ; ) {
                 clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00715)
                        "Deleting directory %s from cache", dir);

                 rc = kuda_dir_remove(dir, r->pool);
                 if (rc != KUDA_SUCCESS && !KUDA_STATUS_IS_ENOENT(rc)) {
                    break;
                 }
                 slash = strrchr(q, '/');
                 *slash = '\0';
            }
        }
    }

    return OK;
}

static kuda_status_t read_array(request_rec *r, kuda_array_header_t* arr,
                               kuda_file_t *file)
{
    char w[MAX_STRING_LEN];
    int p;
    kuda_status_t rv;

    while (1) {
        rv = kuda_file_gets(w, MAX_STRING_LEN - 1, file);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00716)
                          "Premature end of vary array.");
            return rv;
        }

        p = strlen(w);
        if (p > 0 && w[p - 1] == '\n') {
            if (p > 1 && w[p - 2] == CR) {
                w[p - 2] = '\0';
            }
            else {
                w[p - 1] = '\0';
            }
        }

        /* If we've finished reading the array, break out of the loop. */
        if (w[0] == '\0') {
            break;
        }

        *((const char **) kuda_array_push(arr)) = kuda_pstrdup(r->pool, w);
    }

    return KUDA_SUCCESS;
}

static kuda_status_t store_array(kuda_file_t *fd, kuda_array_header_t* arr)
{
    int i;
    kuda_status_t rv;
    struct iovec iov[2];
    kuda_size_t amt;
    const char **elts;

    elts = (const char **) arr->elts;

    for (i = 0; i < arr->nelts; i++) {
        iov[0].iov_base = (char*) elts[i];
        iov[0].iov_len = strlen(elts[i]);
        iov[1].iov_base = CRLF;
        iov[1].iov_len = sizeof(CRLF) - 1;

        rv = kuda_file_writev_full(fd, (const struct iovec *) &iov, 2, &amt);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
    }

    iov[0].iov_base = CRLF;
    iov[0].iov_len = sizeof(CRLF) - 1;

    return kuda_file_writev_full(fd, (const struct iovec *) &iov, 1, &amt);
}

static kuda_status_t read_table(cache_handle_t *handle, request_rec *r,
                               kuda_table_t *table, kuda_file_t *file)
{
    char w[MAX_STRING_LEN];
    char *l;
    int p;
    kuda_status_t rv;

    while (1) {

        /* ### What about KUDA_EOF? */
        rv = kuda_file_gets(w, MAX_STRING_LEN - 1, file);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(00717)
                          "Premature end of cache headers.");
            return rv;
        }

        /* Delete terminal (CR?)LF */

        p = strlen(w);
        /* Indeed, the host's '\n':
           '\012' for UNIX; '\015' for MacOS; '\025' for PLATFORM/390
           -- whatever the script generates.
        */
        if (p > 0 && w[p - 1] == '\n') {
            if (p > 1 && w[p - 2] == CR) {
                w[p - 2] = '\0';
            }
            else {
                w[p - 1] = '\0';
            }
        }

        /* If we've finished reading the headers, break out of the loop. */
        if (w[0] == '\0') {
            break;
        }

#if KUDA_CHARSET_EBCDIC
        /* Chances are that we received an ASCII header text instead of
         * the expected EBCDIC header lines. Try to auto-detect:
         */
        if (!(l = strchr(w, ':'))) {
            int maybeASCII = 0, maybeEBCDIC = 0;
            unsigned char *cp, native;
            kuda_size_t inbytes_left, outbytes_left;

            for (cp = w; *cp != '\0'; ++cp) {
                native = kuda_xlate_conv_byte(clhy_hdrs_from_ascii, *cp);
                if (kuda_isprint(*cp) && !kuda_isprint(native))
                    ++maybeEBCDIC;
                if (!kuda_isprint(*cp) && kuda_isprint(native))
                    ++maybeASCII;
            }
            if (maybeASCII > maybeEBCDIC) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00718)
                        "CGI Interface Error: Script headers apparently ASCII: (CGI = %s)",
                        r->filename);
                inbytes_left = outbytes_left = cp - w;
                kuda_xlate_conv_buffer(clhy_hdrs_from_ascii,
                                      w, &inbytes_left, w, &outbytes_left);
            }
        }
#endif /*KUDA_CHARSET_EBCDIC*/

        /* if we see a bogus header don't ignore it. Shout and scream */
        if (!(l = strchr(w, ':'))) {
            return KUDA_EGENERAL;
        }

        *l++ = '\0';
        while (kuda_isspace(*l)) {
            ++l;
        }

        kuda_table_add(table, w, l);
    }

    return KUDA_SUCCESS;
}

/*
 * Reads headers from a buffer and returns an array of headers.
 * Returns NULL on file error
 * This routine tries to deal with too long lines and continuation lines.
 * @@@: XXX: FIXME: currently the headers are passed thru un-merged.
 * Is that okay, or should they be collapsed where possible?
 */
static kuda_status_t recall_headers(cache_handle_t *h, request_rec *r)
{
    disk_cache_object_t *dobj = (disk_cache_object_t *) h->cache_obj->vobj;
    kuda_status_t rv;

    /* This case should not happen... */
    if (!dobj->hdrs.fd) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00719)
                "recalling headers; but no header fd for %s", dobj->name);
        return KUDA_NOTFOUND;
    }

    h->req_hdrs = kuda_table_make(r->pool, 20);
    h->resp_hdrs = kuda_table_make(r->pool, 20);

    /* Call routine to read the header lines/status line */
    rv = read_table(h, r, h->resp_hdrs, dobj->hdrs.fd);
    if (rv != KUDA_SUCCESS) { 
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02987) 
                      "Error reading response headers from %s for %s",
                      dobj->hdrs.file, dobj->name);
    }
    rv = read_table(h, r, h->req_hdrs, dobj->hdrs.fd);
    if (rv != KUDA_SUCCESS) { 
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02988) 
                      "Error reading request headers from %s for %s",
                      dobj->hdrs.file, dobj->name);
    }

    kuda_file_close(dobj->hdrs.fd);

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00720)
            "Recalled headers for URL %s", dobj->name);
    return KUDA_SUCCESS;
}

static kuda_status_t recall_body(cache_handle_t *h, kuda_pool_t *p, kuda_bucket_brigade *bb)
{
    disk_cache_object_t *dobj = (disk_cache_object_t*) h->cache_obj->vobj;

    if (dobj->data.fd) {
        kuda_brigade_insert_file(bb, dobj->data.fd, 0, dobj->file_size, p);
    }

    return KUDA_SUCCESS;
}

static kuda_status_t store_table(kuda_file_t *fd, kuda_table_t *table)
{
    int i;
    kuda_status_t rv;
    struct iovec iov[4];
    kuda_size_t amt;
    kuda_table_entry_t *elts;

    elts = (kuda_table_entry_t *) kuda_table_elts(table)->elts;
    for (i = 0; i < kuda_table_elts(table)->nelts; ++i) {
        if (elts[i].key != NULL) {
            iov[0].iov_base = elts[i].key;
            iov[0].iov_len = strlen(elts[i].key);
            iov[1].iov_base = ": ";
            iov[1].iov_len = sizeof(": ") - 1;
            iov[2].iov_base = elts[i].val;
            iov[2].iov_len = strlen(elts[i].val);
            iov[3].iov_base = CRLF;
            iov[3].iov_len = sizeof(CRLF) - 1;

            rv = kuda_file_writev_full(fd, (const struct iovec *) &iov, 4, &amt);
            if (rv != KUDA_SUCCESS) {
                return rv;
            }
        }
    }
    iov[0].iov_base = CRLF;
    iov[0].iov_len = sizeof(CRLF) - 1;
    rv = kuda_file_writev_full(fd, (const struct iovec *) &iov, 1, &amt);
    return rv;
}

static kuda_status_t store_headers(cache_handle_t *h, request_rec *r, cache_info *info)
{
    disk_cache_object_t *dobj = (disk_cache_object_t*) h->cache_obj->vobj;

    memcpy(&h->cache_obj->info, info, sizeof(cache_info));

    if (r->headers_out) {
        dobj->headers_out = clhy_cache_cacheable_headers_out(r);
    }

    if (r->headers_in) {
        dobj->headers_in = clhy_cache_cacheable_headers_in(r);
    }

    if (r->header_only && r->status != HTTP_NOT_MODIFIED) {
        dobj->disk_info.header_only = 1;
    }

    return KUDA_SUCCESS;
}

static kuda_status_t write_headers(cache_handle_t *h, request_rec *r)
{
    disk_cache_conf *conf = clhy_get_capi_config(r->server->capi_config,
                                                 &cache_disk_capi);
    kuda_status_t rv;
    kuda_size_t amt;
    disk_cache_object_t *dobj = (disk_cache_object_t*) h->cache_obj->vobj;

    disk_cache_info_t disk_info;
    struct iovec iov[2];

    memset(&disk_info, 0, sizeof(disk_cache_info_t));

    if (dobj->headers_out) {
        const char *tmp;

        tmp = kuda_table_get(dobj->headers_out, "Vary");

        if (tmp) {
            kuda_array_header_t* varray;
            kuda_uint32_t format = VARY_FORMAT_VERSION;

            /* If we were initially opened as a vary format, rollback
             * that internal state for the moment so we can recreate the
             * vary format hints in the appropriate directory.
             */
            if (dobj->prefix) {
                dobj->hdrs.file = dobj->prefix;
                dobj->prefix = NULL;
            }

            rv = mkdir_structure(conf, dobj->hdrs.file, r->pool);

            rv = kuda_file_mktemp(&dobj->vary.tempfd, dobj->vary.tempfile,
                                 KUDA_CREATE | KUDA_WRITE | KUDA_BINARY | KUDA_EXCL,
                                 dobj->vary.pool);

            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, r, CLHYLOGNO(00721)
                        "could not create vary file %s",
                        dobj->vary.tempfile);
                return rv;
            }

            amt = sizeof(format);
            rv = kuda_file_write_full(dobj->vary.tempfd, &format, amt, NULL);
            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, r, CLHYLOGNO(00722)
                        "could not write to vary file %s",
                        dobj->vary.tempfile);
                kuda_file_close(dobj->vary.tempfd);
                kuda_pool_destroy(dobj->vary.pool);
                return rv;
            }

            amt = sizeof(h->cache_obj->info.expire);
            rv = kuda_file_write_full(dobj->vary.tempfd,
                                     &h->cache_obj->info.expire, amt, NULL);
            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, r, CLHYLOGNO(00723)
                        "could not write to vary file %s",
                        dobj->vary.tempfile);
                kuda_file_close(dobj->vary.tempfd);
                kuda_pool_destroy(dobj->vary.pool);
                return rv;
            }

            varray = kuda_array_make(r->pool, 6, sizeof(char*));
            tokens_to_array(r->pool, tmp, varray);

            store_array(dobj->vary.tempfd, varray);

            rv = kuda_file_close(dobj->vary.tempfd);
            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, r, CLHYLOGNO(00724)
                        "could not close vary file %s",
                        dobj->vary.tempfile);
                kuda_pool_destroy(dobj->vary.pool);
                return rv;
            }

            tmp = regen_key(r->pool, dobj->headers_in, varray, dobj->name);
            dobj->prefix = dobj->hdrs.file;
            dobj->hashfile = NULL;
            dobj->data.file = data_file(r->pool, conf, dobj, tmp);
            dobj->hdrs.file = header_file(r->pool, conf, dobj, tmp);
        }
    }


    rv = kuda_file_mktemp(&dobj->hdrs.tempfd, dobj->hdrs.tempfile,
                         KUDA_CREATE | KUDA_WRITE | KUDA_BINARY |
                         KUDA_BUFFERED | KUDA_EXCL, dobj->hdrs.pool);

    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, r, CLHYLOGNO(00725)
                "could not create header file %s",
                dobj->hdrs.tempfile);
        return rv;
    }

    disk_info.format = DISK_FORMAT_VERSION;
    disk_info.date = h->cache_obj->info.date;
    disk_info.expire = h->cache_obj->info.expire;
    disk_info.entity_version = dobj->disk_info.entity_version++;
    disk_info.request_time = h->cache_obj->info.request_time;
    disk_info.response_time = h->cache_obj->info.response_time;
    disk_info.status = h->cache_obj->info.status;
    disk_info.inode = dobj->disk_info.inode;
    disk_info.device = dobj->disk_info.device;
    disk_info.has_body = dobj->disk_info.has_body;
    disk_info.header_only = dobj->disk_info.header_only;

    disk_info.name_len = strlen(dobj->name);

    memcpy(&disk_info.control, &h->cache_obj->info.control, sizeof(cache_control_t));

    iov[0].iov_base = (void*)&disk_info;
    iov[0].iov_len = sizeof(disk_cache_info_t);
    iov[1].iov_base = (void*)dobj->name;
    iov[1].iov_len = disk_info.name_len;

    rv = kuda_file_writev_full(dobj->hdrs.tempfd, (const struct iovec *) &iov,
                              2, &amt);
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, r, CLHYLOGNO(00726)
                "could not write info to header file %s",
                dobj->hdrs.tempfile);
        kuda_file_close(dobj->hdrs.tempfd);
        kuda_pool_destroy(dobj->hdrs.pool);
        return rv;
    }

    if (dobj->headers_out) {
        rv = store_table(dobj->hdrs.tempfd, dobj->headers_out);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, r, CLHYLOGNO(00727)
                    "could not write out-headers to header file %s",
                    dobj->hdrs.tempfile);
            kuda_file_close(dobj->hdrs.tempfd);
            kuda_pool_destroy(dobj->hdrs.pool);
            return rv;
        }
    }

    /* Parse the vary header and dump those fields from the headers_in. */
    /* FIXME: Make call to the same thing cache_select calls to crack Vary. */
    if (dobj->headers_in) {
        rv = store_table(dobj->hdrs.tempfd, dobj->headers_in);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, r, CLHYLOGNO(00728)
                    "could not write in-headers to header file %s",
                    dobj->hdrs.tempfile);
            kuda_file_close(dobj->hdrs.tempfd);
            kuda_pool_destroy(dobj->hdrs.pool);
            return rv;
        }
    }

    rv = kuda_file_close(dobj->hdrs.tempfd); /* flush and close */
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, r, CLHYLOGNO(00729)
                "could not close header file %s",
                dobj->hdrs.tempfile);
        kuda_pool_destroy(dobj->hdrs.pool);
        return rv;
    }

    return KUDA_SUCCESS;
}

static kuda_status_t store_body(cache_handle_t *h, request_rec *r,
                               kuda_bucket_brigade *in, kuda_bucket_brigade *out)
{
    kuda_bucket *e;
    kuda_status_t rv = KUDA_SUCCESS;
    disk_cache_object_t *dobj = (disk_cache_object_t *) h->cache_obj->vobj;
    disk_cache_dir_conf *dconf = clhy_get_capi_config(r->per_dir_config, &cache_disk_capi);
    int seen_eos = 0;

    if (!dobj->offset) {
        dobj->offset = dconf->readsize;
    }
    if (!dobj->timeout && dconf->readtime) {
        dobj->timeout = kuda_time_now() + dconf->readtime;
    }

    if (dobj->offset) {
        kuda_brigade_partition(in, dobj->offset, &e);
    }

    while (KUDA_SUCCESS == rv && !KUDA_BRIGADE_EMPTY(in)) {
        const char *str;
        kuda_size_t length, written;

        e = KUDA_BRIGADE_FIRST(in);

        /* are we done completely? if so, pass any trailing buckets right through */
        if (dobj->done || !dobj->data.pool) {
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(out, e);
            continue;
        }

        /* have we seen eos yet? */
        if (KUDA_BUCKET_IS_EOS(e)) {
            seen_eos = 1;
            dobj->done = 1;
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(out, e);
            break;
        }

        /* honour flush buckets, we'll get called again */
        if (KUDA_BUCKET_IS_FLUSH(e)) {
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(out, e);
            break;
        }

        /* metadata buckets are preserved as is */
        if (KUDA_BUCKET_IS_METADATA(e)) {
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(out, e);
            continue;
        }

        /* read the bucket, write to the cache */
        rv = kuda_bucket_read(e, &str, &length, KUDA_BLOCK_READ);
        KUDA_BUCKET_REMOVE(e);
        KUDA_BRIGADE_INSERT_TAIL(out, e);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00730)
                    "Error when reading bucket for URL %s",
                    h->cache_obj->key);
            /* Remove the intermediate cache file and return non-KUDA_SUCCESS */
            kuda_pool_destroy(dobj->data.pool);
            return rv;
        }

        /* don't write empty buckets to the cache */
        if (!length) {
            continue;
        }

        if (!dobj->disk_info.header_only) {

            /* Attempt to create the data file at the last possible moment, if
             * the body is empty, we don't write a file at all, and save an inode.
             */
            if (!dobj->data.tempfd) {
                kuda_finfo_t finfo;
                rv = kuda_file_mktemp(&dobj->data.tempfd, dobj->data.tempfile,
                        KUDA_CREATE | KUDA_WRITE | KUDA_BINARY | KUDA_BUFFERED
                                | KUDA_EXCL, dobj->data.pool);
                if (rv != KUDA_SUCCESS) {
                    kuda_pool_destroy(dobj->data.pool);
                    return rv;
                }
                dobj->file_size = 0;
                rv = kuda_file_info_get(&finfo, KUDA_FINFO_IDENT,
                        dobj->data.tempfd);
                if (rv != KUDA_SUCCESS) {
                    kuda_pool_destroy(dobj->data.pool);
                    return rv;
                }
                dobj->disk_info.device = finfo.device;
                dobj->disk_info.inode = finfo.inode;
                dobj->disk_info.has_body = 1;
            }

            /* write to the cache, leave if we fail */
            rv = kuda_file_write_full(dobj->data.tempfd, str, length, &written);
            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(
                        CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00731) "Error when writing cache file for URL %s", h->cache_obj->key);
                /* Remove the intermediate cache file and return non-KUDA_SUCCESS */
                kuda_pool_destroy(dobj->data.pool);
                return rv;
            }
            dobj->file_size += written;
            if (dobj->file_size > dconf->maxfs) {
                clhy_log_rerror(
                        CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00732) "URL %s failed the size check "
                        "(%" KUDA_OFF_T_FMT ">%" KUDA_OFF_T_FMT ")", h->cache_obj->key, dobj->file_size, dconf->maxfs);
                /* Remove the intermediate cache file and return non-KUDA_SUCCESS */
                kuda_pool_destroy(dobj->data.pool);
                return KUDA_EGENERAL;
            }

        }

        /* have we reached the limit of how much we're prepared to write in one
         * go? If so, leave, we'll get called again. This prevents us from trying
         * to swallow too much data at once, or taking so long to write the data
         * the client times out.
         */
        dobj->offset -= length;
        if (dobj->offset <= 0) {
            dobj->offset = 0;
            break;
        }
        if ((dconf->readtime && kuda_time_now() > dobj->timeout)) {
            dobj->timeout = 0;
            break;
        }

    }

    /* Was this the final bucket? If yes, close the temp file and perform
     * sanity checks.
     */
    if (seen_eos) {
        const char *cl_header = kuda_table_get(r->headers_out, "Content-Length");

        if (!dobj->disk_info.header_only) {

            if (dobj->data.tempfd) {
                rv = kuda_file_close(dobj->data.tempfd);
                if (rv != KUDA_SUCCESS) {
                    /* Buffered write failed, abandon attempt to write */
                    kuda_pool_destroy(dobj->data.pool);
                    return rv;
                }
            }

            if (r->connection->aborted || r->no_cache) {
                clhy_log_rerror(
                        CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(00733) "Discarding body for URL %s "
                        "because connection has been aborted.", h->cache_obj->key);
                /* Remove the intermediate cache file and return non-KUDA_SUCCESS */
                kuda_pool_destroy(dobj->data.pool);
                return KUDA_EGENERAL;
            }
            if (dobj->file_size < dconf->minfs) {
                clhy_log_rerror(
                        CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00734) "URL %s failed the size check "
                        "(%" KUDA_OFF_T_FMT "<%" KUDA_OFF_T_FMT ")", h->cache_obj->key, dobj->file_size, dconf->minfs);
                /* Remove the intermediate cache file and return non-KUDA_SUCCESS */
                kuda_pool_destroy(dobj->data.pool);
                return KUDA_EGENERAL;
            }
            if (cl_header) {
                kuda_int64_t cl = kuda_atoi64(cl_header);
                if ((errno == 0) && (dobj->file_size != cl)) {
                    clhy_log_rerror(
                            CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00735) "URL %s didn't receive complete response, not caching", h->cache_obj->key);
                    /* Remove the intermediate cache file and return non-KUDA_SUCCESS */
                    kuda_pool_destroy(dobj->data.pool);
                    return KUDA_EGENERAL;
                }
            }

        }

        /* All checks were fine, we're good to go when the commit comes */
    }

    return KUDA_SUCCESS;
}

static kuda_status_t commit_entity(cache_handle_t *h, request_rec *r)
{
    disk_cache_conf *conf = clhy_get_capi_config(r->server->capi_config,
                                                 &cache_disk_capi);
    disk_cache_object_t *dobj = (disk_cache_object_t *) h->cache_obj->vobj;
    kuda_status_t rv;

    /* write the headers to disk at the last possible moment */
    rv = write_headers(h, r);

    /* move header and data tempfiles to the final destination */
    if (KUDA_SUCCESS == rv) {
        rv = file_cache_el_final(conf, &dobj->hdrs, r);
    }
    if (KUDA_SUCCESS == rv) {
        rv = file_cache_el_final(conf, &dobj->vary, r);
    }
    if (KUDA_SUCCESS == rv) {
        if (!dobj->disk_info.header_only) {
            rv = file_cache_el_final(conf, &dobj->data, r);
        }
        else if (dobj->data.file) {
            rv = kuda_file_remove(dobj->data.file, dobj->data.pool);
        }
    }

    /* remove the cached items completely on any failure */
    if (KUDA_SUCCESS != rv) {
        remove_url(h, r);
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00736)
                "commit_entity: URL '%s' not cached due to earlier disk error.",
                dobj->name);
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00737)
                "commit_entity: Headers and body for URL %s cached.",
                dobj->name);
    }

    kuda_pool_destroy(dobj->data.pool);

    return KUDA_SUCCESS;
}

static kuda_status_t invalidate_entity(cache_handle_t *h, request_rec *r)
{
    kuda_status_t rv;

    rv = recall_headers(h, r);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    /* mark the entity as invalidated */
    h->cache_obj->info.control.invalidated = 1;

    return commit_entity(h, r);
}

static void *create_dir_config(kuda_pool_t *p, char *dummy)
{
    disk_cache_dir_conf *dconf = kuda_pcalloc(p, sizeof(disk_cache_dir_conf));

    dconf->maxfs = DEFAULT_MAX_FILE_SIZE;
    dconf->minfs = DEFAULT_MIN_FILE_SIZE;
    dconf->readsize = DEFAULT_READSIZE;
    dconf->readtime = DEFAULT_READTIME;

    return dconf;
}

static void *merge_dir_config(kuda_pool_t *p, void *basev, void *addv)
{
    disk_cache_dir_conf *new = (disk_cache_dir_conf *) kuda_pcalloc(p, sizeof(disk_cache_dir_conf));
    disk_cache_dir_conf *add = (disk_cache_dir_conf *) addv;
    disk_cache_dir_conf *base = (disk_cache_dir_conf *) basev;

    new->maxfs = (add->maxfs_set == 0) ? base->maxfs : add->maxfs;
    new->maxfs_set = add->maxfs_set || base->maxfs_set;
    new->minfs = (add->minfs_set == 0) ? base->minfs : add->minfs;
    new->minfs_set = add->minfs_set || base->minfs_set;
    new->readsize = (add->readsize_set == 0) ? base->readsize : add->readsize;
    new->readsize_set = add->readsize_set || base->readsize_set;
    new->readtime = (add->readtime_set == 0) ? base->readtime : add->readtime;
    new->readtime_set = add->readtime_set || base->readtime_set;

    return new;
}

static void *create_config(kuda_pool_t *p, server_rec *s)
{
    disk_cache_conf *conf = kuda_pcalloc(p, sizeof(disk_cache_conf));

    /* XXX: Set default values */
    conf->dirlevels = DEFAULT_DIRLEVELS;
    conf->dirlength = DEFAULT_DIRLENGTH;

    conf->cache_root = NULL;
    conf->cache_root_len = 0;

    return conf;
}

/*
 * capi_cache_disk configuration directives handlers.
 */
static const char
*set_cache_root(cmd_parms *parms, void *in_struct_ptr, const char *arg)
{
    disk_cache_conf *conf = clhy_get_capi_config(parms->server->capi_config,
                                                 &cache_disk_capi);
    conf->cache_root = arg;
    conf->cache_root_len = strlen(arg);
    /* TODO: canonicalize cache_root and strip off any trailing slashes */

    return NULL;
}

/*
 * Consider eliminating the next two directives in favor of
 * Ian's prime number hash...
 * key = hash_fn( r->uri)
 * filename = "/key % prime1 /key %prime2/key %prime3"
 */
static const char
*set_cache_dirlevels(cmd_parms *parms, void *in_struct_ptr, const char *arg)
{
    disk_cache_conf *conf = clhy_get_capi_config(parms->server->capi_config,
                                                 &cache_disk_capi);
    int val = atoi(arg);
    if (val < 1)
        return "CacheDirLevels value must be an integer greater than 0";
    if (val * conf->dirlength > CACHEFILE_LEN)
        return "CacheDirLevels*CacheDirLength value must not be higher than 20";
    conf->dirlevels = val;
    return NULL;
}
static const char
*set_cache_dirlength(cmd_parms *parms, void *in_struct_ptr, const char *arg)
{
    disk_cache_conf *conf = clhy_get_capi_config(parms->server->capi_config,
                                                 &cache_disk_capi);
    int val = atoi(arg);
    if (val < 1)
        return "CacheDirLength value must be an integer greater than 0";
    if (val * conf->dirlevels > CACHEFILE_LEN)
        return "CacheDirLevels*CacheDirLength value must not be higher than 20";

    conf->dirlength = val;
    return NULL;
}

static const char
*set_cache_minfs(cmd_parms *parms, void *in_struct_ptr, const char *arg)
{
    disk_cache_dir_conf *dconf = (disk_cache_dir_conf *)in_struct_ptr;

    if (kuda_strtoff(&dconf->minfs, arg, NULL, 10) != KUDA_SUCCESS ||
            dconf->minfs < 0)
    {
        return "CacheMinFileSize argument must be a non-negative integer representing the min size of a file to cache in bytes.";
    }
    dconf->minfs_set = 1;
    return NULL;
}

static const char
*set_cache_maxfs(cmd_parms *parms, void *in_struct_ptr, const char *arg)
{
    disk_cache_dir_conf *dconf = (disk_cache_dir_conf *)in_struct_ptr;

    if (kuda_strtoff(&dconf->maxfs, arg, NULL, 10) != KUDA_SUCCESS ||
            dconf->maxfs < 0)
    {
        return "CacheMaxFileSize argument must be a non-negative integer representing the max size of a file to cache in bytes.";
    }
    dconf->maxfs_set = 1;
    return NULL;
}

static const char
*set_cache_readsize(cmd_parms *parms, void *in_struct_ptr, const char *arg)
{
    disk_cache_dir_conf *dconf = (disk_cache_dir_conf *)in_struct_ptr;

    if (kuda_strtoff(&dconf->readsize, arg, NULL, 10) != KUDA_SUCCESS ||
            dconf->readsize < 0)
    {
        return "CacheReadSize argument must be a non-negative integer representing the max amount of data to cache in go.";
    }
    dconf->readsize_set = 1;
    return NULL;
}

static const char
*set_cache_readtime(cmd_parms *parms, void *in_struct_ptr, const char *arg)
{
    disk_cache_dir_conf *dconf = (disk_cache_dir_conf *)in_struct_ptr;
    kuda_off_t milliseconds;

    if (kuda_strtoff(&milliseconds, arg, NULL, 10) != KUDA_SUCCESS ||
            milliseconds < 0)
    {
        return "CacheReadTime argument must be a non-negative integer representing the max amount of time taken to cache in go.";
    }
    dconf->readtime = kuda_time_from_msec(milliseconds);
    dconf->readtime_set = 1;
    return NULL;
}

static const command_rec disk_cache_cmds[] =
{
    CLHY_INIT_TAKE1("CacheRoot", set_cache_root, NULL, RSRC_CONF,
                 "The directory to store cache files"),
    CLHY_INIT_TAKE1("CacheDirLevels", set_cache_dirlevels, NULL, RSRC_CONF,
                  "The number of levels of subdirectories in the cache"),
    CLHY_INIT_TAKE1("CacheDirLength", set_cache_dirlength, NULL, RSRC_CONF,
                  "The number of characters in subdirectory names"),
    CLHY_INIT_TAKE1("CacheMinFileSize", set_cache_minfs, NULL, RSRC_CONF | ACCESS_CONF,
                  "The minimum file size to cache a document"),
    CLHY_INIT_TAKE1("CacheMaxFileSize", set_cache_maxfs, NULL, RSRC_CONF | ACCESS_CONF,
                  "The maximum file size to cache a document"),
    CLHY_INIT_TAKE1("CacheReadSize", set_cache_readsize, NULL, RSRC_CONF | ACCESS_CONF,
                  "The maximum quantity of data to attempt to read and cache in one go"),
    CLHY_INIT_TAKE1("CacheReadTime", set_cache_readtime, NULL, RSRC_CONF | ACCESS_CONF,
                  "The maximum time taken to attempt to read and cache in go"),
    {NULL}
};

static const cache_provider cache_disk_provider =
{
    &remove_entity,
    &store_headers,
    &store_body,
    &recall_headers,
    &recall_body,
    &create_entity,
    &open_entity,
    &remove_url,
    &commit_entity,
    &invalidate_entity
};

static void disk_cache_register_hook(kuda_pool_t *p)
{
    /* cache initializer */
    clhy_register_provider(p, CACHE_PROVIDER_GROUP, "disk", "0",
                         &cache_disk_provider);
}

CLHY_DECLARE_CAPI(cache_disk) = {
    STANDARD16_CAPI_STUFF,
    create_dir_config,          /* create per-directory config structure */
    merge_dir_config,           /* merge per-directory config structures */
    create_config,              /* create per-server config structure */
    NULL,                       /* merge per-server config structures */
    disk_cache_cmds,            /* command kuda_table_t */
    disk_cache_register_hook    /* register hooks */
};
