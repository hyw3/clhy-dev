/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_lib.h"
#include "kuda_file_io.h"
#include "kuda_strings.h"
#include "kuda_buckets.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "http_core.h"
#include "http_protocol.h"
#include "clhy_provider.h"
#include "clhy_socache.h"
#include "util_filter.h"
#include "util_script.h"
#include "util_charset.h"
#include "util_mutex.h"

#include "capi_cache.h"
#include "capi_status.h"

#include "cache_socache_common.h"

/*
 * capi_cache_socache: Shared Object Cache Based HTTP 1.1 Cache.
 *
 * Flow to Find the entry:
 *   Incoming client requests URI /foo/bar/baz
 *   Fetch URI key (may contain Format #1 or Format #2)
 *   If format #1 (Contains a list of Vary Headers):
 *      Use each header name (from .header) with our request values (headers_in) to
 *      regenerate key using HeaderName+HeaderValue+.../foo/bar/baz
 *      re-read in key (must be format #2)
 *
 * Format #1:
 *   kuda_uint32_t format;
 *   kuda_time_t expire;
 *   kuda_array_t vary_headers (delimited by CRLF)
 *
 * Format #2:
 *   cache_socache_info_t (first sizeof(kuda_uint32_t) bytes is the format)
 *   entity name (sobj->name) [length is in cache_socache_info_t->name_len]
 *   r->headers_out (delimited by CRLF)
 *   CRLF
 *   r->headers_in (delimited by CRLF)
 *   CRLF
 */

cAPI CLHY_CAPI_DECLARE_DATA cache_socache_capi;

/*
 * cache_socache_object_t
 * Pointed to by cache_object_t::vobj
 */
typedef struct cache_socache_object_t
{
    kuda_pool_t *pool; /* pool */
    unsigned char *buffer; /* the cache buffer */
    kuda_size_t buffer_len; /* size of the buffer */
    kuda_bucket_brigade *body; /* brigade containing the body, if any */
    kuda_table_t *headers_in; /* Input headers to save */
    kuda_table_t *headers_out; /* Output headers to save */
    cache_socache_info_t socache_info; /* Header information. */
    kuda_size_t body_offset; /* offset to the start of the body */
    kuda_off_t body_length; /* length of the cached entity body */
    unsigned int newbody :1; /* whether a new body is present */
    kuda_time_t expire; /* when to expire the entry */

    const char *name; /* Requested URI without vary bits - suitable for mortals. */
    const char *key; /* On-disk prefix; URI with Vary bits (if present) */
    kuda_off_t offset; /* Max size to set aside */
    kuda_time_t timeout; /* Max time to set aside */
    unsigned int done :1; /* Is the attempt to cache complete? */
} cache_socache_object_t;

/*
 * capi_cache_socache configuration
 */
#define DEFAULT_MAX_FILE_SIZE 100*1024
#define DEFAULT_MAXTIME 86400
#define DEFAULT_MINTIME 600
#define DEFAULT_READSIZE 0
#define DEFAULT_READTIME 0

typedef struct cache_socache_provider_conf
{
    const char *args;
    clhy_socache_provider_t *socache_provider;
    clhy_socache_instance_t *socache_instance;
} cache_socache_provider_conf;

typedef struct cache_socache_conf
{
    cache_socache_provider_conf *provider;
} cache_socache_conf;

typedef struct cache_socache_dir_conf
{
    kuda_off_t max; /* maximum file size for cached files */
    kuda_time_t maxtime; /* maximum expiry time */
    kuda_time_t mintime; /* minimum expiry time */
    kuda_off_t readsize; /* maximum data to attempt to cache in one go */
    kuda_time_t readtime; /* maximum time taken to cache in one go */
    unsigned int max_set :1;
    unsigned int maxtime_set :1;
    unsigned int mintime_set :1;
    unsigned int readsize_set :1;
    unsigned int readtime_set :1;
} cache_socache_dir_conf;

/* Shared object cache and mutex */
static const char * const cache_socache_id = "cache-socache";
static kuda_global_mutex_t *socache_mutex = NULL;

/*
 * Local static functions
 */

static kuda_status_t read_array(request_rec *r, kuda_array_header_t *arr,
        unsigned char *buffer, kuda_size_t buffer_len, kuda_size_t *slider)
{
    kuda_size_t val = *slider;

    while (*slider < buffer_len) {
        if (buffer[*slider] == '\r') {
            if (val == *slider) {
                (*slider)++;
                return KUDA_SUCCESS;
            }
            *((const char **) kuda_array_push(arr)) = kuda_pstrndup(r->pool,
                    (const char *) buffer + val, *slider - val);
            (*slider)++;
            if (buffer[*slider] == '\n') {
                (*slider)++;
            }
            val = *slider;
        }
        else if (buffer[*slider] == '\0') {
            (*slider)++;
            return KUDA_SUCCESS;
        }
        else {
            (*slider)++;
        }
    }

    return KUDA_EOF;
}

static kuda_status_t store_array(kuda_array_header_t *arr, unsigned char *buffer,
        kuda_size_t buffer_len, kuda_size_t *slider)
{
    int i, len;
    const char **elts;

    elts = (const char **) arr->elts;

    for (i = 0; i < arr->nelts; i++) {
        kuda_size_t e_len = strlen(elts[i]);
        if (e_len + 3 >= buffer_len - *slider) {
            return KUDA_EOF;
        }
        len = kuda_snprintf(buffer ? (char *) buffer + *slider : NULL,
                buffer ? buffer_len - *slider : 0, "%s" CRLF, elts[i]);
        *slider += len;
    }
    if (buffer) {
        memcpy(buffer + *slider, CRLF, sizeof(CRLF) - 1);
    }
    *slider += sizeof(CRLF) - 1;

    return KUDA_SUCCESS;
}

static kuda_status_t read_table(cache_handle_t *handle, request_rec *r,
        kuda_table_t *table, unsigned char *buffer, kuda_size_t buffer_len,
        kuda_size_t *slider)
{
    kuda_size_t key = *slider, colon = 0, len = 0;

    while (*slider < buffer_len) {
        if (buffer[*slider] == ':') {
            if (!colon) {
                colon = *slider;
            }
            (*slider)++;
        }
        else if (buffer[*slider] == '\r') {
            len = colon;
            if (key == *slider) {
                (*slider)++;
                if (buffer[*slider] == '\n') {
                    (*slider)++;
                }
                return KUDA_SUCCESS;
            }
            if (!colon || buffer[colon++] != ':') {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02344)
                        "Premature end of cache headers.");
                return KUDA_EGENERAL;
            }
            while (kuda_isspace(buffer[colon])) {
                colon++;
            }
            kuda_table_addn(table, kuda_pstrndup(r->pool, (const char *) buffer
                    + key, len - key), kuda_pstrndup(r->pool,
                    (const char *) buffer + colon, *slider - colon));
            (*slider)++;
            if (buffer[*slider] == '\n') {
                (*slider)++;
            }
            key = *slider;
            colon = 0;
        }
        else if (buffer[*slider] == '\0') {
            (*slider)++;
            return KUDA_SUCCESS;
        }
        else {
            (*slider)++;
        }
    }

    return KUDA_EOF;
}

static kuda_status_t store_table(kuda_table_t *table, unsigned char *buffer,
        kuda_size_t buffer_len, kuda_size_t *slider)
{
    int i, len;
    kuda_table_entry_t *elts;

    elts = (kuda_table_entry_t *) kuda_table_elts(table)->elts;
    for (i = 0; i < kuda_table_elts(table)->nelts; ++i) {
        if (elts[i].key != NULL) {
            kuda_size_t key_len = strlen(elts[i].key);
            kuda_size_t val_len = strlen(elts[i].val);
            if (key_len + val_len + 5 >= buffer_len - *slider) {
                return KUDA_EOF;
            }
            len = kuda_snprintf(buffer ? (char *) buffer + *slider : NULL,
                    buffer ? buffer_len - *slider : 0, "%s: %s" CRLF,
                    elts[i].key, elts[i].val);
            *slider += len;
        }
    }
    if (3 >= buffer_len - *slider) {
        return KUDA_EOF;
    }
    if (buffer) {
        memcpy(buffer + *slider, CRLF, sizeof(CRLF) - 1);
    }
    *slider += sizeof(CRLF) - 1;

    return KUDA_SUCCESS;
}

static const char* regen_key(kuda_pool_t *p, kuda_table_t *headers,
        kuda_array_header_t *varray, const char *oldkey)
{
    struct iovec *iov;
    int i, k;
    int nvec;
    const char *header;
    const char **elts;

    nvec = (varray->nelts * 2) + 1;
    iov = kuda_palloc(p, sizeof(struct iovec) * nvec);
    elts = (const char **) varray->elts;

    /* TODO:
     *    - Handle multiple-value headers better. (sort them?)
     *    - Handle Case in-sensitive Values better.
     *        This isn't the end of the world, since it just lowers the cache
     *        hit rate, but it would be nice to fix.
     *
     * The majority are case insenstive if they are values (encoding etc).
     * Most of rfc2616 is case insensitive on header contents.
     *
     * So the better solution may be to identify headers which should be
     * treated case-sensitive?
     *  HTTP URI's (3.2.3) [host and scheme are insensitive]
     *  HTTP method (5.1.1)
     *  HTTP-date values (3.3.1)
     *  3.7 Media Types [exerpt]
     *     The type, subtype, and parameter attribute names are case-
     *     insensitive. Parameter values might or might not be case-sensitive,
     *     depending on the semantics of the parameter name.
     *  4.20 Except [exerpt]
     *     Comparison of expectation values is case-insensitive for unquoted
     *     tokens (including the 100-continue token), and is case-sensitive for
     *     quoted-string expectation-extensions.
     */

    for (i = 0, k = 0; i < varray->nelts; i++) {
        header = kuda_table_get(headers, elts[i]);
        if (!header) {
            header = "";
        }
        iov[k].iov_base = (char*) elts[i];
        iov[k].iov_len = strlen(elts[i]);
        k++;
        iov[k].iov_base = (char*) header;
        iov[k].iov_len = strlen(header);
        k++;
    }
    iov[k].iov_base = (char*) oldkey;
    iov[k].iov_len = strlen(oldkey);
    k++;

    return kuda_pstrcatv(p, iov, k, NULL);
}

static int array_alphasort(const void *fn1, const void *fn2)
{
    return strcmp(*(char**) fn1, *(char**) fn2);
}

static void tokens_to_array(kuda_pool_t *p, const char *data,
        kuda_array_header_t *arr)
{
    char *token;

    while ((token = clhy_get_list_item(p, &data)) != NULL) {
        *((const char **) kuda_array_push(arr)) = token;
    }

    /* Sort it so that "Vary: A, B" and "Vary: B, A" are stored the same. */
    qsort((void *) arr->elts, arr->nelts, sizeof(char *), array_alphasort);
}

/*
 * Hook and capi_cache callback functions
 */
static int create_entity(cache_handle_t *h, request_rec *r, const char *key,
        kuda_off_t len, kuda_bucket_brigade *bb)
{
    cache_socache_dir_conf *dconf =
            clhy_get_capi_config(r->per_dir_config, &cache_socache_capi);
    cache_socache_conf *conf = clhy_get_capi_config(r->server->capi_config,
            &cache_socache_capi);
    cache_object_t *obj;
    cache_socache_object_t *sobj;
    kuda_size_t total;

    if (conf->provider == NULL) {
        return DECLINED;
    }

    /* we don't support caching of range requests (yet) */
    /* TODO: but we could */
    if (r->status == HTTP_PARTIAL_CONTENT) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02345)
                "URL %s partial content response not cached",
                key);
        return DECLINED;
    }

    /*
     * We have a chicken and egg problem. We don't know until we
     * attempt to store_headers just how big the response will be
     * and whether it will fit in the cache limits set. But we
     * need to make a decision now as to whether we plan to try.
     * If we make the wrong decision, we could prevent another
     * cache implementation, such as cache_disk, from getting the
     * opportunity to cache, and that would be unfortunate.
     *
     * In a series of tests, from cheapest to most expensive,
     * decide whether or not to ignore this attempt to cache,
     * with a small margin just to be sure.
     */
    if (len < 0) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02346)
                "URL '%s' had no explicit size, ignoring", key);
        return DECLINED;
    }
    if (len > dconf->max) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02347)
                "URL '%s' body larger than limit, ignoring "
                "(%" KUDA_OFF_T_FMT " > %" KUDA_OFF_T_FMT ")",
                key, len, dconf->max);
        return DECLINED;
    }

    /* estimate the total cached size, given current headers */
    total = len + sizeof(cache_socache_info_t) + strlen(key);
    if (KUDA_SUCCESS != store_table(r->headers_out, NULL, dconf->max, &total)
            || KUDA_SUCCESS != store_table(r->headers_in, NULL, dconf->max,
                    &total)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02348)
                "URL '%s' estimated headers size larger than limit, ignoring "
                "(%" KUDA_SIZE_T_FMT " > %" KUDA_OFF_T_FMT ")",
                key, total, dconf->max);
        return DECLINED;
    }

    if (total >= dconf->max) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02349)
                "URL '%s' body and headers larger than limit, ignoring "
                "(%" KUDA_OFF_T_FMT " > %" KUDA_OFF_T_FMT ")",
                key, len, dconf->max);
        return DECLINED;
    }

    /* Allocate and initialize cache_object_t and cache_socache_object_t */
    h->cache_obj = obj = kuda_pcalloc(r->pool, sizeof(*obj));
    obj->vobj = sobj = kuda_pcalloc(r->pool, sizeof(*sobj));

    obj->key = kuda_pstrdup(r->pool, key);
    sobj->key = obj->key;
    sobj->name = obj->key;

    return OK;
}

static int open_entity(cache_handle_t *h, request_rec *r, const char *key)
{
    cache_socache_dir_conf *dconf =
            clhy_get_capi_config(r->per_dir_config, &cache_socache_capi);
    cache_socache_conf *conf = clhy_get_capi_config(r->server->capi_config,
            &cache_socache_capi);
    kuda_uint32_t format;
    kuda_size_t slider;
    unsigned int buffer_len;
    const char *nkey;
    kuda_status_t rc;
    cache_object_t *obj;
    cache_info *info;
    cache_socache_object_t *sobj;
    kuda_size_t len;

    nkey = NULL;
    h->cache_obj = NULL;

    if (!conf->provider || !conf->provider->socache_instance) {
        return DECLINED;
    }

    /* Create and init the cache object */
    obj = kuda_pcalloc(r->pool, sizeof(cache_object_t));
    sobj = kuda_pcalloc(r->pool, sizeof(cache_socache_object_t));

    info = &(obj->info);

    /* Create a temporary pool for the buffer, and destroy it if something
     * goes wrong so we don't have large buffers of unused memory hanging
     * about for the lifetime of the response.
     */
    kuda_pool_create(&sobj->pool, r->pool);

    sobj->buffer = kuda_palloc(sobj->pool, dconf->max);
    sobj->buffer_len = dconf->max;

    /* attempt to retrieve the cached entry */
    if (socache_mutex) {
        kuda_status_t status = kuda_global_mutex_lock(socache_mutex);
        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02350)
                    "could not acquire lock, ignoring: %s", obj->key);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return DECLINED;
        }
    }
    buffer_len = sobj->buffer_len;
    rc = conf->provider->socache_provider->retrieve(
            conf->provider->socache_instance, r->server, (unsigned char *) key,
            strlen(key), sobj->buffer, &buffer_len, r->pool);
    if (socache_mutex) {
        kuda_status_t status = kuda_global_mutex_unlock(socache_mutex);
        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02351)
                    "could not release lock, ignoring: %s", obj->key);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return DECLINED;
        }
    }
    if (rc != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rc, r, CLHYLOGNO(02352)
                "Key not found in cache: %s", key);
        kuda_pool_destroy(sobj->pool);
        sobj->pool = NULL;
        return DECLINED;
    }
    if (buffer_len >= sobj->buffer_len) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rc, r, CLHYLOGNO(02353)
                "Key found in cache but too big, ignoring: %s", key);
        kuda_pool_destroy(sobj->pool);
        sobj->pool = NULL;
        return DECLINED;
    }

    /* read the format from the cache file */
    memcpy(&format, sobj->buffer, sizeof(format));
    slider = sizeof(format);

    if (format == CACHE_SOCACHE_VARY_FORMAT_VERSION) {
        kuda_array_header_t* varray;
        kuda_time_t expire;

        memcpy(&expire, sobj->buffer + slider, sizeof(expire));
        slider += sizeof(expire);

        varray = kuda_array_make(r->pool, 5, sizeof(char*));
        rc = read_array(r, varray, sobj->buffer, buffer_len, &slider);
        if (rc != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rc, r, CLHYLOGNO(02354)
                    "Cannot parse vary entry for key: %s", key);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return DECLINED;
        }

        nkey = regen_key(r->pool, r->headers_in, varray, key);

        /* attempt to retrieve the cached entry */
        if (socache_mutex) {
            kuda_status_t status = kuda_global_mutex_lock(socache_mutex);
            if (status != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02355)
                        "could not acquire lock, ignoring: %s", obj->key);
                kuda_pool_destroy(sobj->pool);
                sobj->pool = NULL;
                return DECLINED;
            }
        }
        buffer_len = sobj->buffer_len;
        rc = conf->provider->socache_provider->retrieve(
                conf->provider->socache_instance, r->server,
                (unsigned char *) nkey, strlen(nkey), sobj->buffer,
                &buffer_len, r->pool);
        if (socache_mutex) {
            kuda_status_t status = kuda_global_mutex_unlock(socache_mutex);
            if (status != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02356)
                        "could not release lock, ignoring: %s", obj->key);
                kuda_pool_destroy(sobj->pool);
                sobj->pool = NULL;
                return DECLINED;
            }
        }
        if (rc != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rc, r, CLHYLOGNO(02357)
                    "Key not found in cache: %s", key);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return DECLINED;
        }
        if (buffer_len >= sobj->buffer_len) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rc, r, CLHYLOGNO(02358)
                    "Key found in cache but too big, ignoring: %s", key);
            goto fail;
        }

    }
    else if (format != CACHE_SOCACHE_DISK_FORMAT_VERSION) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02359)
                "Key '%s' found in cache has version %d, expected %d, ignoring",
                key, format, CACHE_SOCACHE_DISK_FORMAT_VERSION);
        goto fail;
    }
    else {
        nkey = key;
    }

    obj->key = nkey;
    sobj->key = nkey;
    sobj->name = key;

    if (buffer_len >= sizeof(cache_socache_info_t)) {
        memcpy(&sobj->socache_info, sobj->buffer, sizeof(cache_socache_info_t));
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rc, r, CLHYLOGNO(02360)
                "Cache entry for key '%s' too short, removing", nkey);
        goto fail;
    }
    slider = sizeof(cache_socache_info_t);

    /* Store it away so we can get it later. */
    info->status = sobj->socache_info.status;
    info->date = sobj->socache_info.date;
    info->expire = sobj->socache_info.expire;
    info->request_time = sobj->socache_info.request_time;
    info->response_time = sobj->socache_info.response_time;

    memcpy(&info->control, &sobj->socache_info.control, sizeof(cache_control_t));

    if (sobj->socache_info.name_len <= buffer_len - slider) {
        if (strncmp((const char *) sobj->buffer + slider, sobj->name,
                sobj->socache_info.name_len)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rc, r, CLHYLOGNO(02361)
                    "Cache entry for key '%s' URL mismatch, ignoring", nkey);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return DECLINED;
        }
        slider += sobj->socache_info.name_len;
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rc, r, CLHYLOGNO(02362)
                "Cache entry for key '%s' too short, removing", nkey);
        goto fail;
    }

    /* Is this a cached HEAD request? */
    if (sobj->socache_info.header_only && !r->header_only) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, KUDA_SUCCESS, r, CLHYLOGNO(02363)
                "HEAD request cached, non-HEAD requested, ignoring: %s",
                sobj->key);
        kuda_pool_destroy(sobj->pool);
        sobj->pool = NULL;
        return DECLINED;
    }

    h->req_hdrs = kuda_table_make(r->pool, 20);
    h->resp_hdrs = kuda_table_make(r->pool, 20);

    /* Call routine to read the header lines/status line */
    if (KUDA_SUCCESS != read_table(h, r, h->resp_hdrs, sobj->buffer, buffer_len,
            &slider)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rc, r, CLHYLOGNO(02364)
                "Cache entry for key '%s' response headers unreadable, removing", nkey);
        goto fail;
    }
    if (KUDA_SUCCESS != read_table(h, r, h->req_hdrs, sobj->buffer, buffer_len,
            &slider)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rc, r, CLHYLOGNO(02365)
                "Cache entry for key '%s' request headers unreadable, removing", nkey);
        goto fail;
    }

    /* Retrieve the body if we have one */
    sobj->body = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
    len = buffer_len - slider;

    /*
     *  Optimisation: if the body is small, we want to make a
     *  copy of the body and free the temporary pool, as we
     *  don't want large blocks of unused memory hanging around
     *  to the end of the response. In contrast, if the body is
     *  large, we would rather leave the body where it is in the
     *  temporary pool, and save ourselves the copy.
     */
    if (len * 2 > dconf->max) {
        kuda_bucket *e;

        /* large - use the brigade as is, we're done */
        e = kuda_bucket_immortal_create((const char *) sobj->buffer + slider,
                len, r->connection->bucket_alloc);

        KUDA_BRIGADE_INSERT_TAIL(sobj->body, e);
    }
    else {

        /* small - make a copy of the data... */
        kuda_brigade_write(sobj->body, NULL, NULL, (const char *) sobj->buffer
                + slider, len);

        /* ...and get rid of the large memory buffer */
        kuda_pool_destroy(sobj->pool);
        sobj->pool = NULL;
    }

    /* make the configuration stick */
    h->cache_obj = obj;
    obj->vobj = sobj;

    return OK;

fail:
    if (socache_mutex) {
        kuda_status_t status = kuda_global_mutex_lock(socache_mutex);
        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02366)
                    "could not acquire lock, ignoring: %s", obj->key);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return DECLINED;
        }
    }
    conf->provider->socache_provider->remove(
            conf->provider->socache_instance, r->server,
            (unsigned char *) nkey, strlen(nkey), r->pool);
    if (socache_mutex) {
        kuda_status_t status = kuda_global_mutex_unlock(socache_mutex);
        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02367)
                    "could not release lock, ignoring: %s", obj->key);
        }
    }
    kuda_pool_destroy(sobj->pool);
    sobj->pool = NULL;
    return DECLINED;
}

static int remove_entity(cache_handle_t *h)
{
    /* Null out the cache object pointer so next time we start from scratch  */
    h->cache_obj = NULL;
    return OK;
}

static int remove_url(cache_handle_t *h, request_rec *r)
{
    cache_socache_conf *conf = clhy_get_capi_config(r->server->capi_config,
            &cache_socache_capi);
    cache_socache_object_t *sobj;

    sobj = (cache_socache_object_t *) h->cache_obj->vobj;
    if (!sobj) {
        return DECLINED;
    }

    /* Remove the key from the cache */
    if (socache_mutex) {
        kuda_status_t status = kuda_global_mutex_lock(socache_mutex);
        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02368)
                    "could not acquire lock, ignoring: %s", sobj->key);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return DECLINED;
        }
    }
    conf->provider->socache_provider->remove(conf->provider->socache_instance,
            r->server, (unsigned char *) sobj->key, strlen(sobj->key), r->pool);
    if (socache_mutex) {
        kuda_status_t status = kuda_global_mutex_unlock(socache_mutex);
        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02369)
                    "could not release lock, ignoring: %s", sobj->key);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return DECLINED;
        }
    }

    return OK;
}

static kuda_status_t recall_headers(cache_handle_t *h, request_rec *r)
{
    /* we recalled the headers during open_entity, so do nothing */
    return KUDA_SUCCESS;
}

static kuda_status_t recall_body(cache_handle_t *h, kuda_pool_t *p,
        kuda_bucket_brigade *bb)
{
    cache_socache_object_t *sobj = (cache_socache_object_t*) h->cache_obj->vobj;
    kuda_bucket *e;

    e = KUDA_BRIGADE_FIRST(sobj->body);

    if (e != KUDA_BRIGADE_SENTINEL(sobj->body)) {
        KUDA_BUCKET_REMOVE(e);
        KUDA_BRIGADE_INSERT_TAIL(bb, e);
    }

    return KUDA_SUCCESS;
}

static kuda_status_t store_headers(cache_handle_t *h, request_rec *r,
        cache_info *info)
{
    cache_socache_dir_conf *dconf =
            clhy_get_capi_config(r->per_dir_config, &cache_socache_capi);
    cache_socache_conf *conf = clhy_get_capi_config(r->server->capi_config,
            &cache_socache_capi);
    kuda_size_t slider;
    kuda_status_t rv;
    cache_object_t *obj = h->cache_obj;
    cache_socache_object_t *sobj = (cache_socache_object_t*) obj->vobj;
    cache_socache_info_t *socache_info;

    memcpy(&h->cache_obj->info, info, sizeof(cache_info));

    if (r->headers_out) {
        sobj->headers_out = clhy_cache_cacheable_headers_out(r);
    }

    if (r->headers_in) {
        sobj->headers_in = clhy_cache_cacheable_headers_in(r);
    }

    sobj->expire
            = obj->info.expire > r->request_time + dconf->maxtime ? r->request_time
                    + dconf->maxtime
                    : obj->info.expire + dconf->mintime;

    kuda_pool_create(&sobj->pool, r->pool);

    sobj->buffer = kuda_palloc(sobj->pool, dconf->max);
    sobj->buffer_len = dconf->max;
    socache_info = (cache_socache_info_t *) sobj->buffer;

    if (sobj->headers_out) {
        const char *vary;

        vary = kuda_table_get(sobj->headers_out, "Vary");

        if (vary) {
            kuda_array_header_t* varray;
            kuda_uint32_t format = CACHE_SOCACHE_VARY_FORMAT_VERSION;

            memcpy(sobj->buffer, &format, sizeof(format));
            slider = sizeof(format);

            memcpy(sobj->buffer + slider, &obj->info.expire,
                    sizeof(obj->info.expire));
            slider += sizeof(obj->info.expire);

            varray = kuda_array_make(r->pool, 6, sizeof(char*));
            tokens_to_array(r->pool, vary, varray);

            if (KUDA_SUCCESS != (rv = store_array(varray, sobj->buffer,
                    sobj->buffer_len, &slider))) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02370)
                        "buffer too small for Vary array, caching aborted: %s",
                        obj->key);
                kuda_pool_destroy(sobj->pool);
                sobj->pool = NULL;
                return rv;
            }
            if (socache_mutex) {
                kuda_status_t status = kuda_global_mutex_lock(socache_mutex);
                if (status != KUDA_SUCCESS) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02371)
                            "could not acquire lock, ignoring: %s", obj->key);
                    kuda_pool_destroy(sobj->pool);
                    sobj->pool = NULL;
                    return status;
                }
            }
            rv = conf->provider->socache_provider->store(
                    conf->provider->socache_instance, r->server,
                    (unsigned char *) obj->key, strlen(obj->key), sobj->expire,
                    (unsigned char *) sobj->buffer, (unsigned int) slider,
                    sobj->pool);
            if (socache_mutex) {
                kuda_status_t status = kuda_global_mutex_unlock(socache_mutex);
                if (status != KUDA_SUCCESS) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02372)
                            "could not release lock, ignoring: %s", obj->key);
                }
            }
            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, r, CLHYLOGNO(02373)
                        "Vary not written to cache, ignoring: %s", obj->key);
                kuda_pool_destroy(sobj->pool);
                sobj->pool = NULL;
                return rv;
            }

            obj->key = sobj->key = regen_key(r->pool, sobj->headers_in, varray,
                    sobj->name);
        }
    }

    socache_info->format = CACHE_SOCACHE_DISK_FORMAT_VERSION;
    socache_info->date = obj->info.date;
    socache_info->expire = obj->info.expire;
    socache_info->entity_version = sobj->socache_info.entity_version++;
    socache_info->request_time = obj->info.request_time;
    socache_info->response_time = obj->info.response_time;
    socache_info->status = obj->info.status;

    if (r->header_only && r->status != HTTP_NOT_MODIFIED) {
        socache_info->header_only = 1;
    }
    else {
        socache_info->header_only = sobj->socache_info.header_only;
    }

    socache_info->name_len = strlen(sobj->name);

    memcpy(&socache_info->control, &obj->info.control, sizeof(cache_control_t));
    slider = sizeof(cache_socache_info_t);

    if (slider + socache_info->name_len >= sobj->buffer_len) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02374)
                "cache buffer too small for name: %s",
                sobj->name);
        kuda_pool_destroy(sobj->pool);
        sobj->pool = NULL;
        return KUDA_EGENERAL;
    }
    memcpy(sobj->buffer + slider, sobj->name, socache_info->name_len);
    slider += socache_info->name_len;

    if (sobj->headers_out) {
        if (KUDA_SUCCESS != store_table(sobj->headers_out, sobj->buffer,
                sobj->buffer_len, &slider)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02375)
                    "out-headers didn't fit in buffer: %s", sobj->name);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return KUDA_EGENERAL;
        }
    }

    /* Parse the vary header and dump those fields from the headers_in. */
    /* TODO: Make call to the same thing cache_select calls to crack Vary. */
    if (sobj->headers_in) {
        if (KUDA_SUCCESS != store_table(sobj->headers_in, sobj->buffer,
                sobj->buffer_len, &slider)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02376)
                    "in-headers didn't fit in buffer %s",
                    sobj->key);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return KUDA_EGENERAL;
        }
    }

    sobj->body_offset = slider;

    return KUDA_SUCCESS;
}

static kuda_status_t store_body(cache_handle_t *h, request_rec *r,
        kuda_bucket_brigade *in, kuda_bucket_brigade *out)
{
    kuda_bucket *e;
    kuda_status_t rv = KUDA_SUCCESS;
    cache_socache_object_t *sobj =
            (cache_socache_object_t *) h->cache_obj->vobj;
    cache_socache_dir_conf *dconf =
            clhy_get_capi_config(r->per_dir_config, &cache_socache_capi);
    int seen_eos = 0;

    if (!sobj->offset) {
        sobj->offset = dconf->readsize;
    }
    if (!sobj->timeout && dconf->readtime) {
        sobj->timeout = kuda_time_now() + dconf->readtime;
    }

    if (!sobj->newbody) {
        sobj->body_length = 0;
        sobj->newbody = 1;
    }
    if (sobj->offset) {
        kuda_brigade_partition(in, sobj->offset, &e);
    }

    while (KUDA_SUCCESS == rv && !KUDA_BRIGADE_EMPTY(in)) {
        const char *str;
        kuda_size_t length;

        e = KUDA_BRIGADE_FIRST(in);

        /* are we done completely? if so, pass any trailing buckets right through */
        if (sobj->done || !sobj->pool) {
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(out, e);
            continue;
        }

        /* have we seen eos yet? */
        if (KUDA_BUCKET_IS_EOS(e)) {
            seen_eos = 1;
            sobj->done = 1;
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(out, e);
            break;
        }

        /* honour flush buckets, we'll get called again */
        if (KUDA_BUCKET_IS_FLUSH(e)) {
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(out, e);
            break;
        }

        /* metadata buckets are preserved as is */
        if (KUDA_BUCKET_IS_METADATA(e)) {
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(out, e);
            continue;
        }

        /* read the bucket, write to the cache */
        rv = kuda_bucket_read(e, &str, &length, KUDA_BLOCK_READ);
        KUDA_BUCKET_REMOVE(e);
        KUDA_BRIGADE_INSERT_TAIL(out, e);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02377)
                    "Error when reading bucket for URL %s",
                    h->cache_obj->key);
            /* Remove the intermediate cache file and return non-KUDA_SUCCESS */
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return rv;
        }

        /* don't write empty buckets to the cache */
        if (!length) {
            continue;
        }

        sobj->body_length += length;
        if (sobj->body_length >= sobj->buffer_len - sobj->body_offset) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02378)
                    "URL %s failed the buffer size check "
                    "(%" KUDA_OFF_T_FMT ">=%" KUDA_SIZE_T_FMT ")",
                    h->cache_obj->key, sobj->body_length,
                    sobj->buffer_len - sobj->body_offset);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return KUDA_EGENERAL;
        }
        memcpy(sobj->buffer + sobj->body_offset + sobj->body_length - length,
               str, length);

        /* have we reached the limit of how much we're prepared to write in one
         * go? If so, leave, we'll get called again. This prevents us from trying
         * to swallow too much data at once, or taking so long to write the data
         * the client times out.
         */
        sobj->offset -= length;
        if (sobj->offset <= 0) {
            sobj->offset = 0;
            break;
        }
        if ((dconf->readtime && kuda_time_now() > sobj->timeout)) {
            sobj->timeout = 0;
            break;
        }

    }

    /* Was this the final bucket? If yes, perform sanity checks.
     */
    if (seen_eos) {
        const char *cl_header = kuda_table_get(r->headers_out, "Content-Length");

        if (r->connection->aborted || r->no_cache) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(02380)
                    "Discarding body for URL %s "
                    "because connection has been aborted.",
                    h->cache_obj->key);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return KUDA_EGENERAL;
        }
        if (cl_header) {
            kuda_off_t cl;
            char *cl_endp;
            if (kuda_strtoff(&cl, cl_header, &cl_endp, 10) != KUDA_SUCCESS
                    || *cl_endp != '\0' || cl != sobj->body_length) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02381)
                        "URL %s didn't receive complete response, not caching",
                        h->cache_obj->key);
                kuda_pool_destroy(sobj->pool);
                sobj->pool = NULL;
                return KUDA_EGENERAL;
            }
        }

        /* All checks were fine, we're good to go when the commit comes */

    }

    return KUDA_SUCCESS;
}

static kuda_status_t commit_entity(cache_handle_t *h, request_rec *r)
{
    cache_socache_conf *conf = clhy_get_capi_config(r->server->capi_config,
            &cache_socache_capi);
    cache_object_t *obj = h->cache_obj;
    cache_socache_object_t *sobj = (cache_socache_object_t *) obj->vobj;
    kuda_status_t rv;

    if (socache_mutex) {
        kuda_status_t status = kuda_global_mutex_lock(socache_mutex);
        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02384)
                    "could not acquire lock, ignoring: %s", obj->key);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return status;
        }
    }
    rv = conf->provider->socache_provider->store(
            conf->provider->socache_instance, r->server,
            (unsigned char *) sobj->key, strlen(sobj->key), sobj->expire,
            sobj->buffer, sobj->body_offset + sobj->body_length, sobj->pool);
    if (socache_mutex) {
        kuda_status_t status = kuda_global_mutex_unlock(socache_mutex);
        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02385)
                    "could not release lock, ignoring: %s", obj->key);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return status;
        }
    }
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, r, CLHYLOGNO(02386)
                "could not write to cache, ignoring: %s", sobj->key);
        goto fail;
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02387)
            "commit_entity: Headers and body for URL %s cached for maximum of %d seconds.",
            sobj->name, (kuda_uint32_t)kuda_time_sec(sobj->expire - r->request_time));

    kuda_pool_destroy(sobj->pool);
    sobj->pool = NULL;

    return KUDA_SUCCESS;

fail:
    /* For safety, remove any existing entry on failure, just in case it could not
     * be revalidated successfully.
     */
    if (socache_mutex) {
        kuda_status_t status = kuda_global_mutex_lock(socache_mutex);
        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02388)
                    "could not acquire lock, ignoring: %s", obj->key);
            kuda_pool_destroy(sobj->pool);
            sobj->pool = NULL;
            return rv;
        }
    }
    conf->provider->socache_provider->remove(conf->provider->socache_instance,
            r->server, (unsigned char *) sobj->key, strlen(sobj->key), r->pool);
    if (socache_mutex) {
        kuda_status_t status = kuda_global_mutex_unlock(socache_mutex);
        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02389)
                    "could not release lock, ignoring: %s", obj->key);
        }
    }

    kuda_pool_destroy(sobj->pool);
    sobj->pool = NULL;
    return rv;
}

static kuda_status_t invalidate_entity(cache_handle_t *h, request_rec *r)
{
    /* mark the entity as invalidated */
    h->cache_obj->info.control.invalidated = 1;

    return commit_entity(h, r);
}

static void *create_dir_config(kuda_pool_t *p, char *dummy)
{
    cache_socache_dir_conf *dconf =
            kuda_pcalloc(p, sizeof(cache_socache_dir_conf));

    dconf->max = DEFAULT_MAX_FILE_SIZE;
    dconf->maxtime = kuda_time_from_sec(DEFAULT_MAXTIME);
    dconf->mintime = kuda_time_from_sec(DEFAULT_MINTIME);
    dconf->readsize = DEFAULT_READSIZE;
    dconf->readtime = DEFAULT_READTIME;

    return dconf;
}

static void *merge_dir_config(kuda_pool_t *p, void *basev, void *addv)
{
    cache_socache_dir_conf
            *new =
                    (cache_socache_dir_conf *) kuda_pcalloc(p, sizeof(cache_socache_dir_conf));
    cache_socache_dir_conf *add = (cache_socache_dir_conf *) addv;
    cache_socache_dir_conf *base = (cache_socache_dir_conf *) basev;

    new->max = (add->max_set == 0) ? base->max : add->max;
    new->max_set = add->max_set || base->max_set;
    new->maxtime = (add->maxtime_set == 0) ? base->maxtime : add->maxtime;
    new->maxtime_set = add->maxtime_set || base->maxtime_set;
    new->mintime = (add->mintime_set == 0) ? base->mintime : add->mintime;
    new->mintime_set = add->mintime_set || base->mintime_set;
    new->readsize = (add->readsize_set == 0) ? base->readsize : add->readsize;
    new->readsize_set = add->readsize_set || base->readsize_set;
    new->readtime = (add->readtime_set == 0) ? base->readtime : add->readtime;
    new->readtime_set = add->readtime_set || base->readtime_set;

    return new;
}

static void *create_config(kuda_pool_t *p, server_rec *s)
{
    cache_socache_conf *conf = kuda_pcalloc(p, sizeof(cache_socache_conf));

    return conf;
}

static void *merge_config(kuda_pool_t *p, void *basev, void *overridesv)
{
    cache_socache_conf *ps;
    cache_socache_conf *base = (cache_socache_conf *) basev;
    cache_socache_conf *overrides = (cache_socache_conf *) overridesv;

    /* socache server config only has one field */
    ps = overrides ? overrides : base;

    return ps;
}

/*
 * capi_cache_socache configuration directives handlers.
 */
static const char *set_cache_socache(cmd_parms *cmd, void *in_struct_ptr,
        const char *arg)
{
    cache_socache_conf *conf = clhy_get_capi_config(cmd->server->capi_config,
            &cache_socache_capi);
    cache_socache_provider_conf *provider = conf->provider
            = kuda_pcalloc(cmd->pool, sizeof(cache_socache_provider_conf));

    const char *err = NULL, *sep, *name;

    /* Argument is of form 'name:args' or just 'name'. */
    sep = clhy_strchr_c(arg, ':');
    if (sep) {
        name = kuda_pstrmemdup(cmd->pool, arg, sep - arg);
        sep++;
        provider->args = sep;
    }
    else {
        name = arg;
    }

    provider->socache_provider = clhy_lookup_provider(CLHY_SOCACHE_PROVIDER_GROUP,
            name, CLHY_SOCACHE_PROVIDER_VERSION);
    if (provider->socache_provider == NULL) {
        err = kuda_psprintf(cmd->pool,
                "Unknown socache provider '%s'. Maybe you need "
                    "to load the appropriate socache cAPI "
                    "(capi_socache_%s?)", name, name);
    }
    return err;
}

static const char *set_cache_max(cmd_parms *parms, void *in_struct_ptr,
        const char *arg)
{
    cache_socache_dir_conf *dconf = (cache_socache_dir_conf *) in_struct_ptr;

    if (kuda_strtoff(&dconf->max, arg, NULL, 10) != KUDA_SUCCESS
            || dconf->max < 1024 || dconf->max > KUDA_UINT32_MAX) {
        return "CacheSocacheMaxSize argument must be a integer representing "
               "the max size of a cached entry (headers and body), at least 1024 "
               "and at most " KUDA_STRINGIFY(KUDA_UINT32_MAX);
    }
    dconf->max_set = 1;
    return NULL;
}

static const char *set_cache_maxtime(cmd_parms *parms, void *in_struct_ptr,
        const char *arg)
{
    cache_socache_dir_conf *dconf = (cache_socache_dir_conf *) in_struct_ptr;
    kuda_off_t seconds;

    if (kuda_strtoff(&seconds, arg, NULL, 10) != KUDA_SUCCESS || seconds < 0) {
        return "CacheSocacheMaxTime argument must be the maximum amount of time in seconds to cache an entry.";
    }
    dconf->maxtime = kuda_time_from_sec(seconds);
    dconf->maxtime_set = 1;
    return NULL;
}

static const char *set_cache_mintime(cmd_parms *parms, void *in_struct_ptr,
        const char *arg)
{
    cache_socache_dir_conf *dconf = (cache_socache_dir_conf *) in_struct_ptr;
    kuda_off_t seconds;

    if (kuda_strtoff(&seconds, arg, NULL, 10) != KUDA_SUCCESS || seconds < 0) {
        return "CacheSocacheMinTime argument must be the minimum amount of time in seconds to cache an entry.";
    }
    dconf->mintime = kuda_time_from_sec(seconds);
    dconf->mintime_set = 1;
    return NULL;
}

static const char *set_cache_readsize(cmd_parms *parms, void *in_struct_ptr,
        const char *arg)
{
    cache_socache_dir_conf *dconf = (cache_socache_dir_conf *) in_struct_ptr;

    if (kuda_strtoff(&dconf->readsize, arg, NULL, 10) != KUDA_SUCCESS
            || dconf->readsize < 0) {
        return "CacheSocacheReadSize argument must be a non-negative integer representing the max amount of data to cache in go.";
    }
    dconf->readsize_set = 1;
    return NULL;
}

static const char *set_cache_readtime(cmd_parms *parms, void *in_struct_ptr,
        const char *arg)
{
    cache_socache_dir_conf *dconf = (cache_socache_dir_conf *) in_struct_ptr;
    kuda_off_t milliseconds;

    if (kuda_strtoff(&milliseconds, arg, NULL, 10) != KUDA_SUCCESS
            || milliseconds < 0) {
        return "CacheSocacheReadTime argument must be a non-negative integer representing the max amount of time taken to cache in go.";
    }
    dconf->readtime = kuda_time_from_msec(milliseconds);
    dconf->readtime_set = 1;
    return NULL;
}

static kuda_status_t remove_lock(void *data)
{
    if (socache_mutex) {
        kuda_global_mutex_destroy(socache_mutex);
        socache_mutex = NULL;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t destroy_cache(void *data)
{
    server_rec *s = data;
    cache_socache_conf *conf =
            clhy_get_capi_config(s->capi_config, &cache_socache_capi);
    if (conf->provider && conf->provider->socache_instance) {
        conf->provider->socache_provider->destroy(
                conf->provider->socache_instance, s);
        conf->provider->socache_instance = NULL;
    }
    return KUDA_SUCCESS;
}

static int socache_status_hook(request_rec *r, int flags)
{
    kuda_status_t status = KUDA_SUCCESS;
    cache_socache_conf *conf = clhy_get_capi_config(r->server->capi_config,
                                                    &cache_socache_capi);
    if (!conf->provider || !conf->provider->socache_provider ||
        !conf->provider->socache_instance) {
        return DECLINED;
    }

    if (!(flags & CLHY_STATUS_SHORT)) {
        clhy_rputs("<hr>\n"
                 "<table cellspacing=0 cellpadding=0>\n"
                 "<tr><td bgcolor=\"#000000\">\n"
                 "<b><font color=\"#ffffff\" face=\"Arial,Helvetica\">"
                 "capi_cache_socache Status:</font></b>\n"
                 "</td></tr>\n"
                 "<tr><td bgcolor=\"#ffffff\">\n", r);
    }
    else {
        clhy_rputs("ModCacheSocacheStatus\n", r);
    }

    if (socache_mutex) {
        status = kuda_global_mutex_lock(socache_mutex);
        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02816)
                    "could not acquire lock for cache status");
        }
    }

    if (status != KUDA_SUCCESS) {
        if (!(flags & CLHY_STATUS_SHORT)) {
            clhy_rputs("No cache status data available\n", r);
        }
        else {
            clhy_rputs("NotAvailable\n", r);
        }
    } else {
        conf->provider->socache_provider->status(conf->provider->socache_instance,
                                                 r, flags);
    }

    if (socache_mutex && status == KUDA_SUCCESS) {
        status = kuda_global_mutex_unlock(socache_mutex);
        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(02817)
                    "could not release lock for cache status");
        }
    }

    if (!(flags & CLHY_STATUS_SHORT)) {
        clhy_rputs("</td></tr>\n</table>\n", r);
    }
    return OK;
}

static void socache_status_register(kuda_pool_t *p)
{
    KUDA_OPTIONAL_HOOK(clhy, status_hook, socache_status_hook, NULL, NULL, KUDA_HOOK_MIDDLE);
}

static int socache_precfg(kuda_pool_t *pconf, kuda_pool_t *plog, kuda_pool_t *ptmp)
{
    kuda_status_t rv = clhy_mutex_register(pconf, cache_socache_id, NULL,
            KUDA_LOCK_DEFAULT, 0);
    if (rv != KUDA_SUCCESS) {
        clhy_log_perror(CLHYLOG_MARK, CLHYLOG_CRIT, rv, plog, CLHYLOGNO(02390)
        "failed to register %s mutex", cache_socache_id);
        return 500; /* An HTTP status would be a misnomer! */
    }

    /* Register to handle capi_status status page generation */
    socache_status_register(pconf);

    return OK;
}

static int socache_post_config(kuda_pool_t *pconf, kuda_pool_t *plog,
        kuda_pool_t *ptmp, server_rec *base_server)
{
    server_rec *s;
    kuda_status_t rv;
    const char *errmsg;
    static struct clhy_socache_hints socache_hints =
    { 64, 2048, 60000000 };

    for (s = base_server; s; s = s->next) {
        cache_socache_conf *conf =
                clhy_get_capi_config(s->capi_config, &cache_socache_capi);

        if (!conf->provider) {
            continue;
        }

        if (!socache_mutex && conf->provider->socache_provider->flags
                & CLHY_SOCACHE_FLAG_NOTMPSAFE) {

            rv = clhy_global_mutex_create(&socache_mutex, NULL, cache_socache_id,
                    NULL, s, pconf, 0);
            if (rv != KUDA_SUCCESS) {
                clhy_log_perror(CLHYLOG_MARK, CLHYLOG_CRIT, rv, plog, CLHYLOGNO(02391)
                "failed to create %s mutex", cache_socache_id);
                return 500; /* An HTTP status would be a misnomer! */
            }
            kuda_pool_cleanup_register(pconf, NULL, remove_lock,
                    kuda_pool_cleanup_null);
        }

        errmsg = conf->provider->socache_provider->create(
                &conf->provider->socache_instance, conf->provider->args, ptmp,
                pconf);
        if (errmsg) {
            clhy_log_perror(CLHYLOG_MARK, CLHYLOG_CRIT, 0, plog,
                    CLHYLOGNO(02392) "%s", errmsg);
            return 500; /* An HTTP status would be a misnomer! */
        }

        rv = conf->provider->socache_provider->init(
                conf->provider->socache_instance, cache_socache_id,
                &socache_hints, s, pconf);
        if (rv != KUDA_SUCCESS) {
            clhy_log_perror(CLHYLOG_MARK, CLHYLOG_CRIT, rv, plog, CLHYLOGNO(02393)
            "failed to initialise %s cache", cache_socache_id);
            return 500; /* An HTTP status would be a misnomer! */
        }
        kuda_pool_cleanup_register(pconf, (void *) s, destroy_cache,
                kuda_pool_cleanup_null);

    }

    return OK;
}

static void socache_child_init(kuda_pool_t *p, server_rec *s)
{
    const char *lock;
    kuda_status_t rv;
    if (!socache_mutex) {
        return; /* don't waste the overhead of creating mutex & cache */
    }
    lock = kuda_global_mutex_lockfile(socache_mutex);
    rv = kuda_global_mutex_child_init(&socache_mutex, lock, p);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(02394)
                "failed to initialise mutex in child_init");
    }
}

static const command_rec cache_socache_cmds[] =
{
    CLHY_INIT_TAKE1("CacheSocache", set_cache_socache, NULL, RSRC_CONF,
            "The shared object cache to store cache files"),
    CLHY_INIT_TAKE1("CacheSocacheMaxTime", set_cache_maxtime, NULL, RSRC_CONF | ACCESS_CONF,
            "The maximum cache expiry age to cache a document in seconds"),
    CLHY_INIT_TAKE1("CacheSocacheMinTime", set_cache_mintime, NULL, RSRC_CONF | ACCESS_CONF,
            "The minimum cache expiry age to cache a document in seconds"),
    CLHY_INIT_TAKE1("CacheSocacheMaxSize", set_cache_max, NULL, RSRC_CONF | ACCESS_CONF,
            "The maximum cache entry size (headers and body) to cache a document"),
    CLHY_INIT_TAKE1("CacheSocacheReadSize", set_cache_readsize, NULL, RSRC_CONF | ACCESS_CONF,
            "The maximum quantity of data to attempt to read and cache in one go"),
    CLHY_INIT_TAKE1("CacheSocacheReadTime", set_cache_readtime, NULL, RSRC_CONF | ACCESS_CONF,
            "The maximum time taken to attempt to read and cache in go"),
    { NULL }
};

static const cache_provider cache_socache_provider =
{
    &remove_entity, &store_headers, &store_body, &recall_headers, &recall_body,
    &create_entity, &open_entity, &remove_url, &commit_entity,
    &invalidate_entity
};

static void cache_socache_register_hook(kuda_pool_t *p)
{
    /* cache initializer */
    clhy_register_provider(p, CACHE_PROVIDER_GROUP, "socache", "0",
            &cache_socache_provider);
    clhy_hook_pre_config(socache_precfg, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_post_config(socache_post_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_child_init(socache_child_init, NULL, NULL, KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(cache_socache) = { STANDARD16_CAPI_STUFF,
    create_dir_config,  /* create per-directory config structure */
    merge_dir_config, /* merge per-directory config structures */
    create_config, /* create per-server config structure */
    merge_config, /* merge per-server config structures */
    cache_socache_cmds, /* command kuda_table_t */
    cache_socache_register_hook /* register hooks */
};
