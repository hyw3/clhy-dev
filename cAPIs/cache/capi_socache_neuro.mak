# Microsoft Developer Studio Generated NMAKE File, Based on capi_socache_neuro.dsp
!IF "$(CFG)" == ""
CFG=capi_socache_neuro - Win32 Debug
!MESSAGE No configuration specified. Defaulting to capi_socache_neuro - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "capi_socache_neuro - Win32 Release" && "$(CFG)" != "capi_socache_neuro - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "capi_socache_neuro.mak" CFG="capi_socache_neuro - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "capi_socache_neuro - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "capi_socache_neuro - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "capi_socache_neuro - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_socache_neuro.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Release" "libkudadelman - Win32 Release" "libkuda - Win32 Release" "$(OUTDIR)\capi_socache_neuro.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudadelman - Win32 ReleaseCLEAN" "libwwhy - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\capi_socache_neuro.obj"
	-@erase "$(INTDIR)\capi_socache_neuro.res"
	-@erase "$(INTDIR)\capi_socache_neuro_src.idb"
	-@erase "$(INTDIR)\capi_socache_neuro_src.pdb"
	-@erase "$(OUTDIR)\capi_socache_neuro.exp"
	-@erase "$(OUTDIR)\capi_socache_neuro.lib"
	-@erase "$(OUTDIR)\capi_socache_neuro.pdb"
	-@erase "$(OUTDIR)\capi_socache_neuro.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/kuda/include" /I "../../include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_socache_neuro_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_socache_neuro.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "NDEBUG" /d BIN_NAME="capi_socache_neuro.so" /d LONG_NAME="socache_neuro_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_socache_neuro.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_socache_neuro.pdb" /debug /out:"$(OUTDIR)\capi_socache_neuro.so" /implib:"$(OUTDIR)\capi_socache_neuro.lib" /base:@..\..\platforms\win32\BaseAddr.ref,capi_socache_neuro.so /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\capi_socache_neuro.obj" \
	"$(INTDIR)\capi_socache_neuro.res" \
	"..\..\kudelrunsrc\kuda\Release\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Release\libkudadelman-1.lib" \
	"..\..\Release\libwwhy.lib"

"$(OUTDIR)\capi_socache_neuro.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\capi_socache_neuro.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_socache_neuro.so"
   if exist .\Release\capi_socache_neuro.so.manifest mt.exe -manifest .\Release\capi_socache_neuro.so.manifest -outputresource:.\Release\capi_socache_neuro.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "capi_socache_neuro - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_socache_neuro.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Debug" "libkudadelman - Win32 Debug" "libkuda - Win32 Debug" "$(OUTDIR)\capi_socache_neuro.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudadelman - Win32 DebugCLEAN" "libwwhy - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\capi_socache_neuro.obj"
	-@erase "$(INTDIR)\capi_socache_neuro.res"
	-@erase "$(INTDIR)\capi_socache_neuro_src.idb"
	-@erase "$(INTDIR)\capi_socache_neuro_src.pdb"
	-@erase "$(OUTDIR)\capi_socache_neuro.exp"
	-@erase "$(OUTDIR)\capi_socache_neuro.lib"
	-@erase "$(OUTDIR)\capi_socache_neuro.pdb"
	-@erase "$(OUTDIR)\capi_socache_neuro.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/kuda/include" /I "../../include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_socache_neuro_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_socache_neuro.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "_DEBUG" /d BIN_NAME="capi_socache_neuro.so" /d LONG_NAME="socache_neuro_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_socache_neuro.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_socache_neuro.pdb" /debug /out:"$(OUTDIR)\capi_socache_neuro.so" /implib:"$(OUTDIR)\capi_socache_neuro.lib" /base:@..\..\platforms\win32\BaseAddr.ref,capi_socache_neuro.so 
LINK32_OBJS= \
	"$(INTDIR)\capi_socache_neuro.obj" \
	"$(INTDIR)\capi_socache_neuro.res" \
	"..\..\kudelrunsrc\kuda\Debug\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Debug\libkudadelman-1.lib" \
	"..\..\Debug\libwwhy.lib"

"$(OUTDIR)\capi_socache_neuro.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\capi_socache_neuro.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_socache_neuro.so"
   if exist .\Debug\capi_socache_neuro.so.manifest mt.exe -manifest .\Debug\capi_socache_neuro.so.manifest -outputresource:.\Debug\capi_socache_neuro.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("capi_socache_neuro.dep")
!INCLUDE "capi_socache_neuro.dep"
!ELSE 
!MESSAGE Warning: cannot find "capi_socache_neuro.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "capi_socache_neuro - Win32 Release" || "$(CFG)" == "capi_socache_neuro - Win32 Debug"

!IF  "$(CFG)" == "capi_socache_neuro - Win32 Release"

"libkuda - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\..\cAPIs\cache"

"libkuda - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\cache"

!ELSEIF  "$(CFG)" == "capi_socache_neuro - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\..\cAPIs\cache"

"libkuda - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\cache"

!ENDIF 

!IF  "$(CFG)" == "capi_socache_neuro - Win32 Release"

"libkudadelman - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" 
   cd "..\..\cAPIs\cache"

"libkudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\cache"

!ELSEIF  "$(CFG)" == "capi_socache_neuro - Win32 Debug"

"libkudadelman - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" 
   cd "..\..\cAPIs\cache"

"libkudadelman - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\cache"

!ENDIF 

!IF  "$(CFG)" == "capi_socache_neuro - Win32 Release"

"libwwhy - Win32 Release" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" 
   cd ".\cAPIs\cache"

"libwwhy - Win32 ReleaseCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" RECURSE=1 CLEAN 
   cd ".\cAPIs\cache"

!ELSEIF  "$(CFG)" == "capi_socache_neuro - Win32 Debug"

"libwwhy - Win32 Debug" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" 
   cd ".\cAPIs\cache"

"libwwhy - Win32 DebugCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" RECURSE=1 CLEAN 
   cd ".\cAPIs\cache"

!ENDIF 

SOURCE=..\..\build\win32\wwhy.rc

!IF  "$(CFG)" == "capi_socache_neuro - Win32 Release"


"$(INTDIR)\capi_socache_neuro.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_socache_neuro.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../build\win32" /d "NDEBUG" /d BIN_NAME="capi_socache_neuro.so" /d LONG_NAME="socache_neuro_capi for cLHy" $(SOURCE)


!ELSEIF  "$(CFG)" == "capi_socache_neuro - Win32 Debug"


"$(INTDIR)\capi_socache_neuro.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_socache_neuro.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../build\win32" /d "_DEBUG" /d BIN_NAME="capi_socache_neuro.so" /d LONG_NAME="socache_neuro_capi for cLHy" $(SOURCE)


!ENDIF 

SOURCE=.\capi_socache_neuro.c

"$(INTDIR)\capi_socache_neuro.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

