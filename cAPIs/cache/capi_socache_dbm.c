/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_log.h"
#include "http_request.h"
#include "http_protocol.h"
#include "http_config.h"
#include "core_common.h"
#include "capi_status.h"

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_time.h"
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"
#include "kuda_dbm.h"

#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "clhy_socache.h"

#if CLHY_NEED_SET_MUTEX_PERMS
#include "unixd.h"
#endif

/* Use of the context structure must be thread-safe after the initial
 * create/init; callers must hold the mutex. */
struct clhy_socache_instance_t {
    const char *data_file;
    /* Pool must only be used with the mutex held. */
    kuda_pool_t *pool;
    kuda_time_t last_expiry;
    kuda_interval_time_t expiry_interval;
};

/**
 * Support for DBM library
 */
#define DBM_FILE_MODE ( KUDA_UREAD | KUDA_UWRITE | KUDA_GREAD | KUDA_WREAD )

#define DEFAULT_DBM_PREFIX "socache-dbm-"

/* ### this should use kuda_dbm_usednames. */
#if !defined(DBM_FILE_SUFFIX_DIR) && !defined(DBM_FILE_SUFFIX_PAG)
#if defined(DBM_SUFFIX)
#define DBM_FILE_SUFFIX_DIR DBM_SUFFIX
#define DBM_FILE_SUFFIX_PAG DBM_SUFFIX
#elif defined(__FreeBSD__) || (defined(DB_LOCK) && defined(DB_SHMEM))
#define DBM_FILE_SUFFIX_DIR ".db"
#define DBM_FILE_SUFFIX_PAG ".db"
#else
#define DBM_FILE_SUFFIX_DIR ".dir"
#define DBM_FILE_SUFFIX_PAG ".pag"
#endif
#endif

static void socache_dbm_expire(clhy_socache_instance_t *ctx, server_rec *s);

static kuda_status_t socache_dbm_remove(clhy_socache_instance_t *ctx,
                                       server_rec *s, const unsigned char *id,
                                       unsigned int idlen, kuda_pool_t *p);

static const char *socache_dbm_create(clhy_socache_instance_t **context,
                                      const char *arg,
                                      kuda_pool_t *tmp, kuda_pool_t *p)
{
    clhy_socache_instance_t *ctx;

    *context = ctx = kuda_pcalloc(p, sizeof *ctx);

    if (arg && *arg) {
        ctx->data_file = clhy_server_root_relative(p, arg);
        if (!ctx->data_file) {
            return kuda_psprintf(tmp, "Invalid cache file path %s", arg);
        }
    }

    kuda_pool_create(&ctx->pool, p);

    return NULL;
}

#if CLHY_NEED_SET_MUTEX_PERMS
static int try_chown(kuda_pool_t *p, server_rec *s,
                     const char *name, const char *suffix)
{
    if (suffix)
        name = kuda_pstrcat(p, name, suffix, NULL);
    if (-1 == chown(name, clhy_unixd_config.user_id,
                    (gid_t)-1 /* no gid change */ ))
    {
        if (errno != ENOENT)
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, KUDA_FROM_PLATFORM_ERROR(errno), s, CLHYLOGNO(00802)
                         "Can't change owner of %s", name);
        return -1;
    }
    return 0;
}
#endif


static kuda_status_t socache_dbm_init(clhy_socache_instance_t *ctx,
                                     const char *namespace,
                                     const struct clhy_socache_hints *hints,
                                     server_rec *s, kuda_pool_t *p)
{
    kuda_dbm_t *dbm;
    kuda_status_t rv;

    /* for the DBM we need the data file */
    if (ctx->data_file == NULL) {
        const char *path = kuda_pstrcat(p, DEFAULT_DBM_PREFIX, namespace,
                                       NULL);

        ctx->data_file = clhy_runtime_dir_relative(p, path);

        if (ctx->data_file == NULL) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00803)
                         "could not use default path '%s' for DBM socache",
                         path);
            return KUDA_EINVAL;
        }
    }

    /* open it once to create it and to make sure it _can_ be created */
    kuda_pool_clear(ctx->pool);

    if ((rv = kuda_dbm_open(&dbm, ctx->data_file,
            KUDA_DBM_RWCREATE, DBM_FILE_MODE, ctx->pool)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00804)
                     "Cannot create socache DBM file `%s'",
                     ctx->data_file);
        return rv;
    }
    kuda_dbm_close(dbm);

    ctx->expiry_interval = (hints && hints->expiry_interval
                            ? hints->expiry_interval : kuda_time_from_sec(30));

#if CLHY_NEED_SET_MUTEX_PERMS
    /*
     * We have to make sure the cLHy child processes have access to
     * the DBM file. But because there are brain-dead platforms where we
     * cannot exactly determine the suffixes we try all possibilities.
     */
    if (geteuid() == 0 /* is superuser */) {
        try_chown(p, s, ctx->data_file, NULL);
        if (try_chown(p, s, ctx->data_file, DBM_FILE_SUFFIX_DIR))
            if (try_chown(p, s, ctx->data_file, ".db"))
                try_chown(p, s, ctx->data_file, ".dir");
        if (try_chown(p, s, ctx->data_file, DBM_FILE_SUFFIX_PAG))
            if (try_chown(p, s, ctx->data_file, ".db"))
                try_chown(p, s, ctx->data_file, ".pag");
    }
#endif
    socache_dbm_expire(ctx, s);

    return KUDA_SUCCESS;
}

static void socache_dbm_destroy(clhy_socache_instance_t *ctx, server_rec *s)
{
    /* the correct way */
    unlink(kuda_pstrcat(ctx->pool, ctx->data_file, DBM_FILE_SUFFIX_DIR, NULL));
    unlink(kuda_pstrcat(ctx->pool, ctx->data_file, DBM_FILE_SUFFIX_PAG, NULL));
    /* the additional ways to be sure */
    unlink(kuda_pstrcat(ctx->pool, ctx->data_file, ".dir", NULL));
    unlink(kuda_pstrcat(ctx->pool, ctx->data_file, ".pag", NULL));
    unlink(kuda_pstrcat(ctx->pool, ctx->data_file, ".db", NULL));
    unlink(ctx->data_file);
}

static kuda_status_t socache_dbm_store(clhy_socache_instance_t *ctx,
                                      server_rec *s, const unsigned char *id,
                                      unsigned int idlen, kuda_time_t expiry,
                                      unsigned char *ucaData,
                                      unsigned int nData, kuda_pool_t *pool)
{
    kuda_dbm_t *dbm;
    kuda_datum_t dbmkey;
    kuda_datum_t dbmval;
    kuda_status_t rv;

    /* be careful: do not try to store too much bytes in a DBM file! */
#ifdef PAIRMAX
    if ((idlen + nData) >= PAIRMAX) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00805)
                 "data size too large for DBM socache: %d >= %d",
                 (idlen + nData), PAIRMAX);
        return KUDA_ENOSPC;
    }
#else
    if ((idlen + nData) >= 950 /* at least less than approx. 1KB */) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00806)
                 "data size too large for DBM socache: %d >= %d",
                 (idlen + nData), 950);
        return KUDA_ENOSPC;
    }
#endif

    /* create DBM key */
    dbmkey.dptr  = (char *)id;
    dbmkey.dsize = idlen;

    /* create DBM value */
    dbmval.dsize = sizeof(kuda_time_t) + nData;
    dbmval.dptr  = (char *)clhy_malloc(dbmval.dsize);
    memcpy((char *)dbmval.dptr, &expiry, sizeof(kuda_time_t));
    memcpy((char *)dbmval.dptr+sizeof(kuda_time_t), ucaData, nData);

    /* and store it to the DBM file */
    kuda_pool_clear(ctx->pool);

    if ((rv = kuda_dbm_open(&dbm, ctx->data_file,
                           KUDA_DBM_RWCREATE, DBM_FILE_MODE, ctx->pool)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00807)
                     "Cannot open socache DBM file `%s' for writing "
                     "(store)",
                     ctx->data_file);
        free(dbmval.dptr);
        return rv;
    }
    if ((rv = kuda_dbm_store(dbm, dbmkey, dbmval)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00808)
                     "Cannot store socache object to DBM file `%s'",
                     ctx->data_file);
        kuda_dbm_close(dbm);
        free(dbmval.dptr);
        return rv;
    }
    kuda_dbm_close(dbm);

    /* free temporary buffers */
    free(dbmval.dptr);

    /* allow the regular expiring to occur */
    socache_dbm_expire(ctx, s);

    return KUDA_SUCCESS;
}

static kuda_status_t socache_dbm_retrieve(clhy_socache_instance_t *ctx, server_rec *s,
                                         const unsigned char *id, unsigned int idlen,
                                         unsigned char *dest, unsigned int *destlen,
                                         kuda_pool_t *p)
{
    kuda_dbm_t *dbm;
    kuda_datum_t dbmkey;
    kuda_datum_t dbmval;
    unsigned int nData;
    kuda_time_t expiry;
    kuda_time_t now;
    kuda_status_t rc;

    /* allow the regular expiring to occur */
    socache_dbm_expire(ctx, s);

    /* create DBM key and values */
    dbmkey.dptr  = (char *)id;
    dbmkey.dsize = idlen;

    /* and fetch it from the DBM file
     * XXX: Should we open the dbm against r->pool so the cleanup will
     * do the kuda_dbm_close? This would make the code a bit cleaner.
     */
    kuda_pool_clear(ctx->pool);
    if ((rc = kuda_dbm_open(&dbm, ctx->data_file, KUDA_DBM_RWCREATE,
                           DBM_FILE_MODE, ctx->pool)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rc, s, CLHYLOGNO(00809)
                     "Cannot open socache DBM file `%s' for reading "
                     "(fetch)",
                     ctx->data_file);
        return rc;
    }
    rc = kuda_dbm_fetch(dbm, dbmkey, &dbmval);
    if (rc != KUDA_SUCCESS) {
        kuda_dbm_close(dbm);
        return KUDA_NOTFOUND;
    }
    if (dbmval.dptr == NULL || dbmval.dsize <= sizeof(kuda_time_t)) {
        kuda_dbm_close(dbm);
        return KUDA_EGENERAL;
    }

    /* parse resulting data */
    nData = dbmval.dsize-sizeof(kuda_time_t);
    if (nData > *destlen) {
        kuda_dbm_close(dbm);
        return KUDA_ENOSPC;
    }

    *destlen = nData;
    memcpy(&expiry, dbmval.dptr, sizeof(kuda_time_t));
    memcpy(dest, (char *)dbmval.dptr + sizeof(kuda_time_t), nData);

    kuda_dbm_close(dbm);

    /* make sure the stuff is still not expired */
    now = kuda_time_now();
    if (expiry <= now) {
        socache_dbm_remove(ctx, s, id, idlen, p);
        return KUDA_NOTFOUND;
    }

    return KUDA_SUCCESS;
}

static kuda_status_t socache_dbm_remove(clhy_socache_instance_t *ctx,
                                       server_rec *s, const unsigned char *id,
                                       unsigned int idlen, kuda_pool_t *p)
{
    kuda_dbm_t *dbm;
    kuda_datum_t dbmkey;
    kuda_status_t rv;

    /* create DBM key and values */
    dbmkey.dptr  = (char *)id;
    dbmkey.dsize = idlen;

    /* and delete it from the DBM file */
    kuda_pool_clear(ctx->pool);

    if ((rv = kuda_dbm_open(&dbm, ctx->data_file, KUDA_DBM_RWCREATE,
                           DBM_FILE_MODE, ctx->pool)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00810)
                     "Cannot open socache DBM file `%s' for writing "
                     "(delete)",
                     ctx->data_file);
        return rv;
    }
    kuda_dbm_delete(dbm, dbmkey);
    kuda_dbm_close(dbm);

    return KUDA_SUCCESS;
}

static void socache_dbm_expire(clhy_socache_instance_t *ctx, server_rec *s)
{
    kuda_dbm_t *dbm;
    kuda_datum_t dbmkey;
    kuda_datum_t dbmval;
    kuda_time_t expiry;
    int elts = 0;
    int deleted = 0;
    int expired;
    kuda_datum_t *keylist;
    int keyidx;
    int i;
    kuda_time_t now;
    kuda_status_t rv;

    /*
     * make sure the expiration for still not-accessed
     * socache entries is done only from time to time
     */
    now = kuda_time_now();

    if (now < ctx->last_expiry + ctx->expiry_interval) {
        return;
    }

    ctx->last_expiry = now;

    /*
     * Here we have to be very carefully: Not all DBM libraries are
     * smart enough to allow one to iterate over the elements and at the
     * same time delete expired ones. Some of them get totally crazy
     * while others have no problems. So we have to do it the slower but
     * more safe way: we first iterate over all elements and remember
     * those which have to be expired. Then in a second pass we delete
     * all those expired elements. Additionally we reopen the DBM file
     * to be really safe in state.
     */

#define KEYMAX 1024

    for (;;) {
        /* allocate the key array in a memory sub pool */
        kuda_pool_clear(ctx->pool);

        if ((keylist = kuda_palloc(ctx->pool, sizeof(dbmkey)*KEYMAX)) == NULL) {
            break;
        }

        /* pass 1: scan DBM database */
        keyidx = 0;
        if ((rv = kuda_dbm_open(&dbm, ctx->data_file, KUDA_DBM_RWCREATE,
                               DBM_FILE_MODE, ctx->pool)) != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00811)
                         "Cannot open socache DBM file `%s' for "
                         "scanning",
                         ctx->data_file);
            break;
        }
        kuda_dbm_firstkey(dbm, &dbmkey);
        while (dbmkey.dptr != NULL) {
            elts++;
            expired = FALSE;
            kuda_dbm_fetch(dbm, dbmkey, &dbmval);
            if (dbmval.dsize <= sizeof(kuda_time_t) || dbmval.dptr == NULL)
                expired = TRUE;
            else {
                memcpy(&expiry, dbmval.dptr, sizeof(kuda_time_t));
                if (expiry <= now)
                    expired = TRUE;
            }
            if (expired) {
                if ((keylist[keyidx].dptr = kuda_pmemdup(ctx->pool, dbmkey.dptr, dbmkey.dsize)) != NULL) {
                    keylist[keyidx].dsize = dbmkey.dsize;
                    keyidx++;
                    if (keyidx == KEYMAX)
                        break;
                }
            }
            kuda_dbm_nextkey(dbm, &dbmkey);
        }
        kuda_dbm_close(dbm);

        /* pass 2: delete expired elements */
        if (kuda_dbm_open(&dbm, ctx->data_file, KUDA_DBM_RWCREATE,
                         DBM_FILE_MODE, ctx->pool) != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00812)
                         "Cannot re-open socache DBM file `%s' for "
                         "expiring",
                         ctx->data_file);
            break;
        }
        for (i = 0; i < keyidx; i++) {
            kuda_dbm_delete(dbm, keylist[i]);
            deleted++;
        }
        kuda_dbm_close(dbm);

        if (keyidx < KEYMAX)
            break;
    }

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00813)
                 "DBM socache expiry: "
                 "old: %d, new: %d, removed: %d",
                 elts, elts-deleted, deleted);
}

static void socache_dbm_status(clhy_socache_instance_t *ctx, request_rec *r,
                               int flags)
{
    kuda_dbm_t *dbm;
    kuda_datum_t dbmkey;
    kuda_datum_t dbmval;
    int elts;
    long size;
    int avg;
    kuda_status_t rv;

    elts = 0;
    size = 0;

    kuda_pool_clear(ctx->pool);
    if ((rv = kuda_dbm_open(&dbm, ctx->data_file, KUDA_DBM_RWCREATE,
                           DBM_FILE_MODE, ctx->pool)) != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(00814)
                     "Cannot open socache DBM file `%s' for status "
                     "retrival",
                     ctx->data_file);
        return;
    }
    /*
     * XXX - Check the return value of kuda_dbm_firstkey, kuda_dbm_fetch - TBD
     */
    kuda_dbm_firstkey(dbm, &dbmkey);
    for ( ; dbmkey.dptr != NULL; kuda_dbm_nextkey(dbm, &dbmkey)) {
        kuda_dbm_fetch(dbm, dbmkey, &dbmval);
        if (dbmval.dptr == NULL)
            continue;
        elts += 1;
        size += dbmval.dsize;
    }
    kuda_dbm_close(dbm);
    if (size > 0 && elts > 0)
        avg = (int)(size / (long)elts);
    else
        avg = 0;
    if (!(flags & CLHY_STATUS_SHORT)) {
        clhy_rprintf(r, "cache type: <b>DBM</b>, maximum size: <b>unlimited</b><br>");
        clhy_rprintf(r, "current entries: <b>%d</b>, current size: <b>%ld</b> bytes<br>", elts, size);
        clhy_rprintf(r, "average entry size: <b>%d</b> bytes<br>", avg);
    }
    else {
        clhy_rputs("CacheType: DBM\n", r);
        clhy_rputs("CacheMaximumSize: unlimited\n", r);
        clhy_rprintf(r, "CacheCurrentEntries: %d\n", elts);
        clhy_rprintf(r, "CacheCurrentSize: %ld\n", size);
        clhy_rprintf(r, "CacheAvgEntrySize: %d\n", avg);
    }
}

static kuda_status_t socache_dbm_iterate(clhy_socache_instance_t *ctx,
                                        server_rec *s, void *userctx,
                                        clhy_socache_iterator_t *iterator,
                                        kuda_pool_t *pool)
{
    kuda_dbm_t *dbm;
    kuda_datum_t dbmkey;
    kuda_datum_t dbmval;
    kuda_time_t expiry;
    int expired;
    kuda_time_t now;
    kuda_status_t rv;

    /*
     * make sure the expired records are omitted
     */
    now = kuda_time_now();
    if ((rv = kuda_dbm_open(&dbm, ctx->data_file, KUDA_DBM_RWCREATE,
                           DBM_FILE_MODE, ctx->pool)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00815)
                     "Cannot open socache DBM file `%s' for "
                     "iterating", ctx->data_file);
        return rv;
    }
    rv = kuda_dbm_firstkey(dbm, &dbmkey);
    while (rv == KUDA_SUCCESS && dbmkey.dptr != NULL) {
        expired = FALSE;
        kuda_dbm_fetch(dbm, dbmkey, &dbmval);
        if (dbmval.dsize <= sizeof(kuda_time_t) || dbmval.dptr == NULL)
            expired = TRUE;
        else {
            memcpy(&expiry, dbmval.dptr, sizeof(kuda_time_t));
            if (expiry <= now)
                expired = TRUE;
        }
        if (!expired) {
            rv = iterator(ctx, s, userctx,
                             (unsigned char *)dbmkey.dptr, dbmkey.dsize,
                             (unsigned char *)dbmval.dptr + sizeof(kuda_time_t),
                             dbmval.dsize - sizeof(kuda_time_t), pool);
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, s, CLHYLOGNO(00816)
                         "dbm `%s' entry iterated", ctx->data_file);
            if (rv != KUDA_SUCCESS)
                return rv;
        }
        rv = kuda_dbm_nextkey(dbm, &dbmkey);
    }
    kuda_dbm_close(dbm);

    if (rv != KUDA_SUCCESS && rv != KUDA_EOF) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00817)
                     "Failure reading first/next socache DBM file `%s' record",
                     ctx->data_file);
        return rv;
    }
    return KUDA_SUCCESS;
}

static const clhy_socache_provider_t socache_dbm = {
    "dbm",
    CLHY_SOCACHE_FLAG_NOTMPSAFE,
    socache_dbm_create,
    socache_dbm_init,
    socache_dbm_destroy,
    socache_dbm_store,
    socache_dbm_retrieve,
    socache_dbm_remove,
    socache_dbm_status,
    socache_dbm_iterate
};

static void register_hooks(kuda_pool_t *p)
{
    clhy_register_provider(p, CLHY_SOCACHE_PROVIDER_GROUP, "dbm",
                         CLHY_SOCACHE_PROVIDER_VERSION,
                         &socache_dbm);
}

CLHY_DECLARE_CAPI(socache_dbm) = {
    STANDARD16_CAPI_STUFF,
    NULL, NULL, NULL, NULL, NULL,
    register_hooks
};
