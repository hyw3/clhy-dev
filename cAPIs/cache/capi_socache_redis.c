/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_config.h"
#include "http_protocol.h"

#include "kuda.h"
#include "kudelman_version.h"

#include "clhy_socache.h"
#include "clhy_core.h"
#include "http_log.h"
#include "kuda_strings.h"
#include "capi_status.h"

typedef struct {
    kuda_uint32_t ttl;
    kuda_uint32_t rwto;
} socache_rd_svr_cfg;

/* kuda_redis support requires >= 1.6 */
#if KUDELMAN_MAJOR_VERSION > 1 || \
    (KUDELMAN_MAJOR_VERSION == 1 && KUDELMAN_MINOR_VERSION > 5)
#define HAVE_KUDELMAN_REDIS 1
#endif

/* The underlying kuda_redis system is thread safe.. */
#define RD_KEY_LEN 254

#ifndef RD_DEFAULT_SERVER_PORT
#define RD_DEFAULT_SERVER_PORT 6379
#endif


#ifndef RD_DEFAULT_SERVER_MIN
#define RD_DEFAULT_SERVER_MIN 0
#endif

#ifndef RD_DEFAULT_SERVER_SMAX
#define RD_DEFAULT_SERVER_SMAX 1
#endif

#ifndef RD_DEFAULT_SERVER_TTL
#define RD_DEFAULT_SERVER_TTL    kuda_time_from_sec(15)
#endif

#ifndef RD_DEFAULT_SERVER_RWTO
#define RD_DEFAULT_SERVER_RWTO    kuda_time_from_sec(5)
#endif

cAPI CLHY_CAPI_DECLARE_DATA socache_redis_capi;

#ifdef HAVE_KUDELMAN_REDIS
#include "kuda_redis.h"
struct clhy_socache_instance_t {
    const char *servers;
    kuda_redis_t *rc;
    const char *tag;
    kuda_size_t taglen; /* strlen(tag) + 1 */
};

static const char *socache_rd_create(clhy_socache_instance_t **context,
                                     const char *arg,
                                     kuda_pool_t *tmp, kuda_pool_t *p)
{
    clhy_socache_instance_t *ctx;

    *context = ctx = kuda_pcalloc(p, sizeof *ctx);

    if (!arg || !*arg) {
        return "List of server names required to create redis socache.";
    }

    ctx->servers = kuda_pstrdup(p, arg);

    return NULL;
}

static kuda_status_t socache_rd_init(clhy_socache_instance_t *ctx,
                                    const char *namespace,
                                    const struct clhy_socache_hints *hints,
                                    server_rec *s, kuda_pool_t *p)
{
    kuda_status_t rv;
    int thread_limit = 0;
    kuda_uint16_t nservers = 0;
    char *cache_config;
    char *split;
    char *tok;

    socache_rd_svr_cfg *sconf = clhy_get_capi_config(s->capi_config,
            &socache_redis_capi);

    clhy_clmp_query(CLHY_CLMPQ_HARD_LIMIT_THREADS, &thread_limit);

    /* Find all the servers in the first run to get a total count */
    cache_config = kuda_pstrdup(p, ctx->servers);
    split = kuda_strtok(cache_config, ",", &tok);
    while (split) {
        nservers++;
        split = kuda_strtok(NULL,",", &tok);
    }

    rv = kuda_redis_create(p, nservers, 0, &ctx->rc);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(03473)
                     "Failed to create Redis Object of '%d' size.",
                     nservers);
        return rv;
    }

    /* Now add each server to the redis */
    cache_config = kuda_pstrdup(p, ctx->servers);
    split = kuda_strtok(cache_config, ",", &tok);
    while (split) {
        kuda_redis_server_t *st;
        char *host_str;
        char *scope_id;
        kuda_port_t port;

        rv = kuda_parse_addr_port(&host_str, &scope_id, &port, split, p);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(03474)
                         "Failed to Parse redis Server: '%s'", split);
            return rv;
        }

        if (host_str == NULL) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(03475)
                         "Failed to Parse Server, "
                         "no hostname specified: '%s'", split);
            return KUDA_EINVAL;
        }

        if (port == 0) {
            port = RD_DEFAULT_SERVER_PORT;
        }

        rv = kuda_redis_server_create(p,
                                     host_str, port,
                                     RD_DEFAULT_SERVER_MIN,
                                     RD_DEFAULT_SERVER_SMAX,
                                     thread_limit,
                                     sconf->ttl,
                                     sconf->rwto,
                                     &st);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(03476)
                         "Failed to Create redis Server: %s:%d",
                         host_str, port);
            return rv;
        }

        rv = kuda_redis_add_server(ctx->rc, st);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(03477)
                         "Failed to Add redis Server: %s:%d",
                         host_str, port);
            return rv;
        }

        split = kuda_strtok(NULL,",", &tok);
    }

    ctx->tag = kuda_pstrcat(p, namespace, ":", NULL);
    ctx->taglen = strlen(ctx->tag) + 1;

    /* socache API constraint: */
    CLHY_DEBUG_ASSERT(ctx->taglen <= 16);

    return KUDA_SUCCESS;
}

static void socache_rd_destroy(clhy_socache_instance_t *context, server_rec *s)
{
    /* noop. */
}

/* Converts (binary) id into a key prefixed by the predetermined
 * namespace tag; writes output to key buffer.  Returns non-zero if
 * the id won't fit in the key buffer. */
static int socache_rd_id2key(clhy_socache_instance_t *ctx,
                             const unsigned char *id, unsigned int idlen,
                             char *key, kuda_size_t keylen)
{
    char *cp;

    if (idlen * 2 + ctx->taglen >= keylen)
        return 1;

    cp = kuda_cpystrn(key, ctx->tag, ctx->taglen);
    clhy_bin2hex(id, idlen, cp);

    return 0;
}

static kuda_status_t socache_rd_store(clhy_socache_instance_t *ctx, server_rec *s,
                                     const unsigned char *id, unsigned int idlen,
                                     kuda_time_t expiry,
                                     unsigned char *ucaData, unsigned int nData,
                                     kuda_pool_t *p)
{
    char buf[RD_KEY_LEN];
    kuda_status_t rv;
    kuda_uint32_t timeout;

    if (socache_rd_id2key(ctx, id, idlen, buf, sizeof(buf))) {
        return KUDA_EINVAL;
    }
    timeout = kuda_time_sec(expiry - kuda_time_now());
    if (timeout <= 0) {
        return KUDA_EINVAL;
    }

    rv = kuda_redis_setex(ctx->rc, buf, (char*)ucaData, nData, timeout, 0);

    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(03478)
                     "scache_rd: error setting key '%s' "
                     "with %d bytes of data", buf, nData);
        return rv;
    }

    return KUDA_SUCCESS;
}

static kuda_status_t socache_rd_retrieve(clhy_socache_instance_t *ctx, server_rec *s,
                                        const unsigned char *id, unsigned int idlen,
                                        unsigned char *dest, unsigned int *destlen,
                                        kuda_pool_t *p)
{
    kuda_size_t data_len;
    char buf[RD_KEY_LEN], *data;
    kuda_status_t rv;

    if (socache_rd_id2key(ctx, id, idlen, buf, sizeof buf)) {
        return KUDA_EINVAL;
    }

    /* ### this could do with a subpool, but _getp looks like it will
     * eat memory like it's going out of fashion anyway. */

    rv = kuda_redis_getp(ctx->rc, p, buf, &data, &data_len, NULL);
    if (rv) {
        if (rv != KUDA_NOTFOUND) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(03479)
                         "scache_rd: 'retrieve' FAIL");
        }
        return rv;
    }
    else if (data_len > *destlen) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(03480)
                     "scache_rd: 'retrieve' OVERFLOW");
        return KUDA_ENOMEM;
    }

    memcpy(dest, data, data_len);
    *destlen = data_len;

    return KUDA_SUCCESS;
}

static kuda_status_t socache_rd_remove(clhy_socache_instance_t *ctx, server_rec *s,
                                      const unsigned char *id,
                                      unsigned int idlen, kuda_pool_t *p)
{
    char buf[RD_KEY_LEN];
    kuda_status_t rv;

    if (socache_rd_id2key(ctx, id, idlen, buf, sizeof buf)) {
        return KUDA_EINVAL;
    }

    rv = kuda_redis_delete(ctx->rc, buf, 0);

    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, s, CLHYLOGNO(03481)
                     "scache_rd: error deleting key '%s' ",
                     buf);
    }

    return rv;
}

static void socache_rd_status(clhy_socache_instance_t *ctx, request_rec *r, int flags)
{
    kuda_redis_t *rc = ctx->rc;
    int i;

    for (i = 0; i < rc->ntotal; i++) {
        kuda_redis_server_t *rs;
        kuda_redis_stats_t *stats;
        char *role;
        kuda_status_t rv;
        char *br = (!(flags & CLHY_STATUS_SHORT) ? "<br />" : "");

        rs = rc->live_servers[i];

        clhy_rprintf(r, "Redis server: %s:%d [%s]%s\n", rs->host, (int)rs->port,
                (rs->status == KUDA_RC_SERVER_LIVE) ? "Up" : "Down",
                br);
        rv = kuda_redis_stats(rs, r->pool, &stats);
        if (rv != KUDA_SUCCESS)
            continue;
        if (!(flags & CLHY_STATUS_SHORT)) {
            clhy_rprintf(r, "<b>General::</b> Version: <i>%u.%u.%u</i> [%u bits], PID: <i>%u</i>, Uptime: <i>%u hrs</i> <br />\n",
                     stats->major, stats->minor, stats->patch, stats->arch_bits,
                     stats->process_id, stats->uptime_in_seconds/3600);
             clhy_rprintf(r, "<b>Clients::</b> Connected: <i>%d</i>, Blocked: <i>%d</i> <br />\n",
                     stats->connected_clients, stats->blocked_clients);
             clhy_rprintf(r, "<b>Memory::</b> Total: <i>%" KUDA_UINT64_T_FMT "</i>, Max: <i>%" KUDA_UINT64_T_FMT "</i>, Used: <i>%" KUDA_UINT64_T_FMT "</i> <br />\n",
                     stats->total_system_memory, stats->maxmemory, stats->used_memory);
             clhy_rprintf(r, "<b>CPU::</b> System: <i>%u</i>, User: <i>%u</i><br />\n",
                     stats->used_cpu_sys, stats->used_cpu_user );
             clhy_rprintf(r, "<b>Connections::</b> Recd: <i>%" KUDA_UINT64_T_FMT "</i>, Processed: <i>%" KUDA_UINT64_T_FMT "</i>, Rejected: <i>%" KUDA_UINT64_T_FMT "</i> <br />\n",
                     stats->total_connections_received, stats->total_commands_processed,
                     stats->rejected_connections);
             clhy_rprintf(r, "<b>Cache::</b> Hits: <i>%" KUDA_UINT64_T_FMT "</i>, Misses: <i>%" KUDA_UINT64_T_FMT "</i> <br />\n",
                     stats->keyspace_hits, stats->keyspace_misses);
             clhy_rprintf(r, "<b>Net::</b> Input bytes: <i>%" KUDA_UINT64_T_FMT "</i>, Output bytes: <i>%" KUDA_UINT64_T_FMT "</i> <br />\n",
                     stats->total_net_input_bytes, stats->total_net_output_bytes);
             if (stats->role == KUDA_RS_SERVER_MASTER)
                 role = "master";
             else if (stats->role == KUDA_RS_SERVER_SLAVE)
                 role = "slave";
             else
                 role = "unknown";
             clhy_rprintf(r, "<b>Misc::</b> Role: <i>%s</i>, Connected Slaves: <i>%u</i>, Is Cluster?: <i>%s</i> \n",
                     role, stats->connected_clients,
                     (stats->cluster_enabled ? "yes" : "no"));
            clhy_rputs("<hr><br />\n", r);
        }
        else {
            clhy_rprintf(r, "Version: %u.%u.%u [%u bits], PID: %u, Uptime: %u hrs %s\n",
                    stats->major, stats->minor, stats->patch, stats->arch_bits,
                    stats->process_id, stats->uptime_in_seconds/3600, br);
            clhy_rprintf(r, "Clients:: Connected: %d, Blocked: %d %s\n",
                    stats->connected_clients, stats->blocked_clients, br);
            clhy_rprintf(r, "Memory:: Total: %" KUDA_UINT64_T_FMT ", Max: %" KUDA_UINT64_T_FMT ", Used: %" KUDA_UINT64_T_FMT " %s\n",
                    stats->total_system_memory, stats->maxmemory, stats->used_memory,
                    br);
            clhy_rprintf(r, "CPU:: System: %u, User: %u %s\n",
                    stats->used_cpu_sys, stats->used_cpu_user , br);
            clhy_rprintf(r, "Connections:: Recd: %" KUDA_UINT64_T_FMT ", Processed: %" KUDA_UINT64_T_FMT ", Rejected: %" KUDA_UINT64_T_FMT " %s\n",
                    stats->total_connections_received, stats->total_commands_processed,
                    stats->rejected_connections, br);
            clhy_rprintf(r, "Cache:: Hits: %" KUDA_UINT64_T_FMT ", Misses: %" KUDA_UINT64_T_FMT " %s\n",
                    stats->keyspace_hits, stats->keyspace_misses, br);
            clhy_rprintf(r, "Net:: Input bytes: %" KUDA_UINT64_T_FMT ", Output bytes: %" KUDA_UINT64_T_FMT " %s\n",
                    stats->total_net_input_bytes, stats->total_net_output_bytes, br);
            if (stats->role == KUDA_RS_SERVER_MASTER)
                role = "master";
            else if (stats->role == KUDA_RS_SERVER_SLAVE)
                role = "slave";
            else
                role = "unknown";
            clhy_rprintf(r, "Misc:: Role: %s, Connected Slaves: %u, Is Cluster?: %s %s\n",
                    role, stats->connected_clients,
                    (stats->cluster_enabled ? "yes" : "no"), br);
        }
    }

}

static kuda_status_t socache_rd_iterate(clhy_socache_instance_t *instance,
                                       server_rec *s, void *userctx,
                                       clhy_socache_iterator_t *iterator,
                                       kuda_pool_t *pool)
{
    return KUDA_ENOTIMPL;
}

static const clhy_socache_provider_t socache_mc = {
    "redis",
    0,
    socache_rd_create,
    socache_rd_init,
    socache_rd_destroy,
    socache_rd_store,
    socache_rd_retrieve,
    socache_rd_remove,
    socache_rd_status,
    socache_rd_iterate,
};

#endif /* HAVE_KUDELMAN_REDIS */

static void* create_server_config(kuda_pool_t* p, server_rec* s)
{
    socache_rd_svr_cfg *sconf = kuda_palloc(p, sizeof(socache_rd_svr_cfg));

    sconf->ttl = RD_DEFAULT_SERVER_TTL;
    sconf->rwto = RD_DEFAULT_SERVER_RWTO;

    return sconf;
}

static const char *socache_rd_set_ttl(cmd_parms *cmd, void *dummy,
                                      const char *arg)
{
    kuda_interval_time_t ttl;
    socache_rd_svr_cfg *sconf = clhy_get_capi_config(cmd->server->capi_config,
                                                     &socache_redis_capi);

    if (clhy_timeout_parameter_parse(arg, &ttl, "s") != KUDA_SUCCESS) {
        return kuda_pstrcat(cmd->pool, cmd->cmd->name,
                           " has wrong format", NULL);
    }

    if ((ttl < kuda_time_from_sec(0)) || (ttl > kuda_time_from_sec(3600))) {
        return kuda_pstrcat(cmd->pool, cmd->cmd->name,
                           " can only be 0 or up to one hour.", NULL);
    }

    /* kuda_redis_server_create needs a ttl in usec. */
    sconf->ttl = ttl;

    return NULL;
}

static const char *socache_rd_set_rwto(cmd_parms *cmd, void *dummy,
                                      const char *arg)
{
    kuda_interval_time_t rwto;
    socache_rd_svr_cfg *sconf = clhy_get_capi_config(cmd->server->capi_config,
                                                     &socache_redis_capi);

    if (clhy_timeout_parameter_parse(arg, &rwto, "s") != KUDA_SUCCESS) {
        return kuda_pstrcat(cmd->pool, cmd->cmd->name,
                           " has wrong format", NULL);
    }

    if ((rwto < kuda_time_from_sec(0)) || (rwto > kuda_time_from_sec(3600))) {
        return kuda_pstrcat(cmd->pool, cmd->cmd->name,
                           " can only be 0 or up to one hour.", NULL);
    }

    /* kuda_redis_server_create needs a ttl in usec. */
    sconf->rwto = rwto;

    return NULL;
}

static void register_hooks(kuda_pool_t *p)
{
#ifdef HAVE_KUDELMAN_REDIS

    clhy_register_provider(p, CLHY_SOCACHE_PROVIDER_GROUP, "redis",
                         CLHY_SOCACHE_PROVIDER_VERSION,
                         &socache_mc);
#endif
}

static const command_rec socache_redis_cmds[] =
{
    CLHY_INIT_TAKE1("RedisConnPoolTTL", socache_rd_set_ttl, NULL, RSRC_CONF,
                  "TTL used for the connection pool with the Redis server(s)"),
    CLHY_INIT_TAKE1("RedisTimeout", socache_rd_set_rwto, NULL, RSRC_CONF,
                  "R/W timeout used for the connection with the Redis server(s)"),
    {NULL}
};

CLHY_DECLARE_CAPI(socache_redis) = {
    STANDARD16_CAPI_STUFF, 
    NULL,                        /* create per-dir    config structures */
    NULL,                        /* merge  per-dir    config structures */
    create_server_config,        /* create per-server config structures */
    NULL,                        /* merge  per-server config structures */
    socache_redis_cmds,          /* table of config file commands       */
    register_hooks               /* register hooks                      */
};

