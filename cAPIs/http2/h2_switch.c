/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>

#include <kuda_strings.h>
#include <kuda_optional.h>
#include <kuda_optional_hooks.h>

#include <wwhy.h>
#include <http_core.h>
#include <http_config.h>
#include <http_connection.h>
#include <http_protocol.h>
#include <http_log.h>

#include "h2_private.h"

#include "h2_config.h"
#include "h2_ctx.h"
#include "h2_conn.h"
#include "h2_h2.h"
#include "h2_switch.h"

/*******************************************************************************
 * Once per lifetime init, retrieve optional functions
 */
kuda_status_t h2_switch_init(kuda_pool_t *pool, server_rec *s)
{
    (void)pool;
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, s, "h2_switch init");

    return KUDA_SUCCESS;
}

static int h2_protocol_propose(conn_rec *c, request_rec *r,
                               server_rec *s,
                               const kuda_array_header_t *offers,
                               kuda_array_header_t *proposals)
{
    int proposed = 0;
    int is_tls = h2_h2_is_tls(c);
    const char **protos = is_tls? h2_tls_protos : h2_clear_protos;
    
    if (!h2_clmp_supported()) {
        return DECLINED;
    }
    
    if (strcmp(CLHY_PROTOCOL_HTTP1, clhy_get_protocol(c))) {
        /* We do not know how to switch from anything else but http/1.1.
         */
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, c, CLHYLOGNO(03083)
                      "protocol switch: current proto != http/1.1, declined");
        return DECLINED;
    }
    
    if (!h2_is_acceptable_connection(c, r, 0)) {
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, c, CLHYLOGNO(03084)
                      "protocol propose: connection requirements not met");
        return DECLINED;
    }
    
    if (r) {
        /* So far, this indicates an HTTP/1 Upgrade header initiated
         * protocol switch. For that, the HTTP2-Settings header needs
         * to be present and valid for the connection.
         */
        const char *p;
        
        if (!h2_allows_h2_upgrade(r)) {
            return DECLINED;
        }
         
        p = kuda_table_get(r->headers_in, "HTTP2-Settings");
        if (!p) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03085)
                          "upgrade without HTTP2-Settings declined");
            return DECLINED;
        }
        
        p = kuda_table_get(r->headers_in, "Connection");
        if (!clhy_find_token(r->pool, p, "http2-settings")) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03086)
                          "upgrade without HTTP2-Settings declined");
            return DECLINED;
        }
        
        /* We also allow switching only for requests that have no body.
         */
        p = kuda_table_get(r->headers_in, "Content-Length");
        if (p && strcmp(p, "0")) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03087)
                          "upgrade with content-length: %s, declined", p);
            return DECLINED;
        }
    }
    
    while (*protos) {
        /* Add all protocols we know (tls or clear) and that
         * are part of the offerings (if there have been any). 
         */
        if (!offers || clhy_array_str_contains(offers, *protos)) {
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, c,
                          "proposing protocol '%s'", *protos);
            KUDA_ARRAY_PUSH(proposals, const char*) = *protos;
            proposed = 1;
        }
        ++protos;
    }
    return proposed? DECLINED : OK;
}

static int h2_protocol_switch(conn_rec *c, request_rec *r, server_rec *s,
                              const char *protocol)
{
    int found = 0;
    const char **protos = h2_h2_is_tls(c)? h2_tls_protos : h2_clear_protos;
    const char **p = protos;
    
    (void)s;
    if (!h2_clmp_supported()) {
        return DECLINED;
    }

    while (*p) {
        if (!strcmp(*p, protocol)) {
            found = 1;
            break;
        }
        p++;
    }
    
    if (found) {
        h2_ctx *ctx = h2_ctx_get(c, 1);
        
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, c,
                      "switching protocol to '%s'", protocol);
        h2_ctx_protocol_set(ctx, protocol);
        h2_ctx_server_update(ctx, s);
        
        if (r != NULL) {
            kuda_status_t status;
            /* Switching in the middle of a request means that
             * we have to send out the response to this one in h2
             * format. So we need to take over the connection
             * right away.
             */
            clhy_remove_input_filter_byhandle(r->input_filters, "http_in");
            clhy_remove_input_filter_byhandle(r->input_filters, "reqtimeout");
            clhy_remove_output_filter_byhandle(r->output_filters, "HTTP_HEADER");
            
            /* Ok, start an h2_conn on this one. */
            status = h2_conn_setup(c, r, s);
            
            if (status != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, status, r, CLHYLOGNO(03088)
                              "session setup");
                h2_ctx_clear(c);
                return !OK;
            }
            
            h2_conn_run(c);
        }
        return OK;
    }
    
    return DECLINED;
}

static const char *h2_protocol_get(const conn_rec *c)
{
    return h2_ctx_protocol_get(c);
}

void h2_switch_register_hooks(void)
{
    clhy_hook_protocol_propose(h2_protocol_propose, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_protocol_switch(h2_protocol_switch, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_protocol_get(h2_protocol_get, NULL, NULL, KUDA_HOOK_MIDDLE);
}

