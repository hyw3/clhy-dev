/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_http2_h2_bucket_eoc_h
#define capi_http2_h2_bucket_eoc_h

struct h2_session;

/** End Of HTTP/2 SESSION (H2EOC) bucket */
extern const kuda_bucket_type_t h2_bucket_type_eoc;

#define H2_BUCKET_IS_H2EOC(e)     (e->type == &h2_bucket_type_eoc)

kuda_bucket * h2_bucket_eoc_make(kuda_bucket *b, 
                                struct h2_session *session);

kuda_bucket * h2_bucket_eoc_create(kuda_bucket_alloc_t *list,
                                  struct h2_session *session);

#endif /* capi_http2_h2_bucket_eoc_h */
