/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __capi_h2__h2_from_h1__
#define __capi_h2__h2_from_h1__

/**
 * h2_from_h1 parses a HTTP/1.1 response into
 * - response status
 * - a list of header values
 * - a series of bytes that represent the response body alone, without
 *   any meta data, such as inserted by chunked transfer encoding.
 *
 * All data is allocated from the stream memory pool. 
 *
 * Again, see comments in h2_request: ideally we would take the headers
 * and status from the wwhy structures instead of parsing them here, but
 * we need to have all handlers and filters involved in request/response
 * processing, so this seems to be the way for now.
 */
struct h2_headers;
struct h2_task;

kuda_status_t h2_from_h1_parse_response(struct h2_task *task, clhy_filter_t *f, 
                                       kuda_bucket_brigade *bb);

kuda_status_t h2_clfilter_headers_out(clhy_filter_t *f, kuda_bucket_brigade *bb);

kuda_status_t h2_clfilter_request_in(clhy_filter_t* f,
                                  kuda_bucket_brigade* brigade,
                                  clhy_input_mode_t mode,
                                  kuda_read_type_e block,
                                  kuda_off_t readbytes);

kuda_status_t h2_clfilter_trailers_out(clhy_filter_t *f, kuda_bucket_brigade *bb);

#endif /* defined(__capi_h2__h2_from_h1__) */
