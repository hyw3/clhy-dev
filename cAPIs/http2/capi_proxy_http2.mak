# Microsoft Developer Studio Generated NMAKE File, Based on capi_proxy_http2.dsp
!IF "$(CFG)" == ""
CFG=capi_proxy_http2 - Win32 Release
!MESSAGE No configuration specified. Defaulting to capi_proxy_http2 - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "capi_proxy_http2 - Win32 Release" && "$(CFG)" != "capi_proxy_http2 - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "capi_proxy_http2.mak" CFG="capi_proxy_http2 - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "capi_proxy_http2 - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "capi_proxy_http2 - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "capi_proxy_http2 - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_proxy_http2.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "capi_proxy - Win32 Release" "capi_http2 - Win32 Release" "libwwhy - Win32 Release" "libkudadelman - Win32 Release" "libkuda - Win32 Release" "$(OUTDIR)\capi_proxy_http2.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudadelman - Win32 ReleaseCLEAN" "libwwhy - Win32 ReleaseCLEAN" "capi_http2 - Win32 ReleaseCLEAN" "capi_proxy - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\h2_proxy_session.obj"
	-@erase "$(INTDIR)\h2_proxy_util.obj"
	-@erase "$(INTDIR)\capi_proxy_http2.obj"
	-@erase "$(INTDIR)\capi_proxy_http2.res"
	-@erase "$(INTDIR)\capi_proxy_http2_src.idb"
	-@erase "$(INTDIR)\capi_proxy_http2_src.pdb"
	-@erase "$(OUTDIR)\capi_proxy_http2.exp"
	-@erase "$(OUTDIR)\capi_proxy_http2.lib"
	-@erase "$(OUTDIR)\capi_proxy_http2.pdb"
	-@erase "$(OUTDIR)\capi_proxy_http2.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../clssl" /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/nghttp2/lib/includes" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D ssize_t=long /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_proxy_http2_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_proxy_http2.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "NDEBUG" /d BIN_NAME="capi_proxy_http2.so" /d LONG_NAME="http2_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_proxy_http2.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib nghttp2.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_proxy_http2.pdb" /debug /out:"$(OUTDIR)\capi_proxy_http2.so" /implib:"$(OUTDIR)\capi_proxy_http2.lib" /libpath:"..\..\kudelrunsrc\nghttp2\lib\MSVC_obj" /base:@..\..\platforms\win32\BaseAddr.ref,capi_proxy_http2.so /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\h2_proxy_session.obj" \
	"$(INTDIR)\h2_proxy_util.obj" \
	"$(INTDIR)\capi_proxy_http2.obj" \
	"$(INTDIR)\capi_proxy_http2.res" \
	"..\..\kudelrunsrc\kuda\Release\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Release\libkudadelman-1.lib" \
	"..\..\Release\libwwhy.lib" \
	"$(OUTDIR)\capi_http2.lib" \
	"..\proxy\Release\capi_proxy.lib"

"$(OUTDIR)\capi_proxy_http2.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\capi_proxy_http2.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_proxy_http2.so"
   if exist .\Release\capi_proxy_http2.so.manifest mt.exe -manifest .\Release\capi_proxy_http2.so.manifest -outputresource:.\Release\capi_proxy_http2.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "capi_proxy_http2 - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_proxy_http2.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "capi_proxy - Win32 Debug" "capi_http2 - Win32 Debug" "libwwhy - Win32 Debug" "libkudadelman - Win32 Debug" "libkuda - Win32 Debug" "$(OUTDIR)\capi_proxy_http2.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudadelman - Win32 DebugCLEAN" "libwwhy - Win32 DebugCLEAN" "capi_http2 - Win32 DebugCLEAN" "capi_proxy - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\h2_proxy_session.obj"
	-@erase "$(INTDIR)\h2_proxy_util.obj"
	-@erase "$(INTDIR)\capi_proxy_http2.obj"
	-@erase "$(INTDIR)\capi_proxy_http2.res"
	-@erase "$(INTDIR)\capi_proxy_http2_src.idb"
	-@erase "$(INTDIR)\capi_proxy_http2_src.pdb"
	-@erase "$(OUTDIR)\capi_proxy_http2.exp"
	-@erase "$(OUTDIR)\capi_proxy_http2.lib"
	-@erase "$(OUTDIR)\capi_proxy_http2.pdb"
	-@erase "$(OUTDIR)\capi_proxy_http2.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../clssl" /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/nghttp2/lib/includes" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D ssize_t=long /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_proxy_http2_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_proxy_http2.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "_DEBUG" /d BIN_NAME="capi_proxy_http2.so" /d LONG_NAME="http2_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_proxy_http2.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib nghttp2d.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_proxy_http2.pdb" /debug /out:"$(OUTDIR)\capi_proxy_http2.so" /implib:"$(OUTDIR)\capi_proxy_http2.lib" /libpath:"..\..\kudelrunsrc\nghttp2\lib\MSVC_obj" /base:@..\..\platforms\win32\BaseAddr.ref,capi_proxy_http2.so 
LINK32_OBJS= \
	"$(INTDIR)\h2_proxy_session.obj" \
	"$(INTDIR)\h2_proxy_util.obj" \
	"$(INTDIR)\capi_proxy_http2.obj" \
	"$(INTDIR)\capi_proxy_http2.res" \
	"..\..\kudelrunsrc\kuda\Debug\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Debug\libkudadelman-1.lib" \
	"..\..\Debug\libwwhy.lib" \
	"$(OUTDIR)\capi_http2.lib" \
	"..\proxy\Debug\capi_proxy.lib"

"$(OUTDIR)\capi_proxy_http2.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\capi_proxy_http2.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_proxy_http2.so"
   if exist .\Debug\capi_proxy_http2.so.manifest mt.exe -manifest .\Debug\capi_proxy_http2.so.manifest -outputresource:.\Debug\capi_proxy_http2.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("capi_proxy_http2.dep")
!INCLUDE "capi_proxy_http2.dep"
!ELSE 
!MESSAGE Warning: cannot find "capi_proxy_http2.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "capi_proxy_http2 - Win32 Release" || "$(CFG)" == "capi_proxy_http2 - Win32 Debug"

!IF  "$(CFG)" == "capi_proxy_http2 - Win32 Release"

"libkuda - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\..\cAPIs\http2"

"libkuda - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\http2"

!ELSEIF  "$(CFG)" == "capi_proxy_http2 - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\..\cAPIs\http2"

"libkuda - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\http2"

!ENDIF 

!IF  "$(CFG)" == "capi_proxy_http2 - Win32 Release"

"libkudadelman - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" 
   cd "..\..\cAPIs\http2"

"libkudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\http2"

!ELSEIF  "$(CFG)" == "capi_proxy_http2 - Win32 Debug"

"libkudadelman - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" 
   cd "..\..\cAPIs\http2"

"libkudadelman - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\http2"

!ENDIF 

!IF  "$(CFG)" == "capi_proxy_http2 - Win32 Release"

"libwwhy - Win32 Release" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" 
   cd ".\cAPIs\http2"

"libwwhy - Win32 ReleaseCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" RECURSE=1 CLEAN 
   cd ".\cAPIs\http2"

!ELSEIF  "$(CFG)" == "capi_proxy_http2 - Win32 Debug"

"libwwhy - Win32 Debug" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" 
   cd ".\cAPIs\http2"

"libwwhy - Win32 DebugCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" RECURSE=1 CLEAN 
   cd ".\cAPIs\http2"

!ENDIF 

!IF  "$(CFG)" == "capi_proxy_http2 - Win32 Release"

"capi_http2 - Win32 Release" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_http2.mak" CFG="capi_http2 - Win32 Release" 
   cd "."

"capi_http2 - Win32 ReleaseCLEAN" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_http2.mak" CFG="capi_http2 - Win32 Release" RECURSE=1 CLEAN 
   cd "."

!ELSEIF  "$(CFG)" == "capi_proxy_http2 - Win32 Debug"

"capi_http2 - Win32 Debug" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_http2.mak" CFG="capi_http2 - Win32 Debug" 
   cd "."

"capi_http2 - Win32 DebugCLEAN" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_http2.mak" CFG="capi_http2 - Win32 Debug" RECURSE=1 CLEAN 
   cd "."

!ENDIF 

!IF  "$(CFG)" == "capi_proxy_http2 - Win32 Release"

"capi_proxy - Win32 Release" : 
   cd ".\..\proxy"
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_proxy.mak" CFG="capi_proxy - Win32 Release" 
   cd "..\http2"

"capi_proxy - Win32 ReleaseCLEAN" : 
   cd ".\..\proxy"
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_proxy.mak" CFG="capi_proxy - Win32 Release" RECURSE=1 CLEAN 
   cd "..\http2"

!ELSEIF  "$(CFG)" == "capi_proxy_http2 - Win32 Debug"

"capi_proxy - Win32 Debug" : 
   cd ".\..\proxy"
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_proxy.mak" CFG="capi_proxy - Win32 Debug" 
   cd "..\http2"

"capi_proxy - Win32 DebugCLEAN" : 
   cd ".\..\proxy"
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_proxy.mak" CFG="capi_proxy - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\http2"

!ENDIF 

SOURCE=./h2_proxy_session.c

"$(INTDIR)\h2_proxy_session.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./h2_proxy_util.c

"$(INTDIR)\h2_proxy_util.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=..\..\build\win32\wwhy.rc

!IF  "$(CFG)" == "capi_proxy_http2 - Win32 Release"


"$(INTDIR)\capi_proxy_http2.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_proxy_http2.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "\build16\wwhy-1.6.0-dev\build\win32" /d "NDEBUG" /d BIN_NAME="capi_proxy_http2.so" /d LONG_NAME="http2_capi for cLHy" $(SOURCE)


!ELSEIF  "$(CFG)" == "capi_proxy_http2 - Win32 Debug"


"$(INTDIR)\capi_proxy_http2.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_proxy_http2.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "\build16\wwhy-1.6.0-dev\build\win32" /d "_DEBUG" /d BIN_NAME="capi_proxy_http2.so" /d LONG_NAME="http2_capi for cLHy" $(SOURCE)


!ENDIF 

SOURCE=./capi_proxy_http2.c

"$(INTDIR)\capi_proxy_http2.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 

