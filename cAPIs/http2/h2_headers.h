/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __capi_h2__h2_headers__
#define __capi_h2__h2_headers__

#include "h2.h"

struct h2_bucket_beam;

extern const kuda_bucket_type_t h2_bucket_type_headers;

#define H2_BUCKET_IS_HEADERS(e)     (e->type == &h2_bucket_type_headers)

kuda_bucket * h2_bucket_headers_make(kuda_bucket *b, h2_headers *r);

kuda_bucket * h2_bucket_headers_create(kuda_bucket_alloc_t *list,
                                       h2_headers *r);

h2_headers *h2_bucket_headers_get(kuda_bucket *b);

kuda_bucket *h2_bucket_headers_beam(struct h2_bucket_beam *beam,
                                    kuda_bucket_brigade *dest,
                                    const kuda_bucket *src);

/**
 * Create the headers from the given status and headers
 * @param status the headers status
 * @param header the headers of the headers
 * @param notes  the notes carried by the headers
 * @param raw_bytes the raw network bytes (if known) used to transmit these
 * @param pool the memory pool to use
 */
h2_headers *h2_headers_create(int status, kuda_table_t *header,
                              kuda_table_t *notes, kuda_off_t raw_bytes,
                              kuda_pool_t *pool);

/**
 * Create the headers from the given request_rec.
 * @param r the request record which was processed
 * @param status the headers status
 * @param header the headers of the headers
 * @param pool the memory pool to use
 */
h2_headers *h2_headers_rcreate(request_rec *r, int status,
                                 kuda_table_t *header, kuda_pool_t *pool);

/**
 * Copy the headers into another pool. This will not copy any
 * header strings.
 */
h2_headers *h2_headers_copy(kuda_pool_t *pool, h2_headers *h);

/**
 * Clone the headers into another pool. This will also clone any
 * header strings.
 */
h2_headers *h2_headers_clone(kuda_pool_t *pool, h2_headers *h);

/**
 * Create the headers for the given error.
 * @param stream_id id of the stream to create the headers for
 * @param type the error code
 * @param req the original h2_request
 * @param pool the memory pool to use
 */
h2_headers *h2_headers_die(kuda_status_t type,
                             const struct h2_request *req, kuda_pool_t *pool);

int h2_headers_are_response(h2_headers *headers);

#endif /* defined(__capi_h2__h2_headers__) */

