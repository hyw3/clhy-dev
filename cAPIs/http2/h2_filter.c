/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>

#include <kuda_strings.h>
#include <wwhy.h>
#include <http_core.h>
#include <http_protocol.h>
#include <http_log.h>
#include <http_connection.h>
#include <scoreboard.h>

#include "h2_private.h"
#include "h2.h"
#include "h2_config.h"
#include "h2_conn_io.h"
#include "h2_ctx.h"
#include "h2_mplx.h"
#include "h2_push.h"
#include "h2_task.h"
#include "h2_stream.h"
#include "h2_request.h"
#include "h2_headers.h"
#include "h2_stream.h"
#include "h2_session.h"
#include "h2_util.h"
#include "h2_version.h"

#include "h2_filter.h"

#define UNSET       -1
#define H2MIN(x,y) ((x) < (y) ? (x) : (y))

static kuda_status_t consume_brigade(h2_filter_cin *cin, 
                                    kuda_bucket_brigade *bb, 
                                    kuda_read_type_e block)
{
    kuda_status_t status = KUDA_SUCCESS;
    kuda_size_t readlen = 0;
    
    while (status == KUDA_SUCCESS && !KUDA_BRIGADE_EMPTY(bb)) {
        
        kuda_bucket* bucket = KUDA_BRIGADE_FIRST(bb);
        if (KUDA_BUCKET_IS_METADATA(bucket)) {
            /* we do nothing regarding any meta here */
        }
        else {
            const char *bucket_data = NULL;
            kuda_size_t bucket_length = 0;
            status = kuda_bucket_read(bucket, &bucket_data,
                                     &bucket_length, block);
            
            if (status == KUDA_SUCCESS && bucket_length > 0) {
                kuda_size_t consumed = 0;

                status = cin->cb(cin->cb_ctx, bucket_data, bucket_length, &consumed);
                if (status == KUDA_SUCCESS && bucket_length > consumed) {
                    /* We have data left in the bucket. Split it. */
                    status = kuda_bucket_split(bucket, consumed);
                }
                readlen += consumed;
                cin->start_read = kuda_time_now();
            }
        }
        kuda_bucket_delete(bucket);
    }
    
    if (readlen == 0 && status == KUDA_SUCCESS && block == KUDA_NONBLOCK_READ) {
        return KUDA_EAGAIN;
    }
    return status;
}

h2_filter_cin *h2_filter_cin_create(kuda_pool_t *p, h2_filter_cin_cb *cb, void *ctx)
{
    h2_filter_cin *cin;
    
    cin = kuda_pcalloc(p, sizeof(*cin));
    cin->pool      = p;
    cin->cb        = cb;
    cin->cb_ctx    = ctx;
    cin->start_read = UNSET;
    return cin;
}

void h2_filter_cin_timeout_set(h2_filter_cin *cin, kuda_interval_time_t timeout)
{
    cin->timeout = timeout;
}

kuda_status_t h2_filter_core_input(clhy_filter_t* f,
                                  kuda_bucket_brigade* brigade,
                                  clhy_input_mode_t mode,
                                  kuda_read_type_e block,
                                  kuda_off_t readbytes) 
{
    h2_filter_cin *cin = f->ctx;
    kuda_status_t status = KUDA_SUCCESS;
    kuda_interval_time_t saved_timeout = UNSET;
    
    clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, f->c,
                  "core_input(%ld): read, %s, mode=%d, readbytes=%ld", 
                  (long)f->c->id, (block == KUDA_BLOCK_READ)? "BLOCK_READ" : "NONBLOCK_READ", 
                  mode, (long)readbytes);
    
    if (mode == CLHY_MODE_INIT || mode == CLHY_MODE_SPECULATIVE) {
        return clhy_get_brigade(f->next, brigade, mode, block, readbytes);
    }
    
    if (mode != CLHY_MODE_READBYTES) {
        return (block == KUDA_BLOCK_READ)? KUDA_SUCCESS : KUDA_EAGAIN;
    }
    
    if (!cin->bb) {
        cin->bb = kuda_brigade_create(cin->pool, f->c->bucket_alloc);
    }

    if (!cin->socket) {
        cin->socket = clhy_get_conn_socket(f->c);
    }
    
    cin->start_read = kuda_time_now();
    if (KUDA_BRIGADE_EMPTY(cin->bb)) {
        /* We only do a blocking read when we have no streams to process. So,
         * in wwhy scoreboard lingo, we are in a KEEPALIVE connection state.
         * When reading non-blocking, we do have streams to process and update
         * child with NULL request. That way, any current request information
         * in the scoreboard is preserved.
         */
        if (block == KUDA_BLOCK_READ) {
            if (cin->timeout > 0) {
                kuda_socket_timeout_get(cin->socket, &saved_timeout);
                kuda_socket_timeout_set(cin->socket, cin->timeout);
            }
        }
        status = clhy_get_brigade(f->next, cin->bb, CLHY_MODE_READBYTES,
                                block, readbytes);
        if (saved_timeout != UNSET) {
            kuda_socket_timeout_set(cin->socket, saved_timeout);
        }
    }
    
    switch (status) {
        case KUDA_SUCCESS:
            status = consume_brigade(cin, cin->bb, block);
            break;
        case KUDA_EOF:
        case KUDA_EAGAIN:
        case KUDA_TIMEUP:
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE1, status, f->c,
                          "core_input(%ld): read", (long)f->c->id);
            break;
        default:
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, status, f->c, CLHYLOGNO(03046)
                          "h2_conn_io: error reading");
            break;
    }
    return status;
}

/*******************************************************************************
 * http2 connection status handler + stream out source
 ******************************************************************************/

typedef struct {
    kuda_bucket_refcount refcount;
    h2_bucket_event_cb *cb;
    void *ctx;
} h2_bucket_observer;
 
static kuda_status_t bucket_read(kuda_bucket *b, const char **str,
                                kuda_size_t *len, kuda_read_type_e block)
{
    (void)b;
    (void)block;
    *str = NULL;
    *len = 0;
    return KUDA_SUCCESS;
}

static void bucket_destroy(void *data)
{
    h2_bucket_observer *h = data;
    if (kuda_bucket_shared_destroy(h)) {
        if (h->cb) {
            h->cb(h->ctx, H2_BUCKET_EV_BEFORE_DESTROY, NULL);
        }
        kuda_bucket_free(h);
    }
}

kuda_bucket * h2_bucket_observer_make(kuda_bucket *b, h2_bucket_event_cb *cb,
                                 void *ctx)
{
    h2_bucket_observer *br;

    br = kuda_bucket_alloc(sizeof(*br), b->list);
    br->cb = cb;
    br->ctx = ctx;

    b = kuda_bucket_shared_make(b, br, 0, 0);
    b->type = &h2_bucket_type_observer;
    return b;
} 

kuda_bucket * h2_bucket_observer_create(kuda_bucket_alloc_t *list, 
                                       h2_bucket_event_cb *cb, void *ctx)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    b = h2_bucket_observer_make(b, cb, ctx);
    return b;
}
                                       
kuda_status_t h2_bucket_observer_fire(kuda_bucket *b, h2_bucket_event event)
{
    if (H2_BUCKET_IS_OBSERVER(b)) {
        h2_bucket_observer *l = (h2_bucket_observer *)b->data; 
        return l->cb(l->ctx, event, b);
    }
    return KUDA_EINVAL;
}

const kuda_bucket_type_t h2_bucket_type_observer = {
    "H2OBS", 5, KUDA_BUCKET_METADATA,
    bucket_destroy,
    bucket_read,
    kuda_bucket_setaside_noop,
    kuda_bucket_split_notimpl,
    kuda_bucket_shared_copy
};

kuda_bucket *h2_bucket_observer_beam(struct h2_bucket_beam *beam,
                                    kuda_bucket_brigade *dest,
                                    const kuda_bucket *src)
{
    if (H2_BUCKET_IS_OBSERVER(src)) {
        h2_bucket_observer *l = (h2_bucket_observer *)src->data; 
        kuda_bucket *b = h2_bucket_observer_create(dest->bucket_alloc, 
                                                  l->cb, l->ctx);
        KUDA_BRIGADE_INSERT_TAIL(dest, b);
        l->cb = NULL;
        l->ctx = NULL;
        h2_bucket_observer_fire(b, H2_BUCKET_EV_BEFORE_MASTER_SEND);
        return b;
    }
    return NULL;
}

static kuda_status_t bbout(kuda_bucket_brigade *bb, const char *fmt, ...)
{
    va_list args;
    kuda_status_t rv;

    va_start(args, fmt);
    rv = kuda_brigade_vprintf(bb, NULL, NULL, fmt, args);
    va_end(args);

    return rv;
}

static void add_settings(kuda_bucket_brigade *bb, h2_session *s, int last) 
{
    h2_mplx *m = s->mplx;
    
    bbout(bb, "  \"settings\": {\n");
    bbout(bb, "    \"SETTINGS_MAX_CONCURRENT_STREAMS\": %d,\n", m->max_streams); 
    bbout(bb, "    \"SETTINGS_MAX_FRAME_SIZE\": %d,\n", 16*1024); 
    bbout(bb, "    \"SETTINGS_INITIAL_WINDOW_SIZE\": %d,\n",
          h2_config_geti(s->config, H2_CONF_WIN_SIZE));
    bbout(bb, "    \"SETTINGS_ENABLE_PUSH\": %d\n", h2_session_push_enabled(s)); 
    bbout(bb, "  }%s\n", last? "" : ",");
}

static void add_peer_settings(kuda_bucket_brigade *bb, h2_session *s, int last) 
{
    bbout(bb, "  \"peerSettings\": {\n");
    bbout(bb, "    \"SETTINGS_MAX_CONCURRENT_STREAMS\": %d,\n", 
        nghttp2_session_get_remote_settings(s->ngh2, NGHTTP2_SETTINGS_MAX_CONCURRENT_STREAMS)); 
    bbout(bb, "    \"SETTINGS_MAX_FRAME_SIZE\": %d,\n", 
        nghttp2_session_get_remote_settings(s->ngh2, NGHTTP2_SETTINGS_MAX_FRAME_SIZE)); 
    bbout(bb, "    \"SETTINGS_INITIAL_WINDOW_SIZE\": %d,\n", 
        nghttp2_session_get_remote_settings(s->ngh2, NGHTTP2_SETTINGS_INITIAL_WINDOW_SIZE)); 
    bbout(bb, "    \"SETTINGS_ENABLE_PUSH\": %d,\n", 
        nghttp2_session_get_remote_settings(s->ngh2, NGHTTP2_SETTINGS_ENABLE_PUSH)); 
    bbout(bb, "    \"SETTINGS_HEADER_TABLE_SIZE\": %d,\n", 
        nghttp2_session_get_remote_settings(s->ngh2, NGHTTP2_SETTINGS_HEADER_TABLE_SIZE)); 
    bbout(bb, "    \"SETTINGS_MAX_HEADER_LIST_SIZE\": %d\n", 
        nghttp2_session_get_remote_settings(s->ngh2, NGHTTP2_SETTINGS_MAX_HEADER_LIST_SIZE)); 
    bbout(bb, "  }%s\n", last? "" : ",");
}

typedef struct {
    kuda_bucket_brigade *bb;
    h2_session *s;
    int idx;
} stream_ctx_t;

static int add_stream(h2_stream *stream, void *ctx)
{
    stream_ctx_t *x = ctx;
    int32_t flowIn, flowOut;
    
    flowIn = nghttp2_session_get_stream_effective_local_window_size(x->s->ngh2, stream->id); 
    flowOut = nghttp2_session_get_stream_remote_window_size(x->s->ngh2, stream->id);
    bbout(x->bb, "%s\n    \"%d\": {\n", (x->idx? "," : ""), stream->id);
    bbout(x->bb, "    \"state\": \"%s\",\n", h2_stream_state_str(stream));
    bbout(x->bb, "    \"created\": %f,\n", ((double)stream->created)/KUDA_USEC_PER_SEC);
    bbout(x->bb, "    \"flowIn\": %d,\n", flowIn);
    bbout(x->bb, "    \"flowOut\": %d,\n", flowOut);
    bbout(x->bb, "    \"dataIn\": %"KUDA_UINT64_T_FMT",\n", stream->in_data_octets);  
    bbout(x->bb, "    \"dataOut\": %"KUDA_UINT64_T_FMT"\n", stream->out_data_octets);  
    bbout(x->bb, "    }");
    
    ++x->idx;
    return 1;
} 

static void add_streams(kuda_bucket_brigade *bb, h2_session *s, int last) 
{
    stream_ctx_t x;
    
    x.bb = bb;
    x.s = s;
    x.idx = 0;
    bbout(bb, "  \"streams\": {");
    h2_mplx_stream_do(s->mplx, add_stream, &x);
    bbout(bb, "\n  }%s\n", last? "" : ",");
}

static void add_push(kuda_bucket_brigade *bb, h2_session *s, 
                     h2_stream *stream, int last) 
{
    h2_push_diary *diary;
    kuda_status_t status;
    
    bbout(bb, "    \"push\": {\n");
    diary = s->push_diary;
    if (diary) {
        const char *data;
        const char *base64_digest;
        kuda_size_t len;
        
        status = h2_push_diary_digest_get(diary, bb->p, 256, 
                                          stream->request->authority, 
                                          &data, &len);
        if (status == KUDA_SUCCESS) {
            base64_digest = h2_util_base64url_encode(data, len, bb->p);
            bbout(bb, "      \"cacheDigest\": \"%s\",\n", base64_digest);
        }
    }
    bbout(bb, "      \"promises\": %d,\n", s->pushes_promised);
    bbout(bb, "      \"submits\": %d,\n", s->pushes_submitted);
    bbout(bb, "      \"resets\": %d\n", s->pushes_reset);
    bbout(bb, "    }%s\n", last? "" : ",");
}

static void add_in(kuda_bucket_brigade *bb, h2_session *s, int last) 
{
    bbout(bb, "    \"in\": {\n");
    bbout(bb, "      \"requests\": %d,\n", s->remote.emitted_count);
    bbout(bb, "      \"resets\": %d, \n", s->streams_reset);
    bbout(bb, "      \"frames\": %ld,\n", (long)s->frames_received);
    bbout(bb, "      \"octets\": %"KUDA_UINT64_T_FMT"\n", s->io.bytes_read);
    bbout(bb, "    }%s\n", last? "" : ",");
}

static void add_out(kuda_bucket_brigade *bb, h2_session *s, int last) 
{
    bbout(bb, "    \"out\": {\n");
    bbout(bb, "      \"responses\": %d,\n", s->responses_submitted);
    bbout(bb, "      \"frames\": %ld,\n", (long)s->frames_sent);
    bbout(bb, "      \"octets\": %"KUDA_UINT64_T_FMT"\n", s->io.bytes_written);
    bbout(bb, "    }%s\n", last? "" : ",");
}

static void add_stats(kuda_bucket_brigade *bb, h2_session *s, 
                     h2_stream *stream, int last) 
{
    bbout(bb, "  \"stats\": {\n");
    add_in(bb, s, 0);
    add_out(bb, s, 0);
    add_push(bb, s, stream, 1);
    bbout(bb, "  }%s\n", last? "" : ",");
}

static kuda_status_t h2_status_insert(h2_task *task, kuda_bucket *b)
{
    h2_mplx *m = task->mplx;
    h2_stream *stream = h2_mplx_stream_get(m, task->stream_id);
    h2_session *s;
    conn_rec *c;
    
    kuda_bucket_brigade *bb;
    kuda_bucket *e;
    int32_t connFlowIn, connFlowOut;
    
    if (!stream) {
        /* stream already done */
        return KUDA_SUCCESS;
    }
    s = stream->session;
    c = s->c;
    
    bb = kuda_brigade_create(stream->pool, c->bucket_alloc);
    
    connFlowIn = nghttp2_session_get_effective_local_window_size(s->ngh2); 
    connFlowOut = nghttp2_session_get_remote_window_size(s->ngh2);
     
    bbout(bb, "{\n");
    bbout(bb, "  \"version\": \"draft-01\",\n");
    add_settings(bb, s, 0);
    add_peer_settings(bb, s, 0);
    bbout(bb, "  \"connFlowIn\": %d,\n", connFlowIn);
    bbout(bb, "  \"connFlowOut\": %d,\n", connFlowOut);
    bbout(bb, "  \"sentGoAway\": %d,\n", s->local.shutdown);

    add_streams(bb, s, 0);
    
    add_stats(bb, s, stream, 1);
    bbout(bb, "}\n");
    
    while ((e = KUDA_BRIGADE_FIRST(bb)) != KUDA_BRIGADE_SENTINEL(bb)) {
        KUDA_BUCKET_REMOVE(e);
        KUDA_BUCKET_INSERT_AFTER(b, e);
        b = e;
    }
    kuda_brigade_destroy(bb);
    
    return KUDA_SUCCESS;
}

static kuda_status_t status_event(void *ctx, h2_bucket_event event, 
                                 kuda_bucket *b)
{
    h2_task *task = ctx;
    
    clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE3, 0, task->c->master, 
                  "status_event(%s): %d", task->id, event);
    switch (event) {
        case H2_BUCKET_EV_BEFORE_MASTER_SEND:
            h2_status_insert(task, b);
            break;
        default:
            break;
    }
    return KUDA_SUCCESS;
}

int h2_filter_h2_status_handler(request_rec *r)
{
    h2_ctx *ctx = h2_ctx_rget(r);
    conn_rec *c = r->connection;
    h2_task *task;
    kuda_bucket_brigade *bb;
    kuda_bucket *b;
    kuda_status_t status;
    
    if (strcmp(r->handler, "http2-status")) {
        return DECLINED;
    }
    if (r->method_number != M_GET && r->method_number != M_POST) {
        return DECLINED;
    }

    task = ctx? h2_ctx_get_task(ctx) : NULL;
    if (task) {

        if ((status = clhy_discard_request_body(r)) != OK) {
            return status;
        }
        
        /* We need to handle the actual output on the main thread, as
         * we need to access h2_session information. */
        r->status = 200;
        r->clength = -1;
        r->chunked = 1;
        kuda_table_unset(r->headers_out, "Content-Length");
        clhy_set_content_type(r, "application/json");
        kuda_table_setn(r->notes, H2_FILTER_DEBUG_NOTE, "on");

        bb = kuda_brigade_create(r->pool, c->bucket_alloc);
        b = h2_bucket_observer_create(c->bucket_alloc, status_event, task);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);
        b = kuda_bucket_eos_create(c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);

        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                      "status_handler(%s): checking for incoming trailers", 
                      task->id);
        if (r->trailers_in && !kuda_is_empty_table(r->trailers_in)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                          "status_handler(%s): seeing incoming trailers", 
                          task->id);
            kuda_table_setn(r->trailers_out, "h2-trailers-in", 
                           kuda_itoa(r->pool, 1));
        }
        
        status = clhy_pass_brigade(r->output_filters, bb);
        if (status == KUDA_SUCCESS
            || r->status != HTTP_OK
            || c->aborted) {
            return OK;
        }
        else {
            /* no way to know what type of error occurred */
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, status, r,
                          "status_handler(%s): clhy_pass_brigade failed", 
                          task->id);
            return CLHY_FILTER_ERROR;
        }
    }
    return DECLINED;
}

