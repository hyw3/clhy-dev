/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __capi_h2__h2_switch__
#define __capi_h2__h2_switch__

/*
 * One time, post config initialization.
 */
kuda_status_t h2_switch_init(kuda_pool_t *pool, server_rec *s);

/* Register cLHy hooks for protocol switching
 */
void h2_switch_register_hooks(void);


#endif /* defined(__capi_h2__h2_switch__) */

