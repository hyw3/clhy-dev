/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <stddef.h>

#include <wwhy.h>
#include <http_core.h>
#include <http_connection.h>
#include <http_log.h>

#include "h2_private.h"
#include "h2.h"
#include "h2_mplx.h"
#include "h2_stream.h"
#include "h2_bucket_eos.h"

typedef struct {
    kuda_bucket_refcount refcount;
    h2_stream *stream;
} h2_bucket_eos;

static kuda_status_t bucket_cleanup(void *data)
{
    h2_stream **pstream = data;

    if (*pstream) {
        /* If bucket_destroy is called after us, this prevents
         * bucket_destroy from trying to destroy the stream again. */
        *pstream = NULL;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t bucket_read(kuda_bucket *b, const char **str,
                                kuda_size_t *len, kuda_read_type_e block)
{
    (void)b;
    (void)block;
    *str = NULL;
    *len = 0;
    return KUDA_SUCCESS;
}

kuda_bucket *h2_bucket_eos_make(kuda_bucket *b, h2_stream *stream)
{
    h2_bucket_eos *h;

    h = kuda_bucket_alloc(sizeof(*h), b->list);
    h->stream = stream;

    b = kuda_bucket_shared_make(b, h, 0, 0);
    b->type = &h2_bucket_type_eos;
    
    return b;
}

kuda_bucket *h2_bucket_eos_create(kuda_bucket_alloc_t *list,
                                 h2_stream *stream)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    b = h2_bucket_eos_make(b, stream);
    if (stream) {
        h2_bucket_eos *h = b->data;
        kuda_pool_pre_cleanup_register(stream->pool, &h->stream, bucket_cleanup);
    }
    return b;
}

static void bucket_destroy(void *data)
{
    h2_bucket_eos *h = data;

    if (kuda_bucket_shared_destroy(h)) {
        h2_stream *stream = h->stream;
        if (stream && stream->pool) {
            kuda_pool_cleanup_kill(stream->pool, &h->stream, bucket_cleanup);
        }
        kuda_bucket_free(h);
        if (stream) {
            h2_stream_dispatch(stream, H2_SEV_EOS_SENT);
        }
    }
}

const kuda_bucket_type_t h2_bucket_type_eos = {
    "H2EOS", 5, KUDA_BUCKET_METADATA,
    bucket_destroy,
    bucket_read,
    kuda_bucket_setaside_noop,
    kuda_bucket_split_notimpl,
    kuda_bucket_shared_copy
};

