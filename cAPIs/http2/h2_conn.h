/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __capi_h2__h2_conn__
#define __capi_h2__h2_conn__

struct h2_ctx;
struct h2_task;

/**
 * Setup the connection and our context for HTTP/2 processing
 *
 * @param c the connection HTTP/2 is starting on
 * @param r the upgrade request that still awaits an answer, optional
 * @param s the server selected for this connection (can be != c->base_server)
 */
kuda_status_t h2_conn_setup(conn_rec *c, request_rec *r, server_rec *s);

/**
 * Run the HTTP/2 connection in synchronous fashion.
 * Return when the HTTP/2 session is done
 * and the connection will close or a fatal error occurred.
 *
 * @param c the http2 connection to run
 * @return KUDA_SUCCESS when session is done.
 */
kuda_status_t h2_conn_run(conn_rec *c);

/**
 * The connection is about to close. If we have not send a GOAWAY
 * yet, this is the last chance.
 */
kuda_status_t h2_conn_pre_close(struct h2_ctx *ctx, conn_rec *c);

/* Initialize this child process for h2 connection work,
 * to be called once during child init before multi processing
 * starts.
 */
kuda_status_t h2_conn_child_init(kuda_pool_t *pool, server_rec *s);


typedef enum {
    H2_CLMP_UNKNOWN,
    H2_CLMP_WORKER,
    H2_CLMP_EVENT,
    H2_CLMP_PREFORK,
    H2_CLMP_MOTORZ,
    H2_CLMP_SIMPLE,
    H2_CLMP_NETWARE,
    H2_CLMP_WINNT,
} h2_clmp_type_t;

/* Returns the type of cLMP detected */
h2_clmp_type_t h2_conn_clmp_type(void);
const char *h2_conn_clmp_name(void);
int h2_clmp_supported(void);

conn_rec *h2_slave_create(conn_rec *master, int slave_id, kuda_pool_t *parent);
void h2_slave_destroy(conn_rec *slave);

kuda_status_t h2_slave_run_pre_connection(conn_rec *slave, kuda_socket_t *csd);
void h2_slave_run_connection(conn_rec *slave);

#endif /* defined(__capi_h2__h2_conn__) */
