/* The cLHy Server
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __capi_h2__h2_clfilter__
#define __capi_h2__h2_clfilter__

struct h2_bucket_beam;
struct h2_headers;
struct h2_stream;
struct h2_session;

typedef struct h2_clfilter_cin {
    kuda_pool_t *pool;
    kuda_socket_t *socket;
    kuda_interval_time_t timeout;
    kuda_bucket_brigade *bb;
    struct h2_session *session;
    kuda_bucket *cur;
} h2_clfilter_cin;

h2_clfilter_cin *h2_clfilter_cin_create(struct h2_session *session);

void h2_clfilter_cin_timeout_set(h2_clfilter_cin *cin, kuda_interval_time_t timeout);

kuda_status_t h2_clfilter_core_input(clhy_filter_t* filter,
                                  kuda_bucket_brigade* brigade,
                                  clhy_input_mode_t mode,
                                  kuda_read_type_e block,
                                  kuda_off_t readbytes);

/******* observer bucket ******************************************************/

typedef enum {
    H2_BUCKET_EV_BEFORE_DESTROY,
    H2_BUCKET_EV_BEFORE_MASTER_SEND
} h2_bucket_event;

extern const kuda_bucket_type_t h2_bucket_type_observer;

typedef kuda_status_t h2_bucket_event_cb(void *ctx, h2_bucket_event event, kuda_bucket *b);

#define H2_BUCKET_IS_OBSERVER(e)     (e->type == &h2_bucket_type_observer)

kuda_bucket * h2_bucket_observer_make(kuda_bucket *b, h2_bucket_event_cb *cb, 
                                     void *ctx); 

kuda_bucket * h2_bucket_observer_create(kuda_bucket_alloc_t *list, 
                                       h2_bucket_event_cb *cb, void *ctx); 
                                       
kuda_status_t h2_bucket_observer_fire(kuda_bucket *b, h2_bucket_event event);

kuda_bucket *h2_bucket_observer_beam(struct h2_bucket_beam *beam,
                                    kuda_bucket_brigade *dest,
                                    const kuda_bucket *src);

/******* /.well-known/h2/state handler ****************************************/

int h2_clfilter_h2_status_handler(request_rec *r);

#endif /* __capi_h2__h2_clfilter__ */
