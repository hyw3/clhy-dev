/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __capi_h2__h2_request__
#define __capi_h2__h2_request__

#include "h2.h"

kuda_status_t h2_request_rcreate(h2_request **preq, kuda_pool_t *pool,
                                request_rec *r);

kuda_status_t h2_request_add_header(h2_request *req, kuda_pool_t *pool,
                                   const char *name, size_t nlen,
                                   const char *value, size_t vlen);

kuda_status_t h2_request_add_trailer(h2_request *req, kuda_pool_t *pool,
                                    const char *name, size_t nlen,
                                    const char *value, size_t vlen);

kuda_status_t h2_request_end_headers(h2_request *req, kuda_pool_t *pool, int eos, size_t raw_bytes);

h2_request *h2_request_clone(kuda_pool_t *p, const h2_request *src);

/**
 * Create a request_rec representing the h2_request to be
 * processed on the given connection.
 *
 * @param req the h2 request to process
 * @param conn the connection to process the request on
 * @return the request_rec representing the request
 */
request_rec *h2_request_create_rec(const h2_request *req, conn_rec *conn);


#endif /* defined(__capi_h2__h2_request__) */

