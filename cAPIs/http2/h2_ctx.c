/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>

#include <wwhy.h>
#include <http_core.h>
#include <http_config.h>

#include "h2_private.h"
#include "h2_session.h"
#include "h2_task.h"
#include "h2_ctx.h"

static h2_ctx *h2_ctx_create(const conn_rec *c)
{
    h2_ctx *ctx = kuda_pcalloc(c->pool, sizeof(h2_ctx));
    clhy_assert(ctx);
    h2_ctx_server_update(ctx, c->base_server);
    clhy_set_capi_config(c->conn_config, &http2_capi, ctx);
    return ctx;
}

void h2_ctx_clear(const conn_rec *c)
{
    clhy_assert(c);
    clhy_set_capi_config(c->conn_config, &http2_capi, NULL);
}

h2_ctx *h2_ctx_create_for(const conn_rec *c, h2_task *task)
{
    h2_ctx *ctx = h2_ctx_create(c);
    if (ctx) {
        ctx->task = task;
    }
    return ctx;
}

h2_ctx *h2_ctx_get(const conn_rec *c, int create)
{
    h2_ctx *ctx = (h2_ctx*)clhy_get_capi_config(c->conn_config, &http2_capi);
    if (ctx == NULL && create) {
        ctx = h2_ctx_create(c);
    }
    return ctx;
}

h2_ctx *h2_ctx_rget(const request_rec *r)
{
    return h2_ctx_get(r->connection, 0);
}

const char *h2_ctx_protocol_get(const conn_rec *c)
{
    h2_ctx *ctx;
    if (c->master) {
        c = c->master;
    }
    ctx = (h2_ctx*)clhy_get_capi_config(c->conn_config, &http2_capi);
    return ctx? ctx->protocol : NULL;
}

h2_ctx *h2_ctx_protocol_set(h2_ctx *ctx, const char *proto)
{
    ctx->protocol = proto;
    return ctx;
}

h2_session *h2_ctx_get_session(conn_rec *c)
{
    h2_ctx *ctx = h2_ctx_get(c, 0);
    return ctx? ctx->session : NULL;
}

void h2_ctx_session_set(h2_ctx *ctx, struct h2_session *session)
{
    ctx->session = session;
}

h2_ctx *h2_ctx_server_update(h2_ctx *ctx, server_rec *s)
{
    if (ctx->server != s) {
        ctx->server = s;
    }
    return ctx;
}

h2_task *h2_ctx_get_task(conn_rec *c)
{
    h2_ctx *ctx = h2_ctx_get(c, 0);
    return ctx? ctx->task : NULL;
}

