/* The cLHy Server
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>

#include <kuda_strings.h>
#include <wwhy.h>
#include <http_core.h>
#include <http_protocol.h>
#include <http_log.h>
#include <http_connection.h>
#include <scoreboard.h>

#include "h2_private.h"
#include "h2.h"
#include "h2_config.h"
#include "h2_conn_io.h"
#include "h2_ctx.h"
#include "h2_mplx.h"
#include "h2_push.h"
#include "h2_task.h"
#include "h2_stream.h"
#include "h2_request.h"
#include "h2_headers.h"
#include "h2_stream.h"
#include "h2_session.h"
#include "h2_util.h"
#include "h2_version.h"

#include "h2_clfilter.h"

#define UNSET       -1
#define H2MIN(x,y) ((x) < (y) ? (x) : (y))

static kuda_status_t recv_RAW_DATA(conn_rec *c, h2_clfilter_cin *cin, 
                                  kuda_bucket *b, kuda_read_type_e block)
{
    h2_session *session = cin->session;
    kuda_status_t status = KUDA_SUCCESS;
    kuda_size_t len;
    const char *data;
    ssize_t n;
    
    (void)c;
    status = kuda_bucket_read(b, &data, &len, block);
    
    while (status == KUDA_SUCCESS && len > 0) {
        n = nghttp2_session_mem_recv(session->ngh2, (const uint8_t *)data, len);
        
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, session->c,
                      H2_SSSN_MSG(session, "fed %ld bytes to nghttp2, %ld read"),
                      (long)len, (long)n);
        if (n < 0) {
            if (nghttp2_is_fatal((int)n)) {
                h2_session_event(session, H2_SESSION_EV_PROTO_ERROR, 
                                 (int)n, nghttp2_strerror((int)n));
                status = KUDA_EGENERAL;
            }
        }
        else {
            session->io.bytes_read += n;
            if ((kuda_ssize_t)len <= n) {
                break;
            }
            len -= (kuda_size_t)n;
            data += n;
        }
    }
    
    return status;
}

static kuda_status_t recv_RAW_brigade(conn_rec *c, h2_clfilter_cin *cin, 
                                     kuda_bucket_brigade *bb, 
                                     kuda_read_type_e block)
{
    kuda_status_t status = KUDA_SUCCESS;
    kuda_bucket* b;
    int consumed = 0;
    
    h2_util_bb_log(c, c->id, CLHYLOG_TRACE2, "RAW_in", bb);
    while (status == KUDA_SUCCESS && !KUDA_BRIGADE_EMPTY(bb)) {
        b = KUDA_BRIGADE_FIRST(bb);

        if (KUDA_BUCKET_IS_METADATA(b)) {
            /* nop */
        }
        else {
            status = recv_RAW_DATA(c, cin, b, block);
        }
        consumed = 1;
        kuda_bucket_delete(b);
    }
    
    if (!consumed && status == KUDA_SUCCESS && block == KUDA_NONBLOCK_READ) {
        return KUDA_EAGAIN;
    }
    return status;
}

h2_clfilter_cin *h2_clfilter_cin_create(h2_session *session)
{
    h2_clfilter_cin *cin;
    
    cin = kuda_pcalloc(session->pool, sizeof(*cin));
    if (!cin) {
        return NULL;
    }
    cin->session = session;
    return cin;
}

void h2_clfilter_cin_timeout_set(h2_clfilter_cin *cin, kuda_interval_time_t timeout)
{
    cin->timeout = timeout;
}

kuda_status_t h2_clfilter_core_input(clhy_filter_t* f,
                                  kuda_bucket_brigade* brigade,
                                  clhy_input_mode_t mode,
                                  kuda_read_type_e block,
                                  kuda_off_t readbytes) 
{
    h2_clfilter_cin *cin = f->ctx;
    kuda_status_t status = KUDA_SUCCESS;
    kuda_interval_time_t saved_timeout = UNSET;
    const int trace1 = CLHYLOGctrace1(f->c);
    
    if (trace1) {
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, f->c,
                      "h2_session(%ld): read, %s, mode=%d, readbytes=%ld", 
                      (long)f->c->id, (block == KUDA_BLOCK_READ)? 
                      "BLOCK_READ" : "NONBLOCK_READ", mode, (long)readbytes);
    }
    
    if (mode == CLHY_MODE_INIT || mode == CLHY_MODE_SPECULATIVE) {
        return clhy_get_brigade(f->next, brigade, mode, block, readbytes);
    }
    
    if (mode != CLHY_MODE_READBYTES) {
        return (block == KUDA_BLOCK_READ)? KUDA_SUCCESS : KUDA_EAGAIN;
    }
    
    if (!cin->bb) {
        cin->bb = kuda_brigade_create(cin->session->pool, f->c->bucket_alloc);
    }

    if (!cin->socket) {
        cin->socket = clhy_get_conn_socket(f->c);
    }
    
    if (KUDA_BRIGADE_EMPTY(cin->bb)) {
        /* 
         * KEEPALIVE connection state in wwhy scoreboard
         */
        if (block == KUDA_BLOCK_READ) {
            if (cin->timeout > 0) {
                kuda_socket_timeout_get(cin->socket, &saved_timeout);
                kuda_socket_timeout_set(cin->socket, cin->timeout);
            }
        }
        status = clhy_get_brigade(f->next, cin->bb, CLHY_MODE_READBYTES,
                                block, readbytes);
        if (saved_timeout != UNSET) {
            kuda_socket_timeout_set(cin->socket, saved_timeout);
        }
    }
    
    switch (status) {
        case KUDA_SUCCESS:
            status = recv_RAW_brigade(f->c, cin, cin->bb, block);
            break;
        case KUDA_EOF:
        case KUDA_EAGAIN:
        case KUDA_TIMEUP:
            if (trace1) {
                clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE1, status, f->c,
                              "h2_session(%ld): read", f->c->id);
            }
            break;
        default:
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, status, f->c, CLHYLOGNO(03046)
                          "h2_session(%ld): error reading", f->c->id);
            break;
    }
    return status;
}

/*******************************************************************************
 * http2 connection status handler + stream out source
 ******************************************************************************/

typedef struct {
    kuda_bucket_refcount refcount;
    h2_bucket_event_cb *cb;
    void *ctx;
} h2_bucket_observer;
 
static kuda_status_t bucket_read(kuda_bucket *b, const char **str,
                                kuda_size_t *len, kuda_read_type_e block)
{
    (void)b;
    (void)block;
    *str = NULL;
    *len = 0;
    return KUDA_SUCCESS;
}

static void bucket_destroy(void *data)
{
    h2_bucket_observer *h = data;
    if (kuda_bucket_shared_destroy(h)) {
        if (h->cb) {
            h->cb(h->ctx, H2_BUCKET_EV_BEFORE_DESTROY, NULL);
        }
        kuda_bucket_free(h);
    }
}

kuda_bucket * h2_bucket_observer_make(kuda_bucket *b, h2_bucket_event_cb *cb,
                                 void *ctx)
{
    h2_bucket_observer *br;

    br = kuda_bucket_alloc(sizeof(*br), b->list);
    br->cb = cb;
    br->ctx = ctx;

    b = kuda_bucket_shared_make(b, br, 0, 0);
    b->type = &h2_bucket_type_observer;
    return b;
} 

kuda_bucket * h2_bucket_observer_create(kuda_bucket_alloc_t *list, 
                                       h2_bucket_event_cb *cb, void *ctx)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    b = h2_bucket_observer_make(b, cb, ctx);
    return b;
}
                                       
kuda_status_t h2_bucket_observer_fire(kuda_bucket *b, h2_bucket_event event)
{
    if (H2_BUCKET_IS_OBSERVER(b)) {
        h2_bucket_observer *l = (h2_bucket_observer *)b->data; 
        return l->cb(l->ctx, event, b);
    }
    return KUDA_EINVAL;
}

const kuda_bucket_type_t h2_bucket_type_observer = {
    "H2OBS", 5, KUDA_BUCKET_METADATA,
    bucket_destroy,
    bucket_read,
    kuda_bucket_setaside_noop,
    kuda_bucket_split_notimpl,
    kuda_bucket_shared_copy
};

kuda_bucket *h2_bucket_observer_beam(struct h2_bucket_beam *beam,
                                    kuda_bucket_brigade *dest,
                                    const kuda_bucket *src)
{
    (void)beam;
    if (H2_BUCKET_IS_OBSERVER(src)) {
        h2_bucket_observer *l = (h2_bucket_observer *)src->data; 
        kuda_bucket *b = h2_bucket_observer_create(dest->bucket_alloc, 
                                                  l->cb, l->ctx);
        KUDA_BRIGADE_INSERT_TAIL(dest, b);
        l->cb = NULL;
        l->ctx = NULL;
        h2_bucket_observer_fire(b, H2_BUCKET_EV_BEFORE_MASTER_SEND);
        return b;
    }
    return NULL;
}

static kuda_status_t bbout(kuda_bucket_brigade *bb, const char *fmt, ...)
                             __attribute__((format(printf,2,3)));
static kuda_status_t bbout(kuda_bucket_brigade *bb, const char *fmt, ...)
{
    va_list args;
    kuda_status_t rv;

    va_start(args, fmt);
    rv = kuda_brigade_vprintf(bb, NULL, NULL, fmt, args);
    va_end(args);

    return rv;
}

static void add_settings(kuda_bucket_brigade *bb, h2_session *s, int last) 
{
    h2_mplx *m = s->mplx;
    
    bbout(bb, "  \"settings\": {\n");
    bbout(bb, "    \"SETTINGS_MAX_CONCURRENT_STREAMS\": %d,\n", m->max_streams); 
    bbout(bb, "    \"SETTINGS_MAX_FRAME_SIZE\": %d,\n", 16*1024); 
    bbout(bb, "    \"SETTINGS_INITIAL_WINDOW_SIZE\": %d,\n", h2_config_sgeti(s->s, H2_CONF_WIN_SIZE));
    bbout(bb, "    \"SETTINGS_ENABLE_PUSH\": %d\n", h2_session_push_enabled(s)); 
    bbout(bb, "  }%s\n", last? "" : ",");
}

static void add_peer_settings(kuda_bucket_brigade *bb, h2_session *s, int last) 
{
    bbout(bb, "  \"peerSettings\": {\n");
    bbout(bb, "    \"SETTINGS_MAX_CONCURRENT_STREAMS\": %d,\n", 
        nghttp2_session_get_remote_settings(s->ngh2, NGHTTP2_SETTINGS_MAX_CONCURRENT_STREAMS)); 
    bbout(bb, "    \"SETTINGS_MAX_FRAME_SIZE\": %d,\n", 
        nghttp2_session_get_remote_settings(s->ngh2, NGHTTP2_SETTINGS_MAX_FRAME_SIZE)); 
    bbout(bb, "    \"SETTINGS_INITIAL_WINDOW_SIZE\": %d,\n", 
        nghttp2_session_get_remote_settings(s->ngh2, NGHTTP2_SETTINGS_INITIAL_WINDOW_SIZE)); 
    bbout(bb, "    \"SETTINGS_ENABLE_PUSH\": %d,\n", 
        nghttp2_session_get_remote_settings(s->ngh2, NGHTTP2_SETTINGS_ENABLE_PUSH)); 
    bbout(bb, "    \"SETTINGS_HEADER_TABLE_SIZE\": %d,\n", 
        nghttp2_session_get_remote_settings(s->ngh2, NGHTTP2_SETTINGS_HEADER_TABLE_SIZE)); 
    bbout(bb, "    \"SETTINGS_MAX_HEADER_LIST_SIZE\": %d\n", 
        nghttp2_session_get_remote_settings(s->ngh2, NGHTTP2_SETTINGS_MAX_HEADER_LIST_SIZE)); 
    bbout(bb, "  }%s\n", last? "" : ",");
}

typedef struct {
    kuda_bucket_brigade *bb;
    h2_session *s;
    int idx;
} stream_ctx_t;

static int add_stream(h2_stream *stream, void *ctx)
{
    stream_ctx_t *x = ctx;
    int32_t flowIn, flowOut;
    
    flowIn = nghttp2_session_get_stream_effective_local_window_size(x->s->ngh2, stream->id); 
    flowOut = nghttp2_session_get_stream_remote_window_size(x->s->ngh2, stream->id);
    bbout(x->bb, "%s\n    \"%d\": {\n", (x->idx? "," : ""), stream->id);
    bbout(x->bb, "    \"state\": \"%s\",\n", h2_stream_state_str(stream));
    bbout(x->bb, "    \"created\": %f,\n", ((double)stream->created)/KUDA_USEC_PER_SEC);
    bbout(x->bb, "    \"flowIn\": %d,\n", flowIn);
    bbout(x->bb, "    \"flowOut\": %d,\n", flowOut);
    bbout(x->bb, "    \"dataIn\": %"KUDA_OFF_T_FMT",\n", stream->in_data_octets);  
    bbout(x->bb, "    \"dataOut\": %"KUDA_OFF_T_FMT"\n", stream->out_data_octets);  
    bbout(x->bb, "    }");
    
    ++x->idx;
    return 1;
} 

static void add_streams(kuda_bucket_brigade *bb, h2_session *s, int last) 
{
    stream_ctx_t x;
    
    x.bb = bb;
    x.s = s;
    x.idx = 0;
    bbout(bb, "  \"streams\": {");
    h2_mplx_stream_do(s->mplx, add_stream, &x);
    bbout(bb, "\n  }%s\n", last? "" : ",");
}

static void add_push(kuda_bucket_brigade *bb, h2_session *s, 
                     h2_stream *stream, int last) 
{
    h2_push_diary *diary;
    kuda_status_t status;
    
    bbout(bb, "    \"push\": {\n");
    diary = s->push_diary;
    if (diary) {
        const char *data;
        const char *base64_digest;
        kuda_size_t len;
        
        status = h2_push_diary_digest_get(diary, bb->p, 256, 
                                          stream->request->authority, 
                                          &data, &len);
        if (status == KUDA_SUCCESS) {
            base64_digest = h2_util_base64url_encode(data, len, bb->p);
            bbout(bb, "      \"cacheDigest\": \"%s\",\n", base64_digest);
        }
    }
    bbout(bb, "      \"promises\": %d,\n", s->pushes_promised);
    bbout(bb, "      \"submits\": %d,\n", s->pushes_submitted);
    bbout(bb, "      \"resets\": %d\n", s->pushes_reset);
    bbout(bb, "    }%s\n", last? "" : ",");
}

static void add_in(kuda_bucket_brigade *bb, h2_session *s, int last) 
{
    bbout(bb, "    \"in\": {\n");
    bbout(bb, "      \"requests\": %d,\n", s->remote.emitted_count);
    bbout(bb, "      \"resets\": %d, \n", s->streams_reset);
    bbout(bb, "      \"frames\": %ld,\n", (long)s->frames_received);
    bbout(bb, "      \"octets\": %"KUDA_UINT64_T_FMT"\n", s->io.bytes_read);
    bbout(bb, "    }%s\n", last? "" : ",");
}

static void add_out(kuda_bucket_brigade *bb, h2_session *s, int last) 
{
    bbout(bb, "    \"out\": {\n");
    bbout(bb, "      \"responses\": %d,\n", s->responses_submitted);
    bbout(bb, "      \"frames\": %ld,\n", (long)s->frames_sent);
    bbout(bb, "      \"octets\": %"KUDA_UINT64_T_FMT"\n", s->io.bytes_written);
    bbout(bb, "    }%s\n", last? "" : ",");
}

static void add_stats(kuda_bucket_brigade *bb, h2_session *s, 
                     h2_stream *stream, int last) 
{
    bbout(bb, "  \"stats\": {\n");
    add_in(bb, s, 0);
    add_out(bb, s, 0);
    add_push(bb, s, stream, 1);
    bbout(bb, "  }%s\n", last? "" : ",");
}

static kuda_status_t h2_status_insert(h2_task *task, kuda_bucket *b)
{
    h2_mplx *m = task->mplx;
    h2_stream *stream = h2_mplx_stream_get(m, task->stream_id);
    h2_session *s;
    conn_rec *c;
    
    kuda_bucket_brigade *bb;
    kuda_bucket *e;
    int32_t connFlowIn, connFlowOut;
    
    if (!stream) {
        return KUDA_SUCCESS;
    }
    s = stream->session;
    c = s->c;
    
    bb = kuda_brigade_create(stream->pool, c->bucket_alloc);
    
    connFlowIn = nghttp2_session_get_effective_local_window_size(s->ngh2); 
    connFlowOut = nghttp2_session_get_remote_window_size(s->ngh2);
     
    bbout(bb, "{\n");
    bbout(bb, "  \"version\": \"draft-01\",\n");
    add_settings(bb, s, 0);
    add_peer_settings(bb, s, 0);
    bbout(bb, "  \"connFlowIn\": %d,\n", connFlowIn);
    bbout(bb, "  \"connFlowOut\": %d,\n", connFlowOut);
    bbout(bb, "  \"sentGoAway\": %d,\n", s->local.shutdown);

    add_streams(bb, s, 0);
    
    add_stats(bb, s, stream, 1);
    bbout(bb, "}\n");
    
    while ((e = KUDA_BRIGADE_FIRST(bb)) != KUDA_BRIGADE_SENTINEL(bb)) {
        KUDA_BUCKET_REMOVE(e);
        KUDA_BUCKET_INSERT_AFTER(b, e);
        b = e;
    }
    kuda_brigade_destroy(bb);
    
    return KUDA_SUCCESS;
}

static kuda_status_t status_event(void *ctx, h2_bucket_event event, 
                                 kuda_bucket *b)
{
    h2_task *task = ctx;
    
    clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE3, 0, task->c->master, 
                  "status_event(%s): %d", task->id, event);
    switch (event) {
        case H2_BUCKET_EV_BEFORE_MASTER_SEND:
            h2_status_insert(task, b);
            break;
        default:
            break;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t discard_body(request_rec *r, kuda_off_t maxlen)
{
    kuda_bucket_brigade *bb;
    int seen_eos;
    kuda_status_t rv;

    bb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
    seen_eos = 0;
    do {
        kuda_bucket *bucket;

        rv = clhy_get_brigade(r->input_filters, bb, CLHY_MODE_READBYTES,
                            KUDA_BLOCK_READ, HUGE_STRING_LEN);

        if (rv != KUDA_SUCCESS) {
            kuda_brigade_destroy(bb);
            return rv;
        }

        for (bucket = KUDA_BRIGADE_FIRST(bb);
             bucket != KUDA_BRIGADE_SENTINEL(bb);
             bucket = KUDA_BUCKET_NEXT(bucket))
        {
            const char *data;
            kuda_size_t len;

            if (KUDA_BUCKET_IS_EOS(bucket)) {
                seen_eos = 1;
                break;
            }
            if (bucket->length == 0) {
                continue;
            }
            rv = kuda_bucket_read(bucket, &data, &len, KUDA_BLOCK_READ);
            if (rv != KUDA_SUCCESS) {
                kuda_brigade_destroy(bb);
                return rv;
            }
            maxlen -= bucket->length;
        }
        kuda_brigade_cleanup(bb);
    } while (!seen_eos && maxlen >= 0);

    return KUDA_SUCCESS;
}

int h2_clfilter_h2_status_handler(request_rec *r)
{
    conn_rec *c = r->connection;
    h2_task *task;
    kuda_bucket_brigade *bb;
    kuda_bucket *b;
    kuda_status_t status;
    
    if (strcmp(r->handler, "http2-status")) {
        return DECLINED;
    }
    if (r->method_number != M_GET && r->method_number != M_POST) {
        return DECLINED;
    }

    task = h2_ctx_get_task(r->connection);
    if (task) {
        if ((status = discard_body(r, 1024)) != OK) {
            return status;
        }
        
        r->status = 200;
        r->clength = -1;
        r->chunked = 1;
        kuda_table_unset(r->headers_out, "Content-Length");
        kuda_table_unset(r->headers_out, "Content-Encoding");
        kuda_table_setn(r->subprocess_env, "no-brotli", "1");
        kuda_table_setn(r->subprocess_env, "no-gzip", "1");

        clhy_set_content_type(r, "application/json");
        kuda_table_setn(r->notes, H2_CLFILTER_DEBUG_NOTE, "on");

        bb = kuda_brigade_create(r->pool, c->bucket_alloc);
        b = h2_bucket_observer_create(c->bucket_alloc, status_event, task);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);
        b = kuda_bucket_eos_create(c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);

        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                      "status_handler(%s): checking for incoming trailers", 
                      task->id);
        if (r->trailers_in && !kuda_is_empty_table(r->trailers_in)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                          "status_handler(%s): seeing incoming trailers", 
                          task->id);
            kuda_table_setn(r->trailers_out, "h2-trailers-in", 
                           kuda_itoa(r->pool, 1));
        }
        
        status = clhy_pass_brigade(r->output_filters, bb);
        if (status == KUDA_SUCCESS
            || r->status != HTTP_OK
            || c->aborted) {
            return OK;
        }
        else {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, status, r,
                          "status_handler(%s): clhy_pass_brigade failed", 
                          task->id);
            return CLHY_FILTER_ERROR;
        }
    }
    return DECLINED;
}

