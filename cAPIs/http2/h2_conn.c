/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <kuda_strings.h>

#include <clhy_core.h>
#include <clhy_capimn.h>

#include <wwhy.h>
#include <http_core.h>
#include <http_config.h>
#include <http_log.h>
#include <http_connection.h>
#include <http_protocol.h>
#include <http_request.h>

#include <core_common.h>

#include "h2_private.h"
#include "h2.h"
#include "h2_config.h"
#include "h2_ctx.h"
#include "h2_clfilter.h"
#include "h2_mplx.h"
#include "h2_session.h"
#include "h2_stream.h"
#include "h2_h2.h"
#include "h2_task.h"
#include "h2_workers.h"
#include "h2_conn.h"
#include "h2_version.h"

static struct h2_workers *workers;

static h2_clmp_type_t clmp_type = H2_CLMP_UNKNOWN;
static cAPI *clmp_capi;
static int async_clmp;
static int clmp_supported = 1;
static kuda_socket_t *dummy_socket;

static void check_capis(int force)
{
    static int checked = 0;
    int i;

    if (force || !checked) {
        for (i = 0; clhy_activated_capis[i]; ++i) {
            cAPI *m = clhy_activated_capis[i];

            if (!strcmp("event.c", m->name)) {
                clmp_type = H2_CLMP_EVENT;
                clmp_capi = m;
                break;
            }
            else if (!strcmp("motorz.c", m->name)) {
                clmp_type = H2_CLMP_MOTORZ;
                clmp_capi = m;
                break;
            }
            else if (!strcmp("core_netware.c", m->name)) {
                clmp_type = H2_CLMP_NETWARE;
                clmp_capi = m;
                break;
            }
            else if (!strcmp("prefork.c", m->name)) {
                clmp_type = H2_CLMP_PREFORK;
                clmp_capi = m;
                /* While http2 can work really well on prefork, it collides
                 * today's use case for prefork: runnning single-thread app engines.
                 * If we restrict h2_workers to 1 per process, this single-thread app
                 * engines will work fine, but browser will be limited to 1 active
                 * request at a time. */
                clmp_supported = 0;
                break;
            }
            else if (!strcmp("simple_api.c", m->name)) {
                clmp_type = H2_CLMP_SIMPLE;
                clmp_capi = m;
                clmp_supported = 0;
                break;
            }
            else if (!strcmp("core_winnt.c", m->name)) {
                clmp_type = H2_CLMP_WINNT;
                clmp_capi = m;
                break;
            }
            else if (!strcmp("worker.c", m->name)) {
                clmp_type = H2_CLMP_WORKER;
                clmp_capi = m;
                break;
            }
        }
        checked = 1;
    }
}

kuda_status_t h2_conn_child_init(kuda_pool_t *pool, server_rec *s)
{
    kuda_status_t status = KUDA_SUCCESS;
    int minw, maxw;
    int max_threads_per_child = 0;
    int idle_secs = 0;

    check_capis(1);
    clhy_clmp_query(CLHY_CLMPQ_MAX_THREADS, &max_threads_per_child);

    status = clhy_clmp_query(CLHY_CLMPQ_IS_ASYNC, &async_clmp);
    if (status != KUDA_SUCCESS) {
        /* some CLMPs do not implemnent this */
        async_clmp = 0;
        status = KUDA_SUCCESS;
    }

    h2_config_init(pool);

    h2_get_num_workers(s, &minw, &maxw);

    idle_secs = h2_config_sgeti(s, H2_CONF_MAX_WORKER_IDLE_SECS);
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE3, 0, s,
                 "h2_workers: min=%d max=%d, mthrpchild=%d, idle_secs=%d",
                 minw, maxw, max_threads_per_child, idle_secs);
    workers = h2_workers_create(s, pool, minw, maxw, idle_secs);

    clhy_register_input_filter("H2_IN", h2_clfilter_core_input,
                             NULL, CLHY_FTYPE_CONNECTION);

    status = h2_mplx_child_init(pool, s);

    if (status == KUDA_SUCCESS) {
        status = kuda_socket_create(&dummy_socket, KUDA_INET, SOCK_STREAM,
                                   KUDA_PROTO_TCP, pool);
    }

    return status;
}

h2_clmp_type_t h2_conn_clmp_type(void)
{
    check_capis(0);
    return clmp_type;
}

const char *h2_conn_clmp_name(void)
{
    check_capis(0);
    return clmp_capi? clmp_capi->name : "unknown";
}

int h2_clmp_supported(void)
{
    check_capis(0);
    return clmp_supported;
}

static cAPI *h2_conn_clmp_capi(void)
{
    check_capis(0);
    return clmp_capi;
}

kuda_status_t h2_conn_setup(conn_rec *c, request_rec *r, server_rec *s)
{
    h2_session *session;
    h2_ctx *ctx;
    kuda_status_t status;

    if (!workers) {
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, c, CLHYLOGNO(02911)
                      "workers not initialized");
        return KUDA_EGENERAL;
    }

    if (KUDA_SUCCESS == (status = h2_session_create(&session, c, r, s, workers))) {
        ctx = h2_ctx_get(c, 1);
        h2_ctx_session_set(ctx, session);
    }

    return status;
}

kuda_status_t h2_conn_run(conn_rec *c)
{
    kuda_status_t status;
    int clmp_state = 0;
    h2_session *session = h2_ctx_get_session(c);

    clhy_assert(session);
    do {
        if (c->cs) {
            c->cs->sense = CONN_SENSE_DEFAULT;
            c->cs->state = CONN_STATE_HANDLER;
        }

        status = h2_session_process(session, async_clmp);

        if (KUDA_STATUS_IS_EOF(status)) {
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, status, c,
                          H2_SSSN_LOG(CLHYLOGNO(03045), session,
                          "process, closing conn"));
            c->keepalive = CLHY_CONN_CLOSE;
        }
        else {
            c->keepalive = CLHY_CONN_KEEPALIVE;
        }

        if (clhy_clmp_query(CLHY_CLMPQ_CLMP_STATE, &clmp_state)) {
            break;
        }
    } while (!async_clmp
             && c->keepalive == CLHY_CONN_KEEPALIVE
             && clmp_state != CLHY_CLMPQ_STOPPING);

    if (c->cs) {
        switch (session->state) {
            case H2_SESSION_ST_INIT:
            case H2_SESSION_ST_IDLE:
            case H2_SESSION_ST_BUSY:
            case H2_SESSION_ST_WAIT:
                c->cs->state = CONN_STATE_WRITE_COMPLETION;
                if (c->cs && (session->open_streams || !session->remote.emitted_count)) {
                    /* let the CLMP know that we are not done and want
                     * the Timeout behaviour instead of a KeepAliveTimeout
                     * See PR 63534.
                     */
                    c->cs->sense = CONN_SENSE_WANT_READ;
                }
                break;
            case H2_SESSION_ST_CLEANUP:
            case H2_SESSION_ST_DONE:
            default:
                c->cs->state = CONN_STATE_LINGER;
            break;
        }
    }

    return KUDA_SUCCESS;
}

kuda_status_t h2_conn_pre_close(struct h2_ctx *ctx, conn_rec *c)
{
    h2_session *session = h2_ctx_get_session(c);
    if (session) {
        kuda_status_t status = h2_session_pre_close(session, async_clmp);
        return (status == KUDA_SUCCESS)? DONE : status;
    }
    return DONE;
}

conn_rec *h2_slave_create(conn_rec *master, int slave_id, kuda_pool_t *parent)
{
    kuda_allocator_t *allocator;
    kuda_status_t status;
    kuda_pool_t *pool;
    conn_rec *c;
    void *cfg;
    cAPI *clmp;

    clhy_assert(master);
    clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE3, 0, master,
                  "h2_stream(%ld-%d): create slave", master->id, slave_id);

    /* We create a pool with its own allocator to be used for
     * processing a request. This is the only way to have the processing
     * independant of its parent pool in the sense that it can work in
     * another thread. Also, the new allocator needs its own mutex to
     * synchronize sub-pools.
     */
    kuda_allocator_create(&allocator);
    kuda_allocator_max_free_set(allocator, clhy_max_mem_free);
    status = kuda_pool_create_ex(&pool, parent, NULL, allocator);
    if (status != KUDA_SUCCESS) {
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_ERR, status, master,
                      CLHYLOGNO(10004) "h2_session(%ld-%d): create slave pool",
                      master->id, slave_id);
        return NULL;
    }
    kuda_allocator_owner_set(allocator, pool);
    kuda_pool_tag(pool, "h2_slave_conn");

    c = (conn_rec *) kuda_palloc(pool, sizeof(conn_rec));
    if (c == NULL) {
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_ERR, KUDA_ENOMEM, master,
                      CLHYLOGNO(02913) "h2_session(%ld-%d): create slave",
                      master->id, slave_id);
        kuda_pool_destroy(pool);
        return NULL;
    }

    memcpy(c, master, sizeof(conn_rec));

    c->master                 = master;
    c->pool                   = pool;
    c->conn_config            = clhy_create_conn_config(pool);
    c->notes                  = kuda_table_make(pool, 5);
    c->input_filters          = NULL;
    c->output_filters         = NULL;
    c->keepalives             = 0;
/* #if CLHY_CAPI_MAGIC_AT_LEAST(20191101, 7)
    c->filter_conn_ctx        = NULL;
#endif */
    c->bucket_alloc           = kuda_bucket_alloc_create(pool);
#if !CLHY_CAPI_MAGIC_AT_LEAST(20191101, 6)
     c->data_in_input_filters  = 0;
     c->data_in_output_filters = 0;
#endif
    /* prevent clmp_event from making wrong assumptions about this connection,
     * like e.g. using its socket for an async read check. */
    c->clogging_input_filters = 1;
    c->log                    = NULL;
    c->log_id                 = kuda_psprintf(pool, "%ld-%d",
                                             master->id, slave_id);
    c->aborted                = 0;
    /* We cannot install the master connection socket on the slaves, as
     * cAPIs mess with timeouts/blocking of the socket, with
     * unwanted side effects to the master connection processing.
     * Fortunately, since we never use the slave socket, we can just install
     * a single, process-wide dummy and everyone is happy.
     */
    clhy_set_capi_config(c->conn_config, &core_capi, dummy_socket);
    /* TODO: these should be unique to this thread */
    c->sbh                    = master->sbh;
    /* TODO: not all cLMP have learned about slave connections yet.
     * copy their config from master to slave.
     */
    if ((clmp = h2_conn_clmp_capi()) != NULL) {
        cfg = clhy_get_capi_config(master->conn_config, clmp);
        clhy_set_capi_config(c->conn_config, clmp, cfg);
    }

    clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE3, 0, c,
                  "h2_slave(%s): created", c->log_id);
    return c;
}

void h2_slave_destroy(conn_rec *slave)
{
    clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE3, 0, slave,
                  "h2_slave(%s): destroy", slave->log_id);
    slave->sbh = NULL;
    kuda_pool_destroy(slave->pool);
}

kuda_status_t h2_slave_run_pre_connection(conn_rec *slave, kuda_socket_t *csd)
{
    if (slave->keepalives == 0) {
        /* Simulate that we had already a request on this connection. Some
         * hooks trigger special behaviour when keepalives is 0.
         * (Not necessarily in pre_connection, but later. Set it here, so it
         * is in place.) */
        slave->keepalives = 1;
        /* We signal that this connection will be closed after the request.
         * Which is true in that sense that we throw away all traffic data
         * on this slave connection after each requests. Although we might
         * reuse internal structures like memory pools.
         * The wanted effect of this is that wwhy does not try to clean up
         * any dangling data on this connection when a request is done. Which
         * is unneccessary on a h2 stream.
         */
        slave->keepalive = CLHY_CONN_CLOSE;
        return clhy_run_pre_connection(slave, csd);
    }
    clhy_assert(slave->output_filters);
    return KUDA_SUCCESS;
}
