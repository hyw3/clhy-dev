/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <kuda_optional.h>
#include <kuda_optional_hooks.h>
#include <kuda_strings.h>
#include <kuda_time.h>
#include <kuda_want.h>

#include <wwhy.h>
#include <http_protocol.h>
#include <http_request.h>
#include <http_log.h>

#include "capi_http2.h"

#include <nghttp2/nghttp2.h>
#include "h2_stream.h"
#include "h2_alt_svc.h"
#include "h2_conn.h"
#include "h2_clfilter.h"
#include "h2_task.h"
#include "h2_session.h"
#include "h2_config.h"
#include "h2_ctx.h"
#include "h2_h2.h"
#include "h2_mplx.h"
#include "h2_push.h"
#include "h2_request.h"
#include "h2_switch.h"
#include "h2_version.h"


static int h2_h2_fixups(request_rec *r);

typedef struct {
    unsigned int change_prio : 1;
    unsigned int sha256 : 1;
    unsigned int inv_headers : 1;
    unsigned int dyn_windows : 1;
} features;

static features myfeats;
static int clmp_warned;

/* The cAPI initialization. Called once as clhy hook, before any multi
 * processing (threaded or not) happens. It is typically at least called twice, 
 * see
 * http://clhy.hyang.org/docs/capi/extension/capi_http2.html
 * Since the first run is just a "practise" run, we want to initialize for real
 * only on the second try. This defeats the purpose of the first dry run a bit, 
 * since clhy wants to verify that a new configuration actually will work. 
 * So if we have trouble with the configuration, this will only be detected 
 * when the server has already switched.
 * On the other hand, when we initialize lib nghttp2, all possible crazy things 
 * might happen and this might even eat threads. So, better init on the real 
 * invocation, for now at least.
 */
static int h2_post_config(kuda_pool_t *p, kuda_pool_t *plog,
                          kuda_pool_t *ptemp, server_rec *s)
{
    void *data = NULL;
    const char *capi_h2_init_key = "capi_http2_init_counter";
    nghttp2_info *ngh2;
    kuda_status_t status;
    
    (void)plog;(void)ptemp;
#ifdef H2_NG2_CHANGE_PRIO
    myfeats.change_prio = 1;
#endif
#ifdef H2_OPENSSL
    myfeats.sha256 = 1;
#endif
#ifdef H2_NG2_INVALID_HEADER_CB
    myfeats.inv_headers = 1;
#endif
#ifdef H2_NG2_LOCAL_WIN_SIZE
    myfeats.dyn_windows = 1;
#endif
    
    kuda_pool_userdata_get(&data, capi_h2_init_key, s->process->pool);
    if ( data == NULL ) {
        clhy_log_error( CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(03089)
                     "initializing post config dry run");
        kuda_pool_userdata_set((const void *)1, capi_h2_init_key,
                              kuda_pool_cleanup_null, s->process->pool);
        return KUDA_SUCCESS;
    }
    
    ngh2 = nghttp2_version(0);
    clhy_log_error( CLHYLOG_MARK, CLHYLOG_INFO, 0, s, CLHYLOGNO(03090)
                 "capi_http2 (v%s, feats=%s%s%s, nghttp2 %s), initializing...",
                 CAPI_HTTP2_VERSION, 
                 myfeats.change_prio? "CHPRIO"  : "", 
                 myfeats.sha256?      "+SHA256" : "",
                 myfeats.inv_headers? "+INVHD"  : "",
                 myfeats.dyn_windows? "+DWINS"  : "",
                 ngh2?                ngh2->version_str : "unknown");
    
    switch (h2_conn_clmp_type()) {
        case H2_CLMP_SIMPLE:
        case H2_CLMP_MOTORZ:
        case H2_CLMP_NETWARE:
        case H2_CLMP_WINNT:
            /* not sure we need something extra for those. */
            break;
        case H2_CLMP_EVENT:
        case H2_CLMP_WORKER:
            /* all fine, we know these ones */
            break;
        case H2_CLMP_PREFORK:
            /* ok, we now know how to handle that one */
            break;
        case H2_CLMP_UNKNOWN:
            /* ??? */
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(03091)
                         "post_config: cLHy core API (cLMP) type unknown");
            break;
    }

    if (!h2_clmp_supported() && !clmp_warned) {
        clmp_warned = 1;
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(10034)
                     "cLHy core API (cLMP) (%s) is not supported by capi_http2. The cLMP determines "
                     "how things are processed in your server. HTTP/2 has more demands in "
                     "this regard and the currently selected cLMP will just not do. "
                     "This is an advisory warning. Your server will continue to work, but "
                     "the HTTP/2 protocol will be inactive.", 
                     h2_conn_clmp_name());
    }
    
    status = h2_h2_init(p, s);
    if (status == KUDA_SUCCESS) {
        status = h2_switch_init(p, s);
    }
    if (status == KUDA_SUCCESS) {
        status = h2_task_init(p, s);
    }
    
    return status;
}

static char *http2_var_lookup(kuda_pool_t *, server_rec *,
                         conn_rec *, request_rec *, char *name);
static int http2_is_h2(conn_rec *);

static void http2_get_num_workers(server_rec *s, int *minw, int *maxw)
{
    h2_get_num_workers(s, minw, maxw);
}

/* Runs once per created child process. Perform any process 
 * related initionalization here.
 */
static void h2_child_init(kuda_pool_t *pool, server_rec *s)
{
    /* Set up our connection processing */
    kuda_status_t status = h2_conn_child_init(pool, s);
    if (status != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, status, s,
                     CLHYLOGNO(02949) "initializing connection handling");
    }
    
}

/* Install this cAPI into the clhydelman infrastructure.
 */
static void h2_hooks(kuda_pool_t *pool)
{
    static const char *const capi_ssl[] = { "capi_ssl.c", NULL};
    
    KUDA_REGISTER_OPTIONAL_FN(http2_is_h2);
    KUDA_REGISTER_OPTIONAL_FN(http2_var_lookup);
    KUDA_REGISTER_OPTIONAL_FN(http2_get_num_workers);

    clhy_log_perror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, pool, "installing hooks");
    
    /* Run once after configuration is set, but before cLMP children initialize. */
    clhy_hook_post_config(h2_post_config, capi_ssl, NULL, KUDA_HOOK_MIDDLE);
    
    /* Run once after a child process has been created.
     */
    clhy_hook_child_init(h2_child_init, NULL, NULL, KUDA_HOOK_MIDDLE);

    h2_h2_register_hooks();
    h2_switch_register_hooks();
    h2_task_register_hooks();

    h2_alt_svc_register_hooks();
    
    /* Setup subprocess env for certain variables 
     */
    clhy_hook_fixups(h2_h2_fixups, NULL,NULL, KUDA_HOOK_MIDDLE);
    
    /* test http2 connection status handler */
    clhy_hook_handler(h2_clfilter_h2_status_handler, NULL, NULL, KUDA_HOOK_MIDDLE);
}

static const char *val_HTTP2(kuda_pool_t *p, server_rec *s,
                             conn_rec *c, request_rec *r, h2_ctx *ctx)
{
    return ctx? "on" : "off";
}

static const char *val_H2_PUSH(kuda_pool_t *p, server_rec *s,
                               conn_rec *c, request_rec *r, h2_ctx *ctx)
{
    if (ctx) {
        if (r) {
            if (ctx->task) {
                h2_stream *stream = h2_mplx_stream_get(ctx->task->mplx, ctx->task->stream_id);
                if (stream && stream->push_policy != H2_PUSH_NONE) {
                    return "on";
                }
            }
        }
        else if (c && h2_session_push_enabled(ctx->session)) {
            return "on";
        }
    }
    else if (s) {
        if (h2_config_geti(r, s, H2_CONF_PUSH)) {
            return "on";
        }
    }
    return "off";
}

static const char *val_H2_PUSHED(kuda_pool_t *p, server_rec *s,
                                 conn_rec *c, request_rec *r, h2_ctx *ctx)
{
    if (ctx) {
        if (ctx->task && !H2_STREAM_CLIENT_INITIATED(ctx->task->stream_id)) {
            return "PUSHED";
        }
    }
    return "";
}

static const char *val_H2_PUSHED_ON(kuda_pool_t *p, server_rec *s,
                                    conn_rec *c, request_rec *r, h2_ctx *ctx)
{
    if (ctx) {
        if (ctx->task && !H2_STREAM_CLIENT_INITIATED(ctx->task->stream_id)) {
            h2_stream *stream = h2_mplx_stream_get(ctx->task->mplx, ctx->task->stream_id);
            if (stream) {
                return kuda_itoa(p, stream->initiated_on);
            }
        }
    }
    return "";
}

static const char *val_H2_STREAM_TAG(kuda_pool_t *p, server_rec *s,
                                     conn_rec *c, request_rec *r, h2_ctx *ctx)
{
    if (ctx) {
        if (ctx->task) {
            return ctx->task->id;
        }
    }
    return "";
}

static const char *val_H2_STREAM_ID(kuda_pool_t *p, server_rec *s,
                                    conn_rec *c, request_rec *r, h2_ctx *ctx)
{
    const char *cp = val_H2_STREAM_TAG(p, s, c, r, ctx);
    if (cp && (cp = clhy_strchr_c(cp, '-'))) {
        return ++cp;
    }
    return NULL;
}

typedef const char *h2_var_lookup(kuda_pool_t *p, server_rec *s,
                                  conn_rec *c, request_rec *r, h2_ctx *ctx);
typedef struct h2_var_def {
    const char *name;
    h2_var_lookup *lookup;
    unsigned int  subprocess : 1;    /* should be set in r->subprocess_env */
} h2_var_def;

static h2_var_def H2_VARS[] = {
    { "HTTP2",               val_HTTP2,  1 },
    { "H2PUSH",              val_H2_PUSH, 1 },
    { "H2_PUSH",             val_H2_PUSH, 1 },
    { "H2_PUSHED",           val_H2_PUSHED, 1 },
    { "H2_PUSHED_ON",        val_H2_PUSHED_ON, 1 },
    { "H2_STREAM_ID",        val_H2_STREAM_ID, 1 },
    { "H2_STREAM_TAG",       val_H2_STREAM_TAG, 1 },
};

#ifndef H2_ALEN
#define H2_ALEN(a)          (sizeof(a)/sizeof((a)[0]))
#endif


static int http2_is_h2(conn_rec *c)
{
    return h2_ctx_get(c->master? c->master : c, 0) != NULL;
}

static char *http2_var_lookup(kuda_pool_t *p, server_rec *s,
                              conn_rec *c, request_rec *r, char *name)
{
    int i;
    /* If the # of vars grow, we need to put definitions in a hash */
    for (i = 0; i < H2_ALEN(H2_VARS); ++i) {
        h2_var_def *vdef = &H2_VARS[i];
        if (!strcmp(vdef->name, name)) {
            h2_ctx *ctx = (r? h2_ctx_get(c, 0) : 
                           h2_ctx_get(c->master? c->master : c, 0));
            return (char *)vdef->lookup(p, s, c, r, ctx);
        }
    }
    return (char*)"";
}

static int h2_h2_fixups(request_rec *r)
{
    if (r->connection->master) {
        h2_ctx *ctx = h2_ctx_get(r->connection, 0);
        int i;
        
        for (i = 0; ctx && i < H2_ALEN(H2_VARS); ++i) {
            h2_var_def *vdef = &H2_VARS[i];
            if (vdef->subprocess) {
                kuda_table_setn(r->subprocess_env, vdef->name, 
                               vdef->lookup(r->pool, r->server, r->connection, 
                                            r, ctx));
            }
        }
    }
    return DECLINED;
}
