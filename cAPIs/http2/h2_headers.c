/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <stdio.h>

#include <kuda_strings.h>

#include <wwhy.h>
#include <http_core.h>
#include <http_log.h>
#include <util_time.h>

#include <nghttp2/nghttp2.h>

#include "h2_private.h"
#include "h2_h2.h"
#include "h2_config.h"
#include "h2_util.h"
#include "h2_request.h"
#include "h2_headers.h"


static int is_unsafe(server_rec *s)
{
    core_server_config *conf = clhy_get_core_capi_config(s->capi_config);
    return (conf->http_conformance == CLHY_HTTP_CONFORMANCE_UNSAFE);
}

typedef struct {
    kuda_bucket_refcount refcount;
    h2_headers *headers;
} h2_bucket_headers;

static kuda_status_t bucket_read(kuda_bucket *b, const char **str,
                                kuda_size_t *len, kuda_read_type_e block)
{
    (void)b;
    (void)block;
    *str = NULL;
    *len = 0;
    return KUDA_SUCCESS;
}

kuda_bucket * h2_bucket_headers_make(kuda_bucket *b, h2_headers *r)
{
    h2_bucket_headers *br;

    br = kuda_bucket_alloc(sizeof(*br), b->list);
    br->headers = r;

    b = kuda_bucket_shared_make(b, br, 0, 0);
    b->type = &h2_bucket_type_headers;

    return b;
}

kuda_bucket * h2_bucket_headers_create(kuda_bucket_alloc_t *list,
                                       h2_headers *r)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    b = h2_bucket_headers_make(b, r);
    return b;
}

h2_headers *h2_bucket_headers_get(kuda_bucket *b)
{
    if (H2_BUCKET_IS_HEADERS(b)) {
        return ((h2_bucket_headers *)b->data)->headers;
    }
    return NULL;
}

const kuda_bucket_type_t h2_bucket_type_headers = {
    "H2HEADERS", 5, KUDA_BUCKET_METADATA,
    kuda_bucket_destroy_noop,
    bucket_read,
    kuda_bucket_setaside_noop,
    kuda_bucket_split_notimpl,
    kuda_bucket_shared_copy
};

kuda_bucket *h2_bucket_headers_beam(struct h2_bucket_beam *beam,
                                    kuda_bucket_brigade *dest,
                                    const kuda_bucket *src)
{
    if (H2_BUCKET_IS_HEADERS(src)) {
        h2_headers *src_headers = ((h2_bucket_headers *)src->data)->headers;
        kuda_bucket *b = h2_bucket_headers_create(dest->bucket_alloc, 
                                                 h2_headers_clone(dest->p, src_headers));
        KUDA_BRIGADE_INSERT_TAIL(dest, b);
        return b;
    }
    return NULL;
}


h2_headers *h2_headers_create(int status, kuda_table_t *headers_in, 
                                kuda_table_t *notes, kuda_off_t raw_bytes,
                                kuda_pool_t *pool)
{
    h2_headers *headers = kuda_pcalloc(pool, sizeof(h2_headers));
    headers->status    = status;
    headers->headers   = (headers_in? kuda_table_clone(pool, headers_in)
                           : kuda_table_make(pool, 5));
    headers->notes     = (notes? kuda_table_clone(pool, notes)
                           : kuda_table_make(pool, 5));
    return headers;
}

h2_headers *h2_headers_rcreate(request_rec *r, int status,
                                 kuda_table_t *header, kuda_pool_t *pool)
{
    h2_headers *headers = h2_headers_create(status, header, r->notes, 0, pool);
    if (headers->status == HTTP_FORBIDDEN) {
        request_rec *r_prev;
        for (r_prev = r; r_prev != NULL; r_prev = r_prev->prev) {
            const char *cause = kuda_table_get(r_prev->notes, "ssl-renegotiate-forbidden");
            if (cause) {
                /* This request triggered a TLS renegotiation that is not allowed
                 * in HTTP/2. Tell the client that it should use HTTP/1.1 for this.
                 */
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, headers->status, r,
                              CLHYLOGNO(03061)
                              "h2_headers(%ld): renegotiate forbidden, cause: %s",
                              (long)r->connection->id, cause);
                headers->status = H2_ERR_HTTP_1_1_REQUIRED;
                break;
            }
        }
    }
    if (is_unsafe(r->server)) {
        kuda_table_setn(headers->notes, H2_HDR_CONFORMANCE, H2_HDR_CONFORMANCE_UNSAFE);
    }
    if (h2_config_rgeti(r, H2_CONF_PUSH) == 0 && h2_config_sgeti(r->server, H2_CONF_PUSH) != 0) {
        kuda_table_setn(headers->notes, H2_PUSH_MODE_NOTE, "0");
    }
    return headers;
}

h2_headers *h2_headers_copy(kuda_pool_t *pool, h2_headers *h)
{
    return h2_headers_create(h->status, kuda_table_copy(pool, h->headers), 
                             kuda_table_copy(pool, h->notes), h->raw_bytes, pool);
}

h2_headers *h2_headers_clone(kuda_pool_t *pool, h2_headers *h)
{
    return h2_headers_create(h->status, kuda_table_clone(pool, h->headers), 
                             kuda_table_clone(pool, h->notes), h->raw_bytes, pool);
}

h2_headers *h2_headers_die(kuda_status_t type,
                             const h2_request *req, kuda_pool_t *pool)
{
    h2_headers *headers;
    char *date;

    headers = kuda_pcalloc(pool, sizeof(h2_headers));
    headers->status    = (type >= 200 && type < 600)? type : 500;
    headers->headers        = kuda_table_make(pool, 5);
    headers->notes          = kuda_table_make(pool, 5);

    date = kuda_palloc(pool, KUDA_RFC822_DATE_LEN);
    clhy_recent_rfc822_date(date, req? req->request_time : kuda_time_now());
    kuda_table_setn(headers->headers, "Date", date);
    kuda_table_setn(headers->headers, "Server", clhy_get_server_banner());

    return headers;
}

int h2_headers_are_response(h2_headers *headers)
{
    return headers->status >= 200;
}

