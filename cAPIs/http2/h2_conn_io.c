/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <kuda_strings.h>
#include <clhy_core.h>

#include <wwhy.h>
#include <http_core.h>
#include <http_log.h>
#include <http_connection.h>
#include <http_request.h>

#include "h2_private.h"
#include "h2_bucket_eos.h"
#include "h2_config.h"
#include "h2_conn_io.h"
#include "h2_h2.h"
#include "h2_session.h"
#include "h2_util.h"

#define TLS_DATA_MAX          (16*1024) 

/* Calculated like this: assuming MTU 1500 bytes
 * 1500 - 40 (IP) - 20 (TCP) - 40 (TCP options) 
 *      - TLS overhead (60-100) 
 * ~= 1300 bytes */
#define WRITE_SIZE_INITIAL    1300

/* The maximum we'd like to write in one chunk is
 * the max size of a TLS record. When pushing
 * many frames down the h2 connection, this might
 * align differently because of headers and other
 * frames or simply as not sufficient data is
 * in a response body.
 * However keeping frames at or below this limit
 * should make optimizations at the layer that writes
 * to TLS easier.
 */
#define WRITE_SIZE_MAX        (TLS_DATA_MAX) 


static void h2_conn_io_bb_log(conn_rec *c, int stream_id, int level, 
                              const char *tag, kuda_bucket_brigade *bb)
{
    char buffer[16 * 1024];
    const char *line = "(null)";
    kuda_size_t bmax = sizeof(buffer)/sizeof(buffer[0]);
    int off = 0;
    kuda_bucket *b;
    
    if (bb) {
        memset(buffer, 0, bmax--);
        for (b = KUDA_BRIGADE_FIRST(bb); 
             bmax && (b != KUDA_BRIGADE_SENTINEL(bb));
             b = KUDA_BUCKET_NEXT(b)) {
            
            if (KUDA_BUCKET_IS_METADATA(b)) {
                if (KUDA_BUCKET_IS_EOS(b)) {
                    off += kuda_snprintf(buffer+off, bmax-off, "eos ");
                }
                else if (KUDA_BUCKET_IS_FLUSH(b)) {
                    off += kuda_snprintf(buffer+off, bmax-off, "flush ");
                }
                else if (CLHY_BUCKET_IS_EOR(b)) {
                    off += kuda_snprintf(buffer+off, bmax-off, "eor ");
                }
                else if (H2_BUCKET_IS_H2EOS(b)) {
                    off += kuda_snprintf(buffer+off, bmax-off, "h2eos ");
                }
                else {
                    off += kuda_snprintf(buffer+off, bmax-off, "meta(unknown) ");
                }
            }
            else {
                const char *btype = "data";
                if (KUDA_BUCKET_IS_FILE(b)) {
                    btype = "file";
                }
                else if (KUDA_BUCKET_IS_PIPE(b)) {
                    btype = "pipe";
                }
                else if (KUDA_BUCKET_IS_SOCKET(b)) {
                    btype = "socket";
                }
                else if (KUDA_BUCKET_IS_HEAP(b)) {
                    btype = "heap";
                }
                else if (KUDA_BUCKET_IS_TRANSIENT(b)) {
                    btype = "transient";
                }
                else if (KUDA_BUCKET_IS_IMMORTAL(b)) {
                    btype = "immortal";
                }
#if KUDA_HAS_MMAP
                else if (KUDA_BUCKET_IS_MMAP(b)) {
                    btype = "mmap";
                }
#endif
                else if (KUDA_BUCKET_IS_POOL(b)) {
                    btype = "pool";
                }
                
                off += kuda_snprintf(buffer+off, bmax-off, "%s[%ld] ", 
                                    btype, 
                                    (long)(b->length == ((kuda_size_t)-1)? 
                                           -1 : b->length));
            }
        }
        line = *buffer? buffer : "(empty)";
    }
    /* Intentional no CLHYLOGNO */
    clhy_log_cerror(CLHYLOG_MARK, level, 0, c, "h2_session(%ld)-%s: %s", 
                  c->id, tag, line);

}

kuda_status_t h2_conn_io_init(h2_conn_io *io, conn_rec *c, server_rec *s)
{
    io->c              = c;
    io->output         = kuda_brigade_create(c->pool, c->bucket_alloc);
    io->is_tls         = h2_h2_is_tls(c);
    io->buffer_output  = io->is_tls;
    io->flush_threshold = (kuda_size_t)h2_config_sgeti64(s, H2_CONF_STREAM_MAX_MEM);

    if (io->is_tls) {
        /* This is what we start with
         * 
         */
        io->warmup_size    = h2_config_sgeti64(s, H2_CONF_TLS_WARMUP_SIZE);
        io->cooldown_usecs = (h2_config_sgeti(s, H2_CONF_TLS_COOLDOWN_SECS) 
                              * KUDA_USEC_PER_SEC);
        io->write_size     = (io->cooldown_usecs > 0? 
                              WRITE_SIZE_INITIAL : WRITE_SIZE_MAX); 
    }
    else {
        io->warmup_size    = 0;
        io->cooldown_usecs = 0;
        io->write_size     = 0;
    }

    if (CLHYLOGctrace1(c)) {
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE4, 0, io->c,
                      "h2_conn_io(%ld): init, buffering=%d, warmup_size=%ld, "
                      "cd_secs=%f", io->c->id, io->buffer_output, 
                      (long)io->warmup_size,
                      ((float)io->cooldown_usecs/KUDA_USEC_PER_SEC));
    }

    return KUDA_SUCCESS;
}

static void append_scratch(h2_conn_io *io) 
{
    if (io->scratch && io->slen > 0) {
        kuda_bucket *b = kuda_bucket_heap_create(io->scratch, io->slen,
                                               kuda_bucket_free,
                                               io->c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(io->output, b);
        io->scratch = NULL;
        io->slen = io->ssize = 0;
    }
}

static kuda_size_t assure_scratch_space(h2_conn_io *io) {
    kuda_size_t remain = io->ssize - io->slen; 
    if (io->scratch && remain == 0) {
        append_scratch(io);
    }
    if (!io->scratch) {
        /* we control the size and it is larger than what buckets usually
         * allocate. */
        io->scratch = kuda_bucket_alloc(io->write_size, io->c->bucket_alloc);
        io->ssize = io->write_size;
        io->slen = 0;
        remain = io->ssize;
    }
    return remain;
}
    
static kuda_status_t read_to_scratch(h2_conn_io *io, kuda_bucket *b)
{
    kuda_status_t status;
    const char *data;
    kuda_size_t len;
    
    if (!b->length) {
        return KUDA_SUCCESS;
    }
    
    clhy_assert(b->length <= (io->ssize - io->slen));
    if (KUDA_BUCKET_IS_FILE(b)) {
        kuda_bucket_file *f = (kuda_bucket_file *)b->data;
        kuda_file_t *fd = f->fd;
        kuda_off_t offset = b->start;
        kuda_size_t len = b->length;
        
        /* file buckets will either mmap (which we do not want) or
         * read 8000 byte chunks and split themself. However, we do
         * know *exactly* how many bytes we need where.
         */
        status = kuda_file_seek(fd, KUDA_SET, &offset);
        if (status != KUDA_SUCCESS) {
            return status;
        }
        status = kuda_file_read(fd, io->scratch + io->slen, &len);
        if (status != KUDA_SUCCESS && status != KUDA_EOF) {
            return status;
        }
        io->slen += len;
    }
    else {
        status = kuda_bucket_read(b, &data, &len, KUDA_BLOCK_READ);
        if (status == KUDA_SUCCESS) {
            memcpy(io->scratch+io->slen, data, len);
            io->slen += len;
        }
    }
    return status;
}

static void check_write_size(h2_conn_io *io) 
{
    if (io->write_size > WRITE_SIZE_INITIAL 
        && (io->cooldown_usecs > 0)
        && (kuda_time_now() - io->last_write) >= io->cooldown_usecs) {
        /* long time not written, reset write size */
        io->write_size = WRITE_SIZE_INITIAL;
        io->bytes_written = 0;
    }
    else if (io->write_size < WRITE_SIZE_MAX 
             && io->bytes_written >= io->warmup_size) {
        /* connection is hot, use max size */
        io->write_size = WRITE_SIZE_MAX;
    }
}

static kuda_status_t pass_output(h2_conn_io *io, int flush)
{
    conn_rec *c = io->c;
    kuda_bucket_brigade *bb = io->output;
    kuda_bucket *b;
    kuda_off_t bblen;
    kuda_status_t status;
    
    append_scratch(io);
    if (flush && !io->is_flushed) {
        b = kuda_bucket_flush_create(c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);
    }
    
    if (KUDA_BRIGADE_EMPTY(bb)) {
        return KUDA_SUCCESS;
    }
    
    clhy_update_child_status(c->sbh, SERVER_BUSY_WRITE, NULL);
    kuda_brigade_length(bb, 0, &bblen);
    h2_conn_io_bb_log(c, 0, CLHYLOG_TRACE2, "out", bb);
    
    status = clhy_pass_brigade(c->output_filters, bb);
    if (status == KUDA_SUCCESS) {
        io->bytes_written += (kuda_size_t)bblen;
        io->last_write = kuda_time_now();
        if (flush) {
            io->is_flushed = 1;
        }
    }
    kuda_brigade_cleanup(bb);

    if (status != KUDA_SUCCESS) {
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, status, c, CLHYLOGNO(03044)
                      "h2_conn_io(%ld): pass_out brigade %ld bytes",
                      c->id, (long)bblen);
    }
    return status;
}

int h2_conn_io_needs_flush(h2_conn_io *io)
{
    if (!io->is_flushed) {
        kuda_off_t len = h2_brigade_mem_size(io->output);
        if (len > io->flush_threshold) {
            return 1;
        }
        /* if we do not exceed flush length due to memory limits,
         * we want at least flush when we have that amount of data. */
        kuda_brigade_length(io->output, 0, &len);
        return len > (4 * io->flush_threshold);
    }
    return 0;
}

kuda_status_t h2_conn_io_flush(h2_conn_io *io)
{
    kuda_status_t status;
    status = pass_output(io, 1);
    check_write_size(io);
    return status;
}

kuda_status_t h2_conn_io_write(h2_conn_io *io, const char *data, size_t length)
{
    kuda_status_t status = KUDA_SUCCESS;
    kuda_size_t remain;
    
    if (length > 0) {
        io->is_flushed = 0;
    }
    
    if (io->buffer_output) {
        while (length > 0) {
            remain = assure_scratch_space(io);
            if (remain >= length) {
                memcpy(io->scratch + io->slen, data, length);
                io->slen += length;
                length = 0;
            }
            else {
                memcpy(io->scratch + io->slen, data, remain);
                io->slen += remain;
                data += remain;
                length -= remain;
            }
        }
    }
    else {
        status = kuda_brigade_write(io->output, NULL, NULL, data, length);
    }
    return status;
}

kuda_status_t h2_conn_io_pass(h2_conn_io *io, kuda_bucket_brigade *bb)
{
    kuda_bucket *b;
    kuda_status_t status = KUDA_SUCCESS;
    
    if (!KUDA_BRIGADE_EMPTY(bb)) {
        io->is_flushed = 0;
    }

    while (!KUDA_BRIGADE_EMPTY(bb) && status == KUDA_SUCCESS) {
        b = KUDA_BRIGADE_FIRST(bb);
        
        if (KUDA_BUCKET_IS_METADATA(b)) {
            /* need to finish any open scratch bucket, as meta data 
             * needs to be forward "in order". */
            append_scratch(io);
            KUDA_BUCKET_REMOVE(b);
            KUDA_BRIGADE_INSERT_TAIL(io->output, b);
        }
        else if (io->buffer_output) {
            kuda_size_t remain = assure_scratch_space(io);
            if (b->length > remain) {
                kuda_bucket_split(b, remain);
                if (io->slen == 0) {
                    /* complete write_size bucket, append unchanged */
                    KUDA_BUCKET_REMOVE(b);
                    KUDA_BRIGADE_INSERT_TAIL(io->output, b);
                    continue;
                }
            }
            else {
                /* bucket fits in remain, copy to scratch */
                status = read_to_scratch(io, b);
                kuda_bucket_delete(b);
                continue;
            }
        }
        else {
            /* no buffering, forward buckets setaside on flush */
            if (KUDA_BUCKET_IS_TRANSIENT(b)) {
                kuda_bucket_setaside(b, io->c->pool);
            }
            KUDA_BUCKET_REMOVE(b);
            KUDA_BRIGADE_INSERT_TAIL(io->output, b);
        }
    }
    return status;
}


