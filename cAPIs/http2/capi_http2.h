/* The cLHy Server
 *
 * Copyright (C) 2015 greenbytes GmbH (https://www.greenbytes.de).
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __CAPI_HTTP2_H__
#define __CAPI_HTTP2_H__

/** The http2_var_lookup() optional function retrieves HTTP2 environment
 * variables. */
KUDA_DECLARE_OPTIONAL_FN(char *,
                        http2_var_lookup, (kuda_pool_t *, server_rec *,
                                           conn_rec *, request_rec *,  char *));

/** An optional function which returns non-zero if the given connection
 * or its master connection is using HTTP/2. */
KUDA_DECLARE_OPTIONAL_FN(int,
                        http2_is_h2, (conn_rec *));


/*******************************************************************************
 * START HTTP/2 request engines (DEPRECATED)
 ******************************************************************************/

/* The following functions were introduced for the experimental capi_proxy_http2
 * support, but have been abandoned since.
 * They are still declared here for backward compatibiliy, in case someone
 * tries to build an old capi_proxy_http2 against it, but will disappear
 * completely sometime in the future.
 */

struct kuda_thread_cond_t;
typedef struct h2_req_engine h2_req_engine;
typedef void http2_output_consumed(void *ctx, conn_rec *c, kuda_off_t consumed);

typedef kuda_status_t http2_req_engine_init(h2_req_engine *engine,
                                           const char *id,
                                           const char *type,
                                           kuda_pool_t *pool,
                                           kuda_size_t req_buffer_size,
                                           request_rec *r,
                                           http2_output_consumed **pconsumed,
                                           void **pbaton);

KUDA_DECLARE_OPTIONAL_FN(kuda_status_t,
                        http2_req_engine_push, (const char *engine_type,
                                                request_rec *r,
                                                http2_req_engine_init *einit));

KUDA_DECLARE_OPTIONAL_FN(kuda_status_t,
                        http2_req_engine_pull, (h2_req_engine *engine,
                                                kuda_read_type_e block,
                                                int capacity,
                                                request_rec **pr));
KUDA_DECLARE_OPTIONAL_FN(void,
                        http2_req_engine_done, (h2_req_engine *engine,
                                                conn_rec *rconn,
                                                kuda_status_t status));

KUDA_DECLARE_OPTIONAL_FN(void,
                        http2_get_num_workers, (server_rec *s,
                                                int *minw, int *max));

/*******************************************************************************
 * END HTTP/2 request engines (DEPRECATED)
 ******************************************************************************/

#endif

