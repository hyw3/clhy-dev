/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "clhy_config.h"
#include "clhy_capimn.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_connection.h"
#include "http_core.h"
#include "http_log.h"

#include "kuda_buckets.h"
#include "kuda_strings.h"
#include "util_filter.h"
#include "scoreboard.h"

cAPI CLHY_CAPI_DECLARE_DATA echo_capi;

typedef struct {
    int bEnabled;
} EchoConfig;

static void *create_echo_server_config(kuda_pool_t *p, server_rec *s)
{
    EchoConfig *pConfig = kuda_pcalloc(p, sizeof *pConfig);

    pConfig->bEnabled = 0;

    return pConfig;
}

static const char *echo_on(cmd_parms *cmd, void *dummy, int arg)
{
    EchoConfig *pConfig = clhy_get_capi_config(cmd->server->capi_config,
                                               &echo_capi);
    pConfig->bEnabled = arg;

    return NULL;
}

static kuda_status_t brigade_peek(kuda_bucket_brigade *bbIn,
                                 char *buff, kuda_size_t bufflen)
{
    kuda_bucket *b;
    kuda_size_t readbytes = 0;

    if (bufflen--)
        /* compensate for NULL */
        *buff = '\0';
    else
        return KUDA_EGENERAL;

    if (KUDA_BRIGADE_EMPTY(bbIn))
        return KUDA_EGENERAL;

    b = KUDA_BRIGADE_FIRST(bbIn);

    while ((b != KUDA_BRIGADE_SENTINEL(bbIn)) && (readbytes < bufflen)) {
        const char *pos;
        const char *str;
        kuda_size_t len;
        kuda_status_t rv;

        if ((rv = kuda_bucket_read(b, &str, &len, KUDA_NONBLOCK_READ))
                != KUDA_SUCCESS)
            return rv;

        if ((pos = memchr(str, KUDA_ASCII_LF, len)) != NULL)
            len = pos - str;
        if (len > bufflen - readbytes)
            len = bufflen - readbytes;
        memcpy (buff + readbytes, str, len);
        readbytes += len;
        buff[readbytes] = '\0';

        b = KUDA_BUCKET_NEXT(b);
    }
    return KUDA_SUCCESS;
}


static int update_echo_child_status(clhy_sb_handle_t *sbh,
                                    int status, conn_rec *c,
                                    kuda_bucket_brigade *last_echoed)
{
    worker_score *ws = clhy_get_scoreboard_worker(sbh);
    int old_status = ws->status;

    ws->status = status;

    if (!clhy_extended_status)
        return old_status;

    ws->last_used = kuda_time_now();

    /* initial pass only, please - in the name of efficiency */
    if (c) {
        kuda_cpystrn(ws->client,
                    clhy_get_remote_host(c, c->base_server->lookup_defaults,
                                       REMOTE_NOLOOKUP, NULL),
                    sizeof(ws->client));
        kuda_cpystrn(ws->vhost, c->base_server->server_hostname,
                    sizeof(ws->vhost));
        /* Deliberate trailing space - filling in string on WRITE passes */
        kuda_cpystrn(ws->request, "ECHO ", sizeof(ws->request));
    }

    /* each subsequent WRITE pass, let's update what we echoed */
    if (last_echoed) {
        brigade_peek(last_echoed, ws->request + sizeof("ECHO ") - 1,
                     sizeof(ws->request) - sizeof("ECHO ") + 1);
    }

    return old_status;
}

static int process_echo_connection(conn_rec *c)
{
    kuda_bucket_brigade *bb;
    kuda_bucket *b;
    kuda_socket_t *csd = NULL;
    EchoConfig *pConfig = clhy_get_capi_config(c->base_server->capi_config,
                                               &echo_capi);

    if (!pConfig->bEnabled) {
        return DECLINED;
    }

    clhy_time_process_request(c->sbh, START_PREQUEST);
    update_echo_child_status(c->sbh, SERVER_BUSY_READ, c, NULL);

    bb = kuda_brigade_create(c->pool, c->bucket_alloc);

    for ( ; ; ) {
        kuda_status_t rv;

        /* Get a single line of input from the client */
        if (((rv = clhy_get_brigade(c->input_filters, bb, CLHY_MODE_GETLINE,
                                  KUDA_BLOCK_READ, 0)) != KUDA_SUCCESS)) {
            kuda_brigade_cleanup(bb);
            if (!KUDA_STATUS_IS_EOF(rv) && ! KUDA_STATUS_IS_TIMEUP(rv))
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, rv, c->base_server, CLHYLOGNO(01611)
                             "ProtocolEcho: Failure reading from %s",
                             c->client_ip);
            break;
        }

        /* Something horribly wrong happened.  Someone didn't block! */
        if (KUDA_BRIGADE_EMPTY(bb)) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, rv, c->base_server, CLHYLOGNO(01612)
                         "ProtocolEcho: Error - read empty brigade from %s!",
                         c->client_ip);
            break;
        }

        if (!csd) {
            csd = clhy_get_conn_socket(c);
            kuda_socket_timeout_set(csd, c->base_server->keep_alive_timeout);
        }

        update_echo_child_status(c->sbh, SERVER_BUSY_WRITE, NULL, bb);

        /* Make sure the data is flushed to the client */
        b = kuda_bucket_flush_create(c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);
        rv = clhy_pass_brigade(c->output_filters, bb);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, rv, c->base_server, CLHYLOGNO(01613)
                         "ProtocolEcho: Failure writing to %s",
                         c->client_ip);
            break;
        }
        kuda_brigade_cleanup(bb);

        /* Announce our intent to loop */
        update_echo_child_status(c->sbh, SERVER_BUSY_KEEPALIVE, NULL, NULL);
    }
    kuda_brigade_destroy(bb);
    clhy_time_process_request(c->sbh, STOP_PREQUEST);
    update_echo_child_status(c->sbh, SERVER_CLOSING, c, NULL);
    return OK;
}

static const command_rec echo_cmds[] =
{
    CLHY_INIT_FLAG("ProtocolEcho", echo_on, NULL, RSRC_CONF,
                 "Run an echo server on this host"),
    { NULL }
};

static void register_hooks(kuda_pool_t *p)
{
    clhy_hook_process_connection(process_echo_connection, NULL, NULL,
                               KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(echo) = {
    STANDARD16_CAPI_STUFF,
    NULL,                       /* create per-directory config structure */
    NULL,                       /* merge per-directory config structures */
    create_echo_server_config,  /* create per-server config structure */
    NULL,                       /* merge per-server config structures */
    echo_cmds,                  /* command kuda_table_t */
    register_hooks              /* register hooks */
};
