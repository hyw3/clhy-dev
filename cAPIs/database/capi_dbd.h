/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  capi_dbd.h
 * @brief Database Access Extension cAPI for cLHy
 *
 * Digunakan untuk mengelola koneksi basis data SQL menggunakan Kuda,
 * yaitu menyediakan koneksi basis data berdasarkan permintaan ke cAPI
 * yang membutuhkan fungsi SQL, dan menanganinya dengan tingkat efisiensi
 * dan skalabilitas optimal, baik untuk threaded-cLMP ataupun non-threaded.
 *  
 * @defgroup CAPI_DBD capi_dbd
 * @ingroup CLHYKUDEL_CAPIS
 * @{
 */

#ifndef DBD_H
#define DBD_H

/* Create a set of DBD_DECLARE(type), DBD_DECLARE_NONSTD(type) and
 * DBD_DECLARE_DATA with appropriate export and import tags for the platform
 */
#if !defined(WIN32)
#define DBD_DECLARE(type)            type
#define DBD_DECLARE_NONSTD(type)     type
#define DBD_DECLARE_DATA
#elif defined(DBD_DECLARE_STATIC)
#define DBD_DECLARE(type)            type __stdcall
#define DBD_DECLARE_NONSTD(type)     type
#define DBD_DECLARE_DATA
#elif defined(DBD_DECLARE_EXPORT)
#define DBD_DECLARE(type)            __declspec(dllexport) type __stdcall
#define DBD_DECLARE_NONSTD(type)     __declspec(dllexport) type
#define DBD_DECLARE_DATA             __declspec(dllexport)
#else
#define DBD_DECLARE(type)            __declspec(dllimport) type __stdcall
#define DBD_DECLARE_NONSTD(type)     __declspec(dllimport) type
#define DBD_DECLARE_DATA             __declspec(dllimport)
#endif

#include <wwhy.h>
#include <kuda_optional.h>
#include <kuda_hash.h>
#include <kuda_hooks.h>

typedef struct {
    server_rec *server;
    const char *name;
    const char *params;
    int persist;
#if KUDA_HAS_THREADS
    int nmin;
    int nkeep;
    int nmax;
    int exptime;
    int set;
#endif
    kuda_hash_t *queries;
    kuda_array_header_t *init_queries;
} dbd_cfg_t;

typedef struct {
    kuda_dbd_t *handle;
    const kuda_dbd_driver_t *driver;
    kuda_hash_t *prepared;
    kuda_pool_t *pool;
} clhy_dbd_t;

/* Export functions to access the database */

/* acquire a connection that MUST be explicitly closed.
 * Returns NULL on error
 */
DBD_DECLARE_NONSTD(clhy_dbd_t*) clhy_dbd_open(kuda_pool_t*, server_rec*);

/* release a connection acquired with clhy_dbd_open */
DBD_DECLARE_NONSTD(void) clhy_dbd_close(server_rec*, clhy_dbd_t*);

/* acquire a connection that will have the lifetime of a request
 * and MUST NOT be explicitly closed.  Return NULL on error.
 * This is the preferred function for most applications.
 */
DBD_DECLARE_NONSTD(clhy_dbd_t*) clhy_dbd_acquire(request_rec*);

/* acquire a connection that will have the lifetime of a connection
 * and MUST NOT be explicitly closed.  Return NULL on error.
 * This is the preferred function for most applications.
 */
DBD_DECLARE_NONSTD(clhy_dbd_t*) clhy_dbd_cacquire(conn_rec*);

/* Prepare a statement for use by a client cAPI during
 * the server startup/configuration phase.  Can't be called
 * after the server has created its children (use kuda_dbd_*).
 */
DBD_DECLARE_NONSTD(void) clhy_dbd_prepare(server_rec*, const char*, const char*);

/* Also export them as optional functions for cAPIs that prefer it */
KUDA_DECLARE_OPTIONAL_FN(clhy_dbd_t*, clhy_dbd_open, (kuda_pool_t*, server_rec*));
KUDA_DECLARE_OPTIONAL_FN(void, clhy_dbd_close, (server_rec*, clhy_dbd_t*));
KUDA_DECLARE_OPTIONAL_FN(clhy_dbd_t*, clhy_dbd_acquire, (request_rec*));
KUDA_DECLARE_OPTIONAL_FN(clhy_dbd_t*, clhy_dbd_cacquire, (conn_rec*));
KUDA_DECLARE_OPTIONAL_FN(void, clhy_dbd_prepare, (server_rec*, const char*, const char*));

KUDA_DECLARE_EXTERNAL_HOOK(dbd, DBD, kuda_status_t, post_connect,
                          (kuda_pool_t *, dbd_cfg_t *, clhy_dbd_t *))

#endif
/** @} */

