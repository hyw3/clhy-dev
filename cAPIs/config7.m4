AC_MSG_CHECKING(for extra cAPIs)
AC_ARG_WITH(cAPI,
  CLHYKUDEL_HELP_STRING(--with-cAPI=cAPI-type:cAPI-file,
                     Enable cAPI-file in the cAPIs/<cAPI-type> directory.),
  [
    withval=`echo $withval | sed -e 's/,/ /g'`
    for mod in $withval
    do
      capitype=`echo $mod | sed -e's/\(.*\):.*/\1/'`
      pkg=`echo $mod | sed -e's/.*:\(.*\)/\1/'`
      capifilec=`echo $pkg | sed -e 's;^.*/;;'`
      capifileo=`echo $pkg | sed -e 's;^.*/;;' -e 's;\.c$;.o;'`
      capipath_current="cAPIs/$capitype"
      if test "x$mod" != "x$capipath_current/$capifilec"; then
        if test ! -d "$capipath_current"; then
          mkdir $capipath_current
          echo 'include $(top_srcdir)/build/special.mk' > $capipath_current/Makefile.in
        fi
        cp $pkg $capipath_current/$capifilec
      fi
      cAPI=`echo $pkg | sed -e 's;\(.*/\).*capi_\(.*\).c;\2;'`
      objects="capi_$cAPI.lo"
      # The filename of a convenience library must have a "lib" prefix:
      libname="libcapi_$cAPI.la"
      BUILTIN_LIBS="$BUILTIN_LIBS $capipath_current/$libname"
      if test ! -s "$capipath_current/cAPIs.mk"; then
        cat >>$capipath_current/cAPIs.mk<<EOF
$libname: $objects
	\$(CAPI_LINK) $objects
DISTCLEAN_TARGETS = cAPIs.mk
static = $libname
shared =
EOF
      else
        cat >>$capipath_current/cAPIs.mk.tmp<<EOF
$libname: $objects
	\$(CAPI_LINK) $objects
EOF
        cat $capipath_current/cAPIs.mk >> $capipath_current/cAPIs.mk.tmp
        rm $capipath_current/cAPIs.mk
        mv $capipath_current/cAPIs.mk.tmp $capipath_current/cAPIs.mk
        sed -e "s/\(static =.*\)/\1 $libname/" $capipath_current/cAPIs.mk > $capipath_current/cAPIs.mk.tmp
        rm $capipath_current/cAPIs.mk
        mv $capipath_current/cAPIs.mk.tmp $capipath_current/cAPIs.mk
      fi
      CAPILIST="$CAPILIST $cAPI"
      EXTRA_CAPILIST="$EXTRA_CAPILIST $capitype:$capifilec"
      CAPI_DIRS="$CAPI_DIRS $capitype"
      CLHYKUDEL_FAST_OUTPUT($capipath_current/Makefile)
    done
    if test ! -z "$EXTRA_CAPILIST"; then
      AC_MSG_RESULT(added:$EXTRA_CAPILIST)
    fi
  ],
  [ AC_MSG_RESULT(none) 
  ])
