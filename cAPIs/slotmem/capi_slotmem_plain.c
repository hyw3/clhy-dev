/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Memory handler for a plain memory divided in slot.
 * This one uses plain memory.
 */

#include  "clhy_slotmem.h"

#define CLHY_SLOTMEM_IS_PREGRAB(t) (t->type & CLHY_SLOTMEM_TYPE_PREGRAB)

struct clhy_slotmem_instance_t {
    char                 *name;       /* per segment name */
    void                 *base;       /* data set start */
    kuda_size_t           size;        /* size of each memory slot */
    unsigned int         num;         /* number of mem slots */
    kuda_pool_t           *gpool;      /* per segment global pool */
    char                 *inuse;      /* in-use flag table*/
    clhy_slotmem_type_t    type;        /* type-specific flags */
    struct clhy_slotmem_instance_t  *next;       /* location of next allocated segment */
};


/* global pool and list of slotmem we are handling */
static struct clhy_slotmem_instance_t *globallistmem = NULL;
static kuda_pool_t *gpool = NULL;

static kuda_status_t slotmem_do(clhy_slotmem_instance_t *mem, clhy_slotmem_callback_fn_t *func, void *data, kuda_pool_t *pool)
{
    unsigned int i;
    char *ptr;
    char *inuse;
    kuda_status_t retval = KUDA_SUCCESS;


    if (!mem)
        return KUDA_ENOSHMAVAIL;

    ptr = (char *)mem->base;
    inuse = mem->inuse;
    for (i = 0; i < mem->num; i++, inuse++) {
        if (!CLHY_SLOTMEM_IS_PREGRAB(mem) ||
           (CLHY_SLOTMEM_IS_PREGRAB(mem) && *inuse)) {
            retval = func((void *) ptr, data, pool);
            if (retval != KUDA_SUCCESS)
                break;
        }
        ptr += mem->size;
    }
    return retval;
}

static kuda_status_t slotmem_create(clhy_slotmem_instance_t **new, const char *name, kuda_size_t item_size, unsigned int item_num, clhy_slotmem_type_t type, kuda_pool_t *pool)
{
    clhy_slotmem_instance_t *res;
    clhy_slotmem_instance_t *next = globallistmem;
    kuda_size_t basesize = (item_size * item_num);

    const char *fname;

    if (name) {
        if (name[0] == ':')
            fname = name;
        else
            fname = clhy_runtime_dir_relative(pool, name);

        /* first try to attach to existing slotmem */
        if (next) {
            for (;;) {
                if (strcmp(next->name, fname) == 0) {
                    /* we already have it */
                    *new = next;
                    return KUDA_SUCCESS;
                }
                if (!next->next) {
                     break;
                }
                next = next->next;
            }
        }
    }
    else
        fname = "anonymous";

    /* create the memory using the gpool */
    res = (clhy_slotmem_instance_t *) kuda_pcalloc(gpool, sizeof(clhy_slotmem_instance_t));
    res->base = kuda_pcalloc(gpool, basesize + (item_num * sizeof(char)));
    if (!res->base)
        return KUDA_ENOSHMAVAIL;

    /* For the chained slotmem stuff */
    res->name = kuda_pstrdup(gpool, fname);
    res->size = item_size;
    res->num = item_num;
    res->next = NULL;
    res->type = type;
    res->inuse = (char *)res->base + basesize;
    if (globallistmem == NULL)
        globallistmem = res;
    else
        next->next = res;

    *new = res;
    return KUDA_SUCCESS;
}

static kuda_status_t slotmem_attach(clhy_slotmem_instance_t **new, const char *name, kuda_size_t *item_size, unsigned int *item_num, kuda_pool_t *pool)
{
    clhy_slotmem_instance_t *next = globallistmem;
    const char *fname;

    if (name) {
        if (name[0] == ':')
            fname = name;
        else
            fname = clhy_runtime_dir_relative(pool, name);
    }
    else
        return KUDA_ENOSHMAVAIL;

    /* first try to attach to existing slotmem */
    while (next) {
        if (strcmp(next->name, fname) == 0) {
            /* we already have it */
            *new = next;
            *item_size = next->size;
            *item_num = next->num;
            return KUDA_SUCCESS;
        }
        next = next->next;
    }

    return KUDA_ENOSHMAVAIL;
}

static kuda_status_t slotmem_dptr(clhy_slotmem_instance_t *score, unsigned int id, void **mem)
{

    char *ptr;

    if (!score)
        return KUDA_ENOSHMAVAIL;
    if (id >= score->num)
        return KUDA_EINVAL;

    ptr = (char *)score->base + score->size * id;
    if (!ptr)
        return KUDA_ENOSHMAVAIL;
    *mem = ptr;
    return KUDA_SUCCESS;
}

static kuda_status_t slotmem_get(clhy_slotmem_instance_t *slot, unsigned int id, unsigned char *dest, kuda_size_t dest_len)
{
    void *ptr;
    char *inuse;
    kuda_status_t ret;

    if (!slot) {
        return KUDA_ENOSHMAVAIL;
    }

    inuse = slot->inuse + id;
    if (id >= slot->num) {
        return KUDA_EINVAL;
    }
    if (CLHY_SLOTMEM_IS_PREGRAB(slot) && !*inuse) {
        return KUDA_NOTFOUND;
    }
    ret = slotmem_dptr(slot, id, &ptr);
    if (ret != KUDA_SUCCESS) {
        return ret;
    }
    *inuse=1;
    memcpy(dest, ptr, dest_len); /* bounds check? */
    return KUDA_SUCCESS;
}

static kuda_status_t slotmem_put(clhy_slotmem_instance_t *slot, unsigned int id, unsigned char *src, kuda_size_t src_len)
{
    void *ptr;
    char *inuse;
    kuda_status_t ret;

    if (!slot) {
        return KUDA_ENOSHMAVAIL;
    }

    inuse = slot->inuse + id;
    if (id >= slot->num) {
        return KUDA_EINVAL;
    }
    if (CLHY_SLOTMEM_IS_PREGRAB(slot) && !*inuse) {
        return KUDA_NOTFOUND;
    }
    ret = slotmem_dptr(slot, id, &ptr);
    if (ret != KUDA_SUCCESS) {
        return ret;
    }
    *inuse=1;
    memcpy(ptr, src, src_len); /* bounds check? */
    return KUDA_SUCCESS;
}

static unsigned int slotmem_num_slots(clhy_slotmem_instance_t *slot)
{
    return slot->num;
}

static unsigned int slotmem_num_free_slots(clhy_slotmem_instance_t *slot)
{
    unsigned int i, counter=0;
    char *inuse = slot->inuse;
    for (i = 0; i < slot->num; i++, inuse++) {
        if (!*inuse)
            counter++;
    }
    return counter;
}

static kuda_size_t slotmem_slot_size(clhy_slotmem_instance_t *slot)
{
    return slot->size;
}

/*
 * XXXX: if !CLHY_SLOTMEM_IS_PREGRAB, then still worry about
 *       inuse for grab and return?
 */
static kuda_status_t slotmem_grab(clhy_slotmem_instance_t *slot, unsigned int *id)
{
    unsigned int i;
    char *inuse;

    if (!slot) {
        return KUDA_ENOSHMAVAIL;
    }

    inuse = slot->inuse;

    for (i = 0; i < slot->num; i++, inuse++) {
        if (!*inuse) {
            break;
        }
    }
    if (i >= slot->num) {
        return KUDA_EINVAL;
    }
    *inuse = 1;
    *id = i;
    return KUDA_SUCCESS;
}

static kuda_status_t slotmem_fgrab(clhy_slotmem_instance_t *slot, unsigned int id)
{
    char *inuse;
    
    if (!slot) {
        return KUDA_ENOSHMAVAIL;
    }
    
    if (id >= slot->num) {
        return KUDA_EINVAL;
    }
    inuse = slot->inuse + id;
    *inuse = 1;
    return KUDA_SUCCESS;
}

static kuda_status_t slotmem_release(clhy_slotmem_instance_t *slot, unsigned int id)
{
    char *inuse;

    if (!slot) {
        return KUDA_ENOSHMAVAIL;
    }

    inuse = slot->inuse;

    if (id >= slot->num) {
        return KUDA_EINVAL;
    }
    if (!inuse[id] ) {
        return KUDA_NOTFOUND;
    }
    inuse[id] = 0;
    return KUDA_SUCCESS;
}

static const clhy_slotmem_provider_t storage = {
    "plainmem",
    &slotmem_do,
    &slotmem_create,
    &slotmem_attach,
    &slotmem_dptr,
    &slotmem_get,
    &slotmem_put,
    &slotmem_num_slots,
    &slotmem_num_free_slots,
    &slotmem_slot_size,
    &slotmem_grab,
    &slotmem_release,
    &slotmem_fgrab
};

static int pre_config(kuda_pool_t *p, kuda_pool_t *plog,
                      kuda_pool_t *ptemp)
{
    gpool = p;
    return OK;
}

static void clhy_slotmem_plain_register_hook(kuda_pool_t *p)
{
    /* XXX: static const char * const prePos[] = { "capi_slotmem.c", NULL }; */
    clhy_register_provider(p, CLHY_SLOTMEM_PROVIDER_GROUP, "plain",
                         CLHY_SLOTMEM_PROVIDER_VERSION, &storage);
    clhy_hook_pre_config(pre_config, NULL, NULL, KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(slotmem_plain) = {
    STANDARD16_CAPI_STUFF,
    NULL,                        /* create per-directory config structure */
    NULL,                        /* merge per-directory config structures */
    NULL,                        /* create per-server config structure */
    NULL,                        /* merge per-server config structures */
    NULL,                        /* command kuda_table_t */
    clhy_slotmem_plain_register_hook    /* register hooks */
};

