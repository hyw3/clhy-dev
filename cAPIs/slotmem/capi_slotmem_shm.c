/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Memory handler for a shared memory divided in slot.
 * This one uses shared memory.
 *
 * Shared memory is cleaned-up for each restart, graceful or
 * otherwise.
 */

#include  "clhy_slotmem.h"

#include "wwhy.h"
#include "http_main.h"
#include "clhy_core.h" /* for clhy_clmp_query() */

#define CLHY_SLOTMEM_IS_PREGRAB(t)    (t->desc.type & CLHY_SLOTMEM_TYPE_PREGRAB)
#define CLHY_SLOTMEM_IS_PERSIST(t)    (t->desc.type & CLHY_SLOTMEM_TYPE_PERSIST)
#define CLHY_SLOTMEM_IS_CLEARINUSE(t) (t->desc.type & CLHY_SLOTMEM_TYPE_CLEARINUSE)

/* The description of the slots to reuse the slotmem */
typedef struct {
    kuda_size_t size;             /* size of each memory slot */
    unsigned int num;            /* number of mem slots */
    clhy_slotmem_type_t type;      /* type-specific flags */
} sharedslotdesc_t;

#define CLHY_SLOTMEM_OFFSET (KUDA_ALIGN_DEFAULT(sizeof(sharedslotdesc_t)))
#define CLHY_UNSIGNEDINT_OFFSET (KUDA_ALIGN_DEFAULT(sizeof(unsigned int)))

struct clhy_slotmem_instance_t {
    char                 *name;       /* file based SHM path/name */
    char                 *pname;      /* persisted file path/name */
    int                  fbased;      /* filebased? */
    void                 *shm;        /* ptr to memory segment (kuda_shm_t *) */
    void                 *base;       /* data set start */
    kuda_pool_t           *gpool;      /* per segment global pool */
    char                 *inuse;      /* in-use flag table*/
    unsigned int         *num_free;   /* slot free count for this instance */
    void                 *persist;    /* persist dataset start */
    sharedslotdesc_t     desc;        /* per slot desc */
    struct clhy_slotmem_instance_t  *next;       /* location of next allocated segment */
};

/*
 * Memory layout:
 *     sharedslotdesc_t | num_free | slots | isuse array |
 *                      ^          ^
 *                      |          . base
 *                      . persist (also num_free)
 */

/* global pool and list of slotmem we are handling */
static struct clhy_slotmem_instance_t *globallistmem = NULL;
static kuda_pool_t *gpool = NULL;

#define DEFAULT_SLOTMEM_PREFIX "slotmem-shm-"
#define DEFAULT_SLOTMEM_SUFFIX ".shm"
#define DEFAULT_SLOTMEM_PERSIST_SUFFIX ".persist"

/* Unixes (and Netware) have the unlink() semantic, which allows to
 * kuda_file_remove() a file still in use (opened elsewhere), the inode
 * remains until the last fd is closed, whereas any file created with
 * the same name/path will use a new inode.
 *
 * On windows and OS2 ("\SHAREMEM\..." tree), kuda_file_remove() marks
 * the files for deletion until the last HANDLE is closed, meanwhile the
 * same file/path can't be opened/recreated.
 * Thus on graceful restart (the only restart mode with core_winnt), the
 * old file may still exist until all the children stop, while we ought
 * to create a new one for our new clear SHM.  Therefore, we would only
 * be able to reuse (attach) the old SHM, preventing some changes to
 * the config file, like the number of balancers/members, since the
 * size checks (to fit the new config) would fail.  Let's avoid this by
 * including the generation number in the SHM filename (obviously not
 * the persisted name!)
 */
#ifndef SLOTMEM_UNLINK_SEMANTIC
#if defined(WIN32) || defined(OS2)
#define SLOTMEM_UNLINK_SEMANTIC 0
#else
#define SLOTMEM_UNLINK_SEMANTIC 1
#endif
#endif

/*
 * Persist the slotmem in a file
 * slotmem name and file name.
 * none      : no persistent data
 * rel_name  : $server_root/rel_name
 * /abs_name : $abs_name
 *
 */
static int slotmem_filenames(kuda_pool_t *pool,
                             const char *slotname,
                             const char **filename,
                             const char **persistname)
{
    const char *fname = NULL, *pname = NULL;

    if (slotname && *slotname && strcasecmp(slotname, "none") != 0) {
        if (slotname[0] != '/') {
#if !SLOTMEM_UNLINK_SEMANTIC
            /* Each generation needs its own file name. */
            int generation = 0;
            clhy_clmp_query(CLHY_CLMPQ_GENERATION, &generation);
            fname = kuda_psprintf(pool, "%s%s_%x%s", DEFAULT_SLOTMEM_PREFIX,
                                 slotname, generation, DEFAULT_SLOTMEM_SUFFIX);
#else
            /* Reuse the same file name for each generation. */
            fname = kuda_pstrcat(pool, DEFAULT_SLOTMEM_PREFIX,
                                slotname, DEFAULT_SLOTMEM_SUFFIX,
                                NULL);
#endif
            fname = clhy_runtime_dir_relative(pool, fname);
        }
        else {
            /* Don't mangle the file name if given an absolute path, it's
             * up to the caller to provide a unique name when necessary.
             */
            fname = slotname;
        }

        if (persistname) {
            /* Persisted file names are immutable... */
#if !SLOTMEM_UNLINK_SEMANTIC
            if (slotname[0] != '/') {
                pname = kuda_pstrcat(pool, DEFAULT_SLOTMEM_PREFIX,
                                    slotname, DEFAULT_SLOTMEM_SUFFIX,
                                    DEFAULT_SLOTMEM_PERSIST_SUFFIX,
                                    NULL);
                pname = clhy_runtime_dir_relative(pool, pname);
            }
            else
#endif
            pname = kuda_pstrcat(pool, fname,
                                DEFAULT_SLOTMEM_PERSIST_SUFFIX,
                                NULL);
        }
    }

    *filename = fname;
    if (persistname) {
        *persistname = pname;
    }
    return (fname != NULL);
}

static void slotmem_clearinuse(clhy_slotmem_instance_t *slot)
{
    unsigned int i;
    char *inuse;
    
    if (!slot) {
        return;
    }
    
    inuse = slot->inuse;
    
    for (i = 0; i < slot->desc.num; i++, inuse++) {
        if (*inuse) {
            *inuse = 0;
            (*slot->num_free)++;
        }
    }
}

static void store_slotmem(clhy_slotmem_instance_t *slotmem)
{
    kuda_file_t *fp;
    kuda_status_t rv;
    kuda_size_t nbytes;
    unsigned char digest[KUDA_MD5_DIGESTSIZE];
    const char *storename = slotmem->pname;

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(02334)
                 "storing %s", storename);

    if (storename) {
        rv = kuda_file_open(&fp, storename, KUDA_CREATE | KUDA_READ | KUDA_WRITE,
                           KUDA_PLATFORM_DEFAULT, slotmem->gpool);
        if (KUDA_STATUS_IS_EEXIST(rv)) {
            kuda_file_remove(storename, slotmem->gpool);
            rv = kuda_file_open(&fp, storename, KUDA_CREATE | KUDA_READ | KUDA_WRITE,
                               KUDA_PLATFORM_DEFAULT, slotmem->gpool);
        }
        if (rv != KUDA_SUCCESS) {
            return;
        }
        if (CLHY_SLOTMEM_IS_CLEARINUSE(slotmem)) {
            slotmem_clearinuse(slotmem);
        }
        nbytes = (slotmem->desc.size * slotmem->desc.num) +
                 (slotmem->desc.num * sizeof(char)) + CLHY_UNSIGNEDINT_OFFSET;
        kuda_md5(digest, slotmem->persist, nbytes);
        rv = kuda_file_write_full(fp, slotmem->persist, nbytes, NULL);
        if (rv == KUDA_SUCCESS) {
            rv = kuda_file_write_full(fp, digest, KUDA_MD5_DIGESTSIZE, NULL);
        }
        kuda_file_close(fp);
        if (rv != KUDA_SUCCESS) {
            kuda_file_remove(storename, slotmem->gpool);
        }
    }
}

static kuda_status_t restore_slotmem(void *ptr, const char *storename,
                                    kuda_size_t size, kuda_pool_t *pool)
{
    kuda_file_t *fp;
    kuda_size_t nbytes = size;
    kuda_status_t rv = KUDA_SUCCESS;
    unsigned char digest[KUDA_MD5_DIGESTSIZE];
    unsigned char digest2[KUDA_MD5_DIGESTSIZE];

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(02335)
                 "restoring %s", storename);

    if (storename) {
        rv = kuda_file_open(&fp, storename, KUDA_READ | KUDA_WRITE, KUDA_PLATFORM_DEFAULT,
                           pool);
        if (rv == KUDA_SUCCESS) {
            rv = kuda_file_read(fp, ptr, &nbytes);
            if ((rv == KUDA_SUCCESS || rv == KUDA_EOF) && nbytes == size) {
                rv = KUDA_SUCCESS;   /* for successful return @ EOF */
                /*
                 * if at EOF, don't bother checking md5
                 *  - backwards compatibility
                 *  */
                if (kuda_file_eof(fp) != KUDA_EOF) {
                    kuda_size_t ds = KUDA_MD5_DIGESTSIZE;
                    rv = kuda_file_read(fp, digest, &ds);
                    if ((rv == KUDA_SUCCESS || rv == KUDA_EOF) &&
                        ds == KUDA_MD5_DIGESTSIZE) {
                        rv = KUDA_SUCCESS;
                        kuda_md5(digest2, ptr, nbytes);
                        if (memcmp(digest, digest2, KUDA_MD5_DIGESTSIZE)) {
                            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, clhy_server_conf,
                                         CLHYLOGNO(02551) "bad md5 match");
                            rv = KUDA_EGENERAL;
                        }
                    }
                }
                else {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, clhy_server_conf,
                                 CLHYLOGNO(02552) "at EOF... bypassing md5 match check (old persist file?)");
                }
            }
            else if (nbytes != size) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, clhy_server_conf,
                             CLHYLOGNO(02553) "Expected %" KUDA_SIZE_T_FMT ": Read %" KUDA_SIZE_T_FMT,
                             size, nbytes);
                rv = KUDA_EGENERAL;
            }
            kuda_file_close(fp);
        }
    }
    return rv;
}

static kuda_status_t cleanup_slotmem(void *param)
{
    clhy_slotmem_instance_t **mem = param;

    if (*mem) {
        clhy_slotmem_instance_t *next = *mem;
        while (next) {
            if (CLHY_SLOTMEM_IS_PERSIST(next)) {
                store_slotmem(next);
            }
            kuda_shm_destroy((kuda_shm_t *)next->shm);
            if (next->fbased) {
                kuda_shm_remove(next->name, next->gpool);
                kuda_file_remove(next->name, next->gpool);
            }
            next = next->next;
        }
    }
    /* kuda_pool_destroy(gpool); */
    globallistmem = NULL;
    return KUDA_SUCCESS;
}

static kuda_status_t slotmem_doall(clhy_slotmem_instance_t *mem,
                                  clhy_slotmem_callback_fn_t *func,
                                  void *data, kuda_pool_t *pool)
{
    unsigned int i;
    char *ptr;
    char *inuse;
    kuda_status_t retval = KUDA_SUCCESS;

    if (!mem) {
        return KUDA_ENOSHMAVAIL;
    }

    ptr = (char *)mem->base;
    inuse = mem->inuse;
    for (i = 0; i < mem->desc.num; i++, inuse++) {
        if (!CLHY_SLOTMEM_IS_PREGRAB(mem) ||
           (CLHY_SLOTMEM_IS_PREGRAB(mem) && *inuse)) {
            retval = func((void *) ptr, data, pool);
            if (retval != KUDA_SUCCESS)
                break;
        }
        ptr += mem->desc.size;
    }
    return retval;
}

static kuda_status_t slotmem_create(clhy_slotmem_instance_t **new,
                                   const char *name, kuda_size_t item_size,
                                   unsigned int item_num,
                                   clhy_slotmem_type_t type, kuda_pool_t *pool)
{
    int fbased = 1;
    int restored = 0;
    char *ptr;
    sharedslotdesc_t desc;
    clhy_slotmem_instance_t *res;
    clhy_slotmem_instance_t *next = globallistmem;
    const char *fname, *pname = NULL;
    kuda_shm_t *shm;
    kuda_size_t basesize = (item_size * item_num);
    kuda_size_t size = CLHY_SLOTMEM_OFFSET + CLHY_UNSIGNEDINT_OFFSET +
                      (item_num * sizeof(char)) + basesize;
    int persist = (type & CLHY_SLOTMEM_TYPE_PERSIST) != 0;
    kuda_status_t rv;

    if (gpool == NULL) {
        return KUDA_ENOSHMAVAIL;
    }
    if (slotmem_filenames(pool, name, &fname, persist ? &pname : NULL)) {
        /* first try to attach to existing slotmem */
        if (next) {
            for (;;) {
                if (strcmp(next->name, fname) == 0) {
                    /* we already have it */
                    *new = next;
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(02603)
                                 "create found %s in global list", fname);
                    return KUDA_SUCCESS;
                }
                if (!next->next) {
                     break;
                }
                next = next->next;
            }
        }
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(02602)
                     "create didn't find %s in global list", fname);
    }
    else {
        fbased = 0;
        fname = "none";
    }

    /* first try to attach to existing shared memory */
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(02300)
                 "create %s: %"KUDA_SIZE_T_FMT"/%u", fname, item_size,
                 item_num);
    if (fbased) {
        rv = kuda_shm_attach(&shm, fname, gpool);
    }
    else {
        rv = KUDA_EINVAL;
    }
    if (rv == KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(02598)
                     "kuda_shm_attach() succeeded");

        /* check size */
        if (kuda_shm_size_get(shm) != size) {
            kuda_shm_detach(shm);
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, clhy_server_conf, CLHYLOGNO(02599)
                         "existing shared memory for %s could not be used (failed size check)",
                         fname);
            return KUDA_EINVAL;
        }
        ptr = (char *)kuda_shm_baseaddr_get(shm);
        memcpy(&desc, ptr, sizeof(desc));
        if (desc.size != item_size || desc.num != item_num) {
            kuda_shm_detach(shm);
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, clhy_server_conf, CLHYLOGNO(02600)
                         "existing shared memory for %s could not be used (failed contents check)",
                         fname);
            return KUDA_EINVAL;
        }
        ptr += CLHY_SLOTMEM_OFFSET;
    }
    else {
        kuda_size_t dsize = size - CLHY_SLOTMEM_OFFSET;
        if (fbased) {
            kuda_shm_remove(fname, gpool);
            rv = kuda_shm_create(&shm, size, fname, gpool);
        }
        else {
            rv = kuda_shm_create(&shm, size, NULL, gpool);
        }
        clhy_log_error(CLHYLOG_MARK, rv == KUDA_SUCCESS ? CLHYLOG_DEBUG : CLHYLOG_ERR,
                     rv, clhy_server_conf, CLHYLOGNO(02611)
                     "create: kuda_shm_create(%s) %s",
                     fname ? fname : "",
                     rv == KUDA_SUCCESS ? "succeeded" : "failed");
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
        ptr = (char *)kuda_shm_baseaddr_get(shm);
        desc.size = item_size;
        desc.num = item_num;
        desc.type = type;
        memcpy(ptr, &desc, sizeof(desc));
        ptr += CLHY_SLOTMEM_OFFSET;
        memset(ptr, 0, dsize);
        /*
         * TODO: Error check the below... What error makes
         * sense if the restore fails? Any?
         */
        if (persist) {
            rv = restore_slotmem(ptr, pname, dsize, pool);
            if (rv == KUDA_SUCCESS) {
                restored = 1;
            }
            else {
                /* just in case, re-zero */
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf,
                             CLHYLOGNO(02554) "could not restore %s", fname);
                memset(ptr, 0, dsize);
            }
        }
    }

    /* For the chained slotmem stuff */
    res = (clhy_slotmem_instance_t *) kuda_pcalloc(gpool,
                                                sizeof(clhy_slotmem_instance_t));
    res->name = kuda_pstrdup(gpool, fname);
    res->pname = kuda_pstrdup(gpool, pname);
    res->fbased = fbased;
    res->shm = shm;
    res->num_free = (unsigned int *)ptr;
    if (!restored) {
        *res->num_free = item_num;
    }
    res->persist = (void *)ptr;
    ptr += CLHY_UNSIGNEDINT_OFFSET;
    res->base = (void *)ptr;
    res->desc = desc;
    res->gpool = gpool;
    res->next = NULL;
    res->inuse = ptr + basesize;
    if (globallistmem == NULL) {
        globallistmem = res;
    }
    else {
        next->next = res;
    }

    *new = res;
    return KUDA_SUCCESS;
}

static kuda_status_t slotmem_attach(clhy_slotmem_instance_t **new,
                                   const char *name, kuda_size_t *item_size,
                                   unsigned int *item_num, kuda_pool_t *pool)
{
/*    void *slotmem = NULL; */
    char *ptr;
    clhy_slotmem_instance_t *res;
    clhy_slotmem_instance_t *next = globallistmem;
    sharedslotdesc_t desc;
    const char *fname;
    kuda_shm_t *shm;
    kuda_status_t rv;

    if (gpool == NULL) {
        return KUDA_ENOSHMAVAIL;
    }
    if (!slotmem_filenames(pool, name, &fname, NULL)) {
        return KUDA_ENOSHMAVAIL;
    }

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(02301)
                 "attach looking for %s", fname);

    /* first try to attach to existing slotmem */
    if (next) {
        for (;;) {
            if (strcmp(next->name, fname) == 0) {
                /* we already have it */
                *new = next;
                *item_size = next->desc.size;
                *item_num = next->desc.num;
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf,
                             CLHYLOGNO(02302)
                             "attach found %s: %"KUDA_SIZE_T_FMT"/%u", fname,
                             *item_size, *item_num);
                return KUDA_SUCCESS;
            }
            if (!next->next) {
                 break;
            }
            next = next->next;
        }
    }

    /* next try to attach to existing shared memory */
    rv = kuda_shm_attach(&shm, fname, gpool);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    /* Read the description of the slotmem */
    ptr = (char *)kuda_shm_baseaddr_get(shm);
    memcpy(&desc, ptr, sizeof(desc));
    ptr += CLHY_SLOTMEM_OFFSET;

    /* For the chained slotmem stuff */
    res = (clhy_slotmem_instance_t *) kuda_pcalloc(gpool,
                                                sizeof(clhy_slotmem_instance_t));
    res->name = kuda_pstrdup(gpool, fname);
    res->fbased = 1;
    res->shm = shm;
    res->num_free = (unsigned int *)ptr;
    res->persist = (void *)ptr;
    ptr += CLHY_UNSIGNEDINT_OFFSET;
    res->base = (void *)ptr;
    res->desc = desc;
    res->gpool = gpool;
    res->inuse = ptr + (desc.size * desc.num);
    res->next = NULL;
    if (globallistmem == NULL) {
        globallistmem = res;
    }
    else {
        next->next = res;
    }

    *new = res;
    *item_size = desc.size;
    *item_num = desc.num;
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf,
                 CLHYLOGNO(02303)
                 "attach found %s: %"KUDA_SIZE_T_FMT"/%u", fname,
                 *item_size, *item_num);
    return KUDA_SUCCESS;
}

static kuda_status_t slotmem_dptr(clhy_slotmem_instance_t *slot,
                                 unsigned int id, void **mem)
{
    char *ptr;

    if (!slot) {
        return KUDA_ENOSHMAVAIL;
    }
    if (id >= slot->desc.num) {
        return KUDA_EINVAL;
    }

    ptr = (char *)slot->base + slot->desc.size * id;
    if (!ptr) {
        return KUDA_ENOSHMAVAIL;
    }
    *mem = (void *)ptr;
    return KUDA_SUCCESS;
}

static kuda_status_t slotmem_get(clhy_slotmem_instance_t *slot, unsigned int id,
                                unsigned char *dest, kuda_size_t dest_len)
{
    void *ptr;
    char *inuse;
    kuda_status_t ret;

    if (!slot) {
        return KUDA_ENOSHMAVAIL;
    }

    inuse = slot->inuse + id;
    if (id >= slot->desc.num) {
        return KUDA_EINVAL;
    }
    if (CLHY_SLOTMEM_IS_PREGRAB(slot) && !*inuse) {
        return KUDA_NOTFOUND;
    }
    ret = slotmem_dptr(slot, id, &ptr);
    if (ret != KUDA_SUCCESS) {
        return ret;
    }
    *inuse = 1;
    memcpy(dest, ptr, dest_len); /* bounds check? */
    return KUDA_SUCCESS;
}

static kuda_status_t slotmem_put(clhy_slotmem_instance_t *slot, unsigned int id,
                                unsigned char *src, kuda_size_t src_len)
{
    void *ptr;
    char *inuse;
    kuda_status_t ret;

    if (!slot) {
        return KUDA_ENOSHMAVAIL;
    }

    inuse = slot->inuse + id;
    if (id >= slot->desc.num) {
        return KUDA_EINVAL;
    }
    if (CLHY_SLOTMEM_IS_PREGRAB(slot) && !*inuse) {
        return KUDA_NOTFOUND;
    }
    ret = slotmem_dptr(slot, id, &ptr);
    if (ret != KUDA_SUCCESS) {
        return ret;
    }
    *inuse=1;
    memcpy(ptr, src, src_len); /* bounds check? */
    return KUDA_SUCCESS;
}

static unsigned int slotmem_num_slots(clhy_slotmem_instance_t *slot)
{
    return slot->desc.num;
}

static unsigned int slotmem_num_free_slots(clhy_slotmem_instance_t *slot)
{
    if (CLHY_SLOTMEM_IS_PREGRAB(slot))
        return *slot->num_free;
    else {
        unsigned int i, counter=0;
        char *inuse = slot->inuse;
        for (i=0; i<slot->desc.num; i++, inuse++) {
            if (!*inuse)
                counter++;
        }
        return counter;
    }
}

static kuda_size_t slotmem_slot_size(clhy_slotmem_instance_t *slot)
{
    return slot->desc.size;
}

static kuda_status_t slotmem_grab(clhy_slotmem_instance_t *slot, unsigned int *id)
{
    unsigned int i;
    char *inuse;

    if (!slot) {
        return KUDA_ENOSHMAVAIL;
    }

    inuse = slot->inuse;

    for (i = 0; i < slot->desc.num; i++, inuse++) {
        if (!*inuse) {
            break;
        }
    }
    if (i >= slot->desc.num) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(02293)
                     "slotmem(%s) grab failed. Num %u/num_free %u",
                     slot->name, slotmem_num_slots(slot),
                     slotmem_num_free_slots(slot));
        return KUDA_EINVAL;
    }
    *inuse = 1;
    *id = i;
    (*slot->num_free)--;
    return KUDA_SUCCESS;
}

static kuda_status_t slotmem_fgrab(clhy_slotmem_instance_t *slot, unsigned int id)
{
    char *inuse;
    
    if (!slot) {
        return KUDA_ENOSHMAVAIL;
    }

    if (id >= slot->desc.num) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(02397)
                     "slotmem(%s) fgrab failed. Num %u/num_free %u",
                     slot->name, slotmem_num_slots(slot),
                     slotmem_num_free_slots(slot));
        return KUDA_EINVAL;
    }
    inuse = slot->inuse + id;

    if (!*inuse) {
        *inuse = 1;
        (*slot->num_free)--;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t slotmem_release(clhy_slotmem_instance_t *slot,
                                    unsigned int id)
{
    char *inuse;

    if (!slot) {
        return KUDA_ENOSHMAVAIL;
    }

    inuse = slot->inuse;

    if (id >= slot->desc.num || !inuse[id] ) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(02294)
                     "slotmem(%s) release failed. Num %u/inuse[%u] %d",
                     slot->name, slotmem_num_slots(slot),
                     id, (int)inuse[id]);
        if (id >= slot->desc.num) {
            return KUDA_EINVAL;
        } else {
            return KUDA_NOTFOUND;
        }
    }
    inuse[id] = 0;
    (*slot->num_free)++;
    return KUDA_SUCCESS;
}

static const clhy_slotmem_provider_t storage = {
    "sharedmem",
    &slotmem_doall,
    &slotmem_create,
    &slotmem_attach,
    &slotmem_dptr,
    &slotmem_get,
    &slotmem_put,
    &slotmem_num_slots,
    &slotmem_num_free_slots,
    &slotmem_slot_size,
    &slotmem_grab,
    &slotmem_release,
    &slotmem_fgrab
};

/* make the storage usable from outside */
static const clhy_slotmem_provider_t *slotmem_shm_getstorage(void)
{
    return (&storage);
}

/* initialise the global pool */
static void slotmem_shm_initgpool(kuda_pool_t *p)
{
    gpool = p;
}

/* Add the pool_clean routine */
static void slotmem_shm_initialize_cleanup(kuda_pool_t *p)
{
    kuda_pool_cleanup_register(p, &globallistmem, cleanup_slotmem,
                              kuda_pool_cleanup_null);
}

/*
 * Make sure the shared memory is cleaned
 */
static int post_config(kuda_pool_t *p, kuda_pool_t *plog, kuda_pool_t *ptemp,
                       server_rec *s)
{
    slotmem_shm_initialize_cleanup(p);
    return OK;
}

static int pre_config(kuda_pool_t *p, kuda_pool_t *plog,
                      kuda_pool_t *ptemp)
{
    slotmem_shm_initgpool(p);
    return OK;
}

static void clhy_slotmem_shm_register_hook(kuda_pool_t *p)
{
    const clhy_slotmem_provider_t *storage = slotmem_shm_getstorage();
    clhy_register_provider(p, CLHY_SLOTMEM_PROVIDER_GROUP, "shm",
                         CLHY_SLOTMEM_PROVIDER_VERSION, storage);
    clhy_hook_post_config(post_config, NULL, NULL, KUDA_HOOK_LAST);
    clhy_hook_pre_config(pre_config, NULL, NULL, KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(slotmem_shm) = {
    STANDARD16_CAPI_STUFF,
    NULL,                       /* create per-directory config structure */
    NULL,                       /* merge per-directory config structures */
    NULL,                       /* create per-server config structure */
    NULL,                       /* merge per-server config structures */
    NULL,                       /* command kuda_table_t */
    clhy_slotmem_shm_register_hook  /* register hooks */
};
