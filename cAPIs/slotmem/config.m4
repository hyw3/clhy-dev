dnl cAPIs enabled in this directory by default

dnl CLHYKUDEL_CAPI(name, helptext[, objects[, structname[, default[, config]]]])

CLHYKUDEL_CAPIPATH_INIT(slotmem)

CLHYKUDEL_CAPI(slotmem_shm, slotmem provider that uses shared memory, , , most)
CLHYKUDEL_CAPI(slotmem_plain, slotmem provider that uses plain memory, , , )

CLHYKUDEL_CAPIPATH_FINISH
