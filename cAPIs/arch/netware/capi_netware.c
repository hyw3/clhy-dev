/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_buckets.h"
#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_protocol.h"
#include "http_request.h"
#include "http_log.h"
#include "util_script.h"
#include "capi_core.h"
#include "kuda_optional.h"
#include "kuda_lib.h"
#include "capi_cgi.h"
#include "core_common.h"

#ifdef NETWARE


cAPI CLHY_CAPI_DECLARE_DATA netware_capi;

typedef struct {
    kuda_table_t *file_type_handlers;    /* CGI map from file types to CGI cAPIs */
    kuda_table_t *file_handler_mode;     /* CGI cAPI mode (spawn in same address space or not) */
    kuda_table_t *extra_env_vars;        /* Environment variables to be added to the CGI environment */
} netware_dir_config;


static void *create_netware_dir_config(kuda_pool_t *p, char *dir)
{
    netware_dir_config *new = (netware_dir_config*) kuda_palloc(p, sizeof(netware_dir_config));

    new->file_type_handlers = kuda_table_make(p, 10);
    new->file_handler_mode = kuda_table_make(p, 10);
    new->extra_env_vars = kuda_table_make(p, 10);

    kuda_table_setn(new->file_type_handlers, "NLM", "PLATFORM");

    return new;
}

static void *merge_netware_dir_configs(kuda_pool_t *p, void *basev, void *addv)
{
    netware_dir_config *base = (netware_dir_config *) basev;
    netware_dir_config *add = (netware_dir_config *) addv;
    netware_dir_config *new = (netware_dir_config *) kuda_palloc(p, sizeof(netware_dir_config));

    new->file_type_handlers = kuda_table_overlay(p, add->file_type_handlers, base->file_type_handlers);
    new->file_handler_mode = kuda_table_overlay(p, add->file_handler_mode, base->file_handler_mode);
    new->extra_env_vars = kuda_table_overlay(p, add->extra_env_vars, base->extra_env_vars);

    return new;
}

static const char *set_extension_map(cmd_parms *cmd, netware_dir_config *m,
                                     char *CGIhdlr, char *ext, char *detach)
{
    int i, len;

    if (*ext == '.')
        ++ext;

    if (CGIhdlr != NULL) {
        len = strlen(CGIhdlr);
        for (i=0; i<len; i++) {
            if (CGIhdlr[i] == '\\') {
                CGIhdlr[i] = '/';
            }
        }
    }

    kuda_table_set(m->file_type_handlers, ext, CGIhdlr);
    if (detach) {
        kuda_table_set(m->file_handler_mode, ext, "y");
    }

    return NULL;
}

static kuda_status_t clhy_cgi_build_command(const char **cmd, const char ***argv,
                                         request_rec *r, kuda_pool_t *p,
                                         cgi_exec_info_t *e_info)
{
    char *ext = NULL;
    char *cmd_only, *ptr;
    const char *new_cmd;
    netware_dir_config *d;
    const char *args = "";

    d = (netware_dir_config *)clhy_get_capi_config(r->per_dir_config,
                                               &netware_capi);

    if (e_info->process_cgi) {
        /* Handle the complete file name, we DON'T want to follow suexec, since
         * an unrooted command is as predictable as shooting craps in Win32.
         *
         * Notice that unlike most mime extension parsing, we have to use the
         * win32 parsing here, therefore the final extension is the only one
         * we will consider
         */
        *cmd = r->filename;
        if (r->args && r->args[0] && !clhy_strchr_c(r->args, '=')) {
            args = r->args;
        }
    }

    cmd_only = kuda_pstrdup(p, *cmd);
    e_info->cmd_type = KUDA_PROGRAM;

    /* truncate any arguments from the cmd */
    for (ptr = cmd_only; *ptr && (*ptr != ' '); ptr++);
    *ptr = '\0';

    /* Figure out what the extension is so that we can match it. */
    ext = strrchr(kuda_filepath_name_get(cmd_only), '.');

    /* If there isn't an extension then give it an empty string */
    if (!ext) {
        ext = "";
    }

    /* eliminate the '.' if there is one */
    if (*ext == '.') {
        ++ext;
    }

    /* check if we have a registered command for the extension*/
    new_cmd = kuda_table_get(d->file_type_handlers, ext);
    e_info->detached = 1;
    if (new_cmd == NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02135)
                  "Could not find a command associated with the %s extension", ext);
        return KUDA_EBADF;
    }
    if (stricmp(new_cmd, "PLATFORM")) {
        /* If we have a registered command then add the file that was passed in as a
          parameter to the registered command. */
        *cmd = kuda_pstrcat (p, new_cmd, " ", cmd_only, NULL);

        /* Run in its own address space if specified */
        if (kuda_table_get(d->file_handler_mode, ext)) {
            e_info->addrspace = 1;
        }
    }

    /* Tokenize the full command string into its arguments */
    kuda_tokenize_to_argv(*cmd, (char***)argv, p);

    /* The first argument should be the executible */
    *cmd = clhy_server_root_relative(p, *argv[0]);

    return KUDA_SUCCESS;
}

static int
netware_pre_config(kuda_pool_t *pconf, kuda_pool_t *plog,
                 kuda_pool_t *ptemp)
{
    clhy_sys_privileges_handlers(1);
    return OK;
}

static void register_hooks(kuda_pool_t *p)
{
    KUDA_REGISTER_OPTIONAL_FN(clhy_cgi_build_command);
    clhy_hook_pre_config(netware_pre_config,
                       NULL, NULL, KUDA_HOOK_FIRST);
}

static const command_rec netware_cmds[] = {
CLHY_INIT_TAKE23("CGIMapExtension", set_extension_map, NULL, OR_FILEINFO,
              "Full path to the CGI NLM cAPI followed by a file extension. If the "
              "first parameter is set to \"PLATFORM\" then the following file extension is "
              "treated as NLM. The optional parameter \"detach\" can be specified if "
              "the NLM should be launched in its own address space."),
{ NULL }
};

CLHY_DECLARE_CAPI(netware) = {
   STANDARD16_CAPI_STUFF,
   create_netware_dir_config,     /* create per-dir config */
   merge_netware_dir_configs,     /* merge per-dir config */
   NULL,                        /* server config */
   NULL,                        /* merge server config */
   netware_cmds,                  /* command kuda_table_t */
   register_hooks               /* register hooks */
};

#endif
