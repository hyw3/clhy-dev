/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_main.h"
#include "http_log.h"
#include "http_core.h"
#include "core_common.h"
#include "platform.h"
#include "clhy_core.h"
#include "capi_unixd.h"
#include "kuda_thread_proc.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#ifdef HAVE_PWD_H
#include <pwd.h>
#endif
#ifdef HAVE_SYS_RESOURCE_H
#include <sys/resource.h>
#endif
/* XXX */
#include <sys/stat.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_GRP_H
#include <grp.h>
#endif
#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif
#ifdef HAVE_SYS_SEM_H
#include <sys/sem.h>
#endif
#ifdef HAVE_SYS_PRCTL_H
#include <sys/prctl.h>
#endif

#ifndef DEFAULT_USER
#define DEFAULT_USER "#-1"
#endif
#ifndef DEFAULT_GROUP
#define DEFAULT_GROUP "#-1"
#endif

#if 0
typedef struct {
  const char *user_name;
  uid_t user_id;
  gid_t group_id;
  const char *chroot_dir;
} unixd_config_t;
#else
/*
 * TODO: clean up the separation between this code
 *       and its data structures and unixd.c, as shown
 *       by the fact that we include unixd.h. Create
 *       capi_unixd.h which does what we need and
 *       clean up unixd.h for what it no longer needs
 */
#include "unixd.h"
#endif


/* Set group privileges.
 *
 * Note that we use the username as set in the config files, rather than
 * the lookup of to uid --- the same uid may have multiple passwd entries,
 * with different sets of groups for each.
 */

static int set_group_privs(void)
{
    if (!geteuid()) {
        const char *name;

        /* Get username if passed as a uid */

        if (clhy_unixd_config.user_name[0] == '#') {
            struct passwd *ent;
            uid_t uid = atol(&clhy_unixd_config.user_name[1]);

            if ((ent = getpwuid(uid)) == NULL) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02155)
                         "getpwuid: couldn't determine user name from uid %ld, "
                         "you probably need to modify the User directive",
                         (long)uid);
                return -1;
            }

            name = ent->pw_name;
        }
        else
            name = clhy_unixd_config.user_name;

#if !defined(OS2)
        /* OS2 doesn't support groups. */
        /*
         * Set the GID before initgroups(), since on some platforms
         * setgid() is known to zap the group list.
         */
        if (setgid(clhy_unixd_config.group_id) == -1) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02156)
                        "setgid: unable to set group id to Group %ld",
                        (long)clhy_unixd_config.group_id);
            return -1;
        }

        /* Reset `groups' attributes. */

        if (initgroups(name, clhy_unixd_config.group_id) == -1) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02157)
                        "initgroups: unable to set groups for User %s "
                        "and Group %ld", name, (long)clhy_unixd_config.group_id);
            return -1;
        }
#endif /* !defined(OS2) */
    }
    return 0;
}


static int
unixd_drop_privileges(kuda_pool_t *pool, server_rec *s)
{
    int rv = set_group_privs();

    if (rv) {
        return rv;
    }

    if (NULL != clhy_unixd_config.chroot_dir) {
        if (geteuid()) {
            rv = errno;
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02158)
                         "Cannot chroot when not started as root");
            return rv;
        }

        if (chdir(clhy_unixd_config.chroot_dir) != 0) {
            rv = errno;
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02159)
                         "Can't chdir to %s", clhy_unixd_config.chroot_dir);
            return rv;
        }

        if (chroot(clhy_unixd_config.chroot_dir) != 0) {
            rv = errno;
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02160)
                         "Can't chroot to %s", clhy_unixd_config.chroot_dir);
            return rv;
        }

        if (chdir("/") != 0) {
            rv = errno;
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02161)
                         "Can't chdir to new root");
            return rv;
        }
    }

    /* Only try to switch if we're running as root */
    if (!geteuid() && (
#ifdef _OSD_POSIX
        platform_init_job_environment(NULL, clhy_unixd_config.user_name, clhy_exists_config_define("DEBUG")) != 0 ||
#endif
        setuid(clhy_unixd_config.user_id) == -1)) {
        rv = errno;
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02162)
                    "setuid: unable to change to uid: %ld",
                    (long) clhy_unixd_config.user_id);
        return rv;
    }
#if defined(HAVE_PRCTL) && defined(PR_SET_DUMPABLE)
    /* this applies to Linux 2.4+ */
    if (clhy_coredumpdir_configured) {
        if (prctl(PR_SET_DUMPABLE, 1)) {
            rv = errno;
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02163)
                         "set dumpable failed - this child will not coredump"
                         " after software errors");
            return rv;
        }
    }
#endif

    return OK;
}


static const char *
unixd_set_user(cmd_parms *cmd, void *dummy,
               const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    clhy_unixd_config.user_name = arg;
    clhy_unixd_config.user_id = clhy_uname2id(arg);
#if !defined (BIG_SECURITY_HOLE) && !defined (OS2)
    if (clhy_unixd_config.user_id == 0) {
        return "Error:\tcLHy has not been designed to serve pages while\n"
                "\trunning as root.  There are known race conditions that\n"
                "\twill allow any local user to read any file on the system.\n"
                "\tIf you still desire to serve pages as root then\n"
                "\tadd -DBIG_SECURITY_HOLE to the CFLAGS env variable\n"
                "\tand then rebuild the server.\n"
                "\tIt is strongly suggested that you instead modify the User\n"
                "\tdirective in your wwhy.conf file to list a non-root\n"
                "\tuser.\n";
    }
#endif

    return NULL;
}

static const char*
unixd_set_group(cmd_parms *cmd, void *dummy,
                                         const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }

    clhy_unixd_config.group_name = arg;
    clhy_unixd_config.group_id = clhy_gname2id(arg);

    return NULL;
}

static const char*
unixd_set_chroot_dir(cmd_parms *cmd, void *dummy,
                    const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }
    if (!clhy_is_directory(cmd->pool, arg)) {
        return "ChrootDir must be a valid directory";
    }

    clhy_unixd_config.chroot_dir = arg;
    return NULL;
}

static const char *
unixd_set_suexec(cmd_parms *cmd, void *dummy, int arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err != NULL) {
        return err;
    }

    if (!clhy_unixd_config.suexec_enabled && arg) {
        return kuda_pstrcat(cmd->pool, "suEXEC isn't supported: ",
                           clhy_unixd_config.suexec_disabled_reason, NULL);
    }

    if (!arg) {
        clhy_unixd_config.suexec_disabled_reason = "Suexec directive is Off";
    }

    clhy_unixd_config.suexec_enabled = arg;
    return NULL;
}

static int
unixd_pre_config(kuda_pool_t *pconf, kuda_pool_t *plog,
                 kuda_pool_t *ptemp)
{
    kuda_finfo_t wrapper;
    clhy_unixd_config.user_name = DEFAULT_USER;
    clhy_unixd_config.user_id = clhy_uname2id(DEFAULT_USER);
    clhy_unixd_config.group_name = DEFAULT_GROUP;
    clhy_unixd_config.group_id = clhy_gname2id(DEFAULT_GROUP);

    clhy_unixd_config.chroot_dir = NULL; /* none */

    /* Check for suexec */
    clhy_unixd_config.suexec_enabled = 0;
    if ((kuda_stat(&wrapper, SUEXEC_BIN, KUDA_FINFO_NORM, ptemp))
         == KUDA_SUCCESS) {
        if ((wrapper.protection & KUDA_USETID) && wrapper.user == 0
            && (access(SUEXEC_BIN, R_OK|X_OK) == 0)) {
            clhy_unixd_config.suexec_enabled = 1;
            clhy_unixd_config.suexec_disabled_reason = "";
        }
        else {
            clhy_unixd_config.suexec_disabled_reason =
                "Invalid owner or file mode for " SUEXEC_BIN;
        }
    }
    else {
        clhy_unixd_config.suexec_disabled_reason =
            "Missing suexec binary " SUEXEC_BIN;
    }

    clhy_sys_privileges_handlers(1);
    return OK;
}

CLHY_DECLARE(int) clhy_unixd_setup_child(void)
{
    if (set_group_privs()) {
        return -1;
    }

    if (NULL != clhy_unixd_config.chroot_dir) {
        if (geteuid()) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02164)
                         "Cannot chroot when not started as root");
            return -1;
        }
        if (chdir(clhy_unixd_config.chroot_dir) != 0) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02165)
                         "Can't chdir to %s", clhy_unixd_config.chroot_dir);
            return -1;
        }
        if (chroot(clhy_unixd_config.chroot_dir) != 0) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02166)
                         "Can't chroot to %s", clhy_unixd_config.chroot_dir);
            return -1;
        }
        if (chdir("/") != 0) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02167)
                         "Can't chdir to new root");
            return -1;
        }
    }

    /* Only try to switch if we're running as root */
    if (!geteuid() && (
#ifdef _OSD_POSIX
        platform_init_job_environment(NULL, clhy_unixd_config.user_name, clhy_exists_config_define("DEBUG")) != 0 ||
#endif
        setuid(clhy_unixd_config.user_id) == -1)) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02168)
                    "setuid: unable to change to uid: %ld",
                    (long) clhy_unixd_config.user_id);
        return -1;
    }
#if defined(HAVE_PRCTL) && defined(PR_SET_DUMPABLE)
    /* this applies to Linux 2.4+ */
    if (clhy_coredumpdir_configured) {
        if (prctl(PR_SET_DUMPABLE, 1)) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ALERT, errno, NULL, CLHYLOGNO(02169)
                         "set dumpable failed - this child will not coredump"
                         " after software errors");
        }
    }
#endif
    return 0;
}

static void unixd_dump_config(kuda_pool_t *p, server_rec *s)
{
    kuda_file_t *out = NULL;
    kuda_uid_t uid = clhy_unixd_config.user_id;
    kuda_gid_t gid = clhy_unixd_config.group_id;
    char *no_root = "";
    if (!clhy_exists_config_define("DUMP_RUN_CFG"))
        return;
    if (geteuid() != 0)
        no_root = " not_used";
    kuda_file_open_stdout(&out, p);
    kuda_file_printf(out, "User: name=\"%s\" id=%lu%s\n",
                    clhy_unixd_config.user_name, (unsigned long)uid, no_root);
    kuda_file_printf(out, "Group: name=\"%s\" id=%lu%s\n",
                    clhy_unixd_config.group_name, (unsigned long)gid, no_root);
    if (clhy_unixd_config.chroot_dir)
        kuda_file_printf(out, "ChrootDir: \"%s\"%s\n",
                        clhy_unixd_config.chroot_dir, no_root);
}

static void unixd_hooks(kuda_pool_t *pool)
{
    clhy_hook_pre_config(unixd_pre_config,
                       NULL, NULL, KUDA_HOOK_FIRST);
    clhy_hook_test_config(unixd_dump_config,
                        NULL, NULL, KUDA_HOOK_FIRST);
    clhy_hook_drop_privileges(unixd_drop_privileges,
                            NULL, NULL, KUDA_HOOK_MIDDLE);
}

static const command_rec unixd_cmds[] = {
    CLHY_INIT_TAKE1("User", unixd_set_user, NULL, RSRC_CONF,
                  "Effective user id for this server"),
    CLHY_INIT_TAKE1("Group", unixd_set_group, NULL, RSRC_CONF,
                  "Effective group id for this server"),
    CLHY_INIT_TAKE1("ChrootDir", unixd_set_chroot_dir, NULL, RSRC_CONF,
                  "The directory to chroot(2) into"),
    CLHY_INIT_FLAG("Suexec", unixd_set_suexec, NULL, RSRC_CONF,
                 "Enable or disable suEXEC support"),
    {NULL}
};

CLHY_DECLARE_CAPI(unixd) = {
    STANDARD16_CAPI_STUFF,
    NULL,
    NULL,
    NULL,
    NULL,
    unixd_cmds,
    unixd_hooks
};

