/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <priv.h>
#include <sys/types.h>
#include <unistd.h>

#include "wwhy.h"
#include "http_config.h"
#include "http_protocol.h"
#include "http_log.h"
#include "core_common.h"
#include "clhy_core.h"
#include "kuda_strings.h"

/* TODO - get rid of unixd dependency */
#include "unixd.h"

#define CFG_CHECK(x) if ((x) == -1) { \
    char msgbuf[128]; \
    kuda_strerror(errno, msgbuf, sizeof(msgbuf)); \
    return kuda_pstrdup(cmd->pool, msgbuf); \
}
#define CR_CHECK(x, y) if (x == -1) \
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, errno, 0, y \
                 "Failed to initialise privileges")

cAPI CLHY_CAPI_DECLARE_DATA privileges_capi;

/* #define BIG_SECURITY_HOLE 1 */

typedef enum { PRIV_UNSET, PRIV_FAST, PRIV_SECURE, PRIV_SELECTIVE } priv_mode;

typedef struct {
    priv_set_t *priv;
    priv_set_t *child_priv;
    uid_t uid;
    gid_t gid;
    priv_mode mode;
} priv_cfg;

typedef struct {
    priv_mode mode;
} priv_dir_cfg;

static priv_set_t *priv_setid;
static priv_set_t *priv_default = NULL;
static int dtrace_enabled = 0;

static kuda_status_t priv_cfg_cleanup(void *CFG)
{
    priv_cfg *cfg = CFG;
    priv_freeset(cfg->priv);
    priv_freeset(cfg->child_priv);
    return KUDA_SUCCESS;
}
static void *privileges_merge_cfg(kuda_pool_t *pool, void *BASE, void *ADD)
{
    /* inherit the mode if it's not set; the rest won't be inherited */
    priv_cfg *base = BASE;
    priv_cfg *add = ADD;
    priv_cfg *ret = kuda_pmemdup(pool, add, sizeof(priv_cfg));
    ret->mode = (add->mode == PRIV_UNSET) ? base->mode : add->mode;
    return ret;
}
static void *privileges_create_cfg(kuda_pool_t *pool, server_rec *s)
{
    priv_cfg *cfg = kuda_palloc(pool, sizeof(priv_cfg));

    /* Start at basic privileges all round. */
    cfg->priv = priv_str_to_set("basic", ",", NULL);
    cfg->child_priv = priv_str_to_set("basic", ",", NULL);

    /* By default, run in secure vhost mode.
     * That means dropping basic privileges we don't usually need.
     */
    CR_CHECK(priv_delset(cfg->priv, PRIV_FILE_LINK_ANY), CLHYLOGNO(03160));
    CR_CHECK(priv_delset(cfg->priv, PRIV_PROC_INFO), CLHYLOGNO(03161));
    CR_CHECK(priv_delset(cfg->priv, PRIV_PROC_SESSION), CLHYLOGNO(03162));

/* Hmmm, should CGI default to secure too ? */
/*
    CR_CHECK(priv_delset(cfg->child_priv, PRIV_FILE_LINK_ANY), CLHYLOGNO(03163));
    CR_CHECK(priv_delset(cfg->child_priv, PRIV_PROC_INFO), CLHYLOGNO(03164));
    CR_CHECK(priv_delset(cfg->child_priv, PRIV_PROC_SESSION), CLHYLOGNO(03165));
    CR_CHECK(priv_delset(cfg->child_priv, PRIV_PROC_FORK), CLHYLOGNO(03166));
    CR_CHECK(priv_delset(cfg->child_priv, PRIV_PROC_EXEC), CLHYLOGNO(03167));
*/

    /* we´ll use 0 for unset */
    cfg->uid = 0;
    cfg->gid = 0;
    cfg->mode = PRIV_UNSET;
    kuda_pool_cleanup_register(pool, cfg, priv_cfg_cleanup,
                              kuda_pool_cleanup_null);

    /* top-level default_priv wants the top-level cfg */
    if (priv_default == NULL) {
        priv_default = cfg->priv;
    }
    return cfg;
}
static void *privileges_create_dir_cfg(kuda_pool_t *pool, char *dummy)
{
    priv_dir_cfg *cfg = kuda_palloc(pool, sizeof(priv_dir_cfg));
    cfg->mode = PRIV_UNSET;
    return cfg;
}
static void *privileges_merge_dir_cfg(kuda_pool_t *pool, void *BASE, void *ADD)
{
    priv_dir_cfg *base = BASE;
    priv_dir_cfg *add = ADD;
    priv_dir_cfg *ret = kuda_palloc(pool, sizeof(priv_dir_cfg));
    ret->mode = (add->mode == PRIV_UNSET) ? base->mode : add->mode;
    return ret;
}

static kuda_status_t privileges_end_req(void *data)
{
    request_rec *r = data;
    priv_cfg *cfg = clhy_get_capi_config(r->server->capi_config,
                                         &privileges_capi);
    priv_dir_cfg *dcfg = clhy_get_capi_config(r->per_dir_config,
                                              &privileges_capi);

    /* ugly hack: grab default uid and gid from unixd */
    extern unixd_config_rec clhy_unixd_config;

    /* If we forked a child, we dropped privilege to revert, so
     * all we can do now is exit
     */
    if ((cfg->mode == PRIV_SECURE) ||
        ((cfg->mode == PRIV_SELECTIVE) && (dcfg->mode == PRIV_SECURE))) {
        exit(0);
    }

    /* if either user or group are not the default, restore them */
    if (cfg->uid || cfg->gid) {
        if (setppriv(PRIV_ON, PRIV_EFFECTIVE, priv_setid) == -1) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02136)
                          "PRIV_ON failed restoring default user/group");
        }
        if (cfg->uid && (setuid(clhy_unixd_config.user_id) == -1)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02137)
                          "Error restoring default userid");
        }
        if (cfg->gid && (setgid(clhy_unixd_config.group_id) == -1)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02138)
                          "Error restoring default group");
        }
    }

    /* restore default privileges */
    if (setppriv(PRIV_SET, PRIV_EFFECTIVE, priv_default) == -1) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, errno, r, CLHYLOGNO(02139)
                      "Error restoring default privileges");
    }
    return KUDA_SUCCESS;
}
static int privileges_req(request_rec *r)
{
    /* secure mode: fork a process to handle the request */
    kuda_proc_t proc;
    kuda_status_t rv;
    int exitcode;
    kuda_exit_why_e exitwhy;
    int fork_req;
    priv_cfg *cfg = clhy_get_capi_config(r->server->capi_config,
                                         &privileges_capi);

    void *breadcrumb = clhy_get_capi_config(r->request_config,
                                            &privileges_capi);

    if (!breadcrumb) {
        /* first call: this is the vhost */
        fork_req = (cfg->mode == PRIV_SECURE);

        /* set breadcrumb */
        clhy_set_capi_config(r->request_config, &privileges_capi, &cfg->mode);

        /* If we have per-dir config, defer doing anything */
        if ((cfg->mode == PRIV_SELECTIVE)) {
            /* Defer dropping privileges 'til we have a directory
             * context that'll tell us whether to fork.
             */
            return DECLINED;
        }
    }
    else {
        /* second call is for per-directory. */
        priv_dir_cfg *dcfg;
        if ((cfg->mode != PRIV_SELECTIVE)) {
            /* Our fate was already determined for the vhost -
             * nothing to do per-directory
             */
            return DECLINED;
        }
        dcfg = clhy_get_capi_config(r->per_dir_config, &privileges_capi);
        fork_req = (dcfg->mode == PRIV_SECURE);
    }

    if (fork_req) {
       rv = kuda_proc_fork(&proc, r->pool);
        switch (rv) {
        case KUDA_INPARENT:
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02140)
                          "parent waiting for child");
            /* FIXME - does the child need to run synchronously?
             * esp. if we enable capi_privileges with threaded cLMPs?
             * We do need at least to ensure r outlives the child.
             */
            rv = kuda_proc_wait(&proc, &exitcode, &exitwhy, KUDA_WAIT);
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02141) "parent: child %s",
                          (rv == KUDA_CHILD_DONE) ? "done" : "notdone");

            /* The child has taken responsibility for reading all input
             * and sending all output.  So we need to bow right out,
             * and even abandon "normal" housekeeping.
             */
            r->eos_sent = 1;
            kuda_table_unset(r->headers_in, "Content-Type");
            kuda_table_unset(r->headers_in, "Content-Length");
            /* Testing with ab and 100k requests reveals no nasties
             * so I infer we're not leaking anything like memory
             * or file descriptors.  That's nice!
             */
            return DONE;
        case KUDA_INCHILD:
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02142) "In child!");
            break;  /* now we'll drop privileges in the child */
        default:
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02143)
                          "Failed to fork secure child process!");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
    }

    /* OK, now drop privileges. */

    /* cleanup should happen even if something fails part-way through here */
    kuda_pool_cleanup_register(r->pool, r, privileges_end_req,
                              kuda_pool_cleanup_null);
    /* set user and group if configured */
    if (cfg->uid || cfg->gid) {
        if (setppriv(PRIV_ON, PRIV_EFFECTIVE, priv_setid) == -1) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02144)
                          "No privilege to set user/group");
        }
        /* if we should be able to set these but can't, it could be
         * a serious security issue.  Bail out rather than risk it!
         */
        if (cfg->uid && (setuid(cfg->uid) == -1)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02145)
                          "Error setting userid");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
        if (cfg->gid && (setgid(cfg->gid) == -1)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02146)
                          "Error setting group");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
    }
    /* set vhost's privileges */
    if (setppriv(PRIV_SET, PRIV_EFFECTIVE, cfg->priv) == -1) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, errno, r, CLHYLOGNO(02147)
                      "Error setting effective privileges");
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    /* ... including those of any subprocesses */
    if (setppriv(PRIV_SET, PRIV_INHERITABLE, cfg->child_priv) == -1) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, errno, r, CLHYLOGNO(02148)
                      "Error setting inheritable privileges");
        return HTTP_INTERNAL_SERVER_ERROR;
    }
    if (setppriv(PRIV_SET, PRIV_LIMIT, cfg->child_priv) == -1) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, errno, r, CLHYLOGNO(02149)
                      "Error setting limit privileges");
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    /* If we're in a child process, drop down PPERM too */
    if (fork_req) {
        if (setppriv(PRIV_SET, PRIV_PERMITTED, cfg->priv) == -1) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, errno, r, CLHYLOGNO(02150)
                          "Error setting permitted privileges");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
    }

    return OK;
}
#define PDROP_CHECK(x) if (x == -1) { \
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, errno, s, CLHYLOGNO(02151) \
                     "Error dropping privileges"); \
        return !OK; \
    }

static int privileges_drop_first(kuda_pool_t *pool, server_rec *s)
{
    /* We need to set privileges before capi_unixd,
     * 'cos otherwise setuid will wipe our privilege to do so
     */
    priv_cfg *spcfg;
    server_rec *sp;
    priv_set_t *ppriv = priv_allocset();

    /* compute ppriv from the union of all the vhosts plus setid */
    priv_copyset(priv_setid, ppriv);
    for (sp = s; sp != NULL; sp=sp->next) {
        spcfg = clhy_get_capi_config(sp->capi_config, &privileges_capi);
        priv_union(spcfg->priv, ppriv);
    }
    PDROP_CHECK(setppriv(PRIV_SET, PRIV_PERMITTED, ppriv))
    PDROP_CHECK(setppriv(PRIV_SET, PRIV_EFFECTIVE, ppriv))
    priv_freeset(ppriv);

    return OK;
}
static int privileges_drop_last(kuda_pool_t *pool, server_rec *s)
{
    /* Our config stuff has set the privileges we need, so now
     * we just set them to those of the parent server_rec
     *
     * This has to happen after capi_unixd, 'cos capi_unixd needs
     * privileges we drop here.
     */
    priv_cfg *cfg = clhy_get_capi_config(s->capi_config, &privileges_capi);

    /* defaults - the default vhost */
    PDROP_CHECK(setppriv(PRIV_SET, PRIV_LIMIT, cfg->child_priv))
    PDROP_CHECK(setppriv(PRIV_SET, PRIV_INHERITABLE, cfg->child_priv))
    PDROP_CHECK(setppriv(PRIV_SET, PRIV_EFFECTIVE, cfg->priv))

    return OK;
}
static kuda_status_t privileges_term(void *rec)
{
    priv_freeset(priv_setid);
    return KUDA_SUCCESS;
}
static int privileges_postconf(kuda_pool_t *pconf, kuda_pool_t *plog,
                               kuda_pool_t *ptemp, server_rec *s)
{
    priv_cfg *cfg;
    server_rec *sp;

    /* if we have dtrace enabled, merge it into everything */
    if (dtrace_enabled) {
        for (sp = s; sp != NULL; sp = sp->next) {
            cfg = clhy_get_capi_config(sp->capi_config, &privileges_capi);
            CR_CHECK(priv_addset(cfg->priv, PRIV_DTRACE_KERNEL), CLHYLOGNO(03168));
            CR_CHECK(priv_addset(cfg->priv, PRIV_DTRACE_PROC), CLHYLOGNO(03169));
            CR_CHECK(priv_addset(cfg->priv, PRIV_DTRACE_USER), CLHYLOGNO(03170));
            CR_CHECK(priv_addset(cfg->child_priv, PRIV_DTRACE_KERNEL), CLHYLOGNO(03171));
            CR_CHECK(priv_addset(cfg->child_priv, PRIV_DTRACE_PROC), CLHYLOGNO(03172));
            CR_CHECK(priv_addset(cfg->child_priv, PRIV_DTRACE_USER), CLHYLOGNO(03173));
        }
        CR_CHECK(priv_addset(priv_default, PRIV_DTRACE_KERNEL), CLHYLOGNO(03174));
        CR_CHECK(priv_addset(priv_default, PRIV_DTRACE_PROC), CLHYLOGNO(03175));
        CR_CHECK(priv_addset(priv_default, PRIV_DTRACE_USER), CLHYLOGNO(03176));
    }

    /* set up priv_setid for per-request use */
    priv_setid = priv_allocset();
    kuda_pool_cleanup_register(pconf, NULL, privileges_term,
                              kuda_pool_cleanup_null);
    priv_emptyset(priv_setid);
    if (priv_addset(priv_setid, PRIV_PROC_SETID) == -1) {
        clhy_log_perror(CLHYLOG_MARK, CLHYLOG_CRIT, errno, ptemp, CLHYLOGNO(02152)
                      "priv_addset");
        return !OK;
    }
    return OK;
}
static int privileges_init(kuda_pool_t *pconf, kuda_pool_t *plog,
                           kuda_pool_t *ptemp)
{
    /* refuse to work if the cLMP is threaded */
    int threaded;
    int rv = clhy_clmp_query(CLHY_CLMPQ_IS_THREADED, &threaded);
    if (rv != KUDA_SUCCESS) {
        clhy_log_perror(CLHYLOG_MARK, CLHYLOG_NOTICE, rv, ptemp, CLHYLOGNO(02153)
                      "capi_privileges: unable to determine cLMP characteristics."
                      "  Please ensure you are using a non-threaded cLMP "
                      "with this cAPI.");
    }
    if (threaded) {
        clhy_log_perror(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ptemp, CLHYLOGNO(02154)
                      "capi_privileges is not compatible with a threaded cLMP.");
        return !OK;
    }
    return OK;
}
static void privileges_hooks(kuda_pool_t *pool)
{
    clhy_hook_post_read_request(privileges_req, NULL, NULL,
                              KUDA_HOOK_REALLY_FIRST);
    clhy_hook_header_parser(privileges_req, NULL, NULL, KUDA_HOOK_REALLY_FIRST);
    clhy_hook_drop_privileges(privileges_drop_first, NULL, NULL, KUDA_HOOK_FIRST);
    clhy_hook_drop_privileges(privileges_drop_last, NULL, NULL, KUDA_HOOK_LAST);
    clhy_hook_post_config(privileges_postconf, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_pre_config(privileges_init, NULL, NULL, KUDA_HOOK_FIRST);
}

static const char *vhost_user(cmd_parms *cmd, void *dir, const char *arg)
{
    priv_cfg *cfg = clhy_get_capi_config(cmd->server->capi_config,
                                         &privileges_capi);
    cfg->uid = clhy_uname2id(arg);
    if (cfg->uid == 0) {
        return kuda_pstrcat(cmd->pool, "Invalid userid for VHostUser: ",
                           arg, NULL);
    }
    return NULL;
}
static const char *vhost_group(cmd_parms *cmd, void *dir, const char *arg)
{
    priv_cfg *cfg = clhy_get_capi_config(cmd->server->capi_config,
                                         &privileges_capi);
    cfg->gid = clhy_gname2id(arg);
    if (cfg->uid == 0) {
        return kuda_pstrcat(cmd->pool, "Invalid groupid for VHostGroup: ",
                           arg, NULL);
    }
    return NULL;
}
static const char *vhost_secure(cmd_parms *cmd, void *dir, int arg)
{
    priv_cfg *cfg = clhy_get_capi_config(cmd->server->capi_config,
                                         &privileges_capi);
    if (!arg) {
        /* add basic privileges, excluding those covered by cgimode */
        CFG_CHECK(priv_addset(cfg->priv, PRIV_FILE_LINK_ANY));
        CFG_CHECK(priv_addset(cfg->priv, PRIV_PROC_INFO));
        CFG_CHECK(priv_addset(cfg->priv, PRIV_PROC_SESSION));
    }
    return NULL;
}
static const char *vhost_cgimode(cmd_parms *cmd, void *dir, const char *arg)
{
    priv_cfg *cfg = clhy_get_capi_config(cmd->server->capi_config,
                                         &privileges_capi);
    if (!strcasecmp(arg, "on")) {
        /* default - nothing to do */
    }
    else if (!strcasecmp(arg, "off")) {
        /* drop fork+exec privs */
        CFG_CHECK(priv_delset(cfg->priv, PRIV_PROC_FORK));
        CFG_CHECK(priv_delset(cfg->priv, PRIV_PROC_EXEC));
    }
    else if (!strcasecmp(arg, "secure")) {
        /* deny privileges to CGI procs */
        CFG_CHECK(priv_delset(cfg->child_priv, PRIV_PROC_FORK));
        CFG_CHECK(priv_delset(cfg->child_priv, PRIV_PROC_EXEC));
        CFG_CHECK(priv_delset(cfg->child_priv, PRIV_FILE_LINK_ANY));
        CFG_CHECK(priv_delset(cfg->child_priv, PRIV_PROC_INFO));
        CFG_CHECK(priv_delset(cfg->child_priv, PRIV_PROC_SESSION));
    }
    else {
        return "VHostCGIMode must be On, Off or Secure";
    }

    return NULL;
}
static const char *dtraceenable(cmd_parms *cmd, void *dir, int arg)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err != NULL) {
        return err;
    }
    dtrace_enabled = arg;
    return NULL;
}

static const char *privs_mode(cmd_parms *cmd, void *dir, const char *arg)
{
    priv_mode mode = PRIV_UNSET;
    if (!strcasecmp(arg, "FAST")) {
        mode = PRIV_FAST;
    }
    else if (!strcasecmp(arg, "SECURE")) {
        mode = PRIV_SECURE;
    }
    else if (!strcasecmp(arg, "SELECTIVE")) {
        mode = PRIV_SELECTIVE;
    }

    if (cmd->path) {
        /* In a directory context, set the per_dir_config */
        priv_dir_cfg *cfg = dir;
        cfg->mode = mode;
        if ((mode == PRIV_UNSET) || (mode == PRIV_SELECTIVE)) {
            return "PrivilegesMode in a Directory context must be FAST or SECURE";
        }
    }
    else {
        /* In a global or vhost context, set the server config */
        priv_cfg *cfg = clhy_get_capi_config(cmd->server->capi_config,
                                             &privileges_capi);
        cfg->mode = mode;
        if (mode == PRIV_UNSET) {
            return "PrivilegesMode must be FAST, SECURE or SELECTIVE";
        }
    }
    return NULL;
}

#ifdef BIG_SECURITY_HOLE
static const char *vhost_privs(cmd_parms *cmd, void *dir, const char *arg)
{
    priv_cfg *cfg = clhy_get_capi_config(cmd->server->capi_config,
                                         &privileges_capi);
    const char *priv = arg;

    if (*priv == '-') {
        CFG_CHECK(priv_delset(cfg->priv, priv+1));
    }
    else if (*priv == '+') {
        CFG_CHECK(priv_addset(cfg->priv, priv+1));
    }
    else {
        priv_emptyset(cfg->priv);
        CFG_CHECK(priv_addset(cfg->priv, priv));
    }
    return NULL;
}
static const char *vhost_cgiprivs(cmd_parms *cmd, void *dir, const char *arg)
{
    priv_cfg *cfg = clhy_get_capi_config(cmd->server->capi_config,
                                         &privileges_capi);
    const char *priv = arg;
    if (*priv == '-') {
        CFG_CHECK(priv_delset(cfg->child_priv, priv+1));
    }
    else if (*priv == '+') {
        CFG_CHECK(priv_addset(cfg->child_priv, priv+1));
    }
    else {
        priv_emptyset(cfg->child_priv);
        CFG_CHECK(priv_addset(cfg->child_priv, priv));
    }
    return NULL;
}
#endif
static const command_rec privileges_cmds[] = {
    CLHY_INIT_TAKE1("VHostUser", vhost_user, NULL, RSRC_CONF,
                  "Userid under which the virtualhost will run"),
    CLHY_INIT_TAKE1("VHostGroup", vhost_group, NULL, RSRC_CONF,
                  "Group under which the virtualhost will run"),
    CLHY_INIT_FLAG("VHostSecure", vhost_secure, NULL, RSRC_CONF,
                 "Run in enhanced security mode (default ON)"),
    CLHY_INIT_TAKE1("VHostCGIMode", vhost_cgimode, NULL, RSRC_CONF,
                  "Enable fork+exec for this virtualhost (Off|Secure|On)"),
    CLHY_INIT_FLAG("DTracePrivileges", dtraceenable, NULL, RSRC_CONF,
                 "Enable DTrace"),
    CLHY_INIT_TAKE1("PrivilegesMode", privs_mode, NULL, RSRC_CONF|ACCESS_CONF,
                  "tradeoff performance vs security (fast or secure)"),
#ifdef BIG_SECURITY_HOLE
    CLHY_INIT_ITERATE("VHostPrivs", vhost_privs, NULL, RSRC_CONF,
                    "Privileges available in the (virtual) server"),
    CLHY_INIT_ITERATE("VHostCGIPrivs", vhost_cgiprivs, NULL, RSRC_CONF,
                    "Privileges available to external programs"),
#endif
    {NULL}
};
CLHY_DECLARE_CAPI(privileges) = {
    STANDARD16_CAPI_STUFF,
    privileges_create_dir_cfg,
    privileges_merge_dir_cfg,
    privileges_create_cfg,
    privileges_merge_cfg,
    privileges_cmds,
    privileges_hooks
};
