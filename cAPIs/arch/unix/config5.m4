
CLHYKUDEL_CAPIPATH_INIT(arch/unix)

if clhy_clmp_is_enabled "worker" \
   || clhy_clmp_is_enabled "event" \
   || clhy_clmp_is_enabled "prefork"; then
    unixd_capis_enable=yes
else
    unixd_capis_enable=no
fi

CLHYKUDEL_CAPI(unixd, unix specific support, , , $unixd_capis_enable)
CLHYKUDEL_CAPI(privileges, Per-virtualhost Unix UserIDs and enhanced security for Solaris, , , no, [
  AC_CHECK_HEADERS(priv.h, [clhy_HAVE_PRIV_H="yes"], [clhy_HAVE_PRIV_H="no"])
  if test $clhy_HAVE_PRIV_H = "no"; then
    AC_MSG_WARN([Your system does not support privileges.])
    enable_privileges="no"
  fi
])

KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

CLHYKUDEL_CAPIPATH_FINISH

