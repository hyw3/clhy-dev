/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_protocol.h"
#include "http_request.h"
#include "http_log.h"
#include "util_script.h"
#include "capi_core.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_buckets.h"
#include "kuda_thread_mutex.h"
#include "kuda_thread_rwlock.h"
#include "kuda_hash.h"
#include "capi_isapi.h"

/* Retry frequency for a failed-to-load isapi .dll */
#define ISAPI_RETRY kuda_time_from_sec(30)

/**********************************************************
 *
 *  ISAPI cAPI Configuration
 *
 **********************************************************/

cAPI CLHY_CAPI_DECLARE_DATA isapi_capi;

#define ISAPI_UNDEF -1

/* Our isapi per-dir config structure */
typedef struct isapi_dir_conf {
    int read_ahead_buflen;
    int log_unsupported;
    int log_to_errlog;
    int log_to_query;
    int fake_async;
} isapi_dir_conf;

typedef struct isapi_loaded isapi_loaded;

kuda_status_t isapi_lookup(kuda_pool_t *p, server_rec *s, request_rec *r,
                          const char *fpath, isapi_loaded** isa);

static void *create_isapi_dir_config(kuda_pool_t *p, char *dummy)
{
    isapi_dir_conf *dir = kuda_palloc(p, sizeof(isapi_dir_conf));

    dir->read_ahead_buflen = ISAPI_UNDEF;
    dir->log_unsupported   = ISAPI_UNDEF;
    dir->log_to_errlog     = ISAPI_UNDEF;
    dir->log_to_query      = ISAPI_UNDEF;
    dir->fake_async        = ISAPI_UNDEF;

    return dir;
}

static void *merge_isapi_dir_configs(kuda_pool_t *p, void *base_, void *add_)
{
    isapi_dir_conf *base = (isapi_dir_conf *) base_;
    isapi_dir_conf *add = (isapi_dir_conf *) add_;
    isapi_dir_conf *dir = kuda_palloc(p, sizeof(isapi_dir_conf));

    dir->read_ahead_buflen = (add->read_ahead_buflen == ISAPI_UNDEF)
                                ? base->read_ahead_buflen
                                 : add->read_ahead_buflen;
    dir->log_unsupported   = (add->log_unsupported == ISAPI_UNDEF)
                                ? base->log_unsupported
                                 : add->log_unsupported;
    dir->log_to_errlog     = (add->log_to_errlog == ISAPI_UNDEF)
                                ? base->log_to_errlog
                                 : add->log_to_errlog;
    dir->log_to_query      = (add->log_to_query == ISAPI_UNDEF)
                                ? base->log_to_query
                                 : add->log_to_query;
    dir->fake_async        = (add->fake_async == ISAPI_UNDEF)
                                ? base->fake_async
                                 : add->fake_async;

    return dir;
}

static const char *isapi_cmd_cachefile(cmd_parms *cmd, void *dummy,
                                       const char *filename)
{
    isapi_loaded *isa;
    kuda_finfo_t tmp;
    kuda_status_t rv;
    char *fspec;

    /* ### Just an observation ... it would be terribly cool to be
     * able to use this per-dir, relative to the directory block being
     * defined.  The hash result remains global, but shorthand of
     * <Directory "c:/webapps/isapi">
     *     ISAPICacheFile myapp.dll anotherapp.dll thirdapp.dll
     * </Directory>
     * would be very convienent.
     */
    fspec = clhy_server_root_relative(cmd->pool, filename);
    if (!fspec) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, KUDA_EBADPATH, cmd->server, CLHYLOGNO(02103)
                     "invalid cAPI path, skipping %s", filename);
        return NULL;
    }
    if ((rv = kuda_stat(&tmp, fspec, KUDA_FINFO_TYPE,
                      cmd->temp_pool)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rv, cmd->server, CLHYLOGNO(02104)
                     "unable to stat, skipping %s", fspec);
        return NULL;
    }
    if (tmp.filetype != KUDA_REG) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, cmd->server, CLHYLOGNO(02105)
                     "not a regular file, skipping %s", fspec);
        return NULL;
    }

    /* Load the extension as cached (with null request_rec) */
    rv = isapi_lookup(cmd->pool, cmd->server, NULL, fspec, &isa);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rv, cmd->server, CLHYLOGNO(02106)
                     "unable to cache, skipping %s", fspec);
        return NULL;
    }

    return NULL;
}

static const command_rec isapi_cmds[] = {
    CLHY_INIT_TAKE1("ISAPIReadAheadBuffer", clhy_set_int_slot,
        (void *)KUDA_OFFSETOF(isapi_dir_conf, read_ahead_buflen),
        OR_FILEINFO, "Maximum client request body to initially pass to the"
                     " ISAPI handler (default: 49152)"),
    CLHY_INIT_FLAG("ISAPILogNotSupported", clhy_set_flag_slot,
        (void *)KUDA_OFFSETOF(isapi_dir_conf, log_unsupported),
        OR_FILEINFO, "Log requests not supported by the ISAPI server"
                     " on or off (default: off)"),
    CLHY_INIT_FLAG("ISAPIAppendLogToErrors", clhy_set_flag_slot,
        (void *)KUDA_OFFSETOF(isapi_dir_conf, log_to_errlog),
        OR_FILEINFO, "Send all Append Log requests to the error log"
                     " on or off (default: off)"),
    CLHY_INIT_FLAG("ISAPIAppendLogToQuery", clhy_set_flag_slot,
        (void *)KUDA_OFFSETOF(isapi_dir_conf, log_to_query),
        OR_FILEINFO, "Append Log requests are concatinated to the query args"
                     " on or off (default: on)"),
    CLHY_INIT_FLAG("ISAPIFakeAsync", clhy_set_flag_slot,
        (void *)KUDA_OFFSETOF(isapi_dir_conf, fake_async),
        OR_FILEINFO, "Fake Asynchronous support for isapi callbacks"
                     " on or off [Experimental] (default: off)"),
    CLHY_INIT_ITERATE("ISAPICacheFile", isapi_cmd_cachefile, NULL,
        RSRC_CONF, "Cache the specified ISAPI extension in-process"),
    {NULL}
};

/**********************************************************
 *
 *  ISAPI cAPI Cache handling section
 *
 **********************************************************/

/* Our isapi global config values */
static struct isapi_global_conf {
    kuda_pool_t         *pool;
    kuda_thread_mutex_t *lock;
    kuda_hash_t         *hash;
} loaded;

/* Our loaded isapi cAPI description structure */
struct isapi_loaded {
    const char          *filename;
    kuda_thread_rwlock_t *in_progress;
    kuda_status_t         last_load_rv;
    kuda_time_t           last_load_time;
    kuda_dso_handle_t    *handle;
    HSE_VERSION_INFO    *isapi_version;
    kuda_uint32_t         report_version;
    kuda_uint32_t         timeout;
    PFN_GETEXTENSIONVERSION GetExtensionVersion;
    PFN_HTTPEXTENSIONPROC   HttpExtensionProc;
    PFN_TERMINATEEXTENSION  TerminateExtension;
};

static kuda_status_t isapi_unload(isapi_loaded *isa, int force)
{
    /* All done with the DLL... get rid of it...
     *
     * If optionally cached, and we weren't asked to force the unload,
     * pass HSE_TERM_ADVISORY_UNLOAD, and if it returns 1, unload,
     * otherwise, leave it alone (it didn't choose to cooperate.)
     */
    if (!isa->handle) {
        return KUDA_SUCCESS;
    }
    if (isa->TerminateExtension) {
        if (force) {
            (*isa->TerminateExtension)(HSE_TERM_MUST_UNLOAD);
        }
        else if (!(*isa->TerminateExtension)(HSE_TERM_ADVISORY_UNLOAD)) {
            return KUDA_EGENERAL;
        }
    }
    kuda_dso_unload(isa->handle);
    isa->handle = NULL;
    return KUDA_SUCCESS;
}

static kuda_status_t cleanup_isapi(void *isa_)
{
    isapi_loaded* isa = (isapi_loaded*) isa_;

    /* We must force the cAPI to unload, we are about
     * to lose the isapi structure's allocation entirely.
     */
    return isapi_unload(isa, 1);
}

static kuda_status_t isapi_load(kuda_pool_t *p, server_rec *s, isapi_loaded *isa)
{
    kuda_status_t rv;

    isa->isapi_version = kuda_pcalloc(p, sizeof(HSE_VERSION_INFO));

    /* TODO: These aught to become overrideable, so that we
     * assure a given isapi can be fooled into behaving well.
     *
     * The tricky bit, they aren't really a per-dir sort of
     * config, they will always be constant across every
     * reference to the .dll no matter what context (vhost,
     * location, etc) they apply to.
     */
    isa->report_version = 0x500; /* Revision 5.0 */
    isa->timeout = 300 * 1000000; /* microsecs, not used */

    rv = kuda_dso_load(&isa->handle, isa->filename, p);
    if (rv)
    {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(02107)
                     "failed to load %s", isa->filename);
        isa->handle = NULL;
        return rv;
    }

    rv = kuda_dso_sym((void**)&isa->GetExtensionVersion, isa->handle,
                     "GetExtensionVersion");
    if (rv)
    {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(02108)
                     "missing GetExtensionVersion() in %s",
                     isa->filename);
        kuda_dso_unload(isa->handle);
        isa->handle = NULL;
        return rv;
    }

    rv = kuda_dso_sym((void**)&isa->HttpExtensionProc, isa->handle,
                     "HttpExtensionProc");
    if (rv)
    {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(02109)
                     "missing HttpExtensionProc() in %s",
                     isa->filename);
        kuda_dso_unload(isa->handle);
        isa->handle = NULL;
        return rv;
    }

    /* TerminateExtension() is an optional interface */
    rv = kuda_dso_sym((void**)&isa->TerminateExtension, isa->handle,
                     "TerminateExtension");
    kuda_set_platform_error(0);

    /* Run GetExtensionVersion() */
    if (!(isa->GetExtensionVersion)(isa->isapi_version)) {
        kuda_status_t rv = kuda_get_platform_error();
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(02110)
                     "failed call to GetExtensionVersion() in %s",
                     isa->filename);
        kuda_dso_unload(isa->handle);
        isa->handle = NULL;
        return rv;
    }

    kuda_pool_cleanup_register(p, isa, cleanup_isapi,
                              kuda_pool_cleanup_null);

    return KUDA_SUCCESS;
}

kuda_status_t isapi_lookup(kuda_pool_t *p, server_rec *s, request_rec *r,
                          const char *fpath, isapi_loaded** isa)
{
    kuda_status_t rv;
    const char *key;

    if ((rv = kuda_thread_mutex_lock(loaded.lock)) != KUDA_SUCCESS) {
        return rv;
    }

    *isa = kuda_hash_get(loaded.hash, fpath, KUDA_HASH_KEY_STRING);

    if (*isa) {

        /* If we find this lock exists, use a set-aside copy of gainlock
         * to avoid race conditions on NULLing the in_progress variable
         * when the load has completed.  Release the global isapi hash
         * lock so other requests can proceed, then rdlock for completion
         * of loading our desired dll or wrlock if we would like to retry
         * loading the dll (because last_load_rv failed and retry is up.)
         */
        kuda_thread_rwlock_t *gainlock = (*isa)->in_progress;

        /* gainlock is NULLed after the cAPI loads successfully.
         * This free-threaded cAPI can be used without any locking.
         */
        if (!gainlock) {
            rv = (*isa)->last_load_rv;
            kuda_thread_mutex_unlock(loaded.lock);
            return rv;
        }


        if ((*isa)->last_load_rv == KUDA_SUCCESS) {
            kuda_thread_mutex_unlock(loaded.lock);
            if ((rv = kuda_thread_rwlock_rdlock(gainlock))
                    != KUDA_SUCCESS) {
                return rv;
            }
            rv = (*isa)->last_load_rv;
            kuda_thread_rwlock_unlock(gainlock);
            return rv;
        }

        if (kuda_time_now() > (*isa)->last_load_time + ISAPI_RETRY) {

            /* Remember last_load_time before releasing the global
             * hash lock to avoid colliding with another thread
             * that hit this exception at the same time as our
             * retry attempt, since we unlock the global mutex
             * before attempting a write lock for this cAPI.
             */
            kuda_time_t check_time = (*isa)->last_load_time;
            kuda_thread_mutex_unlock(loaded.lock);

            if ((rv = kuda_thread_rwlock_wrlock(gainlock))
                    != KUDA_SUCCESS) {
                return rv;
            }

            /* If last_load_time is unchanged, we still own this
             * retry, otherwise presume another thread provided
             * our retry (for good or ill).  Relock the global
             * hash for updating last_load_ vars, so their update
             * is always atomic to the global lock.
             */
            if (check_time == (*isa)->last_load_time) {

                rv = isapi_load(loaded.pool, s, *isa);

                kuda_thread_mutex_lock(loaded.lock);
                (*isa)->last_load_rv = rv;
                (*isa)->last_load_time = kuda_time_now();
                kuda_thread_mutex_unlock(loaded.lock);
            }
            else {
                rv = (*isa)->last_load_rv;
            }
            kuda_thread_rwlock_unlock(gainlock);

            return rv;
        }

        /* We haven't hit timeup on retry, let's grab the last_rv
         * within the hash mutex before unlocking.
         */
        rv = (*isa)->last_load_rv;
        kuda_thread_mutex_unlock(loaded.lock);

        return rv;
    }

    /* If the cAPI was not found, it's time to create a hash key entry
     * before releasing the hash lock to avoid multiple threads from
     * loading the same cAPI.
     */
    key = kuda_pstrdup(loaded.pool, fpath);
    *isa = kuda_pcalloc(loaded.pool, sizeof(isapi_loaded));
    (*isa)->filename = key;
    if (r) {
        /* A mutex that exists only long enough to attempt to
         * load this isapi dll, the release this cAPI to all
         * other takers that came along during the one-time
         * load process.  Short lifetime for this lock would
         * be great, however, using r->pool is nasty if those
         * blocked on the lock haven't all unlocked before we
         * attempt to destroy.  A nastier race condition than
         * I want to deal with at this moment...
         */
        kuda_thread_rwlock_create(&(*isa)->in_progress, loaded.pool);
        kuda_thread_rwlock_wrlock((*isa)->in_progress);
    }

    kuda_hash_set(loaded.hash, key, KUDA_HASH_KEY_STRING, *isa);

    /* Now attempt to load the isapi on our own time,
     * allow other isapi processing to resume.
     */
    kuda_thread_mutex_unlock(loaded.lock);

    rv = isapi_load(loaded.pool, s, *isa);
    (*isa)->last_load_time = kuda_time_now();
    (*isa)->last_load_rv = rv;

    if (r && (rv == KUDA_SUCCESS)) {
        /* Let others who are blocked on this particular
         * cAPI resume their requests, for better or worse.
         */
        kuda_thread_rwlock_t *unlock = (*isa)->in_progress;
        (*isa)->in_progress = NULL;
        kuda_thread_rwlock_unlock(unlock);
    }
    else if (!r && (rv != KUDA_SUCCESS)) {
        /* We must leave a rwlock around for requests to retry
         * loading this dll after timeup... since we were in
         * the setup code we had avoided creating this lock.
         */
        kuda_thread_rwlock_create(&(*isa)->in_progress, loaded.pool);
    }

    return (*isa)->last_load_rv;
}

/**********************************************************
 *
 *  ISAPI cAPI request callbacks section
 *
 **********************************************************/

/* Our "Connection ID" structure */
struct isapi_cid {
    EXTENSION_CONTROL_BLOCK *ecb;
    isapi_dir_conf           dconf;
    isapi_loaded            *isa;
    request_rec             *r;
    int                      headers_set;
    int                      response_sent;
    PFN_HSE_IO_COMPLETION    completion;
    void                    *completion_arg;
    kuda_thread_mutex_t      *completed;
};

static int KUDA_THREAD_FUNC regfnGetServerVariable(isapi_cid    *cid,
                                                  char         *variable_name,
                                                  void         *buf_ptr,
                                                  kuda_uint32_t *buf_size)
{
    request_rec *r = cid->r;
    const char *result;
    char *buf_data = (char*)buf_ptr;
    kuda_uint32_t len;

    if (!strcmp(variable_name, "ALL_HTTP"))
    {
        /* crlf delimited, colon split, comma separated and
         * null terminated list of HTTP_ vars
         */
        const kuda_array_header_t *arr = kuda_table_elts(r->subprocess_env);
        const kuda_table_entry_t *elts = (const kuda_table_entry_t *)arr->elts;
        int i;

        for (len = 0, i = 0; i < arr->nelts; i++) {
            if (!strncmp(elts[i].key, "HTTP_", 5)) {
                len += strlen(elts[i].key) + strlen(elts[i].val) + 3;
            }
        }

        if (*buf_size < len + 1) {
            *buf_size = len + 1;
            kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INSUFFICIENT_BUFFER));
            return 0;
        }

        for (i = 0; i < arr->nelts; i++) {
            if (!strncmp(elts[i].key, "HTTP_", 5)) {
                strcpy(buf_data, elts[i].key);
                buf_data += strlen(elts[i].key);
                *(buf_data++) = ':';
                strcpy(buf_data, elts[i].val);
                buf_data += strlen(elts[i].val);
                *(buf_data++) = '\r';
                *(buf_data++) = '\n';
            }
        }

        *(buf_data++) = '\0';
        *buf_size = len + 1;
        return 1;
    }

    if (!strcmp(variable_name, "ALL_RAW"))
    {
        /* crlf delimited, colon split, comma separated and
         * null terminated list of the raw request header
         */
        const kuda_array_header_t *arr = kuda_table_elts(r->headers_in);
        const kuda_table_entry_t *elts = (const kuda_table_entry_t *)arr->elts;
        int i;

        for (len = 0, i = 0; i < arr->nelts; i++) {
            len += strlen(elts[i].key) + strlen(elts[i].val) + 4;
        }

        if (*buf_size < len + 1) {
            *buf_size = len + 1;
            kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INSUFFICIENT_BUFFER));
            return 0;
        }

        for (i = 0; i < arr->nelts; i++) {
            strcpy(buf_data, elts[i].key);
            buf_data += strlen(elts[i].key);
            *(buf_data++) = ':';
            *(buf_data++) = ' ';
            strcpy(buf_data, elts[i].val);
            buf_data += strlen(elts[i].val);
            *(buf_data++) = '\r';
            *(buf_data++) = '\n';
        }
        *(buf_data++) = '\0';
        *buf_size = len + 1;
        return 1;
    }

    /* Not a special case */
    result = kuda_table_get(r->subprocess_env, variable_name);

    if (result) {
        len = strlen(result);
        if (*buf_size < len + 1) {
            *buf_size = len + 1;
            kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INSUFFICIENT_BUFFER));
            return 0;
        }
        strcpy(buf_data, result);
        *buf_size = len + 1;
        return 1;
    }

    /* Not Found */
    kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_INDEX));
    return 0;
}

static int KUDA_THREAD_FUNC regfnReadClient(isapi_cid    *cid,
                                           void         *buf_data,
                                           kuda_uint32_t *buf_size)
{
    request_rec *r = cid->r;
    kuda_uint32_t read = 0;
    int res = 0;

    if (r->remaining < *buf_size) {
        *buf_size = (kuda_size_t)r->remaining;
    }

    while (read < *buf_size &&
           ((res = clhy_get_client_block(r, (char*)buf_data + read,
                                       *buf_size - read)) > 0)) {
        read += res;
    }

    *buf_size = read;
    if (res < 0) {
        kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_READ_FAULT));
    }
    return (res >= 0);
}

/* Common code invoked for both HSE_REQ_SEND_RESPONSE_HEADER and
 * the newer HSE_REQ_SEND_RESPONSE_HEADER_EX ServerSupportFunction(s)
 * as well as other functions that write responses and presume that
 * the support functions above are optional.
 *
 * Other callers trying to split headers and body bytes should pass
 * head/headlen alone (leaving stat/statlen NULL/0), so that they
 * get a proper count of bytes consumed.  The argument passed to stat
 * isn't counted as the head bytes are.
 */
static kuda_ssize_t send_response_header(isapi_cid *cid,
                                        const char *stat,
                                        const char *head,
                                        kuda_size_t statlen,
                                        kuda_size_t headlen)
{
    int head_present = 1;
    int termarg;
    int res;
    int old_status;
    const char *termch;
    kuda_size_t ate = 0;

    if (!head || headlen == 0 || !*head) {
        head = stat;
        stat = NULL;
        headlen = statlen;
        statlen = 0;
        head_present = 0; /* Don't eat the header */
    }

    if (!stat || statlen == 0 || !*stat) {
        if (head && headlen && *head && ((stat = memchr(head, '\r', headlen))
                                      || (stat = memchr(head, '\n', headlen))
                                      || (stat = memchr(head, '\0', headlen))
                                      || (stat = head + headlen))) {
            statlen = stat - head;
            if (memchr(head, ':', statlen)) {
                stat = "Status: 200 OK";
                statlen = strlen(stat);
            }
            else {
                const char *flip = head;
                head = stat;
                stat = flip;
                headlen -= statlen;
                ate += statlen;
                if (*head == '\r' && headlen)
                    ++head, --headlen, ++ate;
                if (*head == '\n' && headlen)
                    ++head, --headlen, ++ate;
            }
        }
    }

    if (stat && (statlen > 0) && *stat) {
        char *newstat;
        if (!kuda_isdigit(*stat)) {
            const char *stattok = stat;
            int toklen = statlen;
            while (toklen && *stattok && !kuda_isspace(*stattok)) {
                ++stattok; --toklen;
            }
            while (toklen && kuda_isspace(*stattok)) {
                ++stattok; --toklen;
            }
            /* Now decide if we follow the xxx message
             * or the http/x.x xxx message format
             */
            if (toklen && kuda_isdigit(*stattok)) {
                statlen = toklen;
                stat = stattok;
            }
        }
        newstat = kuda_palloc(cid->r->pool, statlen + 9);
        strcpy(newstat, "Status: ");
        kuda_cpystrn(newstat + 8, stat, statlen + 1);
        stat = newstat;
        statlen += 8;
    }

    if (!head || headlen == 0 || !*head) {
        head = "\r\n";
        headlen = 2;
    }
    else
    {
        if (head[headlen - 1] && head[headlen]) {
            /* Whoops... not NULL terminated */
            head = kuda_pstrndup(cid->r->pool, head, headlen);
        }
    }

    /* Seems IIS does not enforce the requirement for \r\n termination
     * on HSE_REQ_SEND_RESPONSE_HEADER, but we won't panic...
     * clhy_scan_script_header_err_strs handles this aspect for us.
     *
     * Parse them out, or die trying
     */
    old_status = cid->r->status;

    if (stat) {
        res = clhy_scan_script_header_err_strs_ex(cid->r, NULL,
                CLHYLOG_CAPI_INDEX, &termch, &termarg, stat, head, NULL);
    }
    else {
        res = clhy_scan_script_header_err_strs_ex(cid->r, NULL,
                CLHYLOG_CAPI_INDEX, &termch, &termarg, head, NULL);
    }

    /* Set our status. */
    if (res) {
        /* This is an immediate error result from the parser
         */
        cid->r->status = res;
        cid->r->status_line = clhy_get_status_line(cid->r->status);
        cid->ecb->dwHttpStatusCode = cid->r->status;
    }
    else if (cid->r->status) {
        /* We have a status in r->status, so let's just use it.
         * This is likely to be the Status: parsed above, and
         * may also be a delayed error result from the parser.
         * If it was filled in, status_line should also have
         * been filled in.
         */
        cid->ecb->dwHttpStatusCode = cid->r->status;
    }
    else if (cid->ecb->dwHttpStatusCode
              && cid->ecb->dwHttpStatusCode != HTTP_OK) {
        /* Now we fall back on dwHttpStatusCode if it appears
         * clhy_scan_script_header fell back on the default code.
         * Any other results set dwHttpStatusCode to the decoded
         * status value.
         */
        cid->r->status = cid->ecb->dwHttpStatusCode;
        cid->r->status_line = clhy_get_status_line(cid->r->status);
    }
    else if (old_status) {
        /* Well... either there is no dwHttpStatusCode or it's HTTP_OK.
         * In any case, we don't have a good status to return yet...
         * Perhaps the one we came in with will be better. Let's use it,
         * if we were given one (note this is a pedantic case, it would
         * normally be covered above unless the scan script code unset
         * the r->status). Should there be a check here as to whether
         * we are setting a valid response code?
         */
        cid->r->status = old_status;
        cid->r->status_line = clhy_get_status_line(cid->r->status);
        cid->ecb->dwHttpStatusCode = cid->r->status;
    }
    else {
        /* None of dwHttpStatusCode, the parser's r->status nor the
         * old value of r->status were helpful, and nothing was decoded
         * from Status: string passed to us.  Let's just say HTTP_OK
         * and get the data out, this was the isapi dev's oversight.
         */
        cid->r->status = HTTP_OK;
        cid->r->status_line = clhy_get_status_line(cid->r->status);
        cid->ecb->dwHttpStatusCode = cid->r->status;
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, cid->r, CLHYLOGNO(02111)
                "Could not determine HTTP response code; using %d",
                cid->r->status);
    }

    if (cid->r->status == HTTP_INTERNAL_SERVER_ERROR) {
        return -1;
    }

    /* If only Status was passed, we consumed nothing
     */
    if (!head_present)
        return 0;

    cid->headers_set = 1;

    /* If all went well, tell the caller we consumed the headers complete
     */
    if (!termch)
        return(ate + headlen);

    /* Any data left must be sent directly by the caller, all we
     * give back is the size of the headers we consumed (which only
     * happens if the parser got to the head arg, which varies based
     * on whether we passed stat+head to scan, or only head.
     */
    if (termch && (termarg == (stat ? 1 : 0))
               && head_present && head + headlen > termch) {
        return ate + termch - head;
    }
    return ate;
}

static int KUDA_THREAD_FUNC regfnWriteClient(isapi_cid    *cid,
                                            void         *buf_ptr,
                                            kuda_uint32_t *size_arg,
                                            kuda_uint32_t  flags)
{
    request_rec *r = cid->r;
    conn_rec *c = r->connection;
    kuda_uint32_t buf_size = *size_arg;
    char *buf_data = (char*)buf_ptr;
    kuda_bucket_brigade *bb;
    kuda_bucket *b;
    kuda_status_t rv = KUDA_SUCCESS;

    if (!cid->headers_set) {
        /* It appears that the foxisapi cAPI and other clients
         * presume that WriteClient("headers\n\nbody") will work.
         * Parse them out, or die trying.
         */
        kuda_ssize_t ate;
        ate = send_response_header(cid, NULL, buf_data, 0, buf_size);
        if (ate < 0) {
            kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
            return 0;
        }

        buf_data += ate;
        buf_size -= ate;
    }

    if (buf_size) {
        bb = kuda_brigade_create(r->pool, c->bucket_alloc);
        b = kuda_bucket_transient_create(buf_data, buf_size, c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);
        b = kuda_bucket_flush_create(c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);
        rv = clhy_pass_brigade(r->output_filters, bb);
        cid->response_sent = 1;
        if (rv != KUDA_SUCCESS)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, r, CLHYLOGNO(02984)
                          "WriteClient clhy_pass_brigade failed: %s",
                          r->filename);
    }

    if ((flags & HSE_IO_ASYNC) && cid->completion) {
        if (rv == KUDA_SUCCESS) {
            cid->completion(cid->ecb, cid->completion_arg,
                            *size_arg, ERROR_SUCCESS);
        }
        else {
            cid->completion(cid->ecb, cid->completion_arg,
                            *size_arg, ERROR_WRITE_FAULT);
        }
    }
    return (rv == KUDA_SUCCESS);
}

static int KUDA_THREAD_FUNC regfnServerSupportFunction(isapi_cid    *cid,
                                                      kuda_uint32_t  HSE_code,
                                                      void         *buf_ptr,
                                                      kuda_uint32_t *buf_size,
                                                      kuda_uint32_t *data_type)
{
    request_rec *r = cid->r;
    conn_rec *c = r->connection;
    char *buf_data = (char*)buf_ptr;
    request_rec *subreq;
    kuda_status_t rv;

    switch (HSE_code) {
    case HSE_REQ_SEND_URL_REDIRECT_RESP:
        /* Set the status to be returned when the HttpExtensionProc()
         * is done.
         * WARNING: Microsoft now advertises HSE_REQ_SEND_URL_REDIRECT_RESP
         *          and HSE_REQ_SEND_URL as equivalent per the Jan 2000 SDK.
         *          They most definitely are not, even in their own samples.
         */
        kuda_table_set (r->headers_out, "Location", buf_data);
        cid->r->status = cid->ecb->dwHttpStatusCode = HTTP_MOVED_TEMPORARILY;
        cid->r->status_line = clhy_get_status_line(cid->r->status);
        cid->headers_set = 1;
        return 1;

    case HSE_REQ_SEND_URL:
        /* Soak up remaining input */
        if (r->remaining > 0) {
            char argsbuffer[HUGE_STRING_LEN];
            while (clhy_get_client_block(r, argsbuffer, HUGE_STRING_LEN));
        }

        /* Reset the method to GET */
        r->method = "GET";
        r->method_number = M_GET;

        /* Don't let anyone think there's still data */
        kuda_table_unset(r->headers_in, "Content-Length");

        /* AV fault per PR3598 - redirected path is lost! */
        buf_data = kuda_pstrdup(r->pool, (char*)buf_data);
        clhy_internal_redirect(buf_data, r);
        return 1;

    case HSE_REQ_SEND_RESPONSE_HEADER:
    {
        /* Parse them out, or die trying */
        kuda_size_t statlen = 0, headlen = 0;
        kuda_ssize_t ate;
        if (buf_data)
            statlen = strlen((char*) buf_data);
        if (data_type)
            headlen = strlen((char*) data_type);
        ate = send_response_header(cid, (char*) buf_data,
                                   (char*) data_type,
                                   statlen, headlen);
        if (ate < 0) {
            kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
            return 0;
        }
        else if ((kuda_size_t)ate < headlen) {
            kuda_bucket_brigade *bb;
            kuda_bucket *b;
            bb = kuda_brigade_create(cid->r->pool, c->bucket_alloc);
            b = kuda_bucket_transient_create((char*) data_type + ate,
                                           headlen - ate, c->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(bb, b);
            b = kuda_bucket_flush_create(c->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(bb, b);
            rv = clhy_pass_brigade(cid->r->output_filters, bb);
            cid->response_sent = 1;
            if (rv != KUDA_SUCCESS)
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, r, CLHYLOGNO(03177)
                              "ServerSupportFunction "
                              "HSE_REQ_SEND_RESPONSE_HEADER "
                              "clhy_pass_brigade failed: %s", r->filename);
            return (rv == KUDA_SUCCESS);
        }
        /* Deliberately hold off sending 'just the headers' to begin to
         * accumulate the body and speed up the overall response, or at
         * least wait for the end the session.
         */
        return 1;
    }

    case HSE_REQ_DONE_WITH_SESSION:
        /* Signal to resume the thread completing this request,
         * leave it to the pool cleanup to dispose of our mutex.
         */
        if (cid->completed) {
            (void)kuda_thread_mutex_unlock(cid->completed);
            return 1;
        }
        else if (cid->dconf.log_unsupported) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02671)
                          "ServerSupportFunction "
                          "HSE_REQ_DONE_WITH_SESSION is not supported: %s",
                          r->filename);
        }
        kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
        return 0;

    case HSE_REQ_MAP_URL_TO_PATH:
    {
        /* Map a URL to a filename */
        char *file = (char *)buf_data;
        kuda_uint32_t len;
        subreq = clhy_sub_req_lookup_uri(
                     kuda_pstrndup(cid->r->pool, file, *buf_size), r, NULL);

        if (!subreq->filename) {
            clhy_destroy_sub_req(subreq);
            return 0;
        }

        len = (kuda_uint32_t)strlen(r->filename);

        if ((subreq->finfo.filetype == KUDA_DIR)
              && (!subreq->path_info)
              && (file[len - 1] != '/'))
            file = kuda_pstrcat(cid->r->pool, subreq->filename, "/", NULL);
        else
            file = kuda_pstrcat(cid->r->pool, subreq->filename,
                                              subreq->path_info, NULL);

        clhy_destroy_sub_req(subreq);

#ifdef WIN32
        /* We need to make this a real Windows path name */
        kuda_filepath_merge(&file, "", file, KUDA_FILEPATH_NATIVE, r->pool);
#endif

        *buf_size = kuda_cpystrn(buf_data, file, *buf_size) - buf_data;

        return 1;
    }

    case HSE_REQ_GET_SSPI_INFO:
        if (cid->dconf.log_unsupported)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02672)
                           "ServerSupportFunction HSE_REQ_GET_SSPI_INFO "
                           "is not supported: %s", r->filename);
        kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
        return 0;

    case HSE_APPEND_LOG_PARAMETER:
        /* Log buf_data, of buf_size bytes, in the URI Query (cs-uri-query) field
         */
        kuda_table_set(r->notes, "isapi-parameter", (char*) buf_data);
        if (cid->dconf.log_to_query) {
            if (r->args)
                r->args = kuda_pstrcat(r->pool, r->args, (char*) buf_data, NULL);
            else
                r->args = kuda_pstrdup(r->pool, (char*) buf_data);
        }
        if (cid->dconf.log_to_errlog)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(02985)
                          "%s: %s", cid->r->filename,
                          (char*) buf_data);
        return 1;

    case HSE_REQ_IO_COMPLETION:
        /* Emulates a completion port...  Record callback address and
         * user defined arg, we will call this after any async request
         * (e.g. transmitfile) as if the request executed async.
         * Per MS docs... HSE_REQ_IO_COMPLETION replaces any prior call
         * to HSE_REQ_IO_COMPLETION, and buf_data may be set to NULL.
         */
        if (cid->dconf.fake_async) {
            cid->completion = (PFN_HSE_IO_COMPLETION) buf_data;
            cid->completion_arg = (void *) data_type;
            return 1;
        }
        if (cid->dconf.log_unsupported)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02673)
                      "ServerSupportFunction HSE_REQ_IO_COMPLETION "
                      "is not supported: %s", r->filename);
        kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
        return 0;

    case HSE_REQ_TRANSMIT_FILE:
    {
        /* we do nothing with (tf->dwFlags & HSE_DISCONNECT_AFTER_SEND)
         */
        HSE_TF_INFO *tf = (HSE_TF_INFO*)buf_data;
        kuda_uint32_t sent = 0;
        kuda_ssize_t ate = 0;
        kuda_bucket_brigade *bb;
        kuda_bucket *b;
        kuda_file_t *fd;
        kuda_off_t fsize;

        if (!cid->dconf.fake_async && (tf->dwFlags & HSE_IO_ASYNC)) {
            if (cid->dconf.log_unsupported)
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02674)
                         "ServerSupportFunction HSE_REQ_TRANSMIT_FILE "
                         "as HSE_IO_ASYNC is not supported: %s", r->filename);
            kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
            return 0;
        }

        /* Presume the handle was opened with the CORRECT semantics
         * for TransmitFile
         */
        if ((rv = kuda_platform_file_put(&fd, &tf->hFile,
                                  KUDA_READ | KUDA_XTHREAD, r->pool))
                != KUDA_SUCCESS) {
            return 0;
        }
        if (tf->BytesToWrite) {
            fsize = tf->BytesToWrite;
        }
        else {
            kuda_finfo_t fi;
            if (kuda_file_info_get(&fi, KUDA_FINFO_SIZE, fd) != KUDA_SUCCESS) {
                kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
                return 0;
            }
            fsize = fi.size - tf->Offset;
        }

        /* kuda_dupfile_oshandle (&fd, tf->hFile, r->pool); */
        bb = kuda_brigade_create(r->pool, c->bucket_alloc);

        /* According to MS: if calling HSE_REQ_TRANSMIT_FILE with the
         * HSE_IO_SEND_HEADERS flag, then you can't otherwise call any
         * HSE_SEND_RESPONSE_HEADERS* fn, but if you don't use the flag,
         * you must have done so.  They document that the pHead headers
         * option is valid only for HSE_IO_SEND_HEADERS - we are a bit
         * more flexible and assume with the flag, pHead are the
         * response headers, and without, pHead simply contains text
         * (handled after this case).
         */
        if ((tf->dwFlags & HSE_IO_SEND_HEADERS) && tf->pszStatusCode) {
            ate = send_response_header(cid, tf->pszStatusCode,
                                            (char*)tf->pHead,
                                            strlen(tf->pszStatusCode),
                                            tf->HeadLength);
        }
        else if (!cid->headers_set && tf->pHead && tf->HeadLength
                                   && *(char*)tf->pHead) {
            ate = send_response_header(cid, NULL, (char*)tf->pHead,
                                            0, tf->HeadLength);
            if (ate < 0)
            {
                kuda_brigade_destroy(bb);
                kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
                return 0;
            }
        }

        if (tf->pHead && (kuda_size_t)ate < tf->HeadLength) {
            b = kuda_bucket_transient_create((char*)tf->pHead + ate,
                                            tf->HeadLength - ate,
                                            c->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(bb, b);
            sent = tf->HeadLength;
        }

        sent += (kuda_uint32_t)fsize;
        kuda_brigade_insert_file(bb, fd, tf->Offset, fsize, r->pool);

        if (tf->pTail && tf->TailLength) {
            sent += tf->TailLength;
            b = kuda_bucket_transient_create((char*)tf->pTail,
                                            tf->TailLength, c->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(bb, b);
        }

        b = kuda_bucket_flush_create(c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);
        rv = clhy_pass_brigade(r->output_filters, bb);
        cid->response_sent = 1;
        if (rv != KUDA_SUCCESS)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, r, CLHYLOGNO(03178)
                          "ServerSupportFunction "
                          "HSE_REQ_TRANSMIT_FILE "
                          "clhy_pass_brigade failed: %s", r->filename);

        /* Use tf->pfnHseIO + tf->pContext, or if NULL, then use cid->fnIOComplete
         * pass pContect to the HseIO callback.
         */
        if (tf->dwFlags & HSE_IO_ASYNC) {
            if (tf->pfnHseIO) {
                if (rv == KUDA_SUCCESS) {
                    tf->pfnHseIO(cid->ecb, tf->pContext,
                                 ERROR_SUCCESS, sent);
                }
                else {
                    tf->pfnHseIO(cid->ecb, tf->pContext,
                                 ERROR_WRITE_FAULT, sent);
                }
            }
            else if (cid->completion) {
                if (rv == KUDA_SUCCESS) {
                    cid->completion(cid->ecb, cid->completion_arg,
                                    sent, ERROR_SUCCESS);
                }
                else {
                    cid->completion(cid->ecb, cid->completion_arg,
                                    sent, ERROR_WRITE_FAULT);
                }
            }
        }
        return (rv == KUDA_SUCCESS);
    }

    case HSE_REQ_REFRESH_ISAPI_ACL:
        if (cid->dconf.log_unsupported)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02675)
                          "ServerSupportFunction "
                          "HSE_REQ_REFRESH_ISAPI_ACL "
                          "is not supported: %s", r->filename);
        kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
        return 0;

    case HSE_REQ_IS_KEEP_CONN:
        *((int *)buf_data) = (r->connection->keepalive == CLHY_CONN_KEEPALIVE);
        return 1;

    case HSE_REQ_ASYNC_READ_CLIENT:
    {
        kuda_uint32_t read = 0;
        int res = 0;
        if (!cid->dconf.fake_async) {
            if (cid->dconf.log_unsupported)
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02986)
                              "asynchronous I/O not supported: %s",
                              r->filename);
            kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
            return 0;
        }

        if (r->remaining < *buf_size) {
            *buf_size = (kuda_size_t)r->remaining;
        }

        while (read < *buf_size &&
            ((res = clhy_get_client_block(r, (char*)buf_data + read,
                                        *buf_size - read)) > 0)) {
            read += res;
        }

        if ((*data_type & HSE_IO_ASYNC) && cid->completion) {
            /* XXX: Many authors issue their next HSE_REQ_ASYNC_READ_CLIENT
             * within the completion logic.  An example is MS's own PSDK
             * sample web/iis/extensions/io/ASyncRead.  This potentially
             * leads to stack exhaustion.  To refactor, the notification
             * logic needs to move to isapi_handler() - differentiating
             * the cid->completed event with a new flag to indicate
             * an async-notice versus the async request completed.
             */
            if (res >= 0) {
                cid->completion(cid->ecb, cid->completion_arg,
                                read, ERROR_SUCCESS);
            }
            else {
                cid->completion(cid->ecb, cid->completion_arg,
                                read, ERROR_READ_FAULT);
            }
        }
        return (res >= 0);
    }

    case HSE_REQ_GET_IMPERSONATION_TOKEN:  /* Added in ISAPI 4.0 */
        if (cid->dconf.log_unsupported)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02676)
                          "ServerSupportFunction "
                          "HSE_REQ_GET_IMPERSONATION_TOKEN "
                          "is not supported: %s", r->filename);
        kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
        return 0;

    case HSE_REQ_MAP_URL_TO_PATH_EX:
    {
        /* Map a URL to a filename */
        HSE_URL_MAPEX_INFO *info = (HSE_URL_MAPEX_INFO*)data_type;
        char* test_uri = kuda_pstrndup(r->pool, (char *)buf_data, *buf_size);

        subreq = clhy_sub_req_lookup_uri(test_uri, r, NULL);
        info->cchMatchingURL = strlen(test_uri);
        info->cchMatchingPath = kuda_cpystrn(info->lpszPath, subreq->filename,
                                      sizeof(info->lpszPath)) - info->lpszPath;

        /* Mapping started with assuming both strings matched.
         * Now roll on the path_info as a mismatch and handle
         * terminating slashes for directory matches.
         */
        if (subreq->path_info && *subreq->path_info) {
            kuda_cpystrn(info->lpszPath + info->cchMatchingPath,
                        subreq->path_info,
                        sizeof(info->lpszPath) - info->cchMatchingPath);
            info->cchMatchingURL -= strlen(subreq->path_info);
            if (subreq->finfo.filetype == KUDA_DIR
                 && info->cchMatchingPath < sizeof(info->lpszPath) - 1) {
                /* roll forward over path_info's first slash */
                ++info->cchMatchingPath;
                ++info->cchMatchingURL;
            }
        }
        else if (subreq->finfo.filetype == KUDA_DIR
                 && info->cchMatchingPath < sizeof(info->lpszPath) - 1) {
            /* Add a trailing slash for directory */
            info->lpszPath[info->cchMatchingPath++] = '/';
            info->lpszPath[info->cchMatchingPath] = '\0';
        }

        /* If the matched isn't a file, roll match back to the prior slash */
        if (subreq->finfo.filetype == KUDA_NOFILE) {
            while (info->cchMatchingPath && info->cchMatchingURL) {
                if (info->lpszPath[info->cchMatchingPath - 1] == '/')
                    break;
                --info->cchMatchingPath;
                --info->cchMatchingURL;
            }
        }

        /* Paths returned with back slashes */
        for (test_uri = info->lpszPath; *test_uri; ++test_uri)
            if (*test_uri == '/')
                *test_uri = '\\';

        /* is a combination of:
         * HSE_URL_FLAGS_READ         0x001 Allow read
         * HSE_URL_FLAGS_WRITE        0x002 Allow write
         * HSE_URL_FLAGS_EXECUTE      0x004 Allow execute
         * HSE_URL_FLAGS_SSL          0x008 Require SSL
         * HSE_URL_FLAGS_DONT_CACHE   0x010 Don't cache (VRoot only)
         * HSE_URL_FLAGS_NEGO_CERT    0x020 Allow client SSL cert
         * HSE_URL_FLAGS_REQUIRE_CERT 0x040 Require client SSL cert
         * HSE_URL_FLAGS_MAP_CERT     0x080 Map client SSL cert to account
         * HSE_URL_FLAGS_SSL128       0x100 Require 128-bit SSL cert
         * HSE_URL_FLAGS_SCRIPT       0x200 Allow script execution
         *
         * XxX: As everywhere, EXEC flags could use some work...
         *      and this could go further with more flags, as desired.
         */
        info->dwFlags = (subreq->finfo.protection & KUDA_UREAD    ? 0x001 : 0)
                      | (subreq->finfo.protection & KUDA_UWRITE   ? 0x002 : 0)
                      | (subreq->finfo.protection & KUDA_UEXECUTE ? 0x204 : 0);
        return 1;
    }

    case HSE_REQ_ABORTIVE_CLOSE:
        if (cid->dconf.log_unsupported)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02677)
                          "ServerSupportFunction HSE_REQ_ABORTIVE_CLOSE"
                          " is not supported: %s", r->filename);
        kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
        return 0;

    case HSE_REQ_GET_CERT_INFO_EX:  /* Added in ISAPI 4.0 */
        if (cid->dconf.log_unsupported)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02678)
                          "ServerSupportFunction "
                          "HSE_REQ_GET_CERT_INFO_EX "
                          "is not supported: %s", r->filename);
        kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
        return 0;

    case HSE_REQ_SEND_RESPONSE_HEADER_EX:  /* Added in ISAPI 4.0 */
    {
        HSE_SEND_HEADER_EX_INFO *shi = (HSE_SEND_HEADER_EX_INFO*)buf_data;

        /*  Ignore shi->fKeepConn - we don't want the advise
         */
        kuda_ssize_t ate = send_response_header(cid, shi->pszStatus,
                                               shi->pszHeader,
                                               shi->cchStatus,
                                               shi->cchHeader);
        if (ate < 0) {
            kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
            return 0;
        }
        else if ((kuda_size_t)ate < shi->cchHeader) {
            kuda_bucket_brigade *bb;
            kuda_bucket *b;
            bb = kuda_brigade_create(cid->r->pool, c->bucket_alloc);
            b = kuda_bucket_transient_create(shi->pszHeader + ate,
                                            shi->cchHeader - ate,
                                            c->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(bb, b);
            b = kuda_bucket_flush_create(c->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(bb, b);
            rv = clhy_pass_brigade(cid->r->output_filters, bb);
            cid->response_sent = 1;
            if (rv != KUDA_SUCCESS)
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, r, CLHYLOGNO(03179)
                              "ServerSupportFunction "
                              "HSE_REQ_SEND_RESPONSE_HEADER_EX "
                              "clhy_pass_brigade failed: %s", r->filename);
            return (rv == KUDA_SUCCESS);
        }
        /* Deliberately hold off sending 'just the headers' to begin to
         * accumulate the body and speed up the overall response, or at
         * least wait for the end the session.
         */
        return 1;
    }

    case HSE_REQ_CLOSE_CONNECTION:  /* Added after ISAPI 4.0 */
        if (cid->dconf.log_unsupported)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02679)
                          "ServerSupportFunction "
                          "HSE_REQ_CLOSE_CONNECTION "
                          "is not supported: %s", r->filename);
        kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
        return 0;

    case HSE_REQ_IS_CONNECTED:  /* Added after ISAPI 4.0 */
        /* Returns True if client is connected c.f. MSKB Q188346
         * assuming the identical return mechanism as HSE_REQ_IS_KEEP_CONN
         */
        *((int *)buf_data) = (r->connection->aborted == 0);
        return 1;

    case HSE_REQ_EXTENSION_TRIGGER:  /* Added after ISAPI 4.0 */
        /*  Undocumented - defined by the Microsoft Jan '00 Platform SDK
         */
        if (cid->dconf.log_unsupported)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02680)
                          "ServerSupportFunction "
                          "HSE_REQ_EXTENSION_TRIGGER "
                          "is not supported: %s", r->filename);
        kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
        return 0;

    default:
        if (cid->dconf.log_unsupported)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02681)
                          "ServerSupportFunction (%d) not supported: "
                          "%s", HSE_code, r->filename);
        kuda_set_platform_error(KUDA_FROM_PLATFORM_ERROR(ERROR_INVALID_PARAMETER));
        return 0;
    }
}

/**********************************************************
 *
 *  ISAPI cAPI request invocation section
 *
 **********************************************************/

static kuda_status_t isapi_handler (request_rec *r)
{
    isapi_dir_conf *dconf;
    kuda_table_t *e;
    kuda_status_t rv;
    isapi_loaded *isa;
    isapi_cid *cid;
    const char *val;
    kuda_uint32_t read;
    int res;

    if (strcmp(r->handler, "isapi-isa")
        && strcmp(r->handler, "isapi-handler")) {
        /* Hang on to the isapi-isa for compatibility with older docs
         * (wtf did '-isa' mean in the first place?) but introduce
         * a newer and clearer "isapi-handler" name.
         */
        return DECLINED;
    }
    dconf = clhy_get_capi_config(r->per_dir_config, &isapi_capi);
    e = r->subprocess_env;

    /* Use similar restrictions as CGIs
     *
     * If this fails, it's pointless to load the isapi dll.
     */
    if (!(clhy_allow_options(r) & OPT_EXECCGI)) {
        return HTTP_FORBIDDEN;
    }
    if (r->finfo.filetype == KUDA_NOFILE) {
        return HTTP_NOT_FOUND;
    }
    if (r->finfo.filetype != KUDA_REG) {
        return HTTP_FORBIDDEN;
    }
    if ((r->used_path_info == CLHY_REQ_REJECT_PATH_INFO) &&
        r->path_info && *r->path_info) {
        /* default to accept */
        return HTTP_NOT_FOUND;
    }

    if (isapi_lookup(r->pool, r->server, r, r->filename, &isa)
           != KUDA_SUCCESS) {
        return HTTP_INTERNAL_SERVER_ERROR;
    }
    /* Set up variables */
    clhy_add_common_vars(r);
    clhy_add_cgi_vars(r);
    kuda_table_setn(e, "UNMAPPED_REMOTE_USER", "REMOTE_USER");
    if ((val = kuda_table_get(e, "HTTPS")) && (strcmp(val, "on") == 0))
        kuda_table_setn(e, "SERVER_PORT_SECURE", "1");
    else
        kuda_table_setn(e, "SERVER_PORT_SECURE", "0");
    kuda_table_setn(e, "URL", r->uri);

    /* Set up connection structure and ecb,
     * NULL or zero out most fields.
     */
    cid = kuda_pcalloc(r->pool, sizeof(isapi_cid));

    /* Fixup defaults for dconf */
    cid->dconf.read_ahead_buflen = (dconf->read_ahead_buflen == ISAPI_UNDEF)
                                     ? 49152 : dconf->read_ahead_buflen;
    cid->dconf.log_unsupported   = (dconf->log_unsupported == ISAPI_UNDEF)
                                     ? 0 : dconf->log_unsupported;
    cid->dconf.log_to_errlog     = (dconf->log_to_errlog == ISAPI_UNDEF)
                                     ? 0 : dconf->log_to_errlog;
    cid->dconf.log_to_query      = (dconf->log_to_query == ISAPI_UNDEF)
                                     ? 1 : dconf->log_to_query;
    cid->dconf.fake_async        = (dconf->fake_async == ISAPI_UNDEF)
                                     ? 0 : dconf->fake_async;

    cid->ecb = kuda_pcalloc(r->pool, sizeof(EXTENSION_CONTROL_BLOCK));
    cid->ecb->ConnID = cid;
    cid->isa = isa;
    cid->r = r;
    r->status = 0;

    cid->ecb->cbSize = sizeof(EXTENSION_CONTROL_BLOCK);
    cid->ecb->dwVersion = isa->report_version;
    cid->ecb->dwHttpStatusCode = 0;
    strcpy(cid->ecb->lpszLogData, "");
    /* TODO: are copies really needed here?
     */
    cid->ecb->lpszMethod = (char*) r->method;
    cid->ecb->lpszQueryString = (char*) kuda_table_get(e, "QUERY_STRING");
    cid->ecb->lpszPathInfo = (char*) kuda_table_get(e, "PATH_INFO");
    cid->ecb->lpszPathTranslated = (char*) kuda_table_get(e, "PATH_TRANSLATED");
    cid->ecb->lpszContentType = (char*) kuda_table_get(e, "CONTENT_TYPE");

    /* Set up the callbacks */
    cid->ecb->GetServerVariable = regfnGetServerVariable;
    cid->ecb->WriteClient = regfnWriteClient;
    cid->ecb->ReadClient = regfnReadClient;
    cid->ecb->ServerSupportFunction = regfnServerSupportFunction;

    /* Set up client input */
    res = clhy_setup_client_block(r, REQUEST_CHUNKED_ERROR);
    if (res) {
        return res;
    }

    if (clhy_should_client_block(r)) {
        /* Time to start reading the appropriate amount of data,
         * and allow the administrator to tweak the number
         */
        if (r->remaining) {
            cid->ecb->cbTotalBytes = (kuda_size_t)r->remaining;
            if (cid->ecb->cbTotalBytes > (kuda_uint32_t)cid->dconf.read_ahead_buflen)
                cid->ecb->cbAvailable = cid->dconf.read_ahead_buflen;
            else
                cid->ecb->cbAvailable = cid->ecb->cbTotalBytes;
        }
        else
        {
            cid->ecb->cbTotalBytes = 0xffffffff;
            cid->ecb->cbAvailable = cid->dconf.read_ahead_buflen;
        }

        cid->ecb->lpbData = kuda_pcalloc(r->pool, cid->ecb->cbAvailable + 1);

        read = 0;
        while (read < cid->ecb->cbAvailable &&
               ((res = clhy_get_client_block(r, (char*)cid->ecb->lpbData + read,
                                        cid->ecb->cbAvailable - read)) > 0)) {
            read += res;
        }

        if (res < 0) {
            return HTTP_INTERNAL_SERVER_ERROR;
        }

        /* Although it's not to spec, IIS seems to null-terminate
         * its lpdData string. So we will too.
         */
        if (res == 0)
            cid->ecb->cbAvailable = cid->ecb->cbTotalBytes = read;
        else
            cid->ecb->cbAvailable = read;
        cid->ecb->lpbData[read] = '\0';
    }
    else {
        cid->ecb->cbTotalBytes = 0;
        cid->ecb->cbAvailable = 0;
        cid->ecb->lpbData = NULL;
    }

    /* To emulate async behavior...
     *
     * We create a cid->completed mutex and lock on it so that the
     * app can believe is it running async.
     *
     * This request completes upon a notification through
     * ServerSupportFunction(HSE_REQ_DONE_WITH_SESSION), which
     * unlocks this mutex.  If the HttpExtensionProc() returns
     * HSE_STATUS_PENDING, we will attempt to gain this lock again
     * which may *only* happen once HSE_REQ_DONE_WITH_SESSION has
     * unlocked the mutex.
     */
    if (cid->dconf.fake_async) {
        rv = kuda_thread_mutex_create(&cid->completed,
                                     KUDA_THREAD_MUTEX_UNNESTED,
                                     r->pool);
        if (cid->completed && (rv == KUDA_SUCCESS)) {
            rv = kuda_thread_mutex_lock(cid->completed);
        }

        if (!cid->completed || (rv != KUDA_SUCCESS)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02112)
                          "Failed to create completion mutex");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
    }

    /* All right... try and run the sucker */
    rv = (*isa->HttpExtensionProc)(cid->ecb);

    /* Check for a log message - and log it */
    if (*cid->ecb->lpszLogData) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(02113)
                      "%s: %s", r->filename, cid->ecb->lpszLogData);
    }

    switch(rv) {
        case 0:  /* Strange, but MS isapi accepts this as success */
        case HSE_STATUS_SUCCESS:
        case HSE_STATUS_SUCCESS_AND_KEEP_CONN:
            /* Ignore the keepalive stuff; cLHy handles it just fine without
             * the ISAPI Handler's "advice".
             * Per Microsoft: "In IIS versions 4.0 and later, the return
             * values HSE_STATUS_SUCCESS and HSE_STATUS_SUCCESS_AND_KEEP_CONN
             * are functionally identical: Keep-Alive connections are
             * maintained, if supported by the client."
             * ... so we were pat all this time
             */
            break;

        case HSE_STATUS_PENDING:
            /* emulating async behavior...
             */
            if (cid->completed) {
                /* The completion port was locked prior to invoking
                 * HttpExtensionProc().  Once we can regain the lock,
                 * when ServerSupportFunction(HSE_REQ_DONE_WITH_SESSION)
                 * is called by the extension to release the lock,
                 * we may finally destroy the request.
                 */
                (void)kuda_thread_mutex_lock(cid->completed);
                break;
            }
            else if (cid->dconf.log_unsupported) {
                 clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(02114)
                               "asynch I/O result HSE_STATUS_PENDING "
                               "from HttpExtensionProc() is not supported: %s",
                               r->filename);
                 r->status = HTTP_INTERNAL_SERVER_ERROR;
            }
            break;

        case HSE_STATUS_ERROR:
            /* end response if we have yet to do so.
             */
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, kuda_get_platform_error(), r, CLHYLOGNO(02115)
                          "HSE_STATUS_ERROR result from "
                          "HttpExtensionProc(): %s", r->filename);
            r->status = HTTP_INTERNAL_SERVER_ERROR;
            break;

        default:
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, kuda_get_platform_error(), r, CLHYLOGNO(02116)
                          "unrecognized result code %d "
                          "from HttpExtensionProc(): %s ",
                          rv, r->filename);
            r->status = HTTP_INTERNAL_SERVER_ERROR;
            break;
    }

    /* Flush the response now, including headers-only responses */
    if (cid->headers_set || cid->response_sent) {
        conn_rec *c = r->connection;
        kuda_bucket_brigade *bb;
        kuda_bucket *b;
        kuda_status_t rv;

        bb = kuda_brigade_create(r->pool, c->bucket_alloc);
        b = kuda_bucket_eos_create(c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);
        rv = clhy_pass_brigade(r->output_filters, bb);
        cid->response_sent = 1;

        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, r, CLHYLOGNO(02117)
                          "clhy_pass_brigade failed to "
                          "complete the response: %s ", r->filename);
        }

        return OK; /* NOT r->status, even if it has changed. */
    }

    /* As the client returned no error, and if we did not error out
     * ourselves, trust dwHttpStatusCode to say something relevant.
     */
    if (!clhy_is_HTTP_SERVER_ERROR(r->status) && cid->ecb->dwHttpStatusCode) {
        r->status = cid->ecb->dwHttpStatusCode;
    }

    /* For all missing-response situations simply return the status,
     * and let the core respond to the client.
     */
    return r->status;
}

/**********************************************************
 *
 *  ISAPI cAPI Setup Hooks
 *
 **********************************************************/

static int isapi_pre_config(kuda_pool_t *pconf, kuda_pool_t *plog, kuda_pool_t *ptemp)
{
    kuda_status_t rv;

    kuda_pool_create_ex(&loaded.pool, pconf, NULL, NULL);
    if (!loaded.pool) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, KUDA_EGENERAL, NULL, CLHYLOGNO(02118)
                     "could not create the isapi cache pool");
        return KUDA_EGENERAL;
    }

    loaded.hash = kuda_hash_make(loaded.pool);
    if (!loaded.hash) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, NULL, CLHYLOGNO(02119)
                     "Failed to create cAPI cache");
        return KUDA_EGENERAL;
    }

    rv = kuda_thread_mutex_create(&loaded.lock, KUDA_THREAD_MUTEX_DEFAULT,
                                 loaded.pool);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, NULL, CLHYLOGNO(02682)
                     "Failed to create cAPI cache lock");
        return rv;
    }
    return OK;
}

static void isapi_hooks(kuda_pool_t *cont)
{
    clhy_hook_pre_config(isapi_pre_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_handler(isapi_handler, NULL, NULL, KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(isapi) = {
   STANDARD16_CAPI_STUFF,
   create_isapi_dir_config,     /* create per-dir config */
   merge_isapi_dir_configs,     /* merge per-dir config */
   NULL,                        /* server config */
   NULL,                        /* merge server config */
   isapi_cmds,                  /* command kuda_table_t */
   isapi_hooks                  /* register hooks */
};
