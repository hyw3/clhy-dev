/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef WIN32

#include "kuda_strings.h"
#include "kuda_portable.h"
#include "kuda_buckets.h"
#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_protocol.h"
#include "http_request.h"
#include "http_log.h"
#include "util_script.h"
#include "capi_core.h"
#include "capi_cgi.h"
#include "kuda_lib.h"
#include "clhy_regkey.h"

/*
 * CGI Script stuff for Win32...
 */
typedef enum { eFileTypeUNKNOWN, eFileTypeBIN, eFileTypeEXE16, eFileTypeEXE32,
               eFileTypeSCRIPT } file_type_e;
typedef enum { INTERPRETER_SOURCE_UNSET, INTERPRETER_SOURCE_REGISTRY_STRICT,
               INTERPRETER_SOURCE_REGISTRY, INTERPRETER_SOURCE_SHEBANG
             } interpreter_source_e;
CLHY_DECLARE(file_type_e) clhy_get_win32_interpreter(const request_rec *,
                                                 char **interpreter,
                                                 char **arguments);

cAPI CLHY_CAPI_DECLARE_DATA win32_capi;

typedef struct {
    /* Where to find interpreter to run scripts */
    interpreter_source_e script_interpreter_source;
} win32_dir_conf;

static void *create_win32_dir_config(kuda_pool_t *p, char *dir)
{
    win32_dir_conf *conf;
    conf = (win32_dir_conf*)kuda_palloc(p, sizeof(win32_dir_conf));
    conf->script_interpreter_source = INTERPRETER_SOURCE_UNSET;
    return conf;
}

static void *merge_win32_dir_configs(kuda_pool_t *p, void *basev, void *addv)
{
    win32_dir_conf *new;
    win32_dir_conf *base = (win32_dir_conf *) basev;
    win32_dir_conf *add = (win32_dir_conf *) addv;

    new = (win32_dir_conf *) kuda_pcalloc(p, sizeof(win32_dir_conf));
    new->script_interpreter_source = (add->script_interpreter_source
                                           != INTERPRETER_SOURCE_UNSET)
                                   ? add->script_interpreter_source
                                   : base->script_interpreter_source;
    return new;
}

static const char *set_interpreter_source(cmd_parms *cmd, void *dv,
                                          char *arg)
{
    win32_dir_conf *d = (win32_dir_conf *)dv;
    if (!strcasecmp(arg, "registry")) {
        d->script_interpreter_source = INTERPRETER_SOURCE_REGISTRY;
    }
    else if (!strcasecmp(arg, "registry-strict")) {
        d->script_interpreter_source = INTERPRETER_SOURCE_REGISTRY_STRICT;
    }
    else if (!strcasecmp(arg, "script")) {
        d->script_interpreter_source = INTERPRETER_SOURCE_SHEBANG;
    }
    else {
        return kuda_pstrcat(cmd->temp_pool, "ScriptInterpreterSource \"", arg,
                           "\" must be \"registry\", \"registry-strict\" or "
                           "\"script\"", NULL);
    }
    return NULL;
}

/* XXX: prep_string should translate the string into unicode,
 * such that it is compatible with whatever codepage the client
 * will read characters 80-ff.  For the moment, use the unicode
 * values 0080-00ff.  This isn't trivial, since the code page
 * varies between msdos and Windows applications.
 * For subsystem 2 [GUI] the default is the system Ansi CP.
 * For subsystem 3 [CLI] the default is the system OEM CP.
 */
static void prep_string(const char ** str, kuda_pool_t *p)
{
    const char *ch = *str;
    char *ch2;
    kuda_size_t widen = 0;

    if (!ch) {
        return;
    }
    while (*ch) {
        if (*(ch++) & 0x80) {
            ++widen;
        }
    }
    if (!widen) {
        return;
    }
    widen += (ch - *str) + 1;
    ch = *str;
    *str = ch2 = kuda_palloc(p, widen);
    while (*ch) {
        if (*ch & 0x80) {
            /* sign extension won't hurt us here */
            *(ch2++) = 0xC0 | ((*ch >> 6) & 0x03);
            *(ch2++) = 0x80 | (*(ch++) & 0x3f);
        }
        else {
            *(ch2++) = *(ch++);
        }
    }
    *(ch2++) = '\0';
}

/* Somewhat more exciting ... figure out where the registry has stashed the
 * ExecCGI or Open command - it may be nested one level deep (or more???)
 */
static char* get_interpreter_from_win32_registry(kuda_pool_t *p,
                                                 const char* ext,
                                                 int strict)
{
    kuda_status_t rv;
    clhy_regkey_t *name_key = NULL;
    clhy_regkey_t *type_key;
    clhy_regkey_t *key;
    char execcgi_path[] = "SHELL\\EXECCGI\\COMMAND";
    char execopen_path[] = "SHELL\\OPEN\\COMMAND";
    char *type_name;
    char *buffer;

    if (!ext) {
        return NULL;
    }
    /*
     * Future optimization:
     * When the registry is successfully searched, store the strings for
     * interpreter and arguments in an ext hash to speed up subsequent look-ups
     */

    /* Open the key associated with the script filetype extension */
    rv = clhy_regkey_open(&type_key, CLHY_REGKEY_CLASSES_ROOT, ext, KUDA_READ, p);

    if (rv != KUDA_SUCCESS) {
        return NULL;
    }

    /* Retrieve the name of the script filetype extension */
    rv = clhy_regkey_value_get(&type_name, type_key, "", p);

    if (rv == KUDA_SUCCESS && type_name[0]) {
        /* Open the key associated with the script filetype extension */
        rv = clhy_regkey_open(&name_key, CLHY_REGKEY_CLASSES_ROOT, type_name,
                            KUDA_READ, p);
    }

    /* Open the key for the script command path by:
     *
     *   1) the 'named' filetype key for ExecCGI/Command
     *   2) the extension's type key for ExecCGI/Command
     *
     * and if the strict arg is false, then continue trying:
     *
     *   3) the 'named' filetype key for Open/Command
     *   4) the extension's type key for Open/Command
     */

    if (name_key) {
        if ((rv = clhy_regkey_open(&key, name_key, execcgi_path, KUDA_READ, p))
                == KUDA_SUCCESS) {
            rv = clhy_regkey_value_get(&buffer, key, "", p);
            clhy_regkey_close(name_key);
        }
    }

    if (!name_key || (rv != KUDA_SUCCESS)) {
        if ((rv = clhy_regkey_open(&key, type_key, execcgi_path, KUDA_READ, p))
                == KUDA_SUCCESS) {
            rv = clhy_regkey_value_get(&buffer, key, "", p);
            clhy_regkey_close(type_key);
        }
    }

    if (!strict && name_key && (rv != KUDA_SUCCESS)) {
        if ((rv = clhy_regkey_open(&key, name_key, execopen_path, KUDA_READ, p))
                == KUDA_SUCCESS) {
            rv = clhy_regkey_value_get(&buffer, key, "", p);
            clhy_regkey_close(name_key);
        }
    }

    if (!strict && (rv != KUDA_SUCCESS)) {
        if ((rv = clhy_regkey_open(&key, type_key, execopen_path, KUDA_READ, p))
                == KUDA_SUCCESS) {
            rv = clhy_regkey_value_get(&buffer, key, "", p);
            clhy_regkey_close(type_key);
        }
    }

    if (name_key) {
        clhy_regkey_close(name_key);
    }

    clhy_regkey_close(type_key);

    if (rv != KUDA_SUCCESS || !buffer[0]) {
        return NULL;
    }

    return buffer;
}


static kuda_array_header_t *split_argv(kuda_pool_t *p, const char *interp,
                                      const char *cgiprg, const char *cgiargs)
{
    kuda_array_header_t *args = kuda_array_make(p, 8, sizeof(char*));
    char *d = kuda_palloc(p, strlen(interp)+1);
    const char *ch = interp;
    const char **arg;
    int prgtaken = 0;
    int argtaken = 0;
    int inquo;
    int sl;

    while (*ch) {
        /* Skip on through Deep Space */
        if (kuda_isspace(*ch)) {
            ++ch; continue;
        }
        /* One Arg */
        if (((*ch == '$') || (*ch == '%')) && (*(ch + 1) == '*')) {
            const char *cgiarg = cgiargs;
            argtaken = 1;
            for (;;) {
                char *w = clhy_getword_nulls(p, &cgiarg, '+');
                if (!*w) {
                    break;
                }
                clhy_unescape_url(w);
                prep_string((const char**)&w, p);
                arg = (const char**)kuda_array_push(args);
                *arg = clhy_escape_shell_cmd(p, w);
            }
            ch += 2;
            continue;
        }
        if (((*ch == '$') || (*ch == '%')) && (*(ch + 1) == '1')) {
            /* Todo: Make short name!!! */
            prgtaken = 1;
            arg = (const char**)kuda_array_push(args);
            if (*ch == '%') {
                char *repl = kuda_pstrdup(p, cgiprg);
                *arg = repl;
                while ((repl = strchr(repl, '/'))) {
                    *repl++ = '\\';
                }
            }
            else {
                *arg = cgiprg;
            }
            ch += 2;
            continue;
        }
        if ((*ch == '\"') && ((*(ch + 1) == '$')
                              || (*(ch + 1) == '%')) && (*(ch + 2) == '1')
            && (*(ch + 3) == '\"')) {
            prgtaken = 1;
            arg = (const char**)kuda_array_push(args);
            if (*(ch + 1) == '%') {
                char *repl = kuda_pstrdup(p, cgiprg);
                *arg = repl;
                while ((repl = strchr(repl, '/'))) {
                    *repl++ = '\\';
                }
            }
            else {
                *arg = cgiprg;
            }
            ch += 4;
            continue;
        }
        arg = (const char**)kuda_array_push(args);
        *arg = d;
        inquo = 0;
        while (*ch) {
            if (kuda_isspace(*ch) && !inquo) {
                ++ch; break;
            }
            /* Get 'em backslashes */
            for (sl = 0; *ch == '\\'; ++sl) {
                *d++ = *ch++;
            }
            if (sl & 1) {
                /* last unmatched '\' + '"' sequence is a '"' */
                if (*ch == '\"') {
                    *(d - 1) = *ch++;
                }
                continue;
            }
            if (*ch == '\"') {
                /* '""' sequence within quotes is a '"' */
                if (*++ch == '\"' && inquo) {
                    *d++ = *ch++; continue;
                }
                /* Flip quote state */
                inquo = !inquo;
                if (kuda_isspace(*ch) && !inquo) {
                    ++ch; break;
                }
                /* All other '"'s are Munched */
                continue;
            }
            /* Anything else is, well, something else */
            *d++ = *ch++;
        }
        /* Term that arg, already pushed on args */
        *d++ = '\0';
    }

    if (!prgtaken) {
        arg = (const char**)kuda_array_push(args);
        *arg = cgiprg;
    }

    if (!argtaken) {
        const char *cgiarg = cgiargs;
        for (;;) {
            char *w = clhy_getword_nulls(p, &cgiarg, '+');
            if (!*w) {
                break;
            }
            clhy_unescape_url(w);
            prep_string((const char**)&w, p);
            arg = (const char**)kuda_array_push(args);
            *arg = clhy_escape_shell_cmd(p, w);
        }
    }

    arg = (const char**)kuda_array_push(args);
    *arg = NULL;

    return args;
}


static kuda_status_t clhy_cgi_build_command(const char **cmd, const char ***argv,
                                         request_rec *r, kuda_pool_t *p,
                                         cgi_exec_info_t *e_info)
{
    const kuda_array_header_t *elts_arr = kuda_table_elts(r->subprocess_env);
    const kuda_table_entry_t *elts = (kuda_table_entry_t *) elts_arr->elts;
    const char *ext = NULL;
    const char *interpreter = NULL;
    win32_dir_conf *d;
    kuda_file_t *fh;
    const char *args = "";
    int i;

    d = (win32_dir_conf *)clhy_get_capi_config(r->per_dir_config,
                                               &win32_capi);

    if (e_info->cmd_type) {
        /* We have to consider that the client gets any QUERY_ARGS
         * without any charset interpretation, use prep_string to
         * create a string of the literal QUERY_ARGS bytes.
         */
        *cmd = r->filename;
        if (r->args && r->args[0] && !clhy_strchr_c(r->args, '=')) {
            args = r->args;
        }
    }
    /* Handle the complete file name, we DON'T want to follow suexec, since
     * an unrooted command is as predictable as shooting craps in Win32.
     * Notice that unlike most mime extension parsing, we have to use the
     * win32 parsing here, therefore the final extension is the only one
     * we will consider.
     */
    ext = strrchr(kuda_filepath_name_get(*cmd), '.');

    /* If the file has an extension and it is not .com and not .exe and
     * we've been instructed to search the registry, then do so.
     * Let kuda_proc_create do all of the .bat/.cmd dirty work.
     */
    if (ext && (!strcasecmp(ext,".exe") || !strcasecmp(ext,".com")
                || !strcasecmp(ext,".bat") || !strcasecmp(ext,".cmd"))) {
        interpreter = "";
    }
    if (!interpreter && ext
          && (d->script_interpreter_source
                     == INTERPRETER_SOURCE_REGISTRY
           || d->script_interpreter_source
                     == INTERPRETER_SOURCE_REGISTRY_STRICT)) {
         /* Check the registry */
        int strict = (d->script_interpreter_source
                      == INTERPRETER_SOURCE_REGISTRY_STRICT);
        interpreter = get_interpreter_from_win32_registry(r->pool, ext,
                                                          strict);
        if (interpreter && e_info->cmd_type != KUDA_SHELLCMD) {
            e_info->cmd_type = KUDA_PROGRAM_PATH;
        }
        else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, r->server,
                 strict ? CLHYLOGNO(03180) "No ExecCGI verb found for files of type '%s'."
                        : CLHYLOGNO(03181) "No ExecCGI or Open verb found for files of type '%s'.",
                 ext);
        }
    }
    if (!interpreter) {
        kuda_status_t rv;
        char buffer[1024];
        kuda_size_t bytes = sizeof(buffer);
        kuda_size_t i;

        /* Need to peek into the file figure out what it really is...
         * ### aught to go back and build a cache for this one of these days.
         */
        if ((rv = kuda_file_open(&fh, *cmd, KUDA_READ | KUDA_BUFFERED,
                                 KUDA_PLATFORM_DEFAULT, r->pool)) != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(02100)
                          "Failed to open cgi file %s for testing", *cmd);
            return rv;
        }
        if ((rv = kuda_file_read(fh, buffer, &bytes)) != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(02101)
                          "Failed to read cgi file %s for testing", *cmd);
            return rv;
        }
        kuda_file_close(fh);

        /* Some twisted character [no pun intended] at MS decided that a
         * zero width joiner as the lead wide character would be ideal for
         * describing Unicode text files.  This was further convoluted to
         * another MSism that the same character mapped into utf-8, EF BB BF
         * would signify utf-8 text files.
         *
         * Since MS configuration files are all protecting utf-8 encoded
         * Unicode path, file and resource names, we already have the correct
         * WinNT encoding.  But at least eat the stupid three bytes up front.
         *
         * ### A more thorough check would also allow UNICODE text in buf, and
         * convert it to UTF-8 for invoking unicode scripts.  Those are few
         * and far between, so leave that code an enterprising soul with a need.
         */
        if ((bytes >= 3) && memcmp(buffer, "\xEF\xBB\xBF", 3) == 0) {
            memmove(buffer, buffer + 3, bytes -= 3);
        }

        /* Script or executable, that is the question...
         * we check here also for '! so that .vbs scripts can work as CGI.
         */
        if ((bytes >= 2) && ((buffer[0] == '#') || (buffer[0] == '\''))
                         && (buffer[1] == '!')) {
            /* Assuming file is a script since it starts with a shebang */
            for (i = 2; i < bytes; i++) {
                if ((buffer[i] == '\r') || (buffer[i] == '\n')) {
                    buffer[i] = '\0';
                    break;
                }
            }
            if (i < bytes) {
                interpreter = buffer + 2;
                while (kuda_isspace(*interpreter)) {
                    ++interpreter;
                }
                if (e_info->cmd_type != KUDA_SHELLCMD) {
                    e_info->cmd_type = KUDA_PROGRAM_PATH;
                }
            }
        }
        else if (bytes >= sizeof(IMAGE_DOS_HEADER)) {
            /* Not a script, is it an executable? */
            IMAGE_DOS_HEADER *hdr = (IMAGE_DOS_HEADER*)buffer;
            if (hdr->e_magic == IMAGE_DOS_SIGNATURE) {
                if (hdr->e_lfarlc < 0x40) {
                    /* Ought to invoke this 16 bit exe by a stub, (cmd /c?) */
                    interpreter = "";
                }
                else {
                    interpreter = "";
                }
            }
        }
    }
    if (!interpreter) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02102)
                      "%s is not executable; ensure interpreted scripts have "
                      "\"#!\" or \"'!\" first line", *cmd);
        return KUDA_EBADF;
    }

    *argv = (const char **)(split_argv(p, interpreter, *cmd,
                                       args)->elts);
    *cmd = (*argv)[0];

    e_info->detached = 1;

    /* XXX: Must fix r->subprocess_env to follow utf-8 conventions from
     * the client's octets so that win32 kuda_proc_create is happy.
     * The -best- way is to determine if the .exe is unicode aware
     * (using 0x0080-0x00ff) or is linked as a command or windows
     * application (following the OEM or Ansi code page in effect.)
     */
    for (i = 0; i < elts_arr->nelts; ++i) {
        if (elts[i].key && *elts[i].key && *elts[i].val
                && !(strncmp(elts[i].key, "REMOTE_", 7) == 0
                || strcmp(elts[i].key, "GATEWAY_INTERFACE") == 0
                || strcmp(elts[i].key, "REQUEST_METHOD") == 0
                || strcmp(elts[i].key, "SERVER_ADDR") == 0
                || strcmp(elts[i].key, "SERVER_PORT") == 0
                || strcmp(elts[i].key, "SERVER_PROTOCOL") == 0)) {
            prep_string((const char**) &elts[i].val, r->pool);
        }
    }
    return KUDA_SUCCESS;
}

static void register_hooks(kuda_pool_t *p)
{
    KUDA_REGISTER_OPTIONAL_FN(clhy_cgi_build_command);
}

static const command_rec win32_cmds[] = {
CLHY_INIT_TAKE1("ScriptInterpreterSource", set_interpreter_source, NULL,
              OR_FILEINFO,
              "Where to find interpreter to run Win32 scripts "
              "(Registry or script shebang line)"),
{ NULL }
};

CLHY_DECLARE_CAPI(win32) = {
   STANDARD16_CAPI_STUFF,
   create_win32_dir_config,     /* create per-dir config */
   merge_win32_dir_configs,     /* merge per-dir config */
   NULL,                        /* server config */
   NULL,                        /* merge server config */
   win32_cmds,                  /* command kuda_table_t */
   register_hooks               /* register hooks */
};

#endif /* defined WIN32 */
