# Microsoft Developer Studio Generated NMAKE File, Based on capi_lua.dsp
!IF "$(CFG)" == ""
CFG=capi_lua - Win32 Release
!MESSAGE No configuration specified. Defaulting to capi_lua - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "capi_lua - Win32 Release" && "$(CFG)" != "capi_lua - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "capi_lua.mak" CFG="capi_lua - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "capi_lua - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "capi_lua - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "capi_lua - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_lua.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Release" "libkudadelman - Win32 Release" "libkuda - Win32 Release" "$(OUTDIR)\capi_lua.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudadelman - Win32 ReleaseCLEAN" "libwwhy - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\lua_kuda.obj"
	-@erase "$(INTDIR)\lua_config.obj"
	-@erase "$(INTDIR)\lua_dbd.obj"
	-@erase "$(INTDIR)\lua_passwd.obj"
	-@erase "$(INTDIR)\lua_request.obj"
	-@erase "$(INTDIR)\lua_vmprep.obj"
	-@erase "$(INTDIR)\capi_lua.obj"
	-@erase "$(INTDIR)\capi_lua.res"
	-@erase "$(INTDIR)\capi_lua_src.idb"
	-@erase "$(INTDIR)\capi_lua_src.pdb"
	-@erase "$(OUTDIR)\capi_lua.exp"
	-@erase "$(OUTDIR)\capi_lua.lib"
	-@erase "$(OUTDIR)\capi_lua.pdb"
	-@erase "$(OUTDIR)\capi_lua.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/lua/src" /I "../clssl" /I "../database" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_LUA_DECLARE_EXPORT" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_lua_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_lua.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "NDEBUG" /d BIN_NAME="capi_lua.so" /d LONG_NAME="lua_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_lua.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib ws2_32.lib lua51.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_lua.pdb" /debug /out:"$(OUTDIR)\capi_lua.so" /implib:"$(OUTDIR)\capi_lua.lib" /libpath:"../../kudelrunsrc/lua/src" /base:@..\..\platforms\win32\BaseAddr.ref,capi_lua.so /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\lua_kuda.obj" \
	"$(INTDIR)\lua_config.obj" \
	"$(INTDIR)\lua_passwd.obj" \
	"$(INTDIR)\lua_request.obj" \
	"$(INTDIR)\lua_vmprep.obj" \
	"$(INTDIR)\capi_lua.obj" \
	"$(INTDIR)\lua_dbd.obj" \
	"$(INTDIR)\capi_lua.res" \
	"..\..\kudelrunsrc\kuda\Release\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Release\libkudadelman-1.lib" \
	"..\..\Release\libwwhy.lib"

"$(OUTDIR)\capi_lua.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\capi_lua.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_lua.so"
   if exist .\Release\capi_lua.so.manifest mt.exe -manifest .\Release\capi_lua.so.manifest -outputresource:.\Release\capi_lua.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "capi_lua - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_lua.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Debug" "libkudadelman - Win32 Debug" "libkuda - Win32 Debug" "$(OUTDIR)\capi_lua.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudadelman - Win32 DebugCLEAN" "libwwhy - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\lua_kuda.obj"
	-@erase "$(INTDIR)\lua_config.obj"
	-@erase "$(INTDIR)\lua_dbd.obj"
	-@erase "$(INTDIR)\lua_passwd.obj"
	-@erase "$(INTDIR)\lua_request.obj"
	-@erase "$(INTDIR)\lua_vmprep.obj"
	-@erase "$(INTDIR)\capi_lua.obj"
	-@erase "$(INTDIR)\capi_lua.res"
	-@erase "$(INTDIR)\capi_lua_src.idb"
	-@erase "$(INTDIR)\capi_lua_src.pdb"
	-@erase "$(OUTDIR)\capi_lua.exp"
	-@erase "$(OUTDIR)\capi_lua.lib"
	-@erase "$(OUTDIR)\capi_lua.pdb"
	-@erase "$(OUTDIR)\capi_lua.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/lua/src" /I "../clssl" /I "../database" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_LUA_DECLARE_EXPORT" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_lua_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_lua.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "_DEBUG" /d BIN_NAME="capi_lua.so" /d LONG_NAME="lua_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_lua.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib ws2_32.lib lua51.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_lua.pdb" /debug /out:"$(OUTDIR)\capi_lua.so" /implib:"$(OUTDIR)\capi_lua.lib" /libpath:"../../kudelrunsrc/lua/src" /base:@..\..\platforms\win32\BaseAddr.ref,capi_lua.so 
LINK32_OBJS= \
	"$(INTDIR)\lua_kuda.obj" \
	"$(INTDIR)\lua_config.obj" \
	"$(INTDIR)\lua_passwd.obj" \
	"$(INTDIR)\lua_request.obj" \
	"$(INTDIR)\lua_vmprep.obj" \
	"$(INTDIR)\capi_lua.obj" \
	"$(INTDIR)\lua_dbd.obj" \
	"$(INTDIR)\capi_lua.res" \
	"..\..\kudelrunsrc\kuda\Debug\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Debug\libkudadelman-1.lib" \
	"..\..\Debug\libwwhy.lib"

"$(OUTDIR)\capi_lua.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\capi_lua.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_lua.so"
   if exist .\Debug\capi_lua.so.manifest mt.exe -manifest .\Debug\capi_lua.so.manifest -outputresource:.\Debug\capi_lua.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("capi_lua.dep")
!INCLUDE "capi_lua.dep"
!ELSE 
!MESSAGE Warning: cannot find "capi_lua.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "capi_lua - Win32 Release" || "$(CFG)" == "capi_lua - Win32 Debug"

!IF  "$(CFG)" == "capi_lua - Win32 Release"

"libkuda - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\..\cAPIs\lua"

"libkuda - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\lua"

!ELSEIF  "$(CFG)" == "capi_lua - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\..\cAPIs\lua"

"libkuda - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\lua"

!ENDIF 

!IF  "$(CFG)" == "capi_lua - Win32 Release"

"libkudadelman - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" 
   cd "..\..\cAPIs\lua"

"libkudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\lua"

!ELSEIF  "$(CFG)" == "capi_lua - Win32 Debug"

"libkudadelman - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" 
   cd "..\..\cAPIs\lua"

"libkudadelman - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\lua"

!ENDIF 

!IF  "$(CFG)" == "capi_lua - Win32 Release"

"libwwhy - Win32 Release" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" 
   cd ".\cAPIs\lua"

"libwwhy - Win32 ReleaseCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" RECURSE=1 CLEAN 
   cd ".\cAPIs\lua"

!ELSEIF  "$(CFG)" == "capi_lua - Win32 Debug"

"libwwhy - Win32 Debug" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" 
   cd ".\cAPIs\lua"

"libwwhy - Win32 DebugCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" RECURSE=1 CLEAN 
   cd ".\cAPIs\lua"

!ENDIF 

SOURCE=..\..\build\win32\wwhy.rc

!IF  "$(CFG)" == "capi_lua - Win32 Release"


"$(INTDIR)\capi_lua.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_lua.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../build\win32" /d "NDEBUG" /d BIN_NAME="capi_lua.so" /d LONG_NAME="lua_capi for cLHy" $(SOURCE)


!ELSEIF  "$(CFG)" == "capi_lua - Win32 Debug"


"$(INTDIR)\capi_lua.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_lua.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../build\win32" /d "_DEBUG" /d BIN_NAME="capi_lua.so" /d LONG_NAME="lua_capi for cLHy" $(SOURCE)


!ENDIF 

SOURCE=.\lua_kuda.c

"$(INTDIR)\lua_kuda.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\lua_config.c

"$(INTDIR)\lua_config.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\lua_dbd.c

"$(INTDIR)\lua_dbd.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\lua_passwd.c

"$(INTDIR)\lua_passwd.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\lua_request.c

"$(INTDIR)\lua_request.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\lua_vmprep.c

"$(INTDIR)\lua_vmprep.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\capi_lua.c

"$(INTDIR)\capi_lua.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 

