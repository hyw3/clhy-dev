/**
 * The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "capi_lua.h"
#include "lua_kuda.h"
CLHYLOG_USE_CAPI(lua);

req_table_t *clhy_lua_check_kuda_table(lua_State *L, int index)
{
    req_table_t* t;
    luaL_checkudata(L, index, "Kuda.Table");
    t = lua_unboxpointer(L, index);
    return t;
}


void clhy_lua_push_kuda_table(lua_State *L, req_table_t *t)
{
    lua_boxpointer(L, t);
    luaL_getmetatable(L, "Kuda.Table");
    lua_setmetatable(L, -2);
}

static int lua_table_set(lua_State *L)
{
    req_table_t    *t = clhy_lua_check_kuda_table(L, 1);
    const char     *key = luaL_checkstring(L, 2);
    const char     *val = luaL_checkstring(L, 3);
    /* Unless it's the 'notes' table, check for newline chars */
    /* t->r will be NULL in case of the connection notes, but since 
       we aren't going to check anything called 'notes', we can safely 
       disregard checking whether t->r is defined.
    */
    if (strcmp(t->n, "notes") && clhy_strchr_c(val, '\n')) {
        char *badchar;
        char *replacement = kuda_pstrdup(t->r->pool, val);
        badchar = replacement;
        while ( (badchar = clhy_strchr(badchar, '\n')) ) {
            *badchar = ' ';
        }
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, t->r, CLHYLOGNO(02614)
                      "capi_lua: Value for '%s' in table '%s' contains newline!",
                  key, t->n);
        kuda_table_set(t->t, key, replacement);
    }
    else {
        kuda_table_set(t->t, key, val);
    }
    return 0;
}

static int lua_table_get(lua_State *L)
{
    req_table_t    *t = clhy_lua_check_kuda_table(L, 1);
    const char     *key = luaL_checkstring(L, 2);
    const char     *val = kuda_table_get(t->t, key);
    lua_pushstring(L, val);
    return 1;
}

static const luaL_Reg lua_table_methods[] = {
    {"set", lua_table_set},
    {"get", lua_table_get},
    {0, 0}
};


int clhy_lua_init(lua_State *L, kuda_pool_t *p)
{
    luaL_newmetatable(L, "Kuda.Table");
    luaL_register(L, "kuda_table", lua_table_methods);
    lua_pushstring(L, "__index");
    lua_pushstring(L, "get");
    lua_gettable(L, 2);
    lua_settable(L, 1);

    lua_pushstring(L, "__newindex");
    lua_pushstring(L, "set");
    lua_gettable(L, 2);
    lua_settable(L, 1);

    return 0;
}



