-*- mode:org -*-
* Building capi_lua
  The first step is to build capi_lua per the instructions in
  building-from-subversion.txt.

* Build and install LuaSocket
    http://www.cs.princeton.edu/~diego/professional/luasocket/
    FreeBSD: /usr/ports/net/luasocket

* Running Tests
  1. Replace clhy's wwhy.conf with test/test_wwhy.conf
  2. Customize the new wwhy.conf to match your directories
  3. Finally, to run the tests, start clhy and run:
     $ cd test
     $ lua ./test.lua
     FreeBSD: lua-5.1 ./test.lua
