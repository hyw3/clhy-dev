/**
 * The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "capi_lua.h"

#ifndef _APL_CONFIG_H_
#define _APL_CONFIG_H_

void clhy_lua_load_config_lcAPI(lua_State *L);

kuda_status_t clhy_lua_map_handler(clhy_lua_dir_cfg *cfg,
                                                const char *file,
                                                const char *function,
                                                const char *pattern,
                                                const char *scope);

#endif /* !_APL_CONFIG_H_ */
