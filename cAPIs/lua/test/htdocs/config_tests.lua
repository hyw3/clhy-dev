require 'string'

local count = 0

function handle(r)
    r:puts("success in handle " .. count)
end

function handle_server_vm(r)
    r:puts("hello from server scope " .. count)
    count = count + 1
end

function handle_request_vm(r)
    r:puts("hello from request scope " .. count)
    count = count + 1
end

function handle_conn_vm(r)
    r:puts("hello from request scope " .. count)
    count = count + 1
end