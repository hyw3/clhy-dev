/**
 * The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _CAPI_LUA_H_
#define _CAPI_LUA_H_

#include <stdio.h>

#include "wwhy.h"
#include "http_core.h"
#include "http_config.h"
#include "http_request.h"
#include "http_log.h"
#include "http_protocol.h"
#include "clhy_regex.h"

#include "clhy_config.h"
#include "util_filter.h"

#include "kuda_thread_rwlock.h"
#include "kuda_strings.h"
#include "kuda_tables.h"
#include "kuda_hash.h"
#include "kuda_buckets.h"
#include "kuda_file_info.h"
#include "kuda_time.h"
#include "kuda_hooks.h"
#include "kuda_reslist.h"

/* Allow for Lua 5.2 backwards compatibility */
#define LUA_COMPAT_ALL

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#if LUA_VERSION_NUM > 501
/* Load mode for lua_load() */
#define lua_load(a,b,c,d) lua_load(a,b,c,d,NULL)
#define lua_resume(a,b)   lua_resume(a, NULL, b)
#endif

/* Create a set of CLHY_LUA_DECLARE(type), CLHY_LUA_DECLARE_NONSTD(type) and
 * CLHY_LUA_DECLARE_DATA with appropriate export and import tags for the platform
 */
#if !defined(WIN32)
#define CLHY_LUA_DECLARE(type)            type
#define CLHY_LUA_DECLARE_NONSTD(type)     type
#define CLHY_LUA_DECLARE_DATA
#elif defined(CLHY_LUA_DECLARE_STATIC)
#define CLHY_LUA_DECLARE(type)            type __stdcall
#define CLHY_LUA_DECLARE_NONSTD(type)     type
#define CLHY_LUA_DECLARE_DATA
#elif defined(CLHY_LUA_DECLARE_EXPORT)
#define CLHY_LUA_DECLARE(type)            __declspec(dllexport) type __stdcall
#define CLHY_LUA_DECLARE_NONSTD(type)     __declspec(dllexport) type
#define CLHY_LUA_DECLARE_DATA             __declspec(dllexport)
#else
#define CLHY_LUA_DECLARE(type)            __declspec(dllimport) type __stdcall
#define CLHY_LUA_DECLARE_NONSTD(type)     __declspec(dllimport) type
#define CLHY_LUA_DECLARE_DATA             __declspec(dllimport)
#endif


#include "lua_request.h"
#include "lua_vmprep.h"

typedef enum {
    CLHY_LUA_INHERIT_UNSET        = -1,
    CLHY_LUA_INHERIT_NONE         =  0,
    CLHY_LUA_INHERIT_PARENT_FIRST =  1,
    CLHY_LUA_INHERIT_PARENT_LAST  =  2
} clhy_lua_inherit_t;

/**
 * make a userdata out of a C pointer, and vice versa
 * instead of using lightuserdata
 */
#ifndef lua_boxpointer
#define lua_boxpointer(L,u) (*(void **)(lua_newuserdata(L, sizeof(void *))) = (u))
#define lua_unboxpointer(L,i)   (*(void **)(lua_touserdata(L, i)))
#endif

void clhy_lua_rstack_dump(lua_State *L, request_rec *r, const char *msg);

typedef struct
{
    kuda_array_header_t *package_paths;
    kuda_array_header_t *package_cpaths;

    /**
     * mapped handlers/filters
     */
    kuda_array_header_t *mapped_handlers;
    kuda_array_header_t *mapped_filters;

    kuda_pool_t *pool;

    /**
     * CLHY_LUA_SCOPE_ONCE | CLHY_LUA_SCOPE_REQUEST | CLHY_LUA_SCOPE_CONN | CLHY_LUA_SCOPE_SERVER
     */
    unsigned int vm_scope;
    unsigned int vm_min;
    unsigned int vm_max;

    /* info for the hook harnesses */
    kuda_hash_t *hooks;          /* <wombat_hook_info> */

    /* the actual directory being configured */
    const char *dir;
  
    /* Whether Lua scripts in a sub-dir are run before parents */
    clhy_lua_inherit_t inherit;
    
    /**
     * CLHY_LUA_CACHE_NEVER | CLHY_LUA_CACHE_STAT | CLHY_LUA_CACHE_FOREVER
     */
    unsigned int codecache;

} clhy_lua_dir_cfg;

typedef struct
{
    kuda_hash_t *vm_reslists;
    kuda_thread_rwlock_t *vm_reslists_lock;

    /* value of the LuaRoot directive */
    const char *root_path;
} clhy_lua_server_cfg;

typedef struct
{
    const char *function_name;
    clhy_lua_vm_spec *spec;
} mapped_request_details;

typedef struct
{
    mapped_request_details *mapped_request_details;
    kuda_hash_t *request_scoped_vms;
} clhy_lua_request_cfg;

typedef struct
{
    lua_State *L;
    const char *function;
} clhy_lua_filter_ctx;

extern cAPI CLHY_CAPI_DECLARE_DATA lua_capi;

KUDA_DECLARE_EXTERNAL_HOOK(clhy_lua, CLHY_LUA, int, lua_open,
                          (lua_State *L, kuda_pool_t *p))

KUDA_DECLARE_EXTERNAL_HOOK(clhy_lua, CLHY_LUA, int, lua_request,
                          (lua_State *L, request_rec *r))

const char *clhy_lua_ssl_val(kuda_pool_t *p, server_rec *s, conn_rec *c,
                           request_rec *r, const char *var);

int clhy_lua_ssl_is_https(conn_rec *c);

#endif /* !_CAPI_LUA_H_ */
