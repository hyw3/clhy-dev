/**
 * The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _LUA_REQUEST_H_
#define _LUA_REQUEST_H_

#include "capi_lua.h"
#include "util_varbuf.h"

void clhy_lua_load_request_lcAPI(lua_State *L, kuda_pool_t *p);
void clhy_lua_push_connection(lua_State *L, conn_rec *r);
void clhy_lua_push_server(lua_State *L, server_rec *r);
void clhy_lua_push_request(lua_State *L, request_rec *r);

#define APL_REQ_FUNTYPE_STRING      1
#define APL_REQ_FUNTYPE_INT         2
#define APL_REQ_FUNTYPE_TABLE       3
#define APL_REQ_FUNTYPE_LUACFUN     4
#define APL_REQ_FUNTYPE_BOOLEAN     5

typedef struct
{
    const void *fun;
    int type;
} req_fun_t;


/* Struct to use as userdata for request_rec tables */
typedef struct
{
    request_rec *r; /* Request_rec */
    kuda_table_t *t; /* kuda_table_t* */
     char  *n; /* name of table */
} req_table_t;

typedef struct {
    int type;
    size_t size;
    size_t vb_size;
    lua_Number number;
    struct clhy_varbuf vb;
} lua_ivm_object;

#endif /* !_LUA_REQUEST_H_ */
