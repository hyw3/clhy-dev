/**
 * The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#include "wwhy.h"

#include "kuda_thread_rwlock.h"
#include "kuda_strings.h"
#include "kuda_tables.h"
#include "kuda_hash.h"
#include "kuda_buckets.h"
#include "kuda_file_info.h"
#include "kuda_time.h"
#include "kuda_pools.h"
#include "kuda_reslist.h"


#ifndef VMPREP_H
#define VMPREP_H

#define CLHY_LUA_SCOPE_UNSET         0
#define CLHY_LUA_SCOPE_ONCE          1
#define CLHY_LUA_SCOPE_REQUEST       2
#define CLHY_LUA_SCOPE_CONN          3
#define CLHY_LUA_SCOPE_THREAD        4
#define CLHY_LUA_SCOPE_SERVER        5

#define CLHY_LUA_CACHE_UNSET         0
#define CLHY_LUA_CACHE_NEVER         1
#define CLHY_LUA_CACHE_STAT          2
#define CLHY_LUA_CACHE_FOREVER       3

#define CLHY_LUA_FILTER_INPUT        1
#define CLHY_LUA_FILTER_OUTPUT       2

typedef void (*clhy_lua_state_open_callback) (lua_State *L, kuda_pool_t *p,
                                             void *ctx);
/**
 * Specification for a lua virtual machine
 */
typedef struct
{
    /* NEED TO ADD ADDITIONAL PACKAGE PATHS AS PART OF SPEC INSTEAD OF DIR CONFIG */
    kuda_array_header_t *package_paths;
    kuda_array_header_t *package_cpaths;

    /* name of base file to load in the vm */
    const char *file;

    /* APL_SCOPE_ONCE | APL_SCOPE_REQUEST | APL_SCOPE_CONN | APL_SCOPE_THREAD | APL_SCOPE_SERVER */
    int scope;
    unsigned int vm_min;
    unsigned int vm_max;

    clhy_lua_state_open_callback cb;
    void* cb_arg;

    /* pool to use for lifecycle if APL_SCOPE_ONCE is set, otherwise unused */
    kuda_pool_t *pool;

    /* Pre-compiled Lua Byte code to load directly.  If bytecode_len is >0,
     * the file part of this structure is ignored for loading purposes, but
     * it is used for error messages.
     */
    const char *bytecode;
    kuda_size_t bytecode_len;
    
    int codecache;
} clhy_lua_vm_spec;

typedef struct
{
    const char *function_name;
    const char *file_name;
    int scope;
    clhy_regex_t *uri_pattern;
    const char *bytecode;
    kuda_size_t bytecode_len;
    int codecache;
} clhy_lua_mapped_handler_spec;

typedef struct
{
    const char *function_name;
    const char *file_name;
    const char* filter_name;
    int         direction; /* CLHY_LUA_FILTER_INPUT | CLHY_LUA_FILTER_OUTPUT */
} clhy_lua_filter_handler_spec;

typedef struct {
    kuda_size_t runs;
    kuda_time_t modified;
    kuda_off_t  size;
} clhy_lua_finfo;

typedef struct {
    lua_State* L;
    clhy_lua_finfo* finfo;
} clhy_lua_server_spec;

/**
 * Fake out addition of the "clhydelman" cAPI
 */
void clhy_lua_load_clhydelman_lcAPI(lua_State *L);

/*
 * alternate means of getting lua_State (preferred eventually)
 * Obtain a lua_State which has loaded file and is associated with lifecycle_pool
 * If one exists, will return extant one, otherwise will create, attach, and return
 * This does no locking around the lua_State, so if the pool is shared between
 * threads, locking is up the client.
 *
 * @lifecycle_pool -> pool whose lifeycle controls the lua_State
 * @file file to be opened, also used as a key for uniquing lua_States
 * @cb callback for vm initialization called *before* the file is opened
 * @ctx a baton passed to cb
 */
lua_State *clhy_lua_get_lua_state(kuda_pool_t *lifecycle_pool,
                                                clhy_lua_vm_spec *spec, request_rec* r);

#if KUDA_HAS_THREADS || defined(DOXYGEN)
/*
 * Initialize capi_lua mutex.
 * @pool pool for mutex
 * @s server_rec for logging
 */
void clhy_lua_init_mutex(kuda_pool_t *pool, server_rec *s);
#endif

#endif
