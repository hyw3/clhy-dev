/**
 * The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _LUA_DBD_H_
#define _LUA_DBD_H_

#include "capi_lua.h"
#include "kuda.h"
#include "kuda_dbd.h"
#include "capi_dbd.h"

#define LUA_DBTYPE_KUDA_DBD 0
#define LUA_DBTYPE_CAPI_DBD 1
typedef struct
{
    kuda_dbd_t               *handle;
    const kuda_dbd_driver_t  *driver;
    int                     alive;
    kuda_pool_t              *pool;
    char                    type;
    clhy_dbd_t *              dbdhandle;
    server_rec              *server;
} lua_db_handle;

typedef struct {
    const kuda_dbd_driver_t  *driver;
    int                     rows;
    int                     cols;
    kuda_dbd_results_t       *results;
    kuda_pool_t              *pool;
} lua_db_result_set;

typedef struct {
    kuda_dbd_prepared_t      *statement;
    int                     variables;
    lua_db_handle           *db;
} lua_db_prepared_statement;

int lua_db_acquire(lua_State* L);
int lua_db_escape(lua_State* L);
int lua_db_close(lua_State* L);
int lua_db_prepare(lua_State* L);
int lua_db_prepared(lua_State* L);
int lua_db_select(lua_State* L);
int lua_db_query(lua_State* L);
int lua_db_prepared_select(lua_State* L);
int lua_db_prepared_query(lua_State* L);
int lua_db_get_row(lua_State* L);
int lua_db_gc(lua_State* L);
int lua_db_active(lua_State* L);

#endif /* !_LUA_DBD_H_ */
