/**
 * The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _LUA_KUDA_H_
#define _LUA_KUDA_H_

#include "scoreboard.h"
#include "http_main.h"
#include "clhy_core.h"
#include "kuda_md5.h"
#include "kuda_sha1.h"
#include "kuda_poll.h"
#include "kuda.h"
#include "kuda_tables.h"
#include "kuda_base64.h"


int clhy_lua_init(lua_State *L, kuda_pool_t * p);
req_table_t *clhy_lua_check_kuda_table(lua_State *L, int index);
void clhy_lua_push_kuda_table(lua_State *L, req_table_t *t);

#endif /* !_LUA_KUDA_H_ */
