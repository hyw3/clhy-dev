
dnl CLHYKUDEL_CAPI(name, helptext[, objects[, structname[, default[, config]]]])

CLHYKUDEL_CAPIPATH_INIT(ldap)

ldap_objects="util_ldap.lo util_ldap_cache.lo util_ldap_cache_mgr.lo"
CLHYKUDEL_CAPI(ldap, LDAP caching and connection pooling services, $ldap_objects, , most , [
  CLHYKUDEL_CHECK_KUDA_HAS_LDAP
  if test "$ac_cv_KUDA_HAS_LDAP" = "yes" ; then
    if test -z "$kudelman_config" ; then
      LDAP_LIBS="`$kuda_config --ldap-libs`"
    else
      LDAP_LIBS="`$kudelman_config --ldap-libs`"
    fi
    KUDA_ADDTO(CAPI_LDAP_LDADD, [$LDAP_LIBS])
    AC_SUBST(CAPI_LDAP_LDADD)
  else
    AC_MSG_WARN([kuda/kuda-delman is compiled without ldap support])
    enable_ldap=no
  fi
])

KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

CLHYKUDEL_CAPIPATH_FINISH
