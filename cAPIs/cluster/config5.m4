
CLHYKUDEL_CAPIPATH_INIT(cluster)

heartbeat_objects='capi_heartbeat.lo'

case "$host" in
  *os2*)
    # OS2 DLLs must resolve all symbols at build time
    # and we need some from the watchdog cAPI
    heartbeat_objects="$heartbeat_objects ../core/capi_watchdog.la"
    ;;
esac

CLHYKUDEL_CAPI(heartbeat, Generates Heartbeats, $heartbeat_objects, , , , watchdog)
CLHYKUDEL_CAPI(heartmonitor, Collects Heartbeats, , , )

CLHYKUDEL_CAPIPATH_FINISH
