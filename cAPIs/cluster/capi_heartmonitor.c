/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "http_core.h"
#include "http_protocol.h"
#include "kuda_strings.h"
#include "kuda_hash.h"
#include "kuda_time.h"
#include "clhy_core.h"
#include "scoreboard.h"
#include "capi_watchdog.h"
#include "clhy_slotmem.h"
#include "heartbeat.h"


#ifndef HM_UPDATE_SEC
/* How often we update the stats file */
/* TODO: Make a runtime config */
#define HM_UPDATE_SEC (5)
#endif

#define HM_WATHCHDOG_NAME ("_heartmonitor_")

static const clhy_slotmem_provider_t *storage = NULL;
static clhy_slotmem_instance_t *slotmem = NULL;
static int maxworkers = 0;

cAPI CLHY_CAPI_DECLARE_DATA heartmonitor_capi;

typedef struct hm_server_t
{
    const char *ip;
    int busy;
    int ready;
    unsigned int port;
    kuda_time_t seen;
} hm_server_t;

typedef struct hm_ctx_t
{
    int active;
    const char *storage_path;
    clhy_watchdog_t *watchdog;
    kuda_interval_time_t interval;
    kuda_sockaddr_t *mcast_addr;
    kuda_status_t status;
    volatile int keep_running;
    kuda_socket_t *sock;
    kuda_pool_t *p;
    kuda_hash_t *servers;
    server_rec *s;
} hm_ctx_t;

typedef struct hm_slot_server_ctx_t {
  hm_server_t *s;
  int found;
  unsigned int item_id;
} hm_slot_server_ctx_t;

static kuda_status_t hm_listen(hm_ctx_t *ctx)
{
    kuda_status_t rv;

    rv = kuda_socket_create(&ctx->sock, ctx->mcast_addr->family,
                           SOCK_DGRAM, KUDA_PROTO_UDP, ctx->p);

    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02068)
                     "Failed to create listening socket.");
        return rv;
    }

    rv = kuda_socket_opt_set(ctx->sock, KUDA_SO_REUSEADDR, 1);
    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02069)
                     "Failed to set KUDA_SO_REUSEADDR to 1 on socket.");
        return rv;
    }


    rv = kuda_socket_opt_set(ctx->sock, KUDA_SO_NONBLOCK, 1);
    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02070)
                     "Failed to set KUDA_SO_NONBLOCK to 1 on socket.");
        return rv;
    }

    rv = kuda_socket_bind(ctx->sock, ctx->mcast_addr);
    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02071)
                     "Failed to bind on socket.");
        return rv;
    }

    rv = kuda_mcast_join(ctx->sock, ctx->mcast_addr, NULL, NULL);

    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02072)
                     "Failed to join multicast group");
        return rv;
    }

    rv = kuda_mcast_loopback(ctx->sock, 1);
    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02073)
                     "Failed to accept localhost mulitcast on socket.");
        return rv;
    }

    return KUDA_SUCCESS;
}

/* XXX: The same exists in capi_lbmethod_heartbeat.c where it is named argstr_to_table */
static void qs_to_table(const char *input, kuda_table_t *parms,
                        kuda_pool_t *p)
{
    char *key;
    char *value;
    char *query_string;
    char *strtok_state;

    if (input == NULL) {
        return;
    }

    query_string = kuda_pstrdup(p, input);

    key = kuda_strtok(query_string, "&", &strtok_state);
    while (key) {
        value = strchr(key, '=');
        if (value) {
            *value = '\0';      /* Split the string in two */
            value++;            /* Skip passed the = */
        }
        else {
            value = "1";
        }
        clhy_unescape_url(key);
        clhy_unescape_url(value);
        kuda_table_set(parms, key, value);
        /*
           clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03182)
           "Found query arg: %s = %s", key, value);
         */
        key = kuda_strtok(NULL, "&", &strtok_state);
    }
}


#define SEEN_TIMEOUT (30)

/* Store in the slotmem */
static kuda_status_t hm_update(void* mem, void *data, kuda_pool_t *p)
{
    hm_slot_server_t *old = (hm_slot_server_t *) mem;
    hm_slot_server_ctx_t *s = (hm_slot_server_ctx_t *) data;
    hm_server_t *new = s->s;
    if (strncmp(old->ip, new->ip, MAXIPSIZE)==0) {
        s->found = 1;
        old->busy = new->busy;
        old->ready = new->ready;
        old->seen = new->seen;
    }
    return KUDA_SUCCESS;
}
/* Read the id corresponding to the entry in the slotmem */
static kuda_status_t hm_readid(void* mem, void *data, kuda_pool_t *p)
{
    hm_slot_server_t *old = (hm_slot_server_t *) mem;
    hm_slot_server_ctx_t *s = (hm_slot_server_ctx_t *) data;
    hm_server_t *new = s->s;
    if (strncmp(old->ip, new->ip, MAXIPSIZE)==0) {
        s->found = 1;
        s->item_id = old->id;
    }
    return KUDA_SUCCESS;
}
/* update the entry or create it if not existing */
static  kuda_status_t  hm_slotmem_update_stat(hm_server_t *s, kuda_pool_t *pool)
{
    /* We call do_all (to try to update) otherwise grab + put */
    hm_slot_server_ctx_t ctx;
    ctx.s = s;
    ctx.found = 0;
    storage->doall(slotmem, hm_update, &ctx, pool);
    if (!ctx.found) {
        unsigned int i;
        hm_slot_server_t hmserver;
        memcpy(hmserver.ip, s->ip, MAXIPSIZE);
        hmserver.busy = s->busy;
        hmserver.ready = s->ready;
        hmserver.seen = s->seen;
        /* XXX locking for grab() / put() */
        storage->grab(slotmem, &i);
        hmserver.id = i;
        storage->put(slotmem, i, (unsigned char *)&hmserver, sizeof(hmserver));
    }
    return KUDA_SUCCESS;
}
static  kuda_status_t  hm_slotmem_remove_stat(hm_server_t *s, kuda_pool_t *pool)
{
    hm_slot_server_ctx_t ctx;
    ctx.s = s;
    ctx.found = 0;
    storage->doall(slotmem, hm_readid, &ctx, pool);
    if (ctx.found) {
        storage->release(slotmem, ctx.item_id);
    }
    return KUDA_SUCCESS;
}
static kuda_status_t hm_file_update_stat(hm_ctx_t *ctx, hm_server_t *s, kuda_pool_t *pool)
{
    kuda_status_t rv;
    kuda_file_t *fp;
    kuda_file_t *fpin;
    kuda_time_t now;
    kuda_time_t fage;
    kuda_finfo_t fi;
    int updated = 0;
    char *path = kuda_pstrcat(pool, ctx->storage_path, ".tmp.XXXXXX", NULL);


    /* TODO: Update stats file (!) */
    rv = kuda_file_mktemp(&fp, path, KUDA_CREATE | KUDA_WRITE, pool);

    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02074)
                     "Unable to open tmp file: %s", path);
        return rv;
    }
    rv = kuda_file_open(&fpin, ctx->storage_path, KUDA_READ|KUDA_BINARY|KUDA_BUFFERED,
                       KUDA_PLATFORM_DEFAULT, pool);

    now = kuda_time_now();
    if (rv == KUDA_SUCCESS) {
        char *t;
        kuda_table_t *hbt = kuda_table_make(pool, 10);
        kuda_bucket_alloc_t *ba;
        kuda_bucket_brigade *bb;
        kuda_bucket_brigade *tmpbb;

        rv = kuda_file_info_get(&fi, KUDA_FINFO_SIZE | KUDA_FINFO_MTIME, fpin);
        if (rv) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02075)
                         "Unable to read file: %s", ctx->storage_path);
            return rv;
        }

        /* Read the file and update the line corresponding to the node */
        ba = kuda_bucket_alloc_create(pool);
        bb = kuda_brigade_create(pool, ba);
        kuda_brigade_insert_file(bb, fpin, 0, fi.size, pool);
        tmpbb = kuda_brigade_create(pool, ba);
        fage = kuda_time_sec(now - fi.mtime);
        do {
            char buf[4096];
            const char *ip;
            kuda_size_t bsize = sizeof(buf);

            kuda_brigade_cleanup(tmpbb);
            if (KUDA_BRIGADE_EMPTY(bb)) {
                break;
            }
            rv = kuda_brigade_split_line(tmpbb, bb,
                                        KUDA_BLOCK_READ, sizeof(buf));

            if (rv) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02076)
                             "Unable to read from file: %s", ctx->storage_path);
                return rv;
            }

            kuda_brigade_flatten(tmpbb, buf, &bsize);
            if (bsize == 0) {
                break;
            }
            buf[bsize - 1] = 0;
            t = strchr(buf, ' ');
            if (t) {
                ip = kuda_pstrmemdup(pool, buf, t - buf);
            }
            else {
                ip = NULL;
            }

            if (!ip || buf[0] == '#') {
                /* copy things we can't process */
                kuda_file_printf(fp, "%s\n", buf);
            }
            else if (strcmp(ip, s->ip) != 0 ) {
                hm_server_t node;
                kuda_time_t seen;
                const char *val;

                /* Update seen time according to the last file modification */
                kuda_table_clear(hbt);
                qs_to_table(kuda_pstrdup(pool, t), hbt, pool);
                if ((val = kuda_table_get(hbt, "busy"))) {
                    node.busy = atoi(val);
                }
                else {
                    node.busy = 0;
                }

                if ((val = kuda_table_get(hbt, "ready"))) {
                    node.ready = atoi(val);
                }
                else {
                    node.ready = 0;
                }

                if ((val = kuda_table_get(hbt, "lastseen"))) {
                    node.seen = atoi(val);
                }
                else {
                    node.seen = SEEN_TIMEOUT;
                }
                seen = fage + node.seen;

                if ((val = kuda_table_get(hbt, "port"))) {
                    node.port = atoi(val);
                }
                else {
                    node.port = 80;
                }
                kuda_file_printf(fp, "%s &ready=%u&busy=%u&lastseen=%u&port=%u\n",
                                ip, node.ready, node.busy, (unsigned int) seen, node.port);
            }
            else {
                kuda_time_t seen;
                seen = kuda_time_sec(now - s->seen);
                kuda_file_printf(fp, "%s &ready=%u&busy=%u&lastseen=%u&port=%u\n",
                                s->ip, s->ready, s->busy, (unsigned int) seen, s->port);
                updated = 1;
            }
        } while (1);
    }

    if (!updated) {
        kuda_time_t seen;
        seen = kuda_time_sec(now - s->seen);
        kuda_file_printf(fp, "%s &ready=%u&busy=%u&lastseen=%u&port=%u\n",
                        s->ip, s->ready, s->busy, (unsigned int) seen, s->port);
    }

    rv = kuda_file_flush(fp);
    if (rv) {
      clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02077)
                   "Unable to flush file: %s", path);
      return rv;
    }

    rv = kuda_file_close(fp);
    if (rv) {
      clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02078)
                   "Unable to close file: %s", path);
      return rv;
    }

    rv = kuda_file_perms_set(path,
                            KUDA_FPROT_UREAD | KUDA_FPROT_GREAD |
                            KUDA_FPROT_WREAD);
    if (rv && rv != KUDA_INCOMPLETE && rv != KUDA_ENOTIMPL) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02079)
                     "Unable to set file permissions on %s",
                     path);
        return rv;
    }

    rv = kuda_file_rename(path, ctx->storage_path, pool);

    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02080)
                     "Unable to move file: %s -> %s", path,
                     ctx->storage_path);
        return rv;
    }

    return KUDA_SUCCESS;
}
static  kuda_status_t  hm_update_stat(hm_ctx_t *ctx, hm_server_t *s, kuda_pool_t *pool)
{
    if (slotmem)
        return hm_slotmem_update_stat(s, pool);
    else
        return hm_file_update_stat(ctx, s, pool);
}

/* Store in a file */
static kuda_status_t hm_file_update_stats(hm_ctx_t *ctx, kuda_pool_t *p)
{
    kuda_status_t rv;
    kuda_file_t *fp;
    kuda_hash_index_t *hi;
    kuda_time_t now;
    char *path = kuda_pstrcat(p, ctx->storage_path, ".tmp.XXXXXX", NULL);
    /* TODO: Update stats file (!) */
    rv = kuda_file_mktemp(&fp, path, KUDA_CREATE | KUDA_WRITE, p);

    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02081)
                     "Unable to open tmp file: %s", path);
        return rv;
    }

    now = kuda_time_now();
    for (hi = kuda_hash_first(p, ctx->servers);
         hi != NULL; hi = kuda_hash_next(hi)) {
        hm_server_t *s = NULL;
        kuda_time_t seen;
        kuda_hash_this(hi, NULL, NULL, (void **) &s);
        seen = kuda_time_sec(now - s->seen);
        if (seen > SEEN_TIMEOUT) {
            /*
             * Skip this entry from the heartbeat file -- when it comes back,
             * we will reuse the memory...
             */
        }
        else {
            kuda_file_printf(fp, "%s &ready=%u&busy=%u&lastseen=%u&port=%u\n",
                            s->ip, s->ready, s->busy, (unsigned int) seen, s->port);
        }
    }

    rv = kuda_file_flush(fp);
    if (rv) {
      clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02082)
                   "Unable to flush file: %s", path);
      return rv;
    }

    rv = kuda_file_close(fp);
    if (rv) {
      clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02083)
                   "Unable to close file: %s", path);
      return rv;
    }

    rv = kuda_file_perms_set(path,
                            KUDA_FPROT_UREAD | KUDA_FPROT_GREAD |
                            KUDA_FPROT_WREAD);
    if (rv && rv != KUDA_INCOMPLETE && rv != KUDA_ENOTIMPL) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02084)
                     "Unable to set file permissions on %s",
                     path);
        return rv;
    }

    rv = kuda_file_rename(path, ctx->storage_path, p);

    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02085)
                     "Unable to move file: %s -> %s", path,
                     ctx->storage_path);
        return rv;
    }

    return KUDA_SUCCESS;
}
/* Store in a slotmem */
static kuda_status_t hm_slotmem_update_stats(hm_ctx_t *ctx, kuda_pool_t *p)
{
    kuda_status_t rv;
    kuda_time_t now;
    kuda_hash_index_t *hi;
    now = kuda_time_now();
    for (hi = kuda_hash_first(p, ctx->servers);
         hi != NULL; hi = kuda_hash_next(hi)) {
        hm_server_t *s = NULL;
        kuda_time_t seen;
        kuda_hash_this(hi, NULL, NULL, (void **) &s);
        seen = kuda_time_sec(now - s->seen);
        if (seen > SEEN_TIMEOUT) {
            /* remove it */
            rv = hm_slotmem_remove_stat(s, p);
        } else {
            /* update it */
            rv = hm_slotmem_update_stat(s, p);
        }
        if (rv !=KUDA_SUCCESS)
            return rv;
    }
    return KUDA_SUCCESS;
}
/* Store/update the stats */
static kuda_status_t hm_update_stats(hm_ctx_t *ctx, kuda_pool_t *p)
{
    if (slotmem)
        return hm_slotmem_update_stats(ctx, p);
    else
        return hm_file_update_stats(ctx, p);
}

static hm_server_t *hm_get_server(hm_ctx_t *ctx, const char *ip, const int port)
{
    hm_server_t *s;

    s = kuda_hash_get(ctx->servers, ip, KUDA_HASH_KEY_STRING);

    if (s == NULL) {
        s = kuda_palloc(ctx->p, sizeof(hm_server_t));
        s->ip = kuda_pstrdup(ctx->p, ip);
        s->port = port;
        s->ready = 0;
        s->busy = 0;
        s->seen = 0;
        kuda_hash_set(ctx->servers, s->ip, KUDA_HASH_KEY_STRING, s);
    }

    return s;
}

/* Process a message received from a backend node */
static void hm_processmsg(hm_ctx_t *ctx, kuda_pool_t *p,
                                  kuda_sockaddr_t *from, char *buf, int len)
{
    kuda_table_t *tbl;

    buf[len] = '\0';

    tbl = kuda_table_make(p, 10);

    qs_to_table(buf, tbl, p);

    if (kuda_table_get(tbl, "v") != NULL &&
        kuda_table_get(tbl, "busy") != NULL &&
        kuda_table_get(tbl, "ready") != NULL) {
        char *ip;
        int port = 80;
        hm_server_t *s;
        /* TODO: REMOVE ME BEFORE PRODUCTION (????) */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, ctx->s, CLHYLOGNO(02086)
                     "%pI busy=%s ready=%s", from,
                     kuda_table_get(tbl, "busy"), kuda_table_get(tbl, "ready"));

        kuda_sockaddr_ip_get(&ip, from);

        if (kuda_table_get(tbl, "port") != NULL)
            port = atoi(kuda_table_get(tbl, "port"));

        s = hm_get_server(ctx, ip, port);

        s->busy = atoi(kuda_table_get(tbl, "busy"));
        s->ready = atoi(kuda_table_get(tbl, "ready"));
        s->seen = kuda_time_now();
    }
    else {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, ctx->s, CLHYLOGNO(02087)
                     "malformed message from %pI",
                     from);
    }

}
/* Read message from multicast socket */
#define MAX_MSG_LEN (1000)
static kuda_status_t hm_recv(hm_ctx_t *ctx, kuda_pool_t *p)
{
    char buf[MAX_MSG_LEN + 1];
    kuda_sockaddr_t from;
    kuda_size_t len = MAX_MSG_LEN;
    kuda_status_t rv;

    from.pool = p;

    rv = kuda_socket_recvfrom(&from, ctx->sock, 0, buf, &len);

    if (KUDA_STATUS_IS_EAGAIN(rv)) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02088) "would block");
        return KUDA_SUCCESS;
    }
    else if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02089) "recvfrom failed");
        return rv;
    }

    hm_processmsg(ctx, p, &from, buf, len);

    return rv;
}

static kuda_status_t hm_watchdog_callback(int state, void *data,
                                         kuda_pool_t *pool)
{
    kuda_status_t rv = KUDA_SUCCESS;
    kuda_time_t cur, now;
    hm_ctx_t *ctx = (hm_ctx_t *)data;

    if (!ctx->active) {
        return rv;
    }

    switch (state) {
        case CLHY_WATCHDOG_STATE_STARTING:
            rv = hm_listen(ctx);
            if (rv) {
                ctx->status = rv;
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, ctx->s, CLHYLOGNO(02090)
                             "Unable to listen for connections!");
            }
            else {
                ctx->keep_running = 1;
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, ctx->s, CLHYLOGNO(02091)
                             "%s listener started.",
                             HM_WATHCHDOG_NAME);
            }
        break;
        case CLHY_WATCHDOG_STATE_RUNNING:
            /* store in the slotmem or in the file depending on configuration */
            hm_update_stats(ctx, pool);
            cur = now = kuda_time_sec(kuda_time_now());
            /* TODO: Insted HN_UPDATE_SEC use
             * the ctx->interval
             */
            while ((now - cur) < kuda_time_sec(ctx->interval)) {
                int n;
                kuda_status_t rc;
                kuda_pool_t *p;
                kuda_pollfd_t pfd;
                kuda_interval_time_t timeout;

                kuda_pool_create(&p, pool);

                pfd.desc_type = KUDA_POLL_SOCKET;
                pfd.desc.s = ctx->sock;
                pfd.p = p;
                pfd.reqevents = KUDA_POLLIN;

                timeout = kuda_time_from_sec(1);

                rc = kuda_poll(&pfd, 1, &n, timeout);

                if (!ctx->keep_running) {
                    kuda_pool_destroy(p);
                    break;
                }
                if (rc == KUDA_SUCCESS && (pfd.rtnevents & KUDA_POLLIN)) {
                    hm_recv(ctx, p);
                }
                now = kuda_time_sec(kuda_time_now());
                kuda_pool_destroy(p);
            }
        break;
        case CLHY_WATCHDOG_STATE_STOPPING:
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, ctx->s, CLHYLOGNO(02092)
                         "stopping %s listener.",
                         HM_WATHCHDOG_NAME);

            ctx->keep_running = 0;
            if (ctx->sock) {
                kuda_socket_close(ctx->sock);
                ctx->sock = NULL;
            }
        break;
    }
    return rv;
}

static int hm_post_config(kuda_pool_t *p, kuda_pool_t *plog,
                          kuda_pool_t *ptemp, server_rec *s)
{
    kuda_status_t rv;
    hm_ctx_t *ctx = clhy_get_capi_config(s->capi_config,
                                         &heartmonitor_capi);
    KUDA_OPTIONAL_FN_TYPE(clhy_watchdog_get_instance) *hm_watchdog_get_instance;
    KUDA_OPTIONAL_FN_TYPE(clhy_watchdog_register_callback) *hm_watchdog_register_callback;

    hm_watchdog_get_instance = KUDA_RETRIEVE_OPTIONAL_FN(clhy_watchdog_get_instance);
    hm_watchdog_register_callback = KUDA_RETRIEVE_OPTIONAL_FN(clhy_watchdog_register_callback);
    if (!hm_watchdog_get_instance || !hm_watchdog_register_callback) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, s, CLHYLOGNO(02093)
                     "capi_watchdog is required");
        return !OK;
    }

    /* Create the slotmem */
    if (clhy_state_query(CLHY_SQ_MAIN_STATE) == CLHY_SQ_MS_CREATE_CONFIG) {
        /* this is the real thing */
        if (maxworkers) {
            storage = clhy_lookup_provider(CLHY_SLOTMEM_PROVIDER_GROUP, "shm",
                                         CLHY_SLOTMEM_PROVIDER_VERSION);
            if (!storage) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, 0, s, CLHYLOGNO(02284)
                             "failed to lookup provider 'shm' for '%s', "
                             "maybe you need to load capi_slotmem_shm?",
                             CLHY_SLOTMEM_PROVIDER_GROUP);
                return !OK;
            }
            storage->create(&slotmem, "capi_heartmonitor", sizeof(hm_slot_server_t), maxworkers, CLHY_SLOTMEM_TYPE_PREGRAB, p);
            if (!slotmem) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, 0, s, CLHYLOGNO(02285)
                             "slotmem_create for status failed");
                return !OK;
            }
        }
    }

    if (!ctx->active) {
        return OK;
    }
    rv = hm_watchdog_get_instance(&ctx->watchdog,
                                  HM_WATHCHDOG_NAME,
                                  0, 1, p);
    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(02094)
                     "Failed to create watchdog instance (%s)",
                     HM_WATHCHDOG_NAME);
        return !OK;
    }
    /* Register a callback with zero interval. */
    rv = hm_watchdog_register_callback(ctx->watchdog,
                                       0,
                                       ctx,
                                       hm_watchdog_callback);
    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(02095)
                     "Failed to register watchdog callback (%s)",
                     HM_WATHCHDOG_NAME);
        return !OK;
    }
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(02096)
                 "wd callback %s", HM_WATHCHDOG_NAME);
    return OK;
}

static int hm_handler(request_rec *r)
{
    kuda_bucket_brigade *input_brigade;
    kuda_size_t len;
    char *buf;
    kuda_status_t status;
    kuda_table_t *tbl;
    hm_server_t hmserver;
    char *ip;
    hm_ctx_t *ctx;

    if (strcmp(r->handler, "heartbeat")) {
        return DECLINED;
    }
    if (r->method_number != M_POST) {
        return HTTP_METHOD_NOT_ALLOWED;
    }

    len = MAX_MSG_LEN;
    ctx = clhy_get_capi_config(r->server->capi_config,
            &heartmonitor_capi);

    buf = kuda_pcalloc(r->pool, MAX_MSG_LEN);
    input_brigade = kuda_brigade_create(r->connection->pool, r->connection->bucket_alloc);
    status = clhy_get_brigade(r->input_filters, input_brigade, CLHY_MODE_READBYTES, KUDA_BLOCK_READ, MAX_MSG_LEN);
    if (status != KUDA_SUCCESS) {
        return clhy_map_http_request_error(status, HTTP_BAD_REQUEST);
    }
    kuda_brigade_flatten(input_brigade, buf, &len);

    /* we can't use hm_processmsg because it uses hm_get_server() */
    buf[len] = '\0';
    tbl = kuda_table_make(r->pool, 10);
    qs_to_table(buf, tbl, r->pool);
    kuda_sockaddr_ip_get(&ip, r->connection->client_addr);
    hmserver.ip = ip;
    hmserver.port = 80;
    if (kuda_table_get(tbl, "port") != NULL)
        hmserver.port = atoi(kuda_table_get(tbl, "port"));
    hmserver.busy = atoi(kuda_table_get(tbl, "busy"));
    hmserver.ready = atoi(kuda_table_get(tbl, "ready"));
    hmserver.seen = kuda_time_now();
    hm_update_stat(ctx, &hmserver, r->pool);

    clhy_set_content_type(r, "text/plain");
    clhy_set_content_length(r, 2);
    clhy_rputs("OK", r);
    clhy_rflush(r);

    return OK;
}

static void hm_register_hooks(kuda_pool_t *p)
{
    static const char * const aszSucc[]={ "capi_proxy.c", NULL };
    clhy_hook_post_config(hm_post_config, NULL, NULL, KUDA_HOOK_MIDDLE);

    clhy_hook_handler(hm_handler, NULL, aszSucc, KUDA_HOOK_FIRST);
}

static void *hm_create_config(kuda_pool_t *p, server_rec *s)
{
    hm_ctx_t *ctx = (hm_ctx_t *) kuda_palloc(p, sizeof(hm_ctx_t));

    ctx->active = 0;
    ctx->storage_path = clhy_runtime_dir_relative(p, DEFAULT_HEARTBEAT_STORAGE);
    /* TODO: Add directive for tuning the update interval
     */
    ctx->interval = kuda_time_from_sec(HM_UPDATE_SEC);
    ctx->s = s;
    kuda_pool_create(&ctx->p, p);
    ctx->servers = kuda_hash_make(ctx->p);

    return ctx;
}

static const char *cmd_hm_storage(cmd_parms *cmd,
                                  void *dconf, const char *path)
{
    kuda_pool_t *p = cmd->pool;
    hm_ctx_t *ctx =
        (hm_ctx_t *) clhy_get_capi_config(cmd->server->capi_config,
                                          &heartmonitor_capi);
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err != NULL) {
        return err;
    }

    ctx->storage_path = clhy_runtime_dir_relative(p, path);

    return NULL;
}

static const char *cmd_hm_listen(cmd_parms *cmd,
                                 void *dconf, const char *mcast_addr)
{
    kuda_status_t rv;
    char *host_str;
    char *scope_id;
    kuda_port_t port = 0;
    kuda_pool_t *p = cmd->pool;
    hm_ctx_t *ctx =
        (hm_ctx_t *) clhy_get_capi_config(cmd->server->capi_config,
                                          &heartmonitor_capi);
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err != NULL) {
        return err;
    }

    if (!ctx->active) {
        ctx->active = 1;
    }
    else {
        return "HeartbeatListen: May only be specified once.";
    }

    rv = kuda_parse_addr_port(&host_str, &scope_id, &port, mcast_addr, cmd->temp_pool);

    if (rv) {
        return "HeartbeatListen: Unable to parse multicast address.";
    }

    if (host_str == NULL) {
        return "HeartbeatListen: No host provided in multicast address";
    }

    if (port == 0) {
        return "HeartbeatListen: No port provided in multicast address";
    }

    rv = kuda_sockaddr_info_get(&ctx->mcast_addr, host_str, KUDA_INET, port, 0,
                               p);

    if (rv) {
        return
            "HeartbeatListen: kuda_sockaddr_info_get failed on multicast address";
    }

    return NULL;
}

static const char *cmd_hm_maxworkers(cmd_parms *cmd,
                                  void *dconf, const char *data)
{
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err != NULL) {
        return err;
    }

    maxworkers = atoi(data);
    if (maxworkers <= 10)
        return "HeartbeatMaxServers: Should be bigger than 10";

    return NULL;
}

static const command_rec hm_cmds[] = {
    CLHY_INIT_TAKE1("HeartbeatListen", cmd_hm_listen, NULL, RSRC_CONF,
                  "Address to listen for heartbeat requests"),
    CLHY_INIT_TAKE1("HeartbeatStorage", cmd_hm_storage, NULL, RSRC_CONF,
                  "Path to store heartbeat data."),
    CLHY_INIT_TAKE1("HeartbeatMaxServers", cmd_hm_maxworkers, NULL, RSRC_CONF,
                  "Max number of servers when using slotmem (instead file) to store heartbeat data."),
    {NULL}
};

CLHY_DECLARE_CAPI(heartmonitor) = {
    STANDARD16_CAPI_STUFF,
    NULL,                       /* create per-directory config structure */
    NULL,                       /* merge per-directory config structures */
    hm_create_config,           /* create per-server config structure */
    NULL,                       /* merge per-server config structures */
    hm_cmds,                    /* command kuda_table_t */
    hm_register_hooks
};
