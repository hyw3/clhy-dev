/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "kuda_strings.h"

#include "clhy_core.h"
#include "scoreboard.h"
#include "capi_watchdog.h"

#ifndef HEARTBEAT_INTERVAL
#define HEARTBEAT_INTERVAL (1)
#endif

cAPI CLHY_CAPI_DECLARE_DATA heartbeat_capi;

typedef struct hb_ctx_t
{
    int active;
    kuda_sockaddr_t *mcast_addr;
    int server_limit;
    int thread_limit;
    kuda_status_t status;
} hb_ctx_t;

static const char *msg_format = "v=%u&ready=%u&busy=%u";

#define MSG_VERSION (1)

static int hb_monitor(hb_ctx_t *ctx, kuda_pool_t *p)
{
    kuda_size_t len;
    kuda_socket_t *sock = NULL;
    char buf[256];
    int i, j;
    kuda_uint32_t ready = 0;
    kuda_uint32_t busy = 0;
    clhy_generation_t clmp_generation;

    clhy_clmp_query(CLHY_CLMPQ_GENERATION, &clmp_generation);

    for (i = 0; i < ctx->server_limit; i++) {
        process_score *ps;
        ps = clhy_get_scoreboard_process(i);

        for (j = 0; j < ctx->thread_limit; j++) {
            int res;

            worker_score *ws = NULL;

            ws = &clhy_scoreboard_image->servers[i][j];

            res = ws->status;

            if (res == SERVER_READY && ps->generation == clmp_generation) {
                ready++;
            }
            else if (res != SERVER_DEAD &&
                     res != SERVER_STARTING && res != SERVER_IDLE_KILL &&
                     ps->generation == clmp_generation) {
                busy++;
            }
        }
    }

    len = kuda_snprintf(buf, sizeof(buf), msg_format, MSG_VERSION, ready, busy);

    do {
        kuda_status_t rv;
        rv = kuda_socket_create(&sock, ctx->mcast_addr->family,
                               SOCK_DGRAM, KUDA_PROTO_UDP, p);
        if (rv) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rv,
                         NULL, CLHYLOGNO(02097) "Heartbeat: kuda_socket_create failed");
            break;
        }

        rv = kuda_mcast_loopback(sock, 1);
        if (rv) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rv,
                         NULL, CLHYLOGNO(02098) "Heartbeat: kuda_mcast_loopback failed");
            break;
        }

        rv = kuda_socket_sendto(sock, ctx->mcast_addr, 0, buf, &len);
        if (rv) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rv,
                         NULL, CLHYLOGNO(02099) "Heartbeat: kuda_socket_sendto failed");
            break;
        }
    } while (0);

    if (sock) {
        kuda_socket_close(sock);
    }

    return OK;
}

static int hb_watchdog_init(server_rec *s, const char *name, kuda_pool_t *pool)
{
    hb_ctx_t *ctx = clhy_get_capi_config(s->capi_config, &heartbeat_capi);

    clhy_clmp_query(CLHY_CLMPQ_HARD_LIMIT_THREADS, &ctx->thread_limit);
    clhy_clmp_query(CLHY_CLMPQ_HARD_LIMIT_DAEMONS, &ctx->server_limit);

    return OK;
}

static int hb_watchdog_exit(server_rec *s, const char *name, kuda_pool_t *pool)
{
    return OK;
}

static int hb_watchdog_step(server_rec *s, const char *name, kuda_pool_t *pool)
{
    hb_ctx_t *ctx = clhy_get_capi_config(s->capi_config, &heartbeat_capi);

    if (!ctx->active || strcmp(name, CLHY_WATCHDOG_SINGLETON)) {
        return OK;
    }
    return hb_monitor(ctx, pool);
}

static int hb_watchdog_need(server_rec *s, const char *name,
                          int parent, int singleton)
{
    hb_ctx_t *ctx = clhy_get_capi_config(s->capi_config, &heartbeat_capi);

    if (ctx->active && singleton && !strcmp(name, CLHY_WATCHDOG_SINGLETON))
        return OK;
    else
        return DECLINED;
}

static void hb_register_hooks(kuda_pool_t *p)
{
    clhy_hook_watchdog_need(hb_watchdog_need, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_watchdog_init(hb_watchdog_init, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_watchdog_step(hb_watchdog_step, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_watchdog_exit(hb_watchdog_exit, NULL, NULL, KUDA_HOOK_MIDDLE);
}

static void *hb_create_config(kuda_pool_t *p, server_rec *s)
{
    hb_ctx_t *cfg = (hb_ctx_t *) kuda_pcalloc(p, sizeof(hb_ctx_t));

    return cfg;
}

static const char *cmd_hb_address(cmd_parms *cmd,
                                  void *dconf, const char *addr)
{
    kuda_status_t rv;
    char *host_str;
    char *scope_id;
    kuda_port_t port = 0;
    kuda_pool_t *p = cmd->pool;
    hb_ctx_t *ctx =
        (hb_ctx_t *) clhy_get_capi_config(cmd->server->capi_config,
                                          &heartbeat_capi);
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err != NULL) {
        return err;
    }

    if (!ctx->active) {
        ctx->active = 1;
    }
    else {
        return "HeartbeatAddress: May only be specified once.";
    }

    rv = kuda_parse_addr_port(&host_str, &scope_id, &port, addr, cmd->temp_pool);

    if (rv) {
        return "HeartbeatAddress: Unable to parse address.";
    }

    if (host_str == NULL) {
        return "HeartbeatAddress: No host provided in address";
    }

    if (port == 0) {
        return "HeartbeatAddress: No port provided in address";
    }

    rv = kuda_sockaddr_info_get(&ctx->mcast_addr, host_str, KUDA_INET, port, 0,
                               p);

    if (rv) {
        return "HeartbeatAddress: kuda_sockaddr_info_get failed.";
    }

    return NULL;
}

static const command_rec hb_cmds[] = {
    CLHY_INIT_TAKE1("HeartbeatAddress", cmd_hb_address, NULL, RSRC_CONF,
                  "Address to send heartbeat requests"),
    {NULL}
};

CLHY_DECLARE_CAPI(heartbeat) = {
    STANDARD16_CAPI_STUFF,
    NULL,                       /* create per-directory config structure */
    NULL,                       /* merge per-directory config structures */
    hb_create_config,           /* create per-server config structure */
    NULL,                       /* merge per-server config structures */
    hb_cmds,                    /* command kuda_table_t */
    hb_register_hooks
};
