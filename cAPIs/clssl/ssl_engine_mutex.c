/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * capi_ssl
 * cLHy API to SSL
 */

#include "ssl_private.h"

int ssl_mutex_init(server_rec *s, kuda_pool_t *p)
{
    SSLcAPIConfigRec *mc = mycAPIConfig(s);
    kuda_status_t rv;

    /* A mutex is only needed if a session cache is configured, and
     * the provider used is not internally multi-process/thread
     * safe. */
    if (!mc->sesscache
        || (mc->sesscache->flags & CLHY_SOCACHE_FLAG_NOTMPSAFE) == 0) {
        return TRUE;
    }

    if (mc->pMutex) {
        return TRUE;
    }

    if ((rv = clhy_global_mutex_create(&mc->pMutex, NULL, SSL_CACHE_MUTEX_TYPE,
                                     NULL, s, s->process->pool, 0))
            != KUDA_SUCCESS) {
        return FALSE;
    }

    return TRUE;
}

int ssl_mutex_reinit(server_rec *s, kuda_pool_t *p)
{
    SSLcAPIConfigRec *mc = mycAPIConfig(s);
    kuda_status_t rv;
    const char *lockfile;

    if (mc->pMutex == NULL || !mc->sesscache
        || (mc->sesscache->flags & CLHY_SOCACHE_FLAG_NOTMPSAFE) == 0) {
        return TRUE;
    }

    lockfile = kuda_global_mutex_lockfile(mc->pMutex);
    if ((rv = kuda_global_mutex_child_init(&mc->pMutex,
                                          lockfile,
                                          p)) != KUDA_SUCCESS) {
        if (lockfile)
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(02024)
                         "Cannot reinit %s mutex with file `%s'",
                         SSL_CACHE_MUTEX_TYPE, lockfile);
        else
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rv, s, CLHYLOGNO(02025)
                         "Cannot reinit %s mutex", SSL_CACHE_MUTEX_TYPE);
        return FALSE;
    }
    return TRUE;
}

int ssl_mutex_on(server_rec *s)
{
    SSLcAPIConfigRec *mc = mycAPIConfig(s);
    kuda_status_t rv;

    if ((rv = kuda_global_mutex_lock(mc->pMutex)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rv, s, CLHYLOGNO(02026)
                     "Failed to acquire SSL session cache lock");
        return FALSE;
    }
    return TRUE;
}

int ssl_mutex_off(server_rec *s)
{
    SSLcAPIConfigRec *mc = mycAPIConfig(s);
    kuda_status_t rv;

    if ((rv = kuda_global_mutex_unlock(mc->pMutex)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rv, s, CLHYLOGNO(02027)
                     "Failed to release SSL session cache lock");
        return FALSE;
    }
    return TRUE;
}

