dnl The cLHy Server
dnl 
dnl Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
dnl The HLF licenses this file under the GNU GPL version 3 or later
dnl (the "License"); you may not use this file except in compliance with
dnl the License.  You may obtain a copy of the License at
dnl
dnl      http://clhy.hyang.org/license.hyss
dnl
dnl Unless required by applicable law or agreed to in writing, software
dnl distributed under the License is distributed on an "AS IS" BASIS,
dnl WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
dnl See the License for the specific language governing permissions and
dnl limitations under the License.

dnl #  start of cAPI specific part
CLHYKUDEL_CAPIPATH_INIT(clssl)

dnl #  list of cAPI object files
ssl_objs="dnl
capi_ssl.lo dnl
ssl_engine_config.lo dnl
ssl_engine_init.lo dnl
ssl_engine_io.lo dnl
ssl_engine_kernel.lo dnl
ssl_engine_log.lo dnl
ssl_engine_mutex.lo dnl
ssl_engine_pphrase.lo dnl
ssl_engine_rand.lo dnl
ssl_engine_vars.lo dnl
ssl_scache.lo dnl
ssl_util_stapling.lo dnl
ssl_util.lo dnl
ssl_util_ssl.lo dnl
ssl_engine_ocsp.lo dnl
ssl_util_ocsp.lo dnl
"
dnl #  hook cAPI into the Autoconf mechanism (--enable-ssl option)
CLHYKUDEL_CAPI(ssl, [SSL/TLS support (capi_ssl)], $ssl_objs, , most, [
    CLHYKUDEL_CHECK_OPENSSL
    if test "$ac_cv_openssl" = "yes" ; then
        if test "x$enable_ssl" = "xshared"; then
           # The only symbol which needs to be exported is the cAPI
           # structure, so ask libtool to hide everything else:
           KUDA_ADDTO(CAPI_SSL_LDADD, [-export-symbols-regex ssl_capi])
        fi
    else
        enable_ssl=no
    fi
])

# Ensure that other cAPIs can pick up capi_ssl.h
KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

dnl #  end of cAPI specific part
CLHYKUDEL_CAPIPATH_FINISH

