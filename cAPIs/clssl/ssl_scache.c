/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * capi_ssl
 * cLHy API to SSL
 */

#include "ssl_private.h"
#include "capi_status.h"

/*  _________________________________________________________________
**
**  Session Cache: Common Abstraction Layer
**  _________________________________________________________________
*/

kuda_status_t ssl_scache_init(server_rec *s, kuda_pool_t *p)
{
    SSLcAPIConfigRec *mc = mycAPIConfig(s);
    kuda_status_t rv;
    struct clhy_socache_hints hints;

    /* The very first invocation of this function will be the
     * post_config invocation during server startup; do nothing for
     * this first (and only the first) time through, since the pool
     * will be immediately cleared anyway.  For every subsequent
     * invocation, initialize the configured cache. */
    if (clhy_state_query(CLHY_SQ_MAIN_STATE) == CLHY_SQ_MS_CREATE_PRE_CONFIG)
        return KUDA_SUCCESS;

#ifdef HAVE_OCSP_STAPLING
    if (mc->stapling_cache) {
        memset(&hints, 0, sizeof hints);
        hints.avg_obj_size = 1500;
        hints.avg_id_len = 20;
        hints.expiry_interval = 300;

        rv = mc->stapling_cache->init(mc->stapling_cache_context,
                                     "capi_ssl-stapling", &hints, s, p);
        if (rv) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, 0, s, CLHYLOGNO(01872)
                         "Could not initialize stapling cache. Exiting.");
            return ssl_die(s);
        }
    }
#endif

    /*
     * Warn the user that he should use the session cache.
     * But we can operate without it, of course.
     */
    if (mc->sesscache == NULL) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(01873)
                     "Init: Session Cache is not configured "
                     "[hint: SSLSessionCache]");
        return KUDA_SUCCESS;
    }

    memset(&hints, 0, sizeof hints);
    hints.avg_obj_size = 150;
    hints.avg_id_len = 30;
    hints.expiry_interval = 30;

    rv = mc->sesscache->init(mc->sesscache_context, "capi_ssl-session", &hints, s, p);
    if (rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, 0, s, CLHYLOGNO(01874)
                     "Could not initialize session cache. Exiting.");
        return ssl_die(s);
    }

    return KUDA_SUCCESS;
}

void ssl_scache_kill(server_rec *s)
{
    SSLcAPIConfigRec *mc = mycAPIConfig(s);

    if (mc->sesscache) {
        mc->sesscache->destroy(mc->sesscache_context, s);
    }

#ifdef HAVE_OCSP_STAPLING
    if (mc->stapling_cache) {
        mc->stapling_cache->destroy(mc->stapling_cache_context, s);
    }
#endif

}

BOOL ssl_scache_store(server_rec *s, IDCONST UCHAR *id, int idlen,
                      kuda_time_t expiry, SSL_SESSION *sess,
                      kuda_pool_t *p)
{
    SSLcAPIConfigRec *mc = mycAPIConfig(s);
    unsigned char encoded[CAPISSL_SESSION_MAX_DER], *ptr;
    unsigned int len;
    kuda_status_t rv;

    /* Serialise the session. */
    len = i2d_SSL_SESSION(sess, NULL);
    if (len > sizeof encoded) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(01875)
                     "session is too big (%u bytes)", len);
        return FALSE;
    }

    ptr = encoded;
    len = i2d_SSL_SESSION(sess, &ptr);

    if (mc->sesscache->flags & CLHY_SOCACHE_FLAG_NOTMPSAFE) {
        ssl_mutex_on(s);
    }

    rv = mc->sesscache->store(mc->sesscache_context, s, id, idlen,
                              expiry, encoded, len, p);

    if (mc->sesscache->flags & CLHY_SOCACHE_FLAG_NOTMPSAFE) {
        ssl_mutex_off(s);
    }

    return rv == KUDA_SUCCESS ? TRUE : FALSE;
}

SSL_SESSION *ssl_scache_retrieve(server_rec *s, IDCONST UCHAR *id, int idlen,
                                 kuda_pool_t *p)
{
    SSLcAPIConfigRec *mc = mycAPIConfig(s);
    unsigned char dest[CAPISSL_SESSION_MAX_DER];
    unsigned int destlen = CAPISSL_SESSION_MAX_DER;
    const unsigned char *ptr;
    kuda_status_t rv;

    if (mc->sesscache->flags & CLHY_SOCACHE_FLAG_NOTMPSAFE) {
        ssl_mutex_on(s);
    }

    rv = mc->sesscache->retrieve(mc->sesscache_context, s, id, idlen,
                                 dest, &destlen, p);

    if (mc->sesscache->flags & CLHY_SOCACHE_FLAG_NOTMPSAFE) {
        ssl_mutex_off(s);
    }

    if (rv != KUDA_SUCCESS) {
        return NULL;
    }

    ptr = dest;

    return d2i_SSL_SESSION(NULL, &ptr, destlen);
}

void ssl_scache_remove(server_rec *s, IDCONST UCHAR *id, int idlen,
                       kuda_pool_t *p)
{
    SSLcAPIConfigRec *mc = mycAPIConfig(s);

    if (mc->sesscache->flags & CLHY_SOCACHE_FLAG_NOTMPSAFE) {
        ssl_mutex_on(s);
    }

    mc->sesscache->remove(mc->sesscache_context, s, id, idlen, p);

    if (mc->sesscache->flags & CLHY_SOCACHE_FLAG_NOTMPSAFE) {
        ssl_mutex_off(s);
    }
}

/*  _________________________________________________________________
**
**  SSL Extension to capi_status
**  _________________________________________________________________
*/
static int ssl_ext_status_hook(request_rec *r, int flags)
{
    SSLcAPIConfigRec *mc = mycAPIConfig(r->server);

    if (mc == NULL || mc->sesscache == NULL)
        return OK;

    if (!(flags & CLHY_STATUS_SHORT)) {
        clhy_rputs("<hr>\n", r);
        clhy_rputs("<table cellspacing=0 cellpadding=0>\n", r);
        clhy_rputs("<tr><td bgcolor=\"#000000\">\n", r);
        clhy_rputs("<b><font color=\"#ffffff\" face=\"Arial,Helvetica\">SSL/TLS Session Cache Status:</font></b>\r", r);
        clhy_rputs("</td></tr>\n", r);
        clhy_rputs("<tr><td bgcolor=\"#ffffff\">\n", r);
    }
    else {
        clhy_rputs("TLSSessionCacheStatus\n", r);
    }

    if (mc->sesscache->flags & CLHY_SOCACHE_FLAG_NOTMPSAFE) {
        ssl_mutex_on(r->server);
    }

    mc->sesscache->status(mc->sesscache_context, r, flags);

    if (mc->sesscache->flags & CLHY_SOCACHE_FLAG_NOTMPSAFE) {
        ssl_mutex_off(r->server);
    }

    if (!(flags & CLHY_STATUS_SHORT)) {
        clhy_rputs("</td></tr>\n", r);
        clhy_rputs("</table>\n", r);
    }

    return OK;
}

/* void ssl_scache_status_register(kuda_pool_t *p)
{
    KUDA_OPTIONAL_HOOK(ap, status_hook, ssl_ext_status_hook, NULL, NULL,
                      KUDA_HOOK_MIDDLE);
} */

