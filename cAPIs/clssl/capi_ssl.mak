# Microsoft Developer Studio Generated NMAKE File, Based on capi_ssl.dsp
!IF "$(CFG)" == ""
CFG=capi_ssl - Win32 Release
!MESSAGE No configuration specified. Defaulting to capi_ssl - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "capi_ssl - Win32 Release" && "$(CFG)" != "capi_ssl - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "capi_ssl.mak" CFG="capi_ssl - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "capi_ssl - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "capi_ssl - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "capi_ssl - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_ssl.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Release" "libkudadelman - Win32 Release" "libkuda - Win32 Release" "$(OUTDIR)\capi_ssl.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudadelman - Win32 ReleaseCLEAN" "libwwhy - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\capi_ssl.obj"
	-@erase "$(INTDIR)\capi_ssl.res"
	-@erase "$(INTDIR)\capi_ssl_src.idb"
	-@erase "$(INTDIR)\capi_ssl_src.pdb"
	-@erase "$(INTDIR)\ssl_engine_config.obj"
	-@erase "$(INTDIR)\ssl_engine_init.obj"
	-@erase "$(INTDIR)\ssl_engine_io.obj"
	-@erase "$(INTDIR)\ssl_engine_kernel.obj"
	-@erase "$(INTDIR)\ssl_engine_log.obj"
	-@erase "$(INTDIR)\ssl_engine_mutex.obj"
	-@erase "$(INTDIR)\ssl_engine_ocsp.obj"
	-@erase "$(INTDIR)\ssl_engine_pphrase.obj"
	-@erase "$(INTDIR)\ssl_engine_rand.obj"
	-@erase "$(INTDIR)\ssl_engine_vars.obj"
	-@erase "$(INTDIR)\ssl_scache.obj"
	-@erase "$(INTDIR)\ssl_util.obj"
	-@erase "$(INTDIR)\ssl_util_ocsp.obj"
	-@erase "$(INTDIR)\ssl_util_ssl.obj"
	-@erase "$(INTDIR)\ssl_util_stapling.obj"
	-@erase "$(OUTDIR)\capi_ssl.exp"
	-@erase "$(OUTDIR)\capi_ssl.lib"
	-@erase "$(OUTDIR)\capi_ssl.pdb"
	-@erase "$(OUTDIR)\capi_ssl.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../../include" /I "../generators" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/openssl/inc32" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "WIN32_LEAN_AND_MEAN" /D "NO_IDEA" /D "NO_RC5" /D "NO_MDC2" /D "OPENSSL_NO_IDEA" /D "OPENSSL_NO_RC5" /D "OPENSSL_NO_MDC2" /D "HAVE_OPENSSL" /D "HAVE_SSL_SET_STATE" /D "HAVE_OPENSSL_ENGINE_H" /D "HAVE_ENGINE_INIT" /D "HAVE_ENGINE_LOAD_BUILTIN_ENGINES" /D "SSL_DECLARE_EXPORT" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_ssl_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_ssl.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "NDEBUG" /d BIN_NAME="capi_ssl.so" /d LONG_NAME="proxy_ssl_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_ssl.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib wsock32.lib ws2_32.lib advapi32.lib gdi32.lib libeay32.lib ssleay32.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_ssl.pdb" /debug /out:"$(OUTDIR)\capi_ssl.so" /implib:"$(OUTDIR)\capi_ssl.lib" /libpath:"../../kudelrunsrc/openssl/out32dll" /libpath:"../../kudelrunsrc/openssl/out32" /base:@..\..\platforms\win32\BaseAddr.ref,capi_ssl.so /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\capi_ssl.obj" \
	"$(INTDIR)\ssl_engine_config.obj" \
	"$(INTDIR)\ssl_engine_init.obj" \
	"$(INTDIR)\ssl_engine_io.obj" \
	"$(INTDIR)\ssl_engine_kernel.obj" \
	"$(INTDIR)\ssl_engine_log.obj" \
	"$(INTDIR)\ssl_engine_mutex.obj" \
	"$(INTDIR)\ssl_engine_pphrase.obj" \
	"$(INTDIR)\ssl_engine_rand.obj" \
	"$(INTDIR)\ssl_engine_vars.obj" \
	"$(INTDIR)\ssl_engine_ocsp.obj" \
	"$(INTDIR)\ssl_util_ocsp.obj" \
	"$(INTDIR)\ssl_scache.obj" \
	"$(INTDIR)\ssl_util_stapling.obj" \
	"$(INTDIR)\ssl_util.obj" \
	"$(INTDIR)\ssl_util_ssl.obj" \
	"$(INTDIR)\capi_ssl.res" \
	"..\..\kudelrunsrc\kuda\Release\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Release\libkudadelman-1.lib" \
	"..\..\Release\libwwhy.lib"

"$(OUTDIR)\capi_ssl.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\capi_ssl.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_ssl.so"
   if exist .\Release\capi_ssl.so.manifest mt.exe -manifest .\Release\capi_ssl.so.manifest -outputresource:.\Release\capi_ssl.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "capi_ssl - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_ssl.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Debug" "libkudadelman - Win32 Debug" "libkuda - Win32 Debug" "$(OUTDIR)\capi_ssl.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudadelman - Win32 DebugCLEAN" "libwwhy - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\capi_ssl.obj"
	-@erase "$(INTDIR)\capi_ssl.res"
	-@erase "$(INTDIR)\capi_ssl_src.idb"
	-@erase "$(INTDIR)\capi_ssl_src.pdb"
	-@erase "$(INTDIR)\ssl_engine_config.obj"
	-@erase "$(INTDIR)\ssl_engine_init.obj"
	-@erase "$(INTDIR)\ssl_engine_io.obj"
	-@erase "$(INTDIR)\ssl_engine_kernel.obj"
	-@erase "$(INTDIR)\ssl_engine_log.obj"
	-@erase "$(INTDIR)\ssl_engine_mutex.obj"
	-@erase "$(INTDIR)\ssl_engine_ocsp.obj"
	-@erase "$(INTDIR)\ssl_engine_pphrase.obj"
	-@erase "$(INTDIR)\ssl_engine_rand.obj"
	-@erase "$(INTDIR)\ssl_engine_vars.obj"
	-@erase "$(INTDIR)\ssl_scache.obj"
	-@erase "$(INTDIR)\ssl_util.obj"
	-@erase "$(INTDIR)\ssl_util_ocsp.obj"
	-@erase "$(INTDIR)\ssl_util_ssl.obj"
	-@erase "$(INTDIR)\ssl_util_stapling.obj"
	-@erase "$(OUTDIR)\capi_ssl.exp"
	-@erase "$(OUTDIR)\capi_ssl.lib"
	-@erase "$(OUTDIR)\capi_ssl.pdb"
	-@erase "$(OUTDIR)\capi_ssl.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../../include" /I "../generators" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/openssl/inc32" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "WIN32_LEAN_AND_MEAN" /D "NO_IDEA" /D "NO_RC5" /D "NO_MDC2" /D "OPENSSL_NO_IDEA" /D "OPENSSL_NO_RC5" /D "OPENSSL_NO_MDC2" /D "HAVE_OPENSSL" /D "HAVE_SSL_SET_STATE" /D "HAVE_OPENSSL_ENGINE_H" /D "HAVE_ENGINE_INIT" /D "HAVE_ENGINE_LOAD_BUILTIN_ENGINES" /D "SSL_DECLARE_EXPORT" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_ssl_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_ssl.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "_DEBUG" /d BIN_NAME="capi_ssl.so" /d LONG_NAME="proxy_ssl_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_ssl.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib wsock32.lib ws2_32.lib advapi32.lib gdi32.lib libeay32.lib ssleay32.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_ssl.pdb" /debug /out:"$(OUTDIR)\capi_ssl.so" /implib:"$(OUTDIR)\capi_ssl.lib" /libpath:"../../kudelrunsrc/openssl/out32dll.dbg" /libpath:"../../kudelrunsrc/openssl/out32.dbg" /libpath:"../../kudelrunsrc/openssl/out32dll" /libpath:"../../kudelrunsrc/openssl/out32" /base:@..\..\platforms\win32\BaseAddr.ref,capi_ssl.so 
LINK32_OBJS= \
	"$(INTDIR)\capi_ssl.obj" \
	"$(INTDIR)\ssl_engine_config.obj" \
	"$(INTDIR)\ssl_engine_init.obj" \
	"$(INTDIR)\ssl_engine_io.obj" \
	"$(INTDIR)\ssl_engine_kernel.obj" \
	"$(INTDIR)\ssl_engine_log.obj" \
	"$(INTDIR)\ssl_engine_mutex.obj" \
	"$(INTDIR)\ssl_engine_pphrase.obj" \
	"$(INTDIR)\ssl_engine_rand.obj" \
	"$(INTDIR)\ssl_engine_vars.obj" \
	"$(INTDIR)\ssl_engine_ocsp.obj" \
	"$(INTDIR)\ssl_util_ocsp.obj" \
	"$(INTDIR)\ssl_scache.obj" \
	"$(INTDIR)\ssl_util_stapling.obj" \
	"$(INTDIR)\ssl_util.obj" \
	"$(INTDIR)\ssl_util_ssl.obj" \
	"$(INTDIR)\capi_ssl.res" \
	"..\..\kudelrunsrc\kuda\Debug\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Debug\libkudadelman-1.lib" \
	"..\..\Debug\libwwhy.lib"

"$(OUTDIR)\capi_ssl.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\capi_ssl.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_ssl.so"
   if exist .\Debug\capi_ssl.so.manifest mt.exe -manifest .\Debug\capi_ssl.so.manifest -outputresource:.\Debug\capi_ssl.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("capi_ssl.dep")
!INCLUDE "capi_ssl.dep"
!ELSE 
!MESSAGE Warning: cannot find "capi_ssl.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "capi_ssl - Win32 Release" || "$(CFG)" == "capi_ssl - Win32 Debug"
SOURCE=.\capi_ssl.c

"$(INTDIR)\capi_ssl.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_engine_config.c

"$(INTDIR)\ssl_engine_config.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_engine_init.c

"$(INTDIR)\ssl_engine_init.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_engine_io.c

"$(INTDIR)\ssl_engine_io.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_engine_kernel.c

"$(INTDIR)\ssl_engine_kernel.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_engine_log.c

"$(INTDIR)\ssl_engine_log.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_engine_mutex.c

"$(INTDIR)\ssl_engine_mutex.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_engine_ocsp.c

"$(INTDIR)\ssl_engine_ocsp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_engine_pphrase.c

"$(INTDIR)\ssl_engine_pphrase.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_engine_rand.c

"$(INTDIR)\ssl_engine_rand.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_engine_vars.c

"$(INTDIR)\ssl_engine_vars.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_scache.c

"$(INTDIR)\ssl_scache.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_util.c

"$(INTDIR)\ssl_util.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_util_ocsp.c

"$(INTDIR)\ssl_util_ocsp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_util_ssl.c

"$(INTDIR)\ssl_util_ssl.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ssl_util_stapling.c

"$(INTDIR)\ssl_util_stapling.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "capi_ssl - Win32 Release"

"libkuda - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\..\cAPIs\clssl"

"libkuda - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clssl"

!ELSEIF  "$(CFG)" == "capi_ssl - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\..\cAPIs\clssl"

"libkuda - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clssl"

!ENDIF 

!IF  "$(CFG)" == "capi_ssl - Win32 Release"

"libkudadelman - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" 
   cd "..\..\cAPIs\clssl"

"libkudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clssl"

!ELSEIF  "$(CFG)" == "capi_ssl - Win32 Debug"

"libkudadelman - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" 
   cd "..\..\cAPIs\clssl"

"libkudadelman - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clssl"

!ENDIF 

!IF  "$(CFG)" == "capi_ssl - Win32 Release"

"libwwhy - Win32 Release" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" 
   cd ".\cAPIs\clssl"

"libwwhy - Win32 ReleaseCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" RECURSE=1 CLEAN 
   cd ".\cAPIs\clssl"

!ELSEIF  "$(CFG)" == "capi_ssl - Win32 Debug"

"libwwhy - Win32 Debug" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" 
   cd ".\cAPIs\clssl"

"libwwhy - Win32 DebugCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" RECURSE=1 CLEAN 
   cd ".\cAPIs\clssl"

!ENDIF 

SOURCE=..\..\build\win32\wwhy.rc

!IF  "$(CFG)" == "capi_ssl - Win32 Release"


"$(INTDIR)\capi_ssl.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_ssl.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../build\win32" /d "NDEBUG" /d BIN_NAME="capi_ssl.so" /d LONG_NAME="proxy_ssl_capi for cLHy" $(SOURCE)


!ELSEIF  "$(CFG)" == "capi_ssl - Win32 Debug"


"$(INTDIR)\capi_ssl.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_ssl.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../build\win32" /d "_DEBUG" /d BIN_NAME="capi_ssl.so" /d LONG_NAME="proxy_ssl_capi for cLHy" $(SOURCE)


!ENDIF 


!ENDIF 

