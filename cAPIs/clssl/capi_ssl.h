/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file capi_ssl.h
 * @brief cLHy API to SSL
 *
 * @defgroup CAPI_SSL capi_ssl
 * @ingroup  CLHYKUDEL_CAPIS
 * @{
 */

#ifndef __CAPI_SSL_H__
#define __CAPI_SSL_H__

#include "wwhy.h"
#include "http_config.h"
#include "kuda_optional.h"
#include "kuda_tables.h" /* for kuda_array_header_t */

/* Tags for platform-specific in the SSL_DECLARE(type), SSL_DECLARE_NONSTD(type)
 * and SSL_DECLARE_DATA
 */
#if !defined(WIN32)
#define SSL_DECLARE(type)            type
#define SSL_DECLARE_NONSTD(type)     type
#define SSL_DECLARE_DATA
#elif defined(SSL_DECLARE_STATIC)
#define SSL_DECLARE(type)            type __stdcall
#define SSL_DECLARE_NONSTD(type)     type
#define SSL_DECLARE_DATA
#elif defined(SSL_DECLARE_EXPORT)
#define SSL_DECLARE(type)            __declspec(dllexport) type __stdcall
#define SSL_DECLARE_NONSTD(type)     __declspec(dllexport) type
#define SSL_DECLARE_DATA             __declspec(dllexport)
#else
#define SSL_DECLARE(type)            __declspec(dllimport) type __stdcall
#define SSL_DECLARE_NONSTD(type)     __declspec(dllimport) type
#define SSL_DECLARE_DATA             __declspec(dllimport)
#endif

/** The ssl_var_lookup() optional function retrieves SSL environment
 * variables. */
KUDA_DECLARE_OPTIONAL_FN(char *, ssl_var_lookup,
                        (kuda_pool_t *, server_rec *,
                         conn_rec *, request_rec *,
                         char *));

/** The ssl_ext_list() optional function attempts to build an array
 * of all the values contained in the named X.509 extension. The
 * returned array will be created in the supplied pool.
 * The client certificate is used if peer is non-zero; the server
 * certificate is used otherwise.
 * Extension specifies the extensions to use as a string. This can be
 * one of the "known" long or short names, or a numeric OID,
 * e.g. "1.2.3.4", 'nsComment' and 'DN' are all valid.
 * A pointer to an kuda_array_header_t structure is returned if at
 * least one matching extension is found, NULL otherwise.
 */
KUDA_DECLARE_OPTIONAL_FN(kuda_array_header_t *, ssl_ext_list,
                        (kuda_pool_t *p, conn_rec *c, int peer,
                         const char *extension));

/** An optional function which returns non-zero if the given connection
 * is using SSL/TLS. */
KUDA_DECLARE_OPTIONAL_FN(int, ssl_is_https, (conn_rec *));

/** The ssl_proxy_enable() and ssl_engine_{set,disable}() optional
 * functions are used by capi_proxy to enable use of SSL for outgoing
 * connections. */

KUDA_DECLARE_OPTIONAL_FN(int, ssl_proxy_enable, (conn_rec *));
KUDA_DECLARE_OPTIONAL_FN(int, ssl_engine_disable, (conn_rec *));
KUDA_DECLARE_OPTIONAL_FN(int, ssl_engine_set, (conn_rec *,
                                              clhy_conf_vector_t *,
                                              int proxy, int enable));
                                              
/* Check for availability of new hooks */
#define SSL_CERT_HOOKS
#ifdef SSL_CERT_HOOKS

/** Lets others add certificate and key files to the given server.
 * For each cert a key must also be added.
 * @param cert_file and array of const char* with the path to the certificate chain
 * @param key_file and array of const char* with the path to the private key file
 */
KUDA_DECLARE_EXTERNAL_HOOK(ssl, SSL, int, add_cert_files,
                          (server_rec *s, kuda_pool_t *p, 
                           kuda_array_header_t *cert_files,
                           kuda_array_header_t *key_files))

/** In case no certificates are available for a server, this
 * lets other cAPIs add a fallback certificate for the time
 * being. Regular requests against this server will be answered
 * with a 503. 
 * @param cert_file and array of const char* with the path to the certificate chain
 * @param key_file and array of const char* with the path to the private key file
 */
KUDA_DECLARE_EXTERNAL_HOOK(ssl, SSL, int, add_fallback_cert_files,
                          (server_rec *s, kuda_pool_t *p, 
                           kuda_array_header_t *cert_files,
                           kuda_array_header_t *key_files))

#endif /* SSL_CERT_HOOKS */

#endif /* __CAPI_SSL_H__ */
/** @} */
