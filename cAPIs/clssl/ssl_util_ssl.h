/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @verbatim
   
   capi_ssl
   cLHy API to SSL
   
   @endverbatim
 * @file  ssl_util_ssl.h
 * @brief Additional Utility Functions for OpenSSL
 *
 * @defgroup CAPI_SSL_UTIL Utilities
 * @ingroup CAPI_SSL
 * @{
 */

#ifndef __SSL_UTIL_SSL_H__
#define __SSL_UTIL_SSL_H__

/**
 * SSL library version number
 */

#define CAPISSL_LIBRARY_VERSION OPENSSL_VERSION_NUMBER
#define CAPISSL_LIBRARY_NAME    "OpenSSL"
#define CAPISSL_LIBRARY_TEXT    OPENSSL_VERSION_TEXT
#if CAPISSL_USE_OPENSSL_PRE_1_1_API
#define CAPISSL_LIBRARY_DYNTEXT SSLeay_version(SSLEAY_VERSION)
#else
#define CAPISSL_LIBRARY_DYNTEXT OpenSSL_version(OPENSSL_VERSION)
#endif

/**
 *  Maximum length of a DER encoded session.
 *  FIXME: There is no define in OpenSSL, but OpenSSL uses 1024*10,
 *         so this value should be ok. Although we have no warm feeling.
 */
#define CAPISSL_SESSION_MAX_DER 1024*10

/** max length for capissl_SSL_SESSION_id2sz */
#define CAPISSL_SESSION_ID_STRING_LEN \
    ((SSL_MAX_SSL_SESSION_ID_LENGTH + 1) * 2)

/**
 *  Additional Functions
 */
void        capissl_init_app_data2_idx(void);
void       *capissl_get_app_data2(SSL *);
void        capissl_set_app_data2(SSL *, void *);
EVP_PKEY   *capissl_read_privatekey(const char *, EVP_PKEY **, pem_password_cb *, void *);
EVP_PKEY   *capissl_read_encrypted_pkey(const char *, EVP_PKEY **, const char *, kuda_size_t);
int         capissl_smart_shutdown(SSL *ssl);
BOOL        capissl_X509_getBC(X509 *, int *, int *);
char       *capissl_X509_NAME_ENTRY_to_string(kuda_pool_t *p, X509_NAME_ENTRY *xsne,
                                             int raw);
char       *capissl_X509_NAME_to_string(kuda_pool_t *, X509_NAME *, int);
BOOL        capissl_X509_getSAN(kuda_pool_t *, X509 *, int, const char *, int, kuda_array_header_t **);
BOOL        capissl_X509_match_name(kuda_pool_t *, X509 *, const char *, BOOL, server_rec *);
char       *capissl_SSL_SESSION_id2sz(IDCONST unsigned char *, int, char *, int);

#endif /* __SSL_UTIL_SSL_H__ */
/** @} */

