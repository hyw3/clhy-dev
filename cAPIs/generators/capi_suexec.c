/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_request.h"
#include "kuda_strings.h"
#include "unixd.h"
#include "core_common.h"
#include "capi_suexec.h"

cAPI CLHY_CAPI_DECLARE_DATA suexec_capi;

/*
 * Create a configuration specific to this cAPI for a server or directory
 * location, and fill it with the default settings.
 */
static void *mkconfig(kuda_pool_t *p)
{
    suexec_config_t *cfg = kuda_palloc(p, sizeof(suexec_config_t));

    cfg->active = 0;
    return cfg;
}

/*
 * Respond to a callback to create configuration record for a server or
 * vhost environment.
 */
static void *create_mconfig_for_server(kuda_pool_t *p, server_rec *s)
{
    return mkconfig(p);
}

/*
 * Respond to a callback to create a config record for a specific directory.
 */
static void *create_mconfig_for_directory(kuda_pool_t *p, char *dir)
{
    return mkconfig(p);
}

static const char *set_suexec_ugid(cmd_parms *cmd, void *mconfig,
                                   const char *uid, const char *gid)
{
    suexec_config_t *cfg = (suexec_config_t *) mconfig;
    const char *err = clhy_check_cmd_context(cmd, NOT_IN_DIR_LOC_FILE);

    if (err != NULL) {
        return err;
    }

    if (!clhy_unixd_config.suexec_enabled) {
        return kuda_pstrcat(cmd->pool, "SuexecUserGroup configured, but "
                           "suEXEC is disabled: ",
                           clhy_unixd_config.suexec_disabled_reason, NULL);
    }

    cfg->ugid.uid = clhy_uname2id(uid);
    cfg->ugid.gid = clhy_gname2id(gid);
    cfg->ugid.userdir = 0;
    cfg->active = 1;

    return NULL;
}

static clhy_unix_identity_t *get_suexec_id_doer(const request_rec *r)
{
    suexec_config_t *cfg =
    (suexec_config_t *) clhy_get_capi_config(r->per_dir_config, &suexec_capi);

    return cfg->active ? &cfg->ugid : NULL;
}

#define SUEXEC_POST_CONFIG_USERDATA "suexec_post_config_userdata"
static int suexec_post_config(kuda_pool_t *p, kuda_pool_t *plog,
                              kuda_pool_t *ptemp, server_rec *s)
{
    void *reported;

    kuda_pool_userdata_get(&reported, SUEXEC_POST_CONFIG_USERDATA,
                          s->process->pool);

    if ((reported == NULL) && clhy_unixd_config.suexec_enabled) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, s, CLHYLOGNO(01232)
                     "suEXEC mechanism enabled (wrapper: %s)", SUEXEC_BIN);

        kuda_pool_userdata_set((void *)1, SUEXEC_POST_CONFIG_USERDATA,
                              kuda_pool_cleanup_null, s->process->pool);
    }

    return OK;
}
#undef SUEXEC_POST_CONFIG_USERDATA

/*
 * Define the directives specific to this cAPI.  This structure is referenced
 * later by the 'cAPI' structure.
 */
static const command_rec suexec_cmds[] =
{
    /* XXX - Another important reason not to allow this in .htaccess is that
     * the clhy_[ug]name2id() is not thread-safe */
    CLHY_INIT_TAKE2("SuexecUserGroup", set_suexec_ugid, NULL, RSRC_CONF,
      "User and group for spawned processes"),
    { NULL }
};

static void suexec_hooks(kuda_pool_t *p)
{
    clhy_hook_get_suexec_identity(get_suexec_id_doer,NULL,NULL,KUDA_HOOK_MIDDLE);
    clhy_hook_post_config(suexec_post_config,NULL,NULL,KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(suexec) =
{
    STANDARD16_CAPI_STUFF,
    create_mconfig_for_directory,   /* create per-dir config */
    NULL,                       /* merge per-dir config */
    create_mconfig_for_server,  /* server config */
    NULL,                       /* merge server config */
    suexec_cmds,                /* command table */
    suexec_hooks                /* register hooks */
};
