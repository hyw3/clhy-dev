/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  capi_cgi.h
 * @brief CGI Script Execution Extension cAPI for cLHy
 *
 * @defgroup CAPI_CGI capi_cgi
 * @ingroup CLHYKUDEL_CAPIS
 * @{
 */

#ifndef _CAPI_CGI_H
#define _CAPI_CGI_H 1

#include "capi_include.h"

typedef enum {RUN_AS_SSI, RUN_AS_CGI} prog_types;

typedef struct {
    kuda_int32_t          in_pipe;
    kuda_int32_t          out_pipe;
    kuda_int32_t          err_pipe;
    int                  process_cgi;
    kuda_cmdtype_e        cmd_type;
    kuda_int32_t          detached;
    prog_types           prog_type;
    kuda_bucket_brigade **bb;
    include_ctx_t       *ctx;
    clhy_filter_t         *next;
    kuda_int32_t          addrspace;
} cgi_exec_info_t;

/**
 * Registerable optional function to override CGI behavior;
 * Reprocess the command and arguments to execute the given CGI script.
 * @param cmd Pointer to the command to execute (may be overridden)
 * @param argv Pointer to the arguments to pass (may be overridden)
 * @param r The current request
 * @param p The pool to allocate correct cmd/argv elements within.
 * @param e_info pass e_info.cmd_type (Set to KUDA_SHELLCMD or KUDA_PROGRAM on entry)
                      and e_info.detached (Should the child start in detached state?)
 * @remark This callback may be registered by the platforms-specific cAPI
 * to correct the command and arguments for kuda_proc_create invocation
 * on a given platforms.  capi_cgi will call the function if registered.
 */
KUDA_DECLARE_OPTIONAL_FN(kuda_status_t, clhy_cgi_build_command,
                        (const char **cmd, const char ***argv,
                         request_rec *r, kuda_pool_t *p,
                         cgi_exec_info_t *e_info));

#endif /* _CAPI_CGI_H */
/** @} */

