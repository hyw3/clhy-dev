capi_status.la: capi_status.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_status.lo $(CAPI_STATUS_LDADD)
capi_autoindex.la: capi_autoindex.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_autoindex.lo $(CAPI_AUTOINDEX_LDADD)
capi_info.la: capi_info.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_info.lo $(CAPI_INFO_LDADD)
capi_suexec.la: capi_suexec.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_suexec.lo $(CAPI_SUEXEC_LDADD)
capi_cgid.la: capi_cgid.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_cgid.lo $(CAPI_CGID_LDADD)
capi_cgi.la: capi_cgi.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_cgi.lo $(CAPI_CGI_LDADD)
DISTCLEAN_TARGETS = cAPIs.mk
static = 
shared =  capi_status.la capi_autoindex.la capi_info.la capi_suexec.la capi_cgid.la capi_cgi.la
