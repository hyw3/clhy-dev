/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Info cAPI.  Display configuration information for the server and
 * all included cAPIs.
 *
 * <Location /server-info>
 * SetHandler server-info
 * </Location>
 *
 * GET /server-info - Returns full configuration page for server and all cAPIs
 * GET /server-info?server - Returns server configuration only
 * GET /server-info?capi_name - Returns configuration for a single cAPI
 * GET /server-info?list - Returns quick list of included cAPIs
 * GET /server-info?config - Returns full configuration
 * GET /server-info?hooks - Returns a listing of the cAPIs active for each hook
 *
 * Original Author:
 *   Rasmus Lerdorf <rasmus vex.net>, May 1996
 *
 * Modified By:
 *   Lou Langholtz <ldl usi.utah.edu>, July 1997
 *
 * cLHy 1.0 Port:
 *   Ryan Morgan <rmorgan covalent.net>, August 2000
 *
 */


#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_lib.h"
#include "kuda_version.h"
#if KUDA_MAJOR_VERSION < 2
#include "kudelman_version.h"
#endif
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_main.h"
#include "http_protocol.h"
#include "http_connection.h"
#include "http_request.h"
#include "util_script.h"
#include "clhy_core.h"
#include "core_common.h"
#include "clhy_provider.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    const char *name;           /* matching cAPI name */
    const char *info;           /* additional info */
} info_entry;

typedef struct
{
    kuda_array_header_t *more_info;
} info_svr_conf;

cAPI CLHY_CAPI_DECLARE_DATA info_capi;

/* current file name when doing -DDUMP_CONFIG */
const char *dump_config_fn_info;
/* file handle when doing -DDUMP_CONFIG */
kuda_file_t *out = NULL;

static void *create_info_config(kuda_pool_t * p, server_rec * s)
{
    info_svr_conf *conf =
        (info_svr_conf *) kuda_pcalloc(p, sizeof(info_svr_conf));

    conf->more_info = kuda_array_make(p, 20, sizeof(info_entry));
    return conf;
}

static void *merge_info_config(kuda_pool_t * p, void *basev, void *overridesv)
{
    info_svr_conf *new =
        (info_svr_conf *) kuda_pcalloc(p, sizeof(info_svr_conf));
    info_svr_conf *base = (info_svr_conf *) basev;
    info_svr_conf *overrides = (info_svr_conf *) overridesv;

    new->more_info =
        kuda_array_append(p, overrides->more_info, base->more_info);
    return new;
}

static void put_int_flush_right(request_rec * r, int i, int field)
{
    if (field > 1 || i > 9)
        put_int_flush_right(r, i / 10, field - 1);
    if (i) {
        if (r)
            clhy_rputc('0' + i % 10, r);
        else
            kuda_file_putc((char)('0' + i % 10), out);
    }
    else {
        if (r)
            clhy_rputs("&nbsp;", r);
        else
            kuda_file_printf(out, " ");
    }
}

static void set_fn_info(request_rec *r, const char *name)
{
    if (r)
        clhy_set_capi_config(r->request_config, &info_capi, (void *)name);
    else
        dump_config_fn_info = name;
}

static const char *get_fn_info(request_rec *r)
{
    if (r)
        return clhy_get_capi_config(r->request_config, &info_capi);
    else
        return dump_config_fn_info;
}


static void capi_info_indent(request_rec * r, int nest,
                            const char *thisfn, int linenum)
{
    int i;
    const char *prevfn = get_fn_info(r);
    if (thisfn == NULL)
        thisfn = "*UNKNOWN*";
    if (prevfn == NULL || 0 != strcmp(prevfn, thisfn)) {
        if (r) {
            thisfn = clhy_escape_html(r->pool, thisfn);
            clhy_rprintf(r, "<dd><tt><strong>In file: %s</strong></tt></dd>\n",
                   thisfn);
        }
        else {
            kuda_file_printf(out, "# In file: %s\n", thisfn);
        }
        set_fn_info(r, thisfn);
    }

    if (r) {
        clhy_rputs("<dd><tt>", r);
        put_int_flush_right(r, linenum > 0 ? linenum : 0, 4);
        clhy_rputs(":&nbsp;", r);
    }
    else if (linenum > 0) {
        for (i = 1; i <= nest; ++i)
            kuda_file_printf(out, "  ");
        kuda_file_putc('#', out);
        put_int_flush_right(r, linenum, 4);
        kuda_file_printf(out, ":\n");
    }

    for (i = 1; i <= nest; ++i) {
        if (r)
            clhy_rputs("&nbsp;&nbsp;", r);
        else
            kuda_file_printf(out, "  ");
    }
}

static void capi_info_show_cmd(request_rec * r, const clhy_directive_t * dir,
                              int nest)
{
    capi_info_indent(r, nest, dir->filename, dir->line_num);
    if (r)
        clhy_rprintf(r, "%s <i>%s</i></tt></dd>\n",
                   clhy_escape_html(r->pool, dir->directive),
                   clhy_escape_html(r->pool, dir->args));
    else
        kuda_file_printf(out, "%s %s\n", dir->directive, dir->args);
}

static void capi_info_show_open(request_rec * r, const clhy_directive_t * dir,
                               int nest)
{
    capi_info_indent(r, nest, dir->filename, dir->line_num);
    if (r)
        clhy_rprintf(r, "%s %s</tt></dd>\n",
                   clhy_escape_html(r->pool, dir->directive),
                   clhy_escape_html(r->pool, dir->args));
    else
        kuda_file_printf(out, "%s %s\n", dir->directive, dir->args);
}

static void capi_info_show_close(request_rec * r, const clhy_directive_t * dir,
                                int nest)
{
    const char *dirname = dir->directive;
    capi_info_indent(r, nest, dir->filename, 0);
    if (*dirname == '<') {
        if (r)
            clhy_rprintf(r, "&lt;/%s&gt;</tt></dd>",
                       clhy_escape_html(r->pool, dirname + 1));
        else
            kuda_file_printf(out, "</%s>\n", dirname + 1);
    }
    else {
        if (r)
            clhy_rprintf(r, "/%s</tt></dd>", clhy_escape_html(r->pool, dirname));
        else
            kuda_file_printf(out, "/%s\n", dirname);
    }
}

static int capi_info_has_cmd(const command_rec * cmds, clhy_directive_t * dir)
{
    const command_rec *cmd;
    if (cmds == NULL)
        return 1;
    for (cmd = cmds; cmd->name; ++cmd) {
        if (strcasecmp(cmd->name, dir->directive) == 0)
            return 1;
    }
    return 0;
}

static void capi_info_show_parents(request_rec * r, clhy_directive_t * node,
                                  int from, int to)
{
    if (from < to)
        capi_info_show_parents(r, node->parent, from, to - 1);
    capi_info_show_open(r, node, to);
}

static int capi_info_capi_cmds(request_rec * r, const command_rec * cmds,
                                clhy_directive_t * node, int from, int level)
{
    int shown = from;
    clhy_directive_t *dir;
    if (level == 0)
        set_fn_info(r, NULL);
    for (dir = node; dir; dir = dir->next) {
        if (dir->first_child != NULL) {
            if (level < capi_info_capi_cmds(r, cmds, dir->first_child,
                                             shown, level + 1)) {
                shown = level;
                capi_info_show_close(r, dir, level);
            }
        }
        else if (capi_info_has_cmd(cmds, dir)) {
            if (shown < level) {
                capi_info_show_parents(r, dir->parent, shown, level - 1);
                shown = level;
            }
            capi_info_show_cmd(r, dir, level);
        }
    }
    return shown;
}

typedef struct
{                               /*XXX: should get something from kuda_hooks.h instead */
    void (*pFunc) (void);       /* just to get the right size */
    const char *szName;
    const char *const *aszPredecessors;
    const char *const *aszSuccessors;
    int nOrder;
} hook_struct_t;

/*
 * hook_get_t is a pointer to a function that takes void as an argument and
 * returns a pointer to an kuda_array_header_t.  The nasty WIN32 ifdef
 * is required to account for the fact that the clhy_hook* calls all use
 * STDCALL calling convention.
 */
typedef kuda_array_header_t *(
#ifdef WIN32
                                __stdcall
#endif
                                * hook_get_t)      (void);

typedef struct
{
    const char *name;
    hook_get_t get;
} hook_lookup_t;

static hook_lookup_t startup_hooks[] = {
    {"Pre-Config", clhy_hook_get_pre_config},
    {"Check Configuration", clhy_hook_get_check_config},
    {"Test Configuration", clhy_hook_get_test_config},
    {"Post Configuration", clhy_hook_get_post_config},
    {"Open Logs", clhy_hook_get_open_logs},
    {"Pre-cLMP", clhy_hook_get_pre_clmp},
    {"cLMP", clhy_hook_get_clcore},
    {"Drop Privileges", clhy_hook_get_drop_privileges},
    {"Retrieve Optional Functions", clhy_hook_get_optional_fn_retrieve},
    {"Child Init", clhy_hook_get_child_init},
    {NULL},
};

static hook_lookup_t request_hooks[] = {
    {"Pre-Connection", clhy_hook_get_pre_connection},
    {"Create Connection", clhy_hook_get_create_connection},
    {"Process Connection", clhy_hook_get_process_connection},
    {"Create Request", clhy_hook_get_create_request},
    {"Pre-Read Request", clhy_hook_get_pre_read_request},
    {"Post-Read Request", clhy_hook_get_post_read_request},
    {"Header Parse", clhy_hook_get_header_parser},
    {"HTTP Scheme", clhy_hook_get_http_scheme},
    {"Default Port", clhy_hook_get_default_port},
    {"Quick Handler", clhy_hook_get_quick_handler},
    {"Translate Name", clhy_hook_get_translate_name},
    {"Map to Storage", clhy_hook_get_map_to_storage},
    {"Check Access", clhy_hook_get_access_checker_ex},
    {"Check Access (legacy)", clhy_hook_get_access_checker},
    {"Verify User ID", clhy_hook_get_check_user_id},
    {"Note Authentication Failure", clhy_hook_get_note_auth_failure},
    {"Verify User Access", clhy_hook_get_auth_checker},
    {"Check Type", clhy_hook_get_type_checker},
    {"Fixups", clhy_hook_get_fixups},
    {"Insert Filters", clhy_hook_get_insert_filter},
    {"Content Handlers", clhy_hook_get_handler},
    {"Transaction Logging", clhy_hook_get_log_transaction},
    {"Insert Errors", clhy_hook_get_insert_error_filter},
    {"Generate Log ID", clhy_hook_get_generate_log_id},
    {NULL},
};

static hook_lookup_t other_hooks[] = {
    {"Monitor", clhy_hook_get_monitor},
    {"Child Status", clhy_hook_get_child_status},
    {"End Generation", clhy_hook_get_end_generation},
    {"Error Logging", clhy_hook_get_error_log},
    {"Query cLMP Attributes", clhy_hook_get_clmp_query},
    {"Query cLMP Name", clhy_hook_get_clmp_get_name},
    {"Register Timed Callback", clhy_hook_get_clmp_register_timed_callback},
    {"Extend Expression Parser", clhy_hook_get_expr_lookup},
    {"Set Management Items", clhy_hook_get_get_mgmt_items},
#if CLHY_ENABLE_EXCEPTION_HOOK
    {"Handle Fatal Exceptions", clhy_hook_get_fatal_exception},
#endif
    {NULL},
};

static int capi_find_hook(cAPI * capip, hook_get_t hook_get)
{
    int i;
    kuda_array_header_t *hooks = hook_get();
    hook_struct_t *elts;

    if (!hooks) {
        return 0;
    }

    elts = (hook_struct_t *) hooks->elts;

    for (i = 0; i < hooks->nelts; i++) {
        if (strcmp(elts[i].szName, capip->name) == 0) {
            return 1;
        }
    }

    return 0;
}

static void capi_participate(request_rec * r,
                               cAPI * capip,
                               hook_lookup_t * lookup, int *comma)
{
    if (capi_find_hook(capip, lookup->get)) {
        if (*comma) {
            clhy_rputs(", ", r);
        }
        clhy_rvputs(r, "<tt>", lookup->name, "</tt>", NULL);
        *comma = 1;
    }
}

static void capi_request_hook_participate(request_rec * r, cAPI * capip)
{
    int i, comma = 0;

    clhy_rputs("<dt><strong>Request Phase Participation:</strong>\n", r);

    for (i = 0; request_hooks[i].name; i++) {
        capi_participate(r, capip, &request_hooks[i], &comma);
    }

    if (!comma) {
        clhy_rputs("<tt> <em>none</em></tt>", r);
    }
    clhy_rputs("</dt>\n", r);
}

static const char *find_more_info(server_rec * s, const char *capi_name)
{
    int i;
    info_svr_conf *conf =
        (info_svr_conf *) clhy_get_capi_config(s->capi_config,
                                               &info_capi);
    info_entry *entry = (info_entry *) conf->more_info->elts;

    if (!capi_name) {
        return 0;
    }
    for (i = 0; i < conf->more_info->nelts; i++) {
        if (!strcmp(capi_name, entry->name)) {
            return entry->info;
        }
        entry++;
    }
    return 0;
}

static int show_server_settings(request_rec * r)
{
    server_rec *serv = r->server;
    int max_daemons, forked, threaded;

    clhy_rputs("<h2><a name=\"server\">Server Settings</a></h2>", r);
    clhy_rprintf(r,
               "<dl><dt><strong>Server Version:</strong> "
               "<font size=\"+1\"><tt>%s</tt></font></dt>\n",
               clhy_get_server_description());
    clhy_rprintf(r,
               "<dt><strong>Server Built:</strong> "
               "<font size=\"+1\"><tt>%s</tt></font></dt>\n",
               clhy_get_server_built());
    clhy_rprintf(r,
               "<dt><strong>Server loaded KUDA Version:</strong> "
               "<tt>%s</tt></dt>\n", kuda_version_string());
    clhy_rprintf(r,
               "<dt><strong>Compiled with KUDA Version:</strong> "
               "<tt>%s</tt></dt>\n", KUDA_VERSION_STRING);
#if KUDA_MAJOR_VERSION < 2
    clhy_rprintf(r,
               "<dt><strong>Server loaded KUDELMAN Version:</strong> "
               "<tt>%s</tt></dt>\n", kudelman_version_string());
    clhy_rprintf(r,
               "<dt><strong>Compiled with KUDELMAN Version:</strong> "
               "<tt>%s</tt></dt>\n", KUDELMAN_VERSION_STRING);
#endif
    clhy_rprintf(r,
               "<dt><strong>cAPI Magic Number:</strong> "
               "<tt>%d:%d</tt></dt>\n", CAPI_MAGIC_NUMBER_MAJOR,
               CAPI_MAGIC_NUMBER_MINOR);
    clhy_rprintf(r,
               "<dt><strong>Hostname/port:</strong> "
               "<tt>%s:%u</tt></dt>\n",
               clhy_escape_html(r->pool, clhy_get_server_name(r)),
               clhy_get_server_port(r));
    clhy_rprintf(r,
               "<dt><strong>Timeouts:</strong> "
               "<tt>connection: %d &nbsp;&nbsp; "
               "keep-alive: %d</tt></dt>",
               (int) (kuda_time_sec(serv->timeout)),
               (int) (kuda_time_sec(serv->keep_alive_timeout)));
    clhy_clmp_query(CLHY_CLMPQ_MAX_DAEMON_USED, &max_daemons);
    clhy_clmp_query(CLHY_CLMPQ_IS_THREADED, &threaded);
    clhy_clmp_query(CLHY_CLMPQ_IS_FORKED, &forked);
    clhy_rprintf(r, "<dt><strong>cLMP Name:</strong> <tt>%s</tt></dt>\n",
               clhy_show_clmp());
    clhy_rprintf(r,
               "<dt><strong>cLMP Information:</strong> "
               "<tt>Max Daemons: %d Threaded: %s Forked: %s</tt></dt>\n",
               max_daemons, threaded ? "yes" : "no", forked ? "yes" : "no");
    clhy_rprintf(r,
               "<dt><strong>Server Architecture:</strong> "
               "<tt>%ld-bit</tt></dt>\n", 8 * (long) sizeof(void *));
    clhy_rprintf(r,
               "<dt><strong>Server Root:</strong> "
               "<tt>%s</tt></dt>\n", clhy_server_root);
    clhy_rprintf(r,
               "<dt><strong>Config File:</strong> "
               "<tt>%s</tt></dt>\n", clhy_conftree->filename);

    clhy_rputs("<dt><strong>Server Built With:</strong>\n"
             "<tt style=\"white-space: pre;\">\n", r);

    /* TODO: Not all of these defines are getting set like they do in main.c.
     *       Missing some headers?
     */

#ifdef BIG_SECURITY_HOLE
    clhy_rputs(" -D BIG_SECURITY_HOLE\n", r);
#endif

#ifdef SECURITY_HOLE_PASS_AUTHORIZATION
    clhy_rputs(" -D SECURITY_HOLE_PASS_AUTHORIZATION\n", r);
#endif

#ifdef PLATFORM
    clhy_rputs(" -D PLATFORM=\"" PLATFORM "\"\n", r);
#endif

#ifdef HAVE_SHMGET
    clhy_rputs(" -D HAVE_SHMGET\n", r);
#endif

#if KUDA_FILE_BASED_SHM
    clhy_rputs(" -D KUDA_FILE_BASED_SHM\n", r);
#endif

#if KUDA_HAS_SENDFILE
    clhy_rputs(" -D KUDA_HAS_SENDFILE\n", r);
#endif

#if KUDA_HAS_MMAP
    clhy_rputs(" -D KUDA_HAS_MMAP\n", r);
#endif

#ifdef NO_WRITEV
    clhy_rputs(" -D NO_WRITEV\n", r);
#endif

#ifdef NO_LINGCLOSE
    clhy_rputs(" -D NO_LINGCLOSE\n", r);
#endif

#if KUDA_HAVE_IPV6
    clhy_rputs(" -D KUDA_HAVE_IPV6 (IPv4-mapped addresses ", r);
#ifdef CLHY_ENABLE_V4_MAPPED
    clhy_rputs("enabled)\n", r);
#else
    clhy_rputs("disabled)\n", r);
#endif
#endif

#if KUDA_USE_FLOCK_SERIALIZE
    clhy_rputs(" -D KUDA_USE_FLOCK_SERIALIZE\n", r);
#endif

#if KUDA_USE_SYSVSEM_SERIALIZE
    clhy_rputs(" -D KUDA_USE_SYSVSEM_SERIALIZE\n", r);
#endif

#if KUDA_USE_POSIXSEM_SERIALIZE
    clhy_rputs(" -D KUDA_USE_POSIXSEM_SERIALIZE\n", r);
#endif

#if KUDA_USE_FCNTL_SERIALIZE
    clhy_rputs(" -D KUDA_USE_FCNTL_SERIALIZE\n", r);
#endif

#if KUDA_USE_PROC_PTHREAD_SERIALIZE
    clhy_rputs(" -D KUDA_USE_PROC_PTHREAD_SERIALIZE\n", r);
#endif
#if KUDA_PROCESS_LOCK_IS_GLOBAL
    clhy_rputs(" -D KUDA_PROCESS_LOCK_IS_GLOBAL\n", r);
#endif

#ifdef SINGLE_LISTEN_UNSERIALIZED_ACCEPT
    clhy_rputs(" -D SINGLE_LISTEN_UNSERIALIZED_ACCEPT\n", r);
#endif

#if KUDA_HAS_OTHER_CHILD
    clhy_rputs(" -D KUDA_HAS_OTHER_CHILD\n", r);
#endif

#ifdef CLHY_HAVE_RELIABLE_PIPED_LOGS
    clhy_rputs(" -D CLHY_HAVE_RELIABLE_PIPED_LOGS\n", r);
#endif

#ifdef BUFFERED_LOGS
    clhy_rputs(" -D BUFFERED_LOGS\n", r);
#ifdef PIPE_BUF
    clhy_rputs(" -D PIPE_BUF=%ld\n", (long) PIPE_BUF, r);
#endif
#endif

#if KUDA_CHARSET_EBCDIC
    clhy_rputs(" -D KUDA_CHARSET_EBCDIC\n", r);
#endif

#ifdef NEED_HASHBANG_EMUL
    clhy_rputs(" -D NEED_HASHBANG_EMUL\n", r);
#endif

/* This list displays the compiled in default paths: */
#ifdef WWHY_ROOT
    clhy_rputs(" -D WWHY_ROOT=\"" WWHY_ROOT "\"\n", r);
#endif

#ifdef SUEXEC_BIN
    clhy_rputs(" -D SUEXEC_BIN=\"" SUEXEC_BIN "\"\n", r);
#endif

#ifdef DEFAULT_PIDLOG
    clhy_rputs(" -D DEFAULT_PIDLOG=\"" DEFAULT_PIDLOG "\"\n", r);
#endif

#ifdef DEFAULT_SCOREBOARD
    clhy_rputs(" -D DEFAULT_SCOREBOARD=\"" DEFAULT_SCOREBOARD "\"\n", r);
#endif

#ifdef DEFAULT_ERRORLOG
    clhy_rputs(" -D DEFAULT_ERRORLOG=\"" DEFAULT_ERRORLOG "\"\n", r);
#endif


#ifdef CLHY_TYPES_CONFIG_FILE
    clhy_rputs(" -D CLHY_TYPES_CONFIG_FILE=\"" CLHY_TYPES_CONFIG_FILE "\"\n", r);
#endif

#ifdef SERVER_CONFIG_FILE
    clhy_rputs(" -D SERVER_CONFIG_FILE=\"" SERVER_CONFIG_FILE "\"\n", r);
#endif
    clhy_rputs("</tt></dt>\n", r);
    clhy_rputs("</dl><hr />", r);
    return 0;
}

static int dump_a_hook(request_rec * r, hook_get_t hook_get)
{
    int i;
    char qs;
    hook_struct_t *elts;
    kuda_array_header_t *hooks = hook_get();

    if (!hooks) {
        return 0;
    }

    if (r->args && strcasecmp(r->args, "hooks") == 0) {
        qs = '?';
    }
    else {
        qs = '#';
    }

    elts = (hook_struct_t *) hooks->elts;

    for (i = 0; i < hooks->nelts; i++) {
        clhy_rprintf(r,
                   "&nbsp;&nbsp; %02d <a href=\"%c%s\">%s</a> <br/>",
                   elts[i].nOrder, qs, elts[i].szName, elts[i].szName);
    }
    return 0;
}

static int show_active_hooks(request_rec * r)
{
    int i;
    clhy_rputs("<h2><a name=\"startup_hooks\">Startup Hooks</a></h2>\n<dl>", r);

    for (i = 0; startup_hooks[i].name; i++) {
        clhy_rprintf(r, "<dt><strong>%s:</strong>\n <br /><tt>\n",
                   startup_hooks[i].name);
        dump_a_hook(r, startup_hooks[i].get);
        clhy_rputs("\n  </tt>\n</dt>\n", r);
    }

    clhy_rputs
        ("</dl>\n<hr />\n<h2><a name=\"request_hooks\">Request Hooks</a></h2>\n<dl>",
         r);

    for (i = 0; request_hooks[i].name; i++) {
        clhy_rprintf(r, "<dt><strong>%s:</strong>\n <br /><tt>\n",
                   request_hooks[i].name);
        dump_a_hook(r, request_hooks[i].get);
        clhy_rputs("\n  </tt>\n</dt>\n", r);
    }

    clhy_rputs
        ("</dl>\n<hr />\n<h2><a name=\"other_hooks\">Other Hooks</a></h2>\n<dl>",
         r);

    for (i = 0; other_hooks[i].name; i++) {
        clhy_rprintf(r, "<dt><strong>%s:</strong>\n <br /><tt>\n",
                   other_hooks[i].name);
        dump_a_hook(r, other_hooks[i].get);
        clhy_rputs("\n  </tt>\n</dt>\n", r);
    }

    clhy_rputs("</dl>\n<hr />\n", r);

    return 0;
}

static int cmp_provider_groups(const void *a_, const void *b_)
{
    const clhy_list_provider_groups_t *a = a_, *b = b_;
    int ret = strcmp(a->provider_group, b->provider_group);
    if (!ret)
        ret = strcmp(a->provider_version, b->provider_version);
    return ret;
}

static int cmp_provider_names(const void *a_, const void *b_)
{
    const clhy_list_provider_names_t *a = a_, *b = b_;
    return strcmp(a->provider_name, b->provider_name);
}

static void show_providers(request_rec *r)
{
    kuda_array_header_t *groups = clhy_list_provider_groups(r->pool);
    clhy_list_provider_groups_t *group;
    kuda_array_header_t *names;
    clhy_list_provider_names_t *name;
    int i,j;
    const char *cur_group = NULL;

    qsort(groups->elts, groups->nelts, sizeof(clhy_list_provider_groups_t),
          cmp_provider_groups);
    clhy_rputs("<h2><a name=\"providers\">Providers</a></h2>\n<dl>", r);

    for (i = 0; i < groups->nelts; i++) {
        group = &KUDA_ARRAY_IDX(groups, i, clhy_list_provider_groups_t);
        if (!cur_group || strcmp(cur_group, group->provider_group) != 0) {
            if (cur_group)
                clhy_rputs("\n</dt>\n", r);
            cur_group = group->provider_group;
            clhy_rprintf(r, "<dt><strong>%s</strong> (version <tt>%s</tt>):"
                          "\n <br />\n", cur_group, group->provider_version);
        }
        names = clhy_list_provider_names(r->pool, group->provider_group,
                                       group->provider_version);
        qsort(names->elts, names->nelts, sizeof(clhy_list_provider_names_t),
              cmp_provider_names);
        for (j = 0; j < names->nelts; j++) {
            name = &KUDA_ARRAY_IDX(names, j, clhy_list_provider_names_t);
            clhy_rprintf(r, "<tt>&nbsp;&nbsp;%s</tt><br/>", name->provider_name);
        }
    }
    if (cur_group)
        clhy_rputs("\n</dt>\n", r);
    clhy_rputs("</dl>\n<hr />\n", r);
}

static int cmp_capi_name(const void *a_, const void *b_)
{
    const cAPI * const *a = a_;
    const cAPI * const *b = b_;
    return strcmp((*a)->name, (*b)->name);
}

static kuda_array_header_t *get_sorted_capis(kuda_pool_t *p)
{
    kuda_array_header_t *arr = kuda_array_make(p, 64, sizeof(cAPI *));
    cAPI *capip, **entry;
    for (capip = clhy_top_capi; capip; capip = capip->next) {
        entry = &KUDA_ARRAY_PUSH(arr, cAPI *);
        *entry = capip;
    }
    qsort(arr->elts, arr->nelts, sizeof(cAPI *), cmp_capi_name);
    return arr;
}

static int display_info(request_rec * r)
{
    cAPI *capip = NULL;
    const char *more_info;
    const command_rec *cmd;
    kuda_array_header_t *cAPIs = NULL;
    int i;

    if (strcmp(r->handler, "server-info")) {
        return DECLINED;
    }

    r->allowed |= (CLHY_METHOD_BIT << M_GET);
    if (r->method_number != M_GET) {
        return DECLINED;
    }

    clhy_set_content_type(r, "text/html; charset=ISO-8859-1");

    clhy_rputs(DOCTYPE_XHTML_1_0T
             "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
             "<head>\n"
             "  <title>Server Information</title>\n" "</head>\n", r);
    clhy_rputs("<body><h1 style=\"text-align: center\">"
             "cLHy Server Information</h1>\n", r);
    if (!r->args || strcasecmp(r->args, "list")) {
        if (!r->args) {
            clhy_rputs("<dl><dt><tt>Subpages:<br />", r);
            clhy_rputs("<a href=\"?config\">Configuration Files</a>, "
                     "<a href=\"?server\">Server Settings</a>, "
                     "<a href=\"?list\">cAPI List</a>, "
                     "<a href=\"?hooks\">Active Hooks</a>, "
                     "<a href=\"?providers\">Available Providers</a>", r);
            clhy_rputs("</tt></dt></dl><hr />", r);

            clhy_rputs("<dl><dt><tt>Sections:<br />", r);
            clhy_rputs("<a href=\"#cAPIs\">Activated cAPIs</a>, "
                     "<a href=\"#server\">Server Settings</a>, "
                     "<a href=\"#startup_hooks\">Startup Hooks</a>, "
                     "<a href=\"#request_hooks\">Request Hooks</a>, "
                     "<a href=\"#other_hooks\">Other Hooks</a>, "
                     "<a href=\"#providers\">Providers</a>", r);
            clhy_rputs("</tt></dt></dl><hr />", r);

            clhy_rputs("<h2><a name=\"cAPIs\">Activated cAPIs</a></h2>"
                    "<dl><dt><tt>", r);

            cAPIs = get_sorted_capis(r->pool);
            for (i = 0; i < cAPIs->nelts; i++) {
                capip = KUDA_ARRAY_IDX(cAPIs, i, cAPI *);
                clhy_rprintf(r, "<a href=\"#%s\">%s</a>", capip->name,
                           capip->name);
                if (i < cAPIs->nelts) {
                    clhy_rputs(", ", r);
                }
            }
            clhy_rputs("</tt></dt></dl><hr />", r);
        }

        if (!r->args || !strcasecmp(r->args, "server")) {
            show_server_settings(r);
        }

        if (!r->args || !strcasecmp(r->args, "hooks")) {
            show_active_hooks(r);
        }

        if (!r->args || !strcasecmp(r->args, "providers")) {
            show_providers(r);
        }

        if (r->args && 0 == strcasecmp(r->args, "config")) {
            clhy_rputs("<dl><dt><strong>Configuration:</strong>\n", r);
            capi_info_capi_cmds(r, NULL, clhy_conftree, 0, 0);
            clhy_rputs("</dl><hr />", r);
        }
        else {
            int comma = 0;
            if (!cAPIs)
                 cAPIs = get_sorted_capis(r->pool);
            for (i = 0; i < cAPIs->nelts; i++) {
                capip = KUDA_ARRAY_IDX(cAPIs, i, cAPI *);
                if (!r->args || !strcasecmp(capip->name, r->args)) {
                    clhy_rprintf(r,
                               "<dl><dt><a name=\"%s\"><strong>cAPI Name:</strong></a> "
                               "<font size=\"+1\"><tt><a href=\"?%s\">%s</a></tt></font></dt>\n",
                               capip->name, capip->name, capip->name);
                    clhy_rputs("<dt><strong>Content handlers:</strong> ", r);

                    if (capi_find_hook(capip, clhy_hook_get_handler)) {
                        clhy_rputs("<tt> <em>yes</em></tt>", r);
                    }
                    else {
                        clhy_rputs("<tt> <em>none</em></tt>", r);
                    }

                    clhy_rputs("</dt>", r);
                    clhy_rputs
                        ("<dt><strong>Configuration Phase Participation:</strong>\n",
                         r);
                    if (capip->create_dir_config) {
                        if (comma) {
                            clhy_rputs(", ", r);
                        }
                        clhy_rputs("<tt>Create Directory Config</tt>", r);
                        comma = 1;
                    }
                    if (capip->merge_dir_config) {
                        if (comma) {
                            clhy_rputs(", ", r);
                        }
                        clhy_rputs("<tt>Merge Directory Configs</tt>", r);
                        comma = 1;
                    }
                    if (capip->create_server_config) {
                        if (comma) {
                            clhy_rputs(", ", r);
                        }
                        clhy_rputs("<tt>Create Server Config</tt>", r);
                        comma = 1;
                    }
                    if (capip->merge_server_config) {
                        if (comma) {
                            clhy_rputs(", ", r);
                        }
                        clhy_rputs("<tt>Merge Server Configs</tt>", r);
                        comma = 1;
                    }
                    if (!comma)
                        clhy_rputs("<tt> <em>none</em></tt>", r);
                    comma = 0;
                    clhy_rputs("</dt>", r);

                    capi_request_hook_participate(r, capip);

                    cmd = capip->cmds;
                    if (cmd) {
                        clhy_rputs
                            ("<dt><strong>cAPI Directives:</strong></dt>",
                             r);
                        while (cmd) {
                            if (cmd->name) {
                                clhy_rprintf(r, "<dd><tt>%s%s - <i>",
                                           clhy_escape_html(r->pool, cmd->name),
                                           cmd->name[0] == '<' ? "&gt;" : "");
                                if (cmd->errmsg) {
                                    clhy_rputs(clhy_escape_html(r->pool, cmd->errmsg), r);
                                }
                                clhy_rputs("</i></tt></dd>\n", r);
                            }
                            else {
                                break;
                            }
                            cmd++;
                        }
                        clhy_rputs
                            ("<dt><strong>Current Configuration:</strong></dt>\n",
                             r);
                        capi_info_capi_cmds(r, capip->cmds, clhy_conftree, 0,
                                             0);
                    }
                    else {
                        clhy_rputs
                            ("<dt><strong>cAPI Directives:</strong> <tt>none</tt></dt>",
                             r);
                    }
                    more_info = find_more_info(r->server, capip->name);
                    if (more_info) {
                        clhy_rputs
                            ("<dt><strong>Additional Information:</strong>\n</dt><dd>",
                             r);
                        clhy_rputs(more_info, r);
                        clhy_rputs("</dd>", r);
                    }
                    clhy_rputs("</dl><hr />\n", r);
                    if (r->args) {
                        break;
                    }
                }
            }
            if (!capip && r->args && strcasecmp(r->args, "server")) {
                clhy_rputs("<p><b>No such cAPI</b></p>\n", r);
            }
        }
    }
    else {
        clhy_rputs("<dl><dt>Server cAPI List</dt>", r);
        cAPIs = get_sorted_capis(r->pool);
        for (i = 0; i < cAPIs->nelts; i++) {
            capip = KUDA_ARRAY_IDX(cAPIs, i, cAPI *);
            clhy_rputs("<dd>", r);
            clhy_rputs(capip->name, r);
            clhy_rputs("</dd>", r);
        }
        clhy_rputs("</dl><hr />", r);
    }
    clhy_rputs(clhy_psignature("", r), r);
    clhy_rputs("</body></html>\n", r);
    /* Done, turn off timeout, close file and return */
    return 0;
}

static const char *add_capi_info(cmd_parms * cmd, void *dummy,
                                   const char *name, const char *info)
{
    server_rec *s = cmd->server;
    info_svr_conf *conf =
        (info_svr_conf *) clhy_get_capi_config(s->capi_config,
                                               &info_capi);
    info_entry *new = kuda_array_push(conf->more_info);

    new->name = name;
    new->info = info;
    return NULL;
}

static const command_rec info_cmds[] = {
    CLHY_INIT_TAKE2("AddcAPIInfo", add_capi_info, NULL, RSRC_CONF,
                  "a cAPI name and additional information on that cAPI"),
    {NULL}
};

static int check_config(kuda_pool_t *pconf, kuda_pool_t *plog, kuda_pool_t *ptemp,
                        server_rec *s)
{
    if (clhy_exists_config_define("DUMP_CONFIG")) {
        kuda_file_open_stdout(&out, ptemp);
        capi_info_capi_cmds(NULL, NULL, clhy_conftree, 0, 0);
    }

    return DECLINED;
}


static void register_hooks(kuda_pool_t * p)
{
    clhy_hook_handler(display_info, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_check_config(check_config, NULL, NULL, KUDA_HOOK_FIRST);
}

CLHY_DECLARE_CAPI(info) = {
    STANDARD16_CAPI_STUFF,
    NULL,                       /* dir config creater */
    NULL,                       /* dir merger --- default is to override */
    create_info_config,         /* server config */
    merge_info_config,          /* merge server config */
    info_cmds,                  /* command kuda_table_t */
    register_hooks
};
