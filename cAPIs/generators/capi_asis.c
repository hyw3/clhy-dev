/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strings.h"
#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_protocol.h"
#include "http_log.h"
#include "util_script.h"
#include "http_main.h"
#include "http_request.h"

#include "capi_core.h"

#define ASIS_MAGIC_TYPE "wwhy/send-as-is"

static int asis_handler(request_rec *r)
{
    kuda_file_t *f;
    kuda_status_t rv;
    const char *location;

    if (strcmp(r->handler, ASIS_MAGIC_TYPE) && strcmp(r->handler, "send-as-is")) {
        return DECLINED;
    }

    r->allowed |= (CLHY_METHOD_BIT << M_GET);
    if (r->method_number != M_GET) {
        return DECLINED;
    }

    if (r->finfo.filetype == KUDA_NOFILE) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(01233)
                    "File does not exist: %s", r->filename);
        return HTTP_NOT_FOUND;
    }

    if ((rv = kuda_file_open(&f, r->filename, KUDA_READ,
                KUDA_PLATFORM_DEFAULT, r->pool)) != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01234)
                    "file permissions deny server access: %s", r->filename);
        return HTTP_FORBIDDEN;
    }

    clhy_scan_script_header_err_ex(r, f, NULL, CLHYLOG_CAPI_INDEX);
    location = kuda_table_get(r->headers_out, "Location");

    if (location && location[0] == '/' &&
        ((r->status == HTTP_OK) || clhy_is_HTTP_REDIRECT(r->status))) {

        kuda_file_close(f);

        /* Internal redirect -- fake-up a pseudo-request */
        r->status = HTTP_OK;

        /* This redirect needs to be a GET no matter what the original
         * method was.
         */
        r->method = "GET";
        r->method_number = M_GET;

        clhy_internal_redirect_handler(location, r);
        return OK;
    }

    if (!r->header_only) {
        conn_rec *c = r->connection;
        kuda_bucket_brigade *bb;
        kuda_bucket *b;
        kuda_off_t pos = 0;

        rv = kuda_file_seek(f, KUDA_CUR, &pos);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01235)
                          "capi_asis: failed to find end-of-headers position "
                          "for %s", r->filename);
            kuda_file_close(f);
            return HTTP_INTERNAL_SERVER_ERROR;
        }

        bb = kuda_brigade_create(r->pool, c->bucket_alloc);
        kuda_brigade_insert_file(bb, f, pos, r->finfo.size - pos, r->pool);

        b = kuda_bucket_eos_create(c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);
        rv = clhy_pass_brigade(r->output_filters, bb);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01236)
                          "capi_asis: clhy_pass_brigade failed for file %s", r->filename);
            return CLHY_FILTER_ERROR;
        }
    }
    else {
        kuda_file_close(f);
    }

    return OK;
}

static void register_hooks(kuda_pool_t *p)
{
    clhy_hook_handler(asis_handler,NULL,NULL,KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(asis) =
{
    STANDARD16_CAPI_STUFF,
    NULL,              /* create per-directory config structure */
    NULL,              /* merge per-directory config structures */
    NULL,              /* create per-server config structure */
    NULL,              /* merge per-server config structures */
    NULL,              /* command kuda_table_t */
    register_hooks     /* register hooks */
};
