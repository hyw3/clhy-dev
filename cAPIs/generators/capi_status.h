/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  capi_status.h
 * @brief Status Report Extension cAPI to cLHy
 *
 * @defgroup CAPI_STATUS capi_status
 * @ingroup  CLHYKUDEL_CAPIS
 * @{
 */

#ifndef CAPI_STATUS_H
#define CAPI_STATUS_H

#include "clhy_config.h"
#include "wwhy.h"

#define CLHY_STATUS_SHORT    (0x1)  /* short, non-HTML report requested */
#define CLHY_STATUS_NOTABLE  (0x2)  /* HTML report without tables */
#define CLHY_STATUS_EXTENDED (0x4)  /* detailed report */

#if !defined(WIN32)
#define STATUS_DECLARE(type)            type
#define STATUS_DECLARE_NONSTD(type)     type
#define STATUS_DECLARE_DATA
#elif defined(STATUS_DECLARE_STATIC)
#define STATUS_DECLARE(type)            type __stdcall
#define STATUS_DECLARE_NONSTD(type)     type
#define STATUS_DECLARE_DATA
#elif defined(STATUS_DECLARE_EXPORT)
#define STATUS_DECLARE(type)            __declspec(dllexport) type __stdcall
#define STATUS_DECLARE_NONSTD(type)     __declspec(dllexport) type
#define STATUS_DECLARE_DATA             __declspec(dllexport)
#else
#define STATUS_DECLARE(type)            __declspec(dllimport) type __stdcall
#define STATUS_DECLARE_NONSTD(type)     __declspec(dllimport) type
#define STATUS_DECLARE_DATA             __declspec(dllimport)
#endif

/* Optional hooks which can insert extra content into the capi_status
 * output.  FLAGS will be set to the bitwise OR of any of the
 * CLHY_STATUS_* flags.
 *
 * Implementations of this hook should generate content using
 * functions in the clhy_rputs/clhy_rprintf family; each hook should
 * return OK or DECLINED. */
KUDA_DECLARE_EXTERNAL_HOOK(clhy, STATUS, int, status_hook,
                          (request_rec *r, int flags))
#endif
/** @} */
