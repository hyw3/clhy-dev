dnl cAPIs enabled in this directory by default

dnl CLHYKUDEL_CAPI(name, helptext[, objects[, structname[, default[, config]]]])

CLHYKUDEL_CAPIPATH_INIT(generators)

CLHYKUDEL_CAPI(status, process/thread monitoring, , , yes)
CLHYKUDEL_CAPI(autoindex, directory listing, , , yes)
CLHYKUDEL_CAPI(asis, as-is filetypes, , , )
CLHYKUDEL_CAPI(info, server information, , , most)
CLHYKUDEL_CAPI(suexec, set uid and gid for spawned processes, , , no, [
              other_targets=suexec ] )

# Is capi_cgid needed?
case $host in
    *mingw*)
        dnl No fork+thread+fd issues, and cgid doesn't work anyway.
        cgid_needed="no"
        ;;
    *)
        if clhy_clmp_is_threaded; then
            dnl if we are using a threaded cLMP on Unix, we can get better
            dnl performance with capi_cgid, and also avoid potential issues
            dnl with forking from a threaded process.
            cgid_needed="yes"
        else
            dnl if we are using a non-threaded cLMP, it makes little sense to
            dnl use capi_cgid, and it just opens up holes we don't need.
            cgid_needed="no"
        fi
        ;;
esac

if test $cgid_needed = "yes"; then
    CLHYKUDEL_CAPI(cgid, CGI scripts.  Enabled by default with threaded cLMPs, , , most, [
    case $host in
      *-solaris2*)
        case `uname -r` in
          5.10)
          dnl Does the system have the appropriate patches?
          case `uname -p` in
            i386)
              patch_id="120665"
              ;;
            sparc)
              patch_id="120664"
              ;;
            *)
              AC_MSG_WARN([Unknown platform])
              patch_id="120664"
              ;;
          esac
          AC_MSG_CHECKING([for Solaris patch $patch_id])
          showrev -p | grep "$patch_id" >/dev/null 2>&1
          if test $? -eq 1; then
          dnl Solaris 11 (next release) as of snv_19 doesn't have this problem.
          dnl It may be possible to use /kernel/drv/tl from later releases.
          AC_MSG_ERROR([Patch for either Sparc or x86 to functioning capi_cgid.
Without these patches, capi_cgid is non-functional on Solaris 10 due to an PLATFORM
bug with AF_UNIX sockets.
If you can not apply these patches, you can do one of the following:
 - run configure with --disable-cgid
 - switch to the prefork cLMP
Fix this to improve checks on Solaris 10 about the AF_UNIX bugs.])
          else
            AC_MSG_RESULT(yes)
          fi
          ;;
        esac
        ;;
    esac
  ])
    CLHYKUDEL_CAPI(cgi, CGI scripts.  Enabled by default with non-threaded cLMPs, , , no)
else
    CLHYKUDEL_CAPI(cgi, CGI scripts.  Enabled by default with non-threaded cLMPs, , , most)
    CLHYKUDEL_CAPI(cgid, CGI scripts.  Enabled by default with threaded cLMPs, , , no)
fi

KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

CLHYKUDEL_CAPIPATH_FINISH
