/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * http_script: keeps all script-related ramblings together.
 *
 * Compliant to CGI/1.1 spec
 *
 * Adapted by rst from original NCSA code by Rob McCool
 *
 * This cAPIs uses a wwhy core function (clhy_add_common_vars) to add some new env vars, 
 * like REDIRECT_URL and REDIRECT_QUERY_STRING for custom error responses and DOCUMENT_ROOT.
 * It also adds SERVER_ADMIN - useful for scripts to know who to mail when they fail.
 * 
 */

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_thread_proc.h"    /* for RLIMIT stuff */
#include "kuda_optional.h"
#include "kuda_buckets.h"
#include "kuda_lib.h"
#include "kuda_poll.h"

#define KUDA_WANT_STRFUNC
#define KUDA_WANT_MEMFUNC
#include "kuda_want.h"

#include "util_filter.h"
#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_request.h"
#include "http_core.h"
#include "http_protocol.h"
#include "http_main.h"
#include "http_log.h"
#include "util_script.h"
#include "clhy_core.h"
#include "capi_core.h"
#include "capi_cgi.h"

#if KUDA_HAVE_STRUCT_RLIMIT
#if defined (RLIMIT_CPU) || defined (RLIMIT_NPROC) || defined (RLIMIT_DATA) || defined(RLIMIT_VMEM) || defined(RLIMIT_AS)
#define CLHY_CGI_USE_RLIMIT
#endif
#endif

cAPI CLHY_CAPI_DECLARE_DATA cgi_capi;

static KUDA_OPTIONAL_FN_TYPE(clhy_register_include_handler) *cgi_pfn_reg_with_ssi;
static KUDA_OPTIONAL_FN_TYPE(clhy_ssi_get_tag_and_value) *cgi_pfn_gtv;
static KUDA_OPTIONAL_FN_TYPE(clhy_ssi_parse_string) *cgi_pfn_ps;
static KUDA_OPTIONAL_FN_TYPE(clhy_cgi_build_command) *cgi_build_command;

/* Read and discard the data in the brigade produced by a CGI script */
static void discard_script_output(kuda_bucket_brigade *bb);

/* KLUDGE --- for back-combatibility, we don't have to check ExecCGI
 * in ScriptAliased directories, which means we need to know if this
 * request came through ScriptAlias or not... so the Alias cAPI
 * leaves a note for us.
 */

static int is_scriptaliased(request_rec *r)
{
    const char *t = kuda_table_get(r->notes, "alias-forced-type");
    return t && (!strcasecmp(t, "cgi-script"));
}

/* Configuration stuff */

#define DEFAULT_LOGBYTES 10385760
#define DEFAULT_BUFBYTES 1024

typedef struct {
    const char *logname;
    long        logbytes;
    kuda_size_t  bufbytes;
} cgi_server_conf;

static void *create_cgi_config(kuda_pool_t *p, server_rec *s)
{
    cgi_server_conf *c =
    (cgi_server_conf *) kuda_pcalloc(p, sizeof(cgi_server_conf));

    c->logname = NULL;
    c->logbytes = DEFAULT_LOGBYTES;
    c->bufbytes = DEFAULT_BUFBYTES;

    return c;
}

static void *merge_cgi_config(kuda_pool_t *p, void *basev, void *overridesv)
{
    cgi_server_conf *base = (cgi_server_conf *) basev,
                    *overrides = (cgi_server_conf *) overridesv;

    return overrides->logname ? overrides : base;
}

static const char *set_scriptlog(cmd_parms *cmd, void *dummy, const char *arg)
{
    server_rec *s = cmd->server;
    cgi_server_conf *conf = clhy_get_capi_config(s->capi_config,
                                                 &cgi_capi);

    conf->logname = clhy_server_root_relative(cmd->pool, arg);

    if (!conf->logname) {
        return kuda_pstrcat(cmd->pool, "Invalid ScriptLog path ",
                           arg, NULL);
    }

    return NULL;
}

static const char *set_scriptlog_length(cmd_parms *cmd, void *dummy,
                                        const char *arg)
{
    server_rec *s = cmd->server;
    cgi_server_conf *conf = clhy_get_capi_config(s->capi_config,
                                                 &cgi_capi);

    conf->logbytes = atol(arg);
    return NULL;
}

static const char *set_scriptlog_buffer(cmd_parms *cmd, void *dummy,
                                        const char *arg)
{
    server_rec *s = cmd->server;
    cgi_server_conf *conf = clhy_get_capi_config(s->capi_config,
                                                 &cgi_capi);

    conf->bufbytes = atoi(arg);
    return NULL;
}

static const command_rec cgi_cmds[] =
{
CLHY_INIT_TAKE1("ScriptLog", set_scriptlog, NULL, RSRC_CONF,
     "the name of a log for script debugging info"),
CLHY_INIT_TAKE1("ScriptLogLength", set_scriptlog_length, NULL, RSRC_CONF,
     "the maximum length (in bytes) of the script debug log"),
CLHY_INIT_TAKE1("ScriptLogBuffer", set_scriptlog_buffer, NULL, RSRC_CONF,
     "the maximum size (in bytes) to record of a POST request"),
    {NULL}
};

static int log_scripterror(request_rec *r, cgi_server_conf * conf, int ret,
                           kuda_status_t rv, char *logno, char *error)
{
    kuda_file_t *f = NULL;
    kuda_finfo_t finfo;
    char time_str[KUDA_CTIME_LEN];
    int log_flags = rv ? CLHYLOG_ERR : CLHYLOG_ERR;

    /* Intentional no CLHYLOGNO */
    /* Callee provides CLHYLOGNO in error text */
    clhy_log_rerror(CLHYLOG_MARK, log_flags, rv, r,
                  "%s%s: %s", logno ? logno : "", error, r->filename);

    /* XXX Very expensive mainline case! Open, then getfileinfo! */
    if (!conf->logname ||
        ((kuda_stat(&finfo, conf->logname,
                   KUDA_FINFO_SIZE, r->pool) == KUDA_SUCCESS) &&
         (finfo.size > conf->logbytes)) ||
        (kuda_file_open(&f, conf->logname,
                       KUDA_APPEND|KUDA_WRITE|KUDA_CREATE, KUDA_PLATFORM_DEFAULT,
                       r->pool) != KUDA_SUCCESS)) {
        return ret;
    }

    /* "%% [Wed Jun 19 10:53:21 1996] GET /cgi-bin/printenv HTTP/1.0" */
    kuda_ctime(time_str, kuda_time_now());
    kuda_file_printf(f, "%%%% [%s] %s %s%s%s %s\n", time_str, r->method, r->uri,
                    r->args ? "?" : "", r->args ? r->args : "", r->protocol);
    /* "%% 500 /usr/local/clhy/cgi-bin */
    kuda_file_printf(f, "%%%% %d %s\n", ret, r->filename);

    kuda_file_printf(f, "%%error\n%s\n", error);

    kuda_file_close(f);
    return ret;
}

/* Soak up stderr from a script and redirect it to the error log.
 */
static kuda_status_t log_script_err(request_rec *r, kuda_file_t *script_err)
{
    char argsbuffer[HUGE_STRING_LEN];
    char *newline;
    kuda_status_t rv;
    cgi_server_conf *conf = clhy_get_capi_config(r->server->capi_config, &cgi_capi);

    while ((rv = kuda_file_gets(argsbuffer, HUGE_STRING_LEN,
                               script_err)) == KUDA_SUCCESS) {
        newline = strchr(argsbuffer, '\n');
        if (newline) {
            *newline = '\0';
        }
        log_scripterror(r, conf, r->status, 0, CLHYLOGNO(01215), argsbuffer);
    }

    return rv;
}

static int log_script(request_rec *r, cgi_server_conf * conf, int ret,
                      char *dbuf, const char *sbuf, kuda_bucket_brigade *bb,
                      kuda_file_t *script_err)
{
    const kuda_array_header_t *hdrs_arr = kuda_table_elts(r->headers_in);
    const kuda_table_entry_t *hdrs = (const kuda_table_entry_t *) hdrs_arr->elts;
    char argsbuffer[HUGE_STRING_LEN];
    kuda_file_t *f = NULL;
    kuda_bucket *e;
    const char *buf;
    kuda_size_t len;
    kuda_status_t rv;
    int first;
    int i;
    kuda_finfo_t finfo;
    char time_str[KUDA_CTIME_LEN];

    /* XXX Very expensive mainline case! Open, then getfileinfo! */
    if (!conf->logname ||
        ((kuda_stat(&finfo, conf->logname,
                   KUDA_FINFO_SIZE, r->pool) == KUDA_SUCCESS) &&
         (finfo.size > conf->logbytes)) ||
        (kuda_file_open(&f, conf->logname,
                       KUDA_APPEND|KUDA_WRITE|KUDA_CREATE, KUDA_PLATFORM_DEFAULT,
                       r->pool) != KUDA_SUCCESS)) {
        /* Soak up script output */
        discard_script_output(bb);
        log_script_err(r, script_err);
        return ret;
    }

    /* "%% [Wed Jun 19 10:53:21 1996] GET /cgi-bin/printenv HTTP/1.0" */
    kuda_ctime(time_str, kuda_time_now());
    kuda_file_printf(f, "%%%% [%s] %s %s%s%s %s\n", time_str, r->method, r->uri,
                    r->args ? "?" : "", r->args ? r->args : "", r->protocol);
    /* "%% 500 /usr/local/clhy/cgi-bin" */
    kuda_file_printf(f, "%%%% %d %s\n", ret, r->filename);

    kuda_file_puts("%request\n", f);
    for (i = 0; i < hdrs_arr->nelts; ++i) {
        if (!hdrs[i].key)
            continue;
        kuda_file_printf(f, "%s: %s\n", hdrs[i].key, hdrs[i].val);
    }
    if ((r->method_number == M_POST || r->method_number == M_PUT) &&
        *dbuf) {
        kuda_file_printf(f, "\n%s\n", dbuf);
    }

    kuda_file_puts("%response\n", f);
    hdrs_arr = kuda_table_elts(r->err_headers_out);
    hdrs = (const kuda_table_entry_t *) hdrs_arr->elts;

    for (i = 0; i < hdrs_arr->nelts; ++i) {
        if (!hdrs[i].key)
            continue;
        kuda_file_printf(f, "%s: %s\n", hdrs[i].key, hdrs[i].val);
    }

    if (sbuf && *sbuf)
        kuda_file_printf(f, "%s\n", sbuf);

    first = 1;
    for (e = KUDA_BRIGADE_FIRST(bb);
         e != KUDA_BRIGADE_SENTINEL(bb);
         e = KUDA_BUCKET_NEXT(e))
    {
        if (KUDA_BUCKET_IS_EOS(e)) {
            break;
        }
        rv = kuda_bucket_read(e, &buf, &len, KUDA_BLOCK_READ);
        if (rv != KUDA_SUCCESS || (len == 0)) {
            break;
        }
        if (first) {
            kuda_file_puts("%stdout\n", f);
            first = 0;
        }
        kuda_file_write(f, buf, &len);
        kuda_file_puts("\n", f);
    }

    if (kuda_file_gets(argsbuffer, HUGE_STRING_LEN, script_err) == KUDA_SUCCESS) {
        kuda_file_puts("%stderr\n", f);
        kuda_file_puts(argsbuffer, f);
        while (kuda_file_gets(argsbuffer, HUGE_STRING_LEN,
                             script_err) == KUDA_SUCCESS) {
            kuda_file_puts(argsbuffer, f);
        }
        kuda_file_puts("\n", f);
    }

    kuda_brigade_destroy(bb);
    kuda_file_close(script_err);

    kuda_file_close(f);
    return ret;
}


/* This is the special environment used for running the "exec cmd="
 *   variety of SSI directives.
 */
static void add_ssi_vars(request_rec *r)
{
    kuda_table_t *e = r->subprocess_env;

    if (r->path_info && r->path_info[0] != '\0') {
        request_rec *pa_req;

        kuda_table_setn(e, "PATH_INFO", clhy_escape_shell_cmd(r->pool,
                                                           r->path_info));

        pa_req = clhy_sub_req_lookup_uri(clhy_escape_uri(r->pool, r->path_info),
                                       r, NULL);
        if (pa_req->filename) {
            kuda_table_setn(e, "PATH_TRANSLATED",
                           kuda_pstrcat(r->pool, pa_req->filename,
                                       pa_req->path_info, NULL));
        }
        clhy_destroy_sub_req(pa_req);
    }

    if (r->args) {
        char *arg_copy = kuda_pstrdup(r->pool, r->args);

        kuda_table_setn(e, "QUERY_STRING", r->args);
        clhy_unescape_url(arg_copy);
        kuda_table_setn(e, "QUERY_STRING_UNESCAPED",
                       clhy_escape_shell_cmd(r->pool, arg_copy));
    }
}

static void cgi_child_errfn(kuda_pool_t *pool, kuda_status_t err,
                            const char *description)
{
    kuda_file_t *stderr_log;

    kuda_file_open_stderr(&stderr_log, pool);
    /* Escape the logged string because it may be something that
     * came in over the network.
     */
    kuda_file_printf(stderr_log,
                    "(%d)%pm: %s\n",
                    err,
                    &err,
#ifndef CLHY_UNSAFE_ERROR_LOG_UNESCAPED
                    clhy_escape_logitem(pool,
#endif
                    description
#ifndef CLHY_UNSAFE_ERROR_LOG_UNESCAPED
                    )
#endif
                    );
}

static kuda_status_t run_cgi_child(kuda_file_t **script_out,
                                  kuda_file_t **script_in,
                                  kuda_file_t **script_err,
                                  const char *command,
                                  const char * const argv[],
                                  request_rec *r,
                                  kuda_pool_t *p,
                                  cgi_exec_info_t *e_info)
{
    const char * const *env;
    kuda_procattr_t *procattr;
    kuda_proc_t *procnew;
    kuda_status_t rc = KUDA_SUCCESS;

#ifdef CLHY_CGI_USE_RLIMIT
    core_dir_config *conf = clhy_get_core_capi_config(r->per_dir_config);
#endif

#ifdef DEBUG_CGI
#ifdef OS2
    /* Under OS2 need to use device con. */
    FILE *dbg = fopen("con", "w");
#else
    FILE *dbg = fopen("/dev/tty", "w");
#endif
    int i;
#endif

    RAISE_SIGSTOP(CGI_CHILD);
#ifdef DEBUG_CGI
    fprintf(dbg, "Attempting to exec %s as CGI child (argv0 = %s)\n",
            r->filename, argv[0]);
#endif

    env = (const char * const *)clhy_create_environment(p, r->subprocess_env);

#ifdef DEBUG_CGI
    fprintf(dbg, "Environment: \n");
    for (i = 0; env[i]; ++i)
        fprintf(dbg, "'%s'\n", env[i]);
    fclose(dbg);
#endif

    /* Transmute ourselves into the script.
     * NB only ISINDEX scripts get decoded arguments.
     */
    if (((rc = kuda_procattr_create(&procattr, p)) != KUDA_SUCCESS) ||
        ((rc = kuda_procattr_io_set(procattr,
                                   e_info->in_pipe,
                                   e_info->out_pipe,
                                   e_info->err_pipe)) != KUDA_SUCCESS) ||
        ((rc = kuda_procattr_dir_set(procattr,
                        clhy_make_dirstr_parent(r->pool,
                                              r->filename))) != KUDA_SUCCESS) ||
#if defined(RLIMIT_CPU) && defined(CLHY_CGI_USE_RLIMIT)
        ((rc = kuda_procattr_limit_set(procattr, KUDA_LIMIT_CPU,
                                      conf->limit_cpu)) != KUDA_SUCCESS) ||
#endif
#if defined(CLHY_CGI_USE_RLIMIT) && (defined(RLIMIT_DATA) || defined(RLIMIT_VMEM) || defined(RLIMIT_AS))
        ((rc = kuda_procattr_limit_set(procattr, KUDA_LIMIT_MEM,
                                      conf->limit_mem)) != KUDA_SUCCESS) ||
#endif
#if defined(RLIMIT_NPROC) && defined(CLHY_CGI_USE_RLIMIT)
        ((rc = kuda_procattr_limit_set(procattr, KUDA_LIMIT_NPROC,
                                      conf->limit_nproc)) != KUDA_SUCCESS) ||
#endif
        ((rc = kuda_procattr_cmdtype_set(procattr,
                                        e_info->cmd_type)) != KUDA_SUCCESS) ||

        ((rc = kuda_procattr_detach_set(procattr,
                                        e_info->detached)) != KUDA_SUCCESS) ||
        ((rc = kuda_procattr_addrspace_set(procattr,
                                        e_info->addrspace)) != KUDA_SUCCESS) ||
        ((rc = kuda_procattr_child_errfn_set(procattr, cgi_child_errfn)) != KUDA_SUCCESS)) {
        /* Something bad happened, tell the world. */
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rc, r, CLHYLOGNO(01216)
                      "couldn't set child process attributes: %s", r->filename);
    }
    else {
        procnew = kuda_pcalloc(p, sizeof(*procnew));
        rc = clhy_platform_create_privileged_process(r, procnew, command, argv, env,
                                             procattr, p);

        if (rc != KUDA_SUCCESS) {
            /* Bad things happened. Everyone should have cleaned up. */
            /* Intentional no CLHYLOGNO */
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR|CLHYLOG_TOCLIENT, rc, r,
                          "couldn't create child process: %d: %s", rc,
                          kuda_filepath_name_get(r->filename));
        }
        else {
            kuda_pool_note_subprocess(p, procnew, KUDA_KILL_AFTER_TIMEOUT);

            *script_in = procnew->out;
            if (!*script_in)
                return KUDA_EBADF;
            kuda_file_pipe_timeout_set(*script_in, r->server->timeout);

            if (e_info->prog_type == RUN_AS_CGI) {
                *script_out = procnew->in;
                if (!*script_out)
                    return KUDA_EBADF;
                kuda_file_pipe_timeout_set(*script_out, r->server->timeout);

                *script_err = procnew->err;
                if (!*script_err)
                    return KUDA_EBADF;
                kuda_file_pipe_timeout_set(*script_err, r->server->timeout);
            }
        }
    }
    return (rc);
}


static kuda_status_t default_build_command(const char **cmd, const char ***argv,
                                          request_rec *r, kuda_pool_t *p,
                                          cgi_exec_info_t *e_info)
{
    int numwords, x, idx;
    char *w;
    const char *args = NULL;

    if (e_info->process_cgi) {
        *cmd = r->filename;
        /* Do not process r->args if they contain an '=' assignment
         */
        if (r->args && r->args[0] && !clhy_strchr_c(r->args, '=')) {
            args = r->args;
        }
    }

    if (!args) {
        numwords = 1;
    }
    else {
        /* count the number of keywords */
        for (x = 0, numwords = 2; args[x]; x++) {
            if (args[x] == '+') {
                ++numwords;
            }
        }
    }
    /* Everything is - 1 to account for the first parameter
     * which is the program name.
     */
    if (numwords > CLHYKUDEL_ARG_MAX - 1) {
        numwords = CLHYKUDEL_ARG_MAX - 1;    /* Truncate args to prevent overrun */
    }
    *argv = kuda_palloc(p, (numwords + 2) * sizeof(char *));
    (*argv)[0] = *cmd;
    for (x = 1, idx = 1; x < numwords; x++) {
        w = clhy_getword_nulls(p, &args, '+');
        clhy_unescape_url(w);
        (*argv)[idx++] = clhy_escape_shell_cmd(p, w);
    }
    (*argv)[idx] = NULL;

    return KUDA_SUCCESS;
}

static void discard_script_output(kuda_bucket_brigade *bb)
{
    kuda_bucket *e;
    const char *buf;
    kuda_size_t len;
    kuda_status_t rv;

    for (e = KUDA_BRIGADE_FIRST(bb);
         e != KUDA_BRIGADE_SENTINEL(bb);
         e = KUDA_BUCKET_NEXT(e))
    {
        if (KUDA_BUCKET_IS_EOS(e)) {
            break;
        }
        rv = kuda_bucket_read(e, &buf, &len, KUDA_BLOCK_READ);
        if (rv != KUDA_SUCCESS) {
            break;
        }
    }
}

#if KUDA_FILES_AS_SOCKETS

/* A CGI bucket type is needed to catch any output to stderr from the
 * script; see PR 22030. */
static const kuda_bucket_type_t bucket_type_cgi;

struct cgi_bucket_data {
    kuda_pollset_t *pollset;
    request_rec *r;
};

/* Create a CGI bucket using pipes from script stdout 'out'
 * and stderr 'err', for request 'r'. */
static kuda_bucket *cgi_bucket_create(request_rec *r,
                                     kuda_file_t *out, kuda_file_t *err,
                                     kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);
    kuda_status_t rv;
    kuda_pollfd_t fd;
    struct cgi_bucket_data *data = kuda_palloc(r->pool, sizeof *data);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    b->type = &bucket_type_cgi;
    b->length = (kuda_size_t)(-1);
    b->start = -1;

    /* Create the pollset */
    rv = kuda_pollset_create(&data->pollset, 2, r->pool, 0);
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01217)
                     "kuda_pollset_create(); check system or user limits");
        return NULL;
    }

    fd.desc_type = KUDA_POLL_FILE;
    fd.reqevents = KUDA_POLLIN;
    fd.p = r->pool;
    fd.desc.f = out; /* script's stdout */
    fd.client_data = (void *)1;
    rv = kuda_pollset_add(data->pollset, &fd);
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01218)
                     "kuda_pollset_add(); check system or user limits");
        return NULL;
    }

    fd.desc.f = err; /* script's stderr */
    fd.client_data = (void *)2;
    rv = kuda_pollset_add(data->pollset, &fd);
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01219)
                     "kuda_pollset_add(); check system or user limits");
        return NULL;
    }

    data->r = r;
    b->data = data;
    return b;
}

/* Create a duplicate CGI bucket using given bucket data */
static kuda_bucket *cgi_bucket_dup(struct cgi_bucket_data *data,
                                  kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);
    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    b->type = &bucket_type_cgi;
    b->length = (kuda_size_t)(-1);
    b->start = -1;
    b->data = data;
    return b;
}

/* Handle stdout from CGI child.  Duplicate of logic from the _read
 * method of the real KUDA pipe bucket implementation. */
static kuda_status_t cgi_read_stdout(kuda_bucket *a, kuda_file_t *out,
                                    const char **str, kuda_size_t *len)
{
    char *buf;
    kuda_status_t rv;

    *str = NULL;
    *len = KUDA_BUCKET_BUFF_SIZE;
    buf = kuda_bucket_alloc(*len, a->list); /* XXX: check for failure? */

    rv = kuda_file_read(out, buf, len);

    if (rv != KUDA_SUCCESS && rv != KUDA_EOF) {
        kuda_bucket_free(buf);
        return rv;
    }

    if (*len > 0) {
        struct cgi_bucket_data *data = a->data;
        kuda_bucket_heap *h;

        /* Change the current bucket to refer to what we read */
        a = kuda_bucket_heap_make(a, buf, *len, kuda_bucket_free);
        h = a->data;
        h->alloc_len = KUDA_BUCKET_BUFF_SIZE; /* note the real buffer size */
        *str = buf;
        KUDA_BUCKET_INSERT_AFTER(a, cgi_bucket_dup(data, a->list));
    }
    else {
        kuda_bucket_free(buf);
        a = kuda_bucket_immortal_make(a, "", 0);
        *str = a->data;
    }
    return rv;
}

/* Read method of CGI bucket: polls on stderr and stdout of the child,
 * sending any stderr output immediately away to the error log. */
static kuda_status_t cgi_bucket_read(kuda_bucket *b, const char **str,
                                    kuda_size_t *len, kuda_read_type_e block)
{
    struct cgi_bucket_data *data = b->data;
    kuda_interval_time_t timeout;
    kuda_status_t rv;
    int gotdata = 0;

    timeout = block == KUDA_NONBLOCK_READ ? 0 : data->r->server->timeout;

    do {
        const kuda_pollfd_t *results;
        kuda_int32_t num;

        rv = kuda_pollset_poll(data->pollset, timeout, &num, &results);
        if (KUDA_STATUS_IS_TIMEUP(rv)) {
            if (timeout) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, data->r, CLHYLOGNO(01220)
                              "Timeout waiting for output from CGI script %s",
                              data->r->filename);
                return rv;
            }
            else {
                return KUDA_EAGAIN;
            }
        }
        else if (KUDA_STATUS_IS_EINTR(rv)) {
            continue;
        }
        else if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, data->r, CLHYLOGNO(01221)
                          "poll failed waiting for CGI child");
            return rv;
        }

        for (; num; num--, results++) {
            if (results[0].client_data == (void *)1) {
                /* stdout */
                rv = cgi_read_stdout(b, results[0].desc.f, str, len);
                if (KUDA_STATUS_IS_EOF(rv)) {
                    rv = KUDA_SUCCESS;
                }
                gotdata = 1;
            } else {
                /* stderr */
                kuda_status_t rv2 = log_script_err(data->r, results[0].desc.f);
                if (KUDA_STATUS_IS_EOF(rv2)) {
                    kuda_pollset_remove(data->pollset, &results[0]);
                }
            }
        }

    } while (!gotdata);

    return rv;
}

static const kuda_bucket_type_t bucket_type_cgi = {
    "CGI", 5, KUDA_BUCKET_DATA,
    kuda_bucket_destroy_noop,
    cgi_bucket_read,
    kuda_bucket_setaside_notimpl,
    kuda_bucket_split_notimpl,
    kuda_bucket_copy_notimpl
};

#endif

static int cgi_handler(request_rec *r)
{
    int nph;
    kuda_size_t dbpos = 0;
    const char *argv0;
    const char *command;
    const char **argv;
    char *dbuf = NULL;
    kuda_file_t *script_out = NULL, *script_in = NULL, *script_err = NULL;
    kuda_bucket_brigade *bb;
    kuda_bucket *b;
    int is_included;
    int seen_eos, child_stopped_reading;
    kuda_pool_t *p;
    cgi_server_conf *conf;
    kuda_status_t rv;
    cgi_exec_info_t e_info;
    conn_rec *c;

    if (strcmp(r->handler, CGI_MAGIC_TYPE) && strcmp(r->handler, "cgi-script")) {
        return DECLINED;
    }

    c = r->connection;

    is_included = !strcmp(r->protocol, "INCLUDED");

    p = r->main ? r->main->pool : r->pool;

    argv0 = kuda_filepath_name_get(r->filename);
    nph = !(strncmp(argv0, "nph-", 4));
    conf = clhy_get_capi_config(r->server->capi_config, &cgi_capi);

    if (!(clhy_allow_options(r) & OPT_EXECCGI) && !is_scriptaliased(r))
        return log_scripterror(r, conf, HTTP_FORBIDDEN, 0, CLHYLOGNO(02809),
                               "Options ExecCGI is off in this directory");
    if (nph && is_included)
        return log_scripterror(r, conf, HTTP_FORBIDDEN, 0, CLHYLOGNO(02810),
                               "attempt to include NPH CGI script");

    if (r->finfo.filetype == KUDA_NOFILE)
        return log_scripterror(r, conf, HTTP_NOT_FOUND, 0, CLHYLOGNO(02811),
                               "script not found or unable to stat");
    if (r->finfo.filetype == KUDA_DIR)
        return log_scripterror(r, conf, HTTP_FORBIDDEN, 0, CLHYLOGNO(02812),
                               "attempt to invoke directory as script");

    if ((r->used_path_info == CLHY_REQ_REJECT_PATH_INFO) &&
        r->path_info && *r->path_info)
    {
        /* default to accept */
        return log_scripterror(r, conf, HTTP_NOT_FOUND, 0, CLHYLOGNO(02813),
                               "AcceptPathInfo off disallows user's path");
    }
/*
    if (!clhy_suexec_enabled) {
        if (!clhy_can_exec(&r->finfo))
            return log_scripterror(r, conf, HTTP_FORBIDDEN, 0, CLHYLOGNO(03194)
                                   "file permissions deny server execution");
    }

*/
    clhy_add_common_vars(r);
    clhy_add_cgi_vars(r);

    e_info.process_cgi = 1;
    e_info.cmd_type    = KUDA_PROGRAM;
    e_info.detached    = 0;
    e_info.in_pipe     = KUDA_CHILD_BLOCK;
    e_info.out_pipe    = KUDA_CHILD_BLOCK;
    e_info.err_pipe    = KUDA_CHILD_BLOCK;
    e_info.prog_type   = RUN_AS_CGI;
    e_info.bb          = NULL;
    e_info.ctx         = NULL;
    e_info.next        = NULL;
    e_info.addrspace   = 0;

    /* build the command line */
    if ((rv = cgi_build_command(&command, &argv, r, p, &e_info)) != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01222)
                      "don't know how to spawn child process: %s",
                      r->filename);
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    /* run the script in its own process */
    if ((rv = run_cgi_child(&script_out, &script_in, &script_err,
                            command, argv, r, p, &e_info)) != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01223)
                      "couldn't spawn child process: %s", r->filename);
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    /* Transfer any put/post args, CERN style...
     * Note that we already ignore SIGPIPE in the core server.
     */
    bb = kuda_brigade_create(r->pool, c->bucket_alloc);
    seen_eos = 0;
    child_stopped_reading = 0;
    if (conf->logname) {
        dbuf = kuda_palloc(r->pool, conf->bufbytes + 1);
        dbpos = 0;
    }
    do {
        kuda_bucket *bucket;

        rv = clhy_get_brigade(r->input_filters, bb, CLHY_MODE_READBYTES,
                            KUDA_BLOCK_READ, HUGE_STRING_LEN);

        if (rv != KUDA_SUCCESS) {
            if (KUDA_STATUS_IS_TIMEUP(rv)) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01224)
                              "Timeout during reading request entity data");
                return HTTP_REQUEST_TIME_OUT;
            }
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01225)
                          "Error reading request entity data");
            return clhy_map_http_request_error(rv, HTTP_BAD_REQUEST);
        }

        for (bucket = KUDA_BRIGADE_FIRST(bb);
             bucket != KUDA_BRIGADE_SENTINEL(bb);
             bucket = KUDA_BUCKET_NEXT(bucket))
        {
            const char *data;
            kuda_size_t len;

            if (KUDA_BUCKET_IS_EOS(bucket)) {
                seen_eos = 1;
                break;
            }

            /* We can't do much with this. */
            if (KUDA_BUCKET_IS_FLUSH(bucket)) {
                continue;
            }

            /* If the child stopped, we still must read to EOS. */
            if (child_stopped_reading) {
                continue;
            }

            /* read */
            kuda_bucket_read(bucket, &data, &len, KUDA_BLOCK_READ);

            if (conf->logname && dbpos < conf->bufbytes) {
                int cursize;

                if ((dbpos + len) > conf->bufbytes) {
                    cursize = conf->bufbytes - dbpos;
                }
                else {
                    cursize = len;
                }
                memcpy(dbuf + dbpos, data, cursize);
                dbpos += cursize;
            }

            /* Keep writing data to the child until done or too much time
             * elapses with no progress or an error occurs.
             */
            rv = kuda_file_write_full(script_out, data, len, NULL);

            if (rv != KUDA_SUCCESS) {
                /* silly script stopped reading, soak up remaining message */
                child_stopped_reading = 1;
            }
        }
        kuda_brigade_cleanup(bb);
    }
    while (!seen_eos);

    if (conf->logname) {
        dbuf[dbpos] = '\0';
    }
    /* Is this flush really needed? */
    kuda_file_flush(script_out);
    kuda_file_close(script_out);

    CLHY_DEBUG_ASSERT(script_in != NULL);

#if KUDA_FILES_AS_SOCKETS
    kuda_file_pipe_timeout_set(script_in, 0);
    kuda_file_pipe_timeout_set(script_err, 0);

    b = cgi_bucket_create(r, script_in, script_err, c->bucket_alloc);
    if (b == NULL)
        return HTTP_INTERNAL_SERVER_ERROR;
#else
    b = kuda_bucket_pipe_create(script_in, c->bucket_alloc);
#endif
    KUDA_BRIGADE_INSERT_TAIL(bb, b);
    b = kuda_bucket_eos_create(c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bb, b);

    /* Handle script return... */
    if (!nph) {
        const char *location;
        char sbuf[MAX_STRING_LEN];
        int ret;

        if ((ret = clhy_scan_script_header_err_brigade_ex(r, bb, sbuf,
                                                        CLHYLOG_CAPI_INDEX)))
        {
            ret = log_script(r, conf, ret, dbuf, sbuf, bb, script_err);

            /*
             * ret could be HTTP_NOT_MODIFIED in the case that the CGI script
             * does not set an explicit status and clhy_meets_conditions, which
             * is called by clhy_scan_script_header_err_brigade, detects that
             * the conditions of the requests are met and the response is
             * not modified.
             * In this case set r->status and return OK in order to prevent
             * running through the error processing stack as this would
             * break with capi_cache, if the conditions had been set by
             * capi_cache itself to validate a stale entity.
             * BTW: We circumvent the error processing stack anyway if the
             * CGI script set an explicit status code (whatever it is) and
             * the only possible values for ret here are:
             *
             * HTTP_NOT_MODIFIED          (set by clhy_meets_conditions)
             * HTTP_PRECONDITION_FAILED   (set by clhy_meets_conditions)
             * HTTP_INTERNAL_SERVER_ERROR (if something went wrong during the
             * processing of the response of the CGI script, e.g broken headers
             * or a crashed CGI process).
             */
            if (ret == HTTP_NOT_MODIFIED) {
                r->status = ret;
                return OK;
            }

            return ret;
        }

        location = kuda_table_get(r->headers_out, "Location");

        if (location && r->status == 200) {
            /* For a redirect whether internal or not, discard any
             * remaining stdout from the script, and log any remaining
             * stderr output, as normal. */
            discard_script_output(bb);
            kuda_brigade_destroy(bb);
            kuda_file_pipe_timeout_set(script_err, r->server->timeout);
            log_script_err(r, script_err);
        }

        if (location && location[0] == '/' && r->status == 200) {
            /* This redirect needs to be a GET no matter what the original
             * method was.
             */
            r->method = "GET";
            r->method_number = M_GET;

            /* We already read the message body (if any), so don't allow
             * the redirected request to think it has one.  We can ignore
             * Transfer-Encoding, since we used REQUEST_CHUNKED_ERROR.
             */
            kuda_table_unset(r->headers_in, "Content-Length");

            clhy_internal_redirect_handler(location, r);
            return OK;
        }
        else if (location && r->status == 200) {
            /* XXX: Note that if a script wants to produce its own Redirect
             * body, it now has to explicitly *say* "Status: 302"
             */
            return HTTP_MOVED_TEMPORARILY;
        }

        rv = clhy_pass_brigade(r->output_filters, bb);
    }
    else /* nph */ {
        struct clhy_filter_t *cur;

        /* get rid of all filters up through protocol...  since we
         * haven't parsed off the headers, there is no way they can
         * work
         */

        cur = r->proto_output_filters;
        while (cur && cur->frec->ftype < CLHY_FTYPE_CONNECTION) {
            cur = cur->next;
        }
        r->output_filters = r->proto_output_filters = cur;

        rv = clhy_pass_brigade(r->output_filters, bb);
    }

    /* don't soak up script output if errors occurred writing it
     * out...  otherwise, we prolong the life of the script when the
     * connection drops or we stopped sending output for some other
     * reason */
    if (rv == KUDA_SUCCESS && !r->connection->aborted) {
        kuda_file_pipe_timeout_set(script_err, r->server->timeout);
        log_script_err(r, script_err);
    }

    kuda_file_close(script_err);

    return OK;                      /* NOT r->status, even if it has changed. */
}

/*============================================================================
 *============================================================================
 * This is the beginning of the cgi filter code moved from capi_include. This
 *   is the code required to handle the "exec" SSI directive.
 *============================================================================
 *============================================================================*/
static kuda_status_t include_cgi(include_ctx_t *ctx, clhy_filter_t *f,
                                kuda_bucket_brigade *bb, char *s)
{
    request_rec *r = f->r;
    request_rec *rr = clhy_sub_req_lookup_uri(s, r, f->next);
    int rr_status;

    if (rr->status != HTTP_OK) {
        clhy_destroy_sub_req(rr);
        return KUDA_EGENERAL;
    }

    /* No hardwired path info or query allowed */
    if ((rr->path_info && rr->path_info[0]) || rr->args) {
        clhy_destroy_sub_req(rr);
        return KUDA_EGENERAL;
    }
    if (rr->finfo.filetype != KUDA_REG) {
        clhy_destroy_sub_req(rr);
        return KUDA_EGENERAL;
    }

    /* Script gets parameters of the *document*, for back compatibility */
    rr->path_info = r->path_info;       /* hard to get right; see capi_cgi.c */
    rr->args = r->args;

    /* Force sub_req to be treated as a CGI request, even if ordinary
     * typing rules would have called it something else.
     */
    clhy_set_content_type(rr, CGI_MAGIC_TYPE);

    /* Run it. */
    rr_status = clhy_run_sub_req(rr);
    if (clhy_is_HTTP_REDIRECT(rr_status)) {
        const char *location = kuda_table_get(rr->headers_out, "Location");

        if (location) {
            char *buffer;

            location = clhy_escape_html(rr->pool, location);
            buffer = kuda_pstrcat(ctx->pool, "<a href=\"", location, "\">",
                                 location, "</a>", NULL);

            KUDA_BRIGADE_INSERT_TAIL(bb, kuda_bucket_pool_create(buffer,
                                    strlen(buffer), ctx->pool,
                                    f->c->bucket_alloc));
        }
    }

    clhy_destroy_sub_req(rr);

    return KUDA_SUCCESS;
}

static kuda_status_t include_cmd(include_ctx_t *ctx, clhy_filter_t *f,
                                kuda_bucket_brigade *bb, const char *command)
{
    cgi_exec_info_t  e_info;
    const char **argv;
    kuda_file_t *script_out = NULL, *script_in = NULL, *script_err = NULL;
    kuda_status_t rv;
    request_rec *r = f->r;

    add_ssi_vars(r);

    e_info.process_cgi = 0;
    e_info.cmd_type    = KUDA_SHELLCMD;
    e_info.detached    = 0;
    e_info.in_pipe     = KUDA_NO_PIPE;
    e_info.out_pipe    = KUDA_FULL_BLOCK;
    e_info.err_pipe    = KUDA_NO_PIPE;
    e_info.prog_type   = RUN_AS_SSI;
    e_info.bb          = &bb;
    e_info.ctx         = ctx;
    e_info.next        = f->next;
    e_info.addrspace   = 0;

    if ((rv = cgi_build_command(&command, &argv, r, r->pool,
                                &e_info)) != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01226)
                      "don't know how to spawn cmd child process: %s",
                      r->filename);
        return rv;
    }

    /* run the script in its own process */
    if ((rv = run_cgi_child(&script_out, &script_in, &script_err,
                            command, argv, r, r->pool,
                            &e_info)) != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01227)
                      "couldn't spawn child process: %s", r->filename);
        return rv;
    }

    KUDA_BRIGADE_INSERT_TAIL(bb, kuda_bucket_pipe_create(script_in,
                            f->c->bucket_alloc));
    ctx->flush_now = 1;

    /* We can't close the pipe here, because we may return before the
     * full CGI has been sent to the network.  That's okay though,
     * because we can rely on the pool to close the pipe for us.
     */
    return KUDA_SUCCESS;
}

static kuda_status_t handle_exec(include_ctx_t *ctx, clhy_filter_t *f,
                                kuda_bucket_brigade *bb)
{
    char *tag = NULL;
    char *tag_val = NULL;
    request_rec *r = f->r;
    char *file = r->filename;
    char parsed_string[MAX_STRING_LEN];

    if (!ctx->argc) {
        clhy_log_rerror(CLHYLOG_MARK,
                      (ctx->flags & SSI_FLAG_PRINTING)
                          ? CLHYLOG_ERR : CLHYLOG_WARNING,
                      0, r, CLHYLOGNO(03195)
                      "missing argument for exec element in %s", r->filename);
    }

    if (!(ctx->flags & SSI_FLAG_PRINTING)) {
        return KUDA_SUCCESS;
    }

    if (!ctx->argc) {
        SSI_CREATE_ERROR_BUCKET(ctx, f, bb);
        return KUDA_SUCCESS;
    }

    if (ctx->flags & SSI_FLAG_NO_EXEC) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01228) "exec used but not allowed "
                      "in %s", r->filename);
        SSI_CREATE_ERROR_BUCKET(ctx, f, bb);
        return KUDA_SUCCESS;
    }

    while (1) {
        cgi_pfn_gtv(ctx, &tag, &tag_val, SSI_VALUE_DECODED);
        if (!tag || !tag_val) {
            break;
        }

        if (!strcmp(tag, "cmd")) {
            kuda_status_t rv;

            cgi_pfn_ps(ctx, tag_val, parsed_string, sizeof(parsed_string),
                       SSI_EXPAND_LEAVE_NAME);

            rv = include_cmd(ctx, f, bb, parsed_string);
            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01229) "execution failure "
                              "for parameter \"%s\" to tag exec in file %s",
                              tag, r->filename);
                SSI_CREATE_ERROR_BUCKET(ctx, f, bb);
                break;
            }
        }
        else if (!strcmp(tag, "cgi")) {
            kuda_status_t rv;

            cgi_pfn_ps(ctx, tag_val, parsed_string, sizeof(parsed_string),
                       SSI_EXPAND_DROP_NAME);

            rv = include_cgi(ctx, f, bb, parsed_string);
            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01230) "invalid CGI ref "
                              "\"%s\" in %s", tag_val, file);
                SSI_CREATE_ERROR_BUCKET(ctx, f, bb);
                break;
            }
        }
        else {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01231) "unknown parameter "
                          "\"%s\" to tag exec in %s", tag, file);
            SSI_CREATE_ERROR_BUCKET(ctx, f, bb);
            break;
        }
    }

    return KUDA_SUCCESS;
}


/*============================================================================
 *============================================================================
 * This is the end of the cgi filter code moved from capi_include.
 *============================================================================
 *============================================================================*/


static int cgi_post_config(kuda_pool_t *p, kuda_pool_t *plog,
                                kuda_pool_t *ptemp, server_rec *s)
{
    cgi_pfn_reg_with_ssi = KUDA_RETRIEVE_OPTIONAL_FN(clhy_register_include_handler);
    cgi_pfn_gtv          = KUDA_RETRIEVE_OPTIONAL_FN(clhy_ssi_get_tag_and_value);
    cgi_pfn_ps           = KUDA_RETRIEVE_OPTIONAL_FN(clhy_ssi_parse_string);

    if ((cgi_pfn_reg_with_ssi) && (cgi_pfn_gtv) && (cgi_pfn_ps)) {
        /* Required by capi_include filter. This is how capi_cgi registers
         *   with capi_include to provide processing of the exec directive.
         */
        cgi_pfn_reg_with_ssi("exec", handle_exec);
    }

    /* This is the means by which unusual (non-unix) platforms's may find alternate
     * means to run a given command (e.g. shebang/registry parsing on Win32)
     */
    cgi_build_command    = KUDA_RETRIEVE_OPTIONAL_FN(clhy_cgi_build_command);
    if (!cgi_build_command) {
        cgi_build_command = default_build_command;
    }
    return OK;
}

static void register_hooks(kuda_pool_t *p)
{
    static const char * const aszPre[] = { "capi_include.c", NULL };
    clhy_hook_handler(cgi_handler, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_post_config(cgi_post_config, aszPre, NULL, KUDA_HOOK_REALLY_FIRST);
}

CLHY_DECLARE_CAPI(cgi) =
{
    STANDARD16_CAPI_STUFF,
    NULL,                        /* dir config creater */
    NULL,                        /* dir merger --- default is to override */
    create_cgi_config,           /* server config */
    merge_cgi_config,            /* merge server config */
    cgi_cmds,                    /* command kuda_table_t */
    register_hooks               /* register hooks */
};
