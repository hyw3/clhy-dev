/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This cAPI is used to load cLHy cAPIs at runtime. This means that the
 * server functionality can be extended without recompiling and even without
 * taking the server down at all. Only a HUP or CLHY_SIG_GRACEFUL signal
 * needs to be sent to the server to reload the dynamically loaded cAPIs.
 *
 * To use, you'll first need to build your cAPI as a shared library, then
 * update your configuration (wwhy.conf) to get the cLHy core to load the
 * cAPI at start-up.
 *
 * The easiest way to build a cAPI as a shared library is to use the
 * `SharedcAPI' command in the Configuration file, instead of `AddcAPI'.
 * You should also change the file extension from `.o' to `.so'. So, for
 * example, to build the status cAPI as a shared library edit Configuration
 * and change
 *   AddcAPI    cAPIs/standard/capi_status.o
 * to
 *   SharedcAPI cAPIs/standard/capi_status.so
 *
 * Run Configure and make. Now cLHy's wwhy binary will _not_ include
 * capi_status. Instead a shared object called capi_status.so will be build, in
 * the cAPIs/standard directory. You can build most of the cAPIs as shared
 * libraries like this.
 *
 * To use the shared cAPI, move the .so file(s) into an appropriate
 * directory. You might like to create a directory called "cAPIs" under you
 * server root for this (e.g. /usr/local/wwhy/cAPIs).
 *
 * Then edit your conf/wwhy.conf file, and add ActivatecAPI lines. For
 * example
 *   ActivatecAPI  status_capi   cAPIs/capi_status.so
 *
 * The first argument is the cAPI's structure name (look at the end of the
 * cAPI source to find this). The second option is the path to the cAPI
 * file, relative to the server root.  Put these directives right at the top
 * of your wwhy.conf file.
 *
 * Now you can start cLHy. A message will be logged at "debug" level to your
 * error_log to confirm that the cAPI(s) are loaded (use "LogLevel debug"
 * directive to get these log messages).
 *
 * If you edit the ActivatecAPI directives while the server is live you can get
 * cLHy to re-load the cAPIs by sending it a HUP or CLHY_SIG_GRACEFUL
 * signal as normal.  You can use this to dynamically change the capability
 * of your server without bringing it down.
 *
 * Because currently there is only limited builtin support in the Configure
 * script for creating the shared library files (`.so'), please consult your
 * vendors cc(1), ld(1) and dlopen(3) manpages to find out the appropriate
 * compiler and linker flags and insert them manually into the Configuration
 * file under CFLAGS_SHLIB, LDFLAGS_SHLIB and LDFLAGS_SHLIB_EXPORT.
 *
 * If you still have problems figuring out the flags both try the paper
 *     http://developer.netscape.com/library/documentation/enterprise
 *                                          /unix/svrplug.htm#1013807
 * or install a Perl 5 interpreter on your platform and then run the command
 *
 *     $ perl -V:usedl -V:ccdlflags -V:cccdlflags -V:lddlflags
 *
 * This gives you what type of dynamic loading Perl 5 uses on your platform
 * and which compiler and linker flags Perl 5 uses to create the shared object
 * files.
 *
 * Another location where you can find useful hints is the `ltconfig' script
 * of the GNU libtool 1.2 package. Search for your platform name inside the
 * various "case" constructs.
 *
 */

#include "kuda.h"
#include "kuda_dso.h"
#include "kuda_strings.h"
#include "kuda_errno.h"

#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "http_core.h"

#include "capi_so.h"

cAPI CLHY_CAPI_DECLARE_DATA so_capi;


/*
 * Server configuration to keep track of actually
 * loaded cAPIs and the corresponding cAPI name.
 */

typedef struct so_server_conf {
    kuda_array_header_t *activated_capis;
} so_server_conf;

static void *so_sconf_create(kuda_pool_t *p, server_rec *s)
{
    so_server_conf *soc;

    soc = (so_server_conf *)kuda_pcalloc(p, sizeof(so_server_conf));
    soc->activated_capis = kuda_array_make(p, DYNAMIC_CAPI_LIMIT,
                                     sizeof(clhy_capi_symbol_t));

    return (void *)soc;
}

#ifndef NO_DLOPEN

/*
 * This is the cleanup for a loaded shared object. It unloads the cAPI.
 * This is called as a cleanup function from the core.
 */

static kuda_status_t unload_capi(void *data)
{
    clhy_capi_symbol_t *capii = (clhy_capi_symbol_t*)data;

    /* only unload if cAPI information is still existing */
    if (capii->capip == NULL)
        return KUDA_SUCCESS;

    /* remove the cAPI pointer from the core structure */
    clhy_remove_activated_capi(capii->capip);

    /* destroy the cAPI information */
    capii->capip = NULL;
    capii->name = NULL;
    return KUDA_SUCCESS;
}

static const char *dso_load(cmd_parms *cmd, kuda_dso_handle_t **capihandlep,
                            const char *filename, const char **used_filename)
{
    int retry = 0;
    const char *fullname = clhy_server_root_relative(cmd->temp_pool, filename);
    char my_error[256];
    if (filename != NULL && clhy_strchr_c(filename, '/') == NULL) {
        /* retry on error without path to use dlopen()'s search path */
        retry = 1;
    }

    if (fullname == NULL && !retry) {
        return kuda_psprintf(cmd->temp_pool, "Invalid %s path %s",
                            cmd->cmd->name, filename);
    }
    *used_filename = fullname;
    if (kuda_dso_load(capihandlep, fullname, cmd->pool) == KUDA_SUCCESS) {
        return NULL;
    }
    if (retry) {
        *used_filename = filename;
        if (kuda_dso_load(capihandlep, filename, cmd->pool) == KUDA_SUCCESS)
            return NULL;
    }

    return kuda_pstrcat(cmd->temp_pool, "Cannot load ", filename,
                        " into server: ",
                        kuda_dso_error(*capihandlep, my_error, sizeof(my_error)),
                        NULL);
}

/*
 * This is called for the directive ActivatecAPI and actually loads
 * a shared object file into the address space of the server process.
 */

static const char *activate_capi(cmd_parms *cmd, void *dummy,
                               const char *capiname, const char *filename)
{
    kuda_dso_handle_t *capihandle;
    kuda_dso_handle_sym_t capisym;
    cAPI *capip;
    const char *capi_file;
    so_server_conf *sconf;
    clhy_capi_symbol_t *capii;
    clhy_capi_symbol_t *capie;
    int i;
    const char *error;

    /* we need to setup this value for dummy to make sure that we don't try
     * to add a non-existent tree into the build when we return to
     * execute_now.
     */
    *(clhy_directive_t **)dummy = NULL;

    /*
     * check for already existing cAPI
     * If it already exists, we have nothing to do
     * Check both dynamically-loaded cAPIs and statically-linked cAPIs.
     */
    sconf = (so_server_conf *)clhy_get_capi_config(cmd->server->capi_config,
                                                &so_capi);
    capie = (clhy_capi_symbol_t *)sconf->activated_capis->elts;
    for (i = 0; i < sconf->activated_capis->nelts; i++) {
        capii = &capie[i];
        if (capii->name != NULL && strcmp(capii->name, capiname) == 0) {
            clhy_log_perror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, cmd->pool, CLHYLOGNO(01574)
                          "cAPI %s is already loaded, skipping",
                          capiname);
            return NULL;
        }
    }

    for (i = 0; clhy_preactivated_capis[i]; i++) {
        const char *preload_name;
        kuda_size_t preload_len;
        kuda_size_t thiscapi_len;

        capip = clhy_preactivated_capis[i];

        /* make sure we're comparing apples with apples
         * make sure name of preloaded cAPI is capi_FOO.c
         * make sure name of structure being loaded is FOO_capi
         */

        if (memcmp(capip->name, "capi_", 4)) {
            continue;
        }

        preload_name = capip->name + strlen("capi_");
        preload_len = strlen(preload_name) - 2;

        if (strlen(capiname) <= strlen("_capi")) {
            continue;
        }
        thiscapi_len = strlen(capiname) - strlen("_capi");
        if (strcmp(capiname + thiscapi_len, "_capi")) {
            continue;
        }

        if (thiscapi_len != preload_len) {
            continue;
        }

        if (!memcmp(capiname, preload_name, preload_len)) {
            return kuda_pstrcat(cmd->pool, "cAPI ", capiname,
                               " is built-in and can't be loaded",
                               NULL);
        }
    }

    capii = kuda_array_push(sconf->activated_capis);
    capii->name = capiname;

    /*
     * Load the file into the cLHy address space
     */
    error = dso_load(cmd, &capihandle, filename, &capi_file);
    if (error)
        return error;
    clhy_log_perror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, cmd->pool, CLHYLOGNO(01575)
                 "activated cAPI %s from %s", capiname, capi_file);

    /*
     * Retrieve the pointer to the cAPI structure through the cAPI name:
     * First with the hidden variant (prefix `CLHY_') and then with the plain
     * symbol name.
     */
    if (kuda_dso_sym(&capisym, capihandle, capiname) != KUDA_SUCCESS) {
        char my_error[256];

        return kuda_pstrcat(cmd->pool, "Can't locate API cAPI structure `",
                          capiname, "' in file ", capi_file, ": ",
                          kuda_dso_error(capihandle, my_error, sizeof(my_error)),
                          NULL);
    }
    capip = (cAPI*) capisym;
    capip->dynamic_load_handle = (kuda_dso_handle_t *)capihandle;
    capii->capip = capip;

    /*
     * Make sure the found cAPI structure is really a cAPI structure
     *
     */
    if (capip->magic != CAPI_MAGIC_COOKIE) {
        return kuda_psprintf(cmd->pool, "API cAPI structure '%s' in file %s "
                            "is garbled - expected signature %08lx but saw "
                            "%08lx - perhaps this is not an cLHy cAPI DSO, "
                            "or was compiled for a different cLHy version?",
                            capiname, capi_file,
                            CAPI_MAGIC_COOKIE, capip->magic);
    }

    /*
     * Add this cAPI to the cLHy core structures
     */
    error = clhy_add_activated_capi(capip, cmd->pool, capiname);
    if (error) {
        return error;
    }

    /*
     * Register a cleanup in the config kuda_pool_t (normally pconf). When
     * we do a restart (or shutdown) this cleanup will cause the
     * shared object to be unloaded.
     */
    kuda_pool_cleanup_register(cmd->pool, capii, unload_capi, kuda_pool_cleanup_null);

    /*
     * Finally we need to run the configuration process for the cAPI
     */
    clhy_single_capi_configure(cmd->pool, cmd->server, capip);

    return NULL;
}

/*
 * This implements the LoadFile directive and loads an arbitrary
 * shared object file into the address space of the server process.
 */

static const char *load_file(cmd_parms *cmd, void *dummy, const char *filename)
{
    kuda_dso_handle_t *handle;
    const char *used_file, *error;

    error = dso_load(cmd, &handle, filename, &used_file);
    if (error)
        return error;

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, NULL, CLHYLOGNO(01576)
                 "loaded file %s", used_file);

    return NULL;
}

static cAPI *clhy_find_activated_capi_symbol(server_rec *s, const char *capiname)
{
    so_server_conf *sconf;
    clhy_capi_symbol_t *capii;
    clhy_capi_symbol_t *capie;
    int i;

    sconf = (so_server_conf *)clhy_get_capi_config(s->capi_config,
                                                   &so_capi);
    capie = (clhy_capi_symbol_t *)sconf->activated_capis->elts;

    for (i = 0; i < sconf->activated_capis->nelts; i++) {
        capii = &capie[i];
        if (capii->name != NULL && strcmp(capii->name, capiname) == 0) {
            return capii->capip;
        }
    }
    return NULL;
}

static void dump_activated_capis(kuda_pool_t *p, server_rec *s)
{
    clhy_capi_symbol_t *capie;
    clhy_capi_symbol_t *capii;
    so_server_conf *sconf;
    int i;
    kuda_file_t *out = NULL;

    if (!clhy_exists_config_define("DUMP_CAPIS")) {
        return;
    }

    kuda_file_open_stdout(&out, p);

    kuda_file_printf(out, "Activated cAPIs:\n");

    sconf = (so_server_conf *)clhy_get_capi_config(s->capi_config,
                                                   &so_capi);
    for (i = 0; ; i++) {
        capii = &clhy_prelinked_capi_symbols[i];
        if (capii->name != NULL) {
            kuda_file_printf(out, " %s (static)\n", capii->name);
        }
        else {
            break;
        }
    }

    capie = (clhy_capi_symbol_t *)sconf->activated_capis->elts;
    for (i = 0; i < sconf->activated_capis->nelts; i++) {
        capii = &capie[i];
        if (capii->name != NULL) {
            kuda_file_printf(out, " %s (shared)\n", capii->name);
        }
    }
}

#else /* not NO_DLOPEN */

static const char *load_file(cmd_parms *cmd, void *dummy, const char *filename)
{
    clhy_log_perror(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, cmd->pool, CLHYLOGNO(01577)
                 "WARNING: LoadFile not supported on this platform");
    return NULL;
}

static const char *activate_capi(cmd_parms *cmd, void *dummy,
                               const char *capiname, const char *filename)
{
    clhy_log_perror(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, cmd->pool, CLHYLOGNO(01578)
                 "WARNING: ActivatecAPI not supported on this platform");
    return NULL;
}

#endif /* NO_DLOPEN */

static void register_hooks(kuda_pool_t *p)
{
#ifndef NO_DLOPEN
    KUDA_REGISTER_OPTIONAL_FN(clhy_find_activated_capi_symbol);
    clhy_hook_test_config(dump_activated_capis, NULL, NULL, KUDA_HOOK_MIDDLE);
#endif
}

static const command_rec so_cmds[] = {
    CLHY_INIT_TAKE2("ActivatecAPI", activate_capi, NULL, RSRC_CONF | EXEC_ON_READ,
      "a cAPI name and the name of a shared object file to load it from"),
    CLHY_INIT_ITERATE("LoadFile", load_file, NULL, RSRC_CONF  | EXEC_ON_READ,
      "shared object file or library to load into the server at runtime"),
    { NULL }
};

CLHY_DECLARE_CAPI(so) = {
   STANDARD16_CAPI_STUFF,
   NULL,                 /* create per-dir config */
   NULL,                 /* merge per-dir config */
   so_sconf_create,      /* server config */
   NULL,                 /* merge server config */
   so_cmds,              /* command kuda_table_t */
   register_hooks        /* register hooks */
};
