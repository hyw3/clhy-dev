dnl cAPIs enabled in this directory by default

dnl CLHYKUDEL_CAPI(name, helptext[, objects[, structname[, default[, config]]]])

CLHYKUDEL_CAPIPATH_INIT(core)

KUDA_CHECK_KUDA_DEFINE(KUDA_HAS_DSO)

case "x$enable_so" in
    "xyes")
        if test $ac_cv_define_KUDA_HAS_DSO = "no"; then
            AC_MSG_ERROR([capi_so has been requested but cannot be built on your system])
        fi
        ;;
    "xshared")
        AC_MSG_ERROR([capi_so can not be built as a shared DSO])
        ;;
    "xno")
        ;;
    "x")
        enable_so=$ac_cv_define_KUDA_HAS_DSO
        ;;
esac

dnl capi_so can only be built statically. Override the default here.
if test "x$enable_so" = "xyes"; then
    enable_so="static"
fi

if test "x$enable_so" = "xstatic"; then
    KUDA_ADDTO(WWHY_LDFLAGS, [-export-dynamic])
    INSTALL_DSO=yes
else
    INSTALL_DSO=no
fi
CLHYKUDEL_SUBST(INSTALL_DSO)

if test "$sharedobjs" = "yes"; then
    if test $ac_cv_define_KUDA_HAS_DSO = "no"; then
        AC_MSG_ERROR([shared objects have been requested but cannot be built since capi_so cannot be built])
    elif test $enable_so = "no"; then
        AC_MSG_ERROR([shared objects have been requested but cannot be built since capi_so was disabled])
    fi
fi

CLHYKUDEL_CAPI(so, DSO capability.  This cAPI will be automatically enabled unless you build all cAPIs statically., , , $enable_so)

CLHYKUDEL_CAPI(watchdog, Watchdog cAPI, , , most, [
    KUDA_CHECK_KUDA_DEFINE(KUDA_HAS_THREADS)
    if test $ac_cv_define_KUDA_HAS_THREADS = "no"; then
        AC_MSG_WARN([capi_watchdog requires kuda to be built with --enable-threads])
        enable_watchdog=no
    fi
])

CLHYKUDEL_CAPI(macro, Define and use macros in configuration files, , , most)

KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

CLHYKUDEL_CAPIPATH_FINISH
