/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file capi_so.h
 * @brief Shared Object Loader Extension cAPI for cLHy
 *
 * @defgroup CAPI_SO capi_so
 * @ingroup CLHYKUDEL_CAPIS
 * @{
 */

#ifndef CAPI_SO_H
#define CAPI_SO_H 1

#include "kuda_optional.h"
#include "wwhy.h"

/* optional function declaration */
KUDA_DECLARE_OPTIONAL_FN(cAPI *, clhy_find_activated_capi_symbol,
                        (server_rec *s, const char *capiname));

#endif /* CAPI_SO_H */
/** @} */

