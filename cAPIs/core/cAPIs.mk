libcapi_so.la: capi_so.lo
	$(CAPI_LINK) capi_so.lo $(CAPI_SO_LDADD)
capi_watchdog.la: capi_watchdog.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_watchdog.lo $(CAPI_WATCHDOG_LDADD)
capi_macro.la: capi_macro.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_macro.lo $(CAPI_MACRO_LDADD)
DISTCLEAN_TARGETS = cAPIs.mk
static =  libcapi_so.la
shared =  capi_watchdog.la capi_macro.la
