/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Watchdog cAPI.
 */

#include "kuda.h"
#include "capi_watchdog.h"
#include "clhy_provider.h"
#include "clhy_core.h"
#include "http_core.h"
#include "util_mutex.h"

#define CLHY_WATCHDOG_PGROUP    "watchdog"
#define CLHY_WATCHDOG_PVERSION  "parent"
#define CLHY_WATCHDOG_CVERSION  "child"

typedef struct watchdog_list_t watchdog_list_t;

struct watchdog_list_t
{
    struct watchdog_list_t *next;
    clhy_watchdog_t *wd;
    kuda_status_t status;
    kuda_interval_time_t interval;
    kuda_interval_time_t step;
    const void *data;
    clhy_watchdog_callback_fn_t *callback_fn;
};

struct clhy_watchdog_t
{
    kuda_thread_mutex_t   *startup;
    kuda_proc_mutex_t     *mutex;
    const char           *name;
    watchdog_list_t      *callbacks;
    int                   is_running;
    int                   singleton;
    int                   active;
    kuda_interval_time_t   step;
    kuda_thread_t         *thread;
    kuda_pool_t           *pool;
};

typedef struct wd_server_conf_t wd_server_conf_t;
struct wd_server_conf_t
{
    int child_workers;
    int parent_workers;
    kuda_pool_t *pool;
    server_rec *s;
};

static wd_server_conf_t *wd_server_conf = NULL;
static kuda_interval_time_t wd_interval = CLHY_WD_TM_INTERVAL;
static int wd_interval_set = 0;
static int clmp_is_forked = CLHY_CLMPQ_NOT_SUPPORTED;
static const char *wd_proc_mutex_type = "watchdog-callback";

static kuda_status_t wd_worker_cleanup(void *data)
{
    kuda_status_t rv;
    clhy_watchdog_t *w = (clhy_watchdog_t *)data;

    if (w->is_running) {
        watchdog_list_t *wl = w->callbacks;
        while (wl) {
            if (wl->status == KUDA_SUCCESS) {
                /* Execute watchdog callback with STOPPING state */
                (*wl->callback_fn)(CLHY_WATCHDOG_STATE_STOPPING,
                                    (void *)wl->data, w->pool);
                wl->status = KUDA_EOF;
            }
            wl = wl->next;
        }
    }
    w->is_running = 0;
    kuda_thread_join(&rv, w->thread);
    return rv;
}

/*--------------------------------------------------------------------------*/
/*                                                                          */
/* Main watchdog worker thread.                                             */
/* For singleton workers child thread that first obtains the process       */
/* mutex is running. Threads in other child's are locked on mutex.          */
/*                                                                          */
/*--------------------------------------------------------------------------*/
static void* KUDA_THREAD_FUNC wd_worker(kuda_thread_t *thread, void *data)
{
    clhy_watchdog_t *w = (clhy_watchdog_t *)data;
    kuda_status_t rv;
    int locked = 0;
    int probed = 0;
    int inited = 0;
    int clmpq_s = 0;

    w->pool = kuda_thread_pool_get(thread);
    w->is_running = 1;

    kuda_thread_mutex_unlock(w->startup);
    if (w->mutex) {
        while (w->is_running) {
            if (clhy_clmp_query(CLHY_CLMPQ_CLMP_STATE, &clmpq_s) != KUDA_SUCCESS) {
                w->is_running = 0;
                break;
            }
            if (clmpq_s == CLHY_CLMPQ_STOPPING) {
                w->is_running = 0;
                break;
            }
            rv = kuda_proc_mutex_trylock(w->mutex);
            if (rv == KUDA_SUCCESS) {
                if (probed) {
                    /* Sleep after we were locked
                     * up to 1 second. WWHy can be
                     * in the middle of shutdown, and
                     * our child didn't yet received
                     * the shutdown signal.
                     */
                    probed = 10;
                    while (w->is_running && probed > 0) {
                        kuda_sleep(CLHY_WD_TM_INTERVAL);
                        probed--;
                        if (clhy_clmp_query(CLHY_CLMPQ_CLMP_STATE, &clmpq_s) != KUDA_SUCCESS) {
                            w->is_running = 0;
                            break;
                        }
                        if (clmpq_s == CLHY_CLMPQ_STOPPING) {
                            w->is_running = 0;
                            break;
                        }
                    }
                }
                locked = 1;
                break;
            }
            probed = 1;
            kuda_sleep(CLHY_WD_TM_SLICE);
        }
    }
    if (w->is_running) {
        watchdog_list_t *wl = w->callbacks;
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, wd_server_conf->s,
                     CLHYLOGNO(02972) "%sWatchdog (%s) running",
                     w->singleton ? "Singleton " : "", w->name);
        kuda_time_clock_hires(w->pool);
        if (wl) {
            kuda_pool_t *ctx = NULL;
            kuda_pool_create(&ctx, w->pool);
            while (wl && w->is_running) {
                /* Execute watchdog callback */
                wl->status = (*wl->callback_fn)(CLHY_WATCHDOG_STATE_STARTING,
                                                (void *)wl->data, ctx);
                wl = wl->next;
            }
            kuda_pool_destroy(ctx);
        }
        else {
            clhy_run_watchdog_init(wd_server_conf->s, w->name, w->pool);
            inited = 1;
        }
    }

    /* Main execution loop */
    while (w->is_running) {
        kuda_pool_t *ctx = NULL;
        kuda_time_t curr;
        watchdog_list_t *wl = w->callbacks;

        kuda_sleep(CLHY_WD_TM_SLICE);
        if (clhy_clmp_query(CLHY_CLMPQ_CLMP_STATE, &clmpq_s) != KUDA_SUCCESS) {
            w->is_running = 0;
        }
        if (clmpq_s == CLHY_CLMPQ_STOPPING) {
            w->is_running = 0;
        }
        if (!w->is_running) {
            break;
        }
        curr = kuda_time_now() - CLHY_WD_TM_SLICE;
        while (wl && w->is_running) {
            if (wl->status == KUDA_SUCCESS) {
                wl->step += (kuda_time_now() - curr);
                if (wl->step >= wl->interval) {
                    if (!ctx)
                        kuda_pool_create(&ctx, w->pool);
                    wl->step = 0;
                    /* Execute watchdog callback */
                    wl->status = (*wl->callback_fn)(CLHY_WATCHDOG_STATE_RUNNING,
                                                    (void *)wl->data, ctx);
                    if (clhy_clmp_query(CLHY_CLMPQ_CLMP_STATE, &clmpq_s) != KUDA_SUCCESS) {
                        w->is_running = 0;
                    }
                    if (clmpq_s == CLHY_CLMPQ_STOPPING) {
                        w->is_running = 0;
                    }
                }
            }
            wl = wl->next;
        }
        if (w->is_running && w->callbacks == NULL) {
            /* This is hook mode watchdog
             * running on WatchogInterval
             */
            w->step += (kuda_time_now() - curr);
            if (w->step >= wd_interval) {
                if (!ctx)
                    kuda_pool_create(&ctx, w->pool);
                w->step = 0;
                /* Run watchdog step hook */
                clhy_run_watchdog_step(wd_server_conf->s, w->name, ctx);
            }
        }
        if (ctx)
            kuda_pool_destroy(ctx);
        if (!w->is_running) {
            break;
        }
    }
    if (inited) {
        /* Run the watchdog exit hooks.
         * If this was singleton watchdog the init hook
         * might never been called, so skip the exit hook
         * in that case as well.
         */
        clhy_run_watchdog_exit(wd_server_conf->s, w->name, w->pool);
    }
    else {
        watchdog_list_t *wl = w->callbacks;
        while (wl) {
            if (wl->status == KUDA_SUCCESS) {
                /* Execute watchdog callback with STOPPING state */
                (*wl->callback_fn)(CLHY_WATCHDOG_STATE_STOPPING,
                                   (void *)wl->data, w->pool);
            }
            wl = wl->next;
        }
    }
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, wd_server_conf->s,
                 CLHYLOGNO(02973) "%sWatchdog (%s) stopping",
                 w->singleton ? "Singleton " : "", w->name);

    if (locked)
        kuda_proc_mutex_unlock(w->mutex);
    kuda_thread_exit(w->thread, KUDA_SUCCESS);

    return NULL;
}

static kuda_status_t wd_startup(clhy_watchdog_t *w, kuda_pool_t *p)
{
    kuda_status_t rc;

    /* Create thread startup mutex */
    rc = kuda_thread_mutex_create(&w->startup, KUDA_THREAD_MUTEX_UNNESTED, p);
    if (rc != KUDA_SUCCESS)
        return rc;

    if (w->singleton) {
        /* Initialize singleton mutex in child */
        rc = kuda_proc_mutex_child_init(&w->mutex,
                                       kuda_proc_mutex_lockfile(w->mutex), p);
        if (rc != KUDA_SUCCESS)
            return rc;
    }

    /* This mutex fixes problems with a fast start/fast end, where the pool
     * cleanup was being invoked before the thread completely spawned.
     */
    kuda_thread_mutex_lock(w->startup);
    kuda_pool_pre_cleanup_register(p, w, wd_worker_cleanup);

    /* Start the newly created watchdog */
    rc = kuda_thread_create(&w->thread, NULL, wd_worker, w, p);
    if (rc) {
        kuda_pool_cleanup_kill(p, w, wd_worker_cleanup);
    }

    kuda_thread_mutex_lock(w->startup);
    kuda_thread_mutex_unlock(w->startup);
    kuda_thread_mutex_destroy(w->startup);

    return rc;
}

static kuda_status_t clhy_watchdog_get_instance(clhy_watchdog_t **watchdog,
                                             const char *name,
                                             int parent,
                                             int singleton,
                                             kuda_pool_t *p)
{
    clhy_watchdog_t *w;
    const char *pver = parent ? CLHY_WATCHDOG_PVERSION : CLHY_WATCHDOG_CVERSION;

    if (parent && clmp_is_forked != CLHY_CLMPQ_NOT_SUPPORTED) {
        /* Parent threads are not supported for
         * forked cLHy core APIs (cLMP)'s
         */
        *watchdog = NULL;
        return KUDA_ENOTIMPL;
    }
    w = clhy_lookup_provider(CLHY_WATCHDOG_PGROUP, name, pver);
    if (w) {
        *watchdog = w;
        return KUDA_SUCCESS;
    }
    w = kuda_pcalloc(p, sizeof(clhy_watchdog_t));
    w->name      = name;
    w->pool      = p;
    w->singleton = parent ? 0 : singleton;
    *watchdog    = w;
    return clhy_register_provider(p, CLHY_WATCHDOG_PGROUP, name,
                                pver, *watchdog);
}

static kuda_status_t clhy_watchdog_set_callback_interval(clhy_watchdog_t *w,
                                                      kuda_interval_time_t interval,
                                                      const void *data,
                                                      clhy_watchdog_callback_fn_t *callback)
{
    watchdog_list_t *c = w->callbacks;
    kuda_status_t rv = KUDA_EOF;

    while (c) {
        if (c->data == data && c->callback_fn == callback) {
            /* We have existing callback.
             * Update the interval and reset status, so the
             * callback and continue execution if stopped earlier.
             */
            c->interval = interval;
            c->step     = 0;
            c->status   = KUDA_SUCCESS;
            rv          = KUDA_SUCCESS;
            break;
        }
        c = c->next;
    }
    return rv;
}

static kuda_status_t clhy_watchdog_register_callback(clhy_watchdog_t *w,
                                                  kuda_interval_time_t interval,
                                                  const void *data,
                                                  clhy_watchdog_callback_fn_t *callback)
{
    watchdog_list_t *c = w->callbacks;

    while (c) {
        if (c->data == data && c->callback_fn == callback) {
            /* We have already registered callback.
             * Do not allow callbacks that have the same
             * function and data pointers.
             */
            return KUDA_EEXIST;
        }
        c = c->next;
    }
    c = kuda_palloc(w->pool, sizeof(watchdog_list_t));
    c->data        = data;
    c->callback_fn = callback;
    c->interval    = interval;
    c->step        = 0;
    c->status      = KUDA_EINIT;

    c->wd          = w;
    c->next        = w->callbacks;
    w->callbacks   = c;
    w->active++;

    return KUDA_SUCCESS;
}

/*--------------------------------------------------------------------------*/
/*                                                                          */
/* Pre config hook.                                                         */
/* Create default watchdogs for parent and child                            */
/* Parent watchdog executes inside parent process so it doesn't need the    */
/* singleton mutex                                                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/
static int wd_pre_config_hook(kuda_pool_t *pconf, kuda_pool_t *plog,
                              kuda_pool_t *ptemp)
{
    kuda_status_t rv;
    clhy_watchdog_t *w;

    clhy_clmp_query(CLHY_CLMPQ_IS_FORKED, &clmp_is_forked);
    if ((rv = clhy_watchdog_get_instance(&w,
                CLHY_WATCHDOG_SINGLETON, 0, 1, pconf)) != KUDA_SUCCESS) {
        return rv;
    }
    if ((rv = clhy_watchdog_get_instance(&w,
                CLHY_WATCHDOG_DEFAULT, 0, 0, pconf)) != KUDA_SUCCESS) {
        return rv;
    }
    if (clmp_is_forked == CLHY_CLMPQ_NOT_SUPPORTED) {
        /* Create parent process watchdog for
         * non forked cLMP's only.
         */
        if ((rv = clhy_watchdog_get_instance(&w,
                    CLHY_WATCHDOG_DEFAULT, 1, 0, pconf)) != KUDA_SUCCESS) {
            return rv;
        }
    }

    if ((rv = clhy_mutex_register(pconf, wd_proc_mutex_type, NULL,
                                KUDA_LOCK_DEFAULT, 0)) != KUDA_SUCCESS) {
        return rv;
    }

    return OK;
}

/*--------------------------------------------------------------------------*/
/*                                                                          */
/* Post config hook.                                                        */
/* Create watchdog thread in parent and initializes Watchdog cAPI         */
/*                                                                          */
/*--------------------------------------------------------------------------*/
static int wd_post_config_hook(kuda_pool_t *pconf, kuda_pool_t *plog,
                               kuda_pool_t *ptemp, server_rec *s)
{
    kuda_status_t rv;
    const char *pk = "watchdog_init_capi_tag";
    kuda_pool_t *pproc = s->process->pool;
    const kuda_array_header_t *wl;

    if (clhy_state_query(CLHY_SQ_MAIN_STATE) == CLHY_SQ_MS_CREATE_PRE_CONFIG)
        /* First time config phase -- skip. */
        return OK;

    kuda_pool_userdata_get((void *)&wd_server_conf, pk, pproc);
    if (!wd_server_conf) {
        if (!(wd_server_conf = kuda_pcalloc(pproc, sizeof(wd_server_conf_t))))
            return KUDA_ENOMEM;
        kuda_pool_create(&wd_server_conf->pool, pproc);
        kuda_pool_userdata_set(wd_server_conf, pk, kuda_pool_cleanup_null, pproc);
    }
    wd_server_conf->s = s;
    if ((wl = clhy_list_provider_names(pconf, CLHY_WATCHDOG_PGROUP,
                                            CLHY_WATCHDOG_PVERSION))) {
        const clhy_list_provider_names_t *wn;
        int i;

        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(02974)
                "Watchdog: found parent providers.");

        wn = (clhy_list_provider_names_t *)wl->elts;
        for (i = 0; i < wl->nelts; i++) {
            clhy_watchdog_t *w = clhy_lookup_provider(CLHY_WATCHDOG_PGROUP,
                                                  wn[i].provider_name,
                                                  CLHY_WATCHDOG_PVERSION);
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(02975)
                    "Watchdog: Looking for parent (%s).", wn[i].provider_name);
            if (w) {
                if (!w->active) {
                    int status = clhy_run_watchdog_need(s, w->name, 1,
                                                      w->singleton);
                    if (status == OK) {
                        /* One of the cAPIs returned OK to this watchog.
                         * Mark it as active
                         */
                        w->active = 1;
                    }
                }
                if (w->active) {
                    /* We have active watchdog.
                     * Create the watchdog thread
                     */
                    if ((rv = wd_startup(w, wd_server_conf->pool)) != KUDA_SUCCESS) {
                        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(01571)
                                "Watchdog: Failed to create parent worker thread.");
                        return rv;
                    }
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, s, CLHYLOGNO(02976)
                            "Watchdog: Created parent worker thread (%s).", w->name);
                    wd_server_conf->parent_workers++;
                }
            }
        }
    }
    if (wd_server_conf->parent_workers) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(01572)
                     "Spawned %d parent worker threads.",
                     wd_server_conf->parent_workers);
    }
    if ((wl = clhy_list_provider_names(pconf, CLHY_WATCHDOG_PGROUP,
                                            CLHY_WATCHDOG_CVERSION))) {
        const clhy_list_provider_names_t *wn;
        int i;
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(02977)
                "Watchdog: found child providers.");

        wn = (clhy_list_provider_names_t *)wl->elts;
        for (i = 0; i < wl->nelts; i++) {
            clhy_watchdog_t *w = clhy_lookup_provider(CLHY_WATCHDOG_PGROUP,
                                                  wn[i].provider_name,
                                                  CLHY_WATCHDOG_CVERSION);
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(02978)
                    "Watchdog: Looking for child (%s).", wn[i].provider_name);
            if (w) {
                if (!w->active) {
                    int status = clhy_run_watchdog_need(s, w->name, 0,
                                                      w->singleton);
                    if (status == OK) {
                        /* One of the cAPIs returned OK to this watchog.
                         * Mark it as active
                         */
                        w->active = 1;
                    }
                }
                if (w->active) {
                    /* We have some callbacks registered.
                     * Create mutexes for singleton watchdogs
                     */
                    if (w->singleton) {
                        rv = clhy_proc_mutex_create(&w->mutex, NULL, wd_proc_mutex_type,
                                                  w->name, s,
                                                  wd_server_conf->pool, 0);
                        if (rv != KUDA_SUCCESS) {
                            return rv;
                        }
                    }
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, s, CLHYLOGNO(02979)
                            "Watchdog: Created child worker thread (%s).", w->name);
                    wd_server_conf->child_workers++;
                }
            }
        }
    }
    return OK;
}

/*--------------------------------------------------------------------------*/
/*                                                                          */
/* Child init hook.                                                         */
/* Create watchdog threads and initializes Mutexes in child                 */
/*                                                                          */
/*--------------------------------------------------------------------------*/
static void wd_child_init_hook(kuda_pool_t *p, server_rec *s)
{
    kuda_status_t rv = OK;
    const kuda_array_header_t *wl;

    if (!wd_server_conf->child_workers) {
        /* We don't have anything configured, bail out.
         */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, s, CLHYLOGNO(02980)
                     "Watchdog: nothing configured?");
        return;
    }
    if ((wl = clhy_list_provider_names(p, CLHY_WATCHDOG_PGROUP,
                                        CLHY_WATCHDOG_CVERSION))) {
        const clhy_list_provider_names_t *wn;
        int i;
        wn = (clhy_list_provider_names_t *)wl->elts;
        for (i = 0; i < wl->nelts; i++) {
            clhy_watchdog_t *w = clhy_lookup_provider(CLHY_WATCHDOG_PGROUP,
                                                  wn[i].provider_name,
                                                  CLHY_WATCHDOG_CVERSION);
            if (w && w->active) {
                /* We have some callbacks registered.
                 * Kick of the watchdog
                 */
                if ((rv = wd_startup(w, wd_server_conf->pool)) != KUDA_SUCCESS) {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(01573)
                                 "Watchdog: Failed to create worker thread.");
                    /* No point to continue */
                    return;
                }
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, s, CLHYLOGNO(02981)
                             "Watchdog: Created worker thread (%s).", wn[i].provider_name);
            }
        }
    }
}

/*--------------------------------------------------------------------------*/
/*                                                                          */
/* WatchdogInterval directive                                               */
/*                                                                          */
/*--------------------------------------------------------------------------*/
static const char *wd_cmd_watchdog_int(cmd_parms *cmd, void *dummy,
                                       const char *arg)
{
    int i;
    const char *errs = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (errs != NULL)
        return errs;
    if (wd_interval_set)
       return "Duplicate WatchdogInterval directives are not allowed";
    if ((i = atoi(arg)) < 1)
        return "Invalid WatchdogInterval value";

    wd_interval = kuda_time_from_sec(i);
    wd_interval_set = 1;
    return NULL;
}

/*--------------------------------------------------------------------------*/
/*                                                                          */
/* List of directives specific to our cAPI.                               */
/*                                                                          */
/*--------------------------------------------------------------------------*/
static const command_rec wd_directives[] =
{
    CLHY_INIT_TAKE1(
        "WatchdogInterval",                 /* directive name               */
        wd_cmd_watchdog_int,                /* config action routine        */
        NULL,                               /* argument to include in call  */
        RSRC_CONF,                          /* where available              */
        "Watchdog interval in seconds"
    ),
    {NULL}
};

/*--------------------------------------------------------------------------*/
/*                                                                          */
/* Which functions are responsible for which hooks in the server.           */
/*                                                                          */
/*--------------------------------------------------------------------------*/
static void wd_register_hooks(kuda_pool_t *p)
{

    /* Only cLHy core API for Windows (core_winnt) has child init hook handler.
     * Make sure that we are called after cLMP child init handler initializes.
     */
    static const char *const after_clmp[]      = { "core_winnt.c", NULL};

    /* Pre config handling
     */
    clhy_hook_pre_config(wd_pre_config_hook,
                       NULL,
                       NULL,
                       KUDA_HOOK_FIRST);

    /* Post config handling
     */
    clhy_hook_post_config(wd_post_config_hook,
                        NULL,
                        NULL,
                        KUDA_HOOK_LAST);

    /* Child init hook
     */
    clhy_hook_child_init(wd_child_init_hook,
                       after_clmp,
                       NULL,
                       KUDA_HOOK_MIDDLE);

    KUDA_REGISTER_OPTIONAL_FN(clhy_watchdog_get_instance);
    KUDA_REGISTER_OPTIONAL_FN(clhy_watchdog_register_callback);
    KUDA_REGISTER_OPTIONAL_FN(clhy_watchdog_set_callback_interval);
}

/*--------------------------------------------------------------------------*/
/*                                                                          */
/* The list of callback routines and data structures that provide           */
/* the static hooks into our cAPI from the other parts of the server.     */
/*                                                                          */
/*--------------------------------------------------------------------------*/
CLHY_DECLARE_CAPI(watchdog) = {
    STANDARD16_CAPI_STUFF,
    NULL,                       /* create per-directory config structure    */
    NULL,                       /* merge per-directory config structures    */
    NULL,                       /* create per-server config structure       */
    NULL,                       /* merge per-server config structures       */
    wd_directives,              /* command kuda_table_t                      */
    wd_register_hooks           /* register hooks                           */
};

/*--------------------------------------------------------------------------*/
/*                                                                          */
/* The list of optional hooks that we provide                               */
/*                                                                          */
/*--------------------------------------------------------------------------*/
KUDA_HOOK_STRUCT(
    KUDA_HOOK_LINK(watchdog_need)
    KUDA_HOOK_LINK(watchdog_init)
    KUDA_HOOK_LINK(watchdog_exit)
    KUDA_HOOK_LINK(watchdog_step)
)

KUDA_IMPLEMENT_EXTERNAL_HOOK_RUN_FIRST(clhy, CLHY_WD, int, watchdog_need,
                                      (server_rec *s, const char *name,
                                       int parent, int singleton),
                                      (s, name, parent, singleton),
                                      DECLINED)
KUDA_IMPLEMENT_EXTERNAL_HOOK_RUN_ALL(clhy, CLHY_WD, int, watchdog_init,
                                    (server_rec *s, const char *name,
                                     kuda_pool_t *pool),
                                    (s, name, pool),
                                    OK, DECLINED)
KUDA_IMPLEMENT_EXTERNAL_HOOK_RUN_ALL(clhy, CLHY_WD, int, watchdog_exit,
                                    (server_rec *s, const char *name,
                                     kuda_pool_t *pool),
                                    (s, name, pool),
                                    OK, DECLINED)
KUDA_IMPLEMENT_EXTERNAL_HOOK_RUN_ALL(clhy, CLHY_WD, int, watchdog_step,
                                    (server_rec *s, const char *name,
                                     kuda_pool_t *pool),
                                    (s, name, pool),
                                    OK, DECLINED)
