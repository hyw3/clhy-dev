/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CAPI_WATCHDOG_H
#define CAPI_WATCHDOG_H

/**
 * @file  capi_watchdog.h
 * @brief Watchdog cAPI for cLHy
 *
 * @defgroup CAPI_WATCHDOG capi_watchdog
 * @ingroup  CLHYKUDEL_CAPIS
 * @{
 */

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "clhy_provider.h"

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_pools.h"
#include "kuda_shm.h"
#include "kuda_hash.h"
#include "kuda_hooks.h"
#include "kuda_optional.h"
#include "kuda_file_io.h"
#include "kuda_time.h"
#include "kuda_thread_proc.h"
#include "kuda_global_mutex.h"
#include "kuda_thread_mutex.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Default singleton watchdog instance name.
 * Singleton watchdog is protected by mutex and
 * guaranteed to be run inside a single child process
 * at any time.
 */
#define CLHY_WATCHDOG_SINGLETON       "_singleton_"

/**
 * Default watchdog instance name
 */
#define CLHY_WATCHDOG_DEFAULT         "_default_"

/**
 * Default Watchdog interval
 */
#define CLHY_WD_TM_INTERVAL           KUDA_TIME_C(1000000)  /* 1 second     */

/**
 * Watchdog thread timer resolution
 */
#define CLHY_WD_TM_SLICE              KUDA_TIME_C(100000)   /* 100 ms       */

/* State values for callback */
#define CLHY_WATCHDOG_STATE_STARTING  1
#define CLHY_WATCHDOG_STATE_RUNNING   2
#define CLHY_WATCHDOG_STATE_STOPPING  3

typedef struct clhy_watchdog_t clhy_watchdog_t;

/* Create a set of CLHY_WD_DECLARE(type), CLHY_WD_DECLARE_NONSTD(type) and
 * CLHY_WD_DECLARE_DATA with appropriate export and import tags for the platform
 */
#if !defined(CLHY_WD_DECLARE)
#if !defined(WIN32)
#define CLHY_WD_DECLARE(type)            type
#define CLHY_WD_DECLARE_NONSTD(type)     type
#define CLHY_WD_DECLARE_DATA
#elif defined(CLHY_WD_DECLARE_STATIC)
#define CLHY_WD_DECLARE(type)            type __stdcall
#define CLHY_WD_DECLARE_NONSTD(type)     type
#define CLHY_WD_DECLARE_DATA
#elif defined(CLHY_WD_DECLARE_EXPORT)
#define CLHY_WD_DECLARE(type)            __declspec(dllexport) type __stdcall
#define CLHY_WD_DECLARE_NONSTD(type)     __declspec(dllexport) type
#define CLHY_WD_DECLARE_DATA             __declspec(dllexport)
#else
#define CLHY_WD_DECLARE(type)            __declspec(dllimport) type __stdcall
#define CLHY_WD_DECLARE_NONSTD(type)     __declspec(dllimport) type
#define CLHY_WD_DECLARE_DATA             __declspec(dllimport)
#endif
#endif

/**
 * Callback function used for watchdog.
 * @param state Watchdog state function. See @p CLHY_WATCHDOG_STATE_ .
 * @param data is what was passed to @p clhy_watchdog_register_callback function.
 * @param pool Temporary callback pool destroyed after the call.
 * @return KUDA_SUCCESS to continue calling this callback.
 */
typedef kuda_status_t clhy_watchdog_callback_fn_t(int state, void *data,
                                               kuda_pool_t *pool);

/**
 * Get watchdog instance.
 * @param watchdog Storage for watchdog instance.
 * @param name Watchdog name.
 * @param parent Non-zero to get the parent process watchdog instance.
 * @param singleton Non-zero to get the singleton watchdog instance.
 * @param p The pool to use.
 * @return KUDA_SUCCESS if all went well
 * @remark Use @p CLHY_WATCHDOG_DEFAULT to get default watchdog instance.
 *         If separate watchdog thread is needed provide unique name
 *         and function will create a new watchdog instance.
 *         Note that default client process watchdog works in singleton mode.
 */
KUDA_DECLARE_OPTIONAL_FN(kuda_status_t, clhy_watchdog_get_instance,
                        (clhy_watchdog_t **watchdog, const char *name, int parent,
                         int singleton, kuda_pool_t *p));

/**
 * Register watchdog callback.
 * @param watchdog Watchdog to use
 * @param interval Interval on which the callback function will execute.
 * @param callback  The function to call on watchdog event.
 * @param data The data to pass to the callback function.
 * @return KUDA_SUCCESS if all went well. KUDA_EEXIST if already registered.
 */
KUDA_DECLARE_OPTIONAL_FN(kuda_status_t, clhy_watchdog_register_callback,
                        (clhy_watchdog_t *watchdog, kuda_interval_time_t interval,
                         const void *data, clhy_watchdog_callback_fn_t *callback));

/**
 * Update registered watchdog callback interval.
 * @param w Watchdog to use
 * @param interval New interval on which the callback function will execute.
 * @param callback  The function to call on watchdog event.
 * @param data The data to pass to the callback function.
 * @return KUDA_SUCCESS if all went well. KUDA_EOF if callback was not found.
 */
KUDA_DECLARE_OPTIONAL_FN(kuda_status_t, clhy_watchdog_set_callback_interval,
                        (clhy_watchdog_t *w, kuda_interval_time_t interval,
                         const void *data, clhy_watchdog_callback_fn_t *callback));

/**
 * Watchdog require hook.
 * @param s The server record
 * @param name Watchdog name.
 * @param parent Non-zero to indicate the parent process watchdog mode.
 * @param singleton Non-zero to indicate the singleton watchdog mode.
 * @return OK to enable notifications from this watchdog, DECLINED otherwise.
 * @remark This is called in post config phase for all watchdog instances
 *         that have no callbacks registered. cAPIs using this hook
 *         should ensure that their post_config hook is called after watchdog
 *         post_config.
 */
KUDA_DECLARE_EXTERNAL_HOOK(clhy, CLHY_WD, int, watchdog_need, (server_rec *s,
                          const char *name,
                          int parent, int singleton))


/**
 * Watchdog initialize hook.
 * It is called after the watchdog thread is initialized.
 * @param s The server record
 * @param name Watchdog name.
 * @param pool The pool used to create the watchdog.
 */
KUDA_DECLARE_EXTERNAL_HOOK(clhy, CLHY_WD, int, watchdog_init, (
                          server_rec *s,
                          const char *name,
                          kuda_pool_t *pool))

/**
 * Watchdog terminate hook.
 * It is called when the watchdog thread is terminated.
 * @param s The server record
 * @param name Watchdog name.
 * @param pool The pool used to create the watchdog.
 */
KUDA_DECLARE_EXTERNAL_HOOK(clhy, CLHY_WD, int, watchdog_exit, (
                          server_rec *s,
                          const char *name,
                          kuda_pool_t *pool))

/**
 * Fixed interval watchdog hook.
 * It is called regularly on @p CLHY_WD_TM_INTERVAL interval.
 * @param s The server record
 * @param name Watchdog name.
 * @param pool Temporary pool destroyed after the call.
 */
KUDA_DECLARE_EXTERNAL_HOOK(clhy, CLHY_WD, int, watchdog_step, (
                          server_rec *s,
                          const char *name,
                          kuda_pool_t *pool))

#ifdef __cplusplus
}
#endif

#endif /* CAPI_WATCHDOG_H */
/** @} */
