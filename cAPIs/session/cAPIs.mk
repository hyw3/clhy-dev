capi_session.la: capi_session.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_session.lo $(CAPI_SESSION_LDADD)
capi_session_cookie.la: capi_session_cookie.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_session_cookie.lo $(CAPI_SESSION_COOKIE_LDADD)
capi_session_crypto.la: capi_session_crypto.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_session_crypto.lo $(CAPI_SESSION_CRYPTO_LDADD)
capi_session_dbd.la: capi_session_dbd.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_session_dbd.lo $(CAPI_SESSION_DBD_LDADD)
DISTCLEAN_TARGETS = cAPIs.mk
static = 
shared =  capi_session.la capi_session_cookie.la capi_session_crypto.la capi_session_dbd.la
