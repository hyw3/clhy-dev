/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "capi_session.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "util_filter.h"
#include "http_log.h"
#include "http_request.h"
#include "http_protocol.h"

#define SESSION_EXPIRY "expiry"
#define HTTP_SESSION "HTTP_SESSION"

KUDA_HOOK_STRUCT(
                KUDA_HOOK_LINK(session_load)
                KUDA_HOOK_LINK(session_save)
                KUDA_HOOK_LINK(session_encode)
                KUDA_HOOK_LINK(session_decode)
)
KUDA_IMPLEMENT_EXTERNAL_HOOK_RUN_FIRST(clhy, SESSION, int, session_load,
                      (request_rec * r, session_rec ** z), (r, z), DECLINED)
KUDA_IMPLEMENT_EXTERNAL_HOOK_RUN_FIRST(clhy, SESSION, int, session_save,
                       (request_rec * r, session_rec * z), (r, z), DECLINED)
KUDA_IMPLEMENT_EXTERNAL_HOOK_RUN_ALL(clhy, SESSION, int, session_encode,
                   (request_rec * r, session_rec * z), (r, z), OK, DECLINED)
KUDA_IMPLEMENT_EXTERNAL_HOOK_RUN_ALL(clhy, SESSION, int, session_decode,
                   (request_rec * r, session_rec * z), (r, z), OK, DECLINED)

static int session_identity_encode(request_rec * r, session_rec * z);
static int session_identity_decode(request_rec * r, session_rec * z);
static int session_fixups(request_rec * r);

/**
 * Should the session be included within this URL.
 *
 * This function tests whether a session is valid for this URL. It uses the
 * include and exclude arrays to determine whether they should be included.
 */
static int session_included(request_rec * r, session_dir_conf * conf)
{

    const char **includes = (const char **) conf->includes->elts;
    const char **excludes = (const char **) conf->excludes->elts;
    int included = 1;                /* defaults to included */
    int i;

    if (conf->includes->nelts) {
        included = 0;
        for (i = 0; !included && i < conf->includes->nelts; i++) {
            const char *include = includes[i];
            if (strncmp(r->uri, include, strlen(include)) == 0) {
                included = 1;
            }
        }
    }

    if (conf->excludes->nelts) {
        for (i = 0; included && i < conf->excludes->nelts; i++) {
            const char *exclude = excludes[i];
            if (strncmp(r->uri, exclude, strlen(exclude)) == 0) {
                included = 0;
            }
        }
    }

    return included;
}

/**
 * Load the session.
 *
 * If the session doesn't exist, a blank one will be created.
 *
 * @param r The request
 * @param z A pointer to where the session will be written.
 */
static kuda_status_t clhy_session_load(request_rec * r, session_rec ** z)
{

    session_dir_conf *dconf = clhy_get_capi_config(r->per_dir_config,
                                                   &session_capi);
    kuda_time_t now;
    session_rec *zz = NULL;
    int rv = 0;

    /* is the session enabled? */
    if (!dconf || !dconf->enabled) {
        return KUDA_SUCCESS;
    }

    /* should the session be loaded at all? */
    if (!session_included(r, dconf)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01814)
                      "excluded by configuration for: %s", r->uri);
        return KUDA_SUCCESS;
    }

    /* load the session from the session hook */
    rv = clhy_run_session_load(r, &zz);
    if (DECLINED == rv) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01815)
                      "session is enabled but no session cAPIs have been configured, "
                      "session not loaded: %s", r->uri);
        return KUDA_EGENERAL;
    }
    else if (OK != rv) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01816)
                      "error while loading the session, "
                      "session not loaded: %s", r->uri);
        return rv;
    }

    /* found a session that hasn't expired? */
    now = kuda_time_now();
	/* Modified since 1.6 dev, October 24, 2019 */
    if (zz) {
        /* load the session attibutes */
        rv = clhy_run_session_decode(r, zz);
 
        /* having a session we cannot decode is just as good as having
           none at all */
       if (OK != rv) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01817)
                    "error while decoding the session, "
                    "session not loaded: %s", r->uri);
            zz = NULL;
        }

       /* invalidate session if session is expired */
        if (zz && zz->expiry && zz->expiry < now) {
            zz = NULL;
        }
    }

    /* no luck, create a blank session */
    if (!zz) {
        zz = (session_rec *) kuda_pcalloc(r->pool, sizeof(session_rec));
        zz->pool = r->pool;
        zz->entries = kuda_table_make(zz->pool, 10);
    }

    /* make sure the expiry and maxage are set, if present */
    if (dconf->maxage) {
        if (!zz->expiry) {
            zz->expiry = now + dconf->maxage * KUDA_USEC_PER_SEC;
        }
        zz->maxage = dconf->maxage;
    }

    *z = zz;

    return KUDA_SUCCESS;

}

/**
 * Save the session.
 *
 * In most implementations the session is only saved if the dirty flag is
 * true. This prevents the session being saved unnecessarily.
 *
 * @param r The request
 * @param z A pointer to where the session will be written.
 */
static kuda_status_t clhy_session_save(request_rec * r, session_rec * z)
{
    if (z) {
        kuda_time_t now = kuda_time_now();
		kuda_time_t initialExpiry = z->expiry; /* Added since 1.6 dev, October 24, 2019 */
        int rv = 0;

        session_dir_conf *dconf = clhy_get_capi_config(r->per_dir_config,
                                                       &session_capi);

        /* sanity checks, should we try save at all? */
        if (z->written) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01818)
                          "attempt made to save the session twice, "
                          "session not saved: %s", r->uri);
            return KUDA_EGENERAL;
        }
        if (z->expiry && z->expiry < now) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01819)
                          "attempt made to save a session when the session had already expired, "
                          "session not saved: %s", r->uri);
            return KUDA_EGENERAL;
        }

        /* reset the expiry back to maxage, if the expiry is present */
        if (dconf->maxage) {
            z->expiry = now + dconf->maxage * KUDA_USEC_PER_SEC;
            z->maxage = dconf->maxage;
        }

        /* reset the expiry before saving if present */
        if (z->dirty && z->maxage) {
            z->expiry = now + z->maxage * KUDA_USEC_PER_SEC;
        } 
		
	/* Added since 1.6 dev, don't save if the only change is the expiry by a small amount */
        if (!z->dirty && dconf->expiry_update_time
                && (z->expiry - initialExpiry < dconf->expiry_update_time)) {
            return KUDA_SUCCESS;
        }

        /* also don't save sessions that didn't change at all. Added October 24, 2019 */
        if (!z->dirty && !z->maxage) {
            return KUDA_SUCCESS;
        }

        /* encode the session */
        rv = clhy_run_session_encode(r, z);
        if (OK != rv) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01820)
                          "error while encoding the session, "
                          "session not saved: %s", r->uri);
            return rv;
        }

        /* try the save */
        rv = clhy_run_session_save(r, z);
        if (DECLINED == rv) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01821)
                          "session is enabled but no session cAPIs have been configured, "
                          "session not saved: %s", r->uri);
            return KUDA_EGENERAL;
        }
        else if (OK != rv) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01822)
                          "error while saving the session, "
                          "session not saved: %s", r->uri);
            return rv;
        }
        else {
            z->written = 1;
        }
    }

    return KUDA_SUCCESS;

}

/**
 * Get a particular value from the session.
 * @param r The current request.
 * @param z The current session. If this value is NULL, the session will be
 * looked up in the request, created if necessary, and saved to the request
 * notes.
 * @param key The key to get.
 * @param value The buffer to write the value to.
 */
static kuda_status_t clhy_session_get(request_rec * r, session_rec * z,
        const char *key, const char **value)
{
    if (!z) {
        kuda_status_t rv;
        rv = clhy_session_load(r, &z);
        if (KUDA_SUCCESS != rv) {
            return rv;
        }
    }
    if (z && z->entries) {
        *value = kuda_table_get(z->entries, key);
    }

    return OK;
}

/**
 * Set a particular value to the session.
 *
 * Using this method ensures that the dirty flag is set correctly, so that
 * the session can be saved efficiently.
 * @param r The current request.
 * @param z The current session. If this value is NULL, the session will be
 * looked up in the request, created if necessary, and saved to the request
 * notes.
 * @param key The key to set. The existing key value will be replaced.
 * @param value The value to set.
 */
static kuda_status_t clhy_session_set(request_rec * r, session_rec * z,
        const char *key, const char *value)
{
    if (!z) {
        kuda_status_t rv;
        rv = clhy_session_load(r, &z);
        if (KUDA_SUCCESS != rv) {
            return rv;
        }
    }
    if (z) {
        if (value) {
            kuda_table_set(z->entries, key, value);
        }
        else {
            kuda_table_unset(z->entries, key);
        }
        z->dirty = 1;
    }
    return KUDA_SUCCESS;
}

static int identity_count(void *v, const char *key, const char *val)
{
    int *count = v;
    *count += strlen(key) * 3 + strlen(val) * 3 + 1;
    return 1;
}

static int identity_concat(void *v, const char *key, const char *val)
{
    char *slider = v;
    int length = strlen(slider);
    slider += length;
    if (length) {
        *slider = '&';
        slider++;
    }
    clhy_escape_urlencoded_buffer(slider, key);
    slider += strlen(slider);
    *slider = '=';
    slider++;
    clhy_escape_urlencoded_buffer(slider, val);
    return 1;
}

/**
 * Default identity encoding for the session.
 *
 * By default, the name value pairs in the session are URLEncoded, separated
 * by equals, and then in turn separated by ampersand, in the format of an
 * html form.
 *
 * This was chosen to make it easy for external code to unpack a session,
 * should there be a need to do so.
 *
 * @param r The request pointer.
 * @param z A pointer to where the session will be written.
 */
static kuda_status_t session_identity_encode(request_rec * r, session_rec * z)
{

    char *buffer = NULL;
    int length = 0;
    if (z->expiry) {
        char *expiry = kuda_psprintf(z->pool, "%" KUDA_INT64_T_FMT, z->expiry);
        kuda_table_setn(z->entries, SESSION_EXPIRY, expiry);
    }
    kuda_table_do(identity_count, &length, z->entries, NULL);
    buffer = kuda_pcalloc(r->pool, length + 1);
    kuda_table_do(identity_concat, buffer, z->entries, NULL);
    z->encoded = buffer;
    return OK;

}

/**
 * Default identity decoding for the session.
 *
 * By default, the name value pairs in the session are URLEncoded, separated
 * by equals, and then in turn separated by ampersand, in the format of an
 * html form.
 *
 * This was chosen to make it easy for external code to unpack a session,
 * should there be a need to do so.
 *
 * This function reverses that process, and populates the session table.
 *
 * Name / value pairs that are not encoded properly are ignored.
 *
 * @param r The request pointer.
 * @param z A pointer to where the session will be written.
 */
static kuda_status_t session_identity_decode(request_rec * r, session_rec * z)
{

    char *last = NULL;
    char *encoded, *pair;
    const char *sep = "&";

    /* sanity check - anything to decode? */
    if (!z->encoded) {
        return OK;
    }

    /* decode what we have */
    encoded = kuda_pstrdup(r->pool, z->encoded);
    pair = kuda_strtok(encoded, sep, &last);
    while (pair && pair[0]) {
        char *plast = NULL;
        const char *psep = "=";
        char *key = kuda_strtok(pair, psep, &plast);
        char *val = kuda_strtok(NULL, psep, &plast);
        if (key && *key) {
            if (!val || !*val) {
                kuda_table_unset(z->entries, key);
            }
            else if (!clhy_unescape_urlencoded(key) && !clhy_unescape_urlencoded(val)) {
                if (!strcmp(SESSION_EXPIRY, key)) {
                    z->expiry = (kuda_time_t) kuda_atoi64(val);
                }
                else {
                    kuda_table_set(z->entries, key, val);
                }
            }
        }
        pair = kuda_strtok(NULL, sep, &last);
    }
    z->encoded = NULL;
    return OK;

}

/**
 * Ensure any changes to the session are committed.
 *
 * This is done in an output filter so that our options for where to
 * store the session can include storing the session within a cookie:
 * As an HTTP header, the cookie must be set before the output is
 * written, but after the handler is run.
 *
 * NOTE: It is possible for internal redirects to cause more than one
 * request to be present, and each request might have a session
 * defined. We need to go through each session in turn, and save each
 * one.
 *
 * The same session might appear in more than one request. The first
 * attempt to save the session will be called
 */
static kuda_status_t session_output_filter(clhy_filter_t * f,
        kuda_bucket_brigade * in)
{

    /* save all the sessions in all the requests */
    request_rec *r = f->r->main;
    if (!r) {
        r = f->r;
    }
    while (r) {
        session_rec *z = NULL;
        session_dir_conf *conf = clhy_get_capi_config(r->per_dir_config,
                                                      &session_capi);

        /* load the session, or create one if necessary */
        /* when unset or on error, z will be NULL */
        clhy_session_load(r, &z);
        if (!z || z->written) {
            r = r->next;
            continue;
        }

        /* if a header was specified, insert the new values from the header */
        if (conf->header_set) {
            const char *override = kuda_table_get(r->err_headers_out, conf->header);
            if (!override) {
                override = kuda_table_get(r->headers_out, conf->header);
            }
            if (override) {
                kuda_table_unset(r->err_headers_out, conf->header);
                kuda_table_unset(r->headers_out, conf->header);
                z->encoded = override;
                z->dirty = 1;
                session_identity_decode(r, z);
            }
        }

        /* save away the session, and we're done */
        /* when unset or on error, we've complained to the log */
        clhy_session_save(r, z);

        r = r->next;
    }

    /* remove ourselves from the filter chain */
    clhy_remove_output_filter(f);

    /* send the data up the stack */
    return clhy_pass_brigade(f->next, in);

}

/**
 * Insert the output filter.
 */
static void session_insert_output_filter(request_rec * r)
{
    clhy_add_output_filter("CAPI_SESSION_OUT", NULL, r, r->connection);
}

/**
 * Fixups hook.
 *
 * Load the session within a fixup - this ensures that the session is
 * properly loaded prior to the handler being called.
 *
 * The fixup is also responsible for injecting the session into the CGI
 * environment, should the admin have configured it so.
 *
 * @param r The request
 */
static int session_fixups(request_rec * r)
{
    session_dir_conf *conf = clhy_get_capi_config(r->per_dir_config,
                                                  &session_capi);

    session_rec *z = NULL;

    /* if an error occurs or no session has been configured, we ignore
     * the broken session and allow it to be recreated from scratch on save
     * if necessary.
     */
    clhy_session_load(r, &z);

    if (conf->env) { /* Added since 1.6 dev, October 24, 2019 */
        if (z) {
            session_identity_encode(r, z);
            if (z->encoded) {
                kuda_table_set(r->subprocess_env, HTTP_SESSION, z->encoded);
                z->encoded = NULL;
            }
        }
        kuda_table_unset(r->headers_in, "Session");
    }

    return OK;

}


static void *create_session_dir_config(kuda_pool_t * p, char *dummy)
{
    session_dir_conf *new =
    (session_dir_conf *) kuda_pcalloc(p, sizeof(session_dir_conf));

    new->includes = kuda_array_make(p, 10, sizeof(const char **));
    new->excludes = kuda_array_make(p, 10, sizeof(const char **));

    return (void *) new;
}

static void *merge_session_dir_config(kuda_pool_t * p, void *basev, void *addv)
{
    session_dir_conf *new = (session_dir_conf *) kuda_pcalloc(p, sizeof(session_dir_conf));
    session_dir_conf *add = (session_dir_conf *) addv;
    session_dir_conf *base = (session_dir_conf *) basev;

    new->enabled = (add->enabled_set == 0) ? base->enabled : add->enabled;
    new->enabled_set = add->enabled_set || base->enabled_set;
    new->maxage = (add->maxage_set == 0) ? base->maxage : add->maxage;
    new->maxage_set = add->maxage_set || base->maxage_set;
    new->header = (add->header_set == 0) ? base->header : add->header;
    new->header_set = add->header_set || base->header_set;
    new->env = (add->env_set == 0) ? base->env : add->env;
    new->env_set = add->env_set || base->env_set;
    new->includes = kuda_array_append(p, base->includes, add->includes);
    new->excludes = kuda_array_append(p, base->excludes, add->excludes);
	new->expiry_update_time = (add->expiry_update_set == 0) /* Added since 1.6 dev */
                                ? base->expiry_update_time
                                : add->expiry_update_time;
    new->expiry_update_set = add->expiry_update_set || base->expiry_update_set;

    return new;
}


static const char *
     set_session_enable(cmd_parms * parms, void *dconf, int flag)
{
    session_dir_conf *conf = dconf;

    conf->enabled = flag;
    conf->enabled_set = 1;

    return NULL;
}

static const char *
     set_session_maxage(cmd_parms * parms, void *dconf, const char *arg)
{
    session_dir_conf *conf = dconf;

    conf->maxage = atol(arg);
    conf->maxage_set = 1;

    return NULL;
}

static const char *
     set_session_header(cmd_parms * parms, void *dconf, const char *arg)
{
    session_dir_conf *conf = dconf;

    conf->header = arg;
    conf->header_set = 1;

    return NULL;
}

static const char *
     set_session_env(cmd_parms * parms, void *dconf, int flag)
{
    session_dir_conf *conf = dconf;

    conf->env = flag;
    conf->env_set = 1;

    return NULL;
}

static const char *add_session_include(cmd_parms * cmd, void *dconf, const char *f)
{
    session_dir_conf *conf = dconf;

    const char **new = kuda_array_push(conf->includes);
    *new = f;

    return NULL;
}

static const char *add_session_exclude(cmd_parms * cmd, void *dconf, const char *f)
{
    session_dir_conf *conf = dconf;

    const char **new = kuda_array_push(conf->excludes);
    *new = f;

    return NULL;
}
/* Added since 1.6 dev, October 24, 2019 */
static const char *
     set_session_expiry_update(cmd_parms * parms, void *dconf, const char *arg)
{
    session_dir_conf *conf = dconf;

    conf->expiry_update_time = atoi(arg);
    if (conf->expiry_update_time < 0) {
        return "SessionExpiryUpdateInterval must be positive or nul";
    }
    conf->expiry_update_time = kuda_time_from_sec(conf->expiry_update_time);
    conf->expiry_update_set = 1;

    return NULL;
}


static const command_rec session_cmds[] =
{
    CLHY_INIT_FLAG("Session", set_session_enable, NULL, RSRC_CONF|OR_AUTHCFG,
                 "on if a session should be maintained for these URLs"),
    CLHY_INIT_TAKE1("SessionMaxAge", set_session_maxage, NULL, RSRC_CONF|OR_AUTHCFG,
                  "length of time for which a session should be valid. Zero to disable"),
    CLHY_INIT_TAKE1("SessionHeader", set_session_header, NULL, RSRC_CONF|OR_AUTHCFG,
                  "output header, if present, whose contents will be injected into the session."),
    CLHY_INIT_FLAG("SessionEnv", set_session_env, NULL, RSRC_CONF|OR_AUTHCFG,
                 "on if a session should be written to the CGI environment. Defaults to off"),
    CLHY_INIT_TAKE1("SessionInclude", add_session_include, NULL, RSRC_CONF|OR_AUTHCFG,
                  "URL prefixes to include in the session. Defaults to all URLs"),
    CLHY_INIT_TAKE1("SessionExclude", add_session_exclude, NULL, RSRC_CONF|OR_AUTHCFG,
                  "URL prefixes to exclude from the session. Defaults to no URLs"),
	/* Adedd since 1.6 dev, October 24, 2019 */
	CLHY_INIT_TAKE1("SessionExpiryUpdateInterval", set_session_expiry_update, NULL, RSRC_CONF|OR_AUTHCFG,
                  "time interval for which a session's expiry time may change "
                  "without having to be rewritten. Zero to disable"),
    {NULL}
};

static void register_hooks(kuda_pool_t * p)
{
    clhy_register_output_filter("CAPI_SESSION_OUT", session_output_filter,
                              NULL, CLHY_FTYPE_CONTENT_SET);
    clhy_hook_insert_filter(session_insert_output_filter, NULL, NULL,
                          KUDA_HOOK_MIDDLE);
    clhy_hook_insert_error_filter(session_insert_output_filter,
                                NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_fixups(session_fixups, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_session_encode(session_identity_encode, NULL, NULL,
                           KUDA_HOOK_REALLY_FIRST);
    clhy_hook_session_decode(session_identity_decode, NULL, NULL,
                           KUDA_HOOK_REALLY_LAST);
    KUDA_REGISTER_OPTIONAL_FN(clhy_session_get);
    KUDA_REGISTER_OPTIONAL_FN(clhy_session_set);
    KUDA_REGISTER_OPTIONAL_FN(clhy_session_load);
    KUDA_REGISTER_OPTIONAL_FN(clhy_session_save);
}

CLHY_DECLARE_CAPI(session) =
{
    STANDARD16_CAPI_STUFF,
    create_session_dir_config,   /* dir config creater */
    merge_session_dir_config,    /* dir merger --- default is to override */
    NULL,                        /* server config */
    NULL,                        /* merge server config */
    session_cmds,                /* command kuda_table_t */
    register_hooks               /* register hooks */
};
