dnl cAPIs enabled in this directory by default

if test -z "$enable_session" ; then
  session_capis_enable=most
else
  session_capis_enable=$enable_session
fi

dnl Session

dnl CLHYKUDEL_CAPI(name, helptext[, objects[, structname[, default[, config]]]])

CLHYKUDEL_CAPIPATH_INIT(session)

dnl Session cAPIs; cAPIs that are capable of storing key value pairs in
dnl various places, such as databases, LDAP, or cookies.
dnl
session_cookie_objects='capi_session_cookie.lo'
session_crypto_objects='capi_session_crypto.lo'
session_dbd_objects='capi_session_dbd.lo'

case "$host" in
  *os2*)
    # OS2 DLLs must resolve all symbols at build time
    # and we need some from main session cAPI
    session_cookie_objects="$session_cookie_objects capi_session.la"
    session_crypto_objects="$session_crypto_objects capi_session.la"
    session_dbd_objects="$session_dbd_objects capi_session.la"
    ;;
esac

CLHYKUDEL_CAPI(session, session cAPI, , , most)
CLHYKUDEL_CAPI(session_cookie, session cookie cAPI, $session_cookie_objects, , $session_capis_enable,,session)

if test "$enable_session_crypto" != ""; then
  session_capis_enable_crypto=$enable_session_crypto
else
  session_capis_enable_crypto=$session_capis_enable
fi
if test "$session_capis_enable_crypto" != "no"; then
  saved_CPPFLAGS="$CPPFLAGS"
  CPPFLAGS="$CPPFLAGS $KUDA_INCLUDES $KUDELMAN_INCLUDES"
  AC_TRY_COMPILE([#include <kuda_crypto.h>],[
#if KUDELMAN_HAVE_CRYPTO == 0
#error no crypto support
#endif
  ], [clhy_HAVE_KUDA_CRYPTO="yes"], [clhy_HAVE_KUDA_CRYPTO="no"])
  CPPFLAGS="$saved_CPPFLAGS"
  if test $clhy_HAVE_KUDA_CRYPTO = "no"; then
    AC_MSG_WARN([Your KUDA does not include SSL/EVP support. To enable it: configure --with-crypto])
    if test "$enable_session_crypto" != "" -a "$enable_session_crypto" != "no"; then
        AC_MSG_ERROR([capi_session_crypto cannot be enabled])
    fi
    session_capis_enable_crypto="no"
  fi
fi
CLHYKUDEL_CAPI(session_crypto, session crypto cAPI, $session_crypto_objects, , $session_capis_enable_crypto, [
if test "$session_capis_enable_crypto" = "no" ; then
  enable_session_crypto=no
fi
],session)

CLHYKUDEL_CAPI(session_dbd, session dbd cAPI, $session_dbd_objects, , $session_capis_enable,,session)

KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

CLHYKUDEL_CAPIPATH_FINISH

