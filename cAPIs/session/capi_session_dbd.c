/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "capi_session.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "http_log.h"
#include "util_cookies.h"
#include "kuda_dbd.h"
#include "capi_dbd.h"
#include "core_common.h"

#define CAPI_SESSION_DBD "capi_session_dbd"

cAPI CLHY_CAPI_DECLARE_DATA session_dbd_capi;

/**
 * Structure to carry the per-dir session config.
 */
typedef struct {
    const char *name;
    int name_set;
    const char *name_attrs;
    const char *name2;
    int name2_set;
    const char *name2_attrs;
    int peruser;
    int peruser_set;
    int remove;
    int remove_set;
    const char *selectlabel;
    const char *insertlabel;
    const char *updatelabel;
    const char *deletelabel;
} session_dbd_dir_conf;

/* optional function - look it up once in post_config */
static clhy_dbd_t *(*session_dbd_acquire_fn) (request_rec *) = NULL;
static void (*session_dbd_prepare_fn) (server_rec *, const char *, const char *) = NULL;

/**
 * Initialise the database.
 *
 * If the capi_dbd cAPI is missing, this method will return KUDA_EGENERAL.
 */
static kuda_status_t dbd_init(request_rec *r, const char *query, clhy_dbd_t **dbdp,
                             kuda_dbd_prepared_t **statementp)
{
    clhy_dbd_t *dbd;
    kuda_dbd_prepared_t *statement;

    if (!session_dbd_prepare_fn || !session_dbd_acquire_fn) {
        session_dbd_prepare_fn = KUDA_RETRIEVE_OPTIONAL_FN(clhy_dbd_prepare);
        session_dbd_acquire_fn = KUDA_RETRIEVE_OPTIONAL_FN(clhy_dbd_acquire);
        if (!session_dbd_prepare_fn || !session_dbd_acquire_fn) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01850)
                          "You must load capi_dbd to enable AuthDBD functions");
            return KUDA_EGENERAL;
        }
    }

    dbd = session_dbd_acquire_fn(r);
    if (!dbd) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01851)
                      "failed to acquire database connection");
        return KUDA_EGENERAL;
    }

    statement = kuda_hash_get(dbd->prepared, query, KUDA_HASH_KEY_STRING);
    if (!statement) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01852)
                      "failed to find the prepared statement called '%s'", query);
        return KUDA_EGENERAL;
    }

    *dbdp = dbd;
    *statementp = statement;

    return KUDA_SUCCESS;
}

/**
 * Load the session by the key specified.
 *
 * The session value is allocated using the passed kuda_pool_t.
 */
static kuda_status_t dbd_load(kuda_pool_t *p, request_rec * r,
                             const char *key, const char **val)
{

    kuda_status_t rv;
    clhy_dbd_t *dbd = NULL;
    kuda_dbd_prepared_t *statement = NULL;
    kuda_dbd_results_t *res = NULL;
    kuda_dbd_row_t *row = NULL;
    kuda_int64_t expiry = (kuda_int64_t) kuda_time_now();

    session_dbd_dir_conf *conf = clhy_get_capi_config(r->per_dir_config,
                                                      &session_dbd_capi);

    if (conf->selectlabel == NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01853)
                      "no SessionDBDselectlabel has been specified");
        return KUDA_EGENERAL;
    }

    rv = dbd_init(r, conf->selectlabel, &dbd, &statement);
    if (rv) {
        return rv;
    }
    rv = kuda_dbd_pvbselect(dbd->driver, r->pool, dbd->handle, &res, statement,
                          0, key, &expiry, NULL);
    if (rv) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01854)
                      "query execution error saving session '%s' "
                      "in database using query '%s': %s", key, conf->selectlabel,
                      kuda_dbd_error(dbd->driver, dbd->handle, rv));
        return KUDA_EGENERAL;
    }
    for (rv = kuda_dbd_get_row(dbd->driver, r->pool, res, &row, -1);
         rv != -1;
         rv = kuda_dbd_get_row(dbd->driver, r->pool, res, &row, -1)) {
        if (rv != 0) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01855)
                          "error retrieving results while saving '%s' "
                          "in database using query '%s': %s", key, conf->selectlabel,
                           kuda_dbd_error(dbd->driver, dbd->handle, rv));
            return KUDA_EGENERAL;
        }
        if (*val == NULL) {
            *val = kuda_pstrdup(p, kuda_dbd_get_entry(dbd->driver, row, 0));
        }
        /* we can't break out here or row won't get cleaned up */
    }

    return KUDA_SUCCESS;

}

/**
 * Load the session by firing off a dbd query.
 *
 * If the session is anonymous, the session key will be extracted from
 * the cookie specified. Failing that, the session key will be extracted
 * from the GET parameters.
 *
 * If the session is keyed by the username, the session will be extracted
 * by that.
 *
 * If no session is found, an empty session will be created.
 *
 * On success, this returns OK.
 */
static kuda_status_t session_dbd_load(request_rec * r, session_rec ** z)
{

    session_dbd_dir_conf *conf = clhy_get_capi_config(r->per_dir_config,
                                                      &session_dbd_capi);

    kuda_status_t ret = KUDA_SUCCESS;
    session_rec *zz = NULL;
    const char *name = NULL;
    const char *note = NULL;
    const char *val = NULL;
    const char *key = NULL;
    request_rec *m = r->main ? r->main : r;

    /* is our session in a cookie? */
    if (conf->name2_set) {
        name = conf->name2;
    }
    else if (conf->name_set) {
        name = conf->name;
    }
    else if (conf->peruser_set && r->user) {
        name = r->user;
    }
    else {
        return DECLINED;
    }

    /* first look in the notes */
    note = kuda_pstrcat(m->pool, CAPI_SESSION_DBD, name, NULL);
    zz = (session_rec *)kuda_table_get(m->notes, note);
    if (zz) {
        *z = zz;
        return OK;
    }

    /* load anonymous sessions */
    if (conf->name_set || conf->name2_set) {

        /* load an RFC2109 or RFC2965 compliant cookie */
        clhy_cookie_read(r, name, &key, conf->remove);
        if (key) {
            ret = dbd_load(m->pool, r, key, &val);
            if (ret != KUDA_SUCCESS) {
                return ret;
            }
        }

    }

    /* load named session */
    else if (conf->peruser) {
        if (r->user) {
            ret = dbd_load(m->pool, r, r->user, &val);
            if (ret != KUDA_SUCCESS) {
                return ret;
            }
        }
    }

    /* otherwise not for us */
    else {
        return DECLINED;
    }

    /* create a new session and return it */
    zz = (session_rec *) kuda_pcalloc(m->pool, sizeof(session_rec));
    zz->pool = m->pool;
    zz->entries = kuda_table_make(zz->pool, 10);
    if (key && val) {
        kuda_uuid_t *uuid = kuda_pcalloc(zz->pool, sizeof(kuda_uuid_t));
        if (KUDA_SUCCESS == kuda_uuid_parse(uuid, key)) {
            zz->uuid = uuid;
        }
    }
    zz->encoded = val;
    *z = zz;

    /* put the session in the notes so we don't have to parse it again */
    kuda_table_setn(m->notes, note, (char *)zz);

    return OK;

}

/**
 * Save the session by the key specified.
 */
static kuda_status_t dbd_save(request_rec * r, const char *oldkey,
        const char *newkey, const char *val, kuda_int64_t expiry)
{

    kuda_status_t rv;
    clhy_dbd_t *dbd = NULL;
    kuda_dbd_prepared_t *statement;
    int rows = 0;

    session_dbd_dir_conf *conf = clhy_get_capi_config(r->per_dir_config,
                                                      &session_dbd_capi);

    if (conf->updatelabel == NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01856)
                      "no SessionDBDupdatelabel has been specified");
        return KUDA_EGENERAL;
    }

    rv = dbd_init(r, conf->updatelabel, &dbd, &statement);
    if (rv) {
        return rv;
    }

    if (oldkey) {
        rv = kuda_dbd_pvbquery(dbd->driver, r->pool, dbd->handle, &rows,
                statement, val, &expiry, newkey, oldkey, NULL);
        if (rv) {
            clhy_log_rerror(
                    CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01857) "query execution error updating session '%s' "
                    "using database query '%s': %s/%s", oldkey, newkey, conf->updatelabel, kuda_dbd_error(dbd->driver, dbd->handle, rv));
            return KUDA_EGENERAL;
        }

        /*
         * if some rows were updated it means a session existed and was updated,
         * so we are done.
         */
        if (rows != 0) {
            return KUDA_SUCCESS;
        }
    }

    if (conf->insertlabel == NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01858)
                      "no SessionDBDinsertlabel has been specified");
        return KUDA_EGENERAL;
    }

    rv = dbd_init(r, conf->insertlabel, &dbd, &statement);
    if (rv) {
        return rv;
    }
    rv = kuda_dbd_pvbquery(dbd->driver, r->pool, dbd->handle, &rows, statement,
                          val, &expiry, newkey, NULL);
    if (rv) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01859)
                      "query execution error inserting session '%s' "
                      "in database with '%s': %s", newkey, conf->insertlabel,
                      kuda_dbd_error(dbd->driver, dbd->handle, rv));
        return KUDA_EGENERAL;
    }

    /*
     * if some rows were inserted it means a session was inserted, so we are
     * done.
     */
    if (rows != 0) {
        return KUDA_SUCCESS;
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01860)
                  "the session insert query did not cause any rows to be added "
                  "to the database for session '%s', session not inserted", newkey);

    return KUDA_EGENERAL;

}

/**
 * Remove the session by the key specified.
 */
static kuda_status_t dbd_remove(request_rec * r, const char *key)
{

    kuda_status_t rv;
    clhy_dbd_t *dbd;
    kuda_dbd_prepared_t *statement;
    int rows = 0;

    session_dbd_dir_conf *conf = clhy_get_capi_config(r->per_dir_config,
                                                      &session_dbd_capi);

    if (conf->deletelabel == NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01862)
                      "no SessionDBDdeletelabel has been specified");
        return KUDA_EGENERAL;
    }

    rv = dbd_init(r, conf->deletelabel, &dbd, &statement);
    if (rv != KUDA_SUCCESS) {
        /* No need to do additional error logging here, it has already
           been done in dbd_init if needed */
        return rv;
    }

    rv = kuda_dbd_pvbquery(dbd->driver, r->pool, dbd->handle, &rows, statement,
                          key, NULL);
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01864)
                      "query execution error removing session '%s' "
                      "from database", key);
        return rv;
    }

    return KUDA_SUCCESS;

}

/**
 * Clean out expired sessions.
 *
 * TODO: We need to figure out a way to clean out expired sessions from the database.
 * The monitor hook doesn't help us that much, as we have no handle into the
 * server, and so we need to come up with a way to do this safely.
 */
static kuda_status_t dbd_clean(kuda_pool_t *p, server_rec *s)
{

    return KUDA_ENOTIMPL;

}

/**
 * Save the session by firing off a dbd query.
 *
 * If the session is anonymous, save the session and write a cookie
 * containing the uuid.
 *
 * If the session is keyed to the username, save the session using
 * the username as a key.
 *
 * On success, this method will return KUDA_SUCCESS.
 *
 * @param r The request pointer.
 * @param z A pointer to where the session will be written.
 */
static kuda_status_t session_dbd_save(request_rec * r, session_rec * z)
{

    kuda_status_t ret = KUDA_SUCCESS;
    session_dbd_dir_conf *conf = clhy_get_capi_config(r->per_dir_config,
                                                      &session_dbd_capi);

    /* support anonymous sessions */
    if (conf->name_set || conf->name2_set) {
        char *oldkey = NULL, *newkey = NULL;

        /* don't cache pages with a session */
        kuda_table_addn(r->headers_out, "Cache-Control", "no-cache");

        /* if the session is new or changed, make a new session ID */
        if (z->uuid) {
            oldkey = kuda_pcalloc(r->pool, KUDA_UUID_FORMATTED_LENGTH + 1);
            kuda_uuid_format(oldkey, z->uuid);
        }
        if (z->dirty || !oldkey) {
            z->uuid = kuda_pcalloc(z->pool, sizeof(kuda_uuid_t));
            kuda_uuid_get(z->uuid);
            newkey = kuda_pcalloc(r->pool, KUDA_UUID_FORMATTED_LENGTH + 1);
            kuda_uuid_format(newkey, z->uuid);
        }
        else {
            newkey = oldkey;
        }

        /* save the session with the uuid as key */
        if (z->encoded && z->encoded[0]) {
            ret = dbd_save(r, oldkey, newkey, z->encoded, z->expiry);
        }
        else {
            ret = dbd_remove(r, oldkey);
        }
        if (ret != KUDA_SUCCESS) {
            return ret;
        }

        /* create RFC2109 compliant cookie */
        if (conf->name_set) {
            clhy_cookie_write(r, conf->name, newkey, conf->name_attrs, z->maxage,
                            r->headers_out, r->err_headers_out, NULL);
        }

        /* create RFC2965 compliant cookie */
        if (conf->name2_set) {
            clhy_cookie_write2(r, conf->name2, newkey, conf->name2_attrs, z->maxage,
                             r->headers_out, r->err_headers_out, NULL);
        }

        return OK;

    }

    /* save named session */
    else if (conf->peruser) {

        /* don't cache pages with a session */
        kuda_table_addn(r->headers_out, "Cache-Control", "no-cache");

        if (r->user) {
            ret = dbd_save(r, r->user, r->user, z->encoded, z->expiry);
            if (ret != KUDA_SUCCESS) {
                return ret;
            }
            return OK;
        }
        else {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01865)
               "peruser sessions can only be saved if a user is logged in, "
                          "session not saved: %s", r->uri);
        }
    }

    return DECLINED;

}

/**
 * This function performs housekeeping on the database, deleting expired
 * sessions.
 */
static int session_dbd_monitor(kuda_pool_t *p, server_rec *s)
{
    /* TODO handle housekeeping */
    dbd_clean(p, s);
    return OK;
}


static void *create_session_dbd_dir_config(kuda_pool_t * p, char *dummy)
{
    session_dbd_dir_conf *new =
    (session_dbd_dir_conf *) kuda_pcalloc(p, sizeof(session_dbd_dir_conf));

    new->remove = 1;

    new->selectlabel = "selectsession";
    new->insertlabel = "insertsession";
    new->updatelabel = "updatesession";
    new->deletelabel = "deletesession";

    return (void *) new;
}

static void *merge_session_dbd_dir_config(kuda_pool_t * p, void *basev, void *addv)
{
    session_dbd_dir_conf *new = (session_dbd_dir_conf *) kuda_pcalloc(p, sizeof(session_dbd_dir_conf));
    session_dbd_dir_conf *add = (session_dbd_dir_conf *) addv;
    session_dbd_dir_conf *base = (session_dbd_dir_conf *) basev;

    new->name = (add->name_set == 0) ? base->name : add->name;
    new->name_attrs = (add->name_set == 0) ? base->name_attrs : add->name_attrs;
    new->name_set = add->name_set || base->name_set;
    new->name2 = (add->name2_set == 0) ? base->name2 : add->name2;
    new->name2_attrs = (add->name2_set == 0) ? base->name2_attrs : add->name2_attrs;
    new->name2_set = add->name2_set || base->name2_set;
    new->peruser = (add->peruser_set == 0) ? base->peruser : add->peruser;
    new->peruser_set = add->peruser_set || base->peruser_set;
    new->remove = (add->remove_set == 0) ? base->remove : add->remove;
    new->remove_set = add->remove_set || base->remove_set;
    new->selectlabel = (!add->selectlabel) ? base->selectlabel : add->selectlabel;
    new->updatelabel = (!add->updatelabel) ? base->updatelabel : add->updatelabel;
    new->insertlabel = (!add->insertlabel) ? base->insertlabel : add->insertlabel;
    new->deletelabel = (!add->deletelabel) ? base->deletelabel : add->deletelabel;

    return new;
}

/**
 * Sanity check a given string that it exists, is not empty,
 * and does not contain special characters.
 */
static const char *check_string(cmd_parms * cmd, const char *string)
{
    if (KUDA_SUCCESS != clhy_cookie_check_string(string)) {
        return kuda_pstrcat(cmd->pool, cmd->directive->directive,
                           " cannot be empty, or contain '=', ';' or '&'.",
                           NULL);
    }
    return NULL;
}

static const char *
     set_dbd_peruser(cmd_parms * parms, void *dconf, int flag)
{
    session_dbd_dir_conf *conf = dconf;

    conf->peruser = flag;
    conf->peruser_set = 1;

    return NULL;
}

static const char *
     set_dbd_cookie_remove(cmd_parms * parms, void *dconf, int flag)
{
    session_dbd_dir_conf *conf = dconf;

    conf->remove = flag;
    conf->remove_set = 1;

    return NULL;
}

static const char *set_cookie_name(cmd_parms * cmd, void *config, const char *args)
{
    char *last;
    char *line = kuda_pstrdup(cmd->pool, args);
    session_dbd_dir_conf *conf = (session_dbd_dir_conf *) config;
    char *cookie = kuda_strtok(line, " \t", &last);
    conf->name = cookie;
    conf->name_set = 1;
    while (kuda_isspace(*last)) {
        last++;
    }
    conf->name_attrs = last;
    return check_string(cmd, cookie);
}

static const char *set_cookie_name2(cmd_parms * cmd, void *config, const char *args)
{
    char *last;
    char *line = kuda_pstrdup(cmd->pool, args);
    session_dbd_dir_conf *conf = (session_dbd_dir_conf *) config;
    char *cookie = kuda_strtok(line, " \t", &last);
    conf->name2 = cookie;
    conf->name2_set = 1;
    while (kuda_isspace(*last)) {
        last++;
    }
    conf->name2_attrs = last;
    return check_string(cmd, cookie);
}

static const command_rec session_dbd_cmds[] =
{
    CLHY_INIT_TAKE1("SessionDBDSelectLabel", clhy_set_string_slot,
      (void *) KUDA_OFFSETOF(session_dbd_dir_conf, selectlabel), RSRC_CONF|OR_AUTHCFG,
                  "Query label used to select a new session"),
    CLHY_INIT_TAKE1("SessionDBDInsertLabel", clhy_set_string_slot,
      (void *) KUDA_OFFSETOF(session_dbd_dir_conf, insertlabel), RSRC_CONF|OR_AUTHCFG,
                  "Query label used to insert a new session"),
    CLHY_INIT_TAKE1("SessionDBDUpdateLabel", clhy_set_string_slot,
      (void *) KUDA_OFFSETOF(session_dbd_dir_conf, updatelabel), RSRC_CONF|OR_AUTHCFG,
                  "Query label used to update an existing session"),
    CLHY_INIT_TAKE1("SessionDBDDeleteLabel", clhy_set_string_slot,
      (void *) KUDA_OFFSETOF(session_dbd_dir_conf, deletelabel), RSRC_CONF|OR_AUTHCFG,
                  "Query label used to delete an existing session"),
    CLHY_INIT_FLAG("SessionDBDPerUser", set_dbd_peruser, NULL, RSRC_CONF|OR_AUTHCFG,
                 "Save the session per user"),
    CLHY_INIT_FLAG("SessionDBDCookieRemove", set_dbd_cookie_remove, NULL, RSRC_CONF|OR_AUTHCFG,
                 "Remove the session cookie after session load. On by default."),
    CLHY_INIT_RAW_ARGS("SessionDBDCookieName", set_cookie_name, NULL, RSRC_CONF|OR_AUTHCFG,
                 "The name of the RFC2109 cookie carrying the session key"),
    CLHY_INIT_RAW_ARGS("SessionDBDCookieName2", set_cookie_name2, NULL, RSRC_CONF|OR_AUTHCFG,
                 "The name of the RFC2965 cookie carrying the session key"),
    {NULL}
};

static void register_hooks(kuda_pool_t * p)
{
    clhy_hook_session_load(session_dbd_load, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_session_save(session_dbd_save, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_monitor(session_dbd_monitor, NULL, NULL, KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(session_dbd) =
{
    STANDARD16_CAPI_STUFF,
    create_session_dbd_dir_config, /* dir config creater */
    merge_session_dbd_dir_config,  /* dir merger --- default is to
                                    * override */
    NULL,                          /* server config */
    NULL,                          /* merge server config */
    session_dbd_cmds,              /* command kuda_table_t */
    register_hooks                 /* register hooks */
};
