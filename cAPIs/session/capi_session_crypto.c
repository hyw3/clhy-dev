/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "capi_session.h"
#include "kudelman_version.h"
#include "kuda_base64.h"                /* for kuda_base64_decode et al */
#include "kuda_lib.h"
#include "kuda_md5.h"
#include "kuda_strings.h"
#include "http_log.h"
#include "http_core.h"

#if KUDELMAN_MAJOR_VERSION == 1 && KUDELMAN_MINOR_VERSION < 4

#error session_crypto_capi requires KUDELMAN v1.4.0 or later

#elif KUDELMAN_HAVE_CRYPTO == 0

#error Crypto support must be enabled in KUDA

#else

#include "kuda_crypto.h"                /* for kuda_*_crypt et al */

#define CRYPTO_KEY "session_crypto_context"

cAPI CLHY_CAPI_DECLARE_DATA session_crypto_capi;

/**
 * Structure to carry the per-dir session config.
 */
typedef struct {
    kuda_array_header_t *passphrases;
    int passphrases_set;
    const char *cipher;
    int cipher_set;
} session_crypto_dir_conf;

/**
 * Structure to carry the server wide session config.
 */
typedef struct {
    const char *library;
    const char *params;
    int library_set;
} session_crypto_conf;

/* Wrappers around kuda_siphash24() and kuda_crypto_equals(),
 * available in KUDELMAN-1.6/KUDA-2.0 only.
 */
#if KUDELMAN_MAJOR_VERSION > 1 || (KUDELMAN_MAJOR_VERSION == 1 && KUDELMAN_MINOR_VERSION >= 6)

#include "kuda_siphash.h"

#define CLHY_SIPHASH_DSIZE    KUDA_SIPHASH_DSIZE
#define CLHY_SIPHASH_KSIZE    KUDA_SIPHASH_KSIZE
#define clhy_siphash24_auth   kuda_siphash24_auth

#define clhy_crypto_equals    kuda_crypto_equals

#else

#define CLHY_SIPHASH_DSIZE    8
#define CLHY_SIPHASH_KSIZE    16

#define ROTL64(x, n) (((x) << (n)) | ((x) >> (64 - (n))))

#define U8TO64_LE(p) \
    (((kuda_uint64_t)((p)[0])      ) | \
     ((kuda_uint64_t)((p)[1]) <<  8) | \
     ((kuda_uint64_t)((p)[2]) << 16) | \
     ((kuda_uint64_t)((p)[3]) << 24) | \
     ((kuda_uint64_t)((p)[4]) << 32) | \
     ((kuda_uint64_t)((p)[5]) << 40) | \
     ((kuda_uint64_t)((p)[6]) << 48) | \
     ((kuda_uint64_t)((p)[7]) << 56))

#define U64TO8_LE(p, v) \
do { \
    (p)[0] = (unsigned char)((v)      ); \
    (p)[1] = (unsigned char)((v) >>  8); \
    (p)[2] = (unsigned char)((v) >> 16); \
    (p)[3] = (unsigned char)((v) >> 24); \
    (p)[4] = (unsigned char)((v) >> 32); \
    (p)[5] = (unsigned char)((v) >> 40); \
    (p)[6] = (unsigned char)((v) >> 48); \
    (p)[7] = (unsigned char)((v) >> 56); \
} while (0)

#define SIPROUND() \
do { \
    v0 += v1; v1=ROTL64(v1,13); v1 ^= v0; v0=ROTL64(v0,32); \
    v2 += v3; v3=ROTL64(v3,16); v3 ^= v2; \
    v0 += v3; v3=ROTL64(v3,21); v3 ^= v0; \
    v2 += v1; v1=ROTL64(v1,17); v1 ^= v2; v2=ROTL64(v2,32); \
} while(0)

static kuda_uint64_t clhy_siphash24(const void *src, kuda_size_t len,
                                 const unsigned char key[CLHY_SIPHASH_KSIZE])
{
    const unsigned char *ptr, *end;
    kuda_uint64_t v0, v1, v2, v3, m;
    kuda_uint64_t k0, k1;
    unsigned int rem;

    k0 = U8TO64_LE(key + 0);
    k1 = U8TO64_LE(key + 8);
    v3 = k1 ^ (kuda_uint64_t)0x7465646279746573ULL;
    v2 = k0 ^ (kuda_uint64_t)0x6c7967656e657261ULL;
    v1 = k1 ^ (kuda_uint64_t)0x646f72616e646f6dULL;
    v0 = k0 ^ (kuda_uint64_t)0x736f6d6570736575ULL;

    rem = (unsigned int)(len & 0x7);
    for (ptr = src, end = ptr + len - rem; ptr < end; ptr += 8) {
        m = U8TO64_LE(ptr);
        v3 ^= m;
        SIPROUND();
        SIPROUND();
        v0 ^= m;
    }
    m = (kuda_uint64_t)(len & 0xff) << 56;
    switch (rem) {
        case 7: m |= (kuda_uint64_t)ptr[6] << 48;
        case 6: m |= (kuda_uint64_t)ptr[5] << 40;
        case 5: m |= (kuda_uint64_t)ptr[4] << 32;
        case 4: m |= (kuda_uint64_t)ptr[3] << 24;
        case 3: m |= (kuda_uint64_t)ptr[2] << 16;
        case 2: m |= (kuda_uint64_t)ptr[1] << 8;
        case 1: m |= (kuda_uint64_t)ptr[0];
        case 0: break;
    }
    v3 ^= m;
    SIPROUND();
    SIPROUND();
    v0 ^= m;

    v2 ^= 0xff;
    SIPROUND();
    SIPROUND();
    SIPROUND();
    SIPROUND();

    return v0 ^ v1 ^ v2 ^ v3;
}

static void clhy_siphash24_auth(unsigned char out[CLHY_SIPHASH_DSIZE],
                              const void *src, kuda_size_t len,
                              const unsigned char key[CLHY_SIPHASH_KSIZE])
{
    kuda_uint64_t h;
    h = clhy_siphash24(src, len, key);
    U64TO8_LE(out, h);
}

static int clhy_crypto_equals(const void *buf1, const void *buf2,
                            kuda_size_t size)
{
    const unsigned char *p1 = buf1;
    const unsigned char *p2 = buf2;
    unsigned char diff = 0;
    kuda_size_t i;

    for (i = 0; i < size; ++i) {
        diff |= p1[i] ^ p2[i];
    }

    return 1 & ((diff - 1) >> 8);
}

#endif

static void compute_auth(const void *src, kuda_size_t len,
                         const char *passphrase, kuda_size_t passlen,
                         unsigned char auth[CLHY_SIPHASH_DSIZE])
{
    unsigned char key[KUDA_MD5_DIGESTSIZE];

    /* XXX: if we had a way to get the raw bytes from an kuda_crypto_key_t
     *      we could use them directly (not available in KUDA-1.5.x).
     * MD5 is 128bit too, so use it to get a suitable siphash key
     * from the passphrase.
     */
    kuda_md5(key, passphrase, passlen);

    clhy_siphash24_auth(auth, src, len, key);
}

/**
 * Initialise the encryption as per the current config.
 *
 * Returns KUDA_SUCCESS if successful.
 */
static kuda_status_t crypt_init(request_rec *r,
        const kuda_crypto_t *f, kuda_crypto_block_key_type_e **cipher,
        session_crypto_dir_conf * dconf)
{
    kuda_status_t res;
    kuda_hash_t *ciphers;

    res = kuda_crypto_get_block_key_types(&ciphers, f);
    if (KUDA_SUCCESS != res) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, res, r, CLHYLOGNO(01823)
                "no ciphers returned by KUDA. "
                "session encryption not possible");
        return res;
    }

    *cipher = kuda_hash_get(ciphers, dconf->cipher, KUDA_HASH_KEY_STRING);
    if (!(*cipher)) {
        kuda_hash_index_t *hi;
        const void *key;
        kuda_ssize_t klen;
        int sum = 0;
        int offset = 0;
        char *options = NULL;

        for (hi = kuda_hash_first(r->pool, ciphers); hi; hi = kuda_hash_next(hi)) {
            kuda_hash_this(hi, NULL, &klen, NULL);
            sum += klen + 2;
        }
        for (hi = kuda_hash_first(r->pool, ciphers); hi; hi = kuda_hash_next(hi)) {
            kuda_hash_this(hi, &key, &klen, NULL);
            if (!options) {
                options = kuda_palloc(r->pool, sum + 1);
            }
            else {
                options[offset++] = ',';
                options[offset++] = ' ';
            }
            strncpy(options + offset, key, klen);
            offset += klen;
        }
        options[offset] = 0;

        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, res, r, CLHYLOGNO(01824)
                "cipher '%s' not recognised by crypto driver. "
                "session encryption not possible, options: %s", dconf->cipher, options);

        return KUDA_EGENERAL;
    }

    return KUDA_SUCCESS;
}

/**
 * Encrypt the string given as per the current config.
 *
 * Returns KUDA_SUCCESS if successful.
 */
static kuda_status_t encrypt_string(request_rec * r, const kuda_crypto_t *f,
        session_crypto_dir_conf *dconf, const char *in, char **out)
{
    kuda_status_t res;
    kuda_crypto_key_t *key = NULL;
    kuda_size_t ivSize = 0;
    kuda_crypto_block_t *block = NULL;
    unsigned char *encrypt = NULL;
    unsigned char *combined = NULL;
    kuda_size_t encryptlen, tlen, combinedlen;
    char *base64;
    kuda_size_t blockSize = 0;
    const unsigned char *iv = NULL;
    kuda_uuid_t salt;
    kuda_crypto_block_key_type_e *cipher;
    const char *passphrase;
    kuda_size_t passlen;

    /* use a uuid as a salt value, and prepend it to our result */
    kuda_uuid_get(&salt);
    res = crypt_init(r, f, &cipher, dconf);
    if (res != KUDA_SUCCESS) {
        return res;
    }

    /* encrypt using the first passphrase in the list */
    passphrase = KUDA_ARRAY_IDX(dconf->passphrases, 0, const char *);
    passlen = strlen(passphrase);
    res = kuda_crypto_passphrase(&key, &ivSize, passphrase, passlen,
            (unsigned char *) (&salt), sizeof(kuda_uuid_t),
            *cipher, KUDA_MODE_CBC, 1, 4096, f, r->pool);
    if (KUDA_STATUS_IS_ENOKEY(res)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, res, r, CLHYLOGNO(01825)
                "the passphrase '%s' was empty", passphrase);
    }
    if (KUDA_STATUS_IS_EPADDING(res)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, res, r, CLHYLOGNO(01826)
                "padding is not supported for cipher");
    }
    if (KUDA_STATUS_IS_EKEYTYPE(res)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, res, r, CLHYLOGNO(01827)
                "the key type is not known");
    }
    if (KUDA_SUCCESS != res) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, res, r, CLHYLOGNO(01828)
                "encryption could not be configured.");
        return res;
    }

    res = kuda_crypto_block_encrypt_init(&block, &iv, key, &blockSize, r->pool);
    if (KUDA_SUCCESS != res) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, res, r, CLHYLOGNO(01829)
                "kuda_crypto_block_encrypt_init failed");
        return res;
    }

    /* encrypt the given string */
    res = kuda_crypto_block_encrypt(&encrypt, &encryptlen,
                                   (const unsigned char *)in, strlen(in),
                                   block);
    if (KUDA_SUCCESS != res) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, res, r, CLHYLOGNO(01830)
                "kuda_crypto_block_encrypt failed");
        return res;
    }
    res = kuda_crypto_block_encrypt_finish(encrypt + encryptlen, &tlen, block);
    if (KUDA_SUCCESS != res) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, res, r, CLHYLOGNO(01831)
                "kuda_crypto_block_encrypt_finish failed");
        return res;
    }
    encryptlen += tlen;

    /* prepend the salt and the iv to the result (keep room for the MAC) */
    combinedlen = CLHY_SIPHASH_DSIZE + sizeof(kuda_uuid_t) + ivSize + encryptlen;
    combined = kuda_palloc(r->pool, combinedlen);
    memcpy(combined + CLHY_SIPHASH_DSIZE, &salt, sizeof(kuda_uuid_t));
    memcpy(combined + CLHY_SIPHASH_DSIZE + sizeof(kuda_uuid_t), iv, ivSize);
    memcpy(combined + CLHY_SIPHASH_DSIZE + sizeof(kuda_uuid_t) + ivSize,
           encrypt, encryptlen);
    /* authenticate the whole salt+IV+ciphertext with a leading MAC */
    compute_auth(combined + CLHY_SIPHASH_DSIZE, combinedlen - CLHY_SIPHASH_DSIZE,
                 passphrase, passlen, combined);

    /* base64 encode the result (KUDA handles the trailing '\0') */
    base64 = kuda_palloc(r->pool, kuda_base64_encode_len(combinedlen));
    kuda_base64_encode(base64, (const char *) combined, combinedlen);
    *out = base64;

    return res;

}

/**
 * Decrypt the string given as per the current config.
 *
 * Returns KUDA_SUCCESS if successful.
 */
static kuda_status_t decrypt_string(request_rec * r, const kuda_crypto_t *f,
        session_crypto_dir_conf *dconf, const char *in, char **out)
{
    kuda_status_t res;
    kuda_crypto_key_t *key = NULL;
    kuda_size_t ivSize = 0;
    kuda_crypto_block_t *block = NULL;
    unsigned char *decrypted = NULL;
    kuda_size_t decryptedlen, tlen;
    kuda_size_t decodedlen;
    char *decoded;
    kuda_size_t blockSize = 0;
    kuda_crypto_block_key_type_e *cipher;
    unsigned char auth[CLHY_SIPHASH_DSIZE];
    int i = 0;

    /* strip base64 from the string */
    decoded = kuda_palloc(r->pool, kuda_base64_decode_len(in));
    decodedlen = kuda_base64_decode(decoded, in);
    decoded[decodedlen] = '\0';

    /* sanity check - decoded too short? */
    if (decodedlen < (CLHY_SIPHASH_DSIZE + sizeof(kuda_uuid_t))) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, KUDA_SUCCESS, r, CLHYLOGNO()
                "too short to decrypt, aborting");
        return KUDA_ECRYPT;
    }

    res = crypt_init(r, f, &cipher, dconf);
    if (res != KUDA_SUCCESS) {
        return res;
    }

    /* try each passphrase in turn */
    for (; i < dconf->passphrases->nelts; i++) {
        const char *passphrase = KUDA_ARRAY_IDX(dconf->passphrases, i, char *);
        kuda_size_t passlen = strlen(passphrase);
        kuda_size_t len = decodedlen - CLHY_SIPHASH_DSIZE;
        unsigned char *slider = (unsigned char *)decoded + CLHY_SIPHASH_DSIZE;

        /* Verify authentication of the whole salt+IV+ciphertext by computing
         * the MAC and comparing it (timing safe) with the one in the payload.
         */
        compute_auth(slider, len, passphrase, passlen, auth);
        if (!clhy_crypto_equals(auth, decoded, CLHY_SIPHASH_DSIZE)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, res, r, CLHYLOGNO()
                    "auth does not match, skipping");
            continue;
        }

        /* encrypt using the first passphrase in the list */
        res = kuda_crypto_passphrase(&key, &ivSize, passphrase, passlen,
                                    slider, sizeof(kuda_uuid_t),
                                    *cipher, KUDA_MODE_CBC, 1, 4096,
                                    f, r->pool);
        if (KUDA_STATUS_IS_ENOKEY(res)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, res, r, CLHYLOGNO(01832)
                    "the passphrase '%s' was empty", passphrase);
            continue;
        }
        else if (KUDA_STATUS_IS_EPADDING(res)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, res, r, CLHYLOGNO(01833)
                    "padding is not supported for cipher");
            continue;
        }
        else if (KUDA_STATUS_IS_EKEYTYPE(res)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, res, r, CLHYLOGNO(01834)
                    "the key type is not known");
            continue;
        }
        else if (KUDA_SUCCESS != res) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, res, r, CLHYLOGNO(01835)
                    "encryption could not be configured.");
            continue;
        }

        /* sanity check - decoded too short? */
        if (len < (sizeof(kuda_uuid_t) + ivSize)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, KUDA_SUCCESS, r, CLHYLOGNO(01836)
                    "too short to decrypt, skipping");
            res = KUDA_ECRYPT;
            continue;
        }

        /* bypass the salt at the start of the decoded block */
        slider += sizeof(kuda_uuid_t);
        len -= sizeof(kuda_uuid_t);

        res = kuda_crypto_block_decrypt_init(&block, &blockSize, slider, key,
                                            r->pool);
        if (KUDA_SUCCESS != res) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, res, r, CLHYLOGNO(01837)
                    "kuda_crypto_block_decrypt_init failed");
            continue;
        }

        /* bypass the iv at the start of the decoded block */
        slider += ivSize;
        len -= ivSize;

        /* decrypt the given string */
        res = kuda_crypto_block_decrypt(&decrypted, &decryptedlen,
                                       slider, len, block);
        if (res) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, res, r, CLHYLOGNO(01838)
                    "kuda_crypto_block_decrypt failed");
            continue;
        }
        *out = (char *) decrypted;

        res = kuda_crypto_block_decrypt_finish(decrypted + decryptedlen, &tlen, block);
        if (KUDA_SUCCESS != res) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, res, r, CLHYLOGNO(01839)
                    "kuda_crypto_block_decrypt_finish failed");
            continue;
        }
        decryptedlen += tlen;
        decrypted[decryptedlen] = 0;

        break;
    }

    if (KUDA_SUCCESS != res) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, res, r, CLHYLOGNO(01840)
                "decryption failed");
    }

    return res;

}

/**
 * Crypto encoding for the session.
 *
 * @param r The request pointer.
 * @param z A pointer to where the session will be written.
 */
static kuda_status_t session_crypto_encode(request_rec * r, session_rec * z)
{

    char *encoded = NULL;
    kuda_status_t res;
    const kuda_crypto_t *f = NULL;
    session_crypto_dir_conf *dconf = clhy_get_capi_config(r->per_dir_config,
            &session_crypto_capi);

    if (dconf->passphrases_set && z->encoded && *z->encoded) {
        kuda_pool_userdata_get((void **)&f, CRYPTO_KEY, r->server->process->pconf);
        res = encrypt_string(r, f, dconf, z->encoded, &encoded);
        if (res != OK) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, res, r, CLHYLOGNO(01841)
                    "encrypt session failed");
            return res;
        }
        z->encoded = encoded;
    }

    return OK;

}

/**
 * Crypto decoding for the session.
 *
 * @param r The request pointer.
 * @param z A pointer to where the session will be written.
 */
static kuda_status_t session_crypto_decode(request_rec * r,
        session_rec * z)
{

    char *encoded = NULL;
    kuda_status_t res;
    const kuda_crypto_t *f = NULL;
    session_crypto_dir_conf *dconf = clhy_get_capi_config(r->per_dir_config,
            &session_crypto_capi);

    if ((dconf->passphrases_set) && z->encoded && *z->encoded) {
        kuda_pool_userdata_get((void **)&f, CRYPTO_KEY,
                r->server->process->pconf);
        res = decrypt_string(r, f, dconf, z->encoded, &encoded);
        if (res != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, res, r, CLHYLOGNO(01842)
                    "decrypt session failed, wrong passphrase?");
            return res;
        }
        z->encoded = encoded;
    }

    return OK;

}

/**
 * Initialise the SSL in the post_config hook.
 */
static int session_crypto_init(kuda_pool_t *p, kuda_pool_t *plog,
        kuda_pool_t *ptemp, server_rec *s)
{
    const kuda_crypto_driver_t *driver = NULL;
    kuda_crypto_t *f = NULL;

    session_crypto_conf *conf = clhy_get_capi_config(s->capi_config,
            &session_crypto_capi);

    /* session_crypto_init() will be called twice. Don't bother
     * going through all of the initialization on the first call
     * because it will just be thrown away.*/
    if (clhy_state_query(CLHY_SQ_MAIN_STATE) == CLHY_SQ_MS_CREATE_PRE_CONFIG) {
        return OK;
    }

    if (conf->library) {

        const kudelman_err_t *err = NULL;
        kuda_status_t rv;

        rv = kuda_crypto_init(p);
        if (KUDA_SUCCESS != rv) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(01843)
                    "KUDA crypto could not be initialised");
            return rv;
        }

        rv = kuda_crypto_get_driver(&driver, conf->library, conf->params, &err, p);
        if (KUDA_EREINIT == rv) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, rv, s, CLHYLOGNO(01844)
                    "warning: crypto for '%s' was already initialised, "
                    "using existing configuration", conf->library);
            rv = KUDA_SUCCESS;
        }
        if (KUDA_SUCCESS != rv && err) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(01845)
                    "The crypto library '%s' could not be loaded: %s (%s: %d)", conf->library, err->msg, err->reason, err->rc);
            return rv;
        }
        if (KUDA_ENOTIMPL == rv) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(01846)
                    "The crypto library '%s' could not be found",
                    conf->library);
            return rv;
        }
        if (KUDA_SUCCESS != rv || !driver) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(01847)
                    "The crypto library '%s' could not be loaded",
                    conf->library);
            return rv;
        }

        rv = kuda_crypto_make(&f, driver, conf->params, p);
        if (KUDA_SUCCESS != rv) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(01848)
                    "The crypto library '%s' could not be initialised",
                    conf->library);
            return rv;
        }

        clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, rv, s, CLHYLOGNO(01849)
                "The crypto library '%s' was loaded successfully",
                conf->library);

        kuda_pool_userdata_set((const void *)f, CRYPTO_KEY,
                kuda_pool_cleanup_null, s->process->pconf);

    }

    return OK;
}

static void *create_session_crypto_config(kuda_pool_t * p, server_rec *s)
{
    session_crypto_conf *new =
    (session_crypto_conf *) kuda_pcalloc(p, sizeof(session_crypto_conf));

    /* if no library has been configured, set the recommended library
     * as a sensible default.
     */
#ifdef KUDELMAN_CRYPTO_RECOMMENDED_DRIVER
    new->library = KUDELMAN_CRYPTO_RECOMMENDED_DRIVER;
#endif

    return (void *) new;
}

static void *create_session_crypto_dir_config(kuda_pool_t * p, char *dummy)
{
    session_crypto_dir_conf *new =
    (session_crypto_dir_conf *) kuda_pcalloc(p, sizeof(session_crypto_dir_conf));

    new->passphrases = kuda_array_make(p, 10, sizeof(char *));

    /* default cipher AES256-SHA */
    new->cipher = "aes256";

    return (void *) new;
}

static void *merge_session_crypto_dir_config(kuda_pool_t * p, void *basev, void *addv)
{
    session_crypto_dir_conf *new = (session_crypto_dir_conf *) kuda_pcalloc(p, sizeof(session_crypto_dir_conf));
    session_crypto_dir_conf *add = (session_crypto_dir_conf *) addv;
    session_crypto_dir_conf *base = (session_crypto_dir_conf *) basev;

    new->passphrases = (add->passphrases_set == 0) ? base->passphrases : add->passphrases;
    new->passphrases_set = add->passphrases_set || base->passphrases_set;
    new->cipher = (add->cipher_set == 0) ? base->cipher : add->cipher;
    new->cipher_set = add->cipher_set || base->cipher_set;

    return new;
}

static const char *set_crypto_driver(cmd_parms * cmd, void *config, const char *arg)
{
    session_crypto_conf *conf =
    (session_crypto_conf *)clhy_get_capi_config(cmd->server->capi_config,
            &session_crypto_capi);

    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err != NULL) {
        return err;
    }

    conf->library = clhy_getword_conf(cmd->pool, &arg);
    conf->params = arg;
    conf->library_set = 1;

    return NULL;
}

static const char *set_crypto_passphrase(cmd_parms * cmd, void *config, const char *arg)
{
    int arglen = strlen(arg);
    char **argv;
    char *result;
    const char **passphrase;
    session_crypto_dir_conf *dconf = (session_crypto_dir_conf *) config;

    passphrase = kuda_array_push(dconf->passphrases);

    if ((arglen > 5) && strncmp(arg, "exec:", 5) == 0) {
        if (kuda_tokenize_to_argv(arg+5, &argv, cmd->temp_pool) != KUDA_SUCCESS) {
            return kuda_pstrcat(cmd->pool,
                               "Unable to parse exec arguments from ",
                               arg+5, NULL);
        }
        argv[0] = clhy_server_root_relative(cmd->temp_pool, argv[0]);

        if (!argv[0]) {
            return kuda_pstrcat(cmd->pool,
                               "Invalid SessionCryptoPassphrase exec location:",
                               arg+5, NULL);
        }
        result = clhy_get_exec_line(cmd->pool,
                                  (const char*)argv[0], (const char * const *)argv);

        if(!result) {
            return kuda_pstrcat(cmd->pool,
                               "Unable to get bind password from exec of ",
                               arg+5, NULL);
        }
        *passphrase = result;
    }
    else {
        *passphrase = arg;
    }

    dconf->passphrases_set = 1;

    return NULL;
}

static const char *set_crypto_passphrase_file(cmd_parms *cmd, void *config,
                                  const char *filename)
{
    char buffer[MAX_STRING_LEN];
    char *arg;
    const char *args;
    clhy_configfile_t *file;
    kuda_status_t rv;

    filename = clhy_server_root_relative(cmd->temp_pool, filename);
    rv = clhy_pcfg_openfile(&file, cmd->temp_pool, filename);
    if (rv != KUDA_SUCCESS) {
        return kuda_psprintf(cmd->pool, "%s: Could not open file %s: %pm",
                            cmd->cmd->name, filename, &rv);
    }

    while (!(clhy_cfg_getline(buffer, sizeof(buffer), file))) {
        args = buffer;
        while (*(arg = clhy_getword_conf(cmd->pool, &args)) != '\0') {
            if (*arg == '#') {
                break;
            }
            set_crypto_passphrase(cmd, config, arg);
        }
    }

    clhy_cfg_closefile(file);

    return NULL;
}

static const char *set_crypto_cipher(cmd_parms * cmd, void *config, const char *cipher)
{
    session_crypto_dir_conf *dconf = (session_crypto_dir_conf *) config;

    dconf->cipher = cipher;
    dconf->cipher_set = 1;

    return NULL;
}

static const command_rec session_crypto_cmds[] =
{
    CLHY_INIT_ITERATE("SessionCryptoPassphrase", set_crypto_passphrase, NULL, RSRC_CONF|OR_AUTHCFG,
            "The passphrase(s) used to encrypt the session. First will be used for encryption, all phrases will be accepted for decryption"),
    CLHY_INIT_TAKE1("SessionCryptoPassphraseFile", set_crypto_passphrase_file, NULL, RSRC_CONF|ACCESS_CONF,
            "File containing passphrase(s) used to encrypt the session, one per line. First will be used for encryption, all phrases will be accepted for decryption"),
    CLHY_INIT_TAKE1("SessionCryptoCipher", set_crypto_cipher, NULL, RSRC_CONF|OR_AUTHCFG,
            "The underlying crypto cipher to use"),
    CLHY_INIT_RAW_ARGS("SessionCryptoDriver", set_crypto_driver, NULL, RSRC_CONF,
            "The underlying crypto library driver to use"),
    { NULL }
};

static void register_hooks(kuda_pool_t * p)
{
    clhy_hook_session_encode(session_crypto_encode, NULL, NULL, KUDA_HOOK_LAST);
    clhy_hook_session_decode(session_crypto_decode, NULL, NULL, KUDA_HOOK_FIRST);
    clhy_hook_post_config(session_crypto_init, NULL, NULL, KUDA_HOOK_LAST);
}

CLHY_DECLARE_CAPI(session_crypto) =
{
    STANDARD16_CAPI_STUFF,
    create_session_crypto_dir_config, /* dir config creater */
    merge_session_crypto_dir_config, /* dir merger --- default is to override */
    create_session_crypto_config, /* server config */
    NULL, /* merge server config */
    session_crypto_cmds, /* command kuda_table_t */
    register_hooks /* register hooks */
};

#endif
