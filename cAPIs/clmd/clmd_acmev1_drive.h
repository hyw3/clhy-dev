/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef clmd_acmev1_drive_h
#define clmd_acmev1_drive_h

struct clmd_acme_driver_t;
struct clmd_proto_driver_t;
struct clmd_result_t;

kuda_status_t clmd_acmev1_drive_renew(struct clmd_acme_driver_t *ad, 
                                   struct clmd_proto_driver_t *d, 
                                   struct clmd_result_t *result);

#endif /* clmd_acmev1_drive_h */
