/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_http_h
#define capi_clmd_clmd_http_h

struct kuda_table_t;
struct kuda_bucket_brigade;
struct kuda_bucket_alloc_t;

typedef struct clmd_http_t clmd_http_t;

typedef struct clmd_http_request_t clmd_http_request_t;
typedef struct clmd_http_response_t clmd_http_response_t;

typedef kuda_status_t clmd_http_cb(const clmd_http_response_t *res);

struct clmd_http_request_t {
    clmd_http_t *http;
    kuda_pool_t *pool;
    struct kuda_bucket_alloc_t *bucket_alloc;
    const char *method;
    const char *url;
    const char *user_agent;
    const char *proxy_url;
    kuda_table_t *headers;
    struct kuda_bucket_brigade *body;
    kuda_off_t body_len;
    kuda_off_t resp_limit;
    clmd_http_cb *cb;
    void *baton;
    void *internals;
};

struct clmd_http_response_t {
    clmd_http_request_t *req;
    kuda_status_t rv;
    int status;
    kuda_table_t *headers;
    struct kuda_bucket_brigade *body;
};

kuda_status_t clmd_http_create(clmd_http_t **phttp, kuda_pool_t *p, const char *user_agent,
                            const char *proxy_url);

void clmd_http_set_response_limit(clmd_http_t *http, kuda_off_t resp_limit);

kuda_status_t clmd_http_GET(clmd_http_t *http, 
                         const char *url, struct kuda_table_t *headers,
                         clmd_http_cb *cb, void *baton);

kuda_status_t clmd_http_HEAD(clmd_http_t *http, 
                          const char *url, struct kuda_table_t *headers,
                          clmd_http_cb *cb, void *baton);

kuda_status_t clmd_http_POST(clmd_http_t *http, const char *url, 
                          struct kuda_table_t *headers, const char *content_type, 
                          struct kuda_bucket_brigade *body,
                          clmd_http_cb *cb, void *baton);

kuda_status_t clmd_http_POSTd(clmd_http_t *http, const char *url, 
                           struct kuda_table_t *headers, const char *content_type, 
                           const char *data, size_t data_len, 
                           clmd_http_cb *cb, void *baton);

void clmd_http_req_destroy(clmd_http_request_t *req);

/**************************************************************************************************/
/* interface to implementation */

typedef kuda_status_t clmd_http_init_cb(void);
typedef void clmd_http_req_cleanup_cb(clmd_http_request_t *req);
typedef kuda_status_t clmd_http_perform_cb(clmd_http_request_t *req);

typedef struct clmd_http_impl_t clmd_http_impl_t;
struct clmd_http_impl_t {
    clmd_http_init_cb *init;
    clmd_http_req_cleanup_cb *req_cleanup;
    clmd_http_perform_cb *perform;
};

void clmd_http_use_implementation(clmd_http_impl_t *impl);



#endif /* clmd_http_h */
