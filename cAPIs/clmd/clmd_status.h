/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef clmd_status_h
#define clmd_status_h

struct clmd_json_t;
struct clmd_reg_t;
struct clmd_result_t;

/** 
 * Get a JSON summary of the CLMD and its status (certificates, jobs, etc.).
 */
kuda_status_t clmd_status_get_clmd_json(struct clmd_json_t **pjson, const clmd_t *clmd, 
                                   struct clmd_reg_t *reg, kuda_pool_t *p);

/** 
 * Get a JSON summary of all CLMDs and their status.
 */
kuda_status_t clmd_status_get_json(struct clmd_json_t **pjson, kuda_array_header_t *mds, 
                                struct clmd_reg_t *reg, kuda_pool_t *p);

/**
 * Take stock of all CLMDs given for a short overview. The JSON returned
 * will carry intergers for CLMD_KEY_COMPLETE, CLMD_KEY_RENEWING, 
 * CLMD_KEY_ERRORED, CLMD_KEY_READY and CLMD_KEY_TOTAL.
 */
void  clmd_status_take_stock(struct clmd_json_t **pjson, kuda_array_header_t *mds, 
                           struct clmd_reg_t *reg, kuda_pool_t *p);

typedef struct clmd_job_t clmd_job_t;
struct clmd_job_t {
    const char *name;      /* Name of the CLMD this job is about */
    kuda_pool_t *p;     
    kuda_time_t next_run;   /* Time this job wants to be processed next */
    kuda_time_t last_run;   /* Time this job ran last (or 0) */
    struct clmd_result_t *last_result; /* Result from last run */
    int finished;          /* true iff the job finished successfully */
    kuda_time_t valid_from; /* at which time the finished job results become valid, 0 if immediate */
    int error_runs;        /* Number of errored runs of an unfinished job */
    clmd_json_t *log;        /* array of log objects with minimum fields
                              CLMD_KEY_WHEN (timestamp) and CLMD_KEY_TYPE (string) */   
};

/**
 * Create a new job instance for the given CLMD name. Job load/save will work
 * on the CLMD_SG_STAGING for the name.
 */
clmd_job_t *clmd_job_make(kuda_pool_t *p, const char *name);

/**
 * Update the job from storage in <group>/job->name.
 */
kuda_status_t clmd_job_load(clmd_job_t *job, struct clmd_reg_t *reg, 
                         clmd_store_group_t group, kuda_pool_t *p);

/**
 * Update storage from job in <group>/job->name.
 */
kuda_status_t clmd_job_save(clmd_job_t *job, struct clmd_reg_t *reg, 
                         clmd_store_group_t group, struct clmd_result_t *result, 
                         kuda_pool_t *p);

/**
 * Append to the job's log. Timestamp is automatically added.
 * @param type          type of log entry
 * @param status        status of entry (maybe NULL)
 * @param detail        description of what happened
 */
void clmd_job_log_append(clmd_job_t *job, const char *type, 
                       const char *status, const char *detail);

/**
 * Retrieve the lastest log entry of a certain type.
 */
clmd_json_t *clmd_job_log_get_latest(clmd_job_t *job, const char *type);

/**
 * Get the time the latest log entry of the given type happened, or 0 if
 * none is found.
 */
kuda_time_t clmd_job_log_get_time_of_latest(clmd_job_t *job, const char *type);

#endif /* clmd_status_h */
