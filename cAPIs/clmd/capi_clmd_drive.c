/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <kuda_optional.h>
#include <kuda_hash.h>
#include <kuda_strings.h>
#include <kuda_date.h>

#include <wwhy.h>
#include <http_core.h>
#include <http_protocol.h>
#include <http_request.h>
#include <http_log.h>

#include "capi_watchdog.h"

#include "clmd.h"
#include "clmd_curl.h"
#include "clmd_crypt.h"
#include "clmd_http.h"
#include "clmd_json.h"
#include "clmd_status.h"
#include "clmd_store.h"
#include "clmd_store_fs.h"
#include "clmd_log.h"
#include "clmd_result.h"
#include "clmd_reg.h"
#include "clmd_util.h"
#include "clmd_version.h"
#include "clmd_acme.h"
#include "clmd_acme_authz.h"

#include "capi_clmd.h"
#include "capi_clmd_private.h"
#include "capi_clmd_config.h"
#include "capi_clmd_status.h"
#include "capi_clmd_drive.h"

/**************************************************************************************************/
/* watchdog based impl. */

#define CLMD_WATCHDOG_NAME   "_clmd_"

static KUDA_OPTIONAL_FN_TYPE(clhy_watchdog_get_instance) *wd_get_instance;
static KUDA_OPTIONAL_FN_TYPE(clhy_watchdog_register_callback) *wd_register_callback;
static KUDA_OPTIONAL_FN_TYPE(clhy_watchdog_set_callback_interval) *wd_set_interval;

struct clmd_drive_ctx {
    kuda_pool_t *p;
    server_rec *s;
    clmd_capi_conf_t *mc;
    clhy_watchdog_t *watchdog;
    
    kuda_array_header_t *jobs;
};

typedef struct {
    kuda_pool_t *p;
    clmd_job_t *job;
    clmd_reg_t *reg;
    clmd_result_t *last;
    kuda_time_t last_save;
} clmd_job_result_ctx;

static void job_result_update(clmd_result_t *result, void *data)
{
    clmd_job_result_ctx *ctx = data;
    kuda_time_t now;
    const char *msg, *sep;
    
    if (clmd_result_cmp(ctx->last, result)) {
        now = kuda_time_now();
        clmd_result_assign(ctx->last, result);
        if (result->activity || result->problem || result->detail) {
            msg = sep = "";
            if (result->activity) {
                msg = kuda_psprintf(result->p, "%s", result->activity);
                sep = ": ";
            }
            if (result->detail) {
                msg = kuda_psprintf(result->p, "%s%s%s", msg, sep, result->detail);
                sep = ", ";
            }
            if (result->problem) {
                msg = kuda_psprintf(result->p, "%s%sproblem: %s", msg, sep, result->problem);
                sep = " ";
            }
            clmd_job_log_append(ctx->job, "progress", NULL, msg);

            if (kuda_time_msec(now - ctx->last_save) > 500) {
                clmd_job_save(ctx->job, ctx->reg, CLMD_SG_STAGING, result, ctx->p);
                ctx->last_save = now;
            }
        }
    }
}

static void job_result_observation_start(clmd_job_t *job, clmd_result_t *result, 
                                         clmd_reg_t *reg, kuda_pool_t *p)
{
    clmd_job_result_ctx *ctx;

    ctx = kuda_pcalloc(p, sizeof(*ctx));
    ctx->p = p;
    ctx->job = job;
    ctx->reg = reg;
    ctx->last = clmd_result_clmd_make(p, KUDA_SUCCESS);
    clmd_result_assign(ctx->last, result);
    clmd_result_on_change(result, job_result_update, ctx);
}

static void job_result_observation_end(clmd_job_t *job, clmd_result_t *result)
{
    (void)job;
    clmd_result_on_change(result, NULL, NULL);
} 

static kuda_time_t calc_err_delay(int err_count)
{
    kuda_time_t delay = 0;
    
    if (err_count > 0) {
        /* back off duration, depending on the errors we encounter in a row */
        delay = kuda_time_from_sec(5 << (err_count - 1));
        if (delay > kuda_time_from_sec(60*60)) {
            delay = kuda_time_from_sec(60*60);
        }
    }
    return delay;
}

static kuda_status_t send_notification(clmd_drive_ctx *dctx, clmd_job_t *job, const clmd_t *clmd, 
                                      const char *reason, clmd_result_t *result, kuda_pool_t *ptemp)
{
    const char * const *argv;
    const char *cmdline;
    int exit_code;
    kuda_status_t rv = KUDA_SUCCESS;            
    
    if (!strcmp("renewed", reason)) {
        if (dctx->mc->notify_cmd) {
            cmdline = kuda_psprintf(ptemp, "%s %s", dctx->mc->notify_cmd, clmd->name); 
            kuda_tokenize_to_argv(cmdline, (char***)&argv, ptemp);
            rv = clmd_util_exec(ptemp, argv[0], argv, &exit_code);
            
            if (KUDA_SUCCESS == rv && exit_code) rv = KUDA_EGENERAL;
            if (KUDA_SUCCESS != rv) {
                if (!result) result = clmd_result_make(ptemp, rv);
                clmd_result_problem_printf(result, rv, CLMD_RESULT_LOG_ID(CLHYLOGNO(10108)), 
                                         "MDNotifyCmd %s failed with exit code %d.", 
                                         dctx->mc->notify_cmd, exit_code);
                clmd_result_log(result, CLMD_LOG_ERR);
                clmd_job_log_append(job, "notify-error", result->problem, result->detail);
                goto leave;
            }
        }
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, dctx->s, CLHYLOGNO(10059) 
                     "The Managed Domain %s has been setup and changes "
                     "will be activated on next (graceful) server restart.", clmd->name);
    }
    if (dctx->mc->message_cmd) {
        cmdline = kuda_psprintf(ptemp, "%s %s %s", dctx->mc->message_cmd, reason, clmd->name); 
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, dctx->s, "Message command: %s", cmdline);
        kuda_tokenize_to_argv(cmdline, (char***)&argv, ptemp);
        rv = clmd_util_exec(ptemp, argv[0], argv, &exit_code);
        
        if (KUDA_SUCCESS == rv && exit_code) rv = KUDA_EGENERAL;
        if (KUDA_SUCCESS != rv) {
            if (!result) result = clmd_result_make(ptemp, rv);
            clmd_result_problem_printf(result, rv, CLMD_RESULT_LOG_ID(CLHYLOGNO(10109)), 
                                     "MDMessageCmd %s failed with exit code %d.", 
                                     dctx->mc->notify_cmd, exit_code);
            clmd_result_log(result, CLMD_LOG_ERR);
            clmd_job_log_append(job, "message-error", reason, result->detail);
            goto leave;
        }
    }
leave:
    return rv;
}

static void check_expiration(clmd_drive_ctx *dctx, clmd_job_t *job, const clmd_t *clmd, kuda_pool_t *ptemp)
{
    clmd_timeperiod_t since_last;
    
    clhy_log_error( CLHYLOG_MARK, CLHYLOG_TRACE1, 0, dctx->s, "clmd(%s): check expiration", clmd->name);
    if (!clmd_reg_should_warn(dctx->mc->reg, clmd, dctx->p)) return;
    
    /* Sends these out at most once per day */
    since_last.start = clmd_job_log_get_time_of_latest(job, "message-expiring");
    since_last.end = kuda_time_now();

    if (clmd_timeperiod_length(&since_last) >= kuda_time_from_sec(CLMD_SECS_PER_DAY)) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, dctx->s, 
                     "clmd(%s): message expiration warning", clmd->name);
        send_notification(dctx, job, clmd, "expiring", NULL, ptemp);
    }
}

static void process_drive_job(clmd_drive_ctx *dctx, clmd_job_t *job, kuda_pool_t *ptemp)
{
    const clmd_t *clmd;
    clmd_result_t *result;
    int error_run = 0, fatal_run = 0, save = 0;
    kuda_status_t rv;
    
    clmd_job_load(job, dctx->mc->reg, CLMD_SG_STAGING, ptemp);
    /* Evaluate again on loaded value. Values will change when watchdog switches child process */
    if (kuda_time_now() < job->next_run) return;
    
    clmd = clmd_get_by_name(dctx->mc->mds, job->name);
    CLHY_DEBUG_ASSERT(clmd);

    result = clmd_result_clmd_make(ptemp, clmd);
    if (job->last_result) clmd_result_assign(result, job->last_result); 
    
    if (clmd->state == CLMD_S_MISSING_INFORMATION) {
        /* Missing information, this will not change until configuration
         * is changed and server reloaded. */
        fatal_run = 1;
        goto leave;
    }
    
    while (clmd_will_renew_cert(clmd)) {
        if (job->finished) {
            job->next_run = 0;
            /* Finished jobs might take a while before the results become valid.
             * If that is in the future, request to run then */
            if (kuda_time_now() < job->valid_from) {
                job->next_run = job->valid_from;
            }
            else if (clmd_job_log_get_time_of_latest(job, "notified") == 0) {
                rv = send_notification(dctx, job, clmd, "renewed", result, ptemp);
                if (KUDA_SUCCESS == rv) {
                    clmd_job_log_append(job, "notified", NULL, NULL);
                    save = 1;
                }
                else { 
                    /* we treat this as an error that triggers retries */
                    error_run = 1;
                }
            }
            goto leave;
        }
        
        if (!clmd_reg_should_renew(dctx->mc->reg, clmd, dctx->p)) {
            clhy_log_error( CLHYLOG_MARK, CLHYLOG_DEBUG, 0, dctx->s, CLHYLOGNO(10053) 
                         "clmd(%s): no need to renew yet", job->name);
            job->next_run = 0;
            goto leave;
        }

        /* Renew the CLMDs credentials in a STAGING area. Might be invoked repeatedly 
         * without discarding previous/intermediate results.
         * Only returns SUCCESS when the renewal is complete, e.g. STAGING as a
         * complete set of new credentials.
         */
        clhy_log_error( CLHYLOG_MARK, CLHYLOG_DEBUG, 0, dctx->s, CLHYLOGNO(10052) 
                     "clmd(%s): state=%d, driving", job->name, clmd->state);
        clmd_job_log_append(job, "renewal-start", NULL, NULL);
        /* observe result changes and persist them with limited frequency */
        job_result_observation_start(job, result, dctx->mc->reg, ptemp);
        
        clmd_reg_renew(dctx->mc->reg, clmd, dctx->mc->env, 0, result, ptemp);
        
        job_result_observation_end(job, result);
        if (KUDA_SUCCESS != result->status) {
            clhy_log_error( CLHYLOG_MARK, CLHYLOG_ERR, result->status, dctx->s, CLHYLOGNO(10056) 
                         "processing %s: %s", job->name, result->detail);
            error_run = 1;
            clmd_job_log_append(job, "renewal-error", result->problem, result->detail);
            send_notification(dctx, job, clmd, "errored", result, ptemp);
            goto leave;
        }
        
        job->finished = 1;
        job->valid_from = result->ready_at;
        job->error_runs = 0;
        clmd_job_log_append(job, "renewal-finish", NULL, NULL);
        save = 1;
    }
    
leave:
    if (!job->finished) {
        check_expiration(dctx, job, clmd, ptemp);
    }
    
    if (fatal_run) {
        save = 1;
        job->next_run = 0;
    }
    if (error_run) {
        ++job->error_runs;
        save = 1;
        job->next_run = kuda_time_now() + calc_err_delay(job->error_runs);
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_INFO, 0, dctx->s, CLHYLOGNO(10057) 
                     "%s: encountered error for the %d. time, next run in %s",
                     job->name, job->error_runs, 
                     clmd_duration_print(ptemp, job->next_run - kuda_time_now()));
    }
    if (save) {
        kuda_status_t rv2 = clmd_job_save(job, dctx->mc->reg, CLMD_SG_STAGING, result, ptemp);
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE1, rv2, dctx->s, "%s: saving job props", job->name);
    }
}

int clmd_will_renew_cert(const clmd_t *clmd)
{
    if (clmd->renew_mode == CLMD_RENEW_MANUAL) {
        return 0;
    }
    else if (clmd->renew_mode == CLMD_RENEW_AUTO && clmd->cert_file) {
        return 0;
    } 
    return 1;
}

static kuda_time_t next_run_default(void)
{
    /* we'd like to run at least twice a day by default */
    return kuda_time_now() + kuda_time_from_sec(CLMD_SECS_PER_DAY / 2);
}

static kuda_status_t run_watchdog(int state, void *baton, kuda_pool_t *ptemp)
{
    clmd_drive_ctx *dctx = baton;
    clmd_job_t *job;
    kuda_time_t next_run, wait_time;
    int i;
    
    /* capi_watchdog invoked us as a single thread inside the whole server (on this machine).
     * This might be a repeated run inside the same child (capi_watchdog keeps affinity as
     * long as the child lives) or another/new child.
     */
    switch (state) {
        case CLHY_WATCHDOG_STATE_STARTING:
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, dctx->s, CLHYLOGNO(10054)
                         "clmd watchdog start, auto drive %d mds", dctx->jobs->nelts);
            break;
            
        case CLHY_WATCHDOG_STATE_RUNNING:
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, dctx->s, CLHYLOGNO(10055)
                         "clmd watchdog run, auto drive %d mds", dctx->jobs->nelts);
                         
            /* Process all drive jobs. They will update their next_run property
             * and we schedule ourself at the earliest of all. A job may specify 0
             * as next_run to indicate that it wants to participate in the normal
             * regular runs. */
            next_run = next_run_default();
            for (i = 0; i < dctx->jobs->nelts; ++i) {
                job = KUDA_ARRAY_IDX(dctx->jobs, i, clmd_job_t *);
                
                if (kuda_time_now() >= job->next_run) {
                    process_drive_job(dctx, job, ptemp);
                }
                
                if (job->next_run && job->next_run < next_run) {
                    next_run = job->next_run;
                }
            }

            wait_time = next_run - kuda_time_now();
            if (CLHYLOGdebug(dctx->s)) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, dctx->s, CLHYLOGNO(10107)
                             "next run in %s", clmd_duration_print(ptemp, wait_time));
            }
            wd_set_interval(dctx->watchdog, wait_time, dctx, run_watchdog);
            break;
            
        case CLHY_WATCHDOG_STATE_STOPPING:
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, dctx->s, CLHYLOGNO(10058)
                         "clmd watchdog stopping");
            break;
    }
    
    return KUDA_SUCCESS;
}

kuda_status_t clmd_start_watching(clmd_capi_conf_t *mc, server_rec *s, kuda_pool_t *p)
{
    kuda_allocator_t *allocator;
    clmd_drive_ctx *dctx;
    kuda_pool_t *dctxp;
    kuda_status_t rv;
    const char *name;
    clmd_t *clmd;
    clmd_job_t *job;
    int i;
    
    /* We use capi_watchdog to run a single thread in one of the child processes
     * to monitor the CLMDs in mc->watched_names, using the const data in the list
     * mc->mds of our CLMD structures.
     *
     * The data in mc cannot be changed, as we may spawn copies in new child processes
     * of the original data at any time. The child which hosts the watchdog thread
     * may also die or be recycled, which causes a new watchdog thread to run
     * in another process with the original data.
     * 
     * Instead, we use our store to persist changes in group STAGING. This is
     * kept writable to child processes, but the data stored there is not live.
     * However, capi_watchdog makes sure that we only ever have a single thread in
     * our server (on this machine) that writes there. Other processes, e.g. informing
     * the user about progress, only read from there.
     *
     * All changes during driving an CLMD are stored as files in MG_SG_STAGING/<CLMD.name>.
     * All will have "clmd.json" and "job.json". There may be a range of other files used
     * by the protocol obtaining the certificate/keys.
     * 
     * 
     */
    wd_get_instance = KUDA_RETRIEVE_OPTIONAL_FN(clhy_watchdog_get_instance);
    wd_register_callback = KUDA_RETRIEVE_OPTIONAL_FN(clhy_watchdog_register_callback);
    wd_set_interval = KUDA_RETRIEVE_OPTIONAL_FN(clhy_watchdog_set_callback_interval);
    
    if (!wd_get_instance || !wd_register_callback || !wd_set_interval) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, s, CLHYLOGNO(10061) "capi_watchdog is required");
        return !OK;
    }
    
    /* We want our own pool with own allocator to keep data across watchdog invocations.
     * Since we'll run in a single watchdog thread, using our own allocator will prevent 
     * any confusion in the parent pool. */
    kuda_allocator_create(&allocator);
    kuda_allocator_max_free_set(allocator, 1);
    rv = kuda_pool_create_ex(&dctxp, p, NULL, allocator);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(10062) "clmd_drive_ctx: create pool");
        return rv;
    }
    kuda_allocator_owner_set(allocator, dctxp);
    kuda_pool_tag(dctxp, "clmd_drive_ctx");

    dctx = kuda_pcalloc(dctxp, sizeof(*dctx));
    dctx->p = dctxp;
    dctx->s = s;
    dctx->mc = mc;
    
    dctx->jobs = kuda_array_make(dctx->p, mc->watched_names->nelts, sizeof(clmd_job_t *));
    for (i = 0; i < mc->watched_names->nelts; ++i) {
        name = KUDA_ARRAY_IDX(mc->watched_names, i, const char *);
        clmd = clmd_get_by_name(mc->mds, name);
        if (!clmd) continue;
        
        job = clmd_job_make(p, clmd->name);
        KUDA_ARRAY_PUSH(dctx->jobs, clmd_job_t*) = job;
        clhy_log_error( CLHYLOG_MARK, CLHYLOG_TRACE1, 0, dctx->s,  
                     "clmd(%s): state=%d, created drive job", name, clmd->state);
        
        clmd_job_load(job, mc->reg, CLMD_SG_STAGING, dctx->p);
        if (job->error_runs) {
            /* Server has just restarted. If we encounter an CLMD job with errors
             * on a previous driving, we purge its STAGING area.
             * This will reset the driving for the CLMD. It may run into the same
             * error again, or in case of race/confusion/our error/CA error, it
             * might allow the CLMD to succeed by a fresh start.
             */
            clhy_log_error( CLHYLOG_MARK, CLHYLOG_NOTICE, 0, dctx->s, CLHYLOGNO(10064) 
                         "clmd(%s): previous drive job showed %d errors, purging STAGING "
                         "area to reset.", name, job->error_runs);
            clmd_store_purge(clmd_reg_store_get(dctx->mc->reg), p, CLMD_SG_STAGING, clmd->name);
            clmd_store_purge(clmd_reg_store_get(dctx->mc->reg), p, CLMD_SG_CHALLENGES, clmd->name);
            job->error_runs = 0;
        }
    }

    if (!dctx->jobs->nelts) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(10065)
                     "no managed domain to drive, no watchdog needed.");
        kuda_pool_destroy(dctx->p);
        return KUDA_SUCCESS;
    }
    
    if (KUDA_SUCCESS != (rv = wd_get_instance(&dctx->watchdog, CLMD_WATCHDOG_NAME, 0, 1, dctx->p))) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(10066) 
                     "create clmd watchdog(%s)", CLMD_WATCHDOG_NAME);
        return rv;
    }
    rv = wd_register_callback(dctx->watchdog, 0, dctx, run_watchdog);
    clhy_log_error(CLHYLOG_MARK, rv? CLHYLOG_CRIT : CLHYLOG_DEBUG, rv, s, CLHYLOGNO(10067) 
                 "register clmd watchdog(%s)", CLMD_WATCHDOG_NAME);
    return rv;
}
