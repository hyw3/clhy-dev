capi_clmd.la: clmd_acme.slo clmd_acme_acct.slo clmd_acme_authz.slo clmd_acme_drive.slo clmd_acmev1_drive.slo clmd_acmev2_drive.slo clmd_acme_order.slo clmd_core.slo clmd_curl.slo clmd_crypt.slo clmd_http.slo clmd_json.slo clmd_jws.slo clmd_log.slo clmd_result.slo clmd_reg.slo clmd_status.slo clmd_store.slo clmd_store_fs.slo clmd_time.slo clmd_util.slo capi_clmd.slo capi_clmd_config.slo capi_clmd_drive.slo capi_clmd_platform.slo capi_clmd_status.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  clmd_acme.lo clmd_acme_acct.lo clmd_acme_authz.lo clmd_acme_drive.lo clmd_acmev1_drive.lo clmd_acmev2_drive.lo clmd_acme_order.lo clmd_core.lo clmd_curl.lo clmd_crypt.lo clmd_http.lo clmd_json.lo clmd_jws.lo clmd_log.lo clmd_result.lo clmd_reg.lo clmd_status.lo clmd_store.lo clmd_store_fs.lo clmd_time.lo clmd_util.lo capi_clmd.lo capi_clmd_config.lo capi_clmd_drive.lo capi_clmd_platform.lo capi_clmd_status.lo  $(CAPI_CLMD_LDADD)
DISTCLEAN_TARGETS = cAPIs.mk
static = 
shared =  capi_clmd.la
CAPI_CFLAGS = -I/usr/include/x86_64-linux-gnu
CAPI_LDFLAGS = -lssl -lcrypto  -luuid -lrt -lcrypt  -lpthread -ldl -ljansson -L/usr/lib/x86_64-linux-gnu/mit-krb5 -Wl,-z,relro -lcurl -lnghttp2 -lidn2 -lrtmp -lssh2 -lpsl -lgssapi_krb5 -lkrb5 -lk5crypto -lcom_err -llber -lldap -lz
