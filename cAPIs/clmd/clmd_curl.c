/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>

#include <curl/curl.h>

#include <kuda_lib.h>
#include <kuda_strings.h>
#include <kuda_buckets.h>

#include "clmd_http.h"
#include "clmd_log.h"
#include "clmd_curl.h"

/**************************************************************************************************/
/* clmd_http curl implementation */


static kuda_status_t curl_status(int curl_code)
{
    switch (curl_code) {
        case CURLE_OK:                   return KUDA_SUCCESS;
        case CURLE_UNSUPPORTED_PROTOCOL: return KUDA_ENOTIMPL; 
        case CURLE_NOT_BUILT_IN:         return KUDA_ENOTIMPL; 
        case CURLE_URL_MALFORMAT:        return KUDA_EINVAL;
        case CURLE_COULDNT_RESOLVE_PROXY:return KUDA_ECONNREFUSED;
        case CURLE_COULDNT_RESOLVE_HOST: return KUDA_ECONNREFUSED;
        case CURLE_COULDNT_CONNECT:      return KUDA_ECONNREFUSED;
        case CURLE_REMOTE_ACCESS_DENIED: return KUDA_EACCES;
        case CURLE_OUT_OF_MEMORY:        return KUDA_ENOMEM;
        case CURLE_OPERATION_TIMEDOUT:   return KUDA_TIMEUP;
        case CURLE_SSL_CONNECT_ERROR:    return KUDA_ECONNABORTED;
        case CURLE_AGAIN:                return KUDA_EAGAIN;
        default:                         return KUDA_EGENERAL;
    }
}

static size_t req_data_cb(void *data, size_t len, size_t nmemb, void *baton)
{
    kuda_bucket_brigade *body = baton;
    size_t blen, read_len = 0, max_len = len * nmemb;
    const char *bdata;
    kuda_bucket *b;
    kuda_status_t rv;
    
    while (body && !KUDA_BRIGADE_EMPTY(body) && max_len > 0) {
        b = KUDA_BRIGADE_FIRST(body);
        if (KUDA_BUCKET_IS_METADATA(b)) {
            if (KUDA_BUCKET_IS_EOS(b)) {
                body = NULL;
            }
        }
        else {
            rv = kuda_bucket_read(b, &bdata, &blen, KUDA_BLOCK_READ);
            if (rv == KUDA_SUCCESS) {
                if (blen > max_len) {
                    kuda_bucket_split(b, max_len);
                    blen = max_len;
                }
                memcpy(data, bdata, blen);
                read_len += blen;
                max_len -= blen;
            }
            else {
                body = NULL;
                if (!KUDA_STATUS_IS_EOF(rv)) {
                    /* everything beside EOF is an error */
                    read_len = CURL_READFUNC_ABORT;
                }
            }
            
        }
        kuda_bucket_delete(b);
    }
    
    return read_len;
}

static size_t resp_data_cb(void *data, size_t len, size_t nmemb, void *baton)
{
    clmd_http_response_t *res = baton;
    size_t blen = len * nmemb;
    kuda_status_t rv;
    
    if (res->body) {
        if (res->req->resp_limit) {
            kuda_off_t body_len = 0;
            kuda_brigade_length(res->body, 0, &body_len);
            if (body_len + (kuda_off_t)len > res->req->resp_limit) {
                return 0; /* signal curl failure */
            }
        }
        rv = kuda_brigade_write(res->body, NULL, NULL, (const char *)data, blen);
        if (rv != KUDA_SUCCESS) {
            /* returning anything != blen will make CURL fail this */
            return 0;
        }
    }
    return blen;
}

static size_t header_cb(void *buffer, size_t elen, size_t nmemb, void *baton)
{
    clmd_http_response_t *res = baton;
    size_t len, clen = elen * nmemb;
    const char *name = NULL, *value = "", *b = buffer;
    kuda_size_t i;
    
    len = (clen && b[clen-1] == '\n')? clen-1 : clen;
    len = (len && b[len-1] == '\r')? len-1 : len;
    for (i = 0; i < len; ++i) {
        if (b[i] == ':') {
            name = kuda_pstrndup(res->req->pool, b, i);
            ++i;
            while (i < len && b[i] == ' ') {
                ++i;
            }
            if (i < len) {
                value = kuda_pstrndup(res->req->pool, b+i, len - i);
            }
            break;
        }
    }
    
    if (name != NULL) {
        kuda_table_add(res->headers, name, value);
    }
    return clen;
}

static kuda_status_t curl_init(clmd_http_request_t *req)
{
    CURL *curl = curl_easy_init();
    if (!curl) {
        return KUDA_EGENERAL;
    }
    
    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, header_cb);
    curl_easy_setopt(curl, CURLOPT_HEADERDATA, NULL);
    curl_easy_setopt(curl, CURLOPT_READFUNCTION, req_data_cb);
    curl_easy_setopt(curl, CURLOPT_READDATA, NULL);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, resp_data_cb);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, NULL);
    
    req->internals = curl;
    return KUDA_SUCCESS;
}

typedef struct {
    clmd_http_request_t *req;
    struct curl_slist *hdrs;
    kuda_status_t rv;
} curlify_hdrs_ctx;

static int curlify_headers(void *baton, const char *key, const char *value)
{
    curlify_hdrs_ctx *ctx = baton;
    const char *s;
    
    if (strchr(key, '\r') || strchr(key, '\n')
        || strchr(value, '\r') || strchr(value, '\n')) {
        ctx->rv = KUDA_EINVAL;
        return 0;
    }
    s = kuda_psprintf(ctx->req->pool, "%s: %s", key, value);
    ctx->hdrs = curl_slist_append(ctx->hdrs, s);
    return 1;
}

static kuda_status_t curl_perform(clmd_http_request_t *req)
{
    kuda_status_t rv = KUDA_SUCCESS;
    CURLcode curle;
    clmd_http_response_t *res;
    CURL *curl;
    struct curl_slist *req_hdrs = NULL;

    rv = curl_init(req);
    curl = req->internals;
    
    res = kuda_pcalloc(req->pool, sizeof(*res));
    
    res->req = req;
    res->rv = KUDA_SUCCESS;
    res->status = 400;
    res->headers = kuda_table_make(req->pool, 5);
    res->body = kuda_brigade_create(req->pool, req->bucket_alloc);
    
    curl_easy_setopt(curl, CURLOPT_URL, req->url);
    if (!kuda_strnatcasecmp("GET", req->method)) {
        /* nop */
    }
    else if (!kuda_strnatcasecmp("HEAD", req->method)) {
        curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);
    }
    else if (!kuda_strnatcasecmp("POST", req->method)) {
        curl_easy_setopt(curl, CURLOPT_POST, 1L);
    }
    else {
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, req->method);
    }
    curl_easy_setopt(curl, CURLOPT_HEADERDATA, res);
    curl_easy_setopt(curl, CURLOPT_READDATA, req->body);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, res);
    
    if (req->user_agent) {
        curl_easy_setopt(curl, CURLOPT_USERAGENT, req->user_agent);
    }
    if (req->proxy_url) {
        curl_easy_setopt(curl, CURLOPT_PROXY, req->proxy_url);
    }
    if (!kuda_is_empty_table(req->headers)) {
        curlify_hdrs_ctx ctx;
        
        ctx.req = req;
        ctx.hdrs = NULL;
        ctx.rv = KUDA_SUCCESS;
        kuda_table_do(curlify_headers, &ctx, req->headers, NULL);
        req_hdrs = ctx.hdrs;
        if (ctx.rv == KUDA_SUCCESS) {
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, req_hdrs);
        }
    }
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, req->pool, 
                  "request --> %s %s", req->method, req->url);
    
    if (clmd_log_is_level(req->pool, CLMD_LOG_TRACE3)) {
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    }
    
    curle = curl_easy_perform(curl);
    res->rv = curl_status(curle);
    
    if (KUDA_SUCCESS == res->rv) {
        long l;
        res->rv = curl_status(curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &l));
        if (KUDA_SUCCESS == res->rv) {
            res->status = (int)l;
        }
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, res->rv, req->pool, 
                      "request <-- %d", res->status);
    }
    else {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, res->rv, req->pool, 
                      "request failed(%d): %s", curle, 
                      curl_easy_strerror(curle));
    }
    
    if (req->cb) {
        res->rv = req->cb(res);
    }
    
    rv = res->rv;
    clmd_http_req_destroy(req);
    if (req_hdrs) {
        curl_slist_free_all(req_hdrs);
    }
    
    return rv;
}

static int initialized;

static kuda_status_t clmd_curl_init(void) {
    if (!initialized) {
        initialized = 1;
        curl_global_init(CURL_GLOBAL_DEFAULT);
    }
    return KUDA_SUCCESS;
}

static void curl_req_cleanup(clmd_http_request_t *req) 
{
    if (req->internals) {
        curl_easy_cleanup(req->internals);
        req->internals = NULL;
    }
}

static clmd_http_impl_t impl = {
    clmd_curl_init,
    curl_req_cleanup,
    curl_perform
};

clmd_http_impl_t * clmd_curl_get_impl(kuda_pool_t *p)
{
    /* trigger early global curl init, before we are down a rabbit hole */
    (void)p;
    clmd_curl_init();
    return &impl;
}
