/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_drive_h
#define capi_clmd_clmd_drive_h

struct clmd_capi_conf_t;
struct clmd_reg_t;

typedef struct clmd_drive_ctx clmd_drive_ctx;

int clmd_will_renew_cert(const clmd_t *clmd);

/**
 * Start driving the certificate procotol for the domains mentioned in mc->watched_names.
 */
kuda_status_t clmd_start_watching(struct clmd_capi_conf_t *mc, server_rec *s, kuda_pool_t *p);




#endif /* capi_clmd_clmd_drive_h */
