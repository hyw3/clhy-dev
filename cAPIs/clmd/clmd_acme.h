/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_acme_h
#define capi_clmd_clmd_acme_h

struct kuda_array_header_t;
struct kuda_bucket_brigade;
struct clmd_http_response_t;
struct kuda_hash_t;
struct clmd_http_t;
struct clmd_json_t;
struct clmd_pkey_t;
struct clmd_t;
struct clmd_acme_acct_t;
struct clmd_acmev2_acct_t;
struct clmd_store_t;
struct clmd_result_t;

#define CLMD_PROTO_ACME               "ACME"

#define CLMD_AUTHZ_CHA_HTTP_01        "http-01"
#define CLMD_AUTHZ_CHA_SNI_01         "tls-sni-01"

#define CLMD_ACME_VERSION_UNKNOWN    0x0
#define CLMD_ACME_VERSION_1          0x010000
#define CLMD_ACME_VERSION_2          0x020000

#define CLMD_ACME_VERSION_MAJOR(i)    (((i)&0xFF0000) >> 16)

typedef enum {
    CLMD_ACME_S_UNKNOWN,              /* CLMD has not been analysed yet */
    CLMD_ACME_S_REGISTERED,           /* CLMD is registered at CA, but not more */
    CLMD_ACME_S_TOS_ACCEPTED,         /* Terms of Service were accepted by account holder */
    CLMD_ACME_S_CHALLENGED,           /* CLMD challenge information for all domains is known */
    CLMD_ACME_S_VALIDATED,            /* CLMD domains have been validated */
    CLMD_ACME_S_CERTIFIED,            /* CLMD has valid certificate */
    CLMD_ACME_S_DENIED,               /* CLMD domains (at least one) have been denied by CA */
} clmd_acme_state_t;

typedef struct clmd_acme_t clmd_acme_t;

typedef struct clmd_acme_req_t clmd_acme_req_t;
/**
 * Request callback on a successful HTTP response (status 2xx).
 */
typedef kuda_status_t clmd_acme_req_res_cb(clmd_acme_t *acme, 
                                        const struct clmd_http_response_t *res, void *baton);

/**
 * Request callback to initialize before sending. May be invoked more than once in
 * case of retries.
 */
typedef kuda_status_t clmd_acme_req_init_cb(clmd_acme_req_t *req, void *baton);

/**
 * Request callback on a successful response (HTTP response code 2xx) and content
 * type matching application/.*json.
 */
typedef kuda_status_t clmd_acme_req_json_cb(clmd_acme_t *acme, kuda_pool_t *p, 
                                         const kuda_table_t *headers, 
                                         struct clmd_json_t *jbody, void *baton);

/**
 * Request callback on detected errors.
 */
typedef kuda_status_t clmd_acme_req_err_cb(clmd_acme_req_t *req, 
                                        const struct clmd_result_t *result, void *baton);


typedef kuda_status_t clmd_acme_new_nonce_fn(clmd_acme_t *acme);
typedef kuda_status_t clmd_acme_req_init_fn(clmd_acme_req_t *req, struct clmd_json_t *jpayload);

typedef kuda_status_t clmd_acme_post_fn(clmd_acme_t *acme, 
                                     clmd_acme_req_init_cb *on_init,
                                     clmd_acme_req_json_cb *on_json,
                                     clmd_acme_req_res_cb *on_res,
                                     clmd_acme_req_err_cb *on_err,
                                     void *baton);

struct clmd_acme_t {
    const char *url;                /* directory url of the ACME service */
    const char *sname;              /* short name for the service, not necessarily unique */
    kuda_pool_t *p;
    const char *user_agent;
    const char *proxy_url;
    
    const char *acct_id;            /* local storage id account was loaded from or NULL */
    struct clmd_acme_acct_t *acct;  /* account at ACME server to use for requests */
    struct clmd_pkey_t *acct_key;   /* private RSA key belonging to account */
    
    int version;                    /* as detected from the server */
    union {
        struct {
            const char *new_authz;
            const char *new_cert;
            const char *new_reg;
            const char *revoke_cert;
            
        } v1;
        struct {
            const char *new_account;
            const char *new_order;
            const char *key_change;
            const char *revoke_cert;
            const char *new_nonce;
        } v2;
    } api;
    const char *ca_agreement;
    const char *acct_name;
    
    clmd_acme_new_nonce_fn *new_nonce_fn;
    clmd_acme_req_init_fn *req_init_fn;
    clmd_acme_post_fn *post_new_account_fn;
    
    struct clmd_http_t *http;
    
    const char *nonce;
    int max_retries;
    struct clmd_result_t *last;      /* result of last request */
};

/**
 * Global init, call once at start up.
 */
kuda_status_t clmd_acme_init(kuda_pool_t *pool, const char *base_version, int init_ssl);

/**
 * Create a new ACME server instance. If path is not NULL, will use that directory
 * for persisting information. Will load any information persisted in earlier session.
 * url needs only be specified for instances where this has never been persisted before.
 *
 * @param pacme   will hold the ACME server instance on success
 * @param p       pool to used
 * @param url     url of the server, optional if known at path
 * @param proxy_url optional url of a HTTP(S) proxy to use
 */
kuda_status_t clmd_acme_create(clmd_acme_t **pacme, kuda_pool_t *p, const char *url,
                            const char *proxy_url);

/**
 * Contact the ACME server and retrieve its directory information.
 * 
 * @param acme    the ACME server to contact
 */
kuda_status_t clmd_acme_setup(clmd_acme_t *acme, struct clmd_result_t *result);

void clmd_acme_report_result(clmd_acme_t *acme, kuda_status_t rv, struct clmd_result_t *result);

/**************************************************************************************************/
/* account handling */

/**
 * Clear any existing account data from acme instance.
 */
void clmd_acme_clear_acct(clmd_acme_t *acme);

kuda_status_t clmd_acme_POST_new_account(clmd_acme_t *acme, 
                                      clmd_acme_req_init_cb *on_init,
                                      clmd_acme_req_json_cb *on_json,
                                      clmd_acme_req_res_cb *on_res,
                                      clmd_acme_req_err_cb *on_err,
                                      void *baton);

/**
 * Get the local name of the account currently used by the acme instance.
 * Will be NULL if no account has been setup successfully.
 */
const char *clmd_acme_acct_id_get(clmd_acme_t *acme);
const char *clmd_acme_acct_url_get(clmd_acme_t *acme);

/** 
 * Specify the account to use by name in local store. On success, the account
 * the "current" one used by the acme instance.
 */
kuda_status_t clmd_acme_use_acct(clmd_acme_t *acme, struct clmd_store_t *store, 
                              kuda_pool_t *p, const char *acct_id);

/**
 * Get the local name of the account currently used by the acme instance.
 * Will be NULL if no account has been setup successfully.
 */
const char *clmd_acme_acct_id_get(clmd_acme_t *acme);

/**
 * Agree to the given Terms-of-Service url for the current account.
 */
kuda_status_t clmd_acme_agree(clmd_acme_t *acme, kuda_pool_t *p, const char *tos);

/**
 * Confirm with the server that the current account agrees to the Terms-of-Service
 * given in the agreement url.
 * If the known agreement is equal to this, nothing is done.
 * If it differs, the account is re-validated in the hope that the server
 * announces the Tos URL it wants. If this is equal to the agreement specified,
 * the server is notified of this. If the server requires a ToS that the account
 * thinks it has already given, it is resend.
 *
 * If an agreement is required, different from the current one, KUDA_INCOMPLETE is
 * returned and the agreement url is returned in the parameter.
 */
kuda_status_t clmd_acme_check_agreement(clmd_acme_t *acme, kuda_pool_t *p, 
                                     const char *agreement, const char **prequired);

kuda_status_t clmd_acme_save_acct(clmd_acme_t *acme, kuda_pool_t *p, struct clmd_store_t *store);
                               
/**
 * Deactivate the current account at the ACME server.. 
 */
kuda_status_t clmd_acme_acct_deactivate(clmd_acme_t *acme, kuda_pool_t *p);

/**************************************************************************************************/
/* request handling */

struct clmd_acme_req_t {
    clmd_acme_t *acme;              /* the ACME server to talk to */
    kuda_pool_t *p;                 /* pool for the request duration */
    
    const char *url;                /* url to POST the request to */
    const char *method;             /* HTTP method to use */
    kuda_table_t *prot_hdrs;        /* JWS headers needing protection (nonce) */
    struct clmd_json_t *req_json;   /* JSON to be POSTed in request body */

    kuda_table_t *resp_hdrs;        /* HTTP response headers */
    struct clmd_json_t *resp_json;  /* JSON response body received */
    
    kuda_status_t rv;               /* status of request */
    
    clmd_acme_req_init_cb *on_init;  /* callback to initialize the request before submit */
    clmd_acme_req_json_cb *on_json;  /* callback on successful JSON response */
    clmd_acme_req_res_cb *on_res;    /* callback on generic HTTP response */
    clmd_acme_req_err_cb *on_err;    /* callback on encountered error */
    int max_retries;                 /* how often this might be retried */
    void *baton;                     /* userdata for callbacks */
    struct clmd_result_t *result;    /* result of this request */
};

kuda_status_t clmd_acme_req_body_init(clmd_acme_req_t *req, struct clmd_json_t *payload);

kuda_status_t clmd_acme_GET(clmd_acme_t *acme, const char *url,
                         clmd_acme_req_init_cb *on_init,
                         clmd_acme_req_json_cb *on_json,
                         clmd_acme_req_res_cb *on_res,
                         clmd_acme_req_err_cb *on_err,
                         void *baton);
/**
 * Perform a POST against the ACME url. If a on_json callback is given and
 * the HTTP response is JSON, only this callback is invoked. Otherwise, on HTTP status
 * 2xx, the on_res callback is invoked. If no on_res is given, it is considered a
 * response error, since only JSON was expected.
 * At least one callback needs to be non-NULL.
 * 
 * @param acme        the ACME server to talk to
 * @param url         the url to send the request to
 * @param on_init     callback to initialize the request data
 * @param on_json     callback on successful JSON response
 * @param on_res      callback on successful HTTP response
 * @param baton       userdata for callbacks
 */
kuda_status_t clmd_acme_POST(clmd_acme_t *acme, const char *url,
                          clmd_acme_req_init_cb *on_init,
                          clmd_acme_req_json_cb *on_json,
                          clmd_acme_req_res_cb *on_res,
                          clmd_acme_req_err_cb *on_err,
                          void *baton);

/**
 * Retrieve a JSON resource from the ACME server 
 */
kuda_status_t clmd_acme_get_json(struct clmd_json_t **pjson, clmd_acme_t *acme, 
                              const char *url, kuda_pool_t *p);


kuda_status_t clmd_acme_req_body_init(clmd_acme_req_t *req, struct clmd_json_t *jpayload);

kuda_status_t clmd_acme_protos_add(struct kuda_hash_t *protos, kuda_pool_t *p);

#endif /* clmd_acme_h */
