/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <stdio.h>

#include <kuda_lib.h>
#include <kuda_strings.h>
#include <kuda_time.h>

#include "clmd.h"
#include "clmd_time.h"

kuda_time_t clmd_timeperiod_length(const clmd_timeperiod_t *period)
{
    return (period->start < period->end)? (period->end - period->start) : 0;
}

int clmd_timeperiod_contains(const clmd_timeperiod_t *period, kuda_time_t time)
{
    return clmd_timeperiod_has_started(period, time) 
        && !clmd_timeperiod_has_ended(period, time);
}

int clmd_timeperiod_has_started(const clmd_timeperiod_t *period, kuda_time_t time)
{
    return (time >= period->start);
}

int clmd_timeperiod_has_ended(const clmd_timeperiod_t *period, kuda_time_t time)
{
    return (time >= period->start) && (time <= period->end);
}

char *clmd_timeperiod_print(kuda_pool_t *p, const clmd_timeperiod_t *period)
{
    char tstart[KUDA_RFC822_DATE_LEN];
    char tend[KUDA_RFC822_DATE_LEN];

    kuda_rfc822_date(tstart, period->start);
    kuda_rfc822_date(tend, period->end);
    return kuda_pstrcat(p, tstart, " - ", tend, NULL);
}

const char *clmd_duration_print(kuda_pool_t *p, kuda_interval_time_t duration)
{
    const char *s = "", *sep = "";
    long days = (long)(kuda_time_sec(duration) / CLMD_SECS_PER_DAY);
    int rem = (int)(kuda_time_sec(duration) % CLMD_SECS_PER_DAY);
    
    if (days > 0) {
        s = kuda_psprintf(p, "%ld days", days);
        sep = " "; 
    }
    if (rem > 0) {
        int hours = (rem / CLMD_SECS_PER_HOUR);
        rem = (rem % CLMD_SECS_PER_HOUR);
        if (hours > 0) {
            s = kuda_psprintf(p, "%s%s%02d hours", s, sep, hours); 
            sep = " "; 
        }
        if (rem > 0) {
            int minutes = (rem / 60);
            rem = (rem % 60);
            if (minutes > 0) {
                s = kuda_psprintf(p, "%s%s%02d minutes", s, sep, minutes); 
            }
            if (rem > 0) {
                s = kuda_psprintf(p, "%s%s%02d seconds", s, sep, rem); 
            }
        }
    }
    else if (days == 0) {
        s = "0 seconds";
        if (duration != 0) {
            s = kuda_psprintf(p, "%d ms", (int)kuda_time_msec(duration));
        }
    }
    return s;
}

static const char *duration_format(kuda_pool_t *p, kuda_interval_time_t duration)
{
    const char *s = "0";
    int units = (int)(kuda_time_sec(duration) / CLMD_SECS_PER_DAY);
    int rem = (int)(kuda_time_sec(duration) % CLMD_SECS_PER_DAY);
    
    if (rem == 0) {
        s = kuda_psprintf(p, "%dd", units); 
    }
    else {
        units = (int)(kuda_time_sec(duration) / CLMD_SECS_PER_HOUR);
        rem = (int)(kuda_time_sec(duration) % CLMD_SECS_PER_HOUR);
        if (rem == 0) {
            s = kuda_psprintf(p, "%dh", units); 
        }
        else {
            units = (int)(kuda_time_sec(duration) / 60);
            rem = (int)(kuda_time_sec(duration) % 60);
            if (rem == 0) {
                s = kuda_psprintf(p, "%dmi", units); 
            }
            else {
                units = (int)(kuda_time_sec(duration));
                rem = (int)(kuda_time_msec(duration) % 1000);
                if (rem == 0) {
                    s = kuda_psprintf(p, "%ds", units); 
                }
                else {
                    s = kuda_psprintf(p, "%dms", (int)(kuda_time_msec(duration))); 
                }
            }
        }
    }
    return s;
}

kuda_status_t clmd_duration_parse(kuda_interval_time_t *ptimeout, const char *value, 
                               const char *def_unit)
{
    char *endp;
    kuda_int64_t n;
    
    n = kuda_strtoi64(value, &endp, 10);
    if (errno) {
        return errno;
    }
    if (!endp || !*endp) {
        if (!def_unit) def_unit = "s";
    }
    else if (endp == value) {
        return KUDA_EINVAL;
    }
    else {
        def_unit = endp;
    }
    
    switch (*def_unit) {
    case 'D':
    case 'd':
        *ptimeout = kuda_time_from_sec(n * CLMD_SECS_PER_DAY);
        break;
    case 's':
    case 'S':
        *ptimeout = (kuda_interval_time_t) kuda_time_from_sec(n);
        break;
    case 'h':
    case 'H':
        /* Time is in hours */
        *ptimeout = (kuda_interval_time_t) kuda_time_from_sec(n * CLMD_SECS_PER_HOUR);
        break;
    case 'm':
    case 'M':
        switch (*(++def_unit)) {
        /* Time is in milliseconds */
        case 's':
        case 'S':
            *ptimeout = (kuda_interval_time_t) n * 1000;
            break;
        /* Time is in minutes */
        case 'i':
        case 'I':
            *ptimeout = (kuda_interval_time_t) kuda_time_from_sec(n * 60);
            break;
        default:
            return KUDA_EGENERAL;
        }
        break;
    default:
        return KUDA_EGENERAL;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t percentage_parse(const char *value, int *ppercent)
{
    char *endp;
    kuda_int64_t n;
    
    n = kuda_strtoi64(value, &endp, 10);
    if (errno) {
        return errno;
    }
    if (*endp == '%') {
        if (n < 0) {
            return KUDA_BADARG;
        }
        *ppercent = (int)n;
        return KUDA_SUCCESS;
    }
    return KUDA_EINVAL;
}

kuda_status_t clmd_timeslice_create(const clmd_timeslice_t **pts, kuda_pool_t *p,
                                 kuda_interval_time_t norm, kuda_interval_time_t len)
{
    clmd_timeslice_t *ts;

    ts = kuda_pcalloc(p, sizeof(*ts));
    ts->norm = norm;
    ts->len = len;
    *pts = ts;
    return KUDA_SUCCESS;
}

const char *clmd_timeslice_parse(const clmd_timeslice_t **pts, kuda_pool_t *p, 
                               const char *val, kuda_interval_time_t norm)
{
    clmd_timeslice_t *ts;
    int percent = 0;

    *pts = NULL;
    if (!val) {
        return "cannot parse NULL value";
    }

    ts = kuda_pcalloc(p, sizeof(*ts));
    if (clmd_duration_parse(&ts->len, val, "d") == KUDA_SUCCESS) {
        *pts = ts;
        return NULL;
    }
    else {
        switch (percentage_parse(val, &percent)) {
            case KUDA_SUCCESS:
                ts->norm = norm;
                ts->len = kuda_time_from_sec((kuda_time_sec(norm) * percent / 100L));
                *pts = ts;
                return NULL;
            case KUDA_BADARG:
                return "percent must be less than 100";
        }
    }
    return "has unrecognized format";
}

const char *clmd_timeslice_format(const clmd_timeslice_t *ts, kuda_pool_t *p) {
    if (ts->norm > 0) {
        int percent = (int)(((long)kuda_time_sec(ts->len)) * 100L 
                            / ((long)kuda_time_sec(ts->norm))); 
        return kuda_psprintf(p, "%d%%", percent);
    }
    return duration_format(p, ts->len);
}

clmd_timeperiod_t clmd_timeperiod_slice_before_end(const clmd_timeperiod_t *period, 
                                               const clmd_timeslice_t *ts)
{
    clmd_timeperiod_t r;
    kuda_time_t duration = ts->len;
    
    if (ts->norm > 0) {
        int percent = (int)(((long)kuda_time_sec(ts->len)) * 100L 
                            / ((long)kuda_time_sec(ts->norm))); 
        kuda_time_t plen = clmd_timeperiod_length(period);
        if (kuda_time_sec(plen) > 100) {
            duration = kuda_time_from_sec(kuda_time_sec(plen) * percent / 100);
        }
        else {
            duration = plen * percent / 100;
        }
    }
    r.start = period->end - duration;
    r.end = period->end;
    return r;
}

int clmd_timeslice_eq(const clmd_timeslice_t *ts1, const clmd_timeslice_t *ts2)
{
    if (ts1 == ts2) return 1;
    if (!ts1 || !ts2) return 0;
    return (ts1->norm == ts2->norm) && (ts1->len == ts2->len);
}
