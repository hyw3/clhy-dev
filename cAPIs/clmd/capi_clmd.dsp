# Microsoft Developer Studio Project File - Name="capi_clmd" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=capi_clmd - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "capi_clmd.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "capi_clmd.mak" CFG="capi_clmd - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "capi_clmd - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "capi_clmd - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "capi_clmd - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /CLMD /W3 /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "ssize_t=long" /FD /c
# ADD CPP /nologo /CLMD /W3 /O2 /Oy- /Zi /I "../../server/clcore/winnt" "/I ../clssl" /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/openssl/inc32" /I "../../kudelrunsrc/jansson/include" /I "../../kudelrunsrc/curl/include" /I "../core"   /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "ssize_t=long" /Fd"Release\capi_clmd_src" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /fo"Release/capi_clmd.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "NDEBUG" /d "BIN_NAME=capi_clmd.so" /d "LONG_NAME=Letsencrypt cAPI for cLHy"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:windows /dll /out:".\Release\capi_clmd.so" /base:@..\..\platforms\win32\BaseAddr.ref,capi_clmd.so
# ADD LINK32 kernel32.lib libwwhy.lib libkuda-1.lib libkudadelman-1.lib ssleay32.lib libeay32.lib jansson.lib libcurl.lib /libpath:"../../kudelrunsrc/kuda/Release" /libpath:"../../kudelrunsrc/kuda-delman/Release" /libpath:"../../Release/" /libpath:"../../kudelrunsrc/openssl/out32dll" /libpath:"../../kudelrunsrc/jansson/lib" /libpath:"../../kudelrunsrc/curl/lib" /nologo /subsystem:windows /dll /incremental:no /debug /out:".\Release\capi_clmd.so" /base:@..\..\platforms\win32\BaseAddr.ref,capi_clmd.so /opt:ref
# Begin Special Build Tool
TargetPath=.\Release\capi_clmd.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ELSEIF  "$(CFG)" == "capi_clmd - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /EHsc /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "ssize_t=long" /FD /c
# ADD CPP /nologo /MDd /W3 /EHsc /Zi /Od /I "../clssl" /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/openssl/inc32" /I "../../kudelrunsrc/jansson/include" /I "../../kudelrunsrc/curl/include" /I "../core" /src" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "ssize_t=long" /Fd"Debug\capi_clmd_src" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /fo"Debug/capi_clmd.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "_DEBUG" /d "BIN_NAME=capi_clmd.so" /d "LONG_NAME=clmd_capi for cLHy"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:windows /dll /incremental:no /debug /out:".\Debug\capi_clmd.so" /base:@..\..\platforms\win32\BaseAddr.ref,capi_clmd.so
# ADD LINK32 kernel32.lib libwwhy.lib libkuda-1.lib libkudadelman-1.lib ssleay32.lib libeay32.lib jansson_d.lib libcurl_debug.lib /nologo /subsystem:windows /dll /libpath:"../../kudelrunsrc/openssl/out32dll" /libpath:"../../kudelrunsrc/jansson/lib" /libpath:"../../kudelrunsrc/curl/lib" /incremental:no /debug /out:".\Debug\capi_clmd.so" /base:@..\..\platforms\win32\BaseAddr.ref,capi_clmd.so
# Begin Special Build Tool
TargetPath=.\Debug\capi_clmd.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "capi_clmd - Win32 Release"
# Name "capi_clmd - Win32 Debug"
# Begin Source File

SOURCE=./capi_clmd.c
# End Source File
# Begin Source File

SOURCE=./capi_clmd_config.c
# End Source File
# Begin Source File

SOURCE=./capi_clmd_drive.c
# End Source File
# Begin Source File

SOURCE=./capi_clmd_platform.c
# End Source File
# Begin Source File

SOURCE=./capi_clmd_status.c
# End Source File
# Begin Source File

SOURCE=./clmd_acme.c
# End Source File
# Begin Source File

SOURCE=./clmd_acme_acct.c
# End Source File
# Begin Source File

SOURCE=./clmd_acme_authz.c
# End Source File
# Begin Source File

SOURCE=./clmd_acme_drive.c
# End Source File
# Begin Source File

SOURCE=./clmd_acme_order.c
# End Source File
# Begin Source File

SOURCE=./clmd_acmev1_drive.c
# End Source File
# Begin Source File

SOURCE=./clmd_acmev2_drive.c
# End Source File
# Begin Source File

SOURCE=./clmd_core.c
# End Source File
# Begin Source File

SOURCE=./clmd_crypt.c
# End Source File
# Begin Source File

SOURCE=./clmd_curl.c
# End Source File
# Begin Source File

SOURCE=./clmd_http.c
# End Source File
# Begin Source File

SOURCE=./clmd_json.c
# End Source File
# Begin Source File

SOURCE=./clmd_jws.c
# End Source File
# Begin Source File

SOURCE=./clmd_log.c
# End Source File
# Begin Source File

SOURCE=./clmd_reg.c
# End Source File
# Begin Source File

SOURCE=./clmd_result.c
# End Source File
# Begin Source File

SOURCE=./clmd_status.c
# End Source File
# Begin Source File

SOURCE=./clmd_store.c
# End Source File
# Begin Source File

SOURCE=./clmd_store_fs.c
# End Source File
# Begin Source File

SOURCE=./clmd_time.c
# End Source File
# Begin Source File

SOURCE=./clmd_util.c
# End Source File
# Begin Source File

SOURCE=..\..\build\win32\wwhy.rc
# End Source File
# End Target
# End Project
