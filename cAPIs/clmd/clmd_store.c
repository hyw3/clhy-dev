/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include <kuda_lib.h>
#include <kuda_file_info.h>
#include <kuda_file_io.h>
#include <kuda_fnmatch.h>
#include <kuda_hash.h>
#include <kuda_strings.h>

#include "clmd.h"
#include "clmd_crypt.h"
#include "clmd_log.h"
#include "clmd_json.h"
#include "clmd_store.h"
#include "clmd_util.h"

/**************************************************************************************************/
/* generic callback handling */

#define ASPECT_MD           "clmd.json"
#define ASPECT_CERT         "cert.pem"
#define ASPECT_PKEY         "key.pem"
#define ASPECT_CHAIN        "chain.pem"

#define GNAME_ACCOUNTS     
#define GNAME_CHALLENGES   
#define GNAME_DOMAINS      
#define GNAME_STAGING      
#define GNAME_ARCHIVE      

static const char *GROUP_NAME[] = {
    "none",
    "accounts",
    "challenges",
    "domains",
    "staging",
    "archive",
    "tmp",
    NULL
};

const char *clmd_store_group_name(int group)
{
    if ((size_t)group < sizeof(GROUP_NAME)/sizeof(GROUP_NAME[0])) {
        return GROUP_NAME[group];
    }
    return "UNKNOWN";
}

void clmd_store_destroy(clmd_store_t *store)
{
    if (store->destroy) store->destroy(store);
}

kuda_status_t clmd_store_load(clmd_store_t *store, clmd_store_group_t group, 
                           const char *name, const char *aspect, 
                           clmd_store_vtype_t vtype, void **pdata, 
                           kuda_pool_t *p)
{
    return store->load(store, group, name, aspect, vtype, pdata, p);
}

kuda_status_t clmd_store_save(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group, 
                           const char *name, const char *aspect, 
                           clmd_store_vtype_t vtype, void *data, 
                           int create)
{
    return store->save(store, p, group, name, aspect, vtype, data, create);
}

kuda_status_t clmd_store_remove(clmd_store_t *store, clmd_store_group_t group, 
                             const char *name, const char *aspect, 
                             kuda_pool_t *p, int force)
{
    return store->remove(store, group, name, aspect, p, force);
}

kuda_status_t clmd_store_purge(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group, 
                             const char *name)
{
    return store->purge(store, p, group, name);
}

kuda_status_t clmd_store_iter(clmd_store_inspect *inspect, void *baton, clmd_store_t *store, 
                           kuda_pool_t *p, clmd_store_group_t group, const char *pattern, 
                           const char *aspect, clmd_store_vtype_t vtype)
{
    return store->iterate(inspect, baton, store, p, group, pattern, aspect, vtype);
}

kuda_status_t clmd_store_load_json(clmd_store_t *store, clmd_store_group_t group, 
                                const char *name, const char *aspect, 
                                struct clmd_json_t **pdata, kuda_pool_t *p)
{
    return clmd_store_load(store, group, name, aspect, CLMD_SV_JSON, (void**)pdata, p);
}

kuda_status_t clmd_store_save_json(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group, 
                                const char *name, const char *aspect, 
                                struct clmd_json_t *data, int create)
{
    return clmd_store_save(store, p, group, name, aspect, CLMD_SV_JSON, (void*)data, create);
}

kuda_status_t clmd_store_move(clmd_store_t *store, kuda_pool_t *p, 
                           clmd_store_group_t from, clmd_store_group_t to,
                           const char *name, int archive)
{
    return store->move(store, p, from, to, name, archive);
}

kuda_status_t clmd_store_get_fname(const char **pfname, 
                                clmd_store_t *store, clmd_store_group_t group, 
                                const char *name, const char *aspect, 
                                kuda_pool_t *p)
{
    if (store->get_fname) {
        return store->get_fname(pfname, store, group, name, aspect, p);
    }
    return KUDA_ENOTIMPL;
}

int clmd_store_is_newer(clmd_store_t *store, clmd_store_group_t group1, clmd_store_group_t group2,  
                      const char *name, const char *aspect, kuda_pool_t *p)
{
    return store->is_newer(store, group1, group2, name, aspect, p);
}

kuda_status_t clmd_store_iter_names(clmd_store_inspect *inspect, void *baton, clmd_store_t *store, 
                                 kuda_pool_t *p, clmd_store_group_t group, const char *pattern)
{
    return store->iterate_names(inspect, baton, store, p, group, pattern);
}

/**************************************************************************************************/
/* convenience */

typedef struct {
    clmd_store_t *store;
    clmd_store_group_t group;
} clmd_group_ctx;

kuda_status_t clmd_load(clmd_store_t *store, clmd_store_group_t group, 
                     const char *name, clmd_t **pmd, kuda_pool_t *p)
{
    clmd_json_t *json;
    kuda_status_t rv;
    
    rv = clmd_store_load_json(store, group, name, CLMD_FN_MD, pmd? &json : NULL, p);
    if (KUDA_SUCCESS == rv) {
        if (pmd) {
            *pmd = clmd_from_json(json, p);
        }
        return KUDA_SUCCESS;
    }
    return rv;
}

static kuda_status_t p_save(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_group_ctx *ctx = baton;
    clmd_json_t *json;
    clmd_t *clmd;
    int create;
    
    clmd = va_arg(ap, clmd_t *);
    create = va_arg(ap, int);

    json = clmd_to_json(clmd, ptemp);
    assert(json);
    assert(clmd->name);
    return clmd_store_save_json(ctx->store, p, ctx->group, clmd->name, CLMD_FN_MD, json, create);
}

kuda_status_t clmd_save(clmd_store_t *store, kuda_pool_t *p, 
                     clmd_store_group_t group, clmd_t *clmd, int create)
{
    clmd_group_ctx ctx;
    
    ctx.store = store;
    ctx.group = group;
    return clmd_util_pool_vdo(p_save, &ctx, p, clmd, create, NULL);
}

static kuda_status_t p_remove(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_group_ctx *ctx = baton;
    const char *name;
    int force;
    
    (void)p;
    name = va_arg(ap, const char *);
    force = va_arg(ap, int);

    assert(name);
    return clmd_store_remove(ctx->store, ctx->group, name, CLMD_FN_MD, ptemp, force);
}

kuda_status_t clmd_remove(clmd_store_t *store, kuda_pool_t *p, 
                       clmd_store_group_t group, const char *name, int force)
{
    clmd_group_ctx ctx;
    
    ctx.store = store;
    ctx.group = group;
    return clmd_util_pool_vdo(p_remove, &ctx, p, name, force, NULL);
}

int clmd_is_newer(clmd_store_t *store, clmd_store_group_t group1, clmd_store_group_t group2,  
                      const char *name, kuda_pool_t *p)
{
    return clmd_store_is_newer(store, group1, group2, name, CLMD_FN_MD, p);
}


typedef struct {
    kuda_pool_t *p;
    kuda_array_header_t *mds;
} clmd_load_ctx;

kuda_status_t clmd_pkey_load(clmd_store_t *store, clmd_store_group_t group, const char *name, 
                          clmd_pkey_t **ppkey, kuda_pool_t *p)
{
    return clmd_store_load(store, group, name, CLMD_FN_PRIVKEY, CLMD_SV_PKEY, (void**)ppkey, p);
}

kuda_status_t clmd_pkey_save(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group, const char *name, 
                          struct clmd_pkey_t *pkey, int create)
{
    return clmd_store_save(store, p, group, name, CLMD_FN_PRIVKEY, CLMD_SV_PKEY, pkey, create);
}

kuda_status_t clmd_pubcert_load(clmd_store_t *store, clmd_store_group_t group, const char *name, 
                             struct kuda_array_header_t **ppubcert, kuda_pool_t *p)
{
    return clmd_store_load(store, group, name, CLMD_FN_PUBCERT, CLMD_SV_CHAIN, (void**)ppubcert, p);
}

kuda_status_t clmd_pubcert_save(clmd_store_t *store, kuda_pool_t *p, 
                             clmd_store_group_t group, const char *name, 
                             struct kuda_array_header_t *pubcert, int create)
{
    return clmd_store_save(store, p, group, name, CLMD_FN_PUBCERT, CLMD_SV_CHAIN, pubcert, create);
}

typedef struct {
    clmd_store_t *store;
    clmd_store_group_t group;
    const char *pattern;
    const char *aspect;
    clmd_store_clmd_inspect *inspect;
    void *baton;
} inspect_clmd_ctx;

static int insp_clmd(void *baton, const char *name, const char *aspect, 
                   clmd_store_vtype_t vtype, void *value, kuda_pool_t *ptemp)
{
    inspect_clmd_ctx *ctx = baton;
    
    if (!strcmp(CLMD_FN_MD, aspect) && vtype == CLMD_SV_JSON) {
        clmd_t *clmd = clmd_from_json(value, ptemp);
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, 0, ptemp, "inspecting clmd at: %s", name);
        return ctx->inspect(ctx->baton, ctx->store, clmd, ptemp);
    }
    return 1;
}

kuda_status_t clmd_store_clmd_iter(clmd_store_clmd_inspect *inspect, void *baton, clmd_store_t *store, 
                              kuda_pool_t *p, clmd_store_group_t group, const char *pattern)
{
    inspect_clmd_ctx ctx;
    
    ctx.store = store;
    ctx.group = group;
    ctx.inspect = inspect;
    ctx.baton = baton;
    
    return clmd_store_iter(insp_clmd, &ctx, store, p, group, pattern, CLMD_FN_MD, CLMD_SV_JSON);
}

