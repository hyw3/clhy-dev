dnl The cLHy Server
dnl 
dnl Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
dnl The HLF licenses this file under the GNU GPL version 3 or later
dnl (the "License"); you may not use this file except in compliance with
dnl the License.  You may obtain a copy of the License at
dnl
dnl      http://clhy.hyang.org/license.hyss
dnl
dnl Unless required by applicable law or agreed to in writing, software
dnl distributed under the License is distributed on an "AS IS" BASIS,
dnl WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
dnl See the License for the specific language governing permissions and
dnl limitations under the License.

dnl
dnl CLHYKUDEL_CHECK_CURL
dnl
dnl Configure for libcurl, giving preference to
dnl "--with-curl=<path>" if it was specified.
dnl
AC_DEFUN([CLHYKUDEL_CHECK_CURL],[
  AC_CACHE_CHECK([for curl], [ac_cv_curl], [
    dnl initialise the variables we use
    ac_cv_curl=no
    clhy_curl_found=""
    clhy_curl_base=""
    clhy_curl_libs=""

    dnl Determine the curl base directory, if any
    AC_MSG_CHECKING([for user-provided curl base directory])
    AC_ARG_WITH(curl, CLHYKUDEL_HELP_STRING(--with-curl=PATH, curl installation directory), [
      dnl If --with-curl specifies a directory, we use that directory
      if test "x$withval" != "xyes" -a "x$withval" != "x"; then
        dnl This ensures $withval is actually a directory and that it is absolute
        clhy_curl_base="`cd $withval ; pwd`"
      fi
    ])
    if test "x$clhy_curl_base" = "x"; then
      AC_MSG_RESULT(none)
    else
      AC_MSG_RESULT($clhy_curl_base)
    fi

    dnl Run header and version checks
    saved_CPPFLAGS="$CPPFLAGS"
    saved_LIBS="$LIBS"
    saved_LDFLAGS="$LDFLAGS"

    dnl Before doing anything else, load in pkg-config variables
    if test -n "$PKGCONFIG"; then
      saved_PKG_CONFIG_PATH="$PKG_CONFIG_PATH"
      AC_MSG_CHECKING([for pkg-config along $PKG_CONFIG_PATH])
      if test "x$clhy_curl_base" != "x" ; then
        if test -f "${clhy_curl_base}/lib/pkgconfig/libcurl.pc"; then
          dnl Ensure that the given path is used by pkg-config too, otherwise
          dnl the system libcurl.pc might be picked up instead.
          PKG_CONFIG_PATH="${clhy_curl_base}/lib/pkgconfig${PKG_CONFIG_PATH+:}${PKG_CONFIG_PATH}"
          export PKG_CONFIG_PATH
        elif test -f "${clhy_curl_base}/lib64/pkgconfig/libcurl.pc"; then
          dnl Ensure that the given path is used by pkg-config too, otherwise
          dnl the system libcurl.pc might be picked up instead.
          PKG_CONFIG_PATH="${clhy_curl_base}/lib64/pkgconfig${PKG_CONFIG_PATH+:}${PKG_CONFIG_PATH}"
          export PKG_CONFIG_PATH
        fi
      fi
      AC_ARG_ENABLE(curl-staticlib-deps,CLHYKUDEL_HELP_STRING(--enable-curl-staticlib-deps,[link capi_clmd with dependencies of libcurl's static libraries (as indicated by "pkg-config --static"). Must be specified in addition to --enable-clmd.]), [
        if test "$enableval" = "yes"; then
          PKGCONFIG_LIBOPTS="--static"
        fi
      ])
      clhy_curl_libs="`$PKGCONFIG $PKGCONFIG_LIBOPTS --libs-only-l --silence-errors libcurl`"
      if test $? -eq 0; then
        clhy_curl_found="yes"
        pkglookup="`$PKGCONFIG --cflags-only-I libcurl`"
        KUDA_ADDTO(CPPFLAGS, [$pkglookup])
        KUDA_ADDTO(CAPI_CFLAGS, [$pkglookup])
        pkglookup="`$PKGCONFIG $PKGCONFIG_LIBOPTS --libs-only-L libcurl`"
        KUDA_ADDTO(LDFLAGS, [$pkglookup])
        KUDA_ADDTO(CAPI_LDFLAGS, [$pkglookup])
        pkglookup="`$PKGCONFIG $PKGCONFIG_LIBOPTS --libs-only-other libcurl`"
        KUDA_ADDTO(LDFLAGS, [$pkglookup])
        KUDA_ADDTO(CAPI_LDFLAGS, [$pkglookup])
      fi
      PKG_CONFIG_PATH="$saved_PKG_CONFIG_PATH"
    fi

    dnl fall back to the user-supplied directory if not found via pkg-config
    if test "x$clhy_curl_base" != "x" -a "x$clhy_curl_found" = "x"; then
      KUDA_ADDTO(CPPFLAGS, [-I$clhy_curl_base/include])
      KUDA_ADDTO(CAPI_CFLAGS, [-I$clhy_curl_base/include])
      KUDA_ADDTO(LDFLAGS, [-L$clhy_curl_base/lib])
      KUDA_ADDTO(CAPI_LDFLAGS, [-L$clhy_curl_base/lib])
      if test "x$clhy_platform_runtime_link_flag" != "x"; then
        KUDA_ADDTO(LDFLAGS, [$clhy_platform_runtime_link_flag$clhy_curl_base/lib])
        KUDA_ADDTO(CAPI_LDFLAGS, [$clhy_platform_runtime_link_flag$clhy_curl_base/lib])
      fi
    fi

    AC_CHECK_HEADERS([curl/curl.h])

    AC_MSG_CHECKING([for curl version >= 7.50])
    AC_TRY_COMPILE([#include <curl/curlver.h>],[
#if !defined(LIBCURL_VERSION_MAJOR)
#error "Missing libcurl version"
#endif
#if LIBCURL_VERSION_MAJOR < 7
#error "Unsupported libcurl version " LIBCURL_VERSION
#endif
#if LIBCURL_VERSION_MAJOR == 7 && LIBCURL_VERSION_MINOR < 50
#error "Unsupported libcurl version " LIBCURL_VERSION
#endif],
      [AC_MSG_RESULT(OK)
       ac_cv_curl=yes],
      [AC_MSG_RESULT(FAILED)])

    if test "x$ac_cv_curl" = "xyes"; then
      clhy_curl_libs="${clhy_curl_libs:--lcurl} `$kuda_config --libs`"
      KUDA_ADDTO(CAPI_LDFLAGS, [$clhy_curl_libs])
      KUDA_ADDTO(LIBS, [$clhy_curl_libs])
    fi

    dnl restore
    CPPFLAGS="$saved_CPPFLAGS"
    LIBS="$saved_LIBS"
    LDFLAGS="$saved_LDFLAGS"
  ])
  if test "x$ac_cv_curl" = "xyes"; then
    AC_DEFINE(HAVE_CURL, 1, [Define if curl is available])
  fi
])


dnl
dnl CLHYKUDEL_CHECK_JANSSON
dnl
dnl Configure for libjansson, giving preference to
dnl "--with-jansson=<path>" if it was specified.
dnl
AC_DEFUN([CLHYKUDEL_CHECK_JANSSON],[
  AC_CACHE_CHECK([for jansson], [ac_cv_jansson], [
    dnl initialise the variables we use
    ac_cv_jansson=no
    clhy_jansson_found=""
    clhy_jansson_base=""
    clhy_jansson_libs=""

    dnl Determine the jansson base directory, if any
    AC_MSG_CHECKING([for user-provided jansson base directory])
    AC_ARG_WITH(jansson, CLHYKUDEL_HELP_STRING(--with-jansson=PATH, jansson installation directory), [
      dnl If --with-jansson specifies a directory, we use that directory
      if test "x$withval" != "xyes" -a "x$withval" != "x"; then
        dnl This ensures $withval is actually a directory and that it is absolute
        clhy_jansson_base="`cd $withval ; pwd`"
      fi
    ])
    if test "x$clhy_jansson_base" = "x"; then
      AC_MSG_RESULT(none)
    else
      AC_MSG_RESULT($clhy_jansson_base)
    fi

    dnl Run header and version checks
    saved_CPPFLAGS="$CPPFLAGS"
    saved_LIBS="$LIBS"
    saved_LDFLAGS="$LDFLAGS"

    dnl Before doing anything else, load in pkg-config variables
    if test -n "$PKGCONFIG"; then
      saved_PKG_CONFIG_PATH="$PKG_CONFIG_PATH"
      AC_MSG_CHECKING([for pkg-config along $PKG_CONFIG_PATH])
      if test "x$clhy_jansson_base" != "x" ; then
        if test -f "${clhy_jansson_base}/lib/pkgconfig/libjansson.pc"; then
          dnl Ensure that the given path is used by pkg-config too, otherwise
          dnl the system libjansson.pc might be picked up instead.
          PKG_CONFIG_PATH="${clhy_jansson_base}/lib/pkgconfig${PKG_CONFIG_PATH+:}${PKG_CONFIG_PATH}"
          export PKG_CONFIG_PATH
        elif test -f "${clhy_jansson_base}/lib64/pkgconfig/libjansson.pc"; then
          dnl Ensure that the given path is used by pkg-config too, otherwise
          dnl the system libjansson.pc might be picked up instead.
          PKG_CONFIG_PATH="${clhy_jansson_base}/lib64/pkgconfig${PKG_CONFIG_PATH+:}${PKG_CONFIG_PATH}"
          export PKG_CONFIG_PATH
        fi
      fi
      AC_ARG_ENABLE(jansson-staticlib-deps,CLHYKUDEL_HELP_STRING(--enable-jansson-staticlib-deps,[link capi_clmd with dependencies of libjansson's static libraries (as indicated by "pkg-config --static"). Must be specified in addition to --enable-clmd.]), [
        if test "$enableval" = "yes"; then
          PKGCONFIG_LIBOPTS="--static"
        fi
      ])
      clhy_jansson_libs="`$PKGCONFIG $PKGCONFIG_LIBOPTS --libs-only-l --silence-errors libjansson`"
      if test $? -eq 0; then
        clhy_jansson_found="yes"
        pkglookup="`$PKGCONFIG --cflags-only-I libjansson`"
        KUDA_ADDTO(CPPFLAGS, [$pkglookup])
        KUDA_ADDTO(CAPI_CFLAGS, [$pkglookup])
        pkglookup="`$PKGCONFIG $PKGCONFIG_LIBOPTS --libs-only-L libjansson`"
        KUDA_ADDTO(LDFLAGS, [$pkglookup])
        KUDA_ADDTO(CAPI_LDFLAGS, [$pkglookup])
        pkglookup="`$PKGCONFIG $PKGCONFIG_LIBOPTS --libs-only-other libjansson`"
        KUDA_ADDTO(LDFLAGS, [$pkglookup])
        KUDA_ADDTO(CAPI_LDFLAGS, [$pkglookup])
      fi
      PKG_CONFIG_PATH="$saved_PKG_CONFIG_PATH"
    fi

    dnl fall back to the user-supplied directory if not found via pkg-config
    if test "x$clhy_jansson_base" != "x" -a "x$clhy_jansson_found" = "x"; then
      KUDA_ADDTO(CPPFLAGS, [-I$clhy_jansson_base/include])
      KUDA_ADDTO(CAPI_CFLAGS, [-I$clhy_jansson_base/include])
      KUDA_ADDTO(LDFLAGS, [-L$clhy_jansson_base/lib])
      KUDA_ADDTO(CAPI_LDFLAGS, [-L$clhy_jansson_base/lib])
      if test "x$clhy_platform_runtime_link_flag" != "x"; then
        KUDA_ADDTO(LDFLAGS, [$clhy_platform_runtime_link_flag$clhy_jansson_base/lib])
        KUDA_ADDTO(CAPI_LDFLAGS, [$clhy_platform_runtime_link_flag$clhy_jansson_base/lib])
      fi
    fi

    # attempts to include jansson.h fail me. So lets make sure we can at least
    # include its other header file
    AC_TRY_COMPILE([#include <jansson_config.h>],[],
      [AC_MSG_RESULT(OK) 
       ac_cv_jansson=yes], 
       [AC_MSG_RESULT(FAILED)])

    if test "x$ac_cv_jansson" = "xyes"; then
      clhy_jansson_libs="${clhy_jansson_libs:--ljansson} `$kuda_config --libs`"
      KUDA_ADDTO(CAPI_LDFLAGS, [$clhy_jansson_libs])
      KUDA_ADDTO(LIBS, [$clhy_jansson_libs])
    fi

    dnl restore
    CPPFLAGS="$saved_CPPFLAGS"
    LIBS="$saved_LIBS"
    LDFLAGS="$saved_LDFLAGS"
  ])
  if test "x$ac_cv_jansson" = "xyes"; then
    AC_DEFINE(HAVE_JANSSON, 1, [Define if jansson is available])
  fi
])


dnl #  start of cAPI specific part
CLHYKUDEL_CAPIPATH_INIT(clmd)

dnl #  list of cAPI object files
clmd_objs="dnl
clmd_acme.lo dnl
clmd_acme_acct.lo dnl
clmd_acme_authz.lo dnl
clmd_acme_drive.lo dnl
clmd_acmev1_drive.lo dnl
clmd_acmev2_drive.lo dnl
clmd_acme_order.lo dnl
clmd_core.lo dnl
clmd_curl.lo dnl
clmd_crypt.lo dnl
clmd_http.lo dnl
clmd_json.lo dnl
clmd_jws.lo dnl
clmd_log.lo dnl
clmd_result.lo dnl
clmd_reg.lo dnl
clmd_status.lo dnl
clmd_store.lo dnl
clmd_store_fs.lo dnl
clmd_time.lo dnl
clmd_util.lo dnl
capi_clmd.lo dnl
capi_clmd_config.lo dnl
capi_clmd_drive.lo dnl
capi_clmd_platform.lo dnl
capi_clmd_status.lo dnl
"

# Ensure that other cAPIs can pick up capi_clmd.h
KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

dnl # hook cAPI into the Autoconf mechanism (--enable-clmd)
CLHYKUDEL_CAPI(clmd, [Managed Domain handling], $clmd_objs, , most, [
    CLHYKUDEL_CHECK_OPENSSL
    if test "x$ac_cv_openssl" = "xno" ; then
        AC_MSG_WARN([libssl (or compatible) not found])
        enable_clmd=no
    fi
    
    CLHYKUDEL_CHECK_JANSSON
    if test "x$ac_cv_jansson" != "xyes" ; then
        AC_MSG_WARN([libjansson not found])
        enable_clmd=no
    fi

    CLHYKUDEL_CHECK_CURL
    if test "x$ac_cv_curl" != "xyes" ; then
        AC_MSG_WARN([libcurl not found])
        enable_clmd=no
    fi
    
    AC_CHECK_FUNCS([arc4random_buf], 
        [KUDA_ADDTO(CAPI_CPPFLAGS, ["-DCLMD_HAVE_ARC4RANDOM"])], [])

    if test "x$enable_clmd" = "xshared"; then
       KUDA_ADDTO(CAPI_CLMD_LDADD, [-export-symbols-regex clmd_capi])
    fi
])

dnl #  end of cAPI specific part
CLHYKUDEL_CAPIPATH_FINISH

