/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <stdio.h>

#include <kuda_lib.h>
#include <kuda_strings.h>
#include <kuda_portable.h>
#include <kuda_file_info.h>
#include <kuda_fnmatch.h>
#include <kuda_tables.h>
#include <kuda_uri.h>

#include "clmd.h"
#include "clmd_log.h"
#include "clmd_util.h"

/**************************************************************************************************/
/* pool utils */

kuda_status_t clmd_util_pool_do(clmd_util_action *cb, void *baton, kuda_pool_t *p)
{
    kuda_pool_t *ptemp;
    kuda_status_t rv = kuda_pool_create(&ptemp, p);
    if (KUDA_SUCCESS == rv) {
        rv = cb(baton, p, ptemp);
        
        kuda_pool_destroy(ptemp);
    }
    return rv;
}
 
static kuda_status_t pool_vado(clmd_util_vaction *cb, void *baton, kuda_pool_t *p, va_list ap)
{
    kuda_pool_t *ptemp;
    kuda_status_t rv;
    
    rv = kuda_pool_create(&ptemp, p);
    if (KUDA_SUCCESS == rv) {
        rv = cb(baton, p, ptemp, ap);
        kuda_pool_destroy(ptemp);
    }
    return rv;
}
 
kuda_status_t clmd_util_pool_vdo(clmd_util_vaction *cb, void *baton, kuda_pool_t *p, ...)
{
    va_list ap;
    kuda_status_t rv;
    
    va_start(ap, p);
    rv = pool_vado(cb, baton, p, ap);
    va_end(ap);
    return rv;
}
 
/**************************************************************************************************/
/* data chunks */

clmd_data *clmd_data_create(kuda_pool_t *p, const char *data, kuda_size_t len)
{
    clmd_data *d;
    
    d = kuda_palloc(p, sizeof(*d));
    d->len = len;
    d->data = len? kuda_pstrndup(p, data, len) : NULL;
    return d;
}

static const char * const hex_const[] = {
    "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "0a", "0b", "0c", "0d", "0e", "0f", 
    "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "1a", "1b", "1c", "1d", "1e", "1f", 
    "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "2a", "2b", "2c", "2d", "2e", "2f", 
    "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "3a", "3b", "3c", "3d", "3e", "3f", 
    "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "4a", "4b", "4c", "4d", "4e", "4f", 
    "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "5a", "5b", "5c", "5d", "5e", "5f", 
    "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6a", "6b", "6c", "6d", "6e", "6f", 
    "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "7a", "7b", "7c", "7d", "7e", "7f", 
    "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "8a", "8b", "8c", "8d", "8e", "8f", 
    "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "9a", "9b", "9c", "9d", "9e", "9f", 
    "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "aa", "ab", "ac", "ad", "ae", "af", 
    "b0", "b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9", "ba", "bb", "bc", "bd", "be", "bf", 
    "c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "ca", "cb", "cc", "cd", "ce", "cf", 
    "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "da", "db", "dc", "dd", "de", "df", 
    "e0", "e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9", "ea", "eb", "ec", "ed", "ee", "ef", 
    "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "fa", "fb", "fc", "fd", "fe", "ff", 
};

kuda_status_t clmd_data_to_hex(const char **phex, char separator,
                            kuda_pool_t *p, const clmd_data *data)
{
    char *hex, *cp;
    const char * x;
    unsigned int i;
    
    cp = hex = kuda_pcalloc(p, ((separator? 3 : 2) * data->len) + 1);
    if (!hex) {
        *phex = NULL;
        return KUDA_ENOMEM;
    }
    for (i = 0; i < data->len; ++i) {
        x = hex_const[(unsigned char)data->data[i]];
        if (i && separator) *cp++ = separator;
        *cp++ = x[0];
        *cp++ = x[1];
    }
    *phex = hex;
    return KUDA_SUCCESS;
}

/**************************************************************************************************/
/* string related */

int clmd_array_is_empty(const struct kuda_array_header_t *array)
{
    return (array == NULL) || (array->nelts == 0);
}

char *clmd_util_str_tolower(char *s)
{
    char *orig = s;
    while (*s) {
        *s = (char)kuda_tolower(*s);
        ++s;
    }
    return orig;
}

int clmd_array_str_index(const kuda_array_header_t *array, const char *s, 
                       int start, int case_sensitive)
{
    if (start >= 0) {
        int i;
        
        for (i = start; i < array->nelts; i++) {
            const char *p = KUDA_ARRAY_IDX(array, i, const char *);
            if ((case_sensitive && !strcmp(p, s))
                || (!case_sensitive && !kuda_strnatcasecmp(p, s))) {
                return i;
            }
        }
    }
    
    return -1;
}

int clmd_array_str_eq(const struct kuda_array_header_t *a1, 
                    const struct kuda_array_header_t *a2, int case_sensitive)
{
    int i;
    const char *s1, *s2;
    
    if (a1 == a2) return 1;
    if (!a1) return 0;
    if (a1->nelts != a2->nelts) return 0;
    for (i = 0; i < a1->nelts; ++i) {
        s1 = KUDA_ARRAY_IDX(a1, i, const char *);
        s2 = KUDA_ARRAY_IDX(a2, i, const char *);
        if ((case_sensitive && strcmp(s1, s2))
            || (!case_sensitive && kuda_strnatcasecmp(s1, s2))) {
            return 0;
        }
    }
    return 1;
}

kuda_array_header_t *clmd_array_str_clone(kuda_pool_t *p, kuda_array_header_t *src)
{
    kuda_array_header_t *dest = kuda_array_make(p, src->nelts, sizeof(const char*));
    if (dest) {
        int i;
        for (i = 0; i < src->nelts; i++) {
            const char *s = KUDA_ARRAY_IDX(src, i, const char *);
            KUDA_ARRAY_PUSH(dest, const char *) = kuda_pstrdup(p, s); 
        }
    }
    return dest;
}

struct kuda_array_header_t *clmd_array_str_compact(kuda_pool_t *p, struct kuda_array_header_t *src,
                                                int case_sensitive)
{
    kuda_array_header_t *dest = kuda_array_make(p, src->nelts, sizeof(const char*));
    if (dest) {
        const char *s;
        int i;
        for (i = 0; i < src->nelts; ++i) {
            s = KUDA_ARRAY_IDX(src, i, const char *);
            if (clmd_array_str_index(dest, s, 0, case_sensitive) < 0) {
                KUDA_ARRAY_PUSH(dest, char *) = clmd_util_str_tolower(kuda_pstrdup(p, s));
            }
        }
    }
    return dest;
}

kuda_array_header_t *clmd_array_str_remove(kuda_pool_t *p, kuda_array_header_t *src, 
                                        const char *exclude, int case_sensitive)
{
    kuda_array_header_t *dest = kuda_array_make(p, src->nelts, sizeof(const char*));
    if (dest) {
        int i;
        for (i = 0; i < src->nelts; i++) {
            const char *s = KUDA_ARRAY_IDX(src, i, const char *);
            if (!exclude 
                || (case_sensitive && strcmp(exclude, s))
                || (!case_sensitive && kuda_strnatcasecmp(exclude, s))) {
                KUDA_ARRAY_PUSH(dest, const char *) = kuda_pstrdup(p, s); 
            }
        }
    }
    return dest;
}

int clmd_array_str_add_missing(kuda_array_header_t *dest, kuda_array_header_t *src, int case_sensitive)
{
    int i, added = 0;
    for (i = 0; i < src->nelts; i++) {
        const char *s = KUDA_ARRAY_IDX(src, i, const char *);
        if (clmd_array_str_index(dest, s, 0, case_sensitive) < 0) {
            KUDA_ARRAY_PUSH(dest, const char *) = s;
            ++added; 
        }
    }
    return added;
}

/**************************************************************************************************/
/* file system related */

kuda_status_t clmd_util_fopen(FILE **pf, const char *fn, const char *mode)
{
    *pf = fopen(fn, mode);
    if (*pf == NULL) {
        return errno;
    }

    return KUDA_SUCCESS;
}

kuda_status_t clmd_util_fcreatex(kuda_file_t **pf, const char *fn, 
                              kuda_fileperms_t perms, kuda_pool_t *p)
{
    kuda_status_t rv;
    rv = kuda_file_open(pf, fn, (KUDA_FOPEN_WRITE|KUDA_FOPEN_CREATE|KUDA_FOPEN_EXCL),
                       perms, p);
    if (KUDA_SUCCESS == rv) {
        /* 
         * Some people set umask 007 to deny all world read/writability to files
         * created by cLHy. While this is a noble effort, we need the store files
         * to have the permissions as specified. */
        rv = kuda_file_perms_set(fn, perms);
        if (KUDA_STATUS_IS_ENOTIMPL(rv)) {
            rv = KUDA_SUCCESS;
        }
    }
    return rv;
}

kuda_status_t clmd_util_is_dir(const char *path, kuda_pool_t *pool)
{
    kuda_finfo_t info;
    kuda_status_t rv = kuda_stat(&info, path, KUDA_FINFO_TYPE, pool);
    if (rv == KUDA_SUCCESS) {
        rv = (info.filetype == KUDA_DIR)? KUDA_SUCCESS : KUDA_EINVAL;
    }
    return rv;
}

kuda_status_t clmd_util_is_file(const char *path, kuda_pool_t *pool)
{
    kuda_finfo_t info;
    kuda_status_t rv = kuda_stat(&info, path, KUDA_FINFO_TYPE, pool);
    if (rv == KUDA_SUCCESS) {
        rv = (info.filetype == KUDA_REG)? KUDA_SUCCESS : KUDA_EINVAL;
    }
    return rv;
}

int clmd_file_exists(const char *fname, kuda_pool_t *p)
{
    return (fname && *fname && KUDA_SUCCESS == clmd_util_is_file(fname, p));
}

kuda_status_t clmd_util_path_merge(const char **ppath, kuda_pool_t *p, ...)
{
    const char *segment, *path;
    va_list ap;
    kuda_status_t rv = KUDA_SUCCESS;
    
    va_start(ap, p);
    path = va_arg(ap, char *);
    while (path && KUDA_SUCCESS == rv && (segment = va_arg(ap, char *))) {
        rv = kuda_filepath_merge((char **)&path, path, segment, KUDA_FILEPATH_SECUREROOT , p);
    }
    va_end(ap);
    
    *ppath = (KUDA_SUCCESS == rv)? (path? path : "") : NULL;
    return rv;
}

kuda_status_t clmd_util_freplace(const char *fpath, kuda_fileperms_t perms, kuda_pool_t *p, 
                              clmd_util_file_cb *write_cb, void *baton)
{
    kuda_status_t rv = KUDA_EEXIST;
    kuda_file_t *f;
    const char *tmp;
    int i, max;
    
    tmp = kuda_psprintf(p, "%s.tmp", fpath);
    i = 0; max = 20;
creat:
    while (i < max && KUDA_EEXIST == (rv = clmd_util_fcreatex(&f, tmp, perms, p))) {
        ++i;
        kuda_sleep(kuda_time_msec(50));
    } 
    if (KUDA_EEXIST == rv 
        && KUDA_SUCCESS == (rv = kuda_file_remove(tmp, p))
        && max <= 20) {
        max *= 2;
        goto creat;
    }
    
    if (KUDA_SUCCESS == rv) {
        rv = write_cb(baton, f, p);
        kuda_file_close(f);
        
        if (KUDA_SUCCESS == rv) {
            rv = kuda_file_rename(tmp, fpath, p);
            if (KUDA_SUCCESS != rv) {
                kuda_file_remove(tmp, p);
            }
        }
    }
    return rv;
}                            

/**************************************************************************************************/
/* text files */

kuda_status_t clmd_text_fread8k(const char **ptext, kuda_pool_t *p, const char *fpath)
{
    kuda_status_t rv;
    kuda_file_t *f;
    char buffer[8 * 1024];

    *ptext = NULL;
    if (KUDA_SUCCESS == (rv = kuda_file_open(&f, fpath, KUDA_FOPEN_READ, 0, p))) {
        kuda_size_t blen = sizeof(buffer)/sizeof(buffer[0]) - 1;
        rv = kuda_file_read_full(f, buffer, blen, &blen);
        if (KUDA_SUCCESS == rv || KUDA_STATUS_IS_EOF(rv)) {
            *ptext = kuda_pstrndup(p, buffer, blen);
            rv = KUDA_SUCCESS;
        }
        kuda_file_close(f);
    }
    return rv;
}

static kuda_status_t write_text(void *baton, struct kuda_file_t *f, kuda_pool_t *p)
{
    const char *text = baton;
    kuda_size_t len = strlen(text);
    
    (void)p;
    return kuda_file_write_full(f, text, len, &len);
}

kuda_status_t clmd_text_fcreatex(const char *fpath, kuda_fileperms_t perms, 
                              kuda_pool_t *p, const char *text)
{
    kuda_status_t rv;
    kuda_file_t *f;
    
    rv = clmd_util_fcreatex(&f, fpath, perms, p);
    if (KUDA_SUCCESS == rv) {
        rv = write_text((void*)text, f, p);
        kuda_file_close(f);
        /* When a umask is set, files need to be assigned permissions explicitly.
         * Otherwise, as in the issues reported, it will break our access model. */
        rv = kuda_file_perms_set(fpath, perms);
        if (KUDA_STATUS_IS_ENOTIMPL(rv)) {
            rv = KUDA_SUCCESS;
        }
    }
    return rv;
}

kuda_status_t clmd_text_freplace(const char *fpath, kuda_fileperms_t perms, 
                              kuda_pool_t *p, const char *text)
{
    return clmd_util_freplace(fpath, perms, p, write_text, (void*)text);
}

typedef struct {
    const char *path;
    kuda_array_header_t *patterns;
    int follow_links;
    void *baton;
    clmd_util_fdo_cb *cb;
} clmd_util_fwalk_t;

static kuda_status_t rm_recursive(const char *fpath, kuda_pool_t *p, int max_level)
{
    kuda_finfo_t info;
    kuda_status_t rv;
    const char *npath;
    
    if (KUDA_SUCCESS != (rv = kuda_stat(&info, fpath, (KUDA_FINFO_TYPE|KUDA_FINFO_LINK), p))) {
        return rv;
    }
    
    if (info.filetype == KUDA_DIR) {
        if (max_level > 0) {
            kuda_dir_t *d;
            
            if (KUDA_SUCCESS == (rv = kuda_dir_open(&d, fpath, p))) {
            
                while (KUDA_SUCCESS == rv && 
                       KUDA_SUCCESS == (rv = kuda_dir_read(&info, KUDA_FINFO_TYPE, d))) {
                    if (!strcmp(".", info.name) || !strcmp("..", info.name)) {
                        continue;
                    }
                    
                    rv = clmd_util_path_merge(&npath, p, fpath, info.name, NULL);
                    if (KUDA_SUCCESS == rv) {
                        rv = rm_recursive(npath, p, max_level - 1);
                    }
                }
                kuda_dir_close(d);
                if (KUDA_STATUS_IS_ENOENT(rv)) {
                    rv = KUDA_SUCCESS;
                }
            }
        }
        if (KUDA_SUCCESS == rv) {
            rv = kuda_dir_remove(fpath, p);
        }
    }
    else {
        rv = kuda_file_remove(fpath, p);
    }
    return rv;
}

static kuda_status_t prm_recursive(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    int max_level = va_arg(ap, int);
    
    (void)p;
    return rm_recursive(baton, ptemp, max_level); 
}

kuda_status_t clmd_util_rm_recursive(const char *fpath, kuda_pool_t *p, int max_level)
{
    return clmd_util_pool_vdo(prm_recursive, (void*)fpath, p, max_level, NULL);
}

static kuda_status_t match_and_do(clmd_util_fwalk_t *ctx, const char *path, int depth, 
                                 kuda_pool_t *p, kuda_pool_t *ptemp)
{
    kuda_status_t rv = KUDA_SUCCESS;
    const char *pattern, *npath;
    kuda_dir_t *d;
    kuda_finfo_t finfo;
    int ndepth = depth + 1;
    kuda_int32_t wanted = (KUDA_FINFO_TYPE);

    if (depth >= ctx->patterns->nelts) {
        return KUDA_SUCCESS;
    }
    pattern = KUDA_ARRAY_IDX(ctx->patterns, depth, const char *);
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE4, 0, ptemp, "match_and_do "
                  "path=%s depth=%d pattern=%s", path, depth, pattern);
    rv = kuda_dir_open(&d, path, ptemp);
    if (KUDA_SUCCESS != rv) {
        return rv;
    }
    
    while (KUDA_SUCCESS == (rv = kuda_dir_read(&finfo, wanted, d))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE4, 0, ptemp, "match_and_do "
                      "candidate=%s", finfo.name);
        if (!strcmp(".", finfo.name) || !strcmp("..", finfo.name)) {
            continue;
        } 
        if (KUDA_SUCCESS == kuda_fnmatch(pattern, finfo.name, 0)) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE4, 0, ptemp, "match_and_do "
                          "candidate=%s matches pattern", finfo.name);
            if (ndepth < ctx->patterns->nelts) {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE4, 0, ptemp, "match_and_do "
                              "need to go deepter");
                if (KUDA_DIR == finfo.filetype) { 
                    /* deeper and deeper, irgendwo in der tiefe leuchtet ein licht */
                    rv = clmd_util_path_merge(&npath, ptemp, path, finfo.name, NULL);
                    if (KUDA_SUCCESS == rv) {
                        rv = match_and_do(ctx, npath, ndepth, p, ptemp);
                    }
                }
            }
            else {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE4, 0, ptemp, "match_and_do "
                              "invoking inspector on name=%s", finfo.name);
                rv = ctx->cb(ctx->baton, p, ptemp, path, finfo.name, finfo.filetype);
            }
        }
        if (KUDA_SUCCESS != rv) {
            break;
        }
    }

    if (KUDA_STATUS_IS_ENOENT(rv)) {
        rv = KUDA_SUCCESS;
    }

    kuda_dir_close(d);
    return rv;
}

static kuda_status_t files_do_start(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_util_fwalk_t *ctx = baton;
    const char *segment;

    ctx->patterns = kuda_array_make(ptemp, 5, sizeof(const char*));
    
    segment = va_arg(ap, char *);
    while (segment) {
        KUDA_ARRAY_PUSH(ctx->patterns, const char *) = segment;
        segment = va_arg(ap, char *);
    }
    
    return match_and_do(ctx, ctx->path, 0, p, ptemp);
}

kuda_status_t clmd_util_files_do(clmd_util_fdo_cb *cb, void *baton, kuda_pool_t *p,
                              const char *path, ...)
{
    kuda_status_t rv;
    va_list ap;
    clmd_util_fwalk_t ctx;

    memset(&ctx, 0, sizeof(ctx));
    ctx.path = path;
    ctx.follow_links = 1;
    ctx.cb = cb;
    ctx.baton = baton;
    
    va_start(ap, path);
    rv = pool_vado(files_do_start, &ctx, p, ap);
    va_end(ap);
    
    return rv;
}

static kuda_status_t tree_do(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, const char *path)
{
    clmd_util_fwalk_t *ctx = baton;

    kuda_status_t rv = KUDA_SUCCESS;
    const char *name, *fpath;
    kuda_filetype_e ftype;
    kuda_dir_t *d;
    kuda_int32_t wanted = KUDA_FINFO_TYPE;
    kuda_finfo_t finfo;

    if (KUDA_SUCCESS == (rv = kuda_dir_open(&d, path, ptemp))) {
        while (KUDA_SUCCESS == (rv = kuda_dir_read(&finfo, wanted, d))) {
            name = finfo.name;
            if (!strcmp(".", name) || !strcmp("..", name)) {
                continue;
            }

            fpath = NULL;
            ftype = finfo.filetype;
            
            if (KUDA_LNK == ftype && ctx->follow_links) {
                rv = clmd_util_path_merge(&fpath, ptemp, path, name, NULL);
                if (KUDA_SUCCESS == rv) {
                    rv = kuda_stat(&finfo, ctx->path, wanted, ptemp);
                }
            }
            
            if (KUDA_DIR == finfo.filetype) {
                if (!fpath) {
                    rv = clmd_util_path_merge(&fpath, ptemp, path, name, NULL);
                }
                if (KUDA_SUCCESS == rv) {
                    rv = tree_do(ctx, p, ptemp, fpath);
                    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, rv, ptemp, "dir cb(%s/%s)", 
                                  path, name);
                    rv = ctx->cb(ctx->baton, p, ptemp, path, name, ftype);
                }
            }
            else {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, rv, ptemp, "file cb(%s/%s)", 
                              path, name);
                rv = ctx->cb(ctx->baton, p, ptemp, path, name, finfo.filetype);
            }
        }

        kuda_dir_close(d);
        
        if (KUDA_STATUS_IS_ENOENT(rv)) {
            rv = KUDA_SUCCESS;
        }
    }
    return rv;
}

static kuda_status_t tree_start_do(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp)
{
    clmd_util_fwalk_t *ctx = baton;
    kuda_finfo_t info;
    kuda_status_t rv;
    kuda_int32_t wanted = ctx->follow_links? KUDA_FINFO_TYPE : (KUDA_FINFO_TYPE|KUDA_FINFO_LINK);
    
    rv = kuda_stat(&info, ctx->path, wanted, ptemp);
    if (rv == KUDA_SUCCESS) {
        switch (info.filetype) {
            case KUDA_DIR:
                rv = tree_do(ctx, p, ptemp, ctx->path);
                break;
            default:
                rv = KUDA_EINVAL;
        }
    }
    return rv;
}

kuda_status_t clmd_util_tree_do(clmd_util_fdo_cb *cb, void *baton, kuda_pool_t *p, 
                             const char *path, int follow_links)
{
    kuda_status_t rv;
    clmd_util_fwalk_t ctx;

    memset(&ctx, 0, sizeof(ctx));
    ctx.path = path;
    ctx.follow_links = follow_links;
    ctx.cb = cb;
    ctx.baton = baton;
    
    rv = clmd_util_pool_do(tree_start_do, &ctx, p);
    
    return rv;
}

static kuda_status_t rm_cb(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, 
                          const char *path, const char *name, kuda_filetype_e ftype)
{
    kuda_status_t rv;
    const char *fpath;
    
    (void)baton;
    (void)p;
    rv = clmd_util_path_merge(&fpath, ptemp, path, name, NULL);
    if (KUDA_SUCCESS == rv) {
        if (KUDA_DIR == ftype) {
            rv = kuda_dir_remove(fpath, ptemp);
        }
        else {
            rv = kuda_file_remove(fpath, ptemp);
        }
    }
    return rv;
}

kuda_status_t clmd_util_ftree_remove(const char *path, kuda_pool_t *p)
{
    kuda_status_t rv = clmd_util_tree_do(rm_cb, NULL, p, path, 0);
    if (KUDA_SUCCESS == rv) {
        rv = kuda_dir_remove(path, p);
    }
    return rv;
}

/* DNS name checks ********************************************************************************/

int clmd_dns_is_name(kuda_pool_t *p, const char *hostname, int need_fqdn)
{
    char c, last = 0;
    const char *cp = hostname;
    int dots = 0;
    
    /* Since we use the names in certificates, we need pure ASCII domain names
     * and IDN need to be converted to unicode. */
    while ((c = *cp++)) {
        switch (c) {
            case '.':
                if (last == '.') {
                    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, 0, p, "dns name with ..: %s", 
                                  hostname);
                    return 0;
                }
                ++dots;
                break;
            case '-':
                break;
            default:
                if (!kuda_isalnum(c)) {
                    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, 0, p, "dns invalid char %c: %s", 
                                  c, hostname);
                    return 0;
                }
                break;
        }
        last = c;
    }
    
    if (last == '.') { /* DNS names may end with '.' */
        --dots;
    }
    if (need_fqdn && dots <= 0) { /* do not accept just top level domains */
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, 0, p, "not a FQDN: %s", hostname);
        return 0;
    }
    return 1; /* empty string not allowed */
}

int clmd_dns_is_wildcard(kuda_pool_t *p, const char *domain)
{
    if (domain[0] != '*' || domain[1] != '.') return 0;
    return clmd_dns_is_name(p, domain+2, 1);
}

int clmd_dns_matches(const char *pattern, const char *domain)
{
    const char *s;
    
    if (!kuda_strnatcasecmp(pattern, domain)) return 1;
    if (pattern[0] == '*' && pattern[1] == '.') {
        s = strchr(domain, '.');
        if (s && !kuda_strnatcasecmp(pattern+1, s)) return 1;
    }
    return 0;
}

kuda_array_header_t *clmd_dns_make_minimal(kuda_pool_t *p, kuda_array_header_t *domains)
{
    kuda_array_header_t *minimal;
    const char *domain, *pattern;
    int i, j, duplicate;
    
    minimal = kuda_array_make(p, domains->nelts, sizeof(const char *));
    for (i = 0; i < domains->nelts; ++i) {
        domain = KUDA_ARRAY_IDX(domains, i, const char*);
        duplicate = 0;
        /* is it matched in minimal already? */
        for (j = 0; j < minimal->nelts; ++j) {
            pattern = KUDA_ARRAY_IDX(minimal, j, const char*);
            if (clmd_dns_matches(pattern, domain)) {
                duplicate = 1;
                break;
            }
        }
        if (!duplicate) {
            if (!clmd_dns_is_wildcard(p, domain)) {
                /* plain name, will we see a wildcard that replaces it? */
                for (j = i+1; j < domains->nelts; ++j) {
                    pattern = KUDA_ARRAY_IDX(domains, j, const char*);
                    if (clmd_dns_is_wildcard(p, pattern) && clmd_dns_matches(pattern, domain)) {
                        duplicate = 1;
                        break;
                    }
                }
            }
            if (!duplicate) {
                KUDA_ARRAY_PUSH(minimal, const char *) = domain; 
            }
        }
    }
    return minimal;
}

int clmd_dns_domains_match(const kuda_array_header_t *domains, const char *name)
{
    const char *domain;
    int i;
    
    for (i = 0; i < domains->nelts; ++i) {
        domain = KUDA_ARRAY_IDX(domains, i, const char*);
        if (clmd_dns_matches(domain, name)) return 1;
    }
    return 0;
}

const char *clmd_util_schemify(kuda_pool_t *p, const char *s, const char *def_scheme)
{
    const char *cp = s;
    while (*cp) {
        if (*cp == ':') {
            /* could be an url scheme, leave unchanged */
            return s;
        }
        else if (!kuda_isalnum(*cp)) {
            break;
        }
        ++cp;
    }
    return kuda_psprintf(p, "%s:%s", def_scheme, s);
}

static kuda_status_t uri_check(kuda_uri_t *uri_parsed, kuda_pool_t *p, 
                              const char *uri, const char **perr)
{
    const char *s, *err = NULL;
    kuda_status_t rv;
    
    if (KUDA_SUCCESS != (rv = kuda_uri_parse(p, uri, uri_parsed))) {
        err = "not an uri";
    }
    else if (uri_parsed->scheme) {
        if (strlen(uri_parsed->scheme) + 1 >= strlen(uri)) {
            err = "missing uri identifier";
        }
        else if (!strncmp("http", uri_parsed->scheme, 4)) {
            if (!uri_parsed->hostname) {
                err = "missing hostname";
            }
            else if (!clmd_dns_is_name(p, uri_parsed->hostname, 0)) {
                err = "invalid hostname";
            }
            if (uri_parsed->port_str 
                && (!kuda_isdigit(uri_parsed->port_str[0])
                || uri_parsed->port == 0
                || uri_parsed->port > 65353)) {
                err = "invalid port";
            }
        }
        else if (!strcmp("mailto", uri_parsed->scheme)) {
            s = strchr(uri, '@');
            if (!s) {
                err = "missing @";
            }
            else if (strchr(s+1, '@')) {
                err = "duplicate @";
            }
            else if (s == uri + strlen(uri_parsed->scheme) + 1) {
                err = "missing local part";
            }
            else if (s == (uri + strlen(uri)-1)) {
                err = "missing hostname";
            }
            else if (strstr(uri, "..")) {
                err = "double period";
            }
        }
    }
    if (strchr(uri, ' ') || strchr(uri, '\t') ) {
        err = "whitespace in uri";
    }
    
    if (err) {
        rv = KUDA_EINVAL;
    }
    *perr = err;
    return rv;
}

kuda_status_t clmd_util_abs_uri_check(kuda_pool_t *p, const char *uri, const char **perr)
{
    kuda_uri_t uri_parsed;
    kuda_status_t rv;

    if (KUDA_SUCCESS == (rv = uri_check(&uri_parsed, p, uri, perr))) {
        if (!uri_parsed.scheme) {
            *perr = "missing uri scheme";
            return KUDA_EINVAL;
        }
    }
    return rv;
}

kuda_status_t clmd_util_abs_http_uri_check(kuda_pool_t *p, const char *uri, const char **perr)
{
    kuda_uri_t uri_parsed;
    kuda_status_t rv;

    if (KUDA_SUCCESS == (rv = uri_check(&uri_parsed, p, uri, perr))) {
        if (!uri_parsed.scheme) {
            *perr = "missing uri scheme";
            return KUDA_EINVAL;
        }
        if (kuda_strnatcasecmp("http", uri_parsed.scheme) 
            && kuda_strnatcasecmp("https", uri_parsed.scheme)) {
            *perr = "uri scheme must be http or https";
            return KUDA_EINVAL;
        }
    }
    return rv;
}

/* try and retry for a while **********************************************************************/

kuda_status_t clmd_util_try(clmd_util_try_fn *fn, void *baton, int ignore_errs, 
                         kuda_interval_time_t timeout, kuda_interval_time_t start_delay, 
                         kuda_interval_time_t max_delay, int backoff)
{
    kuda_status_t rv;
    kuda_time_t now = kuda_time_now();
    kuda_time_t giveup = now + timeout;
    kuda_interval_time_t nap_duration = start_delay? start_delay : kuda_time_from_msec(100);
    kuda_interval_time_t nap_max = max_delay? max_delay : kuda_time_from_sec(10);
    kuda_interval_time_t left;
    int i = 0;
    
    while (1) {
        if (KUDA_SUCCESS == (rv = fn(baton, i++))) {
            break;
        }
        else if (!KUDA_STATUS_IS_EAGAIN(rv) && !ignore_errs) {
            break;
        }
        
        now = kuda_time_now();
        if (now > giveup) {
            rv = KUDA_TIMEUP;
            break;
        }
        
        left = giveup - now;
        if (nap_duration > left) {
            nap_duration = left;
        }
        if (nap_duration > nap_max) {
            nap_duration = nap_max;
        }
        
        kuda_sleep(nap_duration);
        if (backoff) {
            nap_duration *= 2;
        } 
    }
    return rv;
}

/* execute process ********************************************************************************/

kuda_status_t clmd_util_exec(kuda_pool_t *p, const char *cmd, const char * const *argv,
                          int *exit_code)
{
    kuda_status_t rv;
    kuda_procattr_t *procattr;
    kuda_proc_t *proc;
    kuda_exit_why_e ewhy;
    char buffer[1024];
    
    *exit_code = 0;
    if (!(proc = kuda_pcalloc(p, sizeof(*proc)))) {
        return KUDA_ENOMEM;
    }
    if (   KUDA_SUCCESS == (rv = kuda_procattr_create(&procattr, p))
        && KUDA_SUCCESS == (rv = kuda_procattr_io_set(procattr, KUDA_NO_FILE, 
                                                    KUDA_NO_PIPE, KUDA_FULL_BLOCK))
        && KUDA_SUCCESS == (rv = kuda_procattr_cmdtype_set(procattr, KUDA_PROGRAM))
        && KUDA_SUCCESS == (rv = kuda_proc_create(proc, cmd, argv, NULL, procattr, p))) {
        
        /* read stderr and log on INFO for possible fault analysis. */
        while(KUDA_SUCCESS == (rv = kuda_file_gets(buffer, sizeof(buffer)-1, proc->err))) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, 0, p, "cmd(%s) stderr: %s", cmd, buffer);
        }
        if (!KUDA_STATUS_IS_EOF(rv)) goto out;
        kuda_file_close(proc->err);
        
        if (KUDA_CHILD_DONE == (rv = kuda_proc_wait(proc, exit_code, &ewhy, KUDA_WAIT))) {
            /* let's not dwell on exit stati, but core should signal something's bad */
            if (*exit_code > 127 || KUDA_PROC_SIGNAL_CORE == ewhy) {
                return KUDA_EINCOMPLETE;
            }
            return KUDA_SUCCESS;
        }
    }
out:
    return rv;
}

/* base64 url encoding ****************************************************************************/

#define N6 (unsigned int)-1

static const unsigned int BASE64URL_UINT6[] = {
/*   0   1   2   3   4   5   6   7   8   9   a   b   c   d   e   f        */
    N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, /*  0 */
    N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, /*  1 */ 
    N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, 62, N6, N6, /*  2 */
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, N6, N6, N6, N6, N6, N6, /*  3 */ 
    N6, 0,  1,  2,  3,  4,  5,  6,   7,  8,  9, 10, 11, 12, 13, 14, /*  4 */
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, N6, N6, N6, N6, 63, /*  5 */
    N6, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, /*  6 */
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, N6, N6, N6, N6, N6, /*  7 */
    N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, /*  8 */
    N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, /*  9 */
    N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, /*  a */
    N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, /*  b */
    N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, /*  c */
    N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, /*  d */
    N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, /*  e */
    N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6, N6  /*  f */
};
static const unsigned char BASE64URL_CHARS[] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', /*  0 -  9 */
    'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', /* 10 - 19 */
    'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', /* 20 - 29 */
    'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', /* 30 - 39 */
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', /* 40 - 49 */
    'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', /* 50 - 59 */
    '8', '9', '-', '_', ' ', ' ', ' ', ' ', ' ', ' ', /* 60 - 69 */
};

#define BASE64URL_CHAR(x)    BASE64URL_CHARS[ (unsigned int)(x) & 0x3fu ]
   
kuda_size_t clmd_util_base64url_decode(const char **decoded, const char *encoded, 
                                    kuda_pool_t *pool)
{
    const unsigned char *e = (const unsigned char *)encoded;
    const unsigned char *p = e;
    unsigned char *d;
    unsigned int n;
    long len, mlen, remain, i;
    
    while (*p && BASE64URL_UINT6[ *p ] != N6) {
        ++p;
    }
    len = (int)(p - e);
    mlen = (len/4)*4;
    *decoded = kuda_pcalloc(pool, (kuda_size_t)len + 1);
    
    i = 0;
    d = (unsigned char*)*decoded;
    for (; i < mlen; i += 4) {
        n = ((BASE64URL_UINT6[ e[i+0] ] << 18) +
             (BASE64URL_UINT6[ e[i+1] ] << 12) +
             (BASE64URL_UINT6[ e[i+2] ] << 6) +
             (BASE64URL_UINT6[ e[i+3] ]));
        *d++ = (unsigned char)(n >> 16);
        *d++ = (unsigned char)(n >> 8 & 0xffu);
        *d++ = (unsigned char)(n & 0xffu);
    }
    remain = len - mlen;
    switch (remain) {
        case 2:
            n = ((BASE64URL_UINT6[ e[mlen+0] ] << 18) +
                 (BASE64URL_UINT6[ e[mlen+1] ] << 12));
            *d++ = (unsigned char)(n >> 16);
            remain = 1;
            break;
        case 3:
            n = ((BASE64URL_UINT6[ e[mlen+0] ] << 18) +
                 (BASE64URL_UINT6[ e[mlen+1] ] << 12) +
                 (BASE64URL_UINT6[ e[mlen+2] ] << 6));
            *d++ = (unsigned char)(n >> 16);
            *d++ = (unsigned char)(n >> 8 & 0xffu);
            remain = 2;
            break;
        default: /* do nothing */
            break;
    }
    return (kuda_size_t)(mlen/4*3 + remain);
}

const char *clmd_util_base64url_encode(const char *data, kuda_size_t dlen, kuda_pool_t *pool)
{
    int i, len = (int)dlen;
    kuda_size_t slen = ((dlen+2)/3)*4 + 1; /* 0 terminated */
    const unsigned char *udata = (const unsigned char*)data;
    unsigned char *enc, *p = kuda_pcalloc(pool, slen);
    
    enc = p;
    for (i = 0; i < len-2; i+= 3) {
        *p++ = BASE64URL_CHAR( (udata[i]   >> 2) );
        *p++ = BASE64URL_CHAR( (udata[i]   << 4) + (udata[i+1] >> 4) );
        *p++ = BASE64URL_CHAR( (udata[i+1] << 2) + (udata[i+2] >> 6) );
        *p++ = BASE64URL_CHAR( (udata[i+2]) );
    }
    
    if (i < len) {
        *p++ = BASE64URL_CHAR( (udata[i] >> 2) );
        if (i == (len - 1)) {
            *p++ = BASE64URL_CHARS[ ((unsigned int)udata[i] << 4) & 0x3fu ];
        }
        else {
            *p++ = BASE64URL_CHAR( (udata[i] << 4) + (udata[i+1] >> 4) );
            *p++ = BASE64URL_CHAR( (udata[i+1] << 2) );
        }
    }
    *p++ = '\0';
    return (char *)enc;
}

/*******************************************************************************
 * link header handling 
 ******************************************************************************/

typedef struct {
    const char *s;
    kuda_size_t slen;
    kuda_size_t i;
    kuda_size_t link_start;
    kuda_size_t link_len;
    kuda_size_t pn_start;
    kuda_size_t pn_len;
    kuda_size_t pv_start;
    kuda_size_t pv_len;
} link_ctx;

static int attr_char(char c) 
{
    switch (c) {
        case '!':
        case '#':
        case '$':
        case '&':
        case '+':
        case '-':
        case '.':
        case '^':
        case '_':
        case '`':
        case '|':
        case '~':
            return 1;
        default:
            return kuda_isalnum(c);
    }
}

static int ptoken_char(char c) 
{
    switch (c) {
        case '!':
        case '#':
        case '$':
        case '&':
        case '\'':
        case '(':
        case ')':
        case '*':
        case '+':
        case '-':
        case '.':
        case '/':
        case ':':
        case '<':
        case '=':
        case '>':
        case '?':
        case '@':
        case '[':
        case ']':
        case '^':
        case '_':
        case '`':
        case '{':
        case '|':
        case '}':
        case '~':
            return 1;
        default:
            return kuda_isalnum(c);
    }
}

static int skip_ws(link_ctx *ctx)
{
    char c;
    while (ctx->i < ctx->slen 
           && (((c = ctx->s[ctx->i]) == ' ') || (c == '\t'))) {
        ++ctx->i;
    }
    return (ctx->i < ctx->slen);
}

static int skip_nonws(link_ctx *ctx)
{
    char c;
    while (ctx->i < ctx->slen 
           && (((c = ctx->s[ctx->i]) != ' ') && (c != '\t'))) {
        ++ctx->i;
    }
    return (ctx->i < ctx->slen);
}

static unsigned int find_chr(link_ctx *ctx, char c, kuda_size_t *pidx)
{
    kuda_size_t j;
    for (j = ctx->i; j < ctx->slen; ++j) {
        if (ctx->s[j] == c) {
            *pidx = j;
            return 1;
        }
    } 
    return 0;
}

static int read_chr(link_ctx *ctx, char c)
{
    if (ctx->i < ctx->slen && ctx->s[ctx->i] == c) {
        ++ctx->i;
        return 1;
    }
    return 0;
}

static int skip_qstring(link_ctx *ctx)
{
    if (skip_ws(ctx) && read_chr(ctx, '\"')) {
        kuda_size_t end;
        if (find_chr(ctx, '\"', &end)) {
            ctx->i = end + 1;
            return 1;
        }
    }
    return 0;
}

static int skip_ptoken(link_ctx *ctx)
{
    if (skip_ws(ctx)) {
        kuda_size_t i;
        for (i = ctx->i; i < ctx->slen && ptoken_char(ctx->s[i]); ++i) {
            /* nop */
        }
        if (i > ctx->i) {
            ctx->i = i;
            return 1;
        }
    }
    return 0;
}


static int read_link(link_ctx *ctx)
{
    ctx->link_start = ctx->link_len = 0;
    if (skip_ws(ctx) && read_chr(ctx, '<')) {
        kuda_size_t end;
        if (find_chr(ctx, '>', &end)) {
            ctx->link_start = ctx->i;
            ctx->link_len = end - ctx->link_start;
            ctx->i = end + 1;
            return 1;
        }
    }
    return 0;
}

static int skip_pname(link_ctx *ctx)
{
    if (skip_ws(ctx)) {
        kuda_size_t i;
        for (i = ctx->i; i < ctx->slen && attr_char(ctx->s[i]); ++i) {
            /* nop */
        }
        if (i > ctx->i) {
            ctx->i = i;
            return 1;
        }
    }
    return 0;
}

static int skip_pvalue(link_ctx *ctx)
{
    if (skip_ws(ctx) && read_chr(ctx, '=')) {
        ctx->pv_start = ctx->i;
        if (skip_qstring(ctx) || skip_ptoken(ctx)) {
            ctx->pv_len = ctx->i - ctx->pv_start;
            return 1;
        }
    }
    return 0;
}

static int skip_param(link_ctx *ctx)
{
    if (skip_ws(ctx) && read_chr(ctx, ';')) {
        ctx->pn_start = ctx->i;
        ctx->pn_len = 0;
        if (skip_pname(ctx)) {
            ctx->pn_len = ctx->i - ctx->pn_start;
            ctx->pv_len = 0;
            skip_pvalue(ctx); /* value is optional */
            return 1;
        }
    }
    return 0;
}

static int pv_contains(link_ctx *ctx, const char *s)
{
    kuda_size_t pvstart = ctx->pv_start;
    kuda_size_t pvlen = ctx->pv_len;
    
    if (ctx->s[pvstart] == '\"' && pvlen > 1) {
        ++pvstart;
        pvlen -= 2;
    }
    if (pvlen > 0) {
        kuda_size_t slen = strlen(s);
        link_ctx pvctx;
        kuda_size_t i;
        
        memset(&pvctx, 0, sizeof(pvctx));
        pvctx.s = ctx->s + pvstart;
        pvctx.slen = pvlen;

        for (i = 0; i < pvctx.slen; i = pvctx.i) {
            skip_nonws(&pvctx);
            if ((pvctx.i - i) == slen && !strncmp(s, pvctx.s + i, slen)) {
                return 1;
            }
            skip_ws(&pvctx);
        }
    }
    return 0;
}

/* RFC 5988 <https://tools.ietf.org/html/rfc5988#section-6.2.1>
  Link           = "Link" ":" #link-value
  link-value     = "<" URI-Reference ">" *( ";" link-param )
  link-param     = ( ( "rel" "=" relation-types )
                 | ( "anchor" "=" <"> URI-Reference <"> )
                 | ( "rev" "=" relation-types )
                 | ( "hreflang" "=" Language-Tag )
                 | ( "media" "=" ( MediaDesc | ( <"> MediaDesc <"> ) ) )
                 | ( "title" "=" quoted-string )
                 | ( "title*" "=" ext-value )
                 | ( "type" "=" ( media-type | quoted-mt ) )
                 | ( link-extension ) )
  link-extension = ( parmname [ "=" ( ptoken | quoted-string ) ] )
                 | ( ext-name-star "=" ext-value )
  ext-name-star  = parmname "*" ; reserved for RFC2231-profiled
                                ; extensions.  Whitespace NOT
                                ; allowed in between.
  ptoken         = 1*ptokenchar
  ptokenchar     = "!" | "#" | "$" | "%" | "&" | "'" | "("
                 | ")" | "*" | "+" | "-" | "." | "/" | DIGIT
                 | ":" | "<" | "=" | ">" | "?" | "@" | ALPHA
                 | "[" | "]" | "^" | "_" | "`" | "{" | "|"
                 | "}" | "~"
  media-type     = type-name "/" subtype-name
  quoted-mt      = <"> media-type <">
  relation-types = relation-type
                 | <"> relation-type *( 1*SP relation-type ) <">
  relation-type  = reg-rel-type | ext-rel-type
  reg-rel-type   = LOALPHA *( LOALPHA | DIGIT | "." | "-" )
  ext-rel-type   = URI
  
  and from <https://tools.ietf.org/html/rfc5987>
  parmname      = 1*attr-char
  attr-char     = ALPHA / DIGIT
                   / "!" / "#" / "$" / "&" / "+" / "-" / "."
                   / "^" / "_" / "`" / "|" / "~"
 */

typedef struct {
    kuda_pool_t *pool;
    const char *relation;
    const char *url;
} find_ctx;

static int find_url(void *baton, const char *key, const char *value)
{
    find_ctx *outer = baton;
    
    if (!kuda_strnatcasecmp("link", key)) {
        link_ctx ctx;
        
        memset(&ctx, 0, sizeof(ctx));
        ctx.s = value;
        ctx.slen = strlen(value);
        
        while (read_link(&ctx)) {
            while (skip_param(&ctx)) {
                if (ctx.pn_len == 3 && !strncmp("rel", ctx.s + ctx.pn_start, 3)
                    && pv_contains(&ctx, outer->relation)) {
                    /* this is the link relation we are looking for */
                    outer->url = kuda_pstrndup(outer->pool, ctx.s + ctx.link_start, ctx.link_len);
                    return 0;
                }
            }
        }
    }
    return 1;
}

const char *clmd_link_find_relation(const kuda_table_t *headers, 
                                  kuda_pool_t *pool, const char *relation)
{
    find_ctx ctx;
    
    memset(&ctx, 0, sizeof(ctx));
    ctx.pool = pool;
    ctx.relation = relation;
    
    kuda_table_do(find_url, &ctx, headers, NULL);
    
    return ctx.url;
}

