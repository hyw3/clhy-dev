/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <kuda_optional.h>
#include <kuda_time.h>
#include <kuda_date.h>
#include <kuda_strings.h>

#include <wwhy.h>
#include <http_core.h>
#include <http_protocol.h>
#include <http_request.h>
#include <http_log.h>

#include "capi_status.h"

#include "clmd.h"
#include "clmd_curl.h"
#include "clmd_crypt.h"
#include "clmd_http.h"
#include "clmd_json.h"
#include "clmd_status.h"
#include "clmd_store.h"
#include "clmd_store_fs.h"
#include "clmd_log.h"
#include "clmd_reg.h"
#include "clmd_util.h"
#include "clmd_version.h"
#include "clmd_acme.h"
#include "clmd_acme_authz.h"

#include "capi_clmd.h"
#include "capi_clmd_private.h"
#include "capi_clmd_config.h"
#include "capi_clmd_drive.h"
#include "capi_clmd_status.h"

/**************************************************************************************************/
/* Certificate status */

#define CLHYKUDEL_PREFIX               "/.wwhy/"
#define CLMD_STATUS_RESOURCE          CLHYKUDEL_PREFIX"certificate-status"

int clmd_http_cert_status(request_rec *r)
{
    clmd_json_t *resp, *j, *mdj, *certj;
    const clmd_srv_conf_t *sc;
    const clmd_t *clmd;
    kuda_bucket_brigade *bb;
    kuda_status_t rv;
    
    if (!r->parsed_uri.path || strcmp(CLMD_STATUS_RESOURCE, r->parsed_uri.path))
        return DECLINED;
        
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                  "requesting status for: %s", r->hostname);
    
    /* We are looking for information about a staged certificate */
    sc = clhy_get_capi_config(r->server->capi_config, &clmd_capi);
    if (!sc || !sc->mc || !sc->mc->reg || !sc->mc->certificate_status_enabled) return DECLINED;
    clmd = clmd_get_by_domain(sc->mc->mds, r->hostname);
    if (!clmd) return DECLINED;

    if (r->method_number != M_GET) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                      "clmd(%s): status supports only GET", clmd->name);
        return HTTP_NOT_IMPLEMENTED;
    }
    
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                  "requesting status for CLMD: %s", clmd->name);

    if (KUDA_SUCCESS != (rv = clmd_status_get_clmd_json(&mdj, clmd, sc->mc->reg, r->pool))) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(10175)
                      "loading clmd status for %s", clmd->name);
        return HTTP_INTERNAL_SERVER_ERROR;
    }
    
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                  "status for CLMD: %s is %s", clmd->name, clmd_json_writep(mdj, r->pool, CLMD_JSON_FMT_INDENT));

    resp = clmd_json_create(r->pool);
    
    if (clmd_json_has_key(mdj, CLMD_KEY_CERT, CLMD_KEY_VALID_UNTIL, NULL)) {
        clmd_json_sets(clmd_json_gets(mdj, CLMD_KEY_CERT, CLMD_KEY_VALID_UNTIL, NULL), 
                     resp, CLMD_KEY_VALID_UNTIL, NULL);
    }
    if (clmd_json_has_key(mdj, CLMD_KEY_CERT, CLMD_KEY_VALID_FROM, NULL)) {
        clmd_json_sets(clmd_json_gets(mdj, CLMD_KEY_CERT, CLMD_KEY_VALID_FROM, NULL), 
                     resp, CLMD_KEY_VALID_FROM, NULL);
    }
    if (clmd_json_has_key(mdj, CLMD_KEY_CERT, CLMD_KEY_SERIAL, NULL)) {
        clmd_json_sets(clmd_json_gets(mdj, CLMD_KEY_CERT, CLMD_KEY_SERIAL, NULL), 
                     resp, CLMD_KEY_SERIAL, NULL);
    }
    if (clmd_json_has_key(mdj, CLMD_KEY_CERT, CLMD_KEY_SHA256_FINGERPRINT, NULL)) {
        clmd_json_sets(clmd_json_gets(mdj, CLMD_KEY_CERT, CLMD_KEY_SHA256_FINGERPRINT, NULL), 
                     resp, CLMD_KEY_SHA256_FINGERPRINT, NULL);
    }
    
    if (clmd_json_has_key(mdj, CLMD_KEY_RENEWAL, NULL)) {
        /* copy over the information we want to make public about this:
         *  - when not finished, add an empty object to indicate something is going on
         *  - when a certificate is staged, add the information from that */
        certj = clmd_json_getj(mdj, CLMD_KEY_RENEWAL, CLMD_KEY_CERT, NULL);
        j = certj? certj : clmd_json_create(r->pool);; 
        clmd_json_setj(j, resp, CLMD_KEY_RENEWAL, NULL);
    }
    
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r, "clmd[%s]: sending status", clmd->name);
    kuda_table_set(r->headers_out, "Content-Type", "application/json"); 
    bb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
    clmd_json_writeb(resp, CLMD_JSON_FMT_INDENT, bb);
    clhy_pass_brigade(r->output_filters, bb);
    kuda_brigade_cleanup(bb);
    
    return DONE;
}

/**************************************************************************************************/
/* Status hook */

typedef struct {
    kuda_pool_t *p;
    const clmd_capi_conf_t *mc;
    kuda_bucket_brigade *bb;
    const char *separator;
} status_ctx;

typedef struct status_info status_info; 

static void add_json_val(status_ctx *ctx, clmd_json_t *j);

typedef void add_status_fn(status_ctx *ctx, clmd_json_t *mdj, const status_info *info);

struct status_info {
    const char *label;
    const char *key;
    add_status_fn *fn;
};

static void si_val_status(status_ctx *ctx, clmd_json_t *mdj, const status_info *info)
{
    const char *s = "unknown";
    (void)info;
    switch (clmd_json_getl(mdj, CLMD_KEY_STATE, NULL)) {
        case CLMD_S_INCOMPLETE: s = "incomplete"; break;
        case CLMD_S_EXPIRED_DEPRECATED:
        case CLMD_S_COMPLETE: s = "ok"; break;
        case CLMD_S_ERROR: s = "error"; break;
        case CLMD_S_MISSING_INFORMATION: s = "missing information"; break;
        default: break;
    }
    kuda_brigade_puts(ctx->bb, NULL, NULL, s);
}

static void si_val_renew_mode(status_ctx *ctx, clmd_json_t *mdj, const status_info *info)
{
    const char *s;
    switch (clmd_json_getl(mdj, info->key, NULL)) {
        case CLMD_RENEW_MANUAL: s = "manual"; break;
        case CLMD_RENEW_ALWAYS: s = "always"; break;
        default: s = "auto"; break;
    }
    kuda_brigade_puts(ctx->bb, NULL, NULL, s);
}


static void si_val_date(status_ctx *ctx, kuda_time_t timestamp)
{
    if (timestamp > 0) {
        char ts[128];
        char ts2[128];
        kuda_time_exp_t texp;
        kuda_size_t len;
        
        kuda_time_exp_gmt(&texp, timestamp);
        kuda_strftime(ts, &len, sizeof(ts)-1, "%Y-%m-%dT%H:%M:%SZ", &texp);
        ts[len] = '\0';
        kuda_strftime(ts2, &len, sizeof(ts2)-1, "%Y-%m-%d", &texp);
        ts2[len] = '\0';
        kuda_brigade_printf(ctx->bb, NULL, NULL, 
                           "<span title='%s' style='white-space: nowrap;'>%s</span>", 
                           ts, ts2);
    }
    else {
        kuda_brigade_puts(ctx->bb, NULL, NULL, "-");
    }
}

static void si_val_time(status_ctx *ctx, kuda_time_t timestamp)
{
    if (timestamp > 0) {
        char ts[128];
        char ts2[128];
        kuda_time_exp_t texp;
        kuda_size_t len;
        
        kuda_time_exp_gmt(&texp, timestamp);
        kuda_strftime(ts, &len, sizeof(ts)-1, "%Y-%m-%dT%H:%M:%SZ", &texp);
        ts[len] = '\0';
        kuda_strftime(ts2, &len, sizeof(ts2)-1, "%H:%M:%SZ", &texp);
        ts2[len] = '\0';
        kuda_brigade_printf(ctx->bb, NULL, NULL, 
                           "<span title='%s' style='white-space: nowrap;'>%s</span>", 
                           ts, ts2);
    }
    else {
        kuda_brigade_puts(ctx->bb, NULL, NULL, "-");
    }
}

static void si_val_expires(status_ctx *ctx, clmd_json_t *mdj, const status_info *info)
{
    const char *s;
    kuda_time_t t;
    
    (void)info;
    s = clmd_json_dups(ctx->p, mdj, CLMD_KEY_CERT, CLMD_KEY_VALID_UNTIL, NULL);
    if (s) {
        t = kuda_date_parse_rfc(s);
        si_val_date(ctx, t);
    }
}

static void si_val_valid_from(status_ctx *ctx, clmd_json_t *mdj, const status_info *info)
{
    const char *s;
    kuda_time_t t;
    
    (void)info;
    s = clmd_json_dups(ctx->p, mdj, CLMD_KEY_CERT, CLMD_KEY_VALID_FROM, NULL);
    if (s) {
        t = kuda_date_parse_rfc(s);
        si_val_date(ctx, t);
    }
}
    
static void si_val_props(status_ctx *ctx, clmd_json_t *mdj, const status_info *info)
{
    const char *s, *url;
    clmd_pkey_type_t ptype;
    int i = 0;
    (void)info;

    if (clmd_json_getb(mdj, CLMD_KEY_MUST_STAPLE, NULL)) {
        ++i;
        kuda_brigade_puts(ctx->bb, NULL, NULL, "must-staple");
    }
    s = clmd_json_gets(mdj, CLMD_KEY_RENEW_WINDOW, NULL);
    if (s) {
        if (i++) kuda_brigade_puts(ctx->bb, NULL, NULL, " \n"); 
        kuda_brigade_printf(ctx->bb, NULL, NULL, "renew-at[%s]", s);
    }
    url = s = clmd_json_gets(mdj, CLMD_KEY_CA, CLMD_KEY_URL, NULL);
    if (s) {
        if (i++) kuda_brigade_puts(ctx->bb, NULL, NULL, " \n"); 
        if (!strcmp(LE_ACMEv2_PROD, s)) s = "letsencrypt(v2)";
        else if (!strcmp(LE_ACMEv1_PROD, s)) s = "letsencrypt(v1)";
        else if (!strcmp(LE_ACMEv2_STAGING, s)) s = "letsencrypt(Testv2)";
        else if (!strcmp(LE_ACMEv1_STAGING, s)) s = "letsencrypt(Testv1)";
        
        kuda_brigade_printf(ctx->bb, NULL, NULL, "ca=[<a href=\"%s\">%s</a>]", url, s);
    }
    if (clmd_json_has_key(mdj, CLMD_KEY_CONTACTS, NULL)) {
        if (i++) kuda_brigade_puts(ctx->bb, NULL, NULL, " \n"); 
        kuda_brigade_puts(ctx->bb, NULL, NULL, "contacts=[");
        add_json_val(ctx, clmd_json_getj(mdj, CLMD_KEY_CONTACTS, NULL));
        kuda_brigade_puts(ctx->bb, NULL, NULL, "]");
    }
    ptype = clmd_json_has_key(mdj, CLMD_KEY_PKEY, CLMD_KEY_TYPE, NULL)?
            (unsigned)clmd_json_getl(mdj, CLMD_KEY_PKEY, CLMD_KEY_TYPE, NULL) : CLMD_PKEY_TYPE_DEFAULT; 
    switch (ptype) {
        case CLMD_PKEY_TYPE_RSA:
            if (i++) kuda_brigade_puts(ctx->bb, NULL, NULL, " \n"); 
            kuda_brigade_printf(ctx->bb, NULL, NULL, "key[RSA(%u)]", 
                (unsigned)clmd_json_getl(mdj, CLMD_KEY_PKEY, CLMD_PKEY_RSA_BITS_MIN, NULL));
        default:
            break;
    }
}

static void si_val_renewal(status_ctx *ctx, clmd_json_t *mdj, const status_info *info)
{
    char buffer[HUGE_STRING_LEN];
    kuda_status_t rv;
    int finished, errors;
    kuda_time_t t;
    const char *s;
    
    (void)info;
    if (!clmd_json_has_key(mdj, CLMD_KEY_RENEWAL, NULL)) {
        return;
    }
    
    finished = (int)clmd_json_getl(mdj, CLMD_KEY_RENEWAL, CLMD_KEY_FINISHED, NULL);
    errors = (int)clmd_json_getl(mdj, CLMD_KEY_RENEWAL, CLMD_KEY_ERRORS, NULL);
    rv = (kuda_status_t)clmd_json_getl(mdj, CLMD_KEY_RENEWAL, CLMD_KEY_LAST, CLMD_KEY_STATUS, NULL);
    
    if (rv != KUDA_SUCCESS) {
        s = clmd_json_gets(mdj, CLMD_KEY_RENEWAL, CLMD_KEY_LAST, CLMD_KEY_PROBLEM, NULL);
        kuda_brigade_printf(ctx->bb, NULL, NULL, "Error[%s]: %s", 
                           kuda_strerror(rv, buffer, sizeof(buffer)), s? s : "");
    }
    
    if (finished) {
        kuda_brigade_puts(ctx->bb, NULL, NULL, "Finished");
        if (clmd_json_has_key(mdj, CLMD_KEY_RENEWAL, CLMD_KEY_VALID_FROM, NULL)) {
            s = clmd_json_gets(mdj,  CLMD_KEY_RENEWAL, CLMD_KEY_VALID_FROM, NULL);
            t = kuda_date_parse_rfc(s);
            kuda_brigade_puts(ctx->bb, NULL, NULL, (kuda_time_now() >= t)?
                             ", valid since: " : ", activate at: ");
            si_val_time(ctx, t);
        }
        kuda_brigade_puts(ctx->bb, NULL, NULL, ".");
    } 
    
    s = clmd_json_gets(mdj, CLMD_KEY_RENEWAL, CLMD_KEY_LAST, CLMD_KEY_DETAIL, NULL);
    if (s) kuda_brigade_puts(ctx->bb, NULL, NULL, s);
    
    errors = (int)clmd_json_getl(mdj, CLMD_KEY_ERRORS, NULL);
    if (errors > 0) {
        kuda_brigade_printf(ctx->bb, NULL, NULL, ", Had %d errors.", errors);
    } 
    
    s = clmd_json_gets(mdj,  CLMD_KEY_RENEWAL, CLMD_KEY_NEXT_RUN, NULL);
    if (s) {
        t = kuda_date_parse_rfc(s);
        kuda_brigade_puts(ctx->bb, NULL, NULL, "Next attempt: ");
        si_val_time(ctx, t);
        kuda_brigade_puts(ctx->bb, NULL, NULL, ".");
    }
}

static void si_val_remote_check(status_ctx *ctx, clmd_json_t *mdj, const status_info *info)
{
    const char *fingerprint;
    
    (void)info;
    fingerprint = clmd_json_gets(mdj, CLMD_KEY_CERT, CLMD_KEY_SHA256_FINGERPRINT, NULL);
    if (fingerprint) {
        kuda_brigade_printf(ctx->bb, NULL, NULL, 
                           "<a href=\"https://censys.io/certificates/%s\">censys.io</a> ", 
                           fingerprint);
        kuda_brigade_printf(ctx->bb, NULL, NULL, 
                           "<a href=\"https://crt.sh?q=%s\">crt.sh</a> ", fingerprint);
    }
}

const status_info status_infos[] = {
    { "Name", CLMD_KEY_NAME, NULL },
    { "Domains", CLMD_KEY_DOMAINS, NULL },
    { "Status", CLMD_KEY_STATUS, si_val_status },
    { "Valid", CLMD_KEY_VALID_FROM, si_val_valid_from },
    { "Expires", CLMD_KEY_VALID_UNTIL, si_val_expires },
    { "Renew", CLMD_KEY_RENEW_MODE, si_val_renew_mode },
    { "Check@", CLMD_KEY_SHA256_FINGERPRINT, si_val_remote_check },
    { "Configuration", CLMD_KEY_MUST_STAPLE, si_val_props },
    { "Renewal",  CLMD_KEY_NOTIFIED, si_val_renewal },
};

static int json_iter_val(void *data, size_t index, clmd_json_t *json)
{
    status_ctx *ctx = data;
    if (index) kuda_brigade_puts(ctx->bb, NULL, NULL, ctx->separator);
    add_json_val(ctx, json);
    return 1;
}

static void add_json_val(status_ctx *ctx, clmd_json_t *j)
{
    if (!j) return;
    else if (clmd_json_is(CLMD_JSON_TYPE_ARRAY, j, NULL)) {
        clmd_json_itera(json_iter_val, ctx, j, NULL);
    }
    else if (clmd_json_is(CLMD_JSON_TYPE_INT, j, NULL)) {
        clmd_json_writeb(j, CLMD_JSON_FMT_COMPACT, ctx->bb);
    }
    else if (clmd_json_is(CLMD_JSON_TYPE_STRING, j, NULL)) {
        kuda_brigade_puts(ctx->bb, NULL, NULL, clmd_json_gets(j, NULL));
    }
    else if (clmd_json_is(CLMD_JSON_TYPE_OBJECT, j, NULL)) {
        clmd_json_writeb(j, CLMD_JSON_FMT_COMPACT, ctx->bb);
    }
}

static void add_status_cell(status_ctx *ctx, clmd_json_t *mdj, const status_info *info)
{
    if (info->fn) {
        info->fn(ctx, mdj, info);
    }
    else {
        add_json_val(ctx, clmd_json_getj(mdj, info->key, NULL));
    }
}

static int add_clmd_row(void *baton, kuda_size_t index, clmd_json_t *mdj)
{
    status_ctx *ctx = baton;
    int i;
    
    kuda_brigade_printf(ctx->bb, NULL, NULL, "<tr class=\"%s\">", (index % 2)? "odd" : "even");
    for (i = 0; i < (int)(sizeof(status_infos)/sizeof(status_infos[0])); ++i) {
        kuda_brigade_puts(ctx->bb, NULL, NULL, "<td>");
        add_status_cell(ctx, mdj, &status_infos[i]);
        kuda_brigade_puts(ctx->bb, NULL, NULL, "</td>");
    }
    kuda_brigade_puts(ctx->bb, NULL, NULL, "</tr>");
    return 1;
}

static int clmd_name_cmp(const void *v1, const void *v2)
{
    return strcmp((*(const clmd_t**)v1)->name, (*(const clmd_t**)v2)->name);
}

int clmd_status_hook(request_rec *r, int flags)
{
    const clmd_srv_conf_t *sc;
    const clmd_capi_conf_t *mc;
    int i, html;
    status_ctx ctx;
    kuda_array_header_t *mds;
    clmd_json_t *jstatus, *jstock;
    
    sc = clhy_get_capi_config(r->server->capi_config, &clmd_capi);
    if (!sc) return DECLINED;
    mc = sc->mc;
    if (!mc || !mc->server_status_enabled) return DECLINED;

    html = !(flags & CLHY_STATUS_SHORT);
    ctx.p = r->pool;
    ctx.mc = mc;
    ctx.bb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
    ctx.separator = " ";

    mds = kuda_array_copy(r->pool, mc->mds);
    qsort(mds->elts, (size_t)mds->nelts, sizeof(clmd_t *), clmd_name_cmp);

    if (!html) {
        kuda_brigade_puts(ctx.bb, NULL, NULL, "ManagedDomains: ");
        if (mc->mds->nelts > 0) {
            clmd_status_take_stock(&jstock, mds, mc->reg, r->pool);
            kuda_brigade_printf(ctx.bb, NULL, NULL, "total=%d, ok=%d renew=%d errored=%d ready=%d",
                                (int)clmd_json_getl(jstock, CLMD_KEY_TOTAL, NULL), 
                                (int)clmd_json_getl(jstock, CLMD_KEY_COMPLETE, NULL), 
                                (int)clmd_json_getl(jstock, CLMD_KEY_RENEWING, NULL), 
                                (int)clmd_json_getl(jstock, CLMD_KEY_ERRORED, NULL), 
                                (int)clmd_json_getl(jstock, CLMD_KEY_READY, NULL));
        } 
        else {
            kuda_brigade_puts(ctx.bb, NULL, NULL, "[]"); 
        }
        kuda_brigade_puts(ctx.bb, NULL, NULL, "\n"); 
    }
    else if (mc->mds->nelts > 0) {
        clmd_status_get_json(&jstatus, mds, mc->reg, r->pool);
        kuda_brigade_puts(ctx.bb, NULL, NULL, 
                         "<hr>\n<h2>Managed Domains</h2>\n<table class='clmd_status'><thead><tr>\n");
        for (i = 0; i < (int)(sizeof(status_infos)/sizeof(status_infos[0])); ++i) {
            kuda_brigade_puts(ctx.bb, NULL, NULL, "<th>");
            kuda_brigade_puts(ctx.bb, NULL, NULL, status_infos[i].label);
            kuda_brigade_puts(ctx.bb, NULL, NULL, "</th>");
        }
        kuda_brigade_puts(ctx.bb, NULL, NULL, "</tr>\n</thead><tbody>");
        clmd_json_itera(add_clmd_row, &ctx, jstatus, CLMD_KEY_MDS, NULL);
        kuda_brigade_puts(ctx.bb, NULL, NULL, "</td></tr>\n</tbody>\n</table>\n");
    }

    clhy_pass_brigade(r->output_filters, ctx.bb);
    kuda_brigade_cleanup(ctx.bb);
    
    return OK;
}

/**************************************************************************************************/
/* Status handler */

int clmd_status_handler(request_rec *r)
{
    const clmd_srv_conf_t *sc;
    const clmd_capi_conf_t *mc;
    kuda_array_header_t *mds;
    clmd_json_t *jstatus;
    kuda_bucket_brigade *bb;
    const clmd_t *clmd;
    const char *name;

    if (strcmp(r->handler, "clmd-status")) {
        return DECLINED;
    }

    sc = clhy_get_capi_config(r->server->capi_config, &clmd_capi);
    if (!sc) return DECLINED;
    mc = sc->mc;
    if (!mc) return DECLINED;

    if (r->method_number != M_GET) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r, "clmd-status supports only GET");
        return HTTP_NOT_IMPLEMENTED;
    }
    
    jstatus = NULL;
    clmd = NULL;
    if (r->path_info && r->path_info[0] == '/' && r->path_info[1] != '\0') {
        name = strrchr(r->path_info, '/') + 1;
        clmd = clmd_get_by_name(mc->mds, name);
        if (!clmd) clmd = clmd_get_by_domain(mc->mds, name);
    }
    
    if (clmd) {
        clmd_status_get_clmd_json(&jstatus, clmd, mc->reg, r->pool);
    }
    else {
        mds = kuda_array_copy(r->pool, mc->mds);
        qsort(mds->elts, (size_t)mds->nelts, sizeof(clmd_t *), clmd_name_cmp);
        clmd_status_get_json(&jstatus, mds, mc->reg, r->pool);
    }

    if (jstatus) {
        kuda_table_set(r->headers_out, "Content-Type", "application/json"); 
        bb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
        clmd_json_writeb(jstatus, CLMD_JSON_FMT_INDENT, bb);
        clhy_pass_brigade(r->output_filters, bb);
        kuda_brigade_cleanup(bb);
        
        return DONE;
    }
    return DECLINED;
}
