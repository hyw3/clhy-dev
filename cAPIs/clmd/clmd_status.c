/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <stdlib.h>

#include <kuda_lib.h>
#include <kuda_strings.h>
#include <kuda_tables.h>
#include <kuda_time.h>
#include <kuda_date.h>

#include "clmd_json.h"
#include "clmd.h"
#include "clmd_crypt.h"
#include "clmd_log.h"
#include "clmd_store.h"
#include "clmd_result.h"
#include "clmd_reg.h"
#include "clmd_util.h"
#include "clmd_status.h"

#define CLMD_STATUS_WITH_SCTS     0

/**************************************************************************************************/
/* certificate status information */

static kuda_status_t status_get_cert_json(clmd_json_t **pjson, const clmd_cert_t *cert, kuda_pool_t *p)
{
    char ts[KUDA_RFC822_DATE_LEN];
    const char *finger;
    kuda_status_t rv = KUDA_SUCCESS;
    clmd_json_t *json;
    
    json = clmd_json_create(p);
    kuda_rfc822_date(ts, clmd_cert_get_not_before(cert));
    clmd_json_sets(ts, json, CLMD_KEY_VALID_FROM, NULL);
    kuda_rfc822_date(ts, clmd_cert_get_not_after(cert));
    clmd_json_sets(ts, json, CLMD_KEY_VALID_UNTIL, NULL);
    clmd_json_sets(clmd_cert_get_serial_number(cert, p), json, CLMD_KEY_SERIAL, NULL);
    if (KUDA_SUCCESS != (rv = clmd_cert_to_sha256_fingerprint(&finger, cert, p))) goto leave;
    clmd_json_sets(finger, json, CLMD_KEY_SHA256_FINGERPRINT, NULL);

#if CLMD_STATUS_WITH_SCTS
    do {
        kuda_array_header_t *scts;
        const char *hex;
        const clmd_sct *sct;
        clmd_json_t *sctj;
        int i;
        
        scts = kuda_array_make(p, 5, sizeof(const clmd_sct*));
        if (KUDA_SUCCESS == clmd_cert_get_ct_scts(scts, p, cert)) {
            for (i = 0; i < scts->nelts; ++i) {
                sct = KUDA_ARRAY_IDX(scts, i, const clmd_sct*);
                sctj = clmd_json_create(p);
                
                kuda_rfc822_date(ts, sct->timestamp);
                clmd_json_sets(ts, sctj, "signed", NULL);
                clmd_json_setl(sct->version, sctj, CLMD_KEY_VERSION, NULL);
                clmd_data_to_hex(&hex, 0, p, sct->logid);
                clmd_json_sets(hex, sctj, "logid", NULL);
                clmd_data_to_hex(&hex, 0, p, sct->signature);
                clmd_json_sets(hex, sctj, "signature", NULL);
                clmd_json_sets(clmd_nid_get_sname(sct->signature_type_nid), sctj, "signature-type", NULL);
                clmd_json_addj(sctj, json, "scts", NULL);
            }
        }
    while (0);
#endif
leave:
    *pjson = (KUDA_SUCCESS == rv)? json : NULL;
    return rv;
}

/**************************************************************************************************/
/* clmd status information */

static kuda_status_t get_staging_cert_json(clmd_json_t **pjson, kuda_pool_t *p, 
                                          clmd_reg_t *reg, const clmd_t *clmd)
{ 
    clmd_json_t *json = NULL;
    kuda_array_header_t *certs;
    clmd_cert_t *cert;
    kuda_status_t rv = KUDA_SUCCESS;
    
    rv = clmd_pubcert_load(clmd_reg_store_get(reg), CLMD_SG_STAGING, clmd->name, &certs, p);
    if (KUDA_STATUS_IS_ENOENT(rv)) {
        rv = KUDA_SUCCESS;
        goto leave;
    }
    else if (KUDA_SUCCESS != rv) {
        goto leave;
    }
    cert = KUDA_ARRAY_IDX(certs, 0, clmd_cert_t *);
    rv = status_get_cert_json(&json, cert, p);
leave:
    *pjson = (KUDA_SUCCESS == rv)? json : NULL;
    return rv;
}

static kuda_status_t job_loadj(clmd_json_t **pjson, const char *name, 
                              struct clmd_reg_t *reg, kuda_pool_t *p)
{
    clmd_store_t *store = clmd_reg_store_get(reg);
    return clmd_store_load_json(store, CLMD_SG_STAGING, name, CLMD_FN_JOB, pjson, p);
}

kuda_status_t clmd_status_get_clmd_json(clmd_json_t **pjson, const clmd_t *clmd, 
                                   clmd_reg_t *reg, kuda_pool_t *p)
{
    clmd_json_t *mdj, *jobj, *certj;
    int renew;
    const clmd_pubcert_t *pubcert;
    const clmd_cert_t *cert;
    kuda_status_t rv = KUDA_SUCCESS;

    mdj = clmd_to_json(clmd, p);
    if (KUDA_SUCCESS == clmd_reg_get_pubcert(&pubcert, reg, clmd, p)) {
        cert = KUDA_ARRAY_IDX(pubcert->certs, 0, const clmd_cert_t*);
        if (KUDA_SUCCESS != (rv = status_get_cert_json(&certj, cert, p))) goto leave;
        clmd_json_setj(certj, mdj, CLMD_KEY_CERT, NULL);
    }
    
    renew = clmd_reg_should_renew(reg, clmd, p);
    clmd_json_setb(renew, mdj, CLMD_KEY_RENEW, NULL);
    if (renew) {
        rv = job_loadj(&jobj, clmd->name, reg, p);
        if (KUDA_SUCCESS == rv) {
            rv = get_staging_cert_json(&certj, p, reg, clmd);
            if (KUDA_SUCCESS != rv) goto leave;
            if (certj) clmd_json_setj(certj, jobj, CLMD_KEY_CERT, NULL);
            clmd_json_setj(jobj, mdj, CLMD_KEY_RENEWAL, NULL);
        }
        else if (KUDA_STATUS_IS_ENOENT(rv)) rv = KUDA_SUCCESS;
        else goto leave;
    }
leave:
    *pjson = (KUDA_SUCCESS == rv)? mdj : NULL;
    return rv;
}

kuda_status_t clmd_status_get_json(clmd_json_t **pjson, kuda_array_header_t *mds, 
                                clmd_reg_t *reg, kuda_pool_t *p) 
{
    clmd_json_t *json, *mdj;
    kuda_status_t rv = KUDA_SUCCESS;
    const clmd_t *clmd;
    int i;
    
    json = clmd_json_create(p);
    clmd_json_sets(CAPI_CLMD_VERSION, json, CLMD_KEY_VERSION, NULL);
    for (i = 0; i < mds->nelts; ++i) {
        clmd = KUDA_ARRAY_IDX(mds, i, const clmd_t *);
        rv = clmd_status_get_clmd_json(&mdj, clmd, reg, p);
        if (KUDA_SUCCESS != rv) goto leave;
        clmd_json_addj(mdj, json, CLMD_KEY_MDS, NULL);
    }
leave:
    *pjson = (KUDA_SUCCESS == rv)? json : NULL;
    return rv;
}

/**************************************************************************************************/
/* drive job persistence */

clmd_job_t *clmd_job_make(kuda_pool_t *p, const char *name)
{
    clmd_job_t *job = kuda_pcalloc(p, sizeof(*job));
    job->name = kuda_pstrdup(p, name);
    job->p = p;
    return job;
}

static void clmd_job_from_json(clmd_job_t *job, clmd_json_t *json, kuda_pool_t *p)
{
    const char *s;
    
    /* not good, this is malloced from a temp pool */
    /*job->name = clmd_json_gets(json, CLMD_KEY_NAME, NULL);*/
    job->finished = clmd_json_getb(json, CLMD_KEY_FINISHED, NULL);
    s = clmd_json_dups(p, json, CLMD_KEY_NEXT_RUN, NULL);
    if (s && *s) job->next_run = kuda_date_parse_rfc(s);
    s = clmd_json_dups(p, json, CLMD_KEY_LAST_RUN, NULL);
    if (s && *s) job->last_run = kuda_date_parse_rfc(s);
    s = clmd_json_dups(p, json, CLMD_KEY_VALID_FROM, NULL);
    if (s && *s) job->valid_from = kuda_date_parse_rfc(s);
    job->error_runs = (int)clmd_json_getl(json, CLMD_KEY_ERRORS, NULL);
    if (clmd_json_has_key(json, CLMD_KEY_LAST, NULL)) {
        job->last_result = clmd_result_from_json(clmd_json_getcj(json, CLMD_KEY_LAST, NULL), p);
    }
    job->log = clmd_json_getj(json, CLMD_KEY_LOG, NULL);
}

static void job_to_json(clmd_json_t *json, const clmd_job_t *job, 
                        clmd_result_t *result, kuda_pool_t *p)
{
    char ts[KUDA_RFC822_DATE_LEN];

    clmd_json_sets(job->name, json, CLMD_KEY_NAME, NULL);
    clmd_json_setb(job->finished, json, CLMD_KEY_FINISHED, NULL);
    if (job->next_run > 0) {
        kuda_rfc822_date(ts, job->next_run);
        clmd_json_sets(ts, json, CLMD_KEY_NEXT_RUN, NULL);
    }
    if (job->last_run > 0) {
        kuda_rfc822_date(ts, job->last_run);
        clmd_json_sets(ts, json, CLMD_KEY_LAST_RUN, NULL);
    }
    if (job->valid_from > 0) {
        kuda_rfc822_date(ts, job->valid_from);
        clmd_json_sets(ts, json, CLMD_KEY_VALID_FROM, NULL);
    }
    clmd_json_setl(job->error_runs, json, CLMD_KEY_ERRORS, NULL);
    if (!result) result = job->last_result;
    if (result) {
        clmd_json_setj(clmd_result_to_json(result, p), json, CLMD_KEY_LAST, NULL);
    }
    if (job->log) clmd_json_setj(job->log, json, CLMD_KEY_LOG, NULL);
}

kuda_status_t clmd_job_load(clmd_job_t *job, clmd_reg_t *reg, 
                         clmd_store_group_t group, kuda_pool_t *p)
{
    clmd_store_t *store = clmd_reg_store_get(reg);
    clmd_json_t *jprops;
    kuda_status_t rv;
    
    rv = clmd_store_load_json(store, group, job->name, CLMD_FN_JOB, &jprops, p);
    if (KUDA_SUCCESS == rv) {
        clmd_job_from_json(job, jprops, p);
    }
    return rv;
}

kuda_status_t clmd_job_save(clmd_job_t *job, struct clmd_reg_t *reg, 
                         clmd_store_group_t group, clmd_result_t *result, 
                         kuda_pool_t *p)
{
    clmd_store_t *store = clmd_reg_store_get(reg);
    clmd_json_t *jprops;
    kuda_status_t rv;
    
    jprops = clmd_json_create(p);
    job_to_json(jprops, job, result, p);
    rv = clmd_store_save_json(store, p, group, job->name, CLMD_FN_JOB, jprops, 0);
    return rv;
}

void clmd_job_log_append(clmd_job_t *job, const char *type, 
                       const char *status, const char *detail)
{
    clmd_json_t *entry;
    char ts[KUDA_RFC822_DATE_LEN];
    
    entry = clmd_json_create(job->p);
    kuda_rfc822_date(ts, kuda_time_now());
    clmd_json_sets(ts, entry, CLMD_KEY_WHEN, NULL);
    clmd_json_sets(type, entry, CLMD_KEY_TYPE, NULL);
    if (status) clmd_json_sets(status, entry, CLMD_KEY_STATUS, NULL);
    if (detail) clmd_json_sets(detail, entry, CLMD_KEY_DETAIL, NULL);
    if (!job->log) job->log = clmd_json_create(job->p);
    clmd_json_insertj(entry, 0, job->log, CLMD_KEY_ENTRIES, NULL);
}

typedef struct {
    clmd_job_t *job;
    const char *type;
    clmd_json_t *entry;
    size_t index;
} log_find_ctx;

static int find_first_log_entry(void *baton, size_t index, clmd_json_t *entry)
{
    log_find_ctx *ctx = baton;
    const char *etype;
    
    etype = clmd_json_gets(entry, CLMD_KEY_TYPE, NULL);
    if (etype == ctx->type || (etype && ctx->type && !strcmp(etype, ctx->type))) {
        ctx->entry = entry;
        ctx->index = index;
        return 0;
    }
    return 1;
}

clmd_json_t *clmd_job_log_get_latest(clmd_job_t *job, const char *type)

{
    log_find_ctx ctx;
    ctx.job = job;
    ctx.type = type;
    memset(&ctx, 0, sizeof(ctx));
    if (job->log) clmd_json_itera(find_first_log_entry, &ctx, job->log, CLMD_KEY_ENTRIES, NULL);
    return ctx.entry;
}

kuda_time_t clmd_job_log_get_time_of_latest(clmd_job_t *job, const char *type)
{
    clmd_json_t *entry;
    const char *s;
    
    entry = clmd_job_log_get_latest(job, type);
    if (entry) {
        s = clmd_json_gets(entry, CLMD_KEY_WHEN, NULL);
        if (s) return kuda_date_parse_rfc(s);
    }
    return 0;
}

void  clmd_status_take_stock(clmd_json_t **pjson, kuda_array_header_t *mds, 
                           clmd_reg_t *reg, kuda_pool_t *p)
{
    const clmd_t *clmd;
    clmd_job_t job;
    int i, complete, renewing, errored, ready, total;
    clmd_json_t *json;

    json = clmd_json_create(p);
    complete = renewing = errored = ready = total = 0;
    for (i = 0; i < mds->nelts; ++i) {
        clmd = KUDA_ARRAY_IDX(mds, i, const clmd_t *);
        ++total;
        switch (clmd->state) {
            case CLMD_S_COMPLETE: ++complete; /* fall through */
            case CLMD_S_INCOMPLETE:
                if (clmd_reg_should_renew(reg, clmd, p)) {
                    ++renewing;
                    memset(&job, 0, sizeof(job));
                    job.name = clmd->name;
                    if (KUDA_SUCCESS == clmd_job_load(&job, reg, CLMD_SG_STAGING, p)) {
                        if (job.error_runs > 0 
                            || (job.last_result && job.last_result->status != KUDA_SUCCESS)) {
                            ++errored;
                        }
                        else if (job.finished) {
                            ++ready;
                        }
                    }
                }
                break;
            default: ++errored; break;
        }
    }
    clmd_json_setl(total, json, CLMD_KEY_TOTAL, NULL);
    clmd_json_setl(complete, json, CLMD_KEY_COMPLETE, NULL);
    clmd_json_setl(renewing, json, CLMD_KEY_RENEWING, NULL);
    clmd_json_setl(errored, json, CLMD_KEY_ERRORED, NULL);
    clmd_json_setl(ready, json, CLMD_KEY_READY, NULL);
    *pjson = json;
}
