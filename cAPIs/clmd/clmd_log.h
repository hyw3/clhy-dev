/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_log_h
#define capi_clmd_clmd_log_h

typedef enum {
    CLMD_LOG_EMERG,
    CLMD_LOG_ALERT,
    CLMD_LOG_CRIT,
    CLMD_LOG_ERR, 
    CLMD_LOG_WARNING, 
    CLMD_LOG_NOTICE, 
    CLMD_LOG_INFO, 
    CLMD_LOG_DEBUG, 
    CLMD_LOG_TRACE1, 
    CLMD_LOG_TRACE2, 
    CLMD_LOG_TRACE3, 
    CLMD_LOG_TRACE4, 
    CLMD_LOG_TRACE5, 
    CLMD_LOG_TRACE6, 
    CLMD_LOG_TRACE7, 
    CLMD_LOG_TRACE8, 
} clmd_log_level_t;

#define CLMD_LOG_MARK     __FILE__,__LINE__

const char *clmd_log_level_name(clmd_log_level_t level);

int clmd_log_is_level(kuda_pool_t *p, clmd_log_level_t level);

void clmd_log_perror(const char *file, int line, clmd_log_level_t level, 
                   kuda_status_t rv, kuda_pool_t *p, const char *fmt, ...)
                                __attribute__((format(printf,6,7)));

typedef int clmd_log_level_cb(void *baton, kuda_pool_t *p, clmd_log_level_t level);

typedef void clmd_log_print_cb(const char *file, int line, clmd_log_level_t level, 
                kuda_status_t rv, void *baton, kuda_pool_t *p, const char *fmt, va_list ap);

void clmd_log_set(clmd_log_level_cb *level_cb, clmd_log_print_cb *print_cb, void *baton);

#endif /* clmd_log_h */
