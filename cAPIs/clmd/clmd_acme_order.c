/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <stdio.h>

#include <kuda_lib.h>
#include <kuda_buckets.h>
#include <kuda_file_info.h>
#include <kuda_file_io.h>
#include <kuda_fnmatch.h>
#include <kuda_hash.h>
#include <kuda_strings.h>
#include <kuda_tables.h>

#include "clmd.h"
#include "clmd_crypt.h"
#include "clmd_json.h"
#include "clmd_http.h"
#include "clmd_log.h"
#include "clmd_jws.h"
#include "clmd_result.h"
#include "clmd_store.h"
#include "clmd_util.h"

#include "clmd_acme.h"
#include "clmd_acme_authz.h"
#include "clmd_acme_order.h"


clmd_acme_order_t *clmd_acme_order_create(kuda_pool_t *p)
{
    clmd_acme_order_t *order;
    
    order = kuda_pcalloc(p, sizeof(*order));
    order->p = p;
    order->authz_urls = kuda_array_make(p, 5, sizeof(const char *));
    order->challenge_setups = kuda_array_make(p, 5, sizeof(const char *));
    
    return order;
}

/**************************************************************************************************/
/* order conversion */

#define CLMD_KEY_CHALLENGE_SETUPS   "challenge-setups"

static clmd_acme_order_st order_st_from_str(const char *s) 
{
    if (s) {
        if (!strcmp("valid", s)) {
            return CLMD_ACME_ORDER_ST_VALID;
        }
        else if (!strcmp("invalid", s)) {
            return CLMD_ACME_ORDER_ST_INVALID;
        }
        else if (!strcmp("ready", s)) {
            return CLMD_ACME_ORDER_ST_READY;
        }
        else if (!strcmp("pending", s)) {
            return CLMD_ACME_ORDER_ST_PENDING;
        }
        else if (!strcmp("processing", s)) {
            return CLMD_ACME_ORDER_ST_PROCESSING;
        }
    }
    return CLMD_ACME_ORDER_ST_PENDING;
}

static const char *order_st_to_str(clmd_acme_order_st status) 
{
    switch (status) {
        case CLMD_ACME_ORDER_ST_PENDING:
            return "pending";
        case CLMD_ACME_ORDER_ST_READY:
            return "ready";
        case CLMD_ACME_ORDER_ST_PROCESSING:
            return "processing";
        case CLMD_ACME_ORDER_ST_VALID:
            return "valid";
        case CLMD_ACME_ORDER_ST_INVALID:
            return "invalid";
        default:
            return "invalid";
    }
}

clmd_json_t *clmd_acme_order_to_json(clmd_acme_order_t *order, kuda_pool_t *p)
{
    clmd_json_t *json = clmd_json_create(p);

    if (order->url) {
        clmd_json_sets(order->url, json, CLMD_KEY_URL, NULL);
    }
    clmd_json_sets(order_st_to_str(order->status), json, CLMD_KEY_STATUS, NULL);
    clmd_json_setsa(order->authz_urls, json, CLMD_KEY_AUTHORIZATIONS, NULL);
    clmd_json_setsa(order->challenge_setups, json, CLMD_KEY_CHALLENGE_SETUPS, NULL);
    if (order->finalize) {
        clmd_json_sets(order->finalize, json, CLMD_KEY_FINALIZE, NULL);
    }
    if (order->certificate) {
        clmd_json_sets(order->certificate, json, CLMD_KEY_CERTIFICATE, NULL);
    }
    return json;
}

static void order_update_from_json(clmd_acme_order_t *order, clmd_json_t *json, kuda_pool_t *p)
{
    if (!order->url && clmd_json_has_key(json, CLMD_KEY_URL, NULL)) {
        order->url = clmd_json_dups(p, json, CLMD_KEY_URL, NULL);
    }
    order->status = order_st_from_str(clmd_json_gets(json, CLMD_KEY_STATUS, NULL));
    if (clmd_json_has_key(json, CLMD_KEY_AUTHORIZATIONS, NULL)) {
        clmd_json_dupsa(order->authz_urls, p, json, CLMD_KEY_AUTHORIZATIONS, NULL);
    }
    if (clmd_json_has_key(json, CLMD_KEY_CHALLENGE_SETUPS, NULL)) {
        clmd_json_dupsa(order->challenge_setups, p, json, CLMD_KEY_CHALLENGE_SETUPS, NULL);
    }
    if (clmd_json_has_key(json, CLMD_KEY_FINALIZE, NULL)) {
        order->finalize = clmd_json_dups(p, json, CLMD_KEY_FINALIZE, NULL);
    }
    if (clmd_json_has_key(json, CLMD_KEY_CERTIFICATE, NULL)) {
        order->certificate = clmd_json_dups(p, json, CLMD_KEY_CERTIFICATE, NULL);
    }
}

clmd_acme_order_t *clmd_acme_order_from_json(clmd_json_t *json, kuda_pool_t *p)
{
    clmd_acme_order_t *order = clmd_acme_order_create(p);

    order_update_from_json(order, json, p);
    return order;
}

kuda_status_t clmd_acme_order_add(clmd_acme_order_t *order, const char *authz_url)
{
    assert(authz_url);
    if (clmd_array_str_index(order->authz_urls, authz_url, 0, 1) < 0) {
        KUDA_ARRAY_PUSH(order->authz_urls, const char*) = kuda_pstrdup(order->p, authz_url);
    }
    return KUDA_SUCCESS;
}

kuda_status_t clmd_acme_order_remove(clmd_acme_order_t *order, const char *authz_url)
{
    int i;
    
    assert(authz_url);
    i = clmd_array_str_index(order->authz_urls, authz_url, 0, 1);
    if (i >= 0) {
        order->authz_urls = clmd_array_str_remove(order->p, order->authz_urls, authz_url, 1);
        return KUDA_SUCCESS;
    }
    return KUDA_ENOENT;
}

static kuda_status_t add_setup_token(clmd_acme_order_t *order, const char *token)
{
    if (clmd_array_str_index(order->challenge_setups, token, 0, 1) < 0) {
        KUDA_ARRAY_PUSH(order->challenge_setups, const char*) = kuda_pstrdup(order->p, token);
    }
    return KUDA_SUCCESS;
}

/**************************************************************************************************/
/* persistence */

kuda_status_t clmd_acme_order_load(struct clmd_store_t *store, clmd_store_group_t group, 
                                    const char *clmd_name, clmd_acme_order_t **pauthz_set, 
                                    kuda_pool_t *p)
{
    kuda_status_t rv;
    clmd_json_t *json;
    clmd_acme_order_t *authz_set;
    
    rv = clmd_store_load_json(store, group, clmd_name, CLMD_FN_ORDER, &json, p);
    if (KUDA_SUCCESS == rv) {
        authz_set = clmd_acme_order_from_json(json, p);
    }
    *pauthz_set = (KUDA_SUCCESS == rv)? authz_set : NULL;
    return rv;  
}

static kuda_status_t p_save(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_store_t *store = baton;
    clmd_json_t *json;
    clmd_store_group_t group;
    clmd_acme_order_t *set;
    const char *clmd_name;
    int create;
 
    (void)p;   
    group = (clmd_store_group_t)va_arg(ap, int);
    clmd_name = va_arg(ap, const char *);
    set = va_arg(ap, clmd_acme_order_t *);
    create = va_arg(ap, int);

    json = clmd_acme_order_to_json(set, ptemp);
    assert(json);
    return clmd_store_save_json(store, ptemp, group, clmd_name, CLMD_FN_ORDER, json, create);
}

kuda_status_t clmd_acme_order_save(struct clmd_store_t *store, kuda_pool_t *p,
                                    clmd_store_group_t group, const char *clmd_name, 
                                    clmd_acme_order_t *authz_set, int create)
{
    return clmd_util_pool_vdo(p_save, store, p, group, clmd_name, authz_set, create, NULL);
}

static kuda_status_t p_purge(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_store_t *store = baton;
    clmd_acme_order_t *order;
    clmd_store_group_t group;
    const char *clmd_name, *setup_token;
    kuda_table_t *env;
    int i;

    group = (clmd_store_group_t)va_arg(ap, int);
    clmd_name = va_arg(ap, const char *);
    env = va_arg(ap, kuda_table_t *);

    if (KUDA_SUCCESS == clmd_acme_order_load(store, group, clmd_name, &order, p)) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, p, "order loaded for %s", clmd_name);
        for (i = 0; i < order->challenge_setups->nelts; ++i) {
            setup_token = KUDA_ARRAY_IDX(order->challenge_setups, i, const char*);
            if (setup_token) {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, p, 
                              "order teardown setup %s", setup_token);
                clmd_acme_authz_teardown(store, setup_token, env, p);
            }
        }
    }
    return clmd_store_remove(store, group, clmd_name, CLMD_FN_ORDER, ptemp, 1);
}

kuda_status_t clmd_acme_order_purge(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group,
                                 const char *clmd_name, kuda_table_t *env)
{
    return clmd_util_pool_vdo(p_purge, store, p, group, clmd_name, env, NULL);
}

/**************************************************************************************************/
/* ACMEv2 order requests */

typedef struct {
    kuda_pool_t *p;
    clmd_acme_order_t *order;
    clmd_acme_t *acme;
    const char *name;
    kuda_array_header_t *domains;
    clmd_result_t *result;
} order_ctx_t;

#define ORDER_CTX_INIT(ctx, p, o, a, n, d, r) \
    (ctx)->p = (p); (ctx)->order = (o); (ctx)->acme = (a); \
    (ctx)->name = (n); (ctx)->domains = d; (ctx)->result = r

static kuda_status_t identifier_to_json(void *value, clmd_json_t *json, kuda_pool_t *p, void *baton)
{
    clmd_json_t *jid;
    
    (void)baton;
    jid = clmd_json_create(p);
    clmd_json_sets("dns", jid, "type", NULL);
    clmd_json_sets(value, jid, "value", NULL);
    return clmd_json_setj(jid, json, NULL);
}

static kuda_status_t on_init_order_register(clmd_acme_req_t *req, void *baton)
{
    order_ctx_t *ctx = baton;
    clmd_json_t *jpayload;

    jpayload = clmd_json_create(req->p);
    clmd_json_seta(ctx->domains, identifier_to_json, NULL, jpayload, "identifiers", NULL);

    return clmd_acme_req_body_init(req, jpayload);
} 

static kuda_status_t on_order_upd(clmd_acme_t *acme, kuda_pool_t *p, const kuda_table_t *hdrs, 
                                 clmd_json_t *body, void *baton)
{
    order_ctx_t *ctx = baton;
    const char *location = kuda_table_get(hdrs, "location");
    kuda_status_t rv = KUDA_SUCCESS;
    
    (void)acme;
    (void)p;
    if (!ctx->order) {
        if (location) {
            ctx->order = clmd_acme_order_create(ctx->p);
            ctx->order->url = kuda_pstrdup(ctx->p, location);
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, rv, ctx->p, "new order at %s", location);
        }
        else {
            rv = KUDA_EINVAL;
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, rv, ctx->p, "new order, no location header");
            goto out;
        }
    }
    
    order_update_from_json(ctx->order, body, ctx->p);
out:
    return rv;
}

kuda_status_t clmd_acme_order_register(clmd_acme_order_t **porder, clmd_acme_t *acme, kuda_pool_t *p, 
                                    const char *name, kuda_array_header_t *domains)
{
    order_ctx_t ctx;
    kuda_status_t rv;
    
    assert(CLMD_ACME_VERSION_MAJOR(acme->version) > 1);
    ORDER_CTX_INIT(&ctx, p, NULL, acme, name, domains, NULL);
    rv = clmd_acme_POST(acme, acme->api.v2.new_order, on_init_order_register, on_order_upd, NULL, NULL, &ctx);
    *porder = (KUDA_SUCCESS == rv)? ctx.order : NULL;
    return rv;
}

kuda_status_t clmd_acme_order_update(clmd_acme_order_t *order, clmd_acme_t *acme, 
                                  clmd_result_t *result, kuda_pool_t *p)
{
    order_ctx_t ctx;
    kuda_status_t rv;
    
    assert(CLMD_ACME_VERSION_MAJOR(acme->version) > 1);
    ORDER_CTX_INIT(&ctx, p, order, acme, NULL, NULL, result);
    rv = clmd_acme_GET(acme, order->url, NULL, on_order_upd, NULL, NULL, &ctx);
    if (KUDA_SUCCESS != rv && KUDA_SUCCESS != acme->last->status) {
        clmd_result_dup(result, acme->last);
    }
    return rv;
}

static kuda_status_t await_ready(void *baton, int attempt)
{
    order_ctx_t *ctx = baton;
    kuda_status_t rv = KUDA_SUCCESS;
    
    (void)attempt;
    if (KUDA_SUCCESS != (rv = clmd_acme_order_update(ctx->order, ctx->acme,
                                                  ctx->result, ctx->p))) goto out;
    switch (ctx->order->status) {
        case CLMD_ACME_ORDER_ST_READY:
        case CLMD_ACME_ORDER_ST_PROCESSING:
        case CLMD_ACME_ORDER_ST_VALID:
            break;
        case CLMD_ACME_ORDER_ST_PENDING:
            rv = KUDA_EAGAIN;
            break;
        default:
            rv = KUDA_EINVAL;
            break;
    }
out:    
    return rv;
}

kuda_status_t clmd_acme_order_await_ready(clmd_acme_order_t *order, clmd_acme_t *acme, 
                                       const clmd_t *clmd, kuda_interval_time_t timeout, 
                                       clmd_result_t *result, kuda_pool_t *p)
{
    order_ctx_t ctx;
    kuda_status_t rv;
    
    assert(CLMD_ACME_VERSION_MAJOR(acme->version) > 1);
    ORDER_CTX_INIT(&ctx, p, order, acme, clmd->name, NULL, result);

    clmd_result_activity_setn(result, "Waiting for order to become ready");
    rv = clmd_util_try(await_ready, &ctx, 0, timeout, 0, 0, 1);
    clmd_result_log(result, CLMD_LOG_DEBUG);
    return rv;
}

static kuda_status_t await_valid(void *baton, int attempt)
{
    order_ctx_t *ctx = baton;
    kuda_status_t rv = KUDA_SUCCESS;

    (void)attempt;
    if (KUDA_SUCCESS != (rv = clmd_acme_order_update(ctx->order, ctx->acme, 
                                                  ctx->result, ctx->p))) goto out;
    switch (ctx->order->status) {
        case CLMD_ACME_ORDER_ST_VALID:
            break;
        case CLMD_ACME_ORDER_ST_PROCESSING:
            rv = KUDA_EAGAIN;
            break;
        default:
            rv = KUDA_EINVAL;
            break;
    }
out:    
    return rv;
}

kuda_status_t clmd_acme_order_await_valid(clmd_acme_order_t *order, clmd_acme_t *acme, 
                                       const clmd_t *clmd, kuda_interval_time_t timeout, 
                                       clmd_result_t *result, kuda_pool_t *p)
{
    order_ctx_t ctx;
    kuda_status_t rv;
    
    assert(CLMD_ACME_VERSION_MAJOR(acme->version) > 1);
    ORDER_CTX_INIT(&ctx, p, order, acme, clmd->name, NULL, result);

    clmd_result_activity_setn(result, "Waiting for finalized order to become valid");
    rv = clmd_util_try(await_valid, &ctx, 0, timeout, 0, 0, 1);
    clmd_result_log(result, CLMD_LOG_DEBUG);
    return rv;
}

/**************************************************************************************************/
/* processing */

kuda_status_t clmd_acme_order_start_challenges(clmd_acme_order_t *order, clmd_acme_t *acme, 
                                            kuda_array_header_t *challenge_types,
                                            clmd_store_t *store, const clmd_t *clmd, 
                                            kuda_table_t *env, clmd_result_t *result, 
                                            kuda_pool_t *p)
{
    kuda_status_t rv = KUDA_SUCCESS;
    clmd_acme_authz_t *authz;
    const char *url, *setup_token;
    int i;
    
    clmd_result_activity_printf(result, "Starting challenges for domains");
    for (i = 0; i < order->authz_urls->nelts; ++i) {
        url = KUDA_ARRAY_IDX(order->authz_urls, i, const char*);
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, "%s: check AUTHZ at %s", clmd->name, url);
        
        if (KUDA_SUCCESS != (rv = clmd_acme_authz_retrieve(acme, p, url, &authz))) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, p, "%s: check authz for %s",
                          clmd->name, authz->domain);
            goto out;
        }

        switch (authz->state) {
            case CLMD_ACME_AUTHZ_S_VALID:
                break;
                
            case CLMD_ACME_AUTHZ_S_PENDING:
                rv = clmd_acme_authz_respond(authz, acme, store, challenge_types, 
                                           clmd->pkey_spec, clmd->acme_tls_1_domains,
                                           env, p, &setup_token, result);
                if (KUDA_SUCCESS != rv) {
                    goto out;
                }
                add_setup_token(order, setup_token);
                clmd_acme_order_save(store, p, CLMD_SG_STAGING, clmd->name, order, 0);
                break;
                
            default:
                rv = KUDA_EINVAL;
                clmd_result_printf(result, rv, "unexpected AUTHZ state %d for domain %s", 
                                 authz->state, authz->domain);
                clmd_result_log(result, CLMD_LOG_ERR);
             goto out;
        }
    }
out:    
    return rv;
}

static kuda_status_t check_challenges(void *baton, int attempt)
{
    order_ctx_t *ctx = baton;
    const char *url;
    clmd_acme_authz_t *authz;
    kuda_status_t rv = KUDA_SUCCESS;
    int i;
    
    for (i = 0; i < ctx->order->authz_urls->nelts; ++i) {
        url = KUDA_ARRAY_IDX(ctx->order->authz_urls, i, const char*);
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, ctx->p, "%s: check AUTHZ at %s (attempt %d)", 
                      ctx->name, url, attempt);
        
        rv = clmd_acme_authz_retrieve(ctx->acme, ctx->p, url, &authz);
        if (KUDA_SUCCESS == rv) {
            switch (authz->state) {
                case CLMD_ACME_AUTHZ_S_VALID:
                    clmd_result_printf(ctx->result, rv, 
                                     "domain authorization for %s is valid", authz->domain);
                    break;
                case CLMD_ACME_AUTHZ_S_PENDING:
                    rv = KUDA_EAGAIN;
                    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, ctx->p, 
                                  "%s: status pending at %s", authz->domain, authz->url);
                    goto leave;
                default:
                    rv = KUDA_EINVAL;
                    clmd_result_printf(ctx->result, rv, 
                                     "domain authorization for %s failed with state %d", 
                                     authz->domain, authz->state);
                    clmd_result_log(ctx->result, CLMD_LOG_ERR);
                    goto leave;
            }
        }
        else {
            clmd_result_printf(ctx->result, rv, "authorization retrieval failed for domain %s", 
                             authz->domain);
        }
    }
leave:
    return rv;
}

kuda_status_t clmd_acme_order_monitor_authzs(clmd_acme_order_t *order, clmd_acme_t *acme, 
                                          const clmd_t *clmd, kuda_interval_time_t timeout, 
                                          clmd_result_t *result, kuda_pool_t *p)
{
    order_ctx_t ctx;
    kuda_status_t rv;
    
    ORDER_CTX_INIT(&ctx, p, order, acme, clmd->name, NULL, result);
    
    clmd_result_activity_printf(result, "Monitoring challenge status for %s", clmd->name);
    rv = clmd_util_try(check_challenges, &ctx, 0, timeout, 0, 0, 1);
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, rv, p, "%s: checked authorizations", clmd->name);
    return rv;
}

