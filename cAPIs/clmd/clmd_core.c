/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <stdlib.h>

#include <kuda_lib.h>
#include <kuda_strings.h>
#include <kuda_tables.h>
#include <kuda_time.h>
#include <kuda_date.h>

#include "clmd_json.h"
#include "clmd.h"
#include "clmd_crypt.h"
#include "clmd_log.h"
#include "clmd_store.h"
#include "clmd_util.h"


int clmd_contains(const clmd_t *clmd, const char *domain, int case_sensitive)
{
   return clmd_array_str_index(clmd->domains, domain, 0, case_sensitive) >= 0;
}

const char *clmd_common_name(const clmd_t *md1, const clmd_t *md2)
{
    int i;
    
    if (md1 == NULL || md1->domains == NULL
        || md2 == NULL || md2->domains == NULL) {
        return NULL;
    }
    
    for (i = 0; i < md1->domains->nelts; ++i) {
        const char *name1 = KUDA_ARRAY_IDX(md1->domains, i, const char*);
        if (clmd_contains(md2, name1, 0)) {
            return name1;
        }
    }
    return NULL;
}

int clmd_domains_overlap(const clmd_t *md1, const clmd_t *md2)
{
    return clmd_common_name(md1, md2) != NULL;
}

kuda_size_t clmd_common_name_count(const clmd_t *md1, const clmd_t *md2)
{
    int i;
    kuda_size_t hits;
    
    if (md1 == NULL || md1->domains == NULL
        || md2 == NULL || md2->domains == NULL) {
        return 0;
    }
    
    hits = 0;
    for (i = 0; i < md1->domains->nelts; ++i) {
        const char *name1 = KUDA_ARRAY_IDX(md1->domains, i, const char*);
        if (clmd_contains(md2, name1, 0)) {
            ++hits;
        }
    }
    return hits;
}

int clmd_is_covered_by_alt_names(const clmd_t *clmd, const struct kuda_array_header_t* alt_names)
{
    const char *name;
    int i;
    
    if (alt_names) {
        for (i = 0; i < clmd->domains->nelts; ++i) {
            name = KUDA_ARRAY_IDX(clmd->domains, i, const char *);
            if (!clmd_dns_domains_match(alt_names, name)) {
                return 0;
            }
        }
        return 1;
    }
    return 0;
}

clmd_t *clmd_create_empty(kuda_pool_t *p)
{
    clmd_t *clmd = kuda_pcalloc(p, sizeof(*clmd));
    if (clmd) {
        clmd->domains = kuda_array_make(p, 5, sizeof(const char *));
        clmd->contacts = kuda_array_make(p, 5, sizeof(const char *));
        clmd->renew_mode = CLMD_RENEW_DEFAULT;
        clmd->require_https = CLMD_REQUIRE_UNSET;
        clmd->must_staple = -1;
        clmd->transitive = -1;
        clmd->acme_tls_1_domains = kuda_array_make(p, 5, sizeof(const char *));
        clmd->defn_name = "unknown";
        clmd->defn_line_number = 0;
    }
    return clmd;
}

int clmd_equal_domains(const clmd_t *md1, const clmd_t *md2, int case_sensitive)
{
    int i;
    if (md1->domains->nelts == md2->domains->nelts) {
        for (i = 0; i < md1->domains->nelts; ++i) {
            const char *name1 = KUDA_ARRAY_IDX(md1->domains, i, const char*);
            if (!clmd_contains(md2, name1, case_sensitive)) {
                return 0;
            }
        }
        return 1;
    }
    return 0;
}

int clmd_contains_domains(const clmd_t *md1, const clmd_t *md2)
{
    int i;
    if (md1->domains->nelts >= md2->domains->nelts) {
        for (i = 0; i < md2->domains->nelts; ++i) {
            const char *name2 = KUDA_ARRAY_IDX(md2->domains, i, const char*);
            if (!clmd_contains(md1, name2, 0)) {
                return 0;
            }
        }
        return 1;
    }
    return 0;
}

clmd_t *clmd_find_closest_match(kuda_array_header_t *mds, const clmd_t *clmd)
{
    clmd_t *candidate, *m;
    kuda_size_t cand_n, n;
    int i;
    
    candidate = clmd_get_by_name(mds, clmd->name);
    if (!candidate) {
        /* try to find an instance that contains all domain names from clmd */ 
        for (i = 0; i < mds->nelts; ++i) {
            m = KUDA_ARRAY_IDX(mds, i, clmd_t *);
            if (clmd_contains_domains(m, clmd)) {
                return m;
            }
        }
        /* no matching name and no clmd in the list has all domains.
         * We consider that managed domain as closest match that contains at least one
         * domain name from clmd, ONLY if there is no other one that also has.
         */
        cand_n = 0;
        for (i = 0; i < mds->nelts; ++i) {
            m = KUDA_ARRAY_IDX(mds, i, clmd_t *);
            n = clmd_common_name_count(clmd, m);
            if (n > cand_n) {
                candidate = m;
                cand_n = n;
            }
        }
    }
    return candidate;
}

clmd_t *clmd_get_by_name(struct kuda_array_header_t *mds, const char *name)
{
    int i;
    for (i = 0; i < mds->nelts; ++i) {
        clmd_t *clmd = KUDA_ARRAY_IDX(mds, i, clmd_t *);
        if (!strcmp(name, clmd->name)) {
            return clmd;
        }
    }
    return NULL;
}

clmd_t *clmd_get_by_domain(struct kuda_array_header_t *mds, const char *domain)
{
    int i;
    for (i = 0; i < mds->nelts; ++i) {
        clmd_t *clmd = KUDA_ARRAY_IDX(mds, i, clmd_t *);
        if (clmd_contains(clmd, domain, 0)) {
            return clmd;
        }
    }
    return NULL;
}

clmd_t *clmd_get_by_dns_overlap(struct kuda_array_header_t *mds, const clmd_t *clmd)
{
    int i;
    for (i = 0; i < mds->nelts; ++i) {
        clmd_t *o = KUDA_ARRAY_IDX(mds, i, clmd_t *);
        if (strcmp(o->name, clmd->name) && clmd_common_name(o, clmd)) {
            return o;
        }
    }
    return NULL;
}

clmd_t *clmd_create(kuda_pool_t *p, kuda_array_header_t *domains)
{
    clmd_t *clmd;
    
    clmd = clmd_create_empty(p);
    clmd->domains = clmd_array_str_compact(p, domains, 0);
    clmd->name = KUDA_ARRAY_IDX(clmd->domains, 0, const char *);
    
    return clmd;
}

/**************************************************************************************************/
/* lifetime */

clmd_t *clmd_copy(kuda_pool_t *p, const clmd_t *src)
{
    clmd_t *clmd;
    
    clmd = kuda_pcalloc(p, sizeof(*clmd));
    if (clmd) {
        memcpy(clmd, src, sizeof(*clmd));
        clmd->domains = kuda_array_copy(p, src->domains);
        clmd->contacts = kuda_array_copy(p, src->contacts);
        if (src->ca_challenges) {
            clmd->ca_challenges = kuda_array_copy(p, src->ca_challenges);
        }
        clmd->acme_tls_1_domains = kuda_array_copy(p, src->acme_tls_1_domains);
    }    
    return clmd;   
}

clmd_t *clmd_clone(kuda_pool_t *p, const clmd_t *src)
{
    clmd_t *clmd;
    
    clmd = kuda_pcalloc(p, sizeof(*clmd));
    if (clmd) {
        clmd->state = src->state;
        clmd->name = kuda_pstrdup(p, src->name);
        clmd->require_https = src->require_https;
        clmd->must_staple = src->must_staple;
        clmd->renew_mode = src->renew_mode;
        clmd->domains = clmd_array_str_compact(p, src->domains, 0);
        clmd->pkey_spec = src->pkey_spec;
        clmd->renew_window = src->renew_window;
        clmd->warn_window = src->warn_window;
        clmd->contacts = clmd_array_str_clone(p, src->contacts);
        if (src->ca_url) clmd->ca_url = kuda_pstrdup(p, src->ca_url);
        if (src->ca_proto) clmd->ca_proto = kuda_pstrdup(p, src->ca_proto);
        if (src->ca_account) clmd->ca_account = kuda_pstrdup(p, src->ca_account);
        if (src->ca_agreement) clmd->ca_agreement = kuda_pstrdup(p, src->ca_agreement);
        if (src->defn_name) clmd->defn_name = kuda_pstrdup(p, src->defn_name);
        clmd->defn_line_number = src->defn_line_number;
        if (src->ca_challenges) {
            clmd->ca_challenges = clmd_array_str_clone(p, src->ca_challenges);
        }
        clmd->acme_tls_1_domains = clmd_array_str_compact(p, src->acme_tls_1_domains, 0);
        if (src->cert_file) clmd->cert_file = kuda_pstrdup(p, src->cert_file);
        if (src->pkey_file) clmd->pkey_file = kuda_pstrdup(p, src->pkey_file);
    }    
    return clmd;   
}

/**************************************************************************************************/
/* format conversion */

clmd_json_t *clmd_to_json(const clmd_t *clmd, kuda_pool_t *p)
{
    clmd_json_t *json = clmd_json_create(p);
    if (json) {
        kuda_array_header_t *domains = clmd_array_str_compact(p, clmd->domains, 0);
        clmd_json_sets(clmd->name, json, CLMD_KEY_NAME, NULL);
        clmd_json_setsa(domains, json, CLMD_KEY_DOMAINS, NULL);
        clmd_json_setsa(clmd->contacts, json, CLMD_KEY_CONTACTS, NULL);
        clmd_json_setl(clmd->transitive, json, CLMD_KEY_TRANSITIVE, NULL);
        clmd_json_sets(clmd->ca_account, json, CLMD_KEY_CA, CLMD_KEY_ACCOUNT, NULL);
        clmd_json_sets(clmd->ca_proto, json, CLMD_KEY_CA, CLMD_KEY_PROTO, NULL);
        clmd_json_sets(clmd->ca_url, json, CLMD_KEY_CA, CLMD_KEY_URL, NULL);
        clmd_json_sets(clmd->ca_agreement, json, CLMD_KEY_CA, CLMD_KEY_AGREEMENT, NULL);
        if (clmd->pkey_spec) {
            clmd_json_setj(clmd_pkey_spec_to_json(clmd->pkey_spec, p), json, CLMD_KEY_PKEY, NULL);
        }
        clmd_json_setl(clmd->state, json, CLMD_KEY_STATE, NULL);
        clmd_json_setl(clmd->renew_mode, json, CLMD_KEY_RENEW_MODE, NULL);
        if (clmd->renew_window)
            clmd_json_sets(clmd_timeslice_format(clmd->renew_window, p), json, CLMD_KEY_RENEW_WINDOW, NULL);
        if (clmd->warn_window)
            clmd_json_sets(clmd_timeslice_format(clmd->warn_window, p), json, CLMD_KEY_WARN_WINDOW, NULL);
        if (clmd->ca_challenges && clmd->ca_challenges->nelts > 0) {
            kuda_array_header_t *na;
            na = clmd_array_str_compact(p, clmd->ca_challenges, 0);
            clmd_json_setsa(na, json, CLMD_KEY_CA, CLMD_KEY_CHALLENGES, NULL);
        }
        switch (clmd->require_https) {
            case CLMD_REQUIRE_TEMPORARY:
                clmd_json_sets(CLMD_KEY_TEMPORARY, json, CLMD_KEY_REQUIRE_HTTPS, NULL);
                break;
            case CLMD_REQUIRE_PERMANENT:
                clmd_json_sets(CLMD_KEY_PERMANENT, json, CLMD_KEY_REQUIRE_HTTPS, NULL);
                break;
            default:
                break;
        }
        clmd_json_setb(clmd->must_staple > 0, json, CLMD_KEY_MUST_STAPLE, NULL);
        if (!kuda_is_empty_array(clmd->acme_tls_1_domains))
            clmd_json_setsa(clmd->acme_tls_1_domains, json, CLMD_KEY_PROTO, CLMD_KEY_ACME_TLS_1, NULL);
        clmd_json_sets(clmd->cert_file, json, CLMD_KEY_CERT_FILE, NULL);
        clmd_json_sets(clmd->pkey_file, json, CLMD_KEY_PKEY_FILE, NULL);
        return json;
    }
    return NULL;
}

clmd_t *clmd_from_json(clmd_json_t *json, kuda_pool_t *p)
{
    const char *s;
    clmd_t *clmd = clmd_create_empty(p);
    if (clmd) {
        clmd->name = clmd_json_dups(p, json, CLMD_KEY_NAME, NULL);            
        clmd_json_dupsa(clmd->domains, p, json, CLMD_KEY_DOMAINS, NULL);
        clmd_json_dupsa(clmd->contacts, p, json, CLMD_KEY_CONTACTS, NULL);
        clmd->ca_account = clmd_json_dups(p, json, CLMD_KEY_CA, CLMD_KEY_ACCOUNT, NULL);
        clmd->ca_proto = clmd_json_dups(p, json, CLMD_KEY_CA, CLMD_KEY_PROTO, NULL);
        clmd->ca_url = clmd_json_dups(p, json, CLMD_KEY_CA, CLMD_KEY_URL, NULL);
        clmd->ca_agreement = clmd_json_dups(p, json, CLMD_KEY_CA, CLMD_KEY_AGREEMENT, NULL);
        if (clmd_json_has_key(json, CLMD_KEY_PKEY, CLMD_KEY_TYPE, NULL)) {
            clmd->pkey_spec = clmd_pkey_spec_from_json(clmd_json_getj(json, CLMD_KEY_PKEY, NULL), p);
        }
        clmd->state = (clmd_state_t)clmd_json_getl(json, CLMD_KEY_STATE, NULL);
        if (CLMD_S_EXPIRED_DEPRECATED == clmd->state) clmd->state = CLMD_S_COMPLETE;
        clmd->renew_mode = (int)clmd_json_getl(json, CLMD_KEY_RENEW_MODE, NULL);
        clmd->domains = clmd_array_str_compact(p, clmd->domains, 0);
        clmd->transitive = (int)clmd_json_getl(json, CLMD_KEY_TRANSITIVE, NULL);
        s = clmd_json_gets(json, CLMD_KEY_RENEW_WINDOW, NULL);
        clmd_timeslice_parse(&clmd->renew_window, p, s, CLMD_TIME_LIFE_NORM);
        s = clmd_json_gets(json, CLMD_KEY_WARN_WINDOW, NULL);
        clmd_timeslice_parse(&clmd->warn_window, p, s, CLMD_TIME_LIFE_NORM);
        if (clmd_json_has_key(json, CLMD_KEY_CA, CLMD_KEY_CHALLENGES, NULL)) {
            clmd->ca_challenges = kuda_array_make(p, 5, sizeof(const char*));
            clmd_json_dupsa(clmd->ca_challenges, p, json, CLMD_KEY_CA, CLMD_KEY_CHALLENGES, NULL);
        }
        clmd->require_https = CLMD_REQUIRE_OFF;
        s = clmd_json_gets(json, CLMD_KEY_REQUIRE_HTTPS, NULL);
        if (s && !strcmp(CLMD_KEY_TEMPORARY, s)) {
            clmd->require_https = CLMD_REQUIRE_TEMPORARY;
        }
        else if (s && !strcmp(CLMD_KEY_PERMANENT, s)) {
            clmd->require_https = CLMD_REQUIRE_PERMANENT;
        }
        clmd->must_staple = (int)clmd_json_getb(json, CLMD_KEY_MUST_STAPLE, NULL);
        clmd_json_dupsa(clmd->acme_tls_1_domains, p, json, CLMD_KEY_PROTO, CLMD_KEY_ACME_TLS_1, NULL);
            
        clmd->cert_file = clmd_json_dups(p, json, CLMD_KEY_CERT_FILE, NULL); 
        clmd->pkey_file = clmd_json_dups(p, json, CLMD_KEY_PKEY_FILE, NULL); 
        
        return clmd;
    }
    return NULL;
}

