# Microsoft Developer Studio Generated NMAKE File, Based on capi_clmd.dsp
!IF "$(CFG)" == ""
CFG=capi_clmd - Win32 Release
!MESSAGE No configuration specified. Defaulting to capi_clmd - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "capi_clmd - Win32 Release" && "$(CFG)" != "capi_clmd - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "capi_clmd.mak" CFG="capi_clmd - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "capi_clmd - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "capi_clmd - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF "$(_HAVE_OSSL110)" == "1"
SSLCRP=libcrypto
SSLLIB=libssl
SSLINC=/I ../../kudelrunsrc/openssl/include
SSLBIN=/libpath:../../kudelrunsrc/openssl
!ELSE 
SSLCRP=libeay32
SSLLIB=ssleay32
SSLINC=/I ../../kudelrunsrc/openssl/inc32
SSLBIN=/libpath:../../kudelrunsrc/openssl/out32dll
!ENDIF 

!IF  "$(CFG)" == "capi_clmd - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_clmd.so"

!ELSE 

ALL : "libwwhy - Win32 Release" "libkudadelman - Win32 Release" "libkuda - Win32 Release" "$(OUTDIR)\capi_clmd.so"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudadelman - Win32 ReleaseCLEAN" "libwwhy - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\clmd_acme.obj"
	-@erase "$(INTDIR)\clmd_acme_acct.obj"
	-@erase "$(INTDIR)\clmd_acme_authz.obj"
	-@erase "$(INTDIR)\clmd_acme_drive.obj"
	-@erase "$(INTDIR)\clmd_acme_order.obj"
	-@erase "$(INTDIR)\clmd_acmev1_drive.obj"
	-@erase "$(INTDIR)\clmd_acmev2_drive.obj"
	-@erase "$(INTDIR)\clmd_core.obj"
	-@erase "$(INTDIR)\clmd_crypt.obj"
	-@erase "$(INTDIR)\clmd_curl.obj"
	-@erase "$(INTDIR)\clmd_http.obj"
	-@erase "$(INTDIR)\clmd_json.obj"
	-@erase "$(INTDIR)\clmd_jws.obj"
	-@erase "$(INTDIR)\clmd_log.obj"
	-@erase "$(INTDIR)\clmd_reg.obj"
	-@erase "$(INTDIR)\clmd_result.obj"
	-@erase "$(INTDIR)\clmd_status.obj"
	-@erase "$(INTDIR)\clmd_store.obj"
	-@erase "$(INTDIR)\clmd_store_fs.obj"
	-@erase "$(INTDIR)\clmd_time.obj"
	-@erase "$(INTDIR)\clmd_util.obj"
	-@erase "$(INTDIR)\capi_clmd.obj"
	-@erase "$(INTDIR)\capi_clmd.res"
	-@erase "$(INTDIR)\capi_clmd_config.obj"
	-@erase "$(INTDIR)\capi_clmd_drive.obj"
	-@erase "$(INTDIR)\capi_clmd_status.obj"
	-@erase "$(INTDIR)\capi_clmd_platform.obj"
	-@erase "$(INTDIR)\capi_clmd_src.idb"
	-@erase "$(INTDIR)\capi_clmd_src.pdb"
	-@erase "$(OUTDIR)\capi_clmd.exp"
	-@erase "$(OUTDIR)\capi_clmd.lib"
	-@erase "$(OUTDIR)\capi_clmd.pdb"
	-@erase "$(OUTDIR)\capi_clmd.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /CLMD /W3 /Zi /O2 /Oy- /I "../../server/clcore/winnt" /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" $(SSLINC) /I "../../kudelrunsrc/jansson/include" /I "../../kudelrunsrc/curl/include" /I "../clssl" /I "../core" /I "../generators" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D ssize_t=long /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_clmd_src" /FD /I " ../clssl" /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_clmd.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "NDEBUG" /d BIN_NAME=capi_clmd.so /d LONG_NAME=Letsencrypt cAPI for cLHy 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_clmd.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib libwwhy.lib libkuda-1.lib libkudadelman-1.lib $(SSLCRP).lib $(SSLLIB).lib jansson.lib libcurl.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_clmd.pdb" /debug  /out:"$(OUTDIR)\capi_clmd.so" /implib:"$(OUTDIR)\capi_clmd.lib" /libpath:"../../kudelrunsrc/kuda/Release" /libpath:"../../kudelrunsrc/kuda-delman/Release" /libpath:"../../Release/" $(SSLBIN) /libpath:"../../kudelrunsrc/jansson/lib" /libpath:"../../kudelrunsrc/curl/lib" /base:@..\..\platforms\win32\BaseAddr.ref,capi_clmd.so /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\capi_clmd.obj" \
	"$(INTDIR)\capi_clmd_config.obj" \
	"$(INTDIR)\capi_clmd_drive.obj" \
	"$(INTDIR)\capi_clmd_platform.obj" \
	"$(INTDIR)\capi_clmd_status.obj" \
	"$(INTDIR)\clmd_core.obj" \
	"$(INTDIR)\clmd_crypt.obj" \
	"$(INTDIR)\clmd_curl.obj" \
	"$(INTDIR)\clmd_http.obj" \
	"$(INTDIR)\clmd_json.obj" \
	"$(INTDIR)\clmd_jws.obj" \
	"$(INTDIR)\clmd_log.obj" \
	"$(INTDIR)\clmd_reg.obj" \
	"$(INTDIR)\clmd_result.obj" \
	"$(INTDIR)\clmd_status.obj" \
	"$(INTDIR)\clmd_store.obj" \
	"$(INTDIR)\clmd_store_fs.obj" \
	"$(INTDIR)\clmd_time.obj" \
	"$(INTDIR)\clmd_util.obj" \
	"$(INTDIR)\clmd_acme.obj" \
	"$(INTDIR)\clmd_acme_acct.obj" \
	"$(INTDIR)\clmd_acme_authz.obj" \
	"$(INTDIR)\clmd_acme_drive.obj" \
	"$(INTDIR)\clmd_acme_order.obj" \
	"$(INTDIR)\clmd_acmev1_drive.obj" \
	"$(INTDIR)\clmd_acmev2_drive.obj" \
	"$(INTDIR)\capi_clmd.res" \
	"..\..\kudelrunsrc\kuda\Release\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Release\libkudadelman-1.lib" \
	"..\..\Release\libwwhy.lib"

"$(OUTDIR)\capi_clmd.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\capi_clmd.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

ALL : $(DS_POSTBUILD_DEP)

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

$(DS_POSTBUILD_DEP) : "libwwhy - Win32 Release" "libkudadelman - Win32 Release" "libkuda - Win32 Release" "$(OUTDIR)\capi_clmd.so"
   if exist .\Release\capi_clmd.so.manifest mt.exe -manifest .\Release\capi_clmd.so.manifest -outputresource:.\Release\capi_clmd.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "capi_clmd - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_clmd.so"

!ELSE 

ALL : "libwwhy - Win32 Debug" "libkudadelman - Win32 Debug" "libkuda - Win32 Debug" "$(OUTDIR)\capi_clmd.so"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudadelman - Win32 DebugCLEAN" "libwwhy - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\clmd_acme.obj"
	-@erase "$(INTDIR)\clmd_acme_acct.obj"
	-@erase "$(INTDIR)\clmd_acme_authz.obj"
	-@erase "$(INTDIR)\clmd_acme_drive.obj"
	-@erase "$(INTDIR)\clmd_acme_order.obj"
	-@erase "$(INTDIR)\clmd_acmev1_drive.obj"
	-@erase "$(INTDIR)\clmd_acmev2_drive.obj"
	-@erase "$(INTDIR)\clmd_core.obj"
	-@erase "$(INTDIR)\clmd_crypt.obj"
	-@erase "$(INTDIR)\clmd_curl.obj"
	-@erase "$(INTDIR)\clmd_http.obj"
	-@erase "$(INTDIR)\clmd_json.obj"
	-@erase "$(INTDIR)\clmd_jws.obj"
	-@erase "$(INTDIR)\clmd_log.obj"
	-@erase "$(INTDIR)\clmd_reg.obj"
	-@erase "$(INTDIR)\clmd_result.obj"
	-@erase "$(INTDIR)\clmd_status.obj"
	-@erase "$(INTDIR)\clmd_store.obj"
	-@erase "$(INTDIR)\clmd_store_fs.obj"
	-@erase "$(INTDIR)\clmd_time.obj"
	-@erase "$(INTDIR)\clmd_util.obj"
	-@erase "$(INTDIR)\capi_clmd.obj"
	-@erase "$(INTDIR)\capi_clmd.res"
	-@erase "$(INTDIR)\capi_clmd_config.obj"
	-@erase "$(INTDIR)\capi_clmd_drive.obj"
	-@erase "$(INTDIR)\capi_clmd_status.obj"
	-@erase "$(INTDIR)\capi_clmd_platform.obj"
	-@erase "$(INTDIR)\capi_clmd_src.idb"
	-@erase "$(INTDIR)\capi_clmd_src.pdb"
	-@erase "$(OUTDIR)\capi_clmd.exp"
	-@erase "$(OUTDIR)\capi_clmd.lib"
	-@erase "$(OUTDIR)\capi_clmd.pdb"
	-@erase "$(OUTDIR)\capi_clmd.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" $(SSLINC) /I "../../kudelrunsrc/jansson/include" /I "../../kudelrunsrc/curl/include" /I "../core" /I "../generators" /I "../clssl" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D ssize_t=long /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_clmd_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_clmd.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "_DEBUG" /d BIN_NAME=capi_clmd.so /d LONG_NAME=http2_capi for cLHy 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_clmd.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib libwwhy.lib libkuda-1.lib libkudadelman-1.lib $(SSLCRP).lib $(SSLLIB).lib jansson_d.lib libcurl_debug.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_clmd.pdb" /debug  /out:"$(OUTDIR)\capi_clmd.so" /implib:"$(OUTDIR)\capi_clmd.lib" $(SSLBIN) /libpath:"../../kudelrunsrc/jansson/lib" /libpath:"../../kudelrunsrc/curl/lib" /base:@..\..\platforms\win32\BaseAddr.ref,capi_clmd.so 
LINK32_OBJS= \
	"$(INTDIR)\capi_clmd.obj" \
	"$(INTDIR)\capi_clmd_config.obj" \
	"$(INTDIR)\capi_clmd_drive.obj" \
	"$(INTDIR)\capi_clmd_platform.obj" \
	"$(INTDIR)\capi_clmd_status.obj" \
	"$(INTDIR)\clmd_core.obj" \
	"$(INTDIR)\clmd_crypt.obj" \
	"$(INTDIR)\clmd_curl.obj" \
	"$(INTDIR)\clmd_http.obj" \
	"$(INTDIR)\clmd_json.obj" \
	"$(INTDIR)\clmd_jws.obj" \
	"$(INTDIR)\clmd_log.obj" \
	"$(INTDIR)\clmd_reg.obj" \
	"$(INTDIR)\clmd_result.obj" \
	"$(INTDIR)\clmd_status.obj" \
	"$(INTDIR)\clmd_store.obj" \
	"$(INTDIR)\clmd_store_fs.obj" \
	"$(INTDIR)\clmd_time.obj" \
	"$(INTDIR)\clmd_util.obj" \
	"$(INTDIR)\clmd_acme.obj" \
	"$(INTDIR)\clmd_acme_acct.obj" \
	"$(INTDIR)\clmd_acme_authz.obj" \
	"$(INTDIR)\clmd_acme_drive.obj" \
	"$(INTDIR)\clmd_acme_order.obj" \
	"$(INTDIR)\clmd_acmev1_drive.obj" \
	"$(INTDIR)\clmd_acmev2_drive.obj" \
	"$(INTDIR)\capi_clmd.res" \
	"..\..\kudelrunsrc\kuda\Debug\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Debug\libkudadelman-1.lib" \
	"..\..\Debug\libwwhy.lib"

"$(OUTDIR)\capi_clmd.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\capi_clmd.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

ALL : $(DS_POSTBUILD_DEP)

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

$(DS_POSTBUILD_DEP) : "libwwhy - Win32 Debug" "libkudadelman - Win32 Debug" "libkuda - Win32 Debug" "$(OUTDIR)\capi_clmd.so"
   if exist .\Debug\capi_clmd.so.manifest mt.exe -manifest .\Debug\capi_clmd.so.manifest -outputresource:.\Debug\capi_clmd.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("capi_clmd.dep")
!INCLUDE "capi_clmd.dep"
!ELSE 
!MESSAGE Warning: cannot find "capi_clmd.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "capi_clmd - Win32 Release" || "$(CFG)" == "capi_clmd - Win32 Debug"

!IF  "$(CFG)" == "capi_clmd - Win32 Release"

"libkuda - Win32 Release" : 
   cd "..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\..\cAPIs\clmd"

"libkuda - Win32 ReleaseCLEAN" : 
   cd "..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clmd"

!ELSEIF  "$(CFG)" == "capi_clmd - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd "..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\..\cAPIs\clmd"

"libkuda - Win32 DebugCLEAN" : 
   cd "..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clmd"

!ENDIF 

!IF  "$(CFG)" == "capi_clmd - Win32 Release"

"libkudadelman - Win32 Release" : 
   cd "..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" 
   cd "..\..\cAPIs\clmd"

"libkudadelman - Win32 ReleaseCLEAN" : 
   cd "..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clmd"

!ELSEIF  "$(CFG)" == "capi_clmd - Win32 Debug"

"libkudadelman - Win32 Debug" : 
   cd "..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" 
   cd "..\..\cAPIs\clmd"

"libkudadelman - Win32 DebugCLEAN" : 
   cd "..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clmd"

!ENDIF 

!IF  "$(CFG)" == "capi_clmd - Win32 Release"

"libwwhy - Win32 Release" : 
   cd "..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" 
   cd ".\cAPIs\clmd"

"libwwhy - Win32 ReleaseCLEAN" : 
   cd "..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" RECURSE=1 CLEAN 
   cd ".\cAPIs\clmd"

!ELSEIF  "$(CFG)" == "capi_clmd - Win32 Debug"

"libwwhy - Win32 Debug" : 
   cd "..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" 
   cd ".\cAPIs\clmd"

"libwwhy - Win32 DebugCLEAN" : 
   cd "..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" RECURSE=1 CLEAN 
   cd ".\cAPIs\clmd"

!ENDIF 

SOURCE=..\..\build\win32\wwhy.rc

!IF  "$(CFG)" == "capi_clmd - Win32 Release"


"$(INTDIR)\capi_clmd.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_clmd.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../build\win32" /d "NDEBUG" /d BIN_NAME="capi_clmd.so" /d LONG_NAME="clmd_capi for cLHy" $(SOURCE)


!ELSEIF  "$(CFG)" == "capi_clmd - Win32 Debug"


"$(INTDIR)\capi_clmd.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_clmd.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../build\win32" /d "_DEBUG" /d BIN_NAME="capi_clmd.so" /d LONG_NAME="clmd_capi for cLHy" $(SOURCE)


!ENDIF 

SOURCE=./clmd_acme.c

"$(INTDIR)\clmd_acme.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_acme_acct.c

"$(INTDIR)\clmd_acme_acct.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_acme_authz.c

"$(INTDIR)\clmd_acme_authz.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_acme_drive.c

"$(INTDIR)\clmd_acme_drive.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_acme_order.c

"$(INTDIR)\clmd_acme_order.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_acmev1_drive.c

"$(INTDIR)\clmd_acmev1_drive.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_acmev2_drive.c

"$(INTDIR)\clmd_acmev2_drive.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_core.c

"$(INTDIR)\clmd_core.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_crypt.c

"$(INTDIR)\clmd_crypt.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_curl.c

"$(INTDIR)\clmd_curl.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_http.c

"$(INTDIR)\clmd_http.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_json.c

"$(INTDIR)\clmd_json.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_jws.c

"$(INTDIR)\clmd_jws.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_log.c

"$(INTDIR)\clmd_log.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_reg.c

"$(INTDIR)\clmd_reg.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_result.c

"$(INTDIR)\clmd_result.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_status.c

"$(INTDIR)\clmd_status.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_store.c

"$(INTDIR)\clmd_store.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_store_fs.c

"$(INTDIR)\clmd_store_fs.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_time.c

"$(INTDIR)\clmd_time.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./clmd_util.c

"$(INTDIR)\clmd_util.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./capi_clmd.c

"$(INTDIR)\capi_clmd.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./capi_clmd_config.c

"$(INTDIR)\capi_clmd_config.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./capi_clmd_drive.c

"$(INTDIR)\capi_clmd_drive.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./capi_clmd_platform.c

"$(INTDIR)\capi_clmd_platform.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=./capi_clmd_status.c

"$(INTDIR)\capi_clmd_status.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

