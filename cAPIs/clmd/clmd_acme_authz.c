/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <stdio.h>

#include <kuda_lib.h>
#include <kuda_buckets.h>
#include <kuda_file_info.h>
#include <kuda_file_io.h>
#include <kuda_fnmatch.h>
#include <kuda_hash.h>
#include <kuda_strings.h>
#include <kuda_tables.h>

#include "clmd.h"
#include "clmd_crypt.h"
#include "clmd_json.h"
#include "clmd_http.h"
#include "clmd_log.h"
#include "clmd_jws.h"
#include "clmd_result.h"
#include "clmd_store.h"
#include "clmd_util.h"

#include "clmd_acme.h"
#include "clmd_acme_authz.h"

clmd_acme_authz_t *clmd_acme_authz_create(kuda_pool_t *p)
{
    clmd_acme_authz_t *authz;
    authz = kuda_pcalloc(p, sizeof(*authz));
    
    return authz;
}

/**************************************************************************************************/
/* Register a new authorization */

typedef struct {
    size_t index;
    const char *type;
    const char *uri;
    const char *token;
    const char *key_authz;
} clmd_acme_authz_cha_t;

typedef struct {
    kuda_pool_t *p;
    clmd_acme_t *acme;
    const char *domain;
    clmd_acme_authz_t *authz;
    clmd_acme_authz_cha_t *challenge;
} authz_req_ctx;

static void authz_req_ctx_init(authz_req_ctx *ctx, clmd_acme_t *acme, 
                               const char *domain, clmd_acme_authz_t *authz, kuda_pool_t *p)
{
    memset(ctx, 0, sizeof(*ctx));
    ctx->p = p;
    ctx->acme = acme;
    ctx->domain = domain;
    ctx->authz = authz;
}

static kuda_status_t on_init_authz(clmd_acme_req_t *req, void *baton)
{
    authz_req_ctx *ctx = baton;
    clmd_json_t *jpayload;

    jpayload = clmd_json_create(req->p);
    clmd_json_sets("new-authz", jpayload, CLMD_KEY_RESOURCE, NULL);
    clmd_json_sets("dns", jpayload, CLMD_KEY_IDENTIFIER, CLMD_KEY_TYPE, NULL);
    clmd_json_sets(ctx->domain, jpayload, CLMD_KEY_IDENTIFIER, CLMD_KEY_VALUE, NULL);
    
    return clmd_acme_req_body_init(req, jpayload);
} 

static kuda_status_t authz_created(clmd_acme_t *acme, kuda_pool_t *p, const kuda_table_t *hdrs, 
                                  clmd_json_t *body, void *baton)
{
    authz_req_ctx *ctx = baton;
    const char *location = kuda_table_get(hdrs, "location");
    kuda_status_t rv = KUDA_SUCCESS;
    
    (void)acme;
    (void)p;
    if (location) {
        ctx->authz = clmd_acme_authz_create(ctx->p);
        ctx->authz->domain = kuda_pstrdup(ctx->p, ctx->domain);
        ctx->authz->url = kuda_pstrdup(ctx->p, location);
        ctx->authz->resource = clmd_json_clone(ctx->p, body);
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, rv, ctx->p, "authz_new at %s", location);
    }
    else {
        rv = KUDA_EINVAL;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, rv, ctx->p, "new authz, no location header");
    }
    return rv;
}

kuda_status_t clmd_acme_authz_register(struct clmd_acme_authz_t **pauthz, clmd_acme_t *acme, 
                                    const char *domain, kuda_pool_t *p)
{
    kuda_status_t rv;
    authz_req_ctx ctx;
    
    authz_req_ctx_init(&ctx, acme, domain, NULL, p);
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, acme->p, "create new authz");
    rv = clmd_acme_POST(acme, acme->api.v1.new_authz, on_init_authz, authz_created, NULL, NULL, &ctx);
    
    *pauthz = (KUDA_SUCCESS == rv)? ctx.authz : NULL;
    return rv;
}

/**************************************************************************************************/
/* Update an existing authorization */

kuda_status_t clmd_acme_authz_retrieve(clmd_acme_t *acme, kuda_pool_t *p, const char *url, 
                                    clmd_acme_authz_t **pauthz)
{
    clmd_acme_authz_t *authz;
    kuda_status_t rv;
    
    authz = kuda_pcalloc(p, sizeof(*authz));
    authz->url = kuda_pstrdup(p, url);
    rv = clmd_acme_authz_update(authz, acme, p);
    
    *pauthz = (KUDA_SUCCESS == rv)? authz : NULL;
    return rv;
}

kuda_status_t clmd_acme_authz_update(clmd_acme_authz_t *authz, clmd_acme_t *acme, kuda_pool_t *p)
{
    clmd_json_t *json;
    const char *s, *err;
    clmd_log_level_t log_level;
    kuda_status_t rv;
    
    assert(acme);
    assert(acme->http);
    assert(authz);
    assert(authz->url);

    authz->state = CLMD_ACME_AUTHZ_S_UNKNOWN;
    json = NULL;
    err = "unable to parse response";
    log_level = CLMD_LOG_ERR;
    
    if (KUDA_SUCCESS == (rv = clmd_acme_get_json(&json, acme, authz->url, p))
        && (s = clmd_json_gets(json, CLMD_KEY_STATUS, NULL))) {
            
        authz->domain = clmd_json_gets(json, CLMD_KEY_IDENTIFIER, CLMD_KEY_VALUE, NULL); 
        authz->resource = json;
        if (!strcmp(s, "pending")) {
            authz->state = CLMD_ACME_AUTHZ_S_PENDING;
            err = "challenge 'pending'";
            log_level = CLMD_LOG_DEBUG;
        }
        else if (!strcmp(s, "valid")) {
            authz->state = CLMD_ACME_AUTHZ_S_VALID;
            err = "challenge 'valid'";
            log_level = CLMD_LOG_DEBUG;
        }
        else if (!strcmp(s, "invalid")) {
            authz->state = CLMD_ACME_AUTHZ_S_INVALID;
            err = "challenge 'invalid'";
        }
    }

    if (json && authz->state == CLMD_ACME_AUTHZ_S_UNKNOWN) {
        err = "unable to understand response";
        rv = KUDA_EINVAL;
    }
    
    if (clmd_log_is_level(p, log_level)) {
        clmd_log_perror(CLMD_LOG_MARK, log_level, rv, p, "ACME server authz: %s for %s at %s. "
                      "Exact response was: %s", err? err : "", authz->domain, authz->url,
                      json? clmd_json_writep(json, p, CLMD_JSON_FMT_COMPACT) : "not available");
    }
    
    return rv;
}

/**************************************************************************************************/
/* response to a challenge */

static clmd_acme_authz_cha_t *cha_from_json(kuda_pool_t *p, size_t index, clmd_json_t *json)
{
    clmd_acme_authz_cha_t * cha;
    
    cha = kuda_pcalloc(p, sizeof(*cha));
    cha->index = index;
    cha->type = clmd_json_dups(p, json, CLMD_KEY_TYPE, NULL);
    if (clmd_json_has_key(json, CLMD_KEY_URL, NULL)) { /* ACMEv2 */
        cha->uri = clmd_json_dups(p, json, CLMD_KEY_URL, NULL);
    }
    else {                                         /* ACMEv1 */
        cha->uri = clmd_json_dups(p, json, CLMD_KEY_URI, NULL);
    }
    cha->token = clmd_json_dups(p, json, CLMD_KEY_TOKEN, NULL);
    cha->key_authz = clmd_json_dups(p, json, CLMD_KEY_KEYAUTHZ, NULL);

    return cha;
}

static kuda_status_t on_init_authz_resp(clmd_acme_req_t *req, void *baton)
{
    authz_req_ctx *ctx = baton;
    clmd_json_t *jpayload;

    jpayload = clmd_json_create(req->p);
    if (CLMD_ACME_VERSION_MAJOR(req->acme->version) <= 1) {
        clmd_json_sets(CLMD_KEY_CHALLENGE, jpayload, CLMD_KEY_RESOURCE, NULL);
    }
    if (ctx->challenge->key_authz) {
        clmd_json_sets(ctx->challenge->key_authz, jpayload, CLMD_KEY_KEYAUTHZ, NULL);
    }
    
    return clmd_acme_req_body_init(req, jpayload);
} 

static kuda_status_t authz_http_set(clmd_acme_t *acme, kuda_pool_t *p, const kuda_table_t *hdrs, 
                                   clmd_json_t *body, void *baton)
{
    authz_req_ctx *ctx = baton;
    
    (void)acme;
    (void)p;
    (void)hdrs;
    (void)body;
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, 0, ctx->p, "updated authz %s", ctx->authz->url);
    return KUDA_SUCCESS;
}

static kuda_status_t setup_key_authz(clmd_acme_authz_cha_t *cha, clmd_acme_authz_t *authz,
                                    clmd_acme_t *acme, kuda_pool_t *p, int *pchanged)
{
    const char *thumb64, *key_authz;
    kuda_status_t rv;
    
    (void)authz;
    assert(cha);
    assert(cha->token);
    
    *pchanged = 0;
    if (KUDA_SUCCESS == (rv = clmd_jws_pkey_thumb(&thumb64, p, acme->acct_key))) {
        key_authz = kuda_psprintf(p, "%s.%s", cha->token, thumb64);
        if (cha->key_authz) {
            if (strcmp(key_authz, cha->key_authz)) {
                /* Hu? Did the account change key? */
                cha->key_authz = NULL;
            }
        }
        if (!cha->key_authz) {
            cha->key_authz = key_authz;
            *pchanged = 1;
        }
    }
    return rv;
}

static kuda_status_t cha_http_01_setup(clmd_acme_authz_cha_t *cha, clmd_acme_authz_t *authz, 
                                      clmd_acme_t *acme, clmd_store_t *store, 
                                      clmd_pkey_spec_t *key_spec, 
                                      kuda_array_header_t *acme_tls_1_domains, 
                                      kuda_table_t *env, kuda_pool_t *p)
{
    const char *data;
    kuda_status_t rv;
    int notify_server;
    
    (void)key_spec;
    (void)env;
    (void)acme_tls_1_domains;
    if (KUDA_SUCCESS != (rv = setup_key_authz(cha, authz, acme, p, &notify_server))) {
        goto out;
    }
    
    rv = clmd_store_load(store, CLMD_SG_CHALLENGES, authz->domain, CLMD_FN_HTTP01,
                       CLMD_SV_TEXT, (void**)&data, p);
    if ((KUDA_SUCCESS == rv && strcmp(cha->key_authz, data)) || KUDA_STATUS_IS_ENOENT(rv)) {
        rv = clmd_store_save(store, p, CLMD_SG_CHALLENGES, authz->domain, CLMD_FN_HTTP01,
                           CLMD_SV_TEXT, (void*)cha->key_authz, 0);
        notify_server = 1;
    }
    
    if (KUDA_SUCCESS == rv && notify_server) {
        authz_req_ctx ctx;

        /* challenge is setup or was changed from previous data, tell ACME server
         * so it may (re)try verification */        
        authz_req_ctx_init(&ctx, acme, NULL, authz, p);
        ctx.challenge = cha;
        rv = clmd_acme_POST(acme, cha->uri, on_init_authz_resp, authz_http_set, NULL, NULL, &ctx);
    }
out:
    return rv;
}

static kuda_status_t cha_tls_alpn_01_setup(clmd_acme_authz_cha_t *cha, clmd_acme_authz_t *authz, 
                                          clmd_acme_t *acme, clmd_store_t *store, 
                                          clmd_pkey_spec_t *key_spec,  
                                          kuda_array_header_t *acme_tls_1_domains, 
                                          kuda_table_t *env, kuda_pool_t *p)
{
    clmd_cert_t *cha_cert;
    clmd_pkey_t *cha_key;
    const char *acme_id, *token;
    kuda_status_t rv;
    int notify_server;
    clmd_data data;
    
    (void)env;
    if (clmd_array_str_index(acme_tls_1_domains, authz->domain, 0, 0) < 0) {
        rv = KUDA_ENOTIMPL;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, 
                      "%s: protocol 'acme-tls/1' not enabled for this domain.", 
                      authz->domain);
        goto out;
    }
    if (KUDA_SUCCESS != (rv = setup_key_authz(cha, authz, acme, p, &notify_server))) {
        goto out;
    }
    rv = clmd_store_load(store, CLMD_SG_CHALLENGES, authz->domain, CLMD_FN_TLSALPN01_CERT,
                       CLMD_SV_CERT, (void**)&cha_cert, p);
    if ((KUDA_SUCCESS == rv && !clmd_cert_covers_domain(cha_cert, authz->domain)) 
        || KUDA_STATUS_IS_ENOENT(rv)) {
        
        if (KUDA_SUCCESS != (rv = clmd_pkey_gen(&cha_key, p, key_spec))) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, p, "%s: create tls-alpn-01 challenge key",
                          authz->domain);
            goto out;
        }

        /* Create a "tls-alpn-01" certificate for the domain we want to authenticate.
         * The server will need to answer a TLS connection with SNI == authz->domain
         * and ALPN procotol "acme-tls/1" with this certificate.
         */
        CLMD_DATA_SET_STR(&data, cha->key_authz);
        rv = clmd_crypt_sha256_digest_hex(&token, p, &data);
        if (KUDA_SUCCESS != rv) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, p, "%s: create tls-alpn-01 cert",
                          authz->domain);
            goto out;
        }
        
        acme_id = kuda_psprintf(p, "critical,DER:04:20:%s", token);
        if (KUDA_SUCCESS != (rv = clmd_cert_make_tls_alpn_01(&cha_cert, authz->domain, acme_id, cha_key, 
                                            kuda_time_from_sec(7 * CLMD_SECS_PER_DAY), p))) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, p, "%s: create tls-alpn-01 cert",
                          authz->domain);
            goto out;
        }
        
        if (KUDA_SUCCESS == (rv = clmd_store_save(store, p, CLMD_SG_CHALLENGES, authz->domain, CLMD_FN_TLSALPN01_PKEY,
                                CLMD_SV_PKEY, (void*)cha_key, 0))) {
            rv = clmd_store_save(store, p, CLMD_SG_CHALLENGES, authz->domain, CLMD_FN_TLSALPN01_CERT,
                               CLMD_SV_CERT, (void*)cha_cert, 0);
        }
        notify_server = 1;
    }
    
    if (KUDA_SUCCESS == rv && notify_server) {
        authz_req_ctx ctx;

        /* challenge is setup or was changed from previous data, tell ACME server
         * so it may (re)try verification */        
        authz_req_ctx_init(&ctx, acme, NULL, authz, p);
        ctx.challenge = cha;
        rv = clmd_acme_POST(acme, cha->uri, on_init_authz_resp, authz_http_set, NULL, NULL, &ctx);
    }
out:    
    return rv;
}

static kuda_status_t cha_dns_01_setup(clmd_acme_authz_cha_t *cha, clmd_acme_authz_t *authz, 
                                     clmd_acme_t *acme, clmd_store_t *store, 
                                     clmd_pkey_spec_t *key_spec, 
                                     kuda_array_header_t *acme_tls_1_domains, 
                                     kuda_table_t *env, kuda_pool_t *p)
{
    const char *token;
    const char * const *argv;
    const char *cmdline, *dns01_cmd;
    kuda_status_t rv;
    int exit_code, notify_server;
    authz_req_ctx ctx;
    clmd_data data;
    
    (void)store;
    (void)key_spec;
    (void)acme_tls_1_domains;
    
    dns01_cmd = kuda_table_get(env, CLMD_KEY_CMD_DNS01);
    if (!dns01_cmd) {
        rv = KUDA_ENOTIMPL;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, "%s: dns-01 command not set", 
                      authz->domain);
        goto out;
    }
    
    if (KUDA_SUCCESS != (rv = setup_key_authz(cha, authz, acme, p, &notify_server))) {
        goto out;
    }
    
    CLMD_DATA_SET_STR(&data, cha->key_authz);
    rv = clmd_crypt_sha256_digest64(&token, p, &data);
    if (KUDA_SUCCESS != rv) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, p, "%s: create dns-01 token",
                      authz->domain);
        goto out;
    }

    cmdline = kuda_psprintf(p, "%s setup %s %s", dns01_cmd, authz->domain, token); 
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, p, 
                  "%s: dns-01 setup command: %s", authz->domain, cmdline);
    kuda_tokenize_to_argv(cmdline, (char***)&argv, p);
    if (KUDA_SUCCESS != (rv = clmd_util_exec(p, argv[0], argv, &exit_code))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, rv, p, 
                      "%s: dns-01 setup command failed to execute", authz->domain);
        goto out;
    }
    if (exit_code) {
        rv = KUDA_EGENERAL;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, rv, p, 
                      "%s: dns-01 setup command returns %d", authz->domain, exit_code);
        goto out;
    }
    
    /* challenge is setup, tell ACME server so it may (re)try verification */        
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, "%s: dns-01 setup succeeded", authz->domain);
    authz_req_ctx_init(&ctx, acme, NULL, authz, p);
    ctx.challenge = cha;
    rv = clmd_acme_POST(acme, cha->uri, on_init_authz_resp, authz_http_set, NULL, NULL, &ctx);
    
out:    
    return rv;
}

static kuda_status_t cha_dns_01_teardown(clmd_store_t *store, const char *domain, 
                                        kuda_table_t *env, kuda_pool_t *p)
{
    const char * const *argv;
    const char *cmdline, *dns01_cmd;
    kuda_status_t rv;
    int exit_code;
    
    (void)store;
    
    dns01_cmd = kuda_table_get(env, CLMD_KEY_CMD_DNS01);
    if (!dns01_cmd) {
        rv = KUDA_ENOTIMPL;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, p, "%s: dns-01 command not set", domain);
        goto out;
    }
    
    cmdline = kuda_psprintf(p, "%s teardown %s", dns01_cmd, domain); 
    kuda_tokenize_to_argv(cmdline, (char***)&argv, p);
    if (KUDA_SUCCESS != (rv = clmd_util_exec(p, argv[0], argv, &exit_code)) || exit_code) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, rv, p, 
                      "%s: dns-01 teardown command failed (exit code=%d)",
                      domain, exit_code);
    }
out:    
    return rv;
}

static kuda_status_t cha_teardown_dir(clmd_store_t *store, const char *domain, 
                                     kuda_table_t *env, kuda_pool_t *p)
{
    (void)env;
    return clmd_store_purge(store, p, CLMD_SG_CHALLENGES, domain);
}

typedef kuda_status_t cha_setup(clmd_acme_authz_cha_t *cha, clmd_acme_authz_t *authz, 
                               clmd_acme_t *acme, clmd_store_t *store, 
                               clmd_pkey_spec_t *key_spec, 
                               kuda_array_header_t *acme_tls_1_domains, 
                               kuda_table_t *env, kuda_pool_t *p);
                               
typedef kuda_status_t cha_teardown(clmd_store_t *store, const char *domain, 
                                  kuda_table_t *env, kuda_pool_t *p);
                                 
typedef struct {
    const char *name;
    cha_setup *setup;
    cha_teardown *teardown;
} cha_type;

static const cha_type CHA_TYPES[] = {
    { CLMD_AUTHZ_TYPE_HTTP01,     cha_http_01_setup,      cha_teardown_dir },
    { CLMD_AUTHZ_TYPE_TLSALPN01,  cha_tls_alpn_01_setup,  cha_teardown_dir },
    { CLMD_AUTHZ_TYPE_DNS01,      cha_dns_01_setup,       cha_dns_01_teardown },
};
static const kuda_size_t CHA_TYPES_LEN = (sizeof(CHA_TYPES)/sizeof(CHA_TYPES[0]));

typedef struct {
    kuda_pool_t *p;
    const char *type;
    clmd_acme_authz_cha_t *accepted;
    kuda_array_header_t *offered;
} cha_find_ctx;

static kuda_status_t collect_offered(void *baton, size_t index, clmd_json_t *json)
{
    cha_find_ctx *ctx = baton;
    const char *ctype;
    
    (void)index;
    if ((ctype = clmd_json_gets(json, CLMD_KEY_TYPE, NULL))) {
        KUDA_ARRAY_PUSH(ctx->offered, const char*) = kuda_pstrdup(ctx->p, ctype);
    }
    return 1;
}

static kuda_status_t find_type(void *baton, size_t index, clmd_json_t *json)
{
    cha_find_ctx *ctx = baton;
    
    const char *ctype = clmd_json_gets(json, CLMD_KEY_TYPE, NULL);
    if (ctype && !kuda_strnatcasecmp(ctx->type, ctype)) {
        ctx->accepted = cha_from_json(ctx->p, index, json);
        return 0;
    }
    return 1;
}

kuda_status_t clmd_acme_authz_respond(clmd_acme_authz_t *authz, clmd_acme_t *acme, clmd_store_t *store, 
                                   kuda_array_header_t *challenges, clmd_pkey_spec_t *key_spec,
                                   kuda_array_header_t *acme_tls_1_domains, 
                                   kuda_table_t *env, kuda_pool_t *p, const char **psetup_token,
                                   clmd_result_t *result)
{
    kuda_status_t rv;
    int i;
    cha_find_ctx fctx;
    const char *challenge_setup;
    
    assert(acme);
    assert(authz);
    assert(authz->resource);

    fctx.p = p;
    fctx.accepted = NULL;
    
    /* Look in the order challenge types are defined:
     * - if they are offered by the CA, try to set it up
     * - if setup was successful, we are done and the CA will evaluate us
     * - if setup failed, continue to look for another supported challenge type
     * - if there is no overlap in types, tell the user that she has to configure
     *   either more types (dns, tls-alpn-01), make ports available or refrain
     *   from useing wildcard domains when dns is not available. etc.
     * - if there was an overlap, but no setup was successfull, report that. We
     *   will retry this, maybe the failure is temporary (e.g. command to setup DNS
     */
    rv = KUDA_ENOTIMPL;
    challenge_setup = NULL;
    for (i = 0; i < challenges->nelts && !fctx.accepted; ++i) {
        fctx.type = KUDA_ARRAY_IDX(challenges, i, const char *);
        clmd_json_itera(find_type, &fctx, authz->resource, CLMD_KEY_CHALLENGES, NULL);

        if (fctx.accepted) {
            for (i = 0; i < (int)CHA_TYPES_LEN; ++i) {
                if (!kuda_strnatcasecmp(CHA_TYPES[i].name, fctx.accepted->type)) {
                    clmd_result_activity_printf(result, "Setting up challenge '%s' for domain %s", 
                                              fctx.accepted->type, authz->domain);
                    rv = CHA_TYPES[i].setup(fctx.accepted, authz, acme, store, key_spec, 
                                            acme_tls_1_domains, env, p);
                    if (KUDA_SUCCESS == rv) {
                        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, rv, p, 
                                      "%s: set up challenge '%s'", 
                                      authz->domain, fctx.accepted->type);
                        challenge_setup = CHA_TYPES[i].name; 
                        goto out;
                    }
                    clmd_result_printf(result, rv, "error setting up challenge '%s', "
                                     "for domain %s, looking for other option",
                                     fctx.accepted->type, authz->domain);
                    clmd_result_log(result, CLMD_LOG_INFO);
                }
            }
        }
    }
    
out:
    *psetup_token = (KUDA_SUCCESS == rv)? kuda_psprintf(p, "%s:%s", challenge_setup, authz->domain) : NULL;
    if (!fctx.accepted || KUDA_ENOTIMPL == rv) {
        rv = KUDA_EINVAL;
        fctx.offered = kuda_array_make(p, 5, sizeof(const char*));
        clmd_json_itera(collect_offered, &fctx, authz->resource, CLMD_KEY_CHALLENGES, NULL);
        clmd_result_printf(result, rv, "None of offered challenge types for domain %s are supported. "
                      "The server offered '%s' and available are: '%s'.",
                      authz->domain, 
                      kuda_array_pstrcat(p, fctx.offered, ' '),
                      kuda_array_pstrcat(p, challenges, ' '));
        result->problem = "challenge-mismatch";
        clmd_result_log(result, CLMD_LOG_ERR);
    }
    else if (KUDA_SUCCESS != rv) {
        fctx.offered = kuda_array_make(p, 5, sizeof(const char*));
        clmd_json_itera(collect_offered, &fctx, authz->resource, CLMD_KEY_CHALLENGES, NULL);
        clmd_result_printf(result, rv, "None of the offered challenge types %s offered "
                         "for domain %s could be setup successfully. Please check the "
                         "log for errors.", authz->domain, 
                         kuda_array_pstrcat(p, fctx.offered, ' '));
        result->problem = "challenge-setup-failure";
        clmd_result_log(result, CLMD_LOG_ERR);
    }
    return rv;
}

kuda_status_t clmd_acme_authz_teardown(struct clmd_store_t *store, 
                                    const char *token, kuda_table_t *env, kuda_pool_t *p)
{
    char *challenge, *domain;
    int i;
    
    if (strchr(token, ':')) {
        challenge = kuda_pstrdup(p, token);
        domain = strchr(challenge, ':');
        *domain = '\0'; domain++;
        for (i = 0; i < (int)CHA_TYPES_LEN; ++i) {
            if (!kuda_strnatcasecmp(CHA_TYPES[i].name, challenge)) {
                if (CHA_TYPES[i].teardown) {
                    return CHA_TYPES[i].teardown(store, domain, env, p);
                }
                break;
            }
        }
    }
    return KUDA_SUCCESS;
}

