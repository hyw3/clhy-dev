/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_result_h
#define capi_clmd_clmd_result_h

struct clmd_json_t;
struct clmd_t;

typedef struct clmd_result_t clmd_result_t;

typedef void clmd_result_change_cb(clmd_result_t *result, void *data);

struct clmd_result_t {
    kuda_pool_t *p;
    const struct clmd_t *clmd;
    kuda_status_t status;
    const char *problem;
    const char *detail;
    const char *activity;
    kuda_time_t ready_at;
    clmd_result_change_cb *on_change;
    void *on_change_data;
};

clmd_result_t *clmd_result_make(kuda_pool_t *p, kuda_status_t status);
clmd_result_t *clmd_result_clmd_make(kuda_pool_t *p, const struct clmd_t *clmd);
void clmd_result_reset(clmd_result_t *result);

void clmd_result_activity_set(clmd_result_t *result, const char *activity);
void clmd_result_activity_setn(clmd_result_t *result, const char *activity);
void clmd_result_activity_printf(clmd_result_t *result, const char *fmt, ...);

void clmd_result_set(clmd_result_t *result, kuda_status_t status, const char *detail);
void clmd_result_problem_set(clmd_result_t *result, kuda_status_t status, 
                           const char *problem, const char *detail);
void clmd_result_problem_printf(clmd_result_t *result, kuda_status_t status,
                              const char *problem, const char *fmt, ...);

#define CLMD_RESULT_LOG_ID(logno)       "urn:org:clhy:wwhy:log:"logno

void clmd_result_printf(clmd_result_t *result, kuda_status_t status, const char *fmt, ...);

void clmd_result_delay_set(clmd_result_t *result, kuda_time_t ready_at);

clmd_result_t*clmd_result_from_json(const struct clmd_json_t *json, kuda_pool_t *p);
struct clmd_json_t *clmd_result_to_json(const clmd_result_t *result, kuda_pool_t *p);

int clmd_result_cmp(const clmd_result_t *r1, const clmd_result_t *r2);

void clmd_result_assign(clmd_result_t *dest, const clmd_result_t *src);
void clmd_result_dup(clmd_result_t *dest, const clmd_result_t *src);

void clmd_result_log(clmd_result_t *result, int level);

void clmd_result_on_change(clmd_result_t *result, clmd_result_change_cb *cb, void *data);

#endif /* capi_clmd_clmd_result_h */
