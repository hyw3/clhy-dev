/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_config_h
#define capi_clmd_clmd_config_h

struct kuda_hash_t;
struct clmd_store_t;
struct clmd_reg_t;
struct clmd_pkey_spec_t;

typedef enum {
    CLMD_CONFIG_CA_URL,
    CLMD_CONFIG_CA_PROTO,
    CLMD_CONFIG_BASE_DIR,
    CLMD_CONFIG_CA_AGREEMENT,
    CLMD_CONFIG_DRIVE_MODE,
    CLMD_CONFIG_LOCAL_80,
    CLMD_CONFIG_LOCAL_443,
    CLMD_CONFIG_RENEW_WINDOW,
    CLMD_CONFIG_WARN_WINDOW,
    CLMD_CONFIG_TRANSITIVE,
    CLMD_CONFIG_PROXY,
    CLMD_CONFIG_REQUIRE_HTTPS,
    CLMD_CONFIG_MUST_STAPLE,
    CLMD_CONFIG_NOTIFY_CMD,
    CLMD_CONFIG_MESSGE_CMD,
} clmd_config_var_t;

typedef struct clmd_capi_conf_t clmd_capi_conf_t;
struct clmd_capi_conf_t {
    kuda_array_header_t *mds;           /* all clmd_t* defined in the config, shared */
    const char *base_dir;              /* base dir for store */
    const char *proxy_url;             /* proxy url to use (or NULL) */
    struct clmd_reg_t *reg;              /* clmd registry instance, singleton, shared */

    int local_80;                      /* On which port http:80 arrives */
    int local_443;                     /* On which port https:443 arrives */
    int can_http;                      /* Does someone listen to the local port 80 equivalent? */
    int can_https;                     /* Does someone listen to the local port 443 equivalent? */
    int manage_base_server;            /* If base server outside vhost may be managed */
    int hsts_max_age;                  /* max-age of HSTS (rfc6797) header */
    const char *hsts_header;           /* computed HTST header to use or NULL */
    kuda_array_header_t *unused_names;  /* post config, names of all CLMDs not assigned to a vhost */
    kuda_array_header_t *watched_names; /* post config, names of all CLMDs that we need to watch */
    struct kuda_hash_t *init_errors;    /* init errors reported with CLMD name as key */

    const char *notify_cmd;            /* notification command to execute on signup/renew */
    const char *message_cmd;           /* message command to execute on signup/renew/warnings */
    struct kuda_table_t *env;           /* environment for operation */
    int dry_run;                       /* != 0 iff config dry run */
    int server_status_enabled;         /* if cAPI should add to server-status handler */
    int certificate_status_enabled;    /* if cAPI should expose /.wwhy/certificate-status */
};

typedef struct clmd_srv_conf_t {
    const char *name;
    const server_rec *s;               /* server this config belongs to */
    clmd_capi_conf_t *mc;                 /* global config settings */
    
    int transitive;                    /* != 0 iff VirtualHost names/aliases are auto-added */
    clmd_require_t require_https;        /* If CLMDs require https: access */
    int renew_mode;                    /* mode of obtaining credentials */
    int must_staple;                   /* certificates should set the OCSP Must Staple extension */
    struct clmd_pkey_spec_t *pkey_spec;  /* specification for generating private keys */
    const clmd_timeslice_t *renew_window; /* time before expiration that starts renewal */
    const clmd_timeslice_t *warn_window;  /* time before expiration that warning are sent out */
    
    const char *ca_url;                /* url of CA certificate service */
    const char *ca_proto;              /* protocol used vs CA (e.g. ACME) */
    const char *ca_agreement;          /* accepted agreement uri between CA and user */ 
    struct kuda_array_header_t *ca_challenges; /* challenge types configured */

    clmd_t *current;                     /* clmd currently defined in <MDomainSet xxx> section */
    clmd_t *assigned;                    /* post_config: CLMD that applies to this server or NULL */
} clmd_srv_conf_t;

void *clmd_config_create_svr(kuda_pool_t *pool, server_rec *s);
void *clmd_config_merge_svr(kuda_pool_t *pool, void *basev, void *addv);

extern const command_rec clmd_cmds[];

kuda_status_t clmd_config_post_config(server_rec *s, kuda_pool_t *p);

/* Get the effective clmd configuration for the connection */
clmd_srv_conf_t *clmd_config_cget(conn_rec *c);
/* Get the effective clmd configuration for the server */
clmd_srv_conf_t *clmd_config_get(server_rec *s);
/* Get the effective clmd configuration for the server, but make it
 * unique to this server_rec, so that any changes only affect this server */
clmd_srv_conf_t *clmd_config_get_unique(server_rec *s, kuda_pool_t *p);

const char *clmd_config_gets(const clmd_srv_conf_t *config, clmd_config_var_t var);
int clmd_config_geti(const clmd_srv_conf_t *config, clmd_config_var_t var);

void clmd_config_get_timespan(const clmd_timeslice_t **pspan, const clmd_srv_conf_t *sc, clmd_config_var_t var);


#endif /* clmd_config_h */
