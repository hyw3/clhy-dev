/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_store_h
#define capi_clmd_clmd_store_h

struct kuda_array_header_t;
struct clmd_cert_t;
struct clmd_pkey_t;

typedef struct clmd_store_t clmd_store_t;

typedef void clmd_store_destroy_cb(clmd_store_t *store);

const char *clmd_store_group_name(int group);


typedef kuda_status_t clmd_store_load_cb(clmd_store_t *store, clmd_store_group_t group, 
                                      const char *name, const char *aspect, 
                                      clmd_store_vtype_t vtype, void **pvalue, 
                                      kuda_pool_t *p);
typedef kuda_status_t clmd_store_save_cb(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group, 
                                      const char *name, const char *aspect, 
                                      clmd_store_vtype_t vtype, void *value, 
                                      int create);
typedef kuda_status_t clmd_store_remove_cb(clmd_store_t *store, clmd_store_group_t group, 
                                        const char *name, const char *aspect,  
                                        kuda_pool_t *p, int force);
typedef kuda_status_t clmd_store_purge_cb(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group, 
                                        const char *name);

typedef int clmd_store_inspect(void *baton, const char *name, const char *aspect, 
                             clmd_store_vtype_t vtype, void *value, kuda_pool_t *ptemp);

typedef kuda_status_t clmd_store_iter_cb(clmd_store_inspect *inspect, void *baton, clmd_store_t *store, 
                                      kuda_pool_t *p, clmd_store_group_t group, const char *pattern,
                                      const char *aspect, clmd_store_vtype_t vtype);

typedef kuda_status_t clmd_store_names_iter_cb(clmd_store_inspect *inspect, void *baton, clmd_store_t *store, 
                                            kuda_pool_t *p, clmd_store_group_t group, const char *pattern);

typedef kuda_status_t clmd_store_move_cb(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t from, 
                                      clmd_store_group_t to, const char *name, int archive);

typedef kuda_status_t clmd_store_get_fname_cb(const char **pfname, 
                                           clmd_store_t *store, clmd_store_group_t group, 
                                           const char *name, const char *aspect, 
                                           kuda_pool_t *p);

typedef int clmd_store_is_newer_cb(clmd_store_t *store, 
                                 clmd_store_group_t group1, clmd_store_group_t group2,  
                                 const char *name, const char *aspect, kuda_pool_t *p);

struct clmd_store_t {
    clmd_store_destroy_cb *destroy;

    clmd_store_save_cb *save;
    clmd_store_load_cb *load;
    clmd_store_remove_cb *remove;
    clmd_store_move_cb *move;
    clmd_store_iter_cb *iterate;
    clmd_store_names_iter_cb *iterate_names;
    clmd_store_purge_cb *purge;
    clmd_store_get_fname_cb *get_fname;
    clmd_store_is_newer_cb *is_newer;
};

void clmd_store_destroy(clmd_store_t *store);

kuda_status_t clmd_store_load_json(clmd_store_t *store, clmd_store_group_t group, 
                                const char *name, const char *aspect, 
                                struct clmd_json_t **pdata, kuda_pool_t *p);
kuda_status_t clmd_store_save_json(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group, 
                                const char *name, const char *aspect, 
                                struct clmd_json_t *data, int create);


kuda_status_t clmd_store_load(clmd_store_t *store, clmd_store_group_t group, 
                           const char *name, const char *aspect, 
                           clmd_store_vtype_t vtype, void **pdata, 
                           kuda_pool_t *p);
kuda_status_t clmd_store_save(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group, 
                           const char *name, const char *aspect, 
                           clmd_store_vtype_t vtype, void *data, 
                           int create);
kuda_status_t clmd_store_remove(clmd_store_t *store, clmd_store_group_t group, 
                             const char *name, const char *aspect, 
                             kuda_pool_t *p, int force);
kuda_status_t clmd_store_purge(clmd_store_t *store, kuda_pool_t *p, 
                            clmd_store_group_t group, const char *name);


kuda_status_t clmd_store_iter(clmd_store_inspect *inspect, void *baton, clmd_store_t *store, 
                           kuda_pool_t *p, clmd_store_group_t group, const char *pattern, 
                           const char *aspect, clmd_store_vtype_t vtype);

kuda_status_t clmd_store_move(clmd_store_t *store, kuda_pool_t *p,
                           clmd_store_group_t from, clmd_store_group_t to,
                           const char *name, int archive);

kuda_status_t clmd_store_get_fname(const char **pfname, 
                                clmd_store_t *store, clmd_store_group_t group, 
                                const char *name, const char *aspect, 
                                kuda_pool_t *p);

int clmd_store_is_newer(clmd_store_t *store, clmd_store_group_t group1, clmd_store_group_t group2,  
                      const char *name, const char *aspect, kuda_pool_t *p);

kuda_status_t clmd_store_iter_names(clmd_store_inspect *inspect, void *baton, clmd_store_t *store, 
                                 kuda_pool_t *p, clmd_store_group_t group, const char *pattern);


/**************************************************************************************************/
/* Storage handling utils */

kuda_status_t clmd_load(clmd_store_t *store, clmd_store_group_t group, 
                     const char *name, clmd_t **pmd, kuda_pool_t *p);
kuda_status_t clmd_save(struct clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group, 
                     clmd_t *clmd, int create);
kuda_status_t clmd_remove(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group, 
                     const char *name, int force);

int clmd_is_newer(clmd_store_t *store, clmd_store_group_t group1, clmd_store_group_t group2,  
                const char *name, kuda_pool_t *p);

typedef int clmd_store_clmd_inspect(void *baton, clmd_store_t *store, clmd_t *clmd, kuda_pool_t *ptemp);

kuda_status_t clmd_store_clmd_iter(clmd_store_clmd_inspect *inspect, void *baton, clmd_store_t *store, 
                              kuda_pool_t *p, clmd_store_group_t group, const char *pattern);


kuda_status_t clmd_pkey_load(clmd_store_t *store, clmd_store_group_t group, 
                          const char *name, struct clmd_pkey_t **ppkey, kuda_pool_t *p);
kuda_status_t clmd_pkey_save(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group, 
                          const char *name, struct clmd_pkey_t *pkey, int create);

kuda_status_t clmd_pubcert_load(clmd_store_t *store, clmd_store_group_t group, const char *name, 
                             struct kuda_array_header_t **ppubcert, kuda_pool_t *p);
kuda_status_t clmd_pubcert_save(clmd_store_t *store, kuda_pool_t *p, 
                             clmd_store_group_t group, const char *name, 
                             struct kuda_array_header_t *pubcert, int create);


#endif /* capi_clmd_clmd_store_h */
