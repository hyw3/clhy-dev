/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <stdio.h>

#include <kuda_lib.h>
#include <kuda_file_info.h>
#include <kuda_file_io.h>
#include <kuda_fnmatch.h>
#include <kuda_hash.h>
#include <kuda_strings.h>
#include <kuda_tables.h>

#include "clmd.h"
#include "clmd_crypt.h"
#include "clmd_json.h"
#include "clmd_jws.h"
#include "clmd_log.h"
#include "clmd_store.h"
#include "clmd_util.h"
#include "clmd_version.h"

#include "clmd_acme.h"
#include "clmd_acme_acct.h"

static kuda_status_t acct_make(clmd_acme_acct_t **pacct, kuda_pool_t *p, 
                              const char *ca_url, kuda_array_header_t *contacts)
{
    clmd_acme_acct_t *acct;
    
    acct = kuda_pcalloc(p, sizeof(*acct));

    acct->ca_url = ca_url;
    if (!contacts || kuda_is_empty_array(contacts)) {
        acct->contacts = kuda_array_make(p, 5, sizeof(const char *));
    }
    else {
        acct->contacts = kuda_array_copy(p, contacts);
    }
    
    *pacct = acct;
    return KUDA_SUCCESS;
}


static const char *mk_acct_id(kuda_pool_t *p, clmd_acme_t *acme, int i)
{
    return kuda_psprintf(p, "ACME-%s-%04d", acme->sname, i);
}

static const char *mk_acct_pattern(kuda_pool_t *p, clmd_acme_t *acme)
{
    return kuda_psprintf(p, "ACME-%s-*", acme->sname);
}
 
/**************************************************************************************************/
/* json load/save */

static clmd_acme_acct_st acct_st_from_str(const char *s) 
{
    if (s) {
        if (!strcmp("valid", s)) {
            return CLMD_ACME_ACCT_ST_VALID;
        }
        else if (!strcmp("deactivated", s)) {
            return CLMD_ACME_ACCT_ST_DEACTIVATED;
        }
        else if (!strcmp("revoked", s)) {
            return CLMD_ACME_ACCT_ST_REVOKED;
        }
    }
    return CLMD_ACME_ACCT_ST_UNKNOWN;
}

clmd_json_t *clmd_acme_acct_to_json(clmd_acme_acct_t *acct, kuda_pool_t *p)
{
    clmd_json_t *jacct;
    const char *s;

    assert(acct);
    jacct = clmd_json_create(p);
    switch (acct->status) {
        case CLMD_ACME_ACCT_ST_VALID:
            s = "valid";
            break;
        case CLMD_ACME_ACCT_ST_DEACTIVATED:
            s = "deactivated";
            break;
        case CLMD_ACME_ACCT_ST_REVOKED:
            s = "revoked";
            break;
        default:
            s = NULL;
            break;
    }    
    if (s) {
        clmd_json_sets(s, jacct, CLMD_KEY_STATUS, NULL);
    }
    clmd_json_sets(acct->url, jacct, CLMD_KEY_URL, NULL);
    clmd_json_sets(acct->ca_url, jacct, CLMD_KEY_CA_URL, NULL);
    clmd_json_setsa(acct->contacts, jacct, CLMD_KEY_CONTACT, NULL);
    clmd_json_setj(acct->registration, jacct, CLMD_KEY_REGISTRATION, NULL);
    if (acct->agreement) {
        clmd_json_sets(acct->agreement, jacct, CLMD_KEY_AGREEMENT, NULL);
    }
    if (acct->orders) {
        clmd_json_sets(acct->orders, jacct, CLMD_KEY_ORDERS, NULL);
    }
    
    return jacct;
}

kuda_status_t clmd_acme_acct_from_json(clmd_acme_acct_t **pacct, clmd_json_t *json, kuda_pool_t *p)
{
    kuda_status_t rv = KUDA_EINVAL;
    clmd_acme_acct_t *acct;
    clmd_acme_acct_st status = CLMD_ACME_ACCT_ST_UNKNOWN;
    const char *ca_url, *url;
    kuda_array_header_t *contacts;
    
    if (clmd_json_has_key(json, CLMD_KEY_STATUS, NULL)) {
        status = acct_st_from_str(clmd_json_gets(json, CLMD_KEY_STATUS, NULL));
    }
    else {
        /* old accounts only had disabled boolean field */
        status = clmd_json_getb(json, CLMD_KEY_DISABLED, NULL)? 
            CLMD_ACME_ACCT_ST_DEACTIVATED : CLMD_ACME_ACCT_ST_VALID;
    }
    
    url = clmd_json_gets(json, CLMD_KEY_URL, NULL);
    if (!url) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, p, "account has no url");
        goto out;
    }

    ca_url = clmd_json_gets(json, CLMD_KEY_CA_URL, NULL);
    if (!ca_url) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, p, "account has no CA url: %s", url);
        goto out;
    }
    
    contacts = kuda_array_make(p, 5, sizeof(const char *));
    if (clmd_json_has_key(json, CLMD_KEY_CONTACT, NULL)) {
        clmd_json_getsa(contacts, json, CLMD_KEY_CONTACT, NULL);
    }
    else {
        clmd_json_getsa(contacts, json, CLMD_KEY_REGISTRATION, CLMD_KEY_CONTACT, NULL);
    }
    rv = acct_make(&acct, p, ca_url, contacts);
    if (KUDA_SUCCESS == rv) {
        acct->status = status;
        acct->url = url;
        acct->agreement = clmd_json_gets(json, "terms-of-service", NULL);
        acct->orders = clmd_json_gets(json, CLMD_KEY_ORDERS, NULL);
    }

out:
    *pacct = (KUDA_SUCCESS == rv)? acct : NULL;
    return rv;
}

kuda_status_t clmd_acme_acct_save(clmd_store_t *store, kuda_pool_t *p, clmd_acme_t *acme, 
                               const char **pid, clmd_acme_acct_t *acct, clmd_pkey_t *acct_key)
{
    clmd_json_t *jacct;
    kuda_status_t rv;
    int i;
    const char *id = pid? *pid : NULL;
    
    jacct = clmd_acme_acct_to_json(acct, p);
    if (id) {
        rv = clmd_store_save(store, p, CLMD_SG_ACCOUNTS, id, CLMD_FN_ACCOUNT, CLMD_SV_JSON, jacct, 0);
    }
    else {
        rv = KUDA_EAGAIN;
        for (i = 0; i < 1000 && KUDA_SUCCESS != rv; ++i) {
            id = mk_acct_id(p, acme, i);
            rv = clmd_store_save(store, p, CLMD_SG_ACCOUNTS, id, CLMD_FN_ACCOUNT, CLMD_SV_JSON, jacct, 1);
        }
    }
    if (KUDA_SUCCESS == rv) {
        if (pid) *pid = id;
        rv = clmd_store_save(store, p, CLMD_SG_ACCOUNTS, id, CLMD_FN_ACCT_KEY, CLMD_SV_PKEY, acct_key, 0);
    }
    return rv;
}

kuda_status_t clmd_acme_acct_load(clmd_acme_acct_t **pacct, clmd_pkey_t **ppkey,
                               clmd_store_t *store, clmd_store_group_t group, 
                               const char *name, kuda_pool_t *p)
{
    clmd_json_t *json;
    kuda_status_t rv;

    rv = clmd_store_load_json(store, group, name, CLMD_FN_ACCOUNT, &json, p);
    if (KUDA_STATUS_IS_ENOENT(rv)) {
        goto out;
    }
    if (KUDA_SUCCESS != rv) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, "error reading account: %s", name);
        goto out;
    }
    
    rv = clmd_acme_acct_from_json(pacct, json, p);
    if (KUDA_SUCCESS == rv) {
        rv = clmd_store_load(store, group, name, CLMD_FN_ACCT_KEY, CLMD_SV_PKEY, (void**)ppkey, p);
        if (KUDA_SUCCESS != rv) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, "loading key: %s", name);
            goto out;
        }
    }
out:
    if (KUDA_SUCCESS != rv) {
        *pacct = NULL;
        *ppkey = NULL;
    } 
    return rv;
}

/**************************************************************************************************/
/* Lookup */

typedef struct {
    kuda_pool_t *p;
    clmd_acme_t *acme;
    int url_match;
    const char *id;
} find_ctx;

static int find_acct(void *baton, const char *name, const char *aspect,
                     clmd_store_vtype_t vtype, void *value, kuda_pool_t *ptemp)
{
    find_ctx *ctx = baton;
    int disabled;
    const char *ca_url, *status;
    
    (void)aspect;
    (void)ptemp;
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, ctx->p, "account candidate %s/%s", name, aspect); 
    if (CLMD_SV_JSON == vtype) {
        clmd_json_t *json = value;
        
        status = clmd_json_gets(json, CLMD_KEY_STATUS, NULL);
        disabled = clmd_json_getb(json, CLMD_KEY_DISABLED, NULL);
        ca_url = clmd_json_gets(json, CLMD_KEY_CA_URL, NULL);
        
        if ((!status || !strcmp("valid", status)) && !disabled 
            && (!ctx->url_match || (ca_url && !strcmp(ctx->acme->url, ca_url)))) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, ctx->p, 
                          "found account %s for %s: %s, status=%s, disabled=%d, ca-url=%s", 
                          name, ctx->acme->url, aspect, status, disabled, ca_url);
            ctx->id = kuda_pstrdup(ctx->p, name);
            return 0;
        }
    }
    return 1;
}

static kuda_status_t acct_find(const char **pid, clmd_acme_acct_t **pacct, clmd_pkey_t **ppkey, 
                              clmd_store_t *store, clmd_store_group_t group,
                              const char *name_pattern, int url_match, 
                              clmd_acme_t *acme, kuda_pool_t *p)
{
    kuda_status_t rv;
    find_ctx ctx;
    
    ctx.p = p;
    ctx.acme = acme;
    ctx.id = NULL;
    ctx.url_match = url_match;
    *pid = NULL;
    
    rv = clmd_store_iter(find_acct, &ctx, store, p, group, name_pattern, CLMD_FN_ACCOUNT, CLMD_SV_JSON);
    if (ctx.id) {
        *pid = ctx.id;
        rv = clmd_acme_acct_load(pacct, ppkey, store, group, ctx.id, p);
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, "loading account %s", ctx.id);
    }
    else {
        *pacct = NULL;
        rv = KUDA_ENOENT;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, p, "acct_find: none found"); 
    }
    return rv;
}

static kuda_status_t acct_find_and_verify(clmd_store_t *store, clmd_store_group_t group, 
                                         const char *name_pattern, clmd_acme_t *acme, kuda_pool_t *p)
{
    clmd_acme_acct_t *acct;
    clmd_pkey_t *pkey;
    const char *id;
    kuda_status_t rv;

    if (KUDA_SUCCESS == (rv = acct_find(&id, &acct, &pkey, store, group, name_pattern, 1, acme, p))) {
        acme->acct_id = (CLMD_SG_STAGING == group)? NULL : id;
        acme->acct = acct;
        acme->acct_key = pkey;
        rv = clmd_acme_acct_validate(acme, NULL, p);
    
        if (KUDA_SUCCESS != rv) {
            acme->acct_id = NULL;
            acme->acct = NULL;
            acme->acct_key = NULL;
            if (KUDA_STATUS_IS_ENOENT(rv)) {
                /* verification failed and account has been disabled.
                   Indicate to caller that he may try again. */
                rv = KUDA_EAGAIN;
            }
        }
    }
    return rv;
}

kuda_status_t clmd_acme_find_acct(clmd_acme_t *acme, clmd_store_t *store)
{
    kuda_status_t rv;
    
    while (KUDA_EAGAIN == (rv = acct_find_and_verify(store, CLMD_SG_ACCOUNTS, 
                                                    mk_acct_pattern(acme->p, acme), 
                                                    acme, acme->p))) {
        /* nop */
    }
    
    if (KUDA_STATUS_IS_ENOENT(rv)) {
        /* No suitable account found in CLMD_SG_ACCOUNTS. Maybe a new account
         * can already be found in CLMD_SG_STAGING? */
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, acme->p, 
                      "no account found, looking in STAGING");
        while (KUDA_EAGAIN == (rv = acct_find_and_verify(store, CLMD_SG_STAGING, "*", 
                                                        acme, acme->p))) {
            /* nop */
        }
    }
    return rv;
}

typedef struct {
    kuda_pool_t *p;
    const char *url;
    const char *id;
} load_ctx;

static int id_by_url(void *baton, const char *name, const char *aspect,
                     clmd_store_vtype_t vtype, void *value, kuda_pool_t *ptemp)
{
    load_ctx *ctx = baton;
    int disabled;
    const char *acct_url, *status;
    
    (void)aspect;
    (void)ptemp;
    if (CLMD_SV_JSON == vtype) {
        clmd_json_t *json = value;
        
        status = clmd_json_gets(json, CLMD_KEY_STATUS, NULL);
        disabled = clmd_json_getb(json, CLMD_KEY_DISABLED, NULL);
        acct_url = clmd_json_gets(json, CLMD_KEY_URL, NULL);
        
        if ((!status || !strcmp("valid", status)) && !disabled 
            && acct_url && !strcmp(ctx->url, acct_url)) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, ctx->p, 
                          "found account %s for url %s: %s, status=%s, disabled=%d", 
                          name, ctx->url, aspect, status, disabled);
            ctx->id = kuda_pstrdup(ctx->p, name);
            return 0;
        }
    }
    return 1;
}

kuda_status_t clmd_acme_acct_id_for_url(const char **pid, clmd_store_t *store, 
                                     clmd_store_group_t group, const char *url, kuda_pool_t *p)
{
    kuda_status_t rv;
    load_ctx ctx;
    
    ctx.p = p;
    ctx.url = url;
    ctx.id = NULL;
    
    rv = clmd_store_iter(id_by_url, &ctx, store, p, group, "*", CLMD_FN_ACCOUNT, CLMD_SV_JSON);
    *pid = (KUDA_SUCCESS == rv)? ctx.id : NULL;
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, "acct_id_by_url %s -> %s", url, *pid);
    return rv;
}

/**************************************************************************************************/
/* acct operation context */
typedef struct {
    clmd_acme_t *acme;
    kuda_pool_t *p;
    const char *agreement;
} acct_ctx_t;

/**************************************************************************************************/
/* acct update */

static kuda_status_t on_init_acct_upd(clmd_acme_req_t *req, void *baton)
{
    clmd_json_t *jpayload;

    (void)baton;
    jpayload = clmd_json_create(req->p);
    switch (CLMD_ACME_VERSION_MAJOR(req->acme->version)) {
        case 1:
            clmd_json_sets("reg", jpayload, CLMD_KEY_RESOURCE, NULL);
            break;
        default:
            break;
    }
    return clmd_acme_req_body_init(req, jpayload);
} 

static kuda_status_t acct_upd(clmd_acme_t *acme, kuda_pool_t *p, 
                             const kuda_table_t *hdrs, clmd_json_t *body, void *baton)
{
    acct_ctx_t *ctx = baton;
    kuda_status_t rv = KUDA_SUCCESS;
    clmd_acme_acct_t *acct = acme->acct;
    
    if (!acct->url) {
        const char *location = kuda_table_get(hdrs, "location");
        if (!location) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, KUDA_EINVAL, p, "new acct without location");
            return KUDA_EINVAL;
        }
        acct->url = kuda_pstrdup(ctx->p, location);
    }
    
    kuda_array_clear(acct->contacts);
    clmd_json_dupsa(acct->contacts, acme->p, body, CLMD_KEY_CONTACT, NULL);
    if (clmd_json_has_key(body, CLMD_KEY_STATUS, NULL)) {
        acct->status = acct_st_from_str(clmd_json_gets(body, CLMD_KEY_STATUS, NULL));
    }
    if (clmd_json_has_key(body, CLMD_KEY_AGREEMENT, NULL)) {
        acct->agreement = clmd_json_dups(acme->p, body, CLMD_KEY_AGREEMENT, NULL);
    }
    if (clmd_json_has_key(body, CLMD_KEY_ORDERS, NULL)) {
        acct->orders = clmd_json_dups(acme->p, body, CLMD_KEY_ORDERS, NULL);
    }
    acct->registration = clmd_json_clone(ctx->p, body);
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, "updated acct %s", acct->url);
    return rv;
}

kuda_status_t clmd_acme_acct_update(clmd_acme_t *acme)
{
    acct_ctx_t ctx;
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, acme->p, "acct update");
    if (!acme->acct) {
        return KUDA_EINVAL;
    }
    ctx.acme = acme;
    ctx.p = acme->p;
    return clmd_acme_POST(acme, acme->acct->url, on_init_acct_upd, acct_upd, NULL, NULL, &ctx);
}

kuda_status_t clmd_acme_acct_validate(clmd_acme_t *acme, clmd_store_t *store, kuda_pool_t *p)
{
    kuda_status_t rv;
    
    if (KUDA_SUCCESS != (rv = clmd_acme_acct_update(acme))) {
        if (acme->acct && (KUDA_ENOENT == rv || KUDA_EACCES == rv)) {
            if (CLMD_ACME_ACCT_ST_VALID == acme->acct->status) {
                acme->acct->status = CLMD_ACME_ACCT_ST_UNKNOWN;
                if (store) {
                    clmd_acme_acct_save(store, p, acme, &acme->acct_id, acme->acct, acme->acct_key); 
                }
            }
            acme->acct = NULL;
            acme->acct_key = NULL;
            rv = KUDA_ENOENT;
        }
    }
    return rv;
}

/**************************************************************************************************/
/* Register a new account */

static kuda_status_t on_init_acct_new(clmd_acme_req_t *req, void *baton)
{
    acct_ctx_t *ctx = baton;
    clmd_json_t *jpayload;

    jpayload = clmd_json_create(req->p);
    
    switch (CLMD_ACME_VERSION_MAJOR(req->acme->version)) {
        case 1:
            clmd_json_sets("new-reg", jpayload, CLMD_KEY_RESOURCE, NULL);
            clmd_json_setsa(ctx->acme->acct->contacts, jpayload, CLMD_KEY_CONTACT, NULL);
            if (ctx->agreement) {
                clmd_json_sets(ctx->agreement, jpayload, CLMD_KEY_AGREEMENT, NULL);
            }
            break;
        default:
            clmd_json_setsa(ctx->acme->acct->contacts, jpayload, CLMD_KEY_CONTACT, NULL);
            if (ctx->agreement) {
                clmd_json_setb(1, jpayload, "termsOfServiceAgreed", NULL);
            }
        break;
    }
    
    return clmd_acme_req_body_init(req, jpayload);
} 

kuda_status_t clmd_acme_acct_register(clmd_acme_t *acme, clmd_store_t *store, kuda_pool_t *p, 
                                   kuda_array_header_t *contacts, const char *agreement)
{
    kuda_status_t rv;
    clmd_pkey_t *pkey;
    const char *err = NULL, *uri;
    clmd_pkey_spec_t spec;
    int i;
    acct_ctx_t ctx;
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, p, "create new account");
    
    ctx.acme = acme;
    ctx.p = p;
    /* The agreement URL is submitted when the ACME server announces Terms-of-Service
     * in its directory meta data. The magic value "accepted" will always use the
     * advertised URL. */
    ctx.agreement = NULL;
    if (acme->ca_agreement && agreement) {
        ctx.agreement = !strcmp("accepted", agreement)? acme->ca_agreement : agreement;
    }
    
    if (ctx.agreement) {
        if (KUDA_SUCCESS != (rv = clmd_util_abs_uri_check(acme->p, ctx.agreement, &err))) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, 0, p, 
                          "invalid agreement uri (%s): %s", err, ctx.agreement);
            goto out;
        }
    }
    
    for (i = 0; i < contacts->nelts; ++i) {
        uri = KUDA_ARRAY_IDX(contacts, i, const char *);
        if (KUDA_SUCCESS != (rv = clmd_util_abs_uri_check(acme->p, uri, &err))) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, 0, p, 
                          "invalid contact uri (%s): %s", err, uri);
            goto out;
        }
    }
    
    /* If there is no key selected yet, try to find an existing one for the same host. 
     * Let's Encrypt identifies accounts by their key for their ACMEv1 and v2 services.
     * Although the account appears on both services with different urls, it is 
     * internally the same one.
     * I think this is beneficial if someone migrates from ACMEv1 to v2 and not a leak
     * of identifying information.
     */
    if (!acme->acct_key) {
        find_ctx fctx;
    
        fctx.p = p;
        fctx.acme = acme;
        fctx.id = NULL;
        fctx.url_match = 0;
        
        clmd_store_iter(find_acct, &fctx, store, p, CLMD_SG_ACCOUNTS, 
                      mk_acct_pattern(p, acme), CLMD_FN_ACCOUNT, CLMD_SV_JSON);
        if (fctx.id) {
            rv = clmd_store_load(store, CLMD_SG_ACCOUNTS, fctx.id, CLMD_FN_ACCT_KEY, CLMD_SV_PKEY, 
                               (void**)&acme->acct_key, p);
            if (KUDA_SUCCESS == rv) {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, "reusing key from account %s", fctx.id);
            }
            else {
                acme->acct_key = NULL;
            }
        }
    }
    
    /* If we still have no key, generate a new one */
    if (!acme->acct_key) {
        spec.type = CLMD_PKEY_TYPE_RSA;
        spec.params.rsa.bits = CLMD_ACME_ACCT_PKEY_BITS;
        
        if (KUDA_SUCCESS != (rv = clmd_pkey_gen(&pkey, acme->p, &spec))) goto out;
        acme->acct_key = pkey;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, "created new account key");
    }
    
    if (KUDA_SUCCESS != (rv = acct_make(&acme->acct,  p, acme->url, contacts))) goto out;
    rv = clmd_acme_POST_new_account(acme,  on_init_acct_new, acct_upd, NULL, NULL, &ctx);
    if (KUDA_SUCCESS == rv) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, 0, p, 
                      "registered new account %s", acme->acct->url);
    }

out:    
    if (KUDA_SUCCESS != rv && acme->acct) {
        acme->acct = NULL;
    }
    return rv;
}

/**************************************************************************************************/
/* Deactivate the account */

static kuda_status_t on_init_acct_del(clmd_acme_req_t *req, void *baton)
{
    clmd_json_t *jpayload;

    (void)baton;
    jpayload = clmd_json_create(req->p);
    switch (CLMD_ACME_VERSION_MAJOR(req->acme->version)) {
        case 1:
            clmd_json_sets("reg", jpayload, CLMD_KEY_RESOURCE, NULL);
            clmd_json_setb(1, jpayload, "delete", NULL);
            break;
        default:
            clmd_json_sets("deactivated", jpayload, CLMD_KEY_STATUS, NULL);
            break;
    }
    return clmd_acme_req_body_init(req, jpayload);
} 

kuda_status_t clmd_acme_acct_deactivate(clmd_acme_t *acme, kuda_pool_t *p)
{
    clmd_acme_acct_t *acct = acme->acct;
    acct_ctx_t ctx;
    
    (void)p;
    if (!acct) {
        return KUDA_EINVAL;
    }
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, acme->p, "delete account %s from %s", 
                  acct->url, acct->ca_url);
    ctx.acme = acme;
    ctx.p = p;
    return clmd_acme_POST(acme, acct->url, on_init_acct_del, acct_upd, NULL, NULL, &ctx);
}

/**************************************************************************************************/
/* terms-of-service */

static kuda_status_t on_init_agree_tos(clmd_acme_req_t *req, void *baton)
{
    acct_ctx_t *ctx = baton;
    clmd_json_t *jpayload;

    jpayload = clmd_json_create(req->p);
    switch (CLMD_ACME_VERSION_MAJOR(req->acme->version)) {
        case 1:
            clmd_json_sets("reg", jpayload, CLMD_KEY_RESOURCE, NULL);
            clmd_json_sets(ctx->acme->acct->agreement, jpayload, CLMD_KEY_AGREEMENT, NULL);
            break;
        default:
            if (ctx->acme->acct->agreement) {
                clmd_json_setb(1, jpayload, "termsOfServiceAgreed", NULL);
            }
            break;
    }
    return clmd_acme_req_body_init(req, jpayload);
} 

kuda_status_t clmd_acme_agree(clmd_acme_t *acme, kuda_pool_t *p, const char *agreement)
{
    acct_ctx_t ctx;
    
    acme->acct->agreement = agreement;
    if (!strcmp("accepted", agreement) && acme->ca_agreement) {
        acme->acct->agreement = acme->ca_agreement;
    }
    
    ctx.acme = acme;
    ctx.p = p;
    return clmd_acme_POST(acme, acme->acct->url, on_init_agree_tos, acct_upd, NULL, NULL, &ctx);
}

kuda_status_t clmd_acme_check_agreement(clmd_acme_t *acme, kuda_pool_t *p, 
                                     const char *agreement, const char **prequired)
{
    kuda_status_t rv = KUDA_SUCCESS;
    
    /* We used to really check if the account agreement and the one indicated in meta
     * are the very same. However, LE is happy if the account has agreed to a ToS in 
     * the past and does not require a renewed acceptance.
     */
    *prequired = NULL;
    if (!acme->acct->agreement && acme->ca_agreement) {
        if (agreement) {
            rv = clmd_acme_agree(acme, p, acme->ca_agreement);
        }
        else {
            *prequired = acme->ca_agreement;
            rv = KUDA_INCOMPLETE;
        }
    }
    return rv;
}        
