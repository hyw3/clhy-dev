/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_json_h
#define capi_clmd_clmd_json_h

#include <kuda_file_io.h>

struct kuda_bucket_brigade;
struct kuda_file_t;

struct clmd_http_t;
struct clmd_http_response_t;


typedef struct clmd_json_t clmd_json_t;

typedef enum {
    CLMD_JSON_TYPE_OBJECT,
    CLMD_JSON_TYPE_ARRAY,
    CLMD_JSON_TYPE_STRING,
    CLMD_JSON_TYPE_REAL,
    CLMD_JSON_TYPE_INT,
    CLMD_JSON_TYPE_BOOL,
    CLMD_JSON_TYPE_NULL,
} clmd_json_type_t;


typedef enum {
    CLMD_JSON_FMT_COMPACT,
    CLMD_JSON_FMT_INDENT,
} clmd_json_fmt_t;

clmd_json_t *clmd_json_create(kuda_pool_t *pool);
void clmd_json_destroy(clmd_json_t *json);

clmd_json_t *clmd_json_copy(kuda_pool_t *pool, clmd_json_t *json);
clmd_json_t *clmd_json_clone(kuda_pool_t *pool, clmd_json_t *json);


int clmd_json_has_key(const clmd_json_t *json, ...);
int clmd_json_is(const clmd_json_type_t type, clmd_json_t *json, ...);

/* boolean manipulation */
int clmd_json_getb(const clmd_json_t *json, ...);
kuda_status_t clmd_json_setb(int value, clmd_json_t *json, ...);

/* number manipulation */
double clmd_json_getn(const clmd_json_t *json, ...);
kuda_status_t clmd_json_setn(double value, clmd_json_t *json, ...);

/* long manipulation */
long clmd_json_getl(const clmd_json_t *json, ...);
kuda_status_t clmd_json_setl(long value, clmd_json_t *json, ...);

/* string manipulation */
clmd_json_t *clmd_json_create_s(kuda_pool_t *pool, const char *s);
const char *clmd_json_gets(const clmd_json_t *json, ...);
const char *clmd_json_dups(kuda_pool_t *p, const clmd_json_t *json, ...);
kuda_status_t clmd_json_sets(const char *s, clmd_json_t *json, ...);

/* json manipulation */
clmd_json_t *clmd_json_getj(clmd_json_t *json, ...);
const clmd_json_t *clmd_json_getcj(const clmd_json_t *json, ...);
kuda_status_t clmd_json_setj(clmd_json_t *value, clmd_json_t *json, ...);
kuda_status_t clmd_json_addj(clmd_json_t *value, clmd_json_t *json, ...);
kuda_status_t clmd_json_insertj(clmd_json_t *value, size_t index, clmd_json_t *json, ...);

/* Array/Object manipulation */
kuda_status_t clmd_json_clr(clmd_json_t *json, ...);
kuda_status_t clmd_json_del(clmd_json_t *json, ...);

/* conversion function from and to json */
typedef kuda_status_t clmd_json_to_cb(void *value, clmd_json_t *json, kuda_pool_t *p, void *baton);
typedef kuda_status_t clmd_json_from_cb(void **pvalue, clmd_json_t *json, kuda_pool_t *p, void *baton);

/* identity pass through from json to json */
kuda_status_t clmd_json_pass_to(void *value, clmd_json_t *json, kuda_pool_t *p, void *baton);
kuda_status_t clmd_json_pass_from(void **pvalue, clmd_json_t *json, kuda_pool_t *p, void *baton);

/* conversions from json to json in specified pool */
kuda_status_t clmd_json_clone_to(void *value, clmd_json_t *json, kuda_pool_t *p, void *baton);
kuda_status_t clmd_json_clone_from(void **pvalue, clmd_json_t *json, kuda_pool_t *p, void *baton);

/* Manipulating/Iteration on generic Arrays */
kuda_status_t clmd_json_geta(kuda_array_header_t *a, clmd_json_from_cb *cb, 
                          void *baton, const clmd_json_t *json, ...);
kuda_status_t clmd_json_seta(kuda_array_header_t *a, clmd_json_to_cb *cb, 
                          void *baton, clmd_json_t *json, ...);

/* Called on each array element, aborts iteration when returning 0 */
typedef int clmd_json_itera_cb(void *baton, size_t index, clmd_json_t *json);
int clmd_json_itera(clmd_json_itera_cb *cb, void *baton, clmd_json_t *json, ...);

/* Manipulating Object String values */
kuda_status_t clmd_json_gets_dict(kuda_table_t *dict, const clmd_json_t *json, ...);
kuda_status_t clmd_json_sets_dict(kuda_table_t *dict, clmd_json_t *json, ...);

/* Manipulating String Arrays */
kuda_status_t clmd_json_getsa(kuda_array_header_t *a, const clmd_json_t *json, ...);
kuda_status_t clmd_json_dupsa(kuda_array_header_t *a, kuda_pool_t *p, clmd_json_t *json, ...);
kuda_status_t clmd_json_setsa(kuda_array_header_t *a, clmd_json_t *json, ...);

/* serialization & parsing */
kuda_status_t clmd_json_writeb(clmd_json_t *json, clmd_json_fmt_t fmt, struct kuda_bucket_brigade *bb);
const char *clmd_json_writep(clmd_json_t *json, kuda_pool_t *p, clmd_json_fmt_t fmt);
kuda_status_t clmd_json_writef(clmd_json_t *json, kuda_pool_t *p, 
                            clmd_json_fmt_t fmt, struct kuda_file_t *f);
kuda_status_t clmd_json_fcreatex(clmd_json_t *json, kuda_pool_t *p, clmd_json_fmt_t fmt, 
                              const char *fpath, kuda_fileperms_t perms);
kuda_status_t clmd_json_freplace(clmd_json_t *json, kuda_pool_t *p, clmd_json_fmt_t fmt, 
                              const char *fpath, kuda_fileperms_t perms);

kuda_status_t clmd_json_readb(clmd_json_t **pjson, kuda_pool_t *pool, struct kuda_bucket_brigade *bb);
kuda_status_t clmd_json_readd(clmd_json_t **pjson, kuda_pool_t *pool, const char *data, size_t data_len);
kuda_status_t clmd_json_readf(clmd_json_t **pjson, kuda_pool_t *pool, const char *fpath);


/* http retrieval */
kuda_status_t clmd_json_http_get(clmd_json_t **pjson, kuda_pool_t *pool,
                              struct clmd_http_t *http, const char *url);
kuda_status_t clmd_json_read_http(clmd_json_t **pjson, kuda_pool_t *pool, 
                               const struct clmd_http_response_t *res);

kuda_status_t clmd_json_copy_to(clmd_json_t *dest, const clmd_json_t *src, ...);

#endif /* clmd_json_h */
