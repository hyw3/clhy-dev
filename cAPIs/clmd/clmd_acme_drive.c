/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <stdlib.h>

#include <kuda_lib.h>
#include <kuda_strings.h>
#include <kuda_buckets.h>
#include <kuda_hash.h>
#include <kuda_uri.h>

#include "clmd.h"
#include "clmd_crypt.h"
#include "clmd_json.h"
#include "clmd_jws.h"
#include "clmd_http.h"
#include "clmd_log.h"
#include "clmd_result.h"
#include "clmd_reg.h"
#include "clmd_store.h"
#include "clmd_util.h"

#include "clmd_acme.h"
#include "clmd_acme_acct.h"
#include "clmd_acme_authz.h"
#include "clmd_acme_order.h"

#include "clmd_acme_drive.h"
#include "clmd_acmev1_drive.h"
#include "clmd_acmev2_drive.h"

/**************************************************************************************************/
/* account setup */

static kuda_status_t use_staged_acct(clmd_acme_t *acme, struct clmd_store_t *store, 
                                    const char *clmd_name, kuda_pool_t *p)
{
    clmd_acme_acct_t *acct;
    clmd_pkey_t *pkey;
    kuda_status_t rv;
    
    if (KUDA_SUCCESS == (rv = clmd_acme_acct_load(&acct, &pkey, store, 
                                               CLMD_SG_STAGING, clmd_name, acme->p))) {
        acme->acct_id = NULL;
        acme->acct = acct;
        acme->acct_key = pkey;
        rv = clmd_acme_acct_validate(acme, NULL, p);
    }
    return rv;
}

static kuda_status_t save_acct_staged(clmd_acme_t *acme, clmd_store_t *store, 
                                     const char *clmd_name, kuda_pool_t *p)
{
    clmd_json_t *jacct;
    kuda_status_t rv;
    
    jacct = clmd_acme_acct_to_json(acme->acct, p);
    
    rv = clmd_store_save(store, p, CLMD_SG_STAGING, clmd_name, CLMD_FN_ACCOUNT, CLMD_SV_JSON, jacct, 0);
    if (KUDA_SUCCESS == rv) {
        rv = clmd_store_save(store, p, CLMD_SG_STAGING, clmd_name, CLMD_FN_ACCT_KEY, 
                           CLMD_SV_PKEY, acme->acct_key, 0);
    }
    return rv;
}

kuda_status_t clmd_acme_drive_set_acct(clmd_proto_driver_t *d, clmd_result_t *result) 
{
    clmd_acme_driver_t *ad = d->baton;
    clmd_t *clmd = ad->clmd;
    kuda_status_t rv = KUDA_SUCCESS;
    int update_clmd = 0, update_acct = 0;
    
    clmd_result_activity_printf(result, "Selecting account to use for %s", d->clmd->name);
    clmd_acme_clear_acct(ad->acme);
    
    /* Do we have a staged (modified) account? */
    if (KUDA_SUCCESS == (rv = use_staged_acct(ad->acme, d->store, clmd->name, d->p))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "re-using staged account");
    }
    else if (!KUDA_STATUS_IS_ENOENT(rv)) {
        goto out;
    }
    
    /* Get an account for the ACME server for this CLMD */
    if (!ad->acme->acct && clmd->ca_account) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "re-use account '%s'", clmd->ca_account);
        rv = clmd_acme_use_acct(ad->acme, d->store, d->p, clmd->ca_account);
        if (KUDA_STATUS_IS_ENOENT(rv) || KUDA_STATUS_IS_EINVAL(rv)) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "rejected %s", clmd->ca_account);
            clmd->ca_account = NULL;
            update_clmd = 1;
        }
        else if (KUDA_SUCCESS != rv) {
            goto out;
        }
    }

    if (!ad->acme->acct && !clmd->ca_account) {
        /* Find a local account for server, store at CLMD */ 
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "%s: looking at existing accounts",
                      d->proto->protocol);
        if (KUDA_SUCCESS == (rv = clmd_acme_find_acct(ad->acme, d->store))) {
            clmd->ca_account = clmd_acme_acct_id_get(ad->acme);
            update_clmd = 1;
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "%s: using account %s (id=%s)",
                          d->proto->protocol, ad->acme->acct->url, clmd->ca_account);
        }
    }
    
    if (!ad->acme->acct) {
        /* No account staged, no suitable found in store, register a new one */
        clmd_result_activity_printf(result, "Creating new ACME account for %s", d->clmd->name);
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "%s: creating new account", 
                      d->proto->protocol);
        
        if (!ad->clmd->contacts || kuda_is_empty_array(clmd->contacts)) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, KUDA_EINVAL, d->p, 
                          "no contact information for clmd %s", clmd->name);            
            rv = KUDA_EINVAL;
            goto out;
        }
        
        /* ACMEv1 allowed registration of accounts without accepted Terms-of-Service.
         * ACMEv2 requires it. Fail early in this case with a meaningful error message.
         */ 
        if (!clmd->ca_agreement && CLMD_ACME_VERSION_MAJOR(ad->acme->version) > 1) {
            clmd_result_printf(result, KUDA_EINVAL,
                  "the CA requires you to accept the terms-of-service "
                  "as specified in <%s>. "
                  "Please read the document that you find at that URL and, "
                  "if you agree to the conditions, configure "
                  "\"MDCertificateAgreement accepted\" "
                  "in your cLHy. Then (graceful) restart the server to activate.", 
                  ad->acme->ca_agreement);
            clmd_result_log(result, CLMD_LOG_ERR);
            rv = result->status;
            goto out;
        }
    
        rv = clmd_acme_acct_register(ad->acme, d->store, d->p, clmd->contacts, clmd->ca_agreement);
        if (KUDA_SUCCESS == rv) {
            clmd->ca_account = NULL;
            update_clmd = 1;
            update_acct = 1;
        }
    }
    
out:
    /* Persist CLMD changes in STAGING, so we pick them up on next run */
    if (KUDA_SUCCESS == rv&& update_clmd) {
        rv = clmd_save(d->store, d->p, CLMD_SG_STAGING, ad->clmd, 0);
    }
    /* Persist account changes in STAGING, so we pick them up on next run */
    if (KUDA_SUCCESS == rv&& update_acct) {
        rv = save_acct_staged(ad->acme, d->store, clmd->name, d->p);
    }
    return rv;
}

/**************************************************************************************************/
/* poll cert */

static void get_up_link(clmd_proto_driver_t *d, kuda_table_t *headers)
{
    clmd_acme_driver_t *ad = d->baton;

    ad->next_up_link = clmd_link_find_relation(headers, d->p, "up");
    if (ad->next_up_link) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, d->p, 
                      "server reports up link as %s", ad->next_up_link);
    }
} 

static kuda_status_t add_http_certs(kuda_array_header_t *chain, kuda_pool_t *p,
                                   const clmd_http_response_t *res)
{
    kuda_status_t rv = KUDA_SUCCESS;
    const char *ct;
    
    ct = kuda_table_get(res->headers, "Content-Type");
    if (ct && !strcmp("application/x-pkcs7-mime", ct)) {
        /* this looks like a root cert and we do not want those in our chain */
        goto out; 
    }

    /* Lets try to read one or more certificates */
    if (KUDA_SUCCESS != (rv = clmd_cert_chain_read_http(chain, p, res))
        && KUDA_STATUS_IS_ENOENT(rv)) {
        rv = KUDA_EAGAIN;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, 
                      "cert not in response from %s", res->req->url);
    }
out:
    return rv;
}

static kuda_status_t on_add_cert(clmd_acme_t *acme, const clmd_http_response_t *res, void *baton)
{
    clmd_proto_driver_t *d = baton;
    clmd_acme_driver_t *ad = d->baton;
    kuda_status_t rv = KUDA_SUCCESS;
    int count;
    
    (void)acme;
    count = ad->certs->nelts;
    if (KUDA_SUCCESS == (rv = add_http_certs(ad->certs, d->p, res))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "%d certs parsed", 
                      ad->certs->nelts - count);
        get_up_link(d, res->headers);
    }
    return rv;
}

static kuda_status_t get_cert(void *baton, int attempt)
{
    clmd_proto_driver_t *d = baton;
    clmd_acme_driver_t *ad = d->baton;
    
    (void)attempt;
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, d->p, "retrieving cert from %s",
                  ad->order->certificate);
    return clmd_acme_GET(ad->acme, ad->order->certificate, NULL, NULL, on_add_cert, NULL, d);
}

kuda_status_t clmd_acme_drive_cert_poll(clmd_proto_driver_t *d, int only_once)
{
    clmd_acme_driver_t *ad = d->baton;
    kuda_status_t rv;
    
    assert(ad->clmd);
    assert(ad->acme);
    assert(ad->order);
    assert(ad->order->certificate);
    
    if (only_once) {
        rv = get_cert(d, 0);
    }
    else {
        rv = clmd_util_try(get_cert, d, 1, ad->cert_poll_timeout, 0, 0, 1);
    }
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, 0, d->p, "poll for cert at %s", ad->order->certificate);
    return rv;
}

/**************************************************************************************************/
/* order finalization */

static kuda_status_t on_init_csr_req(clmd_acme_req_t *req, void *baton)
{
    clmd_proto_driver_t *d = baton;
    clmd_acme_driver_t *ad = d->baton;
    clmd_json_t *jpayload;

    jpayload = clmd_json_create(req->p);
    if (CLMD_ACME_VERSION_MAJOR(req->acme->version) == 1) {
        clmd_json_sets("new-cert", jpayload, CLMD_KEY_RESOURCE, NULL);
    }
    clmd_json_sets(ad->csr_der_64, jpayload, CLMD_KEY_CSR, NULL);
    
    return clmd_acme_req_body_init(req, jpayload);
} 

static kuda_status_t csr_req(clmd_acme_t *acme, const clmd_http_response_t *res, void *baton)
{
    clmd_proto_driver_t *d = baton;
    clmd_acme_driver_t *ad = d->baton;
    const char *location;
    clmd_cert_t *cert;
    kuda_status_t rv = KUDA_SUCCESS;
    
    (void)acme;
    location = kuda_table_get(res->headers, "location");
    if (!location) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, KUDA_EINVAL, d->p, 
                      "cert created without giving its location header");
        return KUDA_EINVAL;
    }
    ad->order->certificate = kuda_pstrdup(d->p, location);
    if (KUDA_SUCCESS != (rv = clmd_acme_order_save(d->store, d->p, CLMD_SG_STAGING, 
                                                d->clmd->name, ad->order, 0))) { 
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, KUDA_EINVAL, d->p, 
                      "%s: saving cert url %s", d->clmd->name, location);
        return rv;
    }
    
    /* Check if it already was sent with this response */
    ad->next_up_link = NULL;
    if (KUDA_SUCCESS == (rv = clmd_cert_read_http(&cert, d->p, res))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "cert parsed");
        kuda_array_clear(ad->certs);
        KUDA_ARRAY_PUSH(ad->certs, clmd_cert_t*) = cert;
        get_up_link(d, res->headers);
    }
    else if (KUDA_STATUS_IS_ENOENT(rv)) {
        rv = KUDA_SUCCESS;
        if (location) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, 
                          "cert not in response, need to poll %s", location);
        }
    }
    
    return rv;
}

/**
 * Pre-Req: all domains have been validated by the ACME server, e.g. all have AUTHZ
 * resources that have status 'valid'
 * - Setup private key, if not already there
 * - Generate a CSR with org, contact, etc
 * - Optionally enable must-staple OCSP extension
 * - Submit CSR, expect 201 with location
 * - POLL location for certificate
 * - store certificate
 * - retrieve cert chain information from cert
 * - GET cert chain
 * - store cert chain
 */
kuda_status_t clmd_acme_drive_setup_certificate(clmd_proto_driver_t *d, clmd_result_t *result)
{
    clmd_acme_driver_t *ad = d->baton;
    clmd_pkey_t *privkey;
    kuda_status_t rv;

    clmd_result_activity_printf(result, "Finalizing order for %s", ad->clmd->name);
    
    rv = clmd_pkey_load(d->store, CLMD_SG_STAGING, d->clmd->name, &privkey, d->p);
    if (KUDA_STATUS_IS_ENOENT(rv)) {
        if (KUDA_SUCCESS == (rv = clmd_pkey_gen(&privkey, d->p, d->clmd->pkey_spec))) {
            rv = clmd_pkey_save(d->store, d->p, CLMD_SG_STAGING, d->clmd->name, privkey, 1);
        }
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "%s: generate privkey", d->clmd->name);
    }
    if (KUDA_SUCCESS != rv) goto leave;
    
    clmd_result_activity_printf(result, "Creating CSR for %s", d->clmd->name);
    rv = clmd_cert_req_create(&ad->csr_der_64, d->clmd->name, ad->domains, 
                            ad->clmd->must_staple, privkey, d->p);
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "%s: create CSR", d->clmd->name);
    if (KUDA_SUCCESS != rv) goto leave;

    clmd_result_activity_printf(result, "Submitting CSR to CA for %s", d->clmd->name);
    switch (CLMD_ACME_VERSION_MAJOR(ad->acme->version)) {
        case 1:
            rv = clmd_acme_POST(ad->acme, ad->acme->api.v1.new_cert, on_init_csr_req, NULL, csr_req, NULL, d);
            break;
        default:
            assert(ad->order->finalize);
            rv = clmd_acme_POST(ad->acme, ad->order->finalize, on_init_csr_req, NULL, csr_req, NULL, d);
            break;
    }
leave:
    clmd_acme_report_result(ad->acme, rv, result);
    return rv;
}

/**************************************************************************************************/
/* cert chain retrieval */

static kuda_status_t on_add_chain(clmd_acme_t *acme, const clmd_http_response_t *res, void *baton)
{
    clmd_proto_driver_t *d = baton;
    clmd_acme_driver_t *ad = d->baton;
    kuda_status_t rv = KUDA_SUCCESS;
    const char *ct;
    
    (void)acme;
    ct = kuda_table_get(res->headers, "Content-Type");
    if (ct && !strcmp("application/x-pkcs7-mime", ct)) {
        /* root cert most likely, end it here */
        return KUDA_SUCCESS;
    }
    
    if (KUDA_SUCCESS == (rv = add_http_certs(ad->certs, d->p, res))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "chain cert parsed");
        get_up_link(d, res->headers);
    }
    return rv;
}

static kuda_status_t get_chain(void *baton, int attempt)
{
    clmd_proto_driver_t *d = baton;
    clmd_acme_driver_t *ad = d->baton;
    const char *prev_link = NULL;
    kuda_status_t rv = KUDA_SUCCESS;

    while (KUDA_SUCCESS == rv && ad->certs->nelts < 10) {
        int nelts = ad->certs->nelts;
        
        if (ad->next_up_link && (!prev_link || strcmp(prev_link, ad->next_up_link))) {
            prev_link = ad->next_up_link;

            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, 
                          "next chain cert at  %s", ad->next_up_link);
            rv = clmd_acme_GET(ad->acme, ad->next_up_link, NULL, NULL, on_add_chain, NULL, d);
            
            if (KUDA_SUCCESS == rv && nelts == ad->certs->nelts) {
                break;
            }
        }
        else if (ad->certs->nelts <= 1) {
            /* This cannot be the complete chain (no one signs new web certs with their root)
             * and we did not see a "Link: ...rel=up", so we do not know how to continue. */
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, d->p, 
                          "no link header 'up' for new certificate, unable to retrieve chain");
            rv = KUDA_EINVAL;
            break;
        }
        else {
            rv = KUDA_SUCCESS;
            break;
        }
    }
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, rv, d->p, 
                  "got chain with %d certs (%d. attempt)", ad->certs->nelts, attempt);
    return rv;
}

static kuda_status_t ad_chain_retrieve(clmd_proto_driver_t *d)
{
    clmd_acme_driver_t *ad = d->baton;
    kuda_status_t rv;
    
    /* This may be called repeatedly and needs to progress. The relevant state is in
     * ad->certs                the certificate chain, starting with the new cert for the clmd
     * ad->order->certificate   the url where ACME offers us the new clmd certificate. This may
     *                          be a single one or even the complete chain
     * ad->next_up_link         in case the last certificate retrieval did not end the chain,
     *                          the link header with relation "up" gives us the location
     *                          for the next cert in the chain
     */
    if (clmd_array_is_empty(ad->certs)) {
        /* Need to start at the order */
        ad->next_up_link = NULL;
        if (!ad->order) {
            rv = KUDA_EGENERAL;
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, d->p, 
                "%s: asked to retrieve chain, but no order in context", d->clmd->name);
            goto out;
        }
        if (!ad->order->certificate) {
            rv = KUDA_EGENERAL;
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, d->p, 
                "%s: asked to retrieve chain, but no certificate url part of order", d->clmd->name);
            goto out;
        }
        
        if (KUDA_SUCCESS != (rv = clmd_acme_drive_cert_poll(d, 0))) {
            goto out;
        }
    }
    
    rv = clmd_util_try(get_chain, d, 0, ad->cert_poll_timeout, 0, 0, 0);
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "chain retrieved");
    
out:
    return rv;
}

/**************************************************************************************************/
/* ACME driver init */

static kuda_status_t acme_driver_init(clmd_proto_driver_t *d, clmd_result_t *result)
{
    clmd_acme_driver_t *ad;
    int dis_http, dis_https, dis_alpn_acme, dis_dns;
    const char *challenge;
    
    clmd_result_set(result, KUDA_SUCCESS, NULL);
    
    ad = kuda_pcalloc(d->p, sizeof(*ad));
    
    d->baton = ad;
    
    ad->driver = d;
    ad->authz_monitor_timeout = kuda_time_from_sec(30);
    ad->cert_poll_timeout = kuda_time_from_sec(30);
    ad->ca_challenges = kuda_array_make(d->p, 3, sizeof(const char*));
    ad->certs = kuda_array_make(d->p, 5, sizeof(clmd_cert_t*));
    
    /* We can only support challenges if the server is reachable from the outside
     * via port 80 and/or 443. These ports might be mapped for wwhy to something
     * else, but a mapping needs to exist. */
    challenge = kuda_table_get(d->env, CLMD_KEY_CHALLENGE); 
    if (challenge) {
        KUDA_ARRAY_PUSH(ad->ca_challenges, const char*) = kuda_pstrdup(d->p, challenge);
    }
    else if (d->clmd->ca_challenges && d->clmd->ca_challenges->nelts > 0) {
        /* pre-configured set for this managed domain */
        kuda_array_cat(ad->ca_challenges, d->clmd->ca_challenges);
    }
    else {
        /* free to chose. Add all we support and see what we get offered */
        KUDA_ARRAY_PUSH(ad->ca_challenges, const char*) = CLMD_AUTHZ_TYPE_HTTP01;
        KUDA_ARRAY_PUSH(ad->ca_challenges, const char*) = CLMD_AUTHZ_TYPE_TLSALPN01;
        KUDA_ARRAY_PUSH(ad->ca_challenges, const char*) = CLMD_AUTHZ_TYPE_DNS01;
    }
    
    if (!d->can_http && !d->can_https 
        && clmd_array_str_index(ad->ca_challenges, CLMD_AUTHZ_TYPE_DNS01, 0, 0) < 0) {
        clmd_result_printf(result, KUDA_EGENERAL,
            "the server seems neither reachable via http (port 80) nor https (port 443). "
            "Please look at the MDPortMap configuration directive on how to correct this. "
            "The ACME protocol needs at least one of those so the CA can talk to the server "
            "and verify a domain ownership. Alternatively, you may configure support "
            "for the %s challenge directive.", CLMD_AUTHZ_TYPE_DNS01);
        goto leave;
    }
    
    dis_http = dis_https = dis_alpn_acme = dis_dns = 0;
    if (!d->can_http && clmd_array_str_index(ad->ca_challenges, CLMD_AUTHZ_TYPE_HTTP01, 0, 1) >= 0) {
        ad->ca_challenges = clmd_array_str_remove(d->p, ad->ca_challenges, CLMD_AUTHZ_TYPE_HTTP01, 0);
        dis_http = 1;
    }
    if (!d->can_https && clmd_array_str_index(ad->ca_challenges, CLMD_AUTHZ_TYPE_TLSALPN01, 0, 1) >= 0) {
        ad->ca_challenges = clmd_array_str_remove(d->p, ad->ca_challenges, CLMD_AUTHZ_TYPE_TLSALPN01, 0);
        dis_https = 1;
    }
    if (kuda_is_empty_array(d->clmd->acme_tls_1_domains)
        && clmd_array_str_index(ad->ca_challenges, CLMD_AUTHZ_TYPE_TLSALPN01, 0, 1) >= 0) {
        ad->ca_challenges = clmd_array_str_remove(d->p, ad->ca_challenges, CLMD_AUTHZ_TYPE_TLSALPN01, 0);
        dis_alpn_acme = 1;
    }
    if (!kuda_table_get(d->env, CLMD_KEY_CMD_DNS01) && clmd_array_str_index(ad->ca_challenges, CLMD_AUTHZ_TYPE_DNS01, 0, 1) >= 0) {
        ad->ca_challenges = clmd_array_str_remove(d->p, ad->ca_challenges, CLMD_AUTHZ_TYPE_DNS01, 0);
        dis_dns = 1;
    }

    if (kuda_is_empty_array(ad->ca_challenges)) {
        clmd_result_printf(result, KUDA_EGENERAL, 
            "None of the ACME challenge methods configured for this domain are suitable.%s%s%s%s",
            dis_http? " The http: challenge 'http-01' is disabled because the server seems not reachable on port 80." : "",
            dis_https? " The https: challenge 'tls-alpn-01' is disabled because the server seems not reachable on port 443." : "",
            dis_alpn_acme? "The https: challenge 'tls-alpn-01' is disabled because the Protocols configuration does not include the 'acme-tls/1' protocol." : "",
            dis_dns? "The DNS challenge 'dns-01' is disabled because the directive 'MDChallengeDns01' is not configured." : ""
            );
        goto leave;
    }

leave:    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, result->status, d->p, "%s: init driver", d->clmd->name);
    return result->status;
}

/**************************************************************************************************/
/* ACME staging */

static kuda_status_t acme_renew(clmd_proto_driver_t *d, clmd_result_t *result)
{
    clmd_acme_driver_t *ad = d->baton;
    int reset_staging = d->reset;
    kuda_status_t rv = KUDA_SUCCESS;
    kuda_time_t now;
    kuda_array_header_t *staged_certs;
    char ts[KUDA_RFC822_DATE_LEN];

    if (clmd_log_is_level(d->p, CLMD_LOG_DEBUG)) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, d->p, "%s: staging started, "
                      "state=%d, can_http=%d, can_https=%d, challenges='%s'",
                      d->clmd->name, d->clmd->state, d->can_http, d->can_https,
                      kuda_array_pstrcat(d->p, ad->ca_challenges, ' '));
    }

    /* When not explicitly told to reset, we check the existing data. If
     * it is incomplete or old, we trigger the reset for a clean start. */
    if (!reset_staging) {
        clmd_result_activity_setn(result, "Checking staging area");
        rv = clmd_load(d->store, CLMD_SG_STAGING, d->clmd->name, &ad->clmd, d->p);
        if (KUDA_SUCCESS == rv) {
            /* So, we have a copy in staging, but is it a recent or an old one? */
            if (clmd_is_newer(d->store, CLMD_SG_DOMAINS, CLMD_SG_STAGING, d->clmd->name, d->p)) {
                reset_staging = 1;
            }
        }
        else if (KUDA_STATUS_IS_ENOENT(rv)) {
            reset_staging = 1;
            rv = KUDA_SUCCESS;
        }
    }
    
    if (reset_staging) {
        clmd_result_activity_setn(result, "Resetting staging area");
        /* reset the staging area for this domain */
        rv = clmd_store_purge(d->store, d->p, CLMD_SG_STAGING, d->clmd->name);
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, rv, d->p, 
                      "%s: reset staging area, will", d->clmd->name);
        if (KUDA_SUCCESS != rv && !KUDA_STATUS_IS_ENOENT(rv)) {
            clmd_result_printf(result, rv, "resetting staging area");
            goto out;
        }
        rv = KUDA_SUCCESS;
        ad->clmd = NULL;
        ad->order = NULL;
    }
    
    clmd_result_activity_setn(result, "Assessing current status");
    if (ad->clmd && ad->clmd->state == CLMD_S_MISSING_INFORMATION) {
        /* ToS agreement is missing. It makes no sense to drive this CLMD further */
        clmd_result_printf(result, KUDA_INCOMPLETE, 
            "The managed domain %s is missing required information", d->clmd->name);
        goto out;
    }
    
    if (ad->clmd) {
        const char *keyfile, *certfile;

        rv = clmd_reg_get_cred_files(&keyfile, &certfile, d->reg, CLMD_SG_STAGING, d->clmd, d->p);
        if (KUDA_SUCCESS == rv) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, 0, d->p, "%s: all data staged", d->clmd->name);
            goto ready;
        }
    }
    
    /* Need to renew */
    clmd_result_activity_printf(result, "Contacting ACME server for %s at %s", 
                              d->clmd->name, d->clmd->ca_url);
    if (KUDA_SUCCESS != (rv = clmd_acme_create(&ad->acme, d->p, d->clmd->ca_url, d->proxy_url))) {
        clmd_result_printf(result, rv, "setup ACME communications");
        clmd_result_log(result, CLMD_LOG_ERR);
        goto out;
    } 
    if (KUDA_SUCCESS != (rv = clmd_acme_setup(ad->acme, result))) {
        clmd_result_log(result, CLMD_LOG_ERR);
        goto out;
    }
    
    if (!ad->clmd || strcmp(ad->clmd->ca_url, d->clmd->ca_url)) {
        clmd_result_activity_printf(result, "Resetting staging for %s", d->clmd->name);
        /* re-initialize staging */
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, 0, d->p, "%s: setup staging", d->clmd->name);
        clmd_store_purge(d->store, d->p, CLMD_SG_STAGING, d->clmd->name);
        ad->clmd = clmd_copy(d->p, d->clmd);
        ad->order = NULL;
        rv = clmd_save(d->store, d->p, CLMD_SG_STAGING, ad->clmd, 0);
        if (KUDA_SUCCESS != rv) {
            clmd_result_printf(result, rv, "Saving CLMD information in staging area.");
            clmd_result_log(result, CLMD_LOG_ERR);
            goto out;
        }
    }
    if (!ad->domains) {
        ad->domains = clmd_dns_make_minimal(d->p, ad->clmd->domains);
    }
    
    if (clmd_array_is_empty(ad->certs)
        && KUDA_SUCCESS == clmd_pubcert_load(d->store, CLMD_SG_STAGING, d->clmd->name, &staged_certs, d->p)) {
        kuda_array_cat(ad->certs, staged_certs);
    }
    
    if (clmd_array_is_empty(ad->certs)) {
        clmd_result_activity_printf(result, "Driving ACME protocol for renewal of %s", d->clmd->name);
        /* The process of setting up challenges and verifying domain
         * names differs between ACME versions. */
        switch (CLMD_ACME_VERSION_MAJOR(ad->acme->version)) {
                case 1:
                rv = clmd_acmev1_drive_renew(ad, d, result);
                break;
                case 2:
                rv = clmd_acmev2_drive_renew(ad, d, result);
                break;
            default:
                clmd_result_printf(result, KUDA_EINVAL,
                    "ACME server has unknown major version %d (%x)",
                    CLMD_ACME_VERSION_MAJOR(ad->acme->version), ad->acme->version);
                rv = result->status;
                break;
        }
        if (KUDA_SUCCESS != rv) goto out;
    }
    
    if (clmd_array_is_empty(ad->certs) || ad->next_up_link) {
        clmd_result_activity_printf(result, "Retrieving certificate chain for %s", d->clmd->name);
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, 0, d->p, 
                      "%s: retrieving certificate chain", d->clmd->name);
        rv = ad_chain_retrieve(d);
        if (KUDA_SUCCESS != rv) {
            clmd_result_printf(result, rv, "Unable to retrive certificate chain.");
            goto out;
        }
        
        if (!clmd_array_is_empty(ad->certs)) {
            rv = clmd_pubcert_save(d->store, d->p, CLMD_SG_STAGING, d->clmd->name, ad->certs, 0);
            if (KUDA_SUCCESS != rv) {
                clmd_result_printf(result, rv, "Saving new certificate chain.");
                goto out;
            }
        }
    }
    
    /* As last step, cleanup any order we created so that challenge data
     * may be removed asap. */
    clmd_acme_order_purge(d->store, d->p, CLMD_SG_STAGING, d->clmd->name, d->env);

ready:
    clmd_result_activity_setn(result, NULL);
    /* we should have the complete cert chain now */
    assert(!clmd_array_is_empty(ad->certs));
    assert(ad->certs->nelts > 1);
    
    /* determine when it should be activated */
    clmd_result_delay_set(result, clmd_cert_get_not_before(KUDA_ARRAY_IDX(ad->certs, 0, clmd_cert_t*)));

    /* If the existing CLMD is complete and un-expired, delay the activation
     * to 24 hours after new cert is valid (if there is enough time left), so
     * that cients with skewed clocks do not see a problem. */
    now = kuda_time_now();
    if (d->clmd->state == CLMD_S_COMPLETE) {
        const clmd_pubcert_t *pub;
        kuda_time_t valid_until, delay_activation;
        
        if (KUDA_SUCCESS == clmd_reg_get_pubcert(&pub, d->reg, d->clmd, d->p)) {
            valid_until = clmd_cert_get_not_after(KUDA_ARRAY_IDX(pub->certs, 0, const clmd_cert_t*));
            if (valid_until > now) {            
                delay_activation = kuda_time_from_sec(CLMD_SECS_PER_DAY);
                if (delay_activation > (valid_until - now)) {
                    delay_activation = (valid_until - now);
                }
                clmd_result_delay_set(result, result->ready_at + delay_activation);
            }
        }
    }
    
    /* There is a full set staged, to be loaded */
    kuda_rfc822_date(ts, result->ready_at);
    if (result->ready_at > now) {
        clmd_result_printf(result, KUDA_SUCCESS, 
            "The certificate for the managed domain has been renewed successfully and can "
            "be used from %s on. A graceful server restart in %s is recommended.",
            ts, clmd_duration_print(d->p, result->ready_at - now));
    }
    else {
        clmd_result_printf(result, KUDA_SUCCESS, 
            "The certificate for the managed domain has been renewed successfully and can "
            "be used. A graceful server restart now is recommended.");
    }

out:
    return rv;
}

static kuda_status_t acme_driver_renew(clmd_proto_driver_t *d, clmd_result_t *result)
{
    kuda_status_t rv;

    rv = acme_renew(d, result);
    clmd_result_log(result, CLMD_LOG_DEBUG);
    return rv;
}

/**************************************************************************************************/
/* ACME preload */

static kuda_status_t acme_preload(clmd_proto_driver_t *d, clmd_store_group_t load_group, 
                                 const char *name, clmd_result_t *result) 
{
    kuda_status_t rv;
    clmd_pkey_t *privkey, *acct_key;
    clmd_t *clmd;
    kuda_array_header_t *pubcert;
    struct clmd_acme_acct_t *acct;

    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, d->p, "%s: preload start", name);
    /* Load data from CLMD_SG_STAGING and save it into "load_group".
     * This serves several purposes:
     *  1. It's a format check on the input data. 
     *  2. We write back what we read, creating data with our own access permissions
     *  3. We ignore any other accumulated data in STAGING
     *  4. Once "load_group" is complete an ok, we can swap/archive groups with a rename
     *  5. Reading/Writing the data will apply/remove any group specific data encryption.
     */
    if (KUDA_SUCCESS != (rv = clmd_load(d->store, CLMD_SG_STAGING, name, &clmd, d->p))) {
        clmd_result_set(result, rv, "loading staged clmd.json");
        goto leave;
    }
    if (KUDA_SUCCESS != (rv = clmd_pkey_load(d->store, CLMD_SG_STAGING, name, &privkey, d->p))) {
        clmd_result_set(result, rv, "loading staged privkey.pem");
        goto leave;
    }
    if (KUDA_SUCCESS != (rv = clmd_pubcert_load(d->store, CLMD_SG_STAGING, name, &pubcert, d->p))) {
        clmd_result_set(result, rv, "loading staged pubcert.pem");
        goto leave;
    }

    /* See if staging holds a new or modified account data */
    rv = clmd_acme_acct_load(&acct, &acct_key, d->store, CLMD_SG_STAGING, name, d->p);
    if (KUDA_STATUS_IS_ENOENT(rv)) {
        acct = NULL;
        acct_key = NULL;
        rv = KUDA_SUCCESS;
    }
    else if (KUDA_SUCCESS != rv) {
        clmd_result_set(result, rv, "loading staged account");
        goto leave;
    }

    clmd_result_activity_setn(result, "purging order information");
    clmd_acme_order_purge(d->store, d->p, CLMD_SG_STAGING, name, d->env);

    clmd_result_activity_setn(result, "purging store tmp space");
    rv = clmd_store_purge(d->store, d->p, load_group, name);
    if (KUDA_SUCCESS != rv) {
        clmd_result_set(result, rv, NULL);
        goto leave;
    }
    
    if (acct) {
        clmd_acme_t *acme;
        const char *id = clmd->ca_account;

        /* We may have STAGED the same account several times. This happens when
         * several CLMDs are renewed at once and need a new account. They will all store
         * the new account in their own STAGING area. By checking for accounts with
         * the same url, we save them all into a single one.
         */
        clmd_result_activity_setn(result, "saving staged account");
        if (!id && acct->url) {
            rv = clmd_acme_acct_id_for_url(&id, d->store, CLMD_SG_ACCOUNTS, acct->url, d->p);
            if (KUDA_STATUS_IS_ENOENT(rv)) {
                id = NULL;
            }
            else if (KUDA_SUCCESS != rv) {
                clmd_result_set(result, rv, "error searching for existing account by url");
                goto leave;
            }
        }
        
        if (KUDA_SUCCESS != (rv = clmd_acme_create(&acme, d->p, clmd->ca_url, d->proxy_url))) {
            clmd_result_set(result, rv, "error setting up acme");
            goto leave;
        }
        
        if (KUDA_SUCCESS != (rv = clmd_acme_acct_save(d->store, d->p, acme, &id, acct, acct_key))) {
            clmd_result_set(result, rv, "error saving account");
            goto leave;
        }
        clmd->ca_account = id;
    }
    
    clmd_result_activity_setn(result, "saving staged clmd/privkey/pubcert");
    if (KUDA_SUCCESS != (rv = clmd_save(d->store, d->p, load_group, clmd, 1))) {
        clmd_result_set(result, rv, "writing clmd.json");
        goto leave;
    }
    if (KUDA_SUCCESS != (rv = clmd_pubcert_save(d->store, d->p, load_group, name, pubcert, 1))) {
        clmd_result_set(result, rv, "writing pubcert.pem");
        goto leave;
    }
    if (KUDA_SUCCESS != (rv = clmd_pkey_save(d->store, d->p, load_group, name, privkey, 1))) {
        clmd_result_set(result, rv, "writing privkey.pem");
        goto leave;
    }
    clmd_result_set(result, KUDA_SUCCESS, "saved staged data successfully");
    
leave:
    clmd_result_log(result, CLMD_LOG_DEBUG);
    return rv;
}

static kuda_status_t acme_driver_preload(clmd_proto_driver_t *d, 
                                        clmd_store_group_t group, clmd_result_t *result)
{
    kuda_status_t rv;

    rv = acme_preload(d, group, d->clmd->name, result);
    clmd_result_log(result, CLMD_LOG_DEBUG);
    return rv;
}

static clmd_proto_t ACME_PROTO = {
    CLMD_PROTO_ACME, acme_driver_init, acme_driver_renew, acme_driver_preload
};
 
kuda_status_t clmd_acme_protos_add(kuda_hash_t *protos, kuda_pool_t *p)
{
    (void)p;
    kuda_hash_set(protos, CLMD_PROTO_ACME, sizeof(CLMD_PROTO_ACME)-1, &ACME_PROTO);
    return KUDA_SUCCESS;
}
