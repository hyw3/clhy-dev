/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>

#include <kuda_lib.h>
#include <kuda_strings.h>

#include <wwhy.h>
#include <http_core.h>
#include <http_config.h>
#include <http_log.h>
#include <http_vhost.h>

#include "clmd.h"
#include "clmd_crypt.h"
#include "clmd_log.h"
#include "clmd_util.h"
#include "capi_clmd_private.h"
#include "capi_clmd_config.h"

#define CLMD_CMD_CLMD_SECTION     "<MDomainSet"
#define CLMD_CMD_MD2_SECTION    "<MDomain"

#define DEF_VAL     (-1)

#ifndef CLMD_DEFAULT_BASE_DIR
#define CLMD_DEFAULT_BASE_DIR "clmd"
#endif

/* Default settings for the global conf */
static clmd_capi_conf_t defmc = {
    NULL,                      /* list of mds */
#if CLHY_CAPI_MAGIC_AT_LEAST(20180906, 2)
    NULL,                      /* base dir by default state-dir-relative */
#else
    CLMD_DEFAULT_BASE_DIR,
#endif
    NULL,                      /* proxy url for outgoing http */
    NULL,                      /* clmd_reg */
    80,                        /* local http: port */
    443,                       /* local https: port */
    0,                         /* can http: */
    0,                         /* can https: */
    0,                         /* manage base server */
    CLMD_HSTS_MAX_AGE_DEFAULT,   /* hsts max-age */
    NULL,                      /* hsts headers */
    NULL,                      /* unused names */
    NULL,                      /* watched names */
    NULL,                      /* init errors hash */
    NULL,                      /* notify cmd */
    NULL,                      /* message cmd */
    NULL,                      /* env table */
    0,                         /* dry_run flag */
    1,                         /* server_status_enabled */
    1,                         /* certificate_status_enabled */
};

static clmd_timeslice_t def_renew_window = {
    CLMD_TIME_LIFE_NORM,
    CLMD_TIME_RENEW_WINDOW_DEF,
};
static clmd_timeslice_t def_warn_window = {
    CLMD_TIME_LIFE_NORM,
    CLMD_TIME_WARN_WINDOW_DEF,
};

/* Default server specific setting */
static clmd_srv_conf_t defconf = {
    "default",                 /* name */
    NULL,                      /* server_rec */
    &defmc,                    /* mc */
    1,                         /* transitive */
    CLMD_REQUIRE_OFF,            /* require https */
    CLMD_RENEW_AUTO,             /* renew mode */
    0,                         /* must staple */
    NULL,                      /* pkey spec */
    &def_renew_window,         /* renew window */
    &def_warn_window,          /* warn window */
    NULL,                      /* ca url */
    "ACME",                    /* ca protocol */
    NULL,                      /* ca agreemnent */
    NULL,                      /* ca challenges array */
    NULL,                      /* currently defined clmd */
    NULL,                      /* assigned clmd, post config */
};

static clmd_capi_conf_t *capi_clmd_config;

static kuda_status_t cleanup_capi_config(void *dummy)
{
    (void)dummy;
    capi_clmd_config = NULL;
    return KUDA_SUCCESS;
}

static clmd_capi_conf_t *clmd_capi_conf_get(kuda_pool_t *pool, int create)
{
    if (capi_clmd_config) {
        return capi_clmd_config; /* reused for lifetime of the pool */
    }

    if (create) {
        capi_clmd_config = kuda_pcalloc(pool, sizeof(*capi_clmd_config));
        memcpy(capi_clmd_config, &defmc, sizeof(*capi_clmd_config));
        capi_clmd_config->mds = kuda_array_make(pool, 5, sizeof(const clmd_t *));
        capi_clmd_config->unused_names = kuda_array_make(pool, 5, sizeof(const clmd_t *));
        capi_clmd_config->watched_names = kuda_array_make(pool, 5, sizeof(const clmd_t *));
        capi_clmd_config->env = kuda_table_make(pool, 10);
        capi_clmd_config->init_errors = kuda_hash_make(pool);
         
        kuda_pool_cleanup_register(pool, NULL, cleanup_capi_config, kuda_pool_cleanup_null);
    }
    
    return capi_clmd_config;
}

#define CONF_S_NAME(s)  (s && s->server_hostname? s->server_hostname : "default")

static void srv_conf_props_clear(clmd_srv_conf_t *sc)
{
    sc->transitive = DEF_VAL;
    sc->require_https = CLMD_REQUIRE_UNSET;
    sc->renew_mode = DEF_VAL;
    sc->must_staple = DEF_VAL;
    sc->pkey_spec = NULL;
    sc->renew_window = NULL;
    sc->warn_window = NULL;
    sc->ca_url = NULL;
    sc->ca_proto = NULL;
    sc->ca_agreement = NULL;
    sc->ca_challenges = NULL;
}

static void srv_conf_props_copy(clmd_srv_conf_t *to, const clmd_srv_conf_t *from)
{
    to->transitive = from->transitive;
    to->require_https = from->require_https;
    to->renew_mode = from->renew_mode;
    to->must_staple = from->must_staple;
    to->pkey_spec = from->pkey_spec;
    to->warn_window = from->warn_window;
    to->renew_window = from->renew_window;
    to->ca_url = from->ca_url;
    to->ca_proto = from->ca_proto;
    to->ca_agreement = from->ca_agreement;
    to->ca_challenges = from->ca_challenges;
}

static void srv_conf_props_apply(clmd_t *clmd, const clmd_srv_conf_t *from, kuda_pool_t *p)
{
    if (from->require_https != CLMD_REQUIRE_UNSET) clmd->require_https = from->require_https;
    if (from->transitive != DEF_VAL) clmd->transitive = from->transitive;
    if (from->renew_mode != DEF_VAL) clmd->renew_mode = from->renew_mode;
    if (from->must_staple != DEF_VAL) clmd->must_staple = from->must_staple;
    if (from->pkey_spec) clmd->pkey_spec = from->pkey_spec;
    if (from->renew_window) clmd->renew_window = from->renew_window;
    if (from->warn_window) clmd->warn_window = from->warn_window;
    if (from->ca_url) clmd->ca_url = from->ca_url;
    if (from->ca_proto) clmd->ca_proto = from->ca_proto;
    if (from->ca_agreement) clmd->ca_agreement = from->ca_agreement;
    if (from->ca_challenges) clmd->ca_challenges = kuda_array_copy(p, from->ca_challenges);
}

void *clmd_config_create_svr(kuda_pool_t *pool, server_rec *s)
{
    clmd_srv_conf_t *conf = (clmd_srv_conf_t *)kuda_pcalloc(pool, sizeof(clmd_srv_conf_t));

    conf->name = kuda_pstrcat(pool, "srv[", CONF_S_NAME(s), "]", NULL);
    conf->s = s;
    conf->mc = clmd_capi_conf_get(pool, 1);

    srv_conf_props_clear(conf);
    
    return conf;
}

static void *clmd_config_merge(kuda_pool_t *pool, void *basev, void *addv)
{
    clmd_srv_conf_t *base = (clmd_srv_conf_t *)basev;
    clmd_srv_conf_t *add = (clmd_srv_conf_t *)addv;
    clmd_srv_conf_t *nsc;
    char *name = kuda_pstrcat(pool, "[", CONF_S_NAME(add->s), ", ", CONF_S_NAME(base->s), "]", NULL);
    
    nsc = (clmd_srv_conf_t *)kuda_pcalloc(pool, sizeof(clmd_srv_conf_t));
    nsc->name = name;
    nsc->mc = add->mc? add->mc : base->mc;
    nsc->assigned = add->assigned? add->assigned : base->assigned;

    nsc->transitive = (add->transitive != DEF_VAL)? add->transitive : base->transitive;
    nsc->require_https = (add->require_https != CLMD_REQUIRE_UNSET)? add->require_https : base->require_https;
    nsc->renew_mode = (add->renew_mode != DEF_VAL)? add->renew_mode : base->renew_mode;
    nsc->must_staple = (add->must_staple != DEF_VAL)? add->must_staple : base->must_staple;
    nsc->pkey_spec = add->pkey_spec? add->pkey_spec : base->pkey_spec;
    nsc->renew_window = add->renew_window? add->renew_window : base->renew_window;
    nsc->warn_window = add->warn_window? add->warn_window : base->warn_window;

    nsc->ca_url = add->ca_url? add->ca_url : base->ca_url;
    nsc->ca_proto = add->ca_proto? add->ca_proto : base->ca_proto;
    nsc->ca_agreement = add->ca_agreement? add->ca_agreement : base->ca_agreement;
    nsc->ca_challenges = (add->ca_challenges? kuda_array_copy(pool, add->ca_challenges) 
                    : (base->ca_challenges? kuda_array_copy(pool, base->ca_challenges) : NULL));
    nsc->current = NULL;
    nsc->assigned = NULL;
    
    return nsc;
}

void *clmd_config_merge_svr(kuda_pool_t *pool, void *basev, void *addv)
{
    return clmd_config_merge(pool, basev, addv);
}

static int inside_section(cmd_parms *cmd, const char *section) {
    clhy_directive_t *d;
    for (d = cmd->directive->parent; d; d = d->parent) {
       if (!clhy_cstr_casecmp(d->directive, section)) {
           return 1;
       }
    }
    return 0; 
}

static int inside_clmd_section(cmd_parms *cmd) {
    return (inside_section(cmd, CLMD_CMD_CLMD_SECTION) || inside_section(cmd, CLMD_CMD_MD2_SECTION));
}

static const char *clmd_section_check(cmd_parms *cmd) {
    if (!inside_clmd_section(cmd)) {
        return kuda_pstrcat(cmd->pool, cmd->cmd->name, " is only valid inside a '",  
                           CLMD_CMD_CLMD_SECTION, "' context, not here", NULL);
    }
    return NULL;
}

static const char *set_on_off(int *pvalue, const char *s, kuda_pool_t *p)
{
    if (!kuda_strnatcasecmp("off", s)) {
        *pvalue = 0;
    }
    else if (!kuda_strnatcasecmp("on", s)) {
        *pvalue = 1;
    }
    else {
        return kuda_pstrcat(p, "unknown '", s, 
                           "', supported parameter values are 'on' and 'off'", NULL);
    }
    return NULL;
}


static void add_domain_name(kuda_array_header_t *domains, const char *name, kuda_pool_t *p)
{
    if (clmd_array_str_index(domains, name, 0, 0) < 0) {
        KUDA_ARRAY_PUSH(domains, char *) = clmd_util_str_tolower(kuda_pstrdup(p, name));
    }
}

static const char *set_transitive(int *ptransitive, const char *value)
{
    if (!kuda_strnatcasecmp("auto", value)) {
        *ptransitive = 1;
        return NULL;
    }
    else if (!kuda_strnatcasecmp("manual", value)) {
        *ptransitive = 0;
        return NULL;
    }
    return "unknown value, use \"auto|manual\"";
}

static const char *clmd_config_sec_start(cmd_parms *cmd, void *mconfig, const char *arg)
{
    clmd_srv_conf_t *sc;
    clmd_srv_conf_t save;
    const char *endp;
    const char *err, *name;
    kuda_array_header_t *domains;
    clmd_t *clmd;
    int transitive = -1;
    
    (void)mconfig;
    if ((err = clhy_check_cmd_context(cmd, GLOBAL_ONLY))) {
        return err;
    }
        
    sc = clmd_config_get(cmd->server);
    endp = clhy_strrchr_c(arg, '>');
    if (endp == NULL) {
        return  CLMD_CMD_CLMD_SECTION "> directive missing closing '>'";
    }

    arg = kuda_pstrndup(cmd->pool, arg, (kuda_size_t)(endp-arg));
    if (!arg || !*arg) {
        return CLMD_CMD_CLMD_SECTION " > section must specify a unique domain name";
    }

    name = clhy_getword_white(cmd->pool, &arg);
    domains = kuda_array_make(cmd->pool, 5, sizeof(const char *));
    add_domain_name(domains, name, cmd->pool);
    while (*arg != '\0') {
        name = clhy_getword_white(cmd->pool, &arg);
        if (NULL != set_transitive(&transitive, name)) {
            add_domain_name(domains, name, cmd->pool);
        }
    }

    if (domains->nelts == 0) {
        return "needs at least one domain name";
    }
    
    clmd = clmd_create(cmd->pool, domains);
    if (transitive >= 0) {
        clmd->transitive = transitive;
    }
    
    /* Save the current settings in this srv_conf and apply+restore at the
     * end of this section */
    memcpy(&save, sc, sizeof(save));
    srv_conf_props_clear(sc);
    sc->current = clmd;
    
    if (NULL == (err = clhy_walk_config(cmd->directive->first_child, cmd, cmd->context))) {
        srv_conf_props_apply(clmd, sc, cmd->pool);
        KUDA_ARRAY_PUSH(sc->mc->mds, const clmd_t *) = clmd;
    }
    
    sc->current = NULL;
    srv_conf_props_copy(sc, &save);
    
    return err;
}

static const char *clmd_config_sec_add_members(cmd_parms *cmd, void *dc, 
                                             int argc, char *const argv[])
{
    clmd_srv_conf_t *sc = clmd_config_get(cmd->server);
    const char *err;
    int i;
    
    (void)dc;
    if (NULL != (err = clmd_section_check(cmd))) {
        if (argc == 1) {
            /* only these values are allowed outside a section */
            return set_transitive(&sc->transitive, argv[0]);
        }
        return err;
    }
    
    assert(sc->current);
    for (i = 0; i < argc; ++i) {
        if (NULL != set_transitive(&sc->transitive, argv[i])) {
            add_domain_name(sc->current->domains, argv[i], cmd->pool);
        }
    }
    return NULL;
}

static const char *clmd_config_set_names(cmd_parms *cmd, void *dc, 
                                       int argc, char *const argv[])
{
    clmd_srv_conf_t *sc = clmd_config_get(cmd->server);
    kuda_array_header_t *domains = kuda_array_make(cmd->pool, 5, sizeof(const char *));
    const char *err;
    clmd_t *clmd;
    int i, transitive = -1;

    (void)dc;
    err = clhy_check_cmd_context(cmd, NOT_IN_DIR_LOC_FILE);
    if (err) {
        return err;
    }

    for (i = 0; i < argc; ++i) {
        if (NULL != set_transitive(&transitive, argv[i])) {
            add_domain_name(domains, argv[i], cmd->pool);
        }
    }
    
    if (domains->nelts == 0) {
        return "needs at least one domain name";
    }
    clmd = clmd_create(cmd->pool, domains);

    if (transitive >= 0) {
        clmd->transitive = transitive;
    }
    
    if (cmd->config_file) {
        clmd->defn_name = cmd->config_file->name;
        clmd->defn_line_number = cmd->config_file->line_number;
    }

    KUDA_ARRAY_PUSH(sc->mc->mds, clmd_t *) = clmd;

    return NULL;
}

static const char *clmd_config_set_ca(cmd_parms *cmd, void *dc, const char *value)
{
    clmd_srv_conf_t *sc = clmd_config_get(cmd->server);
    const char *err;

    (void)dc;
    if (!inside_clmd_section(cmd) && (err = clhy_check_cmd_context(cmd, GLOBAL_ONLY))) {
        return err;
    }
    sc->ca_url = value;
    return NULL;
}

static const char *clmd_config_set_ca_proto(cmd_parms *cmd, void *dc, const char *value)
{
    clmd_srv_conf_t *config = clmd_config_get(cmd->server);
    const char *err;

    (void)dc;
    if (!inside_clmd_section(cmd) && (err = clhy_check_cmd_context(cmd, GLOBAL_ONLY))) {
        return err;
    }
    config->ca_proto = value;
    return NULL;
}

static const char *clmd_config_set_agreement(cmd_parms *cmd, void *dc, const char *value)
{
    clmd_srv_conf_t *config = clmd_config_get(cmd->server);
    const char *err;

    (void)dc;
    if (!inside_clmd_section(cmd) && (err = clhy_check_cmd_context(cmd, GLOBAL_ONLY))) {
        return err;
    }
    config->ca_agreement = value;
    return NULL;
}

static const char *clmd_config_set_renew_mode(cmd_parms *cmd, void *dc, const char *value)
{
    clmd_srv_conf_t *config = clmd_config_get(cmd->server);
    const char *err;
    clmd_renew_mode_t renew_mode;

    (void)dc;
    if (!kuda_strnatcasecmp("auto", value) || !kuda_strnatcasecmp("automatic", value)) {
        renew_mode = CLMD_RENEW_AUTO;
    }
    else if (!kuda_strnatcasecmp("always", value)) {
        renew_mode = CLMD_RENEW_ALWAYS;
    }
    else if (!kuda_strnatcasecmp("manual", value) || !kuda_strnatcasecmp("stick", value)) {
        renew_mode = CLMD_RENEW_MANUAL;
    }
    else {
        return kuda_pstrcat(cmd->pool, "unknown MDDriveMode ", value, NULL);
    }
    
    if (!inside_clmd_section(cmd) && (err = clhy_check_cmd_context(cmd, GLOBAL_ONLY))) {
        return err;
    }
    config->renew_mode = renew_mode;
    return NULL;
}

static const char *clmd_config_set_must_staple(cmd_parms *cmd, void *dc, const char *value)
{
    clmd_srv_conf_t *config = clmd_config_get(cmd->server);
    const char *err;

    (void)dc;
    if (!inside_clmd_section(cmd) && (err = clhy_check_cmd_context(cmd, GLOBAL_ONLY))) {
        return err;
    }
    return set_on_off(&config->must_staple, value, cmd->pool);
}

static const char *clmd_config_set_base_server(cmd_parms *cmd, void *dc, const char *value)
{
    clmd_srv_conf_t *config = clmd_config_get(cmd->server);
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    (void)dc;
    if (err) return err;
    return set_on_off(&config->mc->manage_base_server, value, cmd->pool);
}

static const char *clmd_config_set_require_https(cmd_parms *cmd, void *dc, const char *value)
{
    clmd_srv_conf_t *config = clmd_config_get(cmd->server);
    const char *err;

    (void)dc;
    if (!inside_clmd_section(cmd) && (err = clhy_check_cmd_context(cmd, GLOBAL_ONLY))) {
        return err;
    }

    if (!kuda_strnatcasecmp("off", value)) {
        config->require_https = CLMD_REQUIRE_OFF;
    }
    else if (!kuda_strnatcasecmp(CLMD_KEY_TEMPORARY, value)) {
        config->require_https = CLMD_REQUIRE_TEMPORARY;
    }
    else if (!kuda_strnatcasecmp(CLMD_KEY_PERMANENT, value)) {
        config->require_https = CLMD_REQUIRE_PERMANENT;
    }
    else {
        return kuda_pstrcat(cmd->pool, "unknown '", value, 
                           "', supported parameter values are 'temporary' and 'permanent'", NULL);
    }
    return NULL;
}

static const char *clmd_config_set_renew_window(cmd_parms *cmd, void *dc, const char *value)
{
    clmd_srv_conf_t *config = clmd_config_get(cmd->server);
    const char *err;
    
    (void)dc;
    if (!inside_clmd_section(cmd)
        && (err = clhy_check_cmd_context(cmd, GLOBAL_ONLY))) {
        return err;
    }
    err = clmd_timeslice_parse(&config->renew_window, cmd->pool, value, CLMD_TIME_LIFE_NORM);
    if (!err && config->renew_window->norm 
        && (config->renew_window->len >= config->renew_window->norm)) {
        err = "a length of 100% or more is not allowed.";
    }
    if (err) return kuda_psprintf(cmd->pool, "MDRenewWindow %s", err);
    return NULL;
}

static const char *clmd_config_set_warn_window(cmd_parms *cmd, void *dc, const char *value)
{
    clmd_srv_conf_t *config = clmd_config_get(cmd->server);
    const char *err;
    
    (void)dc;
    if (!inside_clmd_section(cmd)
        && (err = clhy_check_cmd_context(cmd, GLOBAL_ONLY))) {
        return err;
    }
    err = clmd_timeslice_parse(&config->warn_window, cmd->pool, value, CLMD_TIME_LIFE_NORM);
    if (!err && config->warn_window->norm 
        && (config->warn_window->len >= config->warn_window->norm)) {
        err = "a length of 100% or more is not allowed.";
    }
    if (err) return kuda_psprintf(cmd->pool, "MDWarnWindow %s", err);
    return NULL;
}

static const char *clmd_config_set_proxy(cmd_parms *cmd, void *arg, const char *value)
{
    clmd_srv_conf_t *sc = clmd_config_get(cmd->server);
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err) {
        return err;
    }
    clmd_util_abs_http_uri_check(cmd->pool, value, &err);
    if (err) {
        return err;
    }
    sc->mc->proxy_url = value;
    (void)arg;
    return NULL;
}

static const char *clmd_config_set_store_dir(cmd_parms *cmd, void *arg, const char *value)
{
    clmd_srv_conf_t *sc = clmd_config_get(cmd->server);
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err) {
        return err;
    }
    sc->mc->base_dir = value;
    (void)arg;
    return NULL;
}

static const char *set_port_map(clmd_capi_conf_t *mc, const char *value)
{
    int net_port, local_port;
    const char *endp;

    if (!strncmp("http:", value, sizeof("http:") - 1)) {
        net_port = 80; endp = value + sizeof("http") - 1; 
    }
    else if (!strncmp("https:", value, sizeof("https:") - 1)) {
        net_port = 443; endp = value + sizeof("https") - 1; 
    }
    else {
        net_port = (int)kuda_strtoi64(value, (char**)&endp, 10);
        if (errno) {
            return "unable to parse first port number";
        }
    }
    if (!endp || *endp != ':') {
        return "no ':' after first port number";
    }
    ++endp;
    if (*endp == '-') {
        local_port = 0;
    }
    else {
        local_port = (int)kuda_strtoi64(endp, (char**)&endp, 10);
        if (errno) {
            return "unable to parse second port number";
        }
        if (local_port <= 0 || local_port > 65535) {
            return "invalid number for port map, must be in ]0,65535]";
        }
    }
    switch (net_port) {
        case 80:
            mc->local_80 = local_port;
            break;
        case 443:
            mc->local_443 = local_port;
            break;
        default:
            return "mapped port number must be 80 or 443";
    }
    return NULL;
}

static const char *clmd_config_set_port_map(cmd_parms *cmd, void *arg, 
                                          const char *v1, const char *v2)
{
    clmd_srv_conf_t *sc = clmd_config_get(cmd->server);
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    (void)arg;
    if (!err) {
        err = set_port_map(sc->mc, v1);
    }
    if (!err && v2) {
        err = set_port_map(sc->mc, v2);
    }
    return err;
}

static const char *clmd_config_set_cha_tyes(cmd_parms *cmd, void *dc, 
                                          int argc, char *const argv[])
{
    clmd_srv_conf_t *config = clmd_config_get(cmd->server);
    kuda_array_header_t **pcha, *ca_challenges;
    const char *err;
    int i;

    (void)dc;
    if (!inside_clmd_section(cmd)
        && (err = clhy_check_cmd_context(cmd, GLOBAL_ONLY))) {
        return err;
    }
    pcha = &config->ca_challenges; 
    
    ca_challenges = *pcha;
    if (!ca_challenges) {
        *pcha = ca_challenges = kuda_array_make(cmd->pool, 5, sizeof(const char *));
    }
    for (i = 0; i < argc; ++i) {
        KUDA_ARRAY_PUSH(ca_challenges, const char *) = argv[i];
    }
    
    return NULL;
}

static const char *clmd_config_set_pkeys(cmd_parms *cmd, void *dc, 
                                       int argc, char *const argv[])
{
    clmd_srv_conf_t *config = clmd_config_get(cmd->server);
    const char *err, *ptype;
    kuda_int64_t bits;
    
    (void)dc;
    if (!inside_clmd_section(cmd)
        && (err = clhy_check_cmd_context(cmd, GLOBAL_ONLY))) {
        return err;
    }
    if (argc <= 0) {
        return "needs to specify the private key type";
    }
    
    ptype = argv[0];
    if (!kuda_strnatcasecmp("Default", ptype)) {
        if (argc > 1) {
            return "type 'Default' takes no parameter";
        }
        if (!config->pkey_spec) {
            config->pkey_spec = kuda_pcalloc(cmd->pool, sizeof(*config->pkey_spec));
        }
        config->pkey_spec->type = CLMD_PKEY_TYPE_DEFAULT;
        return NULL;
    }
    else if (!kuda_strnatcasecmp("RSA", ptype)) {
        if (argc == 1) {
            bits = CLMD_PKEY_RSA_BITS_DEF;
        }
        else if (argc == 2) {
            bits = (int)kuda_atoi64(argv[1]);
            if (bits < CLMD_PKEY_RSA_BITS_MIN || bits >= INT_MAX) {
                return kuda_psprintf(cmd->pool, "must be %d or higher in order to be considered "
                "safe. Too large a value will slow down everything. Larger then 4096 probably does "
                "not make sense unless quantum cryptography really changes spin.", 
                CLMD_PKEY_RSA_BITS_MIN);
            }
        }
        else {
            return "key type 'RSA' has only one optional parameter, the number of bits";
        }

        if (!config->pkey_spec) {
            config->pkey_spec = kuda_pcalloc(cmd->pool, sizeof(*config->pkey_spec));
        }
        config->pkey_spec->type = CLMD_PKEY_TYPE_RSA;
        config->pkey_spec->params.rsa.bits = (unsigned int)bits;
        return NULL;
    }
    return kuda_pstrcat(cmd->pool, "unsupported private key type \"", ptype, "\"", NULL);
}

static const char *clmd_config_set_notify_cmd(cmd_parms *cmd, void *mconfig, const char *arg)
{
    clmd_srv_conf_t *sc = clmd_config_get(cmd->server);
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err) {
        return err;
    }
    sc->mc->notify_cmd = arg;
    (void)mconfig;
    return NULL;
}

static const char *clmd_config_set_msg_cmd(cmd_parms *cmd, void *mconfig, const char *arg)
{
    clmd_srv_conf_t *sc = clmd_config_get(cmd->server);
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err) {
        return err;
    }
    sc->mc->message_cmd = arg;
    (void)mconfig;
    return NULL;
}

static const char *clmd_config_set_dns01_cmd(cmd_parms *cmd, void *mconfig, const char *arg)
{
    clmd_srv_conf_t *sc = clmd_config_get(cmd->server);
    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err) {
        return err;
    }
    kuda_table_set(sc->mc->env, CLMD_KEY_CMD_DNS01, arg);
    (void)mconfig;
    return NULL;
}

static const char *clmd_config_set_cert_file(cmd_parms *cmd, void *mconfig, const char *arg)
{
    clmd_srv_conf_t *sc = clmd_config_get(cmd->server);
    const char *err;
    
    (void)mconfig;
    if (NULL != (err = clmd_section_check(cmd))) return err;
    assert(sc->current);
    sc->current->cert_file = arg;
    return NULL;
}

static const char *clmd_config_set_key_file(cmd_parms *cmd, void *mconfig, const char *arg)
{
    clmd_srv_conf_t *sc = clmd_config_get(cmd->server);
    const char *err;
    
    (void)mconfig;
    if (NULL != (err = clmd_section_check(cmd))) return err;
    assert(sc->current);
    sc->current->pkey_file = arg;
    return NULL;
}

static const char *clmd_config_set_server_status(cmd_parms *cmd, void *dc, const char *value)
{
    clmd_srv_conf_t *sc = clmd_config_get(cmd->server);
    const char *err;

    (void)dc;
    if (!inside_clmd_section(cmd) && (err = clhy_check_cmd_context(cmd, GLOBAL_ONLY))) {
        return err;
    }
    return set_on_off(&sc->mc->server_status_enabled, value, cmd->pool);
}

static const char *clmd_config_set_certificate_status(cmd_parms *cmd, void *dc, const char *value)
{
    clmd_srv_conf_t *sc = clmd_config_get(cmd->server);
    const char *err;

    (void)dc;
    if (!inside_clmd_section(cmd) && (err = clhy_check_cmd_context(cmd, GLOBAL_ONLY))) {
        return err;
    }
    return set_on_off(&sc->mc->certificate_status_enabled, value, cmd->pool);
}


const command_rec clmd_cmds[] = {
    CLHY_INIT_TAKE1("MDCertificateAuthority", clmd_config_set_ca, NULL, RSRC_CONF, 
                  "URL of CA issuing the certificates"),
    CLHY_INIT_TAKE1("MDCertificateAgreement", clmd_config_set_agreement, NULL, RSRC_CONF, 
                  "either 'accepted' or the URL of CA Terms-of-Service agreement you accept"),
    CLHY_INIT_TAKE_ARGV("MDCAChallenges", clmd_config_set_cha_tyes, NULL, RSRC_CONF, 
                      "A list of challenge types to be used."),
    CLHY_INIT_TAKE1("MDCertificateProtocol", clmd_config_set_ca_proto, NULL, RSRC_CONF, 
                  "Protocol used to obtain/renew certificates"),
    CLHY_INIT_TAKE1("MDDriveMode", clmd_config_set_renew_mode, NULL, RSRC_CONF, 
                  "deprecated, older name for MDRenewMode"),
    CLHY_INIT_TAKE1("MDRenewMode", clmd_config_set_renew_mode, NULL, RSRC_CONF, 
                  "Controls how renewal of Managed Domain certificates shall be handled."),
    CLHY_INIT_TAKE_ARGV("MDomain", clmd_config_set_names, NULL, RSRC_CONF, 
                      "A group of server names with one certificate"),
    CLHY_INIT_RAW_ARGS(CLMD_CMD_CLMD_SECTION, clmd_config_sec_start, NULL, RSRC_CONF, 
                     "Container for a managed domain with common settings and certificate."),
    CLHY_INIT_RAW_ARGS(CLMD_CMD_MD2_SECTION, clmd_config_sec_start, NULL, RSRC_CONF, 
                     "Short form for <MDomainSet> container."),
    CLHY_INIT_TAKE_ARGV("MDMember", clmd_config_sec_add_members, NULL, RSRC_CONF, 
                      "Define domain name(s) part of the Managed Domain. Use 'auto' or "
                      "'manual' to enable/disable auto adding names from virtual hosts."),
    CLHY_INIT_TAKE_ARGV("MDMembers", clmd_config_sec_add_members, NULL, RSRC_CONF, 
                      "Define domain name(s) part of the Managed Domain. Use 'auto' or "
                      "'manual' to enable/disable auto adding names from virtual hosts."),
    CLHY_INIT_TAKE1("MDMustStaple", clmd_config_set_must_staple, NULL, RSRC_CONF, 
                  "Enable/Disable the Must-Staple flag for new certificates."),
    CLHY_INIT_TAKE12("MDPortMap", clmd_config_set_port_map, NULL, RSRC_CONF, 
                  "Declare the mapped ports 80 and 443 on the local server. E.g. 80:8000 "
                  "to indicate that the server port 8000 is reachable as port 80 from the "
                  "internet. Use 80:- to indicate that port 80 is not reachable from "
                  "the outside."),
    CLHY_INIT_TAKE_ARGV("MDPrivateKeys", clmd_config_set_pkeys, NULL, RSRC_CONF, 
                  "set the type and parameters for private key generation"),
    CLHY_INIT_TAKE1("MDHttpProxy", clmd_config_set_proxy, NULL, RSRC_CONF, 
                  "URL of a HTTP(S) proxy to use for outgoing connections"),
    CLHY_INIT_TAKE1("MDStoreDir", clmd_config_set_store_dir, NULL, RSRC_CONF, 
                  "the directory for file system storage of managed domain data."),
    CLHY_INIT_TAKE1("MDRenewWindow", clmd_config_set_renew_window, NULL, RSRC_CONF, 
                  "Time length for renewal before certificate expires (defaults to days)"),
    CLHY_INIT_TAKE1("MDRequireHttps", clmd_config_set_require_https, NULL, RSRC_CONF, 
                  "Redirect non-secure requests to the https: equivalent."),
    CLHY_INIT_RAW_ARGS("MDNotifyCmd", clmd_config_set_notify_cmd, NULL, RSRC_CONF, 
                  "Set the command to run when signup/renew of domain is complete."),
    CLHY_INIT_TAKE1("MDBaseServer", clmd_config_set_base_server, NULL, RSRC_CONF, 
                  "Allow managing of base server outside virtual hosts."),
    CLHY_INIT_RAW_ARGS("MDChallengeDns01", clmd_config_set_dns01_cmd, NULL, RSRC_CONF, 
                  "Set the command for setup/teardown of dns-01 challenges"),
    CLHY_INIT_TAKE1("MDCertificateFile", clmd_config_set_cert_file, NULL, RSRC_CONF, 
                  "set the static certificate (chain) file to use for this domain."),
    CLHY_INIT_TAKE1("MDCertificateKeyFile", clmd_config_set_key_file, NULL, RSRC_CONF, 
                  "set the static private key file to use for this domain."),
    CLHY_INIT_TAKE1("MDServerStatus", clmd_config_set_server_status, NULL, RSRC_CONF, 
                  "On to see Managed Domains in server-status."),
    CLHY_INIT_TAKE1("MDCertificateStatus", clmd_config_set_certificate_status, NULL, RSRC_CONF, 
                  "On to see Managed Domain expose /.wwhy/certificate-status."),
    CLHY_INIT_TAKE1("MDWarnWindow", clmd_config_set_warn_window, NULL, RSRC_CONF, 
                  "When less time remains for a certificate, send our/log a warning (defaults to days)"),
    CLHY_INIT_RAW_ARGS("MDMessageCmd", clmd_config_set_msg_cmd, NULL, RSRC_CONF, 
                  "Set the command run when a message about a domain is issued."),

    CLHY_INIT_TAKE1(NULL, NULL, NULL, RSRC_CONF, NULL)
};

kuda_status_t clmd_config_post_config(server_rec *s, kuda_pool_t *p)
{
    clmd_srv_conf_t *sc;
    clmd_capi_conf_t *mc;

    sc = clmd_config_get(s);
    mc = sc->mc;

    mc->hsts_header = NULL;
    if (mc->hsts_max_age > 0) {
        mc->hsts_header = kuda_psprintf(p, "max-age=%d", mc->hsts_max_age);
    }
    
#if CLHY_CAPI_MAGIC_AT_LEAST(20180906, 2)
    if (mc->base_dir == NULL) {
        mc->base_dir = clhy_runtime_dir_relative(p, CLMD_DEFAULT_BASE_DIR);
    }
#endif
    
    return KUDA_SUCCESS;
}

static clmd_srv_conf_t *config_get_int(server_rec *s, kuda_pool_t *p)
{
    clmd_srv_conf_t *sc = (clmd_srv_conf_t *)clhy_get_capi_config(s->capi_config, &clmd_capi);
    clhy_assert(sc);
    if (sc->s != s && p) {
        sc = clmd_config_merge(p, &defconf, sc);
        sc->name = kuda_pstrcat(p, CONF_S_NAME(s), sc->name, NULL);
        sc->mc = clmd_capi_conf_get(p, 1);
        clhy_set_capi_config(s->capi_config, &clmd_capi, sc);
    }
    return sc;
}

clmd_srv_conf_t *clmd_config_get(server_rec *s)
{
    return config_get_int(s, NULL);
}

clmd_srv_conf_t *clmd_config_get_unique(server_rec *s, kuda_pool_t *p)
{
    assert(p);
    return config_get_int(s, p);
}

clmd_srv_conf_t *clmd_config_cget(conn_rec *c)
{
    return clmd_config_get(c->base_server);
}

const char *clmd_config_gets(const clmd_srv_conf_t *sc, clmd_config_var_t var)
{
    switch (var) {
        case CLMD_CONFIG_CA_URL:
            return sc->ca_url? sc->ca_url : defconf.ca_url;
        case CLMD_CONFIG_CA_PROTO:
            return sc->ca_proto? sc->ca_proto : defconf.ca_proto;
        case CLMD_CONFIG_BASE_DIR:
            return sc->mc->base_dir;
        case CLMD_CONFIG_PROXY:
            return sc->mc->proxy_url;
        case CLMD_CONFIG_CA_AGREEMENT:
            return sc->ca_agreement? sc->ca_agreement : defconf.ca_agreement;
        case CLMD_CONFIG_NOTIFY_CMD:
            return sc->mc->notify_cmd;
        default:
            return NULL;
    }
}

int clmd_config_geti(const clmd_srv_conf_t *sc, clmd_config_var_t var)
{
    switch (var) {
        case CLMD_CONFIG_DRIVE_MODE:
            return (sc->renew_mode != DEF_VAL)? sc->renew_mode : defconf.renew_mode;
        case CLMD_CONFIG_LOCAL_80:
            return sc->mc->local_80;
        case CLMD_CONFIG_LOCAL_443:
            return sc->mc->local_443;
        case CLMD_CONFIG_TRANSITIVE:
            return (sc->transitive != DEF_VAL)? sc->transitive : defconf.transitive;
        case CLMD_CONFIG_REQUIRE_HTTPS:
            return (sc->require_https != CLMD_REQUIRE_UNSET)? sc->require_https : defconf.require_https;
        case CLMD_CONFIG_MUST_STAPLE:
            return (sc->must_staple != DEF_VAL)? sc->must_staple : defconf.must_staple;
        default:
            return 0;
    }
}

void clmd_config_get_timespan(const clmd_timeslice_t **pspan, const clmd_srv_conf_t *sc, clmd_config_var_t var)
{
    switch (var) {
        case CLMD_CONFIG_RENEW_WINDOW:
            *pspan = sc->renew_window? sc->renew_window : defconf.renew_window;
            break;
        case CLMD_CONFIG_WARN_WINDOW:
            *pspan = sc->warn_window? sc->warn_window : defconf.warn_window;
            break;
        default:
            break;
    }
}

