/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <kuda_lib.h>
#include <kuda_strings.h>
#include <kuda_tables.h>
#include <kuda_buckets.h>

#include "clmd_crypt.h"
#include "clmd_json.h"
#include "clmd_jws.h"
#include "clmd_log.h"
#include "clmd_util.h"

static int header_set(void *data, const char *key, const char *val)
{
    clmd_json_sets(val, (clmd_json_t *)data, key, NULL);
    return 1;
}

kuda_status_t clmd_jws_sign(clmd_json_t **pmsg, kuda_pool_t *p,
                         const char *payload, size_t len, 
                         struct kuda_table_t *protected, 
                         struct clmd_pkey_t *pkey, const char *key_id)
{
    clmd_json_t *msg, *jprotected;
    const char *prot64, *pay64, *sign64, *sign, *prot;
    kuda_status_t rv = KUDA_SUCCESS;

    *pmsg = NULL;
    
    msg = clmd_json_create(p);

    jprotected = clmd_json_create(p);
    clmd_json_sets("RS256", jprotected, "alg", NULL);
    if (key_id) {
        clmd_json_sets(key_id, jprotected, "kid", NULL);
    }
    else {
        clmd_json_sets(clmd_pkey_get_rsa_e64(pkey, p), jprotected, "jwk", "e", NULL);
        clmd_json_sets("RSA", jprotected, "jwk", "kty", NULL);
        clmd_json_sets(clmd_pkey_get_rsa_n64(pkey, p), jprotected, "jwk", "n", NULL);
    }
    kuda_table_do(header_set, jprotected, protected, NULL);
    prot = clmd_json_writep(jprotected, p, CLMD_JSON_FMT_COMPACT);
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE4, 0, p, "protected: %s",
                  prot ? prot : "<failed to serialize!>");

    if (!prot) {
        rv = KUDA_EINVAL;
    }
    
    if (rv == KUDA_SUCCESS) {
        prot64 = clmd_util_base64url_encode(prot, strlen(prot), p);
        clmd_json_sets(prot64, msg, "protected", NULL);
        pay64 = clmd_util_base64url_encode(payload, len, p);

        clmd_json_sets(pay64, msg, "payload", NULL);
        sign = kuda_psprintf(p, "%s.%s", prot64, pay64);

        rv = clmd_crypt_sign64(&sign64, pkey, p, sign, strlen(sign));
    }

    if (rv == KUDA_SUCCESS) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, 0, p, 
                      "jws pay64=%s\nprot64=%s\nsign64=%s", pay64, prot64, sign64);
        
        clmd_json_sets(sign64, msg, "signature", NULL);
    }
    else {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, rv, p, "jwk signed message");
    } 
    
    *pmsg = (KUDA_SUCCESS == rv)? msg : NULL;
    return rv;
}

kuda_status_t clmd_jws_pkey_thumb(const char **pthumb, kuda_pool_t *p, struct clmd_pkey_t *pkey)
{
    const char *e64, *n64, *s;
    clmd_data data;
    kuda_status_t rv;
    
    e64 = clmd_pkey_get_rsa_e64(pkey, p);
    n64 = clmd_pkey_get_rsa_n64(pkey, p);
    if (!e64 || !n64) {
        return KUDA_EINVAL;
    }

    /* whitespace and order is relevant, since we hand out a digest of this */
    s = kuda_psprintf(p, "{\"e\":\"%s\",\"kty\":\"RSA\",\"n\":\"%s\"}", e64, n64);
    CLMD_DATA_SET_STR(&data, s);
    rv = clmd_crypt_sha256_digest64(pthumb, p, &data);
    return rv;
}
