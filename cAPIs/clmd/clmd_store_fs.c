/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include <kuda_lib.h>
#include <kuda_file_info.h>
#include <kuda_file_io.h>
#include <kuda_fnmatch.h>
#include <kuda_hash.h>
#include <kuda_strings.h>

#include "clmd.h"
#include "clmd_crypt.h"
#include "clmd_json.h"
#include "clmd_log.h"
#include "clmd_store.h"
#include "clmd_store_fs.h"
#include "clmd_util.h"
#include "clmd_version.h"

/**************************************************************************************************/
/* file system based implementation of clmd_store_t */

#define CLMD_STORE_VERSION        3

typedef struct {
    kuda_fileperms_t dir;
    kuda_fileperms_t file;
} perms_t;

typedef struct clmd_store_fs_t clmd_store_fs_t;
struct clmd_store_fs_t {
    clmd_store_t s;
    
    const char *base;       /* base directory of store */
    perms_t def_perms;
    perms_t group_perms[CLMD_SG_COUNT];
    clmd_store_fs_cb *event_cb;
    void *event_baton;
    
    const unsigned char *key;
    kuda_size_t key_len;
    int plain_pkey[CLMD_SG_COUNT];
    
    int port_80;
    int port_443;
};

#define FS_STORE(store)     (clmd_store_fs_t*)(((char*)store)-offsetof(clmd_store_fs_t, s))
#define FS_STORE_JSON       "clmd_store.json"
#define FS_STORE_KLEN       48

static kuda_status_t fs_load(clmd_store_t *store, clmd_store_group_t group, 
                            const char *name, const char *aspect,  
                            clmd_store_vtype_t vtype, void **pvalue, kuda_pool_t *p);
static kuda_status_t fs_save(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group, 
                            const char *name, const char *aspect,  
                            clmd_store_vtype_t vtype, void *value, int create);
static kuda_status_t fs_remove(clmd_store_t *store, clmd_store_group_t group, 
                              const char *name, const char *aspect, 
                              kuda_pool_t *p, int force);
static kuda_status_t fs_purge(clmd_store_t *store, kuda_pool_t *p, 
                             clmd_store_group_t group, const char *name);
static kuda_status_t fs_move(clmd_store_t *store, kuda_pool_t *p, 
                            clmd_store_group_t from, clmd_store_group_t to, 
                            const char *name, int archive);
static kuda_status_t fs_iterate(clmd_store_inspect *inspect, void *baton, clmd_store_t *store, 
                               kuda_pool_t *p, clmd_store_group_t group,  const char *pattern,
                               const char *aspect, clmd_store_vtype_t vtype);
static kuda_status_t fs_iterate_names(clmd_store_inspect *inspect, void *baton, clmd_store_t *store, 
                                     kuda_pool_t *p, clmd_store_group_t group, const char *pattern);

static kuda_status_t fs_get_fname(const char **pfname, 
                                 clmd_store_t *store, clmd_store_group_t group, 
                                 const char *name, const char *aspect, 
                                 kuda_pool_t *p);
static int fs_is_newer(clmd_store_t *store, clmd_store_group_t group1, clmd_store_group_t group2,  
                       const char *name, const char *aspect, kuda_pool_t *p);

static kuda_status_t init_store_file(clmd_store_fs_t *s_fs, const char *fname, 
                                    kuda_pool_t *p, kuda_pool_t *ptemp)
{
    clmd_json_t *json = clmd_json_create(p);
    const char *key64;
    unsigned char *key;
    kuda_status_t rv;
    
    clmd_json_setn(CLMD_STORE_VERSION, json, CLMD_KEY_STORE, CLMD_KEY_VERSION, NULL);

    s_fs->key_len = FS_STORE_KLEN;
    s_fs->key = key = kuda_pcalloc(p, FS_STORE_KLEN);
    if (KUDA_SUCCESS != (rv = clmd_rand_bytes(key, s_fs->key_len, p))) {
        return rv;
    }
        
    key64 = clmd_util_base64url_encode((char *)key, s_fs->key_len, ptemp);
    clmd_json_sets(key64, json, CLMD_KEY_KEY, NULL);
    rv = clmd_json_fcreatex(json, ptemp, CLMD_JSON_FMT_INDENT, fname, CLMD_FPROT_F_UONLY);
    memset((char*)key64, 0, strlen(key64));

    return rv;
}

static kuda_status_t rename_pkey(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, 
                                const char *dir, const char *name, 
                                kuda_filetype_e ftype)
{
    const char *from, *to;
    kuda_status_t rv = KUDA_SUCCESS;
    
    (void)baton;
    (void)ftype;
    if (   CLMD_OK(clmd_util_path_merge(&from, ptemp, dir, name, NULL))
        && CLMD_OK(clmd_util_path_merge(&to, ptemp, dir, CLMD_FN_PRIVKEY, NULL))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, p, "renaming %s/%s to %s", 
                      dir, name, CLMD_FN_PRIVKEY);
        return kuda_file_rename(from, to, ptemp);
    }
    return rv;
}

static kuda_status_t mk_pubcert(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, 
                               const char *dir, const char *name, 
                               kuda_filetype_e ftype)
{
    clmd_cert_t *cert;
    kuda_array_header_t *chain, *pubcert;
    const char *fname, *fpubcert;
    kuda_status_t rv = KUDA_SUCCESS;
    
    (void)baton;
    (void)ftype;
    (void)p;
    if (   CLMD_OK(clmd_util_path_merge(&fpubcert, ptemp, dir, CLMD_FN_PUBCERT, NULL))
        && KUDA_STATUS_IS_ENOENT(rv = clmd_chain_fload(&pubcert, ptemp, fpubcert))
        && CLMD_OK(clmd_util_path_merge(&fname, ptemp, dir, name, NULL))
        && CLMD_OK(clmd_cert_fload(&cert, ptemp, fname))
        && CLMD_OK(clmd_util_path_merge(&fname, ptemp, dir, "chain.pem", NULL))) {
        
        rv = clmd_chain_fload(&chain, ptemp, fname);
        if (KUDA_STATUS_IS_ENOENT(rv)) {
            chain = kuda_array_make(ptemp, 1, sizeof(clmd_cert_t*));
            rv = KUDA_SUCCESS;
        }
        if (KUDA_SUCCESS == rv) {
            pubcert = kuda_array_make(ptemp, chain->nelts + 1, sizeof(clmd_cert_t*));
            KUDA_ARRAY_PUSH(pubcert, clmd_cert_t *) = cert;
            kuda_array_cat(pubcert, chain);
            rv = clmd_chain_fsave(pubcert, ptemp, fpubcert, CLMD_FPROT_F_UONLY);
        }
    }
    return rv;
}

static kuda_status_t upgrade_from_1_0(clmd_store_fs_t *s_fs, kuda_pool_t *p, kuda_pool_t *ptemp)
{
    clmd_store_group_t g;
    kuda_status_t rv = KUDA_SUCCESS;
    
    (void)ptemp;
    /* Migrate pkey.pem -> privkey.pem */
    for (g = CLMD_SG_NONE; g < CLMD_SG_COUNT && KUDA_SUCCESS == rv; ++g) {
        rv = clmd_util_files_do(rename_pkey, s_fs, p, s_fs->base, 
                              clmd_store_group_name(g), "*", "pkey.pem", NULL);
    }
    /* Generate fullcert.pem from cert.pem and chain.pem where missing */
    rv = clmd_util_files_do(mk_pubcert, s_fs, p, s_fs->base, 
                          clmd_store_group_name(CLMD_SG_DOMAINS), "*", CLMD_FN_CERT, NULL);
    rv = clmd_util_files_do(mk_pubcert, s_fs, p, s_fs->base, 
                          clmd_store_group_name(CLMD_SG_ARCHIVE), "*", CLMD_FN_CERT, NULL);
    
    return rv;
}

static kuda_status_t read_store_file(clmd_store_fs_t *s_fs, const char *fname, 
                                    kuda_pool_t *p, kuda_pool_t *ptemp)
{
    clmd_json_t *json;
    const char *key64, *key;
    kuda_status_t rv;
    double store_version;
    
    if (CLMD_OK(clmd_json_readf(&json, p, fname))) {
        store_version = clmd_json_getn(json, CLMD_KEY_STORE, CLMD_KEY_VERSION, NULL);
        if (store_version <= 0.0) {
            /* ok, an old one, compatible to 1.0 */
            store_version = 1.0;
        }
        if (store_version > CLMD_STORE_VERSION) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, 0, p, "version too new: %f", store_version);
            return KUDA_EINVAL;
        }

        key64 = clmd_json_dups(p, json, CLMD_KEY_KEY, NULL);
        if (!key64) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, 0, p, "missing key: %s", CLMD_KEY_KEY);
            return KUDA_EINVAL;
        }
        
        s_fs->key_len = clmd_util_base64url_decode(&key, key64, p);
        s_fs->key = (const unsigned char*)key;
        if (s_fs->key_len != FS_STORE_KLEN) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, 0, p, "key length unexpected: %" KUDA_SIZE_T_FMT, 
                          s_fs->key_len);
            return KUDA_EINVAL;
        }

        /* Need to migrate format? */
        if (store_version < CLMD_STORE_VERSION) {
            if (store_version <= 1.0) {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, p, "migrating store v1 -> v2");
                rv = upgrade_from_1_0(s_fs, p, ptemp);
            }
            if (store_version <= 2.0) {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, p, "migrating store v2 -> v3");
                clmd_json_del(json, CLMD_KEY_VERSION, NULL);
            }
            
            if (KUDA_SUCCESS == rv) {
                clmd_json_setn(CLMD_STORE_VERSION, json, CLMD_KEY_STORE, CLMD_KEY_VERSION, NULL);
                rv = clmd_json_freplace(json, ptemp, CLMD_JSON_FMT_INDENT, fname, CLMD_FPROT_F_UONLY);
           }
           clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, rv, p, "migrated store");
        } 
    }
    return rv;
}

static kuda_status_t setup_store_file(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_store_fs_t *s_fs = baton;
    const char *fname;
    kuda_status_t rv;

    (void)ap;
    s_fs->plain_pkey[CLMD_SG_DOMAINS] = 1;
    s_fs->plain_pkey[CLMD_SG_TMP] = 1;
    
    if (!CLMD_OK(clmd_util_path_merge(&fname, ptemp, s_fs->base, FS_STORE_JSON, NULL))) {
        return rv;
    }
    
read:
    if (CLMD_OK(clmd_util_is_file(fname, ptemp))) {
        rv = read_store_file(s_fs, fname, p, ptemp);
    }
    else if (KUDA_STATUS_IS_ENOENT(rv)
        && KUDA_STATUS_IS_EEXIST(rv = init_store_file(s_fs, fname, p, ptemp))) {
        goto read;
    }
    return rv;
}

kuda_status_t clmd_store_fs_init(clmd_store_t **pstore, kuda_pool_t *p, const char *path)
{
    clmd_store_fs_t *s_fs;
    kuda_status_t rv = KUDA_SUCCESS;
    
    s_fs = kuda_pcalloc(p, sizeof(*s_fs));

    s_fs->s.load = fs_load;
    s_fs->s.save = fs_save;
    s_fs->s.remove = fs_remove;
    s_fs->s.move = fs_move;
    s_fs->s.purge = fs_purge;
    s_fs->s.iterate = fs_iterate;
    s_fs->s.iterate_names = fs_iterate_names;
    s_fs->s.get_fname = fs_get_fname;
    s_fs->s.is_newer = fs_is_newer;
    
    /* by default, everything is only readable by the current user */ 
    s_fs->def_perms.dir = CLMD_FPROT_D_UONLY;
    s_fs->def_perms.file = CLMD_FPROT_F_UONLY;

    /* Account information needs to be accessible to wwhy child processes.
     * private keys are, similar to staging, encrypted. */
    s_fs->group_perms[CLMD_SG_ACCOUNTS].dir = CLMD_FPROT_D_UALL_WREAD;
    s_fs->group_perms[CLMD_SG_ACCOUNTS].file = CLMD_FPROT_F_UALL_WREAD;
    s_fs->group_perms[CLMD_SG_STAGING].dir = CLMD_FPROT_D_UALL_WREAD;
    s_fs->group_perms[CLMD_SG_STAGING].file = CLMD_FPROT_F_UALL_WREAD;
    /* challenges dir and files are readable by all, no secrets involved */ 
    s_fs->group_perms[CLMD_SG_CHALLENGES].dir = CLMD_FPROT_D_UALL_WREAD;
    s_fs->group_perms[CLMD_SG_CHALLENGES].file = CLMD_FPROT_F_UALL_WREAD;

    s_fs->base = kuda_pstrdup(p, path);
    
    if (KUDA_STATUS_IS_ENOENT(rv = clmd_util_is_dir(s_fs->base, p))
        && CLMD_OK(kuda_dir_make_recursive(s_fs->base, s_fs->def_perms.dir, p))) {
        rv = kuda_file_perms_set(s_fs->base, CLMD_FPROT_D_UALL_WREAD);
        if (KUDA_STATUS_IS_ENOTIMPL(rv)) {
            rv = KUDA_SUCCESS;
        }
    }
    
    if ((KUDA_SUCCESS != rv) || !CLMD_OK(clmd_util_pool_vdo(setup_store_file, s_fs, p, NULL))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, p, "init fs store at %s", path);
    }
    *pstore = (rv == KUDA_SUCCESS)? &(s_fs->s) : NULL;
    return rv;
}

kuda_status_t clmd_store_fs_default_perms_set(clmd_store_t *store, 
                                           kuda_fileperms_t file_perms,
                                           kuda_fileperms_t dir_perms)
{
    clmd_store_fs_t *s_fs = FS_STORE(store);
    
    s_fs->def_perms.file = file_perms;
    s_fs->def_perms.dir = dir_perms;
    return KUDA_SUCCESS;
}

kuda_status_t clmd_store_fs_group_perms_set(clmd_store_t *store, clmd_store_group_t group, 
                                         kuda_fileperms_t file_perms,
                                         kuda_fileperms_t dir_perms)
{
    clmd_store_fs_t *s_fs = FS_STORE(store);
    
    if (group >= (sizeof(s_fs->group_perms)/sizeof(s_fs->group_perms[0]))) {
        return KUDA_ENOTIMPL;
    }
    s_fs->group_perms[group].file = file_perms;
    s_fs->group_perms[group].dir = dir_perms;
    return KUDA_SUCCESS;
}

kuda_status_t clmd_store_fs_set_event_cb(struct clmd_store_t *store, clmd_store_fs_cb *cb, void *baton)
{
    clmd_store_fs_t *s_fs = FS_STORE(store);
    
    s_fs->event_cb = cb;
    s_fs->event_baton = baton;
    return KUDA_SUCCESS;
}

static const perms_t *gperms(clmd_store_fs_t *s_fs, clmd_store_group_t group)
{
    if (group >= (sizeof(s_fs->group_perms)/sizeof(s_fs->group_perms[0]))
        || !s_fs->group_perms[group].dir) {
        return &s_fs->def_perms;
    }
    return &s_fs->group_perms[group];
}

static kuda_status_t fs_get_fname(const char **pfname, 
                                 clmd_store_t *store, clmd_store_group_t group, 
                                 const char *name, const char *aspect, 
                                 kuda_pool_t *p)
{
    clmd_store_fs_t *s_fs = FS_STORE(store);
    if (group == CLMD_SG_NONE) {
        return clmd_util_path_merge(pfname, p, s_fs->base, aspect, NULL);
    }
    return clmd_util_path_merge(pfname, p, 
                              s_fs->base, clmd_store_group_name(group), name, aspect, NULL);
}

static kuda_status_t fs_get_dname(const char **pdname, 
                                 clmd_store_t *store, clmd_store_group_t group, 
                                 const char *name, kuda_pool_t *p)
{
    clmd_store_fs_t *s_fs = FS_STORE(store);
    if (group == CLMD_SG_NONE) {
        *pdname = s_fs->base;
        return KUDA_SUCCESS;
    }
    return clmd_util_path_merge(pdname, p, s_fs->base, clmd_store_group_name(group), name, NULL);
}

static void get_pass(const char **ppass, kuda_size_t *plen, 
                     clmd_store_fs_t *s_fs, clmd_store_group_t group)
{
    if (s_fs->plain_pkey[group]) {
        *ppass = NULL;
        *plen = 0;
    }
    else {
        *ppass = (const char *)s_fs->key;
        *plen = s_fs->key_len;
    }
}
 
static kuda_status_t fs_fload(void **pvalue, clmd_store_fs_t *s_fs, const char *fpath, 
                             clmd_store_group_t group, clmd_store_vtype_t vtype, 
                             kuda_pool_t *p, kuda_pool_t *ptemp)
{
    kuda_status_t rv;
    const char *pass;
    kuda_size_t pass_len;
    
    if (pvalue != NULL) {
        switch (vtype) {
            case CLMD_SV_TEXT:
                rv = clmd_text_fread8k((const char **)pvalue, p, fpath);
                break;
            case CLMD_SV_JSON:
                rv = clmd_json_readf((clmd_json_t **)pvalue, p, fpath);
                break;
            case CLMD_SV_CERT:
                rv = clmd_cert_fload((clmd_cert_t **)pvalue, p, fpath);
                break;
            case CLMD_SV_PKEY:
                get_pass(&pass, &pass_len, s_fs, group);
                rv = clmd_pkey_fload((clmd_pkey_t **)pvalue, p, pass, pass_len, fpath);
                break;
            case CLMD_SV_CHAIN:
                rv = clmd_chain_fload((kuda_array_header_t **)pvalue, p, fpath);
                break;
            default:
                rv = KUDA_ENOTIMPL;
                break;
        }
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, rv, ptemp, 
                      "loading type %d from %s", vtype, fpath);
    }
    else { /* check for existence only */
        rv = clmd_util_is_file(fpath, p);
    }
    return rv;
}

static kuda_status_t pfs_load(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_store_fs_t *s_fs = baton;
    const char *fpath, *name, *aspect;
    clmd_store_vtype_t vtype;
    clmd_store_group_t group;
    void **pvalue;
    kuda_status_t rv;
    
    group = (clmd_store_group_t)va_arg(ap, int);
    name = va_arg(ap, const char *);
    aspect = va_arg(ap, const char *);
    vtype = (clmd_store_vtype_t)va_arg(ap, int);
    pvalue= va_arg(ap, void **);
        
    if (CLMD_OK(fs_get_fname(&fpath, &s_fs->s, group, name, aspect, ptemp))) {
        rv = fs_fload(pvalue, s_fs, fpath, group, vtype, p, ptemp);
    }
    return rv;
}

static kuda_status_t dispatch(clmd_store_fs_t *s_fs, clmd_store_fs_ev_t ev, unsigned int group, 
                             const char *fname, kuda_filetype_e ftype, kuda_pool_t *p)
{
    (void)ev;
    if (s_fs->event_cb) {
        return s_fs->event_cb(s_fs->event_baton, &s_fs->s, CLMD_S_FS_EV_CREATED, 
                              group, fname, ftype, p);
    }
    return KUDA_SUCCESS;
}

static kuda_status_t mk_group_dir(const char **pdir, clmd_store_fs_t *s_fs, 
                                 clmd_store_group_t group, const char *name,
                                 kuda_pool_t *p)
{
    const perms_t *perms;
    kuda_status_t rv;
    
    perms = gperms(s_fs, group);

    if (CLMD_OK(fs_get_dname(pdir, &s_fs->s, group, name, p)) && (CLMD_SG_NONE != group)) {
        if (  !CLMD_OK(clmd_util_is_dir(*pdir, p))
            && CLMD_OK(kuda_dir_make_recursive(*pdir, perms->dir, p))) {
            rv = dispatch(s_fs, CLMD_S_FS_EV_CREATED, group, *pdir, KUDA_DIR, p);
        }
        
        if (KUDA_SUCCESS == rv) {
            rv = kuda_file_perms_set(*pdir, perms->dir);
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, 0, p, "mk_group_dir %s perm set", *pdir);
            if (KUDA_STATUS_IS_ENOTIMPL(rv)) {
                rv = KUDA_SUCCESS;
            }
        }
    }
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, 0, p, "mk_group_dir %d %s", group, name);
    return rv;
}

static kuda_status_t pfs_is_newer(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_store_fs_t *s_fs = baton;
    const char *fname1, *fname2, *name, *aspect;
    clmd_store_group_t group1, group2;
    kuda_finfo_t inf1, inf2;
    int *pnewer;
    kuda_status_t rv;
    
    (void)p;
    group1 = (clmd_store_group_t)va_arg(ap, int);
    group2 = (clmd_store_group_t)va_arg(ap, int);
    name = va_arg(ap, const char*);
    aspect = va_arg(ap, const char*);
    pnewer = va_arg(ap, int*);
    
    *pnewer = 0;
    if (   CLMD_OK(fs_get_fname(&fname1, &s_fs->s, group1, name, aspect, ptemp))
        && CLMD_OK(fs_get_fname(&fname2, &s_fs->s, group2, name, aspect, ptemp))
        && CLMD_OK(kuda_stat(&inf1, fname1, KUDA_FINFO_MTIME, ptemp))
        && CLMD_OK(kuda_stat(&inf2, fname2, KUDA_FINFO_MTIME, ptemp))) {
        *pnewer = inf1.mtime > inf2.mtime;
    }

    return rv;
}

 
static int fs_is_newer(clmd_store_t *store, clmd_store_group_t group1, clmd_store_group_t group2,  
                       const char *name, const char *aspect, kuda_pool_t *p)
{
    clmd_store_fs_t *s_fs = FS_STORE(store);
    int newer = 0;
    kuda_status_t rv;
    
    rv = clmd_util_pool_vdo(pfs_is_newer, s_fs, p, group1, group2, name, aspect, &newer, NULL);
    if (KUDA_SUCCESS == rv) {
        return newer;
    }
    return 0;
}

static kuda_status_t pfs_save(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_store_fs_t *s_fs = baton;
    const char *gdir, *dir, *fpath, *name, *aspect;
    clmd_store_vtype_t vtype;
    clmd_store_group_t group;
    void *value;
    int create;
    kuda_status_t rv;
    const perms_t *perms;
    const char *pass;
    kuda_size_t pass_len;
    
    group = (clmd_store_group_t)va_arg(ap, int);
    name = va_arg(ap, const char*);
    aspect = va_arg(ap, const char*);
    vtype = (clmd_store_vtype_t)va_arg(ap, int);
    value = va_arg(ap, void *);
    create = va_arg(ap, int);
    
    perms = gperms(s_fs, group);
    
    if (   CLMD_OK(mk_group_dir(&gdir, s_fs, group, NULL, p)) 
        && CLMD_OK(mk_group_dir(&dir, s_fs, group, name, p))
        && CLMD_OK(clmd_util_path_merge(&fpath, ptemp, dir, aspect, NULL))) {
        
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, 0, ptemp, "storing in %s", fpath);
        switch (vtype) {
            case CLMD_SV_TEXT:
                rv = (create? clmd_text_fcreatex(fpath, perms->file, p, value)
                      : clmd_text_freplace(fpath, perms->file, p, value));
                break;
            case CLMD_SV_JSON:
                rv = (create? clmd_json_fcreatex((clmd_json_t *)value, p, CLMD_JSON_FMT_INDENT, 
                                               fpath, perms->file)
                      : clmd_json_freplace((clmd_json_t *)value, p, CLMD_JSON_FMT_INDENT, 
                                         fpath, perms->file));
                break;
            case CLMD_SV_CERT:
                rv = clmd_cert_fsave((clmd_cert_t *)value, ptemp, fpath, perms->file);
                break;
            case CLMD_SV_PKEY:
                /* Take care that we write private key with access only to the user,
                 * unless we write the key encrypted */
                get_pass(&pass, &pass_len, s_fs, group);
                rv = clmd_pkey_fsave((clmd_pkey_t *)value, ptemp, pass, pass_len, 
                                   fpath, (pass && pass_len)? perms->file : CLMD_FPROT_F_UONLY);
                break;
            case CLMD_SV_CHAIN:
                rv = clmd_chain_fsave((kuda_array_header_t*)value, ptemp, fpath, perms->file);
                break;
            default:
                return KUDA_ENOTIMPL;
        }
        if (KUDA_SUCCESS == rv) {
            rv = dispatch(s_fs, CLMD_S_FS_EV_CREATED, group, fpath, KUDA_REG, p);
        }
    }
    return rv;
}

static kuda_status_t pfs_remove(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_store_fs_t *s_fs = baton;
    const char *dir, *name, *fpath, *groupname, *aspect;
    kuda_status_t rv;
    int force;
    kuda_finfo_t info;
    clmd_store_group_t group;
    
    (void)p;
    group = (clmd_store_group_t)va_arg(ap, int);
    name = va_arg(ap, const char*);
    aspect = va_arg(ap, const char *);
    force = va_arg(ap, int);
    
    groupname = clmd_store_group_name(group);
    
    if (   CLMD_OK(clmd_util_path_merge(&dir, ptemp, s_fs->base, groupname, name, NULL))
        && CLMD_OK(clmd_util_path_merge(&fpath, ptemp, dir, aspect, NULL))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "start remove of clmd %s/%s/%s", 
                      groupname, name, aspect);

        if (!CLMD_OK(kuda_stat(&info, dir, KUDA_FINFO_TYPE, ptemp))) {
            if (KUDA_ENOENT == rv && force) {
                return KUDA_SUCCESS;
            }
            return rv;
        }
    
        rv = kuda_file_remove(fpath, ptemp);
        if (KUDA_ENOENT == rv && force) {
            rv = KUDA_SUCCESS;
        }
    }
    return rv;
}

static kuda_status_t fs_load(clmd_store_t *store, clmd_store_group_t group, 
                            const char *name, const char *aspect,  
                            clmd_store_vtype_t vtype, void **pvalue, kuda_pool_t *p)
{
    clmd_store_fs_t *s_fs = FS_STORE(store);
    return clmd_util_pool_vdo(pfs_load, s_fs, p, group, name, aspect, vtype, pvalue, NULL);
}

static kuda_status_t fs_save(clmd_store_t *store, kuda_pool_t *p, clmd_store_group_t group, 
                            const char *name, const char *aspect,  
                            clmd_store_vtype_t vtype, void *value, int create)
{
    clmd_store_fs_t *s_fs = FS_STORE(store);
    return clmd_util_pool_vdo(pfs_save, s_fs, p, group, name, aspect, 
                            vtype, value, create, NULL);
}

static kuda_status_t fs_remove(clmd_store_t *store, clmd_store_group_t group, 
                              const char *name, const char *aspect, 
                              kuda_pool_t *p, int force)
{
    clmd_store_fs_t *s_fs = FS_STORE(store);
    return clmd_util_pool_vdo(pfs_remove, s_fs, p, group, name, aspect, force, NULL);
}

static kuda_status_t pfs_purge(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_store_fs_t *s_fs = baton;
    const char *dir, *name, *groupname;
    clmd_store_group_t group;
    kuda_status_t rv;
    
    (void)p;
    group = (clmd_store_group_t)va_arg(ap, int);
    name = va_arg(ap, const char*);
    
    groupname = clmd_store_group_name(group);

    if (CLMD_OK(clmd_util_path_merge(&dir, ptemp, s_fs->base, groupname, name, NULL))) {
        /* Remove all files in dir, there should be no sub-dirs */
        rv = clmd_util_rm_recursive(dir, ptemp, 1);
    }
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE2, rv, ptemp, "purge %s/%s (%s)", groupname, name, dir);
    return KUDA_SUCCESS;
}

static kuda_status_t fs_purge(clmd_store_t *store, kuda_pool_t *p, 
                             clmd_store_group_t group, const char *name)
{
    clmd_store_fs_t *s_fs = FS_STORE(store);
    return clmd_util_pool_vdo(pfs_purge, s_fs, p, group, name, NULL);
}

/**************************************************************************************************/
/* iteration */

typedef struct {
    clmd_store_fs_t *s_fs;
    clmd_store_group_t group;
    const char *pattern;
    const char *aspect;
    clmd_store_vtype_t vtype;
    clmd_store_inspect *inspect;
    const char *dirname;
    void *baton;
} inspect_ctx;

static kuda_status_t insp(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, 
                         const char *dir, const char *name, kuda_filetype_e ftype)
{
    inspect_ctx *ctx = baton;
    kuda_status_t rv;
    void *value;
    const char *fpath;
 
    (void)ftype;   
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, 0, ptemp, "inspecting value at: %s/%s", dir, name);
    if (KUDA_SUCCESS == (rv = clmd_util_path_merge(&fpath, ptemp, dir, name, NULL))) {
        rv = fs_fload(&value, ctx->s_fs, fpath, ctx->group, ctx->vtype, p, ptemp);
        if (KUDA_SUCCESS == rv 
            && !ctx->inspect(ctx->baton, ctx->dirname, name, ctx->vtype, value, p)) {
            return KUDA_EOF;
        }
        else if (KUDA_STATUS_IS_ENOENT(rv)) {
            rv = KUDA_SUCCESS;
        }
    } 
    return rv;
}

static kuda_status_t insp_dir(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, 
                             const char *dir, const char *name, kuda_filetype_e ftype)
{
    inspect_ctx *ctx = baton;
    kuda_status_t rv;
    const char *fpath;
 
    (void)ftype;
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, 0, ptemp, "inspecting dir at: %s/%s", dir, name);
    if (CLMD_OK(clmd_util_path_merge(&fpath, p, dir, name, NULL))) {
        ctx->dirname = name;
        rv = clmd_util_files_do(insp, ctx, p, fpath, ctx->aspect, NULL);
        if (KUDA_STATUS_IS_ENOENT(rv)) {
            rv = KUDA_SUCCESS;
        }
    } 
    return rv;
}

static kuda_status_t fs_iterate(clmd_store_inspect *inspect, void *baton, clmd_store_t *store, 
                               kuda_pool_t *p, clmd_store_group_t group, const char *pattern, 
                               const char *aspect, clmd_store_vtype_t vtype)
{
    const char *groupname;
    kuda_status_t rv;
    inspect_ctx ctx;
    
    ctx.s_fs = FS_STORE(store);
    ctx.group = group;
    ctx.pattern = pattern;
    ctx.aspect = aspect;
    ctx.vtype = vtype;
    ctx.inspect = inspect;
    ctx.baton = baton;
    groupname = clmd_store_group_name(group);

    rv = clmd_util_files_do(insp_dir, &ctx, p, ctx.s_fs->base, groupname, pattern, NULL);
    
    return rv;
}

static kuda_status_t insp_name(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, 
                              const char *dir, const char *name, kuda_filetype_e ftype)
{
    inspect_ctx *ctx = baton;
    
    (void)ftype;
    (void)p;
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE3, 0, ptemp, "inspecting name at: %s/%s", dir, name);
    return ctx->inspect(ctx->baton, dir, name, 0, NULL, ptemp);
}

static kuda_status_t fs_iterate_names(clmd_store_inspect *inspect, void *baton, clmd_store_t *store, 
                                     kuda_pool_t *p, clmd_store_group_t group, const char *pattern)
{
    const char *groupname;
    kuda_status_t rv;
    inspect_ctx ctx;
    
    ctx.s_fs = FS_STORE(store);
    ctx.group = group;
    ctx.pattern = pattern;
    ctx.inspect = inspect;
    ctx.baton = baton;
    groupname = clmd_store_group_name(group);

    rv = clmd_util_files_do(insp_name, &ctx, p, ctx.s_fs->base, groupname, pattern, NULL);
    
    return rv;
}

/**************************************************************************************************/
/* moving */

static kuda_status_t pfs_move(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_store_fs_t *s_fs = baton;
    const char *name, *from_group, *to_group, *from_dir, *to_dir, *arch_dir, *dir;
    clmd_store_group_t from, to;
    int archive;
    kuda_status_t rv;
    
    (void)p;
    from = (clmd_store_group_t)va_arg(ap, int);
    to = (clmd_store_group_t)va_arg(ap, int);
    name = va_arg(ap, const char*);
    archive = va_arg(ap, int);
    
    from_group = clmd_store_group_name(from);
    to_group = clmd_store_group_name(to);
    if (!strcmp(from_group, to_group)) {
        return KUDA_EINVAL;
    }

    if (   !CLMD_OK(clmd_util_path_merge(&from_dir, ptemp, s_fs->base, from_group, name, NULL))
        || !CLMD_OK(clmd_util_path_merge(&to_dir, ptemp, s_fs->base, to_group, name, NULL))) {
        goto out;
    }
    
    if (!CLMD_OK(clmd_util_is_dir(from_dir, ptemp))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, ptemp, "source is no dir: %s", from_dir);
        goto out;
    }
    
    if (CLMD_OK(archive? clmd_util_is_dir(to_dir, ptemp) : KUDA_ENOENT)) {
        int n = 1;
        const char *narch_dir;

        if (    !CLMD_OK(clmd_util_path_merge(&dir, ptemp, s_fs->base, 
                                          clmd_store_group_name(CLMD_SG_ARCHIVE), NULL))
            || !CLMD_OK(kuda_dir_make_recursive(dir, CLMD_FPROT_D_UONLY, ptemp))
            || !CLMD_OK(clmd_util_path_merge(&arch_dir, ptemp, dir, name, NULL))) {
            goto out;
        }
        
#ifdef WIN32
        /* WIN32 and handling of files/dirs. What can one say? */
        
        while (n < 1000) {
            narch_dir = kuda_psprintf(ptemp, "%s.%d", arch_dir, n);
            rv = clmd_util_is_dir(narch_dir, ptemp);
            if (KUDA_STATUS_IS_ENOENT(rv)) {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, rv, ptemp, "using archive dir: %s", 
                              narch_dir);
                break;
            }
            else {
                ++n;
                narch_dir = NULL;
            }
        }

#else   /* ifdef WIN32 */

        while (n < 1000) {
            narch_dir = kuda_psprintf(ptemp, "%s.%d", arch_dir, n);
            if (CLMD_OK(kuda_dir_make(narch_dir, CLMD_FPROT_D_UONLY, ptemp))) {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, rv, ptemp, "using archive dir: %s", 
                              narch_dir);
                break;
            }
            else if (KUDA_EEXIST == rv) {
                ++n;
                narch_dir = NULL;
            }
            else {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, ptemp, "creating archive dir: %s", 
                              narch_dir);
                goto out;
            }
        }
         
#endif   /* ifdef WIN32 (else part) */
        
        if (!narch_dir) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, ptemp, "ran out of numbers less than 1000 "
                          "while looking for an available one in %s to archive the data "
                          "from %s. Either something is generally wrong or you need to "
                          "clean up some of those directories.", arch_dir, from_dir);
            rv = KUDA_EGENERAL;
            goto out;
        }
        
        if (!CLMD_OK(kuda_file_rename(to_dir, narch_dir, ptemp))) {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, ptemp, "rename from %s to %s", 
                              to_dir, narch_dir);
                goto out;
        }
        if (!CLMD_OK(kuda_file_rename(from_dir, to_dir, ptemp))) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, ptemp, "rename from %s to %s", 
                          from_dir, to_dir);
            kuda_file_rename(narch_dir, to_dir, ptemp);
            goto out;
        }
        if (CLMD_OK(dispatch(s_fs, CLMD_S_FS_EV_MOVED, to, to_dir, KUDA_DIR, ptemp))) {
            rv = dispatch(s_fs, CLMD_S_FS_EV_MOVED, CLMD_SG_ARCHIVE, narch_dir, KUDA_DIR, ptemp);
        }
    }
    else if (KUDA_STATUS_IS_ENOENT(rv)) {
        if (KUDA_SUCCESS != (rv = kuda_file_rename(from_dir, to_dir, ptemp))) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, ptemp, "rename from %s to %s", 
                          from_dir, to_dir);
            goto out;
        }
    }
    else {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, ptemp, "target is no dir: %s", to_dir);
        goto out;
    }
    
out:
    return rv;
}

static kuda_status_t fs_move(clmd_store_t *store, kuda_pool_t *p, 
                            clmd_store_group_t from, clmd_store_group_t to, 
                            const char *name, int archive)
{
    clmd_store_fs_t *s_fs = FS_STORE(store);
    return clmd_util_pool_vdo(pfs_move, s_fs, p, from, to, name, archive, NULL);
}
