/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_acme_authz_h
#define capi_clmd_clmd_acme_authz_h

struct kuda_array_header_t;
struct kuda_table_t;
struct clmd_acme_t;
struct clmd_acme_acct_t;
struct clmd_json_t;
struct clmd_store_t;
struct clmd_pkey_spec_t;
struct clmd_result_t;

typedef struct clmd_acme_challenge_t clmd_acme_challenge_t;

/**************************************************************************************************/
/* authorization request for a specific domain name */

#define CLMD_AUTHZ_TYPE_DNS01         "dns-01"
#define CLMD_AUTHZ_TYPE_HTTP01        "http-01"
#define CLMD_AUTHZ_TYPE_TLSALPN01     "tls-alpn-01"

typedef enum {
    CLMD_ACME_AUTHZ_S_UNKNOWN,
    CLMD_ACME_AUTHZ_S_PENDING,
    CLMD_ACME_AUTHZ_S_VALID,
    CLMD_ACME_AUTHZ_S_INVALID,
} clmd_acme_authz_state_t;

typedef struct clmd_acme_authz_t clmd_acme_authz_t;

struct clmd_acme_authz_t {
    const char *domain;
    const char *url;
    clmd_acme_authz_state_t state;
    kuda_time_t expires;
    struct clmd_json_t *resource;
};

#define CLMD_FN_HTTP01            "acme-http-01.txt"
#define CLMD_FN_TLSSNI01_CERT     "acme-tls-sni-01.cert.pem"
#define CLMD_FN_TLSSNI01_PKEY     "acme-tls-sni-01.key.pem"
#define CLMD_FN_TLSALPN01_CERT    "acme-tls-alpn-01.cert.pem"
#define CLMD_FN_TLSALPN01_PKEY    "acme-tls-alpn-01.key.pem"


clmd_acme_authz_t *clmd_acme_authz_create(kuda_pool_t *p);

/* authz interaction with ACME server */
kuda_status_t clmd_acme_authz_register(struct clmd_acme_authz_t **pauthz, struct clmd_acme_t *acme,
                                    const char *domain, kuda_pool_t *p);

kuda_status_t clmd_acme_authz_retrieve(clmd_acme_t *acme, kuda_pool_t *p, const char *url, 
                                    clmd_acme_authz_t **pauthz);
kuda_status_t clmd_acme_authz_update(clmd_acme_authz_t *authz, struct clmd_acme_t *acme, kuda_pool_t *p);

kuda_status_t clmd_acme_authz_respond(clmd_acme_authz_t *authz, struct clmd_acme_t *acme, 
                                   struct clmd_store_t *store, kuda_array_header_t *challenges, 
                                   struct clmd_pkey_spec_t *key_spec,
                                   kuda_array_header_t *acme_tls_1_domains, 
                                   struct kuda_table_t *env,
                                   kuda_pool_t *p, const char **setup_token,
                                   struct clmd_result_t *result);

kuda_status_t clmd_acme_authz_teardown(struct clmd_store_t *store, const char *setup_token, 
                                    struct kuda_table_t *env, kuda_pool_t *p);

#endif /* clmd_acme_authz_h */
