/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_util_h
#define capi_clmd_clmd_util_h

#include <stdio.h>
#include <kuda_file_io.h>

struct kuda_array_header_t;
struct kuda_table_t;

/**************************************************************************************************/
/* pool utils */

typedef kuda_status_t clmd_util_action(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp);
typedef kuda_status_t clmd_util_vaction(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap);

kuda_status_t clmd_util_pool_do(clmd_util_action *cb, void *baton, kuda_pool_t *p); 
kuda_status_t clmd_util_pool_vdo(clmd_util_vaction *cb, void *baton, kuda_pool_t *p, ...); 

/**************************************************************************************************/
/* data chunks */

typedef struct clmd_data clmd_data;
struct clmd_data {
    const char *data;
    kuda_size_t len;
};

clmd_data *clmd_data_create(kuda_pool_t *p, const char *data, kuda_size_t len);

kuda_status_t clmd_data_to_hex(const char **phex, char separator,
                            kuda_pool_t *p, const clmd_data *data);

/**************************************************************************************************/
/* string related */
char *clmd_util_str_tolower(char *s);

/**
 * Return != 0 iff array is either NULL or empty 
 */ 
int clmd_array_is_empty(const struct kuda_array_header_t *array);

int clmd_array_str_index(const struct kuda_array_header_t *array, const char *s, 
                       int start, int case_sensitive);

int clmd_array_str_eq(const struct kuda_array_header_t *a1, 
                    const struct kuda_array_header_t *a2, int case_sensitive);

struct kuda_array_header_t *clmd_array_str_clone(kuda_pool_t *p, struct kuda_array_header_t *array);

/**
 * Create a new array with duplicates removed.
 */
struct kuda_array_header_t *clmd_array_str_compact(kuda_pool_t *p, struct kuda_array_header_t *src,
                                                int case_sensitive);

/**
 * Create a new array with all occurances of <exclude> removed.
 */
struct kuda_array_header_t *clmd_array_str_remove(kuda_pool_t *p, struct kuda_array_header_t *src, 
                                               const char *exclude, int case_sensitive);

int clmd_array_str_add_missing(struct kuda_array_header_t *dest, 
                             struct kuda_array_header_t *src, int case_sensitive);

/**************************************************************************************************/
/* process execution */
kuda_status_t clmd_util_exec(kuda_pool_t *p, const char *cmd, const char * const *argv,
                          int *exit_code);

/**************************************************************************************************/
/* dns name check */

/**
 * Is a host/domain name using allowed characters. Not a wildcard.
 * @param domain     name to check
 * @param need_fqdn  iff != 0, check that domain contains '.'
 * @return != 0 iff domain looks like  a non-wildcard, legal DNS domain name.
 */
int clmd_dns_is_name(kuda_pool_t *p, const char *domain, int need_fqdn);

/**
 * Check if the given domain is a valid wildcard DNS name, e.g. *.example.org
 * @param domain    name to check
 * @return != 0 iff domain is a DNS wildcard.
 */
int clmd_dns_is_wildcard(kuda_pool_t *p, const char *domain);

/**
 * Determine iff pattern matches domain, including case-ignore and wildcard domains.
 * It is assumed that both names follow dns syntax.
 * @return != 0 iff pattern matches domain
 */ 
int clmd_dns_matches(const char *pattern, const char *domain);

/**
 * Create a new array with the minimal set out of the given domain names that match all
 * of them. If none of the domains is a wildcard, only duplicates are removed.
 * If domains contain a wildcard, any name matching the wildcard will be removed.
 */
struct kuda_array_header_t *clmd_dns_make_minimal(kuda_pool_t *p, 
                                               struct kuda_array_header_t *domains);

/**
 * Determine if the given domains cover the name, including wildcard matching.
 * @return != 0 iff name is matched by list of domains
 */
int clmd_dns_domains_match(const kuda_array_header_t *domains, const char *name);

/**************************************************************************************************/
/* file system related */

struct kuda_file_t;
struct kuda_finfo_t;

kuda_status_t clmd_util_fopen(FILE **pf, const char *fn, const char *mode);

kuda_status_t clmd_util_fcreatex(struct kuda_file_t **pf, const char *fn, 
                              kuda_fileperms_t perms, kuda_pool_t *p);

kuda_status_t clmd_util_path_merge(const char **ppath, kuda_pool_t *p, ...);

kuda_status_t clmd_util_is_dir(const char *path, kuda_pool_t *pool);
kuda_status_t clmd_util_is_file(const char *path, kuda_pool_t *pool);
int clmd_file_exists(const char *fname, kuda_pool_t *p);

typedef kuda_status_t clmd_util_file_cb(void *baton, struct kuda_file_t *f, kuda_pool_t *p);

kuda_status_t clmd_util_freplace(const char *fpath, kuda_fileperms_t perms, kuda_pool_t *p, 
                              clmd_util_file_cb *write, void *baton);

/** 
 * Remove a file/directory and all files/directories contain up to max_level. If max_level == 0,
 * only an empty directory or a file can be removed.
 */
kuda_status_t clmd_util_rm_recursive(const char *fpath, kuda_pool_t *p, int max_level);

typedef kuda_status_t clmd_util_fdo_cb(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, 
                                         const char *dir, const char *name, 
                                         kuda_filetype_e ftype);
                                         
kuda_status_t clmd_util_files_do(clmd_util_fdo_cb *cb, void *baton, kuda_pool_t *p, 
                              const char *path, ...);

/**
 * Depth first traversal of directory tree starting at path.
 */
kuda_status_t clmd_util_tree_do(clmd_util_fdo_cb *cb, void *baton, kuda_pool_t *p, 
                             const char *path, int follow_links);

kuda_status_t clmd_util_ftree_remove(const char *path, kuda_pool_t *p);

kuda_status_t clmd_text_fread8k(const char **ptext, kuda_pool_t *p, const char *fpath);
kuda_status_t clmd_text_fcreatex(const char *fpath, kuda_fileperms_t 
                              perms, kuda_pool_t *p, const char *text);
kuda_status_t clmd_text_freplace(const char *fpath, kuda_fileperms_t perms, 
                              kuda_pool_t *p, const char *text); 

/**************************************************************************************************/
/* base64 url encodings */
const char *clmd_util_base64url_encode(const char *data, 
                                     kuda_size_t len, kuda_pool_t *pool);
kuda_size_t clmd_util_base64url_decode(const char **decoded, const char *encoded, 
                                    kuda_pool_t *pool);

/**************************************************************************************************/
/* http/url related */
const char *clmd_util_schemify(kuda_pool_t *p, const char *s, const char *def_scheme);

kuda_status_t clmd_util_abs_uri_check(kuda_pool_t *p, const char *s, const char **perr);
kuda_status_t clmd_util_abs_http_uri_check(kuda_pool_t *p, const char *uri, const char **perr);

const char *clmd_link_find_relation(const struct kuda_table_t *headers, 
                                  kuda_pool_t *pool, const char *relation);

/**************************************************************************************************/
/* retry logic */

typedef kuda_status_t clmd_util_try_fn(void *baton, int i);

kuda_status_t clmd_util_try(clmd_util_try_fn *fn, void *baton, int ignore_errs,  
                         kuda_interval_time_t timeout, kuda_interval_time_t start_delay, 
                         kuda_interval_time_t max_delay, int backoff);

#endif /* clmd_util_h */
