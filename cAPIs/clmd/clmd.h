/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_h
#define capi_clmd_clmd_h

#include "clmd_time.h"
#include "clmd_version.h"

struct kuda_array_header_t;
struct kuda_hash_t;
struct clmd_json_t;
struct clmd_cert_t;
struct clmd_pkey_t;
struct clmd_store_t;
struct clmd_srv_conf_t;
struct clmd_pkey_spec_t;

#define CLMD_PKEY_RSA_BITS_MIN       2048
#define CLMD_PKEY_RSA_BITS_DEF       2048

/* Minimum age for the HSTS header (RFC 6797), considered appropriate by Mozilla Security */
#define CLMD_HSTS_HEADER             "Strict-Transport-Security"
#define CLMD_HSTS_MAX_AGE_DEFAULT    15768000

#define PROTO_ACME_TLS_1        "acme-tls/1"

#define CLMD_TIME_LIFE_NORM           (kuda_time_from_sec(100 * CLMD_SECS_PER_DAY))
#define CLMD_TIME_RENEW_WINDOW_DEF    (kuda_time_from_sec(33 * CLMD_SECS_PER_DAY))
#define CLMD_TIME_WARN_WINDOW_DEF     (kuda_time_from_sec(10 * CLMD_SECS_PER_DAY))

typedef enum {
    CLMD_S_UNKNOWN = 0,               /* CLMD has not been analysed yet */
    CLMD_S_INCOMPLETE = 1,            /* CLMD is missing necessary information, cannot go live */
    CLMD_S_COMPLETE = 2,              /* CLMD has all necessary information, can go live */
    CLMD_S_EXPIRED_DEPRECATED = 3,    /* deprecated */
    CLMD_S_ERROR = 4,                 /* CLMD data is flawed, unable to be processed as is */ 
    CLMD_S_MISSING_INFORMATION = 5,   /* User has not agreed to ToS */
} clmd_state_t;

typedef enum {
    CLMD_REQUIRE_UNSET = -1,
    CLMD_REQUIRE_OFF,
    CLMD_REQUIRE_TEMPORARY,
    CLMD_REQUIRE_PERMANENT,
} clmd_require_t;

typedef enum {
    CLMD_SV_TEXT,
    CLMD_SV_JSON,
    CLMD_SV_CERT,
    CLMD_SV_PKEY,
    CLMD_SV_CHAIN,
} clmd_store_vtype_t;

typedef enum {
    CLMD_SG_NONE,
    CLMD_SG_ACCOUNTS,
    CLMD_SG_CHALLENGES,
    CLMD_SG_DOMAINS,
    CLMD_SG_STAGING,
    CLMD_SG_ARCHIVE,
    CLMD_SG_TMP,
    CLMD_SG_COUNT,
} clmd_store_group_t;

typedef enum {
    CLMD_RENEW_DEFAULT = -1,          /* default value */
    CLMD_RENEW_MANUAL,                /* manually triggered renewal of certificate */
    CLMD_RENEW_AUTO,                  /* automatic process performed by wwhy */
    CLMD_RENEW_ALWAYS,                /* always renewed by wwhy, even if not necessary */
} clmd_renew_mode_t;

typedef struct clmd_t clmd_t;
struct clmd_t {
    const char *name;               /* unique name of this CLMD */
    struct kuda_array_header_t *domains; /* all DNS names this CLMD includes */
    struct kuda_array_header_t *contacts;   /* list of contact uris, e.g. mailto:xxx */

    int transitive;                 /* != 0 iff VirtualHost names/aliases are auto-added */
    clmd_require_t require_https;     /* Iff https: is required for this CLMD */
    
    int renew_mode;                 /* mode of obtaining credentials */
    struct clmd_pkey_spec_t *pkey_spec;/* specification for generating new private keys */
    int must_staple;                /* certificates should set the OCSP Must Staple extension */
    const clmd_timeslice_t *renew_window;  /* time before expiration that starts renewal */
    const clmd_timeslice_t *warn_window;   /* time before expiration that warnings are sent out */
    
    const char *ca_url;             /* url of CA certificate service */
    const char *ca_proto;           /* protocol used vs CA (e.g. ACME) */
    const char *ca_account;         /* account used at CA */
    const char *ca_agreement;       /* accepted agreement uri between CA and user */ 
    struct kuda_array_header_t *ca_challenges; /* challenge types configured for this CLMD */
    const char *cert_file;          /* != NULL iff pubcert file explicitly configured */
    const char *pkey_file;          /* != NULL iff privkey file explicitly configured */
    
    clmd_state_t state;               /* state of this CLMD */
    
    struct kuda_array_header_t *acme_tls_1_domains; /* domains supporting "acme-tls/1" protocol */
    
    const struct clmd_srv_conf_t *sc; /* server config where it was defined or NULL */
    const char *defn_name;          /* config file this CLMD was defined */
    unsigned defn_line_number;      /* line number of definition */
    
    const char *configured_name;    /* name this CLMD was configured with, if different */
};

#define CLMD_KEY_ACCOUNT          "account"
#define CLMD_KEY_ACME_TLS_1       "acme-tls/1"
#define CLMD_KEY_ACTIVITY         "activity"
#define CLMD_KEY_AGREEMENT        "agreement"
#define CLMD_KEY_AUTHORIZATIONS   "authorizations"
#define CLMD_KEY_BITS             "bits"
#define CLMD_KEY_CA               "ca"
#define CLMD_KEY_CA_URL           "ca-url"
#define CLMD_KEY_CERT             "cert"
#define CLMD_KEY_CERT_FILE        "cert-file"
#define CLMD_KEY_CERTIFICATE      "certificate"
#define CLMD_KEY_CHALLENGE        "challenge"
#define CLMD_KEY_CHALLENGES       "challenges"
#define CLMD_KEY_CMD_DNS01        "cmd-dns-01"
#define CLMD_KEY_COMPLETE         "complete"
#define CLMD_KEY_CONTACT          "contact"
#define CLMD_KEY_CONTACTS         "contacts"
#define CLMD_KEY_CSR              "csr"
#define CLMD_KEY_DETAIL           "detail"
#define CLMD_KEY_DISABLED         "disabled"
#define CLMD_KEY_DIR              "dir"
#define CLMD_KEY_DOMAIN           "domain"
#define CLMD_KEY_DOMAINS          "domains"
#define CLMD_KEY_ENTRIES          "entries"
#define CLMD_KEY_ERRORED          "errored"
#define CLMD_KEY_ERRORS           "errors"
#define CLMD_KEY_EXPIRES          "expires"
#define CLMD_KEY_FINALIZE         "finalize"
#define CLMD_KEY_FINISHED         "finished"
#define CLMD_KEY_HTTP             "http"
#define CLMD_KEY_HTTPS            "https"
#define CLMD_KEY_ID               "id"
#define CLMD_KEY_IDENTIFIER       "identifier"
#define CLMD_KEY_KEY              "key"
#define CLMD_KEY_KEYAUTHZ         "keyAuthorization"
#define CLMD_KEY_LAST             "last"
#define CLMD_KEY_LAST_RUN         "last-run"
#define CLMD_KEY_LOCATION         "location"
#define CLMD_KEY_LOG              "log"
#define CLMD_KEY_MDS              "managed-domains"
#define CLMD_KEY_MESSAGE          "message"
#define CLMD_KEY_MUST_STAPLE      "must-staple"
#define CLMD_KEY_NAME             "name"
#define CLMD_KEY_NEXT_RUN         "next-run"
#define CLMD_KEY_NOTIFIED         "notified"
#define CLMD_KEY_ORDERS           "orders"
#define CLMD_KEY_PERMANENT        "permanent"
#define CLMD_KEY_PKEY             "privkey"
#define CLMD_KEY_PKEY_FILE        "pkey-file"
#define CLMD_KEY_PROBLEM          "problem"
#define CLMD_KEY_PROTO            "proto"
#define CLMD_KEY_READY            "ready"
#define CLMD_KEY_REGISTRATION     "registration"
#define CLMD_KEY_RENEW            "renew"
#define CLMD_KEY_RENEW_MODE       "renew-mode"
#define CLMD_KEY_RENEWAL          "renewal"
#define CLMD_KEY_RENEWING         "renewing"
#define CLMD_KEY_RENEW_WINDOW     "renew-window"
#define CLMD_KEY_REQUIRE_HTTPS    "require-https"
#define CLMD_KEY_RESOURCE         "resource"
#define CLMD_KEY_SERIAL           "serial"
#define CLMD_KEY_SHA256_FINGERPRINT  "sha256-fingerprint"
#define CLMD_KEY_STATE            "state"
#define CLMD_KEY_STATUS           "status"
#define CLMD_KEY_STORE            "store"
#define CLMD_KEY_TEMPORARY        "temporary"
#define CLMD_KEY_TOKEN            "token"
#define CLMD_KEY_TOTAL            "total"
#define CLMD_KEY_TRANSITIVE       "transitive"
#define CLMD_KEY_TYPE             "type"
#define CLMD_KEY_URL              "url"
#define CLMD_KEY_URI              "uri"
#define CLMD_KEY_VALID_FROM       "valid-from"
#define CLMD_KEY_VALID_UNTIL      "valid-until"
#define CLMD_KEY_VALUE            "value"
#define CLMD_KEY_VERSION          "version"
#define CLMD_KEY_WHEN             "when"
#define CLMD_KEY_WARN_WINDOW      "warn-window"

#define CLMD_FN_MD                "clmd.json"
#define CLMD_FN_JOB               "job.json"
#define CLMD_FN_PRIVKEY           "privkey.pem"
#define CLMD_FN_PUBCERT           "pubcert.pem"
#define CLMD_FN_CERT              "cert.pem"
#define CLMD_FN_WWHY_JSON         "wwhy.json"

#define CLMD_FN_FALLBACK_PKEY     "fallback-privkey.pem"
#define CLMD_FN_FALLBACK_CERT     "fallback-cert.pem"

/* Check if a string member of a new CLMD (n) has 
 * a value and if it differs from the old CLMD o
 */
#define CLMD_VAL_UPDATE(n,o,s)    ((n)->s != (o)->s)
#define CLMD_SVAL_UPDATE(n,o,s)   ((n)->s && (!(o)->s || strcmp((n)->s, (o)->s)))

/**
 * Determine if the Managed Domain contains a specific domain name.
 */
int clmd_contains(const clmd_t *clmd, const char *domain, int case_sensitive);

/**
 * Determine if the names of the two managed domains overlap.
 */
int clmd_domains_overlap(const clmd_t *md1, const clmd_t *md2);

/**
 * Determine if the domain names are equal.
 */
int clmd_equal_domains(const clmd_t *md1, const clmd_t *md2, int case_sensitive);

/**
 * Determine if the domains in md1 contain all domains of md2.
 */
int clmd_contains_domains(const clmd_t *md1, const clmd_t *md2);

/**
 * Get one common domain name of the two managed domains or NULL.
 */
const char *clmd_common_name(const clmd_t *md1, const clmd_t *md2);

/**
 * Get the number of common domains.
 */
kuda_size_t clmd_common_name_count(const clmd_t *md1, const clmd_t *md2);

/**
 * Look up a managed domain by its name.
 */
clmd_t *clmd_get_by_name(struct kuda_array_header_t *mds, const char *name);

/**
 * Look up a managed domain by a DNS name it contains.
 */
clmd_t *clmd_get_by_domain(struct kuda_array_header_t *mds, const char *domain);

/**
 * Find a managed domain, different from the given one, that has overlaps
 * in the domain list.
 */
clmd_t *clmd_get_by_dns_overlap(struct kuda_array_header_t *mds, const clmd_t *clmd);

/**
 * Find the managed domain in the list that, for the given clmd, 
 * has the same name, or the most number of overlaps in domains
 */
clmd_t *clmd_find_closest_match(struct kuda_array_header_t *mds, const clmd_t *clmd);

/**
 * Create and empty clmd record, structures initialized.
 */
clmd_t *clmd_create_empty(kuda_pool_t *p);

/**
 * Create a managed domain, given a list of domain names.
 */
clmd_t *clmd_create(kuda_pool_t *p, struct kuda_array_header_t *domains);

/**
 * Deep copy an clmd record into another pool.
 */
clmd_t *clmd_clone(kuda_pool_t *p, const clmd_t *src);

/**
 * Shallow copy an clmd record into another pool.
 */
clmd_t *clmd_copy(kuda_pool_t *p, const clmd_t *src);

/** 
 * Convert the managed domain into a JSON representation and vice versa. 
 *
 * This reads and writes the following information: name, domains, ca_url, ca_proto and state.
 */
struct clmd_json_t *clmd_to_json (const clmd_t *clmd, kuda_pool_t *p);
clmd_t *clmd_from_json(struct clmd_json_t *json, kuda_pool_t *p);

int clmd_is_covered_by_alt_names(const clmd_t *clmd, const struct kuda_array_header_t* alt_names);

#define LE_ACMEv1_PROD      "https://acme-v01.api.letsencrypt.org/directory"
#define LE_ACMEv1_STAGING   "https://acme-staging.api.letsencrypt.org/directory"

#define LE_ACMEv2_PROD      "https://acme-v02.api.letsencrypt.org/directory"  
#define LE_ACMEv2_STAGING   "https://acme-staging-v02.api.letsencrypt.org/directory"


/**************************************************************************************************/
/* domain credentials */

typedef struct clmd_pubcert_t clmd_pubcert_t;
struct clmd_pubcert_t {
    struct kuda_array_header_t *certs;     /* chain of const clmd_cert*, leaf cert first */
    struct kuda_array_header_t *alt_names; /* alt-names of leaf cert */
    const char *cert_file;                 /* file path of chain */
    const char *key_file;                  /* file path of key for leaf cert */
};

#define CLMD_OK(c)                    (KUDA_SUCCESS == (rv = c))

#endif /* capi_clmd_clmd_h */
