/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_reg_h
#define capi_clmd_clmd_reg_h

struct kuda_hash_t;
struct kuda_array_header_t;
struct clmd_store_t;
struct clmd_pkey_t;
struct clmd_cert_t;
struct clmd_result_t;

/**
 * A registry for managed domains with a clmd_store_t as persistence.
 *
 */
typedef struct clmd_reg_t clmd_reg_t;

/**
 * Create the CLMD registry, using the pool and store.
 */
kuda_status_t clmd_reg_create(clmd_reg_t **preg, kuda_pool_t *pm, struct clmd_store_t *store,
                           const char *proxy_url);

struct clmd_store_t *clmd_reg_store_get(clmd_reg_t *reg);

kuda_status_t clmd_reg_set_props(clmd_reg_t *reg, kuda_pool_t *p, int can_http, int can_https);

/**
 * Add a new clmd to the registry. This will check the name for uniqueness and
 * that domain names do not overlap with already existing mds.
 */
kuda_status_t clmd_reg_add(clmd_reg_t *reg, clmd_t *clmd, kuda_pool_t *p);

/**
 * Find the clmd, if any, that contains the given domain name. 
 * NULL if none found.
 */
clmd_t *clmd_reg_find(clmd_reg_t *reg, const char *domain, kuda_pool_t *p);

/**
 * Find one clmd, which domain names overlap with the given clmd and that has a different
 * name. There may be more than one existing clmd that overlaps. It is not defined
 * which one will be returned. 
 */
clmd_t *clmd_reg_find_overlap(clmd_reg_t *reg, const clmd_t *clmd, const char **pdomain, kuda_pool_t *p);

/**
 * Get the clmd with the given unique name. NULL if it does not exist.
 * Will update the clmd->state.
 */
clmd_t *clmd_reg_get(clmd_reg_t *reg, const char *name, kuda_pool_t *p);

/**
 * Re-compute the state of the CLMD, given current store contents.
 */
kuda_status_t clmd_reg_reinit_state(clmd_reg_t *reg, clmd_t *clmd, kuda_pool_t *p);

/**
 * Callback invoked for every clmd in the registry. If 0 is returned, iteration stops.
 */
typedef int clmd_reg_do_cb(void *baton, clmd_reg_t *reg, clmd_t *clmd);

/**
 * Invoke callback for all mds in this registry. Order is not guaranteed.
 * If the callback returns 0, iteration stops. Returns 0 if iteration was
 * aborted.
 */
int clmd_reg_do(clmd_reg_do_cb *cb, void *baton, clmd_reg_t *reg, kuda_pool_t *p);

/**
 * Bitmask for fields that are updated.
 */
#define CLMD_UPD_DOMAINS       0x0001
#define CLMD_UPD_CA_URL        0x0002
#define CLMD_UPD_CA_PROTO      0x0004
#define CLMD_UPD_CA_ACCOUNT    0x0008
#define CLMD_UPD_CONTACTS      0x0010
#define CLMD_UPD_AGREEMENT     0x0020
#define CLMD_UPD_DRIVE_MODE    0x0080
#define CLMD_UPD_RENEW_WINDOW  0x0100
#define CLMD_UPD_CA_CHALLENGES 0x0200
#define CLMD_UPD_PKEY_SPEC     0x0400
#define CLMD_UPD_REQUIRE_HTTPS 0x0800
#define CLMD_UPD_TRANSITIVE    0x1000
#define CLMD_UPD_MUST_STAPLE   0x2000
#define CLMD_UPD_PROTO         0x4000
#define CLMD_UPD_WARN_WINDOW   0x8000
#define CLMD_UPD_ALL           0x7FFFFFFF

/**
 * Update the given fields for the managed domain. Take the new
 * values from the given clmd, all other values remain unchanged.
 */
kuda_status_t clmd_reg_update(clmd_reg_t *reg, kuda_pool_t *p, 
                           const char *name, const clmd_t *clmd, int fields);

/**
 * Get the chain of public certificates of the managed domain clmd, starting with the cert
 * of the domain and going up the issuers. Returns KUDA_ENOENT when not available. 
 */
kuda_status_t clmd_reg_get_pubcert(const clmd_pubcert_t **ppubcert, clmd_reg_t *reg, 
                                const clmd_t *clmd, kuda_pool_t *p);

/**
 * Get the filenames of private key and pubcert of the CLMD - if they exist.
 * @return KUDA_ENOENT if one or both do not exist.
 */
kuda_status_t clmd_reg_get_cred_files(const char **pkeyfile, const char **pcertfile,
                                   clmd_reg_t *reg, clmd_store_group_t group, 
                                   const clmd_t *clmd, kuda_pool_t *p);

/**
 * Synchronise the give master mds with the store.
 */
kuda_status_t clmd_reg_sync(clmd_reg_t *reg, kuda_pool_t *p, kuda_pool_t *ptemp, 
                         kuda_array_header_t *master_clmds);

kuda_status_t clmd_reg_remove(clmd_reg_t *reg, kuda_pool_t *p, const char *name, int archive);

/**
 * Delete the account from the local store.
 */
kuda_status_t clmd_reg_delete_acct(clmd_reg_t *reg, kuda_pool_t *p, const char *acct_id);


/**
 * Cleanup any challenges that are no longer in use.
 * 
 * @param reg   the registry
 * @param p     pool for permament storage
 * @param ptemp pool for temporary storage
 * @param mds   the list of configured CLMDs
 */
kuda_status_t clmd_reg_cleanup_challenges(clmd_reg_t *reg, kuda_pool_t *p, kuda_pool_t *ptemp, 
                                       kuda_array_header_t *mds);

/**
 * Mark all information from group CLMD_SG_DOMAINS as readonly, deny future modifications 
 * (CLMD_SG_STAGING and CLMD_SG_CHALLENGES remain writeable). For the given CLMDs, cache
 * the public information (CLMDs themselves and their pubcerts or lack of).
 */
kuda_status_t clmd_reg_freeze_domains(clmd_reg_t *reg, kuda_array_header_t *mds);

/**
 * Return if the certificate of the CLMD shoud be renewed. This includes reaching
 * the renewal window of an otherwise valid certificate. It return also !0 iff
 * no certificate has been obtained yet.
 */
int clmd_reg_should_renew(clmd_reg_t *reg, const clmd_t *clmd, kuda_pool_t *p);

/**
 * Return if a warning should be issued about the certificate expiration. 
 * This applies the configured warn window to the remaining lifetime of the 
 * current certiciate. If no certificate is present, this returns 0.
 */
int clmd_reg_should_warn(clmd_reg_t *reg, const clmd_t *clmd, kuda_pool_t *p);

/**************************************************************************************************/
/* protocol drivers */

typedef struct clmd_proto_t clmd_proto_t;

typedef struct clmd_proto_driver_t clmd_proto_driver_t;

/** 
 * Operating environment for a protocol driver. This is valid only for the
 * duration of one run (init + renew, init + preload).
 */
struct clmd_proto_driver_t {
    const clmd_proto_t *proto;
    kuda_pool_t *p;
    void *baton;
    struct kuda_table_t *env;

    clmd_reg_t *reg;
    struct clmd_store_t *store;
    const char *proxy_url;
    const clmd_t *clmd;

    int can_http;
    int can_https;
    int reset;
};

typedef kuda_status_t clmd_proto_init_cb(clmd_proto_driver_t *driver, struct clmd_result_t *result);
typedef kuda_status_t clmd_proto_renew_cb(clmd_proto_driver_t *driver, struct clmd_result_t *result);
typedef kuda_status_t clmd_proto_preload_cb(clmd_proto_driver_t *driver, 
                                         clmd_store_group_t group, struct clmd_result_t *result);

struct clmd_proto_t {
    const char *protocol;
    clmd_proto_init_cb *init;
    clmd_proto_renew_cb *renew;
    clmd_proto_preload_cb *preload;
};

/**
 * Run a test intialization of the renew protocol for the given CLMD. This verifies
 * basic parameter settings and is expected to return a description of encountered
 * problems in <pmessage> when != KUDA_SUCCESS.
 * A message return is allocated fromt the given pool.
 */
kuda_status_t clmd_reg_test_init(clmd_reg_t *reg, const clmd_t *clmd, struct kuda_table_t *env, 
                              struct clmd_result_t *result, kuda_pool_t *p);

/**
 * Obtain new credentials for the given managed domain in STAGING.
 *
 * @return KUDA_SUCCESS if new credentials have been staged successfully
 */
kuda_status_t clmd_reg_renew(clmd_reg_t *reg, const clmd_t *clmd, 
                          struct kuda_table_t *env, int reset, 
                          struct clmd_result_t *result, kuda_pool_t *p);

/**
 * Load a new set of credentials for the managed domain from STAGING - if it exists. 
 * This will archive any existing credential data and make the staged set the new one
 * in DOMAINS.
 * If staging is incomplete or missing, the load will fail and all credentials remain
 * as they are.
 *
 * @return KUDA_SUCCESS on loading new data, KUDA_ENOENT when nothing is staged, error otherwise.
 */
kuda_status_t clmd_reg_load_staging(clmd_reg_t *reg, const clmd_t *clmd, struct kuda_table_t *env, 
                                 struct clmd_result_t *result, kuda_pool_t *p);

void clmd_reg_set_renew_window_default(clmd_reg_t *reg, const clmd_timeslice_t *renew_window);
void clmd_reg_set_warn_window_default(clmd_reg_t *reg, const clmd_timeslice_t *warn_window);

#endif /* capi_clmd_clmd_reg_h */
