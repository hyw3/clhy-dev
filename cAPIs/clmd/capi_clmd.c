/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <kuda_optional.h>
#include <kuda_strings.h>

#include <core_common.h>
#include <wwhy.h>
#include <http_core.h>
#include <http_protocol.h>
#include <http_request.h>
#include <http_log.h>
#include <http_vhost.h>
#include <clhy_listen.h>

#include "capi_status.h"

#include "clmd.h"
#include "clmd_curl.h"
#include "clmd_crypt.h"
#include "clmd_http.h"
#include "clmd_json.h"
#include "clmd_store.h"
#include "clmd_store_fs.h"
#include "clmd_log.h"
#include "clmd_result.h"
#include "clmd_reg.h"
#include "clmd_util.h"
#include "clmd_version.h"
#include "clmd_acme.h"
#include "clmd_acme_authz.h"

#include "capi_clmd.h"
#include "capi_clmd_config.h"
#include "capi_clmd_drive.h"
#include "capi_clmd_platform.h"
#include "capi_clmd_status.h"
#include "capi_ssl_openssl.h"

static void clmd_hooks(kuda_pool_t *pool);

CLHY_DECLARE_CAPI(clmd) = {
    STANDARD16_CAPI_STUFF,
    NULL,                   /* func to create per dir config */
    NULL,                   /* func to merge per dir config */
    clmd_config_create_svr, /* func to create per server config */
    clmd_config_merge_svr,  /* func to merge per server config */
    clmd_cmds,              /* command handlers */
    clmd_hooks,
#if defined(CLHY_CAPI_FLAG_NONE)
    CLHY_CAPI_FLAG_ALWAYS_MERGE
#endif
};

/**************************************************************************************************/
/* logging setup */

static server_rec *log_server;

static int log_is_level(void *baton, kuda_pool_t *p, clmd_log_level_t level)
{
    (void)baton;
    (void)p;
    if (log_server) {
        return CLHYLOG_IS_LEVEL(log_server, (int)level);
    }
    return level <= CLMD_LOG_INFO;
}

#define LOG_BUF_LEN 16*1024

static void log_print(const char *file, int line, clmd_log_level_t level, 
                      kuda_status_t rv, void *baton, kuda_pool_t *p, const char *fmt, va_list ap)
{
    if (log_is_level(baton, p, level)) {
        char buffer[LOG_BUF_LEN];
        
        memset(buffer, 0, sizeof(buffer));
        kuda_vsnprintf(buffer, LOG_BUF_LEN-1, fmt, ap);
        buffer[LOG_BUF_LEN-1] = '\0';

        if (log_server) {
            clhy_log_error(file, line, CLHYLOG_CAPI_INDEX, (int)level, rv, log_server, "%s",buffer);
        }
        else {
            clhy_log_perror(file, line, CLHYLOG_CAPI_INDEX, (int)level, rv, p, "%s", buffer);
        }
    }
}

/**************************************************************************************************/
/* capi_ssl interface */

static KUDA_OPTIONAL_FN_TYPE(ssl_is_https) *opt_ssl_is_https;

static void init_ssl(void)
{
    opt_ssl_is_https = KUDA_RETRIEVE_OPTIONAL_FN(ssl_is_https);
}

/**************************************************************************************************/
/* lifecycle */

static kuda_status_t cleanup_setups(void *dummy)
{
    (void)dummy;
    log_server = NULL;
    return KUDA_SUCCESS;
}

static void init_setups(kuda_pool_t *p, server_rec *base_server) 
{
    log_server = base_server;
    kuda_pool_cleanup_register(p, NULL, cleanup_setups, kuda_pool_cleanup_null);
}

/**************************************************************************************************/
/* store & registry setup */

static kuda_status_t store_file_ev(void *baton, struct clmd_store_t *store,
                                    clmd_store_fs_ev_t ev, unsigned int group, 
                                    const char *fname, kuda_filetype_e ftype,  
                                    kuda_pool_t *p)
{
    server_rec *s = baton;
    kuda_status_t rv;
    
    (void)store;
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE3, 0, s, "store event=%d on %s %s (group %d)", 
                 ev, (ftype == KUDA_DIR)? "dir" : "file", fname, group);
                 
    /* Directories in group CHALLENGES, STAGING and OCSP are written to 
     * under a different user. Give her ownership. 
     */
    if (ftype == KUDA_DIR) {
        switch (group) {
            case CLMD_SG_CHALLENGES:
            case CLMD_SG_STAGING:
                rv = clmd_make_worker_accessible(fname, p);
                if (KUDA_ENOTIMPL != rv) {
                    return rv;
                }
                break;
            default: 
                break;
        }
    }
    return KUDA_SUCCESS;
}

static kuda_status_t check_group_dir(clmd_store_t *store, clmd_store_group_t group, 
                                    kuda_pool_t *p, server_rec *s)
{
    const char *dir;
    kuda_status_t rv;
    
    if (KUDA_SUCCESS == (rv = clmd_store_get_fname(&dir, store, group, NULL, NULL, p))
        && KUDA_SUCCESS == (rv = kuda_dir_make_recursive(dir, CLMD_FPROT_D_UALL_GREAD, p))) {
        rv = store_file_ev(s, store, CLMD_S_FS_EV_CREATED, group, dir, KUDA_DIR, p);
    }
    return rv;
}

static kuda_status_t setup_store(clmd_store_t **pstore, clmd_capi_conf_t *mc, 
                                kuda_pool_t *p, server_rec *s)
{
    const char *base_dir;
    kuda_status_t rv;
    
    base_dir = clhy_server_root_relative(p, mc->base_dir);
    
    if (KUDA_SUCCESS != (rv = clmd_store_fs_init(pstore, p, base_dir))) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(10046)"setup store for %s", base_dir);
        goto out;
    }

    clmd_store_fs_set_event_cb(*pstore, store_file_ev, s);
    if (KUDA_SUCCESS != (rv = check_group_dir(*pstore, CLMD_SG_CHALLENGES, p, s))
        || KUDA_SUCCESS != (rv = check_group_dir(*pstore, CLMD_SG_STAGING, p, s))
        || KUDA_SUCCESS != (rv = check_group_dir(*pstore, CLMD_SG_ACCOUNTS, p, s))
        ) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(10047) 
                     "setup challenges directory");
    }
    
out:
    return rv;
}

/**************************************************************************************************/
/* post config handling */

static void merge_srv_config(clmd_t *clmd, clmd_srv_conf_t *base_sc, kuda_pool_t *p)
{
    if (!clmd->sc) {
        clmd->sc = base_sc;
    }

    if (!clmd->ca_url) {
        clmd->ca_url = clmd_config_gets(clmd->sc, CLMD_CONFIG_CA_URL);
    }
    if (!clmd->ca_proto) {
        clmd->ca_proto = clmd_config_gets(clmd->sc, CLMD_CONFIG_CA_PROTO);
    }
    if (!clmd->ca_agreement) {
        clmd->ca_agreement = clmd_config_gets(clmd->sc, CLMD_CONFIG_CA_AGREEMENT);
    }
    if (clmd->sc->s->server_admin && strcmp(DEFAULT_ADMIN, clmd->sc->s->server_admin)) {
        kuda_array_clear(clmd->contacts);
        KUDA_ARRAY_PUSH(clmd->contacts, const char *) = 
        clmd_util_schemify(p, clmd->sc->s->server_admin, "mailto");
    }
    if (clmd->renew_mode == CLMD_RENEW_DEFAULT) {
        clmd->renew_mode = clmd_config_geti(clmd->sc, CLMD_CONFIG_DRIVE_MODE);
    }
    if (!clmd->renew_window) clmd_config_get_timespan(&clmd->renew_window, clmd->sc, CLMD_CONFIG_RENEW_WINDOW);
    if (!clmd->warn_window) clmd_config_get_timespan(&clmd->warn_window, clmd->sc, CLMD_CONFIG_WARN_WINDOW);
    if (clmd->transitive < 0) {
        clmd->transitive = clmd_config_geti(clmd->sc, CLMD_CONFIG_TRANSITIVE);
    }
    if (!clmd->ca_challenges && clmd->sc->ca_challenges) {
        clmd->ca_challenges = kuda_array_copy(p, clmd->sc->ca_challenges);
    }        
    if (!clmd->pkey_spec) {
        clmd->pkey_spec = clmd->sc->pkey_spec;
        
    }
    if (clmd->require_https < 0) {
        clmd->require_https = clmd_config_geti(clmd->sc, CLMD_CONFIG_REQUIRE_HTTPS);
    }
    if (clmd->must_staple < 0) {
        clmd->must_staple = clmd_config_geti(clmd->sc, CLMD_CONFIG_MUST_STAPLE);
    }
}

static kuda_status_t check_coverage(clmd_t *clmd, const char *domain, server_rec *s, kuda_pool_t *p)
{
    if (clmd_contains(clmd, domain, 0)) {
        return KUDA_SUCCESS;
    }
    else if (clmd->transitive) {
        KUDA_ARRAY_PUSH(clmd->domains, const char*) = kuda_pstrdup(p, domain);
        return KUDA_SUCCESS;
    }
    else {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(10040)
                     "Virtual Host %s:%d matches Managed Domain '%s', but the "
                     "name/alias %s itself is not managed. A requested CLMD certificate "
                     "will not match ServerName.",
                     s->server_hostname, s->port, clmd->name, domain);
        return KUDA_EINVAL;
    }
}

static kuda_status_t clmd_covers_server(clmd_t *clmd, server_rec *s, kuda_pool_t *p)
{
    kuda_status_t rv;
    const char *name;
    int i;
    
    if (KUDA_SUCCESS == (rv = check_coverage(clmd, s->server_hostname, s, p)) && s->names) {
        for (i = 0; i < s->names->nelts; ++i) {
            name = KUDA_ARRAY_IDX(s->names, i, const char*);
            if (KUDA_SUCCESS != (rv = check_coverage(clmd, name, s, p))) {
                break;
            }
        }
    }
    return rv;
}

static int matches_port_somewhere(server_rec *s, int port)
{
    server_addr_rec *sa;
    
    for (sa = s->addrs; sa; sa = sa->next) {
        if (sa->host_port == port) {
            /* host_addr might be general (0.0.0.0) or specific, we count this as match */
            return 1;
        }
        if (sa->host_port == 0) {
            /* wildcard port, answers to all ports. Rare, but may work. */
            return 1;
        }
    }
    return 0;
}

static int uses_port(server_rec *s, int port)
{
    server_addr_rec *sa;
    int match = 0;
    for (sa = s->addrs; sa; sa = sa->next) {
        if (sa->host_port == port) {
            /* host_addr might be general (0.0.0.0) or specific, we count this as match */
            match = 1;
        }
        else {
            /* uses other port/wildcard */
            return 0;
        }
    }
    return match;
}

static kuda_status_t detect_supported_ports(clmd_capi_conf_t *mc, server_rec *s, 
                                           kuda_pool_t *p, int log_level)
{
    clhy_listen_rec *lr;
    kuda_sockaddr_t *sa;

    mc->can_http = 0;
    mc->can_https = 0;
    for (lr = clhy_listeners; lr; lr = lr->next) {
        for (sa = lr->bind_addr; sa; sa = sa->next) {
            if  (sa->port == mc->local_80 
                 && (!lr->protocol || !strncmp("http", lr->protocol, 4))) {
                mc->can_http = 1;
            }
            else if (sa->port == mc->local_443
                     && (!lr->protocol || !strncmp("http", lr->protocol, 4))) {
                mc->can_https = 1;
            }
        }
    }

    clhy_log_error(CLHYLOG_MARK, log_level, 0, s, CLHYLOGNO(10037)
                 "server seems%s reachable via http: (port 80->%d) "
                 "and%s reachable via https: (port 443->%d) ",
                 mc->can_http? "" : " not", mc->local_80,
                 mc->can_https? "" : " not", mc->local_443);
    return clmd_reg_set_props(mc->reg, p, mc->can_http, mc->can_https); 
}

static server_rec *get_https_server(const char *domain, server_rec *base_server)
{
    clmd_srv_conf_t *sc;
    clmd_capi_conf_t *mc;
    server_rec *s;
    request_rec r;

    sc = clmd_config_get(base_server);
    mc = sc->mc;
    memset(&r, 0, sizeof(r));
    
    for (s = base_server; s && (mc->local_443 > 0); s = s->next) {
        if (!mc->manage_base_server && s == base_server) {
            /* we shall not assign ourselves to the base server */
            continue;
        }
        r.server = s;
        if (clhy_matches_request_vhost(&r, domain, s->port) && uses_port(s, mc->local_443)) {
            return s;
        }
    }
    return NULL;
}

static void init_acme_tls_1_domains(clmd_t *clmd, server_rec *base_server)
{
    server_rec *s;
    int i;
    const char *domain;
    
    /* Collect those domains that support the "acme-tls/1" protocol. This
     * is part of the CLMD (and not tested dynamically), since challenge selection
     * may be done outside the server, e.g. in the a2md command. */
     kuda_array_clear(clmd->acme_tls_1_domains);
    for (i = 0; i < clmd->domains->nelts; ++i) {
        domain = KUDA_ARRAY_IDX(clmd->domains, i, const char*);
        if (NULL == (s = get_https_server(domain, base_server))) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, base_server, CLHYLOGNO(10168)
                         "%s: no https server_rec found for %s", clmd->name, domain);
            continue;
        }
        if (!clhy_is_allowed_protocol(NULL, NULL, s, PROTO_ACME_TLS_1)) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, base_server, CLHYLOGNO(10169)
                         "%s: https server_rec for %s does not have protocol %s enabled", 
                         clmd->name, domain, PROTO_ACME_TLS_1);
            continue;
        }
        KUDA_ARRAY_PUSH(clmd->acme_tls_1_domains, const char*) = domain;
    }
}

static kuda_status_t link_clmd_to_servers(clmd_capi_conf_t *mc, clmd_t *clmd, server_rec *base_server, 
                                       kuda_pool_t *p, kuda_pool_t *ptemp)
{
    server_rec *s, *s_https;
    request_rec r;
    clmd_srv_conf_t *sc;
    kuda_status_t rv = KUDA_SUCCESS;
    int i;
    const char *domain;
    kuda_array_header_t *servers;
    
    sc = clmd_config_get(base_server);

    /* Assign the CLMD to all server_rec configs that it matches. If there already
     * is an assigned CLMD not equal this one, the configuration is in error.
     */
    memset(&r, 0, sizeof(r));
    servers = kuda_array_make(ptemp, 5, sizeof(server_rec*));
    
    for (s = base_server; s; s = s->next) {
        if (!mc->manage_base_server && s == base_server) {
            /* we shall not assign ourselves to the base server */
            continue;
        }
        
        r.server = s;
        for (i = 0; i < clmd->domains->nelts; ++i) {
            domain = KUDA_ARRAY_IDX(clmd->domains, i, const char*);
            
            if (clhy_matches_request_vhost(&r, domain, s->port)) {
                /* Create a unique clmd_srv_conf_t record for this server, if there is none yet */
                sc = clmd_config_get_unique(s, p);
                
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, base_server, CLHYLOGNO(10041)
                             "Server %s:%d matches clmd %s (config %s)", 
                             s->server_hostname, s->port, clmd->name, sc->name);
                
                if (sc->assigned == clmd) {
                    /* already matched via another domain name */
                    goto next_server;
                }
                else if (sc->assigned) {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, base_server, CLHYLOGNO(10042)
                                 "conflict: CLMD %s matches server %s, but CLMD %s also matches.",
                                 clmd->name, s->server_hostname, sc->assigned->name);
                    return KUDA_EINVAL;
                }
                
                /* If this server_rec is only for http: requests. Defined
                 * alias names do not matter for this CLMD.
                 * Otherwise, if server has name or an alias not covered,
                 * it is by default auto-added (config transitive).
                 * If mode is "manual", a generated certificate will not match
                 * all necessary names. */
                if (!mc->local_80 || !uses_port(s, mc->local_80)) {
                    if (KUDA_SUCCESS != (rv = clmd_covers_server(clmd, s, p))) {
                        return rv;
                    }
                }

                sc->assigned = clmd;
                KUDA_ARRAY_PUSH(servers, server_rec*) = s;
                
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, base_server, CLHYLOGNO(10043)
                             "Managed Domain %s applies to vhost %s:%d", clmd->name,
                             s->server_hostname, s->port);
                
                goto next_server;
            }
        }
    next_server:
        continue;
    }

    if (KUDA_SUCCESS == rv) {
        if (kuda_is_empty_array(servers)) {
            if (clmd->renew_mode != CLMD_RENEW_ALWAYS) {
                /* Not an error, but looks suspicious */
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, base_server, CLHYLOGNO(10045)
                             "No VirtualHost matches Managed Domain %s", clmd->name);
                KUDA_ARRAY_PUSH(mc->unused_names, const char*)  = clmd->name;
            }
        }
        else {
            const char *uri;
            
            /* Found matching server_rec's. Collect all 'ServerAdmin's into CLMD's contact list */
            kuda_array_clear(clmd->contacts);
            for (i = 0; i < servers->nelts; ++i) {
                s = KUDA_ARRAY_IDX(servers, i, server_rec*);
                if (s->server_admin && strcmp(DEFAULT_ADMIN, s->server_admin)) {
                    uri = clmd_util_schemify(p, s->server_admin, "mailto");
                    if (clmd_array_str_index(clmd->contacts, uri, 0, 0) < 0) {
                        KUDA_ARRAY_PUSH(clmd->contacts, const char *) = uri; 
                        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, base_server, CLHYLOGNO(10044)
                                     "%s: added contact %s", clmd->name, uri);
                    }
                }
            }
            
            if (clmd->require_https > CLMD_REQUIRE_OFF) {
                /* We require https for this CLMD, but do we have port 443 (or a mapped one)
                 * available? */
                if (mc->local_443 <= 0) {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, base_server, CLHYLOGNO(10105)
                                 "MDPortMap says there is no port for https (443), "
                                 "but CLMD %s is configured to require https. This "
                                 "only works when a 443 port is available.", clmd->name);
                    return KUDA_EINVAL;
                    
                }
                
                /* Ok, we know which local port represents 443, do we have a server_rec
                 * for CLMD that has addresses with port 443? */
                s_https = NULL;
                for (i = 0; i < servers->nelts; ++i) {
                    s = KUDA_ARRAY_IDX(servers, i, server_rec*);
                    if (matches_port_somewhere(s, mc->local_443)) {
                        s_https = s;
                        break;
                    }
                }
                
                if (!s_https) {
                    /* Did not find any server_rec that matches this CLMD *and* has an
                     * s->addrs match for the https port. Suspicious. */
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, base_server, CLHYLOGNO(10106)
                                 "CLMD %s is configured to require https, but there seems to be "
                                 "no VirtualHost for it that has port %d in its address list. "
                                 "This looks as if it will not work.", 
                                 clmd->name, mc->local_443);
                }
            }
            
        }
        
    }
    return rv;
}

static kuda_status_t link_clmds_to_servers(clmd_capi_conf_t *mc, server_rec *s, 
                                            kuda_pool_t *p, kuda_pool_t *ptemp)
{
    int i;
    clmd_t *clmd;
    kuda_status_t rv = KUDA_SUCCESS;
    
    kuda_array_clear(mc->unused_names);
    for (i = 0; i < mc->mds->nelts; ++i) {
        clmd = KUDA_ARRAY_IDX(mc->mds, i, clmd_t*);
        if (KUDA_SUCCESS != (rv = link_clmd_to_servers(mc, clmd, s, p, ptemp))) {
            goto leave;
        }
    }
leave:
    return rv;
}

static kuda_status_t merge_clmds_with_conf(clmd_capi_conf_t *mc, kuda_pool_t *p, 
                                        server_rec *base_server, int log_level)
{
    clmd_srv_conf_t *base_conf;
    clmd_t *clmd, *omd;
    const char *domain;
    const clmd_timeslice_t *ts;
    kuda_status_t rv = KUDA_SUCCESS;
    int i, j;

    /* The global cAPI configuration 'mc' keeps a list of all configured MDomains
     * in the server. This list is collected during configuration processing and,
     * in the post config phase, get updated from all merged server configurations
     * before the server starts processing.
     */ 
    base_conf = clmd_config_get(base_server);
    clmd_config_get_timespan(&ts, base_conf, CLMD_CONFIG_RENEW_WINDOW);
    if (ts) clmd_reg_set_renew_window_default(mc->reg, ts);
    clmd_config_get_timespan(&ts, base_conf, CLMD_CONFIG_WARN_WINDOW);
    if (ts) clmd_reg_set_warn_window_default(mc->reg, ts);
 
    /* Complete the properties of the CLMDs, now that we have the complete, merged
     * server configurations.
     */
    for (i = 0; i < mc->mds->nelts; ++i) {
        clmd = KUDA_ARRAY_IDX(mc->mds, i, clmd_t*);
        merge_srv_config(clmd, base_conf, p);

        /* Check that we have no overlap with the CLMDs already completed */
        for (j = 0; j < i; ++j) {
            omd = KUDA_ARRAY_IDX(mc->mds, j, clmd_t*);
            if ((domain = clmd_common_name(clmd, omd)) != NULL) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, base_server, CLHYLOGNO(10038)
                             "two Managed Domains have an overlap in domain '%s'"
                             ", first definition in %s(line %d), second in %s(line %d)",
                             domain, clmd->defn_name, clmd->defn_line_number,
                             omd->defn_name, omd->defn_line_number);
                return KUDA_EINVAL;
            }
        }
        
        if (clmd->cert_file && !clmd->pkey_file) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, base_server, CLHYLOGNO(10170)
                         "The Managed Domain '%s', defined in %s(line %d), "
                         "has a MDCertificateFile but no MDCertificateKeyFile.",
                         clmd->name, clmd->defn_name, clmd->defn_line_number);
            return KUDA_EINVAL;
        }
        if (!clmd->cert_file && clmd->pkey_file) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, base_server, CLHYLOGNO(10171)
                         "The Managed Domain '%s', defined in %s(line %d), "
                         "has a MDCertificateKeyFile but no MDCertificateFile.",
                         clmd->name, clmd->defn_name, clmd->defn_line_number);
            return KUDA_EINVAL;
        }

        init_acme_tls_1_domains(clmd, base_server);

        if (CLHYLOG_IS_LEVEL(base_server, log_level)) {
            clhy_log_error(CLHYLOG_MARK, log_level, 0, base_server, CLHYLOGNO(10039)
                         "Completed CLMD[%s, CA=%s, Proto=%s, Agreement=%s, renew-mode=%d "
                         "renew_window=%s, warn_window=%s",
                         clmd->name, clmd->ca_url, clmd->ca_proto, clmd->ca_agreement, clmd->renew_mode,
                         clmd->renew_window? clmd_timeslice_format(clmd->renew_window, p) : "unset",
                         clmd->warn_window? clmd_timeslice_format(clmd->warn_window, p) : "unset");
        }
    }
    return rv;
}

static void load_staged_data(clmd_capi_conf_t *mc, server_rec *s, kuda_pool_t *p)
{
    kuda_status_t rv;
    clmd_t *clmd;
    clmd_result_t *result;
    int i;
    
    for (i = 0; i < mc->mds->nelts; ++i) {
        clmd = KUDA_ARRAY_IDX(mc->mds, i, clmd_t *);
        result = clmd_result_clmd_make(p, clmd);
        if (KUDA_SUCCESS == (rv = clmd_reg_load_staging(mc->reg, clmd, mc->env, result, p))) {
            clhy_log_error( CLHYLOG_MARK, CLHYLOG_INFO, rv, s, CLHYLOGNO(10068) 
                         "%s: staged set activated", clmd->name);
        }
        else if (!KUDA_STATUS_IS_ENOENT(rv)) {
            clhy_log_error( CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(10069)
                         "%s: error loading staged set", clmd->name);
        }
    }
}

static kuda_status_t reinit_clmds(clmd_capi_conf_t *mc, server_rec *s, kuda_pool_t *p)
{
    clmd_t *clmd; 
    kuda_status_t rv = KUDA_SUCCESS;
    int i;
    
    for (i = 0; i < mc->mds->nelts; ++i) {
        clmd = KUDA_ARRAY_IDX(mc->mds, i, clmd_t *);
        if (KUDA_SUCCESS != (rv = clmd_reg_reinit_state(mc->reg, (clmd_t*)clmd, p))) {
            clhy_log_error( CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(10172)
                         "%s: error reinitiazing from store", clmd->name);
            break;
        }
    }
    return rv;
}

static void init_watched_names(clmd_capi_conf_t *mc, kuda_pool_t *p, kuda_pool_t *ptemp, server_rec *s)
{
    const clmd_t *clmd;
    clmd_result_t *result;
    int i;
    
    /* Calculate the list of CLMD names which we need to watch:
     * - all CLMDs that are used somewhere
     * - all CLMDs in drive mode 'AUTO' that are not in 'unused_names'
     */
    result = clmd_result_make(ptemp, KUDA_SUCCESS);
    kuda_array_clear(mc->watched_names);
    for (i = 0; i < mc->mds->nelts; ++i) {
        clmd = KUDA_ARRAY_IDX(mc->mds, i, const clmd_t *);
        clmd_result_set(result, KUDA_SUCCESS, NULL);

        if (clmd->state == CLMD_S_ERROR) {
            clmd_result_set(result, KUDA_EGENERAL, 
                          "in error state, unable to drive forward. This "
                          "indicates an incomplete or inconsistent configuration. "
                          "Please check the log for warnings in this regard.");
            continue;
        }

        if (clmd->renew_mode == CLMD_RENEW_AUTO
            && clmd_array_str_index(mc->unused_names, clmd->name, 0, 0) >= 0) {
            /* This CLMD is not used in any virtualhost, do not watch */
            continue;
        }
        
        if (clmd_will_renew_cert(clmd)) {
            /* make a test init to detect early errors. */
            clmd_reg_test_init(mc->reg, clmd, mc->env, result, p);
            if (KUDA_SUCCESS != result->status && result->detail) {
                kuda_hash_set(mc->init_errors, clmd->name, KUDA_HASH_KEY_STRING, kuda_pstrdup(p, result->detail));
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(10173) 
                             "clmd[%s]: %s", clmd->name, result->detail);
            }
        }
        
        KUDA_ARRAY_PUSH(mc->watched_names, const char *) = clmd->name; 
    }
}   

static kuda_status_t clmd_post_config_before_ssl(kuda_pool_t *p, kuda_pool_t *plog,
                                              kuda_pool_t *ptemp, server_rec *s)
{
    void *data = NULL;
    const char *capi_clmd_init_key = "capi_clmd_init_counter";
    clmd_srv_conf_t *sc;
    clmd_capi_conf_t *mc;
    kuda_status_t rv = KUDA_SUCCESS;
    int dry_run = 0, log_level = CLHYLOG_DEBUG;
    clmd_store_t *store;

    kuda_pool_userdata_get(&data, capi_clmd_init_key, s->process->pool);
    if (data == NULL) {
        /* At the first start, wwhy makes a config check dry run. It
         * runs all config hooks to check if it can. If so, it does
         * this all again and starts serving requests.
         * 
         * On a dry run, we therefore do all the cheap config things we
         * need to do to find out if the settings are ok. More expensive
         * things we delay to the real run.
         */
        dry_run = 1;
        log_level = CLHYLOG_TRACE1;
        clhy_log_error( CLHYLOG_MARK, log_level, 0, s, CLHYLOGNO(10070)
                     "initializing post config dry run");
        kuda_pool_userdata_set((const void *)1, capi_clmd_init_key,
                              kuda_pool_cleanup_null, s->process->pool);
    }
    else {
        clhy_log_error( CLHYLOG_MARK, CLHYLOG_INFO, 0, s, CLHYLOGNO(10071)
                     "capi_clmd (v%s), initializing...", CAPI_CLMD_VERSION);
    }

    (void)plog;
    init_setups(p, s);
    clmd_log_set(log_is_level, log_print, NULL);

    clmd_config_post_config(s, p);
    sc = clmd_config_get(s);
    mc = sc->mc;
    mc->dry_run = dry_run;

    if (KUDA_SUCCESS != (rv = setup_store(&store, mc, p, s))
        || KUDA_SUCCESS != (rv = clmd_reg_create(&mc->reg, p, store, mc->proxy_url))) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(10072) "setup clmd registry");
        goto leave;
    }

    init_ssl();

    /* How to bootstrap this cAPI:
     * 1. find out if we know where http: and https: requests will arrive
     * 2. apply the now complete configuration setttings to the CLMDs
     * 3. Link CLMDs to the server_recs they are used in. Detect unused CLMDs.
     * 4. Update the store with the CLMDs. Change domain names, create new CLMDs, etc.
     *    Basically all CLMD properties that are configured directly.
     *    WARNING: this may change the name of an CLMD. If an CLMD loses the first
     *    of its domain names, it first gets the new first one as name. The 
     *    store will find the old settings and "recover" the previous name.
     * 5. Load any staged data from previous driving.
     * 6. on a dry run, this is all we do
     * 7. Read back the CLMD properties that reflect the existance and aspect of
     *    credentials that are in the store (or missing there). 
     *    Expiry times, CLMD state, etc.
     * 8. Determine the list of CLMDs that need driving/supervision.
     * 9. Cleanup any left-overs in registry/store that are no longer needed for
     *    the list of CLMDs as we know it now.
     * 10. If this list is non-empty, setup a watchdog to run. 
     */
    /*1*/
    if (KUDA_SUCCESS != (rv = detect_supported_ports(mc, s, p, log_level))) goto leave;
    /*2*/
    if (KUDA_SUCCESS != (rv = merge_clmds_with_conf(mc, p, s, log_level))) goto leave;
    /*3*/
    if (KUDA_SUCCESS != (rv = link_clmds_to_servers(mc, s, p, ptemp))) goto leave;
    /*4*/
    if (KUDA_SUCCESS != (rv = clmd_reg_sync(mc->reg, p, ptemp, mc->mds))) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(10073)
                     "synching %d mds to registry", mc->mds->nelts);
        goto leave;
    }
    /*5*/
    load_staged_data(mc, s, p);
    /*6*/
    if (dry_run) goto leave;
    /*7*/
    if (KUDA_SUCCESS != (rv = reinit_clmds(mc, s, p))) goto leave;
    /*8*/
    init_watched_names(mc, p, ptemp, s);
    /*9*/
    clmd_reg_cleanup_challenges(mc->reg, p, ptemp, mc->mds);
    
    /* From here on, the domains in the registry are readonly 
     * and only staging/challenges may be manipulated */
    clmd_reg_freeze_domains(mc->reg, mc->mds);
    
    if (mc->watched_names->nelts > 0) {
        /*10*/
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, s, CLHYLOGNO(10074)
                     "%d out of %d mds need watching", 
                     mc->watched_names->nelts, mc->mds->nelts);
    
        clmd_http_use_implementation(clmd_curl_get_impl(p));
        rv = clmd_start_watching(mc, s, p);
    }
    else {
        clhy_log_error( CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(10075) "no mds to drive");
    }
leave:
    return rv;
}

/**************************************************************************************************/
/* connection context */

typedef struct {
    const char *protocol;
} clmd_conn_ctx;

static const char *clmd_protocol_get(const conn_rec *c)
{
    clmd_conn_ctx *ctx;

    ctx = (clmd_conn_ctx*)clhy_get_capi_config(c->conn_config, &clmd_capi);
    return ctx? ctx->protocol : NULL;
}

/**************************************************************************************************/
/* ALPN handling */

static int clmd_protocol_propose(conn_rec *c, request_rec *r,
                               server_rec *s,
                               const kuda_array_header_t *offers,
                               kuda_array_header_t *proposals)
{
    (void)s;
    if (!r && offers && opt_ssl_is_https && opt_ssl_is_https(c) 
        && clhy_array_str_contains(offers, PROTO_ACME_TLS_1)) {
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, c,
                      "proposing protocol '%s'", PROTO_ACME_TLS_1);
        KUDA_ARRAY_PUSH(proposals, const char*) = PROTO_ACME_TLS_1;
        return OK;
    }
    return DECLINED;
}

static int clmd_protocol_switch(conn_rec *c, request_rec *r, server_rec *s,
                              const char *protocol)
{
    clmd_conn_ctx *ctx;
    
    (void)s;
    if (!r && opt_ssl_is_https && opt_ssl_is_https(c) && !strcmp(PROTO_ACME_TLS_1, protocol)) {
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, c,
                      "switching protocol '%s'", PROTO_ACME_TLS_1);
        ctx = kuda_pcalloc(c->pool, sizeof(*ctx));
        ctx->protocol = PROTO_ACME_TLS_1;
        clhy_set_capi_config(c->conn_config, &clmd_capi, ctx);

        c->keepalive = CLHY_CONN_CLOSE;
        return OK;
    }
    return DECLINED;
}

 
/**************************************************************************************************/
/* Access API to other wwhy components */

static int clmd_is_managed(server_rec *s)
{
    clmd_srv_conf_t *conf = clmd_config_get(s);

    if (conf && conf->assigned) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(10076) 
                     "%s: manages server %s", conf->assigned->name, s->server_hostname);
        return 1;
    }
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, s,  
                 "server %s is not managed", s->server_hostname);
    return 0;
}

static kuda_status_t setup_fallback_cert(clmd_store_t *store, const clmd_t *clmd, 
                                        server_rec *s, kuda_pool_t *p)
{
    clmd_pkey_t *pkey;
    clmd_cert_t *cert;
    clmd_pkey_spec_t spec;
    kuda_status_t rv;
    
    spec.type = CLMD_PKEY_TYPE_RSA;
    spec.params.rsa.bits = CLMD_PKEY_RSA_BITS_DEF;
    
    if (KUDA_SUCCESS != (rv = clmd_pkey_gen(&pkey, p, &spec))
        || KUDA_SUCCESS != (rv = clmd_store_save(store, p, CLMD_SG_DOMAINS, clmd->name, 
                                CLMD_FN_FALLBACK_PKEY, CLMD_SV_PKEY, (void*)pkey, 0))
        || KUDA_SUCCESS != (rv = clmd_cert_self_sign(&cert, "cLHy Managed Domain Fallback", 
                                    clmd->domains, pkey, kuda_time_from_sec(14 * CLMD_SECS_PER_DAY), p))
        || KUDA_SUCCESS != (rv = clmd_store_save(store, p, CLMD_SG_DOMAINS, clmd->name, 
                                CLMD_FN_FALLBACK_CERT, CLMD_SV_CERT, (void*)cert, 0))) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(10174)
                     "%s: setup fallback certificate", clmd->name);
    }
    return rv;
}

static kuda_status_t get_certificate(server_rec *s, kuda_pool_t *p, int fallback,
                                    const char **pcertfile, const char **pkeyfile)
{
    kuda_status_t rv = KUDA_ENOENT;    
    clmd_srv_conf_t *sc;
    clmd_reg_t *reg;
    clmd_store_t *store;
    const clmd_t *clmd;
    
    *pkeyfile = NULL;
    *pcertfile = NULL;

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(10113)
                 "get_certificate called for vhost %s.", s->server_hostname);

    sc = clmd_config_get(s);
    if (!sc) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, s,  
                     "asked for certificate of server %s which has no clmd config", 
                     s->server_hostname);
        return KUDA_ENOENT;
    }
    
    if (!sc->assigned) {
        /* With the new hooks in capi_ssl, we are invoked for all server_rec. It is
         * therefore normal, when we have nothing to add here. */
        return KUDA_ENOENT;
    }
    
    assert(sc->mc);
    reg = sc->mc->reg;
    assert(reg);
    
    clmd = sc->assigned;
    if (!clmd) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(10115) 
                     "unable to hand out certificates, as registry can no longer "
                     "find CLMD '%s'.", sc->assigned->name);
        return KUDA_ENOENT;
    }
    
    rv = clmd_reg_get_cred_files(pkeyfile, pcertfile, reg, CLMD_SG_DOMAINS, clmd, p);
    if (KUDA_STATUS_IS_ENOENT(rv)) {
        if (fallback) {
            /* Provide temporary, self-signed certificate as fallback, so that
             * clients do not get obscure TLS handshake errors or will see a fallback
             * virtual host that is not intended to be served here. */
            store = clmd_reg_store_get(reg);
            assert(store);    
            
            clmd_store_get_fname(pkeyfile, store, CLMD_SG_DOMAINS, clmd->name, CLMD_FN_FALLBACK_PKEY, p);
            clmd_store_get_fname(pcertfile, store, CLMD_SG_DOMAINS, clmd->name, CLMD_FN_FALLBACK_CERT, p);
            if (!clmd_file_exists(*pkeyfile, p) || !clmd_file_exists(*pcertfile, p)) { 
                if (KUDA_SUCCESS != (rv = setup_fallback_cert(store, clmd, s, p))) {
                    return rv;
                }
            }
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(10116)  
                         "%s: providing fallback certificate for server %s", 
                         clmd->name, s->server_hostname);
            return KUDA_EAGAIN;
        }
    }
    else if (KUDA_SUCCESS != rv) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(10110) 
                     "retrieving credentials for CLMD %s", clmd->name);
        return rv;
    }
    
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, s, CLHYLOGNO(10077) 
                 "%s[state=%d]: providing certificate for server %s", 
                 clmd->name, clmd->state, s->server_hostname);
    return rv;
}

static kuda_status_t clmd_get_certificate(server_rec *s, kuda_pool_t *p,
                                       const char **pkeyfile, const char **pcertfile)
{
    return get_certificate(s, p, 1, pcertfile, pkeyfile);
}

static int clmd_add_cert_files(server_rec *s, kuda_pool_t *p,
                             kuda_array_header_t *cert_files, 
                             kuda_array_header_t *key_files)
{
    const char *certfile, *keyfile;
    kuda_status_t rv;
    
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, s, "hook ssl_add_cert_files for %s",
                 s->server_hostname);
    rv = get_certificate(s, p, 0, &certfile, &keyfile);
    if (KUDA_SUCCESS == rv) {
        if (!kuda_is_empty_array(cert_files)) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, s, CLHYLOGNO(10084)
                         "host '%s' is covered by a Managed Domain, but "
                         "certificate/key files are already configured "
                         "for it (most likely via SSLCertificateFile).", 
                         s->server_hostname);
        } 
        KUDA_ARRAY_PUSH(cert_files, const char*) = certfile;
        KUDA_ARRAY_PUSH(key_files, const char*) = keyfile;
        return DONE;
    }
    return DECLINED;
}

static int clmd_add_fallback_cert_files(server_rec *s, kuda_pool_t *p,
                                      kuda_array_header_t *cert_files, 
                                      kuda_array_header_t *key_files)
{
    const char *certfile, *keyfile;
    kuda_status_t rv;
    
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, s, "hook ssl_add_fallback_cert_files for %s",
                 s->server_hostname);
    rv = get_certificate(s, p, 1, &certfile, &keyfile);
    if (KUDA_EAGAIN == rv) {
        KUDA_ARRAY_PUSH(cert_files, const char*) = certfile;
        KUDA_ARRAY_PUSH(key_files, const char*) = keyfile;
        return DONE;
    }
    return DECLINED;
}

static int clmd_is_challenge(conn_rec *c, const char *servername,
                           X509 **pcert, EVP_PKEY **pkey)
{
    clmd_srv_conf_t *sc;
    const char *protocol, *challenge, *cert_name, *pkey_name;
    kuda_status_t rv;

    if (!servername) goto out;
                  
    challenge = NULL;
    if ((protocol = clmd_protocol_get(c)) && !strcmp(PROTO_ACME_TLS_1, protocol)) {
        challenge = "tls-alpn-01";
        cert_name = CLMD_FN_TLSALPN01_CERT;
        pkey_name = CLMD_FN_TLSALPN01_PKEY;

        sc = clmd_config_get(c->base_server);
        if (sc && sc->mc->reg) {
            clmd_store_t *store = clmd_reg_store_get(sc->mc->reg);
            clmd_cert_t *mdcert;
            clmd_pkey_t *mdpkey;
            
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, c, "%s: load certs/keys %s/%s",
                          servername, cert_name, pkey_name);
            rv = clmd_store_load(store, CLMD_SG_CHALLENGES, servername, cert_name, 
                               CLMD_SV_CERT, (void**)&mdcert, c->pool);
            if (KUDA_SUCCESS == rv && (*pcert = clmd_cert_get_X509(mdcert))) {
                rv = clmd_store_load(store, CLMD_SG_CHALLENGES, servername, pkey_name, 
                                   CLMD_SV_PKEY, (void**)&mdpkey, c->pool);
                if (KUDA_SUCCESS == rv && (*pkey = clmd_pkey_get_EVP_PKEY(mdpkey))) {
                    clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, c, CLHYLOGNO(10078)
                                  "%s: is a %s challenge host", servername, challenge);
                    return 1;
                }
                clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, c, CLHYLOGNO(10079)
                              "%s: challenge data not complete, key unavailable", servername);
            }
            else {
                clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_INFO, rv, c, CLHYLOGNO(10080)
                              "%s: unknown %s challenge host", servername, challenge);
            }
        }
    }
out:
    *pcert = NULL;
    *pkey = NULL;
    return 0;
}

static int clmd_answer_challenge(conn_rec *c, const char *servername,
                               X509 **pcert, EVP_PKEY **pkey)
{
    if (clmd_is_challenge(c, servername, pcert, pkey)) {
        return KUDA_SUCCESS;
    }
    return DECLINED;
}

/**************************************************************************************************/
/* ACME 'http-01' challenge responses */

#define WELL_KNOWN_PREFIX           "/.well-known/"
#define ACME_CHALLENGE_PREFIX       WELL_KNOWN_PREFIX"acme-challenge/"

static int clmd_http_challenge_pr(request_rec *r)
{
    kuda_bucket_brigade *bb;
    const clmd_srv_conf_t *sc;
    const char *name, *data;
    clmd_reg_t *reg;
    const clmd_t *clmd;
    kuda_status_t rv;
    
    if (r->parsed_uri.path 
        && !strncmp(ACME_CHALLENGE_PREFIX, r->parsed_uri.path, sizeof(ACME_CHALLENGE_PREFIX)-1)) {
        sc = clhy_get_capi_config(r->server->capi_config, &clmd_capi);
        if (sc && sc->mc) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, r, 
                          "access inside /.well-known/acme-challenge for %s%s", 
                          r->hostname, r->parsed_uri.path);
            clmd = clmd_get_by_domain(sc->mc->mds, r->hostname);
            name = r->parsed_uri.path + sizeof(ACME_CHALLENGE_PREFIX)-1;
            reg = sc && sc->mc? sc->mc->reg : NULL;
            
            if (strlen(name) && !clhy_strchr_c(name, '/') && reg) {
                clmd_store_t *store = clmd_reg_store_get(reg);
                
                rv = clmd_store_load(store, CLMD_SG_CHALLENGES, r->hostname, 
                                   CLMD_FN_HTTP01, CLMD_SV_TEXT, (void**)&data, r->pool);
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, r, 
                              "loading challenge for %s (%s)", r->hostname, r->uri);
                if (KUDA_SUCCESS == rv) {
                    kuda_size_t len = strlen(data);
                    
                    if (r->method_number != M_GET) {
                        return HTTP_NOT_IMPLEMENTED;
                    }
                    /* A GET on a challenge resource for a hostname we are
                     * configured for. Let's send the content back */
                    r->status = HTTP_OK;
                    kuda_table_setn(r->headers_out, "Content-Length", kuda_ltoa(r->pool, (long)len));
                    
                    bb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
                    kuda_brigade_write(bb, NULL, NULL, data, len);
                    clhy_pass_brigade(r->output_filters, bb);
                    kuda_brigade_cleanup(bb);
                    
                    return DONE;
                }
                else if (!clmd || clmd->renew_mode == CLMD_RENEW_MANUAL
                    || (clmd->cert_file && clmd->renew_mode == CLMD_RENEW_AUTO)) {
                    /* The request hostname is not for a domain - or at least not for
                     * a domain that we renew ourselves. We are not
                     * the sole authority here for /.well-known/acme-challenge (see PR62189).
                     * So, we decline to handle this and give others a chance to provide
                     * the answer.
                     */
                    return DECLINED;
                }
                else if (KUDA_STATUS_IS_ENOENT(rv)) {
                    return HTTP_NOT_FOUND;
                }
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(10081)
                              "loading challenge %s from store", name);
                return HTTP_INTERNAL_SERVER_ERROR;
            }
        }
    }
    return DECLINED;
}

/**************************************************************************************************/
/* Require Https hook */

static int clmd_require_https_maybe(request_rec *r)
{
    const clmd_srv_conf_t *sc;
    kuda_uri_t uri;
    const char *s;
    int status;
    
    if (opt_ssl_is_https && r->parsed_uri.path
        && strncmp(WELL_KNOWN_PREFIX, r->parsed_uri.path, sizeof(WELL_KNOWN_PREFIX)-1)) {
        
        sc = clhy_get_capi_config(r->server->capi_config, &clmd_capi);
        if (sc && sc->assigned && sc->assigned->require_https > CLMD_REQUIRE_OFF) {
            if (opt_ssl_is_https(r->connection)) {
                /* Using https:
                 * if 'permanent' and no one else set a HSTS header already, do it */
                if (sc->assigned->require_https == CLMD_REQUIRE_PERMANENT 
                    && sc->mc->hsts_header && !kuda_table_get(r->headers_out, CLMD_HSTS_HEADER)) {
                    kuda_table_setn(r->headers_out, CLMD_HSTS_HEADER, sc->mc->hsts_header);
                }
            }
            else {
                /* Not using https:, but require it. Redirect. */
                if (r->method_number == M_GET) {
                    /* safe to use the old-fashioned codes */
                    status = ((CLMD_REQUIRE_PERMANENT == sc->assigned->require_https)? 
                              HTTP_MOVED_PERMANENTLY : HTTP_MOVED_TEMPORARILY);
                }
                else {
                    /* these should keep the method unchanged on retry */
                    status = ((CLMD_REQUIRE_PERMANENT == sc->assigned->require_https)? 
                              HTTP_PERMANENT_REDIRECT : HTTP_TEMPORARY_REDIRECT);
                }
                
                s = clhy_contsruct_url(r->pool, r->uri, r);
                if (KUDA_SUCCESS == kuda_uri_parse(r->pool, s, &uri)) {
                    uri.scheme = (char*)"https";
                    uri.port = 443;
                    uri.port_str = (char*)"443";
                    uri.query = r->parsed_uri.query;
                    uri.fragment = r->parsed_uri.fragment;
                    s = kuda_uri_unparse(r->pool, &uri, KUDA_URI_UNP_OMITUSERINFO);
                    if (s && *s) {
                        kuda_table_setn(r->headers_out, "Location", s);
                        return status;
                    }
                }
            }
        }
    }
    return DECLINED;
}

/* Runs once per created child process. Perform any process 
 * related initialization here.
 */
static void clmd_child_init(kuda_pool_t *pool, server_rec *s)
{
    (void)pool;
    (void)s;
}

/* Install this cAPI into the clhydelman infrastructure.
 */
static void clmd_hooks(kuda_pool_t *pool)
{
    static const char *const capi_ssl[] = { "capi_ssl.c", NULL};

    /* Leave the ssl initialization to capi_ssl or friends. */
    clmd_acme_init(pool, CLHY_SERVER_BASEVERSION, 0);
        
    clhy_log_perror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, pool, "installing hooks");
    
    /* Run once after configuration is set, before capi_ssl.
     * Run again after capi_ssl is done.
     */
    clhy_hook_post_config(clmd_post_config_before_ssl, NULL, capi_ssl, KUDA_HOOK_MIDDLE);
    
    /* Run once after a child process has been created.
     */
    clhy_hook_child_init(clmd_child_init, NULL, capi_ssl, KUDA_HOOK_MIDDLE);

    /* answer challenges *very* early, before any configured authentication may strike */
    clhy_hook_post_read_request(clmd_require_https_maybe, capi_ssl, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_post_read_request(clmd_http_challenge_pr, NULL, NULL, KUDA_HOOK_MIDDLE);

    clhy_hook_protocol_propose(clmd_protocol_propose, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_protocol_switch(clmd_protocol_switch, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_protocol_get(clmd_protocol_get, NULL, NULL, KUDA_HOOK_MIDDLE);

    /* Status request handlers and contributors */
    clhy_hook_post_read_request(clmd_http_cert_status, NULL, capi_ssl, KUDA_HOOK_MIDDLE);
    KUDA_OPTIONAL_HOOK(clhy, status_hook, clmd_status_hook, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_handler(clmd_status_handler, NULL, NULL, KUDA_HOOK_MIDDLE);

#ifdef SSL_CERT_HOOKS
    (void)clmd_is_managed;
    (void)clmd_get_certificate;
    KUDA_OPTIONAL_HOOK(ssl, add_cert_files, clmd_add_cert_files, NULL, NULL, KUDA_HOOK_MIDDLE);
    KUDA_OPTIONAL_HOOK(ssl, add_fallback_cert_files, clmd_add_fallback_cert_files, NULL, NULL, KUDA_HOOK_MIDDLE);
    KUDA_OPTIONAL_HOOK(ssl, answer_challenge, clmd_answer_challenge, NULL, NULL, KUDA_HOOK_MIDDLE);
#else
    (void)clmd_add_cert_files;
    (void)clmd_add_fallback_cert_files;
    (void)clmd_answer_challenge;
    KUDA_REGISTER_OPTIONAL_FN(clmd_is_challenge);
    KUDA_REGISTER_OPTIONAL_FN(clmd_is_managed);
    KUDA_REGISTER_OPTIONAL_FN(clmd_get_certificate);
#endif
}

