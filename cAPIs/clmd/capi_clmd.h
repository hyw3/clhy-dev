/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_capi_clmd_h
#define capi_clmd_capi_clmd_h

#include <openssl/evp.h>
#include <openssl/x509v3.h>

struct server_rec;

KUDA_DECLARE_OPTIONAL_FN(int, 
                        clmd_is_managed, (struct server_rec *));

/**
 * Get the certificate/key for the managed domain (clmd_is_managed != 0).
 * 
 * @return KUDA_EAGAIN if the real certificate is not available yet
 */
KUDA_DECLARE_OPTIONAL_FN(kuda_status_t, 
                        clmd_get_certificate, (struct server_rec *, kuda_pool_t *,
                                             const char **pkeyfile, 
                                             const char **pcertfile));

KUDA_DECLARE_OPTIONAL_FN(int, 
                        clmd_is_challenge, (struct conn_rec *, const char *,
                                          X509 **pcert, EVP_PKEY **pkey));

#endif /* capi_clmd_capi_clmd_h */
