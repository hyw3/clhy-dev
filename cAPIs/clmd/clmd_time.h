/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_time_h
#define capi_clmd_clmd_time_h

#include <stdio.h>

#define CLMD_SECS_PER_HOUR      (60*60)
#define CLMD_SECS_PER_DAY       (24*CLMD_SECS_PER_HOUR)

typedef struct {
    kuda_time_t start;
    kuda_time_t end;
} clmd_timeperiod_t;

kuda_time_t clmd_timeperiod_length(const clmd_timeperiod_t *period);

int clmd_timeperiod_contains(const clmd_timeperiod_t *period, kuda_time_t time);
int clmd_timeperiod_has_started(const clmd_timeperiod_t *period, kuda_time_t time);
int clmd_timeperiod_has_ended(const clmd_timeperiod_t *period, kuda_time_t time);

char *clmd_timeperiod_print(kuda_pool_t *p, const clmd_timeperiod_t *period);

/**
 * Print a human readable form of the give duration in days/hours/min/sec 
 */
const char *clmd_duration_print(kuda_pool_t *p, kuda_interval_time_t duration);

/**
 * Parse a machine readable string duration in the form of NN[unit], where
 * unit is d/h/mi/s/ms with the default given should the unit not be specified.
 */
kuda_status_t clmd_duration_parse(kuda_interval_time_t *ptimeout, const char *value, 
                               const char *def_unit);

typedef struct {
    kuda_interval_time_t norm; /* if > 0, normalized base length */
    kuda_interval_time_t len;  /* length of the timespan */
} clmd_timeslice_t;

kuda_status_t clmd_timeslice_create(const clmd_timeslice_t **pts, kuda_pool_t *p,
                                 kuda_interval_time_t norm, kuda_interval_time_t len); 

int clmd_timeslice_eq(const clmd_timeslice_t *ts1, const clmd_timeslice_t *ts2);

const char *clmd_timeslice_parse(const clmd_timeslice_t **pts, kuda_pool_t *p, 
                              const char *val, kuda_interval_time_t defnorm);
const char *clmd_timeslice_format(const clmd_timeslice_t *ts, kuda_pool_t *p);

clmd_timeperiod_t clmd_timeperiod_slice_before_end(const clmd_timeperiod_t *period, 
                                               const clmd_timeslice_t *ts);

#endif /* clmd_util_h */
