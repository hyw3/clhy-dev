/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <stdlib.h>

#include <kuda_lib.h>
#include <kuda_strings.h>
#include <kuda_buckets.h>
#include <kuda_hash.h>
#include <kuda_uri.h>

#include "clmd.h"
#include "clmd_crypt.h"
#include "clmd_json.h"
#include "clmd_jws.h"
#include "clmd_http.h"
#include "clmd_log.h"
#include "clmd_store.h"
#include "clmd_result.h"
#include "clmd_util.h"
#include "clmd_version.h"

#include "clmd_acme.h"
#include "clmd_acme_acct.h"


static const char *base_product= "-";

typedef struct acme_problem_status_t acme_problem_status_t;

struct acme_problem_status_t {
    const char *type;
    kuda_status_t rv;
};

static acme_problem_status_t Problems[] = {
    { "acme:error:badCSR",                       KUDA_EINVAL },
    { "acme:error:badNonce",                     KUDA_EAGAIN },
    { "acme:error:badSignatureAlgorithm",        KUDA_EINVAL },
    { "acme:error:invalidContact",               KUDA_BADARG },
    { "acme:error:unsupportedContact",           KUDA_EGENERAL },
    { "acme:error:malformed",                    KUDA_EINVAL },
    { "acme:error:rateLimited",                  KUDA_BADARG },
    { "acme:error:rejectedIdentifier",           KUDA_BADARG },
    { "acme:error:serverInternal",               KUDA_EGENERAL },
    { "acme:error:unauthorized",                 KUDA_EACCES },
    { "acme:error:unsupportedIdentifier",        KUDA_BADARG },
    { "acme:error:userActionRequired",           KUDA_EAGAIN },
    { "acme:error:badRevocationReason",          KUDA_EINVAL },
    { "acme:error:caa",                          KUDA_EGENERAL },
    { "acme:error:dns",                          KUDA_EGENERAL },
    { "acme:error:connection",                   KUDA_EGENERAL },
    { "acme:error:tls",                          KUDA_EGENERAL },
    { "acme:error:incorrectResponse",            KUDA_EGENERAL },
};

static kuda_status_t problem_status_get(const char *type) {
    size_t i;

    if (strstr(type, "urn:ietf:params:") == type) {
        type += strlen("urn:ietf:params:");
    }
    else if (strstr(type, "urn:") == type) {
        type += strlen("urn:");
    }
     
    for(i = 0; i < (sizeof(Problems)/sizeof(Problems[0])); ++i) {
        if (!kuda_strnatcasecmp(type, Problems[i].type)) {
            return Problems[i].rv;
        }
    }
    return KUDA_EGENERAL;
}

/**************************************************************************************************/
/* acme requests */

static void req_update_nonce(clmd_acme_t *acme, kuda_table_t *hdrs)
{
    if (hdrs) {
        const char *nonce = kuda_table_get(hdrs, "Replay-Nonce");
        if (nonce) {
            acme->nonce = kuda_pstrdup(acme->p, nonce);
        }
    }
}

static kuda_status_t http_update_nonce(const clmd_http_response_t *res)
{
    if (res->headers) {
        const char *nonce = kuda_table_get(res->headers, "Replay-Nonce");
        if (nonce) {
            clmd_acme_t *acme = res->req->baton;
            acme->nonce = kuda_pstrdup(acme->p, nonce);
        }
    }
    return res->rv;
}

static clmd_acme_req_t *clmd_acme_req_create(clmd_acme_t *acme, const char *method, const char *url)
{
    kuda_pool_t *pool;
    clmd_acme_req_t *req;
    kuda_status_t rv;
    
    rv = kuda_pool_create(&pool, acme->p);
    if (rv != KUDA_SUCCESS) {
        return NULL;
    }
    
    req = kuda_pcalloc(pool, sizeof(*req));
    if (!req) {
        kuda_pool_destroy(pool);
        return NULL;
    }
        
    req->acme = acme;
    req->p = pool;
    req->method = method;
    req->url = url;
    req->prot_hdrs = kuda_table_make(pool, 5);
    if (!req->prot_hdrs) {
        kuda_pool_destroy(pool);
        return NULL;
    }
    req->max_retries = acme->max_retries;
    req->result = clmd_result_make(req->p, KUDA_SUCCESS);
    return req;
}
 
static kuda_status_t acmev1_new_nonce(clmd_acme_t *acme)
{
    return clmd_http_HEAD(acme->http, acme->api.v1.new_reg, NULL, http_update_nonce, acme);
}

static kuda_status_t acmev2_new_nonce(clmd_acme_t *acme)
{
    return clmd_http_HEAD(acme->http, acme->api.v2.new_nonce, NULL, http_update_nonce, acme);
}


kuda_status_t clmd_acme_init(kuda_pool_t *p, const char *base,  int init_ssl)
{
    base_product = base;
    return init_ssl? clmd_crypt_init(p) : KUDA_SUCCESS;
}

static kuda_status_t inspect_problem(clmd_acme_req_t *req, const clmd_http_response_t *res)
{
    const char *ctype;
    clmd_json_t *problem;
    
    ctype = kuda_table_get(req->resp_hdrs, "content-type");
    if (ctype && !strcmp(ctype, "application/problem+json")) {
        /* RFC 7807 */
        clmd_json_read_http(&problem, req->p, res);
        if (problem) {
            const char *ptype, *pdetail;
            
            req->resp_json = problem;
            ptype = clmd_json_gets(problem, CLMD_KEY_TYPE, NULL); 
            pdetail = clmd_json_gets(problem, CLMD_KEY_DETAIL, NULL);
            req->rv = problem_status_get(ptype);
            clmd_result_problem_set(req->result, req->rv, ptype, pdetail);
            
            if (KUDA_STATUS_IS_EAGAIN(req->rv)) {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, req->rv, req->p,
                              "acme reports %s: %s", ptype, pdetail);
            }
            else {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, req->rv, req->p,
                              "acme problem %s: %s", ptype, pdetail);
            }
            return req->rv;
        }
    }
    
    if (KUDA_SUCCESS == res->rv) {
        switch (res->status) {
            case 400:
                return KUDA_EINVAL;
            case 403:
                return KUDA_EACCES;
            case 404:
                return KUDA_ENOENT;
            default:
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, 0, req->p,
                              "acme problem unknown: http status %d", res->status);
                clmd_result_printf(req->result, KUDA_EGENERAL, "unexpected http status: %d",
                                 res->status);
                return req->result->status;
        }
    }
    return res->rv;
}

/**************************************************************************************************/
/* ACME requests with nonce handling */

static kuda_status_t acmev1_req_init(clmd_acme_req_t *req, clmd_json_t *jpayload)
{
    const char *payload;
    size_t payload_len;
    
    if (!req->acme->acct) {
        return KUDA_EINVAL;
    }
    if (jpayload) {
        payload = clmd_json_writep(jpayload, req->p, CLMD_JSON_FMT_COMPACT);
        if (!payload) {
            return KUDA_EINVAL;
        }
    }
    else {
        payload = "";
    }

    payload_len = strlen(payload);
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, req->p, 
                  "acme payload(len=%" KUDA_SIZE_T_FMT "): %s", payload_len, payload);
    return clmd_jws_sign(&req->req_json, req->p, payload, payload_len,
                       req->prot_hdrs, req->acme->acct_key, NULL);
}

static kuda_status_t acmev2_req_init(clmd_acme_req_t *req, clmd_json_t *jpayload)
{
    const char *payload;
    size_t payload_len;
    
    if (!req->acme->acct) {
        return KUDA_EINVAL;
    }
    if (jpayload) {
        payload = clmd_json_writep(jpayload, req->p, CLMD_JSON_FMT_COMPACT);
        if (!payload) {
            return KUDA_EINVAL;
        }
    }
    else {
        payload = "";
    }

    payload_len = strlen(payload);
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, req->p, 
                  "acme payload(len=%" KUDA_SIZE_T_FMT "): %s", payload_len, payload);
    return clmd_jws_sign(&req->req_json, req->p, payload, payload_len,
                       req->prot_hdrs, req->acme->acct_key, req->acme->acct->url);
}

kuda_status_t clmd_acme_req_body_init(clmd_acme_req_t *req, clmd_json_t *payload)
{
    return req->acme->req_init_fn(req, payload);
}

static kuda_status_t clmd_acme_req_done(clmd_acme_req_t *req, kuda_status_t rv)
{
    if (req->result->status != KUDA_SUCCESS) {
        if (req->on_err) {
            req->on_err(req, req->result, req->baton);
        }
    }
    /* An error in rv superceeds the result->status */
    if (KUDA_SUCCESS != rv) req->result->status = rv;
    rv = req->result->status;
    /* transfer results into the acme's central result for longer life and later inspection */
    clmd_result_dup(req->acme->last, req->result);
    if (req->p) {
        kuda_pool_destroy(req->p);
    }
    return rv;
}

static kuda_status_t on_response(const clmd_http_response_t *res)
{
    clmd_acme_req_t *req = res->req->baton;
    kuda_status_t rv = res->rv;
    
    if (KUDA_SUCCESS != rv) {
        goto out;
    }
    
    req->resp_hdrs = kuda_table_clone(req->p, res->headers);
    req_update_nonce(req->acme, res->headers);
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, rv, req->p, "response: %d", res->status);
    if (res->status >= 200 && res->status < 300) {
        int processed = 0;
        
        if (req->on_json) {
            processed = 1;
            rv = clmd_json_read_http(&req->resp_json, req->p, res);
            if (KUDA_SUCCESS == rv) {
                if (clmd_log_is_level(req->p, CLMD_LOG_TRACE2)) {
                    const char *s;
                    s = clmd_json_writep(req->resp_json, req->p, CLMD_JSON_FMT_INDENT);
                    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE2, rv, req->p,
                                  "response: %s",
                                  s ? s : "<failed to serialize!>");
                }
                rv = req->on_json(req->acme, req->p, req->resp_hdrs, req->resp_json, req->baton);
            }        
            else if (KUDA_STATUS_IS_ENOENT(rv)) {
                /* not JSON content, fall through */
                processed = 0;
            }
            else {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, req->p, "parsing JSON body");
            }
        }
        
        if (!processed && req->on_res) {
            processed = 1;
            rv = req->on_res(req->acme, res, req->baton);
        }
        
        if (!processed) {
            rv = KUDA_EINVAL;
            clmd_result_printf(req->result, rv, "unable to process the response: "
                             "http-status=%d, content-type=%s", 
                             res->status, kuda_table_get(res->headers, "Content-Type"));
            clmd_result_log(req->result, CLMD_LOG_ERR);
        }
    }
    else if (KUDA_EAGAIN == (rv = inspect_problem(req, res))) {
        /* leave req alive */
        return rv;
    }

out:
    clmd_acme_req_done(req, rv);
    return rv;
}

static kuda_status_t acmev2_GET_as_POST_init(clmd_acme_req_t *req, void *baton)
{
    (void)baton;
    return clmd_acme_req_body_init(req, NULL);
}

static kuda_status_t clmd_acme_req_send(clmd_acme_req_t *req)
{
    kuda_status_t rv;
    clmd_acme_t *acme = req->acme;
    const char *body = NULL;
    clmd_result_t *result;

    assert(acme->url);
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, req->p, 
                  "sending req: %s %s", req->method, req->url);
    clmd_result_reset(req->acme->last);
    result = clmd_result_make(req->p, KUDA_SUCCESS);
    
    /* Whom are we talking to? */
    if (acme->version == CLMD_ACME_VERSION_UNKNOWN) {
        rv = clmd_acme_setup(acme, result);
        if (KUDA_SUCCESS != rv) goto leave;
    }
    
    if (!strcmp("GET", req->method) && !req->on_init && !req->req_json 
        && CLMD_ACME_VERSION_MAJOR(acme->version) > 1) {
        /* See <https://ietf-wg-acme.github.io/acme/draft-ietf-acme-acme.html#rfc.section.6.3>
         * and <https://mailarchive.ietf.org/arch/msg/acme/sotffSQ0OWV-qQJodLwWYWcEVKI>
         * and <https://community.letsencrypt.org/t/acme-v2-scheduled-deprecation-of-unauthenticated-resource-gets/74380>
         * We implement this change in ACMEv2 and higher as keeping the clmd_acme_GET() methods,
         * but switching them to POSTs with a empty, JWS signed, body when we call
         * our HTTP client. */
        req->method = "POST";
        req->on_init = acmev2_GET_as_POST_init;
    }
    
    /* Besides GET/HEAD, we always need a fresh nonce */
    if (strcmp("GET", req->method) && strcmp("HEAD", req->method)) {
        if (acme->version == CLMD_ACME_VERSION_UNKNOWN) {
            rv = clmd_acme_setup(acme, result);
            if (KUDA_SUCCESS != rv) goto leave;
        }
        if (!acme->nonce && (KUDA_SUCCESS != (rv = acme->new_nonce_fn(acme)))) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, rv, req->p, 
                          "error retrieving new nonce from ACME server");
            goto leave;
        }
        
        kuda_table_set(req->prot_hdrs, "nonce", acme->nonce);
        if (CLMD_ACME_VERSION_MAJOR(acme->version) > 1) {
            kuda_table_set(req->prot_hdrs, "url", req->url);
        }
        acme->nonce = NULL;
    }
    
    rv = req->on_init? req->on_init(req, req->baton) : KUDA_SUCCESS;
    if (KUDA_SUCCESS != rv) goto leave;
    
    if (req->req_json) {
        body = clmd_json_writep(req->req_json, req->p, CLMD_JSON_FMT_INDENT);
        if (!body) {
            rv = KUDA_EINVAL; goto leave;
        }
    }

    if (body && clmd_log_is_level(req->p, CLMD_LOG_TRACE2)) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE2, 0, req->p, 
                      "req: %s %s, body:\n%s", req->method, req->url, body);
    }
    else {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, req->p, 
                      "req: %s %s", req->method, req->url);
    }
    
    if (!strcmp("GET", req->method)) {
        rv = clmd_http_GET(req->acme->http, req->url, NULL, on_response, req);
    }
    else if (!strcmp("POST", req->method)) {
        rv = clmd_http_POSTd(req->acme->http, req->url, NULL, "application/jose+json",  
                           body, body? strlen(body) : 0, on_response, req);
    }
    else if (!strcmp("HEAD", req->method)) {
        rv = clmd_http_HEAD(req->acme->http, req->url, NULL, on_response, req);
    }
    else {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, 0, req->p, 
                      "HTTP method %s against: %s", req->method, req->url);
        rv = KUDA_ENOTIMPL;
    }
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, req->p, "req sent");
    
    if (KUDA_EAGAIN == rv && req->max_retries > 0) {
        --req->max_retries;
        rv = clmd_acme_req_send(req);
    }
    req = NULL;

leave:
    if (req) clmd_acme_req_done(req, rv);
    return rv;
}

kuda_status_t clmd_acme_POST(clmd_acme_t *acme, const char *url,
                          clmd_acme_req_init_cb *on_init,
                          clmd_acme_req_json_cb *on_json,
                          clmd_acme_req_res_cb *on_res,
                          clmd_acme_req_err_cb *on_err,
                          void *baton)
{
    clmd_acme_req_t *req;
    
    assert(url);
    assert(on_json || on_res);

    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, acme->p, "add acme POST: %s", url);
    req = clmd_acme_req_create(acme, "POST", url);
    req->on_init = on_init;
    req->on_json = on_json;
    req->on_res = on_res;
    req->on_err = on_err;
    req->baton = baton;
    
    return clmd_acme_req_send(req);
}

kuda_status_t clmd_acme_GET(clmd_acme_t *acme, const char *url,
                         clmd_acme_req_init_cb *on_init,
                         clmd_acme_req_json_cb *on_json,
                         clmd_acme_req_res_cb *on_res,
                          clmd_acme_req_err_cb *on_err,
                         void *baton)
{
    clmd_acme_req_t *req;
    
    assert(url);
    assert(on_json || on_res);

    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, acme->p, "add acme GET: %s", url);
    req = clmd_acme_req_create(acme, "GET", url);
    req->on_init = on_init;
    req->on_json = on_json;
    req->on_res = on_res;
    req->on_err = on_err;
    req->baton = baton;
    
    return clmd_acme_req_send(req);
}

void clmd_acme_report_result(clmd_acme_t *acme, kuda_status_t rv, struct clmd_result_t *result)
{
    if (acme->last->status == KUDA_SUCCESS) {
        clmd_result_set(result, rv, NULL);
    }
    else {
        clmd_result_problem_set(result, acme->last->status, acme->last->problem, acme->last->detail);
    }
}

/**************************************************************************************************/
/* GET JSON */

typedef struct {
    kuda_pool_t *pool;
    clmd_json_t *json;
} json_ctx;

static kuda_status_t on_got_json(clmd_acme_t *acme, kuda_pool_t *p, const kuda_table_t *headers, 
                                clmd_json_t *jbody, void *baton)
{
    json_ctx *ctx = baton;

    (void)acme;
    (void)p;
    (void)headers;
    ctx->json = clmd_json_clone(ctx->pool, jbody);
    return KUDA_SUCCESS;
}

kuda_status_t clmd_acme_get_json(struct clmd_json_t **pjson, clmd_acme_t *acme, 
                              const char *url, kuda_pool_t *p)
{
    kuda_status_t rv;
    json_ctx ctx;
    
    ctx.pool = p;
    ctx.json = NULL;
    
    rv = clmd_acme_GET(acme, url, NULL, on_got_json, NULL, NULL, &ctx);
    *pjson = (KUDA_SUCCESS == rv)? ctx.json : NULL;
    return rv;
}

/**************************************************************************************************/
/* Generic ACME operations */

void clmd_acme_clear_acct(clmd_acme_t *acme)
{
    acme->acct_id = NULL;
    acme->acct = NULL;
    acme->acct_key = NULL;
}

const char *clmd_acme_acct_id_get(clmd_acme_t *acme)
{
    return acme->acct_id;
}

const char *clmd_acme_acct_url_get(clmd_acme_t *acme)
{
    return acme->acct? acme->acct->url : NULL;
}

kuda_status_t clmd_acme_use_acct(clmd_acme_t *acme, clmd_store_t *store,
                              kuda_pool_t *p, const char *acct_id)
{
    clmd_acme_acct_t *acct;
    clmd_pkey_t *pkey;
    kuda_status_t rv;
    
    if (KUDA_SUCCESS == (rv = clmd_acme_acct_load(&acct, &pkey, 
                                               store, CLMD_SG_ACCOUNTS, acct_id, acme->p))) {
        if (acct->ca_url && !strcmp(acct->ca_url, acme->url)) {
            acme->acct_id = kuda_pstrdup(p, acct_id);
            acme->acct = acct;
            acme->acct_key = pkey;
            rv = clmd_acme_acct_validate(acme, store, p);
        }
        else {
            /* account is from a nother server or, more likely, from another
             * protocol endpoint on the same server */
            rv = KUDA_ENOENT;
        }
    }
    return rv;
}

kuda_status_t clmd_acme_save_acct(clmd_acme_t *acme, kuda_pool_t *p, clmd_store_t *store)
{
    return clmd_acme_acct_save(store, p, acme, &acme->acct_id, acme->acct, acme->acct_key);
}

static kuda_status_t acmev1_POST_new_account(clmd_acme_t *acme, 
                                            clmd_acme_req_init_cb *on_init,
                                            clmd_acme_req_json_cb *on_json,
                                            clmd_acme_req_res_cb *on_res,
                                            clmd_acme_req_err_cb *on_err,
                                            void *baton)
{
    return clmd_acme_POST(acme, acme->api.v1.new_reg, on_init, on_json, on_res, on_err, baton);
}

static kuda_status_t acmev2_POST_new_account(clmd_acme_t *acme, 
                                            clmd_acme_req_init_cb *on_init,
                                            clmd_acme_req_json_cb *on_json,
                                            clmd_acme_req_res_cb *on_res,
                                            clmd_acme_req_err_cb *on_err,
                                            void *baton)
{
    return clmd_acme_POST(acme, acme->api.v2.new_account, on_init, on_json, on_res, on_err, baton);
}

kuda_status_t clmd_acme_POST_new_account(clmd_acme_t *acme, 
                                      clmd_acme_req_init_cb *on_init,
                                      clmd_acme_req_json_cb *on_json,
                                      clmd_acme_req_res_cb *on_res,
                                      clmd_acme_req_err_cb *on_err,
                                      void *baton)
{
    return acme->post_new_account_fn(acme, on_init, on_json, on_res, on_err, baton);
}

/**************************************************************************************************/
/* ACME setup */

kuda_status_t clmd_acme_create(clmd_acme_t **pacme, kuda_pool_t *p, const char *url,
                            const char *proxy_url)
{
    clmd_acme_t *acme;
    const char *err = NULL;
    kuda_status_t rv;
    kuda_uri_t uri_parsed;
    size_t len;
    
    if (!url) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, KUDA_EINVAL, p, "create ACME without url");
        return KUDA_EINVAL;
    }
    
    if (KUDA_SUCCESS != (rv = clmd_util_abs_uri_check(p, url, &err))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, p, "invalid ACME uri (%s): %s", err, url);
        return rv;
    }
    
    acme = kuda_pcalloc(p, sizeof(*acme));
    acme->url = url;
    acme->p = p;
    acme->user_agent = kuda_psprintf(p, "%s capi_clmd/%s", 
                                    base_product, CAPI_CLMD_VERSION);
    acme->proxy_url = proxy_url? kuda_pstrdup(p, proxy_url) : NULL;
    acme->max_retries = 3;
    
    if (KUDA_SUCCESS != (rv = kuda_uri_parse(p, url, &uri_parsed))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, p, "parsing ACME uri: %s", url);
        return KUDA_EINVAL;
    }
    
    len = strlen(uri_parsed.hostname);
    acme->sname = (len <= 16)? uri_parsed.hostname : kuda_pstrdup(p, uri_parsed.hostname + len - 16);
    acme->version = CLMD_ACME_VERSION_UNKNOWN;
    acme->last = clmd_result_make(acme->p, KUDA_SUCCESS);
    
    *pacme = (KUDA_SUCCESS == rv)? acme : NULL;
    return rv;
}

typedef struct {
    clmd_acme_t *acme;
    clmd_result_t *result;
} update_dir_ctx;

static kuda_status_t update_directory(const clmd_http_response_t *res)
{
    clmd_http_request_t *req = res->req;
    clmd_acme_t *acme = ((update_dir_ctx *)req->baton)->acme;
    clmd_result_t *result = ((update_dir_ctx *)req->baton)->result;
    kuda_status_t rv = res->rv;
    clmd_json_t *json;
    const char *s;
    
    if (KUDA_SUCCESS != rv) goto leave;
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, rv, req->pool, "directory lookup response: %d", res->status);
    if (res->status == 503) {
        clmd_result_printf(result, KUDA_EAGAIN,
            "The ACME server at <%s> reports that Service is Unavailable (503). This "
            "may happen during maintenance for short periods of time.", acme->url); 
        clmd_result_log(result, CLMD_LOG_INFO);
        rv = result->status;
        goto leave;
    }
    else if (res->status < 200 || res->status >= 300) {
        clmd_result_printf(result, KUDA_EAGAIN,
            "The ACME server at <%s> responded with HTTP status %d. This "
            "is unusual. Please verify that the URL is correct and that you can indeed "
            "make request from the server to it by other means, e.g. invoking curl/wget.", 
            acme->url, res->status); 
        goto leave;
    }
    
    rv = clmd_json_read_http(&json, req->pool, res);
    if (KUDA_SUCCESS != rv) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, req->pool, "reading JSON body");
        goto leave;
    }
    
    if (clmd_log_is_level(acme->p, CLMD_LOG_TRACE2)) {
        s = clmd_json_writep(json, req->pool, CLMD_JSON_FMT_INDENT);
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE2, rv, req->pool,
                      "response: %s", s ? s : "<failed to serialize!>");
    }
    
    /* What have we got? */
    if ((s = clmd_json_dups(acme->p, json, "new-authz", NULL))) {
        acme->api.v1.new_authz = s;
        acme->api.v1.new_cert = clmd_json_dups(acme->p, json, "new-cert", NULL);
        acme->api.v1.new_reg = clmd_json_dups(acme->p, json, "new-reg", NULL);
        acme->api.v1.revoke_cert = clmd_json_dups(acme->p, json, "revoke-cert", NULL);
        if (acme->api.v1.new_authz && acme->api.v1.new_cert 
            && acme->api.v1.new_reg && acme->api.v1.revoke_cert) {
            acme->version = CLMD_ACME_VERSION_1;
        }
        acme->ca_agreement = clmd_json_dups(acme->p, json, "meta", "terms-of-service", NULL);
        acme->new_nonce_fn = acmev1_new_nonce;
        acme->req_init_fn = acmev1_req_init;
        acme->post_new_account_fn = acmev1_POST_new_account;
    }
    else if ((s = clmd_json_dups(acme->p, json, "newAccount", NULL))) {
        acme->api.v2.new_account = s;
        acme->api.v2.new_order = clmd_json_dups(acme->p, json, "newOrder", NULL);
        acme->api.v2.revoke_cert = clmd_json_dups(acme->p, json, "revokeCert", NULL);
        acme->api.v2.key_change = clmd_json_dups(acme->p, json, "keyChange", NULL);
        acme->api.v2.new_nonce = clmd_json_dups(acme->p, json, "newNonce", NULL);
        if (acme->api.v2.new_account && acme->api.v2.new_order 
            && acme->api.v2.revoke_cert && acme->api.v2.key_change
            && acme->api.v2.new_nonce) {
            acme->version = CLMD_ACME_VERSION_2;
        }
        acme->ca_agreement = clmd_json_dups(acme->p, json, "meta", "termsOfService", NULL);
        acme->new_nonce_fn = acmev2_new_nonce;
        acme->req_init_fn = acmev2_req_init;
        acme->post_new_account_fn = acmev2_POST_new_account;
    }
    
    if (CLMD_ACME_VERSION_UNKNOWN == acme->version) {
        clmd_result_printf(result, KUDA_EINVAL,
            "Unable to understand ACME server response from <%s>. "
            "Wrong ACME protocol version or link?", acme->url); 
        clmd_result_log(result, CLMD_LOG_WARNING);
        rv = result->status;
    }
leave:
    return rv;
}

kuda_status_t clmd_acme_setup(clmd_acme_t *acme, clmd_result_t *result)
{
    kuda_status_t rv;
    update_dir_ctx ctx;
   
    assert(acme->url);
    acme->version = CLMD_ACME_VERSION_UNKNOWN;
    
    if (!acme->http && KUDA_SUCCESS != (rv = clmd_http_create(&acme->http, acme->p,
                                                           acme->user_agent, acme->proxy_url))) {
        return rv;
    }
    clmd_http_set_response_limit(acme->http, 1024*1024);
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, acme->p, "get directory from %s", acme->url);
    
    ctx.acme = acme;
    ctx.result = result;
    rv = clmd_http_GET(acme->http, acme->url, NULL, update_directory, &ctx);
    
    if (KUDA_SUCCESS != rv && KUDA_SUCCESS == result->status) {
        /* If the result reports no error, we never got a response from the server */
        clmd_result_printf(result, rv, 
            "Unsuccessful in contacting ACME server at <%s>. If this problem persists, "
            "please check your network connectivity from your cLHy server to the "
            "ACME server. Also, older servers might have trouble verifying the certificates "
            "of the ACME server. You can check if you are able to contact it manually via the "
            "curl command. Sometimes, the ACME server might be down for maintenance, "
            "so failing to contact it is not an immediate problem. cLHy will "
            "continue retrying this.", acme->url);
        clmd_result_log(result, CLMD_LOG_WARNING);
    }
    return rv;
}


