/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_acme_acct_h
#define capi_clmd_clmd_acme_acct_h

struct clmd_acme_req;
struct clmd_json_t;
struct clmd_pkey_t;


/** 
 * An ACME account at an ACME server.
 */
typedef struct clmd_acme_acct_t clmd_acme_acct_t;

typedef enum {
    CLMD_ACME_ACCT_ST_UNKNOWN,
    CLMD_ACME_ACCT_ST_VALID,
    CLMD_ACME_ACCT_ST_DEACTIVATED,
    CLMD_ACME_ACCT_ST_REVOKED,
} clmd_acme_acct_st;

struct clmd_acme_acct_t {
    const char *id;                   /* short, unique id for the account */
    const char *url;                  /* url of the account, once registered */
    const char *ca_url;               /* url of the ACME protocol endpoint */
    clmd_acme_acct_st status;         /* status of this account */
    kuda_array_header_t *contacts;    /* list of contact uris, e.g. mailto:xxx */
    const char *tos_required;         /* terms of service asked for by CA */
    const char *agreement;            /* terms of service agreed to by user */
    const char *orders;               /* URL where certificate orders are found (ACMEv2) */
    struct clmd_json_t *registration; /* data from server registration */
};

#define CLMD_FN_ACCOUNT           "account.json"
#define CLMD_FN_ACCT_KEY          "account.pem"

/* ACME account private keys are always RSA and have that many bits. Since accounts
 * are expected to live long, better err on the safe side. */
#define CLMD_ACME_ACCT_PKEY_BITS  3072

#define CLMD_ACME_ACCT_STAGED     "staged"

/**
 * Convert an ACME account form/to JSON.
 */
struct clmd_json_t *clmd_acme_acct_to_json(clmd_acme_acct_t *acct, kuda_pool_t *p);
kuda_status_t clmd_acme_acct_from_json(clmd_acme_acct_t **pacct, struct clmd_json_t *json, kuda_pool_t *p);

/**
 * Update the account from the ACME server.
 * - Will update acme->acct structure from server on success
 * - Will return error status when request failed or account is not known.
 */
kuda_status_t clmd_acme_acct_update(clmd_acme_t *acme);

/**
 * Update the account and persist changes in the store, if given (and not NULL).
 */
kuda_status_t clmd_acme_acct_validate(clmd_acme_t *acme, struct clmd_store_t *store, kuda_pool_t *p);

/**
 * Agree to the given Terms-of-Service url for the current account.
 */
kuda_status_t clmd_acme_agree(clmd_acme_t *acme, kuda_pool_t *p, const char *tos);

/**
 * Confirm with the server that the current account agrees to the Terms-of-Service
 * given in the agreement url.
 * If the known agreement is equal to this, nothing is done.
 * If it differs, the account is re-validated in the hope that the server
 * announces the Tos URL it wants. If this is equal to the agreement specified,
 * the server is notified of this. If the server requires a ToS that the account
 * thinks it has already given, it is resend.
 *
 * If an agreement is required, different from the current one, KUDA_INCOMPLETE is
 * returned and the agreement url is returned in the parameter.
 */
kuda_status_t clmd_acme_check_agreement(clmd_acme_t *acme, kuda_pool_t *p, 
                                     const char *agreement, const char **prequired);

/**
 * Get the ToS agreement for current account.
 */
const char *clmd_acme_get_agreement(clmd_acme_t *acme);


/** 
 * Find an existing account in the local store. On KUDA_SUCCESS, the acme
 * instance will have a current, validated account to use.
 */ 
kuda_status_t clmd_acme_find_acct(clmd_acme_t *acme, struct clmd_store_t *store);

/**
 * Find the account id for a given account url. 
 */
kuda_status_t clmd_acme_acct_id_for_url(const char **pid, struct clmd_store_t *store, 
                                     clmd_store_group_t group, const char *url, kuda_pool_t *p);

/**
 * Create a new account at the ACME server. The
 * new account is the one used by the acme instance afterwards, on success.
 */
kuda_status_t clmd_acme_acct_register(clmd_acme_t *acme, struct clmd_store_t *store, 
                                   kuda_pool_t *p, kuda_array_header_t *contacts, 
                                   const char *agreement);

kuda_status_t clmd_acme_acct_save(struct clmd_store_t *store, kuda_pool_t *p, clmd_acme_t *acme,  
                               const char **pid, struct clmd_acme_acct_t *acct, 
                               struct clmd_pkey_t *acct_key);
                               
/**
 * Deactivate the current account at the ACME server. 
 */
kuda_status_t clmd_acme_acct_deactivate(clmd_acme_t *acme, kuda_pool_t *p);

kuda_status_t clmd_acme_acct_load(struct clmd_acme_acct_t **pacct, struct clmd_pkey_t **ppkey,
                               struct clmd_store_t *store, clmd_store_group_t group, 
                               const char *name, kuda_pool_t *p);

#endif /* clmd_acme_acct_h */
