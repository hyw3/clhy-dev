/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_crypt_h
#define capi_clmd_clmd_crypt_h

#include <kuda_file_io.h>

struct kuda_array_header_t;
struct clmd_t;
struct clmd_http_response_t;
struct clmd_cert_t;
struct clmd_pkey_t;
struct clmd_data;


/**************************************************************************************************/
/* random */

kuda_status_t clmd_rand_bytes(unsigned char *buf, kuda_size_t len, kuda_pool_t *p);

/**************************************************************************************************/
/* digests */
kuda_status_t clmd_crypt_sha256_digest64(const char **pdigest64, kuda_pool_t *p, 
                                      const struct clmd_data *data);
kuda_status_t clmd_crypt_sha256_digest_hex(const char **pdigesthex, kuda_pool_t *p, 
                                        const struct clmd_data *data);

#define CLMD_DATA_SET_STR(d, s)       do { (d)->data = (s); (d)->len = strlen(s); } while(0)

/**************************************************************************************************/
/* private keys */

typedef struct clmd_pkey_t clmd_pkey_t;

typedef enum {
    CLMD_PKEY_TYPE_DEFAULT,
    CLMD_PKEY_TYPE_RSA,
} clmd_pkey_type_t;

typedef struct clmd_pkey_rsa_spec_t {
    kuda_uint32_t bits;
} clmd_pkey_rsa_spec_t;

typedef struct clmd_pkey_spec_t {
    clmd_pkey_type_t type;
    union {
        clmd_pkey_rsa_spec_t rsa;
    } params;
} clmd_pkey_spec_t;

kuda_status_t clmd_crypt_init(kuda_pool_t *pool);

kuda_status_t clmd_pkey_gen(clmd_pkey_t **ppkey, kuda_pool_t *p, clmd_pkey_spec_t *spec);
void clmd_pkey_free(clmd_pkey_t *pkey);

const char *clmd_pkey_get_rsa_e64(clmd_pkey_t *pkey, kuda_pool_t *p);
const char *clmd_pkey_get_rsa_n64(clmd_pkey_t *pkey, kuda_pool_t *p);

kuda_status_t clmd_pkey_fload(clmd_pkey_t **ppkey, kuda_pool_t *p, 
                           const char *pass_phrase, kuda_size_t pass_len,
                           const char *fname);
kuda_status_t clmd_pkey_fsave(clmd_pkey_t *pkey, kuda_pool_t *p, 
                           const char *pass_phrase, kuda_size_t pass_len, 
                           const char *fname, kuda_fileperms_t perms);

kuda_status_t clmd_crypt_sign64(const char **psign64, clmd_pkey_t *pkey, kuda_pool_t *p, 
                             const char *d, size_t dlen);

void *clmd_pkey_get_EVP_PKEY(struct clmd_pkey_t *pkey);

struct clmd_json_t *clmd_pkey_spec_to_json(const clmd_pkey_spec_t *spec, kuda_pool_t *p);
clmd_pkey_spec_t *clmd_pkey_spec_from_json(struct clmd_json_t *json, kuda_pool_t *p);
int clmd_pkey_spec_eq(clmd_pkey_spec_t *spec1, clmd_pkey_spec_t *spec2);

/**************************************************************************************************/
/* X509 certificates */

typedef struct clmd_cert_t clmd_cert_t;

typedef enum {
    CLMD_CERT_UNKNOWN,
    CLMD_CERT_VALID,
    CLMD_CERT_EXPIRED
} clmd_cert_state_t;

void clmd_cert_free(clmd_cert_t *cert);
void *clmd_cert_get_X509(const clmd_cert_t *cert);

kuda_status_t clmd_cert_fload(clmd_cert_t **pcert, kuda_pool_t *p, const char *fname);
kuda_status_t clmd_cert_fsave(clmd_cert_t *cert, kuda_pool_t *p, 
                           const char *fname, kuda_fileperms_t perms);

/**
 * Read a x509 certificate from a http response.
 * Will return KUDA_ENOENT if content-type is not recognized (currently
 * only "application/pkix-cert" is supported).
 */
kuda_status_t clmd_cert_read_http(clmd_cert_t **pcert, kuda_pool_t *pool, 
                               const struct clmd_http_response_t *res);

/**
 * Read one or even a chain of certificates from a http response.
 * Will return KUDA_ENOENT if content-type is not recognized (currently
 * supports only "application/pem-certificate-chain" and "application/pkix-cert").
 * @param chain    must be non-NULL, retrieved certificates will be added.
 */
kuda_status_t clmd_cert_chain_read_http(struct kuda_array_header_t *chain,
                                     kuda_pool_t *pool, const struct clmd_http_response_t *res);

clmd_cert_state_t clmd_cert_state_get(const clmd_cert_t *cert);
int clmd_cert_is_valid_now(const clmd_cert_t *cert);
int clmd_cert_has_expired(const clmd_cert_t *cert);
int clmd_cert_covers_domain(clmd_cert_t *cert, const char *domain_name);
int clmd_cert_covers_clmd(clmd_cert_t *cert, const struct clmd_t *clmd);
int clmd_cert_must_staple(const clmd_cert_t *cert);
kuda_time_t clmd_cert_get_not_after(const clmd_cert_t *cert);
kuda_time_t clmd_cert_get_not_before(const clmd_cert_t *cert);

kuda_status_t clmd_cert_get_issuers_uri(const char **puri, const clmd_cert_t *cert, kuda_pool_t *p);
kuda_status_t clmd_cert_get_alt_names(kuda_array_header_t **pnames, const clmd_cert_t *cert, kuda_pool_t *p);

kuda_status_t clmd_cert_to_base64url(const char **ps64, const clmd_cert_t *cert, kuda_pool_t *p);
kuda_status_t clmd_cert_from_base64url(clmd_cert_t **pcert, const char *s64, kuda_pool_t *p);

kuda_status_t clmd_cert_to_sha256_digest(struct clmd_data **pdigest, const clmd_cert_t *cert, kuda_pool_t *p);
kuda_status_t clmd_cert_to_sha256_fingerprint(const char **pfinger, const clmd_cert_t *cert, kuda_pool_t *p);

const char *clmd_cert_get_serial_number(const clmd_cert_t *cert, kuda_pool_t *p);

kuda_status_t clmd_chain_fload(struct kuda_array_header_t **pcerts, 
                            kuda_pool_t *p, const char *fname);
kuda_status_t clmd_chain_fsave(struct kuda_array_header_t *certs, 
                            kuda_pool_t *p, const char *fname, kuda_fileperms_t perms);
kuda_status_t clmd_chain_fappend(struct kuda_array_header_t *certs, 
                              kuda_pool_t *p, const char *fname);

kuda_status_t clmd_cert_req_create(const char **pcsr_der_64, const char *name,
                                kuda_array_header_t *domains, int must_staple, 
                                clmd_pkey_t *pkey, kuda_pool_t *p);

/**
 * Create a self-signed cerftificate with the given cn, key and list
 * of alternate domain names.
 */
kuda_status_t clmd_cert_self_sign(clmd_cert_t **pcert, const char *cn, 
                               struct kuda_array_header_t *domains, clmd_pkey_t *pkey,
                               kuda_interval_time_t valid_for, kuda_pool_t *p);
   
/**
 * Create a certificate for answering "tls-alpn-01" ACME challenges 
 * (see <https://tools.ietf.org/html/draft-ietf-acme-tls-alpn-01>).
 */
kuda_status_t clmd_cert_make_tls_alpn_01(clmd_cert_t **pcert, const char *domain, 
                                      const char *acme_id, clmd_pkey_t *pkey, 
                                      kuda_interval_time_t valid_for, kuda_pool_t *p);

kuda_status_t clmd_cert_get_ct_scts(kuda_array_header_t *scts, kuda_pool_t *p, const clmd_cert_t *cert);


/**************************************************************************************************/
/* X509 certificate transparency */

const char *clmd_nid_get_sname(int nid);
const char *clmd_nid_get_lname(int nid);

typedef struct clmd_sct clmd_sct;
struct clmd_sct {
    int version;
    kuda_time_t timestamp;
    struct clmd_data *logid;
    int signature_type_nid;
    struct clmd_data *signature;
};

#endif /* clmd_crypt_h */
