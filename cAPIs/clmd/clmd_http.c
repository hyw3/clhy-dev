/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>

#include <kuda_lib.h>
#include <kuda_strings.h>
#include <kuda_buckets.h>

#include "clmd_http.h"
#include "clmd_log.h"

struct clmd_http_t {
    kuda_pool_t *pool;
    kuda_bucket_alloc_t *bucket_alloc;
    kuda_off_t resp_limit;
    clmd_http_impl_t *impl;
    const char *user_agent;
    const char *proxy_url;
};

static clmd_http_impl_t *cur_impl;
static int cur_init_done;

void clmd_http_use_implementation(clmd_http_impl_t *impl)
{
    if (cur_impl != impl) {
        cur_impl = impl;
        cur_init_done = 0;
    }
}

kuda_status_t clmd_http_create(clmd_http_t **phttp, kuda_pool_t *p, const char *user_agent,
                            const char *proxy_url)
{
    clmd_http_t *http;
    kuda_status_t rv = KUDA_SUCCESS;

    if (!cur_impl) {
        *phttp = NULL;
        return KUDA_ENOTIMPL;
    }
    
    if (!cur_init_done) {
        if (KUDA_SUCCESS == (rv = cur_impl->init())) {
            cur_init_done = 1;
        }
        else {
            return rv;
        }
    }
    
    http = kuda_pcalloc(p, sizeof(*http));
    http->pool = p;
    http->impl = cur_impl;
    http->user_agent = kuda_pstrdup(p, user_agent);
    http->proxy_url = proxy_url? kuda_pstrdup(p, proxy_url) : NULL;
    http->bucket_alloc = kuda_bucket_alloc_create(p);
    if (!http->bucket_alloc) {
        return KUDA_EGENERAL;
    }
    *phttp = http;
    return KUDA_SUCCESS;
}

void clmd_http_set_response_limit(clmd_http_t *http, kuda_off_t resp_limit)
{
    http->resp_limit = resp_limit;
}

static kuda_status_t req_create(clmd_http_request_t **preq, clmd_http_t *http, 
                               const char *method, const char *url, struct kuda_table_t *headers,
                               clmd_http_cb *cb, void *baton)
{
    clmd_http_request_t *req;
    kuda_pool_t *pool;
    kuda_status_t rv;
    
    rv = kuda_pool_create(&pool, http->pool);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }
    
    req = kuda_pcalloc(pool, sizeof(*req));
    req->pool = pool;
    req->bucket_alloc = http->bucket_alloc;
    req->http = http;
    req->method = method;
    req->url = url;
    req->headers = headers? kuda_table_copy(req->pool, headers) : kuda_table_make(req->pool, 5);
    req->resp_limit = http->resp_limit;
    req->cb = cb;
    req->baton = baton;
    req->user_agent = http->user_agent;
    req->proxy_url = http->proxy_url;

    *preq = req;
    return rv;
}

void clmd_http_req_destroy(clmd_http_request_t *req) 
{
    if (req->internals) {
        req->http->impl->req_cleanup(req);
        req->internals = NULL;
    }
    kuda_pool_destroy(req->pool);
}

static kuda_status_t schedule(clmd_http_request_t *req, 
                             kuda_bucket_brigade *body, int detect_clen) 
{
    kuda_status_t rv;
    
    req->body = body;
    req->body_len = body? -1 : 0;

    if (req->body && detect_clen) {
        rv = kuda_brigade_length(req->body, 1, &req->body_len);
        if (rv != KUDA_SUCCESS) {
            clmd_http_req_destroy(req);
            return rv;
        }
    }
    
    if (req->body_len == 0 && kuda_strnatcasecmp("GET", req->method)) {
        kuda_table_setn(req->headers, "Content-Length", "0");
    }
    else if (req->body_len > 0) {
        kuda_table_setn(req->headers, "Content-Length", kuda_off_t_toa(req->pool, req->body_len));
    }
    
    return req->http->impl->perform(req);
}

kuda_status_t clmd_http_GET(struct clmd_http_t *http, 
                         const char *url, struct kuda_table_t *headers,
                         clmd_http_cb *cb, void *baton)
{
    clmd_http_request_t *req;
    kuda_status_t rv;
    
    rv = req_create(&req, http, "GET", url, headers, cb, baton);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }
    
    return schedule(req, NULL, 0);
}

kuda_status_t clmd_http_HEAD(struct clmd_http_t *http, 
                          const char *url, struct kuda_table_t *headers,
                          clmd_http_cb *cb, void *baton)
{
    clmd_http_request_t *req;
    kuda_status_t rv;
    
    rv = req_create(&req, http, "HEAD", url, headers, cb, baton);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }
    
    return schedule(req, NULL, 0);
}

kuda_status_t clmd_http_POST(struct clmd_http_t *http, const char *url, 
                          struct kuda_table_t *headers, const char *content_type, 
                          kuda_bucket_brigade *body,
                          clmd_http_cb *cb, void *baton)
{
    clmd_http_request_t *req;
    kuda_status_t rv;
    
    rv = req_create(&req, http, "POST", url, headers, cb, baton);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }
    
    if (content_type) {
        kuda_table_set(req->headers, "Content-Type", content_type); 
    }
    return schedule(req, body, 1);
}

kuda_status_t clmd_http_POSTd(clmd_http_t *http, const char *url, 
                           struct kuda_table_t *headers, const char *content_type, 
                           const char *data, size_t data_len, 
                           clmd_http_cb *cb, void *baton)
{
    clmd_http_request_t *req;
    kuda_status_t rv;
    kuda_bucket_brigade *body = NULL;
    
    rv = req_create(&req, http, "POST", url, headers, cb, baton);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    if (data && data_len > 0) {
        body = kuda_brigade_create(req->pool, req->http->bucket_alloc);
        rv = kuda_brigade_write(body, NULL, NULL, data, data_len);
        if (rv != KUDA_SUCCESS) {
            clmd_http_req_destroy(req);
            return rv;
        }
    }
    
    if (content_type) {
        kuda_table_set(req->headers, "Content-Type", content_type); 
    }
     
    return schedule(req, body, 1);
}
