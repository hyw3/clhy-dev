/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include <kuda_lib.h>
#include <kuda_date.h>
#include <kuda_time.h>
#include <kuda_strings.h>

#include "clmd.h"
#include "clmd_json.h"
#include "clmd_log.h"
#include "clmd_result.h"

static const char *dup_trim(kuda_pool_t *p, const char *s)
{
    char *d = kuda_pstrdup(p, s);
    kuda_collapse_spaces(d, d);
    return d;
}

clmd_result_t *clmd_result_make(kuda_pool_t *p, kuda_status_t status)
{
    clmd_result_t *result;
    
    result = kuda_pcalloc(p, sizeof(*result));
    result->p = p;
    result->status = status;
    return result;
}

clmd_result_t *clmd_result_clmd_make(kuda_pool_t *p, const struct clmd_t *clmd)
{
    clmd_result_t *result = clmd_result_make(p, KUDA_SUCCESS);
    result->clmd = clmd;
    return result;
}

void clmd_result_reset(clmd_result_t *result)
{
    kuda_pool_t *p = result->p;
    memset(result, 0, sizeof(*result));
    result->p = p;
}

static void on_change(clmd_result_t *result)
{
    if (result->on_change) result->on_change(result, result->on_change_data);
}

void clmd_result_activity_set(clmd_result_t *result, const char *activity)
{
    clmd_result_activity_setn(result, activity? kuda_pstrdup(result->p, activity) : NULL);
}

void clmd_result_activity_setn(clmd_result_t *result, const char *activity)
{
    result->activity = activity;
    result->problem = result->detail = NULL;
    on_change(result);
}

void clmd_result_activity_printf(clmd_result_t *result, const char *fmt, ...)
{
    va_list ap;

    va_start(ap, fmt);
    clmd_result_activity_setn(result, kuda_pvsprintf(result->p, fmt, ap));
    va_end(ap);
}

void clmd_result_set(clmd_result_t *result, kuda_status_t status, const char *detail)
{
    result->status = status;
    result->problem = NULL;
    result->detail = detail? kuda_pstrdup(result->p, detail) : NULL;
    on_change(result);
}

void clmd_result_problem_set(clmd_result_t *result, kuda_status_t status,
                           const char *problem, const char *detail)
{
    result->status = status;
    result->problem = dup_trim(result->p, problem);
    result->detail = kuda_pstrdup(result->p, detail);
    on_change(result);
}

void clmd_result_problem_printf(clmd_result_t *result, kuda_status_t status,
                              const char *problem, const char *fmt, ...)
{
    va_list ap;

    result->status = status;
    result->problem = dup_trim(result->p, problem);

    va_start(ap, fmt);
    result->detail = kuda_pvsprintf(result->p, fmt, ap);
    va_end(ap);
    on_change(result);
}

void clmd_result_printf(clmd_result_t *result, kuda_status_t status, const char *fmt, ...)
{
    va_list ap;

    result->status = status;
    va_start(ap, fmt);
    result->detail = kuda_pvsprintf(result->p, fmt, ap);
    va_end(ap);
    on_change(result);
}

void clmd_result_delay_set(clmd_result_t *result, kuda_time_t ready_at)
{
    result->ready_at = ready_at;
    on_change(result);
}

clmd_result_t*clmd_result_from_json(const struct clmd_json_t *json, kuda_pool_t *p)
{
    clmd_result_t *result;
    const char *s;
    
    result = clmd_result_make(p, KUDA_SUCCESS);
    result->status = (int)clmd_json_getl(json, CLMD_KEY_STATUS, NULL);
    result->problem = clmd_json_dups(p, json, CLMD_KEY_PROBLEM, NULL);
    result->detail = clmd_json_dups(p, json, CLMD_KEY_DETAIL, NULL);
    result->activity = clmd_json_dups(p, json, CLMD_KEY_ACTIVITY, NULL);
    s = clmd_json_dups(p, json, CLMD_KEY_VALID_FROM, NULL);
    if (s && *s) result->ready_at = kuda_date_parse_rfc(s);

    return result;
}

struct clmd_json_t *clmd_result_to_json(const clmd_result_t *result, kuda_pool_t *p)
{
    clmd_json_t *json;
    char ts[KUDA_RFC822_DATE_LEN];
   
    json = clmd_json_create(p);
    clmd_json_setl(result->status, json, CLMD_KEY_STATUS, NULL);
    if (result->status > 0) {
        char buffer[HUGE_STRING_LEN];
        kuda_strerror(result->status, buffer, sizeof(buffer));
        clmd_json_sets(buffer, json, "status-description", NULL);
    }
    if (result->problem) clmd_json_sets(result->problem, json, CLMD_KEY_PROBLEM, NULL);
    if (result->detail) clmd_json_sets(result->detail, json, CLMD_KEY_DETAIL, NULL);
    if (result->activity) clmd_json_sets(result->activity, json, CLMD_KEY_ACTIVITY, NULL);
    if (result->ready_at > 0) {
        kuda_rfc822_date(ts, result->ready_at);
        clmd_json_sets(ts, json, CLMD_KEY_VALID_FROM, NULL);
    }
    return json;
}

static int str_cmp(const char *s1, const char *s2)
{
    if (s1 == s2) return 0;
    if (!s1) return -1;
    if (!s2) return 1;
    return strcmp(s1, s2);
}

int clmd_result_cmp(const clmd_result_t *r1, const clmd_result_t *r2)
{
    int n;
    if (r1 == r2) return 0;
    if (!r1) return -1;
    if (!r2) return 1;
    if ((n = r1->status - r2->status)) return n;
    if ((n = str_cmp(r1->problem, r2->problem))) return n;
    if ((n = str_cmp(r1->detail, r2->detail))) return n;
    if ((n = str_cmp(r1->activity, r2->activity))) return n;
    return (int)(r1->ready_at - r2->ready_at);
}

void clmd_result_assign(clmd_result_t *dest, const clmd_result_t *src)
{
   dest->status = src->status;
   dest->problem = src->problem;
   dest->detail = src->detail;
   dest->activity = src->activity;
   dest->ready_at = src->ready_at;
}

void clmd_result_dup(clmd_result_t *dest, const clmd_result_t *src)
{
   dest->status = src->status;
   dest->problem = src->problem? dup_trim(dest->p, src->problem) : NULL; 
   dest->detail = src->detail? kuda_pstrdup(dest->p, src->detail) : NULL; 
   dest->activity = src->activity? kuda_pstrdup(dest->p, src->activity) : NULL; 
   dest->ready_at = src->ready_at;
   on_change(dest);
}

void clmd_result_log(clmd_result_t *result, int level)
{
    if (clmd_log_is_level(result->p, (clmd_log_level_t)level)) {
        const char *sep = "";
        const char *msg = "";
        
        if (result->clmd) {
            msg = kuda_psprintf(result->p, "clmd[%s]", result->clmd->name);
            sep = " ";
        }
        if (result->activity) {
            msg = kuda_psprintf(result->p, "%s%swhile[%s]", msg, sep, result->activity);
            sep = " ";
        }
        if (result->problem) {
            msg = kuda_psprintf(result->p, "%s%sproblem[%s]", msg, sep, result->problem);
            sep = " ";
        }
        if (result->detail) {
            msg = kuda_psprintf(result->p, "%s%sdetail[%s]", msg, sep, result->detail);
            sep = " ";
        }
        clmd_log_perror(CLMD_LOG_MARK, (clmd_log_level_t)level, result->status, result->p, "%s", msg);
    }
}

void clmd_result_on_change(clmd_result_t *result, clmd_result_change_cb *cb, void *data)
{
    result->on_change = cb;
    result->on_change_data = data;
}
