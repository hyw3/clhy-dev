/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <kuda_lib.h>
#include <kuda_strings.h>
#include <kuda_buckets.h>

#include "clmd_json.h"
#include "clmd_log.h"
#include "clmd_http.h"
#include "clmd_util.h"

/* jansson thinks everyone compiles with the platform's cc in its fullest capabilities
 * when undefining their INLINEs, we get static, unused functions, arg 
 */
#if defined(__GNUC__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)
#pragma GCC diagnostic push
#endif
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunreachable-code"
#elif defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-function"
#endif

#include <jansson_config.h>
#undef  JSON_INLINE
#define JSON_INLINE 
#include <jansson.h>

#if defined(__GNUC__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)
#pragma GCC diagnostic pop
#endif
#elif defined(__clang__)
#pragma clang diagnostic pop
#endif

struct clmd_json_t {
    kuda_pool_t *p;
    json_t *j;
};

/**************************************************************************************************/
/* lifecycle */

static kuda_status_t json_pool_cleanup(void *data)
{
    clmd_json_t *json = data;
    if (json) {
        clmd_json_destroy(json);
    }
    return KUDA_SUCCESS;
}

static clmd_json_t *json_create(kuda_pool_t *pool, json_t *j)
{
    clmd_json_t *json;
    
    if (!j) {
        kuda_abortfunc_t abfn = kuda_pool_abort_get(pool);
        if (abfn) {
            abfn(KUDA_ENOMEM);
        }
        assert(j != NULL); /* failsafe in case abort is unset */
    }
    json = kuda_pcalloc(pool, sizeof(*json));
    json->p = pool;
    json->j = j;
    kuda_pool_cleanup_register(pool, json, json_pool_cleanup, kuda_pool_cleanup_null);
        
    return json;
}

clmd_json_t *clmd_json_create(kuda_pool_t *pool)
{
    return json_create(pool, json_object());
}

clmd_json_t *clmd_json_create_s(kuda_pool_t *pool, const char *s)
{
    return json_create(pool, json_string(s));
}

void clmd_json_destroy(clmd_json_t *json)
{
    if (json && json->j) {
        assert(json->j->refcount > 0);
        json_decref(json->j);
        json->j = NULL;
    }
}

clmd_json_t *clmd_json_copy(kuda_pool_t *pool, clmd_json_t *json)
{
    return json_create(pool, json_copy(json->j));
}

clmd_json_t *clmd_json_clone(kuda_pool_t *pool, clmd_json_t *json)
{
    return json_create(pool, json_deep_copy(json->j));
}

/**************************************************************************************************/
/* selectors */


static json_t *jselect(const clmd_json_t *json, va_list ap)
{
    json_t *j;
    const char *key;
    
    j = json->j;
    key = va_arg(ap, char *);
    while (key && j) {
        j = json_object_get(j, key);
        key = va_arg(ap, char *);
    }
    return j;
}

static json_t *jselect_parent(const char **child_key, int create, clmd_json_t *json, va_list ap)
{
    const char *key, *next;
    json_t *j, *jn;
    
    *child_key = NULL;
    j = json->j;
    key = va_arg(ap, char *);
    while (key && j) {
        next = va_arg(ap, char *);
        if (next) {
            jn = json_object_get(j, key);
            if (!jn && create) {
                jn = json_object();
                json_object_set_new(j, key, jn);
            }
            j = jn;
        }
        else {
            *child_key = key;
        }
        key = next;
    }
    return j;
}

static kuda_status_t jselect_add(json_t *val, clmd_json_t *json, va_list ap)
{
    const char *key;
    json_t *j, *aj;
    
    j = jselect_parent(&key, 1, json, ap);
    
    if (!j || !json_is_object(j)) {
        json_decref(val);
        return KUDA_EINVAL;
    }
    
    aj = json_object_get(j, key);
    if (!aj) {
        aj = json_array();
        json_object_set_new(j, key, aj);
    }
    
    if (!json_is_array(aj)) {
        json_decref(val);
        return KUDA_EINVAL;
    }

    json_array_append(aj, val);
    return KUDA_SUCCESS;
}

static kuda_status_t jselect_insert(json_t *val, size_t index, clmd_json_t *json, va_list ap)
{
    const char *key;
    json_t *j, *aj;
    
    j = jselect_parent(&key, 1, json, ap);
    
    if (!j || !json_is_object(j)) {
        json_decref(val);
        return KUDA_EINVAL;
    }
    
    aj = json_object_get(j, key);
    if (!aj) {
        aj = json_array();
        json_object_set_new(j, key, aj);
    }
    
    if (!json_is_array(aj)) {
        json_decref(val);
        return KUDA_EINVAL;
    }

    if (json_array_size(aj) <= index) {
        json_array_append(aj, val);
    }
    else {
        json_array_insert(aj, index, val);
    }
    return KUDA_SUCCESS;
}

static kuda_status_t jselect_set(json_t *val, clmd_json_t *json, va_list ap)
{
    const char *key;
    json_t *j;
    
    j = jselect_parent(&key, 1, json, ap);
    
    if (!j) {
        json_decref(val);
        return KUDA_EINVAL;
    }
    
    if (key) {
        if (!json_is_object(j)) {
            json_decref(val);
            return KUDA_EINVAL;
        }
        json_object_set(j, key, val);
    }
    else {
        /* replace */
        if (json->j) {
            json_decref(json->j);
        }
        json_incref(val);
        json->j = val;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t jselect_set_new(json_t *val, clmd_json_t *json, va_list ap)
{
    const char *key;
    json_t *j;
    
    j = jselect_parent(&key, 1, json, ap);
    
    if (!j) {
        json_decref(val);
        return KUDA_EINVAL;
    }
    
    if (key) {
        if (!json_is_object(j)) {
            json_decref(val);
            return KUDA_EINVAL;
        }
        json_object_set_new(j, key, val);
    }
    else {
        /* replace */
        if (json->j) {
            json_decref(json->j);
        }
        json->j = val;
    }
    return KUDA_SUCCESS;
}

int clmd_json_has_key(const clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);

    return j != NULL;
}

/**************************************************************************************************/
/* type things */

int clmd_json_is(const clmd_json_type_t jtype, clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);
    switch (jtype) {
        case CLMD_JSON_TYPE_OBJECT: return (j && json_is_object(j));
        case CLMD_JSON_TYPE_ARRAY: return (j && json_is_array(j));
        case CLMD_JSON_TYPE_STRING: return (j && json_is_string(j));
        case CLMD_JSON_TYPE_REAL: return (j && json_is_real(j));
        case CLMD_JSON_TYPE_INT: return (j && json_is_integer(j));
        case CLMD_JSON_TYPE_BOOL: return (j && (json_is_true(j) || json_is_false(j)));
        case CLMD_JSON_TYPE_NULL: return (j == NULL);
    }
    return 0;
}

/**************************************************************************************************/
/* booleans */

int clmd_json_getb(const clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);

    return j? json_is_true(j) : 0;
}

kuda_status_t clmd_json_setb(int value, clmd_json_t *json, ...)
{
    va_list ap;
    kuda_status_t rv;
    
    va_start(ap, json);
    rv = jselect_set_new(json_boolean(value), json, ap);
    va_end(ap);
    return rv;
}

/**************************************************************************************************/
/* numbers */

double clmd_json_getn(const clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);
    return (j && json_is_number(j))? json_number_value(j) : 0.0;
}

kuda_status_t clmd_json_setn(double value, clmd_json_t *json, ...)
{
    va_list ap;
    kuda_status_t rv;
    
    va_start(ap, json);
    rv = jselect_set_new(json_real(value), json, ap);
    va_end(ap);
    return rv;
}

/**************************************************************************************************/
/* longs */

long clmd_json_getl(const clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);
    return (long)((j && json_is_number(j))? json_integer_value(j) : 0L);
}

kuda_status_t clmd_json_setl(long value, clmd_json_t *json, ...)
{
    va_list ap;
    kuda_status_t rv;
    
    va_start(ap, json);
    rv = jselect_set_new(json_integer(value), json, ap);
    va_end(ap);
    return rv;
}

/**************************************************************************************************/
/* strings */

const char *clmd_json_gets(const clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);

    return (j && json_is_string(j))? json_string_value(j) : NULL;
}

const char *clmd_json_dups(kuda_pool_t *p, const clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);

    return (j && json_is_string(j))? kuda_pstrdup(p, json_string_value(j)) : NULL;
}

kuda_status_t clmd_json_sets(const char *value, clmd_json_t *json, ...)
{
    va_list ap;
    kuda_status_t rv;
    
    va_start(ap, json);
    rv = jselect_set_new(json_string(value), json, ap);
    va_end(ap);
    return rv;
}

/**************************************************************************************************/
/* json itself */

clmd_json_t *clmd_json_getj(clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);
    
    if (j) {
        if (j == json->j) {
            return json;
        }
        json_incref(j);
        return json_create(json->p, j);
    }
    return NULL;
}

const clmd_json_t *clmd_json_getcj(const clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);
    
    if (j) {
        if (j == json->j) {
            return json;
        }
        json_incref(j);
        return json_create(json->p, j);
    }
    return NULL;
}

kuda_status_t clmd_json_setj(clmd_json_t *value, clmd_json_t *json, ...)
{
    va_list ap;
    kuda_status_t rv;
    const char *key;
    json_t *j;
    
    if (value) {
        va_start(ap, json);
        rv = jselect_set(value->j, json, ap);
        va_end(ap);
    }
    else {
        va_start(ap, json);
        j = jselect_parent(&key, 1, json, ap);
        va_end(ap);
        
        if (key && j && !json_is_object(j)) {
            json_object_del(j, key);
            rv = KUDA_SUCCESS;
        }
        else {
            rv = KUDA_EINVAL;
        }
    }
    return rv;
}

kuda_status_t clmd_json_addj(clmd_json_t *value, clmd_json_t *json, ...)
{
    va_list ap;
    kuda_status_t rv;
    
    va_start(ap, json);
    rv = jselect_add(value->j, json, ap);
    va_end(ap);
    return rv;
}

kuda_status_t clmd_json_insertj(clmd_json_t *value, size_t index, clmd_json_t *json, ...)
{
    va_list ap;
    kuda_status_t rv;
    
    va_start(ap, json);
    rv = jselect_insert(value->j, index, json, ap);
    va_end(ap);
    return rv;
}


/**************************************************************************************************/
/* arrays / objects */

kuda_status_t clmd_json_clr(clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);

    if (j && json_is_object(j)) {
        json_object_clear(j);
    }
    else if (j && json_is_array(j)) {
        json_array_clear(j);
    }
    return KUDA_SUCCESS;
}

kuda_status_t clmd_json_del(clmd_json_t *json, ...)
{
    const char *key;
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect_parent(&key, 0, json, ap);
    va_end(ap);
    
    if (key && j && json_is_object(j)) {
        json_object_del(j, key);
    }
    return KUDA_SUCCESS;
}

/**************************************************************************************************/
/* object strings */

kuda_status_t clmd_json_gets_dict(kuda_table_t *dict, const clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);

    if (j && json_is_object(j)) {
        const char *key;
        json_t *val;
        
        json_object_foreach(j, key, val) {
            if (json_is_string(val)) {
                kuda_table_set(dict, key, json_string_value(val));
            }
        }
        return KUDA_SUCCESS;
    }
    return KUDA_ENOENT;
}

static int object_set(void *data, const char *key, const char *val)
{
    json_t *j = data, *nj = json_string(val);
    json_object_set(j, key, nj);
    json_decref(nj);
    return 1;
}
 
kuda_status_t clmd_json_sets_dict(kuda_table_t *dict, clmd_json_t *json, ...)
{
    json_t *nj, *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);
    
    if (!j || !json_is_object(j)) {
        const char *key;
        
        va_start(ap, json);
        j = jselect_parent(&key, 1, json, ap);
        va_end(ap);
        
        if (!key || !j || !json_is_object(j)) {
            return KUDA_EINVAL;
        }
        nj = json_object();
        json_object_set_new(j, key, nj);
        j = nj; 
    }
    
    kuda_table_do(object_set, j, dict, NULL);
    return KUDA_SUCCESS;
}

/**************************************************************************************************/
/* conversions */

kuda_status_t clmd_json_pass_to(void *value, clmd_json_t *json, kuda_pool_t *p, void *baton)
{
    (void)p;
    (void)baton;
    return clmd_json_setj(value, json, NULL);
}

kuda_status_t clmd_json_pass_from(void **pvalue, clmd_json_t *json, kuda_pool_t *p, void *baton)
{
    (void)p;
    (void)baton;
    *pvalue = json;
    return KUDA_SUCCESS;
}

kuda_status_t clmd_json_clone_to(void *value, clmd_json_t *json, kuda_pool_t *p, void *baton)
{
    (void)baton;
    return clmd_json_setj(clmd_json_clone(p, value), json, NULL);
}

kuda_status_t clmd_json_clone_from(void **pvalue, clmd_json_t *json, kuda_pool_t *p, void *baton)
{
    (void)baton;
    *pvalue = clmd_json_clone(p, json);
    return KUDA_SUCCESS;
}

/**************************************************************************************************/
/* array generic */

kuda_status_t clmd_json_geta(kuda_array_header_t *a, clmd_json_from_cb *cb, void *baton,
                          const clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    kuda_status_t rv = KUDA_SUCCESS;
    size_t index;
    json_t *val;
    clmd_json_t wrap;
    void *element;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);
    
    if (!j || !json_is_array(j)) {
        return KUDA_ENOENT;
    }
        
    wrap.p = a->pool;
    json_array_foreach(j, index, val) {
        wrap.j = val;
        if (KUDA_SUCCESS == (rv = cb(&element, &wrap, wrap.p, baton))) {
            if (element) {
                KUDA_ARRAY_PUSH(a, void*) = element;
            }
        }
        else if (KUDA_ENOENT == rv) {
            rv = KUDA_SUCCESS;
        }
        else {
            break;
        }
    }
    return rv;
}

kuda_status_t clmd_json_seta(kuda_array_header_t *a, clmd_json_to_cb *cb, void *baton, 
                          clmd_json_t *json, ...)
{
    json_t *j, *nj;
    clmd_json_t wrap;
    kuda_status_t rv = KUDA_SUCCESS;
    va_list ap;
    int i;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);
    
    if (!j || !json_is_array(j)) {
        const char *key;
        
        va_start(ap, json);
        j = jselect_parent(&key, 1, json, ap);
        va_end(ap);
        
        if (!key || !j || !json_is_object(j)) {
            return KUDA_EINVAL;
        }
        nj = json_array();
        json_object_set_new(j, key, nj);
        j = nj; 
    }
    
    json_array_clear(j);
    wrap.p = json->p;
    for (i = 0; i < a->nelts; ++i) {
        if (!cb) {
            return KUDA_EINVAL;
        }    
        wrap.j = json_string("");
        if (KUDA_SUCCESS == (rv = cb(KUDA_ARRAY_IDX(a, i, void*), &wrap, json->p, baton))) {
            json_array_append_new(j, wrap.j);
        }
    }
    return rv;
}

int clmd_json_itera(clmd_json_itera_cb *cb, void *baton, clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    size_t index;
    json_t *val;
    clmd_json_t wrap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);
    
    if (!j || !json_is_array(j)) {
        return 0;
    }
        
    wrap.p = json->p;
    json_array_foreach(j, index, val) {
        wrap.j = val;
        if (!cb(baton, index, &wrap)) {
            return 0;
        }
    }
    return 1;
}

/**************************************************************************************************/
/* array strings */

kuda_status_t clmd_json_getsa(kuda_array_header_t *a, const clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);

    if (j && json_is_array(j)) {
        size_t index;
        json_t *val;
        
        json_array_foreach(j, index, val) {
            if (json_is_string(val)) {
                KUDA_ARRAY_PUSH(a, const char *) = json_string_value(val);
            }
        }
        return KUDA_SUCCESS;
    }
    return KUDA_ENOENT;
}

kuda_status_t clmd_json_dupsa(kuda_array_header_t *a, kuda_pool_t *p, clmd_json_t *json, ...)
{
    json_t *j;
    va_list ap;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);

    if (j && json_is_array(j)) {
        size_t index;
        json_t *val;
        
        kuda_array_clear(a);
        json_array_foreach(j, index, val) {
            if (json_is_string(val)) {
                KUDA_ARRAY_PUSH(a, const char *) = kuda_pstrdup(p, json_string_value(val));
            }
        }
        return KUDA_SUCCESS;
    }
    return KUDA_ENOENT;
}

kuda_status_t clmd_json_setsa(kuda_array_header_t *a, clmd_json_t *json, ...)
{
    json_t *nj, *j;
    va_list ap;
    int i;
    
    va_start(ap, json);
    j = jselect(json, ap);
    va_end(ap);
    
    if (!j || !json_is_array(j)) {
        const char *key;
        
        va_start(ap, json);
        j = jselect_parent(&key, 1, json, ap);
        va_end(ap);
        
        if (!key || !j || !json_is_object(j)) {
            return KUDA_EINVAL;
        }
        nj = json_array();
        json_object_set_new(j, key, nj);
        j = nj; 
    }
    
    json_array_clear(j);
    for (i = 0; i < a->nelts; ++i) {
        json_array_append_new(j, json_string(KUDA_ARRAY_IDX(a, i, const char*)));
    }
    return KUDA_SUCCESS;
}

/**************************************************************************************************/
/* formatting, parsing */

typedef struct {
    clmd_json_t *json;
    clmd_json_fmt_t fmt;
    const char *fname;
    kuda_file_t *f;
} j_write_ctx;

/* Convert from clmd_json_fmt_t to the Jansson json_dumpX flags. */
static size_t fmt_to_flags(clmd_json_fmt_t fmt)
{
    /* NOTE: JSON_PRESERVE_ORDER is off by default before Jansson 2.8. It
     * doesn't have any semantic effect on the protocol, but it does let the
     * clmd_json_writeX unit tests run deterministically. */
    return JSON_PRESERVE_ORDER |
           ((fmt == CLMD_JSON_FMT_COMPACT) ? JSON_COMPACT : JSON_INDENT(2)); 
}

static int dump_cb(const char *buffer, size_t len, void *baton)
{
    kuda_bucket_brigade *bb = baton;
    kuda_status_t rv;
    
    rv = kuda_brigade_write(bb, NULL, NULL, buffer, len);
    return (rv == KUDA_SUCCESS)? 0 : -1;
}

kuda_status_t clmd_json_writeb(clmd_json_t *json, clmd_json_fmt_t fmt, kuda_bucket_brigade *bb)
{
    int rv = json_dump_callback(json->j, dump_cb, bb, fmt_to_flags(fmt));
    return rv? KUDA_EGENERAL : KUDA_SUCCESS;
}

static int chunk_cb(const char *buffer, size_t len, void *baton)
{
    kuda_array_header_t *chunks = baton;
    char *chunk = kuda_pcalloc(chunks->pool, len+1);
    
    memcpy(chunk, buffer, len);
    KUDA_ARRAY_PUSH(chunks, const char *) = chunk;
    return 0;
}

const char *clmd_json_writep(clmd_json_t *json, kuda_pool_t *p, clmd_json_fmt_t fmt)
{
    kuda_array_header_t *chunks;
    int rv;

    chunks = kuda_array_make(p, 10, sizeof(char *));
    rv = json_dump_callback(json->j, chunk_cb, chunks, fmt_to_flags(fmt));
    if (KUDA_SUCCESS != rv) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, 0, p,
                      "clmd_json_writep failed to dump JSON");
        return NULL;
    }

    switch (chunks->nelts) {
        case 0:
            return "";
        case 1:
            return KUDA_ARRAY_IDX(chunks, 0, const char *);
        default:
            return kuda_array_pstrcat(p, chunks, 0);
    }
}

kuda_status_t clmd_json_writef(clmd_json_t *json, kuda_pool_t *p, clmd_json_fmt_t fmt, kuda_file_t *f)
{
    kuda_status_t rv;
    const char *s;
    
    if ((s = clmd_json_writep(json, p, fmt))) {
        rv = kuda_file_write_full(f, s, strlen(s), NULL);
        if (KUDA_SUCCESS != rv) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, json->p, "clmd_json_writef: error writing file");
        }
    }
    else {
        rv = KUDA_EINVAL;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, json->p, "clmd_json_writef: error dumping json");
    }
    return rv;
}

kuda_status_t clmd_json_fcreatex(clmd_json_t *json, kuda_pool_t *p, clmd_json_fmt_t fmt, 
                              const char *fpath, kuda_fileperms_t perms)
{
    kuda_status_t rv;
    kuda_file_t *f;
    
    rv = clmd_util_fcreatex(&f, fpath, perms, p);
    if (KUDA_SUCCESS == rv) {
        rv = clmd_json_writef(json, p, fmt, f);
        kuda_file_close(f);
    }
    return rv;
}

static kuda_status_t write_json(void *baton, kuda_file_t *f, kuda_pool_t *p)
{
    j_write_ctx *ctx = baton;
    kuda_status_t rv = clmd_json_writef(ctx->json, p, ctx->fmt, f);
    if (KUDA_SUCCESS != rv) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, p, "freplace json in %s", ctx->fname);
    }
    return rv;
}

kuda_status_t clmd_json_freplace(clmd_json_t *json, kuda_pool_t *p, clmd_json_fmt_t fmt, 
                              const char *fpath, kuda_fileperms_t perms)
{
    j_write_ctx ctx;
    ctx.json = json;
    ctx.fmt = fmt;
    ctx.fname = fpath;
    return clmd_util_freplace(fpath, perms, p, write_json, &ctx);
}

kuda_status_t clmd_json_readd(clmd_json_t **pjson, kuda_pool_t *pool, const char *data, size_t data_len)
{
    json_error_t error;
    json_t *j;
    
    j = json_loadb(data, data_len, 0, &error);
    if (!j) {
        return KUDA_EINVAL;
    }
    *pjson = json_create(pool, j);
    return KUDA_SUCCESS;
}

static size_t load_cb(void *data, size_t max_len, void *baton)
{
    kuda_bucket_brigade *body = baton;
    size_t blen, read_len = 0;
    const char *bdata;
    char *dest = data;
    kuda_bucket *b;
    kuda_status_t rv;
    
    while (body && !KUDA_BRIGADE_EMPTY(body) && max_len > 0) {
        b = KUDA_BRIGADE_FIRST(body);
        if (KUDA_BUCKET_IS_METADATA(b)) {
            if (KUDA_BUCKET_IS_EOS(b)) {
                body = NULL;
            }
        }
        else {
            rv = kuda_bucket_read(b, &bdata, &blen, KUDA_BLOCK_READ);
            if (rv == KUDA_SUCCESS) {
                if (blen > max_len) {
                    kuda_bucket_split(b, max_len);
                    blen = max_len;
                }
                memcpy(dest, bdata, blen);
                read_len += blen;
                max_len -= blen;
                dest += blen;
            }
            else {
                body = NULL;
                if (!KUDA_STATUS_IS_EOF(rv)) {
                    /* everything beside EOF is an error */
                    read_len = (size_t)-1;
                }
            }
        }
        KUDA_BUCKET_REMOVE(b);
        kuda_bucket_delete(b);
    }
    
    return read_len;
}

kuda_status_t clmd_json_readb(clmd_json_t **pjson, kuda_pool_t *pool, kuda_bucket_brigade *bb)
{
    json_error_t error;
    json_t *j;
    
    j = json_load_callback(load_cb, bb, 0, &error);
    if (!j) {
        return KUDA_EINVAL;
    }
    *pjson = json_create(pool, j);
    return KUDA_SUCCESS;
}

static size_t load_file_cb(void *data, size_t max_len, void *baton)
{
    kuda_file_t *f = baton;
    kuda_size_t len = max_len;
    kuda_status_t rv;
    
    rv = kuda_file_read(f, data, &len);
    if (KUDA_SUCCESS == rv) {
        return len;
    }
    else if (KUDA_EOF == rv) {
        return 0;
    }
    return (size_t)-1;
}

kuda_status_t clmd_json_readf(clmd_json_t **pjson, kuda_pool_t *p, const char *fpath)
{
    kuda_file_t *f;
    json_t *j;
    kuda_status_t rv;
    json_error_t error;
    
    rv = kuda_file_open(&f, fpath, KUDA_FOPEN_READ, 0, p);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    j = json_load_callback(load_file_cb, f, 0, &error);
    if (j) {
        *pjson = json_create(p, j);
    }
    else {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, 0, p,
                      "failed to load JSON file %s: %s (line %d:%d)",
                      fpath, error.text, error.line, error.column);
    }

    kuda_file_close(f);
    return (j && *pjson) ? KUDA_SUCCESS : KUDA_EINVAL;
}

/**************************************************************************************************/
/* http get */

kuda_status_t clmd_json_read_http(clmd_json_t **pjson, kuda_pool_t *pool, const clmd_http_response_t *res)
{
    kuda_status_t rv = KUDA_ENOENT;
    if (res->rv == KUDA_SUCCESS) {
        const char *ctype = kuda_table_get(res->headers, "content-type");
        if (ctype && res->body && (strstr(ctype, "/json") || strstr(ctype, "+json"))) {
            rv = clmd_json_readb(pjson, pool, res->body);
        }
    }
    return rv;
}

typedef struct {
    kuda_status_t rv;
    kuda_pool_t *pool;
    clmd_json_t *json;
} resp_data;

static kuda_status_t json_resp_cb(const clmd_http_response_t *res)
{
    resp_data *resp = res->req->baton;
    return clmd_json_read_http(&resp->json, resp->pool, res);
}

kuda_status_t clmd_json_http_get(clmd_json_t **pjson, kuda_pool_t *pool,
                              struct clmd_http_t *http, const char *url)
{
    kuda_status_t rv;
    resp_data resp;
    
    memset(&resp, 0, sizeof(resp));
    resp.pool = pool;
    
    rv = clmd_http_GET(http, url, NULL, json_resp_cb, &resp);
    
    if (rv == KUDA_SUCCESS) {
        *pjson = resp.json;
        return resp.rv;
    }
    *pjson = NULL;
    return rv;
}


kuda_status_t clmd_json_copy_to(clmd_json_t *dest, const clmd_json_t *src, ...)
{
    json_t *j;
    va_list ap;
    kuda_status_t rv = KUDA_SUCCESS;
    
    va_start(ap, src);
    j = jselect(src, ap);
    va_end(ap);

    if (j) {
        va_start(ap, src);
        rv = jselect_set(j, dest, ap);
        va_end(ap);
    }
    return rv;
}
