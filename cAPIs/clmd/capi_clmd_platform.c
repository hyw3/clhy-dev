/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <kuda_strings.h>

#include <core_common.h>
#include <wwhy.h>
#include <http_log.h>
#include <clhy_core.h>

#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef WIN32
#include "core_winnt.h"
#endif
#if CLHY_NEED_SET_MUTEX_PERMS
#include "unixd.h"
#endif

#include "clmd_util.h"
#include "capi_clmd_platform.h"

kuda_status_t clmd_try_chown(const char *fname, unsigned int uid, int gid, kuda_pool_t *p)
{
#if CLHY_NEED_SET_MUTEX_PERMS && HAVE_UNISTD_H
    /* Since we only switch user when running as root, we only need to chown directories
     * in that case. Otherwise, the server will ignore any "user/group" directives and
     * child processes have the same privileges as the parent.
     */
    if (!geteuid()) {
        if (-1 == chown(fname, (uid_t)uid, (gid_t)gid)) {
            kuda_status_t rv = KUDA_FROM_PLATFORM_ERROR(errno);
            if (!KUDA_STATUS_IS_ENOENT(rv)) {
                clhy_log_perror(CLHYLOG_MARK, CLHYLOG_ERR, rv, p, CLHYLOGNO(10082)
                              "Can't change owner of %s", fname);
            }
            return rv;
        }
    }
    return KUDA_SUCCESS;
#else 
    return KUDA_ENOTIMPL;
#endif
}

kuda_status_t clmd_make_worker_accessible(const char *fname, kuda_pool_t *p)
{
#ifdef WIN32
    return KUDA_ENOTIMPL;
#else 
    return clmd_try_chown(fname, clhy_unixd_config.user_id, -1, p);
#endif
}

#ifdef WIN32

kuda_status_t clmd_server_graceful(kuda_pool_t *p, server_rec *s)
{
    return KUDA_ENOTIMPL;
}
 
#else

kuda_status_t clmd_server_graceful(kuda_pool_t *p, server_rec *s)
{ 
    kuda_status_t rv;
    
    (void)p;
    (void)s;
    rv = (kill(getppid(), CLHY_SIG_GRACEFUL) < 0)? KUDA_ENOTIMPL : KUDA_SUCCESS;
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE1, errno, NULL, "sent signal to parent");
    return rv;
}

#endif

