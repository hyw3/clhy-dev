/* The cLHy Server
 * 
 * Copyright (C) 2019 greenbytes GmbH (https://www.greenbytes.de)
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef clmd_acme_order_h
#define clmd_acme_order_h

struct clmd_json_t;
struct clmd_result_t;

typedef struct clmd_acme_order_t clmd_acme_order_t;

typedef enum {
    CLMD_ACME_ORDER_ST_PENDING,
    CLMD_ACME_ORDER_ST_READY,
    CLMD_ACME_ORDER_ST_PROCESSING,
    CLMD_ACME_ORDER_ST_VALID,
    CLMD_ACME_ORDER_ST_INVALID,
} clmd_acme_order_st;

struct clmd_acme_order_t {
    kuda_pool_t *p;
    const char *url;
    clmd_acme_order_st status;
    struct kuda_array_header_t *authz_urls;
    struct kuda_array_header_t *challenge_setups;
    struct clmd_json_t *json;
    const char *finalize;
    const char *certificate;
};

#define CLMD_FN_ORDER             "order.json"

/**************************************************************************************************/

clmd_acme_order_t *clmd_acme_order_create(kuda_pool_t *p);

kuda_status_t clmd_acme_order_add(clmd_acme_order_t *order, const char *authz_url);
kuda_status_t clmd_acme_order_remove(clmd_acme_order_t *order, const char *authz_url);

struct clmd_json_t *clmd_acme_order_to_json(clmd_acme_order_t *set, kuda_pool_t *p);
clmd_acme_order_t *clmd_acme_order_from_json(struct clmd_json_t *json, kuda_pool_t *p);

kuda_status_t clmd_acme_order_load(struct clmd_store_t *store, clmd_store_group_t group, 
                                    const char *clmd_name, clmd_acme_order_t **pauthz_set, 
                                    kuda_pool_t *p);
kuda_status_t clmd_acme_order_save(struct clmd_store_t *store, kuda_pool_t *p, 
                                    clmd_store_group_t group, const char *clmd_name, 
                                    clmd_acme_order_t *authz_set, int create);

kuda_status_t clmd_acme_order_purge(struct clmd_store_t *store, kuda_pool_t *p, 
                                 clmd_store_group_t group, const char *clmd_name,
                                 kuda_table_t *env);


kuda_status_t clmd_acme_order_start_challenges(clmd_acme_order_t *order, clmd_acme_t *acme, 
                                            kuda_array_header_t *challenge_types,
                                            clmd_store_t *store, const clmd_t *clmd, 
                                            kuda_table_t *env, struct clmd_result_t *result,
                                            kuda_pool_t *p);

kuda_status_t clmd_acme_order_monitor_authzs(clmd_acme_order_t *order, clmd_acme_t *acme, 
                                          const clmd_t *clmd, kuda_interval_time_t timeout,
                                          struct clmd_result_t *result, kuda_pool_t *p);

/* ACMEv2 only ************************************************************************************/

kuda_status_t clmd_acme_order_register(clmd_acme_order_t **porder, clmd_acme_t *acme, kuda_pool_t *p, 
                                    const char *name, struct kuda_array_header_t *domains);

kuda_status_t clmd_acme_order_update(clmd_acme_order_t *order, clmd_acme_t *acme, 
                                  struct clmd_result_t *result, kuda_pool_t *p);

kuda_status_t clmd_acme_order_await_ready(clmd_acme_order_t *order, clmd_acme_t *acme, 
                                       const clmd_t *clmd, kuda_interval_time_t timeout, 
                                       struct clmd_result_t *result, kuda_pool_t *p);
kuda_status_t clmd_acme_order_await_valid(clmd_acme_order_t *order, clmd_acme_t *acme, 
                                       const clmd_t *clmd, kuda_interval_time_t timeout, 
                                       struct clmd_result_t *result, kuda_pool_t *p);

#endif /* clmd_acme_order_h */
