/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_platform_h
#define capi_clmd_clmd_platform_h

/**
 * Try chown'ing the file/directory. Give id -1 to not change uid/gid.
 * Will return KUDA_ENOTIMPL on platforms not supporting this operation.
 */
kuda_status_t clmd_try_chown(const char *fname, unsigned int uid, int gid, kuda_pool_t *p);

/**
 * Make a file or directory read/write(/searchable) by wwhy workers.
 */
kuda_status_t clmd_make_worker_accessible(const char *fname, kuda_pool_t *p);

/**
 * Trigger a graceful restart of the server. Depending on the architecture, may
 * return KUDA_ENOTIMPL.
 */
kuda_status_t clmd_server_graceful(kuda_pool_t *p, server_rec *s);

#endif /* capi_clmd_clmd_platform_h */
