/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <stdlib.h>

#include <kuda_lib.h>
#include <kuda_strings.h>
#include <kuda_buckets.h>
#include <kuda_hash.h>
#include <kuda_uri.h>

#include "clmd.h"
#include "clmd_crypt.h"
#include "clmd_json.h"
#include "clmd_jws.h"
#include "clmd_http.h"
#include "clmd_log.h"
#include "clmd_result.h"
#include "clmd_reg.h"
#include "clmd_store.h"
#include "clmd_util.h"

#include "clmd_acme.h"
#include "clmd_acme_acct.h"
#include "clmd_acme_authz.h"
#include "clmd_acme_order.h"

#include "clmd_acme_drive.h"
#include "clmd_acmev2_drive.h"



/**************************************************************************************************/
/* order setup */

/**
 * Either we have an order stored in the STAGING area, or we need to create a 
 * new one at the ACME server.
 */
static kuda_status_t ad_setup_order(clmd_proto_driver_t *d, clmd_result_t *result)
{
    clmd_acme_driver_t *ad = d->baton;
    kuda_status_t rv;
    clmd_t *clmd = ad->clmd;
    
    assert(ad->clmd);
    assert(ad->acme);

    /* For each domain in CLMD: AUTHZ setup
     * if an AUTHZ resource is known, check if it is still valid
     * if known AUTHZ resource is not valid, remove, goto 4.1.1
     * if no AUTHZ available, create a new one for the domain, store it
     */
    rv = clmd_acme_order_load(d->store, CLMD_SG_STAGING, clmd->name, &ad->order, d->p);
    if (KUDA_SUCCESS == rv) {
        clmd_result_activity_setn(result, "Loaded order from staging");
        goto leave;
    }
    else if (!KUDA_STATUS_IS_ENOENT(rv)) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "%s: loading order", clmd->name);
        clmd_acme_order_purge(d->store, d->p, CLMD_SG_STAGING, clmd->name, d->env);
    }
    
    clmd_result_activity_setn(result, "Creating new order");
    rv = clmd_acme_order_register(&ad->order, ad->acme, d->p, d->clmd->name, ad->domains);
    if (KUDA_SUCCESS !=rv) goto leave;
    rv = clmd_acme_order_save(d->store, d->p, CLMD_SG_STAGING, d->clmd->name, ad->order, 0);
    if (KUDA_SUCCESS != rv) {
        clmd_result_set(result, rv, "saving order in staging");
    }
    
leave:
    clmd_acme_report_result(ad->acme, rv, result);
    return rv;
}

/**************************************************************************************************/
/* ACMEv2 renewal */

kuda_status_t clmd_acmev2_drive_renew(clmd_acme_driver_t *ad, clmd_proto_driver_t *d, clmd_result_t *result)
{
    kuda_status_t rv = KUDA_SUCCESS;
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, 0, d->p, "%s: (ACMEv2) need certificate", d->clmd->name);
    
    /* Chose (or create) and ACME account to use */
    rv = clmd_acme_drive_set_acct(d, result);
    if (KUDA_SUCCESS != rv) goto leave;

    if (!clmd_array_is_empty(ad->certs)) goto leave;
        
    /* ACMEv2 strategy:
     * 1. load an clmd_acme_order_t from STAGING, if present
     * 2. if no order found, register a new order at ACME server
     * 3. update the order from the server
     * 4. Switch order state:
     *   * PENDING: process authz challenges
     *   * READY: finalize the order
     *   * PROCESSING: wait and re-assses later
     *   * VALID: retrieve certificate
     *   * COMPLETE: all done, return success
     *   * INVALID and otherwise: fail renewal, delete local order
     */
    if (KUDA_SUCCESS != (rv = ad_setup_order(d, result))) {
        goto leave;
    }
    
    rv = clmd_acme_order_update(ad->order, ad->acme, result, d->p);
    if (KUDA_STATUS_IS_ENOENT(rv)) {
        /* order is no longer known at the ACME server */
        ad->order = NULL;
        clmd_acme_order_purge(d->store, d->p, CLMD_SG_STAGING, d->clmd->name, d->env);
    }
    else if (KUDA_SUCCESS != rv) {
        goto leave;
    }
    
    if (!ad->order) {
        rv = ad_setup_order(d, result);
        if (KUDA_SUCCESS != rv) goto leave;
    }
    
    rv = clmd_acme_order_start_challenges(ad->order, ad->acme, ad->ca_challenges,
                                        d->store, d->clmd, d->env, result, d->p);
    if (KUDA_SUCCESS != rv) goto leave;
    
    rv = clmd_acme_order_monitor_authzs(ad->order, ad->acme, d->clmd,
                                      ad->authz_monitor_timeout, result, d->p);
    if (KUDA_SUCCESS != rv) goto leave;
    
    rv = clmd_acme_order_await_ready(ad->order, ad->acme, d->clmd, 
                                   ad->authz_monitor_timeout, result, d->p);
    if (KUDA_SUCCESS != rv) goto leave;
    
    rv = clmd_acme_drive_setup_certificate(d, result);
    if (KUDA_SUCCESS != rv) goto leave;
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, 0, d->p, "%s: finalized order", d->clmd->name);
    
    rv = clmd_acme_order_await_valid(ad->order, ad->acme, d->clmd, 
                                   ad->authz_monitor_timeout, result, d->p);
    if (KUDA_SUCCESS != rv) goto leave;
    
    if (ad->order->certificate) goto leave;
    clmd_result_set(result, KUDA_EINVAL, "Order valid, but certifiate url is missing.");

leave:    
    clmd_result_log(result, CLMD_LOG_DEBUG);
    return result->status;
}

