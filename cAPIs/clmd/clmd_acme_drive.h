/* The cLHy Server
 * 
 * Copyright (C) 2019 greenbytes GmbH (https://www.greenbytes.de)
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef clmd_acme_drive_h
#define clmd_acme_drive_h

struct kuda_array_header_t;
struct clmd_acme_order_t;
struct clmd_result_t;

typedef struct clmd_acme_driver_t {
    clmd_proto_driver_t *driver;
    void *sub_driver;
    
    int complete;

    clmd_pkey_t *privkey;            /* the new private key */
    kuda_array_header_t *certs;      /* the certifiacte chain, starting with the new one */
    const char *next_up_link;        /* where the next chain cert is */
    
    clmd_acme_t *acme;
    clmd_t *clmd;
    struct kuda_array_header_t *domains;
    
    kuda_array_header_t *ca_challenges;
    struct clmd_acme_order_t *order;
    kuda_interval_time_t authz_monitor_timeout;
    
    const char *csr_der_64;
    kuda_interval_time_t cert_poll_timeout;
    
} clmd_acme_driver_t;

kuda_status_t clmd_acme_drive_set_acct(struct clmd_proto_driver_t *d, 
                                    struct clmd_result_t *result);
kuda_status_t clmd_acme_drive_setup_certificate(struct clmd_proto_driver_t *d, 
                                             struct clmd_result_t *result);
kuda_status_t clmd_acme_drive_cert_poll(struct clmd_proto_driver_t *d, int only_once);

#endif /* clmd_acme_drive_h */

