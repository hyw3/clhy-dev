/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <stdlib.h>

#include <kuda_lib.h>
#include <kuda_strings.h>
#include <kuda_buckets.h>
#include <kuda_hash.h>
#include <kuda_uri.h>

#include "clmd.h"
#include "clmd_crypt.h"
#include "clmd_json.h"
#include "clmd_jws.h"
#include "clmd_http.h"
#include "clmd_log.h"
#include "clmd_result.h"
#include "clmd_reg.h"
#include "clmd_store.h"
#include "clmd_util.h"

#include "clmd_acme.h"
#include "clmd_acme_acct.h"
#include "clmd_acme_authz.h"
#include "clmd_acme_order.h"

#include "clmd_acme_drive.h"
#include "clmd_acmev1_drive.h"

/**************************************************************************************************/
/* authz/challenge setup */

/**
 * Pre-Req: we have an account for the ACME server that has accepted the current license agreement
 * For each domain in CLMD: 
 * - check if there already is a valid AUTHZ resource
 * - if ot, create an AUTHZ resource with challenge data 
 */
static kuda_status_t ad_setup_order(clmd_proto_driver_t *d, clmd_result_t *result)
{
    clmd_acme_driver_t *ad = d->baton;
    kuda_status_t rv;
    clmd_t *clmd = ad->clmd;
    const char *url;
    clmd_acme_authz_t *authz;
    kuda_array_header_t *domains_covered;
    int i;
    int changed = 0;
    
    assert(ad->clmd);
    assert(ad->acme);

    clmd_result_activity_printf(result, "Setup order resource for %s", ad->clmd->name);
    
    /* For each domain in CLMD: AUTHZ setup
     * if an AUTHZ resource is known, check if it is still valid
     * if known AUTHZ resource is not valid, remove, goto 4.1.1
     * if no AUTHZ available, create a new one for the domain, store it
     */
    rv = clmd_acme_order_load(d->store, CLMD_SG_STAGING, clmd->name, &ad->order, d->p);
    if (!ad->order || KUDA_STATUS_IS_ENOENT(rv)) {
        ad->order = clmd_acme_order_create(d->p);
        rv = KUDA_SUCCESS;
    }
    else if (KUDA_SUCCESS != rv) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "%s: loading authz data", clmd->name);
        clmd_acme_order_purge(d->store, d->p, CLMD_SG_STAGING, clmd->name, d->env);
        return KUDA_EAGAIN;
    }
    
    /* Retrieve all known authz from ACME server and check status etc. */
    domains_covered = kuda_array_make(d->p, 5, sizeof(const char *));
    
    for (i = 0; i < ad->order->authz_urls->nelts;) {
        url = KUDA_ARRAY_IDX(ad->order->authz_urls, i, const char*);
        rv = clmd_acme_authz_retrieve(ad->acme, d->p, url, &authz);
        if (KUDA_SUCCESS == rv) {
            if (clmd_array_str_index(ad->domains, authz->domain, 0, 0) < 0) {
                clmd_acme_order_remove(ad->order, url);
                changed = 1;
                continue;
            }
        
            KUDA_ARRAY_PUSH(domains_covered, const char *) = authz->domain;
            ++i;
        }
        else if (KUDA_STATUS_IS_ENOENT(rv)) {
            clmd_acme_order_remove(ad->order, url);
            changed = 1;
            continue;
        }
        else {
            goto leave;
        }
    }
    
    /* Do we have authz urls for all domains? If not, register a new one */
    for (i = 0; i < ad->domains->nelts && KUDA_SUCCESS == rv; ++i) {
        const char *domain = KUDA_ARRAY_IDX(ad->domains, i, const char *);
    
        if (clmd_array_str_index(domains_covered, domain, 0, 0) < 0) {
            clmd_result_activity_printf(result, "Creating authz resource for %s", domain);
            rv = clmd_acme_authz_register(&authz, ad->acme, domain, d->p);
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, d->p, "%s: created authz for %s (last problem: %s)", 
                          clmd->name, domain, ad->acme->last->problem);
            if (KUDA_SUCCESS != rv) goto leave;
            rv = clmd_acme_order_add(ad->order, authz->url);
            changed = 1;
        }
    }
    
    if (changed) {
        rv = clmd_acme_order_save(d->store, d->p, CLMD_SG_STAGING, clmd->name, ad->order, 0);
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, rv, d->p, "%s: saved", clmd->name);
    }
    
leave:
    clmd_acme_report_result(ad->acme, rv, result);
    return rv;
}

kuda_status_t clmd_acmev1_drive_renew(clmd_acme_driver_t *ad, clmd_proto_driver_t *d, clmd_result_t *result)
{
    kuda_status_t rv = KUDA_SUCCESS;
    const char *required;
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, 0, d->p, "%s: (ACMEv1) need certificate", d->clmd->name);
    
    /* Chose (or create) and ACME account to use */
    if (KUDA_SUCCESS != (rv = clmd_acme_drive_set_acct(d, result))) goto leave;
    
    /* Check that the account agreed to the terms-of-service, otherwise
     * requests for new authorizations are denied. ToS may change during the
     * lifetime of an account */
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_INFO, 0, d->p, 
                  "%s: (ACMEv1) check Tems-of-Service agreement", d->clmd->name);
    
    rv = clmd_acme_check_agreement(ad->acme, d->p, ad->clmd->ca_agreement, &required);
    if (KUDA_STATUS_IS_INCOMPLETE(rv) && required) {
        /* The CA wants the user to agree to Terms-of-Services. Until the user
         * has reconfigured and restarted the server, this CLMD cannot be
         * driven further */
        ad->clmd->state = CLMD_S_MISSING_INFORMATION;
        clmd_save(d->store, d->p, CLMD_SG_STAGING, ad->clmd, 0);
        clmd_result_printf(result, rv, 
            "the CA requires you to accept the terms-of-service as specified in <%s>. "
            "Please read the document that you find at that URL and, if you agree to "
            "the conditions, configure \"MDCertificateAgreement accepted\" "
            "in your cLHy. Then (graceful) restart the server to activate.", 
            required);
        goto leave;
    }
    else if (KUDA_SUCCESS != rv) goto leave;
    
    if (!clmd_array_is_empty(ad->certs)) goto leave;
    
    rv = ad_setup_order(d, result);
    if (KUDA_SUCCESS != rv) goto leave;
    
    rv = clmd_acme_order_start_challenges(ad->order, ad->acme, ad->ca_challenges,
                                        d->store, d->clmd, d->env, result, d->p);
    if (KUDA_SUCCESS != rv) goto leave;
    
    rv = clmd_acme_order_monitor_authzs(ad->order, ad->acme, d->clmd,
                                      ad->authz_monitor_timeout, result, d->p);
    if (KUDA_SUCCESS != rv) goto leave;
    
    rv = clmd_acme_drive_setup_certificate(d, result);

leave:    
    clmd_result_log(result, CLMD_LOG_DEBUG);
    return result->status;
}

