/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include <kuda_lib.h>
#include <kuda_hash.h>
#include <kuda_strings.h>
#include <kuda_uri.h>

#include "clmd.h"
#include "clmd_crypt.h"
#include "clmd_log.h"
#include "clmd_json.h"
#include "clmd_result.h"
#include "clmd_reg.h"
#include "clmd_store.h"
#include "clmd_status.h"
#include "clmd_util.h"

#include "clmd_acme.h"
#include "clmd_acme_acct.h"

struct clmd_reg_t {
    kuda_pool_t *p;
    struct clmd_store_t *store;
    struct kuda_hash_t *protos;
    struct kuda_hash_t *certs;
    int can_http;
    int can_https;
    const char *proxy_url;
    int domains_frozen;
    const clmd_timeslice_t *renew_window;
    const clmd_timeslice_t *warn_window;
};

/**************************************************************************************************/
/* life cycle */

static kuda_status_t load_props(clmd_reg_t *reg, kuda_pool_t *p)
{
    clmd_json_t *json;
    kuda_status_t rv;
    
    rv = clmd_store_load(reg->store, CLMD_SG_NONE, NULL, CLMD_FN_WWHY_JSON, 
                       CLMD_SV_JSON, (void**)&json, p);
    if (KUDA_SUCCESS == rv) {
        if (clmd_json_has_key(json, CLMD_KEY_PROTO, CLMD_KEY_HTTP, NULL)) {
            reg->can_http = clmd_json_getb(json, CLMD_KEY_PROTO, CLMD_KEY_HTTP, NULL);
        }
        if (clmd_json_has_key(json, CLMD_KEY_PROTO, CLMD_KEY_HTTPS, NULL)) {
            reg->can_https = clmd_json_getb(json, CLMD_KEY_PROTO, CLMD_KEY_HTTPS, NULL);
        }
    }
    else if (KUDA_STATUS_IS_ENOENT(rv)) {
        rv = KUDA_SUCCESS;
    }
    return rv;
}

kuda_status_t clmd_reg_create(clmd_reg_t **preg, kuda_pool_t *p, struct clmd_store_t *store,
                           const char *proxy_url)
{
    clmd_reg_t *reg;
    kuda_status_t rv;
    
    reg = kuda_pcalloc(p, sizeof(*reg));
    reg->p = p;
    reg->store = store;
    reg->protos = kuda_hash_make(p);
    reg->certs = kuda_hash_make(p);
    reg->can_http = 1;
    reg->can_https = 1;
    reg->proxy_url = proxy_url? kuda_pstrdup(p, proxy_url) : NULL;
    
    clmd_timeslice_create(&reg->renew_window, p, CLMD_TIME_LIFE_NORM, CLMD_TIME_RENEW_WINDOW_DEF); 
    clmd_timeslice_create(&reg->warn_window, p, CLMD_TIME_LIFE_NORM, CLMD_TIME_WARN_WINDOW_DEF); 
    
    if (KUDA_SUCCESS == (rv = clmd_acme_protos_add(reg->protos, p))) {
        rv = load_props(reg, p);
    }
    
    *preg = (rv == KUDA_SUCCESS)? reg : NULL;
    return rv;
}

struct clmd_store_t *clmd_reg_store_get(clmd_reg_t *reg)
{
    return reg->store;
}

/**************************************************************************************************/
/* checks */

static kuda_status_t check_values(clmd_reg_t *reg, kuda_pool_t *p, const clmd_t *clmd, int fields)
{
    kuda_status_t rv = KUDA_SUCCESS;
    const char *err = NULL;
    
    if (CLMD_UPD_DOMAINS & fields) {
        const clmd_t *other;
        const char *domain;
        int i;
        
        if (!clmd->domains || clmd->domains->nelts <= 0) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, KUDA_EINVAL, p, 
                          "empty domain list: %s", clmd->name);
            return KUDA_EINVAL;
        }
        
        for (i = 0; i < clmd->domains->nelts; ++i) {
            domain = KUDA_ARRAY_IDX(clmd->domains, i, const char *);
            if (!clmd_dns_is_name(p, domain, 1) && !clmd_dns_is_wildcard(p, domain)) {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, KUDA_EINVAL, p, 
                              "clmd %s with invalid domain name: %s", clmd->name, domain);
                return KUDA_EINVAL;
            }
        }

        if (NULL != (other = clmd_reg_find_overlap(reg, clmd, &domain, p))) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, KUDA_EINVAL, p, 
                          "clmd %s shares domain '%s' with clmd %s", 
                          clmd->name, domain, other->name);
            return KUDA_EINVAL;
        }
    }
    
    if (CLMD_UPD_CONTACTS & fields) {
        const char *contact;
        int i;

        for (i = 0; i < clmd->contacts->nelts && !err; ++i) {
            contact = KUDA_ARRAY_IDX(clmd->contacts, i, const char *);
            rv = clmd_util_abs_uri_check(p, contact, &err);
            
            if (err) {
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, KUDA_EINVAL, p, 
                              "contact for %s invalid (%s): %s", clmd->name, err, contact);
                return KUDA_EINVAL;
            }
        }
    }
    
    if ((CLMD_UPD_CA_URL & fields) && clmd->ca_url) { /* setting to empty is ok */
        rv = clmd_util_abs_uri_check(p, clmd->ca_url, &err);
        if (err) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, KUDA_EINVAL, p, 
                          "CA url for %s invalid (%s): %s", clmd->name, err, clmd->ca_url);
            return KUDA_EINVAL;
        }
    }
    
    if ((CLMD_UPD_CA_PROTO & fields) && clmd->ca_proto) { /* setting to empty is ok */
        /* Do we want to restrict this to "known" protocols? */
    }
    
    if ((CLMD_UPD_CA_ACCOUNT & fields) && clmd->ca_account) { /* setting to empty is ok */
        /* hmm, in case we know the protocol, some checks could be done */
    }

    if ((CLMD_UPD_AGREEMENT & fields) && clmd->ca_agreement
        && strcmp("accepted", clmd->ca_agreement)) { /* setting to empty is ok */
        rv = clmd_util_abs_uri_check(p, clmd->ca_agreement, &err);
        if (err) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, KUDA_EINVAL, p, 
                          "CA url for %s invalid (%s): %s", clmd->name, err, clmd->ca_agreement);
            return KUDA_EINVAL;
        }
    }

    return rv;
}

/**************************************************************************************************/
/* state assessment */

static kuda_status_t state_init(clmd_reg_t *reg, kuda_pool_t *p, clmd_t *clmd)
{
    clmd_state_t state = CLMD_S_UNKNOWN;
    const clmd_pubcert_t *pub;
    const clmd_cert_t *cert;
    kuda_status_t rv;

    if (clmd->renew_window == NULL) clmd->renew_window = reg->renew_window;
    if (clmd->warn_window == NULL) clmd->warn_window = reg->warn_window;

    if (KUDA_SUCCESS == (rv = clmd_reg_get_pubcert(&pub, reg, clmd, p))) {
        cert = KUDA_ARRAY_IDX(pub->certs, 0, const clmd_cert_t*);
        if (!clmd_is_covered_by_alt_names(clmd, pub->alt_names)) {
            state = CLMD_S_INCOMPLETE;
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, 
                          "clmd{%s}: incomplete, cert no longer covers all domains, "
                          "needs sign up for a new certificate", clmd->name);
            goto out;
        }
        if (!clmd->must_staple != !clmd_cert_must_staple(cert)) {
            state = CLMD_S_INCOMPLETE;
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, 
                          "clmd{%s}: OCSP Stapling is%s requested, but certificate "
                          "has it%s enabled. Need to get a new certificate.", clmd->name,
                          clmd->must_staple? "" : " not", 
                          !clmd->must_staple? "" : " not");
            goto out;
        }
        
        state = CLMD_S_COMPLETE;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, "clmd{%s}: is complete", clmd->name);
    }
    else if (KUDA_STATUS_IS_ENOENT(rv)) {
        state = CLMD_S_INCOMPLETE;
        rv = KUDA_SUCCESS;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, 
                      "clmd{%s}: incomplete, credentials not all there", clmd->name);
    }

out:    
    if (KUDA_SUCCESS != rv) {
        state = CLMD_S_ERROR;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, rv, p, "clmd{%s}: error", clmd->name);
    }
    clmd->state = state;
    return rv;
}

/**************************************************************************************************/
/* iteration */

typedef struct {
    clmd_reg_t *reg;
    clmd_reg_do_cb *cb;
    void *baton;
    const char *exclude;
    const void *result;
} reg_do_ctx;

static int reg_clmd_iter(void *baton, clmd_store_t *store, clmd_t *clmd, kuda_pool_t *ptemp)
{
    reg_do_ctx *ctx = baton;
    
    (void)store;
    if (!ctx->exclude || strcmp(ctx->exclude, clmd->name)) {
        state_init(ctx->reg, ptemp, (clmd_t*)clmd);
        return ctx->cb(ctx->baton, ctx->reg, clmd);
    }
    return 1;
}

static int reg_do(clmd_reg_do_cb *cb, void *baton, clmd_reg_t *reg, kuda_pool_t *p, const char *exclude)
{
    reg_do_ctx ctx;
    
    ctx.reg = reg;
    ctx.cb = cb;
    ctx.baton = baton;
    ctx.exclude = exclude;
    return clmd_store_clmd_iter(reg_clmd_iter, &ctx, reg->store, p, CLMD_SG_DOMAINS, "*");
}


int clmd_reg_do(clmd_reg_do_cb *cb, void *baton, clmd_reg_t *reg, kuda_pool_t *p)
{
    return reg_do(cb, baton, reg, p, NULL);
}

/**************************************************************************************************/
/* lookup */

clmd_t *clmd_reg_get(clmd_reg_t *reg, const char *name, kuda_pool_t *p)
{
    clmd_t *clmd;
    
    if (KUDA_SUCCESS == clmd_load(reg->store, CLMD_SG_DOMAINS, name, &clmd, p)) {
        state_init(reg, p, clmd);
        return clmd;
    }
    return NULL;
}

kuda_status_t clmd_reg_reinit_state(clmd_reg_t *reg, clmd_t *clmd, kuda_pool_t *p)
{
    return state_init(reg, p, clmd);
}

typedef struct {
    const char *domain;
    clmd_t *clmd;
} find_domain_ctx;

static int find_domain(void *baton, clmd_reg_t *reg, clmd_t *clmd)
{
    find_domain_ctx *ctx = baton;
    
    (void)reg;
    if (clmd_contains(clmd, ctx->domain, 0)) {
        ctx->clmd = clmd;
        return 0;
    }
    return 1;
}

clmd_t *clmd_reg_find(clmd_reg_t *reg, const char *domain, kuda_pool_t *p)
{
    find_domain_ctx ctx;

    ctx.domain = domain;
    ctx.clmd = NULL;
    
    clmd_reg_do(find_domain, &ctx, reg, p);
    if (ctx.clmd) {
        state_init(reg, p, ctx.clmd);
    }
    return ctx.clmd;
}

typedef struct {
    const clmd_t *clmd_checked;
    clmd_t *clmd;
    const char *s;
} find_overlap_ctx;

static int find_overlap(void *baton, clmd_reg_t *reg, clmd_t *clmd)
{
    find_overlap_ctx *ctx = baton;
    const char *overlap;
    
    (void)reg;
    if ((overlap = clmd_common_name(ctx->clmd_checked, clmd))) {
        ctx->clmd = clmd;
        ctx->s = overlap;
        return 0;
    }
    return 1;
}

clmd_t *clmd_reg_find_overlap(clmd_reg_t *reg, const clmd_t *clmd, const char **pdomain, kuda_pool_t *p)
{
    find_overlap_ctx ctx;
    
    ctx.clmd_checked = clmd;
    ctx.clmd = NULL;
    ctx.s = NULL;
    
    reg_do(find_overlap, &ctx, reg, p, clmd->name);
    if (pdomain && ctx.s) {
        *pdomain = ctx.s;
    }
    if (ctx.clmd) {
        state_init(reg, p, ctx.clmd);
    }
    return ctx.clmd;
}

/**************************************************************************************************/
/* manipulation */

static kuda_status_t p_clmd_add(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_reg_t *reg = baton;
    kuda_status_t rv = KUDA_SUCCESS;
    clmd_t *clmd, *mine;
    int do_check;
    
    clmd = va_arg(ap, clmd_t *);
    do_check = va_arg(ap, int);

    if (reg->domains_frozen) return KUDA_EACCES; 
    mine = clmd_clone(ptemp, clmd);
    if (do_check && KUDA_SUCCESS != (rv = check_values(reg, ptemp, clmd, CLMD_UPD_ALL))) goto leave;
    if (KUDA_SUCCESS != (rv = state_init(reg, ptemp, mine))) goto leave;
    rv = clmd_save(reg->store, p, CLMD_SG_DOMAINS, mine, 1);
leave:
    return rv;
}

static kuda_status_t add_clmd(clmd_reg_t *reg, clmd_t *clmd, kuda_pool_t *p, int do_checks)
{
    return clmd_util_pool_vdo(p_clmd_add, reg, p, clmd, do_checks, NULL);
}

kuda_status_t clmd_reg_add(clmd_reg_t *reg, clmd_t *clmd, kuda_pool_t *p)
{
    return add_clmd(reg, clmd, p, 1);
}

static kuda_status_t p_clmd_update(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_reg_t *reg = baton;
    kuda_status_t rv = KUDA_SUCCESS;
    const char *name;
    const clmd_t *clmd, *updates;
    int fields, do_checks;
    clmd_t *nmd;
    
    name = va_arg(ap, const char *);
    updates = va_arg(ap, const clmd_t *);
    fields = va_arg(ap, int);
    do_checks = va_arg(ap, int);
    
    if (NULL == (clmd = clmd_reg_get(reg, name, ptemp))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, KUDA_ENOENT, ptemp, "clmd %s", name);
        return KUDA_ENOENT;
    }
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, ptemp, "update clmd %s", name);
    
    if (do_checks && KUDA_SUCCESS != (rv = check_values(reg, ptemp, updates, fields))) {
        return rv;
    }
    
    if (reg->domains_frozen) return KUDA_EACCES; 
    nmd = clmd_copy(ptemp, clmd);
    if (CLMD_UPD_DOMAINS & fields) {
        nmd->domains = updates->domains;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update domains: %s", name);
    }
    if (CLMD_UPD_CA_URL & fields) {
        nmd->ca_url = updates->ca_url;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update ca url: %s", name);
    }
    if (CLMD_UPD_CA_PROTO & fields) {
        nmd->ca_proto = updates->ca_proto;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update ca protocol: %s", name);
    }
    if (CLMD_UPD_CA_ACCOUNT & fields) {
        nmd->ca_account = updates->ca_account;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update account: %s", name);
    }
    if (CLMD_UPD_CONTACTS & fields) {
        nmd->contacts = updates->contacts;
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update contacts: %s", name);
    }
    if (CLMD_UPD_AGREEMENT & fields) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update agreement: %s", name);
        nmd->ca_agreement = updates->ca_agreement;
    }
    if (CLMD_UPD_DRIVE_MODE & fields) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update drive-mode: %s", name);
        nmd->renew_mode = updates->renew_mode;
    }
    if (CLMD_UPD_RENEW_WINDOW & fields) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update renew-window: %s", name);
        nmd->renew_window = updates->renew_window;
    }
    if (CLMD_UPD_WARN_WINDOW & fields) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update warn-window: %s", name);
        nmd->warn_window = updates->warn_window;
    }
    if (CLMD_UPD_CA_CHALLENGES & fields) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update ca challenges: %s", name);
        nmd->ca_challenges = (updates->ca_challenges? 
                              kuda_array_copy(p, updates->ca_challenges) : NULL);
    }
    if (CLMD_UPD_PKEY_SPEC & fields) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update pkey spec: %s", name);
        nmd->pkey_spec = NULL;
        if (updates->pkey_spec) {
            nmd->pkey_spec = kuda_pmemdup(p, updates->pkey_spec, sizeof(clmd_pkey_spec_t));
        }
    }
    if (CLMD_UPD_REQUIRE_HTTPS & fields) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update require-https: %s", name);
        nmd->require_https = updates->require_https;
    }
    if (CLMD_UPD_TRANSITIVE & fields) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update transitive: %s", name);
        nmd->transitive = updates->transitive;
    }
    if (CLMD_UPD_MUST_STAPLE & fields) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update must-staple: %s", name);
        nmd->must_staple = updates->must_staple;
    }
    if (CLMD_UPD_PROTO & fields) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, ptemp, "update proto: %s", name);
        nmd->acme_tls_1_domains = updates->acme_tls_1_domains;
    }
    
    if (fields && KUDA_SUCCESS == (rv = clmd_save(reg->store, p, CLMD_SG_DOMAINS, nmd, 0))) {
        rv = state_init(reg, ptemp, nmd);
    }
    return rv;
}

static kuda_status_t update_clmd(clmd_reg_t *reg, kuda_pool_t *p, 
                              const char *name, const clmd_t *clmd, 
                              int fields, int do_checks)
{
    return clmd_util_pool_vdo(p_clmd_update, reg, p, name, clmd, fields, do_checks, NULL);
}

kuda_status_t clmd_reg_update(clmd_reg_t *reg, kuda_pool_t *p, 
                           const char *name, const clmd_t *clmd, int fields)
{
    return update_clmd(reg, p, name, clmd, fields, 1);
}

kuda_status_t clmd_reg_delete_acct(clmd_reg_t *reg, kuda_pool_t *p, const char *acct_id) 
{
    kuda_status_t rv = KUDA_SUCCESS;
    
    rv = clmd_store_remove(reg->store, CLMD_SG_ACCOUNTS, acct_id, CLMD_FN_ACCOUNT, p, 1);
    if (KUDA_SUCCESS == rv) {
        clmd_store_remove(reg->store, CLMD_SG_ACCOUNTS, acct_id, CLMD_FN_ACCT_KEY, p, 1);
    }
    return rv;
}

/**************************************************************************************************/
/* certificate related */

static kuda_status_t pubcert_load(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_reg_t *reg = baton;
    kuda_array_header_t *certs;
    clmd_pubcert_t *pubcert, **ppubcert;
    const clmd_t *clmd;
    const clmd_cert_t *cert;
    clmd_cert_state_t cert_state;
    clmd_store_group_t group;
    kuda_status_t rv;
    
    ppubcert = va_arg(ap, clmd_pubcert_t **);
    group = (clmd_store_group_t)va_arg(ap, int);
    clmd = va_arg(ap, const clmd_t *);
    
    if (clmd->cert_file) {
        rv = clmd_chain_fload(&certs, p, clmd->cert_file);
    }
    else {
        rv = clmd_pubcert_load(reg->store, group, clmd->name, &certs, p);
    }
    if (KUDA_SUCCESS != rv) goto leave;
            
    pubcert = kuda_pcalloc(p, sizeof(*pubcert));
    pubcert->certs = certs;
    cert = KUDA_ARRAY_IDX(certs, 0, const clmd_cert_t *);
    if (KUDA_SUCCESS != (rv = clmd_cert_get_alt_names(&pubcert->alt_names, cert, p))) goto leave;
    switch ((cert_state = clmd_cert_state_get(cert))) {
        case CLMD_CERT_VALID:
        case CLMD_CERT_EXPIRED:
            break;
        default:
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, KUDA_EINVAL, ptemp, 
                          "clmd %s has unexpected cert state: %d", clmd->name, cert_state);
            rv = KUDA_ENOTIMPL;
            break;
    }
leave:
    *ppubcert = (KUDA_SUCCESS == rv)? pubcert : NULL;
    return rv;
}

kuda_status_t clmd_reg_get_pubcert(const clmd_pubcert_t **ppubcert, clmd_reg_t *reg, 
                                const clmd_t *clmd, kuda_pool_t *p)
{
    kuda_status_t rv = KUDA_SUCCESS;
    const clmd_pubcert_t *pubcert;
    const char *name;

    pubcert = kuda_hash_get(reg->certs, clmd->name, (kuda_ssize_t)strlen(clmd->name));
    if (!pubcert && !reg->domains_frozen) {
        rv = clmd_util_pool_vdo(pubcert_load, reg, reg->p, &pubcert, CLMD_SG_DOMAINS, clmd, NULL);
        if (KUDA_STATUS_IS_ENOENT(rv)) {
            /* We cache it missing with an empty record */
            pubcert = kuda_pcalloc(reg->p, sizeof(*pubcert));
        }
        else if (KUDA_SUCCESS != rv) goto leave;
        name = (p != reg->p)? kuda_pstrdup(reg->p, clmd->name) : clmd->name;
        kuda_hash_set(reg->certs, name, (kuda_ssize_t)strlen(name), pubcert);
    }
leave:
    if (KUDA_SUCCESS == rv && (!pubcert || !pubcert->certs)) {
        rv = KUDA_ENOENT;
    }
    *ppubcert = (KUDA_SUCCESS == rv)? pubcert : NULL;
    return rv;
}

kuda_status_t clmd_reg_get_cred_files(const char **pkeyfile, const char **pcertfile,
                                   clmd_reg_t *reg, clmd_store_group_t group, 
                                   const clmd_t *clmd, kuda_pool_t *p)
{
    kuda_status_t rv;
    
    if (clmd->cert_file) {
        /* With fixed files configured, we use those without further checking them ourself */
        *pcertfile = clmd->cert_file;
        *pkeyfile = clmd->pkey_file;
        return KUDA_SUCCESS;
    }
    rv = clmd_store_get_fname(pkeyfile, reg->store, group, clmd->name, CLMD_FN_PRIVKEY, p);
    if (KUDA_SUCCESS != rv) return rv;
    if (!clmd_file_exists(*pkeyfile, p)) return KUDA_ENOENT;
    rv = clmd_store_get_fname(pcertfile, reg->store, group, clmd->name, CLMD_FN_PUBCERT, p);
    if (KUDA_SUCCESS != rv) return rv;
    if (!clmd_file_exists(*pcertfile, p)) return KUDA_ENOENT;
    return KUDA_SUCCESS;
}

int clmd_reg_should_renew(clmd_reg_t *reg, const clmd_t *clmd, kuda_pool_t *p) 
{
    const clmd_pubcert_t *pub;
    const clmd_cert_t *cert;
    clmd_timeperiod_t certlife, renewal;
    kuda_status_t rv;
    
    if (clmd->state == CLMD_S_INCOMPLETE) return 1;
    rv = clmd_reg_get_pubcert(&pub, reg, clmd, p);
    if (KUDA_STATUS_IS_ENOENT(rv)) return 1;
    if (KUDA_SUCCESS == rv) {
        cert = KUDA_ARRAY_IDX(pub->certs, 0, const clmd_cert_t*);
        certlife.start = clmd_cert_get_not_before(cert);
        certlife.end = clmd_cert_get_not_after(cert);

        renewal = clmd_timeperiod_slice_before_end(&certlife, clmd->renew_window);
        if (clmd_log_is_level(p, CLMD_LOG_TRACE1)) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, p, 
                          "clmd[%s]: cert-life[%s] renewal[%s]", clmd->name, 
                          clmd_timeperiod_print(p, &certlife),
                          clmd_timeperiod_print(p, &renewal));
        }
        return clmd_timeperiod_has_started(&renewal, kuda_time_now());
    }
    return 0;
}

int clmd_reg_should_warn(clmd_reg_t *reg, const clmd_t *clmd, kuda_pool_t *p)
{
    const clmd_pubcert_t *pub;
    const clmd_cert_t *cert;
    clmd_timeperiod_t certlife, warn;
    kuda_status_t rv;
    
    if (clmd->state == CLMD_S_INCOMPLETE) return 0;
    rv = clmd_reg_get_pubcert(&pub, reg, clmd, p);
    if (KUDA_STATUS_IS_ENOENT(rv)) return 0;
    if (KUDA_SUCCESS == rv) {
        cert = KUDA_ARRAY_IDX(pub->certs, 0, const clmd_cert_t*);
        certlife.start = clmd_cert_get_not_before(cert);
        certlife.end = clmd_cert_get_not_after(cert);

        warn = clmd_timeperiod_slice_before_end(&certlife, clmd->warn_window);
        if (clmd_log_is_level(p, CLMD_LOG_TRACE1)) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE1, 0, p, 
                          "clmd[%s]: cert-life[%s] warn[%s]", clmd->name, 
                          clmd_timeperiod_print(p, &certlife),
                          clmd_timeperiod_print(p, &warn));
        }
        return clmd_timeperiod_has_started(&warn, kuda_time_now());
    }
    return 0;
}

/**************************************************************************************************/
/* synching */

typedef struct {
    kuda_pool_t *p;
    kuda_array_header_t *store_clmds;
} sync_ctx;

static int do_add_clmd(void *baton, clmd_store_t *store, clmd_t *clmd, kuda_pool_t *ptemp)
{
    sync_ctx *ctx = baton;

    (void)store;
    (void)ptemp;
    KUDA_ARRAY_PUSH(ctx->store_clmds, const clmd_t*) = clmd_clone(ctx->p, clmd);
    return 1;
}

static kuda_status_t read_store_clmds(clmd_reg_t *reg, sync_ctx *ctx)
{
    int rv;
    
    kuda_array_clear(ctx->store_clmds);
    rv = clmd_store_clmd_iter(do_add_clmd, ctx, reg->store, ctx->p, CLMD_SG_DOMAINS, "*");
    if (KUDA_STATUS_IS_ENOENT(rv) || KUDA_STATUS_IS_EINVAL(rv)) {
        rv = KUDA_SUCCESS;
    }
    return rv;
}

kuda_status_t clmd_reg_set_props(clmd_reg_t *reg, kuda_pool_t *p, int can_http, int can_https)
{
    if (reg->can_http != can_http || reg->can_https != can_https) {
        clmd_json_t *json;
        
        if (reg->domains_frozen) return KUDA_EACCES; 
        reg->can_http = can_http;
        reg->can_https = can_https;
        
        json = clmd_json_create(p);
        clmd_json_setb(can_http, json, CLMD_KEY_PROTO, CLMD_KEY_HTTP, NULL);
        clmd_json_setb(can_https, json, CLMD_KEY_PROTO, CLMD_KEY_HTTPS, NULL);
        
        return clmd_store_save(reg->store, p, CLMD_SG_NONE, NULL, CLMD_FN_WWHY_JSON, CLMD_SV_JSON, json, 0);
    }
    return KUDA_SUCCESS;
}

static kuda_status_t update_clmd(clmd_reg_t *reg, kuda_pool_t *p, 
                              const char *name, const clmd_t *clmd, 
                              int fields, int do_checks);
 
/**
 * Procedure:
 * 1. Collect all defined "managed domains" (CLMD). It does not matter where a CLMD is defined. 
 *    All CLMDs need to be unique and have no overlaps in their domain names. 
 *    Fail the config otherwise. Also, if a vhost matches an CLMD, it
 *    needs to *only* have ServerAliases from that CLMD. There can be no more than one
 *    matching CLMD for a vhost. But an CLMD can apply to several vhosts.
 * 2. Synchronize with the persistent store. Iterate over all configured CLMDs and 
 *   a. create them in the store if they do not already exist, neither under the
 *      name or with a common domain.
 *   b. compare domain lists from store and config, if
 *      - store has dns name in other CLMD than from config, remove dns name from store def,
 *        issue WARNING.
 *      - store misses dns name from config, add dns name and update store
 *   c. compare CLMD acme url/protocol, update if changed
 */
kuda_status_t clmd_reg_sync(clmd_reg_t *reg, kuda_pool_t *p, kuda_pool_t *ptemp, 
                         kuda_array_header_t *master_clmds) 
{
    sync_ctx ctx;
    kuda_status_t rv;

    ctx.p = ptemp;
    ctx.store_clmds = kuda_array_make(ptemp, 100, sizeof(clmd_t *));
    rv = read_store_clmds(reg, &ctx);
    
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, 
                  "sync: found %d mds in store", ctx.store_clmds->nelts);
    if (reg->domains_frozen) return KUDA_EACCES; 
    if (KUDA_SUCCESS == rv) {
        int i, fields;
        clmd_t *clmd, *config_clmd, *smd, *omd;
        const char *common;
        
        for (i = 0; i < master_clmds->nelts; ++i) {
            clmd = KUDA_ARRAY_IDX(master_clmds, i, clmd_t *);
            
            /* find the store clmd that is closest match for the configured clmd */
            smd = clmd_find_closest_match(ctx.store_clmds, clmd);
            if (smd) {
                fields = 0;
                
                /* Did the name change? This happens when the order of names in configuration
                 * changes or when the first name is removed. Use the name from the store, but
                 * remember the original one. We try to align this later on. */
                if (strcmp(clmd->name, smd->name)) {
                    clmd->configured_name = clmd->name;
                    clmd->name = kuda_pstrdup(p, smd->name);
                }
                
                /* Make the stored domain list *exactly* the same, even if
                 * someone only changed upper/lowercase, we'd like to persist that. */
                if (!clmd_equal_domains(clmd, smd, 1)) {
                    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, 
                                 "%s: domains changed", smd->name);
                    smd->domains = clmd_array_str_clone(ptemp, clmd->domains);
                    fields |= CLMD_UPD_DOMAINS;
                }
                
                /* Look for other store mds which have domains now being part of smd */
                while (KUDA_SUCCESS == rv && (omd = clmd_get_by_dns_overlap(ctx.store_clmds, clmd))) {
                    /* find the name now duplicate */
                    common = clmd_common_name(clmd, omd);
                    assert(common);
                    
                    /* Is this clmd still configured or has it been abandoned in the config? */
                    config_clmd = clmd_get_by_name(master_clmds, omd->name);
                    if (config_clmd && clmd_contains(config_clmd, common, 0)) {
                        /* domain used in two configured mds, not allowed */
                        rv = KUDA_EINVAL;
                        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, p, 
                                      "domain %s used in clmd %s and %s", 
                                      common, clmd->name, omd->name);
                    }
                    else {
                        /* remove it from the other clmd and update store, or, if it
                         * is now empty, move it into the archive */
                        omd->domains = clmd_array_str_remove(ptemp, omd->domains, common, 0);
                        if (kuda_is_empty_array(omd->domains)) {
                            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, rv, p, 
                                          "All domains of the CLMD %s have moved elsewhere, "
                                          " moving it to the archive. ", omd->name);
                            clmd_reg_remove(reg, ptemp, omd->name, 1); /* best effort */
                        }
                        else {
                            rv = update_clmd(reg, ptemp, omd->name, omd, CLMD_UPD_DOMAINS, 0);
                        }
                    }
                }

                /* If no CA url/proto is configured for the CLMD, take the default */
                if (!clmd->ca_url) {
                    clmd->ca_url = CLMD_ACME_DEF_URL;
                    clmd->ca_proto = CLMD_PROTO_ACME; 
                }
                
                if (CLMD_SVAL_UPDATE(clmd, smd, ca_url)) {
                    smd->ca_url = clmd->ca_url;
                    fields |= CLMD_UPD_CA_URL;
                }
                if (CLMD_SVAL_UPDATE(clmd, smd, ca_proto)) {
                    smd->ca_proto = clmd->ca_proto;
                    fields |= CLMD_UPD_CA_PROTO;
                }
                if (CLMD_SVAL_UPDATE(clmd, smd, ca_agreement)) {
                    smd->ca_agreement = clmd->ca_agreement;
                    fields |= CLMD_UPD_AGREEMENT;
                }
                if (CLMD_VAL_UPDATE(clmd, smd, transitive)) {
                    smd->transitive = clmd->transitive;
                    fields |= CLMD_UPD_TRANSITIVE;
                }
                if (CLMD_VAL_UPDATE(clmd, smd, renew_mode)) {
                    smd->renew_mode = clmd->renew_mode;
                    fields |= CLMD_UPD_DRIVE_MODE;
                }
                if (!kuda_is_empty_array(clmd->contacts) 
                    && !clmd_array_str_eq(clmd->contacts, smd->contacts, 0)) {
                    smd->contacts = clmd->contacts;
                    fields |= CLMD_UPD_CONTACTS;
                }
                if (!clmd_timeslice_eq(clmd->renew_window, smd->renew_window)) {
                    smd->renew_window = clmd->renew_window;
                    fields |= CLMD_UPD_RENEW_WINDOW;
                }
                if (!clmd_timeslice_eq(clmd->warn_window, smd->warn_window)) {
                    smd->warn_window = clmd->warn_window;
                    fields |= CLMD_UPD_WARN_WINDOW;
                }
                if (clmd->ca_challenges) {
                    clmd->ca_challenges = clmd_array_str_compact(p, clmd->ca_challenges, 0);
                    if (!smd->ca_challenges 
                        || !clmd_array_str_eq(clmd->ca_challenges, smd->ca_challenges, 0)) {
                        smd->ca_challenges = kuda_array_copy(ptemp, clmd->ca_challenges);
                        fields |= CLMD_UPD_CA_CHALLENGES;
                    }
                }
                else if (smd->ca_challenges) {
                    smd->ca_challenges = NULL;
                    fields |= CLMD_UPD_CA_CHALLENGES;
                }
                if (!clmd_pkey_spec_eq(clmd->pkey_spec, smd->pkey_spec)) {
                    fields |= CLMD_UPD_PKEY_SPEC;
                    smd->pkey_spec = NULL;
                    if (clmd->pkey_spec) {
                        smd->pkey_spec = kuda_pmemdup(p, clmd->pkey_spec, sizeof(clmd_pkey_spec_t));
                    }
                }
                if (CLMD_VAL_UPDATE(clmd, smd, require_https)) {
                    smd->require_https = clmd->require_https;
                    fields |= CLMD_UPD_REQUIRE_HTTPS;
                }
                if (CLMD_VAL_UPDATE(clmd, smd, must_staple)) {
                    smd->must_staple = clmd->must_staple;
                    fields |= CLMD_UPD_MUST_STAPLE;
                }
                if (!clmd_array_str_eq(clmd->acme_tls_1_domains, smd->acme_tls_1_domains, 0)) {
                    smd->acme_tls_1_domains = clmd->acme_tls_1_domains;
                    fields |= CLMD_UPD_PROTO;
                }
                
                if (fields) {
                    rv = update_clmd(reg, ptemp, smd->name, smd, fields, 0);
                    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, "clmd %s updated", smd->name);
                }
            }
            else {
                /* new managed domain */
                /* If no CA url/proto is configured for the CLMD, take the default */
                if (!clmd->ca_url) {
                    clmd->ca_url = CLMD_ACME_DEF_URL;
                    clmd->ca_proto = CLMD_PROTO_ACME; 
                }
                rv = add_clmd(reg, clmd, ptemp, 0);
                clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, p, "new clmd %s added", clmd->name);
            }
        }
    }
    else {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_ERR, rv, p, "loading mds");
    }
    
    return rv;
}

kuda_status_t clmd_reg_remove(clmd_reg_t *reg, kuda_pool_t *p, const char *name, int archive)
{
    if (reg->domains_frozen) return KUDA_EACCES; 
    return clmd_store_move(reg->store, p, CLMD_SG_DOMAINS, CLMD_SG_ARCHIVE, name, archive);
}

typedef struct {
    clmd_reg_t *reg;
    kuda_pool_t *p;
    kuda_array_header_t *mds;
} cleanup_challenge_ctx;
 
static kuda_status_t cleanup_challenge_inspector(void *baton, const char *dir, const char *name, 
                                                clmd_store_vtype_t vtype, void *value, 
                                                kuda_pool_t *ptemp)
{
    cleanup_challenge_ctx *ctx = baton;
    const clmd_t *clmd;
    int i, used;
    kuda_status_t rv;
    
    (void)value;
    (void)vtype;
    (void)dir;
    for (used = 0, i = 0; i < ctx->mds->nelts && !used; ++i) {
        clmd = KUDA_ARRAY_IDX(ctx->mds, i, const clmd_t *);
        used = !strcmp(name, clmd->name);
    }
    if (!used) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, ptemp, 
                      "challenges/%s: not in use, purging", name);
        rv = clmd_store_purge(ctx->reg->store, ctx->p, CLMD_SG_CHALLENGES, name);
        if (KUDA_SUCCESS != rv) {
            clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, rv, ptemp, 
                          "challenges/%s: unable to purge", name);
        }
    }
    return KUDA_SUCCESS;
}

kuda_status_t clmd_reg_cleanup_challenges(clmd_reg_t *reg, kuda_pool_t *p, kuda_pool_t *ptemp, 
                                       kuda_array_header_t *mds)
{
    kuda_status_t rv;
    cleanup_challenge_ctx ctx;

    (void)p;
    ctx.reg = reg;
    ctx.p = ptemp;
    ctx.mds = mds;
    rv = clmd_store_iter_names(cleanup_challenge_inspector, &ctx, reg->store, ptemp, 
                             CLMD_SG_CHALLENGES, "*");
    return rv;
}


/**************************************************************************************************/
/* driving */

static kuda_status_t run_init(void *baton, kuda_pool_t *p, ...)
{
    va_list ap;
    clmd_reg_t *reg = baton;
    const clmd_t *clmd;
    clmd_proto_driver_t *driver, **pdriver;
    clmd_result_t *result;
    kuda_table_t *env;
    
    (void)p;
    va_start(ap, p);
    pdriver = va_arg(ap, clmd_proto_driver_t **);
    clmd = va_arg(ap, const clmd_t *);
    env = va_arg(ap, kuda_table_t *);
    result = va_arg(ap, clmd_result_t *); 
    va_end(ap);
    
    *pdriver = driver = kuda_pcalloc(p, sizeof(*driver));

    driver->p = p;
    driver->env = env? kuda_table_copy(p, env) : kuda_table_make(p, 10);
    driver->reg = reg;
    driver->store = clmd_reg_store_get(reg);
    driver->proxy_url = reg->proxy_url;
    driver->clmd = clmd;
    driver->can_http = reg->can_http;
    driver->can_https = reg->can_https;

    if (!clmd->ca_proto) {
        clmd_result_printf(result, KUDA_EGENERAL, "CA protocol is not defined"); 
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, 0, p, "clmd[%s]: %s", clmd->name, result->detail);
        goto leave;
    }
    
    driver->proto = kuda_hash_get(reg->protos, clmd->ca_proto, (kuda_ssize_t)strlen(clmd->ca_proto));
    if (!driver->proto) {
        clmd_result_printf(result, KUDA_EGENERAL, "Unknown CA protocol '%s'", clmd->ca_proto); 
        goto leave;
    }
    
    result->status = driver->proto->init(driver, result);

leave:
    if (KUDA_SUCCESS != result->status) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_WARNING, result->status, p, "clmd[%s]: %s", clmd->name, 
                      result->detail? result->detail : "<see error log for details>");
    }
    else {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, p, "%s: init done", clmd->name);
    }
    return result->status;
}

static kuda_status_t run_test_init(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    const clmd_t *clmd;
    kuda_table_t *env;
    clmd_result_t *result;
    clmd_proto_driver_t *driver;
    
    (void)p;
    clmd = va_arg(ap, const clmd_t *);
    env = va_arg(ap, kuda_table_t *);
    result = va_arg(ap, clmd_result_t *); 

    return run_init(baton, ptemp, &driver, clmd, env, result, NULL);
}

kuda_status_t clmd_reg_test_init(clmd_reg_t *reg, const clmd_t *clmd, struct kuda_table_t *env, 
                              clmd_result_t *result, kuda_pool_t *p)
{
    return clmd_util_pool_vdo(run_test_init, reg, p, clmd, env, result, NULL);
}

static kuda_status_t run_renew(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    const clmd_t *clmd;
    int reset;
    clmd_proto_driver_t *driver;
    kuda_table_t *env;
    kuda_status_t rv;
    clmd_result_t *result;
    
    (void)p;
    clmd = va_arg(ap, const clmd_t *);
    env = va_arg(ap, kuda_table_t *);
    reset = va_arg(ap, int); 
    result = va_arg(ap, clmd_result_t *); 

    rv = run_init(baton, ptemp, &driver, clmd, env, result, NULL);
    if (KUDA_SUCCESS == rv) { 
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, 0, ptemp, "%s: run staging", clmd->name);
        driver->reset = reset;
        rv = driver->proto->renew(driver, result);
    }
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, ptemp, "%s: staging done", clmd->name);
    return rv;
}

kuda_status_t clmd_reg_renew(clmd_reg_t *reg, const clmd_t *clmd, kuda_table_t *env, 
                          int reset, clmd_result_t *result, kuda_pool_t *p)
{
    return clmd_util_pool_vdo(run_renew, reg, p, clmd, env, reset, result, NULL);
}

static kuda_status_t run_load_staging(void *baton, kuda_pool_t *p, kuda_pool_t *ptemp, va_list ap)
{
    clmd_reg_t *reg = baton;
    const clmd_t *clmd;
    clmd_proto_driver_t *driver;
    clmd_result_t *result;
    kuda_table_t *env;
    clmd_job_t *job;
    kuda_status_t rv;
    
    /* For the CLMD,  check if something is in the STAGING area. If none is there, 
     * return that status. Otherwise ask the protocol driver to preload it into
     * a new, temporary area. 
     * If that succeeds, we move the TEMP area over the DOMAINS (causing the 
     * existing one go to ARCHIVE).
     * Finally, we clean up the data from CHALLENGES and STAGING.
     */
    clmd = va_arg(ap, const clmd_t*);
    env =  va_arg(ap, kuda_table_t*);
    result =  va_arg(ap, clmd_result_t*);
    
    if (KUDA_STATUS_IS_ENOENT(rv = clmd_load(reg->store, CLMD_SG_STAGING, clmd->name, NULL, ptemp))) {
        clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_TRACE2, rv, ptemp, "%s: nothing staged", clmd->name);
        goto out;
    }
    
    rv = run_init(baton, ptemp, &driver, clmd, env, result, NULL);
    if (KUDA_SUCCESS != rv) goto out;
    
    kuda_hash_set(reg->certs, clmd->name, (kuda_ssize_t)strlen(clmd->name), NULL);
    clmd_result_activity_setn(result, "preloading staged to tmp");
    rv = driver->proto->preload(driver, CLMD_SG_TMP, result);
    if (KUDA_SUCCESS != rv) goto out;

    /* If we had a job saved in STAGING, copy it over too */
    job = clmd_job_make(ptemp, clmd->name);
    if (KUDA_SUCCESS == clmd_job_load(job, reg, CLMD_SG_STAGING, ptemp)) {
        clmd_job_save(job, reg, CLMD_SG_TMP, NULL, ptemp);
    }
    
    /* swap */
    clmd_result_activity_setn(result, "moving tmp to become new domains");
    rv = clmd_store_move(reg->store, p, CLMD_SG_TMP, CLMD_SG_DOMAINS, clmd->name, 1);
    if (KUDA_SUCCESS != rv) {
        clmd_result_set(result, rv, NULL);
        goto out;
    }
    
    clmd_store_purge(reg->store, p, CLMD_SG_STAGING, clmd->name);
    clmd_store_purge(reg->store, p, CLMD_SG_CHALLENGES, clmd->name);
    clmd_result_set(result, KUDA_SUCCESS, "new certificate successfully saved in domains");

out:
    clmd_log_perror(CLMD_LOG_MARK, CLMD_LOG_DEBUG, rv, ptemp, "%s: load done", clmd->name);
    return rv;
}

kuda_status_t clmd_reg_load_staging(clmd_reg_t *reg, const clmd_t *clmd, kuda_table_t *env, 
                                 clmd_result_t *result, kuda_pool_t *p)
{
    if (reg->domains_frozen) return KUDA_EACCES;
    return clmd_util_pool_vdo(run_load_staging, reg, p, clmd, env, result, NULL);
}

kuda_status_t clmd_reg_freeze_domains(clmd_reg_t *reg, kuda_array_header_t *mds)
{
    kuda_status_t rv = KUDA_SUCCESS;
    clmd_t *clmd;
    const clmd_pubcert_t *pubcert;
    int i;
    
    assert(!reg->domains_frozen);
    /* prefill the certs cache for all mds */
    for (i = 0; i < mds->nelts; ++i) {
        clmd = KUDA_ARRAY_IDX(mds, i, clmd_t*);
        rv = clmd_reg_get_pubcert(&pubcert, reg, clmd, reg->p);
        if (KUDA_SUCCESS != rv && !KUDA_STATUS_IS_ENOENT(rv)) goto leave;
    }
    reg->domains_frozen = 1;
leave:
    return rv;
}

void clmd_reg_set_renew_window_default(clmd_reg_t *reg, const clmd_timeslice_t *renew_window)
{
    reg->renew_window = renew_window;
}

void clmd_reg_set_warn_window_default(clmd_reg_t *reg, const clmd_timeslice_t *warn_window)
{
    reg->warn_window = warn_window;
}
