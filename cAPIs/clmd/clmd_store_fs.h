/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_store_fs_h
#define capi_clmd_clmd_store_fs_h

struct clmd_store_t;

/** 
 * Default file permissions set by the store, user only read/write(/exec),
 * if so supported by the Kuda. 
 */
#define CLMD_FPROT_F_UONLY      (KUDA_FPROT_UREAD|KUDA_FPROT_UWRITE)
#define CLMD_FPROT_D_UONLY      (CLMD_FPROT_F_UONLY|KUDA_FPROT_UEXECUTE)

/**
 * User has all permission, group can read, other none
 */
#define CLMD_FPROT_F_UALL_GREAD (CLMD_FPROT_F_UONLY|KUDA_FPROT_GREAD)
#define CLMD_FPROT_D_UALL_GREAD (CLMD_FPROT_D_UONLY|KUDA_FPROT_GREAD|KUDA_FPROT_GEXECUTE)

/**
 * User has all permission, group and others can read
 */
#define CLMD_FPROT_F_UALL_WREAD (CLMD_FPROT_F_UALL_GREAD|KUDA_FPROT_WREAD)
#define CLMD_FPROT_D_UALL_WREAD (CLMD_FPROT_D_UALL_GREAD|KUDA_FPROT_WREAD|KUDA_FPROT_WEXECUTE)

kuda_status_t clmd_store_fs_init(struct clmd_store_t **pstore, kuda_pool_t *p, 
                              const char *path);


kuda_status_t clmd_store_fs_default_perms_set(struct clmd_store_t *store, 
                                           kuda_fileperms_t file_perms,
                                           kuda_fileperms_t dir_perms);
kuda_status_t clmd_store_fs_group_perms_set(struct clmd_store_t *store, 
                                         clmd_store_group_t group, 
                                         kuda_fileperms_t file_perms,
                                         kuda_fileperms_t dir_perms);

typedef enum {
    CLMD_S_FS_EV_CREATED,
    CLMD_S_FS_EV_MOVED,
} clmd_store_fs_ev_t; 

typedef kuda_status_t clmd_store_fs_cb(void *baton, struct clmd_store_t *store,
                                    clmd_store_fs_ev_t ev, unsigned int group, 
                                    const char *fname, kuda_filetype_e ftype,  
                                    kuda_pool_t *p);
                                    
kuda_status_t clmd_store_fs_set_event_cb(struct clmd_store_t *store, clmd_store_fs_cb *cb, void *baton);

#endif /* capi_clmd_clmd_store_fs_h */
