/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef capi_clmd_clmd_jws_h
#define capi_clmd_clmd_jws_h

struct kuda_table_t;
struct clmd_json_t;
struct clmd_pkey_t;

kuda_status_t clmd_jws_sign(clmd_json_t **pmsg, kuda_pool_t *p,
                         const char *payload, size_t len, struct kuda_table_t *protected, 
                         struct clmd_pkey_t *pkey, const char *key_id);

kuda_status_t clmd_jws_pkey_thumb(const char **pthumb, kuda_pool_t *p, struct clmd_pkey_t *pkey);

#endif /* clmd_jws_h */
