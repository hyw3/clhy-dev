/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <kuda_lib.h>
#include <kuda_strings.h>
#include <kuda_buckets.h>

#include "clmd_log.h"

#define LOG_BUFFER_LEN  1024

static const char *level_names[] = {
    "emergency",
    "alert",
    "crit",
    "err",
    "warning",
    "notice",
    "info",
    "debug",
    "trace1",
    "trace2",
    "trace3",
    "trace4",
    "trace5",
    "trace6",
    "trace7",
    "trace8",
};

const char *clmd_log_level_name(clmd_log_level_t level)
{
    return level_names[level];
}

static clmd_log_print_cb *log_printv;
static clmd_log_level_cb *log_level;
static void *log_baton;

void clmd_log_set(clmd_log_level_cb *level_cb, clmd_log_print_cb *print_cb, void *baton)
{
    log_printv = print_cb;
    log_level = level_cb;
    log_baton = baton;
}

int clmd_log_is_level(kuda_pool_t *p, clmd_log_level_t level)
{
    if (!log_level) {
        return 0;
    }
    return log_level(log_baton, p, level);
}

void clmd_log_perror(const char *file, int line, clmd_log_level_t level, 
                   kuda_status_t rv, kuda_pool_t *p, const char *fmt, ...)
{
    va_list ap;

    va_start(ap, fmt);
    if (log_printv) {
        log_printv(file, line, level, rv, log_baton, p, fmt, ap);
    }
    va_end(ap);
}
