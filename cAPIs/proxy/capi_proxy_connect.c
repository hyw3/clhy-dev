/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* CONNECT method for cLHy proxy */

#include "capi_proxy.h"
#include "kuda_poll.h"

#define CONN_BLKSZ CLHY_IOBUFSIZE

cAPI CLHY_CAPI_DECLARE_DATA proxy_connect_capi;

/*
 * This handles Netscape CONNECT method secure proxy requests.
 * A connection is opened to the specified host and data is
 * passed through between the WWW site and the browser.
 *
 * This code is based on the INTERNET-DRAFT document
 * "Tunneling SSL Through a WWW Proxy" currently at
 * http://www.mcom.com/newsref/std/tunneling_ssl.html.
 *
 * If proxyhost and proxyport are set, we send a CONNECT to
 * the specified proxy..
 *
 * FIXME: this doesn't log the number of bytes sent, but
 *        that may be okay, since the data is supposed to
 *        be transparent. In fact, this doesn't log at all
 *        yet. 8^)
 * FIXME: doesn't check any headers initally sent from the
 *        client.
 * FIXME: should allow authentication, but hopefully the
 *        generic proxy authentication is good enough.
 * FIXME: no check for r->assbackwards, whatever that is.
 */

typedef struct {
    kuda_array_header_t *allowed_connect_ports;
} connect_conf;

typedef struct {
    int first;
    int last;
} port_range;

static void *create_config(kuda_pool_t *p, server_rec *s)
{
    connect_conf *c = kuda_pcalloc(p, sizeof(connect_conf));
    c->allowed_connect_ports = kuda_array_make(p, 10, sizeof(port_range));
    return c;
}

static void *merge_config(kuda_pool_t *p, void *basev, void *overridesv)
{
    connect_conf *c = kuda_pcalloc(p, sizeof(connect_conf));
    connect_conf *base = (connect_conf *) basev;
    connect_conf *overrides = (connect_conf *) overridesv;

    c->allowed_connect_ports = kuda_array_append(p,
                                                base->allowed_connect_ports,
                                                overrides->allowed_connect_ports);

    return c;
}


/*
 * Set the ports CONNECT can use
 */
static const char *
    set_allowed_ports(cmd_parms *parms, void *dummy, const char *arg)
{
    server_rec *s = parms->server;
    int first, last;
    connect_conf *conf =
        clhy_get_capi_config(s->capi_config, &proxy_connect_capi);
    port_range *New;
    char *endptr;
    const char *p = arg;

    if (!kuda_isdigit(arg[0]))
        return "AllowCONNECT: port numbers must be numeric";

    first = strtol(p, &endptr, 10);
    if (*endptr == '-') {
        p = endptr + 1;
        last = strtol(p, &endptr, 10);
    }
    else {
        last = first;
    }

    if (endptr == p || *endptr != '\0')  {
        return kuda_psprintf(parms->temp_pool,
                            "Cannot parse '%s' as port number", p);
    }

    New = kuda_array_push(conf->allowed_connect_ports);
    New->first = first;
    New->last  = last;
    return NULL;
}


static int allowed_port(connect_conf *conf, int port)
{
    int i;
    port_range *list = (port_range *) conf->allowed_connect_ports->elts;

    if (kuda_is_empty_array(conf->allowed_connect_ports)) {
        return port == KUDA_URI_HTTPS_DEFAULT_PORT
               || port == KUDA_URI_SNEWS_DEFAULT_PORT;
    }

    for (i = 0; i < conf->allowed_connect_ports->nelts; i++) {
        if (port >= list[i].first && port <= list[i].last)
            return 1;
    }
    return 0;
}

/* canonicalise CONNECT URLs. */
static int proxy_connect_canon(request_rec *r, char *url)
{

    if (r->method_number != M_CONNECT) {
    return DECLINED;
    }
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, r, "canonicalising URL %s", url);

    return OK;
}

/* CONNECT handler */
static int proxy_connect_handler(request_rec *r, proxy_worker *worker,
                                 proxy_server_conf *conf,
                                 char *url, const char *proxyname,
                                 kuda_port_t proxyport)
{
    connect_conf *c_conf =
        clhy_get_capi_config(r->server->capi_config, &proxy_connect_capi);

    kuda_pool_t *p = r->pool;
    kuda_socket_t *sock;
    conn_rec *c = r->connection;
    conn_rec *backconn;
    int done = 0;

    kuda_bucket_brigade *bb_front;
    kuda_bucket_brigade *bb_back;
    kuda_status_t rv;
    kuda_size_t nbytes;
    char buffer[HUGE_STRING_LEN];
    kuda_socket_t *client_socket = clhy_get_conn_socket(c);
    int failed, rc;
    kuda_pollset_t *pollset;
    kuda_pollfd_t pollfd;
    const kuda_pollfd_t *signalled;
    kuda_int32_t pollcnt, pi;
    kuda_int16_t pollevent;
    kuda_sockaddr_t *nexthop;

    kuda_uri_t uri;
    const char *connectname;
    kuda_port_t connectport = 0;

    /* is this for us? */
    if (r->method_number != M_CONNECT) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, r, "declining URL %s", url);
        return DECLINED;
    }
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, r, "serving URL %s", url);


    /*
     * Step One: Determine Who To Connect To
     *
     * Break up the URL to determine the host to connect to
     */

    /* we break the URL into host, port, uri */
    if (KUDA_SUCCESS != kuda_uri_parse_hostinfo(p, url, &uri)) {
        return clhy_proxyerror(r, HTTP_BAD_REQUEST,
                             kuda_pstrcat(p, "URI cannot be parsed: ", url,
                                         NULL));
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01019)
                  "connecting %s to %s:%d", url, uri.hostname, uri.port);

    /* Determine host/port of next hop; from request URI or of a proxy. */
    connectname = proxyname ? proxyname : uri.hostname;
    connectport = proxyname ? proxyport : uri.port;

    /* Do a DNS lookup for the next hop */
    rv = kuda_sockaddr_info_get(&nexthop, connectname, KUDA_UNSPEC, 
                               connectport, 0, p);
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(02327)
                      "failed to resolve hostname '%s'", connectname);
        return clhy_proxyerror(r, HTTP_BAD_GATEWAY,
                             kuda_pstrcat(p, "DNS lookup failure for: ",
                                         connectname, NULL));
    }

    /* Check ProxyBlock directive on the hostname/address.  */
    if (clhy_proxy_checkproxyblock2(r, conf, uri.hostname, 
                                 proxyname ? NULL : nexthop) != OK) {
        return clhy_proxyerror(r, HTTP_FORBIDDEN,
                             "Connect to remote machine blocked");
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, r,
                  "connecting to remote proxy %s on port %d",
                  connectname, connectport);

    /* Check if it is an allowed port */
    if (!allowed_port(c_conf, uri.port)) {
        return clhy_proxyerror(r, HTTP_FORBIDDEN,
                             "Connect to remote machine blocked");
    }

    /*
     * Step Two: Make the Connection
     *
     * We have determined who to connect to. Now make the connection.
     */

    /*
     * At this point we have a list of one or more IP addresses of
     * the machine to connect to. If configured, reorder this
     * list so that the "best candidate" is first try. "best
     * candidate" could mean the least loaded server, the fastest
     * responding server, whatever.
     *
     * For now we do nothing, ie we get DNS round robin.
     * XXX FIXME
     */
    failed = clhy_proxy_connect_to_backend(&sock, "CONNECT", nexthop,
                                         connectname, conf, r);

    /* handle a permanent error from the above loop */
    if (failed) {
        if (proxyname) {
            return DECLINED;
        }
        else {
            return HTTP_SERVICE_UNAVAILABLE;
        }
    }

    /* setup polling for connection */
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r, "setting up poll()");

    if ((rv = kuda_pollset_create(&pollset, 2, r->pool, 0)) != KUDA_SUCCESS) {
        kuda_socket_close(sock);
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01020)
                      "error kuda_pollset_create()");
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    /* Add client side to the poll */
    pollfd.p = r->pool;
    pollfd.desc_type = KUDA_POLL_SOCKET;
    pollfd.reqevents = KUDA_POLLIN | KUDA_POLLHUP;
    pollfd.desc.s = client_socket;
    pollfd.client_data = NULL;
    kuda_pollset_add(pollset, &pollfd);

    /* Add the server side to the poll */
    pollfd.desc.s = sock;
    kuda_pollset_add(pollset, &pollfd);

    /*
     * Step Three: Send the Request
     *
     * Send the HTTP/1.1 CONNECT request to the remote server
     */

    backconn = clhy_run_create_connection(c->pool, r->server, sock,
                                        c->id, c->sbh, c->bucket_alloc);
    if (!backconn) {
        /* peer reset */
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(01021)
                      "an error occurred creating a new connection "
                      "to %pI (%s)", nexthop, connectname);
        kuda_socket_close(sock);
        return HTTP_INTERNAL_SERVER_ERROR;
    }
    clhy_proxy_ssl_disable(backconn);
    rc = clhy_run_pre_connection(backconn, sock);
    if (rc != OK && rc != DONE) {
        backconn->aborted = 1;
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01022)
                      "pre_connection setup failed (%d)", rc);
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE3, 0, r,
                  "connection complete to %pI (%s)",
                  nexthop, connectname);
    kuda_table_setn(r->notes, "proxy-source-port", kuda_psprintf(r->pool, "%hu",
                   backconn->local_addr->port));


    bb_front = kuda_brigade_create(p, c->bucket_alloc);
    bb_back = kuda_brigade_create(p, backconn->bucket_alloc);

    /* If we are connecting through a remote proxy, we need to pass
     * the CONNECT request on to it.
     */
    if (proxyport) {
    /* FIXME: Error checking ignored.
     */
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                      "sending the CONNECT request to the remote proxy");
        clhy_fprintf(backconn->output_filters, bb_back,
                   "CONNECT %s HTTP/1.0" CRLF, r->uri);
        clhy_fprintf(backconn->output_filters, bb_back,
                   "Proxy-agent: %s" CRLF CRLF, clhy_get_server_banner());
        clhy_fflush(backconn->output_filters, bb_back);
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, r, "Returning 200 OK");
        nbytes = kuda_snprintf(buffer, sizeof(buffer),
                              "HTTP/1.0 200 Connection Established" CRLF);
        clhy_xlate_proto_to_ascii(buffer, nbytes);
        clhy_fwrite(c->output_filters, bb_front, buffer, nbytes);
        nbytes = kuda_snprintf(buffer, sizeof(buffer),
                              "Proxy-agent: %s" CRLF CRLF,
                              clhy_get_server_banner());
        clhy_xlate_proto_to_ascii(buffer, nbytes);
        clhy_fwrite(c->output_filters, bb_front, buffer, nbytes);
        clhy_fflush(c->output_filters, bb_front);
#if 0
        /* This is safer code, but it doesn't work yet.  I'm leaving it
         * here so that I can fix it later.
         */
        r->status = HTTP_OK;
        r->header_only = 1;
        kuda_table_set(r->headers_out, "Proxy-agent: %s", clhy_get_server_banner());
        clhy_rflush(r);
#endif
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r, "setting up poll()");

    /*
     * Step Four: Handle Data Transfer
     *
     * Handle two way transfer of data over the socket (this is a tunnel).
     */

    /* we are now acting as a tunnel - the input/output filter stacks should
     * not contain any non-connection filters.
     */
    r->output_filters = c->output_filters;
    r->proto_output_filters = c->output_filters;
    r->input_filters = c->input_filters;
    r->proto_input_filters = c->input_filters;
/*    r->sent_bodyct = 1;*/

    do { /* Loop until done (one side closes the connection, or an error) */
        rv = kuda_pollset_poll(pollset, -1, &pollcnt, &signalled);
        if (rv != KUDA_SUCCESS) {
            if (KUDA_STATUS_IS_EINTR(rv)) {
                continue;
            }
            kuda_socket_close(sock);
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01023) "error kuda_poll()");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
#ifdef DEBUGGING
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01024)
                      "woke from poll(), i=%d", pollcnt);
#endif

        for (pi = 0; pi < pollcnt; pi++) {
            const kuda_pollfd_t *cur = &signalled[pi];

            if (cur->desc.s == sock) {
                pollevent = cur->rtnevents;
                if (pollevent & (KUDA_POLLIN | KUDA_POLLHUP)) {
#ifdef DEBUGGING
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01025)
                                  "sock was readable");
#endif
                    done |= clhy_proxy_transfer_between_connections(r, backconn,
                                                                  c, bb_back,
                                                                  bb_front,
                                                                  "sock", NULL,
                                                                  CONN_BLKSZ, 1)
                                                                 != KUDA_SUCCESS;
                }
                else if (pollevent & KUDA_POLLERR) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, r, CLHYLOGNO(01026)
                                  "err on backconn");
                    backconn->aborted = 1;
                    done = 1;
                }
            }
            else if (cur->desc.s == client_socket) {
                pollevent = cur->rtnevents;
                if (pollevent & (KUDA_POLLIN | KUDA_POLLHUP)) {
#ifdef DEBUGGING
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01027)
                                  "client was readable");
#endif
                    done |= clhy_proxy_transfer_between_connections(r, c,
                                                                  backconn,
                                                                  bb_front,
                                                                  bb_back,
                                                                  "client",
                                                                  NULL,
                                                                  CONN_BLKSZ, 1)
                                                                 != KUDA_SUCCESS;
                }
                else if (pollevent & KUDA_POLLERR) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, r, CLHYLOGNO(02827)
                                  "err on client");
                    c->aborted = 1;
                    done = 1;
                }
            }
            else {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(01028)
                              "unknown socket in pollset");
                done = 1;
            }

        }
    } while (!done);

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                  "finished with poll() - cleaning up");

    /*
     * Step Five: Clean Up
     *
     * Close the socket and clean up
     */

    if (backconn->aborted)
        kuda_socket_close(sock);
    else
        clhy_lingering_close(backconn);

    c->keepalive = CLHY_CONN_CLOSE;

    return OK;
}

static void clhy_proxy_connect_register_hook(kuda_pool_t *p)
{
    proxy_hook_scheme_handler(proxy_connect_handler, NULL, NULL, KUDA_HOOK_MIDDLE);
    proxy_hook_canon_handler(proxy_connect_canon, NULL, NULL, KUDA_HOOK_MIDDLE);
}

static const command_rec cmds[] =
{
    CLHY_INIT_ITERATE("AllowCONNECT", set_allowed_ports, NULL, RSRC_CONF,
     "A list of ports or port ranges which CONNECT may connect to"),
    {NULL}
};

CLHY_DECLARE_CAPI(proxy_connect) = {
    STANDARD16_CAPI_STUFF,
    NULL,       /* create per-directory config structure */
    NULL,       /* merge per-directory config structures */
    create_config,       /* create per-server config structure */
    merge_config,       /* merge per-server config structures */
    cmds,       /* command kuda_table_t */
    clhy_proxy_connect_register_hook  /* register hooks */
};
