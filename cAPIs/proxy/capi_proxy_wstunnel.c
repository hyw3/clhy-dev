/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "capi_proxy.h"

cAPI CLHY_CAPI_DECLARE_DATA proxy_wstunnel_capi;

/*
 * Canonicalise http-like URLs.
 * scheme is the scheme for the URL
 * url is the URL starting with the first '/'
 * def_port is the default port for this scheme.
 */
static int proxy_wstunnel_canon(request_rec *r, char *url)
{
    char *host, *path, sport[7];
    char *search = NULL;
    const char *err;
    char *scheme;
    kuda_port_t port, def_port;

    /* clhy_port_of_scheme() */
    if (strncasecmp(url, "ws:", 3) == 0) {
        url += 3;
        scheme = "ws:";
        def_port = kuda_uri_port_of_scheme("http");
    }
    else if (strncasecmp(url, "wss:", 4) == 0) {
        url += 4;
        scheme = "wss:";
        def_port = kuda_uri_port_of_scheme("https");
    }
    else {
        return DECLINED;
    }

    port = def_port;
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, r, "canonicalising URL %s", url);

    /*
     * do syntactic check.
     * We break the URL into host, port, path, search
     */
    err = clhy_proxy_canon_netloc(r->pool, &url, NULL, NULL, &host, &port);
    if (err) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02439) "error parsing URL %s: %s",
                      url, err);
        return HTTP_BAD_REQUEST;
    }

    /*
     * now parse path/search args, according to rfc1738:
     * process the path. With proxy-nocanon set (by
     * capi_proxy) we use the raw, unparsed uri
     */
    if (kuda_table_get(r->notes, "proxy-nocanon")) {
        path = url;   /* this is the raw path */
    }
    else {
        path = clhy_proxy_canonenc(r->pool, url, strlen(url), enc_path, 0,
                                 r->proxyreq);
        search = r->args;
    }
    if (path == NULL)
        return HTTP_BAD_REQUEST;

    kuda_snprintf(sport, sizeof(sport), ":%d", port);

    if (clhy_strchr_c(host, ':')) {
        /* if literal IPv6 address */
        host = kuda_pstrcat(r->pool, "[", host, "]", NULL);
    }
    r->filename = kuda_pstrcat(r->pool, "proxy:", scheme, "//", host, sport,
                              "/", path, (search) ? "?" : "",
                              (search) ? search : "", NULL);
    return OK;
}

/*
 * process the request and write the response.
 */
static int proxy_wstunnel_request(kuda_pool_t *p, request_rec *r,
                                proxy_conn_rec *conn,
                                proxy_worker *worker,
                                proxy_server_conf *conf,
                                kuda_uri_t *uri,
                                char *url, char *server_portstr)
{
    kuda_status_t rv;
    kuda_pollset_t *pollset;
    kuda_pollfd_t pollfd;
    const kuda_pollfd_t *signalled;
    kuda_int32_t pollcnt, pi;
    kuda_int16_t pollevent;
    conn_rec *c = r->connection;
    kuda_socket_t *sock = conn->sock;
    conn_rec *backconn = conn->connection;
    char *buf;
    kuda_bucket_brigade *header_brigade;
    kuda_bucket *e;
    char *old_cl_val = NULL;
    char *old_te_val = NULL;
    kuda_bucket_brigade *bb = kuda_brigade_create(p, c->bucket_alloc);
    kuda_socket_t *client_socket = clhy_get_conn_socket(c);
    int done = 0, replied = 0;

    header_brigade = kuda_brigade_create(p, backconn->bucket_alloc);

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r, "sending request");

    rv = clhy_proxy_create_hdrbrgd(p, header_brigade, r, conn,
                                 worker, conf, uri, url, server_portstr,
                                 &old_cl_val, &old_te_val);
    if (rv != OK) {
        return rv;
    }

    buf = kuda_pstrdup(p, "Upgrade: WebSocket" CRLF "Connection: Upgrade" CRLF CRLF);
    clhy_xlate_proto_to_ascii(buf, strlen(buf));
    e = kuda_bucket_pool_create(buf, strlen(buf), p, c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(header_brigade, e);

    if ((rv = clhy_proxy_pass_brigade(backconn->bucket_alloc, r, conn, backconn,
                                    header_brigade, 1)) != OK)
        return rv;

    kuda_brigade_cleanup(header_brigade);

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r, "setting up poll()");

    if ((rv = kuda_pollset_create(&pollset, 2, p, 0)) != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(02443)
                      "error kuda_pollset_create()");
        return HTTP_INTERNAL_SERVER_ERROR;
    }

#if 0
    kuda_socket_opt_set(sock, KUDA_SO_NONBLOCK, 1);
    kuda_socket_opt_set(sock, KUDA_SO_KEEPALIVE, 1);
    kuda_socket_opt_set(client_socket, KUDA_SO_NONBLOCK, 1);
    kuda_socket_opt_set(client_socket, KUDA_SO_KEEPALIVE, 1);
#endif

    pollfd.p = p;
    pollfd.desc_type = KUDA_POLL_SOCKET;
    pollfd.reqevents = KUDA_POLLIN | KUDA_POLLHUP;
    pollfd.desc.s = sock;
    pollfd.client_data = NULL;
    kuda_pollset_add(pollset, &pollfd);

    pollfd.desc.s = client_socket;
    kuda_pollset_add(pollset, &pollfd);

    clhy_remove_input_filter_byhandle(c->input_filters, "reqtimeout");

    r->output_filters = c->output_filters;
    r->proto_output_filters = c->output_filters;
    r->input_filters = c->input_filters;
    r->proto_input_filters = c->input_filters;

    /* This handler should take care of the entire connection; make it so that
     * nothing else is attempted on the connection after returning. */
    c->keepalive = CLHY_CONN_CLOSE;

    do { /* Loop until done (one side closes the connection, or an error) */
        rv = kuda_pollset_poll(pollset, -1, &pollcnt, &signalled);
        if (rv != KUDA_SUCCESS) {
            if (KUDA_STATUS_IS_EINTR(rv)) {
                continue;
            }
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(02444) "error kuda_poll()");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, r, CLHYLOGNO(02445)
                      "woke from poll(), i=%d", pollcnt);

        for (pi = 0; pi < pollcnt; pi++) {
            const kuda_pollfd_t *cur = &signalled[pi];

            if (cur->desc.s == sock) {
                pollevent = cur->rtnevents;
                if (pollevent & (KUDA_POLLIN | KUDA_POLLHUP)) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, r, CLHYLOGNO(02446)
                                  "sock was readable");
                    done |= clhy_proxy_transfer_between_connections(r, backconn,
                                                                  c,
                                                                  header_brigade,
                                                                  bb, "sock",
                                                                  NULL,
                                                                  CLHY_IOBUFSIZE,
                                                                  0)
                                                                 != KUDA_SUCCESS;
                }
                else if (pollevent & KUDA_POLLERR) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, r, CLHYLOGNO(02447)
                            "error on backconn");
                    backconn->aborted = 1;
                    done = 1;
                }
                else { 
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, r, CLHYLOGNO(02605)
                            "unknown event on backconn %d", pollevent);
                    done = 1;
                }
            }
            else if (cur->desc.s == client_socket) {
                pollevent = cur->rtnevents;
                if (pollevent & (KUDA_POLLIN | KUDA_POLLHUP)) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, r, CLHYLOGNO(02448)
                                  "client was readable");
                    done |= clhy_proxy_transfer_between_connections(r, c,
                                                                  backconn, bb,
                                                                  header_brigade,
                                                                  "client",
                                                                  &replied,
                                                                  CLHY_IOBUFSIZE,
                                                                  0)
                                                                 != KUDA_SUCCESS;
                }
                else if (pollevent & KUDA_POLLERR) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, r, CLHYLOGNO(02607)
                            "error on client conn");
                    c->aborted = 1;
                    done = 1;
                }
                else { 
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, r, CLHYLOGNO(02606)
                            "unknown event on client conn %d", pollevent);
                    done = 1;
                }
            }
            else {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(02449)
                              "unknown socket in pollset");
                done = 1;
            }

        }
    } while (!done);

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                  "finished with poll() - cleaning up");

    if (!replied) {
        return HTTP_BAD_GATEWAY;
    }
    else {
        return OK;
    }

    return OK;
}

/*
 */
static int proxy_wstunnel_handler(request_rec *r, proxy_worker *worker,
                             proxy_server_conf *conf,
                             char *url, const char *proxyname,
                             kuda_port_t proxyport)
{
    int status;
    char server_portstr[32];
    proxy_conn_rec *backend = NULL;
    const char *upgrade;
    char *scheme;
    int retry;
    conn_rec *c = r->connection;
    kuda_pool_t *p = r->pool;
    kuda_uri_t *uri;
    int is_ssl = 0;

    if (strncasecmp(url, "wss:", 4) == 0) {
        scheme = "WSS";
        is_ssl = 1;
    }
    else if (strncasecmp(url, "ws:", 3) == 0) {
        scheme = "WS";
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02450) "declining URL %s", url);
        return DECLINED;
    }

    upgrade = kuda_table_get(r->headers_in, "Upgrade");
    if (!upgrade || strcasecmp(upgrade, "WebSocket") != 0) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02900)
                      "declining URL %s  (not WebSocket)", url);
        return DECLINED;
    }

    uri = kuda_palloc(p, sizeof(*uri));
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02451) "serving URL %s", url);

    /* create space for state information */
    status = clhy_proxy_acquire_connection(scheme, &backend, worker,
                                         r->server);
    if (status != OK) {
        if (backend) {
            backend->close = 1;
            clhy_proxy_release_connection(scheme, backend, r->server);
        }
        return status;
    }

    backend->is_ssl = is_ssl;
    backend->close = 0;

    retry = 0;
    while (retry < 2) {
        char *locurl = url;
        /* Step One: Determine Who To Connect To */
        status = clhy_proxy_determine_connection(p, r, conf, worker, backend,
                                               uri, &locurl, proxyname, proxyport,
                                               server_portstr,
                                               sizeof(server_portstr));

        if (status != OK)
            break;

        /* Step Two: Make the Connection */
        if (clhy_proxy_connect_backend(scheme, backend, worker, r->server)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02452)
                          "failed to make connection to backend: %s",
                          backend->hostname);
            status = HTTP_SERVICE_UNAVAILABLE;
            break;
        }
        /* Step Three: Create conn_rec */
        if (!backend->connection) {
            if ((status = clhy_proxy_connection_create(scheme, backend,
                                                     c, r->server)) != OK)
                break;
        }

        backend->close = 1; /* must be after clhy_proxy_determine_connection */


        /* Step Three: Process the Request */
        status = proxy_wstunnel_request(p, r, backend, worker, conf, uri, locurl,
                                      server_portstr);
        break;
    }

    /* Do not close the socket */
    clhy_proxy_release_connection(scheme, backend, r->server);
    return status;
}

static void clhy_proxy_http_register_hook(kuda_pool_t *p)
{
    proxy_hook_scheme_handler(proxy_wstunnel_handler, NULL, NULL, KUDA_HOOK_FIRST);
    proxy_hook_canon_handler(proxy_wstunnel_canon, NULL, NULL, KUDA_HOOK_FIRST);
}

CLHY_DECLARE_CAPI(proxy_wstunnel) = {
    STANDARD16_CAPI_STUFF,
    NULL,                       /* create per-directory config structure */
    NULL,                       /* merge per-directory config structures */
    NULL,                       /* create per-server config structure */
    NULL,                       /* merge per-server config structures */
    NULL,                       /* command kuda_table_t */
    clhy_proxy_http_register_hook /* register hooks */
};
