/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "capi_proxy.h"
#include "scoreboard.h"
#include "clhy_core.h"
#include "kuda_version.h"
#include "clhy_hooks.h"
#include "clhy_slotmem.h"
#include "heartbeat.h"

#ifndef LBM_HEARTBEAT_MAX_LASTSEEN
/* If we haven't seen a heartbeat in the last N seconds, don't count this IP
 * as allive.
 */
#define LBM_HEARTBEAT_MAX_LASTSEEN (10)
#endif

cAPI CLHY_CAPI_DECLARE_DATA lbmethod_heartbeat_capi;

static int (*clhy_proxy_retry_worker_fn)(const char *proxy_function,
        proxy_worker *worker, server_rec *s) = NULL;

static const clhy_slotmem_provider_t *storage = NULL;
static clhy_slotmem_instance_t *hm_serversmem = NULL;

/*
 * configuration structure
 * path: path of the file where the heartbeat information is stored.
 */
typedef struct lb_hb_ctx_t
{
    const char *path;
} lb_hb_ctx_t;

typedef struct hb_server_t {
    const char *ip;
    int busy;
    int ready;
    int port;
    int id;
    kuda_time_t seen;
    proxy_worker *worker;
} hb_server_t;

typedef struct ctx_servers {
    kuda_time_t now;
    kuda_hash_t *servers;
} ctx_servers_t;

static void
argstr_to_table(kuda_pool_t *p, char *str, kuda_table_t *parms)
{
    char *key;
    char *value;
    char *strtok_state;

    key = kuda_strtok(str, "&", &strtok_state);
    while (key) {
        value = strchr(key, '=');
        if (value) {
            *value = '\0';      /* Split the string in two */
            value++;            /* Skip passed the = */
        }
        else {
            value = "1";
        }
        clhy_unescape_url(key);
        clhy_unescape_url(value);
        kuda_table_set(parms, key, value);
        /*
         clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03230)
         "Found query arg: %s = %s", key, value);
         */
        key = kuda_strtok(NULL, "&", &strtok_state);
    }
}

static kuda_status_t readfile_heartbeats(const char *path, kuda_hash_t *servers,
                                    kuda_pool_t *pool)
{
    kuda_finfo_t fi;
    kuda_status_t rv;
    kuda_file_t *fp;

    if (!path) {
        return KUDA_SUCCESS;
    }

    rv = kuda_file_open(&fp, path, KUDA_READ|KUDA_BINARY|KUDA_BUFFERED,
                       KUDA_PLATFORM_DEFAULT, pool);

    if (rv) {
        return rv;
    }

    rv = kuda_file_info_get(&fi, KUDA_FINFO_SIZE, fp);

    if (rv) {
        return rv;
    }

    {
        char *t;
        int lineno = 0;
        kuda_bucket_alloc_t *ba = kuda_bucket_alloc_create(pool);
        kuda_bucket_brigade *bb = kuda_brigade_create(pool, ba);
        kuda_bucket_brigade *tmpbb = kuda_brigade_create(pool, ba);
        kuda_table_t *hbt = kuda_table_make(pool, 10);

        kuda_brigade_insert_file(bb, fp, 0, fi.size, pool);

        do {
            hb_server_t *server;
            char buf[4096];
            kuda_size_t bsize = sizeof(buf);
            const char *ip, *val;

            kuda_brigade_cleanup(tmpbb);

            if (KUDA_BRIGADE_EMPTY(bb)) {
                break;
            }

            rv = kuda_brigade_split_line(tmpbb, bb,
                                        KUDA_BLOCK_READ, sizeof(buf));
            lineno++;

            if (rv) {
                return rv;
            }

            kuda_brigade_flatten(tmpbb, buf, &bsize);

            if (bsize == 0) {
                break;
            }

            buf[bsize - 1] = 0;

            /* comment */
            if (buf[0] == '#') {
                continue;
            }

            /* line format: <IP> <query_string>\n */
            t = strchr(buf, ' ');
            if (!t) {
                continue;
            }

            ip = kuda_pstrmemdup(pool, buf, t - buf);
            t++;

            server = kuda_hash_get(servers, ip, KUDA_HASH_KEY_STRING);

            if (server == NULL) {
                server = kuda_pcalloc(pool, sizeof(hb_server_t));
                server->ip = ip;
                server->port = 80;
                server->seen = -1;

                kuda_hash_set(servers, server->ip, KUDA_HASH_KEY_STRING, server);
            }

            kuda_table_clear(hbt);

            argstr_to_table(pool, kuda_pstrdup(pool, t), hbt);

            if ((val = kuda_table_get(hbt, "busy"))) {
                server->busy = atoi(val);
            }

            if ((val = kuda_table_get(hbt, "ready"))) {
                server->ready = atoi(val);
            }

            if ((val = kuda_table_get(hbt, "lastseen"))) {
                server->seen = atoi(val);
            }

            if ((val = kuda_table_get(hbt, "port"))) {
                server->port = atoi(val);
            }

            if (server->busy == 0 && server->ready != 0) {
                /* Server has zero threads active, but lots of them ready,
                 * it likely just started up, so lets /4 the number ready,
                 * to prevent us from completely flooding it with all new
                 * requests.
                 */
                server->ready = server->ready / 4;
            }

        } while (1);
    }

    return KUDA_SUCCESS;
}

static kuda_status_t hm_read(void* mem, void *data, kuda_pool_t *pool)
{
    hm_slot_server_t *slotserver = (hm_slot_server_t *) mem;
    ctx_servers_t *ctx = (ctx_servers_t *) data;
    kuda_hash_t *servers = (kuda_hash_t *) ctx->servers;
    hb_server_t *server = kuda_hash_get(servers, slotserver->ip, KUDA_HASH_KEY_STRING);
    if (server == NULL) {
        server = kuda_pcalloc(pool, sizeof(hb_server_t));
        server->ip = kuda_pstrdup(pool, slotserver->ip);
        server->seen = -1;

        kuda_hash_set(servers, server->ip, KUDA_HASH_KEY_STRING, server);

    }
    server->busy = slotserver->busy;
    server->ready = slotserver->ready;
    server->seen = kuda_time_sec(ctx->now - slotserver->seen);
    server->id = slotserver->id;
    if (server->busy == 0 && server->ready != 0) {
        server->ready = server->ready / 4;
    }
    return KUDA_SUCCESS;
}
static kuda_status_t readslot_heartbeats(ctx_servers_t *ctx,
                                    kuda_pool_t *pool)
{
    storage->doall(hm_serversmem, hm_read, ctx, pool);
    return KUDA_SUCCESS;
}


static kuda_status_t read_heartbeats(const char *path, kuda_hash_t *servers,
                                        kuda_pool_t *pool)
{
    kuda_status_t rv;
    if (hm_serversmem) {
        ctx_servers_t ctx;
        ctx.now = kuda_time_now();
        ctx.servers = servers;
        rv = readslot_heartbeats(&ctx, pool);
    } else
        rv = readfile_heartbeats(path, servers, pool);
    return rv;
}

static proxy_worker *find_best_hb(proxy_balancer *balancer,
                                  request_rec *r)
{
    kuda_status_t rv;
    int i;
    kuda_uint32_t openslots = 0;
    proxy_worker **worker;
    hb_server_t *server;
    kuda_array_header_t *up_servers;
    proxy_worker *mycandidate = NULL;
    kuda_pool_t *tpool;
    kuda_hash_t *servers;

    lb_hb_ctx_t *ctx =
        clhy_get_capi_config(r->server->capi_config,
                             &lbmethod_heartbeat_capi);

    if (!clhy_proxy_retry_worker_fn) {
        clhy_proxy_retry_worker_fn =
                KUDA_RETRIEVE_OPTIONAL_FN(clhy_proxy_retry_worker);
        if (!clhy_proxy_retry_worker_fn) {
            /* can only happen if capi_proxy isn't loaded */
            return NULL;
        }
    }

    kuda_pool_create(&tpool, r->pool);

    servers = kuda_hash_make(tpool);

    rv = read_heartbeats(ctx->path, servers, tpool);

    if (rv) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01213)
                      "lb_heartbeat: Unable to read heartbeats at '%s'",
                      ctx->path);
        kuda_pool_destroy(tpool);
        return NULL;
    }

    up_servers = kuda_array_make(tpool, kuda_hash_count(servers), sizeof(hb_server_t *));

    for (i = 0; i < balancer->workers->nelts; i++) {
        worker = &KUDA_ARRAY_IDX(balancer->workers, i, proxy_worker *);
        server = kuda_hash_get(servers, (*worker)->s->hostname, KUDA_HASH_KEY_STRING);

        if (!server) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, r, CLHYLOGNO(01214)
                      "lb_heartbeat: No server for worker %s", (*worker)->s->name);
            continue;
        }

        if (!PROXY_WORKER_IS_USABLE(*worker)) {
            clhy_proxy_retry_worker_fn("BALANCER", *worker, r->server);
        }

        if (PROXY_WORKER_IS_USABLE(*worker)) {
            server->worker = *worker;
            if (server->seen < LBM_HEARTBEAT_MAX_LASTSEEN) {
                openslots += server->ready;
                KUDA_ARRAY_PUSH(up_servers, hb_server_t *) = server;
            }
        }
    }

    if (openslots > 0) {
        kuda_uint32_t c = 0;
        kuda_uint32_t pick = 0;

        pick = clhy_random_pick(0, openslots);

        for (i = 0; i < up_servers->nelts; i++) {
            server = KUDA_ARRAY_IDX(up_servers, i, hb_server_t *);
            if (pick >= c && pick <= c + server->ready) {
                mycandidate = server->worker;
            }

            c += server->ready;
        }
    }

    kuda_pool_destroy(tpool);

    return mycandidate;
}

static kuda_status_t reset(proxy_balancer *balancer, server_rec *s)
{
    return KUDA_SUCCESS;
}

static kuda_status_t age(proxy_balancer *balancer, server_rec *s)
{
    return KUDA_SUCCESS;
}

static const proxy_balancer_method heartbeat =
{
    "heartbeat",
    &find_best_hb,
    NULL,
    &reset,
    &age
};

static int lb_hb_init(kuda_pool_t *p, kuda_pool_t *plog,
                      kuda_pool_t *ptemp, server_rec *s)
{
    kuda_size_t size;
    unsigned int num;
    lb_hb_ctx_t *ctx = clhy_get_capi_config(s->capi_config,
                                            &lbmethod_heartbeat_capi);

    /* do nothing on first call */
    if (clhy_state_query(CLHY_SQ_MAIN_STATE) == CLHY_SQ_MS_CREATE_PRE_CONFIG)
        return OK;

    storage = clhy_lookup_provider(CLHY_SLOTMEM_PROVIDER_GROUP, "shm",
                                 CLHY_SLOTMEM_PROVIDER_VERSION);
    if (!storage) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, s, CLHYLOGNO(02281)
                     "Failed to lookup provider 'shm' for '%s'. Maybe you "
                     "need to load capi_slotmem_shm?",
                     CLHY_SLOTMEM_PROVIDER_GROUP);
        return OK;
    }

    /* Try to use a slotmem created by capi_heartmonitor */
    storage->attach(&hm_serversmem, "capi_heartmonitor", &size, &num, p);
    if (!hm_serversmem)
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, s, CLHYLOGNO(02282)
                     "No slotmem from capi_heartmonitor");
    else
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, s, CLHYLOGNO(02283)
                     "Using slotmem from capi_heartmonitor");

    if (hm_serversmem)
        ctx->path = "(slotmem)";

    return OK;
}

static void register_hooks(kuda_pool_t *p)
{
    static const char * const aszPred[]={ "capi_heartmonitor.c", NULL };
    clhy_register_provider(p, PROXY_LBMETHOD, "heartbeat", "0", &heartbeat);
    clhy_hook_post_config(lb_hb_init, aszPred, NULL, KUDA_HOOK_MIDDLE);
}

static void *lb_hb_create_config(kuda_pool_t *p, server_rec *s)
{
    lb_hb_ctx_t *ctx = (lb_hb_ctx_t *) kuda_palloc(p, sizeof(lb_hb_ctx_t));

    ctx->path = clhy_runtime_dir_relative(p, DEFAULT_HEARTBEAT_STORAGE);

    return ctx;
}

static void *lb_hb_merge_config(kuda_pool_t *p, void *basev, void *overridesv)
{
    lb_hb_ctx_t *ps = kuda_pcalloc(p, sizeof(lb_hb_ctx_t));
    lb_hb_ctx_t *base = (lb_hb_ctx_t *) basev;
    lb_hb_ctx_t *overrides = (lb_hb_ctx_t *) overridesv;

    if (overrides->path) {
        ps->path = kuda_pstrdup(p, overrides->path);
    }
    else {
        ps->path = kuda_pstrdup(p, base->path);
    }

    return ps;
}

static const char *cmd_lb_hb_storage(cmd_parms *cmd,
                                  void *dconf, const char *path)
{
    kuda_pool_t *p = cmd->pool;
    lb_hb_ctx_t *ctx =
    (lb_hb_ctx_t *) clhy_get_capi_config(cmd->server->capi_config,
                                         &lbmethod_heartbeat_capi);

    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err != NULL) {
        return err;
    }

    ctx->path = clhy_runtime_dir_relative(p, path);

    return NULL;
}

static const command_rec cmds[] = {
    CLHY_INIT_TAKE1("HeartbeatStorage", cmd_lb_hb_storage, NULL, RSRC_CONF,
                  "Path to read heartbeat data."),
    {NULL}
};

CLHY_DECLARE_CAPI(lbmethod_heartbeat) = {
    STANDARD16_CAPI_STUFF,
    NULL,                       /* create per-directory config structure */
    NULL,                       /* merge per-directory config structures */
    lb_hb_create_config,        /* create per-server config structure */
    lb_hb_merge_config,         /* merge per-server config structures */
    cmds,                       /* command kuda_table_t */
    register_hooks              /* register hooks */
};
