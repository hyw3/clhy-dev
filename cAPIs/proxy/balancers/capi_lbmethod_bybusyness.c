/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "capi_proxy.h"
#include "scoreboard.h"
#include "clhy_core.h"
#include "kuda_version.h"
#include "clhy_hooks.h"

cAPI CLHY_CAPI_DECLARE_DATA lbmethod_bybusyness_capi;

static int (*clhy_proxy_retry_worker_fn)(const char *proxy_function,
        proxy_worker *worker, server_rec *s) = NULL;

static proxy_worker *find_best_bybusyness(proxy_balancer *balancer,
                                request_rec *r)
{
    int i;
    proxy_worker **worker;
    proxy_worker *mycandidate = NULL;
    int cur_lbset = 0;
    int max_lbset = 0;
    int checking_standby;
    int checked_standby;

    int total_factor = 0;

    if (!clhy_proxy_retry_worker_fn) {
        clhy_proxy_retry_worker_fn =
                KUDA_RETRIEVE_OPTIONAL_FN(clhy_proxy_retry_worker);
        if (!clhy_proxy_retry_worker_fn) {
            /* can only happen if capi_proxy isn't loaded */
            return NULL;
        }
    }

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r->server, CLHYLOGNO(01211)
                 "proxy: Entering bybusyness for BALANCER (%s)",
                 balancer->s->name);

    /* First try to see if we have available candidate */
    do {

        checking_standby = checked_standby = 0;
        while (!mycandidate && !checked_standby) {

            worker = (proxy_worker **)balancer->workers->elts;
            for (i = 0; i < balancer->workers->nelts; i++, worker++) {
                if  (!checking_standby) {    /* first time through */
                    if ((*worker)->s->lbset > max_lbset)
                        max_lbset = (*worker)->s->lbset;
                }
                if (
                    ((*worker)->s->lbset != cur_lbset) ||
                    (checking_standby ? !PROXY_WORKER_IS_STANDBY(*worker) : PROXY_WORKER_IS_STANDBY(*worker)) ||
                    (PROXY_WORKER_IS_DRAINING(*worker))
                    ) {
                    continue;
                }

                /* If the worker is in error state run
                 * retry on that worker. It will be marked as
                 * operational if the retry timeout is elapsed.
                 * The worker might still be unusable, but we try
                 * anyway.
                 */
                if (!PROXY_WORKER_IS_USABLE(*worker)) {
                    clhy_proxy_retry_worker_fn("BALANCER", *worker, r->server);
                }

                /* Take into calculation only the workers that are
                 * not in error state or not disabled.
                 */
                if (PROXY_WORKER_IS_USABLE(*worker)) {

                    (*worker)->s->lbstatus += (*worker)->s->lbfactor;
                    total_factor += (*worker)->s->lbfactor;

                    if (!mycandidate
                        || (*worker)->s->busy < mycandidate->s->busy
                        || ((*worker)->s->busy == mycandidate->s->busy && (*worker)->s->lbstatus > mycandidate->s->lbstatus))
                        mycandidate = *worker;

                }

            }

            checked_standby = checking_standby++;

        }

        cur_lbset++;

    } while (cur_lbset <= max_lbset && !mycandidate);

    if (mycandidate) {
        mycandidate->s->lbstatus -= total_factor;
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r->server, CLHYLOGNO(01212)
                     "proxy: bybusyness selected worker \"%s\" : busy %" KUDA_SIZE_T_FMT " : lbstatus %d",
                     mycandidate->s->name, mycandidate->s->busy, mycandidate->s->lbstatus);

    }

    return mycandidate;
}

/* assumed to be mutex protected by caller */
static kuda_status_t reset(proxy_balancer *balancer, server_rec *s)
{
    int i;
    proxy_worker **worker;
    worker = (proxy_worker **)balancer->workers->elts;
    for (i = 0; i < balancer->workers->nelts; i++, worker++) {
        (*worker)->s->lbstatus = 0;
        (*worker)->s->busy = 0;
    }
    return KUDA_SUCCESS;
}

static kuda_status_t age(proxy_balancer *balancer, server_rec *s)
{
    return KUDA_SUCCESS;
}

static const proxy_balancer_method bybusyness =
{
    "bybusyness",
    &find_best_bybusyness,
    NULL,
    &reset,
    &age
};

static void register_hook(kuda_pool_t *p)
{
    clhy_register_provider(p, PROXY_LBMETHOD, "bybusyness", "0", &bybusyness);
}

CLHY_DECLARE_CAPI(lbmethod_bybusyness) = {
    STANDARD16_CAPI_STUFF,
    NULL,       /* create per-directory config structure */
    NULL,       /* merge per-directory config structures */
    NULL,       /* create per-server config structure */
    NULL,       /* merge per-server config structures */
    NULL,       /* command kuda_table_t */
    register_hook /* register hooks */
};
