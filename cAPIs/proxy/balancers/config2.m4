CLHYKUDEL_CAPIPATH_INIT(proxy/balancers)

CLHYKUDEL_CAPI(lbmethod_byrequests, cLHy proxy Load balancing by request counting, , , $enable_proxy_balancer, , proxy_balancer)
CLHYKUDEL_CAPI(lbmethod_bytraffic, cLHy proxy Load balancing by traffic counting, , , $enable_proxy_balancer, , proxy_balancer)
CLHYKUDEL_CAPI(lbmethod_bybusyness, cLHy proxy Load balancing by busyness, , , $enable_proxy_balancer, , proxy_balancer)
CLHYKUDEL_CAPI(lbmethod_heartbeat, cLHy proxy Load balancing from Heartbeats, , , $enable_proxy_balancer, , proxy_balancer)

CLHYKUDEL_CAPIPATH_FINISH
