/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* capi_proxy_uwsgi
 *
 * Copyright 2009-2017 Unbit S.a.s. <info@unbit.it>,
 * See ./NOTICE file.
 *
 * HLF notes:
 * We use CLHY_CAPI_MAGIC_AT_LEAST(20191101,6) to recognize
 * error doc/log ProxyErrorOverride and add PATH_INFO in
 * worker backend.
 */

#define KUDA_WANT_MEMFUNC
#define KUDA_WANT_STRFUNC
#include "kuda_strings.h"
#include "kuda_hooks.h"
#include "kuda_optional_hooks.h"
#include "kuda_buckets.h"

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_request.h"
#include "util_script.h"

#include "capi_proxy.h"


#define UWSGI_SCHEME "uwsgi"
#define UWSGI_DEFAULT_PORT 3031

cAPI CLHY_CAPI_DECLARE_DATA proxy_uwsgi_capi;


static int uwsgi_canon(request_rec *r, char *url)
{
    char *host, sport[sizeof(":65535")];
    const char *err, *path;
    kuda_port_t port = UWSGI_DEFAULT_PORT;

    if (clhy_cstr_casecmpn(url, UWSGI_SCHEME "://", sizeof(UWSGI_SCHEME) + 2)) {
        return DECLINED;
    }
    url += sizeof(UWSGI_SCHEME);        /* Keep slashes */

    err = clhy_proxy_canon_netloc(r->pool, &url, NULL, NULL, &host, &port);
    if (err) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(10097)
                      "error parsing URL %s: %s", url, err);
        return HTTP_BAD_REQUEST;
    }

    if (port != UWSGI_DEFAULT_PORT)
        kuda_snprintf(sport, sizeof(sport), ":%u", port);
    else
        sport[0] = '\0';

    if (clhy_strchr(host, ':')) { /* if literal IPv6 address */
        host = kuda_pstrcat(r->pool, "[", host, "]", NULL);
    }

    path = clhy_proxy_canonenc(r->pool, url, strlen(url), enc_path, 0,
                             r->proxyreq);
    if (!path) {
        return HTTP_BAD_REQUEST;
    }

    r->filename =
        kuda_pstrcat(r->pool, "proxy:" UWSGI_SCHEME "://", host, sport, "/",
                    path, NULL);

    return OK;
}


static int uwsgi_send(proxy_conn_rec * conn, const char *buf,
                      kuda_size_t length, request_rec *r)
{
    kuda_status_t rv;
    kuda_size_t written;

    while (length > 0) {
        written = length;
        if ((rv = kuda_socket_send(conn->sock, buf, &written)) != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(10098)
                          "sending data to %s:%u failed",
                          conn->hostname, conn->port);
            return HTTP_SERVICE_UNAVAILABLE;
        }

        /* count for stats */
        conn->worker->s->transferred += written;
        buf += written;
        length -= written;
    }

    return OK;
}


/*
 * Send uwsgi header block
 */
static int uwsgi_send_headers(request_rec *r, proxy_conn_rec * conn)
{
    char *buf, *ptr;

    const kuda_array_header_t *env_table;
    const kuda_table_entry_t *env;

    int j;

    kuda_size_t headerlen = 4;
    kuda_uint16_t pktsize, keylen, vallen;
    const char *script_name;
    const char *path_info;
    const char *auth;

    clhy_add_common_vars(r);
    clhy_add_cgi_vars(r);

    /*
       this is not a security problem (in Linux) as uWSGI destroy the env memory area readable in /proc
       and generally if you host untrusted apps in your server and allows them to read others uid /proc/<pid>
       files you have higher problems...
     */
    auth = kuda_table_get(r->headers_in, "Authorization");
    if (auth) {
        kuda_table_setn(r->subprocess_env, "HTTP_AUTHORIZATION", auth);
    }

    script_name = kuda_table_get(r->subprocess_env, "SCRIPT_NAME");
    path_info = kuda_table_get(r->subprocess_env, "PATH_INFO");

    if (script_name && path_info) {
        if (strcmp(path_info, "/")) {
            kuda_table_set(r->subprocess_env, "SCRIPT_NAME",
                          kuda_pstrndup(r->pool, script_name,
                                       strlen(script_name) -
                                       strlen(path_info)));
        }
        else {
            if (!strcmp(script_name, "/")) {
                kuda_table_setn(r->subprocess_env, "SCRIPT_NAME", "");
            }
        }
    }

    env_table = kuda_table_elts(r->subprocess_env);
    env = (kuda_table_entry_t *) env_table->elts;

    for (j = 0; j < env_table->nelts; ++j) {
        headerlen += 2 + strlen(env[j].key) + 2 + strlen(env[j].val);
    }

    ptr = buf = kuda_palloc(r->pool, headerlen);

    ptr += 4;

    for (j = 0; j < env_table->nelts; ++j) {
        keylen = strlen(env[j].key);
        *ptr++ = (kuda_byte_t) (keylen & 0xff);
        *ptr++ = (kuda_byte_t) ((keylen >> 8) & 0xff);
        memcpy(ptr, env[j].key, keylen);
        ptr += keylen;

        vallen = strlen(env[j].val);
        *ptr++ = (kuda_byte_t) (vallen & 0xff);
        *ptr++ = (kuda_byte_t) ((vallen >> 8) & 0xff);
        memcpy(ptr, env[j].val, vallen);
        ptr += vallen;
    }

    pktsize = headerlen - 4;

    buf[0] = 0;
    buf[1] = (kuda_byte_t) (pktsize & 0xff);
    buf[2] = (kuda_byte_t) ((pktsize >> 8) & 0xff);
    buf[3] = 0;

    return uwsgi_send(conn, buf, headerlen, r);
}


static int uwsgi_send_body(request_rec *r, proxy_conn_rec * conn)
{
    if (clhy_should_client_block(r)) {
        char *buf = kuda_palloc(r->pool, CLHY_IOBUFSIZE);
        int status;
        long readlen;

        readlen = clhy_get_client_block(r, buf, CLHY_IOBUFSIZE);
        while (readlen > 0) {
            status = uwsgi_send(conn, buf, (kuda_size_t)readlen, r);
            if (status != OK) {
                return HTTP_SERVICE_UNAVAILABLE;
            }
            readlen = clhy_get_client_block(r, buf, CLHY_IOBUFSIZE);
        }
        if (readlen == -1) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(10099)
                          "receiving request body failed");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
    }

    return OK;
}

static request_rec *make_fake_req(conn_rec *c, request_rec *r)
{
    kuda_pool_t *pool;
    request_rec *rp;

    kuda_pool_create(&pool, c->pool);

    rp = kuda_pcalloc(pool, sizeof(*r));

    rp->pool = pool;
    rp->status = HTTP_OK;

    rp->headers_in = kuda_table_make(pool, 50);
    rp->subprocess_env = kuda_table_make(pool, 50);
    rp->headers_out = kuda_table_make(pool, 12);
    rp->err_headers_out = kuda_table_make(pool, 5);
    rp->notes = kuda_table_make(pool, 5);

    rp->server = r->server;
    rp->log = r->log;
    rp->proxyreq = r->proxyreq;
    rp->request_time = r->request_time;
    rp->connection = c;
    rp->output_filters = c->output_filters;
    rp->input_filters = c->input_filters;
    rp->proto_output_filters = c->output_filters;
    rp->proto_input_filters = c->input_filters;
    rp->useragent_ip = c->client_ip;
    rp->useragent_addr = c->client_addr;

    rp->request_config = clhy_create_request_config(pool);
    proxy_run_create_req(r, rp);

    return rp;
}

static int uwsgi_response(request_rec *r, proxy_conn_rec * backend,
                          proxy_server_conf * conf)
{

    char buffer[HUGE_STRING_LEN];
    const char *buf;
    char *value, *end;
    char keepchar;
    int len;
    int backend_broke = 0;
    int status_start;
    int status_end;
    int finish = 0;
    conn_rec *c = r->connection;
    kuda_off_t readbytes;
    kuda_status_t rv;
    kuda_bucket *e;
    kuda_read_type_e mode = KUDA_NONBLOCK_READ;
    kuda_bucket_brigade *pass_bb;
    kuda_bucket_brigade *bb;
    proxy_dir_conf *dconf;

    request_rec *rp = make_fake_req(backend->connection, r);
    rp->proxyreq = PROXYREQ_RESPONSE;

    bb = kuda_brigade_create(r->pool, c->bucket_alloc);
    pass_bb = kuda_brigade_create(r->pool, c->bucket_alloc);

    len = clhy_getline(buffer, sizeof(buffer), rp, 1);

    if (len <= 0) {
        /* oops */
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    backend->worker->s->read += len;

    if (len >= sizeof(buffer) - 1) {
        /* oops */
        return HTTP_INTERNAL_SERVER_ERROR;
    }
    /* Position of http status code */
    if (kuda_date_checkmask(buffer, "HTTP/#.# ###*")) {
        status_start = 9;
    }
    else if (kuda_date_checkmask(buffer, "HTTP/# ###*")) {
        status_start = 7;
    }
    else {
        /* oops */
        return HTTP_INTERNAL_SERVER_ERROR;
    }
    status_end = status_start + 3;

    keepchar = buffer[status_end];
    buffer[status_end] = '\0';
    r->status = atoi(&buffer[status_start]);

    if (keepchar != '\0') {
        buffer[status_end] = keepchar;
    }
    else {
        /* 2616 requires the space in Status-Line; the origin
         * server may have sent one but clhy_rgetline_core will
         * have stripped it. */
        buffer[status_end] = ' ';
        buffer[status_end + 1] = '\0';
    }
    r->status_line = kuda_pstrdup(r->pool, &buffer[status_start]);

    /* start parsing headers */
    while ((len = clhy_getline(buffer, sizeof(buffer), rp, 1)) > 0) {
        value = strchr(buffer, ':');
        /* invalid header skip */
        if (!value)
            continue;
        *value = '\0';
        ++value;
        while (kuda_isspace(*value))
            ++value;
        for (end = &value[strlen(value) - 1];
             end > value && kuda_isspace(*end); --end)
            *end = '\0';
        kuda_table_add(r->headers_out, buffer, value);
    }

    if ((buf = kuda_table_get(r->headers_out, "Content-Type"))) {
        clhy_set_content_type(r, kuda_pstrdup(r->pool, buf));
    }

    /* honor ProxyErrorOverride and ErrorDocument */
#if CLHY_CAPI_MAGIC_AT_LEAST(20191101,6)
    dconf =
        clhy_get_capi_config(r->per_dir_config, &proxy_capi);
    if (dconf->error_override && clhy_is_HTTP_ERROR(r->status)) {
#else
    if (conf->error_override && clhy_is_HTTP_ERROR(r->status)) {
#endif
        int status = r->status;
        r->status = HTTP_OK;
        r->status_line = NULL;

        kuda_brigade_cleanup(bb);
        kuda_brigade_cleanup(pass_bb);

        return status;
    }

    while (!finish) {
        rv = clhy_get_brigade(rp->input_filters, bb,
                            CLHY_MODE_READBYTES, mode, conf->io_buffer_size);
        if (KUDA_STATUS_IS_EAGAIN(rv)
            || (rv == KUDA_SUCCESS && KUDA_BRIGADE_EMPTY(bb))) {
            e = kuda_bucket_flush_create(c->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(bb, e);
            if (clhy_pass_brigade(r->output_filters, bb) || c->aborted) {
                break;
            }
            kuda_brigade_cleanup(bb);
            mode = KUDA_BLOCK_READ;
            continue;
        }
        else if (rv == KUDA_EOF) {
            break;
        }
        else if (rv != KUDA_SUCCESS) {
            clhy_proxy_backend_broke(r, bb);
            clhy_pass_brigade(r->output_filters, bb);
            backend_broke = 1;
            break;
        }

        mode = KUDA_NONBLOCK_READ;
        kuda_brigade_length(bb, 0, &readbytes);
        backend->worker->s->read += readbytes;

        if (KUDA_BRIGADE_EMPTY(bb)) {
            kuda_brigade_cleanup(bb);
            break;
        }

        clhy_proxy_buckets_lifetime_transform(r, bb, pass_bb);

        /* found the last brigade? */
        if (KUDA_BUCKET_IS_EOS(KUDA_BRIGADE_LAST(bb)))
            finish = 1;

        /* do not pass chunk if it is zero_sized */
        kuda_brigade_length(pass_bb, 0, &readbytes);

        if ((readbytes > 0
             && clhy_pass_brigade(r->output_filters, pass_bb) != KUDA_SUCCESS)
            || c->aborted) {
            finish = 1;
        }

        kuda_brigade_cleanup(bb);
        kuda_brigade_cleanup(pass_bb);
    }

    e = kuda_bucket_eos_create(c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bb, e);
    clhy_pass_brigade(r->output_filters, bb);

    kuda_brigade_cleanup(bb);

    if (c->aborted || backend_broke) {
        return DONE;
    }

    return OK;
}

static int uwsgi_handler(request_rec *r, proxy_worker * worker,
                         proxy_server_conf * conf, char *url,
                         const char *proxyname, kuda_port_t proxyport)
{
    int status;
    int delta = 0;
    int decode_status;
    proxy_conn_rec *backend = NULL;
    kuda_pool_t *p = r->pool;
    size_t w_len;
    char server_portstr[32];
    char *u_path_info;
    kuda_uri_t *uri;

    if (clhy_cstr_casecmpn(url, UWSGI_SCHEME "://", sizeof(UWSGI_SCHEME) + 2)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, "declining URL %s", url);
        return DECLINED;
    }

    uri = kuda_palloc(r->pool, sizeof(*uri));

    /* ADD PATH_INFO */
#if CLHY_CAPI_MAGIC_AT_LEAST(20191101,6)
    w_len = strlen(worker->s->name);
#else
    w_len = strlen(worker->name);
#endif
    u_path_info = r->filename + 6 + w_len;
    if (u_path_info[0] != '/') {
        delta = 1;
    }
    decode_status = clhy_unescape_url(url + w_len - delta);
    if (decode_status) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(10100)
                      "unable to decode uri: %s", url + w_len - delta);
        return HTTP_INTERNAL_SERVER_ERROR;
    }
    kuda_table_add(r->subprocess_env, "PATH_INFO", url + w_len - delta);


    /* Create space for state information */
    status = clhy_proxy_acquire_connection(UWSGI_SCHEME, &backend, worker,
                                         r->server);
    if (status != OK) {
        goto cleanup;
    }
    backend->is_ssl = 0;

    /* Step One: Determine Who To Connect To */
    status = clhy_proxy_determine_connection(p, r, conf, worker, backend,
                                           uri, &url, proxyname, proxyport,
                                           server_portstr,
                                           sizeof(server_portstr));
    if (status != OK) {
        goto cleanup;
    }


    /* Step Two: Make the Connection */
    if (clhy_proxy_connect_backend(UWSGI_SCHEME, backend, worker, r->server)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(10101)
                      "failed to make connection to backend: %s:%u",
                      backend->hostname, backend->port);
        status = HTTP_SERVICE_UNAVAILABLE;
        goto cleanup;
    }

    /* Step Three: Create conn_rec */
    if ((status = clhy_proxy_connection_create(UWSGI_SCHEME, backend,
                                             r->connection,
                                             r->server)) != OK)
        goto cleanup;

    /* Step Four: Process the Request */
    if (((status = clhy_setup_client_block(r, REQUEST_CHUNKED_ERROR)) != OK)
        || ((status = uwsgi_send_headers(r, backend)) != OK)
        || ((status = uwsgi_send_body(r, backend)) != OK)
        || ((status = uwsgi_response(r, backend, conf)) != OK)) {
        goto cleanup;
    }

  cleanup:
    if (backend) {
        backend->close = 1;     /* always close the socket */
        clhy_proxy_release_connection(UWSGI_SCHEME, backend, r->server);
    }
    return status;
}


static void register_hooks(kuda_pool_t * p)
{
    proxy_hook_scheme_handler(uwsgi_handler, NULL, NULL, KUDA_HOOK_FIRST);
    proxy_hook_canon_handler(uwsgi_canon, NULL, NULL, KUDA_HOOK_FIRST);
}


cAPI CLHY_CAPI_DECLARE_DATA proxy_uwsgi_capi = {
    STANDARD16_CAPI_STUFF,
    NULL,                       /* create per-directory config structure */
    NULL,                       /* merge per-directory config structures */
    NULL,                       /* create per-server config structure */
    NULL,                       /* merge per-server config structures */
    NULL,                       /* command table */
    register_hooks              /* register hooks */
};

