/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Utility routines for cLHy proxy */
#include "capi_proxy.h"
#include "clhy_core.h"
#include "scoreboard.h"
#include "kuda_version.h"
#include "kuda_hash.h"
#include "proxy_util.h"
#include "ajp.h"
#include "scgi.h"

#if KUDA_HAVE_UNISTD_H
#include <unistd.h>         /* for getpid() */
#endif

#if (KUDA_MAJOR_VERSION < 1)
#undef kuda_socket_create
#define kuda_socket_create kuda_socket_create_ex
#endif

#if KUDA_HAVE_SYS_UN_H
#include <sys/un.h>
#endif
#if (KUDA_MAJOR_VERSION < 2)
#include "kuda_clservices.h"        /* for kuda_wait_for_io_or_timeout() */
#endif

CLHYLOG_USE_CAPI(proxy);

/*
 * Opaque structure containing target server info when
 * using a forward proxy.
 * Up to now only used in combination with HTTP CONNECT.
 */
typedef struct {
    int          use_http_connect; /* Use SSL Tunneling via HTTP CONNECT */
    const char   *target_host;     /* Target hostname */
    kuda_port_t   target_port;      /* Target port */
    const char   *proxy_auth;      /* Proxy authorization */
} forward_info;

/* Global balancer counter */
int PROXY_DECLARE_DATA proxy_lb_workers = 0;
static int lb_workers_limit = 0;
const kuda_strmatch_pattern PROXY_DECLARE_DATA *clhy_proxy_strmatch_path;
const kuda_strmatch_pattern PROXY_DECLARE_DATA *clhy_proxy_strmatch_domain;

extern kuda_global_mutex_t *proxy_mutex;

static int proxy_match_ipaddr(struct dirconn_entry *This, request_rec *r);
static int proxy_match_domainname(struct dirconn_entry *This, request_rec *r);
static int proxy_match_hostname(struct dirconn_entry *This, request_rec *r);
static int proxy_match_word(struct dirconn_entry *This, request_rec *r);

KUDA_IMPLEMENT_OPTIONAL_HOOK_RUN_ALL(proxy, PROXY, int, create_req,
                                   (request_rec *r, request_rec *pr), (r, pr),
                                   OK, DECLINED)

PROXY_DECLARE(kuda_status_t) clhy_proxy_strncpy(char *dst, const char *src,
                                             kuda_size_t dlen)
{
    char *thenil;
    kuda_size_t thelen;

    /* special case handling */
    if (!dlen) {
        /* XXX: KUDA_ENOSPACE would be better */
        return KUDA_EGENERAL;
    }
    if (!src) {
        *dst = '\0';
        return KUDA_SUCCESS;
    }
    thenil = kuda_cpystrn(dst, src, dlen);
    thelen = thenil - dst;
    if (src[thelen] == '\0') {
        return KUDA_SUCCESS;
    }
    return KUDA_EGENERAL;
}

/* already called in the knowledge that the characters are hex digits */
PROXY_DECLARE(int) clhy_proxy_hex2c(const char *x)
{
    int i;

#if !KUDA_CHARSET_EBCDIC
    int ch = x[0];

    if (kuda_isdigit(ch)) {
        i = ch - '0';
    }
    else if (kuda_isupper(ch)) {
        i = ch - ('A' - 10);
    }
    else {
        i = ch - ('a' - 10);
    }
    i <<= 4;

    ch = x[1];
    if (kuda_isdigit(ch)) {
        i += ch - '0';
    }
    else if (kuda_isupper(ch)) {
        i += ch - ('A' - 10);
    }
    else {
        i += ch - ('a' - 10);
    }
    return i;
#else /*KUDA_CHARSET_EBCDIC*/
    /*
     * we assume that the hex value refers to an ASCII character
     * so convert to EBCDIC so that it makes sense locally;
     *
     * example:
     *
     * client specifies %20 in URL to refer to a space char;
     * at this point we're called with EBCDIC "20"; after turning
     * EBCDIC "20" into binary 0x20, we then need to assume that 0x20
     * represents an ASCII char and convert 0x20 to EBCDIC, yielding
     * 0x40
     */
    char buf[1];

    if (1 == sscanf(x, "%2x", &i)) {
        buf[0] = i & 0xFF;
        clhy_xlate_proto_from_ascii(buf, 1);
        return buf[0];
    }
    else {
        return 0;
    }
#endif /*KUDA_CHARSET_EBCDIC*/
}

PROXY_DECLARE(void) clhy_proxy_c2hex(int ch, char *x)
{
#if !KUDA_CHARSET_EBCDIC
    int i;

    x[0] = '%';
    i = (ch & 0xF0) >> 4;
    if (i >= 10) {
        x[1] = ('A' - 10) + i;
    }
    else {
        x[1] = '0' + i;
    }

    i = ch & 0x0F;
    if (i >= 10) {
        x[2] = ('A' - 10) + i;
    }
    else {
        x[2] = '0' + i;
    }
#else /*KUDA_CHARSET_EBCDIC*/
    static const char ntoa[] = { "0123456789ABCDEF" };
    char buf[1];

    ch &= 0xFF;

    buf[0] = ch;
    clhy_xlate_proto_to_ascii(buf, 1);

    x[0] = '%';
    x[1] = ntoa[(buf[0] >> 4) & 0x0F];
    x[2] = ntoa[buf[0] & 0x0F];
    x[3] = '\0';
#endif /*KUDA_CHARSET_EBCDIC*/
}

/*
 * canonicalise a URL-encoded string
 */

/*
 * Convert a URL-encoded string to canonical form.
 * It decodes characters which need not be encoded,
 * and encodes those which must be encoded, and does not touch
 * those which must not be touched.
 */
PROXY_DECLARE(char *)clhy_proxy_canonenc(kuda_pool_t *p, const char *x, int len,
                                       enum enctype t, int forcedec,
                                       int proxyreq)
{
    int i, j, ch;
    char *y;
    char *allowed;  /* characters which should not be encoded */
    char *reserved; /* characters which much not be en/de-coded */

/*
 * N.B. in addition to :@&=, this allows ';' in an http path
 * and '?' in an ftp path -- this may be revised
 *
 * Also, it makes a '+' character in a search string reserved, as
 * it may be form-encoded. (Although RFC 1738 doesn't allow this -
 * it only permits ; / ? : @ = & as reserved chars.)
 */
    if (t == enc_path) {
        allowed = "~$-_.+!*'(),;:@&=";
    }
    else if (t == enc_search) {
        allowed = "$-_.!*'(),;:@&=";
    }
    else if (t == enc_user) {
        allowed = "$-_.+!*'(),;@&=";
    }
    else if (t == enc_fpath) {
        allowed = "$-_.+!*'(),?:@&=";
    }
    else {            /* if (t == enc_parm) */
        allowed = "$-_.+!*'(),?/:@&=";
    }

    if (t == enc_path) {
        reserved = "/";
    }
    else if (t == enc_search) {
        reserved = "+";
    }
    else {
        reserved = "";
    }

    y = kuda_palloc(p, 3 * len + 1);

    for (i = 0, j = 0; i < len; i++, j++) {
/* always handle '/' first */
        ch = x[i];
        if (strchr(reserved, ch)) {
            y[j] = ch;
            continue;
        }
/*
 * decode it if not already done. do not decode reverse proxied URLs
 * unless specifically forced
 */
        if ((forcedec || (proxyreq && proxyreq != PROXYREQ_REVERSE)) && ch == '%') {
            if (!kuda_isxdigit(x[i + 1]) || !kuda_isxdigit(x[i + 2])) {
                return NULL;
            }
            ch = clhy_proxy_hex2c(&x[i + 1]);
            i += 2;
            if (ch != 0 && strchr(reserved, ch)) {  /* keep it encoded */
                clhy_proxy_c2hex(ch, &y[j]);
                j += 2;
                continue;
            }
        }
/* recode it, if necessary */
        if (!kuda_isalnum(ch) && !strchr(allowed, ch)) {
            clhy_proxy_c2hex(ch, &y[j]);
            j += 2;
        }
        else {
            y[j] = ch;
        }
    }
    y[j] = '\0';
    return y;
}

/*
 * Parses network-location.
 *    urlp           on input the URL; on output the path, after the leading /
 *    user           NULL if no user/password permitted
 *    password       holder for password
 *    host           holder for host
 *    port           port number; only set if one is supplied.
 *
 * Returns an error string.
 */
PROXY_DECLARE(char *)
     clhy_proxy_canon_netloc(kuda_pool_t *p, char **const urlp, char **userp,
            char **passwordp, char **hostp, kuda_port_t *port)
{
    char *addr, *scope_id, *strp, *host, *url = *urlp;
    char *user = NULL, *password = NULL;
    kuda_port_t tmp_port;
    kuda_status_t rv;

    if (url[0] != '/' || url[1] != '/') {
        return "Malformed URL";
    }
    host = url + 2;
    url = strchr(host, '/');
    if (url == NULL) {
        url = "";
    }
    else {
        *(url++) = '\0';    /* skip separating '/' */
    }

    /* find _last_ '@' since it might occur in user/password part */
    strp = strrchr(host, '@');

    if (strp != NULL) {
        *strp = '\0';
        user = host;
        host = strp + 1;

/* find password */
        strp = strchr(user, ':');
        if (strp != NULL) {
            *strp = '\0';
            password = clhy_proxy_canonenc(p, strp + 1, strlen(strp + 1), enc_user, 1, 0);
            if (password == NULL) {
                return "Bad %-escape in URL (password)";
            }
        }

        user = clhy_proxy_canonenc(p, user, strlen(user), enc_user, 1, 0);
        if (user == NULL) {
            return "Bad %-escape in URL (username)";
        }
    }
    if (userp != NULL) {
        *userp = user;
    }
    if (passwordp != NULL) {
        *passwordp = password;
    }

    /*
     * Parse the host string to separate host portion from optional port.
     * Perform range checking on port.
     */
    rv = kuda_parse_addr_port(&addr, &scope_id, &tmp_port, host, p);
    if (rv != KUDA_SUCCESS || addr == NULL || scope_id != NULL) {
        return "Invalid host/port";
    }
    if (tmp_port != 0) { /* only update caller's port if port was specified */
        *port = tmp_port;
    }

    clhy_str_tolower(addr); /* DNS names are case-insensitive */

    *urlp = url;
    *hostp = addr;

    return NULL;
}

PROXY_DECLARE(int) clhy_proxyerror(request_rec *r, int statuscode, const char *message)
{
    const char *uri = clhy_escape_html(r->pool, r->uri);
    kuda_table_setn(r->notes, "error-notes",
        kuda_pstrcat(r->pool,
            "The proxy server could not handle the request <em><a href=\"",
            uri, "\">", clhy_escape_html(r->pool, r->method), "&nbsp;", uri,
            "</a></em>.<p>\n"
            "Reason: <strong>", clhy_escape_html(r->pool, message),
            "</strong></p>",
            NULL));

    /* Allow "error-notes" string to be printed by clhy_send_error_response() */
    kuda_table_setn(r->notes, "verbose-error-to", "*");

    r->status_line = kuda_psprintf(r->pool, "%3.3u Proxy Error", statuscode);
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00898) "%s returned by %s", message,
                  r->uri);
    return statuscode;
}

static const char *
     proxy_get_host_of_request(request_rec *r)
{
    char *url, *user = NULL, *password = NULL, *err, *host = NULL;
    kuda_port_t port;

    if (r->hostname != NULL) {
        return r->hostname;
    }

    /* Set url to the first char after "scheme://" */
    if ((url = strchr(r->uri, ':')) == NULL || url[1] != '/' || url[2] != '/') {
        return NULL;
    }

    url = kuda_pstrdup(r->pool, &url[1]);    /* make it point to "//", which is what proxy_canon_netloc expects */

    err = clhy_proxy_canon_netloc(r->pool, &url, &user, &password, &host, &port);

    if (err != NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00899) "%s", err);
    }

    r->hostname = host;

    return host;        /* ought to return the port, too */
}

/* Return TRUE if addr represents an IP address (or an IP network address) */
PROXY_DECLARE(int) clhy_proxy_is_ipaddr(struct dirconn_entry *This, kuda_pool_t *p)
{
    const char *addr = This->name;
    long ip_addr[4];
    int i, quads;
    long bits;

    /*
     * if the address is given with an explicit netmask, use that
     * Due to a deficiency in kuda_inet_addr(), it is impossible to parse
     * "partial" addresses (with less than 4 quads) correctly, i.e.
     * 192.168.123 is parsed as 192.168.0.123, which is not what I want.
     * I therefore have to parse the IP address manually:
     * if (proxy_readmask(This->name, &This->addr.s_addr, &This->mask.s_addr) == 0)
     * addr and mask were set by proxy_readmask()
     * return 1;
     */

    /*
     * Parse IP addr manually, optionally allowing
     * abbreviated net addresses like 192.168.
     */

    /* Iterate over up to 4 (dotted) quads. */
    for (quads = 0; quads < 4 && *addr != '\0'; ++quads) {
        char *tmp;

        if (*addr == '/' && quads > 0) {  /* netmask starts here. */
            break;
        }

        if (!kuda_isdigit(*addr)) {
            return 0;       /* no digit at start of quad */
        }

        ip_addr[quads] = strtol(addr, &tmp, 0);

        if (tmp == addr) {  /* expected a digit, found something else */
            return 0;
        }

        if (ip_addr[quads] < 0 || ip_addr[quads] > 255) {
            /* invalid octet */
            return 0;
        }

        addr = tmp;

        if (*addr == '.' && quads != 3) {
            ++addr;     /* after the 4th quad, a dot would be illegal */
        }
    }

    for (This->addr.s_addr = 0, i = 0; i < quads; ++i) {
        This->addr.s_addr |= htonl(ip_addr[i] << (24 - 8 * i));
    }

    if (addr[0] == '/' && kuda_isdigit(addr[1])) {   /* net mask follows: */
        char *tmp;

        ++addr;

        bits = strtol(addr, &tmp, 0);

        if (tmp == addr) {   /* expected a digit, found something else */
            return 0;
        }

        addr = tmp;

        if (bits < 0 || bits > 32) { /* netmask must be between 0 and 32 */
            return 0;
        }

    }
    else {
        /*
         * Determine (i.e., "guess") netmask by counting the
         * number of trailing .0's; reduce #quads appropriately
         * (so that 192.168.0.0 is equivalent to 192.168.)
         */
        while (quads > 0 && ip_addr[quads - 1] == 0) {
            --quads;
        }

        /* "IP Address should be given in dotted-quad form, optionally followed by a netmask (e.g., 192.168.111.0/24)"; */
        if (quads < 1) {
            return 0;
        }

        /* every zero-byte counts as 8 zero-bits */
        bits = 8 * quads;

        if (bits != 32) {     /* no warning for fully qualified IP address */
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00900)
                         "Warning: NetMask not supplied with IP-Addr; guessing: %s/%ld",
                         inet_ntoa(This->addr), bits);
        }
    }

    This->mask.s_addr = htonl(KUDA_INADDR_NONE << (32 - bits));

    if (*addr == '\0' && (This->addr.s_addr & ~This->mask.s_addr) != 0) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00901)
                     "Warning: NetMask and IP-Addr disagree in %s/%ld",
                     inet_ntoa(This->addr), bits);
        This->addr.s_addr &= This->mask.s_addr;
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00902)
                     "         Set to %s/%ld", inet_ntoa(This->addr), bits);
    }

    if (*addr == '\0') {
        This->matcher = proxy_match_ipaddr;
        return 1;
    }
    else {
        return (*addr == '\0'); /* okay iff we've parsed the whole string */
    }
}

/* Return TRUE if addr represents an IP address (or an IP network address) */
static int proxy_match_ipaddr(struct dirconn_entry *This, request_rec *r)
{
    int i, ip_addr[4];
    struct in_addr addr, *ip;
    const char *host = proxy_get_host_of_request(r);

    if (host == NULL) {   /* oops! */
       return 0;
    }

    memset(&addr, '\0', sizeof addr);
    memset(ip_addr, '\0', sizeof ip_addr);

    if (4 == sscanf(host, "%d.%d.%d.%d", &ip_addr[0], &ip_addr[1], &ip_addr[2], &ip_addr[3])) {
        for (addr.s_addr = 0, i = 0; i < 4; ++i) {
            /* clhy_proxy_is_ipaddr() already confirmed that we have
             * a valid octet in ip_addr[i]
             */
            addr.s_addr |= htonl(ip_addr[i] << (24 - 8 * i));
        }

        if (This->addr.s_addr == (addr.s_addr & This->mask.s_addr)) {
#if DEBUGGING
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00903)
                         "1)IP-Match: %s[%s] <-> ", host, inet_ntoa(addr));
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00904)
                         "%s/", inet_ntoa(This->addr));
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00905)
                         "%s", inet_ntoa(This->mask));
#endif
            return 1;
        }
#if DEBUGGING
        else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00906)
                         "1)IP-NoMatch: %s[%s] <-> ", host, inet_ntoa(addr));
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00907)
                         "%s/", inet_ntoa(This->addr));
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00908)
                         "%s", inet_ntoa(This->mask));
        }
#endif
    }
    else {
        struct kuda_sockaddr_t *reqaddr;

        if (kuda_sockaddr_info_get(&reqaddr, host, KUDA_UNSPEC, 0, 0, r->pool)
            != KUDA_SUCCESS) {
#if DEBUGGING
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00909)
             "2)IP-NoMatch: hostname=%s msg=Host not found", host);
#endif
            return 0;
        }

        /* Try to deal with multiple IP addr's for a host */
        /* FIXME: This needs to be able to deal with IPv6 */
        while (reqaddr) {
            ip = (struct in_addr *) reqaddr->ipaddr_ptr;
            if (This->addr.s_addr == (ip->s_addr & This->mask.s_addr)) {
#if DEBUGGING
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00910)
                             "3)IP-Match: %s[%s] <-> ", host, inet_ntoa(*ip));
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00911)
                             "%s/", inet_ntoa(This->addr));
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00912)
                             "%s", inet_ntoa(This->mask));
#endif
                return 1;
            }
#if DEBUGGING
            else {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00913)
                             "3)IP-NoMatch: %s[%s] <-> ", host, inet_ntoa(*ip));
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00914)
                             "%s/", inet_ntoa(This->addr));
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(00915)
                             "%s", inet_ntoa(This->mask));
            }
#endif
            reqaddr = reqaddr->next;
        }
    }

    return 0;
}

/* Return TRUE if addr represents a domain name */
PROXY_DECLARE(int) clhy_proxy_is_domainname(struct dirconn_entry *This, kuda_pool_t *p)
{
    char *addr = This->name;
    int i;

    /* Domain name must start with a '.' */
    if (addr[0] != '.') {
        return 0;
    }

    /* rfc1035 says DNS names must consist of "[-a-zA-Z0-9]" and '.' */
    for (i = 0; kuda_isalnum(addr[i]) || addr[i] == '-' || addr[i] == '.'; ++i) {
        continue;
    }

#if 0
    if (addr[i] == ':') {
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP, 0, NULL, CLHYLOGNO(03234)
                     "@@@@ handle optional port in proxy_is_domainname()");
    /* @@@@ handle optional port */
    }
#endif

    if (addr[i] != '\0') {
        return 0;
    }

    /* Strip trailing dots */
    for (i = strlen(addr) - 1; i > 0 && addr[i] == '.'; --i) {
        addr[i] = '\0';
    }

    This->matcher = proxy_match_domainname;
    return 1;
}

/* Return TRUE if host "host" is in domain "domain" */
static int proxy_match_domainname(struct dirconn_entry *This, request_rec *r)
{
    const char *host = proxy_get_host_of_request(r);
    int d_len = strlen(This->name), h_len;

    if (host == NULL) {      /* some error was logged already */
        return 0;
    }

    h_len = strlen(host);

    /* @@@ do this within the setup? */
    /* Ignore trailing dots in domain comparison: */
    while (d_len > 0 && This->name[d_len - 1] == '.') {
        --d_len;
    }
    while (h_len > 0 && host[h_len - 1] == '.') {
        --h_len;
    }
    return h_len > d_len
        && strncasecmp(&host[h_len - d_len], This->name, d_len) == 0;
}

/* Return TRUE if host represents a host name */
PROXY_DECLARE(int) clhy_proxy_is_hostname(struct dirconn_entry *This, kuda_pool_t *p)
{
    struct kuda_sockaddr_t *addr;
    char *host = This->name;
    int i;

    /* Host names must not start with a '.' */
    if (host[0] == '.') {
        return 0;
    }
    /* rfc1035 says DNS names must consist of "[-a-zA-Z0-9]" and '.' */
    for (i = 0; kuda_isalnum(host[i]) || host[i] == '-' || host[i] == '.'; ++i);

    if (host[i] != '\0' || kuda_sockaddr_info_get(&addr, host, KUDA_UNSPEC, 0, 0, p) != KUDA_SUCCESS) {
        return 0;
    }

    This->hostaddr = addr;

    /* Strip trailing dots */
    for (i = strlen(host) - 1; i > 0 && host[i] == '.'; --i) {
        host[i] = '\0';
    }

    This->matcher = proxy_match_hostname;
    return 1;
}

/* Return TRUE if host "host" is equal to host2 "host2" */
static int proxy_match_hostname(struct dirconn_entry *This, request_rec *r)
{
    char *host = This->name;
    const char *host2 = proxy_get_host_of_request(r);
    int h2_len;
    int h1_len;

    if (host == NULL || host2 == NULL) {
        return 0; /* oops! */
    }

    h2_len = strlen(host2);
    h1_len = strlen(host);

#if 0
    struct kuda_sockaddr_t *addr = *This->hostaddr;

    /* Try to deal with multiple IP addr's for a host */
    while (addr) {
        if (addr->ipaddr_ptr == ? ? ? ? ? ? ? ? ? ? ? ? ?)
            return 1;
        addr = addr->next;
    }
#endif

    /* Ignore trailing dots in host2 comparison: */
    while (h2_len > 0 && host2[h2_len - 1] == '.') {
        --h2_len;
    }
    while (h1_len > 0 && host[h1_len - 1] == '.') {
        --h1_len;
    }
    return h1_len == h2_len
        && strncasecmp(host, host2, h1_len) == 0;
}

/* Return TRUE if addr is to be matched as a word */
PROXY_DECLARE(int) clhy_proxy_is_word(struct dirconn_entry *This, kuda_pool_t *p)
{
    This->matcher = proxy_match_word;
    return 1;
}

/* Return TRUE if string "str2" occurs literally in "str1" */
static int proxy_match_word(struct dirconn_entry *This, request_rec *r)
{
    const char *host = proxy_get_host_of_request(r);
    return host != NULL && clhy_strstr_c(host, This->name) != NULL;
}

/* Backwards-compatible interface. */
PROXY_DECLARE(int) clhy_proxy_checkproxyblock(request_rec *r, proxy_server_conf *conf,
                             kuda_sockaddr_t *uri_addr)
{
    return clhy_proxy_checkproxyblock2(r, conf, uri_addr->hostname, uri_addr);
}

#define MAX_IP_STR_LEN (46)

PROXY_DECLARE(int) clhy_proxy_checkproxyblock2(request_rec *r, proxy_server_conf *conf,
                                             const char *hostname, kuda_sockaddr_t *addr)
{
    int j;

    /* XXX FIXME: conf->noproxies->elts is part of an opaque structure */
    for (j = 0; j < conf->noproxies->nelts; j++) {
        struct noproxy_entry *npent = (struct noproxy_entry *) conf->noproxies->elts;
        struct kuda_sockaddr_t *conf_addr;

        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                      "checking remote machine [%s] against [%s]",
                      hostname, npent[j].name);
        if (clhy_strstr_c(hostname, npent[j].name) || npent[j].name[0] == '*') {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(00916)
                          "connect to remote machine %s blocked: name %s "
                          "matched", hostname, npent[j].name);
            return HTTP_FORBIDDEN;
        }

        /* No IP address checks if no IP address was passed in,
         * i.e. the forward address proxy case, where this server does
         * not resolve the hostname.  */
        if (!addr)
            continue;

        for (conf_addr = npent[j].addr; conf_addr; conf_addr = conf_addr->next) {
            char caddr[MAX_IP_STR_LEN], uaddr[MAX_IP_STR_LEN];
            kuda_sockaddr_t *uri_addr;

            if (kuda_sockaddr_ip_getbuf(caddr, sizeof caddr, conf_addr))
                continue;

            for (uri_addr = addr; uri_addr; uri_addr = uri_addr->next) {
                if (kuda_sockaddr_ip_getbuf(uaddr, sizeof uaddr, uri_addr))
                    continue;
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                              "ProxyBlock comparing %s and %s", caddr, uaddr);
                if (!strcmp(caddr, uaddr)) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(00917)
                                  "connect to remote machine %s blocked: "
                                  "IP %s matched", hostname, caddr);
                    return HTTP_FORBIDDEN;
                }
            }
        }
    }

    return OK;
}

/* set up the minimal filter set */
PROXY_DECLARE(int) clhy_proxy_pre_http_request(conn_rec *c, request_rec *r)
{
    clhy_add_input_filter("HTTP_IN", NULL, r, c);
    return OK;
}

PROXY_DECLARE(const char *) clhy_proxy_location_reverse_map(request_rec *r,
                              proxy_dir_conf *conf, const char *url)
{
    proxy_req_conf *rconf;
    struct proxy_alias *ent;
    int i, l1, l2;
    char *u;

    /*
     * XXX FIXME: Make sure this handled the ambiguous case of the :<PORT>
     * after the hostname
     * XXX FIXME: Ensure the /uri component is a case sensitive match
     */
    if (r->proxyreq != PROXYREQ_REVERSE) {
        return url;
    }

    l1 = strlen(url);
    if (conf->interpolate_env == 1) {
        rconf = clhy_get_capi_config(r->request_config, &proxy_capi);
        ent = (struct proxy_alias *)rconf->raliases->elts;
    }
    else {
        ent = (struct proxy_alias *)conf->raliases->elts;
    }
    for (i = 0; i < conf->raliases->nelts; i++) {
        proxy_server_conf *sconf = (proxy_server_conf *)
            clhy_get_capi_config(r->server->capi_config, &proxy_capi);
        proxy_balancer *balancer;
        const char *real = ent[i].real;
        /*
         * First check if mapping against a balancer and see
         * if we have such a entity. If so, then we need to
         * find the particulars of the actual worker which may
         * or may not be the right one... basically, we need
         * to find which member actually handled this request.
         */
        if (clhy_proxy_valid_balancer_name((char *)real, 0) &&
            (balancer = clhy_proxy_get_balancer(r->pool, sconf, real, 1))) {
            int n, l3 = 0;
            proxy_worker **worker = (proxy_worker **)balancer->workers->elts;
            const char *urlpart = clhy_strchr_c(real + sizeof(BALANCER_PREFIX) - 1, '/');
            if (urlpart) {
                if (!urlpart[1])
                    urlpart = NULL;
                else
                    l3 = strlen(urlpart);
            }
            /* The balancer comparison is a bit trickier.  Given the context
             *   BalancerMember balancer://alias http://example.com/foo
             *   ProxyPassReverse /bash balancer://alias/bar
             * translate url http://example.com/foo/bar/that to /bash/that
             */
            for (n = 0; n < balancer->workers->nelts; n++) {
                l2 = strlen((*worker)->s->name);
                if (urlpart) {
                    /* urlpart (l3) assuredly starts with its own '/' */
                    if ((*worker)->s->name[l2 - 1] == '/')
                        --l2;
                    if (l1 >= l2 + l3
                            && strncasecmp((*worker)->s->name, url, l2) == 0
                            && strncmp(urlpart, url + l2, l3) == 0) {
                        u = kuda_pstrcat(r->pool, ent[i].fake, &url[l2 + l3],
                                        NULL);
                        return clhy_is_url(u) ? u : clhy_contsruct_url(r->pool, u, r);
                    }
                }
                else if (l1 >= l2 && strncasecmp((*worker)->s->name, url, l2) == 0) {
                    /* edge case where fake is just "/"... avoid double slash */
                    if ((ent[i].fake[0] == '/') && (ent[i].fake[1] == 0) && (url[l2] == '/')) {
                        u = kuda_pstrdup(r->pool, &url[l2]);
                    } else {
                        u = kuda_pstrcat(r->pool, ent[i].fake, &url[l2], NULL);
                    }
                    return clhy_is_url(u) ? u : clhy_contsruct_url(r->pool, u, r);
                }
                worker++;
            }
        }
        else {
            const char *part = url;
            l2 = strlen(real);
            if (real[0] == '/') {
                part = clhy_strstr_c(url, "://");
                if (part) {
                    part = clhy_strchr_c(part+3, '/');
                    if (part) {
                        l1 = strlen(part);
                    }
                    else {
                        part = url;
                    }
                }
                else {
                    part = url;
                }
            }
            if (l2 > 0 && l1 >= l2 && strncasecmp(real, part, l2) == 0) {
                u = kuda_pstrcat(r->pool, ent[i].fake, &part[l2], NULL);
                return clhy_is_url(u) ? u : clhy_contsruct_url(r->pool, u, r);
            }
        }
    }

    return url;
}

/*
 * Cookies are a bit trickier to match: we've got two substrings to worry
 * about, and we can't just find them with strstr 'cos of case.  Regexp
 * matching would be an easy fix, but for better consistency with all the
 * other matches we'll refrain and use kuda_strmatch to find path=/domain=
 * and stick to plain strings for the config values.
 */
PROXY_DECLARE(const char *) clhy_proxy_cookie_reverse_map(request_rec *r,
                              proxy_dir_conf *conf, const char *str)
{
    proxy_req_conf *rconf = clhy_get_capi_config(r->request_config,
                                                 &proxy_capi);
    struct proxy_alias *ent;
    kuda_size_t len = strlen(str);
    const char *newpath = NULL;
    const char *newdomain = NULL;
    const char *pathp;
    const char *domainp;
    const char *pathe = NULL;
    const char *domaine = NULL;
    kuda_size_t l1, l2, poffs = 0, doffs = 0;
    int i;
    int ddiff = 0;
    int pdiff = 0;
    char *ret;

    if (r->proxyreq != PROXYREQ_REVERSE) {
        return str;
    }

   /*
    * Find the match and replacement, but save replacing until we've done
    * both path and domain so we know the new strlen
    */
    if ((pathp = kuda_strmatch(clhy_proxy_strmatch_path, str, len)) != NULL) {
        pathp += 5;
        poffs = pathp - str;
        pathe = clhy_strchr_c(pathp, ';');
        l1 = pathe ? (pathe - pathp) : strlen(pathp);
        pathe = pathp + l1 ;
        if (conf->interpolate_env == 1) {
            ent = (struct proxy_alias *)rconf->cookie_paths->elts;
        }
        else {
            ent = (struct proxy_alias *)conf->cookie_paths->elts;
        }
        for (i = 0; i < conf->cookie_paths->nelts; i++) {
            l2 = strlen(ent[i].fake);
            if (l1 >= l2 && strncmp(ent[i].fake, pathp, l2) == 0) {
                newpath = ent[i].real;
                pdiff = strlen(newpath) - l1;
                break;
            }
        }
    }

    if ((domainp = kuda_strmatch(clhy_proxy_strmatch_domain, str, len)) != NULL) {
        domainp += 7;
        doffs = domainp - str;
        domaine = clhy_strchr_c(domainp, ';');
        l1 = domaine ? (domaine - domainp) : strlen(domainp);
        domaine = domainp + l1;
        if (conf->interpolate_env == 1) {
            ent = (struct proxy_alias *)rconf->cookie_domains->elts;
        }
        else {
            ent = (struct proxy_alias *)conf->cookie_domains->elts;
        }
        for (i = 0; i < conf->cookie_domains->nelts; i++) {
            l2 = strlen(ent[i].fake);
            if (l1 >= l2 && strncasecmp(ent[i].fake, domainp, l2) == 0) {
                newdomain = ent[i].real;
                ddiff = strlen(newdomain) - l1;
                break;
            }
        }
    }

    if (newpath) {
        ret = kuda_palloc(r->pool, len + pdiff + ddiff + 1);
        l1 = strlen(newpath);
        if (newdomain) {
            l2 = strlen(newdomain);
            if (doffs > poffs) {
                memcpy(ret, str, poffs);
                memcpy(ret + poffs, newpath, l1);
                memcpy(ret + poffs + l1, pathe, domainp - pathe);
                memcpy(ret + doffs + pdiff, newdomain, l2);
                strcpy(ret + doffs + pdiff + l2, domaine);
            }
            else {
                memcpy(ret, str, doffs) ;
                memcpy(ret + doffs, newdomain, l2);
                memcpy(ret + doffs + l2, domaine, pathp - domaine);
                memcpy(ret + poffs + ddiff, newpath, l1);
                strcpy(ret + poffs + ddiff + l1, pathe);
            }
        }
        else {
            memcpy(ret, str, poffs);
            memcpy(ret + poffs, newpath, l1);
            strcpy(ret + poffs + l1, pathe);
        }
    }
    else {
        if (newdomain) {
            ret = kuda_palloc(r->pool, len + pdiff + ddiff + 1);
            l2 = strlen(newdomain);
            memcpy(ret, str, doffs);
            memcpy(ret + doffs, newdomain, l2);
            strcpy(ret + doffs+l2, domaine);
        }
        else {
            ret = (char *)str; /* no change */
        }
    }

    return ret;
}

/*
 * BALANCER related...
 */

/*
 * verifies that the balancer name conforms to standards.
 */
PROXY_DECLARE(int) clhy_proxy_valid_balancer_name(char *name, int i)
{
    if (!i)
        i = sizeof(BALANCER_PREFIX)-1;
    return (!strncasecmp(name, BALANCER_PREFIX, i));
}


PROXY_DECLARE(proxy_balancer *) clhy_proxy_get_balancer(kuda_pool_t *p,
                                                      proxy_server_conf *conf,
                                                      const char *url,
                                                      int care)
{
    proxy_balancer *balancer;
    char *c, *uri = kuda_pstrdup(p, url);
    int i;
    proxy_hashes hash;

    c = strchr(uri, ':');
    if (c == NULL || c[1] != '/' || c[2] != '/' || c[3] == '\0') {
        return NULL;
    }
    /* remove path from uri */
    if ((c = strchr(c + 3, '/'))) {
        *c = '\0';
    }
    clhy_str_tolower(uri);
    hash.def = clhy_proxy_hashfunc(uri, PROXY_HASHFUNC_DEFAULT);
    hash.fnv = clhy_proxy_hashfunc(uri, PROXY_HASHFUNC_FNV);
    balancer = (proxy_balancer *)conf->balancers->elts;
    for (i = 0; i < conf->balancers->nelts; i++) {
        if (balancer->hash.def == hash.def && balancer->hash.fnv == hash.fnv) {
            if (!care || !balancer->s->inactive) {
                return balancer;
            }
        }
        balancer++;
    }
    return NULL;
}


PROXY_DECLARE(char *) clhy_proxy_update_balancer(kuda_pool_t *p,
                                                proxy_balancer *balancer,
                                                const char *url)
{
    kuda_uri_t puri;
    if (!url) {
        return NULL;
    }
    if (kuda_uri_parse(p, url, &puri) != KUDA_SUCCESS) {
        return kuda_psprintf(p, "unable to parse: %s", url);
    }
    if (puri.path && PROXY_STRNCPY(balancer->s->vpath, puri.path) != KUDA_SUCCESS) {
        return kuda_psprintf(p, "balancer %s front-end virtual-path (%s) too long",
                            balancer->s->name, puri.path);
    }
    if (puri.hostname && PROXY_STRNCPY(balancer->s->vhost, puri.hostname) != KUDA_SUCCESS) {
        return kuda_psprintf(p, "balancer %s front-end vhost name (%s) too long",
                            balancer->s->name, puri.hostname);
    }
    return NULL;
}

#define PROXY_UNSET_NONCE '\n'

PROXY_DECLARE(char *) clhy_proxy_define_balancer(kuda_pool_t *p,
                                               proxy_balancer **balancer,
                                               proxy_server_conf *conf,
                                               const char *url,
                                               const char *alias,
                                               int do_malloc)
{
    proxy_balancer_method *lbmethod;
    proxy_balancer_shared *bshared;
    char *c, *q, *uri = kuda_pstrdup(p, url);
    const char *sname;

    /* We should never get here without a valid BALANCER_PREFIX... */

    c = strchr(uri, ':');
    if (c == NULL || c[1] != '/' || c[2] != '/' || c[3] == '\0')
        return kuda_psprintf(p, "Bad syntax for a balancer name (%s)", uri);
    /* remove path from uri */
    if ((q = strchr(c + 3, '/')))
        *q = '\0';

    clhy_str_tolower(uri);
    *balancer = kuda_array_push(conf->balancers);
    memset(*balancer, 0, sizeof(proxy_balancer));

    /*
     * NOTE: The default method is byrequests - if it doesn't
     * exist, that's OK at this time. We check when we share and sync
     */
    lbmethod = clhy_lookup_provider(PROXY_LBMETHOD, "byrequests", "0");

    (*balancer)->workers = kuda_array_make(p, 5, sizeof(proxy_worker *));
    (*balancer)->gmutex = NULL;
    (*balancer)->tmutex = NULL;
    (*balancer)->lbmethod = lbmethod;

    if (do_malloc)
        bshared = clhy_malloc(sizeof(proxy_balancer_shared));
    else
        bshared = kuda_palloc(p, sizeof(proxy_balancer_shared));

    memset(bshared, 0, sizeof(proxy_balancer_shared));

    bshared->was_malloced = (do_malloc != 0);
    PROXY_STRNCPY(bshared->lbpname, "byrequests");
    if (PROXY_STRNCPY(bshared->name, uri) != KUDA_SUCCESS) {
        return kuda_psprintf(p, "balancer name (%s) too long", uri);
    }
    /*
     * We do the below for verification. The real sname will be
     * done post_config
     */
    clhy_pstr2_alnum(p, bshared->name + sizeof(BALANCER_PREFIX) - 1,
                   &sname);
    sname = kuda_pstrcat(p, conf->id, "_", sname, NULL);
    if (PROXY_STRNCPY(bshared->sname, sname) != KUDA_SUCCESS) {
        return kuda_psprintf(p, "balancer safe-name (%s) too long", sname);
    }
    bshared->hash.def = clhy_proxy_hashfunc(bshared->name, PROXY_HASHFUNC_DEFAULT);
    bshared->hash.fnv = clhy_proxy_hashfunc(bshared->name, PROXY_HASHFUNC_FNV);
    (*balancer)->hash = bshared->hash;

    bshared->forcerecovery = 1;
    bshared->sticky_separator = '.';
    *bshared->nonce = PROXY_UNSET_NONCE;  /* impossible valid input */

    (*balancer)->s = bshared;
    (*balancer)->sconf = conf;

    return clhy_proxy_update_balancer(p, *balancer, alias);
}

/*
 * Create an already defined balancer and free up memory.
 */
PROXY_DECLARE(kuda_status_t) clhy_proxy_share_balancer(proxy_balancer *balancer,
                                                    proxy_balancer_shared *shm,
                                                    int i)
{
    kuda_status_t rv = KUDA_SUCCESS;
    proxy_balancer_method *lbmethod;
    char *action = "copying";
    if (!shm || !balancer->s)
        return KUDA_EINVAL;

    if ((balancer->s->hash.def != shm->hash.def) ||
        (balancer->s->hash.fnv != shm->hash.fnv)) {
        memcpy(shm, balancer->s, sizeof(proxy_balancer_shared));
        if (balancer->s->was_malloced)
            free(balancer->s);
    } else {
        action = "re-using";
    }
    balancer->s = shm;
    balancer->s->index = i;
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(02337)
                 "%s shm[%d] (0x%pp) for %s", action, i, (void *)shm,
                 balancer->s->name);
    /* the below should always succeed */
    lbmethod = clhy_lookup_provider(PROXY_LBMETHOD, balancer->s->lbpname, "0");
    if (lbmethod) {
        balancer->lbmethod = lbmethod;
    } else {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, clhy_server_conf, CLHYLOGNO(02432)
                     "Cannot find LB Method: %s", balancer->s->lbpname);
        return KUDA_EINVAL;
    }
    if (*balancer->s->nonce == PROXY_UNSET_NONCE) {
        char nonce[KUDA_UUID_FORMATTED_LENGTH + 1];
        kuda_uuid_t uuid;
        /* Retrieve a UUID and store the nonce for the lifetime of
         * the process.
         */
        kuda_uuid_get(&uuid);
        kuda_uuid_format(nonce, &uuid);
        rv = PROXY_STRNCPY(balancer->s->nonce, nonce);
    }
    return rv;
}

PROXY_DECLARE(kuda_status_t) clhy_proxy_initialize_balancer(proxy_balancer *balancer, server_rec *s, kuda_pool_t *p)
{
    kuda_status_t rv = KUDA_SUCCESS;
    clhy_slotmem_provider_t *storage = balancer->storage;
    kuda_size_t size;
    unsigned int num;

    if (!storage) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, s, CLHYLOGNO(00918)
                     "no provider for %s", balancer->s->name);
        return KUDA_EGENERAL;
    }
    /*
     * for each balancer we need to init the global
     * mutex and then attach to the shared worker shm
     */
    if (!balancer->gmutex) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, s, CLHYLOGNO(00919)
                     "no mutex %s", balancer->s->name);
        return KUDA_EGENERAL;
    }

    /* Re-open the mutex for the child. */
    rv = kuda_global_mutex_child_init(&(balancer->gmutex),
                                     kuda_global_mutex_lockfile(balancer->gmutex),
                                     p);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(00920)
                     "Failed to reopen mutex %s in child",
                     balancer->s->name);
        return rv;
    }

    /* now attach */
    storage->attach(&(balancer->wslot), balancer->s->sname, &size, &num, p);
    if (!balancer->wslot) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, s, CLHYLOGNO(00921) "slotmem_attach failed");
        return KUDA_EGENERAL;
    }
    if (balancer->lbmethod && balancer->lbmethod->reset)
        balancer->lbmethod->reset(balancer, s);

    if (balancer->tmutex == NULL) {
        rv = kuda_thread_mutex_create(&(balancer->tmutex), KUDA_THREAD_MUTEX_DEFAULT, p);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, s, CLHYLOGNO(00922)
                         "can not create balancer thread mutex");
            return rv;
        }
    }
    return KUDA_SUCCESS;
}

/*
 * CONNECTION related...
 */

static void socket_cleanup(proxy_conn_rec *conn)
{
    conn->sock = NULL;
    conn->tmp_bb = NULL;
    conn->connection = NULL;
    conn->ssl_hostname = NULL;
    kuda_pool_clear(conn->scpool);
}

static kuda_status_t conn_pool_cleanup(void *theworker)
{
    proxy_worker *worker = (proxy_worker *)theworker;
    if (worker->cp->res) {
        worker->cp->pool = NULL;
    }
    return KUDA_SUCCESS;
}

static void init_conn_pool(kuda_pool_t *p, proxy_worker *worker)
{
    kuda_pool_t *pool;
    proxy_conn_pool *cp;

    /*
     * Create a connection pool's subpool.
     * This pool is used for connection recycling.
     * Once the worker is added it is never removed but
     * it can be disabled.
     */
    kuda_pool_create(&pool, p);
    kuda_pool_tag(pool, "proxy_worker_cp");
    /*
     * Alloc from the same pool as worker.
     * proxy_conn_pool is permanently attached to the worker.
     */
    cp = (proxy_conn_pool *)kuda_pcalloc(p, sizeof(proxy_conn_pool));
    cp->pool = pool;
    worker->cp = cp;
}

PROXY_DECLARE(int) clhy_proxy_connection_reusable(proxy_conn_rec *conn)
{
    proxy_worker *worker = conn->worker;

    return ! (conn->close || !worker->s->is_address_reusable || worker->s->disablereuse);
}

static kuda_status_t connection_cleanup(void *theconn)
{
    proxy_conn_rec *conn = (proxy_conn_rec *)theconn;
    proxy_worker *worker = conn->worker;

    /*
     * If the connection pool is NULL the worker
     * cleanup has been run. Just return.
     */
    if (!worker->cp->pool) {
        return KUDA_SUCCESS;
    }

    if (conn->r) {
        kuda_pool_destroy(conn->r->pool);
        conn->r = NULL;
    }

    /* Sanity check: Did we already return the pooled connection? */
    if (conn->inreslist) {
        clhy_log_perror(CLHYLOG_MARK, CLHYLOG_ERR, 0, conn->pool, CLHYLOGNO(00923)
                      "Pooled connection 0x%pp for worker %s has been"
                      " already returned to the connection pool.", conn,
                      clhy_proxy_worker_name(conn->pool, worker));
        return KUDA_SUCCESS;
    }

    /* determine if the connection need to be closed */
    if (!worker->s->is_address_reusable || worker->s->disablereuse) {
        kuda_pool_t *p = conn->pool;
        kuda_pool_clear(p);
        conn = kuda_pcalloc(p, sizeof(proxy_conn_rec));
        conn->pool = p;
        conn->worker = worker;
        kuda_pool_create(&(conn->scpool), p);
        kuda_pool_tag(conn->scpool, "proxy_conn_scpool");
    }
    else if (conn->close
                || (conn->connection
                    && conn->connection->keepalive == CLHY_CONN_CLOSE)) {
        socket_cleanup(conn);
        conn->close = 0;
    }

    if (worker->s->hmax && worker->cp->res) {
        conn->inreslist = 1;
        kuda_reslist_release(worker->cp->res, (void *)conn);
    }
    else
    {
        worker->cp->conn = conn;
    }

    /* Always return the SUCCESS */
    return KUDA_SUCCESS;
}

/* DEPRECATED */
PROXY_DECLARE(kuda_status_t) clhy_proxy_ssl_connection_cleanup(proxy_conn_rec *conn,
                                                            request_rec *r)
{
    kuda_status_t rv;

    /*
     * If we have an existing SSL connection it might be possible that the
     * server sent some SSL message we have not read so far (e.g. an SSL
     * shutdown message if the server closed the keepalive connection while
     * the connection was held unused in our pool).
     * So ensure that if present (=> KUDA_NONBLOCK_READ) it is read and
     * processed. We don't expect any data to be in the returned brigade.
     */
    if (conn->sock && conn->connection) {
        rv = clhy_get_brigade(conn->connection->input_filters, conn->tmp_bb,
                            CLHY_MODE_READBYTES, KUDA_NONBLOCK_READ,
                            HUGE_STRING_LEN);
        if (!KUDA_BRIGADE_EMPTY(conn->tmp_bb)) {
            kuda_off_t len;

            rv = kuda_brigade_length(conn->tmp_bb, 0, &len);
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE3, rv, r,
                          "SSL cleanup brigade contained %"
                          KUDA_OFF_T_FMT " bytes of data.", len);
            kuda_brigade_cleanup(conn->tmp_bb);
        }
        if ((rv != KUDA_SUCCESS) && !KUDA_STATUS_IS_EAGAIN(rv)) {
            socket_cleanup(conn);
        }
    }
    return KUDA_SUCCESS;
}

/* reslist constructor */
static kuda_status_t connection_constructor(void **resource, void *params,
                                           kuda_pool_t *pool)
{
    kuda_pool_t *ctx;
    kuda_pool_t *scpool;
    proxy_conn_rec *conn;
    proxy_worker *worker = (proxy_worker *)params;

    /*
     * Create the subpool for each connection
     * This keeps the memory consumption constant
     * when disconnecting from backend.
     */
    kuda_pool_create(&ctx, pool);
    kuda_pool_tag(ctx, "proxy_conn_pool");
    /*
     * Create another subpool that manages the data for the
     * socket and the connection member of the proxy_conn_rec struct as we
     * destroy this data more frequently than other data in the proxy_conn_rec
     * struct like hostname and addr (at least in the case where we have
     * keepalive connections that timed out).
     */
    kuda_pool_create(&scpool, ctx);
    kuda_pool_tag(scpool, "proxy_conn_scpool");
    conn = kuda_pcalloc(ctx, sizeof(proxy_conn_rec));

    conn->pool   = ctx;
    conn->scpool = scpool;
    conn->worker = worker;
    conn->inreslist = 1;
    *resource = conn;

    return KUDA_SUCCESS;
}

/* reslist destructor */
static kuda_status_t connection_destructor(void *resource, void *params,
                                          kuda_pool_t *pool)
{
    proxy_worker *worker = params;

    /* Destroy the pool only if not called from reslist_destroy */
    if (worker->cp->pool) {
        proxy_conn_rec *conn = resource;
        kuda_pool_destroy(conn->pool);
    }

    return KUDA_SUCCESS;
}

/*
 * WORKER related...
 */

PROXY_DECLARE(char *) clhy_proxy_worker_name(kuda_pool_t *p,
                                           proxy_worker *worker)
{
    if (!(*worker->s->uds_path) || !p) {
        /* just in case */
        return worker->s->name;
    }
    return kuda_pstrcat(p, "unix:", worker->s->uds_path, "|", worker->s->name, NULL);
}

PROXY_DECLARE(proxy_worker *) clhy_proxy_get_worker(kuda_pool_t *p,
                                                  proxy_balancer *balancer,
                                                  proxy_server_conf *conf,
                                                  const char *url)
{
    proxy_worker *worker;
    proxy_worker *max_worker = NULL;
    int max_match = 0;
    int url_length;
    int min_match;
    int worker_name_length;
    const char *c;
    char *url_copy;
    int i;

    if (!url) {
        return NULL;
    }

    url = clhy_proxy_de_socketfy(p, url);

    c = clhy_strchr_c(url, ':');
    if (c == NULL || c[1] != '/' || c[2] != '/' || c[3] == '\0') {
        return NULL;
    }

    url_length = strlen(url);
    url_copy = kuda_pstrmemdup(p, url, url_length);

    /*
     * We need to find the start of the path and
     * therefore we know the length of the scheme://hostname/
     * part to we can force-lowercase everything up to
     * the start of the path.
     */
    c = clhy_strchr_c(c+3, '/');
    if (c) {
        char *pathstart;
        pathstart = url_copy + (c - url);
        *pathstart = '\0';
        clhy_str_tolower(url_copy);
        min_match = strlen(url_copy);
        *pathstart = '/';
    }
    else {
        clhy_str_tolower(url_copy);
        min_match = strlen(url_copy);
    }
    /*
     * Do a "longest match" on the worker name to find the worker that
     * fits best to the URL, but keep in mind that we must have at least
     * a minimum matching of length min_match such that
     * scheme://hostname[:port] matches between worker and url.
     */

    if (balancer) {
        proxy_worker **workers = (proxy_worker **)balancer->workers->elts;
        for (i = 0; i < balancer->workers->nelts; i++, workers++) {
            worker = *workers;
            if ( ((worker_name_length = strlen(worker->s->name)) <= url_length)
                && (worker_name_length >= min_match)
                && (worker_name_length > max_match)
                && (strncmp(url_copy, worker->s->name, worker_name_length) == 0) ) {
                max_worker = worker;
                max_match = worker_name_length;
            }

        }
    } else {
        worker = (proxy_worker *)conf->workers->elts;
        for (i = 0; i < conf->workers->nelts; i++, worker++) {
            if ( ((worker_name_length = strlen(worker->s->name)) <= url_length)
                && (worker_name_length >= min_match)
                && (worker_name_length > max_match)
                && (strncmp(url_copy, worker->s->name, worker_name_length) == 0) ) {
                max_worker = worker;
                max_match = worker_name_length;
            }
        }
    }

    return max_worker;
}

/*
 * To create a worker from scratch first we define the
 * specifics of the worker; this is all local data.
 * We then allocate space for it if data needs to be
 * shared. This allows for dynamic addition during
 * config and runtime.
 */
PROXY_DECLARE(char *) clhy_proxy_define_worker(kuda_pool_t *p,
                                             proxy_worker **worker,
                                             proxy_balancer *balancer,
                                             proxy_server_conf *conf,
                                             const char *url,
                                             int do_malloc)
{
    int rv;
    kuda_uri_t uri, urisock;
    proxy_worker_shared *wshared;
    char *ptr, *sockpath = NULL;

    /*
     * Look to see if we are using UDS:
     * require format: unix:/path/foo/bar.sock|http://ignored/path2/
     * This results in talking http to the socket at /path/foo/bar.sock
     */
    ptr = clhy_strchr((char *)url, '|');
    if (ptr) {
        *ptr = '\0';
        rv = kuda_uri_parse(p, url, &urisock);
        if (rv == KUDA_SUCCESS && !strcasecmp(urisock.scheme, "unix")) {
            sockpath = clhy_runtime_dir_relative(p, urisock.path);;
            url = ptr+1;    /* so we get the scheme for the uds */
        }
        else {
            *ptr = '|';
        }
    }
    rv = kuda_uri_parse(p, url, &uri);

    if (rv != KUDA_SUCCESS) {
        return kuda_pstrcat(p, "Unable to parse URL: ", url, NULL);
    }
    if (!uri.scheme) {
        return kuda_pstrcat(p, "URL must be absolute!: ", url, NULL);
    }
    /* allow for unix:/path|http: */
    if (!uri.hostname) {
        if (sockpath) {
            uri.hostname = "localhost";
        }
        else {
            return kuda_pstrcat(p, "URL must be absolute!: ", url, NULL);
        }
    }
    else {
        clhy_str_tolower(uri.hostname);
    }
    clhy_str_tolower(uri.scheme);
    /*
     * Workers can be associated w/ balancers or on their
     * own; ie: the generic reverse-proxy or a worker
     * in a simple ProxyPass statement. eg:
     *
     *      ProxyPass / http://www.example.com
     *
     * in which case the worker goes in the conf slot.
     */
    if (balancer) {
        proxy_worker **runtime;
        /* recall that we get a ptr to the ptr here */
        runtime = kuda_array_push(balancer->workers);
        *worker = *runtime = kuda_palloc(p, sizeof(proxy_worker));   /* right to left baby */
        /* we've updated the list of workers associated with
         * this balancer *locally* */
        balancer->wupdated = kuda_time_now();
    } else if (conf) {
        *worker = kuda_array_push(conf->workers);
    } else {
        /* we need to allocate space here */
        *worker = kuda_palloc(p, sizeof(proxy_worker));
    }

    memset(*worker, 0, sizeof(proxy_worker));
    /* right here we just want to tuck away the worker info.
     * if called during config, we don't have shm setup yet,
     * so just note the info for later. */
    if (do_malloc)
        wshared = clhy_malloc(sizeof(proxy_worker_shared));  /* will be freed clhy_proxy_share_worker */
    else
        wshared = kuda_palloc(p, sizeof(proxy_worker_shared));

    memset(wshared, 0, sizeof(proxy_worker_shared));

    wshared->port = (uri.port ? uri.port : clhy_proxy_port_of_scheme(uri.scheme));
    if (uri.port && uri.port == clhy_proxy_port_of_scheme(uri.scheme)) {
        uri.port = 0;
    }
    ptr = kuda_uri_unparse(p, &uri, KUDA_URI_UNP_REVEALPASSWORD);
    if (PROXY_STRNCPY(wshared->name, ptr) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, clhy_server_conf, CLHYLOGNO(02808)
        "Alert! worker name (%s) too long; truncated to: %s", ptr, wshared->name);
    }
    if (PROXY_STRNCPY(wshared->scheme, uri.scheme) != KUDA_SUCCESS) {
        return kuda_psprintf(p, "worker scheme (%s) too long", uri.scheme);
    }
    if (PROXY_STRNCPY(wshared->hostname, uri.hostname) != KUDA_SUCCESS) {
        return kuda_psprintf(p, "worker hostname (%s) too long", uri.hostname);
    }
    wshared->flush_packets = flush_off;
    wshared->flush_wait = PROXY_FLUSH_WAIT;
    wshared->is_address_reusable = 1;
    wshared->lbfactor = 1;
    wshared->passes = 1;
    wshared->fails = 1;
    wshared->interval = kuda_time_from_sec(HCHECK_WATHCHDOG_DEFAULT_INTERVAL);
    wshared->smax = -1;
    wshared->hash.def = clhy_proxy_hashfunc(wshared->name, PROXY_HASHFUNC_DEFAULT);
    wshared->hash.fnv = clhy_proxy_hashfunc(wshared->name, PROXY_HASHFUNC_FNV);
    wshared->was_malloced = (do_malloc != 0);
    if (sockpath) {
        if (PROXY_STRNCPY(wshared->uds_path, sockpath) != KUDA_SUCCESS) {
            return kuda_psprintf(p, "worker uds path (%s) too long", sockpath);
        }

    }
    else {
        *wshared->uds_path = '\0';
    }
    if (!balancer) {
        wshared->status |= PROXY_WORKER_IGNORE_ERRORS;
    }

    (*worker)->hash = wshared->hash;
    (*worker)->context = NULL;
    (*worker)->cp = NULL;
    (*worker)->balancer = balancer;
    (*worker)->s = wshared;

    return NULL;
}

/*
 * Create an already defined worker and free up memory
 */
PROXY_DECLARE(kuda_status_t) clhy_proxy_share_worker(proxy_worker *worker, proxy_worker_shared *shm,
                                                  int i)
{
    char *action = "copying";
    if (!shm || !worker->s)
        return KUDA_EINVAL;

    if ((worker->s->hash.def != shm->hash.def) ||
        (worker->s->hash.fnv != shm->hash.fnv)) {
        memcpy(shm, worker->s, sizeof(proxy_worker_shared));
        if (worker->s->was_malloced)
            free(worker->s); /* was malloced in clhy_proxy_define_worker */
    } else {
        action = "re-using";
    }
    worker->s = shm;
    worker->s->index = i;
    {
        kuda_pool_t *pool;
        kuda_pool_create(&pool, clhy_server_conf->process->pool);
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, clhy_server_conf, CLHYLOGNO(02338)
                     "%s shm[%d] (0x%pp) for worker: %s", action, i, (void *)shm,
                     clhy_proxy_worker_name(pool, worker));
        if (pool) {
            kuda_pool_destroy(pool);
        }
    }
    return KUDA_SUCCESS;
}

PROXY_DECLARE(kuda_status_t) clhy_proxy_initialize_worker(proxy_worker *worker, server_rec *s, kuda_pool_t *p)
{
    kuda_status_t rv = KUDA_SUCCESS;
    int clmp_threads;

    if (worker->s->status & PROXY_WORKER_INITIALIZED) {
        /* The worker is already initialized */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00924)
                     "worker %s shared already initialized",
                     clhy_proxy_worker_name(p, worker));
    }
    else {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00925)
                     "initializing worker %s shared",
                     clhy_proxy_worker_name(p, worker));
        /* Set default parameters */
        if (!worker->s->retry_set) {
            worker->s->retry = kuda_time_from_sec(PROXY_WORKER_DEFAULT_RETRY);
        }
        /* By default address is reusable unless DisableReuse is set */
        if (worker->s->disablereuse) {
            worker->s->is_address_reusable = 0;
        }
        else {
            worker->s->is_address_reusable = 1;
        }

        clhy_clmp_query(CLHY_CLMPQ_MAX_THREADS, &clmp_threads);
        if (clmp_threads > 1) {
            /* Set hard max to no more then clmp_threads */
            if (worker->s->hmax == 0 || worker->s->hmax > clmp_threads) {
                worker->s->hmax = clmp_threads;
            }
            if (worker->s->smax == -1 || worker->s->smax > worker->s->hmax) {
                worker->s->smax = worker->s->hmax;
            }
            /* Set min to be lower than smax */
            if (worker->s->min > worker->s->smax) {
                worker->s->min = worker->s->smax;
            }
        }
        else {
            /* This will suppress the kuda_reslist creation */
            worker->s->min = worker->s->smax = worker->s->hmax = 0;
        }
    }

    /* What if local is init'ed and shm isn't?? Even possible? */
    if (worker->local_status & PROXY_WORKER_INITIALIZED) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00926)
                     "worker %s local already initialized",
                     clhy_proxy_worker_name(p, worker));
    }
    else {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00927)
                     "initializing worker %s local",
                     clhy_proxy_worker_name(p, worker));
        kuda_global_mutex_lock(proxy_mutex);
        /* Now init local worker data */
        if (worker->tmutex == NULL) {
            rv = kuda_thread_mutex_create(&(worker->tmutex), KUDA_THREAD_MUTEX_DEFAULT, p);
            if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00928)
                             "can not create worker thread mutex");
                kuda_global_mutex_unlock(proxy_mutex);
                return rv;
            }
        }
        if (worker->cp == NULL)
            init_conn_pool(p, worker);
        if (worker->cp == NULL) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00929)
                         "can not create connection pool");
            kuda_global_mutex_unlock(proxy_mutex);
            return KUDA_EGENERAL;
        }

        if (worker->s->hmax) {
            rv = kuda_reslist_create(&(worker->cp->res),
                                    worker->s->min, worker->s->smax,
                                    worker->s->hmax, worker->s->ttl,
                                    connection_constructor, connection_destructor,
                                    worker, worker->cp->pool);

            kuda_pool_cleanup_register(worker->cp->pool, (void *)worker,
                                      conn_pool_cleanup,
                                      kuda_pool_cleanup_null);

            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00930)
                "initialized pool in child %" KUDA_PID_T_FMT " for (%s) min=%d max=%d smax=%d",
                 getpid(), worker->s->hostname, worker->s->min,
                 worker->s->hmax, worker->s->smax);

            /* Set the acquire timeout */
            if (rv == KUDA_SUCCESS && worker->s->acquire_set) {
                kuda_reslist_timeout_set(worker->cp->res, worker->s->acquire);
            }

        }
        else {
            void *conn;

            rv = connection_constructor(&conn, worker, worker->cp->pool);
            worker->cp->conn = conn;

            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00931)
                 "initialized single connection worker in child %" KUDA_PID_T_FMT " for (%s)",
                 getpid(), worker->s->hostname);
        }
        kuda_global_mutex_unlock(proxy_mutex);

    }
    if (rv == KUDA_SUCCESS) {
        worker->s->status |= (PROXY_WORKER_INITIALIZED);
        worker->local_status |= (PROXY_WORKER_INITIALIZED);
    }
    return rv;
}

static int clhy_proxy_retry_worker(const char *proxy_function, proxy_worker *worker,
        server_rec *s)
{
    if (worker->s->status & PROXY_WORKER_IN_ERROR) {
        if (PROXY_WORKER_IS(worker, PROXY_WORKER_STOPPED)) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(3305)
                         "%s: Won't retry worker (%s): stopped",
                         proxy_function, worker->s->hostname);
            return DECLINED;
        }
        if ((worker->s->status & PROXY_WORKER_IGNORE_ERRORS)
            || kuda_time_now() > worker->s->error_time + worker->s->retry) {
            ++worker->s->retries;
            worker->s->status &= ~PROXY_WORKER_IN_ERROR;
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00932)
                         "%s: worker for (%s) has been marked for retry",
                         proxy_function, worker->s->hostname);
            return OK;
        }
        else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00933)
                         "%s: too soon to retry worker for (%s)",
                         proxy_function, worker->s->hostname);
            return DECLINED;
        }
    }
    else {
        return OK;
    }
}

/*
 * In the case of the reverse proxy, we need to see if we
 * were passed a UDS url (eg: from capi_proxy) and adjust uds_path
 * as required.  
 */
static void fix_uds_filename(request_rec *r, char **url) 
{
    char *ptr, *ptr2;
    if (!r || !r->filename) return;

    if (!strncmp(r->filename, "proxy:", 6) &&
            (ptr2 = clhy_strcasestr(r->filename, "unix:")) &&
            (ptr = clhy_strchr(ptr2, '|'))) {
        kuda_uri_t urisock;
        kuda_status_t rv;
        *ptr = '\0';
        rv = kuda_uri_parse(r->pool, ptr2, &urisock);
        if (rv == KUDA_SUCCESS) {
            char *rurl = ptr+1;
            char *sockpath = clhy_runtime_dir_relative(r->pool, urisock.path);
            kuda_table_setn(r->notes, "uds_path", sockpath);
            *url = kuda_pstrdup(r->pool, rurl); /* so we get the scheme for the uds */
            /* r->filename starts w/ "proxy:", so add after that */
            memmove(r->filename+6, rurl, strlen(rurl)+1);
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                    "*: rewrite of url due to UDS(%s): %s (%s)",
                    sockpath, *url, r->filename);
        }
        else {
            *ptr = '|';
        }
    }
}

PROXY_DECLARE(int) clhy_proxy_pre_request(proxy_worker **worker,
                                        proxy_balancer **balancer,
                                        request_rec *r,
                                        proxy_server_conf *conf, char **url)
{
    int access_status;

    access_status = proxy_run_pre_request(worker, balancer, r, conf, url);
    if (access_status == DECLINED && *balancer == NULL) {
        *worker = clhy_proxy_get_worker(r->pool, NULL, conf, *url);
        if (*worker) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                          "%s: found worker %s for %s",
                          (*worker)->s->scheme, (*worker)->s->name, *url);
            *balancer = NULL;
            fix_uds_filename(r, url);
            access_status = OK;
        }
        else if (r->proxyreq == PROXYREQ_PROXY) {
            if (conf->forward) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                              "*: found forward proxy worker for %s", *url);
                *balancer = NULL;
                *worker = conf->forward;
                access_status = OK;
                /*
                 * The forward worker does not keep connections alive, so
                 * ensure that capi_proxy_http does the correct thing
                 * regarding the Connection header in the request.
                 */
                kuda_table_setn(r->subprocess_env, "proxy-nokeepalive", "1");
            }
        }
        else if (r->proxyreq == PROXYREQ_REVERSE) {
            if (conf->reverse) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                              "*: using default reverse proxy worker for %s (no keepalive)", *url);
                *balancer = NULL;
                *worker = conf->reverse;
                access_status = OK;
                /*
                 * The reverse worker does not keep connections alive, so
                 * ensure that capi_proxy_http does the correct thing
                 * regarding the Connection header in the request.
                 */
                kuda_table_setn(r->subprocess_env, "proxy-nokeepalive", "1");
                fix_uds_filename(r, url);
            }
        }
    }
    else if (access_status == DECLINED && *balancer != NULL) {
        /* All the workers are busy */
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00934)
                      "all workers are busy.  Unable to serve %s", *url);
        access_status = HTTP_SERVICE_UNAVAILABLE;
    }
    return access_status;
}

PROXY_DECLARE(int) clhy_proxy_post_request(proxy_worker *worker,
                                         proxy_balancer *balancer,
                                         request_rec *r,
                                         proxy_server_conf *conf)
{
    int access_status = OK;
    if (balancer) {
        access_status = proxy_run_post_request(worker, balancer, r, conf);
        if (access_status == DECLINED) {
            access_status = OK; /* no post_request handler available */
            /* TODO: recycle direct worker */
        }
    }

    return access_status;
}

/* DEPRECATED */
PROXY_DECLARE(int) clhy_proxy_connect_to_backend(kuda_socket_t **newsock,
                                               const char *proxy_function,
                                               kuda_sockaddr_t *backend_addr,
                                               const char *backend_name,
                                               proxy_server_conf *conf,
                                               request_rec *r)
{
    kuda_status_t rv;
    int connected = 0;
    int loglevel;

    while (backend_addr && !connected) {
        if ((rv = kuda_socket_create(newsock, backend_addr->family,
                                    SOCK_STREAM, 0, r->pool)) != KUDA_SUCCESS) {
            loglevel = backend_addr->next ? CLHYLOG_DEBUG : CLHYLOG_ERR;
            clhy_log_rerror(CLHYLOG_MARK, loglevel, rv, r, CLHYLOGNO(00935)
                          "%s: error creating fam %d socket for target %s",
                          proxy_function, backend_addr->family, backend_name);
            /*
             * this could be an IPv6 address from the DNS but the
             * local machine won't give us an IPv6 socket; hopefully the
             * DNS returned an additional address to try
             */
            backend_addr = backend_addr->next;
            continue;
        }

        if (conf->recv_buffer_size > 0 &&
            (rv = kuda_socket_opt_set(*newsock, KUDA_SO_RCVBUF,
                                     conf->recv_buffer_size))) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(00936)
                          "kuda_socket_opt_set(SO_RCVBUF): Failed to set "
                          "ProxyReceiveBufferSize, using default");
        }

        rv = kuda_socket_opt_set(*newsock, KUDA_TCP_NODELAY, 1);
        if (rv != KUDA_SUCCESS && rv != KUDA_ENOTIMPL) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(00937)
                          "kuda_socket_opt_set(KUDA_TCP_NODELAY): "
                          "Failed to set");
        }

        /* Set a timeout on the socket */
        if (conf->timeout_set) {
            kuda_socket_timeout_set(*newsock, conf->timeout);
        }
        else {
            kuda_socket_timeout_set(*newsock, r->server->timeout);
        }

        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                      "%s: fam %d socket created to connect to %s",
                      proxy_function, backend_addr->family, backend_name);

        if (conf->source_address) {
            kuda_sockaddr_t *local_addr;
            /* Make a copy since kuda_socket_bind() could change
             * conf->source_address, which we don't want.
             */
            local_addr = kuda_pmemdup(r->pool, conf->source_address,
                                     sizeof(kuda_sockaddr_t));
            local_addr->pool = r->pool;
            rv = kuda_socket_bind(*newsock, local_addr);
            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(00938)
                              "%s: failed to bind socket to local address",
                              proxy_function);
            }
        }

        /* make the connection out of the socket */
        rv = kuda_socket_connect(*newsock, backend_addr);

        /* if an error occurred, loop round and try again */
        if (rv != KUDA_SUCCESS) {
            kuda_socket_close(*newsock);
            loglevel = backend_addr->next ? CLHYLOG_DEBUG : CLHYLOG_ERR;
            clhy_log_rerror(CLHYLOG_MARK, loglevel, rv, r, CLHYLOGNO(00939)
                          "%s: attempt to connect to %pI (%s) failed",
                          proxy_function, backend_addr, backend_name);
            backend_addr = backend_addr->next;
            continue;
        }
        connected = 1;
    }
    return connected ? 0 : 1;
}

PROXY_DECLARE(int) clhy_proxy_acquire_connection(const char *proxy_function,
                                               proxy_conn_rec **conn,
                                               proxy_worker *worker,
                                               server_rec *s)
{
    kuda_status_t rv;

    if (!PROXY_WORKER_IS_USABLE(worker)) {
        /* Retry the worker */
        clhy_proxy_retry_worker(proxy_function, worker, s);

        if (!PROXY_WORKER_IS_USABLE(worker)) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00940)
                         "%s: disabled connection for (%s)",
                         proxy_function, worker->s->hostname);
            return HTTP_SERVICE_UNAVAILABLE;
        }
    }

    if (worker->s->hmax && worker->cp->res) {
        rv = kuda_reslist_acquire(worker->cp->res, (void **)conn);
    }
    else {
        /* create the new connection if the previous was destroyed */
        if (!worker->cp->conn) {
            connection_constructor((void **)conn, worker, worker->cp->pool);
        }
        else {
            *conn = worker->cp->conn;
            worker->cp->conn = NULL;
        }
        rv = KUDA_SUCCESS;
    }

    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00941)
                     "%s: failed to acquire connection for (%s)",
                     proxy_function, worker->s->hostname);
        return HTTP_SERVICE_UNAVAILABLE;
    }
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00942)
                 "%s: has acquired connection for (%s)",
                 proxy_function, worker->s->hostname);

    (*conn)->worker = worker;
    (*conn)->close  = 0;
    (*conn)->inreslist = 0;

    return OK;
}

PROXY_DECLARE(int) clhy_proxy_release_connection(const char *proxy_function,
                                               proxy_conn_rec *conn,
                                               server_rec *s)
{
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00943)
                "%s: has released connection for (%s)",
                proxy_function, conn->worker->s->hostname);
    connection_cleanup(conn);

    return OK;
}

PROXY_DECLARE(int)
clhy_proxy_determine_connection(kuda_pool_t *p, request_rec *r,
                              proxy_server_conf *conf,
                              proxy_worker *worker,
                              proxy_conn_rec *conn,
                              kuda_uri_t *uri,
                              char **url,
                              const char *proxyname,
                              kuda_port_t proxyport,
                              char *server_portstr,
                              int server_portstr_size)
{
    int server_port;
    kuda_status_t err = KUDA_SUCCESS;
    kuda_status_t uerr = KUDA_SUCCESS;
    const char *uds_path;

    /*
     * Break up the URL to determine the host to connect to
     */

    /* we break the URL into host, port, uri */
    if (KUDA_SUCCESS != kuda_uri_parse(p, *url, uri)) {
        return clhy_proxyerror(r, HTTP_BAD_REQUEST,
                             kuda_pstrcat(p,"URI cannot be parsed: ", *url,
                                         NULL));
    }
    if (!uri->port) {
        uri->port = clhy_proxy_port_of_scheme(uri->scheme);
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00944)
                 "connecting %s to %s:%d", *url, uri->hostname, uri->port);

    /*
     * allocate these out of the specified connection pool
     * The scheme handler decides if this is permanent or
     * short living pool.
     */
    /* Unless we are connecting the backend via a (forward Proxy)Remote, we
     * have to use the original form of the URI (non absolute), but this is
     * also the case via a remote proxy using the CONNECT method since the
     * original request (and URI) is to be embedded in the body.
     */
    if (!proxyname || conn->is_ssl) {
        *url = kuda_pstrcat(p, uri->path, uri->query ? "?" : "",
                           uri->query ? uri->query : "",
                           uri->fragment ? "#" : "",
                           uri->fragment ? uri->fragment : "", NULL);
    }
    /*
     * Figure out if our passed in proxy_conn_rec has a usable
     * address cached.
     *
     * TODO: Handle this much better... 
     *
     * XXX: If generic workers are ever address-reusable, we need 
     *      to check host and port on the conn and be careful about
     *      spilling the cached addr from the worker.
     */
    uds_path = (*worker->s->uds_path ? worker->s->uds_path : kuda_table_get(r->notes, "uds_path"));
    if (uds_path) {
        if (conn->uds_path == NULL) {
            /* use (*conn)->pool instead of worker->cp->pool to match lifetime */
            conn->uds_path = kuda_pstrdup(conn->pool, uds_path);
        }
        if (conn->uds_path) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02545)
                         "%s: has determined UDS as %s",
                         uri->scheme, conn->uds_path);
        }
        else {
            /* should never happen */
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02546)
                         "%s: cannot determine UDS (%s)",
                         uri->scheme, uds_path);

        }
        /*
         * In UDS cases, some structs are NULL. Protect from de-refs
         * and provide info for logging at the same time.
         */
        if (!conn->addr) {
            kuda_sockaddr_t *sa;
            kuda_sockaddr_info_get(&sa, NULL, KUDA_UNSPEC, 0, 0, conn->pool);
            conn->addr = sa;
        }
        conn->hostname = "wwhy-UDS";
        conn->port = 0;
    }
    else {
        int will_reuse = worker->s->is_address_reusable && !worker->s->disablereuse;
        if (!conn->hostname || !will_reuse) {
            if (proxyname) {
                conn->hostname = kuda_pstrdup(conn->pool, proxyname);
                conn->port = proxyport;
                /*
                 * If we have a forward proxy and the protocol is HTTPS,
                 * then we need to prepend a HTTP CONNECT request before
                 * sending our actual HTTPS requests.
                 * Save our real backend data for using it later during HTTP CONNECT.
                 */
                if (conn->is_ssl) {
                    const char *proxy_auth;

                    forward_info *forward = kuda_pcalloc(conn->pool, sizeof(forward_info));
                    conn->forward = forward;
                    forward->use_http_connect = 1;
                    forward->target_host = kuda_pstrdup(conn->pool, uri->hostname);
                    forward->target_port = uri->port;
                    /* Do we want to pass Proxy-Authorization along?
                     * If we haven't used it, then YES
                     * If we have used it then MAYBE: RFC2616 says we MAY propagate it.
                     * So let's make it configurable by env.
                     * The logic here is the same used in capi_proxy_http.
                     */
                    proxy_auth = kuda_table_get(r->headers_in, "Proxy-Authorization");
                    if (proxy_auth != NULL &&
                        proxy_auth[0] != '\0' &&
                        r->user == NULL && /* we haven't yet authenticated */
                        kuda_table_get(r->subprocess_env, "Proxy-Chain-Auth")) {
                        forward->proxy_auth = kuda_pstrdup(conn->pool, proxy_auth);
                    }
                }
            }
            else {
                conn->hostname = kuda_pstrdup(conn->pool, uri->hostname);
                conn->port = uri->port;
            }
            if (!will_reuse) {
                /*
                 * Only do a lookup if we should not reuse the backend address.
                 * Otherwise we will look it up once for the worker.
                 */
                err = kuda_sockaddr_info_get(&(conn->addr),
                                            conn->hostname, KUDA_UNSPEC,
                                            conn->port, 0,
                                            conn->pool);
            }
            socket_cleanup(conn);
            conn->close = 0;
        }
        if (will_reuse) {
            /*
             * Looking up the backend address for the worker only makes sense if
             * we can reuse the address.
             */
            if (!worker->cp->addr) {
                if ((err = PROXY_THREAD_LOCK(worker)) != KUDA_SUCCESS) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, err, r, CLHYLOGNO(00945) "lock");
                    return HTTP_INTERNAL_SERVER_ERROR;
                }

                /*
                 * Worker can have the single constant backend address.
                 * The single DNS lookup is used once per worker.
                 * If dynamic change is needed then set the addr to NULL
                 * inside dynamic config to force the lookup.
                 */
                err = kuda_sockaddr_info_get(&(worker->cp->addr),
                                            conn->hostname, KUDA_UNSPEC,
                                            conn->port, 0,
                                            worker->cp->pool);
                conn->addr = worker->cp->addr;
                if ((uerr = PROXY_THREAD_UNLOCK(worker)) != KUDA_SUCCESS) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, uerr, r, CLHYLOGNO(00946) "unlock");
                }
            }
            else {
                conn->addr = worker->cp->addr;
            }
        }
    }
    /* Close a possible existing socket if we are told to do so */
    if (conn->close) {
        socket_cleanup(conn);
        conn->close = 0;
    }

    if (err != KUDA_SUCCESS) {
        return clhy_proxyerror(r, HTTP_BAD_GATEWAY,
                             kuda_pstrcat(p, "DNS lookup failure for: ",
                                         conn->hostname, NULL));
    }

    /* Get the server port for the Via headers */
    server_port = clhy_get_server_port(r);
    CLHY_DEBUG_ASSERT(server_portstr_size > 0);
    if (clhy_is_default_port(server_port, r)) {
        server_portstr[0] = '\0';
    }
    else {
        kuda_snprintf(server_portstr, server_portstr_size, ":%d",
                     server_port);
    }

    /* check if ProxyBlock directive on this host */
    if (OK != clhy_proxy_checkproxyblock2(r, conf, uri->hostname, 
                                       proxyname ? NULL : conn->addr)) {
        return clhy_proxyerror(r, HTTP_FORBIDDEN,
                             "Connect to remote machine blocked");
    }
    /*
     * When SSL is configured, determine the hostname (SNI) for the request
     * and save it in conn->ssl_hostname. Close any reused connection whose
     * SNI differs.
     */
    if (conn->is_ssl) {
        proxy_dir_conf *dconf;
        const char *ssl_hostname;
        /*
         * In the case of ProxyPreserveHost on use the hostname of
         * the request if present otherwise use the one from the
         * backend request URI.
         */
        dconf = clhy_get_capi_config(r->per_dir_config, &proxy_capi);
        if (dconf->preserve_host) {
            ssl_hostname = r->hostname;
        }
        else if (conn->forward
                 && ((forward_info *)(conn->forward))->use_http_connect) {
            ssl_hostname = ((forward_info *)conn->forward)->target_host;
        }
        else {
            ssl_hostname = conn->hostname;
        }
        /*
         * Close if a SNI is in use but this request requires no or
         * a different one, or no SNI is in use but one is required.
         */
        if ((conn->ssl_hostname && (!ssl_hostname ||
                                    strcasecmp(conn->ssl_hostname,
                                               ssl_hostname) != 0)) ||
                (!conn->ssl_hostname && ssl_hostname && conn->sock)) {
            socket_cleanup(conn);
        }
        if (conn->ssl_hostname == NULL) {
            conn->ssl_hostname = kuda_pstrdup(conn->scpool, ssl_hostname);
        }
    }
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00947)
                 "connected %s to %s:%d", *url, conn->hostname, conn->port);
    return OK;
}

#define USE_ALTERNATE_IS_CONNECTED 1

#if !defined(KUDA_MSG_PEEK) && defined(MSG_PEEK)
#define KUDA_MSG_PEEK MSG_PEEK
#endif

#if USE_ALTERNATE_IS_CONNECTED && defined(KUDA_MSG_PEEK)
PROXY_DECLARE(int) clhy_proxy_is_socket_connected(kuda_socket_t *socket)
{
    kuda_pollfd_t pfds[1];
    kuda_status_t status;
    kuda_int32_t  nfds;

    pfds[0].reqevents = KUDA_POLLIN;
    pfds[0].desc_type = KUDA_POLL_SOCKET;
    pfds[0].desc.s = socket;

    do {
        status = kuda_poll(&pfds[0], 1, &nfds, 0);
    } while (KUDA_STATUS_IS_EINTR(status));

    if (status == KUDA_SUCCESS && nfds == 1 &&
        pfds[0].rtnevents == KUDA_POLLIN) {
        kuda_sockaddr_t unused;
        kuda_size_t len = 1;
        char buf[1];
        /* The socket might be closed in which case
         * the poll will return POLLIN.
         * If there is no data available the socket
         * is closed.
         */
        status = kuda_socket_recvfrom(&unused, socket, KUDA_MSG_PEEK,
                                     &buf[0], &len);
        if (status == KUDA_SUCCESS && len)
            return 1;
        else
            return 0;
    }
    else if (KUDA_STATUS_IS_EAGAIN(status) || KUDA_STATUS_IS_TIMEUP(status)) {
        return 1;
    }
    return 0;

}
#else
PROXY_DECLARE(int) clhy_proxy_is_socket_connected(kuda_socket_t *sock)

{
    kuda_size_t buffer_len = 1;
    char test_buffer[1];
    kuda_status_t socket_status;
    kuda_interval_time_t current_timeout;

    /* save timeout */
    kuda_socket_timeout_get(sock, &current_timeout);
    /* set no timeout */
    kuda_socket_timeout_set(sock, 0);
    socket_status = kuda_socket_recv(sock, test_buffer, &buffer_len);
    /* put back old timeout */
    kuda_socket_timeout_set(sock, current_timeout);
    if (KUDA_STATUS_IS_EOF(socket_status)
        || KUDA_STATUS_IS_ECONNRESET(socket_status)) {
        return 0;
    }
    else {
        return 1;
    }
}
#endif /* USE_ALTERNATE_IS_CONNECTED */


/*
 * Send a HTTP CONNECT request to a forward proxy.
 * The proxy is given by "backend", the target server
 * is contained in the "forward" member of "backend".
 */
static kuda_status_t send_http_connect(proxy_conn_rec *backend,
                                      server_rec *s)
{
    int status;
    kuda_size_t nbytes;
    kuda_size_t left;
    int complete = 0;
    char buffer[HUGE_STRING_LEN];
    char drain_buffer[HUGE_STRING_LEN];
    forward_info *forward = (forward_info *)backend->forward;
    int len = 0;

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00948)
                 "CONNECT: sending the CONNECT request for %s:%d "
                 "to the remote proxy %pI (%s)",
                 forward->target_host, forward->target_port,
                 backend->addr, backend->hostname);
    /* Create the CONNECT request */
    nbytes = kuda_snprintf(buffer, sizeof(buffer),
                          "CONNECT %s:%d HTTP/1.0" CRLF,
                          forward->target_host, forward->target_port);
    /* Add proxy authorization from the initial request if necessary */
    if (forward->proxy_auth != NULL) {
        nbytes += kuda_snprintf(buffer + nbytes, sizeof(buffer) - nbytes,
                               "Proxy-Authorization: %s" CRLF,
                               forward->proxy_auth);
    }
    /* Set a reasonable agent and send everything */
    nbytes += kuda_snprintf(buffer + nbytes, sizeof(buffer) - nbytes,
                           "Proxy-agent: %s" CRLF CRLF,
                           clhy_get_server_banner());
    clhy_xlate_proto_to_ascii(buffer, nbytes);
    kuda_socket_send(backend->sock, buffer, &nbytes);

    /* Receive the whole CONNECT response */
    left = sizeof(buffer) - 1;
    /* Read until we find the end of the headers or run out of buffer */
    do {
        nbytes = left;
        status = kuda_socket_recv(backend->sock, buffer + len, &nbytes);
        len += nbytes;
        left -= nbytes;
        buffer[len] = '\0';
        if (strstr(buffer + len - nbytes, CRLF_ASCII CRLF_ASCII) != NULL) {
            clhy_xlate_proto_from_ascii(buffer, len);
            complete = 1;
            break;
        }
    } while (status == KUDA_SUCCESS && left > 0);
    /* Drain what's left */
    if (!complete) {
        nbytes = sizeof(drain_buffer) - 1;
        while (status == KUDA_SUCCESS && nbytes) {
            status = kuda_socket_recv(backend->sock, drain_buffer, &nbytes);
            drain_buffer[nbytes] = '\0';
            nbytes = sizeof(drain_buffer) - 1;
            if (strstr(drain_buffer, CRLF_ASCII CRLF_ASCII) != NULL) {
                break;
            }
        }
    }

    /* Check for HTTP_OK response status */
    if (status == KUDA_SUCCESS) {
        unsigned int major, minor;
        /* Only scan for three character status code */
        char code_str[4];

        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00949)
                     "send_http_connect: response from the forward proxy: %s",
                     buffer);

        /* Extract the returned code */
        if (sscanf(buffer, "HTTP/%u.%u %3s", &major, &minor, code_str) == 3) {
            status = atoi(code_str);
            if (status == HTTP_OK) {
                status = KUDA_SUCCESS;
            }
            else {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00950)
                             "send_http_connect: the forward proxy returned code is '%s'",
                             code_str);
                status = KUDA_INCOMPLETE;
            }
        }
    }

    return(status);
}


/* TODO: In KUDA 2.x: Extend kuda_sockaddr_t to possibly be a path !!! */
PROXY_DECLARE(kuda_status_t) clhy_proxy_connect_uds(kuda_socket_t *sock,
                                                 const char *uds_path,
                                                 kuda_pool_t *p)
{
#if KUDA_HAVE_SYS_UN_H
    kuda_status_t rv;
    kuda_platform_sock_t rawsock;
    kuda_interval_time_t t;
    struct sockaddr_un *sa;
    kuda_socklen_t addrlen, pathlen;

    rv = kuda_platform_sock_get(&rawsock, sock);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    rv = kuda_socket_timeout_get(sock, &t);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    pathlen = strlen(uds_path);
    /* copy the UDS path (including NUL) to the sockaddr_un */
    addrlen = KUDA_OFFSETOF(struct sockaddr_un, sun_path) + pathlen;
    sa = (struct sockaddr_un *)kuda_palloc(p, addrlen + 1);
    memcpy(sa->sun_path, uds_path, pathlen + 1);
    sa->sun_family = AF_UNIX;

    do {
        rv = connect(rawsock, (struct sockaddr*)sa, addrlen);
    } while (rv == -1 && (rv = errno) == EINTR);

    if (rv && rv != EISCONN) {
        if ((rv == EINPROGRESS || rv == EALREADY) && (t > 0))  {
#if KUDA_MAJOR_VERSION < 2
            rv = kuda_wait_for_io_or_timeout(NULL, sock, 0);
#else
            rv = kuda_socket_wait(sock, KUDA_WAIT_WRITE);
#endif
        }
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
    }

    return KUDA_SUCCESS;
#else
    return KUDA_ENOTIMPL;
#endif
}

PROXY_DECLARE(kuda_status_t) clhy_proxy_check_connection(const char *scheme,
                                                      proxy_conn_rec *conn,
                                                      server_rec *server,
                                                      unsigned max_blank_lines,
                                                      int flags)
{
    kuda_status_t rv = KUDA_SUCCESS;
    proxy_worker *worker = conn->worker;

    if (!PROXY_WORKER_IS_USABLE(worker)) {
        /*
         * The worker is in error likely done by a different thread / process
         * e.g. for a timeout or bad status. We should respect this and should
         * not continue with a connection via this worker even if we got one.
         */
        rv = KUDA_EINVAL;
    }
    else if (conn->connection) {
        /* We have a conn_rec, check the full filter stack for things like
         * SSL alert/shutdown, filters aside data...
         */
        rv = clhy_check_pipeline(conn->connection, conn->tmp_bb,
                               max_blank_lines);
        kuda_brigade_cleanup(conn->tmp_bb);
        if (rv == KUDA_SUCCESS) {
            /* Some data available, the caller might not want them. */
            if (flags & PROXY_CHECK_CONN_EMPTY) {
                rv = KUDA_ENOTEMPTY;
            }
        }
        else if (KUDA_STATUS_IS_EAGAIN(rv)) {
            /* Filter chain is OK and empty, yet we can't determine from
             * clhy_check_pipeline (actually clhy_core_input_filter) whether
             * an empty non-blocking read is EAGAIN or EOF on the socket
             * side (it's always SUCCESS), so check it explicitely here.
             */
            if (clhy_proxy_is_socket_connected(conn->sock)) {
                rv = KUDA_SUCCESS;
            }
            else {
                rv = KUDA_EPIPE;
            }
        }
    }
    else if (conn->sock) {
        /* For cAPIs working with sockets directly, check it. */
        if (!clhy_proxy_is_socket_connected(conn->sock)) {
            rv = KUDA_EPIPE;
        }
    }
    else {
        rv = KUDA_ENOSOCKET;
    }

    if (rv == KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, server,
                     "%s: reusing backend connection %pI<>%pI",
                     scheme, conn->connection->local_addr,
                     conn->connection->client_addr);
    }
    else if (conn->sock) {
        /* This clears conn->scpool (and associated data), so backup and
         * restore any ssl_hostname for this connection set earlier by
         * clhy_proxy_determine_connection().
         */
        char ssl_hostname[PROXY_WORKER_RFC1035_NAME_SIZE];
        if (rv == KUDA_EINVAL
                || !conn->ssl_hostname
                || PROXY_STRNCPY(ssl_hostname, conn->ssl_hostname)) {
            ssl_hostname[0] = '\0';
        }

        socket_cleanup(conn);
        if (rv != KUDA_ENOTEMPTY) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, server, CLHYLOGNO(00951)
                         "%s: backend socket is disconnected.", scheme);
        }
        else {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, server, CLHYLOGNO(03408)
                         "%s: reusable backend connection is not empty: "
                         "forcibly closed", scheme);
        }

        if (ssl_hostname[0]) {
            conn->ssl_hostname = kuda_pstrdup(conn->scpool, ssl_hostname);
        }
    }

    return rv;
}

PROXY_DECLARE(int) clhy_proxy_connect_backend(const char *proxy_function,
                                            proxy_conn_rec *conn,
                                            proxy_worker *worker,
                                            server_rec *s)
{
    kuda_status_t rv;
    int loglevel;
    kuda_sockaddr_t *backend_addr = conn->addr;
    /* the local address to use for the outgoing connection */
    kuda_sockaddr_t *local_addr;
    kuda_socket_t *newsock;
    void *sconf = s->capi_config;
    proxy_server_conf *conf =
        (proxy_server_conf *) clhy_get_capi_config(sconf, &proxy_capi);

    rv = clhy_proxy_check_connection(proxy_function, conn, s, 0, 0);
    if (rv == KUDA_EINVAL) {
        return DECLINED;
    }

    while (rv != KUDA_SUCCESS && (backend_addr || conn->uds_path)) {
#if KUDA_HAVE_SYS_UN_H
        if (conn->uds_path)
        {
            rv = kuda_socket_create(&newsock, AF_UNIX, SOCK_STREAM, 0,
                                   conn->scpool);
            if (rv != KUDA_SUCCESS) {
                loglevel = CLHYLOG_ERR;
                clhy_log_error(CLHYLOG_MARK, loglevel, rv, s, CLHYLOGNO(02453)
                             "%s: error creating Unix domain socket for "
                             "target %s",
                             proxy_function,
                             worker->s->hostname);
                break;
            }
            conn->connection = NULL;

            rv = clhy_proxy_connect_uds(newsock, conn->uds_path, conn->scpool);
            if (rv != KUDA_SUCCESS) {
                kuda_socket_close(newsock);
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(02454)
                             "%s: attempt to connect to Unix domain socket "
                             "%s (%s) failed",
                             proxy_function,
                             conn->uds_path,
                             worker->s->hostname);
                break;
            }

            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(02823)
                         "%s: connection established with Unix domain socket "
                         "%s (%s)",
                         proxy_function,
                         conn->uds_path,
                         worker->s->hostname);
        }
        else
#endif
        {
            if ((rv = kuda_socket_create(&newsock, backend_addr->family,
                                        SOCK_STREAM, KUDA_PROTO_TCP,
                                        conn->scpool)) != KUDA_SUCCESS) {
                loglevel = backend_addr->next ? CLHYLOG_DEBUG : CLHYLOG_ERR;
                clhy_log_error(CLHYLOG_MARK, loglevel, rv, s, CLHYLOGNO(00952)
                             "%s: error creating fam %d socket for "
                             "target %s",
                             proxy_function,
                             backend_addr->family,
                             worker->s->hostname);
                /*
                 * this could be an IPv6 address from the DNS but the
                 * local machine won't give us an IPv6 socket; hopefully the
                 * DNS returned an additional address to try
                 */
                backend_addr = backend_addr->next;
                continue;
            }
            conn->connection = NULL;

            if (worker->s->recv_buffer_size > 0 &&
                (rv = kuda_socket_opt_set(newsock, KUDA_SO_RCVBUF,
                                         worker->s->recv_buffer_size))) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00953)
                             "kuda_socket_opt_set(SO_RCVBUF): Failed to set "
                             "ProxyReceiveBufferSize, using default");
            }

            rv = kuda_socket_opt_set(newsock, KUDA_TCP_NODELAY, 1);
            if (rv != KUDA_SUCCESS && rv != KUDA_ENOTIMPL) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00954)
                             "kuda_socket_opt_set(KUDA_TCP_NODELAY): "
                             "Failed to set");
            }

            /* Set a timeout for connecting to the backend on the socket */
            if (worker->s->conn_timeout_set) {
                kuda_socket_timeout_set(newsock, worker->s->conn_timeout);
            }
            else if (worker->s->timeout_set) {
                kuda_socket_timeout_set(newsock, worker->s->timeout);
            }
            else if (conf->timeout_set) {
                kuda_socket_timeout_set(newsock, conf->timeout);
            }
            else {
                kuda_socket_timeout_set(newsock, s->timeout);
            }
            /* Set a keepalive option */
            if (worker->s->keepalive) {
                if ((rv = kuda_socket_opt_set(newsock,
                                             KUDA_SO_KEEPALIVE, 1)) != KUDA_SUCCESS) {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00955)
                                 "kuda_socket_opt_set(SO_KEEPALIVE): Failed to set"
                                 " Keepalive");
                }
            }
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, s,
                         "%s: fam %d socket created to connect to %s",
                         proxy_function, backend_addr->family, worker->s->hostname);

            if (conf->source_address_set) {
                local_addr = kuda_pmemdup(conn->scpool, conf->source_address,
                                         sizeof(kuda_sockaddr_t));
                local_addr->pool = conn->scpool;
                rv = kuda_socket_bind(newsock, local_addr);
                if (rv != KUDA_SUCCESS) {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00956)
                                 "%s: failed to bind socket to local address",
                                 proxy_function);
                }
            }

            /* make the connection out of the socket */
            rv = kuda_socket_connect(newsock, backend_addr);

            /* if an error occurred, loop round and try again */
            if (rv != KUDA_SUCCESS) {
                kuda_socket_close(newsock);
                loglevel = backend_addr->next ? CLHYLOG_DEBUG : CLHYLOG_ERR;
                clhy_log_error(CLHYLOG_MARK, loglevel, rv, s, CLHYLOGNO(00957)
                             "%s: attempt to connect to %pI (%s) failed",
                             proxy_function,
                             backend_addr,
                             worker->s->hostname);
                backend_addr = backend_addr->next;
                continue;
            }

            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(02824)
                         "%s: connection established with %pI (%s)",
                         proxy_function,
                         backend_addr,
                         worker->s->hostname);
        }

        /* Set a timeout on the socket */
        if (worker->s->timeout_set) {
            kuda_socket_timeout_set(newsock, worker->s->timeout);
        }
        else if (conf->timeout_set) {
            kuda_socket_timeout_set(newsock, conf->timeout);
        }
        else {
             kuda_socket_timeout_set(newsock, s->timeout);
        }

        conn->sock = newsock;

        if (!conn->uds_path && conn->forward) {
            forward_info *forward = (forward_info *)conn->forward;
            /*
             * For HTTP CONNECT we need to prepend CONNECT request before
             * sending our actual HTTPS requests.
             */
            if (forward->use_http_connect) {
                rv = send_http_connect(conn, s);
                /* If an error occurred, loop round and try again */
                if (rv != KUDA_SUCCESS) {
                    conn->sock = NULL;
                    kuda_socket_close(newsock);
                    loglevel = backend_addr->next ? CLHYLOG_DEBUG : CLHYLOG_ERR;
                    clhy_log_error(CLHYLOG_MARK, loglevel, rv, s, CLHYLOGNO(00958)
                                 "%s: attempt to connect to %s:%d "
                                 "via http CONNECT through %pI (%s) failed",
                                 proxy_function,
                                 forward->target_host, forward->target_port,
                                 backend_addr, worker->s->hostname);
                    backend_addr = backend_addr->next;
                    continue;
                }
            }
        }
    }

    if (PROXY_WORKER_IS_USABLE(worker)) {
        /*
         * Put the entire worker to error state if
         * the PROXY_WORKER_IGNORE_ERRORS flag is not set.
         * Although some connections may be alive
         * no further connections to the worker could be made
         */
        if (rv != KUDA_SUCCESS) {
            if (!(worker->s->status & PROXY_WORKER_IGNORE_ERRORS)) {
                worker->s->error_time = kuda_time_now();
                worker->s->status |= PROXY_WORKER_IN_ERROR;
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00959)
                    "clhy_proxy_connect_backend disabling worker for (%s) for %"
                    KUDA_TIME_T_FMT "s",
                    worker->s->hostname, kuda_time_sec(worker->s->retry));
            }
        }
        else {
            if (worker->s->retries) {
                /*
                 * A worker came back. So here is where we need to
                 * either reset all params to initial conditions or
                 * apply some sort of aging
                 */
            }
            worker->s->error_time = 0;
            worker->s->retries = 0;
        }
    }
    else {
        /*
         * The worker is in error likely done by a different thread / process
         * e.g. for a timeout or bad status. We should respect this and should
         * not continue with a connection via this worker even if we got one.
         */
        if (rv == KUDA_SUCCESS) {
            socket_cleanup(conn);
        }
        rv = KUDA_EINVAL;
    }

    return rv == KUDA_SUCCESS ? OK : DECLINED;
}

static kuda_status_t connection_shutdown(void *theconn)
{
    proxy_conn_rec *conn = (proxy_conn_rec *)theconn;
    conn_rec *c = conn->connection;
    if (c) {
        if (!c->aborted) {
            kuda_interval_time_t saved_timeout = 0;
            kuda_socket_timeout_get(conn->sock, &saved_timeout);
            if (saved_timeout) {
                kuda_socket_timeout_set(conn->sock, 0);
            }

            (void)clhy_shutdown_conn(c, 0);
            c->aborted = 1;

            if (saved_timeout) {
                kuda_socket_timeout_set(conn->sock, saved_timeout);
            }
        }

        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, c, CLHYLOGNO(02642)
                      "proxy: connection shutdown");
    }
    return KUDA_SUCCESS;
}


PROXY_DECLARE(int) clhy_proxy_connection_create(const char *proxy_function,
                                              proxy_conn_rec *conn,
                                              conn_rec *c,
                                              server_rec *s)
{
    kuda_sockaddr_t *backend_addr = conn->addr;
    int rc;
    kuda_interval_time_t current_timeout;
    kuda_bucket_alloc_t *bucket_alloc;

    if (conn->connection) {
        return OK;
    }

    bucket_alloc = kuda_bucket_alloc_create(conn->scpool);
    conn->tmp_bb = kuda_brigade_create(conn->scpool, bucket_alloc);
    /*
     * The socket is now open, create a new backend server connection
     */
    conn->connection = clhy_run_create_connection(conn->scpool, s, conn->sock,
                                                0, NULL,
                                                bucket_alloc);

    if (!conn->connection) {
        /*
         * the peer reset the connection already; clhy_run_create_connection()
         * closed the socket
         */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0,
                     s, CLHYLOGNO(00960) "%s: an error occurred creating a "
                     "new connection to %pI (%s)", proxy_function,
                     backend_addr, conn->hostname);
        /* XXX: Will be closed when proxy_conn is closed */
        socket_cleanup(conn);
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    /* For ssl connection to backend */
    if (conn->is_ssl) {
        if (!clhy_proxy_ssl_enable(conn->connection)) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0,
                         s, CLHYLOGNO(00961) "%s: failed to enable ssl support "
                         "for %pI (%s)", proxy_function,
                         backend_addr, conn->hostname);
            return HTTP_INTERNAL_SERVER_ERROR;
        }
    }
    else {
        /* TODO: See if this will break FTP */
        clhy_proxy_ssl_disable(conn->connection);
    }

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00962)
                 "%s: connection complete to %pI (%s)",
                 proxy_function, backend_addr, conn->hostname);

    /*
     * save the timeout of the socket because core_pre_connection
     * will set it to base_server->timeout
     * (core TimeOut directive).
     */
    kuda_socket_timeout_get(conn->sock, &current_timeout);
    /* set up the connection filters */
    rc = clhy_run_pre_connection(conn->connection, conn->sock);
    if (rc != OK && rc != DONE) {
        conn->connection->aborted = 1;
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(00963)
                     "%s: pre_connection setup failed (%d)",
                     proxy_function, rc);
        return rc;
    }
    kuda_socket_timeout_set(conn->sock, current_timeout);

    /* Shutdown the connection before closing it (eg. SSL connections
     * need to be close-notify-ed).
     */
    kuda_pool_pre_cleanup_register(conn->scpool, conn, connection_shutdown);

    return OK;
}

int clhy_proxy_lb_workers(void)
{
    /*
     * Since we can't resize the scoreboard when reconfiguring, we
     * have to impose a limit on the number of workers, we are
     * able to reconfigure to.
     */
    if (!lb_workers_limit)
        lb_workers_limit = proxy_lb_workers + PROXY_DYNAMIC_BALANCER_LIMIT;
    return lb_workers_limit;
}

PROXY_DECLARE(void) clhy_proxy_backend_broke(request_rec *r,
                                           kuda_bucket_brigade *brigade)
{
    kuda_bucket *e;
    conn_rec *c = r->connection;

    r->no_cache = 1;
    /*
     * If this is a subrequest, then prevent also caching of the main
     * request.
     */
    if (r->main)
        r->main->no_cache = 1;
    e = clhy_bucket_error_create(HTTP_BAD_GATEWAY, NULL, c->pool,
                               c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(brigade, e);
    e = kuda_bucket_eos_create(c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(brigade, e);
}

/*
 * Provide a string hashing function for the proxy.
 * We offer 2 methods: one is the KUDA model but we
 * also provide our own, based on either FNV or SDBM.
 * The reason is in case we want to use both to ensure no
 * collisions.
 */
PROXY_DECLARE(unsigned int)
clhy_proxy_hashfunc(const char *str, proxy_hash_t method)
{
    if (method == PROXY_HASHFUNC_KUDA) {
        kuda_ssize_t slen = strlen(str);
        return kuda_hashfunc_default(str, &slen);
    }
    else if (method == PROXY_HASHFUNC_FNV) {
        /* FNV model */
        unsigned int hash;
        const unsigned int fnv_prime = 0x811C9DC5;
        for (hash = 0; *str; str++) {
            hash *= fnv_prime;
            hash ^= (*str);
        }
        return hash;
    }
    else { /* method == PROXY_HASHFUNC_DEFAULT */
        /* SDBM model */
        unsigned int hash;
        for (hash = 0; *str; str++) {
            hash = (*str) + (hash << 6) + (hash << 16) - hash;
        }
        return hash;
    }
}

PROXY_DECLARE(kuda_status_t) clhy_proxy_set_wstatus(char c, int set, proxy_worker *w)
{
    unsigned int *status = &w->s->status;
    char flag = toupper(c);
    proxy_wstat_t *pwt = proxy_wstat_tbl;
    while (pwt->bit) {
        if (flag == pwt->flag) {
            if (set)
                *status |= pwt->bit;
            else
                *status &= ~(pwt->bit);
            return KUDA_SUCCESS;
        }
        pwt++;
    }
    return KUDA_EINVAL;
}

PROXY_DECLARE(char *) clhy_proxy_parse_wstatus(kuda_pool_t *p, proxy_worker *w)
{
    char *ret = "";
    unsigned int status = w->s->status;
    proxy_wstat_t *pwt = proxy_wstat_tbl;
    while (pwt->bit) {
        if (status & pwt->bit)
            ret = kuda_pstrcat(p, ret, pwt->name, NULL);
        pwt++;
    }
    if (!*ret) {
        ret = "??? ";
    }
    if (PROXY_WORKER_IS_USABLE(w))
        ret = kuda_pstrcat(p, ret, "Ok ", NULL);
    return ret;
}

PROXY_DECLARE(kuda_status_t) clhy_proxy_sync_balancer(proxy_balancer *b, server_rec *s,
                                                    proxy_server_conf *conf)
{
    proxy_worker **workers;
    int i;
    int index;
    proxy_worker_shared *shm;
    proxy_balancer_method *lbmethod;
    clhy_slotmem_provider_t *storage = b->storage;

    if (b->s->wupdated <= b->wupdated)
        return KUDA_SUCCESS;
    /* balancer sync */
    lbmethod = clhy_lookup_provider(PROXY_LBMETHOD, b->s->lbpname, "0");
    if (lbmethod) {
        b->lbmethod = lbmethod;
    } else {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, s, CLHYLOGNO(02433)
                     "Cannot find LB Method: %s", b->s->lbpname);
        return KUDA_EINVAL;
    }

    /* worker sync */

    /*
     * Look thru the list of workers in shm
     * and see which one(s) we are lacking...
     * again, the cast to unsigned int is safe
     * since our upper limit is always max_workers
     * which is int.
     */
    for (index = 0; index < b->max_workers; index++) {
        int found;
        kuda_status_t rv;
        if ((rv = storage->dptr(b->wslot, (unsigned int)index, (void *)&shm)) != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, s, CLHYLOGNO(00965) "worker slotmem_dptr failed");
            return KUDA_EGENERAL;
        }
        /* account for possible "holes" in the slotmem
         * (eg: slots 0-2 are used, but 3 isn't, but 4-5 is)
         */
        if (!shm->hash.def || !shm->hash.fnv)
            continue;
        found = 0;
        workers = (proxy_worker **)b->workers->elts;
        for (i = 0; i < b->workers->nelts; i++, workers++) {
            proxy_worker *worker = *workers;
            if (worker->hash.def == shm->hash.def && worker->hash.fnv == shm->hash.fnv) {
                found = 1;
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(02402)
                             "re-grabbing shm[%d] (0x%pp) for worker: %s", i, (void *)shm,
                             clhy_proxy_worker_name(conf->pool, worker));
                break;
            }
        }
        if (!found) {
            proxy_worker **runtime;
            kuda_global_mutex_lock(proxy_mutex);
            runtime = kuda_array_push(b->workers);
            *runtime = kuda_palloc(conf->pool, sizeof(proxy_worker));
            kuda_global_mutex_unlock(proxy_mutex);
            (*runtime)->hash = shm->hash;
            (*runtime)->context = NULL;
            (*runtime)->cp = NULL;
            (*runtime)->balancer = b;
            (*runtime)->s = shm;
            (*runtime)->tmutex = NULL;
            rv = clhy_proxy_initialize_worker(*runtime, s, conf->pool);
            if (rv != KUDA_SUCCESS) {
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, rv, s, CLHYLOGNO(00966) "Cannot init worker");
                return rv;
            }
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, s, CLHYLOGNO(02403)
                         "grabbing shm[%d] (0x%pp) for worker: %s", i, (void *)shm,
                         (*runtime)->s->name);
        }
    }
    if (b->s->need_reset) {
        if (b->lbmethod && b->lbmethod->reset)
            b->lbmethod->reset(b, s);
        b->s->need_reset = 0;
    }
    b->wupdated = b->s->wupdated;
    return KUDA_SUCCESS;
}

PROXY_DECLARE(proxy_worker_shared *) clhy_proxy_find_workershm(clhy_slotmem_provider_t *storage,
                                                               clhy_slotmem_instance_t *slot,
                                                               proxy_worker *worker,
                                                               unsigned int *index)
{
    proxy_worker_shared *shm;
    unsigned int i, limit;
    limit = storage->num_slots(slot);
    for (i = 0; i < limit; i++) {
        if (storage->dptr(slot, i, (void *)&shm) != KUDA_SUCCESS) {
            return NULL;
        }
        if ((worker->s->hash.def == shm->hash.def) &&
            (worker->s->hash.fnv == shm->hash.fnv)) {
            *index = i;
            return shm;
        }
    }
    return NULL;
}

PROXY_DECLARE(proxy_balancer_shared *) clhy_proxy_find_balancershm(clhy_slotmem_provider_t *storage,
                                                                 clhy_slotmem_instance_t *slot,
                                                                 proxy_balancer *balancer,
                                                                 unsigned int *index)
{
    proxy_balancer_shared *shm;
    unsigned int i, limit;
    limit = storage->num_slots(slot);
    for (i = 0; i < limit; i++) {
        if (storage->dptr(slot, i, (void *)&shm) != KUDA_SUCCESS) {
            return NULL;
        }
        if ((balancer->s->hash.def == shm->hash.def) &&
            (balancer->s->hash.fnv == shm->hash.fnv)) {
            *index = i;
            return shm;
        }
    }
    return NULL;
}

typedef struct header_connection {
    kuda_pool_t *pool;
    kuda_array_header_t *array;
    const char *first;
    unsigned int closed:1;
} header_connection;

static int find_conn_headers(void *data, const char *key, const char *val)
{
    header_connection *x = data;
    const char *name;

    do {
        while (*val == ',' || *val == ';') {
            val++;
        }
        name = clhy_get_token(x->pool, &val, 0);
        if (!strcasecmp(name, "close")) {
            x->closed = 1;
        }
        if (!x->first) {
            x->first = name;
        }
        else {
            const char **elt;
            if (!x->array) {
                x->array = kuda_array_make(x->pool, 4, sizeof(char *));
            }
            elt = kuda_array_push(x->array);
            *elt = name;
        }
    } while (*val);

    return 1;
}

/**
 * Remove all headers referred to by the Connection header.
 */
static int clhy_proxy_clear_connection(request_rec *r, kuda_table_t *headers)
{
    const char **name;
    header_connection x;

    x.pool = r->pool;
    x.array = NULL;
    x.first = NULL;
    x.closed = 0;

    kuda_table_unset(headers, "Proxy-Connection");

    kuda_table_do(find_conn_headers, &x, headers, "Connection", NULL);
    if (x.first) {
        /* fast path - no memory allocated for one header */
        kuda_table_unset(headers, "Connection");
        kuda_table_unset(headers, x.first);
    }
    if (x.array) {
        /* two or more headers */
        while ((name = kuda_array_pop(x.array))) {
            kuda_table_unset(headers, *name);
        }
    }

    return x.closed;
}

PROXY_DECLARE(int) clhy_proxy_create_hdrbrgd(kuda_pool_t *p,
                                            kuda_bucket_brigade *header_brigade,
                                            request_rec *r,
                                            proxy_conn_rec *p_conn,
                                            proxy_worker *worker,
                                            proxy_server_conf *conf,
                                            kuda_uri_t *uri,
                                            char *url, char *server_portstr,
                                            char **old_cl_val,
                                            char **old_te_val)
{
    conn_rec *c = r->connection;
    int counter;
    char *buf;
    const kuda_array_header_t *headers_in_array;
    const kuda_table_entry_t *headers_in;
    kuda_table_t *saved_headers_in;
    kuda_bucket *e;
    int do_100_continue;
    conn_rec *origin = p_conn->connection;
    proxy_dir_conf *dconf = clhy_get_capi_config(r->per_dir_config, &proxy_capi);

    /*
     * To be compliant, we only use 100-Continue for requests with bodies.
     * We also make sure we won't be talking HTTP/1.0 as well.
     */
    do_100_continue = (worker->s->ping_timeout_set
                       && clhy_request_has_body(r)
                       && (PROXYREQ_REVERSE == r->proxyreq)
                       && !(kuda_table_get(r->subprocess_env, "force-proxy-request-1.0")));

    if (kuda_table_get(r->subprocess_env, "force-proxy-request-1.0")) {
        /*
         * According to RFC 2616 8.2.3 we are not allowed to forward an
         * Expect: 100-continue to an HTTP/1.0 server. Instead we MUST return
         * a HTTP_EXPECTATION_FAILED
         */
        if (r->expecting_100) {
            return HTTP_EXPECTATION_FAILED;
        }
        buf = kuda_pstrcat(p, r->method, " ", url, " HTTP/1.0" CRLF, NULL);
        p_conn->close = 1;
    } else {
        buf = kuda_pstrcat(p, r->method, " ", url, " HTTP/1.1" CRLF, NULL);
    }
    if (kuda_table_get(r->subprocess_env, "proxy-nokeepalive")) {
        origin->keepalive = CLHY_CONN_CLOSE;
        p_conn->close = 1;
    }
    clhy_xlate_proto_to_ascii(buf, strlen(buf));
    e = kuda_bucket_pool_create(buf, strlen(buf), p, c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(header_brigade, e);
    if (dconf->preserve_host == 0) {
        if (clhy_strchr_c(uri->hostname, ':')) { /* if literal IPv6 address */
            if (uri->port_str && uri->port != DEFAULT_HTTP_PORT) {
                buf = kuda_pstrcat(p, "Host: [", uri->hostname, "]:",
                                  uri->port_str, CRLF, NULL);
            } else {
                buf = kuda_pstrcat(p, "Host: [", uri->hostname, "]", CRLF, NULL);
            }
        } else {
            if (uri->port_str && uri->port != DEFAULT_HTTP_PORT) {
                buf = kuda_pstrcat(p, "Host: ", uri->hostname, ":",
                                  uri->port_str, CRLF, NULL);
            } else {
                buf = kuda_pstrcat(p, "Host: ", uri->hostname, CRLF, NULL);
            }
        }
    }
    else {
        /* don't want to use r->hostname, as the incoming header might have a
         * port attached
         */
        const char* hostname = kuda_table_get(r->headers_in,"Host");
        if (!hostname) {
            hostname =  r->server->server_hostname;
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01092)
                          "no HTTP 0.9 request (with no host line) "
                          "on incoming request and preserve host set "
                          "forcing hostname to be %s for uri %s",
                          hostname, r->uri);
        }
        buf = kuda_pstrcat(p, "Host: ", hostname, CRLF, NULL);
    }
    clhy_xlate_proto_to_ascii(buf, strlen(buf));
    e = kuda_bucket_pool_create(buf, strlen(buf), p, c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(header_brigade, e);

    /*
     * Save the original headers in here and restore them when leaving, since
     * we will apply proxy purpose only modifications (eg. clearing hop-by-hop
     * headers, add Via or X-Forwarded-* or Expect...), whereas the originals
     * will be needed later to prepare the correct response and logging.
     *
     * Note: We need to take r->pool for kuda_table_copy as the key / value
     * pairs in r->headers_in have been created out of r->pool and
     * p might be (and actually is) a longer living pool.
     * This would trigger the bad pool ancestry abort in kuda_table_copy if
     * kuda is compiled with KUDA_POOL_DEBUG.
     */
    saved_headers_in = r->headers_in;
    r->headers_in = kuda_table_copy(r->pool, saved_headers_in);

    /* handle Via */
    if (conf->viaopt == via_block) {
        /* Block all outgoing Via: headers */
        kuda_table_unset(r->headers_in, "Via");
    } else if (conf->viaopt != via_off) {
        const char *server_name = clhy_get_server_name(r);
        /* If USE_CANONICAL_NAME_OFF was configured for the proxy virtual host,
         * then the server name returned by clhy_get_server_name() is the
         * origin server name (which does make too much sense with Via: headers)
         * so we use the proxy vhost's name instead.
         */
        if (server_name == r->hostname)
            server_name = r->server->server_hostname;
        /* Create a "Via:" request header entry and merge it */
        /* Generate outgoing Via: header with/without server comment: */
        kuda_table_mergen(r->headers_in, "Via",
                         (conf->viaopt == via_full)
                         ? kuda_psprintf(p, "%d.%d %s%s (%s)",
                                        HTTP_VERSION_MAJOR(r->proto_num),
                                        HTTP_VERSION_MINOR(r->proto_num),
                                        server_name, server_portstr,
                                        CLHY_SERVER_BASEVERSION)
                         : kuda_psprintf(p, "%d.%d %s%s",
                                        HTTP_VERSION_MAJOR(r->proto_num),
                                        HTTP_VERSION_MINOR(r->proto_num),
                                        server_name, server_portstr)
                         );
    }

    /* Use HTTP/1.1 100-Continue as quick "HTTP ping" test
     * to backend
     */
    if (do_100_continue) {
        const char *val;

        if (!r->expecting_100) {
            /* Don't forward any "100 Continue" response if the client is
             * not expecting it.
             */
            kuda_table_setn(r->subprocess_env, "proxy-interim-response",
                                              "Suppress");
        }

        /* Add the Expect header if not already there. */
        if (((val = kuda_table_get(r->headers_in, "Expect")) == NULL)
                || (strcasecmp(val, "100-Continue") != 0 /* fast path */
                    && !clhy_find_token(r->pool, val, "100-Continue"))) {
            kuda_table_mergen(r->headers_in, "Expect", "100-Continue");
        }
    }

    /* X-Forwarded-*: handling
     *
     * XXX Privacy Note:
     * -----------------
     *
     * These request headers are only really useful when the capi_proxy
     * is used in a reverse proxy configuration, so that useful info
     * about the client can be passed through the reverse proxy and on
     * to the backend server, which may require the information to
     * function properly.
     *
     * In a forward proxy situation, these options are a potential
     * privacy violation, as information about clients behind the proxy
     * are revealed to arbitrary servers out there on the internet.
     *
     * The HTTP/1.1 Via: header is designed for passing client
     * information through proxies to a server, and should be used in
     * a forward proxy configuration instead of X-Forwarded-*. See the
     * ProxyVia option for details.
     */
    if (dconf->add_forwarded_headers) {
        if (PROXYREQ_REVERSE == r->proxyreq) {
            const char *buf;

            /* Add X-Forwarded-For: so that the upstream has a chance to
             * determine, where the original request came from.
             */
            kuda_table_mergen(r->headers_in, "X-Forwarded-For",
                             r->useragent_ip);

            /* Add X-Forwarded-Host: so that upstream knows what the
             * original request hostname was.
             */
            if ((buf = kuda_table_get(r->headers_in, "Host"))) {
                kuda_table_mergen(r->headers_in, "X-Forwarded-Host", buf);
            }

            /* Add X-Forwarded-Server: so that upstream knows what the
             * name of this proxy server is (if there are more than one)
             * XXX: This duplicates Via: - do we strictly need it?
             */
            kuda_table_mergen(r->headers_in, "X-Forwarded-Server",
                             r->server->server_hostname);
        }
    }

    proxy_run_fixups(r);
    if (clhy_proxy_clear_connection(r, r->headers_in) < 0) {
        return HTTP_BAD_REQUEST;
    }

    /* send request headers */
    headers_in_array = kuda_table_elts(r->headers_in);
    headers_in = (const kuda_table_entry_t *) headers_in_array->elts;
    for (counter = 0; counter < headers_in_array->nelts; counter++) {
        if (headers_in[counter].key == NULL
            || headers_in[counter].val == NULL

            /* Already sent */
            || !strcasecmp(headers_in[counter].key, "Host")

            /* Clear out hop-by-hop request headers not to send
             * RFC2616 13.5.1 says we should strip these headers
             */
            || !strcasecmp(headers_in[counter].key, "Keep-Alive")
            || !strcasecmp(headers_in[counter].key, "TE")
            || !strcasecmp(headers_in[counter].key, "Trailer")
            || !strcasecmp(headers_in[counter].key, "Upgrade")

            ) {
            continue;
        }
        /* Do we want to strip Proxy-Authorization ?
         * If we haven't used it, then NO
         * If we have used it then MAYBE: RFC2616 says we MAY propagate it.
         * So let's make it configurable by env.
         */
        if (!strcasecmp(headers_in[counter].key,"Proxy-Authorization")) {
            if (r->user != NULL) { /* we've authenticated */
                if (!kuda_table_get(r->subprocess_env, "Proxy-Chain-Auth")) {
                    continue;
                }
            }
        }

        /* Skip Transfer-Encoding and Content-Length for now.
         */
        if (!strcasecmp(headers_in[counter].key, "Transfer-Encoding")) {
            *old_te_val = headers_in[counter].val;
            continue;
        }
        if (!strcasecmp(headers_in[counter].key, "Content-Length")) {
            *old_cl_val = headers_in[counter].val;
            continue;
        }

        /* for sub-requests, ignore freshness/expiry headers */
        if (r->main) {
            if (   !strcasecmp(headers_in[counter].key, "If-Match")
                || !strcasecmp(headers_in[counter].key, "If-Modified-Since")
                || !strcasecmp(headers_in[counter].key, "If-Range")
                || !strcasecmp(headers_in[counter].key, "If-Unmodified-Since")
                || !strcasecmp(headers_in[counter].key, "If-None-Match")) {
                continue;
            }
        }

        buf = kuda_pstrcat(p, headers_in[counter].key, ": ",
                          headers_in[counter].val, CRLF,
                          NULL);
        clhy_xlate_proto_to_ascii(buf, strlen(buf));
        e = kuda_bucket_pool_create(buf, strlen(buf), p, c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(header_brigade, e);
    }

    /* Restore the original headers in (see comment above),
     * we won't modify them anymore.
     */
    r->headers_in = saved_headers_in;
    return OK;
}

PROXY_DECLARE(int) clhy_proxy_pass_brigade(kuda_bucket_alloc_t *bucket_alloc,
                                         request_rec *r, proxy_conn_rec *p_conn,
                                         conn_rec *origin, kuda_bucket_brigade *bb,
                                         int flush)
{
    kuda_status_t status;
    kuda_off_t transferred;

    if (flush) {
        kuda_bucket *e = kuda_bucket_flush_create(bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, e);
    }
    kuda_brigade_length(bb, 0, &transferred);
    if (transferred != -1)
        p_conn->worker->s->transferred += transferred;
    status = clhy_pass_brigade(origin->output_filters, bb);
    /* Cleanup the brigade now to avoid buckets lifetime
     * issues in case of error returned below. */
    kuda_brigade_cleanup(bb);
    if (status != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01084)
                      "pass request body failed to %pI (%s)",
                      p_conn->addr, p_conn->hostname);
        if (origin->aborted) {
            const char *ssl_note;

            if (((ssl_note = kuda_table_get(origin->notes, "SSL_connect_rv"))
                 != NULL) && (strcmp(ssl_note, "err") == 0)) {
                return clhy_proxyerror(r, HTTP_INTERNAL_SERVER_ERROR,
                                     "Error during SSL Handshake with"
                                     " remote server");
            }
            return KUDA_STATUS_IS_TIMEUP(status) ? HTTP_GATEWAY_TIME_OUT : HTTP_BAD_GATEWAY;
        }
        else {
            return HTTP_BAD_REQUEST;
        }
    }
    return OK;
}

/* Fill in unknown schemes from kuda_uri_port_of_scheme() */

typedef struct proxy_schemes_t {
    const char *name;
    kuda_port_t default_port;
} proxy_schemes_t ;

static proxy_schemes_t pschemes[] =
{
    {"fcgi",     8000},
    {"ajp",      AJP13_DEF_PORT},
    {"scgi",     SCGI_DEF_PORT},
    {"h2c",      DEFAULT_HTTP_PORT},
    {"h2",       DEFAULT_HTTPS_PORT},
    { NULL, 0xFFFF }     /* unknown port */
};

PROXY_DECLARE(kuda_port_t) clhy_proxy_port_of_scheme(const char *scheme)
{
    if (scheme) {
        kuda_port_t port;
        if ((port = kuda_uri_port_of_scheme(scheme)) != 0) {
            return port;
        } else {
            proxy_schemes_t *pscheme;
            for (pscheme = pschemes; pscheme->name != NULL; ++pscheme) {
                if (strcasecmp(scheme, pscheme->name) == 0) {
                    return pscheme->default_port;
                }
            }
        }
    }
    return 0;
}

PROXY_DECLARE(kuda_status_t) clhy_proxy_buckets_lifetime_transform(request_rec *r,
                                                      kuda_bucket_brigade *from,
                                                      kuda_bucket_brigade *to)
{
    kuda_bucket *e;
    kuda_bucket *new;
    const char *data;
    kuda_size_t bytes;
    kuda_status_t rv = KUDA_SUCCESS;
    kuda_bucket_alloc_t *bucket_alloc = to->bucket_alloc;

    kuda_brigade_cleanup(to);
    for (e = KUDA_BRIGADE_FIRST(from);
         e != KUDA_BRIGADE_SENTINEL(from);
         e = KUDA_BUCKET_NEXT(e)) {
        if (!KUDA_BUCKET_IS_METADATA(e)) {
            kuda_bucket_read(e, &data, &bytes, KUDA_BLOCK_READ);
            new = kuda_bucket_transient_create(data, bytes, bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(to, new);
        }
        else if (KUDA_BUCKET_IS_FLUSH(e)) {
            new = kuda_bucket_flush_create(bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(to, new);
        }
        else if (KUDA_BUCKET_IS_EOS(e)) {
            new = kuda_bucket_eos_create(bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(to, new);
        }
        else {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(03304)
                          "Unhandled bucket type of type %s in"
                          " clhy_proxy_buckets_lifetime_transform", e->type->name);
            rv = KUDA_EGENERAL;
        }
    }
    return rv;
}

PROXY_DECLARE(kuda_status_t) clhy_proxy_transfer_between_connections(
                                                       request_rec *r,
                                                       conn_rec *c_i,
                                                       conn_rec *c_o,
                                                       kuda_bucket_brigade *bb_i,
                                                       kuda_bucket_brigade *bb_o,
                                                       const char *name,
                                                       int *sent,
                                                       kuda_off_t bsize,
                                                       int after)
{
    kuda_status_t rv;
#ifdef DEBUGGING
    kuda_off_t len;
#endif

    do {
        kuda_brigade_cleanup(bb_i);
        rv = clhy_get_brigade(c_i->input_filters, bb_i, CLHY_MODE_READBYTES,
                            KUDA_NONBLOCK_READ, bsize);
        if (rv == KUDA_SUCCESS) {
            if (c_o->aborted) {
                return KUDA_EPIPE;
            }
            if (KUDA_BRIGADE_EMPTY(bb_i)) {
                break;
            }
#ifdef DEBUGGING
            len = -1;
            kuda_brigade_length(bb_i, 0, &len);
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03306)
                          "clhy_proxy_transfer_between_connections: "
                          "read %" KUDA_OFF_T_FMT
                          " bytes from %s", len, name);
#endif
            if (sent) {
                *sent = 1;
            }
            clhy_proxy_buckets_lifetime_transform(r, bb_i, bb_o);
            if (!after) {
                kuda_bucket *b;

                /*
                 * Do not use clhy_fflush here since this would cause the flush
                 * bucket to be sent in a separate brigade afterwards which
                 * causes some filters to set aside the buckets from the first
                 * brigade and process them when the flush arrives in the second
                 * brigade. As set asides of our transformed buckets involve
                 * memory copying we try to avoid this. If we have the flush
                 * bucket in the first brigade they directly process the
                 * buckets without setting them aside.
                 */
                b = kuda_bucket_flush_create(bb_o->bucket_alloc);
                KUDA_BRIGADE_INSERT_TAIL(bb_o, b);
            }
            rv = clhy_pass_brigade(c_o->output_filters, bb_o);
            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(03307)
                              "clhy_proxy_transfer_between_connections: "
                              "error on %s - clhy_pass_brigade",
                              name);
            }
        } else if (!KUDA_STATUS_IS_EAGAIN(rv) && !KUDA_STATUS_IS_EOF(rv)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, r, CLHYLOGNO(03308)
                          "clhy_proxy_transfer_between_connections: "
                          "error on %s - clhy_get_brigade",
                          name);
        }
    } while (rv == KUDA_SUCCESS);

    if (after) {
        clhy_fflush(c_o->output_filters, bb_o);
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, rv, r,
                  "clhy_proxy_transfer_between_connections complete");

    if (KUDA_STATUS_IS_EAGAIN(rv)) {
        rv = KUDA_SUCCESS;
    }

    return rv;
}

PROXY_DECLARE (const char *) clhy_proxy_show_hcmethod(hcmethod_t method)
{
    proxy_hcmethods_t *m = proxy_hcmethods;
    for (; m->name; m++) {
        if (m->method == method) {
            return m->name;
        }
    }
    return "???";
}

void proxy_util_register_hooks(kuda_pool_t *p)
{
    KUDA_REGISTER_OPTIONAL_FN(clhy_proxy_retry_worker);
    KUDA_REGISTER_OPTIONAL_FN(clhy_proxy_clear_connection);
}
