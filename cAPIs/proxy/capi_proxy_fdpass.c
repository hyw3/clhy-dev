/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "capi_proxy.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#ifndef CMSG_DATA
#error This cAPI only works on unix platforms with the correct PLATFORM support
#endif

#include "capi_proxy_fdpass.h"

cAPI CLHY_CAPI_DECLARE_DATA proxy_fdpass_capi;

static int proxy_fdpass_canon(request_rec *r, char *url)
{
    const char *path;

    if (strncasecmp(url, "fd://", 5) == 0) {
        url += 5;
    }
    else {
        return DECLINED;
    }

    path = clhy_server_root_relative(r->pool, url);

    r->filename = kuda_pstrcat(r->pool, "proxy:fd://", path, NULL);

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01151)
                  "set r->filename to %s", r->filename);
    return OK;
}

static kuda_status_t get_socket_from_path(kuda_pool_t *p,
                                         const char* path,
                                         kuda_socket_t **out_sock)
{
    kuda_socket_t *s;
    kuda_status_t rv;
    *out_sock = NULL;

    rv = kuda_socket_create(&s, AF_UNIX, SOCK_STREAM, 0, p);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    rv = clhy_proxy_connect_uds(s, path, p);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    *out_sock = s;

    return KUDA_SUCCESS;
}

static kuda_status_t send_socket(kuda_pool_t *p,
                                kuda_socket_t *s,
                                kuda_socket_t *outbound)
{
    kuda_status_t rv;
    kuda_platform_sock_t rawsock;
    kuda_platform_sock_t srawsock;
    struct msghdr msg;
    struct cmsghdr *cmsg;
    struct iovec iov;
    char b = '\0';

    rv = kuda_platform_sock_get(&rawsock, outbound);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    rv = kuda_platform_sock_get(&srawsock, s);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    memset(&msg, 0, sizeof(msg));

    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    iov.iov_base = &b;
    iov.iov_len = 1;

    cmsg = kuda_palloc(p, sizeof(*cmsg) + sizeof(rawsock));
    cmsg->cmsg_len = sizeof(*cmsg) + sizeof(rawsock);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;

    memcpy(CMSG_DATA(cmsg), &rawsock, sizeof(rawsock));

    msg.msg_control = cmsg;
    msg.msg_controllen = cmsg->cmsg_len;

    rv = sendmsg(srawsock, &msg, 0);

    if (rv == -1) {
        return errno;
    }

    return KUDA_SUCCESS;
}

static int proxy_fdpass_handler(request_rec *r, proxy_worker *worker,
                              proxy_server_conf *conf,
                              char *url, const char *proxyname,
                              kuda_port_t proxyport)
{
    kuda_status_t rv;
    kuda_socket_t *sock;
    kuda_socket_t *clientsock;

    if (strncasecmp(url, "fd://", 5) == 0) {
        url += 5;
    }
    else {
        return DECLINED;
    }

    rv = get_socket_from_path(r->pool, url, &sock);

    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01152)
                      "Failed to connect to '%s'", url);
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    {
        int status;
        const char *flush_method = *worker->s->flusher ? worker->s->flusher : "flush";

        proxy_fdpass_flush *flush = clhy_lookup_provider(PROXY_FDPASS_FLUSHER,
                                                       flush_method, "0");

        if (!flush) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01153)
                          "Unable to find configured flush provider '%s'",
                          flush_method);
            return HTTP_INTERNAL_SERVER_ERROR;
        }

        status = flush->flusher(r);
        if (status) {
            return status;
        }
    }

    clientsock = clhy_get_conn_socket(r->connection);

    rv = send_socket(r->pool, sock, clientsock);
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01154) "send_socket failed:");
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    {
        kuda_socket_t *dummy;
        /* Create a dummy unconnected socket, and set it as the one we were
         * connected to, so that when the core closes it, it doesn't close
         * the tcp connection to the client.
         */
        rv = kuda_socket_create(&dummy, KUDA_INET, SOCK_STREAM, KUDA_PROTO_TCP,
                               r->connection->pool);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01155)
                          "failed to create dummy socket");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
        clhy_set_core_capi_config(r->connection->conn_config, dummy);
    }

    return OK;
}

static int standard_flush(request_rec *r)
{
    int status;
    kuda_bucket_brigade *bb;
    kuda_bucket *e;

    r->connection->keepalive = CLHY_CONN_CLOSE;

    bb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
    e = kuda_bucket_flush_create(r->connection->bucket_alloc);

    KUDA_BRIGADE_INSERT_TAIL(bb, e);

    status = clhy_pass_brigade(r->output_filters, bb);

    if (status != OK) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01156)
                      "clhy_pass_brigade failed:");
        return status;
    }

    return OK;
}


static const proxy_fdpass_flush builtin_flush =
{
    "flush",
    &standard_flush,
    NULL
};

static void register_hooks(kuda_pool_t *p)
{
    clhy_register_provider(p, PROXY_FDPASS_FLUSHER, "flush", "0", &builtin_flush);
    proxy_hook_scheme_handler(proxy_fdpass_handler, NULL, NULL, KUDA_HOOK_FIRST);
    proxy_hook_canon_handler(proxy_fdpass_canon, NULL, NULL, KUDA_HOOK_FIRST);
}

CLHY_DECLARE_CAPI(proxy_fdpass) = {
    STANDARD16_CAPI_STUFF,
    NULL,                       /* create per-directory config structure */
    NULL,                       /* merge per-directory config structures */
    NULL,                       /* create per-server config structure */
    NULL,                       /* merge per-server config structures */
    NULL,                       /* command kuda_table_t */
    register_hooks              /* register hooks */
};
