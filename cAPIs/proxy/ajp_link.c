/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ajp.h"

CLHYLOG_USE_CAPI(proxy_ajp);

kuda_status_t ajp_ilink_send(kuda_socket_t *sock, ajp_msg_t *msg)
{
    char         *buf;
    kuda_status_t status;
    kuda_size_t   length;

    ajp_msg_end(msg);

    length = msg->len;
    buf    = (char *)msg->buf;

    do {
        kuda_size_t written = length;

        status = kuda_socket_send(sock, buf, &written);
        if (status != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, status, NULL, CLHYLOGNO(01029)
                          "ajp_ilink_send(): send failed");
            return status;
        }
        length -= written;
        buf    += written;
    } while (length);

    return KUDA_SUCCESS;
}


static kuda_status_t ilink_read(kuda_socket_t *sock, kuda_byte_t *buf,
                               kuda_size_t len)
{
    kuda_size_t   length = len;
    kuda_size_t   rdlen  = 0;
    kuda_status_t status;

    while (rdlen < len) {

        status = kuda_socket_recv(sock, (char *)(buf + rdlen), &length);

        if (status == KUDA_EOF)
            return status;          /* socket closed. */
        else if (KUDA_STATUS_IS_EAGAIN(status))
            continue;
        else if (status != KUDA_SUCCESS)
            return status;          /* any error. */

        rdlen += length;
        length = len - rdlen;
    }
    return KUDA_SUCCESS;
}


kuda_status_t ajp_ilink_receive(kuda_socket_t *sock, ajp_msg_t *msg)
{
    kuda_status_t status;
    kuda_size_t   hlen;
    kuda_size_t   blen;

    hlen = msg->header_len;

    status = ilink_read(sock, msg->buf, hlen);

    if (status != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, status, NULL, CLHYLOGNO(01030)
                     "ajp_ilink_receive() can't receive header");
        return (KUDA_STATUS_IS_TIMEUP(status) ? KUDA_TIMEUP : AJP_ENO_HEADER);
    }

    status = ajp_msg_check_header(msg, &blen);

    if (status != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, NULL, CLHYLOGNO(01031)
                     "ajp_ilink_receive() received bad header");
        return AJP_EBAD_HEADER;
    }

    status = ilink_read(sock, msg->buf + hlen, blen);

    if (status != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, status, NULL, CLHYLOGNO(01032)
                     "ajp_ilink_receive() error while receiving message body "
                     "of length %" KUDA_SIZE_T_FMT,
                     hlen);
        return AJP_EBAD_MESSAGE;
    }

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, NULL, CLHYLOGNO(01033)
                 "ajp_ilink_receive() received packet len=%" KUDA_SIZE_T_FMT
                 "type=%d",
                  blen, (int)msg->buf[hlen]);

    return KUDA_SUCCESS;
}

