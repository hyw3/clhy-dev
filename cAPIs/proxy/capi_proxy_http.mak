# Microsoft Developer Studio Generated NMAKE File, Based on capi_proxy_http.dsp
!IF "$(CFG)" == ""
CFG=capi_proxy_http - Win32 Release
!MESSAGE No configuration specified. Defaulting to capi_proxy_http - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "capi_proxy_http - Win32 Release" && "$(CFG)" != "capi_proxy_http - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "capi_proxy_http.mak" CFG="capi_proxy_http - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "capi_proxy_http - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "capi_proxy_http - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "capi_proxy_http - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_proxy_http.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "capi_proxy - Win32 Release" "libwwhy - Win32 Release" "libkudadelman - Win32 Release" "libkuda - Win32 Release" "$(OUTDIR)\capi_proxy_http.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudadelman - Win32 ReleaseCLEAN" "libwwhy - Win32 ReleaseCLEAN" "capi_proxy - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\capi_proxy_http.obj"
	-@erase "$(INTDIR)\capi_proxy_http.res"
	-@erase "$(INTDIR)\capi_proxy_http_src.idb"
	-@erase "$(INTDIR)\capi_proxy_http_src.pdb"
	-@erase "$(OUTDIR)\capi_proxy_http.exp"
	-@erase "$(OUTDIR)\capi_proxy_http.lib"
	-@erase "$(OUTDIR)\capi_proxy_http.pdb"
	-@erase "$(OUTDIR)\capi_proxy_http.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_proxy_http_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_proxy_http.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "NDEBUG" /d BIN_NAME="capi_proxy_http.so" /d LONG_NAME="proxy_http_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_proxy_http.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib ws2_32.lib mswsock.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_proxy_http.pdb" /debug /out:"$(OUTDIR)\capi_proxy_http.so" /implib:"$(OUTDIR)\capi_proxy_http.lib" /base:@..\..\platforms\win32\BaseAddr.ref,capi_proxy_http.so /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\capi_proxy_http.obj" \
	"$(INTDIR)\capi_proxy_http.res" \
	"..\..\kudelrunsrc\kuda\Release\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Release\libkudadelman-1.lib" \
	"..\..\Release\libwwhy.lib" \
	"$(OUTDIR)\capi_proxy.lib"

"$(OUTDIR)\capi_proxy_http.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\capi_proxy_http.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_proxy_http.so"
   if exist .\Release\capi_proxy_http.so.manifest mt.exe -manifest .\Release\capi_proxy_http.so.manifest -outputresource:.\Release\capi_proxy_http.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "capi_proxy_http - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_proxy_http.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "capi_proxy - Win32 Debug" "libwwhy - Win32 Debug" "libkudadelman - Win32 Debug" "libkuda - Win32 Debug" "$(OUTDIR)\capi_proxy_http.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudadelman - Win32 DebugCLEAN" "libwwhy - Win32 DebugCLEAN" "capi_proxy - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\capi_proxy_http.obj"
	-@erase "$(INTDIR)\capi_proxy_http.res"
	-@erase "$(INTDIR)\capi_proxy_http_src.idb"
	-@erase "$(INTDIR)\capi_proxy_http_src.pdb"
	-@erase "$(OUTDIR)\capi_proxy_http.exp"
	-@erase "$(OUTDIR)\capi_proxy_http.lib"
	-@erase "$(OUTDIR)\capi_proxy_http.pdb"
	-@erase "$(OUTDIR)\capi_proxy_http.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_proxy_http_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_proxy_http.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "_DEBUG" /d BIN_NAME="capi_proxy_http.so" /d LONG_NAME="proxy_http_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_proxy_http.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib ws2_32.lib mswsock.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_proxy_http.pdb" /debug /out:"$(OUTDIR)\capi_proxy_http.so" /implib:"$(OUTDIR)\capi_proxy_http.lib" /base:@..\..\platforms\win32\BaseAddr.ref,capi_proxy_http.so 
LINK32_OBJS= \
	"$(INTDIR)\capi_proxy_http.obj" \
	"$(INTDIR)\capi_proxy_http.res" \
	"..\..\kudelrunsrc\kuda\Debug\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Debug\libkudadelman-1.lib" \
	"..\..\Debug\libwwhy.lib" \
	"$(OUTDIR)\capi_proxy.lib"

"$(OUTDIR)\capi_proxy_http.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\capi_proxy_http.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_proxy_http.so"
   if exist .\Debug\capi_proxy_http.so.manifest mt.exe -manifest .\Debug\capi_proxy_http.so.manifest -outputresource:.\Debug\capi_proxy_http.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("capi_proxy_http.dep")
!INCLUDE "capi_proxy_http.dep"
!ELSE 
!MESSAGE Warning: cannot find "capi_proxy_http.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "capi_proxy_http - Win32 Release" || "$(CFG)" == "capi_proxy_http - Win32 Debug"
SOURCE=.\capi_proxy_http.c

"$(INTDIR)\capi_proxy_http.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "capi_proxy_http - Win32 Release"

"libkuda - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\..\cAPIs\proxy"

"libkuda - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\proxy"

!ELSEIF  "$(CFG)" == "capi_proxy_http - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\..\cAPIs\proxy"

"libkuda - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\proxy"

!ENDIF 

!IF  "$(CFG)" == "capi_proxy_http - Win32 Release"

"libkudadelman - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" 
   cd "..\..\cAPIs\proxy"

"libkudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\proxy"

!ELSEIF  "$(CFG)" == "capi_proxy_http - Win32 Debug"

"libkudadelman - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" 
   cd "..\..\cAPIs\proxy"

"libkudadelman - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\proxy"

!ENDIF 

!IF  "$(CFG)" == "capi_proxy_http - Win32 Release"

"libwwhy - Win32 Release" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" 
   cd ".\cAPIs\proxy"

"libwwhy - Win32 ReleaseCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" RECURSE=1 CLEAN 
   cd ".\cAPIs\proxy"

!ELSEIF  "$(CFG)" == "capi_proxy_http - Win32 Debug"

"libwwhy - Win32 Debug" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" 
   cd ".\cAPIs\proxy"

"libwwhy - Win32 DebugCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" RECURSE=1 CLEAN 
   cd ".\cAPIs\proxy"

!ENDIF 

!IF  "$(CFG)" == "capi_proxy_http - Win32 Release"

"capi_proxy - Win32 Release" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_proxy.mak" CFG="capi_proxy - Win32 Release" 
   cd "."

"capi_proxy - Win32 ReleaseCLEAN" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_proxy.mak" CFG="capi_proxy - Win32 Release" RECURSE=1 CLEAN 
   cd "."

!ELSEIF  "$(CFG)" == "capi_proxy_http - Win32 Debug"

"capi_proxy - Win32 Debug" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_proxy.mak" CFG="capi_proxy - Win32 Debug" 
   cd "."

"capi_proxy - Win32 DebugCLEAN" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_proxy.mak" CFG="capi_proxy - Win32 Debug" RECURSE=1 CLEAN 
   cd "."

!ENDIF 

SOURCE=..\..\build\win32\wwhy.rc

!IF  "$(CFG)" == "capi_proxy_http - Win32 Release"


"$(INTDIR)\capi_proxy_http.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_proxy_http.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../build\win32" /d "NDEBUG" /d BIN_NAME="capi_proxy_http.so" /d LONG_NAME="proxy_http_capi for cLHy" $(SOURCE)


!ELSEIF  "$(CFG)" == "capi_proxy_http - Win32 Debug"


"$(INTDIR)\capi_proxy_http.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_proxy_http.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../build\win32" /d "_DEBUG" /d BIN_NAME="capi_proxy_http.so" /d LONG_NAME="proxy_http_capi for cLHy" $(SOURCE)


!ENDIF 


!ENDIF 

