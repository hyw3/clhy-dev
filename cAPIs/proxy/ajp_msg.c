/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ajp.h"

CLHYLOG_USE_CAPI(proxy_ajp);

#define AJP_MSG_DUMP_BYTES_PER_LINE 16
/* 2 hex digits plus space plus one char per dumped byte */
/* plus prefix plus separator plus '\0' */
#define AJP_MSG_DUMP_PREFIX_LENGTH  strlen("XXXX    ")
#define AJP_MSG_DUMP_LINE_LENGTH    ((AJP_MSG_DUMP_BYTES_PER_LINE * \
                                    strlen("XX .")) + \
                                    AJP_MSG_DUMP_PREFIX_LENGTH + \
                                    strlen(" - ") + 1)

static char *hex_table = "0123456789ABCDEF";

/**
 * Dump the given number of bytes on an AJP Message
 *
 * @param pool      pool to allocate from
 * @param msg       AJP Message to dump
 * @param err       error string to display
 * @param count     the number of bytes to dump
 * @param buf       buffer pointer for dump message
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_dump(kuda_pool_t *pool, ajp_msg_t *msg, char *err,
                          kuda_size_t count, char **buf)
{
    kuda_size_t  i, j;
    char        *current;
    kuda_size_t  bl, rl;
    kuda_byte_t  x;
    kuda_size_t  len = msg->len;
    kuda_size_t  line_len;

    /* Display only first "count" bytes */
    if (len > count)
        len = count;
         /* First the space needed for the first line */
    bl = strlen(err) + 3 * (strlen(" XXX=") + 20) + 1 +
         /* Now for the data lines */
         (len + 15) / 16 * AJP_MSG_DUMP_LINE_LENGTH;
    *buf = kuda_palloc(pool, bl);
    if (!*buf)
        return KUDA_ENOMEM;
    kuda_snprintf(*buf, bl,
                 "%s pos=%" KUDA_SIZE_T_FMT
                 " len=%" KUDA_SIZE_T_FMT " max=%" KUDA_SIZE_T_FMT "\n",
                 err, msg->pos, msg->len, msg->max_size);
    current = *buf + strlen(*buf);
    for (i = 0; i < len; i += AJP_MSG_DUMP_BYTES_PER_LINE) {
        /* Safety check: do we have enough buffer for another line? */
        rl = bl - (current - *buf);
        if (AJP_MSG_DUMP_LINE_LENGTH > rl) {
            *(current - 1) = '\0';
            return KUDA_ENOMEM;
        }
        kuda_snprintf(current, rl, "%.4lx    ", (unsigned long)i);
        current += AJP_MSG_DUMP_PREFIX_LENGTH;
        line_len = len - i;
        if (line_len > AJP_MSG_DUMP_BYTES_PER_LINE) {
            line_len = AJP_MSG_DUMP_BYTES_PER_LINE;
        }
        for (j = 0; j < line_len; j++) {
            x = msg->buf[i + j];

            *current++ = hex_table[x >> 4];
            *current++ = hex_table[x & 0x0f];
            *current++ = ' ';
        }
        *current++ = ' ';
        *current++ = '-';
        *current++ = ' ';
        for (j = 0; j < line_len; j++) {
            x = msg->buf[i + j];

            if (x > 0x20 && x < 0x7F) {
                *current++ = x;
            }
            else {
                *current++ = '.';
            }
        }
        *current++ = '\n';
    }
    *(current - 1) = '\0';

    return KUDA_SUCCESS;
}

/**
 * Log an AJP message
 *
 * @param request   The current request
 * @param msg       AJP Message to dump
 * @param err       error string to display
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_log(request_rec *r, ajp_msg_t *msg, char *err)
{
    int level;
    kuda_size_t count;
    char *buf, *next;
    kuda_status_t rc = KUDA_SUCCESS;

    if (CLHYLOGrtrace7(r)) {
        level = CLHYLOG_TRACE7;
        count = 1024;
        if (CLHYLOGrtrace8(r)) {
            level = CLHYLOG_TRACE8;
            count = AJP_MAX_BUFFER_SZ;
        }
        rc = ajp_msg_dump(r->pool, msg, err, count, &buf);
        if (rc == KUDA_SUCCESS) {
            while ((next = clhy_strchr(buf, '\n'))) {
                *next = '\0';
                /* Intentional no CLHYLOGNO */
                clhy_log_rerror(CLHYLOG_MARK, level, 0, r, "%s", buf);
                buf = next + 1;
            }
            /* Intentional no CLHYLOGNO */
            clhy_log_rerror(CLHYLOG_MARK, level, 0, r, "%s", buf);
        }
    }
    return rc;
}

/**
 * Check a new AJP Message by looking at signature and return its size
 *
 * @param msg       AJP Message to check
 * @param len       Pointer to returned len
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_check_header(ajp_msg_t *msg, kuda_size_t *len)
{
    kuda_byte_t *head = msg->buf;
    kuda_size_t msglen;

    if (!((head[0] == 0x41 && head[1] == 0x42) ||
          (head[0] == 0x12 && head[1] == 0x34))) {

        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, NULL, CLHYLOGNO(01080)
                      "ajp_msg_check_header() got bad signature %02x%02x",
                      head[0], head[1]);

        return AJP_EBAD_SIGNATURE;
    }

    msglen  = ((head[2] & 0xff) << 8);
    msglen += (head[3] & 0xFF);

    if (msglen > msg->max_size) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, NULL, CLHYLOGNO(01081)
                     "ajp_msg_check_header() incoming message is "
                     "too big %" KUDA_SIZE_T_FMT ", max is %" KUDA_SIZE_T_FMT,
                     msglen, msg->max_size);
        return AJP_ETOBIG;
    }

    msg->len = msglen + AJP_HEADER_LEN;
    msg->pos = AJP_HEADER_LEN;
    *len     = msglen;

    return KUDA_SUCCESS;
}

/**
 * Reset an AJP Message
 *
 * @param msg       AJP Message to reset
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_reset(ajp_msg_t *msg)
{
    msg->len = AJP_HEADER_LEN;
    msg->pos = AJP_HEADER_LEN;

    return KUDA_SUCCESS;
}

/**
 * Reuse an AJP Message
 *
 * @param msg       AJP Message to reuse
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_reuse(ajp_msg_t *msg)
{
    kuda_byte_t *buf;
    kuda_size_t max_size;

    buf = msg->buf;
    max_size = msg->max_size;
    memset(msg, 0, sizeof(ajp_msg_t));
    msg->buf = buf;
    msg->max_size = max_size;
    msg->header_len = AJP_HEADER_LEN;
    ajp_msg_reset(msg);
    return KUDA_SUCCESS;
}

/**
 * Mark the end of an AJP Message
 *
 * @param msg       AJP Message to end
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_end(ajp_msg_t *msg)
{
    kuda_size_t len = msg->len - AJP_HEADER_LEN;

    if (msg->server_side) {
        msg->buf[0] = 0x41;
        msg->buf[1] = 0x42;
    }
    else {
        msg->buf[0] = 0x12;
        msg->buf[1] = 0x34;
    }

    msg->buf[2] = (kuda_byte_t)((len >> 8) & 0xFF);
    msg->buf[3] = (kuda_byte_t)(len & 0xFF);

    return KUDA_SUCCESS;
}

static KUDA_INLINE int ajp_log_overflow(ajp_msg_t *msg, const char *context)
{
    clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, NULL, CLHYLOGNO(03229)
                 "%s(): BufferOverflowException %" KUDA_SIZE_T_FMT
                 " %" KUDA_SIZE_T_FMT,
                 context, msg->pos, msg->len);
    return AJP_EOVERFLOW;
}

/**
 * Add an unsigned 32bits value to AJP Message
 *
 * @param msg       AJP Message to get value from
 * @param value     value to add to AJP Message
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_append_uint32(ajp_msg_t *msg, kuda_uint32_t value)
{
    kuda_size_t len = msg->len;

    if ((len + 4) > msg->max_size) {
        return ajp_log_overflow(msg, "ajp_msg_append_uint32");
    }

    msg->buf[len]     = (kuda_byte_t)((value >> 24) & 0xFF);
    msg->buf[len + 1] = (kuda_byte_t)((value >> 16) & 0xFF);
    msg->buf[len + 2] = (kuda_byte_t)((value >> 8) & 0xFF);
    msg->buf[len + 3] = (kuda_byte_t)(value & 0xFF);

    msg->len += 4;

    return KUDA_SUCCESS;
}

/**
 * Add an unsigned 16bits value to AJP Message
 *
 * @param msg       AJP Message to get value from
 * @param value     value to add to AJP Message
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_append_uint16(ajp_msg_t *msg, kuda_uint16_t value)
{
    kuda_size_t len = msg->len;

    if ((len + 2) > msg->max_size) {
        return ajp_log_overflow(msg, "ajp_msg_append_uint16");
    }

    msg->buf[len]     = (kuda_byte_t)((value >> 8) & 0xFF);
    msg->buf[len + 1] = (kuda_byte_t)(value & 0xFF);

    msg->len += 2;

    return KUDA_SUCCESS;
}

/**
 * Add an unsigned 8bits value to AJP Message
 *
 * @param msg       AJP Message to get value from
 * @param value     value to add to AJP Message
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_append_uint8(ajp_msg_t *msg, kuda_byte_t value)
{
    kuda_size_t len = msg->len;

    if ((len + 1) > msg->max_size) {
        return ajp_log_overflow(msg, "ajp_msg_append_uint8");
    }

    msg->buf[len] = value;
    msg->len += 1;

    return KUDA_SUCCESS;
}

/**
 *  Add a String in AJP message, and transform the String in ASCII
 *  if convert is set and we're on an EBCDIC machine
 *
 * @param msg       AJP Message to get value from
 * @param value     Pointer to String
 * @param convert   When set told to convert String to ASCII
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_append_string_ex(ajp_msg_t *msg, const char *value,
                                      int convert)
{
    kuda_size_t len;

    if (value == NULL) {
        return(ajp_msg_append_uint16(msg, 0xFFFF));
    }

    len = strlen(value);
    if ((msg->len + len + 3) > msg->max_size) {
        return ajp_log_overflow(msg, "ajp_msg_append_cvt_string");
    }

    /* ignore error - we checked once */
    ajp_msg_append_uint16(msg, (kuda_uint16_t)len);

    /* We checked for space !!  */
    memcpy(msg->buf + msg->len, value, len + 1); /* including \0 */

    if (convert) {
        /* convert from EBCDIC if needed */
        clhy_xlate_proto_to_ascii((char *)msg->buf + msg->len, len + 1);
    }

    msg->len += len + 1;

    return KUDA_SUCCESS;
}

/**
 * Add a Byte array to AJP Message
 *
 * @param msg       AJP Message to get value from
 * @param value     Pointer to Byte array
 * @param valuelen  Byte array len
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_append_bytes(ajp_msg_t *msg, const kuda_byte_t *value,
                                  kuda_size_t valuelen)
{
    if (! valuelen) {
        return KUDA_SUCCESS; /* Shouldn't we indicate an error ? */
    }

    if ((msg->len + valuelen) > msg->max_size) {
        return ajp_log_overflow(msg, "ajp_msg_append_bytes");
    }

    /* We checked for space !!  */
    memcpy(msg->buf + msg->len, value, valuelen);
    msg->len += valuelen;

    return KUDA_SUCCESS;
}

/**
 * Get a 32bits unsigned value from AJP Message
 *
 * @param msg       AJP Message to get value from
 * @param rvalue    Pointer where value will be returned
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_get_uint32(ajp_msg_t *msg, kuda_uint32_t *rvalue)
{
    kuda_uint32_t value;

    if ((msg->pos + 3) > msg->len) {
        return ajp_log_overflow(msg, "ajp_msg_get_uint32");
    }

    value  = ((msg->buf[(msg->pos++)] & 0xFF) << 24);
    value |= ((msg->buf[(msg->pos++)] & 0xFF) << 16);
    value |= ((msg->buf[(msg->pos++)] & 0xFF) << 8);
    value |= ((msg->buf[(msg->pos++)] & 0xFF));

    *rvalue = value;
    return KUDA_SUCCESS;
}


/**
 * Get a 16bits unsigned value from AJP Message
 *
 * @param msg       AJP Message to get value from
 * @param rvalue    Pointer where value will be returned
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_get_uint16(ajp_msg_t *msg, kuda_uint16_t *rvalue)
{
    kuda_uint16_t value;

    if ((msg->pos + 1) > msg->len) {
        return ajp_log_overflow(msg, "ajp_msg_get_uint16");
    }

    value  = ((msg->buf[(msg->pos++)] & 0xFF) << 8);
    value += ((msg->buf[(msg->pos++)] & 0xFF));

    *rvalue = value;
    return KUDA_SUCCESS;
}

/**
 * Peek a 16bits unsigned value from AJP Message, position in message
 * is not updated
 *
 * @param msg       AJP Message to get value from
 * @param rvalue    Pointer where value will be returned
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_peek_uint16(ajp_msg_t *msg, kuda_uint16_t *rvalue)
{
    kuda_uint16_t value;

    if ((msg->pos + 1) > msg->len) {
        return ajp_log_overflow(msg, "ajp_msg_peek_uint16");
    }

    value = ((msg->buf[(msg->pos)] & 0xFF) << 8);
    value += ((msg->buf[(msg->pos + 1)] & 0xFF));

    *rvalue = value;
    return KUDA_SUCCESS;
}

/**
 * Peek a 8bits unsigned value from AJP Message, position in message
 * is not updated
 *
 * @param msg       AJP Message to get value from
 * @param rvalue    Pointer where value will be returned
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_peek_uint8(ajp_msg_t *msg, kuda_byte_t *rvalue)
{
    if (msg->pos > msg->len) {
        return ajp_log_overflow(msg, "ajp_msg_peek_uint8");
    }

    *rvalue = msg->buf[msg->pos];
    return KUDA_SUCCESS;
}

/**
 * Get a 8bits unsigned value from AJP Message
 *
 * @param msg       AJP Message to get value from
 * @param rvalue    Pointer where value will be returned
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_get_uint8(ajp_msg_t *msg, kuda_byte_t *rvalue)
{

    if (msg->pos > msg->len) {
        return ajp_log_overflow(msg, "ajp_msg_get_uint8");
    }

    *rvalue = msg->buf[msg->pos++];
    return KUDA_SUCCESS;
}


/**
 * Get a String value from AJP Message
 *
 * @param msg       AJP Message to get value from
 * @param rvalue    Pointer where value will be returned
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_get_string(ajp_msg_t *msg, const char **rvalue)
{
    kuda_uint16_t size;
    kuda_size_t   start;
    kuda_status_t status;

    status = ajp_msg_get_uint16(msg, &size);
    start = msg->pos;

    if ((status != KUDA_SUCCESS) || (size + start > msg->max_size)) {
        return ajp_log_overflow(msg, "ajp_msg_get_string");
    }

    msg->pos += (kuda_size_t)size;
    msg->pos++;                   /* a String in AJP is NULL terminated */

    *rvalue = (const char *)(msg->buf + start);
    return KUDA_SUCCESS;
}


/**
 * Get a Byte array from AJP Message
 *
 * @param msg       AJP Message to get value from
 * @param rvalue    Pointer where value will be returned
 * @param rvalueLen Pointer where Byte array len will be returned
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_get_bytes(ajp_msg_t *msg, kuda_byte_t **rvalue,
                               kuda_size_t *rvalue_len)
{
    kuda_uint16_t size;
    kuda_size_t   start;
    kuda_status_t status;

    status = ajp_msg_get_uint16(msg, &size);
    /* save the current position */
    start = msg->pos;

    if ((status != KUDA_SUCCESS) || (size + start > msg->max_size)) {
        return ajp_log_overflow(msg, "ajp_msg_get_bytes");
    }
    msg->pos += (kuda_size_t)size;   /* only bytes, no trailer */

    *rvalue     = msg->buf + start;
    *rvalue_len = size;

    return KUDA_SUCCESS;
}


/**
 * Create an AJP Message from pool
 *
 * @param pool      memory pool to allocate AJP message from
 * @param size      size of the buffer to create
 * @param rmsg      Pointer to newly created AJP message
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_create(kuda_pool_t *pool, kuda_size_t size, ajp_msg_t **rmsg)
{
    ajp_msg_t *msg = (ajp_msg_t *)kuda_pcalloc(pool, sizeof(ajp_msg_t));

    msg->server_side = 0;

    msg->buf = (kuda_byte_t *)kuda_palloc(pool, size);
    msg->len = 0;
    msg->header_len = AJP_HEADER_LEN;
    msg->max_size = size;
    *rmsg = msg;

    return KUDA_SUCCESS;
}

/**
 * Recopy an AJP Message to another
 *
 * @param smsg      source AJP message
 * @param dmsg      destination AJP message
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_copy(ajp_msg_t *smsg, ajp_msg_t *dmsg)
{
    if (smsg->len > smsg->max_size) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, NULL, CLHYLOGNO(01082)
                     "ajp_msg_copy(): destination buffer too "
                     "small %" KUDA_SIZE_T_FMT ", max size is %" KUDA_SIZE_T_FMT,
                     smsg->len, smsg->max_size);
        return  AJP_ETOSMALL;
    }

    memcpy(dmsg->buf, smsg->buf, smsg->len);
    dmsg->len = smsg->len;
    dmsg->pos = smsg->pos;

    return KUDA_SUCCESS;
}


/**
 * Serialize in an AJP Message a PING command
 *
 * +-----------------------+
 * | PING CMD (1 byte)     |
 * +-----------------------+
 *
 * @param smsg      AJP message to put serialized message
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_serialize_ping(ajp_msg_t *msg)
{
    kuda_status_t rc;
    ajp_msg_reset(msg);

    if ((rc = ajp_msg_append_uint8(msg, CMD_AJP13_PING)) != KUDA_SUCCESS)
        return rc;

    return KUDA_SUCCESS;
}

/**
 * Serialize in an AJP Message a CPING command
 *
 * +-----------------------+
 * | CPING CMD (1 byte)    |
 * +-----------------------+
 *
 * @param smsg      AJP message to put serialized message
 * @return          KUDA_SUCCESS or error
 */
kuda_status_t ajp_msg_serialize_cping(ajp_msg_t *msg)
{
    kuda_status_t rc;
    ajp_msg_reset(msg);

    if ((rc = ajp_msg_append_uint8(msg, CMD_AJP13_CPING)) != KUDA_SUCCESS)
        return rc;

    return KUDA_SUCCESS;
}
