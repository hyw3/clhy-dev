dnl cAPIs enabled in this directory by default

CLHYKUDEL_CAPIPATH_INIT(proxy)

proxy_objs="capi_proxy.lo proxy_util.lo"
CLHYKUDEL_CAPI(proxy, cLHy proxy cAPI, $proxy_objs, , most)

dnl set aside cAPI selections and default, and set the cAPI default to the
dnl same scope (shared|static) as selected for capi_proxy, along with setting
dnl the default selection to "most" for remaining proxy cAPIs, mirroring the
dnl earlier behavior, but failing ./configure only if an explicitly
dnl enabled cAPI is missing its prereqs
save_capi_selection=$capi_selection
save_capi_default=$capi_default
if test "$enable_proxy" != "no"; then
    capi_selection=most
    if test "$enable_proxy" = "shared" -o "$enable_proxy" = "static"; then
        capi_default=$enable_proxy
    fi
fi

proxy_connect_objs="capi_proxy_connect.lo"
proxy_ftp_objs="capi_proxy_ftp.lo"
proxy_http_objs="capi_proxy_http.lo"
proxy_fcgi_objs="capi_proxy_fcgi.lo"
proxy_scgi_objs="capi_proxy_scgi.lo"
proxy_uwsgi_objs="capi_proxy_uwsgi.lo"
proxy_fdpass_objs="capi_proxy_fdpass.lo"
proxy_ajp_objs="capi_proxy_ajp.lo ajp_header.lo ajp_link.lo ajp_msg.lo ajp_utils.lo"
proxy_wstunnel_objs="capi_proxy_wstunnel.lo"
proxy_balancer_objs="capi_proxy_balancer.lo"

case "$host" in
  *os2*)
    # OS2 DLLs must resolve all symbols at build time and
    # these sub-cAPIs need some from the main proxy cAPI
    proxy_connect_objs="$proxy_connect_objs capi_proxy.la"
    proxy_ftp_objs="$proxy_ftp_objs capi_proxy.la"
    proxy_http_objs="$proxy_http_objs capi_proxy.la"
    proxy_fcgi_objs="$proxy_fcgi_objs capi_proxy.la"
    proxy_scgi_objs="$proxy_scgi_objs capi_proxy.la"
    proxy_uwsgi_objs="$proxy_uwsgi_objs capi_proxy.la"
    proxy_fdpass_objs="$proxy_fdpass_objs capi_proxy.la"
    proxy_ajp_objs="$proxy_ajp_objs capi_proxy.la"
    proxy_wstunnel_objs="$proxy_wstunnel_objs capi_proxy.la"
    proxy_balancer_objs="$proxy_balancer_objs capi_proxy.la"
    ;;
esac

CLHYKUDEL_CAPI(proxy_connect, cLHy proxy CONNECT cAPI.  Requires --enable-proxy., $proxy_connect_objs, , most, , proxy)
CLHYKUDEL_CAPI(proxy_ftp, cLHy proxy FTP cAPI.  Requires --enable-proxy., $proxy_ftp_objs, , most, , proxy)
CLHYKUDEL_CAPI(proxy_http, cLHy proxy HTTP cAPI.  Requires --enable-proxy., $proxy_http_objs, , most, , proxy)
CLHYKUDEL_CAPI(proxy_fcgi, cLHy proxy FastCGI cAPI.  Requires --enable-proxy., $proxy_fcgi_objs, , most, , proxy)
CLHYKUDEL_CAPI(proxy_scgi, cLHy proxy SCGI cAPI.  Requires --enable-proxy., $proxy_scgi_objs, , most, , proxy)
CLHYKUDEL_CAPI(proxy_uwsgi, cLHy proxy UWSGI cAPI.  Requires --enable-proxy., $proxy_uwsgi_objs, , most, , proxy)
CLHYKUDEL_CAPI(proxy_fdpass, cLHy proxy to Unix Daemon Socket cAPI.  Requires --enable-proxy., $proxy_fdpass_objs, , most, [
  AC_CHECK_DECL(CMSG_DATA,,, [
    #include <sys/types.h>
    #include <sys/socket.h>
  ])
  if test $ac_cv_have_decl_CMSG_DATA = "no"; then
    AC_MSG_WARN([Your system does not support CMSG_DATA.])
    enable_proxy_fdpass=no
  fi
],proxy)
CLHYKUDEL_CAPI(proxy_wstunnel, cLHy proxy Websocket Tunnel cAPI.  Requires --enable-proxy., $proxy_wstunnel_objs, , most, , proxy)
CLHYKUDEL_CAPI(proxy_ajp, cLHy proxy AJP cAPI.  Requires --enable-proxy., $proxy_ajp_objs, , most, , proxy)
CLHYKUDEL_CAPI(proxy_balancer, cLHy proxy BALANCER cAPI.  Requires --enable-proxy., $proxy_balancer_objs, , most, , proxy)

CLHYKUDEL_CAPI(proxy_express, mass reverse-proxy cAPI. Requires --enable-proxy., , , most, , proxy)
CLHYKUDEL_CAPI(proxy_hcheck, [reverse-proxy health-check cAPI. Requires --enable-proxy and --enable-watchdog.], , , most, , [proxy,watchdog])

KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

capi_selection=$save_capi_selection
capi_default=$save_capi_default

CLHYKUDEL_CAPIPATH_FINISH

