/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ajp.h"

CLHYLOG_USE_CAPI(proxy_ajp);

/*
 * Handle the CPING/CPONG
 */
kuda_status_t ajp_handle_cping_cpong(kuda_socket_t *sock,
                                    request_rec *r,
                                    kuda_interval_time_t timeout)
{
    ajp_msg_t *msg;
    kuda_status_t rc, rv;
    kuda_interval_time_t org;
    kuda_byte_t result;

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE8, 0, r,
                         "Into ajp_handle_cping_cpong");

    rc = ajp_msg_create(r->pool, AJP_PING_PONG_SZ, &msg);
    if (rc != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01007)
               "ajp_handle_cping_cpong: ajp_msg_create failed");
        return rc;
    }

    rc = ajp_msg_serialize_cping(msg);
    if (rc != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01008)
               "ajp_handle_cping_cpong: ajp_marshal_into_msgb failed");
        return rc;
    }

    rc = ajp_ilink_send(sock, msg);
    ajp_msg_log(r, msg, "ajp_handle_cping_cpong: ajp_ilink_send packet dump");
    if (rc != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01009)
               "ajp_handle_cping_cpong: ajp_ilink_send failed");
        return rc;
    }

    rc = kuda_socket_timeout_get(sock, &org);
    if (rc != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01010)
               "ajp_handle_cping_cpong: kuda_socket_timeout_get failed");
        return rc;
    }

    /* Set CPING/CPONG response timeout */
    rc = kuda_socket_timeout_set(sock, timeout);
    if (rc != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01011)
               "ajp_handle_cping_cpong: kuda_socket_timeout_set failed");
        return rc;
    }
    ajp_msg_reuse(msg);

    /* Read CPONG reply */
    rv = ajp_ilink_receive(sock, msg);
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01012)
               "ajp_handle_cping_cpong: ajp_ilink_receive failed");
        goto cleanup;
    }

    ajp_msg_log(r, msg, "ajp_handle_cping_cpong: ajp_ilink_receive packet dump");
    rv = ajp_msg_get_uint8(msg, &result);
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01013)
               "ajp_handle_cping_cpong: invalid CPONG message");
        goto cleanup;
    }
    if (result != CMD_AJP13_CPONG) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01014)
               "ajp_handle_cping_cpong: awaited CPONG, received %d ",
               result);
        rv = KUDA_EGENERAL;
        goto cleanup;
    }

cleanup:
    /* Restore original socket timeout */
    rc = kuda_socket_timeout_set(sock, org);
    if (rc != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01015)
               "ajp_handle_cping_cpong: kuda_socket_timeout_set failed");
        return rc;
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE8, 0, r,
                         "ajp_handle_cping_cpong: Done");
    return rv;
}


#define case_to_str(x)    case CMD_AJP13_##x:\
                              return #x;\
                              break;

/**
 * Convert numeric message type into string
 * @param type      AJP message type
 * @return          AJP message type as a string
 */
const char *ajp_type_str(int type)
{
    switch (type) {
        case_to_str(FORWARD_REQUEST)
        case_to_str(SEND_BODY_CHUNK)
        case_to_str(SEND_HEADERS)
        case_to_str(END_RESPONSE)
        case_to_str(GET_BODY_CHUNK)
        case_to_str(SHUTDOWN)
        case_to_str(PING)
        case_to_str(CPONG)
        case_to_str(CPING)
        default:
            return "CMD_AJP13_UNKNOWN";
    }

}
