/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * capi_proxy_scgi.c
 * Proxy backend cAPI for the SCGI protocol
 * (http://python.ca/scgi/protocol.txt)
 *
 * Andre Malo (nd/perlig.de), August 2007
 */

#define KUDA_WANT_MEMFUNC
#define KUDA_WANT_STRFUNC
#include "kuda_strings.h"
#include "clhy_hooks.h"
#include "kuda_optional_hooks.h"
#include "kuda_buckets.h"

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_request.h"
#include "util_script.h"

#include "capi_proxy.h"
#include "scgi.h"


#define SCHEME "scgi"
#define PROXY_FUNCTION "SCGI"
#define SCGI_MAGIC "SCGI"
#define SCGI_PROTOCOL_VERSION "1"

/* just protect from typos */
#define CONTENT_LENGTH "CONTENT_LENGTH"
#define GATEWAY_INTERFACE "GATEWAY_INTERFACE"

cAPI CLHY_CAPI_DECLARE_DATA proxy_scgi_capi;


typedef enum {
    scgi_internal_redirect,
    scgi_sendfile
} scgi_request_type;

typedef struct {
    const char *location;    /* target URL */
    scgi_request_type type;  /* type of request */
} scgi_request_config;

const char *scgi_sendfile_off = "off";
const char *scgi_sendfile_on = "X-Sendfile";
const char *scgi_internal_redirect_off = "off";
const char *scgi_internal_redirect_on = "Location";

typedef struct {
    const char *sendfile;
    const char *internal_redirect;
} scgi_config;


/*
 * We create our own bucket type, which is actually derived (c&p) from the
 * socket bucket.
 * Maybe some time this should be made more abstract (like passing an
 * interception function to read or something) and go into the clhy_ or
 * even kuda_ namespace.
 */

typedef struct {
    kuda_socket_t *sock;
    kuda_off_t *counter;
} socket_ex_data;

static kuda_bucket *bucket_socket_ex_create(socket_ex_data *data,
                                           kuda_bucket_alloc_t *list);


static kuda_status_t bucket_socket_ex_read(kuda_bucket *a, const char **str,
                                          kuda_size_t *len,
                                          kuda_read_type_e block)
{
    socket_ex_data *data = a->data;
    kuda_socket_t *p = data->sock;
    char *buf;
    kuda_status_t rv;
    kuda_interval_time_t timeout;

    if (block == KUDA_NONBLOCK_READ) {
        kuda_socket_timeout_get(p, &timeout);
        kuda_socket_timeout_set(p, 0);
    }

    *str = NULL;
    *len = KUDA_BUCKET_BUFF_SIZE;
    buf = kuda_bucket_alloc(*len, a->list);

    rv = kuda_socket_recv(p, buf, len);

    if (block == KUDA_NONBLOCK_READ) {
        kuda_socket_timeout_set(p, timeout);
    }

    if (rv != KUDA_SUCCESS && rv != KUDA_EOF) {
        kuda_bucket_free(buf);
        return rv;
    }

    if (*len > 0) {
        kuda_bucket_heap *h;

        /* count for stats */
        *data->counter += *len;

        /* Change the current bucket to refer to what we read */
        a = kuda_bucket_heap_make(a, buf, *len, kuda_bucket_free);
        h = a->data;
        h->alloc_len = KUDA_BUCKET_BUFF_SIZE; /* note the real buffer size */
        *str = buf;
        KUDA_BUCKET_INSERT_AFTER(a, bucket_socket_ex_create(data, a->list));
    }
    else {
        kuda_bucket_free(buf);
        a = kuda_bucket_immortal_make(a, "", 0);
        *str = a->data;
    }
    return KUDA_SUCCESS;
}

static const kuda_bucket_type_t bucket_type_socket_ex = {
    "SOCKET_EX", 5, KUDA_BUCKET_DATA,
    kuda_bucket_destroy_noop,
    bucket_socket_ex_read,
    kuda_bucket_setaside_notimpl,
    kuda_bucket_split_notimpl,
    kuda_bucket_copy_notimpl
};

static kuda_bucket *bucket_socket_ex_make(kuda_bucket *b, socket_ex_data *data)
{
    b->type        = &bucket_type_socket_ex;
    b->length      = (kuda_size_t)(-1);
    b->start       = -1;
    b->data        = data;
    return b;
}

static kuda_bucket *bucket_socket_ex_create(socket_ex_data *data,
                                           kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    return bucket_socket_ex_make(b, data);
}


/*
 * Canonicalize scgi-like URLs.
 */
static int scgi_canon(request_rec *r, char *url)
{
    char *host, sport[sizeof(":65535")];
    const char *err, *path;
    kuda_port_t port, def_port;

    if (strncasecmp(url, SCHEME "://", sizeof(SCHEME) + 2)) {
        return DECLINED;
    }
    url += sizeof(SCHEME); /* Keep slashes */

    port = def_port = SCGI_DEF_PORT;

    err = clhy_proxy_canon_netloc(r->pool, &url, NULL, NULL, &host, &port);
    if (err) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00857)
                      "error parsing URL %s: %s", url, err);
        return HTTP_BAD_REQUEST;
    }

    if (port != def_port) {
        kuda_snprintf(sport, sizeof(sport), ":%u", port);
    }
    else {
        sport[0] = '\0';
    }

    if (clhy_strchr(host, ':')) { /* if literal IPv6 address */
        host = kuda_pstrcat(r->pool, "[", host, "]", NULL);
    }

    path = clhy_proxy_canonenc(r->pool, url, strlen(url), enc_path, 0,
                             r->proxyreq);
    if (!path) {
        return HTTP_BAD_REQUEST;
    }

    r->filename = kuda_pstrcat(r->pool, "proxy:" SCHEME "://", host, sport, "/",
                              path, NULL);

    if (kuda_table_get(r->subprocess_env, "proxy-scgi-pathinfo")) {
        r->path_info = kuda_pstrcat(r->pool, "/", path, NULL);
    }

    return OK;
}


/*
 * Send a block of data, ensure, everything is sent
 */
static int sendall(proxy_conn_rec *conn, const char *buf, kuda_size_t length,
                   request_rec *r)
{
    kuda_status_t rv;
    kuda_size_t written;

    while (length > 0) {
        written = length;
        if ((rv = kuda_socket_send(conn->sock, buf, &written)) != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(00858)
                          "sending data to %s:%u failed",
                          conn->hostname, conn->port);
            return HTTP_SERVICE_UNAVAILABLE;
        }

        /* count for stats */
        conn->worker->s->transferred += written;
        buf += written;
        length -= written;
    }

    return OK;
}


/*
 * Send SCGI header block
 */
static int send_headers(request_rec *r, proxy_conn_rec *conn)
{
    char *buf, *cp, *bodylen;
    const char *ns_len;
    const kuda_array_header_t *env_table;
    const kuda_table_entry_t *env;
    int j;
    kuda_size_t len, bodylen_size;
    kuda_size_t headerlen =   sizeof(CONTENT_LENGTH)
                           + sizeof(SCGI_MAGIC)
                           + sizeof(SCGI_PROTOCOL_VERSION);

    clhy_add_common_vars(r);
    clhy_add_cgi_vars(r);

    /*
     * The header blob basically takes the environment and concatenates
     * keys and values using 0 bytes. There are special treatments here:
     *   - GATEWAY_INTERFACE and SCGI_MAGIC are dropped
     *   - CONTENT_LENGTH is always set and must be sent as the very first
     *     variable
     *
     * Additionally it's wrapped into a so-called netstring (see SCGI spec)
     */
    env_table = kuda_table_elts(r->subprocess_env);
    env = (kuda_table_entry_t *)env_table->elts;
    for (j = 0; j < env_table->nelts; ++j) {
        if (   (!strcmp(env[j].key, GATEWAY_INTERFACE))
            || (!strcmp(env[j].key, CONTENT_LENGTH))
            || (!strcmp(env[j].key, SCGI_MAGIC))) {
            continue;
        }
        headerlen += strlen(env[j].key) + strlen(env[j].val) + 2;
    }
    bodylen = kuda_psprintf(r->pool, "%" KUDA_OFF_T_FMT, r->remaining);
    bodylen_size = strlen(bodylen) + 1;
    headerlen += bodylen_size;

    ns_len = kuda_psprintf(r->pool, "%" KUDA_SIZE_T_FMT ":", headerlen);
    len = strlen(ns_len);
    headerlen += len + 1; /* 1 == , */
    cp = buf = kuda_palloc(r->pool, headerlen);
    memcpy(cp, ns_len, len);
    cp += len;

    memcpy(cp, CONTENT_LENGTH, sizeof(CONTENT_LENGTH));
    cp += sizeof(CONTENT_LENGTH);
    memcpy(cp, bodylen, bodylen_size);
    cp += bodylen_size;
    memcpy(cp, SCGI_MAGIC, sizeof(SCGI_MAGIC));
    cp += sizeof(SCGI_MAGIC);
    memcpy(cp, SCGI_PROTOCOL_VERSION, sizeof(SCGI_PROTOCOL_VERSION));
    cp += sizeof(SCGI_PROTOCOL_VERSION);

    for (j = 0; j < env_table->nelts; ++j) {
        if (   (!strcmp(env[j].key, GATEWAY_INTERFACE))
            || (!strcmp(env[j].key, CONTENT_LENGTH))
            || (!strcmp(env[j].key, SCGI_MAGIC))) {
            continue;
        }
        len = strlen(env[j].key) + 1;
        memcpy(cp, env[j].key, len);
        cp += len;
        len = strlen(env[j].val) + 1;
        memcpy(cp, env[j].val, len);
        cp += len;
    }
    *cp++ = ',';

    return sendall(conn, buf, headerlen, r);
}


/*
 * Send request body (if any)
 */
static int send_request_body(request_rec *r, proxy_conn_rec *conn)
{
    if (clhy_should_client_block(r)) {
        char *buf = kuda_palloc(r->pool, CLHY_IOBUFSIZE);
        int status;
        kuda_size_t readlen;

        readlen = clhy_get_client_block(r, buf, CLHY_IOBUFSIZE);
        while (readlen > 0) {
            status = sendall(conn, buf, readlen, r);
            if (status != OK) {
                return HTTP_SERVICE_UNAVAILABLE;
            }
            readlen = clhy_get_client_block(r, buf, CLHY_IOBUFSIZE);
        }
        if (readlen == -1) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00859)
                          "receiving request body failed");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
    }

    return OK;
}


/*
 * Fetch response from backend and pass back to the front
 */
static int pass_response(request_rec *r, proxy_conn_rec *conn)
{
    kuda_bucket_brigade *bb;
    kuda_bucket *b;
    const char *location;
    scgi_config *conf;
    socket_ex_data *sock_data;
    int status;

    sock_data = kuda_palloc(r->pool, sizeof(*sock_data));
    sock_data->sock = conn->sock;
    sock_data->counter = &conn->worker->s->read;

    bb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
    b = bucket_socket_ex_create(sock_data, r->connection->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bb, b);
    b = kuda_bucket_eos_create(r->connection->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bb, b);

    status = clhy_scan_script_header_err_brigade_ex(r, bb, NULL,
                                                  CLHYLOG_CAPI_INDEX);
    if (status != OK) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00860)
                      "error reading response headers from %s:%u",
                      conn->hostname, conn->port);
        r->status_line = NULL;
        kuda_brigade_destroy(bb);
        return status;
    }

    conf = clhy_get_capi_config(r->per_dir_config, &proxy_scgi_capi);
    if (conf->sendfile && conf->sendfile != scgi_sendfile_off) {
        short err = 1;

        location = kuda_table_get(r->err_headers_out, conf->sendfile);
        if (!location) {
            err = 0;
            location = kuda_table_get(r->headers_out, conf->sendfile);
        }
        if (location) {
            scgi_request_config *req_conf = kuda_palloc(r->pool,
                                                       sizeof(*req_conf));
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00861)
                          "Found %s: %s - preparing subrequest.",
                          conf->sendfile, location);

            if (err) {
                kuda_table_unset(r->err_headers_out, conf->sendfile);
            }
            else {
                kuda_table_unset(r->headers_out, conf->sendfile);
            }
            req_conf->location = location;
            req_conf->type = scgi_sendfile;
            clhy_set_capi_config(r->request_config, &proxy_scgi_capi,
                                 req_conf);
            kuda_brigade_destroy(bb);
            return OK;
        }
    }

    if (r->status == HTTP_OK 
        && (!conf->internal_redirect /* default === On */
            || conf->internal_redirect != scgi_internal_redirect_off)) {
        short err = 1;
        const char *location_header = conf->internal_redirect ? 
            conf->internal_redirect : scgi_internal_redirect_on;

        location = kuda_table_get(r->err_headers_out, location_header);
        if (!location) {
            err = 0;
            location = kuda_table_get(r->headers_out, location_header);
        }
        if (location && *location == '/') {
            scgi_request_config *req_conf = kuda_palloc(r->pool,
                                                       sizeof(*req_conf));
            if (strcasecmp(location_header, "Location")) {
                if (err) {
                    kuda_table_unset(r->err_headers_out, location_header);
                }
                else {
                    kuda_table_unset(r->headers_out, location_header);
                }
            }
            req_conf->location = location;
            req_conf->type = scgi_internal_redirect;
            clhy_set_capi_config(r->request_config, &proxy_scgi_capi,
                                 req_conf);
            kuda_brigade_destroy(bb);
            return OK;
        }
    }

    if (clhy_pass_brigade(r->output_filters, bb)) {
        return CLHY_FILTER_ERROR;
    }

    return OK;
}

/*
 * Internal redirect / subrequest handler, working on request_status hook
 */
static int scgi_request_status(int *status, request_rec *r)
{
    scgi_request_config *req_conf;

    if (   (*status == OK)
        && (req_conf = clhy_get_capi_config(r->request_config,
                                            &proxy_scgi_capi))) {
        switch (req_conf->type) {
        case scgi_internal_redirect:
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00862)
                          "Internal redirect to %s", req_conf->location);

            r->status_line = NULL;
            if (r->method_number != M_GET) {
                /* keep HEAD, which is passed around as M_GET, too */
                r->method = "GET";
                r->method_number = M_GET;
            }
            kuda_table_unset(r->headers_in, "Content-Length");
            clhy_internal_redirect_handler(req_conf->location, r);
            return OK;
            /* break; */

        case scgi_sendfile:
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00863)
                          "File subrequest to %s", req_conf->location);
            do {
                request_rec *rr;

                rr = clhy_sub_req_lookup_file(req_conf->location, r,
                                            r->output_filters);
                if (rr->status == HTTP_OK && rr->finfo.filetype != KUDA_NOFILE) {
                    /*
                     * We don't touch Content-Length here. It might be
                     * borked (there's plenty of room for a race condition).
                     * Either the backend sets it or it's gonna be chunked.
                     */
                    clhy_run_sub_req(rr);
                }
                else {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00864)
                                  "Subrequest to file '%s' not possible. "
                                  "(rr->status=%d, rr->finfo.filetype=%d)",
                                  req_conf->location, rr->status,
                                  rr->finfo.filetype);
                    *status = HTTP_INTERNAL_SERVER_ERROR;
                    return *status;
                }
            } while (0);

            return OK;
            /* break; */
        }
    }

    return DECLINED;
}


/*
 * This handles scgi:(dest) URLs
 */
static int scgi_handler(request_rec *r, proxy_worker *worker,
                        proxy_server_conf *conf, char *url,
                        const char *proxyname, kuda_port_t proxyport)
{
    int status;
    proxy_conn_rec *backend = NULL;
    kuda_pool_t *p = r->pool;
    kuda_uri_t *uri;
    char dummy;

    if (strncasecmp(url, SCHEME "://", sizeof(SCHEME) + 2)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(00865)
                      "declining URL %s", url);
        return DECLINED;
    }

    /* Create space for state information */
    status = clhy_proxy_acquire_connection(PROXY_FUNCTION, &backend, worker,
                                         r->server);
    if (status != OK) {
        goto cleanup;
    }
    backend->is_ssl = 0;

    /* Step One: Determine Who To Connect To */
    uri = kuda_palloc(p, sizeof(*uri));
    status = clhy_proxy_determine_connection(p, r, conf, worker, backend,
                                           uri, &url, proxyname, proxyport,
                                           &dummy, 1);
    if (status != OK) {
        goto cleanup;
    }

    /* Step Two: Make the Connection */
    if (clhy_proxy_connect_backend(PROXY_FUNCTION, backend, worker, r->server)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00866)
                      "failed to make connection to backend: %s:%u",
                      backend->hostname, backend->port);
        status = HTTP_SERVICE_UNAVAILABLE;
        goto cleanup;
    }

    /* Step Three: Process the Request */
    if (   ((status = clhy_setup_client_block(r, REQUEST_CHUNKED_ERROR)) != OK)
        || ((status = send_headers(r, backend)) != OK)
        || ((status = send_request_body(r, backend)) != OK)
        || ((status = pass_response(r, backend)) != OK)) {
        goto cleanup;
    }

cleanup:
    if (backend) {
        backend->close = 1; /* always close the socket */
        clhy_proxy_release_connection(PROXY_FUNCTION, backend, r->server);
    }
    return status;
}


static void *create_scgi_config(kuda_pool_t *p, char *dummy)
{
    scgi_config *conf=kuda_palloc(p, sizeof(*conf));

    conf->sendfile = NULL; /* === default (off) */
    conf->internal_redirect = NULL; /* === default (on) */

    return conf;
}


static void *merge_scgi_config(kuda_pool_t *p, void *base_, void *add_)
{
    scgi_config *base=base_, *add=add_, *conf=kuda_palloc(p, sizeof(*conf));

    conf->sendfile = add->sendfile ? add->sendfile: base->sendfile;
    conf->internal_redirect = add->internal_redirect
                              ? add->internal_redirect
                              : base->internal_redirect;
    return conf;
}


static const char *scgi_set_send_file(cmd_parms *cmd, void *mconfig,
                                      const char *arg)
{
    scgi_config *conf=mconfig;

    if (!strcasecmp(arg, "Off")) {
        conf->sendfile = scgi_sendfile_off;
    }
    else if (!strcasecmp(arg, "On")) {
        conf->sendfile = scgi_sendfile_on;
    }
    else {
        conf->sendfile = arg;
    }
    return NULL;
}


static const char *scgi_set_internal_redirect(cmd_parms *cmd, void *mconfig,
                                              const char *arg)
{
    scgi_config *conf = mconfig;

    if (!strcasecmp(arg, "Off")) {
        conf->internal_redirect = scgi_internal_redirect_off;
    }
    else if (!strcasecmp(arg, "On")) {
        conf->internal_redirect = scgi_internal_redirect_on;
    }
    else {
        conf->internal_redirect = arg;
    }
    return NULL;
}


static const command_rec scgi_cmds[] =
{
    CLHY_INIT_TAKE1("ProxySCGISendfile", scgi_set_send_file, NULL,
                  RSRC_CONF|ACCESS_CONF,
                  "The name of the X-Sendfile pseudo response header or "
                  "On or Off"),
    CLHY_INIT_TAKE1("ProxySCGIInternalRedirect", scgi_set_internal_redirect, NULL,
                  RSRC_CONF|ACCESS_CONF,
                  "The name of the pseudo response header or On or Off"),
    {NULL}
};


static void register_hooks(kuda_pool_t *p)
{
    proxy_hook_scheme_handler(scgi_handler, NULL, NULL, KUDA_HOOK_FIRST);
    proxy_hook_canon_handler(scgi_canon, NULL, NULL, KUDA_HOOK_FIRST);
    KUDA_OPTIONAL_HOOK(proxy, request_status, scgi_request_status, NULL, NULL,
                      KUDA_HOOK_MIDDLE);
}


CLHY_DECLARE_CAPI(proxy_scgi) = {
    STANDARD16_CAPI_STUFF,
    create_scgi_config,  /* create per-directory config structure */
    merge_scgi_config,   /* merge per-directory config structures */
    NULL,                /* create per-server config structure */
    NULL,                /* merge per-server config structures */
    scgi_cmds,           /* command table */
    register_hooks       /* register hooks */
};
