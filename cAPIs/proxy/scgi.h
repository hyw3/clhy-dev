/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file scgi.h
 * @brief Shared SCGI-related definitions
 *
 * @ingroup CLHYKUDEL_INTERNAL
 * @{
 */

#ifndef SCGI_H
#define SCGI_H

/* This is not defined by the protocol.  It is a convention
 * of capi_proxy_scgi, and capi_proxy utility routines must
 * use the same value as capi_proxy_scgi.
 */
#define SCGI_DEF_PORT 4000

/** @} */

#endif /* SCGI_H */
