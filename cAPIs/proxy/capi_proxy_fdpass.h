/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file capi_proxy_fdpass.h
 * @brief FD Passing interfaces
 *
 * @ingroup CLHYKUDEL_INTERNAL
 * @{
 */

#include "capi_proxy.h"

#ifndef _PROXY_FDPASS_H_
#define _PROXY_FDPASS_H_

#define PROXY_FDPASS_FLUSHER "proxy_fdpass_flusher"

typedef struct proxy_fdpass_flush proxy_fdpass_flush;
struct proxy_fdpass_flush {
    const char *name;
    int (*flusher)(request_rec *r);
    void *context;
};

#endif /* _PROXY_FDPASS_H_ */
/** @} */

