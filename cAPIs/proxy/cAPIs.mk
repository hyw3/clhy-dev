capi_proxy.la: capi_proxy.slo proxy_util.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy.lo proxy_util.lo $(CAPI_PROXY_LDADD)
capi_proxy_connect.la: capi_proxy_connect.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy_connect.lo $(CAPI_PROXY_CONNECT_LDADD)
capi_proxy_ftp.la: capi_proxy_ftp.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy_ftp.lo $(CAPI_PROXY_FTP_LDADD)
capi_proxy_http.la: capi_proxy_http.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy_http.lo $(CAPI_PROXY_HTTP_LDADD)
capi_proxy_fcgi.la: capi_proxy_fcgi.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy_fcgi.lo $(CAPI_PROXY_FCGI_LDADD)
capi_proxy_scgi.la: capi_proxy_scgi.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy_scgi.lo $(CAPI_PROXY_SCGI_LDADD)
capi_proxy_uwsgi.la: capi_proxy_uwsgi.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy_uwsgi.lo $(CAPI_PROXY_UWSGI_LDADD)
capi_proxy_fdpass.la: capi_proxy_fdpass.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy_fdpass.lo $(CAPI_PROXY_FDPASS_LDADD)
capi_proxy_wstunnel.la: capi_proxy_wstunnel.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy_wstunnel.lo $(CAPI_PROXY_WSTUNNEL_LDADD)
capi_proxy_ajp.la: capi_proxy_ajp.slo ajp_header.slo ajp_link.slo ajp_msg.slo ajp_utils.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy_ajp.lo ajp_header.lo ajp_link.lo ajp_msg.lo ajp_utils.lo $(CAPI_PROXY_AJP_LDADD)
capi_proxy_balancer.la: capi_proxy_balancer.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy_balancer.lo $(CAPI_PROXY_BALANCER_LDADD)
capi_proxy_express.la: capi_proxy_express.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy_express.lo $(CAPI_PROXY_EXPRESS_LDADD)
capi_proxy_hcheck.la: capi_proxy_hcheck.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy_hcheck.lo $(CAPI_PROXY_HCHECK_LDADD)
DISTCLEAN_TARGETS = cAPIs.mk
static = 
shared =  capi_proxy.la capi_proxy_connect.la capi_proxy_ftp.la capi_proxy_http.la capi_proxy_fcgi.la capi_proxy_scgi.la capi_proxy_uwsgi.la capi_proxy_fdpass.la capi_proxy_wstunnel.la capi_proxy_ajp.la capi_proxy_balancer.la capi_proxy_express.la capi_proxy_hcheck.la
