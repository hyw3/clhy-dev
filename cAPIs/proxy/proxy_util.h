/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PROXY_UTIL_H_
#define PROXY_UTIL_H_

/**
 * @file  proxy_util.h
 * @brief Internal interfaces private to capi_proxy.
 *
 * @defgroup CAPI_PROXY_PRIVATE Private
 * @ingroup CAPI_PROXY
 * @{
 */

PROXY_DECLARE(int) clhy_proxy_is_ipaddr(struct dirconn_entry *This, kuda_pool_t *p);
PROXY_DECLARE(int) clhy_proxy_is_domainname(struct dirconn_entry *This, kuda_pool_t *p);
PROXY_DECLARE(int) clhy_proxy_is_hostname(struct dirconn_entry *This, kuda_pool_t *p);
PROXY_DECLARE(int) clhy_proxy_is_word(struct dirconn_entry *This, kuda_pool_t *p);

PROXY_DECLARE_DATA extern int proxy_lb_workers;
PROXY_DECLARE_DATA extern const kuda_strmatch_pattern *clhy_proxy_strmatch_path;
PROXY_DECLARE_DATA extern const kuda_strmatch_pattern *clhy_proxy_strmatch_domain;

/**
 * Register optional functions declared within proxy_util.c.
 */
void proxy_util_register_hooks(kuda_pool_t *p);

/** @} */

#endif /* PROXY_UTIL_H_ */
