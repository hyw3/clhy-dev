/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "capi_proxy.h"
#include "kuda_dbm.h"

cAPI CLHY_CAPI_DECLARE_DATA proxy_express_capi;

static int proxy_available = 0;

typedef struct {
    const char *dbmfile;
    const char *dbmtype;
    int enabled;
} express_server_conf;

static const char *set_dbmfile(cmd_parms *cmd,
                               void *dconf,
                               const char *arg)
{
    express_server_conf *sconf;
    sconf = clhy_get_capi_config(cmd->server->capi_config, &proxy_express_capi);

    if ((sconf->dbmfile = clhy_server_root_relative(cmd->pool, arg)) == NULL) {
        return kuda_pstrcat(cmd->pool, "ProxyExpressDBMFile: bad path to file: ",
                           arg, NULL);
    }
    return NULL;
}

static const char *set_dbmtype(cmd_parms *cmd,
                               void *dconf,
                               const char *arg)
{
    express_server_conf *sconf;
    sconf = clhy_get_capi_config(cmd->server->capi_config, &proxy_express_capi);

    sconf->dbmtype = arg;

    return NULL;
}

static const char *set_enabled(cmd_parms *cmd,
                               void *dconf,
                               int flag)
{
    express_server_conf *sconf;
    sconf = clhy_get_capi_config(cmd->server->capi_config, &proxy_express_capi);

    sconf->enabled = flag;

    return NULL;
}

static void *server_create(kuda_pool_t *p, server_rec *s)
{
    express_server_conf *a;

    a = (express_server_conf *)kuda_pcalloc(p, sizeof(express_server_conf));

    a->dbmfile = NULL;
    a->dbmtype = "default";
    a->enabled = 0;

    return (void *)a;
}

static void *server_merge(kuda_pool_t *p, void *basev, void *overridesv)
{
    express_server_conf *a, *base, *overrides;

    a         = (express_server_conf *)kuda_pcalloc(p,
                                                   sizeof(express_server_conf));
    base      = (express_server_conf *)basev;
    overrides = (express_server_conf *)overridesv;

    a->dbmfile = (overrides->dbmfile) ? overrides->dbmfile : base->dbmfile;
    a->dbmtype = (overrides->dbmtype) ? overrides->dbmtype : base->dbmtype;
    a->enabled = (overrides->enabled) ? overrides->enabled : base->enabled;

    return (void *)a;
}

static int post_config(kuda_pool_t *p,
                       kuda_pool_t *plog,
                       kuda_pool_t *ptemp,
                       server_rec *s)
{
    proxy_available = (clhy_find_linked_capi("capi_proxy.c") != NULL);
    return OK;
}


static int xlate_name(request_rec *r)
{
    int i;
    const char *name;
    char *backend;
    kuda_dbm_t *db;
    kuda_status_t rv;
    kuda_datum_t key, val;
    struct proxy_alias *ralias;
    proxy_dir_conf *dconf;
    express_server_conf *sconf;

    sconf = clhy_get_capi_config(r->server->capi_config, &proxy_express_capi);
    dconf = clhy_get_capi_config(r->per_dir_config, &proxy_capi);

    if (!sconf->enabled) {
        return DECLINED;
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01001) "proxy_express: Enabled");
    if (!sconf->dbmfile || (r->filename && strncmp(r->filename, "proxy:", 6) == 0)) {
        /* it should be go on as an internal proxy request */
        return DECLINED;
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01002)
                  "proxy_express: Opening DBM file: %s (%s)",
                  sconf->dbmfile, sconf->dbmtype);
    rv = kuda_dbm_open_ex(&db, sconf->dbmtype, sconf->dbmfile, KUDA_DBM_READONLY,
                         KUDA_PLATFORM_DEFAULT, r->pool);
    if (rv != KUDA_SUCCESS) {
        return DECLINED;
    }

    name = clhy_get_server_name(r);
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01003)
                  "proxy_express: looking for %s", name);
    key.dptr = (char *)name;
    key.dsize = strlen(key.dptr);

    rv = kuda_dbm_fetch(db, key, &val);
    if (rv == KUDA_SUCCESS) {
        backend = kuda_pstrmemdup(r->pool, val.dptr, val.dsize);
    }
    kuda_dbm_close(db);
    if (rv != KUDA_SUCCESS || !backend) {
        return DECLINED;
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01004)
                  "proxy_express: found %s -> %s", name, backend);
    r->filename = kuda_pstrcat(r->pool, "proxy:", backend, r->uri, NULL);
    r->handler = "proxy-server";
    r->proxyreq = PROXYREQ_REVERSE;

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01005)
                  "proxy_express: rewritten as: %s", r->filename);

    ralias = (struct proxy_alias *)dconf->raliases->elts;
    /*
     * See if we have already added a ProxyPassReverse entry
     * for this host... If so, don't do it again.
     */
    /*
     * NOTE: dconf is process specific so this will only
     *       work as long as we maintain that this process
     *       or thread is handling the backend
     */
    for (i = 0; i < dconf->raliases->nelts; i++, ralias++) {
        if (strcasecmp(backend, ralias->real) == 0) {
            ralias = NULL;
            break;
        }
    }

    /* Didn't find one... add it */
    if (!ralias) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01006)
                      "proxy_express: adding PPR entry");
        ralias = kuda_array_push(dconf->raliases);
        ralias->fake = "/";
        ralias->real = kuda_pstrdup(dconf->raliases->pool, backend);
        ralias->flags = 0;
    }
    return OK;
}

static const command_rec command_table[] = {
    CLHY_INIT_FLAG("ProxyExpressEnable", set_enabled, NULL, OR_FILEINFO,
                 "Enable the ProxyExpress functionality"),
    CLHY_INIT_TAKE1("ProxyExpressDBMFile", set_dbmfile, NULL, OR_FILEINFO,
                  "Location of ProxyExpressDBMFile file"),
    CLHY_INIT_TAKE1("ProxyExpressDBMType", set_dbmtype, NULL, OR_FILEINFO,
                  "Type of ProxyExpressDBMFile file"),
    { NULL }
};

static void register_hooks(kuda_pool_t *p)
{
    clhy_hook_post_config(post_config, NULL, NULL, KUDA_HOOK_LAST);
    clhy_hook_translate_name(xlate_name, NULL, NULL, KUDA_HOOK_FIRST);
}

/* the main config structure */

CLHY_DECLARE_CAPI(proxy_express) =
{
    STANDARD16_CAPI_STUFF,
    NULL,           /* create per-dir config structures */
    NULL,           /* merge  per-dir config structures */
    server_create,  /* create per-server config structures */
    server_merge,   /* merge  per-server config structures */
    command_table,  /* table of config file commands */
    register_hooks  /* register hooks */
};
