/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * http_request.c: functions to get and process requests
 *
 * Rob McCool 3/21/93
 *
 * Thoroughly revamped by rst for cLHy.  NB this file reads
 * best from the bottom up.
 *
 */

#include "kuda_strings.h"
#include "kuda_file_io.h"
#include "kuda_fnmatch.h"

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_request.h"
#include "http_core.h"
#include "http_protocol.h"
#include "http_log.h"
#include "http_main.h"
#include "util_filter.h"
#include "util_charset.h"
#include "scoreboard.h"

#include "capi_core.h"

#if KUDA_HAVE_STDARG_H
#include <stdarg.h>
#endif

CLHYLOG_USE_CAPI(http);

/*****************************************************************
 *
 * Mainline request processing...
 */

/* XXX A cleaner and faster way to do this might be to pass the request_rec
 * down the filter chain as a parameter.  It would need to change for
 * subrequest vs. main request filters; perhaps the subrequest filter could
 * make the switch.
 */
static void update_r_in_filters(clhy_filter_t *f,
                                request_rec *from,
                                request_rec *to)
{
    while (f) {
        if (f->r == from) {
            f->r = to;
        }
        f = f->next;
    }
}

static void clhy_die_r(int type, request_rec *r, int recursive_error)
{
    char *custom_response;
    request_rec *r_1st_err = r;

    if (type == OK || type == DONE) {
        clhy_finalize_request_protocol(r);
        return;
    }

    if (!clhy_is_HTTP_VALID_RESPONSE(type)) {
        clhy_filter_t *next;

        /*
         * Check if we still have the clhy_http_header_filter in place. If
         * this is the case we should not ignore the error here because
         * it means that we have not sent any response at all and never
         * will. This is bad. Sent an internal server error instead.
         */
        next = r->output_filters;
        while (next && (next->frec != clhy_http_header_filter_handle)) {
               next = next->next;
        }

        /*
         * If next != NULL then we left the while above because of
         * next->frec == clhy_http_header_filter
         */
        if (next) {
            if (type != CLHY_FILTER_ERROR) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01579)
                              "Invalid response status %i", type);
            }
            else {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(02831)
                              "Response from CLHY_FILTER_ERROR");
            }
            type = HTTP_INTERNAL_SERVER_ERROR;
        }
        else {
            return;
        }
    }

    /*
     * The following takes care of cLHy redirects to custom response URLs
     * Note that if we are already dealing with the response to some other
     * error condition, we just report on the original error, and give up on
     * any attempt to handle the other thing "intelligently"...
     */
    if (recursive_error != HTTP_OK) {
        while (r_1st_err->prev && (r_1st_err->prev->status != HTTP_OK))
            r_1st_err = r_1st_err->prev;  /* Get back to original error */

        if (r_1st_err != r) {
            /* The recursive error was caused by an ErrorDocument specifying
             * an internal redirect to a bad URI.  clhy_internal_redirect has
             * changed the filter chains to point to the ErrorDocument's
             * request_rec.  Back out those changes so we can safely use the
             * original failing request_rec to send the canned error message.
             *
             * clhy_send_error_response gets rid of existing resource filters
             * on the output side, so we can skip those.
             */
            update_r_in_filters(r_1st_err->proto_output_filters, r, r_1st_err);
            update_r_in_filters(r_1st_err->input_filters, r, r_1st_err);
        }

        custom_response = NULL; /* Do NOT retry the custom thing! */
    }
    else {
        int error_index = clhy_index_of_response(type);
        custom_response = clhy_response_code_string(r, error_index);
        recursive_error = 0;
    }

    r->status = type;

    /*
     * This test is done here so that none of the auth cAPIs needs to know
     * about proxy authentication.  They treat it like normal auth, and then
     * we tweak the status.
     */
    if (HTTP_UNAUTHORIZED == r->status && PROXYREQ_PROXY == r->proxyreq) {
        r->status = HTTP_PROXY_AUTHENTICATION_REQUIRED;
    }

    /* If we don't want to keep the connection, make sure we mark that the
     * connection is not eligible for keepalive.  If we want to keep the
     * connection, be sure that the request body (if any) has been read.
     */
    if (clhy_status_drops_connection(r->status)) {
        r->connection->keepalive = CLHY_CONN_CLOSE;
    }

    /*
     * Two types of custom redirects --- plain text, and URLs. Plain text has
     * a leading '"', so the URL code, here, is triggered on its absence
     */

    if (custom_response && custom_response[0] != '"') {

        if (clhy_is_url(custom_response)) {
            /*
             * The URL isn't local, so lets drop through the rest of this
             * clhy code, and continue with the usual REDIRECT handler.
             * But note that the client will ultimately see the wrong
             * status...
             */
            r->status = HTTP_MOVED_TEMPORARILY;
            kuda_table_setn(r->headers_out, "Location", custom_response);
        }
        else if (custom_response[0] == '/') {
            const char *error_notes;
            r->no_local_copy = 1;       /* Do NOT send HTTP_NOT_MODIFIED for
                                         * error documents! */
            /*
             * This redirect needs to be a GET no matter what the original
             * method was.
             */
            kuda_table_setn(r->subprocess_env, "REQUEST_METHOD", r->method);

            /*
             * Provide a special method for cAPIs to communicate
             * more informative (than the plain canned) messages to us.
             * Propagate them to ErrorDocuments via the ERROR_NOTES variable:
             */
            if ((error_notes = kuda_table_get(r->notes,
                                             "error-notes")) != NULL) {
                kuda_table_setn(r->subprocess_env, "ERROR_NOTES", error_notes);
            }
            r->method = "GET";
            r->method_number = M_GET;
            clhy_internal_redirect(custom_response, r);
            return;
        }
        else {
            /*
             * Dumb user has given us a bad url to redirect to --- fake up
             * dying with a recursive server error...
             */
            recursive_error = HTTP_INTERNAL_SERVER_ERROR;
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01580)
                        "Invalid error redirection directive: %s",
                        custom_response);
        }
    }
    clhy_send_error_response(r_1st_err, recursive_error);
}

CLHY_DECLARE(void) clhy_die(int type, request_rec *r)
{
    clhy_die_r(type, r, r->status);
}

CLHY_DECLARE(kuda_status_t) clhy_check_pipeline(conn_rec *c, kuda_bucket_brigade *bb,
                                           unsigned int max_blank_lines)
{
    kuda_status_t rv = KUDA_EOF;
    clhy_input_mode_t mode = CLHY_MODE_SPECULATIVE;
    unsigned int num_blank_lines = 0;
    kuda_size_t cr = 0;
    char buf[2];

    while (c->keepalive != CLHY_CONN_CLOSE && !c->aborted) {
        kuda_size_t len = cr + 1;

        kuda_brigade_cleanup(bb);
        rv = clhy_get_brigade(c->input_filters, bb, mode,
                            KUDA_NONBLOCK_READ, len);
        if (rv != KUDA_SUCCESS || KUDA_BRIGADE_EMPTY(bb) || !max_blank_lines) {
            if (mode == CLHY_MODE_READBYTES) {
                /* Unexpected error, stop with this connection */
                clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, c, CLHYLOGNO(02967)
                              "Can't consume pipelined empty lines");
                c->keepalive = CLHY_CONN_CLOSE;
                rv = KUDA_EGENERAL;
            }
            else if (rv != KUDA_SUCCESS || KUDA_BRIGADE_EMPTY(bb)) {
                if (rv != KUDA_SUCCESS && !KUDA_STATUS_IS_EAGAIN(rv)) {
                    /* Pipe is dead */
                    c->keepalive = CLHY_CONN_CLOSE;
                }
                else {
                    /* Pipe is up and empty */
                    rv = KUDA_EAGAIN;
                }
            }
            else {
                kuda_off_t n = 0;
                /* Single read asked, (non-meta-)data available? */
                rv = kuda_brigade_length(bb, 0, &n);
                if (rv == KUDA_SUCCESS && n <= 0) {
                    rv = KUDA_EAGAIN;
                }
            }
            break;
        }

        /* Lookup and consume blank lines */
        rv = kuda_brigade_flatten(bb, buf, &len);
        if (rv != KUDA_SUCCESS || len != cr + 1) {
            int log_level;
            if (mode == CLHY_MODE_READBYTES) {
                /* Unexpected error, stop with this connection */
                c->keepalive = CLHY_CONN_CLOSE;
                log_level = CLHYLOG_ERR;
                rv = KUDA_EGENERAL;
            }
            else {
                /* Let outside (non-speculative/blocking) read determine
                 * where this possible failure comes from (metadata,
                 * morphed EOF socket, ...). Debug only here.
                 */
                log_level = CLHYLOG_DEBUG;
                rv = KUDA_SUCCESS;
            }
            clhy_log_cerror(CLHYLOG_MARK, log_level, rv, c, CLHYLOGNO(02968)
                          "Can't check pipelined data");
            break;
        }

        if (mode == CLHY_MODE_READBYTES) {
            /* [CR]LF consumed, try next */
            mode = CLHY_MODE_SPECULATIVE;
            cr = 0;
        }
        else if (cr) {
            CLHY_DEBUG_ASSERT(len == 2 && buf[0] == KUDA_ASCII_CR);
            if (buf[1] == KUDA_ASCII_LF) {
                /* consume this CRLF */
                mode = CLHY_MODE_READBYTES;
                num_blank_lines++;
            }
            else {
                /* CR(?!LF) is data */
                break;
            }
        }
        else {
            if (buf[0] == KUDA_ASCII_LF) {
                /* consume this LF */
                mode = CLHY_MODE_READBYTES;
                num_blank_lines++;
            }
            else if (buf[0] == KUDA_ASCII_CR) {
                cr = 1;
            }
            else {
                /* Not [CR]LF, some data */
                break;
            }
        }
        if (num_blank_lines > max_blank_lines) {
            /* Enough blank lines with this connection,
             * stop and don't recycle it.
             */
            c->keepalive = CLHY_CONN_CLOSE;
            rv = KUDA_NOTFOUND;
            break;
        }
    }

    return rv;
}


CLHY_DECLARE(void) clhy_process_request_after_handler(request_rec *r)
{
    kuda_bucket_brigade *bb;
    kuda_bucket *b;
    conn_rec *c = r->connection;
    kuda_status_t rv;

    /* Send an EOR bucket through the output filter chain.  When
     * this bucket is destroyed, the request will be logged and
     * its pool will be freed
     */
    bb = kuda_brigade_create(c->pool, c->bucket_alloc);
    b = clhy_bucket_eor_create(c->bucket_alloc, r);
    KUDA_BRIGADE_INSERT_HEAD(bb, b);

    clhy_pass_brigade(c->output_filters, bb);
    
    /* The EOR bucket has either been handled by an output filter (eg.
     * deleted or moved to a buffered_bb => no more in bb), or an error
     * occured before that (eg. c->aborted => still in bb) and we ought
     * to destroy it now. So cleanup any remaining bucket along with
     * the orphan request (if any).
     */
    kuda_brigade_cleanup(bb);

    /* From here onward, it is no longer safe to reference r
     * or r->pool, because r->pool may have been destroyed
     * already by the EOR bucket's cleanup function.
     */

    /* Check pipeline consuming blank lines, they must not be interpreted as
     * the next pipelined request, otherwise we would block on the next read
     * without flushing data, and hence possibly delay pending response(s)
     * until the next/real request comes in or the keepalive timeout expires.
     */
    rv = clhy_check_pipeline(c, bb, DEFAULT_LIMIT_BLANK_LINES);
    c->data_in_input_filters = (rv == KUDA_SUCCESS);
    kuda_brigade_destroy(bb);

    if (c->cs)
        c->cs->state = (c->aborted) ? CONN_STATE_LINGER
                                    : CONN_STATE_WRITE_COMPLETION;
    CLHY_PROCESS_REQUEST_RETURN((uintptr_t)r, r->uri, r->status);
    if (clhy_extended_status) {
        clhy_time_process_request(c->sbh, STOP_PREQUEST);
    }
}

void clhy_process_async_request(request_rec *r)
{
    conn_rec *c = r->connection;
    int access_status;

    /* Give quick handlers a shot at serving the request on the fast
     * path, bypassing all of the other cLHy hooks.
     *
     * This hook was added to enable serving files out of a URI keyed
     * content cache ( e.g., Mike Abbott's Quick Shortcut Cache,
     * described here: http://oss.sgi.com/projects/clhy/capi_qsc.html )
     *
     * It may have other uses as well, such as routing requests directly to
     * content handlers that have the ability to grok HTTP and do their
     * own access checking, etc (e.g. servlet engines).
     *
     * Use this hook with extreme care and only if you know what you are
     * doing.
     */
    CLHY_PROCESS_REQUEST_ENTRY((uintptr_t)r, r->uri);
    if (clhy_extended_status) {
        clhy_time_process_request(r->connection->sbh, START_PREQUEST);
    }

    if (CLHYLOGrtrace4(r)) {
        int i;
        const kuda_array_header_t *t_h = kuda_table_elts(r->headers_in);
        const kuda_table_entry_t *t_elt = (kuda_table_entry_t *)t_h->elts;
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE4, 0, r,
                      "Headers received from client:");
        for (i = 0; i < t_h->nelts; i++, t_elt++) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE4, 0, r, "  %s: %s",
                          clhy_escape_logitem(r->pool, t_elt->key),
                          clhy_escape_logitem(r->pool, t_elt->val));
        }
    }

#if KUDA_HAS_THREADS
    kuda_thread_mutex_create(&r->invoke_mtx, KUDA_THREAD_MUTEX_DEFAULT, r->pool);
    kuda_thread_mutex_lock(r->invoke_mtx);
#endif
    access_status = clhy_run_quick_handler(r, 0);  /* Not a look-up request */
    if (access_status == DECLINED) {
        access_status = clhy_process_request_internal(r);
        if (access_status == OK) {
            access_status = clhy_invoke_handler(r);
        }
    }

    if (access_status == SUSPENDED) {
        /* TODO: Should move these steps into a generic function, so cAPIs
         * working on a suspended request can also call _ENTRY again.
         */
        CLHY_PROCESS_REQUEST_RETURN((uintptr_t)r, r->uri, access_status);
        if (clhy_extended_status) {
            clhy_time_process_request(c->sbh, STOP_PREQUEST);
        }
        if (c->cs)
            c->cs->state = CONN_STATE_SUSPENDED;
#if KUDA_HAS_THREADS
        kuda_thread_mutex_unlock(r->invoke_mtx);
#endif
        return;
    }
#if KUDA_HAS_THREADS
    kuda_thread_mutex_unlock(r->invoke_mtx);
#endif

    clhy_die_r(access_status, r, HTTP_OK);

    clhy_process_request_after_handler(r);
}

CLHY_DECLARE(void) clhy_process_request(request_rec *r)
{
    kuda_bucket_brigade *bb;
    kuda_bucket *b;
    conn_rec *c = r->connection;
    kuda_status_t rv;

    clhy_process_async_request(r);

    if (!c->data_in_input_filters) {
        bb = kuda_brigade_create(c->pool, c->bucket_alloc);
        b = kuda_bucket_flush_create(c->bucket_alloc);
        KUDA_BRIGADE_INSERT_HEAD(bb, b);
        rv = clhy_pass_brigade(c->output_filters, bb);
        if (KUDA_STATUS_IS_TIMEUP(rv)) {
            /*
             * Notice a timeout as an error message. This might be
             * valuable for detecting clients with broken network
             * connections or possible DoS attacks.
             *
             * It is still safe to use r / r->pool here as the eor bucket
             * could not have been destroyed in the event of a timeout.
             */
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, rv, r, CLHYLOGNO(01581)
                          "Timeout while writing data for URI %s to the"
                          " client", r->unparsed_uri);
        }
    }
    if (clhy_extended_status) {
        clhy_time_process_request(c->sbh, STOP_PREQUEST);
    }
}

static kuda_table_t *rename_original_env(kuda_pool_t *p, kuda_table_t *t)
{
    const kuda_array_header_t *env_arr = kuda_table_elts(t);
    const kuda_table_entry_t *elts = (const kuda_table_entry_t *) env_arr->elts;
    kuda_table_t *new = kuda_table_make(p, env_arr->nalloc);
    int i;

    for (i = 0; i < env_arr->nelts; ++i) {
        if (!elts[i].key)
            continue;
        kuda_table_setn(new, kuda_pstrcat(p, "REDIRECT_", elts[i].key, NULL),
                  elts[i].val);
    }

    return new;
}

static request_rec *internal_internal_redirect(const char *new_uri,
                                               request_rec *r) {
    int access_status;
    request_rec *new;

    if (clhy_is_recursion_limit_exceeded(r)) {
        clhy_die(HTTP_INTERNAL_SERVER_ERROR, r);
        return NULL;
    }

    new = (request_rec *) kuda_pcalloc(r->pool, sizeof(request_rec));

    new->connection = r->connection;
    new->server     = r->server;
    new->pool       = r->pool;

    /*
     * A whole lot of this really ought to be shared with http_protocol.c...
     * another missing cleanup.  It's particularly inappropriate to be
     * setting header_only, etc., here.
     */

    new->method          = r->method;
    new->method_number   = r->method_number;
    new->allowed_methods = clhy_make_method_list(new->pool, 2);
    clhy_parse_uri(new, new_uri);
    new->parsed_uri.port_str = r->parsed_uri.port_str;
    new->parsed_uri.port = r->parsed_uri.port;

    new->request_config = clhy_create_request_config(r->pool);

    new->per_dir_config = r->server->lookup_defaults;

    new->prev = r;
    r->next   = new;

    new->useragent_addr = r->useragent_addr;
    new->useragent_ip = r->useragent_ip;

    /* Must have prev and next pointers set before calling create_request
     * hook.
     */
    clhy_run_create_request(new);

    /* Inherit the rest of the protocol info... */

    new->the_request = r->the_request;

    new->allowed         = r->allowed;

    new->status          = r->status;
    new->assbackwards    = r->assbackwards;
    new->header_only     = r->header_only;
    new->protocol        = r->protocol;
    new->proto_num       = r->proto_num;
    new->hostname        = r->hostname;
    new->request_time    = r->request_time;
    new->main            = r->main;

    new->headers_in      = r->headers_in;
    new->trailers_in     = r->trailers_in;
    new->headers_out     = kuda_table_make(r->pool, 12);
    if (clhy_is_HTTP_REDIRECT(new->status)) {
        const char *location = kuda_table_get(r->headers_out, "Location");
        if (location)
            kuda_table_setn(new->headers_out, "Location", location);
    }
    new->err_headers_out = r->err_headers_out;
    new->trailers_out    = kuda_table_make(r->pool, 5);
    new->subprocess_env  = rename_original_env(r->pool, r->subprocess_env);
    new->notes           = kuda_table_make(r->pool, 5);

    new->htaccess        = r->htaccess;
    new->no_cache        = r->no_cache;
    new->expecting_100   = r->expecting_100;
    new->no_local_copy   = r->no_local_copy;
    new->read_length     = r->read_length;     /* We can only read it once */
    new->vlist_validator = r->vlist_validator;

    new->proto_output_filters  = r->proto_output_filters;
    new->proto_input_filters   = r->proto_input_filters;

    new->input_filters   = new->proto_input_filters;

    if (new->main) {
        clhy_filter_t *f, *nextf;

        /* If this is a subrequest, the filter chain may contain a
         * mixture of filters specific to the old request (r), and
         * some inherited from r->main.  Here, inherit that filter
         * chain, and remove all those which are specific to the old
         * request; ensuring the subreq filter is left in place. */
        new->output_filters = r->output_filters;

        f = new->output_filters;
        do {
            nextf = f->next;

            if (f->r == r && f->frec != clhy_subreq_core_filter_handle) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01582)
                              "dropping filter '%s' in internal redirect from %s to %s",
                              f->frec->name, r->unparsed_uri, new_uri);

                /* To remove the filter, first set f->r to the *new*
                 * request_rec, so that ->output_filters on 'new' is
                 * changed (if necessary) when removing the filter. */
                f->r = new;
                clhy_remove_output_filter(f);
            }

            f = nextf;

            /* Stop at the protocol filters.  If a protocol filter has
             * been newly installed for this resource, better leave it
             * in place, though it's probably a misconfiguration or
             * filter bug to get into this state. */
        } while (f && f != new->proto_output_filters);
    }
    else {
        /* If this is not a subrequest, clear out all
         * resource-specific filters. */
        new->output_filters  = new->proto_output_filters;
    }

    update_r_in_filters(new->input_filters, r, new);
    update_r_in_filters(new->output_filters, r, new);

    kuda_table_setn(new->subprocess_env, "REDIRECT_STATUS",
                   kuda_itoa(r->pool, r->status));

    /* Begin by presuming any cAPI can make its own path_info assumptions,
     * until some cAPI interjects and changes the value.
     */
    new->used_path_info = CLHY_REQ_DEFAULT_PATH_INFO;

#if KUDA_HAS_THREADS
    new->invoke_mtx = r->invoke_mtx;
#endif

    /*
     * XXX: hmm.  This is because capi_setenvif and capi_unique_id really need
     * to do their thing on internal redirects as well.  Perhaps this is a
     * misnamed function.
     */
    if ((access_status = clhy_run_post_read_request(new))) {
        clhy_die(access_status, new);
        return NULL;
    }

    return new;
}

/* XXX: Is this function is so bogus and fragile that we deep-6 it? */
CLHY_DECLARE(void) clhy_internal_fast_redirect(request_rec *rr, request_rec *r)
{
    /* We need to tell POOL_DEBUG that we're guaranteeing that rr->pool
     * will exist as long as r->pool.  Otherwise we run into troubles because
     * some values in this request will be allocated in r->pool, and others in
     * rr->pool.
     */
    kuda_pool_join(r->pool, rr->pool);
    r->proxyreq = rr->proxyreq;
    r->no_cache = (r->no_cache && rr->no_cache);
    r->no_local_copy = (r->no_local_copy && rr->no_local_copy);
    r->mtime = rr->mtime;
    r->uri = rr->uri;
    r->filename = rr->filename;
    r->canonical_filename = rr->canonical_filename;
    r->path_info = rr->path_info;
    r->args = rr->args;
    r->finfo = rr->finfo;
    r->handler = rr->handler;
    clhy_set_content_type(r, rr->content_type);
    r->content_encoding = rr->content_encoding;
    r->content_languages = rr->content_languages;
    r->per_dir_config = rr->per_dir_config;
    /* copy output headers from subrequest, but leave negotiation headers */
    r->notes = kuda_table_overlay(r->pool, rr->notes, r->notes);
    r->headers_out = kuda_table_overlay(r->pool, rr->headers_out,
                                       r->headers_out);
    r->err_headers_out = kuda_table_overlay(r->pool, rr->err_headers_out,
                                           r->err_headers_out);
    r->trailers_out = kuda_table_overlay(r->pool, rr->trailers_out,
                                           r->trailers_out);
    r->subprocess_env = kuda_table_overlay(r->pool, rr->subprocess_env,
                                          r->subprocess_env);

    r->output_filters = rr->output_filters;
    r->input_filters = rr->input_filters;

    /* If any filters pointed at the now-defunct rr, we must point them
     * at our "new" instance of r.  In particular, some of rr's structures
     * will now be bogus (say rr->headers_out).  If a filter tried to modify
     * their f->r structure when it is pointing to rr, the real request_rec
     * will not get updated.  Fix that here.
     */
    update_r_in_filters(r->input_filters, rr, r);
    update_r_in_filters(r->output_filters, rr, r);

    if (r->main) {
        clhy_filter_t *next = r->output_filters;
        while (next && (next != r->proto_output_filters)) {
            if (next->frec == clhy_subreq_core_filter_handle) {
                break;
            }
            next = next->next;
        }
        if (!next || next == r->proto_output_filters) {
            clhy_add_output_filter_handle(clhy_subreq_core_filter_handle,
                                        NULL, r, r->connection);
        }
    }
    else {
        /*
         * We need to check if we now have the SUBREQ_CORE filter in our filter
         * chain. If this is the case we need to remove it since we are NO
         * subrequest. But we need to keep in mind that the SUBREQ_CORE filter
         * does not necessarily need to be the first filter in our chain. So we
         * need to go through the chain. But we only need to walk up the chain
         * until the proto_output_filters as the SUBREQ_CORE filter is below the
         * protocol filters.
         */
        clhy_filter_t *next;

        next = r->output_filters;
        while (next && (next->frec != clhy_subreq_core_filter_handle)
               && (next != r->proto_output_filters)) {
                next = next->next;
        }
        if (next && (next->frec == clhy_subreq_core_filter_handle)) {
            clhy_remove_output_filter(next);
        }
    }
}

CLHY_DECLARE(void) clhy_internal_redirect(const char *new_uri, request_rec *r)
{
    int access_status;
    request_rec *new = internal_internal_redirect(new_uri, r);

    CLHY_INTERNAL_REDIRECT(r->uri, new_uri);

    /* clhy_die was already called, if an error occured */
    if (!new) {
        return;
    }

    access_status = clhy_run_quick_handler(new, 0);  /* Not a look-up request */
    if (access_status == DECLINED) {
        access_status = clhy_process_request_internal(new);
        if (access_status == OK) {
            access_status = clhy_invoke_handler(new);
        }
    }
    clhy_die(access_status, new);
}

/* This function is designed for things like actions or CGI scripts, when
 * using AddHandler, and you want to preserve the content type across
 * an internal redirect.
 */
CLHY_DECLARE(void) clhy_internal_redirect_handler(const char *new_uri, request_rec *r)
{
    int access_status;
    request_rec *new = internal_internal_redirect(new_uri, r);

    /* clhy_die was already called, if an error occured */
    if (!new) {
        return;
    }

    if (r->handler)
        clhy_set_content_type(new, r->content_type);
    access_status = clhy_process_request_internal(new);
    if (access_status == OK) {
        access_status = clhy_invoke_handler(new);
    }
    clhy_die(access_status, new);
}

CLHY_DECLARE(void) clhy_allow_methods(request_rec *r, int reset, ...)
{
    const char *method;
    va_list methods;

    /*
     * Get rid of any current settings if requested; not just the
     * well-known methods but any extensions as well.
     */
    if (reset) {
        clhy_clear_method_list(r->allowed_methods);
    }

    va_start(methods, reset);
    while ((method = va_arg(methods, const char *)) != NULL) {
        clhy_method_list_add(r->allowed_methods, method);
    }
    va_end(methods);
}

CLHY_DECLARE(void) clhy_allow_standard_methods(request_rec *r, int reset, ...)
{
    int method;
    va_list methods;
    kuda_int64_t mask;

    /*
     * Get rid of any current settings if requested; not just the
     * well-known methods but any extensions as well.
     */
    if (reset) {
        clhy_clear_method_list(r->allowed_methods);
    }

    mask = 0;
    va_start(methods, reset);
    while ((method = va_arg(methods, int)) != -1) {
        mask |= (CLHY_METHOD_BIT << method);
    }
    va_end(methods);

    r->allowed_methods->method_mask |= mask;
}
