dnl cAPIs enabled in this directory by default

CLHYKUDEL_CAPIPATH_INIT(http)

http_objects="http_core.lo http_protocol.lo http_request.lo http_filters.lo chunk_filter.lo byterange_filter.lo http_etag.lo"

dnl capi_http should only be built as a static cAPI for now.
dnl this will hopefully be "fixed" at some point in the future by
dnl refactoring capi_http and moving some things to the core and
dnl vice versa so that the core does not depend upon capi_http.
if test "$enable_http" = "yes"; then
    enable_http="static"
elif test "$enable_http" = "shared"; then
    AC_MSG_ERROR([capi_http can not be built as a shared DSO])
fi

CLHYKUDEL_CAPI(http,[HTTP protocol handling.  The http cAPI is a basic one that enables the server to function as an HTTP server. It is only useful to disable it if you want to use another protocol cAPI instead. Don't disable this cAPI unless you are really sure what you are doing. Note: This cAPI will always be linked statically.], $http_objects, , static)
CLHYKUDEL_CAPI(mime, mapping of file-extension to MIME.  Disabling this cAPI is normally not recommended., , , yes)

CLHYKUDEL_CAPIPATH_FINISH
