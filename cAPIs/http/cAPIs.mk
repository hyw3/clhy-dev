libcapi_http.la: http_core.lo http_protocol.lo http_request.lo http_filters.lo chunk_filter.lo byterange_filter.lo http_etag.lo
	$(CAPI_LINK) http_core.lo http_protocol.lo http_request.lo http_filters.lo chunk_filter.lo byterange_filter.lo http_etag.lo $(CAPI_HTTP_LDADD)
capi_mime.la: capi_mime.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_mime.lo $(CAPI_MIME_LDADD)
DISTCLEAN_TARGETS = cAPIs.mk
static =  libcapi_http.la
shared =  capi_mime.la
