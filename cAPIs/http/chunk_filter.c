/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * chunk_filter.c --- HTTP/1.1 chunked transfer encoding filter.
 */

#include "kuda_strings.h"
#include "kuda_thread_proc.h"    /* for RLIMIT stuff */

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#include "wwhy.h"
#include "http_config.h"
#include "http_connection.h"
#include "http_core.h"
#include "http_protocol.h"  /* For index_of_response().  Grump. */
#include "http_request.h"

#include "util_filter.h"
#include "util_ebcdic.h"
#include "clhy_core.h"
#include "scoreboard.h"

#include "capi_core.h"

/*
 * A pointer to this is used to memorize in the filter context that a bad
 * gateway error bucket had been seen. It is used as an invented unique pointer.
 */
static char bad_gateway_seen;

kuda_status_t clhy_http_chunk_filter(clhy_filter_t *f, kuda_bucket_brigade *b)
{
#define ASCII_CRLF  "\015\012"
#define ASCII_ZERO  "\060"
    conn_rec *c = f->r->connection;
    kuda_bucket_brigade *more, *tmp;
    kuda_bucket *e;
    kuda_status_t rv;

    for (more = tmp = NULL; b; b = more, more = NULL) {
        kuda_off_t bytes = 0;
        kuda_bucket *eos = NULL;
        kuda_bucket *flush = NULL;
        /* XXX: chunk_hdr must remain at this scope since it is used in a
         *      transient bucket.
         */
        char chunk_hdr[20]; /* enough space for the snprintf below */


        for (e = KUDA_BRIGADE_FIRST(b);
             e != KUDA_BRIGADE_SENTINEL(b);
             e = KUDA_BUCKET_NEXT(e))
        {
            if (KUDA_BUCKET_IS_EOS(e)) {
                /* there shouldn't be anything after the eos */
                eos = e;
                break;
            }
            if (CLHY_BUCKET_IS_ERROR(e)
                && (((clhy_bucket_error *)(e->data))->status
                    == HTTP_BAD_GATEWAY)) {
                /*
                 * We had a broken backend. Memorize this in the filter
                 * context.
                 */
                f->ctx = &bad_gateway_seen;
                continue;
            }
            if (KUDA_BUCKET_IS_FLUSH(e)) {
                flush = e;
                if (e != KUDA_BRIGADE_LAST(b)) {
                    more = kuda_brigade_split_ex(b, KUDA_BUCKET_NEXT(e), tmp);
                }
                break;
            }
            else if (e->length == (kuda_size_t)-1) {
                /* unknown amount of data (e.g. a pipe) */
                const char *data;
                kuda_size_t len;

                rv = kuda_bucket_read(e, &data, &len, KUDA_BLOCK_READ);
                if (rv != KUDA_SUCCESS) {
                    return rv;
                }
                if (len > 0) {
                    /*
                     * There may be a new next bucket representing the
                     * rest of the data stream on which a read() may
                     * block so we pass down what we have so far.
                     */
                    bytes += len;
                    more = kuda_brigade_split_ex(b, KUDA_BUCKET_NEXT(e), tmp);
                    break;
                }
                else {
                    /* If there was nothing in this bucket then we can
                     * safely move on to the next one without pausing
                     * to pass down what we have counted up so far.
                     */
                    continue;
                }
            }
            else {
                bytes += e->length;
            }
        }

        /*
         * XXX: if there aren't very many bytes at this point it may
         * be a good idea to set them aside and return for more,
         * unless we haven't finished counting this brigade yet.
         */
        /* if there are content bytes, then wrap them in a chunk */
        if (bytes > 0) {
            kuda_size_t hdr_len;
            /*
             * Insert the chunk header, specifying the number of bytes in
             * the chunk.
             */
            hdr_len = kuda_snprintf(chunk_hdr, sizeof(chunk_hdr),
                                   "%" KUDA_UINT64_T_HEX_FMT CRLF, (kuda_uint64_t)bytes);
            clhy_xlate_proto_to_ascii(chunk_hdr, hdr_len);
            e = kuda_bucket_transient_create(chunk_hdr, hdr_len,
                                            c->bucket_alloc);
            KUDA_BRIGADE_INSERT_HEAD(b, e);

            /*
             * Insert the end-of-chunk CRLF before an EOS or
             * FLUSH bucket, or appended to the brigade
             */
            e = kuda_bucket_immortal_create(ASCII_CRLF, 2, c->bucket_alloc);
            if (eos != NULL) {
                KUDA_BUCKET_INSERT_BEFORE(eos, e);
            }
            else if (flush != NULL) {
                KUDA_BUCKET_INSERT_BEFORE(flush, e);
            }
            else {
                KUDA_BRIGADE_INSERT_TAIL(b, e);
            }
        }

        /* RFC 2616, Section 3.6.1
         *
         * If there is an EOS bucket, then prefix it with:
         *   1) the last-chunk marker ("0" CRLF)
         *   2) the trailer
         *   3) the end-of-chunked body CRLF
         *
         * We only do this if we have not seen an error bucket with
         * status HTTP_BAD_GATEWAY. We have memorized an
         * error bucket that we had seen in the filter context.
         * The error bucket with status HTTP_BAD_GATEWAY indicates that the
         * connection to the backend (capi_proxy) broke in the middle of the
         * response. In order to signal the client that something went wrong
         * we do not create the last-chunk marker and set c->keepalive to
         * CLHY_CONN_CLOSE in the core output filter.
         *
         * XXX: it would be nice to combine this with the end-of-chunk
         * marker above, but this is a bit more straight-forward for
         * now.
         */
        if (eos && !f->ctx) {
            /* XXX: (2) trailers ... does not yet exist */
            e = kuda_bucket_immortal_create(ASCII_ZERO ASCII_CRLF
                                           /* <trailers> */
                                           ASCII_CRLF, 5, c->bucket_alloc);
            KUDA_BUCKET_INSERT_BEFORE(eos, e);
        }

        /* pass the brigade to the next filter. */
        rv = clhy_pass_brigade(f->next, b);
        if (rv != KUDA_SUCCESS || eos != NULL) {
            return rv;
        }
        tmp = b;
        kuda_brigade_cleanup(tmp);
    }
    return KUDA_SUCCESS;
}
