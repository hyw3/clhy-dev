/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strings.h"
#include "kuda_thread_proc.h"    /* for RLIMIT stuff */

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#include "wwhy.h"
#include "http_config.h"
#include "http_connection.h"
#include "http_core.h"
#include "http_protocol.h"   /* For index_of_response().  Grump. */
#include "http_request.h"

#include "util_filter.h"
#include "util_ebcdic.h"
#include "clhy_core.h"
#include "scoreboard.h"

#include "capi_core.h"

/* Handles for core filters */
CLHY_DECLARE_DATA clhy_filter_rec_t *clhy_http_input_filter_handle;
CLHY_DECLARE_DATA clhy_filter_rec_t *clhy_http_header_filter_handle;
CLHY_DECLARE_DATA clhy_filter_rec_t *clhy_chunk_filter_handle;
CLHY_DECLARE_DATA clhy_filter_rec_t *clhy_http_outerror_filter_handle;
CLHY_DECLARE_DATA clhy_filter_rec_t *clhy_byterange_filter_handle;

CLHY_DECLARE_DATA const char *clhy_multipart_boundary;

/* If we are using an cLMP That Supports Async Connections,
 * use a different processing function
 */
static int async_clmp = 0;

static const char *set_keep_alive_timeout(cmd_parms *cmd, void *dummy,
                                          const char *arg)
{
    kuda_interval_time_t timeout;
    const char *err = clhy_check_cmd_context(cmd, NOT_IN_DIR_LOC_FILE);
    if (err != NULL) {
        return err;
    }

    /* Stolen from capi_proxy.c */
    if (clhy_timeout_parameter_parse(arg, &timeout, "s") != KUDA_SUCCESS)
        return "KeepAliveTimeout has wrong format";
    cmd->server->keep_alive_timeout = timeout;

    /* We don't want to take into account whether or not KeepAliveTimeout is
     * set for the main server, because if no http_capi directive is used
     * for a vhost, it will inherit the http_srv_cfg from the main server.
     * However keep_alive_timeout_set helps determine whether the vhost should
     * use its own configured timeout or the one from the vhost declared first
     * on the same IP:port (ie. c->base_server, and the legacy behaviour).
     */
    if (cmd->server->is_virtual) {
        cmd->server->keep_alive_timeout_set = 1;
    }
    return NULL;
}

static const char *set_keep_alive(cmd_parms *cmd, void *dummy,
                                  int arg)
{
    const char *err = clhy_check_cmd_context(cmd, NOT_IN_DIR_LOC_FILE);
    if (err != NULL) {
        return err;
    }

    cmd->server->keep_alive = arg;
    return NULL;
}

static const char *set_keep_alive_max(cmd_parms *cmd, void *dummy,
                                      const char *arg)
{
    const char *err = clhy_check_cmd_context(cmd, NOT_IN_DIR_LOC_FILE);
    if (err != NULL) {
        return err;
    }

    cmd->server->keep_alive_max = atoi(arg);
    return NULL;
}

static const command_rec http_cmds[] = {
    CLHY_INIT_TAKE1("KeepAliveTimeout", set_keep_alive_timeout, NULL, RSRC_CONF,
                  "Keep-Alive timeout duration (sec)"),
    CLHY_INIT_TAKE1("MaxKeepAliveRequests", set_keep_alive_max, NULL, RSRC_CONF,
                  "Maximum number of Keep-Alive requests per connection, "
                  "or 0 for infinite"),
    CLHY_INIT_FLAG("KeepAlive", set_keep_alive, NULL, RSRC_CONF,
                  "Whether persistent connections should be On or Off"),
    { NULL }
};

static const char *http_scheme(const request_rec *r)
{
    /*
     * The http cAPI shouldn't return anything other than
     * "http" (the default) or "https".
     */
    if (r->server->server_scheme &&
        (strcmp(r->server->server_scheme, "https") == 0))
        return "https";

    return "http";
}

static kuda_port_t http_port(const request_rec *r)
{
    if (r->server->server_scheme &&
        (strcmp(r->server->server_scheme, "https") == 0))
        return DEFAULT_HTTPS_PORT;

    return DEFAULT_HTTP_PORT;
}

static int clhy_process_http_async_connection(conn_rec *c)
{
    request_rec *r;
    conn_state_t *cs = c->cs;

    CLHY_DEBUG_ASSERT(cs != NULL);
    CLHY_DEBUG_ASSERT(cs->state == CONN_STATE_READ_REQUEST_LINE);

    while (cs->state == CONN_STATE_READ_REQUEST_LINE) {
        clhy_update_child_status_from_conn(c->sbh, SERVER_BUSY_READ, c);

        if ((r = clhy_read_request(c))) {

            c->keepalive = CLHY_CONN_UNKNOWN;
            /* process the request if it was read without error */

            if (r->status == HTTP_OK) {
                cs->state = CONN_STATE_HANDLER;
                clhy_update_child_status(c->sbh, SERVER_BUSY_WRITE, r);
                clhy_process_async_request(r);
                /* After the call to clhy_process_request, the
                 * request pool may have been deleted.  We set
                 * r=NULL here to ensure that any dereference
                 * of r that might be added later in this function
                 * will result in a segfault immediately instead
                 * of nondeterministic failures later.
                 */
                r = NULL;
            }

            if (cs->state != CONN_STATE_WRITE_COMPLETION &&
                cs->state != CONN_STATE_SUSPENDED) {
                /* Something went wrong; close the connection */
                cs->state = CONN_STATE_LINGER;
            }
        }
        else {   /* clhy_read_request failed - client may have closed */
            cs->state = CONN_STATE_LINGER;
        }
    }

    return OK;
}

static int clhy_process_http_sync_connection(conn_rec *c)
{
    request_rec *r;
    conn_state_t *cs = c->cs;
    kuda_socket_t *csd = NULL;
    int clmp_state = 0;

    /*
     * Read and process each request found on our connection
     * until no requests are left or we decide to close.
     */

    clhy_update_child_status_from_conn(c->sbh, SERVER_BUSY_READ, c);
    while ((r = clhy_read_request(c)) != NULL) {
        kuda_interval_time_t keep_alive_timeout = r->server->keep_alive_timeout;

        /* To preserve legacy behaviour, use the keepalive timeout from the
         * base server (first on this IP:port) when none is explicitly
         * configured on this server.
         */
        if (!r->server->keep_alive_timeout_set) {
            keep_alive_timeout = c->base_server->keep_alive_timeout;
        }

        c->keepalive = CLHY_CONN_UNKNOWN;
        /* process the request if it was read without error */

        if (r->status == HTTP_OK) {
            if (cs)
                cs->state = CONN_STATE_HANDLER;
            clhy_update_child_status(c->sbh, SERVER_BUSY_WRITE, r);
            clhy_process_request(r);
            /* After the call to clhy_process_request, the
             * request pool will have been deleted.  We set
             * r=NULL here to ensure that any dereference
             * of r that might be added later in this function
             * will result in a segfault immediately instead
             * of nondeterministic failures later.
             */
            r = NULL;
        }

        if (c->keepalive != CLHY_CONN_KEEPALIVE || c->aborted)
            break;

        clhy_update_child_status(c->sbh, SERVER_BUSY_KEEPALIVE, NULL);

        if (clhy_clmp_query(CLHY_CLMPQ_CLMP_STATE, &clmp_state)) {
            break;
        }

        if (clmp_state == CLHY_CLMPQ_STOPPING) {
          break;
        }

        if (!csd) {
            csd = clhy_get_conn_socket(c);
        }
        kuda_socket_opt_set(csd, KUDA_INCOMPLETE_READ, 1);
        kuda_socket_timeout_set(csd, keep_alive_timeout);
        /* Go straight to select() to wait for the next request */
    }

    return OK;
}

static int clhy_process_http_connection(conn_rec *c)
{
    if (async_clmp && !c->clogging_input_filters) {
        return clhy_process_http_async_connection(c);
    }
    else {
        return clhy_process_http_sync_connection(c);
    }
}

static int http_create_request(request_rec *r)
{
    if (!r->main && !r->prev) {
        clhy_add_output_filter_handle(clhy_byterange_filter_handle,
                                    NULL, r, r->connection);
        clhy_add_output_filter_handle(clhy_content_length_filter_handle,
                                    NULL, r, r->connection);
        clhy_add_output_filter_handle(clhy_http_header_filter_handle,
                                    NULL, r, r->connection);
        clhy_add_output_filter_handle(clhy_http_outerror_filter_handle,
                                    NULL, r, r->connection);
    }

    return OK;
}

static int http_send_options(request_rec *r)
{
    if ((r->method_number == M_OPTIONS) && r->uri && (r->uri[0] == '*') &&
         (r->uri[1] == '\0')) {
        return DONE;           /* Send HTTP pong, without Allow header */
    }
    return DECLINED;
}

static int http_post_config(kuda_pool_t *p, kuda_pool_t *plog, kuda_pool_t *ptemp, server_rec *s)
{
    kuda_uint64_t val;
    if (clhy_clmp_query(CLHY_CLMPQ_IS_ASYNC, &async_clmp) != KUDA_SUCCESS) {
        async_clmp = 0;
    }
    clhy_random_insecure_bytes(&val, sizeof(val));
    clhy_multipart_boundary = kuda_psprintf(p, "%0" KUDA_UINT64_T_HEX_FMT, val);

    return OK;
}

static void register_hooks(kuda_pool_t *p)
{
    clhy_hook_post_config(http_post_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_process_connection(clhy_process_http_connection, NULL, NULL,
                               KUDA_HOOK_REALLY_LAST);
    clhy_hook_map_to_storage(clhy_send_http_trace,NULL,NULL,KUDA_HOOK_MIDDLE);
    clhy_hook_map_to_storage(http_send_options,NULL,NULL,KUDA_HOOK_MIDDLE);
    clhy_hook_http_scheme(http_scheme,NULL,NULL,KUDA_HOOK_REALLY_LAST);
    clhy_hook_default_port(http_port,NULL,NULL,KUDA_HOOK_REALLY_LAST);
    clhy_hook_create_request(http_create_request, NULL, NULL, KUDA_HOOK_REALLY_LAST);
    clhy_http_input_filter_handle =
        clhy_register_input_filter("HTTP_IN", clhy_http_filter,
                                 NULL, CLHY_FTYPE_PROTOCOL);
    clhy_http_header_filter_handle =
        clhy_register_output_filter("HTTP_HEADER", clhy_http_header_filter,
                                  NULL, CLHY_FTYPE_PROTOCOL);
    clhy_chunk_filter_handle =
        clhy_register_output_filter("CHUNK", clhy_http_chunk_filter,
                                  NULL, CLHY_FTYPE_TRANSCODE);
    clhy_http_outerror_filter_handle =
        clhy_register_output_filter("HTTP_OUTERROR", clhy_http_outerror_filter,
                                  NULL, CLHY_FTYPE_PROTOCOL);
    clhy_byterange_filter_handle =
        clhy_register_output_filter("BYTERANGE", clhy_byterange_filter,
                                  NULL, CLHY_FTYPE_PROTOCOL);
    clhy_method_registry_init(p);
}

CLHY_DECLARE_CAPI(http) = {
    STANDARD16_CAPI_STUFF,
    NULL,              /* create per-directory config structure */
    NULL,              /* merge per-directory config structures */
    NULL,              /* create per-server config structure */
    NULL,              /* merge per-server config structures */
    http_cmds,         /* command kuda_table_t */
    register_hooks     /* register hooks */
};
