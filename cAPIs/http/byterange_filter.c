/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * byterange_filter.c --- HTTP byterange filter and friends.
 */

#include "kuda.h"

#include "kuda_strings.h"
#include "kuda_buckets.h"
#include "kuda_lib.h"
#include "kuda_signal.h"

#define KUDA_WANT_STDIO          /* for sscanf */
#define KUDA_WANT_STRFUNC
#define KUDA_WANT_MEMFUNC
#include "kuda_want.h"

#include "util_filter.h"
#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_protocol.h"
#include "http_main.h"
#include "http_request.h"
#include "http_vhost.h"
#include "http_log.h"           /* For errors detected in basic auth common
                                 * support code... */
#include "kuda_date.h"           /* For kuda_date_parse_http and KUDA_DATE_BAD */
#include "util_charset.h"
#include "util_ebcdic.h"
#include "util_time.h"

#include "capi_core.h"

#if KUDA_HAVE_STDARG_H
#include <stdarg.h>
#endif
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifndef CLHY_DEFAULT_MAX_RANGES
#define CLHY_DEFAULT_MAX_RANGES 200
#endif
#ifndef CLHY_DEFAULT_MAX_OVERLAPS
#define CLHY_DEFAULT_MAX_OVERLAPS 20
#endif
#ifndef CLHY_DEFAULT_MAX_REVERSALS
#define CLHY_DEFAULT_MAX_REVERSALS 20
#endif

#define MAX_PREALLOC_RANGES 100

CLHYLOG_USE_CAPI(http);

typedef struct indexes_t {
    kuda_off_t start;
    kuda_off_t end;
} indexes_t;

/*
 * Returns: number of ranges (merged) or -1 for no-good
 */
static int clhy_set_byterange(request_rec *r, kuda_off_t clength,
                            kuda_array_header_t **indexes,
                            int *overlaps, int *reversals)
{
    const char *range;
    const char *ct;
    char *cur;
    kuda_array_header_t *merged;
    int num_ranges = 0, unsatisfiable = 0;
    kuda_off_t ostart = 0, oend = 0, sum_lengths = 0;
    int in_merge = 0;
    indexes_t *idx;
    int ranges = 1;
    int i;
    const char *it;

    *overlaps = 0;
    *reversals = 0;

    if (r->assbackwards) {
        return 0;
    }

    /*
     * Check for Range request-header (HTTP/1.1) or Request-Range for
     * backwards-compatibility with second-draft Luotonen/Franks
     * byte-ranges (e.g. Netscape Navigator 2-3).
     *
     * We support this form, with Request-Range, and (farther down) we
     * send multipart/x-byteranges instead of multipart/byteranges for
     * Request-Range based requests to work around a bug in Netscape
     * Navigator 2-3 and MSIE 3.
     */

    if (!(range = kuda_table_get(r->headers_in, "Range"))) {
        range = kuda_table_get(r->headers_in, "Request-Range");
    }

    if (!range || strncasecmp(range, "bytes=", 6) || r->status != HTTP_OK) {
        return 0;
    }

    /* is content already a single range? */
    if (kuda_table_get(r->headers_out, "Content-Range")) {
        return 0;
    }

    /* is content already a multiple range? */
    if ((ct = kuda_table_get(r->headers_out, "Content-Type"))
        && (!strncasecmp(ct, "multipart/byteranges", 20)
            || !strncasecmp(ct, "multipart/x-byteranges", 22))) {
            return 0;
        }

    /*
     * Check the If-Range header for Etag or Date.
     */
    if (CLHY_CONDITION_NOMATCH == clhy_condition_if_range(r, r->headers_out)) {
        return 0;
    }

    range += 6;
    it = range;
    while (*it) {
        if (*it++ == ',') {
            ranges++;
        }
    }
    it = range;
    if (ranges > MAX_PREALLOC_RANGES) {
        ranges = MAX_PREALLOC_RANGES;
    }
    *indexes = kuda_array_make(r->pool, ranges, sizeof(indexes_t));
    while ((cur = clhy_getword(r->pool, &range, ','))) {
        char *dash;
        char *errp;
        kuda_off_t number, start, end;

        if (!*cur)
            break;

        /*
         * Per RFC 2616 14.35.1: If there is at least one syntactically invalid
         * byte-range-spec, we must ignore the whole header.
         */

        if (!(dash = strchr(cur, '-'))) {
            return 0;
        }

        if (dash == cur) {
            /* In the form "-5" */
            if (kuda_strtoff(&number, dash+1, &errp, 10) || *errp) {
                return 0;
            }
            if (number < 1) {
                return 0;
            }
            start = clength - number;
            end = clength - 1;
        }
        else {
            *dash++ = '\0';
            if (kuda_strtoff(&number, cur, &errp, 10) || *errp) {
                return 0;
            }
            start = number;
            if (*dash) {
                if (kuda_strtoff(&number, dash, &errp, 10) || *errp) {
                    return 0;
                }
                end = number;
                if (start > end) {
                    return 0;
                }
            }
            else {                  /* "5-" */
                end = clength - 1;
                /*
                 * special case: 0-
                 *   ignore all other ranges provided
                 *   return as a single range: 0-
                 */
                if (start == 0) {
                    num_ranges = 0;
                    sum_lengths = 0;
                    in_merge = 1;
                    oend = end;
                    ostart = start;
                    kuda_array_clear(*indexes);
                    break;
                }
            }
        }

        if (start < 0) {
            start = 0;
        }
        if (start >= clength) {
            unsatisfiable = 1;
            continue;
        }
        if (end >= clength) {
            end = clength - 1;
        }

        if (!in_merge) {
            /* new set */
            ostart = start;
            oend = end;
            in_merge = 1;
            continue;
        }
        in_merge = 0;

        if (start >= ostart && end <= oend) {
            in_merge = 1;
        }

        if (start < ostart && end >= ostart-1) {
            ostart = start;
            ++*reversals;
            in_merge = 1;
        }
        if (end >= oend && start <= oend+1 ) {
            oend = end;
            in_merge = 1;
        }

        if (in_merge) {
            ++*overlaps;
            continue;
        } else {
            idx = (indexes_t *)kuda_array_push(*indexes);
            idx->start = ostart;
            idx->end = oend;
            sum_lengths += oend - ostart + 1;
            /* new set again */
            in_merge = 1;
            ostart = start;
            oend = end;
            num_ranges++;
        }
    }

    if (in_merge) {
        idx = (indexes_t *)kuda_array_push(*indexes);
        idx->start = ostart;
        idx->end = oend;
        sum_lengths += oend - ostart + 1;
        num_ranges++;
    }
    else if (num_ranges == 0 && unsatisfiable) {
        /* If all ranges are unsatisfiable, we should return 416 */
        return -1;
    }
    if (sum_lengths > clength) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, r,
                      "Sum of ranges larger than file, ignoring.");
        return 0;
    }

    /*
     * create the merged table now, now that we know we need it
     */
    merged = kuda_array_make(r->pool, num_ranges, sizeof(char *));
    idx = (indexes_t *)(*indexes)->elts;
    for (i = 0; i < (*indexes)->nelts; i++, idx++) {
        char **new = (char **)kuda_array_push(merged);
        *new = kuda_psprintf(r->pool, "%" KUDA_OFF_T_FMT "-%" KUDA_OFF_T_FMT,
                            idx->start, idx->end);
    }

    r->status = HTTP_PARTIAL_CONTENT;
    r->range = kuda_array_pstrcat(r->pool, merged, ',');
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01583)
                  "Range: %s | %s (%d : %d : %"KUDA_OFF_T_FMT")",
                  it, r->range, *overlaps, *reversals, clength);

    return num_ranges;
}

/*
 * Here we try to be compatible with clients that want multipart/x-byteranges
 * instead of multipart/byteranges (also see above), as per HTTP/1.1. We
 * look for the Request-Range header (e.g. Netscape 2 and 3) as an indication
 * that the browser supports an older protocol. We also check User-Agent
 * for Microsoft Internet Explorer 3, which needs this as well.
 */
static int use_range_x(request_rec *r)
{
    const char *ua;
    return (kuda_table_get(r->headers_in, "Request-Range")
            || ((ua = kuda_table_get(r->headers_in, "User-Agent"))
                && clhy_strstr_c(ua, "MSIE 3")));
}

#define BYTERANGE_FMT "%" KUDA_OFF_T_FMT "-%" KUDA_OFF_T_FMT "/%" KUDA_OFF_T_FMT

static kuda_status_t copy_brigade_range(kuda_bucket_brigade *bb,
                                       kuda_bucket_brigade *bbout,
                                       kuda_off_t start,
                                       kuda_off_t end)
{
    kuda_bucket *first = NULL, *last = NULL, *out_first = NULL, *e;
    kuda_uint64_t pos = 0, off_first = 0, off_last = 0;
    kuda_status_t rv;
    kuda_uint64_t start64, end64;
    kuda_off_t pofft = 0;

    /*
     * Once we know that start and end are >= 0 convert everything to kuda_uint64_t.
     * See the comments in kuda_brigade_partition why.
     * In short kuda_off_t (for values >= 0)and kuda_size_t fit into kuda_uint64_t.
     */
    start64 = (kuda_uint64_t)start;
    end64 = (kuda_uint64_t)end;

    if (start < 0 || end < 0 || start64 > end64)
        return KUDA_EINVAL;

    for (e = KUDA_BRIGADE_FIRST(bb);
         e != KUDA_BRIGADE_SENTINEL(bb);
         e = KUDA_BUCKET_NEXT(e))
    {
        kuda_uint64_t elen64;
        /* we know that no bucket has undefined length (-1) */
        CLHY_DEBUG_ASSERT(e->length != (kuda_size_t)(-1));
        elen64 = (kuda_uint64_t)e->length;
        if (!first && (elen64 + pos > start64)) {
            first = e;
            off_first = pos;
        }
        if (elen64 + pos > end64) {
            last = e;
            off_last = pos;
            break;
        }
        pos += elen64;
    }
    if (!first || !last)
        return KUDA_EINVAL;

    e = first;
    while (1)
    {
        kuda_bucket *copy;
        CLHY_DEBUG_ASSERT(e != KUDA_BRIGADE_SENTINEL(bb));
        rv = kuda_bucket_copy(e, &copy);
        if (rv != KUDA_SUCCESS) {
            kuda_brigade_cleanup(bbout);
            return rv;
        }

        KUDA_BRIGADE_INSERT_TAIL(bbout, copy);
        if (e == first) {
            if (off_first != start64) {
                rv = kuda_bucket_split(copy, (kuda_size_t)(start64 - off_first));
                if (rv != KUDA_SUCCESS) {
                    kuda_brigade_cleanup(bbout);
                    return rv;
                }
                out_first = KUDA_BUCKET_NEXT(copy);
                kuda_bucket_delete(copy);
            }
            else {
                out_first = copy;
            }
        }
        if (e == last) {
            if (e == first) {
                off_last += start64 - off_first;
                copy = out_first;
            }
            if (end64 - off_last != (kuda_uint64_t)e->length) {
                rv = kuda_bucket_split(copy, (kuda_size_t)(end64 + 1 - off_last));
                if (rv != KUDA_SUCCESS) {
                    kuda_brigade_cleanup(bbout);
                    return rv;
                }
                copy = KUDA_BUCKET_NEXT(copy);
                if (copy != KUDA_BRIGADE_SENTINEL(bbout)) {
                    kuda_bucket_delete(copy);
                }
            }
            break;
        }
        e = KUDA_BUCKET_NEXT(e);
    }

    CLHY_DEBUG_ASSERT(KUDA_SUCCESS == kuda_brigade_length(bbout, 1, &pofft));
    pos = (kuda_uint64_t)pofft;
    CLHY_DEBUG_ASSERT(pos == end64 - start64 + 1);
    return KUDA_SUCCESS;
}

static kuda_status_t send_416(clhy_filter_t *f, kuda_bucket_brigade *tmpbb)
{
    kuda_bucket *e;
    conn_rec *c = f->r->connection;
    clhy_remove_output_filter(f);
    f->r->status = HTTP_OK;
    e = clhy_bucket_error_create(HTTP_RANGE_NOT_SATISFIABLE, NULL,
                               f->r->pool, c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(tmpbb, e);
    e = kuda_bucket_eos_create(c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(tmpbb, e);
    return clhy_pass_brigade(f->next, tmpbb);
}

CLHY_CORE_DECLARE_NONSTD(kuda_status_t) clhy_byterange_filter(clhy_filter_t *f,
                                                         kuda_bucket_brigade *bb)
{
    request_rec *r = f->r;
    conn_rec *c = r->connection;
    kuda_bucket *e;
    kuda_bucket_brigade *bsend;
    kuda_bucket_brigade *tmpbb;
    kuda_off_t range_start;
    kuda_off_t range_end;
    kuda_off_t clength = 0;
    kuda_status_t rv;
    int found = 0;
    int num_ranges;
    char *bound_head = NULL;
    kuda_array_header_t *indexes;
    indexes_t *idx;
    int i;
    int original_status;
    int max_ranges, max_overlaps, max_reversals;
    int overlaps = 0, reversals = 0;
    core_dir_config *core_conf = clhy_get_core_capi_config(r->per_dir_config);

    max_ranges = ( (core_conf->max_ranges >= 0 || core_conf->max_ranges == CLHY_MAXRANGES_UNLIMITED)
                   ? core_conf->max_ranges
                   : CLHY_DEFAULT_MAX_RANGES );
    max_overlaps = ( (core_conf->max_overlaps >= 0 || core_conf->max_overlaps == CLHY_MAXRANGES_UNLIMITED)
                  ? core_conf->max_overlaps
                  : CLHY_DEFAULT_MAX_OVERLAPS );
    max_reversals = ( (core_conf->max_reversals >= 0 || core_conf->max_reversals == CLHY_MAXRANGES_UNLIMITED)
                  ? core_conf->max_reversals
                  : CLHY_DEFAULT_MAX_REVERSALS );
    /*
     * Iterate through the brigade until reaching EOS or a bucket with
     * unknown length.
     */
    for (e = KUDA_BRIGADE_FIRST(bb);
         (e != KUDA_BRIGADE_SENTINEL(bb) && !KUDA_BUCKET_IS_EOS(e)
          && e->length != (kuda_size_t)-1);
         e = KUDA_BUCKET_NEXT(e)) {
        clength += e->length;
    }

    /*
     * Don't attempt to do byte range work if this brigade doesn't
     * contain an EOS, or if any of the buckets has an unknown length;
     * this avoids the cases where it is expensive to perform
     * byteranging (i.e. may require arbitrary amounts of memory).
     */
    if (!KUDA_BUCKET_IS_EOS(e) || clength <= 0) {
        clhy_remove_output_filter(f);
        return clhy_pass_brigade(f->next, bb);
    }

    original_status = r->status;
    num_ranges = clhy_set_byterange(r, clength, &indexes, &overlaps, &reversals);

    /* No Ranges or we hit a limit? We have nothing to do, get out of the way. */
    if (num_ranges == 0 ||
        (max_ranges >= 0 && num_ranges > max_ranges) ||
        (max_overlaps >= 0 && overlaps > max_overlaps) ||
        (max_reversals >= 0 && reversals > max_reversals)) {
        r->status = original_status;
        clhy_remove_output_filter(f);
        return clhy_pass_brigade(f->next, bb);
    }

    /* this brigade holds what we will be sending */
    bsend = kuda_brigade_create(r->pool, c->bucket_alloc);

    if (num_ranges < 0)
        return send_416(f, bsend);

    if (num_ranges > 1) {
        /* Is clhy_make_content_type required here? */
        const char *orig_ct = clhy_make_content_type(r, r->content_type);

        clhy_set_content_type(r, kuda_pstrcat(r->pool, "multipart",
                                           use_range_x(r) ? "/x-" : "/",
                                           "byteranges; boundary=",
                                           clhy_multipart_boundary, NULL));

        if (orig_ct) {
            bound_head = kuda_pstrcat(r->pool,
                                     CRLF "--", clhy_multipart_boundary,
                                     CRLF "Content-type: ",
                                     orig_ct,
                                     CRLF "Content-range: bytes ",
                                     NULL);
        }
        else {
            /* if we have no type for the content, do our best */
            bound_head = kuda_pstrcat(r->pool,
                                     CRLF "--", clhy_multipart_boundary,
                                     CRLF "Content-range: bytes ",
                                     NULL);
        }
        clhy_xlate_proto_to_ascii(bound_head, strlen(bound_head));
    }

    tmpbb = kuda_brigade_create(r->pool, c->bucket_alloc);

    idx = (indexes_t *)indexes->elts;
    for (i = 0; i < indexes->nelts; i++, idx++) {
        range_start = idx->start;
        range_end = idx->end;

        rv = copy_brigade_range(bb, tmpbb, range_start, range_end);
        if (rv != KUDA_SUCCESS ) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01584)
                          "copy_brigade_range() failed [%" KUDA_OFF_T_FMT
                          "-%" KUDA_OFF_T_FMT ",%" KUDA_OFF_T_FMT "]",
                          range_start, range_end, clength);
            continue;
        }
        found = 1;

        /*
         * For single range requests, we must produce Content-Range header.
         * Otherwise, we need to produce the multipart boundaries.
         */
        if (num_ranges == 1) {
            kuda_table_setn(r->headers_out, "Content-Range",
                           kuda_psprintf(r->pool, "bytes " BYTERANGE_FMT,
                                        range_start, range_end, clength));
        }
        else {
            char *ts;

            e = kuda_bucket_pool_create(bound_head, strlen(bound_head),
                                       r->pool, c->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(bsend, e);

            ts = kuda_psprintf(r->pool, BYTERANGE_FMT CRLF CRLF,
                              range_start, range_end, clength);
            clhy_xlate_proto_to_ascii(ts, strlen(ts));
            e = kuda_bucket_pool_create(ts, strlen(ts), r->pool,
                                       c->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(bsend, e);
        }

        KUDA_BRIGADE_CONCAT(bsend, tmpbb);
        if (i && !(i & 0x1F)) {
            /*
             * Every now and then, pass what we have down the filter chain.
             * In this case, the content-length filter cannot calculate and
             * set the content length and we must remove any Content-Length
             * header already present.
             */
            kuda_table_unset(r->headers_out, "Content-Length");
            if ((rv = clhy_pass_brigade(f->next, bsend)) != KUDA_SUCCESS)
                return rv;
            kuda_brigade_cleanup(bsend);
        }
    }

    if (found == 0) {
        /* bsend is assumed to be empty if we get here. */
        return send_416(f, bsend);
    }

    if (num_ranges > 1) {
        char *end;

        /* add the final boundary */
        end = kuda_pstrcat(r->pool, CRLF "--", clhy_multipart_boundary, "--" CRLF,
                          NULL);
        clhy_xlate_proto_to_ascii(end, strlen(end));
        e = kuda_bucket_pool_create(end, strlen(end), r->pool, c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bsend, e);
    }

    e = kuda_bucket_eos_create(c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bsend, e);

    /* we're done with the original content - all of our data is in bsend. */
    kuda_brigade_cleanup(bb);
    kuda_brigade_destroy(tmpbb);

    /* send our multipart output */
    return clhy_pass_brigade(f->next, bsend);
}
