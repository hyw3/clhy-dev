/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "clhy_provider.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "http_request.h"
#include "kuda_lib.h"
#include "kuda_dbd.h"
#include "capi_dbd.h"
#include "kuda_strings.h"
#include "capi_auth.h"
#include "kuda_md5.h"
#include "kudelman_version.h"

cAPI CLHY_CAPI_DECLARE_DATA authn_dbd_capi;

typedef struct {
    const char *user;
    const char *realm;
} authn_dbd_conf;

/* optional function - look it up once in post_config */
static clhy_dbd_t *(*authn_dbd_acquire_fn)(request_rec*) = NULL;
static void (*authn_dbd_prepare_fn)(server_rec*, const char*, const char*) = NULL;
static KUDA_OPTIONAL_FN_TYPE(clhy_authn_cache_store) *authn_cache_store = NULL;
#define AUTHN_CACHE_STORE(r,user,realm,data) \
    if (authn_cache_store != NULL) \
        authn_cache_store((r), "dbd", (user), (realm), (data))

static void *authn_dbd_cr_conf(kuda_pool_t *pool, char *dummy)
{
    authn_dbd_conf *ret = kuda_pcalloc(pool, sizeof(authn_dbd_conf));
    return ret;
}

static void *authn_dbd_merge_conf(kuda_pool_t *pool, void *BASE, void *ADD)
{
    authn_dbd_conf *add = ADD;
    authn_dbd_conf *base = BASE;
    authn_dbd_conf *ret = kuda_palloc(pool, sizeof(authn_dbd_conf));
    ret->user = (add->user == NULL) ? base->user : add->user;
    ret->realm = (add->realm == NULL) ? base->realm : add->realm;
    return ret;
}

static const char *authn_dbd_prepare(cmd_parms *cmd, void *cfg, const char *query)
{
    static unsigned int label_num = 0;
    char *label;
    const char *err = clhy_check_cmd_context(cmd, NOT_IN_HTACCESS);
    if (err)
        return err;

    if (authn_dbd_prepare_fn == NULL) {
        authn_dbd_prepare_fn = KUDA_RETRIEVE_OPTIONAL_FN(clhy_dbd_prepare);
        if (authn_dbd_prepare_fn == NULL) {
            return "You must load capi_dbd to enable AuthDBD functions";
        }
        authn_dbd_acquire_fn = KUDA_RETRIEVE_OPTIONAL_FN(clhy_dbd_acquire);
    }
    label = kuda_psprintf(cmd->pool, "authn_dbd_%d", ++label_num);

    authn_dbd_prepare_fn(cmd->server, query, label);

    /* save the label here for our own use */
    return clhy_set_string_slot(cmd, cfg, label);
}

static const command_rec authn_dbd_cmds[] =
{
    CLHY_INIT_TAKE1("AuthDBDUserPWQuery", authn_dbd_prepare,
                  (void *)KUDA_OFFSETOF(authn_dbd_conf, user), ACCESS_CONF,
                  "Query used to fetch password for user"),
    CLHY_INIT_TAKE1("AuthDBDUserRealmQuery", authn_dbd_prepare,
                  (void *)KUDA_OFFSETOF(authn_dbd_conf, realm), ACCESS_CONF,
                  "Query used to fetch password for user+realm"),
    {NULL}
};

static authn_status authn_dbd_password(request_rec *r, const char *user,
                                       const char *password)
{
    kuda_status_t rv;
    const char *dbd_password = NULL;
    kuda_dbd_prepared_t *statement;
    kuda_dbd_results_t *res = NULL;
    kuda_dbd_row_t *row = NULL;
    int ret;

    authn_dbd_conf *conf = clhy_get_capi_config(r->per_dir_config,
                                                &authn_dbd_capi);
    clhy_dbd_t *dbd = authn_dbd_acquire_fn(r);
    if (dbd == NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01653)
                      "Failed to acquire database connection to look up "
                      "user '%s'", user);
        return AUTH_GENERAL_ERROR;
    }

    if (conf->user == NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01654)
                      "No AuthDBDUserPWQuery has been specified");
        return AUTH_GENERAL_ERROR;
    }

    statement = kuda_hash_get(dbd->prepared, conf->user, KUDA_HASH_KEY_STRING);
    if (statement == NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01655)
                      "A prepared statement could not be found for "
                      "AuthDBDUserPWQuery with the key '%s'", conf->user);
        return AUTH_GENERAL_ERROR;
    }
    if ((ret = kuda_dbd_pvselect(dbd->driver, r->pool, dbd->handle, &res,
                                statement, 0, user, NULL)) != 0) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01656)
                      "Query execution error looking up '%s' "
                      "in database [%s]",
                      user, kuda_dbd_error(dbd->driver, dbd->handle, ret));
        return AUTH_GENERAL_ERROR;
    }
    for (rv = kuda_dbd_get_row(dbd->driver, r->pool, res, &row, -1);
         rv != -1;
         rv = kuda_dbd_get_row(dbd->driver, r->pool, res, &row, -1)) {
        if (rv != 0) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01657)
                          "Error retrieving results while looking up '%s' "
                          "in database", user);
            return AUTH_GENERAL_ERROR;
        }
        if (dbd_password == NULL) {
#if KUDELMAN_MAJOR_VERSION > 1 || (KUDELMAN_MAJOR_VERSION == 1 && KUDELMAN_MINOR_VERSION >= 3)
            /* add the rest of the columns to the environment */
            int i = 1;
            const char *name;
            for (name = kuda_dbd_get_name(dbd->driver, res, i);
                 name != NULL;
                 name = kuda_dbd_get_name(dbd->driver, res, i)) {

                char *str = kuda_pstrcat(r->pool, AUTHN_PREFIX,
                                        name,
                                        NULL);
                int j = sizeof(AUTHN_PREFIX)-1; /* string length of "AUTHENTICATE_", excluding the trailing NIL */
                while (str[j]) {
                    if (!kuda_isalnum(str[j])) {
                        str[j] = '_';
                    }
                    else {
                        str[j] = kuda_toupper(str[j]);
                    }
                    j++;
                }
                kuda_table_set(r->subprocess_env, str,
                              kuda_dbd_get_entry(dbd->driver, row, i));
                i++;
            }
#endif
            dbd_password = kuda_pstrdup(r->pool,
                                       kuda_dbd_get_entry(dbd->driver, row, 0));
        }
        /* we can't break out here or row won't get cleaned up */
    }

    if (!dbd_password) {
        return AUTH_USER_NOT_FOUND;
    }
    AUTHN_CACHE_STORE(r, user, NULL, dbd_password);

    rv = kuda_password_validate(password, dbd_password);

    if (rv != KUDA_SUCCESS) {
        return AUTH_DENIED;
    }

    return AUTH_GRANTED;
}

static authn_status authn_dbd_realm(request_rec *r, const char *user,
                                    const char *realm, char **rethash)
{
    kuda_status_t rv;
    const char *dbd_hash = NULL;
    kuda_dbd_prepared_t *statement;
    kuda_dbd_results_t *res = NULL;
    kuda_dbd_row_t *row = NULL;
    int ret;

    authn_dbd_conf *conf = clhy_get_capi_config(r->per_dir_config,
                                                &authn_dbd_capi);
    clhy_dbd_t *dbd = authn_dbd_acquire_fn(r);
    if (dbd == NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01658)
                      "Failed to acquire database connection to look up "
                      "user '%s:%s'", user, realm);
        return AUTH_GENERAL_ERROR;
    }
    if (conf->realm == NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01659)
                      "No AuthDBDUserRealmQuery has been specified");
        return AUTH_GENERAL_ERROR;
    }
    statement = kuda_hash_get(dbd->prepared, conf->realm, KUDA_HASH_KEY_STRING);
    if (statement == NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01660)
                      "A prepared statement could not be found for "
                      "AuthDBDUserRealmQuery with the key '%s'", conf->realm);
        return AUTH_GENERAL_ERROR;
    }
    if ((ret = kuda_dbd_pvselect(dbd->driver, r->pool, dbd->handle, &res,
                                statement, 0, user, realm, NULL)) != 0) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01661)
                      "Query execution error looking up '%s:%s' "
                      "in database [%s]",
                      user, realm,
                      kuda_dbd_error(dbd->driver, dbd->handle, ret));
        return AUTH_GENERAL_ERROR;
    }
    for (rv = kuda_dbd_get_row(dbd->driver, r->pool, res, &row, -1);
         rv != -1;
         rv = kuda_dbd_get_row(dbd->driver, r->pool, res, &row, -1)) {
        if (rv != 0) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01662)
                          "Error retrieving results while looking up '%s:%s' "
                          "in database", user, realm);
            return AUTH_GENERAL_ERROR;
        }
        if (dbd_hash == NULL) {
#if KUDELMAN_MAJOR_VERSION > 1 || (KUDELMAN_MAJOR_VERSION == 1 && KUDELMAN_MINOR_VERSION >= 3)
            /* add the rest of the columns to the environment */
            int i = 1;
            const char *name;
            for (name = kuda_dbd_get_name(dbd->driver, res, i);
                 name != NULL;
                 name = kuda_dbd_get_name(dbd->driver, res, i)) {

                char *str = kuda_pstrcat(r->pool, AUTHN_PREFIX,
                                        name,
                                        NULL);
                int j = sizeof(AUTHN_PREFIX)-1; /* string length of "AUTHENTICATE_", excluding the trailing NIL */
                while (str[j]) {
                    if (!kuda_isalnum(str[j])) {
                        str[j] = '_';
                    }
                    else {
                        str[j] = kuda_toupper(str[j]);
                    }
                    j++;
                }
                kuda_table_set(r->subprocess_env, str,
                              kuda_dbd_get_entry(dbd->driver, row, i));
                i++;
            }
#endif
            dbd_hash = kuda_pstrdup(r->pool,
                                   kuda_dbd_get_entry(dbd->driver, row, 0));
        }
        /* we can't break out here or row won't get cleaned up */
    }

    if (!dbd_hash) {
        return AUTH_USER_NOT_FOUND;
    }
    AUTHN_CACHE_STORE(r, user, realm, dbd_hash);
    *rethash = kuda_pstrdup(r->pool, dbd_hash);
    return AUTH_USER_FOUND;
}

static void opt_retr(void)
{
    authn_cache_store = KUDA_RETRIEVE_OPTIONAL_FN(clhy_authn_cache_store);
}

static void authn_dbd_hooks(kuda_pool_t *p)
{
    static const authn_provider authn_dbd_provider = {
        &authn_dbd_password,
        &authn_dbd_realm
    };

    clhy_register_auth_provider(p, AUTHN_PROVIDER_GROUP, "dbd",
                              AUTHN_PROVIDER_VERSION,
                              &authn_dbd_provider, CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_hook_optional_fn_retrieve(opt_retr, NULL, NULL, KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(authn_dbd) =
{
    STANDARD16_CAPI_STUFF,
    authn_dbd_cr_conf,
    authn_dbd_merge_conf,
    NULL,
    NULL,
    authn_dbd_cmds,
    authn_dbd_hooks
};
