/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CAPI_AUTHZ_OWNER_H
#define CAPI_AUTHZ_OWNER_H

#include "http_request.h"

/* capi_authz_owner exports an optional function which retrieves the
 * group name of the file identified by r->filename, if available, or
 * else returns NULL. */
KUDA_DECLARE_OPTIONAL_FN(char*, authz_owner_get_file_group, (request_rec *r));

#endif /* CAPI_AUTHZ_OWNER_H */
