/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strings.h"
#include "kuda_md5.h"            /* for kuda_password_validate */

#include "clhy_config.h"
#include "clhy_provider.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_request.h"

#include "capi_auth.h"

typedef struct {
    char *pwfile;
} authn_file_config_rec;

static KUDA_OPTIONAL_FN_TYPE(clhy_authn_cache_store) *authn_cache_store = NULL;
#define AUTHN_CACHE_STORE(r,user,realm,data) \
    if (authn_cache_store != NULL) \
        authn_cache_store((r), "file", (user), (realm), (data))

static void *create_authn_file_dir_config(kuda_pool_t *p, char *d)
{
    authn_file_config_rec *conf = kuda_palloc(p, sizeof(*conf));

    conf->pwfile = NULL;     /* just to illustrate the default really */
    return conf;
}

static const command_rec authn_file_cmds[] =
{
    CLHY_INIT_TAKE1("AuthUserFile", clhy_set_file_slot,
                  (void *)KUDA_OFFSETOF(authn_file_config_rec, pwfile),
                  OR_AUTHCFG, "text file containing user IDs and passwords"),
    {NULL}
};

cAPI CLHY_CAPI_DECLARE_DATA authn_file_capi;

static authn_status check_password(request_rec *r, const char *user,
                                   const char *password)
{
    authn_file_config_rec *conf = clhy_get_capi_config(r->per_dir_config,
                                                       &authn_file_capi);
    clhy_configfile_t *f;
    char l[MAX_STRING_LEN];
    kuda_status_t status;
    char *file_password = NULL;

    if (!conf->pwfile) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01619)
                      "AuthUserFile not specified in the configuration");
        return AUTH_GENERAL_ERROR;
    }

    status = clhy_pcfg_openfile(&f, r->pool, conf->pwfile);

    if (status != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01620)
                      "Could not open password file: %s", conf->pwfile);
        return AUTH_GENERAL_ERROR;
    }

    while (!(clhy_cfg_getline(l, MAX_STRING_LEN, f))) {
        const char *rpw, *w;

        /* Skip # or blank lines. */
        if ((l[0] == '#') || (!l[0])) {
            continue;
        }

        rpw = l;
        w = clhy_getword(r->pool, &rpw, ':');

        if (!strcmp(user, w)) {
            file_password = clhy_getword(r->pool, &rpw, ':');
            break;
        }
    }
    clhy_cfg_closefile(f);

    if (!file_password) {
        return AUTH_USER_NOT_FOUND;
    }
    AUTHN_CACHE_STORE(r, user, NULL, file_password);

    status = kuda_password_validate(password, file_password);
    if (status != KUDA_SUCCESS) {
        return AUTH_DENIED;
    }

    return AUTH_GRANTED;
}

static authn_status get_realm_hash(request_rec *r, const char *user,
                                   const char *realm, char **rethash)
{
    authn_file_config_rec *conf = clhy_get_capi_config(r->per_dir_config,
                                                       &authn_file_capi);
    clhy_configfile_t *f;
    char l[MAX_STRING_LEN];
    kuda_status_t status;
    char *file_hash = NULL;

    if (!conf->pwfile) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01621)
                      "AuthUserFile not specified in the configuration");
        return AUTH_GENERAL_ERROR;
    }

    status = clhy_pcfg_openfile(&f, r->pool, conf->pwfile);

    if (status != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01622)
                      "Could not open password file: %s", conf->pwfile);
        return AUTH_GENERAL_ERROR;
    }

    while (!(clhy_cfg_getline(l, MAX_STRING_LEN, f))) {
        const char *rpw, *w, *x;

        /* Skip # or blank lines. */
        if ((l[0] == '#') || (!l[0])) {
            continue;
        }

        rpw = l;
        w = clhy_getword(r->pool, &rpw, ':');
        x = clhy_getword(r->pool, &rpw, ':');

        if (x && w && !strcmp(user, w) && !strcmp(realm, x)) {
            /* Remember that this is a md5 hash of user:realm:password.  */
            file_hash = clhy_getword(r->pool, &rpw, ':');
            break;
        }
    }
    clhy_cfg_closefile(f);

    if (!file_hash) {
        return AUTH_USER_NOT_FOUND;
    }

    *rethash = file_hash;
    AUTHN_CACHE_STORE(r, user, realm, file_hash);

    return AUTH_USER_FOUND;
}

static const authn_provider authn_file_provider =
{
    &check_password,
    &get_realm_hash,
};

static void opt_retr(void)
{
    authn_cache_store = KUDA_RETRIEVE_OPTIONAL_FN(clhy_authn_cache_store);
}
static void register_hooks(kuda_pool_t *p)
{
    clhy_register_auth_provider(p, AUTHN_PROVIDER_GROUP, "file",
                              AUTHN_PROVIDER_VERSION,
                              &authn_file_provider, CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_hook_optional_fn_retrieve(opt_retr, NULL, NULL, KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(authn_file) =
{
    STANDARD16_CAPI_STUFF,
    create_authn_file_dir_config,    /* dir config creater */
    NULL,                            /* dir merger --- default is to override */
    NULL,                            /* server config */
    NULL,                            /* merge server config */
    authn_file_cmds,                 /* command kuda_table_t */
    register_hooks                   /* register hooks */
};
