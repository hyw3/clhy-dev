/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Security options etc.
 *
 * cAPI derived from code originally written by Rob McCool
 *
 */

#include "kuda_strings.h"
#include "kuda_network_io.h"
#include "kuda_md5.h"
#include "kuda_hash.h"

#define KUDA_WANT_STRFUNC
#define KUDA_WANT_BYTEFUNC
#include "kuda_want.h"

#include "clhy_config.h"
#include "clhy_provider.h"
#include "wwhy.h"
#include "http_core.h"
#include "http_config.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_request.h"

#include "capi_auth.h"

#if KUDA_HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif

/*
 * To save memory if the same subnets are used in hundres of vhosts, we store
 * each subnet only once and use this temporary hash to find it again.
 */
static kuda_hash_t *parsed_subnets;

static kuda_ipsubnet_t *localhost_v4;
#if KUDA_HAVE_IPV6
static kuda_ipsubnet_t *localhost_v6;
#endif

static int in_domain(const char *domain, const char *what)
{
    int dl = strlen(domain);
    int wl = strlen(what);

    if ((wl - dl) >= 0) {
        if (strcasecmp(domain, &what[wl - dl]) != 0) {
            return 0;
        }

        /* Make sure we matched an *entire* subdomain --- if the user
         * said 'allow from good.com', we don't want people from nogood.com
         * to be able to get in.
         */

        if (wl == dl) {
            return 1;                /* matched whole thing */
        }
        else {
            return (domain[0] == '.' || what[wl - dl - 1] == '.');
        }
    }
    else {
        return 0;
    }
}

static const char *ip_parse_config(cmd_parms *cmd,
                                   const char *require_line,
                                   const void **parsed_require_line)
{
    const char *t, *w;
    int count = 0;
    kuda_ipsubnet_t **ip;
    kuda_pool_t *ptemp = cmd->temp_pool;
    kuda_pool_t *p = cmd->pool;

    /* The 'ip' provider will allow the configuration to specify a list of
        ip addresses to check rather than a single address.  This is different
        from the previous host based syntax. */

    t = require_line;
    while ((w = clhy_getword_conf(ptemp, &t)) && w[0])
        count++;

    if (count == 0)
        return "'require ip' requires an argument";

    ip = kuda_pcalloc(p, sizeof(kuda_ipsubnet_t *) * (count + 1));
    *parsed_require_line = ip;

    t = require_line;
    while ((w = clhy_getword_conf(ptemp, &t)) && w[0]) {
        char *addr = kuda_pstrdup(ptemp, w);
        char *mask;
        kuda_status_t rv;

        if (parsed_subnets &&
            (*ip = kuda_hash_get(parsed_subnets, w, KUDA_HASH_KEY_STRING)) != NULL)
        {
            /* we already have parsed this subnet */
            ip++;
            continue;
        }

        if ((mask = clhy_strchr(addr, '/')))
            *mask++ = '\0';

        rv = kuda_ipsubnet_create(ip, addr, mask, p);

        if(KUDA_STATUS_IS_EINVAL(rv)) {
            /* looked nothing like an IP address */
            return kuda_psprintf(p, "ip address '%s' appears to be invalid", w);
        }
        else if (rv != KUDA_SUCCESS) {
            return kuda_psprintf(p, "ip address '%s' appears to be invalid: %pm",
                                w, &rv);
        }

        if (parsed_subnets)
            kuda_hash_set(parsed_subnets, w, KUDA_HASH_KEY_STRING, *ip);
        ip++;
    }

    return NULL;
}

static authz_status ip_check_authorization(request_rec *r,
                                           const char *require_line,
                                           const void *parsed_require_line)
{
    /* kuda_ipsubnet_test should accept const but doesn't */
    kuda_ipsubnet_t **ip = (kuda_ipsubnet_t **)parsed_require_line;

    while (*ip) {
        if (kuda_ipsubnet_test(*ip, r->useragent_addr))
            return AUTHZ_GRANTED;
        ip++;
    }

    /* authz_core will log the require line and the result at DEBUG */
    return AUTHZ_DENIED;
}

static authz_status host_check_authorization(request_rec *r,
                                             const char *require_line,
                                             const void *parsed_require_line)
{
    const char *t, *w;
    const char *remotehost = NULL;
    int remotehost_is_ip;

    remotehost = clhy_get_useragent_host(r, REMOTE_DOUBLE_REV, &remotehost_is_ip);

    if ((remotehost == NULL) || remotehost_is_ip) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01753)
                      "access check of '%s' to %s failed, reason: unable to get the "
                      "remote host name", require_line, r->uri);
    }
    else {
        const char *err = NULL;
        const clhy_expr_info_t *expr = parsed_require_line;
        const char *require;

        require = clhy_expr_str_exec(r, expr, &err);
        if (err) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02593)
                          "authz_host authorize: require host: Can't "
                          "evaluate require expression: %s", err);
            return AUTHZ_DENIED;
        }

        /* The 'host' provider will allow the configuration to specify a list of
            host names to check rather than a single name.  This is different
            from the previous host based syntax. */
        t = require;
        while ((w = clhy_getword_conf(r->pool, &t)) && w[0]) {
            if (in_domain(w, remotehost)) {
                return AUTHZ_GRANTED;
            }
        }
    }

    /* authz_core will log the require line and the result at DEBUG */
    return AUTHZ_DENIED;
}

static authz_status
forward_dns_check_authorization(request_rec *r,
                                const char *require_line,
                                const void *parsed_require_line)
{
    const char *err = NULL;
    const clhy_expr_info_t *expr = parsed_require_line;
    const char *require, *t;
    char *w;

    /* the require line is an expression, which is evaluated now. */
    require = clhy_expr_str_exec(r, expr, &err);
    if (err) {
      clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(03354)
                    "authz_host authorize: require forward-dns: "
                    "Can't evaluate require expression: %s", err);
      return AUTHZ_DENIED;
    }

    /* tokenize expected list of names */
    t = require;
    while ((w = clhy_getword_conf(r->pool, &t)) && w[0]) {

        kuda_sockaddr_t *sa;
        kuda_status_t rv;
        char *hash_ptr;

        /* stop on clhy configuration file comments */
        if ((hash_ptr = clhy_strchr(w, '#'))) {
            if (hash_ptr == w) {
                break;
            }
            *hash_ptr = '\0';
        }

        /* does the client ip match one of the names? */
        rv = kuda_sockaddr_info_get(&sa, w, KUDA_UNSPEC, 0, 0, r->pool);
        if (rv == KUDA_SUCCESS) {

            while (sa) {
                int match = kuda_sockaddr_equal(sa, r->useragent_addr);

                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(03355)
                              "access check for %s as '%s': %s",
                              r->useragent_ip, w, match? "yes": "no");
                if (match) {
                    return AUTHZ_GRANTED;
                }

                sa = sa->next;
            }
        }
        else {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(03356)
                          "No sockaddr info for \"%s\"", w);
        }

        /* stop processing, we are in a comment */
        if (hash_ptr) {
            break;
        }
    }

    return AUTHZ_DENIED;
}

static authz_status local_check_authorization(request_rec *r,
                                              const char *require_line,
                                              const void *parsed_require_line)
{
     if (   kuda_sockaddr_equal(r->connection->local_addr,
                               r->useragent_addr)
         || kuda_ipsubnet_test(localhost_v4, r->useragent_addr)
#if KUDA_HAVE_IPV6
         || kuda_ipsubnet_test(localhost_v6, r->useragent_addr)
#endif
        )
     {
        return AUTHZ_GRANTED;
     }

     return AUTHZ_DENIED;
}

static const char *host_parse_config(cmd_parms *cmd, const char *require_line,
                                     const void **parsed_require_line)
{
    const char *expr_err = NULL;
    clhy_expr_info_t *expr;

    expr = clhy_expr_parse_cmd(cmd, require_line, CLHY_EXPR_FLAG_STRING_RESULT,
            &expr_err, NULL);

    if (expr_err)
        return kuda_pstrcat(cmd->temp_pool,
                           "Cannot parse expression in require line: ",
                           expr_err, NULL);

    *parsed_require_line = expr;

    return NULL;
}

static const authz_provider authz_ip_provider =
{
    &ip_check_authorization,
    &ip_parse_config,
};

static const authz_provider authz_host_provider =
{
    &host_check_authorization,
    &host_parse_config,
};

static const authz_provider authz_forward_dns_provider =
{
    &forward_dns_check_authorization,
    &host_parse_config,
};

static const authz_provider authz_local_provider =
{
    &local_check_authorization,
    NULL,
};


static int authz_host_pre_config(kuda_pool_t *p, kuda_pool_t *plog,
                                 kuda_pool_t *ptemp)
{
    /* we only use this hash in the parse config phase, ptemp is enough */
    parsed_subnets = kuda_hash_make(ptemp);

    kuda_ipsubnet_create(&localhost_v4, "127.0.0.0", "8", p);
    kuda_hash_set(parsed_subnets, "127.0.0.0/8", KUDA_HASH_KEY_STRING, localhost_v4);

#if KUDA_HAVE_IPV6
    kuda_ipsubnet_create(&localhost_v6, "::1", NULL, p);
    kuda_hash_set(parsed_subnets, "::1", KUDA_HASH_KEY_STRING, localhost_v6);
#endif

    return OK;
}

static int authz_host_post_config(kuda_pool_t *p, kuda_pool_t *plog,
                                  kuda_pool_t *ptemp, server_rec *s)
{
    /* make sure we don't use this during .htaccess parsing */
    parsed_subnets = NULL;

    return OK;
}

static void register_hooks(kuda_pool_t *p)
{
    clhy_hook_pre_config(authz_host_pre_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_post_config(authz_host_post_config, NULL, NULL, KUDA_HOOK_MIDDLE);

    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "ip",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_ip_provider, CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "host",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_host_provider, CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "forward-dns",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_forward_dns_provider,
                              CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "local",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_local_provider, CLHY_AUTH_INTERNAL_PER_CONF);
}

CLHY_DECLARE_CAPI(authz_host) =
{
    STANDARD16_CAPI_STUFF,
    NULL,                           /* dir config creater */
    NULL,                           /* dir merger --- default is to override */
    NULL,                           /* server config */
    NULL,                           /* merge server config */
    NULL,
    register_hooks                  /* register hooks */
};
