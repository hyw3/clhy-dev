/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * http_auth: authentication
 *
 * Rob McCool & Brian Behlendorf.
 *
 * Adapted to cLHy by rst.
 *
 */

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"
#include "kuda_strings.h"
#include "kuda_dbm.h"
#include "kuda_md5.h"        /* for kuda_password_validate */

#include "clhy_provider.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_request.h"   /* for clhy_hook_(check_user_id | auth_checker)*/

#include "capi_auth.h"

static KUDA_OPTIONAL_FN_TYPE(clhy_authn_cache_store) *authn_cache_store = NULL;
#define AUTHN_CACHE_STORE(r,user,realm,data) \
    if (authn_cache_store != NULL) \
        authn_cache_store((r), "dbm", (user), (realm), (data))

typedef struct {
    const char *pwfile;
    const char *dbmtype;
} authn_dbm_config_rec;

static void *create_authn_dbm_dir_config(kuda_pool_t *p, char *d)
{
    authn_dbm_config_rec *conf = kuda_palloc(p, sizeof(*conf));

    conf->pwfile = NULL;
    conf->dbmtype = "default";

    return conf;
}

static const command_rec authn_dbm_cmds[] =
{
    CLHY_INIT_TAKE1("AuthDBMUserFile", clhy_set_file_slot,
     (void *)KUDA_OFFSETOF(authn_dbm_config_rec, pwfile),
     OR_AUTHCFG, "dbm database file containing user IDs and passwords"),
    CLHY_INIT_TAKE1("AuthDBMType", clhy_set_string_slot,
     (void *)KUDA_OFFSETOF(authn_dbm_config_rec, dbmtype),
     OR_AUTHCFG, "what type of DBM file the user file is"),
    {NULL}
};

cAPI CLHY_CAPI_DECLARE_DATA authn_dbm_capi;

static kuda_status_t fetch_dbm_value(const char *dbmtype, const char *dbmfile,
                                    const char *user, char **value,
                                    kuda_pool_t *pool)
{
    kuda_dbm_t *f;
    kuda_datum_t key, val;
    kuda_status_t rv;

    rv = kuda_dbm_open_ex(&f, dbmtype, dbmfile, KUDA_DBM_READONLY,
                         KUDA_PLATFORM_DEFAULT, pool);

    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    key.dptr = (char*)user;
#ifndef NETSCAPE_DBM_COMPAT
    key.dsize = strlen(key.dptr);
#else
    key.dsize = strlen(key.dptr) + 1;
#endif

    *value = NULL;

    if (kuda_dbm_fetch(f, key, &val) == KUDA_SUCCESS && val.dptr) {
        *value = kuda_pstrmemdup(pool, val.dptr, val.dsize);
    }

    kuda_dbm_close(f);

    return rv;
}

static authn_status check_dbm_pw(request_rec *r, const char *user,
                                 const char *password)
{
    authn_dbm_config_rec *conf = clhy_get_capi_config(r->per_dir_config,
                                                      &authn_dbm_capi);
    kuda_status_t rv;
    char *dbm_password;
    char *colon_pw;

    rv = fetch_dbm_value(conf->dbmtype, conf->pwfile, user, &dbm_password,
                         r->pool);

    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01754)
                      "could not open dbm (type %s) auth file: %s",
                      conf->dbmtype, conf->pwfile);
        return AUTH_GENERAL_ERROR;
    }

    if (!dbm_password) {
        return AUTH_USER_NOT_FOUND;
    }

    colon_pw = clhy_strchr(dbm_password, ':');
    if (colon_pw) {
        *colon_pw = '\0';
    }
    AUTHN_CACHE_STORE(r, user, NULL, dbm_password);

    rv = kuda_password_validate(password, dbm_password);

    if (rv != KUDA_SUCCESS) {
        return AUTH_DENIED;
    }

    return AUTH_GRANTED;
}

static authn_status get_dbm_realm_hash(request_rec *r, const char *user,
                                       const char *realm, char **rethash)
{
    authn_dbm_config_rec *conf = clhy_get_capi_config(r->per_dir_config,
                                                      &authn_dbm_capi);
    kuda_status_t rv;
    char *dbm_hash;
    char *colon_hash;

    rv = fetch_dbm_value(conf->dbmtype, conf->pwfile,
                         kuda_pstrcat(r->pool, user, ":", realm, NULL),
                         &dbm_hash, r->pool);

    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01755)
                      "Could not open dbm (type %s) hash file: %s",
                      conf->dbmtype, conf->pwfile);
        return AUTH_GENERAL_ERROR;
    }

    if (!dbm_hash) {
        return AUTH_USER_NOT_FOUND;
    }

    colon_hash = clhy_strchr(dbm_hash, ':');
    if (colon_hash) {
        *colon_hash = '\0';
    }

    *rethash = dbm_hash;
    AUTHN_CACHE_STORE(r, user, realm, dbm_hash);

    return AUTH_USER_FOUND;
}

static const authn_provider authn_dbm_provider =
{
    &check_dbm_pw,
    &get_dbm_realm_hash,
};

static void opt_retr(void)
{
    authn_cache_store = KUDA_RETRIEVE_OPTIONAL_FN(clhy_authn_cache_store);
}
static void register_hooks(kuda_pool_t *p)
{
    clhy_register_auth_provider(p, AUTHN_PROVIDER_GROUP, "dbm",
                              AUTHN_PROVIDER_VERSION,
                              &authn_dbm_provider, CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_hook_optional_fn_retrieve(opt_retr, NULL, NULL, KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(authn_dbm) =
{
    STANDARD16_CAPI_STUFF,
    create_authn_dbm_dir_config, /* dir config creater */
    NULL,                        /* dir merger --- default is to override */
    NULL,                        /* server config */
    NULL,                        /* merge server config */
    authn_dbm_cmds,              /* command kuda_table_t */
    register_hooks               /* register hooks */
};
