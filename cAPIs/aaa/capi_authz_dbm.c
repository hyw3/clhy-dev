/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"
#include "kuda_strings.h"
#include "kuda_dbm.h"
#include "kuda_md5.h"

#include "wwhy.h"
#include "http_config.h"
#include "clhy_provider.h"
#include "http_core.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_request.h"   /* for clhy_hook_(check_user_id | auth_checker)*/

#include "capi_auth.h"
#include "capi_authz_owner.h"

typedef struct {
    const char *grpfile;
    const char *dbmtype;
} authz_dbm_config_rec;


/* This should go into KUDA; perhaps with some nice
 * caching/locking/flocking of the open dbm file.
 */
static char *get_dbm_entry_as_str(kuda_pool_t *pool, kuda_dbm_t *f, char *key)
{
    kuda_datum_t d, q;
    q.dptr = key;

#ifndef NETSCAPE_DBM_COMPAT
    q.dsize = strlen(q.dptr);
#else
    q.dsize = strlen(q.dptr) + 1;
#endif

    if (kuda_dbm_fetch(f, q, &d) == KUDA_SUCCESS && d.dptr) {
        return kuda_pstrmemdup(pool, d.dptr, d.dsize);
    }

    return NULL;
}

static void *create_authz_dbm_dir_config(kuda_pool_t *p, char *d)
{
    authz_dbm_config_rec *conf = kuda_palloc(p, sizeof(*conf));

    conf->grpfile = NULL;
    conf->dbmtype = "default";

    return conf;
}

static const command_rec authz_dbm_cmds[] =
{
    CLHY_INIT_TAKE1("AuthDBMGroupFile", clhy_set_file_slot,
     (void *)KUDA_OFFSETOF(authz_dbm_config_rec, grpfile),
     OR_AUTHCFG, "database file containing group names and member user IDs"),
    CLHY_INIT_TAKE1("AuthzDBMType", clhy_set_string_slot,
     (void *)KUDA_OFFSETOF(authz_dbm_config_rec, dbmtype),
     OR_AUTHCFG, "what type of DBM file the group file is"),
    {NULL}
};

cAPI CLHY_CAPI_DECLARE_DATA authz_dbm_capi;

/* We do something strange with the group file.  If the group file
 * contains any : we assume the format is
 *      key=username value=":"groupname [":"anything here is ignored]
 * otherwise we now (0.8.14+) assume that the format is
 *      key=username value=groupname
 * The first allows the password and group files to be the same
 * physical DBM file;   key=username value=password":"groupname[":"anything]
 *
 * mark@telescope.org, 22Sep95
 */

static kuda_status_t get_dbm_grp(request_rec *r, char *key1, char *key2,
                                const char *dbmgrpfile, const char *dbtype,
                                const char ** out)
{
    char *grp_colon, *val;
    kuda_status_t retval;
    kuda_dbm_t *f;

    retval = kuda_dbm_open_ex(&f, dbtype, dbmgrpfile, KUDA_DBM_READONLY,
                             KUDA_PLATFORM_DEFAULT, r->pool);

    if (retval != KUDA_SUCCESS) {
        return retval;
    }

    /* Try key2 only if key1 failed */
    if (!(val = get_dbm_entry_as_str(r->pool, f, key1))) {
        val = get_dbm_entry_as_str(r->pool, f, key2);
    }

    kuda_dbm_close(f);

    if (val && (grp_colon = clhy_strchr(val, ':')) != NULL) {
        char *grp_colon2 = clhy_strchr(++grp_colon, ':');

        if (grp_colon2) {
            *grp_colon2 = '\0';
        }
        *out = grp_colon;
    }
    else {
        *out = val;
    }

    return retval;
}

static authz_status dbmgroup_check_authorization(request_rec *r,
                                                 const char *require_args,
                                                 const void *parsed_require_args)
{
    authz_dbm_config_rec *conf = clhy_get_capi_config(r->per_dir_config,
                                                      &authz_dbm_capi);
    char *user = r->user;

    const char *err = NULL;
    const clhy_expr_info_t *expr = parsed_require_args;
    const char *require;

    const char *t;
    char *w;
    const char *orig_groups = NULL;
    const char *realm = clhy_auth_name(r);
    const char *groups;
    char *v;

    if (!user) {
        return AUTHZ_DENIED_NO_USER;
    }

    if (!conf->grpfile) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01798)
                        "No group file was specified in the configuration");
        return AUTHZ_DENIED;
    }

    /* fetch group data from dbm file only once. */
    if (!orig_groups) {
        kuda_status_t status;

        status = get_dbm_grp(r, kuda_pstrcat(r->pool, user, ":", realm, NULL),
                             user, conf->grpfile, conf->dbmtype, &groups);

        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01799)
                          "could not open dbm (type %s) group access "
                          "file: %s", conf->dbmtype, conf->grpfile);
            return AUTHZ_GENERAL_ERROR;
        }

        if (groups == NULL) {
            /* no groups available, so exit immediately */
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01800)
                          "Authorization of user %s to access %s failed, reason: "
                          "user doesn't appear in DBM group file (%s).",
                          r->user, r->uri, conf->grpfile);
            return AUTHZ_DENIED;
        }

        orig_groups = groups;
    }

    require = clhy_expr_str_exec(r, expr, &err);
    if (err) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02591)
                      "authz_dbm authorize: require dbm-group: Can't "
                      "evaluate require expression: %s", err);
        return AUTHZ_DENIED;
    }

    t = require;
    while ((w = clhy_getword_white(r->pool, &t)) && w[0]) {
        groups = orig_groups;
        while (groups[0]) {
            v = clhy_getword(r->pool, &groups, ',');
            if (!strcmp(v, w)) {
                return AUTHZ_GRANTED;
            }
        }
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01801)
                  "Authorization of user %s to access %s failed, reason: "
                  "user is not part of the 'require'ed group(s).",
                  r->user, r->uri);

    return AUTHZ_DENIED;
}

static KUDA_OPTIONAL_FN_TYPE(authz_owner_get_file_group) *authz_owner_get_file_group;

static authz_status dbmfilegroup_check_authorization(request_rec *r,
                                                     const char *require_args,
                                                     const void *parsed_require_args)
{
    authz_dbm_config_rec *conf = clhy_get_capi_config(r->per_dir_config,
                                                      &authz_dbm_capi);
    char *user = r->user;
    const char *realm = clhy_auth_name(r);
    const char *filegroup = NULL;
    kuda_status_t status;
    const char *groups;
    char *v;

    if (!user) {
        return AUTHZ_DENIED_NO_USER;
    }

    if (!conf->grpfile) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01802)
                        "No group file was specified in the configuration");
        return AUTHZ_DENIED;
    }

    /* fetch group data from dbm file. */
    status = get_dbm_grp(r, kuda_pstrcat(r->pool, user, ":", realm, NULL),
                         user, conf->grpfile, conf->dbmtype, &groups);

    if (status != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01803)
                      "could not open dbm (type %s) group access "
                      "file: %s", conf->dbmtype, conf->grpfile);
        return AUTHZ_DENIED;
    }

    if (groups == NULL) {
        /* no groups available, so exit immediately */
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01804)
                      "Authorization of user %s to access %s failed, reason: "
                      "user doesn't appear in DBM group file (%s).",
                      r->user, r->uri, conf->grpfile);
        return AUTHZ_DENIED;
    }

    filegroup = authz_owner_get_file_group(r);

    if (filegroup) {
        while (groups[0]) {
            v = clhy_getword(r->pool, &groups, ',');
            if (!strcmp(v, filegroup)) {
                return AUTHZ_GRANTED;
            }
        }
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01805)
                  "Authorization of user %s to access %s failed, reason: "
                  "user is not part of the 'require'ed group(s).",
                  r->user, r->uri);

    return AUTHZ_DENIED;
}

static const char *dbm_parse_config(cmd_parms *cmd, const char *require_line,
                                     const void **parsed_require_line)
{
    const char *expr_err = NULL;
    clhy_expr_info_t *expr;

    expr = clhy_expr_parse_cmd(cmd, require_line, CLHY_EXPR_FLAG_STRING_RESULT,
            &expr_err, NULL);

    if (expr_err)
        return kuda_pstrcat(cmd->temp_pool,
                           "Cannot parse expression in require line: ",
                           expr_err, NULL);

    *parsed_require_line = expr;

    return NULL;
}

static const authz_provider authz_dbmgroup_provider =
{
    &dbmgroup_check_authorization,
    &dbm_parse_config,
};

static const authz_provider authz_dbmfilegroup_provider =
{
    &dbmfilegroup_check_authorization,
    NULL,
};

static void authz_dbm_getfns(void)
{
    authz_owner_get_file_group = KUDA_RETRIEVE_OPTIONAL_FN(authz_owner_get_file_group);
}

static void register_hooks(kuda_pool_t *p)
{
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "dbm-group",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_dbmgroup_provider,
                              CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "dbm-file-group",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_dbmfilegroup_provider,
                              CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_hook_optional_fn_retrieve(authz_dbm_getfns, NULL, NULL, KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(authz_dbm) =
{
    STANDARD16_CAPI_STUFF,
    create_authz_dbm_dir_config, /* dir config creater */
    NULL,                        /* dir merger --- default is to override */
    NULL,                        /* server config */
    NULL,                        /* merge server config */
    authz_dbm_cmds,              /* command kuda_table_t */
    register_hooks               /* register hooks */
};
