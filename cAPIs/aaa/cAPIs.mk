capi_authn_file.la: capi_authn_file.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authn_file.lo $(CAPI_AUTHN_FILE_LDADD)
capi_authn_dbm.la: capi_authn_dbm.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authn_dbm.lo $(CAPI_AUTHN_DBM_LDADD)
capi_authn_anon.la: capi_authn_anon.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authn_anon.lo $(CAPI_AUTHN_ANON_LDADD)
capi_authn_dbd.la: capi_authn_dbd.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authn_dbd.lo $(CAPI_AUTHN_DBD_LDADD)
capi_authn_socache.la: capi_authn_socache.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authn_socache.lo $(CAPI_AUTHN_SOCACHE_LDADD)
capi_authn_core.la: capi_authn_core.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authn_core.lo $(CAPI_AUTHN_CORE_LDADD)
capi_authz_host.la: capi_authz_host.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authz_host.lo $(CAPI_AUTHZ_HOST_LDADD)
capi_authz_groupfile.la: capi_authz_groupfile.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authz_groupfile.lo $(CAPI_AUTHZ_GROUPFILE_LDADD)
capi_authz_user.la: capi_authz_user.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authz_user.lo $(CAPI_AUTHZ_USER_LDADD)
capi_authz_dbm.la: capi_authz_dbm.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authz_dbm.lo $(CAPI_AUTHZ_DBM_LDADD)
capi_authz_owner.la: capi_authz_owner.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authz_owner.lo $(CAPI_AUTHZ_OWNER_LDADD)
capi_authz_dbd.la: capi_authz_dbd.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authz_dbd.lo $(CAPI_AUTHZ_DBD_LDADD)
capi_authz_core.la: capi_authz_core.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authz_core.lo $(CAPI_AUTHZ_CORE_LDADD)
capi_authnz_ldap.la: capi_authnz_ldap.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authnz_ldap.lo $(CAPI_AUTHNZ_LDAP_LDADD)
capi_authnz_fcgi.la: capi_authnz_fcgi.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_authnz_fcgi.lo $(CAPI_AUTHNZ_FCGI_LDADD)
capi_access_compat.la: capi_access_compat.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_access_compat.lo $(CAPI_ACCESS_COMPAT_LDADD)
capi_auth_basic.la: capi_auth_basic.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_auth_basic.lo $(CAPI_AUTH_BASIC_LDADD)
capi_auth_form.la: capi_auth_form.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_auth_form.lo $(CAPI_AUTH_FORM_LDADD)
capi_auth_digest.la: capi_auth_digest.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_auth_digest.lo $(CAPI_AUTH_DIGEST_LDADD)
capi_allowmethods.la: capi_allowmethods.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_allowmethods.lo $(CAPI_ALLOWMETHODS_LDADD)
DISTCLEAN_TARGETS = cAPIs.mk
static = 
shared =  capi_authn_file.la capi_authn_dbm.la capi_authn_anon.la capi_authn_dbd.la capi_authn_socache.la capi_authn_core.la capi_authz_host.la capi_authz_groupfile.la capi_authz_user.la capi_authz_dbm.la capi_authz_owner.la capi_authz_dbd.la capi_authz_core.la capi_authnz_ldap.la capi_authnz_fcgi.la capi_access_compat.la capi_auth_basic.la capi_auth_form.la capi_auth_digest.la capi_allowmethods.la
