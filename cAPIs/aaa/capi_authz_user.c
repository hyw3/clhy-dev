/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strings.h"

#include "clhy_config.h"
#include "clhy_provider.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_request.h"

#include "capi_auth.h"

typedef struct {
        int dummy;  /* just here to stop compiler warnings for now. */
} authz_user_config_rec;

static void *create_authz_user_dir_config(kuda_pool_t *p, char *d)
{
    authz_user_config_rec *conf = kuda_palloc(p, sizeof(*conf));

    return conf;
}

static const command_rec authz_user_cmds[] =
{
    {NULL}
};

cAPI CLHY_CAPI_DECLARE_DATA authz_user_capi;

static authz_status user_check_authorization(request_rec *r,
                                             const char *require_args,
                                             const void *parsed_require_args)
{
    const char *err = NULL;
    const clhy_expr_info_t *expr = parsed_require_args;
    const char *require;

    const char *t, *w;

    if (!r->user) {
        return AUTHZ_DENIED_NO_USER;
    }

    require = clhy_expr_str_exec(r, expr, &err);
    if (err) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02594)
                      "authz_user authorize: require user: Can't "
                      "evaluate require expression: %s", err);
        return AUTHZ_DENIED;
    }

    t = require;
    while ((w = clhy_getword_conf(r->pool, &t)) && w[0]) {
        if (!strcmp(r->user, w)) {
            return AUTHZ_GRANTED;
        }
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01663)
                  "access to %s failed, reason: user '%s' does not meet "
                  "'require'ments for user to be allowed access",
                  r->uri, r->user);

    return AUTHZ_DENIED;
}

static authz_status validuser_check_authorization(request_rec *r,
                                                  const char *require_line,
                                                  const void *parsed_require_line)
{
    if (!r->user) {
        return AUTHZ_DENIED_NO_USER;
    }

    return AUTHZ_GRANTED;
}

static const char *user_parse_config(cmd_parms *cmd, const char *require_line,
                                     const void **parsed_require_line)
{
    const char *expr_err = NULL;
    clhy_expr_info_t *expr;

    expr = clhy_expr_parse_cmd(cmd, require_line, CLHY_EXPR_FLAG_STRING_RESULT,
            &expr_err, NULL);

    if (expr_err)
        return kuda_pstrcat(cmd->temp_pool,
                           "Cannot parse expression in require line: ",
                           expr_err, NULL);

    *parsed_require_line = expr;

    return NULL;
}

static const authz_provider authz_user_provider =
{
    &user_check_authorization,
    &user_parse_config,
};
static const authz_provider authz_validuser_provider =
{
    &validuser_check_authorization,
    NULL,
};

static void register_hooks(kuda_pool_t *p)
{
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "user",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_user_provider, CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "valid-user",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_validuser_provider,
                              CLHY_AUTH_INTERNAL_PER_CONF);
}

CLHY_DECLARE_CAPI(authz_user) =
{
    STANDARD16_CAPI_STUFF,
    create_authz_user_dir_config, /* dir config creater */
    NULL,                         /* dir merger --- default is to override */
    NULL,                         /* server config */
    NULL,                         /* merge server config */
    authz_user_cmds,              /* command kuda_table_t */
    register_hooks                /* register hooks */
};
