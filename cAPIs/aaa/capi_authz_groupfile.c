/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* This cAPI is triggered by an
 *
 *          AuthGroupFile standard /path/to/file
 *
 * and the presence of a
 *
 *         require group <list-of-groups>
 *
 * In an applicable limit/directory block for that method.
 *
 * If there are no AuthGroupFile directives valid for
 * the request; we DECLINED.
 *
 * If the AuthGroupFile is defined; but somehow not
 * accessible: we SERVER_ERROR (was DECLINED).
 *
 * If there are no 'require ' directives defined for
 * this request then we DECLINED (was OK).
 *
 * If there are no 'require ' directives valid for
 * this request method then we DECLINED. (was OK)
 *
 * If there are any 'require group' blocks and we
 * are not in any group - we HTTP_UNAUTHORIZE
 *
 */

#include "kuda_strings.h"
#include "kuda_lib.h" /* kuda_isspace */

#include "clhy_config.h"
#include "clhy_provider.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_request.h"
#include "util_varbuf.h"

#include "capi_auth.h"
#include "capi_authz_owner.h"

typedef struct {
    char *groupfile;
} authz_groupfile_config_rec;

static void *create_authz_groupfile_dir_config(kuda_pool_t *p, char *d)
{
    authz_groupfile_config_rec *conf = kuda_palloc(p, sizeof(*conf));

    conf->groupfile = NULL;
    return conf;
}

static const command_rec authz_groupfile_cmds[] =
{
    CLHY_INIT_TAKE1("AuthGroupFile", clhy_set_file_slot,
                  (void *)KUDA_OFFSETOF(authz_groupfile_config_rec, groupfile),
                  OR_AUTHCFG,
                  "text file containing group names and member user IDs"),
    {NULL}
};

cAPI CLHY_CAPI_DECLARE_DATA authz_groupfile_capi;

#define VARBUF_INIT_LEN 512
#define VARBUF_MAX_LEN  (16*1024*1024)
static kuda_status_t groups_for_user(kuda_pool_t *p, char *user, char *grpfile,
                                    kuda_table_t ** out)
{
    clhy_configfile_t *f;
    kuda_table_t *grps = kuda_table_make(p, 15);
    kuda_pool_t *sp;
    struct clhy_varbuf vb;
    const char *group_name, *ll, *w;
    kuda_status_t status;
    kuda_size_t group_len;

    if ((status = clhy_pcfg_openfile(&f, p, grpfile)) != KUDA_SUCCESS) {
        return status ;
    }

    kuda_pool_create(&sp, p);
    clhy_varbuf_init(p, &vb, VARBUF_INIT_LEN);

    while (!(clhy_varbuf_cfg_getline(&vb, f, VARBUF_MAX_LEN))) {
        if ((vb.buf[0] == '#') || (!vb.buf[0])) {
            continue;
        }
        ll = vb.buf;
        kuda_pool_clear(sp);

        group_name = clhy_getword(sp, &ll, ':');
        group_len = strlen(group_name);

        while (group_len && kuda_isspace(*(group_name + group_len - 1))) {
            --group_len;
        }

        while (ll[0]) {
            w = clhy_getword_conf(sp, &ll);
            if (!strcmp(w, user)) {
                kuda_table_setn(grps, kuda_pstrmemdup(p, group_name, group_len),
                               "in");
                break;
            }
        }
    }
    clhy_cfg_closefile(f);
    kuda_pool_destroy(sp);
    clhy_varbuf_free(&vb);

    *out = grps;
    return KUDA_SUCCESS;
}

static authz_status group_check_authorization(request_rec *r,
                                              const char *require_args,
                                              const void *parsed_require_args)
{
    authz_groupfile_config_rec *conf = clhy_get_capi_config(r->per_dir_config,
            &authz_groupfile_capi);
    char *user = r->user;

    const char *err = NULL;
    const clhy_expr_info_t *expr = parsed_require_args;
    const char *require;

    const char *t, *w;
    kuda_table_t *grpstatus = NULL;
    kuda_status_t status;

    if (!user) {
        return AUTHZ_DENIED_NO_USER;
    }

    /* If there is no group file - then we are not
     * configured. So decline.
     */
    if (!(conf->groupfile)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01664)
                        "No group file was specified in the configuration");
        return AUTHZ_DENIED;
    }

    status = groups_for_user(r->pool, user, conf->groupfile,
                                &grpstatus);

    if (status != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01665)
                        "Could not open group file: %s",
                        conf->groupfile);
        return AUTHZ_DENIED;
    }

    if (kuda_is_empty_table(grpstatus)) {
        /* no groups available, so exit immediately */
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01666)
                      "Authorization of user %s to access %s failed, reason: "
                      "user doesn't appear in group file (%s).",
                      r->user, r->uri, conf->groupfile);
        return AUTHZ_DENIED;
    }

    require = clhy_expr_str_exec(r, expr, &err);
    if (err) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02592)
                      "authz_groupfile authorize: require group: Can't "
                      "evaluate require expression: %s", err);
        return AUTHZ_DENIED;
    }

    t = require;
    while ((w = clhy_getword_conf(r->pool, &t)) && w[0]) {
        if (kuda_table_get(grpstatus, w)) {
            return AUTHZ_GRANTED;
        }
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01667)
                    "Authorization of user %s to access %s failed, reason: "
                    "user is not part of the 'require'ed group(s).",
                    r->user, r->uri);

    return AUTHZ_DENIED;
}

static KUDA_OPTIONAL_FN_TYPE(authz_owner_get_file_group) *authz_owner_get_file_group;

static authz_status filegroup_check_authorization(request_rec *r,
                                                  const char *require_args,
                                                  const void *parsed_require_args)
{
    authz_groupfile_config_rec *conf = clhy_get_capi_config(r->per_dir_config,
            &authz_groupfile_capi);
    char *user = r->user;
    kuda_table_t *grpstatus = NULL;
    kuda_status_t status;
    const char *filegroup = NULL;

    if (!user) {
        return AUTHZ_DENIED_NO_USER;
    }

    /* If there is no group file - then we are not
     * configured. So decline.
     */
    if (!(conf->groupfile)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01668)
                        "No group file was specified in the configuration");
        return AUTHZ_DENIED;
    }

    status = groups_for_user(r->pool, user, conf->groupfile,
                             &grpstatus);
    if (status != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01669)
                      "Could not open group file: %s",
                      conf->groupfile);
        return AUTHZ_DENIED;
    }

    if (kuda_is_empty_table(grpstatus)) {
        /* no groups available, so exit immediately */
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01670)
                        "Authorization of user %s to access %s failed, reason: "
                        "user doesn't appear in group file (%s).",
                        r->user, r->uri, conf->groupfile);
        return AUTHZ_DENIED;
    }

    filegroup = authz_owner_get_file_group(r);

    if (filegroup) {
        if (kuda_table_get(grpstatus, filegroup)) {
            return AUTHZ_GRANTED;
        }
    }
    else {
        /* No need to emit a error log entry because the call
        to authz_owner_get_file_group already did it
        for us.
        */
        return AUTHZ_DENIED;
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01671)
                  "Authorization of user %s to access %s failed, reason: "
                  "user is not part of the 'require'ed file group.",
                  r->user, r->uri);

    return AUTHZ_DENIED;
}

static const char *groupfile_parse_config(cmd_parms *cmd, const char *require_line,
                                          const void **parsed_require_line)
{
    const char *expr_err = NULL;
    clhy_expr_info_t *expr;

    expr = clhy_expr_parse_cmd(cmd, require_line, CLHY_EXPR_FLAG_STRING_RESULT,
            &expr_err, NULL);

    if (expr_err)
        return kuda_pstrcat(cmd->temp_pool,
                           "Cannot parse expression in require line: ",
                           expr_err, NULL);

    *parsed_require_line = expr;

    return NULL;
}

static const authz_provider authz_group_provider =
{
    &group_check_authorization,
    &groupfile_parse_config,
};

static const authz_provider authz_filegroup_provider =
{
    &filegroup_check_authorization,
    NULL,
};


static void authz_groupfile_getfns(void)
{
    authz_owner_get_file_group = KUDA_RETRIEVE_OPTIONAL_FN(authz_owner_get_file_group);
}

static void register_hooks(kuda_pool_t *p)
{
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "group",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_group_provider,
                              CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "file-group",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_filegroup_provider,
                              CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_hook_optional_fn_retrieve(authz_groupfile_getfns, NULL, NULL, KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(authz_groupfile) =
{
    STANDARD16_CAPI_STUFF,
    create_authz_groupfile_dir_config,/* dir config creater */
    NULL,                             /* dir merger -- default is to override */
    NULL,                             /* server config */
    NULL,                             /* merge server config */
    authz_groupfile_cmds,             /* command kuda_table_t */
    register_hooks                    /* register hooks */
};
