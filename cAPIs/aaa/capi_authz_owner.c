/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strings.h"
#include "kuda_file_info.h"
#include "kuda_user.h"

#include "clhy_config.h"
#include "clhy_provider.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_request.h"

#include "capi_auth.h"
#include "capi_authz_owner.h"

static const command_rec authz_owner_cmds[] =
{
    {NULL}
};

cAPI CLHY_CAPI_DECLARE_DATA authz_owner_capi;

static authz_status fileowner_check_authorization(request_rec *r,
                                                  const char *require_args,
                                                  const void *parsed_require_args)
{
    char *reason = NULL;
    kuda_status_t status = 0;

#if !KUDA_HAS_USER
    reason = "'Require file-owner' is not supported on this platform.";
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01632)
                  "Authorization of user %s to access %s failed, reason: %s",
                  r->user, r->uri, reason ? reason : "unknown");
    return AUTHZ_DENIED;
#else  /* KUDA_HAS_USER */
    char *owner = NULL;
    kuda_finfo_t finfo;

    if (!r->user) {
        return AUTHZ_DENIED_NO_USER;
    }

    if (!r->filename) {
        reason = "no filename available";
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01633)
                      "Authorization of user %s to access %s failed, reason: %s",
                      r->user, r->uri, reason ? reason : "unknown");
        return AUTHZ_DENIED;
    }

    status = kuda_stat(&finfo, r->filename, KUDA_FINFO_USER, r->pool);
    if (status != KUDA_SUCCESS) {
        reason = kuda_pstrcat(r->pool, "could not stat file ",
                                r->filename, NULL);
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01634)
                      "Authorization of user %s to access %s failed, reason: %s",
                      r->user, r->uri, reason ? reason : "unknown");
        return AUTHZ_DENIED;
    }

    if (!(finfo.valid & KUDA_FINFO_USER)) {
        reason = "no file owner information available";
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01635)
                      "Authorization of user %s to access %s failed, reason: %s",
                      r->user, r->uri, reason ? reason : "unknown");
        return AUTHZ_DENIED;
    }

    status = kuda_uid_name_get(&owner, finfo.user, r->pool);
    if (status != KUDA_SUCCESS || !owner) {
        reason = "could not get name of file owner";
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01636)
                      "Authorization of user %s to access %s failed, reason: %s",
                      r->user, r->uri, reason ? reason : "unknown");
        return AUTHZ_DENIED;
    }

    if (strcmp(owner, r->user)) {
        reason = kuda_psprintf(r->pool, "file owner %s does not match.",
                                owner);
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01637)
                      "Authorization of user %s to access %s failed, reason: %s",
                      r->user, r->uri, reason ? reason : "unknown");
        return AUTHZ_DENIED;
    }

    /* this user is authorized */
    return AUTHZ_GRANTED;
#endif /* KUDA_HAS_USER */
}

static char *authz_owner_get_file_group(request_rec *r)
{
    /* file-group only figures out the file's group and lets
    * other cAPIs do the actual authorization (against a group file/db).
    * Thus, these cAPIs have to hook themselves after
    * capi_authz_owner and of course recognize 'file-group', too.
    */
#if !KUDA_HAS_USER
    return NULL;
#else  /* KUDA_HAS_USER */
    char *reason = NULL;
    char *group = NULL;
    kuda_finfo_t finfo;
    kuda_status_t status = 0;

    if (!r->filename) {
        reason = "no filename available";
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01638)
                      "Authorization of user %s to access %s failed, reason: %s",
                      r->user, r->uri, reason ? reason : "unknown");
        return NULL;
    }

    status = kuda_stat(&finfo, r->filename, KUDA_FINFO_GROUP, r->pool);
    if (status != KUDA_SUCCESS) {
        reason = kuda_pstrcat(r->pool, "could not stat file ",
                                r->filename, NULL);
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01639)
                      "Authorization of user %s to access %s failed, reason: %s",
                      r->user, r->uri, reason ? reason : "unknown");
        return NULL;
    }

    if (!(finfo.valid & KUDA_FINFO_GROUP)) {
        reason = "no file group information available";
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01640)
                      "Authorization of user %s to access %s failed, reason: %s",
                      r->user, r->uri, reason ? reason : "unknown");
        return NULL;
    }

    status = kuda_gid_name_get(&group, finfo.group, r->pool);
    if (status != KUDA_SUCCESS || !group) {
        reason = "could not get name of file group";
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, r, CLHYLOGNO(01641)
                      "Authorization of user %s to access %s failed, reason: %s",
                      r->user, r->uri, reason ? reason : "unknown");
        return NULL;
    }

    return group;
#endif /* KUDA_HAS_USER */
}

static const authz_provider authz_fileowner_provider =
{
    &fileowner_check_authorization,
    NULL,
};

static void register_hooks(kuda_pool_t *p)
{
    KUDA_REGISTER_OPTIONAL_FN(authz_owner_get_file_group);

    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "file-owner",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_fileowner_provider,
                              CLHY_AUTH_INTERNAL_PER_CONF);
}

CLHY_DECLARE_CAPI(authz_owner) =
{
    STANDARD16_CAPI_STUFF,
    NULL,                          /* dir config creater */
    NULL,                          /* dir merger --- default is to override */
    NULL,                          /* server config */
    NULL,                          /* merge server config */
    authz_owner_cmds,              /* command kuda_table_t */
    register_hooks                 /* register hooks */
};
