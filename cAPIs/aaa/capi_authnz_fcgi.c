/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_hash.h"
#include "kuda_lib.h"
#include "kuda_strings.h"

#include "clhy_provider.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_protocol.h"
#include "http_request.h"
#include "http_log.h"
#include "util_script.h"
#include "clhy_provider.h"
#include "capi_auth.h"
#include "util_fcgi.h"
#include "clhy_capimn.h"

cAPI CLHY_CAPI_DECLARE_DATA authnz_fcgi_capi;

typedef struct {
    const char *name; /* provider name */
    const char *backend; /* backend address, as configured */
    const char *host;
    kuda_port_t port;
    kuda_sockaddr_t *backend_addrs;
    int is_authn;
    int is_authz;
} fcgi_provider_conf;

typedef struct {
    const char *name; /* provider name */
    const char *default_user; /* this is user if authorizer returns
                               * success and a user expression yields
                               * empty string
                               */
    clhy_expr_info_t *user_expr; /* expr to evaluate to set r->user */
    char authoritative; /* fail request if user is rejected? */
    char require_basic_auth; /* fail if client didn't send credentials? */
} fcgi_dir_conf;

typedef struct {
    /* If an "authnz" provider successfully authenticates, record
     * the provider name here for checking during authz.
     */
    const char *successful_authnz_provider;
} fcgi_request_notes;

static kuda_hash_t *fcgi_authn_providers, *fcgi_authz_providers;

#define FCGI_IO_TIMEOUT kuda_time_from_sec(30)

#ifndef NON200_RESPONSE_BUF_LEN
#define NON200_RESPONSE_BUF_LEN 8192
#endif

/* fcgi://{hostname|IPv4|IPv6}:port[/] */
#define FCGI_BACKEND_REGEX_STR "m%^fcgi://(.*):(\\d{1,5})/?$%"

/*
 * utility function to connect to a peer
 */
static kuda_status_t connect_to_peer(kuda_socket_t **newsock,
                                    request_rec *r,
                                    kuda_sockaddr_t *backend_addrs,
                                    const char *backend_name,
                                    kuda_interval_time_t timeout)
{
    kuda_status_t rv = KUDA_EINVAL;
    int connected = 0;
    kuda_sockaddr_t *addr = backend_addrs;

    while (addr && !connected) {
        int loglevel = addr->next ? CLHYLOG_DEBUG : CLHYLOG_ERR;
        rv = kuda_socket_create(newsock, addr->family,
                               SOCK_STREAM, 0, r->pool);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, loglevel, rv, r,
                          CLHYLOGNO(02494) "error creating family %d socket "
                          "for target %s",
                          addr->family, backend_name);
            addr = addr->next;
            continue;
        }

        kuda_socket_opt_set(*newsock, KUDA_TCP_NODELAY, 1);
        kuda_socket_timeout_set(*newsock,
                               timeout ? timeout : r->server->timeout);

        rv = kuda_socket_connect(*newsock, addr);
        if (rv != KUDA_SUCCESS) {
            kuda_socket_close(*newsock);
            clhy_log_rerror(CLHYLOG_MARK, loglevel, rv, r,
                          CLHYLOGNO(02495) "attempt to connect to %pI (%s) "
                          "failed", addr, backend_name);
            addr = addr->next;
            continue;
        }

        connected = 1;
    }

    return rv;
#undef FN_LOG_MARK
}

static void log_provider_info(const fcgi_provider_conf *conf, request_rec *r)
{
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r,
                  CLHYLOGNO(02496) "name %s, backend %s, host %s, port %d, "
                  "first address %pI, %c%c",
                  conf->name,
                  conf->backend,
                  conf->host,
                  (int)conf->port,
                  conf->backend_addrs,
                  conf->is_authn ? 'N' : '_',
                  conf->is_authz ? 'Z' : '_');
}

static void setupenv(request_rec *r, const char *password, const char *clhy_role)
{
    clhy_add_common_vars(r);
    clhy_add_cgi_vars(r);
    kuda_table_setn(r->subprocess_env, "FCGI_ROLE", CLHY_FCGI_AUTHORIZER_STR);
    if (clhy_role) {
        kuda_table_setn(r->subprocess_env, "FCGI_CLHYKUDEL_ROLE", clhy_role);
    }
    if (password) {
        kuda_table_setn(r->subprocess_env, "REMOTE_PASSWD", password);
    }
    /* Drop the variables CONTENT_LENGTH, PATH_INFO, PATH_TRANSLATED,
     * SCRIPT_NAME and most Hop-By-Hop headers - EXCEPT we will pass
     * PROXY_AUTH to allow CGI to perform proxy auth for wwhy
     */
    kuda_table_unset(r->subprocess_env, "CONTENT_LENGTH");
    kuda_table_unset(r->subprocess_env, "PATH_INFO");
    kuda_table_unset(r->subprocess_env, "PATH_TRANSLATED");
    kuda_table_unset(r->subprocess_env, "SCRIPT_NAME");
    kuda_table_unset(r->subprocess_env, "HTTP_KEEP_ALIVE");
    kuda_table_unset(r->subprocess_env, "HTTP_TE");
    kuda_table_unset(r->subprocess_env, "HTTP_TRAILER");
    kuda_table_unset(r->subprocess_env, "HTTP_TRANSFER_ENCODING");
    kuda_table_unset(r->subprocess_env, "HTTP_UPGRADE");

    /* Connection hop-by-hop header to prevent the CGI from hanging */
    kuda_table_setn(r->subprocess_env, "HTTP_CONNECTION", "close");
}

static kuda_status_t recv_data(const fcgi_provider_conf *conf,
                              request_rec *r,
                              kuda_socket_t *s,
                              char *buf,
                              kuda_size_t *buflen)
{
    kuda_status_t rv;

    rv = kuda_socket_recv(s, buf, buflen);
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r,
                      CLHYLOGNO(02497) "Couldn't read from backend %s",
                      conf->backend);
        return rv;
    }

#if CLHY_CAPI_MAGIC_AT_LEAST(20130702,2) 
    clhy_log_rdata(CLHYLOG_MARK, CLHYLOG_TRACE5, r, "FastCGI data received",
                 buf, *buflen, CLHY_LOG_DATA_SHOW_OFFSET);
#endif
    return KUDA_SUCCESS;
}

static kuda_status_t recv_data_full(const fcgi_provider_conf *conf,
                                   request_rec *r,
                                   kuda_socket_t *s,
                                   char *buf,
                                   kuda_size_t buflen)
{
    kuda_size_t readlen;
    kuda_size_t cumulative_len = 0;
    kuda_status_t rv;

    do {
        readlen = buflen - cumulative_len;
        rv = recv_data(conf, r, s, buf + cumulative_len, &readlen);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
        cumulative_len += readlen;
    } while (cumulative_len < buflen);

    return KUDA_SUCCESS;
}

static kuda_status_t sendv_data(const fcgi_provider_conf *conf,
                               request_rec *r,
                               kuda_socket_t *s,
                               struct iovec *vec,
                               int nvec,
                               kuda_size_t *len)
{
    kuda_size_t to_write = 0, written = 0;
    kuda_status_t rv = KUDA_SUCCESS;
    int i, offset;

    for (i = 0; i < nvec; i++) {
        to_write += vec[i].iov_len;
#if CLHY_CAPI_MAGIC_AT_LEAST(20130702,2) 
        clhy_log_rdata(CLHYLOG_MARK, CLHYLOG_TRACE5, r, "FastCGI data sent",
                     vec[i].iov_base, vec[i].iov_len, CLHY_LOG_DATA_SHOW_OFFSET);
#endif
    }

    offset = 0;
    while (to_write) {
        kuda_size_t n = 0;
        rv = kuda_socket_sendv(s, vec + offset, nvec - offset, &n);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r,
                          CLHYLOGNO(02498) "Sending data to %s failed",
                          conf->backend);
            break;
        }
        if (n > 0) {
            written += n;
            if (written >= to_write)
                break;                 /* short circuit out */
            for (i = offset; i < nvec; ) {
                if (n >= vec[i].iov_len) {
                    offset++;
                    n -= vec[i++].iov_len;
                } else {
                    vec[i].iov_len -= n;
                    vec[i].iov_base = (char *) vec[i].iov_base + n;
                    break;
                }
            }
        }
    }

    *len = written;

    return rv;
}

static kuda_status_t send_begin_request(request_rec *r,
                                       const fcgi_provider_conf *conf,
                                       kuda_socket_t *s, int role,
                                       kuda_uint16_t request_id)
{
    struct iovec vec[2];
    clhy_fcgi_header header;
    unsigned char farray[CLHY_FCGI_HEADER_LEN];
    clhy_fcgi_begin_request_body brb;
    unsigned char abrb[CLHY_FCGI_HEADER_LEN];
    kuda_size_t len;

    clhy_fcgi_fill_in_header(&header, CLHY_FCGI_BEGIN_REQUEST, request_id,
                           sizeof(abrb), 0);
    clhy_fcgi_fill_in_request_body(&brb, role, 0 /* *NOT* CLHY_FCGI_KEEP_CONN */);

    clhy_fcgi_header_to_array(&header, farray);
    clhy_fcgi_begin_request_body_to_array(&brb, abrb);

    vec[0].iov_base = (void *)farray;
    vec[0].iov_len = sizeof(farray);
    vec[1].iov_base = (void *)abrb;
    vec[1].iov_len = sizeof(abrb);

    return sendv_data(conf, r, s, vec, 2, &len);
}

static kuda_status_t send_environment(kuda_socket_t *s,
                                     const fcgi_provider_conf *conf,
                                     request_rec *r, kuda_uint16_t request_id,
                                     kuda_pool_t *temp_pool)
{
    const char *fn = "send_environment";
    const kuda_array_header_t *envarr;
    const kuda_table_entry_t *elts;
    struct iovec vec[2];
    clhy_fcgi_header header;
    unsigned char farray[CLHY_FCGI_HEADER_LEN];
    char *body;
    kuda_status_t rv;
    kuda_size_t avail_len, len, required_len;
    int i, next_elem, starting_elem;

    envarr = kuda_table_elts(r->subprocess_env);
    elts = (const kuda_table_entry_t *) envarr->elts;

    if (CLHYLOG_R_IS_LEVEL(r, CLHYLOG_TRACE2)) {

        for (i = 0; i < envarr->nelts; ++i) {
            if (!elts[i].key) {
                continue;
            }
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                          "%s: '%s': '%s'",
                          fn, elts[i].key, 
                          !strcmp(elts[i].key, "REMOTE_PASSWD") ?
                              "XXXXXXXX" : elts[i].val);
        }
    }

    /* Send envvars over in as many FastCGI records as it takes, */
    next_elem = 0; /* starting with the first one */

    avail_len = 16 * 1024; /* our limit per record, which could have been up
                            * to CLHY_FCGI_MAX_CONTENT_LEN
                            */

    while (next_elem < envarr->nelts) {
        starting_elem = next_elem;
        required_len = clhy_fcgi_encoded_env_len(r->subprocess_env,
                                               avail_len,
                                               &next_elem);

        if (!required_len) {
            if (next_elem < envarr->nelts) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r,
                              CLHYLOGNO(02499) "couldn't encode envvar '%s' in %"
                              KUDA_SIZE_T_FMT " bytes",
                              elts[next_elem].key, avail_len);
                /* skip this envvar and continue */
                ++next_elem;
                continue;
            }
            /* only an unused element at the end of the array */
            break;
        }

        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r,
                      CLHYLOGNO(02500) "required len for encoding envvars: %"
                      KUDA_SIZE_T_FMT ", %d/%d elems processed so far",
                      required_len, next_elem, envarr->nelts);

        body = kuda_palloc(temp_pool, required_len);
        rv = clhy_fcgi_encode_env(r, r->subprocess_env, body, required_len,
                                &starting_elem);
        /* we pre-compute, so we can't run out of space */
        clhy_assert(rv == KUDA_SUCCESS);
        /* compute and encode must be in sync */
        clhy_assert(starting_elem == next_elem);

        clhy_fcgi_fill_in_header(&header, CLHY_FCGI_PARAMS, request_id,
                               (kuda_uint16_t)required_len, 0);
        clhy_fcgi_header_to_array(&header, farray);

        vec[0].iov_base = (void *)farray;
        vec[0].iov_len = sizeof(farray);
        vec[1].iov_base = body;
        vec[1].iov_len = required_len;

        rv = sendv_data(conf, r, s, vec, 2, &len);
        kuda_pool_clear(temp_pool);

        if (rv) {
            return rv;
        }
    }

    /* Envvars sent, so say we're done */
    clhy_fcgi_fill_in_header(&header, CLHY_FCGI_PARAMS, request_id, 0, 0);
    clhy_fcgi_header_to_array(&header, farray);

    vec[0].iov_base = (void *)farray;
    vec[0].iov_len = sizeof(farray);

    return sendv_data(conf, r, s, vec, 1, &len);
}

/*
 * This header-state logic is from capi_proxy_fcgi.
 */
enum {
  HDR_STATE_READING_HEADERS,
  HDR_STATE_GOT_CR,
  HDR_STATE_GOT_CRLF,
  HDR_STATE_GOT_CRLFCR,
  HDR_STATE_GOT_LF,
  HDR_STATE_DONE_WITH_HEADERS
};

/* Try to find the end of the script headers in the response from the back
 * end fastcgi server. STATE holds the current header parsing state for this
 * request.
 *
 * Returns 0 if it can't find the end of the headers, and 1 if it found the
 * end of the headers. */
static int handle_headers(request_rec *r, int *state,
                          const char *readbuf, kuda_size_t readlen)
{
    const char *itr = readbuf;

    while (readlen--) {
        if (*itr == '\r') {
            switch (*state) {
                case HDR_STATE_GOT_CRLF:
                    *state = HDR_STATE_GOT_CRLFCR;
                    break;

                default:
                    *state = HDR_STATE_GOT_CR;
                    break;
            }
        }
        else if (*itr == '\n') {
            switch (*state) {
                 case HDR_STATE_GOT_LF:
                     *state = HDR_STATE_DONE_WITH_HEADERS;
                     break;

                 case HDR_STATE_GOT_CR:
                     *state = HDR_STATE_GOT_CRLF;
                     break;

                 case HDR_STATE_GOT_CRLFCR:
                     *state = HDR_STATE_DONE_WITH_HEADERS;
                     break;

                 default:
                     *state = HDR_STATE_GOT_LF;
                     break;
            }
        }
        else {
            *state = HDR_STATE_READING_HEADERS;
        }

        if (*state == HDR_STATE_DONE_WITH_HEADERS)
            break;

        ++itr;
    }

    if (*state == HDR_STATE_DONE_WITH_HEADERS) {
        return 1;
    }

    return 0;
}

/*
 * handle_response() is based on capi_proxy_fcgi's dispatch()
 */
static kuda_status_t handle_response(const fcgi_provider_conf *conf,
                                    request_rec *r, kuda_socket_t *s,
                                    kuda_pool_t *temp_pool,
                                    kuda_uint16_t request_id,
                                    char *rspbuf,
                                    kuda_size_t *rspbuflen)
{
    kuda_bucket *b;
    kuda_bucket_brigade *ob;
    kuda_size_t orspbuflen = 0;
    kuda_status_t rv = KUDA_SUCCESS;
    const char *fn = "handle_response";
    int header_state = HDR_STATE_READING_HEADERS;
    int seen_end_of_headers = 0, done = 0;

    if (rspbuflen) {
        orspbuflen = *rspbuflen;
        *rspbuflen = 0; /* unless we actually read something */
    }

    ob = kuda_brigade_create(r->pool, r->connection->bucket_alloc);

    while (!done && rv == KUDA_SUCCESS) { /* Keep reading FastCGI records until
                                          * we get CLHY_FCGI_END_REQUEST (done)
                                          * or an error occurs.
                                          */
        kuda_size_t readbuflen;
        kuda_uint16_t clen;
        kuda_uint16_t rid;
        char readbuf[CLHY_IOBUFSIZE + 1];
        unsigned char farray[CLHY_FCGI_HEADER_LEN];
        unsigned char plen;
        unsigned char type;
        unsigned char version;

        rv = recv_data_full(conf, r, s, (char *)farray, CLHY_FCGI_HEADER_LEN);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r,
                          CLHYLOGNO(02501) "%s: Error occurred before reading "
                          "entire header", fn);
            break;
        }

        clhy_fcgi_header_fields_from_array(&version, &type, &rid, &clen, &plen,
                                         farray);

        if (version != CLHY_FCGI_VERSION_1) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r,
                          CLHYLOGNO(02502) "%s: Got bogus FastCGI header "
                          "version %d", fn, (int)version);
            rv = KUDA_EINVAL;
            break;
        }

        if (rid != request_id) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r,
                          CLHYLOGNO(02503) "%s: Got bogus FastCGI header "
                          "request id %d, expected %d",
                          fn, rid, request_id);
            rv = KUDA_EINVAL;
            break;
        }

    recv_again: /* if we need to keep reading more of a record's content */

        if (clen > sizeof(readbuf) - 1) {
            readbuflen = sizeof(readbuf) - 1;
        } else {
            readbuflen = clen;
        }

        /*
         * Now get the actual content of the record.
         */
        if (readbuflen != 0) {
            rv = recv_data(conf, r, s, readbuf, &readbuflen);
            if (rv != KUDA_SUCCESS) {
                break;
            }
            readbuf[readbuflen] = '\0';
        }

        switch (type) {
        case CLHY_FCGI_STDOUT: /* Response headers and optional body */
            if (clen != 0) {
                b = kuda_bucket_transient_create(readbuf,
                                                readbuflen,
                                                r->connection->bucket_alloc);

                KUDA_BRIGADE_INSERT_TAIL(ob, b);

                if (!seen_end_of_headers) {
                    int st = handle_headers(r, &header_state,
                                            readbuf, readbuflen);

                    if (st == 1) {
                        int status;

                        seen_end_of_headers = 1;

                        status =
                            clhy_scan_script_header_err_brigade_ex(r, ob,
                                                                 NULL, 
                                                                 CLHYLOG_CAPI_INDEX);
                        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r,
                                      CLHYLOGNO(02504) "%s: script header "
                                      "parsing -> %d/%d",
                                      fn, status, r->status);

                        if (rspbuf) { /* caller wants to see response body,
                                       * if any
                                       */
                            kuda_status_t tmprv;

                            if (rspbuflen) {
                                *rspbuflen = orspbuflen;
                            }
                            tmprv = kuda_brigade_flatten(ob, rspbuf, rspbuflen);
                            if (tmprv != KUDA_SUCCESS) {
                                /* should not occur for these bucket types;
                                 * does not indicate overflow
                                 */
                                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, tmprv, r,
                                              CLHYLOGNO(02505) "%s: error "
                                              "flattening response body",
                                              fn);
                            }
                        }

                        if (status != OK) {
                            r->status = status;
                            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r,
                                          CLHYLOGNO(02506) "%s: Error parsing "
                                          "script headers from %s",
                                          fn, conf->backend);
                            rv = KUDA_EINVAL;
                            break;
                        }
                        kuda_pool_clear(temp_pool);
                    }
                    else {
                        /* We're still looking for the end of the
                         * headers, so this part of the data will need
                         * to persist. */
                        kuda_bucket_setaside(b, temp_pool);
                    }
                }

                /* If we didn't read all the data go back and get the
                 * rest of it. */
                if (clen > readbuflen) {
                    clen -= readbuflen;
                    goto recv_again;
                }
            }
            break;

        case CLHY_FCGI_STDERR: /* Text to log */
            if (clen) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r,
                              CLHYLOGNO(02507) "%s: Logged from %s: '%s'",
                              fn, conf->backend, readbuf);
            }

            if (clen > readbuflen) {
                clen -= readbuflen;
                goto recv_again; /* continue reading this record */
            }
            break;

        case CLHY_FCGI_END_REQUEST:
            done = 1;
            break;

        default:
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r,
                          CLHYLOGNO(02508) "%s: Got bogus FastCGI record type "
                          "%d", fn, type);
            break;
        }
        /* Leave on above switch's inner error. */
        if (rv != KUDA_SUCCESS) {
            break;
        }

        /*
         * Read/discard any trailing padding.
         */
        if (plen) {
            rv = recv_data_full(conf, r, s, readbuf, plen);
            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r,
                              CLHYLOGNO(02509) "%s: Error occurred reading "
                              "padding",
                              fn);
                break;
            }
        }
    }

    kuda_brigade_cleanup(ob);

    if (rv == KUDA_SUCCESS && !seen_end_of_headers) {
        rv = KUDA_EINVAL;
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r,
                      CLHYLOGNO(02510) "%s: Never reached end of script headers",
                      fn);
    }

    return rv;
}

/* almost from capi_fcgid */
static int capi_fcgid_modify_auth_header(void *vars,
                                        const char *key, const char *val)
{
    /* When the application gives a 200 response, the server ignores response
       headers whose names aren't prefixed with Variable- prefix, and ignores
       any response content */
    if (strncasecmp(key, "Variable-", 9) == 0)
        kuda_table_setn(vars, key, val);
    return 1;
}

static int fix_auth_header(void *vr, const char *key, const char *val)
{
    request_rec *r = vr;

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r, "moving %s->%s", key, val);
    kuda_table_unset(r->err_headers_out, key);
    kuda_table_setn(r->subprocess_env, key + 9, val);
    return 1;
}

static void req_rsp(request_rec *r, const fcgi_provider_conf *conf,
                    const char *password, const char *clhy_role,
                    char *rspbuf, kuda_size_t *rspbuflen)
{
    const char *fn = "req_rsp";
    kuda_pool_t *temp_pool;
    kuda_size_t orspbuflen = 0;
    kuda_socket_t *s;
    kuda_status_t rv;
    kuda_table_t *saved_subprocess_env = 
      kuda_table_copy(r->pool, r->subprocess_env);

    if (rspbuflen) {
        orspbuflen = *rspbuflen;
        *rspbuflen = 0; /* unless we actually read something */
    }

    kuda_pool_create(&temp_pool, r->pool);

    setupenv(r, password, clhy_role);

    rv = connect_to_peer(&s, r, conf->backend_addrs,
                         conf->backend, FCGI_IO_TIMEOUT);
    if (rv == KUDA_SUCCESS) {
        kuda_uint16_t request_id = 1;

        rv = send_begin_request(r, conf, s, CLHY_FCGI_AUTHORIZER, request_id);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r,
                          CLHYLOGNO(02511) "%s: Failed writing request to %s",
                          fn, conf->backend);
        }

        if (rv == KUDA_SUCCESS) {
            rv = send_environment(s, conf, r, request_id, temp_pool);
            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r,
                              CLHYLOGNO(02512) "%s: Failed writing environment "
                              "to %s", fn, conf->backend);
            }
        }

        /* The responder owns the request body, not the authorizer.
         * Don't even send an empty CLHY_FCGI_STDIN block.  libfcgi doesn't care,
         * but it wasn't sent to authorizers by capi_fastcgi or capi_fcgi and
         * may be unhandled by the app.  Additionally, the FastCGI spec does
         * not mention FCGI_STDIN in the Authorizer description, though it
         * does describe FCGI_STDIN elsewhere in more general terms than
         * simply a wrapper for the client's request body.
         */

        if (rv == KUDA_SUCCESS) {
            if (rspbuflen) {
                *rspbuflen = orspbuflen;
            }
            rv = handle_response(conf, r, s, temp_pool, request_id, rspbuf,
                                 rspbuflen);
            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r,
                              CLHYLOGNO(02514) "%s: Failed handling response "
                              "from %s", fn, conf->backend);
            }
        }

        kuda_socket_close(s);
    }

    if (rv != KUDA_SUCCESS) {
        /* some sort of mechanical problem */
        r->status = HTTP_INTERNAL_SERVER_ERROR;
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r,
                      CLHYLOGNO(02515) "%s: Received HTTP status %d",
                      fn, r->status);
    }

    r->subprocess_env = saved_subprocess_env;

    if (r->status == HTTP_OK) {
        /* An Authorizer application's 200 response may include headers
         * whose names are prefixed with Variable-, and they should be
         * available to subsequent phases via subprocess_env (and yanked
         * from the client response).
         */
        kuda_table_t *vars = kuda_table_make(temp_pool, /* not used to allocate
                                                       * any values that end up
                                                       * in r->(anything)
                                                       */
                                           10);
        kuda_table_do(capi_fcgid_modify_auth_header, vars,
                     r->err_headers_out, NULL);
        kuda_table_do(fix_auth_header, r, vars, NULL);
    }

    kuda_pool_destroy(temp_pool);
}

static int fcgi_check_authn(request_rec *r)
{
    const char *fn = "fcgi_check_authn";
    fcgi_dir_conf *dconf = clhy_get_capi_config(r->per_dir_config,
                                                &authnz_fcgi_capi);
    const char *password = NULL;
    const fcgi_provider_conf *conf;
    const char *prov;
    const char *auth_type;
    char rspbuf[NON200_RESPONSE_BUF_LEN + 1]; /* extra byte for '\0' */
    kuda_size_t rspbuflen = sizeof rspbuf - 1;
    int res;

    prov = dconf && dconf->name ? dconf->name : NULL;

    if (!prov || !strcasecmp(prov, "None")) {
        return DECLINED;
    }

    auth_type = clhy_auth_type(r);

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r,
                  CLHYLOGNO(02516) "%s, prov %s, authoritative %s, "
                  "require-basic %s, user expr? %s type %s",
                  fn, prov,
                  dconf->authoritative ? "yes" : "no",
                  dconf->require_basic_auth ? "yes" : "no",
                  dconf->user_expr ? "yes" : "no",
                  auth_type);

    if (auth_type && !strcasecmp(auth_type, "Basic")) {
        if ((res = clhy_get_basic_auth_pw(r, &password))) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r,
                          CLHYLOGNO(02517) "%s: couldn't retrieve basic auth "
                          "password", fn);
            if (dconf->require_basic_auth) {
                return res;
            }
            password = NULL;
        }
    }

    conf = kuda_hash_get(fcgi_authn_providers, prov, KUDA_HASH_KEY_STRING);
    if (!conf) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_CRIT, 0, r,
                      CLHYLOGNO(02518) "%s: can't find config for provider %s",
                      fn, prov);
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    if (CLHYLOGrdebug(r)) {
        log_provider_info(conf, r);
    }

    req_rsp(r, conf, password, CLHY_FCGI_CLHYKUDEL_ROLE_AUTHENTICATOR_STR,
            rspbuf, &rspbuflen);

    if (r->status == HTTP_OK) {
        if (dconf->user_expr) {
            const char *err;
            const char *user = clhy_expr_str_exec(r, dconf->user_expr,
                                                &err);
            if (user && strlen(user)) {
                r->user = kuda_pstrdup(r->pool, user);
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r,
                              CLHYLOGNO(02519) "%s: Setting user to '%s'",
                              fn, r->user);
            }
            else if (user && dconf->default_user) {
                r->user = kuda_pstrdup(r->pool, dconf->default_user);
            }
            else if (user) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r,
                              CLHYLOGNO(02520) "%s: Failure extracting user "
                              "after calling authorizer: user expression "
                              "yielded empty string (variable not set?)",
                              fn);
                r->status = HTTP_INTERNAL_SERVER_ERROR;
            }
            else {
                /* unexpected error, not even an empty string was returned */
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r,
                              CLHYLOGNO(02521) "%s: Failure extracting user "
                              "after calling authorizer: %s",
                              fn, err);
                r->status = HTTP_INTERNAL_SERVER_ERROR;
            }
        }
        if (conf->is_authz) {
            /* combined authn/authz phase, so app won't be invoked for authz
             *
             * Remember that the request was successfully authorized by this
             * provider.
             */
            fcgi_request_notes *rnotes = kuda_palloc(r->pool, sizeof(*rnotes));
            rnotes->successful_authnz_provider = conf->name;
            clhy_set_capi_config(r->request_config, &authnz_fcgi_capi,
                                 rnotes);
        }
    }
    else {
        /* From the spec:
         *   For Authorizer response status values other than "200" (OK), the 
         *   Web server denies access and sends the response status, headers,
         *   and content back to the HTTP client.
         * But:
         *   This only makes sense if this authorizer is authoritative.
         */
        if (rspbuflen > 0 && !dconf->authoritative) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r,
                          CLHYLOGNO(02522) "%s: Ignoring response body from non-"
                          "authoritative authorizer", fn);
        }
        else if (rspbuflen > 0) {
            if (rspbuflen == sizeof rspbuf - 1) {
                /* kuda_brigade_flatten() interface :( */
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r,
                              CLHYLOGNO(02523) "%s: possible overflow handling "
                              "response body", fn);
            }
            rspbuf[rspbuflen] = '\0'; /* we reserved an extra byte for '\0' */
            clhy_custom_response(r, r->status, rspbuf); /* API makes a copy */
        }
    }

    return r->status == HTTP_OK ? 
        OK : dconf->authoritative ? r->status : DECLINED;
}

static authn_status fcgi_check_password(request_rec *r, const char *user,
                                        const char *password)
{
    const char *fn = "fcgi_check_password";
    const char *prov = kuda_table_get(r->notes, AUTHN_PROVIDER_NAME_NOTE);
    const fcgi_provider_conf *conf;

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r,
                  CLHYLOGNO(02524) "%s(%s, XXX): provider %s",
                  fn, user, prov);

    if (!prov) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_CRIT, 0, r,
                      CLHYLOGNO(02525) "%s: provider note isn't set", fn);
        return AUTH_GENERAL_ERROR;
    }

    conf = kuda_hash_get(fcgi_authn_providers, prov, KUDA_HASH_KEY_STRING);
    if (!conf) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_CRIT, 0, r,
                      CLHYLOGNO(02526) "%s: can't find config for provider %s",
                      fn, prov);
        return AUTH_GENERAL_ERROR;
    }

    if (CLHYLOGrdebug(r)) {
        log_provider_info(conf, r);
    }

    req_rsp(r, conf, password, 
            /* combined authn and authz: FCGI_CLHYKUDEL_ROLE not set */
            conf->is_authz ? NULL : CLHY_FCGI_CLHYKUDEL_ROLE_AUTHENTICATOR_STR,
            NULL, NULL);

    if (r->status == HTTP_OK) {
        if (conf->is_authz) {
            /* combined authn/authz phase, so app won't be invoked for authz
             *
             * Remember that the request was successfully authorized by this
             * provider.
             */
            fcgi_request_notes *rnotes = kuda_palloc(r->pool, sizeof(*rnotes));
            rnotes->successful_authnz_provider = conf->name;
            clhy_set_capi_config(r->request_config, &authnz_fcgi_capi,
                                 rnotes);
        }
        return AUTH_GRANTED;
    }
    else if (r->status == HTTP_INTERNAL_SERVER_ERROR) {
        return AUTH_GENERAL_ERROR;
    }
    else {
        return AUTH_DENIED;
    }
}

static const authn_provider fcgi_authn_provider = {
    &fcgi_check_password,
    NULL /* get-realm-hash not supported */
};

static authz_status fcgi_authz_check(request_rec *r,
                                     const char *require_line,
                                     const void *parsed_require_line)
{
    const char *fn = "fcgi_authz_check";
    const char *prov = kuda_table_get(r->notes, AUTHZ_PROVIDER_NAME_NOTE);
    const fcgi_provider_conf *conf;

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r,
                  CLHYLOGNO(02527) "%s(%s)", fn, require_line);

    if (!prov) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_CRIT, 0, r,
                      CLHYLOGNO(02528) "%s: provider note isn't set", fn);
        return AUTHZ_GENERAL_ERROR;
    }

    conf = kuda_hash_get(fcgi_authz_providers, prov, KUDA_HASH_KEY_STRING);
    if (!conf) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_CRIT, 0, r,
                      CLHYLOGNO(02529) "%s: can't find config for provider %s",
                      fn, prov);
        return AUTHZ_GENERAL_ERROR;
    }

    if (CLHYLOGrdebug(r)) {
        log_provider_info(conf, r);
    }

    if (!r->user) {
        return AUTHZ_DENIED_NO_USER;
    }

    if (conf->is_authn) {
        /* combined authn/authz phase, so app won't be invoked for authz
         *
         * If the provider already successfully authorized this request, 
         * success.
         */
        fcgi_request_notes *rnotes = clhy_get_capi_config(r->request_config,
                                                        &authnz_fcgi_capi);
        if (rnotes
            && rnotes->successful_authnz_provider
            && !strcmp(rnotes->successful_authnz_provider, conf->name)) {
            return AUTHZ_GRANTED;
        }
        else {
            return AUTHZ_DENIED;
        }
    }
    else {
        req_rsp(r, conf, NULL, CLHY_FCGI_CLHYKUDEL_ROLE_AUTHORIZER_STR, NULL, NULL);

        if (r->status == HTTP_OK) {
            return AUTHZ_GRANTED;
        }
        else if (r->status == HTTP_INTERNAL_SERVER_ERROR) {
            return AUTHZ_GENERAL_ERROR;
        }
        else {
            return AUTHZ_DENIED;
        }
    }
}

static const char *fcgi_authz_parse(cmd_parms *cmd, const char *require_line,
                                    const void **parsed_require_line)
{
    /* Allowed form: Require [not] registered-provider-name<EOS>
     */
    if (strcmp(require_line, "")) {
        return "capi_authnz_fcgi doesn't support restrictions on providers "
               "(i.e., multiple require args)";
    }

    return NULL;
}

static const authz_provider fcgi_authz_provider = {
    &fcgi_authz_check,
    &fcgi_authz_parse,
};

static const char *fcgi_check_authn_provider(cmd_parms *cmd,
                                        void *d,
                                        int argc,
                                        char *const argv[])
{
    const char *dname = "AuthnzFcgiCheckAuthnProvider";
    fcgi_dir_conf *dc = d;
    int ca = 0;

    if (ca >= argc) {
        return kuda_pstrcat(cmd->pool, dname, ": No provider given", NULL);
    }

    dc->name = argv[ca];
    ca++;

    if (!strcasecmp(dc->name, "None")) {
        if (ca < argc) {
            return "Options aren't supported with \"None\"";
        }
    }

    while (ca < argc) {
        const char *var = argv[ca], *val;
        int badarg = 0;

        ca++;

        /* at present, everything needs an argument */
        if (ca >= argc) {
            return kuda_pstrcat(cmd->pool, dname, ": ", var,
                               "needs an argument", NULL);
        }

        val = argv[ca];
        ca++;

        if (!strcasecmp(var, "Authoritative")) {
            if (!strcasecmp(val, "On")) {
                dc->authoritative = 1;
            }
            else if (!strcasecmp(val, "Off")) {
                dc->authoritative = 0;
            }
            else {
                badarg = 1;
            }
        }
        else if (!strcasecmp(var, "DefaultUser")) {
            dc->default_user = val;
        }
        else if (!strcasecmp(var, "RequireBasicAuth")) {
            if (!strcasecmp(val, "On")) {
                dc->require_basic_auth = 1;
            }
            else if (!strcasecmp(val, "Off")) {
                dc->require_basic_auth = 0;
            }
            else {
                badarg = 1;
            }
        }
        else if (!strcasecmp(var, "UserExpr")) {
            const char *err;
            int flags = CLHY_EXPR_FLAG_DONT_VARY | CLHY_EXPR_FLAG_RESTRICTED
                | CLHY_EXPR_FLAG_STRING_RESULT;

            dc->user_expr = clhy_expr_parse_cmd(cmd, val,
                                              flags, &err, NULL);
            if (err) {
                return kuda_psprintf(cmd->pool, "%s: Error parsing '%s': '%s'",
                                    dname, val, err);
            }
        }
        else {
            return kuda_pstrcat(cmd->pool, dname, ": Unexpected option '",
                               var, "'", NULL);
        }
        if (badarg) {
            return kuda_pstrcat(cmd->pool, dname, ": Bad argument '",
                               val, "' to option '", var, "'", NULL);
        }
    }

    return NULL;
}

/* AuthnzFcgiAuthDefineProvider {authn|authz|authnz} provider-name \
 *   fcgi://backendhost:backendport/
 */
static const char *fcgi_define_provider(cmd_parms *cmd,
                                        void *d,
                                        int argc,
                                        char *const argv[])
{
    const char *dname = "AuthnzFcgiDefineProvider";
    clhy_rxplus_t *fcgi_backend_regex;
    kuda_status_t rv;
    char *host;
    const char *err, *stype;
    fcgi_provider_conf *conf = kuda_pcalloc(cmd->pool, sizeof(*conf));
    int ca = 0, rc, port;

    fcgi_backend_regex = clhy_rxplus_compile(cmd->pool, FCGI_BACKEND_REGEX_STR);
    if (!fcgi_backend_regex) {
        return kuda_psprintf(cmd->pool,
                            "%s: failed to compile regexec '%s'",
                            dname, FCGI_BACKEND_REGEX_STR);
    }

    err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (err)
        return err;

    if (ca >= argc) {
        return kuda_pstrcat(cmd->pool, dname, ": No type given", NULL);
    }

    stype = argv[ca];
    ca++;

    if (!strcasecmp(stype, "authn")) {
        conf->is_authn = 1;
    }
    else if (!strcasecmp(stype, "authz")) {
        conf->is_authz = 1;
    }
    else if (!strcasecmp(stype, "authnz")) {
        conf->is_authn = conf->is_authz = 1;
    }
    else {
        return kuda_pstrcat(cmd->pool,
                           dname,
                           ": Invalid provider type ",
                           stype,
                           NULL);
    }

    if (ca >= argc) {
        return kuda_pstrcat(cmd->pool, dname, ": No provider name given", NULL);
    }
    conf->name = argv[ca];
    ca++;

    if (ca >= argc) {
        return kuda_pstrcat(cmd->pool, dname, ": No backend-address given",
                           NULL);
    }

    rc = clhy_rxplus_exec(cmd->pool, fcgi_backend_regex, argv[ca], NULL);
    if (!rc || clhy_rxplus_nmatch(fcgi_backend_regex) != 3) {
        return kuda_pstrcat(cmd->pool,
                           dname, ": backend-address '",
                           argv[ca],
                           "' has invalid form",
                           NULL);
    }

    host = clhy_rxplus_pmatch(cmd->pool, fcgi_backend_regex, 1);
    if (host[0] == '[' && host[strlen(host) - 1] == ']') {
        host += 1;
        host[strlen(host) - 1] = '\0';
    }

    port = atoi(clhy_rxplus_pmatch(cmd->pool, fcgi_backend_regex, 2));
    if (port > 65535) {
        return kuda_pstrcat(cmd->pool,
                           dname, ": backend-address '",
                           argv[ca],
                           "' has invalid port",
                           NULL);
    }

    conf->backend = argv[ca];
    conf->host = host;
    conf->port = port;
    ca++;

    rv = kuda_sockaddr_info_get(&conf->backend_addrs, conf->host,
                               KUDA_UNSPEC, conf->port, 0, cmd->pool);
    if (rv != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP|CLHYLOG_CRIT, rv, NULL,
                     CLHYLOGNO(02530) "Address %s could not be resolved",
                     conf->backend);
        return kuda_pstrcat(cmd->pool,
                           dname,
                           ": Error resolving backend address",
                           NULL);
    }

    if (ca != argc) {
        return kuda_pstrcat(cmd->pool,
                           dname,
                           ": Unexpected parameter ",
                           argv[ca],
                           NULL);
    }

    if (conf->is_authn) {
        kuda_hash_set(fcgi_authn_providers, conf->name, KUDA_HASH_KEY_STRING,
                     conf);
        clhy_register_auth_provider(cmd->pool, AUTHN_PROVIDER_GROUP,
                                  conf->name,
                                  AUTHN_PROVIDER_VERSION,
                                  &fcgi_authn_provider,
                                  CLHY_AUTH_INTERNAL_PER_CONF);
    }

    if (conf->is_authz) {
        kuda_hash_set(fcgi_authz_providers, conf->name, KUDA_HASH_KEY_STRING,
                     conf);
        clhy_register_auth_provider(cmd->pool, AUTHZ_PROVIDER_GROUP,
                                  conf->name,
                                  AUTHZ_PROVIDER_VERSION,
                                  &fcgi_authz_provider,
                                  CLHY_AUTH_INTERNAL_PER_CONF);
    }

    return NULL;
}

static const command_rec fcgi_cmds[] = {
    CLHY_INIT_TAKE_ARGV("AuthnzFcgiDefineProvider", 
                      fcgi_define_provider,
                      NULL,
                      RSRC_CONF,
                      "Define a FastCGI authn and/or authz provider"),

    CLHY_INIT_TAKE_ARGV("AuthnzFcgiCheckAuthnProvider",
                      fcgi_check_authn_provider,
                      NULL,
                      OR_FILEINFO,
                      "Enable/disable a FastCGI authorizer to handle "
                      "check_authn phase"),

    {NULL}
};

static int fcgi_pre_config(kuda_pool_t *pconf, kuda_pool_t *plog,
                           kuda_pool_t *ptemp)
{
    fcgi_authn_providers = kuda_hash_make(pconf);
    fcgi_authz_providers = kuda_hash_make(pconf);

    return OK;
}

static void fcgi_register_hooks(kuda_pool_t *p)
{
    static const char * const auth_basic_runs_after_me[] = 
        {"capi_auth_basic.c", NULL}; /* to allow for custom response */

    clhy_hook_pre_config(fcgi_pre_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_check_authn(fcgi_check_authn, NULL, auth_basic_runs_after_me,
                        KUDA_HOOK_MIDDLE, CLHY_AUTH_INTERNAL_PER_CONF);
}

static void *create_dir_conf(kuda_pool_t *p, char *dummy)
{
    fcgi_dir_conf *dconf = kuda_pcalloc(p, sizeof(fcgi_dir_conf));

    dconf->authoritative = 1;
    return dconf;
}

static void *merge_dir_conf(kuda_pool_t *p, void *basev, void *overridesv)
{
    fcgi_dir_conf *a = (fcgi_dir_conf *)kuda_pcalloc(p, sizeof(*a));
    fcgi_dir_conf *base = (fcgi_dir_conf *)basev, 
        *over = (fcgi_dir_conf *)overridesv;

    /* currently we just have a single directive applicable to a 
     * directory, so if it is set then grab all fields from fcgi_dir_conf
     */
    if (over->name) {
        memcpy(a, over, sizeof(*a));
    }
    else {
        memcpy(a, base, sizeof(*a));
    }
    
    return a;
}

CLHY_DECLARE_CAPI(authnz_fcgi) =
{
    STANDARD16_CAPI_STUFF,
    create_dir_conf,                 /* dir config creater */
    merge_dir_conf,                  /* dir merger */
    NULL,                            /* server config */
    NULL,                            /* merge server config */
    fcgi_cmds,                       /* command kuda_table_t */
    fcgi_register_hooks              /* register hooks */
};
