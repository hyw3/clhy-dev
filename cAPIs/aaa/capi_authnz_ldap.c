/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "clhy_provider.h"
#include "wwhy.h"
#include "http_config.h"
#include "clhy_provider.h"
#include "http_core.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_request.h"
#include "util_ldap.h"

#include "capi_auth.h"

#include "kuda_strings.h"
#include "kuda_xlate.h"
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"
#include "kuda_lib.h"

#include <ctype.h>

#if !KUDA_HAS_LDAP
#error capi_authnz_ldap requires Kuda-Delman to have LDAP support built in. To fix add --with-ldap to ./configure.
#endif

static char *default_attributes[3] = { "member", "uniqueMember", NULL };

typedef struct {
    kuda_pool_t *pool;               /* Pool that this config is allocated from */
#if KUDA_HAS_THREADS
    kuda_thread_mutex_t *lock;       /* Lock for this config */
#endif

    /* These parameters are all derived from the AuthLDAPURL directive */
    char *url;                      /* String representation of the URL */

    char *host;                     /* Name of the LDAP server (or space separated list) */
    int port;                       /* Port of the LDAP server */
    char *basedn;                   /* Base DN to do all searches from */
    char *attribute;                /* Attribute to search for */
    char **attributes;              /* Array of all the attributes to return */
    int scope;                      /* Scope of the search */
    char *filter;                   /* Filter to further limit the search  */
    deref_options deref;            /* how to handle alias dereferening */
    char *binddn;                   /* DN to bind to server (can be NULL) */
    char *bindpw;                   /* Password to bind to server (can be NULL) */
    int bind_authoritative;         /* If true, will return errors when bind fails */

    int user_is_dn;                 /* If true, r->user is replaced by DN during authn */
    char *remote_user_attribute;    /* If set, r->user is replaced by this attribute during authn */
    int compare_dn_on_server;       /* If true, will use server to do DN compare */

    int have_ldap_url;              /* Set if we have found an LDAP url */

    kuda_array_header_t *groupattr;  /* List of Group attributes identifying user members. Default:"member uniqueMember" */
    int group_attrib_is_dn;         /* If true, the group attribute is the DN, otherwise,
                                        it's the exact string passed by the HTTP client */
    char **sgAttributes;            /* Array of strings constructed (post-config) from subgroupattrs. Last entry is NULL. */
    kuda_array_header_t *subgroupclasses; /* List of object classes of sub-groups. Default:"groupOfNames groupOfUniqueNames" */
    int maxNestingDepth;            /* Maximum recursive nesting depth permitted during subgroup processing. Default: 10 */

    int secure;                     /* True if SSL connections are requested */
    char *authz_prefix;             /* Prefix for environment variables added during authz */
    int initial_bind_as_user;               /* true if we should try to bind (to lookup DN) directly with the basic auth username */
    clhy_regex_t *bind_regex;         /* basic auth -> bind'able username regex */
    const char *bind_subst;         /* basic auth -> bind'able username substitution */
    int search_as_user;             /* true if authz searches should be done with the users credentials (when we did authn) */
    int compare_as_user;            /* true if authz compares should be done with the users credentials (when we did authn) */
} authn_ldap_config_t;

typedef struct {
    char *dn;                       /* The saved dn from a successful search */
    char *user;                     /* The username provided by the client */
    const char **vals;              /* The additional values pulled during the DN search*/
    char *password;                 /* if this cAPI successfully authenticates, the basic auth password, else null */
} authn_ldap_request_t;

enum auth_ldap_phase {
    LDAP_AUTHN, LDAP_AUTHZ
};

enum auth_ldap_optype {
    LDAP_SEARCH, LDAP_COMPARE, LDAP_COMPARE_AND_SEARCH /* nested groups */
};

/* maximum group elements supported */
#define GROUPATTR_MAX_ELTS 10

cAPI CLHY_CAPI_DECLARE_DATA authnz_ldap_capi;

static KUDA_OPTIONAL_FN_TYPE(uldap_connection_close) *util_ldap_connection_close;
static KUDA_OPTIONAL_FN_TYPE(uldap_connection_find) *util_ldap_connection_find;
static KUDA_OPTIONAL_FN_TYPE(uldap_cache_comparedn) *util_ldap_cache_comparedn;
static KUDA_OPTIONAL_FN_TYPE(uldap_cache_compare) *util_ldap_cache_compare;
static KUDA_OPTIONAL_FN_TYPE(uldap_cache_check_subgroups) *util_ldap_cache_check_subgroups;
static KUDA_OPTIONAL_FN_TYPE(uldap_cache_checkuserid) *util_ldap_cache_checkuserid;
static KUDA_OPTIONAL_FN_TYPE(uldap_cache_getuserdn) *util_ldap_cache_getuserdn;
static KUDA_OPTIONAL_FN_TYPE(uldap_ssl_supported) *util_ldap_ssl_supported;

static kuda_hash_t *charset_conversions = NULL;
static char *to_charset = NULL;           /* UTF-8 identifier derived from the charset.conv file */


/* Derive a code page ID give a language name or ID */
static char* derive_codepage_from_lang (kuda_pool_t *p, char *language)
{
    char *charset;

    if (!language)          /* our default codepage */
        return kuda_pstrdup(p, "ISO-8859-1");

    charset = (char*) kuda_hash_get(charset_conversions, language, KUDA_HASH_KEY_STRING);

    if (!charset) {
        language[2] = '\0';
        charset = (char*) kuda_hash_get(charset_conversions, language, KUDA_HASH_KEY_STRING);
    }

    if (charset) {
        charset = kuda_pstrdup(p, charset);
    }

    return charset;
}

static kuda_xlate_t* get_conv_set (request_rec *r)
{
    char *lang_line = (char*)kuda_table_get(r->headers_in, "accept-language");
    char *lang;
    kuda_xlate_t *convset;

    if (lang_line) {
        lang_line = kuda_pstrdup(r->pool, lang_line);
        for (lang = lang_line;*lang;lang++) {
            if ((*lang == ',') || (*lang == ';')) {
                *lang = '\0';
                break;
            }
        }
        lang = derive_codepage_from_lang(r->pool, lang_line);

        if (lang && (kuda_xlate_open(&convset, to_charset, lang, r->pool) == KUDA_SUCCESS)) {
            return convset;
        }
    }

    return NULL;
}


static const char* authn_ldap_xlate_password(request_rec *r,
                                             const char* sent_password)
{
    kuda_xlate_t *convset = NULL;
    kuda_size_t inbytes;
    kuda_size_t outbytes;
    char *outbuf;

    if (charset_conversions && (convset = get_conv_set(r)) ) {
        inbytes = strlen(sent_password);
        outbytes = (inbytes+1)*3;
        outbuf = kuda_pcalloc(r->pool, outbytes);

        /* Convert the password to UTF-8. */
        if (kuda_xlate_conv_buffer(convset, sent_password, &inbytes, outbuf,
                                  &outbytes) == KUDA_SUCCESS)
            return outbuf;
    }

    return sent_password;
}


/*
 * Build the search filter, or at least as much of the search filter that
 * will fit in the buffer. We don't worry about the buffer not being able
 * to hold the entire filter. If the buffer wasn't big enough to hold the
 * filter, ldap_search_s will complain, but the only situation where this
 * is likely to happen is if the client sent a really, really long
 * username, most likely as part of an attack.
 *
 * The search filter consists of the filter provided with the URL,
 * combined with a filter made up of the attribute provided with the URL,
 * and the actual username passed by the HTTP client. For example, assume
 * that the LDAP URL is
 *
 *   ldap://ldap.airius.com/ou=People, o=Airius?uid??(posixid=*)
 *
 * Further, assume that the userid passed by the client was `userj'.  The
 * search filter will be (&(posixid=*)(uid=userj)).
 */
#define FILTER_LENGTH MAX_STRING_LEN
static void authn_ldap_build_filter(char *filtbuf,
                             request_rec *r,
                             const char* sent_user,
                             const char* sent_filter,
                             authn_ldap_config_t *sec)
{
    char *p, *q, *filtbuf_end;
    char *user, *filter;
    kuda_xlate_t *convset = NULL;
    kuda_size_t inbytes;
    kuda_size_t outbytes;
    char *outbuf;
    int nofilter = 0;

    if (sent_user != NULL) {
        user = kuda_pstrdup (r->pool, sent_user);
    }
    else
        return;

    if (sent_filter != NULL) {
        filter = kuda_pstrdup (r->pool, sent_filter);
    }
    else
        filter = sec->filter;

    if (charset_conversions) {
        convset = get_conv_set(r);
    }

    if (convset) {
        inbytes = strlen(user);
        outbytes = (inbytes+1)*3;
        outbuf = kuda_pcalloc(r->pool, outbytes);

        /* Convert the user name to UTF-8.  This is only valid for LDAP v3 */
        if (kuda_xlate_conv_buffer(convset, user, &inbytes, outbuf, &outbytes) == KUDA_SUCCESS) {
            user = kuda_pstrdup(r->pool, outbuf);
        }
    }

    /*
     * Create the first part of the filter, which consists of the
     * config-supplied portions.
     */

    if ((nofilter = (filter && !strcasecmp(filter, "none")))) { 
        kuda_snprintf(filtbuf, FILTER_LENGTH, "(%s=", sec->attribute);
    }
    else { 
        kuda_snprintf(filtbuf, FILTER_LENGTH, "(&(%s)(%s=", filter, sec->attribute);
    }

    /*
     * Now add the client-supplied username to the filter, ensuring that any
     * LDAP filter metachars are escaped.
     */
    filtbuf_end = filtbuf + FILTER_LENGTH - 1;
#if KUDA_HAS_MICROSOFT_LDAPSDK
    for (p = user, q=filtbuf + strlen(filtbuf);
         *p && q < filtbuf_end; ) {
        if (strchr("*()\\", *p) != NULL) {
            if ( q + 3 >= filtbuf_end)
              break;  /* Don't write part of escape sequence if we can't write all of it */
            *q++ = '\\';
            switch ( *p++ )
            {
              case '*':
                *q++ = '2';
                *q++ = 'a';
                break;
              case '(':
                *q++ = '2';
                *q++ = '8';
                break;
              case ')':
                *q++ = '2';
                *q++ = '9';
                break;
              case '\\':
                *q++ = '5';
                *q++ = 'c';
                break;
                        }
        }
        else
            *q++ = *p++;
    }
#else
    for (p = user, q=filtbuf + strlen(filtbuf);
         *p && q < filtbuf_end; *q++ = *p++) {
        if (strchr("*()\\", *p) != NULL) {
            *q++ = '\\';
            if (q >= filtbuf_end) {
              break;
            }
        }
    }
#endif
    *q = '\0';

    /*
     * Append the closing parens of the filter, unless doing so would
     * overrun the buffer.
     */

    if (nofilter) { 
        if (q + 1 <= filtbuf_end)
            strcat(filtbuf, ")");
    } 
    else { 
        if (q + 2 <= filtbuf_end)
            strcat(filtbuf, "))");
    }

}

static void *create_authnz_ldap_dir_config(kuda_pool_t *p, char *d)
{
    authn_ldap_config_t *sec =
        (authn_ldap_config_t *)kuda_pcalloc(p, sizeof(authn_ldap_config_t));

    sec->pool = p;
#if KUDA_HAS_THREADS
    kuda_thread_mutex_create(&sec->lock, KUDA_THREAD_MUTEX_DEFAULT, p);
#endif
/*
    sec->authz_enabled = 1;
*/
    sec->groupattr = kuda_array_make(p, GROUPATTR_MAX_ELTS,
                                    sizeof(struct capi_auth_ldap_groupattr_entry_t));
    sec->subgroupclasses = kuda_array_make(p, GROUPATTR_MAX_ELTS,
                                    sizeof(struct capi_auth_ldap_groupattr_entry_t));

    sec->have_ldap_url = 0;
    sec->url = "";
    sec->host = NULL;
    sec->binddn = NULL;
    sec->bindpw = NULL;
    sec->bind_authoritative = 1;
    sec->deref = always;
    sec->group_attrib_is_dn = 1;
    sec->secure = -1;   /*Initialize to unset*/
    sec->maxNestingDepth = 10;
    sec->sgAttributes = kuda_pcalloc(p, sizeof (char *) * GROUPATTR_MAX_ELTS + 1);

    sec->user_is_dn = 0;
    sec->remote_user_attribute = NULL;
    sec->compare_dn_on_server = 0;

    sec->authz_prefix = AUTHZ_PREFIX;

    return sec;
}

static kuda_status_t authnz_ldap_cleanup_connection_close(void *param)
{
    util_ldap_connection_t *ldc = param;
    util_ldap_connection_close(ldc);
    return KUDA_SUCCESS;
}

static int set_request_vars(request_rec *r, enum auth_ldap_phase phase) {
    char *prefix = NULL;
    int prefix_len;
    int remote_user_attribute_set = 0;
    authn_ldap_request_t *req =
        (authn_ldap_request_t *)clhy_get_capi_config(r->request_config, &authnz_ldap_capi);
    authn_ldap_config_t *sec =
        (authn_ldap_config_t *)clhy_get_capi_config(r->per_dir_config, &authnz_ldap_capi);
    const char **vals = req->vals;

    prefix = (phase == LDAP_AUTHN) ? AUTHN_PREFIX : sec->authz_prefix;
    prefix_len = strlen(prefix);

    if (sec->attributes && vals) {
        kuda_table_t *e = r->subprocess_env;
        int i = 0;
        while (sec->attributes[i]) {
            char *str = kuda_pstrcat(r->pool, prefix, sec->attributes[i], NULL);
            int j = prefix_len;
            while (str[j]) {
                str[j] = kuda_toupper(str[j]);
                j++;
            }
            kuda_table_setn(e, str, vals[i] ? vals[i] : "");

            /* handle remote_user_attribute, if set */
            if ((phase == LDAP_AUTHN) &&
                sec->remote_user_attribute &&
                !strcmp(sec->remote_user_attribute, sec->attributes[i])) {
                r->user = (char *)kuda_pstrdup(r->pool, vals[i]);
                remote_user_attribute_set = 1;
            }
            i++;
        }
    }
    return remote_user_attribute_set;
}

static const char *ldap_determine_binddn(request_rec *r, const char *user) {
    authn_ldap_config_t *sec =
        (authn_ldap_config_t *)clhy_get_capi_config(r->per_dir_config, &authnz_ldap_capi);
    const char *result = user;
    clhy_regmatch_t regm[CLHY_MAX_REG_MATCH];

    if (NULL == user || NULL == sec || !sec->bind_regex || !sec->bind_subst) {
        return result;
    }

    if (!clhy_regexec(sec->bind_regex, user, CLHY_MAX_REG_MATCH, regm, 0)) {
        char *substituted = clhy_pregsub(r->pool, sec->bind_subst, user, CLHY_MAX_REG_MATCH, regm);
        if (NULL != substituted) {
            result = substituted;
        }
    }

    kuda_table_set(r->subprocess_env, "LDAP_BINDASUSER", result);

    return result;
}


/* Some LDAP servers restrict who can search or compare, and the hard-coded ID
 * might be good for the DN lookup but not for later operations.
 */
static util_ldap_connection_t *get_connection_for_authz(request_rec *r, enum auth_ldap_optype type) {
    authn_ldap_request_t *req =
        (authn_ldap_request_t *)clhy_get_capi_config(r->request_config, &authnz_ldap_capi);
    authn_ldap_config_t *sec =
        (authn_ldap_config_t *)clhy_get_capi_config(r->per_dir_config, &authnz_ldap_capi);

    char *binddn = sec->binddn;
    char *bindpw = sec->bindpw;

    /* If the per-request config isn't set, we didn't authenticate this user, and leave the default credentials */
    if (req && req->password &&
         ((type == LDAP_SEARCH && sec->search_as_user)    ||
          (type == LDAP_COMPARE && sec->compare_as_user)  ||
          (type == LDAP_COMPARE_AND_SEARCH && sec->compare_as_user && sec->search_as_user))){
            binddn = req->dn;
            bindpw = req->password;
    }

    return util_ldap_connection_find(r, sec->host, sec->port,
                                     binddn, bindpw,
                                     sec->deref, sec->secure);
}
/*
 * Authentication Phase
 * --------------------
 *
 * This phase authenticates the credentials the user has sent with
 * the request (ie the username and password are checked). This is done
 * by making an attempt to bind to the LDAP server using this user's
 * DN and the supplied password.
 *
 */
static authn_status authn_ldap_check_password(request_rec *r, const char *user,
                                              const char *password)
{
    char filtbuf[FILTER_LENGTH];
    authn_ldap_config_t *sec =
        (authn_ldap_config_t *)clhy_get_capi_config(r->per_dir_config, &authnz_ldap_capi);

    util_ldap_connection_t *ldc = NULL;
    int result = 0;
    int remote_user_attribute_set = 0;
    const char *dn = NULL;
    const char *utfpassword;

    authn_ldap_request_t *req =
        (authn_ldap_request_t *)kuda_pcalloc(r->pool, sizeof(authn_ldap_request_t));
    clhy_set_capi_config(r->request_config, &authnz_ldap_capi, req);

/*
    if (!sec->enabled) {
        return AUTH_USER_NOT_FOUND;
    }
*/

    /*
     * Basic sanity checks before any LDAP operations even happen.
     */
    if (!sec->have_ldap_url) {
        return AUTH_GENERAL_ERROR;
    }

    /* There is a good AuthLDAPURL, right? */
    if (sec->host) {
        const char *binddn = sec->binddn;
        const char *bindpw = sec->bindpw;
        if (sec->initial_bind_as_user) {
            bindpw = password;
            binddn = ldap_determine_binddn(r, user);
        }

        ldc = util_ldap_connection_find(r, sec->host, sec->port,
                                       binddn, bindpw,
                                       sec->deref, sec->secure);
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01690)
                      "auth_ldap authenticate: no sec->host - weird...?");
        return AUTH_GENERAL_ERROR;
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01691)
                  "auth_ldap authenticate: using URL %s", sec->url);

    /* Get the password that the client sent */
    if (password == NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01692)
                      "auth_ldap authenticate: no password specified");
        util_ldap_connection_close(ldc);
        return AUTH_GENERAL_ERROR;
    }

    if (user == NULL) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01693)
                      "auth_ldap authenticate: no user specified");
        util_ldap_connection_close(ldc);
        return AUTH_GENERAL_ERROR;
    }

    /* build the username filter */
    authn_ldap_build_filter(filtbuf, r, user, NULL, sec);

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, r,
                      "auth_ldap authenticate: final authn filter is %s", filtbuf);

    /* convert password to utf-8 */
    utfpassword = authn_ldap_xlate_password(r, password);

    /* do the user search */
    result = util_ldap_cache_checkuserid(r, ldc, sec->url, sec->basedn, sec->scope,
                                         sec->attributes, filtbuf, utfpassword,
                                         &dn, &(req->vals));
    util_ldap_connection_close(ldc);

    /* handle bind failure */
    if (result != LDAP_SUCCESS) {
        if (!sec->bind_authoritative) {
           clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01694)
                      "auth_ldap authenticate: user %s authentication failed; "
                      "URI %s [%s][%s] (not authoritative)",
                      user, r->uri, ldc->reason, ldap_err2string(result));
           return AUTH_USER_NOT_FOUND;
        }

        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(01695)
                      "auth_ldap authenticate: "
                      "user %s authentication failed; URI %s [%s][%s]",
                      user, r->uri, ldc->reason, ldap_err2string(result));

        /* talking to a primitive LDAP server (like RACF-over-LDAP) that doesn't return specific errors */
        if (!strcasecmp(sec->filter, "none") && LDAP_OTHER == result) { 
            return AUTH_USER_NOT_FOUND;
        }

        return (LDAP_NO_SUCH_OBJECT == result) ? AUTH_USER_NOT_FOUND
#ifdef LDAP_SECURITY_ERROR
                 : (LDAP_SECURITY_ERROR(result)) ? AUTH_DENIED
#else
                 : (LDAP_INAPPROPRIATE_AUTH == result) ? AUTH_DENIED
                 : (LDAP_INVALID_CREDENTIALS == result) ? AUTH_DENIED
#ifdef LDAP_INSUFFICIENT_ACCESS
                 : (LDAP_INSUFFICIENT_ACCESS == result) ? AUTH_DENIED
#endif
#ifdef LDAP_INSUFFICIENT_RIGHTS
                 : (LDAP_INSUFFICIENT_RIGHTS == result) ? AUTH_DENIED
#endif
#endif
#ifdef LDAP_CONSTRAINT_VIOLATION
    /* At least Sun Directory Server sends this if a user is
     * locked. This is not covered by LDAP_SECURITY_ERROR.
     */
                 : (LDAP_CONSTRAINT_VIOLATION == result) ? AUTH_DENIED
#endif
                 : AUTH_GENERAL_ERROR;
    }

    /* mark the user and DN */
    req->dn = kuda_pstrdup(r->pool, dn);
    req->user = kuda_pstrdup(r->pool, user);
    req->password = kuda_pstrdup(r->pool, password);
    if (sec->user_is_dn) {
        r->user = req->dn;
    }

    /* add environment variables */
    remote_user_attribute_set = set_request_vars(r, LDAP_AUTHN);

    /* sanity check */
    if (sec->remote_user_attribute && !remote_user_attribute_set) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01696)
                  "auth_ldap authenticate: "
                  "REMOTE_USER was to be set with attribute '%s', "
                  "but this attribute was not requested for in the "
                  "LDAP query for the user. REMOTE_USER will fall "
                  "back to username or DN as appropriate.",
                  sec->remote_user_attribute);
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01697)
                  "auth_ldap authenticate: accepting %s", user);

    return AUTH_GRANTED;
}

static authz_status ldapuser_check_authorization(request_rec *r,
                                                 const char *require_args,
                                                 const void *parsed_require_args)
{
    int result = 0;
    authn_ldap_request_t *req =
        (authn_ldap_request_t *)clhy_get_capi_config(r->request_config, &authnz_ldap_capi);
    authn_ldap_config_t *sec =
        (authn_ldap_config_t *)clhy_get_capi_config(r->per_dir_config, &authnz_ldap_capi);

    util_ldap_connection_t *ldc = NULL;

    const char *err = NULL;
    const clhy_expr_info_t *expr = parsed_require_args;
    const char *require;

    const char *t;
    char *w;

    char filtbuf[FILTER_LENGTH];
    const char *dn = NULL;

    if (!r->user) {
        return AUTHZ_DENIED_NO_USER;
    }

    if (!sec->have_ldap_url) {
        return AUTHZ_DENIED;
    }

    if (sec->host) {
        ldc = get_connection_for_authz(r, LDAP_COMPARE);
        kuda_pool_cleanup_register(r->pool, ldc,
                                  authnz_ldap_cleanup_connection_close,
                                  kuda_pool_cleanup_null);
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01698)
                      "auth_ldap authorize: no sec->host - weird...?");
        return AUTHZ_DENIED;
    }

    /*
     * If we have been authenticated by some other cAPI than capi_authnz_ldap,
     * the req structure needed for authorization needs to be created
     * and populated with the userid and DN of the account in LDAP
     */


    if (!strlen(r->user)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01699)
            "ldap authorize: Userid is blank, AuthType=%s",
            r->clhy_auth_type);
    }

    if(!req) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01700)
            "ldap authorize: Creating LDAP req structure");

        req = (authn_ldap_request_t *)kuda_pcalloc(r->pool,
            sizeof(authn_ldap_request_t));

        /* Build the username filter */
        authn_ldap_build_filter(filtbuf, r, r->user, NULL, sec);

        /* Search for the user DN */
        result = util_ldap_cache_getuserdn(r, ldc, sec->url, sec->basedn,
             sec->scope, sec->attributes, filtbuf, &dn, &(req->vals));

        /* Search failed, log error and return failure */
        if(result != LDAP_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01701)
                "auth_ldap authorise: User DN not found, %s", ldc->reason);
            return AUTHZ_DENIED;
        }

        clhy_set_capi_config(r->request_config, &authnz_ldap_capi, req);
        req->dn = kuda_pstrdup(r->pool, dn);
        req->user = r->user;

    }

    if (req->dn == NULL || strlen(req->dn) == 0) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01702)
                      "auth_ldap authorize: require user: user's DN has not "
                      "been defined; failing authorization");
        return AUTHZ_DENIED;
    }

    require = clhy_expr_str_exec(r, expr, &err);
    if (err) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02585)
                      "auth_ldap authorize: require user: Can't evaluate expression: %s",
                      err);
        return AUTHZ_DENIED;
    }

    /*
     * First do a whole-line compare, in case it's something like
     *   require user Babs Jensen
     */
    result = util_ldap_cache_compare(r, ldc, sec->url, req->dn, sec->attribute, require);
    switch(result) {
        case LDAP_COMPARE_TRUE: {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01703)
                          "auth_ldap authorize: require user: authorization "
                          "successful");
            set_request_vars(r, LDAP_AUTHZ);
            return AUTHZ_GRANTED;
        }
        default: {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01704)
                          "auth_ldap authorize: require user: "
                          "authorization failed [%s][%s]",
                          ldc->reason, ldap_err2string(result));
        }
    }

    /*
     * Now break apart the line and compare each word on it
     */
    t = require;
    while ((w = clhy_getword_conf(r->pool, &t)) && w[0]) {
        result = util_ldap_cache_compare(r, ldc, sec->url, req->dn, sec->attribute, w);
        switch(result) {
            case LDAP_COMPARE_TRUE: {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01705)
                              "auth_ldap authorize: "
                              "require user: authorization successful");
                set_request_vars(r, LDAP_AUTHZ);
                return AUTHZ_GRANTED;
            }
            default: {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01706)
                              "auth_ldap authorize: "
                              "require user: authorization failed [%s][%s]",
                              ldc->reason, ldap_err2string(result));
            }
        }
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01707)
                  "auth_ldap authorize user: authorization denied for "
                  "user %s to %s",
                  r->user, r->uri);

    return AUTHZ_DENIED;
}

static authz_status ldapgroup_check_authorization(request_rec *r,
                                                  const char *require_args,
                                                  const void *parsed_require_args)
{
    int result = 0;
    authn_ldap_request_t *req =
        (authn_ldap_request_t *)clhy_get_capi_config(r->request_config, &authnz_ldap_capi);
    authn_ldap_config_t *sec =
        (authn_ldap_config_t *)clhy_get_capi_config(r->per_dir_config, &authnz_ldap_capi);

    util_ldap_connection_t *ldc = NULL;

    const char *err = NULL;
    const clhy_expr_info_t *expr = parsed_require_args;
    const char *require;

    const char *t;

    char filtbuf[FILTER_LENGTH];
    const char *dn = NULL;
    struct capi_auth_ldap_groupattr_entry_t *ent;
    int i;

    if (!r->user) {
        return AUTHZ_DENIED_NO_USER;
    }

    if (!sec->have_ldap_url) {
        return AUTHZ_DENIED;
    }

    if (sec->host) {
        ldc = get_connection_for_authz(r, LDAP_COMPARE); /* for the top-level group only */
        kuda_pool_cleanup_register(r->pool, ldc,
                                  authnz_ldap_cleanup_connection_close,
                                  kuda_pool_cleanup_null);
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01708)
                      "auth_ldap authorize: no sec->host - weird...?");
        return AUTHZ_DENIED;
    }

    /*
     * If there are no elements in the group attribute array, the default should be
     * member and uniquemember; populate the array now.
     */
    if (sec->groupattr->nelts == 0) {
        struct capi_auth_ldap_groupattr_entry_t *grp;
#if KUDA_HAS_THREADS
        kuda_thread_mutex_lock(sec->lock);
#endif
        grp = kuda_array_push(sec->groupattr);
        grp->name = "member";
        grp = kuda_array_push(sec->groupattr);
        grp->name = "uniqueMember";
#if KUDA_HAS_THREADS
        kuda_thread_mutex_unlock(sec->lock);
#endif
    }

    /*
     * If there are no elements in the sub group classes array, the default
     * should be groupOfNames and groupOfUniqueNames; populate the array now.
     */
    if (sec->subgroupclasses->nelts == 0) {
        struct capi_auth_ldap_groupattr_entry_t *grp;
#if KUDA_HAS_THREADS
        kuda_thread_mutex_lock(sec->lock);
#endif
        grp = kuda_array_push(sec->subgroupclasses);
        grp->name = "groupOfNames";
        grp = kuda_array_push(sec->subgroupclasses);
        grp->name = "groupOfUniqueNames";
#if KUDA_HAS_THREADS
        kuda_thread_mutex_unlock(sec->lock);
#endif
    }

    /*
     * If we have been authenticated by some other cAPI than capi_auth_ldap,
     * the req structure needed for authorization needs to be created
     * and populated with the userid and DN of the account in LDAP
     */

    if (!strlen(r->user)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01709)
            "ldap authorize: Userid is blank, AuthType=%s",
            r->clhy_auth_type);
    }

    if(!req) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01710)
            "ldap authorize: Creating LDAP req structure");

        req = (authn_ldap_request_t *)kuda_pcalloc(r->pool,
            sizeof(authn_ldap_request_t));
        /* Build the username filter */
        authn_ldap_build_filter(filtbuf, r, r->user, NULL, sec);

        /* Search for the user DN */
        result = util_ldap_cache_getuserdn(r, ldc, sec->url, sec->basedn,
             sec->scope, sec->attributes, filtbuf, &dn, &(req->vals));

        /* Search failed, log error and return failure */
        if(result != LDAP_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01711)
                "auth_ldap authorise: User DN not found, %s", ldc->reason);
            return AUTHZ_DENIED;
        }

        clhy_set_capi_config(r->request_config, &authnz_ldap_capi, req);
        req->dn = kuda_pstrdup(r->pool, dn);
        req->user = r->user;
    }

    ent = (struct capi_auth_ldap_groupattr_entry_t *) sec->groupattr->elts;

    if (sec->group_attrib_is_dn) {
        if (req->dn == NULL || strlen(req->dn) == 0) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01712)
                          "auth_ldap authorize: require group: user's DN has "
                          "not been defined; failing authorization for user %s",
                          r->user);
            return AUTHZ_DENIED;
        }
    }
    else {
        if (req->user == NULL || strlen(req->user) == 0) {
            /* We weren't called in the authentication phase, so we didn't have a
             * chance to set the user field. Do so now. */
            req->user = r->user;
        }
    }

    require = clhy_expr_str_exec(r, expr, &err);
    if (err) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02586)
                      "auth_ldap authorize: require group: Can't evaluate expression: %s",
                      err);
        return AUTHZ_DENIED;
    }

    t = require;

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01713)
                  "auth_ldap authorize: require group: testing for group "
                  "membership in \"%s\"",
                  t);

    /* PR52464 exhaust attrs in base group before checking subgroups */
    for (i = 0; i < sec->groupattr->nelts; i++) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01714)
                      "auth_ldap authorize: require group: testing for %s: "
                      "%s (%s)",
                      ent[i].name,
                      sec->group_attrib_is_dn ? req->dn : req->user, t);

        result = util_ldap_cache_compare(r, ldc, sec->url, t, ent[i].name,
                             sec->group_attrib_is_dn ? req->dn : req->user);
        if (result == LDAP_COMPARE_TRUE) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01715)
                          "auth_ldap authorize: require group: "
                          "authorization successful (attribute %s) "
                          "[%s][%d - %s]",
                          ent[i].name, ldc->reason, result,
                          ldap_err2string(result));
            set_request_vars(r, LDAP_AUTHZ);
            return AUTHZ_GRANTED;
        }
        else { 
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01719)
                              "auth_ldap authorize: require group \"%s\": "
                              "didn't match with attr %s [%s][%d - %s]",
                              t, ent[i].name, ldc->reason, result, 
                              ldap_err2string(result));
        }
    }
    
    for (i = 0; i < sec->groupattr->nelts; i++) {
        /* nested groups need searches and compares, so grab a new handle */
        authnz_ldap_cleanup_connection_close(ldc);
        kuda_pool_cleanup_kill(r->pool, ldc,authnz_ldap_cleanup_connection_close);

        ldc = get_connection_for_authz(r, LDAP_COMPARE_AND_SEARCH);
        kuda_pool_cleanup_register(r->pool, ldc,
                                  authnz_ldap_cleanup_connection_close,
                                  kuda_pool_cleanup_null);

        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01716)
                       "auth_ldap authorise: require group \"%s\": "
                       "failed [%s][%d - %s], checking sub-groups",
                       t, ldc->reason, result, ldap_err2string(result));

        result = util_ldap_cache_check_subgroups(r, ldc, sec->url, t, ent[i].name,
                                                 sec->group_attrib_is_dn ? req->dn : req->user,
                                                 sec->sgAttributes[0] ? sec->sgAttributes : default_attributes,
                                                 sec->subgroupclasses,
                                                 0, sec->maxNestingDepth);
        if (result == LDAP_COMPARE_TRUE) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01717)
                          "auth_ldap authorise: require group "
                          "(sub-group): authorisation successful "
                          "(attribute %s) [%s][%d - %s]",
                          ent[i].name, ldc->reason, result,
                          ldap_err2string(result));
            set_request_vars(r, LDAP_AUTHZ);
            return AUTHZ_GRANTED;
        }
        else {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01718)
                          "auth_ldap authorise: require group "
                          "(sub-group) \"%s\": didn't match with attr %s "
                          "[%s][%d - %s]",
                          t, ldc->reason, ent[i].name, result, 
                          ldap_err2string(result));
        }
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01720)
                  "auth_ldap authorize group: authorization denied for "
                  "user %s to %s",
                  r->user, r->uri);

    return AUTHZ_DENIED;
}

static authz_status ldapdn_check_authorization(request_rec *r,
                                               const char *require_args,
                                               const void *parsed_require_args)
{
    int result = 0;
    authn_ldap_request_t *req =
        (authn_ldap_request_t *)clhy_get_capi_config(r->request_config, &authnz_ldap_capi);
    authn_ldap_config_t *sec =
        (authn_ldap_config_t *)clhy_get_capi_config(r->per_dir_config, &authnz_ldap_capi);

    util_ldap_connection_t *ldc = NULL;

    const char *err = NULL;
    const clhy_expr_info_t *expr = parsed_require_args;
    const char *require;

    const char *t;

    char filtbuf[FILTER_LENGTH];
    const char *dn = NULL;

    if (!r->user) {
        return AUTHZ_DENIED_NO_USER;
    }

    if (!sec->have_ldap_url) {
        return AUTHZ_DENIED;
    }

    if (sec->host) {
        ldc = get_connection_for_authz(r, LDAP_SEARCH); /* _comparedn is a searche */
        kuda_pool_cleanup_register(r->pool, ldc,
                                  authnz_ldap_cleanup_connection_close,
                                  kuda_pool_cleanup_null);
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01721)
                      "auth_ldap authorize: no sec->host - weird...?");
        return AUTHZ_DENIED;
    }

    /*
     * If we have been authenticated by some other cAPI than capi_auth_ldap,
     * the req structure needed for authorization needs to be created
     * and populated with the userid and DN of the account in LDAP
     */

    if (!strlen(r->user)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01722)
            "ldap authorize: Userid is blank, AuthType=%s",
            r->clhy_auth_type);
    }

    if(!req) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01723)
            "ldap authorize: Creating LDAP req structure");

        req = (authn_ldap_request_t *)kuda_pcalloc(r->pool,
            sizeof(authn_ldap_request_t));
        /* Build the username filter */
        authn_ldap_build_filter(filtbuf, r, r->user, NULL, sec);

        /* Search for the user DN */
        result = util_ldap_cache_getuserdn(r, ldc, sec->url, sec->basedn,
             sec->scope, sec->attributes, filtbuf, &dn, &(req->vals));

        /* Search failed, log error and return failure */
        if(result != LDAP_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01724)
                "auth_ldap authorise: User DN not found with filter %s: %s", filtbuf, ldc->reason);
            return AUTHZ_DENIED;
        }

        clhy_set_capi_config(r->request_config, &authnz_ldap_capi, req);
        req->dn = kuda_pstrdup(r->pool, dn);
        req->user = r->user;
    }

    require = clhy_expr_str_exec(r, expr, &err);
    if (err) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02587)
                      "auth_ldap authorize: require dn: Can't evaluate expression: %s",
                      err);
        return AUTHZ_DENIED;
    }

    t = require;

    if (req->dn == NULL || strlen(req->dn) == 0) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01725)
                      "auth_ldap authorize: require dn: user's DN has not "
                      "been defined; failing authorization");
        return AUTHZ_DENIED;
    }

    result = util_ldap_cache_comparedn(r, ldc, sec->url, req->dn, t, sec->compare_dn_on_server);
    switch(result) {
        case LDAP_COMPARE_TRUE: {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01726)
                          "auth_ldap authorize: "
                          "require dn: authorization successful");
            set_request_vars(r, LDAP_AUTHZ);
            return AUTHZ_GRANTED;
        }
        default: {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01727)
                          "auth_ldap authorize: "
                          "require dn \"%s\": LDAP error [%s][%s]",
                          t, ldc->reason, ldap_err2string(result));
        }
    }


    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01728)
                  "auth_ldap authorize dn: authorization denied for "
                  "user %s to %s",
                  r->user, r->uri);

    return AUTHZ_DENIED;
}

static authz_status ldapattribute_check_authorization(request_rec *r,
                                                      const char *require_args,
                                                      const void *parsed_require_args)
{
    int result = 0;
    authn_ldap_request_t *req =
        (authn_ldap_request_t *)clhy_get_capi_config(r->request_config, &authnz_ldap_capi);
    authn_ldap_config_t *sec =
        (authn_ldap_config_t *)clhy_get_capi_config(r->per_dir_config, &authnz_ldap_capi);

    util_ldap_connection_t *ldc = NULL;

    const char *err = NULL;
    const clhy_expr_info_t *expr = parsed_require_args;
    const char *require;

    const char *t;
    char *w, *value;

    char filtbuf[FILTER_LENGTH];
    const char *dn = NULL;

    if (!r->user) {
        return AUTHZ_DENIED_NO_USER;
    }

    if (!sec->have_ldap_url) {
        return AUTHZ_DENIED;
    }

    if (sec->host) {
        ldc = get_connection_for_authz(r, LDAP_COMPARE);
        kuda_pool_cleanup_register(r->pool, ldc,
                                  authnz_ldap_cleanup_connection_close,
                                  kuda_pool_cleanup_null);
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01729)
                      "auth_ldap authorize: no sec->host - weird...?");
        return AUTHZ_DENIED;
    }

    /*
     * If we have been authenticated by some other cAPI than capi_auth_ldap,
     * the req structure needed for authorization needs to be created
     * and populated with the userid and DN of the account in LDAP
     */

    if (!strlen(r->user)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01730)
            "ldap authorize: Userid is blank, AuthType=%s",
            r->clhy_auth_type);
    }

    if(!req) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01731)
            "ldap authorize: Creating LDAP req structure");

        req = (authn_ldap_request_t *)kuda_pcalloc(r->pool,
            sizeof(authn_ldap_request_t));
        /* Build the username filter */
        authn_ldap_build_filter(filtbuf, r, r->user, NULL, sec);

        /* Search for the user DN */
        result = util_ldap_cache_getuserdn(r, ldc, sec->url, sec->basedn,
             sec->scope, sec->attributes, filtbuf, &dn, &(req->vals));

        /* Search failed, log error and return failure */
        if(result != LDAP_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01732)
                "auth_ldap authorise: User DN not found with filter %s: %s", filtbuf, ldc->reason);
            return AUTHZ_DENIED;
        }

        clhy_set_capi_config(r->request_config, &authnz_ldap_capi, req);
        req->dn = kuda_pstrdup(r->pool, dn);
        req->user = r->user;
    }

    if (req->dn == NULL || strlen(req->dn) == 0) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01733)
                      "auth_ldap authorize: require ldap-attribute: user's DN "
                      "has not been defined; failing authorization");
        return AUTHZ_DENIED;
    }

    require = clhy_expr_str_exec(r, expr, &err);
    if (err) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02588)
                      "auth_ldap authorize: require ldap-attribute: Can't "
                      "evaluate expression: %s", err);
        return AUTHZ_DENIED;
    }

    t = require;

    while (t[0]) {
        w = clhy_getword(r->pool, &t, '=');
        value = clhy_getword_conf(r->pool, &t);

        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01734)
                      "auth_ldap authorize: checking attribute %s has value %s",
                      w, value);
        result = util_ldap_cache_compare(r, ldc, sec->url, req->dn, w, value);
        switch(result) {
            case LDAP_COMPARE_TRUE: {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01735)
                              "auth_ldap authorize: "
                              "require attribute: authorization successful");
                set_request_vars(r, LDAP_AUTHZ);
                return AUTHZ_GRANTED;
            }
            default: {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01736)
                              "auth_ldap authorize: require attribute: "
                              "authorization failed [%s][%s]",
                              ldc->reason, ldap_err2string(result));
            }
        }
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01737)
                  "auth_ldap authorize attribute: authorization denied for "
                  "user %s to %s",
                  r->user, r->uri);

    return AUTHZ_DENIED;
}

static authz_status ldapfilter_check_authorization(request_rec *r,
                                                   const char *require_args,
                                                   const void *parsed_require_args)
{
    int result = 0;
    authn_ldap_request_t *req =
        (authn_ldap_request_t *)clhy_get_capi_config(r->request_config, &authnz_ldap_capi);
    authn_ldap_config_t *sec =
        (authn_ldap_config_t *)clhy_get_capi_config(r->per_dir_config, &authnz_ldap_capi);

    util_ldap_connection_t *ldc = NULL;

    const char *err = NULL;
    const clhy_expr_info_t *expr = parsed_require_args;
    const char *require;

    const char *t;

    char filtbuf[FILTER_LENGTH];
    const char *dn = NULL;

    if (!r->user) {
        return AUTHZ_DENIED_NO_USER;
    }

    if (!sec->have_ldap_url) {
        return AUTHZ_DENIED;
    }

    if (sec->host) {
        ldc = get_connection_for_authz(r, LDAP_SEARCH);
        kuda_pool_cleanup_register(r->pool, ldc,
                                  authnz_ldap_cleanup_connection_close,
                                  kuda_pool_cleanup_null);
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01738)
                      "auth_ldap authorize: no sec->host - weird...?");
        return AUTHZ_DENIED;
    }

    /*
     * If we have been authenticated by some other cAPI than capi_auth_ldap,
     * the req structure needed for authorization needs to be created
     * and populated with the userid and DN of the account in LDAP
     */

    if (!strlen(r->user)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01739)
            "ldap authorize: Userid is blank, AuthType=%s",
            r->clhy_auth_type);
    }

    if(!req) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01740)
            "ldap authorize: Creating LDAP req structure");

        req = (authn_ldap_request_t *)kuda_pcalloc(r->pool,
            sizeof(authn_ldap_request_t));
        /* Build the username filter */
        authn_ldap_build_filter(filtbuf, r, r->user, NULL, sec);

        /* Search for the user DN */
        result = util_ldap_cache_getuserdn(r, ldc, sec->url, sec->basedn,
             sec->scope, sec->attributes, filtbuf, &dn, &(req->vals));

        /* Search failed, log error and return failure */
        if(result != LDAP_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01741)
                "auth_ldap authorise: User DN not found with filter %s: %s", filtbuf, ldc->reason);
            return AUTHZ_DENIED;
        }

        clhy_set_capi_config(r->request_config, &authnz_ldap_capi, req);
        req->dn = kuda_pstrdup(r->pool, dn);
        req->user = r->user;
    }

    if (req->dn == NULL || strlen(req->dn) == 0) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01742)
                      "auth_ldap authorize: require ldap-filter: user's DN "
                      "has not been defined; failing authorization");
        return AUTHZ_DENIED;
    }

    require = clhy_expr_str_exec(r, expr, &err);
    if (err) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02589)
                      "auth_ldap authorize: require ldap-filter: Can't "
                      "evaluate require expression: %s", err);
        return AUTHZ_DENIED;
    }

    t = require;

    if (t[0]) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01743)
                      "auth_ldap authorize: checking filter %s", t);

        /* Build the username filter */
        authn_ldap_build_filter(filtbuf, r, req->user, t, sec);

        /* Search for the user DN */
        result = util_ldap_cache_getuserdn(r, ldc, sec->url, sec->basedn,
             sec->scope, sec->attributes, filtbuf, &dn, &(req->vals));

        /* Make sure that the filtered search returned the correct user dn */
        if (result == LDAP_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01744)
                          "auth_ldap authorize: checking dn match %s", dn);
            if (sec->compare_as_user) {
                /* ldap-filter is the only authz that requires a search and a compare */
                kuda_pool_cleanup_kill(r->pool, ldc, authnz_ldap_cleanup_connection_close);
                authnz_ldap_cleanup_connection_close(ldc);
                ldc = get_connection_for_authz(r, LDAP_COMPARE);
            }
            result = util_ldap_cache_comparedn(r, ldc, sec->url, req->dn, dn,
                                               sec->compare_dn_on_server);
        }

        switch(result) {
            case LDAP_COMPARE_TRUE: {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01745)
                              "auth_ldap authorize: require ldap-filter: "
                              "authorization successful");
                set_request_vars(r, LDAP_AUTHZ);
                return AUTHZ_GRANTED;
            }
            case LDAP_FILTER_ERROR: {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01746)
                              "auth_ldap authorize: require ldap-filter: "
                              "%s authorization failed [%s][%s]",
                              filtbuf, ldc->reason, ldap_err2string(result));
                break;
            }
            default: {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01747)
                              "auth_ldap authorize: require ldap-filter: "
                              "authorization failed [%s][%s]",
                              ldc->reason, ldap_err2string(result));
            }
        }
    }

    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01748)
                  "auth_ldap authorize filter: authorization denied for "
                  "user %s to %s",
                  r->user, r->uri);

    return AUTHZ_DENIED;
}

static const char *ldap_parse_config(cmd_parms *cmd, const char *require_line,
                                     const void **parsed_require_line)
{
    const char *expr_err = NULL;
    clhy_expr_info_t *expr;

    expr = clhy_expr_parse_cmd(cmd, require_line, CLHY_EXPR_FLAG_STRING_RESULT,
            &expr_err, NULL);

    if (expr_err)
        return kuda_pstrcat(cmd->temp_pool,
                           "Cannot parse expression in require line: ",
                           expr_err, NULL);

    *parsed_require_line = expr;

    return NULL;
}


/*
 * Use the ldap url parsing routines to break up the ldap url into
 * host and port.
 */
static const char *capi_auth_ldap_parse_url(cmd_parms *cmd,
                                    void *config,
                                    const char *url,
                                    const char *mode)
{
    int rc;
    kuda_ldap_url_desc_t *urld;
    kuda_ldap_err_t *result;

    authn_ldap_config_t *sec = config;

    rc = kuda_ldap_url_parse(cmd->pool, url, &(urld), &(result));
    if (rc != KUDA_SUCCESS) {
        return result->reason;
    }
    sec->url = kuda_pstrdup(cmd->pool, url);

    /* Set all the values, or at least some sane defaults */
    if (sec->host) {
        sec->host = kuda_pstrcat(cmd->pool, urld->lud_host, " ", sec->host, NULL);
    }
    else {
        sec->host = urld->lud_host? kuda_pstrdup(cmd->pool, urld->lud_host) : "localhost";
    }
    sec->basedn = urld->lud_dn? kuda_pstrdup(cmd->pool, urld->lud_dn) : "";
    if (urld->lud_attrs && urld->lud_attrs[0]) {
        int i = 1;
        while (urld->lud_attrs[i]) {
            i++;
        }
        sec->attributes = kuda_pcalloc(cmd->pool, sizeof(char *) * (i+1));
        i = 0;
        while (urld->lud_attrs[i]) {
            sec->attributes[i] = kuda_pstrdup(cmd->pool, urld->lud_attrs[i]);
            i++;
        }
        sec->attribute = sec->attributes[0];
    }
    else {
        sec->attribute = "uid";
    }

    sec->scope = urld->lud_scope == LDAP_SCOPE_ONELEVEL ?
        LDAP_SCOPE_ONELEVEL : LDAP_SCOPE_SUBTREE;

    if (urld->lud_filter) {
        if (urld->lud_filter[0] == '(') {
            /*
             * Get rid of the surrounding parens; later on when generating the
             * filter, they'll be put back.
             */
            sec->filter = kuda_pstrmemdup(cmd->pool, urld->lud_filter+1,
                                                    strlen(urld->lud_filter)-2);
        }
        else {
            sec->filter = kuda_pstrdup(cmd->pool, urld->lud_filter);
        }
    }
    else {
        sec->filter = "objectclass=*";
    }

    if (mode) {
        if (0 == strcasecmp("NONE", mode)) {
            sec->secure = KUDA_LDAP_NONE;
        }
        else if (0 == strcasecmp("SSL", mode)) {
            sec->secure = KUDA_LDAP_SSL;
        }
        else if (0 == strcasecmp("TLS", mode) || 0 == strcasecmp("STARTTLS", mode)) {
            sec->secure = KUDA_LDAP_STARTTLS;
        }
        else {
            return "Invalid LDAP connection mode setting: must be one of NONE, "
                   "SSL, or TLS/STARTTLS";
        }
    }

      /* "ldaps" indicates secure ldap connections desired
      */
    if (strncasecmp(url, "ldaps", 5) == 0)
    {
        sec->secure = KUDA_LDAP_SSL;
        sec->port = urld->lud_port? urld->lud_port : LDAPS_PORT;
    }
    else
    {
        sec->port = urld->lud_port? urld->lud_port : LDAP_PORT;
    }

    sec->have_ldap_url = 1;

    clhy_log_error(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, cmd->server,
                 "auth_ldap url parse: `%s', Host: %s, Port: %d, DN: %s, "
                 "attrib: %s, scope: %s, filter: %s, connection mode: %s",
                 url,
                 urld->lud_host,
                 urld->lud_port,
                 urld->lud_dn,
                 urld->lud_attrs? urld->lud_attrs[0] : "(null)",
                 (urld->lud_scope == LDAP_SCOPE_SUBTREE? "subtree" :
                  urld->lud_scope == LDAP_SCOPE_BASE? "base" :
                  urld->lud_scope == LDAP_SCOPE_ONELEVEL? "onelevel" : "unknown"),
                 urld->lud_filter,
                 sec->secure == KUDA_LDAP_SSL  ? "using SSL": "not using SSL"
                 );

    return NULL;
}

static const char *capi_auth_ldap_set_deref(cmd_parms *cmd, void *config, const char *arg)
{
    authn_ldap_config_t *sec = config;

    if (strcmp(arg, "never") == 0 || strcasecmp(arg, "off") == 0) {
        sec->deref = never;
    }
    else if (strcmp(arg, "searching") == 0) {
        sec->deref = searching;
    }
    else if (strcmp(arg, "finding") == 0) {
        sec->deref = finding;
    }
    else if (strcmp(arg, "always") == 0 || strcasecmp(arg, "on") == 0) {
        sec->deref = always;
    }
    else {
        return "Unrecognized value for AuthLDAPDereferenceAliases directive";
    }
    return NULL;
}

static const char *capi_auth_ldap_add_subgroup_attribute(cmd_parms *cmd, void *config, const char *arg)
{
    int i = 0;

    authn_ldap_config_t *sec = config;

    for (i = 0; sec->sgAttributes[i]; i++) {
        ;
    }
    if (i == GROUPATTR_MAX_ELTS)
        return "Too many AuthLDAPSubGroupAttribute values";

    sec->sgAttributes[i] = kuda_pstrdup(cmd->pool, arg);

    return NULL;
}

static const char *capi_auth_ldap_add_subgroup_class(cmd_parms *cmd, void *config, const char *arg)
{
    struct capi_auth_ldap_groupattr_entry_t *new;

    authn_ldap_config_t *sec = config;

    if (sec->subgroupclasses->nelts > GROUPATTR_MAX_ELTS)
        return "Too many AuthLDAPSubGroupClass values";

    new = kuda_array_push(sec->subgroupclasses);
    new->name = kuda_pstrdup(cmd->pool, arg);

    return NULL;
}

static const char *capi_auth_ldap_set_subgroup_maxdepth(cmd_parms *cmd,
                                                       void *config,
                                                       const char *max_depth)
{
    authn_ldap_config_t *sec = config;

    sec->maxNestingDepth = atol(max_depth);

    return NULL;
}

static const char *capi_auth_ldap_add_group_attribute(cmd_parms *cmd, void *config, const char *arg)
{
    struct capi_auth_ldap_groupattr_entry_t *new;

    authn_ldap_config_t *sec = config;

    if (sec->groupattr->nelts > GROUPATTR_MAX_ELTS)
        return "Too many AuthLDAPGroupAttribute directives";

    new = kuda_array_push(sec->groupattr);
    new->name = kuda_pstrdup(cmd->pool, arg);

    return NULL;
}

static const char *set_charset_config(cmd_parms *cmd, void *config, const char *arg)
{
    clhy_set_capi_config(cmd->server->capi_config, &authnz_ldap_capi,
                         (void *)arg);
    return NULL;
}

static const char *set_bind_pattern(cmd_parms *cmd, void *_cfg, const char *exp, const char *subst)
{
    authn_ldap_config_t *sec = _cfg;
    clhy_regex_t *regexp;

    regexp = clhy_pregcomp(cmd->pool, exp, CLHY_REG_EXTENDED);

    if (!regexp) {
        return kuda_pstrcat(cmd->pool, "AuthLDAPInitialBindPattern: cannot compile regular "
                                      "expression '", exp, "'", NULL);
    }

    sec->bind_regex = regexp;
    sec->bind_subst = subst;

    return NULL;
}

static const char *set_bind_password(cmd_parms *cmd, void *_cfg, const char *arg)
{
    authn_ldap_config_t *sec = _cfg;
    int arglen = strlen(arg);
    char **argv;
    char *result;

    if ((arglen > 5) && strncmp(arg, "exec:", 5) == 0) {
        if (kuda_tokenize_to_argv(arg+5, &argv, cmd->temp_pool) != KUDA_SUCCESS) {
            return kuda_pstrcat(cmd->pool,
                               "Unable to parse exec arguments from ",
                               arg+5, NULL);
        }
        argv[0] = clhy_server_root_relative(cmd->temp_pool, argv[0]);

        if (!argv[0]) {
            return kuda_pstrcat(cmd->pool,
                               "Invalid AuthLDAPBindPassword exec location:",
                               arg+5, NULL);
        }
        result = clhy_get_exec_line(cmd->pool,
                                  (const char*)argv[0], (const char * const *)argv);

        if (!result) {
            return kuda_pstrcat(cmd->pool,
                               "Unable to get bind password from exec of ",
                               arg+5, NULL);
        }
        sec->bindpw = result;
    }
    else {
        sec->bindpw = (char *)arg;
    }

    return NULL;
}

static const command_rec authnz_ldap_cmds[] =
{
    CLHY_INIT_TAKE12("AuthLDAPURL", capi_auth_ldap_parse_url, NULL, OR_AUTHCFG,
                  "URL to define LDAP connection. This should be an RFC 2255 compliant\n"
                  "URL of the form ldap://host[:port]/basedn[?attrib[?scope[?filter]]].\n"
                  "<ul>\n"
                  "<li>Host is the name of the LDAP server. Use a space separated list of hosts \n"
                  "to specify redundant servers.\n"
                  "<li>Port is optional, and specifies the port to connect to.\n"
                  "<li>basedn specifies the base DN to start searches from\n"
                  "<li>Attrib specifies what attribute to search for in the directory. If not "
                  "provided, it defaults to <b>uid</b>.\n"
                  "<li>Scope is the scope of the search, and can be either <b>sub</b> or "
                  "<b>one</b>. If not provided, the default is <b>sub</b>.\n"
                  "<li>Filter is a filter to use in the search. If not provided, "
                  "defaults to <b>(objectClass=*)</b>.\n"
                  "</ul>\n"
                  "Searches are performed using the attribute and the filter combined. "
                  "For example, assume that the\n"
                  "LDAP URL is <b>ldap://ldap.airius.com/ou=People, o=Airius?uid?sub?(posixid=*)</b>. "
                  "Searches will\n"
                  "be done using the filter <b>(&((posixid=*))(uid=<i>username</i>))</b>, "
                  "where <i>username</i>\n"
                  "is the user name passed by the HTTP client. The search will be a subtree "
                  "search on the branch <b>ou=People, o=Airius</b>."),

    CLHY_INIT_TAKE1("AuthLDAPBindDN", clhy_set_string_slot,
                  (void *)KUDA_OFFSETOF(authn_ldap_config_t, binddn), OR_AUTHCFG,
                  "DN to use to bind to LDAP server. If not provided, will do an anonymous bind."),

    CLHY_INIT_TAKE1("AuthLDAPBindPassword", set_bind_password, NULL, OR_AUTHCFG,
                  "Password to use to bind to LDAP server. If not provided, will do an anonymous bind."),

    CLHY_INIT_FLAG("AuthLDAPBindAuthoritative", clhy_set_flag_slot,
                  (void *)KUDA_OFFSETOF(authn_ldap_config_t, bind_authoritative), OR_AUTHCFG,
                  "Set to 'on' to return failures when user-specific bind fails - defaults to on."),

    CLHY_INIT_FLAG("AuthLDKUDAemoteUserIsDN", clhy_set_flag_slot,
                 (void *)KUDA_OFFSETOF(authn_ldap_config_t, user_is_dn), OR_AUTHCFG,
                 "Set to 'on' to set the REMOTE_USER environment variable to be the full "
                 "DN of the remote user. By default, this is set to off, meaning that "
                 "the REMOTE_USER variable will contain whatever value the remote user sent."),

    CLHY_INIT_TAKE1("AuthLDKUDAemoteUserAttribute", clhy_set_string_slot,
                 (void *)KUDA_OFFSETOF(authn_ldap_config_t, remote_user_attribute), OR_AUTHCFG,
                 "Override the user supplied username and place the "
                 "contents of this attribute in the REMOTE_USER "
                 "environment variable."),

    CLHY_INIT_FLAG("AuthLDAPCompareDNOnServer", clhy_set_flag_slot,
                 (void *)KUDA_OFFSETOF(authn_ldap_config_t, compare_dn_on_server), OR_AUTHCFG,
                 "Set to 'on' to force auth_ldap to do DN compares (for the \"require dn\" "
                 "directive) using the server, and set it 'off' to do the compares locally "
                 "(at the expense of possible false matches). See the documentation for "
                 "a complete description of this option."),

    CLHY_INIT_ITERATE("AuthLDAPSubGroupAttribute", capi_auth_ldap_add_subgroup_attribute, NULL, OR_AUTHCFG,
                    "Attribute labels used to define sub-group (or nested group) membership in groups - "
                    "defaults to member and uniqueMember"),

    CLHY_INIT_ITERATE("AuthLDAPSubGroupClass", capi_auth_ldap_add_subgroup_class, NULL, OR_AUTHCFG,
                     "LDAP objectClass values used to identify sub-group instances - "
                     "defaults to groupOfNames and groupOfUniqueNames"),

    CLHY_INIT_TAKE1("AuthLDAPMaxSubGroupDepth", capi_auth_ldap_set_subgroup_maxdepth, NULL, OR_AUTHCFG,
                      "Maximum subgroup nesting depth to be evaluated - defaults to 10 (top-level group = 0)"),

    CLHY_INIT_ITERATE("AuthLDAPGroupAttribute", capi_auth_ldap_add_group_attribute, NULL, OR_AUTHCFG,
                    "A list of attribute labels used to identify the user members of groups - defaults to "
                    "member and uniquemember"),

    CLHY_INIT_FLAG("AuthLDAPGroupAttributeIsDN", clhy_set_flag_slot,
                 (void *)KUDA_OFFSETOF(authn_ldap_config_t, group_attrib_is_dn), OR_AUTHCFG,
                 "If set to 'on', auth_ldap uses the DN that is retrieved from the server for"
                 "subsequent group comparisons. If set to 'off', auth_ldap uses the string"
                 "provided by the client directly. Defaults to 'on'."),

    CLHY_INIT_TAKE1("AuthLDAPDereferenceAliases", capi_auth_ldap_set_deref, NULL, OR_AUTHCFG,
                  "Determines how aliases are handled during a search. Can be one of the"
                  "values \"never\", \"searching\", \"finding\", or \"always\". "
                  "Defaults to always."),

    CLHY_INIT_TAKE1("AuthLDAPCharsetConfig", set_charset_config, NULL, RSRC_CONF,
                  "Character set conversion configuration file. If omitted, character set"
                  "conversion is disabled."),

    CLHY_INIT_TAKE1("AuthLDAPAuthorizePrefix", clhy_set_string_slot,
                  (void *)KUDA_OFFSETOF(authn_ldap_config_t, authz_prefix), OR_AUTHCFG,
                  "The prefix to add to environment variables set during "
                  "successful authorization, default '" AUTHZ_PREFIX "'"),

    CLHY_INIT_FLAG("AuthLDAPInitialBindAsUser", clhy_set_flag_slot,
                 (void *)KUDA_OFFSETOF(authn_ldap_config_t, initial_bind_as_user), OR_AUTHCFG,
                 "Set to 'on' to perform the initial DN lookup with the basic auth credentials "
                 "instead of anonymous or hard-coded credentials"),

     CLHY_INIT_TAKE2("AuthLDAPInitialBindPattern", set_bind_pattern, NULL, OR_AUTHCFG,
                   "The regex and substitution to determine a username that can bind based on an HTTP basic auth username"),

     CLHY_INIT_FLAG("AuthLDAPSearchAsUser", clhy_set_flag_slot,
                  (void *)KUDA_OFFSETOF(authn_ldap_config_t, search_as_user), OR_AUTHCFG,
                   "Set to 'on' to perform authorization-based searches with the users credentials, when this cAPI"
                   " has also performed authentication.  Does not affect nested groups lookup."),
     CLHY_INIT_FLAG("AuthLDAPCompareAsUser", clhy_set_flag_slot,
                  (void *)KUDA_OFFSETOF(authn_ldap_config_t, compare_as_user), OR_AUTHCFG,
                  "Set to 'on' to perform authorization-based compares with the users credentials, when this cAPI"
                  " has also performed authentication.  Does not affect nested groups lookups."),
    {NULL}
};

static int authnz_ldap_post_config(kuda_pool_t *p, kuda_pool_t *plog, kuda_pool_t *ptemp, server_rec *s)
{
    clhy_configfile_t *f;
    char l[MAX_STRING_LEN];
    const char *charset_confname = clhy_get_capi_config(s->capi_config,
                                                      &authnz_ldap_capi);
    kuda_status_t status;

    /*
    authn_ldap_config_t *sec = (authn_ldap_config_t *)
                                    clhy_get_capi_config(s->capi_config,
                                                         &authnz_ldap_capi);

    if (sec->secure)
    {
        if (!util_ldap_ssl_supported(s))
        {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, 0, s, CLHYLOGNO(03159)
                         "LDAP: SSL connections (ldaps://) not supported by utilLDAP");
            return(!OK);
        }
    }
    */

    /* make sure that capi_ldap is activated */
    if (clhy_find_linked_capi("util_ldap.c") == NULL) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(01749)
                     "Missing capi_ldap. This cAPI must be activated "
                     "in order for capi_authnz_ldap to function properly");
        return HTTP_INTERNAL_SERVER_ERROR;

    }

    if (!charset_confname) {
        return OK;
    }

    charset_confname = clhy_server_root_relative(p, charset_confname);
    if (!charset_confname) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, KUDA_EBADPATH, s, CLHYLOGNO(01750)
                     "Invalid charset conversion config path %s",
                     (const char *)clhy_get_capi_config(s->capi_config,
                                                        &authnz_ldap_capi));
        return HTTP_INTERNAL_SERVER_ERROR;
    }
    if ((status = clhy_pcfg_openfile(&f, ptemp, charset_confname))
                != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, status, s, CLHYLOGNO(01751)
                     "could not open charset conversion config file %s.",
                     charset_confname);
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    charset_conversions = kuda_hash_make(p);

    while (!(clhy_cfg_getline(l, MAX_STRING_LEN, f))) {
        const char *ll = l;
        char *lang;

        if (l[0] == '#') {
            continue;
        }
        lang = clhy_getword_conf(p, &ll);
        clhy_str_tolower(lang);

        if (ll[0]) {
            char *charset = clhy_getword_conf(p, &ll);
            kuda_hash_set(charset_conversions, lang, KUDA_HASH_KEY_STRING, charset);
        }
    }
    clhy_cfg_closefile(f);

    to_charset = derive_codepage_from_lang (p, "utf-8");
    if (to_charset == NULL) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, status, s, CLHYLOGNO(01752)
                     "could not find the UTF-8 charset in the file %s.",
                     charset_confname);
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    return OK;
}

static const authn_provider authn_ldap_provider =
{
    &authn_ldap_check_password,
};

static const authz_provider authz_ldapuser_provider =
{
    &ldapuser_check_authorization,
    &ldap_parse_config,
};
static const authz_provider authz_ldapgroup_provider =
{
    &ldapgroup_check_authorization,
    &ldap_parse_config,
};

static const authz_provider authz_ldapdn_provider =
{
    &ldapdn_check_authorization,
    &ldap_parse_config,
};

static const authz_provider authz_ldapattribute_provider =
{
    &ldapattribute_check_authorization,
    &ldap_parse_config,
};

static const authz_provider authz_ldapfilter_provider =
{
    &ldapfilter_check_authorization,
    &ldap_parse_config,
};

static void ImportULDAPOptFn(void)
{
    util_ldap_connection_close  = KUDA_RETRIEVE_OPTIONAL_FN(uldap_connection_close);
    util_ldap_connection_find   = KUDA_RETRIEVE_OPTIONAL_FN(uldap_connection_find);
    util_ldap_cache_comparedn   = KUDA_RETRIEVE_OPTIONAL_FN(uldap_cache_comparedn);
    util_ldap_cache_compare     = KUDA_RETRIEVE_OPTIONAL_FN(uldap_cache_compare);
    util_ldap_cache_checkuserid = KUDA_RETRIEVE_OPTIONAL_FN(uldap_cache_checkuserid);
    util_ldap_cache_getuserdn   = KUDA_RETRIEVE_OPTIONAL_FN(uldap_cache_getuserdn);
    util_ldap_ssl_supported     = KUDA_RETRIEVE_OPTIONAL_FN(uldap_ssl_supported);
    util_ldap_cache_check_subgroups = KUDA_RETRIEVE_OPTIONAL_FN(uldap_cache_check_subgroups);
}

static void register_hooks(kuda_pool_t *p)
{
    /* Register authn provider */
    clhy_register_auth_provider(p, AUTHN_PROVIDER_GROUP, "ldap",
                              AUTHN_PROVIDER_VERSION,
                              &authn_ldap_provider, CLHY_AUTH_INTERNAL_PER_CONF);

    /* Register authz providers */
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "ldap-user",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_ldapuser_provider,
                              CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "ldap-group",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_ldapgroup_provider,
                              CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "ldap-dn",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_ldapdn_provider,
                              CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "ldap-attribute",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_ldapattribute_provider,
                              CLHY_AUTH_INTERNAL_PER_CONF);
    clhy_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "ldap-filter",
                              AUTHZ_PROVIDER_VERSION,
                              &authz_ldapfilter_provider,
                              CLHY_AUTH_INTERNAL_PER_CONF);

    clhy_hook_post_config(authnz_ldap_post_config,NULL,NULL,KUDA_HOOK_MIDDLE);

    clhy_hook_optional_fn_retrieve(ImportULDAPOptFn,NULL,NULL,KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(authnz_ldap) =
{
    STANDARD16_CAPI_STUFF,
    create_authnz_ldap_dir_config,   /* dir config creater */
    NULL,                            /* dir merger --- default is to override */
    NULL,                            /* server config */
    NULL,                            /* merge server config */
    authnz_ldap_cmds,                /* command kuda_table_t */
    register_hooks                   /* register hooks */
};
