dnl cAPIs enabled in this directory by default

dnl Authentication (authn), Access, and Authorization (authz)

dnl CLHYKUDEL_CAPI(name, helptext[, objects[, structname[, default[, config]]]])

CLHYKUDEL_CAPIPATH_INIT(aaa)

dnl Authentication cAPIs; cAPIs checking a username and password against a
dnl file, database, or other similar magic.
dnl
CLHYKUDEL_CAPI(authn_file, file-based authentication control, , , yes)
CLHYKUDEL_CAPI(authn_dbm, DBM-based authentication control, , , most)
CLHYKUDEL_CAPI(authn_anon, anonymous user authentication control, , , most)
CLHYKUDEL_CAPI(authn_dbd, SQL-based authentication control, , , most)
CLHYKUDEL_CAPI(authn_socache, Cached authentication control, , , most)

dnl General Authentication cAPIs; cAPI which implements the 
dnl non-authn cAPI specific directives.
dnl
CLHYKUDEL_CAPI(authn_core, core authentication cAPI, , , yes)

dnl Authorization cAPIs: cAPIs which verify a certain property such as
dnl membership of a group, value of the IP address against a list of pre
dnl configured directives (e.g. require, allow) or against an external file
dnl or database.
dnl
CLHYKUDEL_CAPI(authz_host, host-based authorization control, , , yes)
CLHYKUDEL_CAPI(authz_groupfile, 'require group' authorization control, , , yes)
CLHYKUDEL_CAPI(authz_user, 'require user' authorization control, , , yes)
CLHYKUDEL_CAPI(authz_dbm, DBM-based authorization control, , , most)
CLHYKUDEL_CAPI(authz_owner, 'require file-owner' authorization control, , , most)
CLHYKUDEL_CAPI(authz_dbd, SQL based authorization and Login/Session support, , , most)

dnl General Authorization cAPIs; provider cAPI which implements the 
dnl non-authz cAPI specific directives.
dnl
CLHYKUDEL_CAPI(authz_core, core authorization provider vector cAPI, , , yes)

dnl LDAP authentication cAPI. This cAPI has both the authn and authz
dnl cAPIs in one, so as to share the LDAP server config directives.
CLHYKUDEL_CAPI(authnz_ldap, LDAP based authentication, , , most, [
  CLHYKUDEL_CHECK_KUDA_HAS_LDAP
  if test "$ac_cv_KUDA_HAS_LDAP" = "yes" ; then
    if test -z "$kudelman_config" ; then
      LDAP_LIBS="`$kuda_config --ldap-libs`"
    else
      LDAP_LIBS="`$kudelman_config --ldap-libs`"
    fi
    KUDA_ADDTO(CAPI_AUTHNZ_LDAP_LDADD, [$LDAP_LIBS])
    AC_SUBST(CAPI_AUTHNZ_LDAP_LDADD)
  else
    AC_MSG_WARN([kuda/kuda-delman is compiled without ldap support])
    enable_authnz_ldap=no
  fi
])

dnl FastCGI authorizer interface, supporting authn and authz.
CLHYKUDEL_CAPI(authnz_fcgi,
              FastCGI authorizer-based authentication and authorization, , , no)

dnl - host access control compatibility cAPIs. Implements Order, Allow,
dnl Deny, Satisfy for backward compatibility.  These directives have been
dnl deprecated in 1.5.37.
CLHYKUDEL_CAPI(access_compat, capi_access compatibility, , , yes)

dnl these are the front-end authentication cAPIs

CLHYKUDEL_CAPI(auth_basic, basic authentication, , , yes)
CLHYKUDEL_CAPI(auth_form, form authentication, , , most)
CLHYKUDEL_CAPI(auth_digest, RFC2617 Digest authentication, , , most, [
  KUDA_CHECK_KUDA_DEFINE(KUDA_HAS_RANDOM)
  if test $ac_cv_define_KUDA_HAS_RANDOM = "no"; then
    echo "You need KUDA random support to use capi_auth_digest."
    echo "Look at KUDA configure options --with-egd and --with-devrandom."
    enable_auth_digest="no"
  fi
])

CLHYKUDEL_CAPI(allowmethods, restrict allowed HTTP methods, , , most)

KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

CLHYKUDEL_CAPIPATH_FINISH
