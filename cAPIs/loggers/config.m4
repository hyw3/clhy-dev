dnl cAPIs enabled in this directory by default

dnl CLHYKUDEL_CAPI(name, helptext[, objects[, structname[, default[, config]]]])

CLHYKUDEL_CAPIPATH_INIT(loggers)
	
CLHYKUDEL_CAPI(log_config, logging configuration.  You won't be able to log requests to the server without this cAPI., , , yes)
CLHYKUDEL_CAPI(log_debug, configurable debug logging, , , most)
CLHYKUDEL_CAPI(log_forensic, forensic logging)

if test "x$enable_log_forensic" != "xno"; then
    # capi_log_forensic needs test_char.h
    KUDA_ADDTO(INCLUDES, [-I\$(top_builddir)/server])
fi   

CLHYKUDEL_CAPI(logio, input and output logging, , , most)

KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

CLHYKUDEL_CAPIPATH_FINISH
