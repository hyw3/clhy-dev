/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * See also clservices/check_forensic.
 * Relate the forensic log to the transfer log by including
 * %{forensic-id}n in the custom log format, for example:
 * CustomLog logs/custom "%h %l %u %t \"%r\" %>s %b %{forensic-id}n"
 *
 * Credit is due to Tina Bird <tbird precision-guesswork.com>, whose
 * idea this cAPI was.
 *
 *   Ben Laurie 29/12/2003
 */

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "kuda_strings.h"
#include "kuda_atomic.h"
#include "http_protocol.h"
#include "test_char.h"
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif

cAPI CLHY_CAPI_DECLARE_DATA log_forensic_capi;

typedef struct fcfg {
    const char *logname;
    kuda_file_t *fd;
} fcfg;

static kuda_uint32_t next_id;

static void *make_forensic_log_scfg(kuda_pool_t *p, server_rec *s)
{
    fcfg *cfg = kuda_pcalloc(p, sizeof *cfg);

    cfg->logname = NULL;
    cfg->fd = NULL;

    return cfg;
}

static void *merge_forensic_log_scfg(kuda_pool_t *p, void *parent, void *new)
{
    fcfg *cfg = kuda_pcalloc(p, sizeof *cfg);
    fcfg *pc = parent;
    fcfg *nc = new;

    cfg->logname = kuda_pstrdup(p, nc->logname ? nc->logname : pc->logname);
    cfg->fd = NULL;

    return cfg;
}

static int open_log(server_rec *s, kuda_pool_t *p)
{
    fcfg *cfg = clhy_get_capi_config(s->capi_config, &log_forensic_capi);

    if (!cfg->logname || cfg->fd)
        return 1;

    if (*cfg->logname == '|') {
        piped_log *pl;
        const char *pname = clhy_server_root_relative(p, cfg->logname + 1);

        pl = clhy_open_piped_log(p, pname);
        if (pl == NULL) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, s, CLHYLOGNO(00650)
                         "couldn't spawn forensic log pipe %s", cfg->logname);
            return 0;
        }
        cfg->fd = clhy_piped_log_write_fd(pl);
    }
    else {
        const char *fname = clhy_server_root_relative(p, cfg->logname);
        kuda_status_t rv;

        if ((rv = kuda_file_open(&cfg->fd, fname,
                                KUDA_WRITE | KUDA_APPEND | KUDA_CREATE,
                                KUDA_PLATFORM_DEFAULT, p)) != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00651)
                         "could not open forensic log file %s.", fname);
            return 0;
        }
    }

    return 1;
}

static int log_init(kuda_pool_t *pc, kuda_pool_t *p, kuda_pool_t *pt,
                     server_rec *s)
{
    for ( ; s ; s = s->next) {
        if (!open_log(s, p)) {
            return HTTP_INTERNAL_SERVER_ERROR;
        }
    }

    return OK;
}


/* e is the first _invalid_ location in q
   N.B. returns the terminating NUL.
 */
static char *log_escape(char *q, const char *e, const char *p)
{
    for ( ; *p ; ++p) {
        clhy_assert(q < e);
        if (test_char_table[*(unsigned char *)p]&T_ESCAPE_FORENSIC) {
            clhy_assert(q+2 < e);
            *q++ = '%';
            clhy_bin2hex(p, 1, q);
            q += 2;
        }
        else
            *q++ = *p;
    }
    clhy_assert(q < e);
    *q = '\0';

    return q;
}

typedef struct hlog {
    char *log;
    char *pos;
    char *end;
    kuda_pool_t *p;
    kuda_size_t count;
} hlog;

static int count_string(const char *p)
{
    int n;

    for (n = 0 ; *p ; ++p, ++n)
        if (test_char_table[*(unsigned char *)p]&T_ESCAPE_FORENSIC)
            n += 2;
    return n;
}

static int count_headers(void *h_, const char *key, const char *value)
{
    hlog *h = h_;

    h->count += count_string(key)+count_string(value)+2;

    return 1;
}

static int log_headers(void *h_, const char *key, const char *value)
{
    hlog *h = h_;

    /* note that we don't have to check h->pos here, coz its been done
       for us by log_escape */
    *h->pos++ = '|';
    h->pos = log_escape(h->pos, h->end, key);
    *h->pos++ = ':';
    h->pos = log_escape(h->pos, h->end, value);

    return 1;
}

static int log_before(request_rec *r)
{
    fcfg *cfg = clhy_get_capi_config(r->server->capi_config,
                                     &log_forensic_capi);
    const char *id;
    hlog h;
    kuda_size_t n;
    kuda_status_t rv;

    if (!cfg->fd || r->prev) {
        return DECLINED;
    }

    if (!(id = kuda_table_get(r->subprocess_env, "UNIQUE_ID"))) {
        /* we make the assumption that we can't go through all the PIDs in
           under 1 second */
        id = kuda_psprintf(r->pool, "%" KUDA_PID_T_FMT ":%lx:%x", getpid(),
                          time(NULL), kuda_atomic_inc32(&next_id));
    }
    clhy_set_capi_config(r->request_config, &log_forensic_capi, (char *)id);

    h.p = r->pool;
    h.count = 0;

    kuda_table_do(count_headers, &h, r->headers_in, NULL);

    h.count += 1+strlen(id)+1+count_string(r->the_request)+1+1;
    h.log = kuda_palloc(r->pool, h.count);
    h.pos = h.log;
    h.end = h.log+h.count;

    *h.pos++ = '+';
    strcpy(h.pos, id);
    h.pos += strlen(h.pos);
    *h.pos++ = '|';
    h.pos = log_escape(h.pos, h.end, r->the_request);

    kuda_table_do(log_headers, &h, r->headers_in, NULL);

    clhy_assert(h.pos < h.end);
    *h.pos++ = '\n';

    n = h.count-1;
    rv = kuda_file_write(cfg->fd, h.log, &n);
    clhy_assert(rv == KUDA_SUCCESS && n == h.count-1);

    kuda_table_setn(r->notes, "forensic-id", id);

    return OK;
}

static int log_after(request_rec *r)
{
    fcfg *cfg = clhy_get_capi_config(r->server->capi_config,
                                     &log_forensic_capi);
    const char *id = clhy_get_capi_config(r->request_config,
                                          &log_forensic_capi);
    char *s;
    kuda_size_t l, n;
    kuda_status_t rv;

    if (!cfg->fd || id == NULL) {
        return DECLINED;
    }

    s = kuda_pstrcat(r->pool, "-", id, "\n", NULL);
    l = n = strlen(s);
    rv = kuda_file_write(cfg->fd, s, &n);
    clhy_assert(rv == KUDA_SUCCESS && n == l);

    return OK;
}

static const char *set_forensic_log(cmd_parms *cmd, void *dummy, const char *fn)
{
    fcfg *cfg = clhy_get_capi_config(cmd->server->capi_config,
                                     &log_forensic_capi);

    cfg->logname = fn;
    return NULL;
}

static const command_rec forensic_log_cmds[] =
{
    CLHY_INIT_TAKE1("ForensicLog",  set_forensic_log,  NULL,  RSRC_CONF,
                  "the filename of the forensic log"),
    { NULL }
};

static void register_hooks(kuda_pool_t *p)
{
    static const char * const pre[] = { "capi_unique_id.c", NULL };

    clhy_hook_open_logs(log_init,NULL,NULL,KUDA_HOOK_MIDDLE);
    clhy_hook_post_read_request(log_before,pre,NULL,KUDA_HOOK_REALLY_FIRST);
    clhy_hook_log_transaction(log_after,NULL,NULL,KUDA_HOOK_REALLY_LAST);
}

CLHY_DECLARE_CAPI(log_forensic) =
{
    STANDARD16_CAPI_STUFF,
    NULL,                       /* create per-dir config */
    NULL,                       /* merge per-dir config */
    make_forensic_log_scfg,     /* server config */
    merge_forensic_log_scfg,    /* merge server config */
    forensic_log_cmds,          /* command kuda_table_t */
    register_hooks              /* register hooks */
};
