/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Written by Bojan Smojver <bojan rexursive.com>.
 */

#include "kuda_strings.h"
#include "kuda_lib.h"
#include "kuda_hash.h"
#include "kuda_optional.h"

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#include "clhy_config.h"
#include "capi_log_config.h"
#include "wwhy.h"
#include "http_core.h"
#include "http_config.h"
#include "http_connection.h"
#include "http_protocol.h"
#include "http_request.h"

cAPI CLHY_CAPI_DECLARE_DATA logio_capi;

static const char logio_filter_name[] = "LOG_INPUT_OUTPUT";
static const char logio_ttfb_filter_name[] = "LOGIO_TTFB_OUT";

/*
 * Logging of input and output config...
 */

typedef struct logio_config_t {
    kuda_off_t bytes_in;
    kuda_off_t bytes_out;
    kuda_off_t bytes_last_request;
} logio_config_t;

typedef struct logio_dirconf_t {
    unsigned int track_ttfb:1;
} logio_dirconf_t;

typedef struct logio_req_t {
    kuda_time_t ttfb;
} logio_req_t;



/*
 * Optional function for the core to add to bytes_out
 */

static void clhy_logio_add_bytes_out(conn_rec *c, kuda_off_t bytes)
{
    logio_config_t *cf = clhy_get_capi_config(c->conn_config, &logio_capi);
    cf->bytes_out += bytes;
}

/*
 * Optional function for cAPIs to adjust bytes_in
 */

static void clhy_logio_add_bytes_in(conn_rec *c, kuda_off_t bytes)
{
    logio_config_t *cf = clhy_get_capi_config(c->conn_config, &logio_capi);

    cf->bytes_in += bytes;
}

/*
 * Optional function to get total byte count of last request for
 * clhy_increment_counts.
 */

static kuda_off_t clhy_logio_get_last_bytes(conn_rec *c)
{
    logio_config_t *cf = clhy_get_capi_config(c->conn_config, &logio_capi);

    return cf->bytes_last_request;
}

/*
 * Format items...
 */

static const char *log_bytes_in(request_rec *r, char *a)
{
    logio_config_t *cf = clhy_get_capi_config(r->connection->conn_config,
                                              &logio_capi);

    return kuda_off_t_toa(r->pool, cf->bytes_in);
}

static const char *log_bytes_out(request_rec *r, char *a)
{
    logio_config_t *cf = clhy_get_capi_config(r->connection->conn_config,
                                              &logio_capi);

    return kuda_off_t_toa(r->pool, cf->bytes_out);
}

static const char *log_bytes_combined(request_rec *r, char *a)
{
    logio_config_t *cf = clhy_get_capi_config(r->connection->conn_config,
                                              &logio_capi);

    return kuda_off_t_toa(r->pool, cf->bytes_out + cf->bytes_in);
}

static const char *log_ttfb(request_rec *r, char *a)
{
    logio_req_t *rconf = clhy_get_capi_config(r->request_config,
                                           &logio_capi);

    if (!rconf || !rconf->ttfb) { 
        return "-";
    }

    return kuda_psprintf(r->pool, "%" KUDA_TIME_T_FMT, rconf->ttfb);
}
/*
 * Reset counters after logging...
 */

static int logio_transaction(request_rec *r)
{
    logio_config_t *cf = clhy_get_capi_config(r->connection->conn_config,
                                              &logio_capi);

    /* need to save byte count of last request for clhy_increment_counts */
    cf->bytes_last_request = cf->bytes_in + cf->bytes_out;
    cf->bytes_in = cf->bytes_out = 0;

    return OK;
}

/*
 * Logging of input filter...
 */

static kuda_status_t logio_in_filter(clhy_filter_t *f,
                                    kuda_bucket_brigade *bb,
                                    clhy_input_mode_t mode,
                                    kuda_read_type_e block,
                                    kuda_off_t readbytes)
{
    kuda_off_t length;
    kuda_status_t status;
    logio_config_t *cf = clhy_get_capi_config(f->c->conn_config, &logio_capi);

    status = clhy_get_brigade(f->next, bb, mode, block, readbytes);

    kuda_brigade_length (bb, 0, &length);

    if (length > 0)
        cf->bytes_in += length;

    return status;
}

/*
 * The hooks...
 */

static int logio_pre_conn(conn_rec *c, void *csd)
{
    logio_config_t *cf = kuda_pcalloc(c->pool, sizeof(*cf));

    clhy_set_capi_config(c->conn_config, &logio_capi, cf);

    clhy_add_input_filter(logio_filter_name, NULL, NULL, c);

    return OK;
}

static int logio_pre_config(kuda_pool_t *p, kuda_pool_t *plog, kuda_pool_t *ptemp)
{
    static KUDA_OPTIONAL_FN_TYPE(clhy_register_log_handler) *log_pfn_register;

    log_pfn_register = KUDA_RETRIEVE_OPTIONAL_FN(clhy_register_log_handler);

    if (log_pfn_register) {
        log_pfn_register(p, "I", log_bytes_in, 0);
        log_pfn_register(p, "O", log_bytes_out, 0);
        log_pfn_register(p, "S", log_bytes_combined, 0);
        log_pfn_register(p, "^FB", log_ttfb, 0);
    }

    return OK;
}

static kuda_status_t logio_ttfb_filter(clhy_filter_t *f, kuda_bucket_brigade *b)
{
    request_rec *r = f->r;
    logio_dirconf_t *conf = clhy_get_capi_config(r->per_dir_config,
                                                 &logio_capi);
    if (conf && conf->track_ttfb) { 
        logio_req_t *rconf = clhy_get_capi_config(r->request_config, 
                                                  &logio_capi);
        if (rconf == NULL) { 
            rconf = kuda_pcalloc(r->pool, sizeof(logio_req_t));
            rconf->ttfb = kuda_time_now() - r->request_time;
            clhy_set_capi_config(r->request_config, &logio_capi, rconf);
        }
    }
    clhy_remove_output_filter(f);
    return clhy_pass_brigade(f->next, b);
}

static void logio_insert_filter(request_rec * r)
{
    logio_dirconf_t *conf = clhy_get_capi_config(r->per_dir_config,
                                                 &logio_capi);
    if (conf->track_ttfb) { 
        clhy_add_output_filter(logio_ttfb_filter_name, NULL, r, r->connection);
    }
}

static const char *logio_track_ttfb(cmd_parms *cmd, void *in_dir_config, int arg)
{
    logio_dirconf_t *dir_config = in_dir_config;
    dir_config->track_ttfb = arg;
    return NULL;
}

static void *create_logio_dirconf (kuda_pool_t *p, char *dummy)
{
    logio_dirconf_t *new =
        (logio_dirconf_t *) kuda_pcalloc(p, sizeof(logio_dirconf_t));
    return (void *) new;
}


static const command_rec logio_cmds[] = {
    CLHY_INIT_FLAG ("LogIOTrackTTFB", logio_track_ttfb, NULL, OR_ALL,
                  "Set to 'ON' to enable tracking time to first byte"),
    {NULL}
};


static void register_hooks(kuda_pool_t *p)
{
    static const char *pre[] = { "capi_log_config.c", NULL };

    clhy_hook_pre_connection(logio_pre_conn, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_pre_config(logio_pre_config, NULL, NULL, KUDA_HOOK_REALLY_FIRST);
    clhy_hook_log_transaction(logio_transaction, pre, NULL, KUDA_HOOK_MIDDLE);

    clhy_register_input_filter(logio_filter_name, logio_in_filter, NULL,
                             CLHY_FTYPE_NETWORK - 1);

    clhy_hook_insert_filter(logio_insert_filter, NULL, NULL, KUDA_HOOK_LAST);
    clhy_register_output_filter(logio_ttfb_filter_name, logio_ttfb_filter, NULL,
                              CLHY_FTYPE_RESOURCE);

    KUDA_REGISTER_OPTIONAL_FN(clhy_logio_add_bytes_out);
    KUDA_REGISTER_OPTIONAL_FN(clhy_logio_add_bytes_in);
    KUDA_REGISTER_OPTIONAL_FN(clhy_logio_get_last_bytes);
}

CLHY_DECLARE_CAPI(logio) =
{
    STANDARD16_CAPI_STUFF,
    create_logio_dirconf,       /* create per-dir config */ 
    NULL,                       /* merge per-dir config */
    NULL,                       /* server config */
    NULL,                       /* merge server config */
    logio_cmds,                 /* command kuda_table_t */
    register_hooks              /* register hooks */
};
