/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Modified by djm@va.pubnix.com:
 * If no TransferLog is given explicitly, decline to log.
 *
 * This is cAPI implements the TransferLog directive (same as the
 * common log cAPI), and additional directives, LogFormat and CustomLog.
 *
 *
 * Syntax:
 *
 *    TransferLog fn      Logs transfers to fn in standard log format, unless
 *                        a custom format is set with LogFormat
 *    LogFormat format    Set a log format from TransferLog files
 *    CustomLog fn format
 *                        Log to file fn with format given by the format
 *                        argument
 *
 * There can be any number of TransferLog and CustomLog
 * commands. Each request will be logged to _ALL_ the
 * named files, in the appropriate format.
 *
 * If no TransferLog or CustomLog directive appears in a VirtualHost,
 * the request will be logged to the log file(s) defined outside
 * the virtual host section. If a TransferLog or CustomLog directive
 * appears in the VirtualHost section, the log files defined outside
 * the VirtualHost will _not_ be used. This makes this cAPI compatible
 * with the CLF and config log cAPIs, where the use of TransferLog
 * inside the VirtualHost section overrides its use outside.
 *
 * Examples:
 *
 *    TransferLog    logs/access_log
 *    <VirtualHost>
 *    LogFormat      "... custom format ..."
 *    TransferLog    log/virtual_only
 *    CustomLog      log/virtual_useragents "%t %{user-agent}i"
 *    </VirtualHost>
 *
 * This will log using CLF to access_log any requests handled by the
 * main server, while any requests to the virtual host will be logged
 * with the "... custom format..." to virtual_only _AND_ using
 * the custom user-agent log to virtual_useragents.
 *
 * Note that the NCSA referer and user-agent logs are easily added with
 * CustomLog:
 *   CustomLog   logs/referer  "%{referer}i -> %U"
 *   CustomLog   logs/agent    "%{user-agent}i"
 *
 * RefererIgnore functionality can be obtained with conditional
 * logging (SetEnvIf and CustomLog ... env=!VAR).
 *
 * But using this method allows much easier modification of the
 * log format, e.g. to log hosts along with UA:
 *   CustomLog   logs/referer "%{referer}i %U %h"
 *
 * The argument to LogFormat and CustomLog is a string, which can include
 * literal characters copied into the log files, and '%' directives as
 * follows:
 *
 * %...B:  bytes sent, excluding HTTP headers.
 * %...b:  bytes sent, excluding HTTP headers in CLF format, i.e. a '-'
 *         when no bytes where sent (rather than a '0'.
 * %...{FOOBAR}C:  The contents of the HTTP cookie FOOBAR
 * %...{FOOBAR}e:  The contents of the environment variable FOOBAR
 * %...f:  filename
 * %...h:  remote host
 * %...a:  remote IP-address
 * %...A:  local IP-address
 * %...{Foobar}i:  The contents of Foobar: header line(s) in the request
 *                 sent to the client.
 * %...k:  number of keepalive requests served over this connection
 * %...l:  remote logname (from identd, if supplied)
 * %...{Foobar}n:  The contents of note "Foobar" from another cAPI.
 * %...{Foobar}o:  The contents of Foobar: header line(s) in the reply.
 * %...p:  the canonical port for the server
 * %...{format}p: the canonical port for the server, or the actual local
 *                or remote port
 * %...P:  the process ID of the child that serviced the request.
 * %...{format}P: the process ID or thread ID of the child/thread that
 *                serviced the request
 * %...r:  first line of request
 * %...s:  status.  For requests that got internally redirected, this
 *         is status of the *original* request --- %...>s for the last.
 * %...t:  time, in common log format time format
 * %...{format}t:  The time, in the form given by format, which should
 *                 be in strftime(3) format.
 * %...T:  the time taken to serve the request, in seconds.
 * %...{s}T:  the time taken to serve the request, in seconds, same as %T.
 * %...{us}T:  the time taken to serve the request, in micro seconds, same as %D.
 * %...{ms}T:  the time taken to serve the request, in milliseconds.
 * %...D:  the time taken to serve the request, in micro seconds.
 * %...u:  remote user (from auth; may be bogus if return status (%s) is 401)
 * %...U:  the URL path requested.
 * %...v:  the configured name of the server (i.e. which virtual host?)
 * %...V:  the server name according to the UseCanonicalName setting
 * %...m:  the request method
 * %...H:  the request protocol
 * %...q:  the query string prepended by "?", or empty if no query string
 * %...X:  Status of the connection.
 *         'X' = connection aborted before the response completed.
 *         '+' = connection may be kept alive after the response is sent.
 *         '-' = connection will be closed after the response is sent.
 *         (This directive was %...c in late versions of cLHy 1.3, but
 *          this conflicted with the historical ssl %...{var}c syntax.)
 * %...L:  Log-Id of the Request (or '-' if none)
 * %...{c}L:  Log-Id of the Connection (or '-' if none)
 *
 * The '...' can be nothing at all (e.g. "%h %u %r %s %b"), or it can
 * indicate conditions for inclusion of the item (which will cause it
 * to be replaced with '-' if the condition is not met).  Note that
 * there is no escaping performed on the strings from %r, %...i and
 * %...o; some with long memories may remember that I thought this was
 * a bad idea, once upon a time, and I'm still not comfortable with
 * it, but it is difficult to see how to "do the right thing" with all
 * of '%..i', unless we URL-escape everything and break with CLF.
 *
 * The forms of condition are a list of HTTP status codes, which may
 * or may not be preceded by '!'.  Thus, '%400,501{User-agent}i' logs
 * User-agent: on 400 errors and 501 errors (Bad Request, Not
 * Implemented) only; '%!200,304,302{Referer}i' logs Referer: on all
 * requests which did *not* return some sort of normal status.
 *
 * The default LogFormat reproduces CLF; see below.
 *
 * The way this is supposed to work with virtual hosts is as follows:
 * a virtual host can have its own LogFormat, or its own TransferLog.
 * If it doesn't have its own LogFormat, it inherits from the main
 * server.  If it doesn't have its own TransferLog, it writes to the
 * same descriptor (meaning the same process for "| ...").
 *
 * --- rst */

#include "kuda_strings.h"
#include "kuda_lib.h"
#include "kuda_hash.h"
#include "kuda_optional.h"
#include "kuda_anylock.h"

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#include "clhy_config.h"
#include "capi_log_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"          /* For REMOTE_NAME */
#include "http_log.h"
#include "http_protocol.h"
#include "util_time.h"
#include "clhy_core.h"

#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif

#define DEFAULT_LOG_FORMAT "%h %l %u %t \"%r\" %>s %b"

cAPI CLHY_CAPI_DECLARE_DATA log_config_capi;


static int xfer_flags = (KUDA_WRITE | KUDA_APPEND | KUDA_CREATE | KUDA_LARGEFILE);
static kuda_fileperms_t xfer_perms = KUDA_PLATFORM_DEFAULT;
static kuda_hash_t *log_hash;
static kuda_status_t clhy_default_log_writer(request_rec *r,
                           void *handle,
                           const char **strs,
                           int *strl,
                           int nelts,
                           kuda_size_t len);
static kuda_status_t clhy_buffered_log_writer(request_rec *r,
                           void *handle,
                           const char **strs,
                           int *strl,
                           int nelts,
                           kuda_size_t len);
static void *clhy_default_log_writer_init(kuda_pool_t *p, server_rec *s,
                                        const char* name);
static void *clhy_buffered_log_writer_init(kuda_pool_t *p, server_rec *s,
                                        const char* name);

static clhy_log_writer_init *clhy_log_set_writer_init(clhy_log_writer_init *handle);
static clhy_log_writer *clhy_log_set_writer(clhy_log_writer *handle);
static clhy_log_writer *log_writer = clhy_default_log_writer;
static clhy_log_writer_init *log_writer_init = clhy_default_log_writer_init;
static int buffered_logs = 0; /* default unbuffered */
static kuda_array_header_t *all_buffered_logs = NULL;

/* POSIX.1 defines PIPE_BUF as the maximum number of bytes that is
 * guaranteed to be atomic when writing a pipe.  And PIPE_BUF >= 512
 * is guaranteed.  So we'll just guess 512 in the event the system
 * doesn't have this.  Now, for file writes there is actually no limit,
 * the entire write is atomic.  Whether all systems implement this
 * correctly is another question entirely ... so we'll just use PIPE_BUF
 * because it's probably a good guess as to what is implemented correctly
 * everywhere.
 */
#ifdef PIPE_BUF
#define LOG_BUFSIZE     PIPE_BUF
#else
#define LOG_BUFSIZE     (512)
#endif

/*
 * multi_log_state is our per-(virtual)-server configuration. We store
 * an array of the logs we are going to use, each of type config_log_state.
 * If a default log format is given by LogFormat, store in default_format
 * (backward compat. with capi_log_config).  We also store for each virtual
 * server a pointer to the logs specified for the main server, so that if this
 * vhost has no logs defined, we can use the main server's logs instead.
 *
 * So, for the main server, config_logs contains a list of the log files
 * and server_config_logs is empty. For a vhost, server_config_logs
 * points to the same array as config_logs in the main server, and
 * config_logs points to the array of logs defined inside this vhost,
 * which might be empty.
 */

typedef struct {
    const char *default_format_string;
    kuda_array_header_t *default_format;
    kuda_array_header_t *config_logs;
    kuda_array_header_t *server_config_logs;
    kuda_table_t *formats;
} multi_log_state;

/*
 * config_log_state holds the status of a single log file. fname might
 * be NULL, which means this cAPI does no logging for this
 * request. format might be NULL, in which case the default_format
 * from the multi_log_state should be used, or if that is NULL as
 * well, use the CLF.
 * log_writer is NULL before the log file is opened and is
 * set to a opaque structure (usually a fd) after it is opened.

 */
typedef struct {
    kuda_file_t *handle;
    kuda_size_t outcnt;
    char outbuf[LOG_BUFSIZE];
    kuda_anylock_t mutex;
} buffered_log;

typedef struct {
    const char *fname;
    const char *format_string;
    kuda_array_header_t *format;
    void *log_writer;
    char *condition_var;
    int inherit;
    clhy_expr_info_t *condition_expr;
    /** place of definition or NULL if already checked */
    const clhy_directive_t *directive;
} config_log_state;

/*
 * log_request_state holds request specific log data that is not
 * part of the request_rec.
 */
typedef struct {
    kuda_time_t request_end_time;
} log_request_state;

/*
 * Format items...
 * Note that many of these could have clhy_sprintfs replaced with static buffers.
 */

typedef struct {
    clhy_log_handler_fn_t *func;
    char *arg;
    int condition_sense;
    int want_orig;
    kuda_array_header_t *conditions;
} log_format_item;

static char *pfmt(kuda_pool_t *p, int i)
{
    if (i <= 0) {
        return "-";
    }
    else {
        return kuda_itoa(p, i);
    }
}

static const char *constant_item(request_rec *dummy, char *stuff)
{
    return stuff;
}

static const char *log_remote_host(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, clhy_get_remote_host(r->connection,
                                                         r->per_dir_config,
                                                         REMOTE_NAME, NULL));
}

static const char *log_remote_address(request_rec *r, char *a)
{
    if (a && !strcmp(a, "c")) {
        return r->connection->client_ip;
    }
    else {
        return r->useragent_ip;
    }
}

static const char *log_local_address(request_rec *r, char *a)
{
    return r->connection->local_ip;
}

static const char *log_remote_logname(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, clhy_get_remote_logname(r));
}

static const char *log_remote_user(request_rec *r, char *a)
{
    char *rvalue = r->user;

    if (rvalue == NULL) {
        rvalue = "-";
    }
    else if (strlen(rvalue) == 0) {
        rvalue = "\"\"";
    }
    else {
        rvalue = clhy_escape_logitem(r->pool, rvalue);
    }

    return rvalue;
}

static const char *log_request_line(request_rec *r, char *a)
{
    /* NOTE: If the original request contained a password, we
     * re-write the request line here to contain XXXXXX instead:
     * (note the truncation before the protocol string for HTTP/0.9 requests)
     * (note also that r->the_request contains the unmodified request)
     */
    return clhy_escape_logitem(r->pool,
                             (r->parsed_uri.password)
                               ? kuda_pstrcat(r->pool, r->method, " ",
                                             kuda_uri_unparse(r->pool,
                                                             &r->parsed_uri, 0),
                                             r->assbackwards ? NULL : " ",
                                             r->protocol, NULL)
                               : r->the_request);
}

static const char *log_request_file(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, r->filename);
}
static const char *log_request_uri(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, r->uri);
}
static const char *log_request_method(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, r->method);
}
static const char *log_log_id(request_rec *r, char *a)
{
    if (a && !strcmp(a, "c")) {
        return r->connection->log_id ? r->connection->log_id : "-";
    }
    else {
        return r->log_id ? r->log_id : "-";
    }
}
static const char *log_request_protocol(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, r->protocol);
}
static const char *log_request_query(request_rec *r, char *a)
{
    return (r->args) ? kuda_pstrcat(r->pool, "?",
                                   clhy_escape_logitem(r->pool, r->args), NULL)
                     : "";
}
static const char *log_status(request_rec *r, char *a)
{
    return pfmt(r->pool, r->status);
}

static const char *log_handler(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, r->handler);
}

static const char *clf_log_bytes_sent(request_rec *r, char *a)
{
    if (!r->sent_bodyct || !r->bytes_sent) {
        return "-";
    }
    else {
        return kuda_off_t_toa(r->pool, r->bytes_sent);
    }
}

static const char *log_bytes_sent(request_rec *r, char *a)
{
    if (!r->sent_bodyct || !r->bytes_sent) {
        return "0";
    }
    else {
        return kuda_off_t_toa(r->pool, r->bytes_sent);
    }
}


static const char *log_header_in(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, kuda_table_get(r->headers_in, a));
}

static const char *log_trailer_in(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, kuda_table_get(r->trailers_in, a));
}


static KUDA_INLINE char *find_multiple_headers(kuda_pool_t *pool,
                                              const kuda_table_t *table,
                                              const char *key)
{
    const kuda_array_header_t *elts;
    const kuda_table_entry_t *t_elt;
    const kuda_table_entry_t *t_end;
    kuda_size_t len;
    struct sle {
        struct sle *next;
        const char *value;
        kuda_size_t len;
    } *result_list, *rp;

    elts = kuda_table_elts(table);

    if (!elts->nelts) {
        return NULL;
    }

    t_elt = (const kuda_table_entry_t *)elts->elts;
    t_end = t_elt + elts->nelts;
    len = 1; /* \0 */
    result_list = rp = NULL;

    do {
        if (!strcasecmp(t_elt->key, key)) {
            if (!result_list) {
                result_list = rp = kuda_palloc(pool, sizeof(*rp));
            }
            else {
                rp = rp->next = kuda_palloc(pool, sizeof(*rp));
                len += 2; /* ", " */
            }

            rp->next = NULL;
            rp->value = t_elt->val;
            rp->len = strlen(rp->value);

            len += rp->len;
        }
        ++t_elt;
    } while (t_elt < t_end);

    if (result_list) {
        char *result = kuda_palloc(pool, len);
        char *cp = result;

        rp = result_list;
        while (rp) {
            if (rp != result_list) {
                *cp++ = ',';
                *cp++ = ' ';
            }
            memcpy(cp, rp->value, rp->len);
            cp += rp->len;
            rp = rp->next;
        }
        *cp = '\0';

        return result;
    }

    return NULL;
}

static const char *log_header_out(request_rec *r, char *a)
{
    const char *cp = NULL;

    if (!strcasecmp(a, "Content-type") && r->content_type) {
        cp = clhy_field_noparam(r->pool, r->content_type);
    }
    else if (!strcasecmp(a, "Set-Cookie")) {
        cp = find_multiple_headers(r->pool, r->headers_out, a);
    }
    else {
        cp = kuda_table_get(r->headers_out, a);
    }

    return clhy_escape_logitem(r->pool, cp);
}

static const char *log_trailer_out(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, kuda_table_get(r->trailers_out, a));
}

static const char *log_note(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, kuda_table_get(r->notes, a));
}
static const char *log_env_var(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, kuda_table_get(r->subprocess_env, a));
}

static const char *log_cookie(request_rec *r, char *a)
{
    const char *cookies_entry;

    /*
     * This supports Netscape version 0 cookies while being tolerant to
     * some properties of RFC2109/2965 version 1 cookies:
     * - case-insensitive match of cookie names
     * - white space between the tokens
     * It does not support the following version 1 features:
     * - quoted strings as cookie values
     * - commas to separate cookies
     */

    if ((cookies_entry = kuda_table_get(r->headers_in, "Cookie"))) {
        char *cookie, *last1, *last2;
        char *cookies = kuda_pstrdup(r->pool, cookies_entry);

        while ((cookie = kuda_strtok(cookies, ";", &last1))) {
            char *name = kuda_strtok(cookie, "=", &last2);
            /* last2 points to the next char following an '=' delim,
               or the trailing NUL char of the string */
            char *value = last2;
            if (name && *name &&  value && *value) {
                char *last = value - 2;
                /* Move past leading WS */
                name += strspn(name, " \t");
                while (last >= name && kuda_isspace(*last)) {
                    *last = '\0';
                    --last;
                }

                if (!strcasecmp(name, a)) {
                    /* last1 points to the next char following the ';' delim,
                       or the trailing NUL char of the string */
                    last = last1 - (*last1 ? 2 : 1);
                    /* Move past leading WS */
                    value += strspn(value, " \t");
                    while (last >= value && kuda_isspace(*last)) {
                       *last = '\0';
                       --last;
                    }

                    return clhy_escape_logitem(r->pool, value);
                }
            }
            /* Iterate the remaining tokens using kuda_strtok(NULL, ...) */
            cookies = NULL;
        }
    }
    return NULL;
}

static const char *log_request_time_custom(request_rec *r, char *a,
                                           kuda_time_exp_t *xt)
{
    kuda_size_t retcode;
    char tstr[MAX_STRING_LEN];
    kuda_strftime(tstr, &retcode, sizeof(tstr), a, xt);
    return kuda_pstrdup(r->pool, tstr);
}

#define DEFAULT_REQUEST_TIME_SIZE 32
typedef struct {
    unsigned t;
    char timestr[DEFAULT_REQUEST_TIME_SIZE];
    unsigned t_validate;
} cached_request_time;

#define TIME_FMT_CUSTOM          0
#define TIME_FMT_CLF             1
#define TIME_FMT_ABS_SEC         2
#define TIME_FMT_ABS_MSEC        3
#define TIME_FMT_ABS_USEC        4
#define TIME_FMT_ABS_MSEC_FRAC   5
#define TIME_FMT_ABS_USEC_FRAC   6

#define TIME_CACHE_SIZE 4
#define TIME_CACHE_MASK 3
static cached_request_time request_time_cache[TIME_CACHE_SIZE];

static kuda_time_t get_request_end_time(request_rec *r)
{
    log_request_state *state = (log_request_state *)clhy_get_capi_config(r->request_config,
                                                                         &log_config_capi);
    if (!state) {
        state = kuda_pcalloc(r->pool, sizeof(log_request_state));
        clhy_set_capi_config(r->request_config, &log_config_capi, state);
    }
    if (state->request_end_time == 0) {
        state->request_end_time = kuda_time_now();
    }
    return state->request_end_time;
}


static const char *log_request_time(request_rec *r, char *a)
{
    kuda_time_exp_t xt;
    kuda_time_t request_time = r->request_time;
    int fmt_type = TIME_FMT_CUSTOM;
    char *fmt = a;

    if (fmt && *fmt) {
        if (!strncmp(fmt, "begin", 5)) {
            fmt += 5;
            if (!*fmt) {
                fmt_type = TIME_FMT_CLF;
            }
            else if (*fmt == ':') {
                fmt++;
                a = fmt;
            }
        }
        else if (!strncmp(fmt, "end", 3)) {
            fmt += 3;
            if (!*fmt) {
                request_time = get_request_end_time(r);
                fmt_type = TIME_FMT_CLF;
            }
            else if (*fmt == ':') {
                fmt++;
                a = fmt;
                request_time = get_request_end_time(r);
            }
        }
        if (!strncmp(fmt, "msec", 4)) {
            fmt += 4;
            if (!*fmt) {
                fmt_type = TIME_FMT_ABS_MSEC;
            }
            else if (!strcmp(fmt, "_frac")) {
                fmt_type = TIME_FMT_ABS_MSEC_FRAC;
            }
        }
        else if (!strncmp(fmt, "usec", 4)) {
            fmt += 4;
            if (!*fmt) {
                fmt_type = TIME_FMT_ABS_USEC;
            }
            else if (!strcmp(fmt, "_frac")) {
                fmt_type = TIME_FMT_ABS_USEC_FRAC;
            }
        }
        else if (!strcmp(fmt, "sec")) {
            fmt_type = TIME_FMT_ABS_SEC;
        }
        else if (!*fmt) {
            fmt_type = TIME_FMT_CLF;
        }
    }
    else {
        fmt_type = TIME_FMT_CLF;
    }

    if (fmt_type >= TIME_FMT_ABS_SEC) {      /* Absolute (micro-/milli-)second time
                                              * or msec/usec fraction
                                              */
        char* buf = kuda_palloc(r->pool, 20);
        switch (fmt_type) {
        case TIME_FMT_ABS_SEC:
            kuda_snprintf(buf, 20, "%" KUDA_TIME_T_FMT, kuda_time_sec(request_time));
            break;
        case TIME_FMT_ABS_MSEC:
            kuda_snprintf(buf, 20, "%" KUDA_TIME_T_FMT, kuda_time_as_msec(request_time));
            break;
        case TIME_FMT_ABS_USEC:
            kuda_snprintf(buf, 20, "%" KUDA_TIME_T_FMT, request_time);
            break;
        case TIME_FMT_ABS_MSEC_FRAC:
            kuda_snprintf(buf, 20, "%03" KUDA_TIME_T_FMT, kuda_time_msec(request_time));
            break;
        case TIME_FMT_ABS_USEC_FRAC:
            kuda_snprintf(buf, 20, "%06" KUDA_TIME_T_FMT, kuda_time_usec(request_time));
            break;
        default:
            return "-";
        }
        return buf;
    }
    else if (fmt_type == TIME_FMT_CUSTOM) {  /* Custom format */
        /* The custom time formatting uses a very large temp buffer
         * on the stack.  To avoid using so much stack space in the
         * common case where we're not using a custom format, the code
         * for the custom format in a separate function.  (That's why
         * log_request_time_custom is not inlined right here.)
         */
        clhy_explode_recent_localtime(&xt, request_time);
        return log_request_time_custom(r, a, &xt);
    }
    else {                                   /* CLF format */
        /* This code uses the same technique as clhy_explode_recent_localtime():
         * optimistic caching with logic to detect and correct race conditions.
         * See the comments in server/util_time.c for more information.
         */
        cached_request_time* cached_time = kuda_palloc(r->pool,
                                                      sizeof(*cached_time));
        unsigned t_seconds = (unsigned)kuda_time_sec(request_time);
        unsigned i = t_seconds & TIME_CACHE_MASK;
        *cached_time = request_time_cache[i];
        if ((t_seconds != cached_time->t) ||
            (t_seconds != cached_time->t_validate)) {

            /* Invalid or old snapshot, so compute the proper time string
             * and store it in the cache
             */
            char sign;
            int timz;

            clhy_explode_recent_localtime(&xt, request_time);
            timz = xt.tm_gmtoff;
            if (timz < 0) {
                timz = -timz;
                sign = '-';
            }
            else {
                sign = '+';
            }
            cached_time->t = t_seconds;
            kuda_snprintf(cached_time->timestr, DEFAULT_REQUEST_TIME_SIZE,
                         "[%02d/%s/%d:%02d:%02d:%02d %c%.2d%.2d]",
                         xt.tm_mday, kuda_month_snames[xt.tm_mon],
                         xt.tm_year+1900, xt.tm_hour, xt.tm_min, xt.tm_sec,
                         sign, timz / (60*60), (timz % (60*60)) / 60);
            cached_time->t_validate = t_seconds;
            request_time_cache[i] = *cached_time;
        }
        return cached_time->timestr;
    }
}

static const char *log_request_duration_microseconds(request_rec *r, char *a)
{    
    return kuda_psprintf(r->pool, "%" KUDA_TIME_T_FMT,
                        (get_request_end_time(r) - r->request_time));
}

static const char *log_request_duration_scaled(request_rec *r, char *a)
{
    kuda_time_t duration = get_request_end_time(r) - r->request_time;
    if (*a == '\0' || !strcasecmp(a, "s")) {
        duration = kuda_time_sec(duration);
    }
    else if (!strcasecmp(a, "ms")) {
        duration = kuda_time_as_msec(duration);
    }
    else if (!strcasecmp(a, "us")) {
    }
    else {
        /* bogus format */
        return a;
    }
    return kuda_psprintf(r->pool, "%" KUDA_TIME_T_FMT, duration);
}

/* These next two routines use the canonical name:port so that log
 * parsers don't need to duplicate all the vhost parsing crud.
 */
static const char *log_virtual_host(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, r->server->server_hostname);
}

static const char *log_server_port(request_rec *r, char *a)
{
    kuda_port_t port;

    if (*a == '\0' || !strcasecmp(a, "canonical")) {
        port = r->server->port ? r->server->port : clhy_default_port(r);
    }
    else if (!strcasecmp(a, "remote")) {
        port = r->useragent_addr->port;
    }
    else if (!strcasecmp(a, "local")) {
        port = r->connection->local_addr->port;
    }
    else {
        /* bogus format */
        return a;
    }
    return kuda_itoa(r->pool, (int)port);
}

/* This respects the setting of UseCanonicalName so that
 * the dynamic mass virtual hosting trick works better.
 */
static const char *log_server_name(request_rec *r, char *a)
{
    return clhy_escape_logitem(r->pool, clhy_get_server_name(r));
}

static const char *log_pid_tid(request_rec *r, char *a)
{
    if (*a == '\0' || !strcasecmp(a, "pid")) {
        return clhy_append_pid(r->pool, "", "");
    }
    else if (!strcasecmp(a, "tid") || !strcasecmp(a, "hextid")) {
#if KUDA_HAS_THREADS
        kuda_platform_thread_t tid = kuda_platform_thread_current();
#else
        int tid = 0; /* KUDA will format "0" anyway but an arg is needed */
#endif
        return kuda_psprintf(r->pool,
#if KUDA_MAJOR_VERSION > 1 || (KUDA_MAJOR_VERSION == 1 && KUDA_MINOR_VERSION >= 2)
                            /* KUDA can format a thread id in hex */
                            *a == 'h' ? "%pt" : "%pT",
#else
                            /* KUDA is missing the feature, so always use decimal */
                            "%pT",
#endif
                            &tid);
    }
    /* bogus format */
    return a;
}

static const char *log_connection_status(request_rec *r, char *a)
{
    if (r->connection->aborted)
        return "X";

    if (r->connection->keepalive == CLHY_CONN_KEEPALIVE &&
        (!r->server->keep_alive_max ||
         (r->server->keep_alive_max - r->connection->keepalives) > 0)) {
        return "+";
    }
    return "-";
}

static const char *log_requests_on_connection(request_rec *r, char *a)
{
    int num = r->connection->keepalives ? r->connection->keepalives - 1 : 0;
    return kuda_itoa(r->pool, num);
}

/*****************************************************************
 *
 * Parsing the log format string
 */

static char *parse_log_misc_string(kuda_pool_t *p, log_format_item *it,
                                   const char **sa)
{
    const char *s;
    char *d;

    it->func = constant_item;
    it->conditions = NULL;

    s = *sa;
    while (*s && *s != '%') {
        s++;
    }
    /*
     * This might allocate a few chars extra if there's a backslash
     * escape in the format string.
     */
    it->arg = kuda_palloc(p, s - *sa + 1);

    d = it->arg;
    s = *sa;
    while (*s && *s != '%') {
        if (*s != '\\') {
            *d++ = *s++;
        }
        else {
            s++;
            switch (*s) {
            case '\\':
                *d++ = '\\';
                s++;
                break;
            case 'r':
                *d++ = '\r';
                s++;
                break;
            case 'n':
                *d++ = '\n';
                s++;
                break;
            case 't':
                *d++ = '\t';
                s++;
                break;
            default:
                /* copy verbatim */
                *d++ = '\\';
                /*
                 * Allow the loop to deal with this *s in the normal
                 * fashion so that it handles end of string etc.
                 * properly.
                 */
                break;
            }
        }
    }
    *d = '\0';

    *sa = s;
    return NULL;
}

static char *parse_log_item(kuda_pool_t *p, log_format_item *it, const char **sa)
{
    const char *s = *sa;
    clhy_log_handler *handler = NULL;

    if (*s != '%') {
        return parse_log_misc_string(p, it, sa);
    }

    ++s;
    it->condition_sense = 0;
    it->conditions = NULL;

    if (*s == '%') {
        it->arg = "%";
        it->func = constant_item;
        *sa = ++s;

        return NULL;
    }

    it->want_orig = -1;
    it->arg = "";               /* For safety's sake... */

    while (*s) {
        int i;

        switch (*s) {
        case '!':
            ++s;
            it->condition_sense = !it->condition_sense;
            break;

        case '<':
            ++s;
            it->want_orig = 1;
            break;

        case '>':
            ++s;
            it->want_orig = 0;
            break;

        case ',':
            ++s;
            break;

        case '{':
            ++s;
            it->arg = clhy_getword(p, &s, '}');
            break;

        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            i = *s - '0';
            while (kuda_isdigit(*++s)) {
                i = i * 10 + (*s) - '0';
            }
            if (!it->conditions) {
                it->conditions = kuda_array_make(p, 4, sizeof(int));
            }
            *(int *) kuda_array_push(it->conditions) = i;
            break;

        default:
            /* check for '^' + two character format first */
            if (*s == '^' && *(s+1) && *(s+2)) { 
                handler = (clhy_log_handler *)kuda_hash_get(log_hash, s, 3); 
                if (handler) { 
                   s += 3;
                }
            }
            if (!handler) {  
                handler = (clhy_log_handler *)kuda_hash_get(log_hash, s++, 1);  
            }
            if (!handler) {
                char dummy[2];

                dummy[0] = s[-1];
                dummy[1] = '\0';
                return kuda_pstrcat(p, "Unrecognized LogFormat directive %",
                               dummy, NULL);
            }
            it->func = handler->func;
            if (it->want_orig == -1) {
                it->want_orig = handler->want_orig_default;
            }
            *sa = s;
            return NULL;
        }
    }

    return "Ran off end of LogFormat parsing args to some directive";
}

static kuda_array_header_t *parse_log_string(kuda_pool_t *p, const char *s, const char **err)
{
    kuda_array_header_t *a = kuda_array_make(p, 30, sizeof(log_format_item));
    char *res;

    while (*s) {
        if ((res = parse_log_item(p, (log_format_item *) kuda_array_push(a), &s))) {
            *err = res;
            return NULL;
        }
    }

    s = KUDA_EOL_STR;
    parse_log_item(p, (log_format_item *) kuda_array_push(a), &s);
    return a;
}

/*****************************************************************
 *
 * Actually logging.
 */

static const char *process_item(request_rec *r, request_rec *orig,
                          log_format_item *item)
{
    const char *cp;

    /* First, see if we need to process this thing at all... */

    if (item->conditions && item->conditions->nelts != 0) {
        int i;
        int *conds = (int *) item->conditions->elts;
        int in_list = 0;

        for (i = 0; i < item->conditions->nelts; ++i) {
            if (r->status == conds[i]) {
                in_list = 1;
                break;
            }
        }

        if ((item->condition_sense && in_list)
            || (!item->condition_sense && !in_list)) {
            return "-";
        }
    }

    /* We do.  Do it... */

    cp = (*item->func) (item->want_orig ? orig : r, item->arg);
    return cp ? cp : "-";
}

static void flush_log(buffered_log *buf)
{
    if (buf->outcnt && buf->handle != NULL) {
        kuda_file_write(buf->handle, buf->outbuf, &buf->outcnt);
        buf->outcnt = 0;
    }
}


static int config_log_transaction(request_rec *r, config_log_state *cls,
                                  kuda_array_header_t *default_format)
{
    log_format_item *items;
    const char **strs;
    int *strl;
    request_rec *orig;
    int i;
    kuda_size_t len = 0;
    kuda_array_header_t *format;
    char *envar;
    kuda_status_t rv;

    if (cls->fname == NULL) {
        return DECLINED;
    }

    /*
     * See if we've got any conditional envariable-controlled logging decisions
     * to make.
     */
    if (cls->condition_var != NULL) {
        envar = cls->condition_var;
        if (*envar != '!') {
            if (kuda_table_get(r->subprocess_env, envar) == NULL) {
                return DECLINED;
            }
        }
        else {
            if (kuda_table_get(r->subprocess_env, &envar[1]) != NULL) {
                return DECLINED;
            }
        }
    }
    else if (cls->condition_expr != NULL) {
        const char *err;
        int rc = clhy_expr_exec(r, cls->condition_expr, &err);
        if (rc < 0)
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(00644)
                           "Error evaluating log condition: %s", err);
        if (rc <= 0)
            return DECLINED;
    }

    format = cls->format ? cls->format : default_format;

    strs = kuda_palloc(r->pool, sizeof(char *) * (format->nelts));
    strl = kuda_palloc(r->pool, sizeof(int) * (format->nelts));
    items = (log_format_item *) format->elts;

    orig = r;
    while (orig->prev) {
        orig = orig->prev;
    }
    while (r->next) {
        r = r->next;
    }

    for (i = 0; i < format->nelts; ++i) {
        strs[i] = process_item(r, orig, &items[i]);
    }

    for (i = 0; i < format->nelts; ++i) {
        len += strl[i] = strlen(strs[i]);
    }
    if (!log_writer) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(00645)
                "log writer isn't correctly setup");
        return HTTP_INTERNAL_SERVER_ERROR;
    }
    rv = log_writer(r, cls->log_writer, strs, strl, format->nelts, len);
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, r, CLHYLOGNO(00646)
                      "Error writing to %s", cls->fname);
    }
    return OK;
}

static int multi_log_transaction(request_rec *r)
{
    multi_log_state *mls = clhy_get_capi_config(r->server->capi_config,
                                                &log_config_capi);
    config_log_state *clsarray;
    int i;

    /*
     * Initialize per request state
     */
    log_request_state *state = kuda_pcalloc(r->pool, sizeof(log_request_state));
    clhy_set_capi_config(r->request_config, &log_config_capi, state);

    /*
     * Log this transaction..
     */
    if (mls->config_logs->nelts) {
        clsarray = (config_log_state *) mls->config_logs->elts;
        for (i = 0; i < mls->config_logs->nelts; ++i) {
            config_log_state *cls = &clsarray[i];

            config_log_transaction(r, cls, mls->default_format);
        }
    }

    if (mls->server_config_logs) {
        clsarray = (config_log_state *) mls->server_config_logs->elts;
        for (i = 0; i < mls->server_config_logs->nelts; ++i) {
            config_log_state *cls = &clsarray[i];

            if (cls->inherit || !mls->config_logs->nelts) {
                config_log_transaction(r, cls, mls->default_format);
            }
        }
    }

    return OK;
}

/*****************************************************************
 *
 * cAPI glue...
 */

static void *make_config_log_state(kuda_pool_t *p, server_rec *s)
{
    multi_log_state *mls;

    mls = (multi_log_state *) kuda_palloc(p, sizeof(multi_log_state));
    mls->config_logs = kuda_array_make(p, 1, sizeof(config_log_state));
    mls->default_format_string = NULL;
    mls->default_format = NULL;
    mls->server_config_logs = NULL;
    mls->formats = kuda_table_make(p, 4);
    kuda_table_setn(mls->formats, "CLF", DEFAULT_LOG_FORMAT);

    return mls;
}

/*
 * Use the merger to simply add a pointer from the vhost log state
 * to the log of logs specified for the non-vhost configuration.  Make sure
 * vhosts inherit any globally-defined format names.
 */

static void *merge_config_log_state(kuda_pool_t *p, void *basev, void *addv)
{
    multi_log_state *base = (multi_log_state *) basev;
    multi_log_state *add = (multi_log_state *) addv;

    add->server_config_logs = base->config_logs;
    if (!add->default_format) {
        add->default_format_string = base->default_format_string;
        add->default_format = base->default_format;
    }
    add->formats = kuda_table_overlay(p, base->formats, add->formats);

    return add;
}

/*
 * Set the default logfile format, or define a nickname for a format string.
 */
static const char *log_format(cmd_parms *cmd, void *dummy, const char *fmt,
                              const char *name)
{
    const char *err_string = NULL;
    multi_log_state *mls = clhy_get_capi_config(cmd->server->capi_config,
                                                &log_config_capi);

    /*
     * If we were given two arguments, the second is a name to be given to the
     * format.  This syntax just defines the nickname - it doesn't actually
     * make the format the default.
     */
    if (name != NULL) {
        parse_log_string(cmd->pool, fmt, &err_string);
        if (err_string == NULL) {
            kuda_table_setn(mls->formats, name, fmt);
        }
    }
    else {
        mls->default_format_string = fmt;
        mls->default_format = parse_log_string(cmd->pool, fmt, &err_string);
    }
    return err_string;
}


static const char *add_custom_log(cmd_parms *cmd, void *dummy, const char *fn,
                                  const char *fmt, const char *envclause)
{
    const char *err_string = NULL;
    multi_log_state *mls = clhy_get_capi_config(cmd->server->capi_config,
                                                &log_config_capi);
    config_log_state *cls;

    cls = (config_log_state *) kuda_array_push(mls->config_logs);
    cls->condition_var = NULL;
    cls->condition_expr = NULL;
    if (envclause != NULL) {
        if (strncasecmp(envclause, "env=", 4) == 0) {
            if ((envclause[4] == '\0')
                || ((envclause[4] == '!') && (envclause[5] == '\0'))) {
                return "missing environment variable name";
            }
            cls->condition_var = kuda_pstrdup(cmd->pool, &envclause[4]);
        }
        else if (strncasecmp(envclause, "expr=", 5) == 0) {
            const char *err;
            if ((envclause[5] == '\0'))
                return "missing condition";
            cls->condition_expr = clhy_expr_parse_cmd(cmd, &envclause[5],
                                                    CLHY_EXPR_FLAG_DONT_VARY,
                                                    &err, NULL);
            if (err)
                return err;
        }
        else {
            return "error in condition clause";
        }
    }

    cls->fname = fn;
    cls->format_string = fmt;
    cls->directive = cmd->directive;
    if (fmt == NULL) {
        cls->format = NULL;
    }
    else {
        cls->format = parse_log_string(cmd->pool, fmt, &err_string);
    }
    cls->log_writer = NULL;

    return err_string;
}

static const char *add_global_log(cmd_parms *cmd, void *dummy, const char *fn,
                                  const char *fmt, const char *envclause) {
    multi_log_state *mls = clhy_get_capi_config(cmd->server->capi_config,
                                                &log_config_capi);
    config_log_state *clsarray;
    config_log_state *cls;
    const char *ret;

    const char *err = clhy_check_cmd_context(cmd, GLOBAL_ONLY);

    if (err) {
        return err;
    }

    /* Add a custom log through the normal channel */
    ret = add_custom_log(cmd, dummy, fn, fmt, envclause);

    /* Set the inherit flag unless there was some error */
    if (ret == NULL) {
        clsarray = (config_log_state*)mls->config_logs->elts;
        cls = &clsarray[mls->config_logs->nelts-1];
        cls->inherit = 1;
    }

    return ret;
}

static const char *set_transfer_log(cmd_parms *cmd, void *dummy,
                                    const char *fn)
{
    return add_custom_log(cmd, dummy, fn, NULL, NULL);
}

static const char *set_buffered_logs_on(cmd_parms *parms, void *dummy, int flag)
{
    buffered_logs = flag;
    if (buffered_logs) {
        clhy_log_set_writer_init(clhy_buffered_log_writer_init);
        clhy_log_set_writer(clhy_buffered_log_writer);
    }
    else {
        clhy_log_set_writer_init(clhy_default_log_writer_init);
        clhy_log_set_writer(clhy_default_log_writer);
    }
    return NULL;
}
static const command_rec config_log_cmds[] =
{
CLHY_INIT_TAKE23("CustomLog", add_custom_log, NULL, RSRC_CONF,
     "a file name, a custom log format string or format name, "
     "and an optional \"env=\" or \"expr=\" clause (see docs)"),
CLHY_INIT_TAKE23("GlobalLog", add_global_log, NULL, RSRC_CONF,
     "Same as CustomLog, but forces virtualhosts to inherit the log"),
CLHY_INIT_TAKE1("TransferLog", set_transfer_log, NULL, RSRC_CONF,
     "the filename of the access log"),
CLHY_INIT_TAKE12("LogFormat", log_format, NULL, RSRC_CONF,
     "a log format string (see docs) and an optional format name"),
CLHY_INIT_FLAG("BufferedLogs", set_buffered_logs_on, NULL, RSRC_CONF,
                 "Enable Buffered Logging (experimental)"),
    {NULL}
};

static config_log_state *open_config_log(server_rec *s, kuda_pool_t *p,
                                         config_log_state *cls,
                                         kuda_array_header_t *default_format)
{
    if (cls->log_writer != NULL) {
        return cls;             /* virtual config shared w/main server */
    }

    if (cls->fname == NULL) {
        return cls;             /* Leave it NULL to decline.  */
    }

    cls->log_writer = log_writer_init(p, s, cls->fname);
    if (cls->log_writer == NULL)
        return NULL;

    return cls;
}

static int open_multi_logs(server_rec *s, kuda_pool_t *p)
{
    int i;
    multi_log_state *mls = clhy_get_capi_config(s->capi_config,
                                             &log_config_capi);
    config_log_state *clsarray;
    const char *dummy;
    const char *format;

    if (mls->default_format_string) {
        format = kuda_table_get(mls->formats, mls->default_format_string);
        if (format) {
            mls->default_format = parse_log_string(p, format, &dummy);
        }
    }

    if (!mls->default_format) {
        mls->default_format = parse_log_string(p, DEFAULT_LOG_FORMAT, &dummy);
    }

    if (mls->config_logs->nelts) {
        clsarray = (config_log_state *) mls->config_logs->elts;
        for (i = 0; i < mls->config_logs->nelts; ++i) {
            config_log_state *cls = &clsarray[i];

            if (cls->format_string) {
                format = kuda_table_get(mls->formats, cls->format_string);
                if (format) {
                    cls->format = parse_log_string(p, format, &dummy);
                }
            }

            if (!open_config_log(s, p, cls, mls->default_format)) {
                /* Failure already logged by open_config_log */
                return DONE;
            }
        }
    }
    else if (mls->server_config_logs) {
        clsarray = (config_log_state *) mls->server_config_logs->elts;
        for (i = 0; i < mls->server_config_logs->nelts; ++i) {
            config_log_state *cls = &clsarray[i];

            if (cls->format_string) {
                format = kuda_table_get(mls->formats, cls->format_string);
                if (format) {
                    cls->format = parse_log_string(p, format, &dummy);
                }
            }

            if (!open_config_log(s, p, cls, mls->default_format)) {
                /* Failure already logged by open_config_log */
                return DONE;
            }
        }
    }

    return OK;
}


static kuda_status_t flush_all_logs(void *data)
{
    server_rec *s = data;
    multi_log_state *mls;
    kuda_array_header_t *log_list;
    config_log_state *clsarray;
    buffered_log *buf;
    int i;

    if (!buffered_logs)
        return KUDA_SUCCESS;

    for (; s; s = s->next) {
        mls = clhy_get_capi_config(s->capi_config, &log_config_capi);
        log_list = NULL;
        if (mls->config_logs->nelts) {
            log_list = mls->config_logs;
        }
        else if (mls->server_config_logs) {
            log_list = mls->server_config_logs;
        }
        if (log_list) {
            clsarray = (config_log_state *) log_list->elts;
            for (i = 0; i < log_list->nelts; ++i) {
                buf = clsarray[i].log_writer;
                flush_log(buf);
            }
        }
    }
    return KUDA_SUCCESS;
}


static int init_config_log(kuda_pool_t *pc, kuda_pool_t *p, kuda_pool_t *pt, server_rec *s)
{
    int res;

    /* First init the buffered logs array, which is needed when opening the logs. */
    if (buffered_logs) {
        all_buffered_logs = kuda_array_make(p, 5, sizeof(buffered_log *));
    }

    /* Next, do "physical" server, which gets default log fd and format
     * for the virtual servers, if they don't override...
     */
    res = open_multi_logs(s, p);

    /* Then, virtual servers */

    for (s = s->next; (res == OK) && s; s = s->next) {
        res = open_multi_logs(s, p);
    }

    return res;
}

static void init_child(kuda_pool_t *p, server_rec *s)
{
    int clmp_threads;

    clhy_clmp_query(CLHY_CLMPQ_MAX_THREADS, &clmp_threads);

    /* Now register the last buffer flush with the cleanup engine */
    if (buffered_logs) {
        int i;
        buffered_log **array = (buffered_log **)all_buffered_logs->elts;

        kuda_pool_cleanup_register(p, s, flush_all_logs, flush_all_logs);

        for (i = 0; i < all_buffered_logs->nelts; i++) {
            buffered_log *this = array[i];

#if KUDA_HAS_THREADS
            if (clmp_threads > 1) {
                kuda_status_t rv;

                this->mutex.type = kuda_anylock_threadmutex;
                rv = kuda_thread_mutex_create(&this->mutex.lock.tm,
                                             KUDA_THREAD_MUTEX_DEFAULT,
                                             p);
                if (rv != KUDA_SUCCESS) {
                    clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, s, CLHYLOGNO(00647)
                                 "could not initialize buffered log mutex, "
                                 "transfer log may become corrupted");
                    this->mutex.type = kuda_anylock_none;
                }
            }
            else
#endif
            {
                this->mutex.type = kuda_anylock_none;
            }
        }
    }
}

static void clhy_register_log_handler(kuda_pool_t *p, char *tag,
                                    clhy_log_handler_fn_t *handler, int def)
{
    clhy_log_handler *log_struct = kuda_palloc(p, sizeof(*log_struct));
    log_struct->func = handler;
    log_struct->want_orig_default = def;

    kuda_hash_set(log_hash, tag, strlen(tag), (const void *)log_struct);
}
static clhy_log_writer_init *clhy_log_set_writer_init(clhy_log_writer_init *handle)
{
    clhy_log_writer_init *old = log_writer_init;
    log_writer_init = handle;

    return old;

}
static clhy_log_writer *clhy_log_set_writer(clhy_log_writer *handle)
{
    clhy_log_writer *old = log_writer;
    log_writer = handle;

    return old;
}

static kuda_status_t clhy_default_log_writer( request_rec *r,
                           void *handle,
                           const char **strs,
                           int *strl,
                           int nelts,
                           kuda_size_t len)

{
    char *str;
    char *s;
    int i;
    kuda_status_t rv;

    /*
     * We do this memcpy dance because write() is atomic for len < PIPE_BUF,
     * while writev() need not be.
     */
    str = kuda_palloc(r->pool, len + 1);

    for (i = 0, s = str; i < nelts; ++i) {
        memcpy(s, strs[i], strl[i]);
        s += strl[i];
    }

    rv = kuda_file_write((kuda_file_t*)handle, str, &len);

    return rv;
}
static void *clhy_default_log_writer_init(kuda_pool_t *p, server_rec *s,
                                        const char* name)
{
    if (*name == '|') {
        piped_log *pl;

        pl = clhy_open_piped_log(p, name + 1);
        if (pl == NULL) {
           return NULL;
        }
        return clhy_piped_log_write_fd(pl);
    }
    else {
        const char *fname = clhy_server_root_relative(p, name);
        kuda_file_t *fd;
        kuda_status_t rv;

        if (!fname) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, KUDA_EBADPATH, s, CLHYLOGNO(00648)
                            "invalid transfer log path %s.", name);
            return NULL;
        }
        rv = kuda_file_open(&fd, fname, xfer_flags, xfer_perms, p);
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rv, s, CLHYLOGNO(00649)
                            "could not open transfer log file %s.", fname);
            return NULL;
        }
        return fd;
    }
}
static void *clhy_buffered_log_writer_init(kuda_pool_t *p, server_rec *s,
                                        const char* name)
{
    buffered_log *b;
    b = kuda_pcalloc(p, sizeof(buffered_log));
    b->handle = clhy_default_log_writer_init(p, s, name);

    if (b->handle) {
        *(buffered_log **)kuda_array_push(all_buffered_logs) = b;
        return b;
    }
    else
        return NULL;
}
static kuda_status_t clhy_buffered_log_writer(request_rec *r,
                                           void *handle,
                                           const char **strs,
                                           int *strl,
                                           int nelts,
                                           kuda_size_t len)

{
    char *str;
    char *s;
    int i;
    kuda_status_t rv;
    buffered_log *buf = (buffered_log*)handle;

    if ((rv = KUDA_ANYLOCK_LOCK(&buf->mutex)) != KUDA_SUCCESS) {
        return rv;
    }

    if (len + buf->outcnt > LOG_BUFSIZE) {
        flush_log(buf);
    }
    if (len >= LOG_BUFSIZE) {
        kuda_size_t w;

        /*
         * We do this memcpy dance because write() is atomic for
         * len < PIPE_BUF, while writev() need not be.
         */
        str = kuda_palloc(r->pool, len + 1);
        for (i = 0, s = str; i < nelts; ++i) {
            memcpy(s, strs[i], strl[i]);
            s += strl[i];
        }
        w = len;
        rv = kuda_file_write(buf->handle, str, &w);

    }
    else {
        for (i = 0, s = &buf->outbuf[buf->outcnt]; i < nelts; ++i) {
            memcpy(s, strs[i], strl[i]);
            s += strl[i];
        }
        buf->outcnt += len;
        rv = KUDA_SUCCESS;
    }

    KUDA_ANYLOCK_UNLOCK(&buf->mutex);
    return rv;
}

static int log_pre_config(kuda_pool_t *p, kuda_pool_t *plog, kuda_pool_t *ptemp)
{
    static KUDA_OPTIONAL_FN_TYPE(clhy_register_log_handler) *log_pfn_register;

    log_pfn_register = KUDA_RETRIEVE_OPTIONAL_FN(clhy_register_log_handler);

    if (log_pfn_register) {
        log_pfn_register(p, "h", log_remote_host, 0);
        log_pfn_register(p, "a", log_remote_address, 0 );
        log_pfn_register(p, "A", log_local_address, 0 );
        log_pfn_register(p, "l", log_remote_logname, 0);
        log_pfn_register(p, "u", log_remote_user, 0);
        log_pfn_register(p, "t", log_request_time, 0);
        log_pfn_register(p, "f", log_request_file, 0);
        log_pfn_register(p, "b", clf_log_bytes_sent, 0);
        log_pfn_register(p, "B", log_bytes_sent, 0);
        log_pfn_register(p, "i", log_header_in, 0);
        log_pfn_register(p, "o", log_header_out, 0);
        log_pfn_register(p, "n", log_note, 0);
        log_pfn_register(p, "L", log_log_id, 1);
        log_pfn_register(p, "e", log_env_var, 0);
        log_pfn_register(p, "V", log_server_name, 0);
        log_pfn_register(p, "v", log_virtual_host, 0);
        log_pfn_register(p, "p", log_server_port, 0);
        log_pfn_register(p, "P", log_pid_tid, 0);
        log_pfn_register(p, "H", log_request_protocol, 0);
        log_pfn_register(p, "m", log_request_method, 0);
        log_pfn_register(p, "q", log_request_query, 0);
        log_pfn_register(p, "X", log_connection_status, 0);
        log_pfn_register(p, "C", log_cookie, 0);
        log_pfn_register(p, "k", log_requests_on_connection, 0);
        log_pfn_register(p, "r", log_request_line, 1);
        log_pfn_register(p, "D", log_request_duration_microseconds, 1);
        log_pfn_register(p, "T", log_request_duration_scaled, 1);
        log_pfn_register(p, "U", log_request_uri, 1);
        log_pfn_register(p, "s", log_status, 1);
        log_pfn_register(p, "R", log_handler, 1);

        log_pfn_register(p, "^ti", log_trailer_in, 0);
        log_pfn_register(p, "^to", log_trailer_out, 0);
    }

    /* reset to default conditions */
    clhy_log_set_writer_init(clhy_default_log_writer_init);
    clhy_log_set_writer(clhy_default_log_writer);
    buffered_logs = 0;

    return OK;
}

static int check_log_dir(kuda_pool_t *p, server_rec *s, config_log_state *cls)
{
    if (!cls->fname || cls->fname[0] == '|' || !cls->directive) {
        return OK;
    }
    else {
        char *abs = clhy_server_root_relative(p, cls->fname);
        char *dir = clhy_make_dirstr_parent(p, abs);
        kuda_finfo_t finfo;
        const clhy_directive_t *directive = cls->directive;
        kuda_status_t rv = kuda_stat(&finfo, dir, KUDA_FINFO_TYPE, p);
        cls->directive = NULL; /* Don't check this config_log_state again */
        if (rv == KUDA_SUCCESS && finfo.filetype != KUDA_DIR)
            rv = KUDA_ENOTDIR;
        if (rv != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_STARTUP|CLHYLOG_EMERG, rv, s,
                         CLHYLOGNO(02297)
                         "Cannot access directory '%s' for log file '%s' "
                         "defined at %s:%d", dir, cls->fname,
                         directive->filename, directive->line_num);
            return !OK;
        }
    }
    return OK;
}

static int log_check_config(kuda_pool_t *pconf, kuda_pool_t *plog, kuda_pool_t *ptemp, server_rec *s)
{
    int rv = OK;
    while (s) {
        multi_log_state *mls = clhy_get_capi_config(s->capi_config,
                                                    &log_config_capi);
        /*
         * We don't need to check mls->server_config_logs because it just
         * points to the parent server's mls->config_logs.
         */
        kuda_array_header_t *log_list = mls->config_logs;
        config_log_state *clsarray = (config_log_state *) log_list->elts;
        int i;
        for (i = 0; i < log_list->nelts; ++i) {
            if (check_log_dir(ptemp, s, &clsarray[i]) != OK)
                rv = !OK;
        }

        s = s->next;
    }
    return rv;
}

static void register_hooks(kuda_pool_t *p)
{
    clhy_hook_pre_config(log_pre_config,NULL,NULL,KUDA_HOOK_REALLY_FIRST);
    clhy_hook_check_config(log_check_config,NULL,NULL,KUDA_HOOK_MIDDLE);
    clhy_hook_child_init(init_child,NULL,NULL,KUDA_HOOK_MIDDLE);
    clhy_hook_open_logs(init_config_log,NULL,NULL,KUDA_HOOK_MIDDLE);
    clhy_hook_log_transaction(multi_log_transaction,NULL,NULL,KUDA_HOOK_MIDDLE);

    /* Init log_hash before we register the optional function. It is
     * possible for the optional function, clhy_register_log_handler,
     * to be called before any other capi_log_config hooks are called.
     * As a policy, we should init everything required by an optional function
     * before calling KUDA_REGISTER_OPTIONAL_FN.
     */
    log_hash = kuda_hash_make(p);
    KUDA_REGISTER_OPTIONAL_FN(clhy_register_log_handler);
    KUDA_REGISTER_OPTIONAL_FN(clhy_log_set_writer_init);
    KUDA_REGISTER_OPTIONAL_FN(clhy_log_set_writer);
}

CLHY_DECLARE_CAPI(log_config) =
{
    STANDARD16_CAPI_STUFF,
    NULL,                       /* create per-dir config */
    NULL,                       /* merge per-dir config */
    make_config_log_state,      /* server config */
    merge_config_log_state,     /* merge server config */
    config_log_cmds,            /* command kuda_table_t */
    register_hooks              /* register hooks */
};

