/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file capi_log_config.h
 * @brief Logging Configuration Extension cAPI for cLHy
 *
 * @defgroup CAPI_LOG_CONFIG capi_log_config
 * @ingroup CLHYKUDEL_CAPIS
 * @{
 */

#include "kuda_optional.h"
#include "wwhy.h"
#include "scoreboard.h"

#ifndef _CAPI_LOG_CONFIG_H
#define _CAPI_LOG_CONFIG_H 1

/**
 * callback function prototype for a external log handler
 */
typedef const char *clhy_log_handler_fn_t(request_rec *r, char *a);

/**
 * callback function prototype for external writer initialization.
 */
typedef void *clhy_log_writer_init(kuda_pool_t *p, server_rec *s,
                                 const char *name);
/**
 * callback which gets called where there is a log line to write.
 */
typedef kuda_status_t clhy_log_writer(
                            request_rec *r,
                            void *handle,
                            const char **portions,
                            int *lengths,
                            int nelts,
                            kuda_size_t len);

typedef struct clhy_log_handler {
    clhy_log_handler_fn_t *func;
    int want_orig_default;
} clhy_log_handler;

KUDA_DECLARE_OPTIONAL_FN(void, clhy_register_log_handler,
                        (kuda_pool_t *p, char *tag, clhy_log_handler_fn_t *func,
                         int def));
/**
 * you will need to set your init handler *BEFORE* the open_logs
 * in capi_log_config gets executed
 */
KUDA_DECLARE_OPTIONAL_FN(clhy_log_writer_init*, clhy_log_set_writer_init,(clhy_log_writer_init *func));
/**
 * you should probably set the writer at the same time (ie..before open_logs)
 */
KUDA_DECLARE_OPTIONAL_FN(clhy_log_writer*, clhy_log_set_writer, (clhy_log_writer* func));

#endif /* CAPI_LOG_CONFIG */
/** @} */

