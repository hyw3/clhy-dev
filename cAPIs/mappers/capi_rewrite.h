/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  capi_rewrite.h
 * @brief Rewrite Extension cAPI for cLHy
 *
 * @defgroup CAPI_REWRITE capi_rewrite
 * @ingroup CLHYKUDEL_CAPIS
 * @{
 */

#ifndef CAPI_REWRITE_H
#define CAPI_REWRITE_H 1

#include "kuda_optional.h"
#include "wwhy.h"

#define REWRITE_REDIRECT_HANDLER_NAME "redirect-handler"

/* rewrite map function prototype */
typedef char *(rewrite_mapfunc_t)(request_rec *r, char *key);

/* optional function declaration */
KUDA_DECLARE_OPTIONAL_FN(void, clhy_register_rewrite_mapfunc,
                        (char *name, rewrite_mapfunc_t *func));

#endif /* CAPI_REWRITE_H */
/** @} */
