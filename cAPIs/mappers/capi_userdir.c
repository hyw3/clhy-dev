/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * capi_userdir... implement the UserDir command.  Broken away from the
 * Alias stuff for a couple of good and not-so-good reasons:
 *
 * 1) It shows a real minimal working example of how to do something like
 *    this.
 * 2) I know people who are actually interested in changing this *particular*
 *    aspect of server functionality without changing the rest of it.  That's
 *    what this whole modular arrangement is supposed to be good at...
 *
 * Modified by Alexei Kosut to support the following constructs
 * (server running at www.foo.com, request for /~bar/one/two.html)
 *
 * UserDir public_html      -> ~bar/public_html/one/two.html
 * UserDir /usr/web         -> /usr/web/bar/one/two.html
 * UserDir /home/ * /www     -> /home/bar/www/one/two.html
 *  NOTE: theses ^ ^ space only added allow it to work in a comment, ignore
 * UserDir http://x/users   -> (302) http://x/users/bar/one/two.html
 * UserDir http://x/ * /y     -> (302) http://x/bar/y/one/two.html
 *  NOTE: here also ^ ^
 *
 * In addition, you can use multiple entries, to specify alternate
 * user directories (a la Directory Index). For example:
 *
 * UserDir public_html /usr/web http://www.xyz.com/users
 *
 * Modified by Ken Coar to provide for the following:
 *
 * UserDir disable[d] username ...
 * UserDir enable[d] username ...
 *
 * If "disabled" has no other arguments, *all* ~<username> references are
 * disabled, except those explicitly turned on with the "enabled" keyword.
 */

#include "kuda_strings.h"
#include "kuda_user.h"

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_request.h"

#if !defined(WIN32) && !defined(OS2) && !defined(NETWARE)
#define HAVE_UNIX_SUEXEC
#endif

#ifdef HAVE_UNIX_SUEXEC
#include "unixd.h"        /* Contains the suexec_identity hook used on Unix */
#endif


/*
 * The default directory in user's home dir
 * In the default install, the cAPI is disabled
 */
#ifndef DEFAULT_USER_DIR
#define DEFAULT_USER_DIR NULL
#endif

#define O_DEFAULT 0
#define O_ENABLE 1
#define O_DISABLE 2

cAPI CLHY_CAPI_DECLARE_DATA userdir_capi;

typedef struct {
    int globally_disabled;
    const char *userdir;
    kuda_table_t *enabled_users;
    kuda_table_t *disabled_users;
} userdir_config;

/*
 * Server config for this cAPI: global disablement flag, a list of usernames
 * ineligible for UserDir access, a list of those immune to global (but not
 * explicit) disablement, and the replacement string for all others.
 */

static void *create_userdir_config(kuda_pool_t *p, server_rec *s)
{
    userdir_config *newcfg = kuda_pcalloc(p, sizeof(*newcfg));

    newcfg->globally_disabled = O_DEFAULT;
    newcfg->userdir = DEFAULT_USER_DIR;
    newcfg->enabled_users = kuda_table_make(p, 4);
    newcfg->disabled_users = kuda_table_make(p, 4);

    return newcfg;
}

static void *merge_userdir_config(kuda_pool_t *p, void *basev, void *overridesv)
{
    userdir_config *cfg = kuda_pcalloc(p, sizeof(userdir_config));
    userdir_config *base = basev, *overrides = overridesv;

    cfg->globally_disabled = (overrides->globally_disabled != O_DEFAULT) ?
                             overrides->globally_disabled :
                             base->globally_disabled;
    cfg->userdir = (overrides->userdir != DEFAULT_USER_DIR) ?
                   overrides->userdir : base->userdir;

    /* not merged */
    cfg->enabled_users = overrides->enabled_users;
    cfg->disabled_users = overrides->disabled_users;

    return cfg;
}


static const char *set_user_dir(cmd_parms *cmd, void *dummy, const char *arg)
{
    userdir_config *s_cfg = clhy_get_capi_config(cmd->server->capi_config,
                                                 &userdir_capi);
    char *username;
    const char *usernames = arg;
    char *kw = clhy_getword_conf(cmd->temp_pool, &usernames);
    kuda_table_t *usertable;

    /* Since we are a raw argument, it is possible for us to be called with
     * zero arguments.  So that we aren't ambiguous, flat out reject this.
     */
    if (*kw == '\0') {
        return "UserDir requires an argument.";
    }

    /*
     * Let's do the comparisons once.
     */
    if ((!strcasecmp(kw, "disable")) || (!strcasecmp(kw, "disabled"))) {
        /*
         * If there are no usernames specified, this is a global disable - we
         * need do no more at this point than record the fact.
         */
        if (!*usernames) {
            s_cfg->globally_disabled = O_DISABLE;
            return NULL;
        }
        usertable = s_cfg->disabled_users;
    }
    else if ((!strcasecmp(kw, "enable")) || (!strcasecmp(kw, "enabled"))) {
        if (!*usernames) {
            s_cfg->globally_disabled = O_ENABLE;
            return NULL;
        }
        usertable = s_cfg->enabled_users;
    }
    else {
        /*
         * If the first (only?) value isn't one of our keywords, just copy
         * the string to the userdir string.
         */
        s_cfg->userdir = arg;
        return NULL;
    }
    /*
     * Now we just take each word in turn from the command line and add it to
     * the appropriate table.
     */
    while (*usernames) {
        username = clhy_getword_conf(cmd->pool, &usernames);
        kuda_table_setn(usertable, username, "1");
    }
    return NULL;
}

static const command_rec userdir_cmds[] = {
    CLHY_INIT_RAW_ARGS("UserDir", set_user_dir, NULL, RSRC_CONF,
                     "the public subdirectory in users' home directories, or "
                     "'disabled', or 'disabled username username...', or "
                     "'enabled username username...'"),
    {NULL}
};

static int translate_userdir(request_rec *r)
{
    clhy_conf_vector_t *server_conf;
    const userdir_config *s_cfg;
    const char *userdirs;
    const char *user, *dname;
    char *redirect;
    kuda_finfo_t statbuf;

    /*
     * If the URI doesn't match our basic pattern, we've nothing to do with
     * it.
     */
    if (r->uri[0] != '/' || r->uri[1] != '~') {
        return DECLINED;
    }
    server_conf = r->server->capi_config;
    s_cfg = clhy_get_capi_config(server_conf, &userdir_capi);
    userdirs = s_cfg->userdir;
    if (userdirs == NULL) {
        return DECLINED;
    }

    dname = r->uri + 2;
    user = clhy_getword(r->pool, &dname, '/');

    /*
     * The 'dname' funny business involves backing it up to capture the '/'
     * delimiting the "/~user" part from the rest of the URL, in case there
     * was one (the case where there wasn't being just "GET /~user HTTP/1.0",
     * for which we don't want to tack on a '/' onto the filename).
     */

    if (dname[-1] == '/') {
        --dname;
    }

    /*
     * If there's no username, it's not for us.  Ignore . and .. as well.
     */
    if (user[0] == '\0' ||
        (user[1] == '.' && (user[2] == '\0' ||
                            (user[2] == '.' && user[3] == '\0')))) {
        return DECLINED;
    }
    /*
     * Nor if there's an username but it's in the disabled list.
     */
    if (kuda_table_get(s_cfg->disabled_users, user) != NULL) {
        return DECLINED;
    }
    /*
     * If there's a global interdiction on UserDirs, check to see if this
     * name is one of the Blessed.
     */
    if (s_cfg->globally_disabled == O_DISABLE
        && kuda_table_get(s_cfg->enabled_users, user) == NULL) {
        return DECLINED;
    }

    /*
     * Special cases all checked, onward to normal substitution processing.
     */

    while (*userdirs) {
        const char *userdir = clhy_getword_conf(r->pool, &userdirs);
        char *filename = NULL, *prefix = NULL;
        kuda_status_t rv;
        int is_absolute = clhy_platform_is_path_absolute(r->pool, userdir);

        if (clhy_strchr_c(userdir, '*'))
            prefix = clhy_getword(r->pool, &userdir, '*');

        if (userdir[0] == '\0' || is_absolute) {
            if (prefix) {
#ifdef HAVE_DRIVE_LETTERS
                /*
                 * Crummy hack. Need to figure out whether we have been
                 * redirected to a URL or to a file on some drive. Since I
                 * know of no protocols that are a single letter, ignore
                 * a : as the first or second character, and assume a file
                 * was specified
                 */
                if (strchr(prefix + 2, ':'))
#else
                if (strchr(prefix, ':') && !is_absolute)
#endif /* HAVE_DRIVE_LETTERS */
                {
                    redirect = kuda_pstrcat(r->pool, prefix, user, userdir,
                                           dname, NULL);
                    kuda_table_setn(r->headers_out, "Location", redirect);
                    return HTTP_MOVED_TEMPORARILY;
                }
                else
                    filename = kuda_pstrcat(r->pool, prefix, user, userdir,
                                           NULL);
            }
            else
                filename = kuda_pstrcat(r->pool, userdir, "/", user, NULL);
        }
        else if (prefix && clhy_strchr_c(prefix, ':')) {
            redirect = kuda_pstrcat(r->pool, prefix, user, dname, NULL);
            kuda_table_setn(r->headers_out, "Location", redirect);
            return HTTP_MOVED_TEMPORARILY;
        }
        else {
#if KUDA_HAS_USER
            char *homedir;

            if (kuda_uid_homepath_get(&homedir, user, r->pool) == KUDA_SUCCESS) {
                filename = kuda_pstrcat(r->pool, homedir, "/", userdir, NULL);
            }
#else
            return DECLINED;
#endif
        }

        /*
         * Now see if it exists, or we're at the last entry. If we are at the
         * last entry, then use the filename generated (if there is one)
         * anyway, in the hope that some handler might handle it. This can be
         * used, for example, to run a CGI script for the user.
         */
        if (filename && (!*userdirs
                      || ((rv = kuda_stat(&statbuf, filename, KUDA_FINFO_MIN,
                                         r->pool)) == KUDA_SUCCESS
                                             || rv == KUDA_INCOMPLETE))) {
            r->filename = kuda_pstrcat(r->pool, filename, dname, NULL);
            clhy_set_context_info(r, kuda_pstrmemdup(r->pool, r->uri,
                                                  dname - r->uri),
                                filename);
            /* XXX: Does this walk us around FollowSymLink rules?
             * When statbuf contains info on r->filename we can save a syscall
             * by copying it to r->finfo
             */
            if (*userdirs && dname[0] == 0)
                r->finfo = statbuf;

            /* For use in the get_suexec_identity phase */
            kuda_table_setn(r->notes, "capi_userdir_user", user);

            return OK;
        }
    }

    return DECLINED;
}

#ifdef HAVE_UNIX_SUEXEC
static clhy_unix_identity_t *get_suexec_id_doer(const request_rec *r)
{
    clhy_unix_identity_t *ugid = NULL;
#if KUDA_HAS_USER
    const char *username = kuda_table_get(r->notes, "capi_userdir_user");

    if (username == NULL) {
        return NULL;
    }

    if ((ugid = kuda_palloc(r->pool, sizeof(*ugid))) == NULL) {
        return NULL;
    }

    if (kuda_uid_get(&ugid->uid, &ugid->gid, username, r->pool) != KUDA_SUCCESS) {
        return NULL;
    }

    ugid->userdir = 1;
#endif
    return ugid;
}
#endif /* HAVE_UNIX_SUEXEC */

static void register_hooks(kuda_pool_t *p)
{
    static const char * const aszPre[]={ "capi_alias.c",NULL };
    static const char * const aszSucc[]={ "capi_vhost_alias.c",NULL };

    clhy_hook_translate_name(translate_userdir,aszPre,aszSucc,KUDA_HOOK_MIDDLE);
#ifdef HAVE_UNIX_SUEXEC
    clhy_hook_get_suexec_identity(get_suexec_id_doer,NULL,NULL,KUDA_HOOK_FIRST);
#endif
}

CLHY_DECLARE_CAPI(userdir) = {
    STANDARD16_CAPI_STUFF,
    NULL,                       /* dir config creater */
    NULL,                       /* dir merger --- default is to override */
    create_userdir_config,      /* server config */
    merge_userdir_config,       /* merge server config */
    userdir_cmds,               /* command kuda_table_t */
    register_hooks              /* register hooks */
};
