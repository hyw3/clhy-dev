/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * capi_dir.c: handle default index files, and trailing-/ redirects
 */

#include "kuda_strings.h"
#include "kuda_lib.h"
#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_request.h"
#include "http_protocol.h"
#include "http_log.h"
#include "http_main.h"
#include "util_script.h"
#include "capi_rewrite.h"

cAPI CLHY_CAPI_DECLARE_DATA dir_capi;

typedef enum {
    MODDIR_OFF = 0,
    MODDIR_ON,
    MODDIR_UNSET
} moddir_cfg;

#define REDIRECT_OFF   0
#define REDIRECT_UNSET 1

typedef struct dir_config_struct {
    kuda_array_header_t *index_names;
    moddir_cfg do_slash;
    moddir_cfg checkhandler;
    int redirect_index;
    const char *dflt;
} dir_config_rec;

#define DIR_CMD_PERMS OR_INDEXES

static const char *add_index(cmd_parms *cmd, void *dummy, const char *arg)
{
    dir_config_rec *d = dummy;
    const char *t, *w;
    int count = 0;

    if (!d->index_names) {
        d->index_names = kuda_array_make(cmd->pool, 2, sizeof(char *));
    }

    t = arg;
    while ((w = clhy_getword_conf(cmd->pool, &t)) && w[0]) {
        if (count == 0 && !strcasecmp(w, "disabled")) {
            /* peek to see if "disabled" is first in a series of arguments */
            const char *tt = t;
            const char *ww = clhy_getword_conf(cmd->temp_pool, &tt);
            if (ww[0] == '\0') {
               /* "disabled" is first, and alone */
               kuda_array_clear(d->index_names); 
               break;
            }
        }
        *(const char **)kuda_array_push(d->index_names) = w;
        count++;
    }

    return NULL;
}

static const char *configure_slash(cmd_parms *cmd, void *d_, int arg)
{
    dir_config_rec *d = d_;

    d->do_slash = arg ? MODDIR_ON : MODDIR_OFF;
    return NULL;
}
static const char *configure_checkhandler(cmd_parms *cmd, void *d_, int arg)
{
    dir_config_rec *d = d_;

    d->checkhandler = arg ? MODDIR_ON : MODDIR_OFF;
    return NULL;
}
static const char *configure_redirect(cmd_parms *cmd, void *d_, const char *arg1)
{
    dir_config_rec *d = d_;
    int status;

    if (!strcasecmp(arg1, "ON"))
        status = HTTP_MOVED_TEMPORARILY;
    else if (!strcasecmp(arg1, "OFF"))
        status = REDIRECT_OFF;
    else if (!strcasecmp(arg1, "permanent"))
        status = HTTP_MOVED_PERMANENTLY;
    else if (!strcasecmp(arg1, "temp"))
        status = HTTP_MOVED_TEMPORARILY;
    else if (!strcasecmp(arg1, "seeother"))
        status = HTTP_SEE_OTHER;
    else if (kuda_isdigit(*arg1)) {
        status = atoi(arg1);
        if (!clhy_is_HTTP_REDIRECT(status)) {
            return "DirectoryIndexRedirect only accepts values between 300 and 399";
        }
    }
    else {
        return "DirectoryIndexRedirect ON|OFF|permanent|temp|seeother|3xx";
    }

    d->redirect_index = status;
    return NULL;
}
static const command_rec dir_cmds[] =
{
    CLHY_INIT_TAKE1("FallbackResource", clhy_set_string_slot,
                  (void*)KUDA_OFFSETOF(dir_config_rec, dflt),
                  DIR_CMD_PERMS, "Set a default handler"),
    CLHY_INIT_RAW_ARGS("DirectoryIndex", add_index, NULL, DIR_CMD_PERMS,
                    "a list of file names"),
    CLHY_INIT_FLAG("DirectorySlash", configure_slash, NULL, DIR_CMD_PERMS,
                 "On or Off"),
    CLHY_INIT_FLAG("DirectoryCheckHandler", configure_checkhandler, NULL, DIR_CMD_PERMS,
                 "On or Off"),
    CLHY_INIT_TAKE1("DirectoryIndexRedirect", configure_redirect,
                   NULL, DIR_CMD_PERMS, "On, Off, or a 3xx status code."),

    {NULL}
};

static void *create_dir_config(kuda_pool_t *p, char *dummy)
{
    dir_config_rec *new = kuda_pcalloc(p, sizeof(dir_config_rec));

    new->index_names = NULL;
    new->do_slash = MODDIR_UNSET;
    new->checkhandler = MODDIR_UNSET;
    new->redirect_index = REDIRECT_UNSET;
    return (void *) new;
}

static void *merge_dir_configs(kuda_pool_t *p, void *basev, void *addv)
{
    dir_config_rec *new = kuda_pcalloc(p, sizeof(dir_config_rec));
    dir_config_rec *base = (dir_config_rec *)basev;
    dir_config_rec *add = (dir_config_rec *)addv;

    new->index_names = add->index_names ? add->index_names : base->index_names;
    new->do_slash =
        (add->do_slash == MODDIR_UNSET) ? base->do_slash : add->do_slash;
    new->checkhandler =
        (add->checkhandler == MODDIR_UNSET) ? base->checkhandler : add->checkhandler;
    new->redirect_index=
        (add->redirect_index == REDIRECT_UNSET) ? base->redirect_index : add->redirect_index;
    new->dflt = add->dflt ? add->dflt : base->dflt;
    return new;
}

static int fixup_dflt(request_rec *r)
{
    dir_config_rec *d = clhy_get_capi_config(r->per_dir_config, &dir_capi);
    const char *name_ptr;
    request_rec *rr;
    int error_notfound = 0;

    name_ptr = d->dflt;
    if ((name_ptr == NULL) || !(strcasecmp(name_ptr,"disabled"))){
        return DECLINED;
    }
    /* XXX: if FallbackResource points to something that doesn't exist,
     * this may recurse until it hits the limit for internal redirects
     * before returning an Internal Server Error.
     */

    /* The logic of this function is basically cloned and simplified
     * from fixup_dir below.  See the comments there.
     */
    if (r->args != NULL) {
        name_ptr = kuda_pstrcat(r->pool, name_ptr, "?", r->args, NULL);
    }
    rr = clhy_sub_req_lookup_uri(name_ptr, r, r->output_filters);
    if (rr->status == HTTP_OK
        && (   (rr->handler && !strcmp(rr->handler, "proxy-server"))
            || rr->finfo.filetype == KUDA_REG)) {
        clhy_internal_fast_redirect(rr, r);
        return OK;
    }
    else if (clhy_is_HTTP_REDIRECT(rr->status)) {

        kuda_pool_join(r->pool, rr->pool);
        r->notes = kuda_table_overlay(r->pool, r->notes, rr->notes);
        r->headers_out = kuda_table_overlay(r->pool, r->headers_out,
                                           rr->headers_out);
        r->err_headers_out = kuda_table_overlay(r->pool, r->err_headers_out,
                                               rr->err_headers_out);
        error_notfound = rr->status;
    }
    else if (rr->status && rr->status != HTTP_NOT_FOUND
             && rr->status != HTTP_OK) {
        error_notfound = rr->status;
    }

    clhy_destroy_sub_req(rr);
    if (error_notfound) {
        return error_notfound;
    }

    /* nothing for us to do, pass on through */
    return DECLINED;
}

static int fixup_dir(request_rec *r)
{
    dir_config_rec *d;
    char *dummy_ptr[1];
    char **names_ptr;
    int num_names;
    int error_notfound = 0;

    /* In case capi_mime wasn't present, and no handler was assigned. */
    if (!r->handler) {
        r->handler = DIR_MAGIC_TYPE;
    }

    /* Never tolerate path_info on dir requests */
    if (r->path_info && *r->path_info) {
        return DECLINED;
    }

    d = (dir_config_rec *)clhy_get_capi_config(r->per_dir_config,
                                               &dir_capi);

    /* Redirect requests that are not '/' terminated */
    if (r->uri[0] == '\0' || r->uri[strlen(r->uri) - 1] != '/')
    {
        char *ifile;

        if (!d->do_slash) {
            return DECLINED;
        }

        /* Only redirect non-get requests if we have no note to warn
         * that this browser cannot handle redirs on non-GET requests
         * (such as Microsoft's WebFolders).
         */
        if ((r->method_number != M_GET)
                && kuda_table_get(r->subprocess_env, "redirect-carefully")) {
            return DECLINED;
        }

        if (r->args != NULL) {
            ifile = kuda_pstrcat(r->pool, clhy_escape_uri(r->pool, r->uri),
                                "/?", r->args, NULL);
        }
        else {
            ifile = kuda_pstrcat(r->pool, clhy_escape_uri(r->pool, r->uri),
                                "/", NULL);
        }

        kuda_table_setn(r->headers_out, "Location",
                       clhy_contsruct_url(r->pool, ifile, r));
        return HTTP_MOVED_PERMANENTLY;
    }

    /* we're running between capi_rewrites fixup and its internal redirect handler, step aside */
    if (!strcmp(r->handler, REWRITE_REDIRECT_HANDLER_NAME)) { 
        /* Prevent DIR_MAGIC_TYPE from leaking out when someone has taken over */
        if (!strcmp(r->content_type, DIR_MAGIC_TYPE)) { 
            r->content_type = NULL;
        }
        return DECLINED;
    }

    if (d->checkhandler == MODDIR_ON && strcmp(r->handler, DIR_MAGIC_TYPE)) {
        /* Prevent DIR_MAGIC_TYPE from leaking out when someone has taken over */
        if (!strcmp(r->content_type, DIR_MAGIC_TYPE)) { 
            r->content_type = NULL;
        }
        return DECLINED;
    }

    if (d->index_names) {
        names_ptr = (char **)d->index_names->elts;
        num_names = d->index_names->nelts;
    }
    else {
        dummy_ptr[0] = CLHY_DEFAULT_INDEX;
        names_ptr = dummy_ptr;
        num_names = 1;
    }

    for (; num_names; ++names_ptr, --num_names) {
        /* XXX: Is this name_ptr considered escaped yet, or not??? */
        char *name_ptr = *names_ptr;
        request_rec *rr;

        /* Once upon a time args were handled _after_ the successful redirect.
         * But that redirect might then _refuse_ the given r->args, creating
         * a nasty tangle.  It seems safer to consider the r->args while we
         * determine if name_ptr is our viable index, and therefore set them
         * up correctly on redirect.
         */
        if (r->args != NULL) {
            name_ptr = kuda_pstrcat(r->pool, name_ptr, "?", r->args, NULL);
        }

        rr = clhy_sub_req_lookup_uri(name_ptr, r, r->output_filters);

        /* The sub request lookup is very liberal, and the core map_to_storage
         * handler will almost always result in HTTP_OK as /foo/index.html
         * may be /foo with PATH_INFO="/index.html", or even / with
         * PATH_INFO="/foo/index.html". To get around this we insist that the
         * the index be a regular filetype.
         *
         * Another reason is that the core handler also makes the assumption
         * that if r->finfo is still NULL by the time it gets called, the
         * file does not exist.
         */
        if (rr->status == HTTP_OK
            && (   (rr->handler && !strcmp(rr->handler, "proxy-server"))
                || rr->finfo.filetype == KUDA_REG)) {

            if (clhy_is_HTTP_REDIRECT(d->redirect_index)) {
                kuda_table_setn(r->headers_out, "Location", clhy_contsruct_url(r->pool, rr->uri, r));
                return d->redirect_index;
            }

            clhy_internal_fast_redirect(rr, r);
            return OK;
        }

        /* If the request returned a redirect, propagate it to the client */

        if (clhy_is_HTTP_REDIRECT(rr->status)
            || (rr->status == HTTP_NOT_ACCEPTABLE && num_names == 1)
            || (rr->status == HTTP_UNAUTHORIZED && num_names == 1)) {

            kuda_pool_join(r->pool, rr->pool);
            error_notfound = rr->status;
            r->notes = kuda_table_overlay(r->pool, r->notes, rr->notes);
            r->headers_out = kuda_table_overlay(r->pool, r->headers_out,
                                               rr->headers_out);
            r->err_headers_out = kuda_table_overlay(r->pool, r->err_headers_out,
                                                   rr->err_headers_out);
            return error_notfound;
        }

        /* If the request returned something other than 404 (or 200),
         * it means the cAPI encountered some sort of problem. To be
         * secure, we should return the error, rather than allow autoindex
         * to create a (possibly unsafe) directory index.
         *
         * So we store the error, and if none of the listed files
         * exist, we return the last error response we got, instead
         * of a directory listing.
         */
        if (rr->status && rr->status != HTTP_NOT_FOUND
                && rr->status != HTTP_OK) {
            error_notfound = rr->status;
        }

        clhy_destroy_sub_req(rr);
    }

    if (error_notfound) {
        return error_notfound;
    }

    /* record what we tried, mostly for the benefit of capi_autoindex */
    kuda_table_set(r->notes, "dir-index-names",
                  d->index_names ?
                  kuda_array_pstrcat(r->pool, d->index_names, ','):
                  CLHY_DEFAULT_INDEX);

    /* nothing for us to do, pass on through */
    return DECLINED;
}

static int dir_fixups(request_rec *r)
{
    if (r->finfo.filetype == KUDA_DIR) {
        /* serve up a directory */
        return fixup_dir(r);
    }
    else if ((r->finfo.filetype == KUDA_NOFILE) && (r->handler == NULL)) {
        /* No handler and nothing in the filesystem - use fallback */
        return fixup_dflt(r);
    }
    return DECLINED;
}

static void register_hooks(kuda_pool_t *p)
{
    clhy_hook_fixups(dir_fixups,NULL,NULL,KUDA_HOOK_LAST);
}

CLHY_DECLARE_CAPI(dir) = {
    STANDARD16_CAPI_STUFF,
    create_dir_config,          /* create per-directory config structure */
    merge_dir_configs,          /* merge per-directory config structures */
    NULL,                       /* create per-server config structure */
    NULL,                       /* merge per-server config structures */
    dir_cmds,                   /* command kuda_table_t */
    register_hooks              /* register hooks */
};
