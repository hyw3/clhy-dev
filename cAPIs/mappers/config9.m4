dnl cAPIs enabled in this directory by default

dnl CLHYKUDEL_CAPI(name, helptext[, objects[, structname[, default[, config]]]])

CLHYKUDEL_CAPIPATH_INIT(mappers)

CLHYKUDEL_CAPI(vhost_alias, mass virtual hosting cAPI, , , most)
CLHYKUDEL_CAPI(negotiation, content negotiation, , , most)
CLHYKUDEL_CAPI(dir, directory request handling, , , yes)
CLHYKUDEL_CAPI(imagemap, server-side imagemaps, , , no)
CLHYKUDEL_CAPI(actions, Action triggering on requests, , , most)
CLHYKUDEL_CAPI(speling, correct common URL misspellings, , , most)
CLHYKUDEL_CAPI(userdir, mapping of requests to user-specific directories, , , most)
CLHYKUDEL_CAPI(alias, mapping of requests to different filesystem parts, , , yes)
CLHYKUDEL_CAPI(rewrite, rule based URL manipulation, , , most)

KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

CLHYKUDEL_CAPIPATH_FINISH
