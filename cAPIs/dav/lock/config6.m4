dnl cAPIs enabled in this directory by default

CLHYKUDEL_CAPIPATH_INIT(dav/lock)

dav_lock_objects="capi_dav_lock.lo locks.lo"

case "$host" in
  *os2*)
    # OS2 DLLs must resolve all symbols at build time
    # and we need some from main DAV cAPI
    dav_lock_objects="$dav_lock_objects ../main/capi_dav.la"
    ;;
esac

CLHYKUDEL_CAPI(dav_lock, DAV provider for generic locking, $dav_lock_objects, ,,,dav)

CLHYKUDEL_CAPIPATH_FINISH
