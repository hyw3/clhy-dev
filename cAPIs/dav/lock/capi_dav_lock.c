/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_config.h"
#include "kuda_strings.h"
#include "clhy_provider.h"

#include "capi_dav.h"
#include "locks.h"

/* per-dir configuration */
typedef struct {
    const char *lockdb_path;
} dav_lock_dir_conf;

extern const dav_hooks_locks dav_hooks_locks_generic;

extern cAPI CLHY_CAPI_DECLARE_DATA dav_lock_capi;

const char *dav_generic_get_lockdb_path(const request_rec *r)
{
    dav_lock_dir_conf *conf;

    conf = clhy_get_capi_config(r->per_dir_config, &dav_lock_capi);
    return conf->lockdb_path;
}

static void *dav_lock_create_dir_config(kuda_pool_t *p, char *dir)
{
    return kuda_pcalloc(p, sizeof(dav_lock_dir_conf));
}

static void *dav_lock_merge_dir_config(kuda_pool_t *p,
                                       void *base, void *overrides)
{
    dav_lock_dir_conf *parent = base;
    dav_lock_dir_conf *child = overrides;
    dav_lock_dir_conf *newconf;

    newconf = kuda_pcalloc(p, sizeof(*newconf));

    newconf->lockdb_path =
        child->lockdb_path ? child->lockdb_path : parent->lockdb_path;

    return newconf;
}

/*
 * Command handler for the DAVGenericLockDB directive, which is TAKE1
 */
static const char *dav_lock_cmd_davlockdb(cmd_parms *cmd, void *config,
                                        const char *arg1)
{
    dav_lock_dir_conf *conf = config;

    conf->lockdb_path = clhy_server_root_relative(cmd->pool, arg1);

    if (!conf->lockdb_path) {
        return kuda_pstrcat(cmd->pool, "Invalid DAVGenericLockDB path ",
                           arg1, NULL);
    }

    return NULL;
}

static const command_rec dav_lock_cmds[] =
{
    /* per server */
    CLHY_INIT_TAKE1("DAVGenericLockDB", dav_lock_cmd_davlockdb, NULL, ACCESS_CONF,
                  "specify a lock database"),

    { NULL }
};

static void register_hooks(kuda_pool_t *p)
{
    clhy_register_provider(p, "dav-lock", "generic", "0",
                         &dav_hooks_locks_generic);
}

CLHY_DECLARE_CAPI(dav_lock) =
{
    STANDARD16_CAPI_STUFF,
    dav_lock_create_dir_config,     /* dir config creater */
    dav_lock_merge_dir_config,      /* dir merger --- default is to override */
    NULL,                           /* server config */
    NULL,                           /* merge server config */
    dav_lock_cmds,                  /* command table */
    register_hooks,                 /* register hooks */
};
