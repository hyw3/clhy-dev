dnl cAPIs enabled in this directory by default

CLHYKUDEL_CAPIPATH_INIT(dav/fs)

dav_fs_objects="capi_dav_fs.lo dbm.lo lock.lo repos.lo"

if test "x$enable_dav" != "x"; then
  dav_fs_enable=$enable_dav
else
  dav_fs_enable=$dav_enable
fi

case "$host" in
  *os2*)
    # OS2 DLLs must resolve all symbols at build time
    # and we need some from main DAV cAPI
    dav_fs_objects="$dav_fs_objects ../main/capi_dav.la"
    ;;
esac

CLHYKUDEL_CAPI(dav_fs, DAV provider for the filesystem.  --enable-dav also enables capi_dav_fs., $dav_fs_objects, , $dav_fs_enable,,dav)

CLHYKUDEL_CAPIPATH_FINISH
