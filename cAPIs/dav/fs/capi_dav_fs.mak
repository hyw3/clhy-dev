# Microsoft Developer Studio Generated NMAKE File, Based on capi_dav_fs.dsp
!IF "$(CFG)" == ""
CFG=capi_dav_fs - Win32 Release
!MESSAGE No configuration specified. Defaulting to capi_dav_fs - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "capi_dav_fs - Win32 Release" && "$(CFG)" != "capi_dav_fs - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "capi_dav_fs.mak" CFG="capi_dav_fs - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "capi_dav_fs - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "capi_dav_fs - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "capi_dav_fs - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_dav_fs.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "capi_dav - Win32 Release" "libwwhy - Win32 Release" "libkudadelman - Win32 Release" "libkuda - Win32 Release" "$(OUTDIR)\capi_dav_fs.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudadelman - Win32 ReleaseCLEAN" "libwwhy - Win32 ReleaseCLEAN" "capi_dav - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\dbm.obj"
	-@erase "$(INTDIR)\lock.obj"
	-@erase "$(INTDIR)\capi_dav_fs.obj"
	-@erase "$(INTDIR)\capi_dav_fs.res"
	-@erase "$(INTDIR)\capi_dav_fs_src.idb"
	-@erase "$(INTDIR)\capi_dav_fs_src.pdb"
	-@erase "$(INTDIR)\repos.obj"
	-@erase "$(OUTDIR)\capi_dav_fs.exp"
	-@erase "$(OUTDIR)\capi_dav_fs.lib"
	-@erase "$(OUTDIR)\capi_dav_fs.pdb"
	-@erase "$(OUTDIR)\capi_dav_fs.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../../../include" /I "../../../kudelrunsrc/kuda/include" /I "../../../kudelrunsrc/kuda-delman/include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_dav_fs_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_dav_fs.res" /i "../../../include" /i "../../../kudelrunsrc/kuda/include" /d "NDEBUG" /d BIN_NAME="capi_dav_fs.so" /d LONG_NAME="dav_fs_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_dav_fs.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib ws2_32.lib mswsock.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_dav_fs.pdb" /debug /out:"$(OUTDIR)\capi_dav_fs.so" /implib:"$(OUTDIR)\capi_dav_fs.lib" /base:@..\..\..\platforms\win32\BaseAddr.ref,capi_dav_fs.so /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\dbm.obj" \
	"$(INTDIR)\lock.obj" \
	"$(INTDIR)\capi_dav_fs.obj" \
	"$(INTDIR)\repos.obj" \
	"$(INTDIR)\capi_dav_fs.res" \
	"..\..\..\kudelrunsrc\kuda\Release\libkuda-1.lib" \
	"..\..\..\kudelrunsrc\kuda-delman\Release\libkudadelman-1.lib" \
	"..\..\..\Release\libwwhy.lib" \
	"..\main\Release\capi_dav.lib"

"$(OUTDIR)\capi_dav_fs.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\capi_dav_fs.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_dav_fs.so"
   if exist .\Release\capi_dav_fs.so.manifest mt.exe -manifest .\Release\capi_dav_fs.so.manifest -outputresource:.\Release\capi_dav_fs.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "capi_dav_fs - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_dav_fs.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "capi_dav - Win32 Debug" "libwwhy - Win32 Debug" "libkudadelman - Win32 Debug" "libkuda - Win32 Debug" "$(OUTDIR)\capi_dav_fs.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudadelman - Win32 DebugCLEAN" "libwwhy - Win32 DebugCLEAN" "capi_dav - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\dbm.obj"
	-@erase "$(INTDIR)\lock.obj"
	-@erase "$(INTDIR)\capi_dav_fs.obj"
	-@erase "$(INTDIR)\capi_dav_fs.res"
	-@erase "$(INTDIR)\capi_dav_fs_src.idb"
	-@erase "$(INTDIR)\capi_dav_fs_src.pdb"
	-@erase "$(INTDIR)\repos.obj"
	-@erase "$(OUTDIR)\capi_dav_fs.exp"
	-@erase "$(OUTDIR)\capi_dav_fs.lib"
	-@erase "$(OUTDIR)\capi_dav_fs.pdb"
	-@erase "$(OUTDIR)\capi_dav_fs.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../../../include" /I "../../../kudelrunsrc/kuda/include" /I "../../../kudelrunsrc/kuda-delman/include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_dav_fs_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_dav_fs.res" /i "../../../include" /i "../../../kudelrunsrc/kuda/include" /d "_DEBUG" /d BIN_NAME="capi_dav_fs.so" /d LONG_NAME="dav_fs_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_dav_fs.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib ws2_32.lib mswsock.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_dav_fs.pdb" /debug /out:"$(OUTDIR)\capi_dav_fs.so" /implib:"$(OUTDIR)\capi_dav_fs.lib" /base:@..\..\..\platforms\win32\BaseAddr.ref,capi_dav_fs.so 
LINK32_OBJS= \
	"$(INTDIR)\dbm.obj" \
	"$(INTDIR)\lock.obj" \
	"$(INTDIR)\capi_dav_fs.obj" \
	"$(INTDIR)\repos.obj" \
	"$(INTDIR)\capi_dav_fs.res" \
	"..\..\..\kudelrunsrc\kuda\Debug\libkuda-1.lib" \
	"..\..\..\kudelrunsrc\kuda-delman\Debug\libkudadelman-1.lib" \
	"..\..\..\Debug\libwwhy.lib" \
	"..\main\Debug\capi_dav.lib"

"$(OUTDIR)\capi_dav_fs.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\capi_dav_fs.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_dav_fs.so"
   if exist .\Debug\capi_dav_fs.so.manifest mt.exe -manifest .\Debug\capi_dav_fs.so.manifest -outputresource:.\Debug\capi_dav_fs.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("capi_dav_fs.dep")
!INCLUDE "capi_dav_fs.dep"
!ELSE 
!MESSAGE Warning: cannot find "capi_dav_fs.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "capi_dav_fs - Win32 Release" || "$(CFG)" == "capi_dav_fs - Win32 Debug"
SOURCE=.\dbm.c

"$(INTDIR)\dbm.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\lock.c

"$(INTDIR)\lock.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\capi_dav_fs.c

"$(INTDIR)\capi_dav_fs.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\repos.c

"$(INTDIR)\repos.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "capi_dav_fs - Win32 Release"

"libkuda - Win32 Release" : 
   cd ".\..\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\..\cAPIs\dav\fs"

"libkuda - Win32 ReleaseCLEAN" : 
   cd ".\..\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\dav\fs"

!ELSEIF  "$(CFG)" == "capi_dav_fs - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd ".\..\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\..\cAPIs\dav\fs"

"libkuda - Win32 DebugCLEAN" : 
   cd ".\..\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\dav\fs"

!ENDIF 

!IF  "$(CFG)" == "capi_dav_fs - Win32 Release"

"libkudadelman - Win32 Release" : 
   cd ".\..\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" 
   cd "..\..\cAPIs\dav\fs"

"libkudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\dav\fs"

!ELSEIF  "$(CFG)" == "capi_dav_fs - Win32 Debug"

"libkudadelman - Win32 Debug" : 
   cd ".\..\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" 
   cd "..\..\cAPIs\dav\fs"

"libkudadelman - Win32 DebugCLEAN" : 
   cd ".\..\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\dav\fs"

!ENDIF 

!IF  "$(CFG)" == "capi_dav_fs - Win32 Release"

"libwwhy - Win32 Release" : 
   cd ".\..\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" 
   cd ".\cAPIs\dav\fs"

"libwwhy - Win32 ReleaseCLEAN" : 
   cd ".\..\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" RECURSE=1 CLEAN 
   cd ".\cAPIs\dav\fs"

!ELSEIF  "$(CFG)" == "capi_dav_fs - Win32 Debug"

"libwwhy - Win32 Debug" : 
   cd ".\..\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" 
   cd ".\cAPIs\dav\fs"

"libwwhy - Win32 DebugCLEAN" : 
   cd ".\..\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" RECURSE=1 CLEAN 
   cd ".\cAPIs\dav\fs"

!ENDIF 

!IF  "$(CFG)" == "capi_dav_fs - Win32 Release"

"capi_dav - Win32 Release" : 
   cd ".\..\main"
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_dav.mak" CFG="capi_dav - Win32 Release" 
   cd "..\fs"

"capi_dav - Win32 ReleaseCLEAN" : 
   cd ".\..\main"
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_dav.mak" CFG="capi_dav - Win32 Release" RECURSE=1 CLEAN 
   cd "..\fs"

!ELSEIF  "$(CFG)" == "capi_dav_fs - Win32 Debug"

"capi_dav - Win32 Debug" : 
   cd ".\..\main"
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_dav.mak" CFG="capi_dav - Win32 Debug" 
   cd "..\fs"

"capi_dav - Win32 DebugCLEAN" : 
   cd ".\..\main"
   $(MAKE) /$(MAKEFLAGS) /F ".\capi_dav.mak" CFG="capi_dav - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\fs"

!ENDIF 

SOURCE=..\..\..\build\win32\wwhy.rc

!IF  "$(CFG)" == "capi_dav_fs - Win32 Release"


"$(INTDIR)\capi_dav_fs.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_dav_fs.res" /i "../../../include" /i "../../../kudelrunsrc/kuda/include" /i "../../../build\win32" /d "NDEBUG" /d BIN_NAME="capi_dav_fs.so" /d LONG_NAME="dav_fs_capi for cLHy" $(SOURCE)


!ELSEIF  "$(CFG)" == "capi_dav_fs - Win32 Debug"


"$(INTDIR)\capi_dav_fs.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_dav_fs.res" /i "../../../include" /i "../../../kudelrunsrc/kuda/include" /i "../../../build\win32" /d "_DEBUG" /d BIN_NAME="capi_dav_fs.so" /d LONG_NAME="dav_fs_capi for cLHy" $(SOURCE)


!ENDIF 


!ENDIF 

