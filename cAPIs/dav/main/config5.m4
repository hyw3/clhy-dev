dnl cAPIs enabled in this directory by default

CLHYKUDEL_CAPIPATH_INIT(dav/main)

dav_objects="capi_dav.lo props.lo util.lo util_lock.lo liveprop.lo providers.lo std_liveprop.lo"

if test "$enable_http" = "no"; then
  dav_enable=no
else
  dav_enable=most
fi

CLHYKUDEL_CAPI(dav, WebDAV protocol handling.  --enable-dav also enables capi_dav_fs, $dav_objects, , $dav_enable)

if test "$dav_enable" != "no" -o "$enable_dav" != "no"; then
  clhy_need_expat=yes
fi

KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

CLHYKUDEL_CAPIPATH_FINISH
