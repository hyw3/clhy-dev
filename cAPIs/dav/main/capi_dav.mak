# Microsoft Developer Studio Generated NMAKE File, Based on capi_dav.dsp
!IF "$(CFG)" == ""
CFG=capi_dav - Win32 Release
!MESSAGE No configuration specified. Defaulting to capi_dav - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "capi_dav - Win32 Release" && "$(CFG)" != "capi_dav - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "capi_dav.mak" CFG="capi_dav - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "capi_dav - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "capi_dav - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "capi_dav - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_dav.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Release" "libkudadelman - Win32 Release" "libkuda - Win32 Release" "$(OUTDIR)\capi_dav.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudadelman - Win32 ReleaseCLEAN" "libwwhy - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\liveprop.obj"
	-@erase "$(INTDIR)\capi_dav.obj"
	-@erase "$(INTDIR)\capi_dav.res"
	-@erase "$(INTDIR)\capi_dav_src.idb"
	-@erase "$(INTDIR)\capi_dav_src.pdb"
	-@erase "$(INTDIR)\props.obj"
	-@erase "$(INTDIR)\providers.obj"
	-@erase "$(INTDIR)\std_liveprop.obj"
	-@erase "$(INTDIR)\util.obj"
	-@erase "$(INTDIR)\util_lock.obj"
	-@erase "$(OUTDIR)\capi_dav.exp"
	-@erase "$(OUTDIR)\capi_dav.lib"
	-@erase "$(OUTDIR)\capi_dav.pdb"
	-@erase "$(OUTDIR)\capi_dav.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../../../include" /I "../../../kudelrunsrc/kuda/include" /I "../../../kudelrunsrc/kuda-delman/include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "DAV_DECLARE_EXPORT" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_dav_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_dav.res" /i "../../../include" /i "../../../kudelrunsrc/kuda/include" /d "NDEBUG" /d BIN_NAME="capi_dav.so" /d LONG_NAME="dav_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_dav.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib ws2_32.lib mswsock.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_dav.pdb" /debug /out:"$(OUTDIR)\capi_dav.so" /implib:"$(OUTDIR)\capi_dav.lib" /base:@..\..\..\platforms\win32\BaseAddr.ref,capi_dav.so /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\liveprop.obj" \
	"$(INTDIR)\capi_dav.obj" \
	"$(INTDIR)\props.obj" \
	"$(INTDIR)\providers.obj" \
	"$(INTDIR)\std_liveprop.obj" \
	"$(INTDIR)\util.obj" \
	"$(INTDIR)\util_lock.obj" \
	"$(INTDIR)\capi_dav.res" \
	"..\..\..\kudelrunsrc\kuda\Release\libkuda-1.lib" \
	"..\..\..\kudelrunsrc\kuda-delman\Release\libkudadelman-1.lib" \
	"..\..\..\Release\libwwhy.lib"

"$(OUTDIR)\capi_dav.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\capi_dav.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_dav.so"
   if exist .\Release\capi_dav.so.manifest mt.exe -manifest .\Release\capi_dav.so.manifest -outputresource:.\Release\capi_dav.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "capi_dav - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_dav.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Debug" "libkudadelman - Win32 Debug" "libkuda - Win32 Debug" "$(OUTDIR)\capi_dav.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudadelman - Win32 DebugCLEAN" "libwwhy - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\liveprop.obj"
	-@erase "$(INTDIR)\capi_dav.obj"
	-@erase "$(INTDIR)\capi_dav.res"
	-@erase "$(INTDIR)\capi_dav_src.idb"
	-@erase "$(INTDIR)\capi_dav_src.pdb"
	-@erase "$(INTDIR)\props.obj"
	-@erase "$(INTDIR)\providers.obj"
	-@erase "$(INTDIR)\std_liveprop.obj"
	-@erase "$(INTDIR)\util.obj"
	-@erase "$(INTDIR)\util_lock.obj"
	-@erase "$(OUTDIR)\capi_dav.exp"
	-@erase "$(OUTDIR)\capi_dav.lib"
	-@erase "$(OUTDIR)\capi_dav.pdb"
	-@erase "$(OUTDIR)\capi_dav.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../../../include" /I "../../../kudelrunsrc/kuda/include" /I "../../../kudelrunsrc/kuda-delman/include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "DAV_DECLARE_EXPORT" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_dav_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\capi_dav.res" /i "../../../include" /i "../../../kudelrunsrc/kuda/include" /d "_DEBUG" /d BIN_NAME="capi_dav.so" /d LONG_NAME="dav_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_dav.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib ws2_32.lib mswsock.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_dav.pdb" /debug /out:"$(OUTDIR)\capi_dav.so" /implib:"$(OUTDIR)\capi_dav.lib" /base:@..\..\..\platforms\win32\BaseAddr.ref,capi_dav.so 
LINK32_OBJS= \
	"$(INTDIR)\liveprop.obj" \
	"$(INTDIR)\capi_dav.obj" \
	"$(INTDIR)\props.obj" \
	"$(INTDIR)\providers.obj" \
	"$(INTDIR)\std_liveprop.obj" \
	"$(INTDIR)\util.obj" \
	"$(INTDIR)\util_lock.obj" \
	"$(INTDIR)\capi_dav.res" \
	"..\..\..\kudelrunsrc\kuda\Debug\libkuda-1.lib" \
	"..\..\..\kudelrunsrc\kuda-delman\Debug\libkudadelman-1.lib" \
	"..\..\..\Debug\libwwhy.lib"

"$(OUTDIR)\capi_dav.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\capi_dav.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_dav.so"
   if exist .\Debug\capi_dav.so.manifest mt.exe -manifest .\Debug\capi_dav.so.manifest -outputresource:.\Debug\capi_dav.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("capi_dav.dep")
!INCLUDE "capi_dav.dep"
!ELSE 
!MESSAGE Warning: cannot find "capi_dav.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "capi_dav - Win32 Release" || "$(CFG)" == "capi_dav - Win32 Debug"
SOURCE=.\liveprop.c

"$(INTDIR)\liveprop.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\capi_dav.c

"$(INTDIR)\capi_dav.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\props.c

"$(INTDIR)\props.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\providers.c

"$(INTDIR)\providers.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\std_liveprop.c

"$(INTDIR)\std_liveprop.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\util.c

"$(INTDIR)\util.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\util_lock.c

"$(INTDIR)\util_lock.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "capi_dav - Win32 Release"

"libkuda - Win32 Release" : 
   cd ".\..\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\..\cAPIs\dav\main"

"libkuda - Win32 ReleaseCLEAN" : 
   cd ".\..\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\dav\main"

!ELSEIF  "$(CFG)" == "capi_dav - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd ".\..\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\..\cAPIs\dav\main"

"libkuda - Win32 DebugCLEAN" : 
   cd ".\..\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\dav\main"

!ENDIF 

!IF  "$(CFG)" == "capi_dav - Win32 Release"

"libkudadelman - Win32 Release" : 
   cd ".\..\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" 
   cd "..\..\cAPIs\dav\main"

"libkudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\dav\main"

!ELSEIF  "$(CFG)" == "capi_dav - Win32 Debug"

"libkudadelman - Win32 Debug" : 
   cd ".\..\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" 
   cd "..\..\cAPIs\dav\main"

"libkudadelman - Win32 DebugCLEAN" : 
   cd ".\..\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\dav\main"

!ENDIF 

!IF  "$(CFG)" == "capi_dav - Win32 Release"

"libwwhy - Win32 Release" : 
   cd ".\..\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" 
   cd ".\cAPIs\dav\main"

"libwwhy - Win32 ReleaseCLEAN" : 
   cd ".\..\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" RECURSE=1 CLEAN 
   cd ".\cAPIs\dav\main"

!ELSEIF  "$(CFG)" == "capi_dav - Win32 Debug"

"libwwhy - Win32 Debug" : 
   cd ".\..\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" 
   cd ".\cAPIs\dav\main"

"libwwhy - Win32 DebugCLEAN" : 
   cd ".\..\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" RECURSE=1 CLEAN 
   cd ".\cAPIs\dav\main"

!ENDIF 

SOURCE=..\..\..\build\win32\wwhy.rc

!IF  "$(CFG)" == "capi_dav - Win32 Release"


"$(INTDIR)\capi_dav.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_dav.res" /i "../../../include" /i "../../../kudelrunsrc/kuda/include" /i "../../../build\win32" /d "NDEBUG" /d BIN_NAME="capi_dav.so" /d LONG_NAME="dav_capi for cLHy" $(SOURCE)


!ELSEIF  "$(CFG)" == "capi_dav - Win32 Debug"


"$(INTDIR)\capi_dav.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\capi_dav.res" /i "../../../include" /i "../../../kudelrunsrc/kuda/include" /i "../../../build\win32" /d "_DEBUG" /d BIN_NAME="capi_dav.so" /d LONG_NAME="dav_capi for cLHy" $(SOURCE)


!ENDIF 


!ENDIF 

