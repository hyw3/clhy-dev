/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_pools.h"
#include "kuda_hash.h"
#include "clhy_provider.h"
#include "capi_dav.h"

#define DAV_PROVIDER_GROUP "dav"

DAV_DECLARE(void) dav_register_provider(kuda_pool_t *p, const char *name,
                                        const dav_provider *provider)
{
    clhy_register_provider(p, DAV_PROVIDER_GROUP, name, "0", provider);
}

DAV_DECLARE(const dav_provider *) dav_lookup_provider(const char *name)
{
    return clhy_lookup_provider(DAV_PROVIDER_GROUP, name, "0");
}

DAV_DECLARE(void) dav_options_provider_register(kuda_pool_t *p,
                        const char *name,
                        const dav_options_provider *provider)
{
    clhy_register_provider(p, DAV_OPTIONS_EXTENSION_GROUP, name, "0", provider);
}

DAV_DECLARE(const dav_options_provider *) dav_get_options_providers(const char *name)
{
    return clhy_lookup_provider(DAV_OPTIONS_EXTENSION_GROUP, name, "0");
}


DAV_DECLARE(void) dav_resource_type_provider_register(kuda_pool_t *p,
                          const char *name,
                          const dav_resource_type_provider *provider)
{
    clhy_register_provider(p, DAV_RESOURCE_TYPE_GROUP, name, "0", provider);
}

DAV_DECLARE(const dav_resource_type_provider *) dav_get_resource_type_providers(const char *name)
{
    return clhy_lookup_provider(DAV_RESOURCE_TYPE_GROUP, name, "0");
}
