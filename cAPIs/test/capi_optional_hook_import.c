/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "capi_optional_hook_export.h"

static int ImportOptionalHookTestHook(const char *szStr)
{
    clhy_log_error(CLHYLOG_MARK,CLHYLOG_ERR,OK,NULL, CLHYLOGNO(01866)"Optional hook test said: %s",
                 szStr);

    return OK;
}

static void ImportRegisterHooks(kuda_pool_t *p)
{
    CLHY_OPTIONAL_HOOK(optional_hook_test,ImportOptionalHookTestHook,NULL,
                     NULL,KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(optional_hook_import) =
{
    STANDARD16_CAPI_STUFF,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    ImportRegisterHooks
};
