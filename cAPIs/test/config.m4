
CLHYKUDEL_CAPIPATH_INIT(test)

CLHYKUDEL_CAPI(optional_hook_export, example optional hook exporter, , , no)
CLHYKUDEL_CAPI(optional_hook_import, example optional hook importer, , , no)
CLHYKUDEL_CAPI(optional_fn_import, example optional function importer, , , no)
CLHYKUDEL_CAPI(optional_fn_export, example optional function exporter, , , no)

CLHYKUDEL_CAPI(dialup, rate limits static files to dialup modem speeds, , , )

KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

CLHYKUDEL_CAPIPATH_FINISH
