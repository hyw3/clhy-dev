/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_config.h"
#include "capi_optional_fn_export.h"
#include "http_protocol.h"

/* The alert will note a strange mirror-image style resemblance to
 * capi_optional_hook_export.c. Yes, I _did_ mean export. Think about it.
 */

static KUDA_OPTIONAL_FN_TYPE(TestOptionalFn) *pfn;

static int ImportLogTransaction(request_rec *r)
{
    if (pfn)
        return pfn(r->the_request);
    return DECLINED;
}

static void ImportFnRetrieve(void)
{
    pfn = KUDA_RETRIEVE_OPTIONAL_FN(TestOptionalFn);
}

static void ImportRegisterHooks(kuda_pool_t *p)
{
    clhy_hook_log_transaction(ImportLogTransaction,NULL,NULL,KUDA_HOOK_MIDDLE);
    clhy_hook_optional_fn_retrieve(ImportFnRetrieve,NULL,NULL,KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(optional_fn_import) =
{
    STANDARD16_CAPI_STUFF,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    ImportRegisterHooks
};
