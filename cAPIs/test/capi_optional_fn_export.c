/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "capi_optional_fn_export.h"

/* The alert will note a strange mirror-image style resemblance to
 * capi_optional_hook_import.c. Yes, I _did_ mean import. Think about it.
 */

static int TestOptionalFn(const char *szStr)
{
    clhy_log_error(CLHYLOG_MARK,CLHYLOG_ERR,OK,NULL, CLHYLOGNO(01871)
                 "Optional function test said: %s",szStr);

    return OK;
}

static void ExportRegisterHooks(kuda_pool_t *p)
{
    KUDA_REGISTER_OPTIONAL_FN(TestOptionalFn);
}

CLHY_DECLARE_CAPI(optional_fn_export) =
{
    STANDARD16_CAPI_STUFF,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    ExportRegisterHooks
};
