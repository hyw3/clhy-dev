/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_config.h"
#include "capi_optional_hook_export.h"
#include "http_protocol.h"

CLHY_IMPLEMENT_OPTIONAL_HOOK_RUN_ALL(int,optional_hook_test,(const char *szStr),
                                    (szStr),OK,DECLINED)

static int ExportLogTransaction(request_rec *r)
{
    return clhy_run_optional_hook_test(r->the_request);
}

static void ExportRegisterHooks(kuda_pool_t *p)
{
    clhy_hook_log_transaction(ExportLogTransaction,NULL,NULL,KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(optional_hook_export) =
{
    STANDARD16_CAPI_STUFF,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    ExportRegisterHooks
};
