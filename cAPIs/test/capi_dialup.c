/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



#include "wwhy.h"
#include "http_core.h"

#include "util_filter.h"
#include "http_log.h"
#include "http_config.h"
#include "http_request.h"
#include "http_protocol.h"



#include "clhy_core.h"

cAPI CLHY_CAPI_DECLARE_DATA dialup_capi;

typedef struct dialup_dcfg_t {
    kuda_size_t bytes_per_second;
} dialup_dcfg_t;

typedef struct dialup_baton_t {
    kuda_size_t bytes_per_second;
    request_rec *r;
    kuda_file_t *fd;
    kuda_bucket_brigade *bb;
    kuda_bucket_brigade *tmpbb;
} dialup_baton_t;

static int
dialup_send_pulse(dialup_baton_t *db)
{
    int status;
    kuda_off_t len = 0;
    kuda_size_t bytes_sent = 0;

    while (!KUDA_BRIGADE_EMPTY(db->bb) && bytes_sent < db->bytes_per_second) {
        kuda_bucket *e;

        if (db->r->connection->aborted) {
            return HTTP_INTERNAL_SERVER_ERROR;
        }

        status = kuda_brigade_partition(db->bb, db->bytes_per_second, &e);

        if (status != KUDA_SUCCESS && status != KUDA_INCOMPLETE) {
            /* XXXXXX: Log me. */
            return HTTP_INTERNAL_SERVER_ERROR;
        }

        if (e != KUDA_BRIGADE_SENTINEL(db->bb)) {
            kuda_bucket *f;
            kuda_bucket *b = KUDA_BUCKET_PREV(e);
            f = KUDA_RING_FIRST(&db->bb->list);
            KUDA_RING_UNSPLICE(f, b, link);
            KUDA_RING_SPLICE_HEAD(&db->tmpbb->list, f, b, kuda_bucket, link);
        }
        else {
            KUDA_BRIGADE_CONCAT(db->tmpbb, db->bb);
        }

        e = kuda_bucket_flush_create(db->r->connection->bucket_alloc);

        KUDA_BRIGADE_INSERT_TAIL(db->tmpbb, e);

        kuda_brigade_length(db->tmpbb, 1, &len);
        bytes_sent += len;
        status = clhy_pass_brigade(db->r->output_filters, db->tmpbb);

        kuda_brigade_cleanup(db->tmpbb);

        if (status != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, status, db->r, CLHYLOGNO(01867)
                          "dialup: pulse: clhy_pass_brigade failed:");
            return CLHY_FILTER_ERROR;
        }
    }

    if (KUDA_BRIGADE_EMPTY(db->bb)) {
        return DONE;
    }
    else {
        return SUSPENDED;
    }
}

static void
dialup_callback(void *baton)
{
    int status;
    dialup_baton_t *db = (dialup_baton_t *)baton;

    kuda_thread_mutex_lock(db->r->invoke_mtx);

    status = dialup_send_pulse(db);

    if (status == SUSPENDED) {
        clhy_clmp_register_timed_callback(kuda_time_from_sec(1), dialup_callback, baton);
    }
    else if (status == DONE) {
        kuda_thread_mutex_unlock(db->r->invoke_mtx);
        clhy_finalize_request_protocol(db->r);
        clhy_process_request_after_handler(db->r);
        return;
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, db->r, CLHYLOGNO(01868)
                      "dialup: pulse returned: %d", status);
        db->r->status = HTTP_OK;
        clhy_die(status, db->r);
    }

    kuda_thread_mutex_unlock(db->r->invoke_mtx);
}

static int
dialup_handler(request_rec *r)
{
    int status;
    kuda_status_t rv;
    dialup_dcfg_t *dcfg;
    core_dir_config *ccfg;
    kuda_file_t *fd;
    dialup_baton_t *db;
    kuda_bucket *e;


    /* See core.c, default handler for all of the cases we just decline. */
    if (r->method_number != M_GET ||
        r->finfo.filetype == KUDA_NOFILE ||
        r->finfo.filetype == KUDA_DIR) {
        return DECLINED;
    }

    dcfg = clhy_get_capi_config(r->per_dir_config,
                                &dialup_capi);

    if (dcfg->bytes_per_second == 0) {
        return DECLINED;
    }

    ccfg = clhy_get_core_capi_config(r->per_dir_config);


    rv = kuda_file_open(&fd, r->filename, KUDA_READ | KUDA_BINARY
#if KUDA_HAS_SENDFILE
                           | CLHY_SENDFILE_ENABLED(ccfg->enable_sendfile)
#endif
                       , 0, r->pool);

    if (rv) {
        return DECLINED;
    }

    /* copied from default handler: */
    clhy_update_mtime(r, r->finfo.mtime);
    clhy_set_last_modified(r);
    clhy_set_etag(r);
    clhy_set_accept_ranges(r);
    clhy_set_content_length(r, r->finfo.size);

    status = clhy_meets_conditions(r);
    if (status != OK) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01869)
                      "dialup: declined, meets conditions, good luck core handler");
        return DECLINED;
    }

    db = kuda_palloc(r->pool, sizeof(dialup_baton_t));

    db->bb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
    db->tmpbb = kuda_brigade_create(r->pool, r->connection->bucket_alloc);

    e = kuda_brigade_insert_file(db->bb, fd, 0, r->finfo.size, r->pool);

#if KUDA_HAS_MMAP
    if (ccfg->enable_mmap == ENABLE_MMAP_OFF) {
        kuda_bucket_file_enable_mmap(e, 0);
    }
#endif


    db->bytes_per_second = dcfg->bytes_per_second;
    db->r = r;
    db->fd = fd;

    e = kuda_bucket_eos_create(r->connection->bucket_alloc);

    KUDA_BRIGADE_INSERT_TAIL(db->bb, e);

    status = dialup_send_pulse(db);
    if (status != SUSPENDED && status != DONE) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01870)
                      "dialup: failed, send pulse");
        return status;
    }

    clhy_clmp_register_timed_callback(kuda_time_from_sec(1), dialup_callback, db);

    return SUSPENDED;
}



#ifndef KUDA_HOOK_ALMOST_LAST
#define KUDA_HOOK_ALMOST_LAST (KUDA_HOOK_REALLY_LAST - 1)
#endif

static void
dialup_register_hooks(kuda_pool_t *p)
{
    clhy_hook_handler(dialup_handler, NULL, NULL, KUDA_HOOK_ALMOST_LAST);
}

typedef struct modem_speed_t {
    const char *name;
    kuda_size_t bytes_per_second;
} modem_speed_t;

#ifndef BITRATE_TO_BYTES
#define BITRATE_TO_BYTES(x) ((1000 * x)/8)
#endif

static const modem_speed_t modem_bitrates[] =
{
    {"V.21",    BITRATE_TO_BYTES(0.1)},
    {"V.26bis", BITRATE_TO_BYTES(2.4)},
    {"V.32",    BITRATE_TO_BYTES(9.6)},
    {"V.34",    BITRATE_TO_BYTES(28.8)},
    {"V.92",    BITRATE_TO_BYTES(56.0)},
    {"i-was-rich-and-got-a-leased-line", BITRATE_TO_BYTES(1500)},
    {NULL, 0}
};

static const char *
cmd_modem_standard(cmd_parms *cmd,
             void *dconf,
             const char *input)
{
    const modem_speed_t *standard;
    int i = 0;
    dialup_dcfg_t *dcfg = (dialup_dcfg_t*)dconf;

    dcfg->bytes_per_second = 0;

    while (modem_bitrates[i].name != NULL) {
        standard = &modem_bitrates[i];
        if (strcasecmp(standard->name, input) == 0) {
            dcfg->bytes_per_second = standard->bytes_per_second;
            break;
        }
        i++;
    }

    if (dcfg->bytes_per_second == 0) {
        return "capi_diaulup: Unkonwn Modem Standard specified.";
    }

    return NULL;
}

static void *
dialup_dcfg_create(kuda_pool_t *p, char *dummy)
{
    dialup_dcfg_t *cfg = kuda_palloc(p, sizeof(dialup_dcfg_t));

    cfg->bytes_per_second = 0;

    return cfg;
}


static const command_rec dialup_cmds[] =
{
    CLHY_INIT_TAKE1("ModemStandard", cmd_modem_standard, NULL, ACCESS_CONF,
                  "Modem Standard to.. simulate. "
                  "Must be one of: 'V.21', 'V.26bis', 'V.32', 'V.34', or 'V.92'"),
    {NULL}
};

CLHY_DECLARE_CAPI(dialup) =
{
    STANDARD16_CAPI_STUFF,
    dialup_dcfg_create,
    NULL,
    NULL,
    NULL,
    dialup_cmds,
    dialup_register_hooks
};
