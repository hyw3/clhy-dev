/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * capi_ident: Handle RFC 1413 ident request
 * obtained from rfc1413.c
 *
 * rfc1413() speaks a common subset of the RFC 1413, AUTH, TAP and IDENT
 * protocols. The code queries an RFC 1413 etc. compatible daemon on a remote
 * host to look up the owner of a connection. The information should not be
 * used for authentication purposes. This routine intercepts alarm signals.
 *
 * Author: Wietse Venema, Eindhoven University of Technology,
 * The Netherlands.
 */

/* Some small additions for cLHy --- ditch the "sccsid" var if
 * compiling with gcc (it *has* changed), include clhy_config.h for the
 * prototypes it defines on at least one system (SunlOSs) which has
 * them missing from the standard header files, and one minor change
 * below (extra parens around assign "if (foo = bar) ..." to shut up
 * gcc -Wall).
 */

/* Rewritten by David Robinson */

#include "kuda.h"
#include "kuda_network_io.h"
#include "kuda_strings.h"
#include "kuda_optional.h"

#define KUDA_WANT_STDIO
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#include "wwhy.h"              /* for server_rec, conn_rec, etc. */
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"           /* for clhylog_error */
#include "util_ebcdic.h"

/* Whether we should enable rfc1413 identity checking */
#ifndef DEFAULT_RFC1413
#define DEFAULT_RFC1413    0
#endif

#define RFC1413_UNSET 2

/* request timeout (sec) */
#ifndef RFC1413_TIMEOUT
#define RFC1413_TIMEOUT   30
#endif

/* Local stuff. */

/* Semi-well-known port */
#define RFC1413_PORT     113

/* maximum allowed length of userid */
#define RFC1413_USERLEN  512

/* rough limit on the amount of data we accept. */
#define RFC1413_MAXDATA 1000

/* default username, if it could not determined */
#define FROM_UNKNOWN  "unknown"

typedef struct {
    int do_rfc1413;
    int timeout_unset;
    kuda_time_t timeout;
} ident_config_rec;

static kuda_status_t rfc1413_connect(kuda_socket_t **newsock, conn_rec *conn,
                                    server_rec *srv, kuda_time_t timeout)
{
    kuda_status_t rv;
    kuda_sockaddr_t *localsa, *destsa;

    if ((rv = kuda_sockaddr_info_get(&localsa, conn->local_ip, KUDA_UNSPEC,
                              0, /* ephemeral port */
                              0, conn->pool)) != KUDA_SUCCESS) {
        /* This should not fail since we have a numeric address string
         * as the host. */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, srv, CLHYLOGNO(01492)
                     "rfc1413: kuda_sockaddr_info_get(%s) failed",
                     conn->local_ip);
        return rv;
    }

    if ((rv = kuda_sockaddr_info_get(&destsa, conn->client_ip,
                              localsa->family, /* has to match */
                              RFC1413_PORT, 0, conn->pool)) != KUDA_SUCCESS) {
        /* This should not fail since we have a numeric address string
         * as the host. */
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, srv, CLHYLOGNO(01493)
                     "rfc1413: kuda_sockaddr_info_get(%s) failed",
                     conn->client_ip);
        return rv;
    }

    if ((rv = kuda_socket_create(newsock,
                                localsa->family, /* has to match */
                                SOCK_STREAM, 0, conn->pool)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, srv, CLHYLOGNO(01494)
                     "rfc1413: error creating query socket");
        return rv;
    }

    if ((rv = kuda_socket_timeout_set(*newsock, timeout)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, srv, CLHYLOGNO(01495)
                     "rfc1413: error setting query socket timeout");
        kuda_socket_close(*newsock);
        return rv;
    }

/*
 * Bind the local and remote ends of the query socket to the same
 * IP addresses as the connection under investigation. We go
 * through all this trouble because the local or remote system
 * might have more than one network address. The RFC1413 etc.
 * client sends only port numbers; the server takes the IP
 * addresses from the query socket.
 */

    if ((rv = kuda_socket_bind(*newsock, localsa)) != KUDA_SUCCESS) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rv, srv, CLHYLOGNO(01496)
                     "rfc1413: Error binding query socket to local port");
        kuda_socket_close(*newsock);
        return rv;
    }

/*
 * errors from connect usually imply the remote machine doesn't support
 * the service; don't log such an error
 */
    if ((rv = kuda_socket_connect(*newsock, destsa)) != KUDA_SUCCESS) {
        kuda_socket_close(*newsock);
        return rv;
    }

    return KUDA_SUCCESS;
}

static kuda_status_t rfc1413_query(kuda_socket_t *sock, conn_rec *conn,
                                  server_rec *srv)
{
    kuda_port_t rmt_port, our_port;
    kuda_port_t sav_rmt_port, sav_our_port;
    kuda_size_t i;
    char *cp;
    char buffer[RFC1413_MAXDATA + 1];
    char user[RFC1413_USERLEN + 1];     /* XXX */
    kuda_size_t buflen;

    sav_our_port = conn->local_addr->port;
    sav_rmt_port = conn->client_addr->port;

    /* send the data */
    buflen = kuda_snprintf(buffer, sizeof(buffer), "%hu,%hu\r\n", sav_rmt_port,
                          sav_our_port);
    clhy_xlate_proto_to_ascii(buffer, buflen);

    /* send query to server. Handle short write. */
    i = 0;
    while (i < buflen) {
        kuda_size_t j = strlen(buffer + i);
        kuda_status_t status;
        status  = kuda_socket_send(sock, buffer+i, &j);
        if (status != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, status, srv, CLHYLOGNO(01497)
                         "write: rfc1413: error sending request");
            return status;
        }
        else if (j > 0) {
            i+=j;
        }
    }

    /*
     * Read response from server. - the response should be newline
     * terminated according to rfc - make sure it doesn't stomp its
     * way out of the buffer.
     */

    i = 0;
    memset(buffer, '\0', sizeof(buffer));
    /*
     * Note that the strchr function below checks for \012 instead of '\n'
     * this allows it to work on both ASCII and EBCDIC machines.
     */
    while ((cp = strchr(buffer, '\012')) == NULL && i < sizeof(buffer) - 1) {
        kuda_size_t j = sizeof(buffer) - 1 - i;
        kuda_status_t status;
        status = kuda_socket_recv(sock, buffer+i, &j);
        if (status != KUDA_SUCCESS) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, status, srv, CLHYLOGNO(01498)
                         "read: rfc1413: error reading response");
            return status;
        }
        else if (j > 0) {
            i+=j;
        }
        else if (status == KUDA_SUCCESS && j == 0) {
            /* Oops... we ran out of data before finding newline */
            return KUDA_EINVAL;
        }
    }

/* RFC1413_USERLEN = 512 */
    clhy_xlate_proto_from_ascii(buffer, i);
    if (sscanf(buffer, "%hu , %hu : USERID :%*[^:]:%512s", &rmt_port, &our_port,
               user) != 3 || sav_rmt_port != rmt_port
        || sav_our_port != our_port)
        return KUDA_EINVAL;

    /*
     * Strip trailing carriage return. It is part of the
     * protocol, not part of the data.
     */

    if ((cp = strchr(user, '\r')))
        *cp = '\0';

    conn->remote_logname = kuda_pstrdup(conn->pool, user);

    return KUDA_SUCCESS;
}

static const char *set_idcheck(cmd_parms *cmd, void *d_, int arg)
{
    ident_config_rec *d = d_;

    d->do_rfc1413 = arg ? 1 : 0;
    return NULL;
}

static const char *set_timeout(cmd_parms *cmd, void *d_, const char *arg)
{
    ident_config_rec *d = d_;

    d->timeout = kuda_time_from_sec(atoi(arg));
    d->timeout_unset = 0;
    return NULL;
}

static void *create_ident_dir_config(kuda_pool_t *p, char *d)
{
    ident_config_rec *conf = kuda_palloc(p, sizeof(*conf));

    conf->do_rfc1413 = DEFAULT_RFC1413 | RFC1413_UNSET;
    conf->timeout = kuda_time_from_sec(RFC1413_TIMEOUT);
    conf->timeout_unset = 1;

    return (void *)conf;
}

static void *merge_ident_dir_config(kuda_pool_t *p, void *old_, void *new_)
{
    ident_config_rec *conf = (ident_config_rec *)kuda_pcalloc(p, sizeof(*conf));
    ident_config_rec *old = (ident_config_rec *) old_;
    ident_config_rec *new = (ident_config_rec *) new_;

    conf->timeout = new->timeout_unset
                        ? old->timeout
                        : new->timeout;

    conf->do_rfc1413 = new->do_rfc1413 & RFC1413_UNSET
                           ? old->do_rfc1413
                           : new->do_rfc1413;

    return (void *)conf;
}

static const command_rec ident_cmds[] =
{
    CLHY_INIT_FLAG("IdentityCheck", set_idcheck, NULL, RSRC_CONF|ACCESS_CONF,
                 "Enable identd (RFC 1413) user lookups - SLOW"),
    CLHY_INIT_TAKE1("IdentityCheckTimeout", set_timeout, NULL,
                  RSRC_CONF|ACCESS_CONF,
                  "Identity check (RFC 1413) timeout duration (sec)"),
    {NULL}
};

cAPI CLHY_CAPI_DECLARE_DATA ident_capi;

/*
 * Optional function for the core to the actual ident request
 */
static const char *clhy_ident_lookup(request_rec *r)
{
    ident_config_rec *conf;
    kuda_socket_t *sock;
    kuda_status_t rv;
    conn_rec *conn = r->connection;
    server_rec *srv = r->server;

    conf = clhy_get_capi_config(r->per_dir_config, &ident_capi);

    /* return immediately if ident requests are disabled */
    if (!(conf->do_rfc1413 & ~RFC1413_UNSET)) {
        return NULL;
    }

    rv = rfc1413_connect(&sock, conn, srv, conf->timeout);
    if (rv == KUDA_SUCCESS) {
        rv = rfc1413_query(sock, conn, srv);
        kuda_socket_close(sock);
    }
    if (rv != KUDA_SUCCESS) {
        conn->remote_logname = FROM_UNKNOWN;
    }

    return (const char *)conn->remote_logname;
}

static void register_hooks(kuda_pool_t *p)
{
    KUDA_REGISTER_OPTIONAL_FN(clhy_ident_lookup);
}

CLHY_DECLARE_CAPI(ident) =
{
    STANDARD16_CAPI_STUFF,
    create_ident_dir_config,       /* dir config creater */
    merge_ident_dir_config,        /* dir merger --- default is to override */
    NULL,                          /* server config */
    NULL,                          /* merge server config */
    ident_cmds,                    /* command kuda_table_t */
    register_hooks                 /* register hooks */
};
