dnl cAPIs enabled in this directory by default

dnl CLHYKUDEL_CAPI(name, helptext[, objects[, structname[, default[, config]]]])

CLHYKUDEL_CAPIPATH_INIT(metadata)

CLHYKUDEL_CAPI(env, clearing/setting of ENV vars, , , yes)
CLHYKUDEL_CAPI(mime_magic, automagically determining MIME type)
CLHYKUDEL_CAPI(cern_meta, CERN-type meta files, , , no)
CLHYKUDEL_CAPI(expires, Expires header control, , , most)
CLHYKUDEL_CAPI(headers, HTTP header control, , , yes)
CLHYKUDEL_CAPI(ident, RFC 1413 identity check, , , no)

CLHYKUDEL_CAPI(usertrack, user-session tracking, , , , [
  AC_CHECK_HEADERS(sys/times.h)
  AC_CHECK_FUNCS(times)
])

CLHYKUDEL_CAPI(unique_id, per-request unique ids, , , most)
CLHYKUDEL_CAPI(setenvif, basing ENV vars on headers, , , yes)
CLHYKUDEL_CAPI(version, determining wwhy version in config files, , , yes)
CLHYKUDEL_CAPI(remoteip, translate header contents to an apparent client remote_ip, , , most)

CLHYKUDEL_CAPIPATH_FINISH
