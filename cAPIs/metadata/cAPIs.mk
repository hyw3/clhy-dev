capi_env.la: capi_env.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_env.lo $(CAPI_ENV_LDADD)
capi_expires.la: capi_expires.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_expires.lo $(CAPI_EXPIRES_LDADD)
capi_headers.la: capi_headers.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_headers.lo $(CAPI_HEADERS_LDADD)
capi_unique_id.la: capi_unique_id.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_unique_id.lo $(CAPI_UNIQUE_ID_LDADD)
capi_setenvif.la: capi_setenvif.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_setenvif.lo $(CAPI_SETENVIF_LDADD)
capi_version.la: capi_version.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_version.lo $(CAPI_VERSION_LDADD)
capi_remoteip.la: capi_remoteip.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_remoteip.lo $(CAPI_REMOTEIP_LDADD)
DISTCLEAN_TARGETS = cAPIs.mk
static = 
shared =  capi_env.la capi_expires.la capi_headers.la capi_unique_id.la capi_setenvif.la capi_version.la capi_remoteip.la
