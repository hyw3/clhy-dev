/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda.h"
#include "kuda_strings.h"

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif

#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_request.h"
#include "http_log.h"

typedef struct {
    kuda_table_t *vars;
    kuda_table_t *unsetenv;
} env_dir_config_rec;

cAPI CLHY_CAPI_DECLARE_DATA env_capi;

static void *create_env_dir_config(kuda_pool_t *p, char *dummy)
{
    env_dir_config_rec *conf = kuda_palloc(p, sizeof(*conf));

    conf->vars = kuda_table_make(p, 10);
    conf->unsetenv = kuda_table_make(p, 10);

    return conf;
}

static void *merge_env_dir_configs(kuda_pool_t *p, void *basev, void *addv)
{
    env_dir_config_rec *base = basev;
    env_dir_config_rec *add = addv;
    env_dir_config_rec *res = kuda_palloc(p, sizeof(*res));

    const kuda_table_entry_t *elts;
    const kuda_array_header_t *arr;

    int i;

    /*
     * res->vars = copy_table( p, base->vars );
     * foreach $unsetenv ( @add->unsetenv )
     *     table_unset( res->vars, $unsetenv );
     * foreach $element ( @add->vars )
     *     table_set( res->vars, $element.key, $element.val );
     *
     * add->unsetenv already removed the vars from add->vars,
     * if they preceded the UnsetEnv directive.
     */
    res->vars = kuda_table_copy(p, base->vars);
    res->unsetenv = NULL;

    arr = kuda_table_elts(add->unsetenv);
    if (arr) {
        elts = (const kuda_table_entry_t *)arr->elts;

        for (i = 0; i < arr->nelts; ++i) {
            kuda_table_unset(res->vars, elts[i].key);
        }
    }

    arr = kuda_table_elts(add->vars);
    if (arr) {
        elts = (const kuda_table_entry_t *)arr->elts;

        for (i = 0; i < arr->nelts; ++i) {
            kuda_table_setn(res->vars, elts[i].key, elts[i].val);
        }
    }

    return res;
}

static const char *add_env_capi_vars_passed(cmd_parms *cmd, void *sconf_,
                                              const char *arg)
{
    env_dir_config_rec *sconf = sconf_;
    kuda_table_t *vars = sconf->vars;
    const char *env_var;

    env_var = getenv(arg);
    if (env_var != NULL) {
        kuda_table_setn(vars, arg, kuda_pstrdup(cmd->pool, env_var));
    }
    else {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_WARNING, 0, cmd->server, CLHYLOGNO(01506)
                     "PassEnv variable %s was undefined", arg);
    }

    return NULL;
}

static const char *add_env_capi_vars_set(cmd_parms *cmd, void *sconf_,
                                           const char *name, const char *value)
{
    env_dir_config_rec *sconf = sconf_;

    /* name is mandatory, value is optional.  no value means
     * set the variable to an empty string
     */
    kuda_table_setn(sconf->vars, name, value ? value : "");

    return NULL;
}

static const char *add_env_capi_vars_unset(cmd_parms *cmd, void *sconf_,
                                             const char *arg)
{
    env_dir_config_rec *sconf = sconf_;

    /* Always UnsetEnv FOO in the same context as {Set,Pass}Env FOO
     * only if this UnsetEnv follows the {Set,Pass}Env.  The merge
     * will only apply unsetenv to the parent env (main server).
     */
    kuda_table_set(sconf->unsetenv, arg, NULL);
    kuda_table_unset(sconf->vars, arg);

    return NULL;
}

static const command_rec env_capi_cmds[] =
{
CLHY_INIT_ITERATE("PassEnv", add_env_capi_vars_passed, NULL,
     OR_FILEINFO, "a list of environment variables to pass to CGI."),
CLHY_INIT_TAKE12("SetEnv", add_env_capi_vars_set, NULL,
     OR_FILEINFO, "an environment variable name and optional value to pass to CGI."),
CLHY_INIT_ITERATE("UnsetEnv", add_env_capi_vars_unset, NULL,
     OR_FILEINFO, "a list of variables to remove from the CGI environment."),
    {NULL},
};

static int fixup_env_capi(request_rec *r)
{
    env_dir_config_rec *sconf = clhy_get_capi_config(r->per_dir_config,
                                                     &env_capi);

    if (kuda_is_empty_table(sconf->vars)) {
        return DECLINED;
    }

    r->subprocess_env = kuda_table_overlay(r->pool, r->subprocess_env,
            sconf->vars);

    return OK;
}

static void register_hooks(kuda_pool_t *p)
{
    clhy_hook_fixups(fixup_env_capi, NULL, NULL, KUDA_HOOK_MIDDLE);
}

CLHY_DECLARE_CAPI(env) =
{
    STANDARD16_CAPI_STUFF,
    create_env_dir_config,      /* dir config creater */
    merge_env_dir_configs,      /* dir merger --- default is to override */
    NULL,                       /* server config */
    NULL,                       /* merge server configs */
    env_capi_cmds,            /* command kuda_table_t */
    register_hooks              /* register hooks */
};
