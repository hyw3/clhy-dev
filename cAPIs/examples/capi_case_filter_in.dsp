# Microsoft Developer Studio Project File - Name="capi_case_filter_in" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=capi_case_filter_in - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "capi_case_filter_in.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For case_filter_in:
!MESSAGE 
!MESSAGE NMAKE /f "capi_case_filter_in.mak" CFG="capi_case_filter_in - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "capi_case_filter_in - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "capi_case_filter_in - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "capi_case_filter_in - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FD /c
# ADD CPP /nologo /MD /W3 /O2 /Oy- /Zi /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /Fd"Release\capi_case_filter_in_src" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o /win32 "NUL"
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /fo"Release/capi_case_filter_in.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "NDEBUG" /d BIN_NAME="capi_case_filter_in.so" /d LONG_NAME="case_filter_in_capi for cLHy"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:windows /dll /out:".\Release\capi_case_filter_in.so" /base:@..\..\platforms\win32\BaseAddr.ref,capi_case_filter_in.so
# ADD LINK32 kernel32.lib /nologo /subsystem:windows /dll /incremental:no /debug /out:".\Release\capi_case_filter_in.so" /base:@..\..\platforms\win32\BaseAddr.ref,capi_case_filter_in.so /opt:ref
# Begin Special Build Tool
TargetPath=.\Release\capi_case_filter_in.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ELSEIF  "$(CFG)" == "capi_case_filter_in - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /EHsc /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FD /c
# ADD CPP /nologo /MDd /W3 /EHsc /Zi /Od /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /Fd"Debug\capi_case_filter_in_src" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o /win32 "NUL"
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /fo"Debug/capi_case_filter_in.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "_DEBUG" /d BIN_NAME="capi_case_filter_in.so" /d LONG_NAME="case_filter_in_capi for cLHy"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:windows /dll /incremental:no /debug /out:".\Debug\capi_case_filter_in.so" /base:@..\..\platforms\win32\BaseAddr.ref,capi_case_filter_in.so
# ADD LINK32 kernel32.lib /nologo /subsystem:windows /dll /incremental:no /debug /out:".\Debug\capi_case_filter_in.so" /base:@..\..\platforms\win32\BaseAddr.ref,capi_case_filter_in.so
# Begin Special Build Tool
TargetPath=.\Debug\capi_case_filter_in.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "capi_case_filter_in - Win32 Release"
# Name "capi_case_filter_in - Win32 Debug"
# Begin Source File

SOURCE=.\capi_case_filter_in.c
# End Source File
# Begin Source File

SOURCE=..\..\build\win32\wwhy.rc
# End Source File
# End Target
# End Project
