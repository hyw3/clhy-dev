/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * An example input filter - this converts input to upper case. Note that
 * because of the moment it gets inserted it does NOT convert request headers.
 */

#include "wwhy.h"
#include "http_config.h"
#include "kuda_buckets.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "util_filter.h"
#include "http_request.h"

#include <ctype.h>

static const char s_szCaseFilterName[] = "CaseFilterIn";
cAPI CLHY_CAPI_DECLARE_DATA case_filter_in_capi;

typedef struct
{
    int bEnabled;
} CaseFilterInConfig;

typedef struct
{
    kuda_bucket_brigade *pbbTmp;
} CaseFilterInContext;

static void *CaseFilterInCreateServerConfig(kuda_pool_t *p, server_rec *s)
{
    CaseFilterInConfig *pConfig = kuda_pcalloc(p, sizeof *pConfig);

    pConfig->bEnabled = 0;

    return pConfig;
}

static void CaseFilterInInsertFilter(request_rec *r)
{
    CaseFilterInConfig *pConfig = clhy_get_capi_config(r->server->capi_config,
                                                       &case_filter_in_capi);
    if (!pConfig->bEnabled)
        return;

    clhy_add_input_filter(s_szCaseFilterName, NULL, r, r->connection);
}

static kuda_status_t CaseFilterInFilter(clhy_filter_t *f,
                                       kuda_bucket_brigade *pbbOut,
                                       clhy_input_mode_t eMode,
                                       kuda_read_type_e eBlock,
                                       kuda_off_t nBytes)
{
    request_rec *r = f->r;
    conn_rec *c = r->connection;
    CaseFilterInContext *pCtx;
    kuda_status_t ret;

    if (!(pCtx = f->ctx)) {
        f->ctx = pCtx = kuda_palloc(r->pool, sizeof *pCtx);
        pCtx->pbbTmp = kuda_brigade_create(r->pool, c->bucket_alloc);
    }

    if (KUDA_BRIGADE_EMPTY(pCtx->pbbTmp)) {
        ret = clhy_get_brigade(f->next, pCtx->pbbTmp, eMode, eBlock, nBytes);

        if (eMode == CLHY_MODE_EATCRLF || ret != KUDA_SUCCESS)
            return ret;
    }

    while (!KUDA_BRIGADE_EMPTY(pCtx->pbbTmp)) {
        kuda_bucket *pbktIn = KUDA_BRIGADE_FIRST(pCtx->pbbTmp);
        kuda_bucket *pbktOut;
        const char *data;
        kuda_size_t len;
        char *buf;
        kuda_size_t n;

        /* It is tempting to do this...
         * KUDA_BUCKET_REMOVE(pB);
         * KUDA_BRIGADE_INSERT_TAIL(pbbOut,pB);
         * and change the case of the bucket data, but that would be wrong
         * for a file or socket buffer, for example...
         */

        if (KUDA_BUCKET_IS_EOS(pbktIn)) {
            KUDA_BUCKET_REMOVE(pbktIn);
            KUDA_BRIGADE_INSERT_TAIL(pbbOut, pbktIn);
            break;
        }

        ret=kuda_bucket_read(pbktIn, &data, &len, eBlock);
        if (ret != KUDA_SUCCESS)
            return ret;

        buf = clhy_malloc(len);
        for (n=0 ; n < len ; ++n) {
            buf[n] = kuda_toupper(data[n]);
        }

        pbktOut = kuda_bucket_heap_create(buf, len, 0, c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(pbbOut, pbktOut);
        kuda_bucket_delete(pbktIn);
    }

    return KUDA_SUCCESS;
}

static const char *CaseFilterInEnable(cmd_parms *cmd, void *dummy, int arg)
{
    CaseFilterInConfig *pConfig
      = clhy_get_capi_config(cmd->server->capi_config,
                             &case_filter_in_capi);
    pConfig->bEnabled=arg;

    return NULL;
}

static const command_rec CaseFilterInCmds[] =
{
    CLHY_INIT_FLAG("CaseFilterIn", CaseFilterInEnable, NULL, RSRC_CONF,
                 "Run an input case filter on this host"),
    { NULL }
};


static void CaseFilterInRegisterHooks(kuda_pool_t *p)
{
    clhy_hook_insert_filter(CaseFilterInInsertFilter, NULL, NULL,
                          KUDA_HOOK_MIDDLE);
    clhy_register_input_filter(s_szCaseFilterName, CaseFilterInFilter, NULL,
                             CLHY_FTYPE_RESOURCE);
}

CLHY_DECLARE_CAPI(case_filter_in) =
{
    STANDARD16_CAPI_STUFF,
    NULL,
    NULL,
    CaseFilterInCreateServerConfig,
    NULL,
    CaseFilterInCmds,
    CaseFilterInRegisterHooks
};
