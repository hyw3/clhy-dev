/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_config.h"
#include "kuda_buckets.h"
#include "kuda_general.h"
#include "kuda_lib.h"
#include "util_filter.h"
#include "http_request.h"

#include <ctype.h>

static const char s_szCaseFilterName[] = "CaseFilter";
cAPI CLHY_CAPI_DECLARE_DATA case_filter_capi;

typedef struct
{
    int bEnabled;
} CaseFilterConfig;

static void *CaseFilterCreateServerConfig(kuda_pool_t *p, server_rec *s)
{
    CaseFilterConfig *pConfig = kuda_pcalloc(p,sizeof *pConfig);

    pConfig->bEnabled = 0;

    return pConfig;
}

static void CaseFilterInsertFilter(request_rec *r)
{
    CaseFilterConfig *pConfig = clhy_get_capi_config(r->server->capi_config,
                                                     &case_filter_capi);

    if (!pConfig->bEnabled)
        return;

    clhy_add_output_filter(s_szCaseFilterName, NULL, r, r->connection);
}

static kuda_status_t CaseFilterOutFilter(clhy_filter_t *f,
                                        kuda_bucket_brigade *pbbIn)
{
    request_rec *r = f->r;
    conn_rec *c = r->connection;
    kuda_bucket *pbktIn;
    kuda_bucket_brigade *pbbOut;

    pbbOut = kuda_brigade_create(r->pool, c->bucket_alloc);
    for (pbktIn = KUDA_BRIGADE_FIRST(pbbIn);
         pbktIn != KUDA_BRIGADE_SENTINEL(pbbIn);
         pbktIn = KUDA_BUCKET_NEXT(pbktIn))
    {
        const char *data;
        kuda_size_t len;
        char *buf;
        kuda_size_t n;
        kuda_bucket *pbktOut;

        if (KUDA_BUCKET_IS_EOS(pbktIn)) {
            kuda_bucket *pbktEOS = kuda_bucket_eos_create(c->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(pbbOut, pbktEOS);
            continue;
        }

        /* read */
        kuda_bucket_read(pbktIn, &data, &len, KUDA_BLOCK_READ);

        /* write */
        buf = kuda_bucket_alloc(len, c->bucket_alloc);
        for (n=0 ; n < len ; ++n) {
            buf[n] = kuda_toupper(data[n]);
        }

        pbktOut = kuda_bucket_heap_create(buf, len, kuda_bucket_free,
                                         c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(pbbOut, pbktOut);
    }

    /* Q: is there any advantage to passing a brigade for each bucket?
     * A: obviously, it can cut down server resource consumption, if this
     * experimental cAPI was fed a file of 4MB, it would be using 8MB for
     * the 'read' buckets and the 'write' buckets.
     *
     * Note it is more efficient to consume (destroy) each bucket as it's
     * processed above than to do a single cleanup down here.  In any case,
     * don't let our caller pass the same buckets to us, twice;
     */
    kuda_brigade_cleanup(pbbIn);
    return clhy_pass_brigade(f->next, pbbOut);
}

static const char *CaseFilterEnable(cmd_parms *cmd, void *dummy, int arg)
{
    CaseFilterConfig *pConfig = clhy_get_capi_config(cmd->server->capi_config,
                                                     &case_filter_capi);
    pConfig->bEnabled = arg;

    return NULL;
}

static const command_rec CaseFilterCmds[] =
{
    CLHY_INIT_FLAG("CaseFilter", CaseFilterEnable, NULL, RSRC_CONF,
                 "Run a case filter on this host"),
    { NULL }
};

static void CaseFilterRegisterHooks(kuda_pool_t *p)
{
    clhy_hook_insert_filter(CaseFilterInsertFilter, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_register_output_filter(s_szCaseFilterName, CaseFilterOutFilter, NULL,
                              CLHY_FTYPE_RESOURCE);
}

CLHY_DECLARE_CAPI(case_filter) =
{
    STANDARD16_CAPI_STUFF,
    NULL,
    NULL,
    CaseFilterCreateServerConfig,
    NULL,
    CaseFilterCmds,
    CaseFilterRegisterHooks
};
