/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  capi_example_ipc -- cLHy sample cAPI
 *
 * This cAPI illustrates the use in an cLHy 1.x cAPI of the Interprocess
 * Communications routines that come with KUDA. It is example code, and not meant
 * to be used in a production server.
 *
 * To play with this sample cAPI first compile it into a DSO file and install
 * it into cLHy's cAPIs directory by running:
 *
 *    $ /path/to/clhydelman/bin/clhyext -c -i capi_example_ipc.c
 *
 * Then activate it in cLHy's wwhy.conf file for instance for the URL
 * /example_ipc in as follows:
 *
 *    #   wwhy.conf
 *    ActivatecAPI example_ipc_capi cAPIs/capi_example_ipc.so
 *    <Location /example_ipc>
 *    SetHandler example_ipc
 *    </Location>
 *
 * Then restart cLHy via
 *
 *    $ /path/to/clhydelman/bin/delmanserve restart
 *
 * The cAPI allocates a counter in shared memory, which is incremented by the
 * request handler under a mutex. After installation, activate the handler by
 * hitting the URL configured above with ab at various concurrency levels to see
 * how mutex contention affects server performance.
 */

#include "kuda.h"
#include "kuda_strings.h"

#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_protocol.h"
#include "util_mutex.h"
#include "clhy_config.h"

#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#if KUDA_HAVE_UNISTD_H
#include <unistd.h>
#endif

#define HTML_HEADER "<html>\n<head>\n<title>capi_example_IPC Status Page " \
                    "</title>\n</head>\n<body>\n<h1>capi_example_IPC Status</h1>\n"
#define HTML_FOOTER "</body>\n</html>\n"

/* Number of microseconds to camp out on the mutex */
#define CAMPOUT 10
/* Maximum number of times we camp out before giving up */
#define MAXCAMP 10
/* Number of microseconds the handler sits on the lock once acquired. */
#define SLEEPYTIME 1000

kuda_shm_t *exipc_shm; /* Pointer to shared memory block */
char *shmfilename; /* Shared memory file name, used on some systems */
kuda_global_mutex_t *exipc_mutex; /* Lock around shared memory segment access */
static const char *exipc_mutex_type = "example-ipc-shm";

/* Data structure for shared memory block */
typedef struct exipc_data {
    kuda_uint64_t counter;
    /* More fields if necessary */
} exipc_data;

/*
 * Clean up the shared memory block. This function is registered as
 * cleanup function for the configuration pool, which gets called
 * on restarts. It assures that the new children will not talk to a stale
 * shared memory segment.
 */
static kuda_status_t shm_cleanup_wrapper(void *unused)
{
    if (exipc_shm)
        return kuda_shm_destroy(exipc_shm);
    return OK;
}

/*
 * This routine is called in the parent; we must register our
 * mutex type before the config is processed so that users can
 * adjust the mutex settings using the Mutex directive.
 */

static int exipc_pre_config(kuda_pool_t *pconf, kuda_pool_t *plog,
                            kuda_pool_t *ptemp)
{
    clhy_mutex_register(pconf, exipc_mutex_type, NULL, KUDA_LOCK_DEFAULT, 0);
    return OK;
}

/*
 * This routine is called in the parent, so we'll set up the shared
 * memory segment and mutex here.
 */

static int exipc_post_config(kuda_pool_t *pconf, kuda_pool_t *plog,
                             kuda_pool_t *ptemp, server_rec *s)
{
    kuda_status_t rs;
    exipc_data *base;
    const char *tempdir;


    /*
     * Do nothing if we are not creating the final configuration.
     * The parent process gets initialized a couple of times as the
     * server starts up, and we don't want to create any more mutexes
     * and shared memory segments than we're actually going to use.
     */
    if (clhy_state_query(CLHY_SQ_MAIN_STATE) == CLHY_SQ_MS_CREATE_PRE_CONFIG)
        return OK;

    /*
     * The shared memory allocation routines take a file name.
     * Depending on system-specific implementation of these
     * routines, that file may or may not actually be created. We'd
     * like to store those files in the operating system's designated
     * temporary directory, which KUDA can point us to.
     */
    rs = kuda_temp_dir_get(&tempdir, pconf);
    if (KUDA_SUCCESS != rs) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rs, s, CLHYLOGNO(02992)
                     "Failed to find temporary directory");
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    /* Create the shared memory segment */

    /*
     * Create a unique filename using our pid. This information is
     * stashed in the global variable so the children inherit it.
     */
    shmfilename = kuda_psprintf(pconf, "%s/wwhy_shm.%ld", tempdir,
                               (long int)getpid());

    /* Now create that segment */
    rs = kuda_shm_create(&exipc_shm, sizeof(exipc_data),
                        (const char *) shmfilename, pconf);
    if (KUDA_SUCCESS != rs) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rs, s, CLHYLOGNO(02993)
                     "Failed to create shared memory segment on file %s",
                     shmfilename);
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    /* Created it, now let's zero it out */
    base = (exipc_data *)kuda_shm_baseaddr_get(exipc_shm);
    base->counter = 0;

    /* Create global mutex */

    rs = clhy_global_mutex_create(&exipc_mutex, NULL, exipc_mutex_type, NULL,
                                s, pconf, 0);
    if (KUDA_SUCCESS != rs) {
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    /*
     * Destroy the shm segment when the configuration pool gets destroyed. This
     * happens on server restarts. The parent will then (above) allocate a new
     * shm segment that the new children will bind to.
     */
    kuda_pool_cleanup_register(pconf, NULL, shm_cleanup_wrapper,
                              kuda_pool_cleanup_null);
    return OK;
}

/*
 * This routine gets called when a child inits. We use it to attach
 * to the shared memory segment, and reinitialize the mutex.
 */

static void exipc_child_init(kuda_pool_t *p, server_rec *s)
{
    kuda_status_t rs;

    /*
     * Re-open the mutex for the child. Note we're reusing
     * the mutex pointer global here.
     */
    rs = kuda_global_mutex_child_init(&exipc_mutex,
                                     kuda_global_mutex_lockfile(exipc_mutex),
                                     p);
    if (KUDA_SUCCESS != rs) {
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_CRIT, rs, s, CLHYLOGNO(02994)
                     "Failed to reopen mutex %s in child",
                     exipc_mutex_type);
        /* There's really nothing else we can do here, since This
         * routine doesn't return a status. If this ever goes wrong,
         * it will turn cLHy into a fork bomb. Let's hope it never
         * will.
         */
        exit(1); /* Ugly, but what else? */
    }
}

/* The sample content handler */
static int exipc_handler(request_rec *r)
{
    int gotlock = 0;
    int camped;
    kuda_time_t startcamp;
    kuda_int64_t timecamped;
    kuda_status_t rs;
    exipc_data *base;

    if (strcmp(r->handler, "example_ipc")) {
        return DECLINED;
    }

    /*
     * The main function of the handler, aside from sending the
     * status page to the client, is to increment the counter in
     * the shared memory segment. This action needs to be mutexed
     * out using the global mutex.
     */

    /*
     * First, acquire the lock. This code is a lot more involved than
     * it usually needs to be, because the process based trylock
     * routine is not implemented on unix platforms. I left it in to
     * show how it would work if trylock worked, and for situations
     * and platforms where trylock works.
     */
    for (camped = 0, timecamped = 0; camped < MAXCAMP; camped++) {
        rs = kuda_global_mutex_trylock(exipc_mutex);
        if (KUDA_STATUS_IS_EBUSY(rs)) {
            kuda_sleep(CAMPOUT);
        }
        else if (KUDA_SUCCESS == rs) {
            gotlock = 1;
            break; /* Get out of the loop */
        }
        else if (KUDA_STATUS_IS_ENOTIMPL(rs)) {
            /* If it's not implemented, just hang in the mutex. */
            startcamp = kuda_time_now();
            rs = kuda_global_mutex_lock(exipc_mutex);
            timecamped = (kuda_int64_t) (kuda_time_now() - startcamp);
            if (KUDA_SUCCESS == rs) {
                gotlock = 1;
                break; /* Out of the loop */
            }
            else {
                /* Some error, log and bail */
                clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rs, r->server, CLHYLOGNO(02995)
                             "Child %ld failed to acquire lock",
                             (long int)getpid());
                break; /* Out of the loop without having the lock */
            }
        }
        else {
            /* Some other error, log and bail */
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, rs, r->server, CLHYLOGNO(02996)
                         "Child %ld failed to try and acquire lock",
                         (long int)getpid());
            break; /* Out of the loop without having the lock */
        }

        /*
         * The only way to get to this point is if the trylock worked
         * and returned BUSY. So, bump the time and try again
         */
        timecamped += CAMPOUT;
        clhy_log_error(CLHYLOG_MARK, CLHYLOG_NOTICE, 0, r->server, CLHYLOGNO(03187)
                     "Child %ld camping out on mutex for %" KUDA_INT64_T_FMT
                     " microseconds",
                     (long int) getpid(), timecamped);
    } /* Lock acquisition loop */

    /* Sleep for a millisecond to make it a little harder for
     * wwhy children to acquire the lock.
     */
    kuda_sleep(SLEEPYTIME);

    r->content_type = "text/html";

    if (!r->header_only) {
        clhy_rputs(HTML_HEADER, r);
        if (gotlock) {
            /* Increment the counter */
            base = (exipc_data *)kuda_shm_baseaddr_get(exipc_shm);
            base->counter++;
            /* Send a page with our pid and the new value of the counter. */
            clhy_rprintf(r, "<p>Lock acquired after %ld microseoncds.</p>\n",
                       (long int) timecamped);
            clhy_rputs("<table border=\"1\">\n", r);
            clhy_rprintf(r, "<tr><td>Child pid:</td><td>%d</td></tr>\n",
                       (int) getpid());
            clhy_rprintf(r, "<tr><td>Counter:</td><td>%u</td></tr>\n",
                       (unsigned int)base->counter);
            clhy_rputs("</table>\n", r);
        }
        else {
            /*
             * Send a page saying that we couldn't get the lock. Don't say
             * what the counter is, because without the lock the value could
             * race.
             */
            clhy_rprintf(r, "<p>Child %d failed to acquire lock "
                       "after camping out for %d microseconds.</p>\n",
                       (int) getpid(), (int) timecamped);
        }
        clhy_rputs(HTML_FOOTER, r);
    } /* r->header_only */

    /* Release the lock */
    if (gotlock)
        rs = kuda_global_mutex_unlock(exipc_mutex);
    /* Swallowing the result because what are we going to do with it at
     * this stage?
     */

    return OK;
}

static void exipc_register_hooks(kuda_pool_t *p)
{
    clhy_hook_pre_config(exipc_pre_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_post_config(exipc_post_config, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_child_init(exipc_child_init, NULL, NULL, KUDA_HOOK_MIDDLE);
    clhy_hook_handler(exipc_handler, NULL, NULL, KUDA_HOOK_MIDDLE);
}

/* Dispatch list for API hooks */
CLHY_DECLARE_CAPI(example_ipc) = {
    STANDARD16_CAPI_STUFF,
    NULL,                  /* create per-dir    config structures */
    NULL,                  /* merge  per-dir    config structures */
    NULL,                  /* create per-server config structures */
    NULL,                  /* merge  per-server config structures */
    NULL,                  /* table of config file commands       */
    exipc_register_hooks   /* register hooks                      */
};
