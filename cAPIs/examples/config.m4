
CLHYKUDEL_CAPIPATH_INIT(examples)

CLHYKUDEL_CAPI(example_hooks, Example hook callback handler cAPI, , , no)
CLHYKUDEL_CAPI(case_filter, Example uppercase conversion filter, , , no)
CLHYKUDEL_CAPI(case_filter_in, Example uppercase conversion input filter, , , no)
CLHYKUDEL_CAPI(example_ipc, Example of shared memory and mutex usage, , , no)

CLHYKUDEL_CAPIPATH_FINISH
