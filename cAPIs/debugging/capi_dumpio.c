/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Originally written @ Covalent by Jim Jagielski
 */

/*
 * capi_dumpio.c:
 *  Think of this as a filter sniffer for cLHy 1.x. It logs
 *  all filter data right before and after it goes out on the
 *  wire (BUT right before SSL encoded or after SSL decoded).
 *  It can produce a *huge* amount of data.
 */


#include "wwhy.h"
#include "http_connection.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "kuda_strings.h"

cAPI CLHY_CAPI_DECLARE_DATA dumpio_capi ;

typedef struct dumpio_conf_t {
    int enable_input;
    int enable_output;
} dumpio_conf_t;

/* consider up to 80 additional characters, and factor the longest
 * line length of all \xNN sequences; log_error cannot record more
 * than MAX_STRING_LEN characters.
 */
#define dumpio_MAX_STRING_LEN (MAX_STRING_LEN / 4 - 80)

/*
 * Workhorse function: simply log to the current error_log
 * info about the data in the bucket as well as the data itself
 */
static void dumpit(clhy_filter_t *f, kuda_bucket *b, dumpio_conf_t *ptr)
{
    conn_rec *c = f->c;

    clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE7, 0, c,
                  "capi_dumpio:  %s (%s-%s): %" KUDA_SIZE_T_FMT " bytes",
                  f->frec->name,
                  (KUDA_BUCKET_IS_METADATA(b)) ? "metadata" : "data",
                  b->type->name,
                  b->length) ;

    if (!(KUDA_BUCKET_IS_METADATA(b)))
    {
#if KUDA_CHARSET_EBCDIC
        char xlatebuf[dumpio_MAX_STRING_LEN + 1];
#endif
        const char *buf;
        kuda_size_t nbytes;
        kuda_status_t rv = kuda_bucket_read(b, &buf, &nbytes, KUDA_BLOCK_READ);

        if (rv == KUDA_SUCCESS)
        {
            while (nbytes)
            {
                kuda_size_t logbytes = nbytes;
                if (logbytes > dumpio_MAX_STRING_LEN)
                    logbytes = dumpio_MAX_STRING_LEN;
                nbytes -= logbytes;

#if KUDA_CHARSET_EBCDIC
                memcpy(xlatebuf, buf, logbytes);
                clhy_xlate_proto_from_ascii(xlatebuf, logbytes);
                xlatebuf[logbytes] = '\0';
                clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE7, 0, c,
                              "capi_dumpio:  %s (%s-%s): %s", f->frec->name,
                              (KUDA_BUCKET_IS_METADATA(b)) ? "metadata" : "data",
                              b->type->name, xlatebuf);
#else
                /* XXX: Seriously flawed; we do not pay attention to embedded
                 * \0's in the request body, these should be escaped; however,
                 * the logging function already performs a significant amount
                 * of escaping, and so any escaping would be double-escaped.
                 * The coding solution is to throw away the current logic
                 * within clhy_log_error, and introduce new vformatter %-escapes
                 * for escaping text, and for binary text (fixed len strings).
                 */
                clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE7, 0, c,
                              "capi_dumpio:  %s (%s-%s): %.*s", f->frec->name,
                              (KUDA_BUCKET_IS_METADATA(b)) ? "metadata" : "data",
                              b->type->name, (int)logbytes, buf);
#endif
                buf += logbytes;
            }
        }
        else {
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE7, rv, c,
                          "capi_dumpio:  %s (%s-%s): %s", f->frec->name,
                          (KUDA_BUCKET_IS_METADATA(b)) ? "metadata" : "data",
                          b->type->name, "error reading data");
        }
    }
}

#define whichmode( mode ) \
 ( (( mode ) == CLHY_MODE_READBYTES) ? "readbytes" : \
   (( mode ) == CLHY_MODE_GETLINE) ? "getline" : \
   (( mode ) == CLHY_MODE_EATCRLF) ? "eatcrlf" : \
   (( mode ) == CLHY_MODE_SPECULATIVE) ? "speculative" : \
   (( mode ) == CLHY_MODE_EXHAUSTIVE) ? "exhaustive" : \
   (( mode ) == CLHY_MODE_INIT) ? "init" : "unknown" \
 )

static int dumpio_input_filter (clhy_filter_t *f, kuda_bucket_brigade *bb,
    clhy_input_mode_t mode, kuda_read_type_e block, kuda_off_t readbytes)
{

    kuda_bucket *b;
    kuda_status_t ret;
    conn_rec *c = f->c;
    dumpio_conf_t *ptr = f->ctx;

    clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE7, 0, c,
                  "capi_dumpio: %s [%s-%s] %" KUDA_OFF_T_FMT " readbytes",
                  f->frec->name,
                  whichmode(mode),
                  ((block) == KUDA_BLOCK_READ) ? "blocking" : "nonblocking",
                  readbytes);

    ret = clhy_get_brigade(f->next, bb, mode, block, readbytes);

    if (ret == KUDA_SUCCESS) {
        for (b = KUDA_BRIGADE_FIRST(bb); b != KUDA_BRIGADE_SENTINEL(bb); b = KUDA_BUCKET_NEXT(b)) {
          dumpit(f, b, ptr);
        }
    }
    else {
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE7, 0, c,
                      "capi_dumpio: %s - %d", f->frec->name, ret) ;
        return ret;
    }

    return KUDA_SUCCESS ;
}

static int dumpio_output_filter (clhy_filter_t *f, kuda_bucket_brigade *bb)
{
    kuda_bucket *b;
    conn_rec *c = f->c;
    dumpio_conf_t *ptr = f->ctx;

    clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_TRACE7, 0, c, "capi_dumpio: %s", f->frec->name);

    for (b = KUDA_BRIGADE_FIRST(bb); b != KUDA_BRIGADE_SENTINEL(bb); b = KUDA_BUCKET_NEXT(b)) {
        /*
         * If we ever see an EOS, make sure to FLUSH.
         */
        if (KUDA_BUCKET_IS_EOS(b)) {
            kuda_bucket *flush = kuda_bucket_flush_create(f->c->bucket_alloc);
            KUDA_BUCKET_INSERT_BEFORE(b, flush);
        }
        dumpit(f, b, ptr);
    }

    return clhy_pass_brigade(f->next, bb) ;
}

static int dumpio_pre_conn(conn_rec *c, void *csd)
{
    dumpio_conf_t *ptr;

    ptr = (dumpio_conf_t *) clhy_get_capi_config(c->base_server->capi_config,
                                                 &dumpio_capi);

    if (ptr->enable_input)
        clhy_add_input_filter("DUMPIO_IN", ptr, NULL, c);
    if (ptr->enable_output)
        clhy_add_output_filter("DUMPIO_OUT", ptr, NULL, c);
    return OK;
}

static void dumpio_register_hooks(kuda_pool_t *p)
{
/*
 * We know that SSL is CONNECTION + 5
 */
  clhy_register_output_filter("DUMPIO_OUT", dumpio_output_filter,
        NULL, CLHY_FTYPE_CONNECTION + 3) ;

  clhy_register_input_filter("DUMPIO_IN", dumpio_input_filter,
        NULL, CLHY_FTYPE_CONNECTION + 3) ;

  clhy_hook_pre_connection(dumpio_pre_conn, NULL, NULL, KUDA_HOOK_MIDDLE);
}

static void *dumpio_create_sconfig(kuda_pool_t *p, server_rec *s)
{
    dumpio_conf_t *ptr = kuda_pcalloc(p, sizeof *ptr);
    ptr->enable_input = 0;
    ptr->enable_output = 0;
    return ptr;
}

static const char *dumpio_enable_input(cmd_parms *cmd, void *dummy, int arg)
{
    dumpio_conf_t *ptr = clhy_get_capi_config(cmd->server->capi_config,
                                              &dumpio_capi);

    ptr->enable_input = arg;
    return NULL;
}

static const char *dumpio_enable_output(cmd_parms *cmd, void *dummy, int arg)
{
    dumpio_conf_t *ptr = clhy_get_capi_config(cmd->server->capi_config,
                                              &dumpio_capi);

    ptr->enable_output = arg;
    return NULL;
}

static const command_rec dumpio_cmds[] = {
    CLHY_INIT_FLAG("DumpIOInput", dumpio_enable_input, NULL,
                 RSRC_CONF, "Enable I/O Dump on Input Data"),
    CLHY_INIT_FLAG("DumpIOOutput", dumpio_enable_output, NULL,
                 RSRC_CONF, "Enable I/O Dump on Output Data"),
    { NULL }
};

CLHY_DECLARE_CAPI(dumpio) = {
        STANDARD16_CAPI_STUFF,
        NULL,                   /* create per-dir    config structures */
        NULL,                   /* merge  per-dir    config structures */
        dumpio_create_sconfig,  /* create per-server config structures */
        NULL,                   /* merge  per-server config structures */
        dumpio_cmds,            /* table of config file commands       */
        dumpio_register_hooks   /* register hooks                      */
};
