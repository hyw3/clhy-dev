/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * capi_bucketeer.c: split buckets whenever we find a control-char
 *
 * Written by Ian Holsman
 *
 */

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "kuda_strings.h"
#include "kuda_general.h"
#include "util_filter.h"
#include "kuda_buckets.h"
#include "http_request.h"
#include "http_protocol.h"

static const char bucketeerFilterName[] = "BUCKETEER";
cAPI CLHY_CAPI_DECLARE_DATA bucketeer_capi;

typedef struct bucketeer_filter_config_t
{
    char bucketdelimiter;
    char passdelimiter;
    char flushdelimiter;
} bucketeer_filter_config_t;


static void *create_bucketeer_server_config(kuda_pool_t *p, server_rec *s)
{
    bucketeer_filter_config_t *c = kuda_pcalloc(p, sizeof *c);

    c->bucketdelimiter = 0x02; /* ^B */
    c->passdelimiter = 0x10;   /* ^P */
    c->flushdelimiter = 0x06;  /* ^F */

    return c;
}

typedef struct bucketeer_ctx_t
{
    kuda_bucket_brigade *bb;
} bucketeer_ctx_t;

static kuda_status_t bucketeer_out_filter(clhy_filter_t *f,
                                         kuda_bucket_brigade *bb)
{
    kuda_bucket *e;
    request_rec *r = f->r;
    bucketeer_ctx_t *ctx = f->ctx;
    bucketeer_filter_config_t *c;

    c = clhy_get_capi_config(r->server->capi_config, &bucketeer_capi);

    /* If have a context, it means we've done this before successfully. */
    if (!ctx) {
        if (!r->content_type || strncmp(r->content_type, "text/", 5)) {
            clhy_remove_output_filter(f);
            return clhy_pass_brigade(f->next, bb);
        }

        /* We're cool with filtering this. */
        ctx = f->ctx = kuda_pcalloc(f->r->pool, sizeof(*ctx));
        ctx->bb = kuda_brigade_create(f->r->pool, f->c->bucket_alloc);
        kuda_table_unset(f->r->headers_out, "Content-Length");
    }

    for (e = KUDA_BRIGADE_FIRST(bb);
         e != KUDA_BRIGADE_SENTINEL(bb);
         e = KUDA_BUCKET_NEXT(e))
    {
        const char *data;
        kuda_size_t len, i, lastpos;

        if (KUDA_BUCKET_IS_EOS(e)) {
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(ctx->bb, e);

            /* Okay, we've seen the EOS.
             * Time to pass it along down the chain.
             */
            return clhy_pass_brigade(f->next, ctx->bb);
        }

        if (KUDA_BUCKET_IS_FLUSH(e)) {
            /*
             * Ignore flush buckets for the moment..
             * we decide what to stream
             */
            continue;
        }

        if (KUDA_BUCKET_IS_METADATA(e)) {
            /* metadata bucket */
            kuda_bucket *cpy;
            kuda_bucket_copy(e, &cpy);
            KUDA_BRIGADE_INSERT_TAIL(ctx->bb, cpy);
            continue;
        }

        /* read */
        kuda_bucket_read(e, &data, &len, KUDA_BLOCK_READ);

        if (len > 0) {
            lastpos = 0;
            for (i = 0; i < len; i++) {
                if (data[i] == c->flushdelimiter ||
                    data[i] == c->bucketdelimiter ||
                    data[i] == c->passdelimiter) {
                    kuda_bucket *p;
                    if (i - lastpos > 0) {
                        p = kuda_bucket_pool_create(kuda_pmemdup(f->r->pool,
                                                               &data[lastpos],
                                                               i - lastpos),
                                                    i - lastpos,
                                                    f->r->pool,
                                                    f->c->bucket_alloc);
                        KUDA_BRIGADE_INSERT_TAIL(ctx->bb, p);
                    }
                    lastpos = i + 1;
                    if (data[i] == c->flushdelimiter) {
                        p = kuda_bucket_flush_create(f->c->bucket_alloc);
                        KUDA_BRIGADE_INSERT_TAIL(ctx->bb, p);
                    }
                    if (data[i] == c->passdelimiter) {
                        kuda_status_t rv;

                        rv = clhy_pass_brigade(f->next, ctx->bb);
                        if (rv) {
                            return rv;
                        }
                    }
                }
            }
            /* XXX: really should append this to the next 'real' bucket */
            if (lastpos < i) {
                kuda_bucket *p;
                p = kuda_bucket_pool_create(kuda_pmemdup(f->r->pool,
                                                       &data[lastpos],
                                                       i - lastpos),
                                           i - lastpos,
                                           f->r->pool,
                                           f->c->bucket_alloc);
                lastpos = i;
                KUDA_BRIGADE_INSERT_TAIL(ctx->bb, p);
            }
        }
    }

    return KUDA_SUCCESS;
}

static void register_hooks(kuda_pool_t * p)
{
    clhy_register_output_filter(bucketeerFilterName, bucketeer_out_filter,
                              NULL, CLHY_FTYPE_RESOURCE-1);
}

static const command_rec bucketeer_filter_cmds[] = {
    {NULL}
};

CLHY_DECLARE_CAPI(bucketeer) = {
    STANDARD16_CAPI_STUFF,
    NULL,
    NULL,
    create_bucketeer_server_config,
    NULL,
    bucketeer_filter_cmds,
    register_hooks
};
