dnl cAPIs enabled in this directory by default

dnl CLHYKUDEL_CAPI(name, helptext[, objects[, structname[, default[, config]]]])

CLHYKUDEL_CAPIPATH_INIT(clfilters)

CLHYKUDEL_CAPI(buffer, Filter Buffering, , , most)
CLHYKUDEL_CAPI(data, RFC2397 data encoder, , , )
CLHYKUDEL_CAPI(ratelimit, Output Bandwidth Limiting, , , most)
CLHYKUDEL_CAPI(reqtimeout, Limit time waiting for request from client, , , yes)
CLHYKUDEL_CAPI(ext_filter, external filter cAPI, , , most)
CLHYKUDEL_CAPI(request, Request Body Filtering, , , most)
CLHYKUDEL_CAPI(include, Server Side Includes, , , most)
CLHYKUDEL_CAPI(filter, Smart Filtering, , , yes)
CLHYKUDEL_CAPI(reflector, Reflect request through the output filter stack, , , )
CLHYKUDEL_CAPI(substitute, response content rewrite-like filtering, , , most)

sed_obj="capi_sed.lo sed0.lo sed1.lo regexp.lo"
CLHYKUDEL_CAPI(sed, filter request and/or response bodies through sed, $sed_obj, , most, [
    if test "x$enable_sed" = "xshared"; then
        # The only symbol which needs to be exported is the cAPI
        # structure, so ask libtool to hide libsed internals:
        KUDA_ADDTO(CAPI_SED_LDADD, [-export-symbols-regex sed_capi])
    fi
])

if test "$ac_cv_ebcdic" = "yes"; then
# capi_charset_lite can be very useful on an ebcdic system,
#   so include it by default
    CLHYKUDEL_CAPI(charset_lite, character set translation.  Enabled by default only on EBCDIC systems., , , yes)
else
    CLHYKUDEL_CAPI(charset_lite, character set translation.  Enabled by default only on EBCDIC systems., , , )
fi


CLHYKUDEL_CAPI(deflate, Deflate transfer encoding support, , , most, [
  AC_ARG_WITH(z, CLHYKUDEL_HELP_STRING(--with-z=PATH,use a specific zlib library),
  [
    if test "x$withval" != "xyes" && test "x$withval" != "x"; then
      clhy_zlib_base="$withval"
      clhy_zlib_with="yes"
    fi
  ])
  if test "x$clhy_zlib_base" = "x"; then
    AC_MSG_CHECKING([for zlib location])
    AC_CACHE_VAL(clhy_cv_zlib,[
      for dir in /usr/local /usr ; do
        if test -d $dir && test -f $dir/include/zlib.h; then
          clhy_cv_zlib=$dir
          break
        fi
      done
    ])
    clhy_zlib_base=$clhy_cv_zlib
    if test "x$clhy_zlib_base" = "x"; then
      enable_deflate=no
      AC_MSG_RESULT([not found])
    else
      AC_MSG_RESULT([$clhy_zlib_base])
    fi
  fi
  if test "$enable_deflate" != "no"; then
    clhy_save_includes=$INCLUDES
    clhy_save_ldflags=$LDFLAGS
    clhy_save_cppflags=$CPPFLAGS
    clhy_zlib_ldflags=""
    if test "$clhy_zlib_base" != "/usr"; then
      KUDA_ADDTO(INCLUDES, [-I${clhy_zlib_base}/include])
      KUDA_ADDTO(CAPI_INCLUDES, [-I${clhy_zlib_base}/include])
      dnl put in CPPFLAGS temporarily so that AC_TRY_LINK below will work
      CPPFLAGS="$CPPFLAGS $INCLUDES"
      KUDA_ADDTO(LDFLAGS, [-L${clhy_zlib_base}/lib])
      KUDA_ADDTO(clhy_zlib_ldflags, [-L${clhy_zlib_base}/lib])
      if test "x$clhy_platform_runtime_link_flag" != "x"; then
         KUDA_ADDTO(LDFLAGS, [$clhy_platform_runtime_link_flag${clhy_zlib_base}/lib])
         KUDA_ADDTO(clhy_zlib_ldflags, [$clhy_platform_runtime_link_flag${clhy_zlib_base}/lib])
      fi
    fi
    KUDA_ADDTO(LIBS, [-lz])
    AC_MSG_CHECKING([for zlib library])
    AC_TRY_LINK([#include <zlib.h>], [int i = Z_OK;], 
      [AC_MSG_RESULT(found) 
       KUDA_ADDTO(CAPI_DEFLATE_LDADD, [$clhy_zlib_ldflags -lz])],
      [AC_MSG_RESULT(not found)
       enable_deflate=no
       if test "x$clhy_zlib_with" = "x"; then
         AC_MSG_WARN([... Error, zlib was missing or unusable])
       else
         AC_MSG_ERROR([... Error, zlib was missing or unusable])
       fi
      ])
    INCLUDES=$clhy_save_includes
    LDFLAGS=$clhy_save_ldflags
    CPPFLAGS=$clhy_save_cppflags
    KUDA_REMOVEFROM(LIBS, [-lz])
  fi
])

AC_DEFUN([FIND_LIBXML2], [
  AC_CACHE_CHECK([for libxml2], [ac_cv_libxml2], [
    AC_ARG_WITH(libxml2,
      [CLHYKUDEL_HELP_STRING(--with-libxml2=PATH,location for libxml2)],
      [test_paths="${with_libxml2}"],
      [test_paths="/usr/include/libxml2 /usr/local/include/libxml2 /usr/include /usr/local/include"]
    )
    AC_MSG_CHECKING(for libxml2)
    xml2_path=""
    for x in ${test_paths}; do
        if test -f "${x}/libxml/parser.h"; then
          xml2_path="${x}"
          break
        fi
    done
    if test -n "${xml2_path}" ; then
      ac_cv_libxml2=yes
      XML2_INCLUDES="${xml2_path}"
    else
      ac_cv_libxml2=no
    fi
  ])
])

CLHYKUDEL_CAPI(xml2enc, i18n support for markup filters, , , , [
  FIND_LIBXML2
  if test "$ac_cv_libxml2" = "yes" ; then
    KUDA_ADDTO(CAPI_CFLAGS, [-I${XML2_INCLUDES}])
    KUDA_ADDTO(CAPI_XML2ENC_LDADD, [-lxml2])
  else
    enable_xml2enc=no
  fi
])
CLHYKUDEL_CAPI(proxy_html, Fix HTML Links in a Reverse Proxy, , , , [
  FIND_LIBXML2
  if test "$ac_cv_libxml2" = "yes" ; then
    KUDA_ADDTO(CAPI_CFLAGS, [-I${XML2_INCLUDES}])
    KUDA_ADDTO(CAPI_PROXY_HTML_LDADD, [-lxml2])
  else
    enable_proxy_html=no
  fi
]
)

CLHYKUDEL_CAPI(brotli, Brotli compression support, , , most, [
  AC_ARG_WITH(brotli, CLHYKUDEL_HELP_STRING(--with-brotli=PATH,Brotli installation directory),[
    if test "$withval" != "yes" -a "x$withval" != "x"; then
      clhy_brotli_base="$withval"
      clhy_brotli_with=yes
    fi
  ])
  clhy_brotli_found=no
  if test -n "$clhy_brotli_base"; then
    clhy_save_cppflags=$CPPFLAGS
    KUDA_ADDTO(CPPFLAGS, [-I${clhy_brotli_base}/include])
    AC_MSG_CHECKING([for Brotli library >= 0.6.0 via prefix])
    AC_TRY_COMPILE(
      [#include <brotli/encode.h>],[
const uint8_t *o = BrotliEncoderTakeOutput((BrotliEncoderState*)0, (size_t*)0);
if (o) return *o;],
      [AC_MSG_RESULT(yes)
       clhy_brotli_found=yes
       clhy_brotli_cflags="-I${clhy_brotli_base}/include"
       clhy_brotli_libs="-L${clhy_brotli_base}/lib -lbrotlienc -lbrotlicommon"],
      [AC_MSG_RESULT(no)]
    )
    CPPFLAGS=$clhy_save_cppflags
  else
    if test -n "$PKGCONFIG"; then
      AC_MSG_CHECKING([for Brotli library >= 0.6.0 via pkg-config])
      if $PKGCONFIG --exists "libbrotlienc >= 0.6.0"; then
        AC_MSG_RESULT(yes)
        clhy_brotli_found=yes
        clhy_brotli_cflags=`$PKGCONFIG libbrotlienc --cflags`
        clhy_brotli_libs=`$PKGCONFIG libbrotlienc --libs`
      else
        AC_MSG_RESULT(no)
      fi
    fi
  fi
  if test "$clhy_brotli_found" = "yes"; then
    KUDA_ADDTO(CAPI_CFLAGS, [$clhy_brotli_cflags])
    KUDA_ADDTO(CAPI_BROTLI_LDADD, [$clhy_brotli_libs])
    if test "$enable_brotli" = "shared"; then
      dnl The only symbol which needs to be exported is the cAPI
      dnl structure, so ask libtool to hide everything else:
      KUDA_ADDTO(CAPI_BROTLI_LDADD, [-export-symbols-regex brotli_capi])
    fi
  else
    enable_brotli=no
    if test "$clhy_brotli_with" = "yes"; then
      AC_MSG_ERROR([Brotli library was missing or unusable])
    fi
  fi
])

KUDA_ADDTO(INCLUDES, [-I\$(top_srcdir)/$capipath_current])

CLHYKUDEL_CAPIPATH_FINISH
