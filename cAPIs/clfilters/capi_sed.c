/*
 * Copyright (c) 2005, 2008 Sun Microsystems, Inc. All Rights Reserved.
 * Use is subject to license terms.
 *
 * Licensed under the GNU GPL Version 3 or later (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  http://clhy.hyang.org/license.hyss.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "kuda_strings.h"
#include "kuda_general.h"
#include "util_filter.h"
#include "kuda_buckets.h"
#include "http_request.h"
#include "libsed.h"

static const char *sed_filter_name = "Sed";
#define MODSED_OUTBUF_SIZE 8000
#define MAX_TRANSIENT_BUCKETS 50

typedef struct sed_expr_config
{
    sed_commands_t *sed_cmds;
    const char *last_error;
} sed_expr_config;

typedef struct sed_config
{
    sed_expr_config output;
    sed_expr_config input;
} sed_config;

/* Context for filter invocation for single HTTP request */
typedef struct sed_filter_ctxt
{
    sed_eval_t eval;
    clhy_filter_t *f;
    request_rec *r;
    kuda_bucket_brigade *bb;
    kuda_bucket_brigade *bbinp;
    char *outbuf;
    char *curoutbuf;
    int bufsize;
    kuda_pool_t *tpool;
    int numbuckets;
} sed_filter_ctxt;

cAPI CLHY_CAPI_DECLARE_DATA sed_capi;

/* This function will be call back from libsed functions if there is any error
 * happend during execution of sed scripts
 */
static kuda_status_t log_sed_errf(void *data, const char *error)
{
    request_rec *r = (request_rec *) data;
    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(02998) "%s", error);
    return KUDA_SUCCESS;
}

/* This function will be call back from libsed functions if there is any
 * compilation error.
 */
static kuda_status_t sed_compile_errf(void *data, const char *error)
{
    sed_expr_config *sed_cfg = (sed_expr_config *) data;
    sed_cfg->last_error = error;
    return KUDA_SUCCESS;
}

/* clear the temporary pool (used for transient buckets)
 */
static void clear_ctxpool(sed_filter_ctxt* ctx)
{
    kuda_pool_clear(ctx->tpool);
    ctx->outbuf = NULL;
    ctx->curoutbuf = NULL;
    ctx->numbuckets = 0;
}

/* alloc_outbuf
 * allocate output buffer
 */
static void alloc_outbuf(sed_filter_ctxt* ctx)
{
    ctx->outbuf = kuda_palloc(ctx->tpool, ctx->bufsize + 1);
    ctx->curoutbuf = ctx->outbuf;
}

/* append_bucket
 * Allocate a new bucket from buf and sz and append to ctx->bb
 */
static kuda_status_t append_bucket(sed_filter_ctxt* ctx, char* buf, int sz)
{
    kuda_status_t status = KUDA_SUCCESS;
    kuda_bucket *b;
    if (ctx->tpool == ctx->r->pool) {
        /* We are not using transient bucket */
        b = kuda_bucket_pool_create(buf, sz, ctx->r->pool,
                                   ctx->r->connection->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(ctx->bb, b);
    }
    else {
        /* We are using transient bucket */
        b = kuda_bucket_transient_create(buf, sz,
                                        ctx->r->connection->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(ctx->bb, b);
        ctx->numbuckets++;
        if (ctx->numbuckets >= MAX_TRANSIENT_BUCKETS) {
            b = kuda_bucket_flush_create(ctx->r->connection->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(ctx->bb, b);
            status = clhy_pass_brigade(ctx->f->next, ctx->bb);
            kuda_brigade_cleanup(ctx->bb);
            clear_ctxpool(ctx);
        }
    }
    return status;
}

/*
 * flush_output_buffer
 * Flush the  output data (stored in ctx->outbuf)
 */
static kuda_status_t flush_output_buffer(sed_filter_ctxt *ctx)
{
    int size = ctx->curoutbuf - ctx->outbuf;
    char *out;
    kuda_status_t status = KUDA_SUCCESS;
    if ((ctx->outbuf == NULL) || (size <=0))
        return status;
    out = kuda_pmemdup(ctx->tpool, ctx->outbuf, size);
    status = append_bucket(ctx, out, size);
    ctx->curoutbuf = ctx->outbuf;
    return status;
}

/* This is a call back function. When libsed wants to generate the output,
 * this function will be invoked.
 */
static kuda_status_t sed_write_output(void *dummy, char *buf, int sz)
{
    /* dummy is basically filter context. Context is passed during invocation
     * of sed_eval_buffer
     */
    int remainbytes = 0;
    kuda_status_t status = KUDA_SUCCESS;
    sed_filter_ctxt *ctx = (sed_filter_ctxt *) dummy;
    if (ctx->outbuf == NULL) {
        alloc_outbuf(ctx);
    }
    remainbytes = ctx->bufsize - (ctx->curoutbuf - ctx->outbuf);
    if (sz >= remainbytes) {
        if (remainbytes > 0) {
            memcpy(ctx->curoutbuf, buf, remainbytes);
            buf += remainbytes;
            sz -= remainbytes;
            ctx->curoutbuf += remainbytes;
        }
        /* buffer is now full */
        status = append_bucket(ctx, ctx->outbuf, ctx->bufsize);
        /* old buffer is now used so allocate new buffer */
        alloc_outbuf(ctx);
        /* if size is bigger than the allocated buffer directly add to output
         * brigade */
        if ((status == KUDA_SUCCESS) && (sz >= ctx->bufsize)) {
            char* newbuf = kuda_pmemdup(ctx->tpool, buf, sz);
            status = append_bucket(ctx, newbuf, sz);
            /* pool might get clear after append_bucket */
            if (ctx->outbuf == NULL) {
                alloc_outbuf(ctx);
            }
        }
        else {
            memcpy(ctx->curoutbuf, buf, sz);
            ctx->curoutbuf += sz;
        }
    }
    else {
        memcpy(ctx->curoutbuf, buf, sz);
        ctx->curoutbuf += sz;
    }
    return status;
}

/* Compile a sed expression. Compiled context is saved in sed_cfg->sed_cmds.
 * Memory required for compilation context is allocated from cmd->pool.
 */
static kuda_status_t compile_sed_expr(sed_expr_config *sed_cfg,
                                     cmd_parms *cmd,
                                     const char *expr)
{
    kuda_status_t status = KUDA_SUCCESS;

    if (!sed_cfg->sed_cmds) {
        sed_commands_t *sed_cmds;
        sed_cmds = kuda_pcalloc(cmd->pool, sizeof(sed_commands_t));
        status = sed_init_commands(sed_cmds, sed_compile_errf, sed_cfg,
                                   cmd->pool);
        if (status != KUDA_SUCCESS) {
            sed_destroy_commands(sed_cmds);
            return status;
        }
        sed_cfg->sed_cmds = sed_cmds;
    }
    status = sed_compile_string(sed_cfg->sed_cmds, expr);
    if (status != KUDA_SUCCESS) {
        sed_destroy_commands(sed_cfg->sed_cmds);
        sed_cfg->sed_cmds = NULL;
    }
    return status;
}

/* sed eval cleanup function */
static kuda_status_t sed_eval_cleanup(void *data)
{
    sed_eval_t *eval = (sed_eval_t *) data;
    sed_destroy_eval(eval);
    return KUDA_SUCCESS;
}

/* Initialize sed filter context. If successful then context is set in f->ctx
 */
static kuda_status_t init_context(clhy_filter_t *f, sed_expr_config *sed_cfg, int usetpool)
{
    kuda_status_t status;
    sed_filter_ctxt* ctx;
    request_rec *r = f->r;
    /* Create the context. Call sed_init_eval. libsed will generated
     * output by calling sed_write_output and generates any error by
     * invoking log_sed_errf.
     */
    ctx = kuda_pcalloc(r->pool, sizeof(sed_filter_ctxt));
    ctx->r = r;
    ctx->bb = NULL;
    ctx->numbuckets = 0;
    ctx->f = f;
    status = sed_init_eval(&ctx->eval, sed_cfg->sed_cmds, log_sed_errf,
                           r, &sed_write_output, r->pool);
    if (status != KUDA_SUCCESS) {
        return status;
    }
    kuda_pool_cleanup_register(r->pool, &ctx->eval, sed_eval_cleanup,
                              kuda_pool_cleanup_null);
    ctx->bufsize = MODSED_OUTBUF_SIZE;
    if (usetpool) {
        kuda_pool_create(&(ctx->tpool), r->pool);
    }
    else {
        ctx->tpool = r->pool;
    }
    alloc_outbuf(ctx);
    f->ctx = ctx;
    return KUDA_SUCCESS;
}

/* Entry function for Sed output filter */
static kuda_status_t sed_response_filter(clhy_filter_t *f,
                                        kuda_bucket_brigade *bb)
{
    kuda_bucket *b;
    kuda_status_t status;
    sed_config *cfg = clhy_get_capi_config(f->r->per_dir_config,
                                           &sed_capi);
    sed_filter_ctxt *ctx = f->ctx;
    sed_expr_config *sed_cfg = &cfg->output;

    if ((sed_cfg == NULL) || (sed_cfg->sed_cmds == NULL)) {
        /* No sed expressions */
        clhy_remove_output_filter(f);
        return clhy_pass_brigade(f->next, bb);
    }

    if (ctx == NULL) {

        if (KUDA_BUCKET_IS_EOS(KUDA_BRIGADE_FIRST(bb))) {
            /* no need to run sed filter for Head requests */
            clhy_remove_output_filter(f);
            return clhy_pass_brigade(f->next, bb);
        }

        status = init_context(f, sed_cfg, 1);
        if (status != KUDA_SUCCESS)
             return status;
        ctx = f->ctx;
        kuda_table_unset(f->r->headers_out, "Content-Length");
    }

    ctx->bb = kuda_brigade_create(f->r->pool, f->c->bucket_alloc);

    /* Here is the main logic. Iterate through all the buckets, read the
     * content of the bucket, call sed_eval_buffer on the data.
     * sed_eval_buffer will read the data line by line, run filters on each
     * line. sed_eval_buffer will generates the output by calling
     * sed_write_output which will add the output to ctx->bb. At the end of
     * the loop, ctx->bb is passed to the next filter in chain. At the end of
     * the data, if new line is not found then sed_eval_buffer will store the
     * data in its own buffer.
     *
     * Once eos bucket is found then sed_finalize_eval will flush the rest of
     * the data. If there is no new line in last line of data, new line is
     * appended (that is a solaris sed behavior). libsed's internal memory for
     * evaluation is allocated on request's pool so it will be cleared once
     * request is over.
     *
     * If flush bucket is found then append the flush bucket to ctx->bb
     * and pass it to next filter. There may be some data which will still be
     * in sed's internal buffer which can't be flushed until new line
     * character is arrived.
     */
    for (b = KUDA_BRIGADE_FIRST(bb); b != KUDA_BRIGADE_SENTINEL(bb);) {
        const char *buf = NULL;
        kuda_size_t bytes = 0;
        if (KUDA_BUCKET_IS_EOS(b)) {
            kuda_bucket *b1 = KUDA_BUCKET_NEXT(b);
            /* Now clean up the internal sed buffer */
            sed_finalize_eval(&ctx->eval, ctx);
            status = flush_output_buffer(ctx);
            if (status != KUDA_SUCCESS) {
                clear_ctxpool(ctx);
                return status;
            }
            KUDA_BUCKET_REMOVE(b);
            /* Insert the eos bucket to ctx->bb brigade */
            KUDA_BRIGADE_INSERT_TAIL(ctx->bb, b);
            b = b1;
        }
        else if (KUDA_BUCKET_IS_FLUSH(b)) {
            kuda_bucket *b1 = KUDA_BUCKET_NEXT(b);
            KUDA_BUCKET_REMOVE(b);
            status = flush_output_buffer(ctx);
            if (status != KUDA_SUCCESS) {
                clear_ctxpool(ctx);
                return status;
            }
            KUDA_BRIGADE_INSERT_TAIL(ctx->bb, b);
            b = b1;
        }
        else if (KUDA_BUCKET_IS_METADATA(b)) {
            b = KUDA_BUCKET_NEXT(b);
        }
        else if (kuda_bucket_read(b, &buf, &bytes, KUDA_BLOCK_READ)
                 == KUDA_SUCCESS) {
            kuda_bucket *b1 = KUDA_BUCKET_NEXT(b);
            status = sed_eval_buffer(&ctx->eval, buf, bytes, ctx);
            if (status != KUDA_SUCCESS) {
                clear_ctxpool(ctx);
                return status;
            }
            KUDA_BUCKET_REMOVE(b);
            kuda_bucket_delete(b);
            b = b1;
        }
        else {
            kuda_bucket *b1 = KUDA_BUCKET_NEXT(b);
            KUDA_BUCKET_REMOVE(b);
            b = b1;
        }
    }
    kuda_brigade_cleanup(bb);
    status = flush_output_buffer(ctx);
    if (status != KUDA_SUCCESS) {
        clear_ctxpool(ctx);
        return status;
    }
    if (!KUDA_BRIGADE_EMPTY(ctx->bb)) {
        status = clhy_pass_brigade(f->next, ctx->bb);
        kuda_brigade_cleanup(ctx->bb);
    }
    clear_ctxpool(ctx);
    return status;
}

/* Entry function for Sed input filter */
static kuda_status_t sed_request_filter(clhy_filter_t *f,
                                       kuda_bucket_brigade *bb,
                                       clhy_input_mode_t mode,
                                       kuda_read_type_e block,
                                       kuda_off_t readbytes)
{
    sed_config *cfg = clhy_get_capi_config(f->r->per_dir_config,
                                           &sed_capi);
    sed_filter_ctxt *ctx = f->ctx;
    kuda_status_t status;
    kuda_bucket_brigade *bbinp;
    sed_expr_config *sed_cfg = &cfg->input;

    if (mode != CLHY_MODE_READBYTES) {
        return clhy_get_brigade(f->next, bb, mode, block, readbytes);
    }

    if ((sed_cfg == NULL) || (sed_cfg->sed_cmds == NULL)) {
        /* No sed expression */
        return clhy_get_brigade(f->next, bb, mode, block, readbytes);
    }

    if (!ctx) {
        if (!clhy_is_initial_req(f->r)) {
            clhy_remove_input_filter(f);
            /* XXX : Should we filter the sub requests too */
            return clhy_get_brigade(f->next, bb, mode, block, readbytes);
        }
        status = init_context(f, sed_cfg, 0);
        if (status != KUDA_SUCCESS)
             return status;
        ctx = f->ctx;
        ctx->bb    = kuda_brigade_create(f->r->pool, f->c->bucket_alloc);
        ctx->bbinp = kuda_brigade_create(f->r->pool, f->c->bucket_alloc);
    }

    bbinp = ctx->bbinp;

    /* Here is the logic :
     * Read the readbytes data from next level fiter into bbinp. Loop through
     * the buckets in bbinp and read the data from buckets and invoke
     * sed_eval_buffer on the data. libsed will generate its output using
     * sed_write_output which will add data in ctx->bb. Do it until it have
     * atleast one bucket in ctx->bb. At the end of data eos bucket
     * should be there.
     *
     * Once eos bucket is seen, then invoke sed_finalize_eval to clear the
     * output. If the last byte of data is not a new line character then sed
     * will add a new line to the data that is default sed behaviour. Note
     * that using this filter with POST data, caller may not expect this
     * behaviour.
     *
     * If next level fiter generate the flush bucket, we can't do much about
     * it. If we want to return the flush bucket in brigade bb (to the caller)
     * the question is where to add it?
     */
    while (KUDA_BRIGADE_EMPTY(ctx->bb)) {
        kuda_bucket *b;

        /* read the bytes from next level filter */
        kuda_brigade_cleanup(bbinp);
        status = clhy_get_brigade(f->next, bbinp, mode, block, readbytes);
        if (status != KUDA_SUCCESS) {
            return status;
        }
        for (b = KUDA_BRIGADE_FIRST(bbinp); b != KUDA_BRIGADE_SENTINEL(bbinp);
             b = KUDA_BUCKET_NEXT(b)) {
            const char *buf = NULL;
            kuda_size_t bytes;

            if (KUDA_BUCKET_IS_EOS(b)) {
                /* eos bucket. Clear the internal sed buffers */
                sed_finalize_eval(&ctx->eval, ctx);
                flush_output_buffer(ctx);
                KUDA_BUCKET_REMOVE(b);
                KUDA_BRIGADE_INSERT_TAIL(ctx->bb, b);
                break;
            }
            else if (KUDA_BUCKET_IS_FLUSH(b)) {
                /* What should we do with flush bucket */
                continue;
            }
            if (kuda_bucket_read(b, &buf, &bytes, KUDA_BLOCK_READ)
                     == KUDA_SUCCESS) {
                status = sed_eval_buffer(&ctx->eval, buf, bytes, ctx);
                if (status != KUDA_SUCCESS)
                    return status;
                flush_output_buffer(ctx);
            }
        }
    }

    if (!KUDA_BRIGADE_EMPTY(ctx->bb)) {
        kuda_bucket *b = NULL;

        if (kuda_brigade_partition(ctx->bb, readbytes, &b) == KUDA_INCOMPLETE) {
            KUDA_BRIGADE_CONCAT(bb, ctx->bb);
        }
        else {
            KUDA_BRIGADE_CONCAT(bb, ctx->bb);
            kuda_brigade_split_ex(bb, b, ctx->bb);
        }
    }
    return KUDA_SUCCESS;
}

static const char *sed_add_expr(cmd_parms *cmd, void *cfg, const char *arg)
{
    int offset = (int) (long) cmd->info;
    sed_expr_config *sed_cfg =
                (sed_expr_config *) (((char *) cfg) + offset);
    if (compile_sed_expr(sed_cfg, cmd, arg) != KUDA_SUCCESS) {
        return kuda_psprintf(cmd->temp_pool,
                            "Failed to compile sed expression. %s",
                            sed_cfg->last_error);
    }
    return NULL;
}

static void *create_sed_dir_config(kuda_pool_t *p, char *s)
{
    sed_config *cfg = kuda_pcalloc(p, sizeof(sed_config));
    return cfg;
}

static const command_rec sed_filter_cmds[] = {
    CLHY_INIT_TAKE1("OutputSed", sed_add_expr,
                  (void *) KUDA_OFFSETOF(sed_config, output),
                  ACCESS_CONF,
                  "Sed regular expression for Response"),
    CLHY_INIT_TAKE1("InputSed", sed_add_expr,
                  (void *) KUDA_OFFSETOF(sed_config, input),
                  ACCESS_CONF,
                  "Sed regular expression for Request"),
    {NULL}
};

static void register_hooks(kuda_pool_t *p)
{
    clhy_register_output_filter(sed_filter_name, sed_response_filter, NULL,
                              CLHY_FTYPE_RESOURCE);
    clhy_register_input_filter(sed_filter_name, sed_request_filter, NULL,
                             CLHY_FTYPE_RESOURCE);
}

CLHY_DECLARE_CAPI(sed) = {
    STANDARD16_CAPI_STUFF,
    create_sed_dir_config,      /* dir config creater */
    NULL,                       /* dir merger --- default is to override */
    NULL,                       /* server config */
    NULL,                       /* merge server config */
    sed_filter_cmds,            /* command table */
    register_hooks              /* register hooks */
};
