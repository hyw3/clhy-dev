/* The cLHy Server
 *
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define KUDA_WANT_STRFUNC
#include "kuda_want.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_hash.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_request.h"
#include "http_log.h"
#include "util_filter.h"
#include "clhy_expr.h"

cAPI CLHY_CAPI_DECLARE_DATA filter_capi;

/**
 * @brief is a filter provider, as defined and implemented by capi_filter.
 *
 * The struct is a linked list, with dispatch criteria
 * defined for each filter.  The provider implementation itself is a
 * (2.0-compatible) clhy_filter_rec_t* frec.
 */
struct clhy_filter_provider_t {
    clhy_expr_info_t *expr;
    const char **types;

    /** The filter that implements this provider */
    clhy_filter_rec_t *frec;

    /** The next provider in the list */
    clhy_filter_provider_t *next;
};

/** we need provider_ctx to save ctx values set by providers in filter_init */
typedef struct provider_ctx provider_ctx;
struct provider_ctx {
    clhy_filter_provider_t *provider;
    void *ctx;
    provider_ctx *next;
};
typedef struct {
    clhy_out_filter_func func;
    void *fctx;
    provider_ctx *init_ctx;
} harness_ctx;

typedef struct capi_filter_chain {
    const char *fname;
    struct capi_filter_chain *next;
} capi_filter_chain;

typedef struct {
    kuda_hash_t *live_filters;
    capi_filter_chain *chain;
} capi_filter_cfg;

typedef struct {
    const char* range ;
} capi_filter_ctx ;


static void filter_trace(conn_rec *c, int debug, const char *fname,
                         kuda_bucket_brigade *bb)
{
    kuda_bucket *b;

    switch (debug) {
    case 0:        /* normal, operational use */
        return;
    case 1:        /* capi_diagnostics level */
        clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, c, CLHYLOGNO(01375) "%s", fname);
        for (b = KUDA_BRIGADE_FIRST(bb);
             b != KUDA_BRIGADE_SENTINEL(bb);
             b = KUDA_BUCKET_NEXT(b)) {

            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, c, CLHYLOGNO(01376)
                          "%s: type: %s, length: %" KUDA_SIZE_T_FMT,
                          fname, b->type->name ? b->type->name : "(unknown)",
                          b->length);
        }
        break;
    }
}

static int filter_init(clhy_filter_t *f)
{
    clhy_filter_provider_t *p;
    provider_ctx *pctx;
    int err;
    clhy_filter_rec_t *filter = f->frec;

    harness_ctx *fctx = kuda_pcalloc(f->r->pool, sizeof(harness_ctx));
    for (p = filter->providers; p; p = p->next) {
        if (p->frec->filter_init_func == filter_init) {
            clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, f->c, CLHYLOGNO(01377)
                          "Chaining of FilterProviders not supported");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
        else if (p->frec->filter_init_func) {
            f->ctx = NULL;
            if ((err = p->frec->filter_init_func(f)) != OK) {
                clhy_log_cerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, f->c, CLHYLOGNO(01378)
                              "filter_init for %s failed", p->frec->name);
                return err;   /* if anyone errors out here, so do we */
            }
            if (f->ctx != NULL) {
                /* the filter init function set a ctx - we need to record it */
                pctx = kuda_pcalloc(f->r->pool, sizeof(provider_ctx));
                pctx->provider = p;
                pctx->ctx = f->ctx;
                pctx->next = fctx->init_ctx;
                fctx->init_ctx = pctx;
            }
        }
    }
    f->ctx = fctx;
    return OK;
}

static int filter_lookup(clhy_filter_t *f, clhy_filter_rec_t *filter)
{
    clhy_filter_provider_t *provider;
    int match = 0;
    const char *err = NULL;
    request_rec *r = f->r;
    harness_ctx *ctx = f->ctx;
    provider_ctx *pctx;
#ifndef NO_PROTOCOL
    unsigned int proto_flags;
    capi_filter_ctx *rctx = clhy_get_capi_config(r->request_config,
                                                &filter_capi);
#endif

    /* Check registered providers in order */
    for (provider = filter->providers; provider; provider = provider->next) {
        if (provider->expr) {
            match = clhy_expr_exec(r, provider->expr, &err);
            if (err) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01379)
                              "Error evaluating filter dispatch condition: %s",
                              err);
                match = 0;
            }
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                          "Expression condition for '%s' %s",
                          provider->frec->name,
                          match ? "matched" : "did not match");
        }
        else if (r->content_type) {
            const char **type = provider->types;
            size_t len = strcspn(r->content_type, "; \t");
            CLHY_DEBUG_ASSERT(type != NULL);
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE4, 0, r,
                          "Content-Type '%s' ...", r->content_type);
            while (*type) {
                /* Handle 'content-type;charset=...' correctly */
                if (strncmp(*type, r->content_type, len) == 0
                    && (*type)[len] == '\0') {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE4, 0, r,
                                  "... matched '%s'", *type);
                    match = 1;
                    break;
                }
                else {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE4, 0, r,
                                  "... did not match '%s'", *type);
                }
                type++;
            }
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                          "Content-Type condition for '%s' %s",
                          provider->frec->name,
                          match ? "matched" : "did not match");
        }
        else {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE2, 0, r,
                          "Content-Type condition for '%s' did not match: "
                          "no Content-Type", provider->frec->name);
        }

        if (match) {
            /* condition matches this provider */
#ifndef NO_PROTOCOL
            /* check protocol
             *
             * FIXME:
             * This is a quick hack and almost certainly buggy.
             * The idea is that by putting this in capi_filter, we relieve
             * filter implementations of the burden of fixing up HTTP headers
             * for cases that are routinely affected by filters.
             *
             * Default is ALWAYS to do nothing, so as not to tread on the
             * toes of filters which want to do it themselves.
             *
             */
            proto_flags = provider->frec->proto_flags;

            /* some specific things can't happen in a proxy */
            if (r->proxyreq) {
                if (proto_flags & CLHY_FILTER_PROTO_NO_PROXY) {
                    /* can't use this provider; try next */
                    continue;
                }

                if (proto_flags & CLHY_FILTER_PROTO_TRANSFORM) {
                    const char *str = kuda_table_get(r->headers_out,
                                                    "Cache-Control");
                    if (str) {
                        if (clhy_strcasestr(str, "no-transform")) {
                            /* can't use this provider; try next */
                            continue;
                        }
                    }
                    kuda_table_addn(r->headers_out, "Warning",
                                   kuda_psprintf(r->pool,
                                                "214 %s Transformation applied",
                                                r->hostname));
                }
            }

            /* things that are invalidated if the filter transforms content */
            if (proto_flags & CLHY_FILTER_PROTO_CHANGE) {
                kuda_table_unset(r->headers_out, "Content-MD5");
                kuda_table_unset(r->headers_out, "ETag");
                if (proto_flags & CLHY_FILTER_PROTO_CHANGE_LENGTH) {
                    kuda_table_unset(r->headers_out, "Content-Length");
                }
            }

            /* no-cache is for a filter that has different effect per-hit */
            if (proto_flags & CLHY_FILTER_PROTO_NO_CACHE) {
                kuda_table_unset(r->headers_out, "Last-Modified");
                kuda_table_addn(r->headers_out, "Cache-Control", "no-cache");
            }

            if (proto_flags & CLHY_FILTER_PROTO_NO_BYTERANGE) {
                kuda_table_setn(r->headers_out, "Accept-Ranges", "none");
            }
            else if (rctx && rctx->range) {
                /* restore range header we saved earlier */
                kuda_table_setn(r->headers_in, "Range", rctx->range);
                rctx->range = NULL;
            }
#endif
            for (pctx = ctx->init_ctx; pctx; pctx = pctx->next) {
                if (pctx->provider == provider) {
                    ctx->fctx = pctx->ctx ;
                }
            }
            ctx->func = provider->frec->filter_func.out_func;
            return 1;
        }
    }

    /* No provider matched */
    return 0;
}

static kuda_status_t filter_harness(clhy_filter_t *f, kuda_bucket_brigade *bb)
{
    kuda_status_t ret;
#ifndef NO_PROTOCOL
    const char *cachecontrol;
#endif
    harness_ctx *ctx = f->ctx;
    clhy_filter_rec_t *filter = f->frec;

    if (f->r->status != 200
        && !kuda_table_get(f->r->subprocess_env, "filter-errordocs")) {
        clhy_remove_output_filter(f);
        return clhy_pass_brigade(f->next, bb);
    }

    filter_trace(f->c, filter->debug, f->frec->name, bb);

    /* look up a handler function if we haven't already set it */
    if (!ctx->func) {
#ifndef NO_PROTOCOL
        if (f->r->proxyreq) {
            if (filter->proto_flags & CLHY_FILTER_PROTO_NO_PROXY) {
                clhy_remove_output_filter(f);
                return clhy_pass_brigade(f->next, bb);
            }

            if (filter->proto_flags & CLHY_FILTER_PROTO_TRANSFORM) {
                cachecontrol = kuda_table_get(f->r->headers_out,
                                             "Cache-Control");
                if (cachecontrol) {
                    if (clhy_strcasestr(cachecontrol, "no-transform")) {
                        clhy_remove_output_filter(f);
                        return clhy_pass_brigade(f->next, bb);
                    }
                }
            }
        }
#endif
        if (!filter_lookup(f, filter)) {
            clhy_remove_output_filter(f);
            return clhy_pass_brigade(f->next, bb);
        }
        CLHY_DEBUG_ASSERT(ctx->func != NULL);
    }

    /* call the content filter with its own context, then restore our
     * context
     */
    f->ctx = ctx->fctx;
    ret = ctx->func(f, bb);
    ctx->fctx = f->ctx;
    f->ctx = ctx;

    return ret;
}

#ifndef NO_PROTOCOL
static const char *filter_protocol(cmd_parms *cmd, void *CFG, const char *fname,
                                   const char *pname, const char *proto)
{
    static const char *sep = ";, \t";
    char *arg;
    char *tok = 0;
    unsigned int flags = 0;
    capi_filter_cfg *cfg = CFG;
    clhy_filter_provider_t *provider = NULL;
    clhy_filter_rec_t *filter = kuda_hash_get(cfg->live_filters, fname,
                                           KUDA_HASH_KEY_STRING);

    if (!filter) {
        return "FilterProtocol: No such filter";
    }

    /* Fixup the args: it's really pname that's optional */
    if (proto == NULL) {
        proto = pname;
        pname = NULL;
    }
    else {
        /* Find provider */
        for (provider = filter->providers; provider; provider = provider->next) {
            if (!strcasecmp(provider->frec->name, pname)) {
                break;
            }
        }
        if (!provider) {
            return "FilterProtocol: No such provider for this filter";
        }
    }

    /* Now set flags from our args */
    for (arg = kuda_strtok(kuda_pstrdup(cmd->temp_pool, proto), sep, &tok);
         arg; arg = kuda_strtok(NULL, sep, &tok)) {

        if (!strcasecmp(arg, "change=yes")) {
            flags |= CLHY_FILTER_PROTO_CHANGE | CLHY_FILTER_PROTO_CHANGE_LENGTH;
        }
        if (!strcasecmp(arg, "change=no")) {
            flags &= ~(CLHY_FILTER_PROTO_CHANGE | CLHY_FILTER_PROTO_CHANGE_LENGTH);
        }
        else if (!strcasecmp(arg, "change=1:1")) {
            flags |= CLHY_FILTER_PROTO_CHANGE;
        }
        else if (!strcasecmp(arg, "byteranges=no")) {
            flags |= CLHY_FILTER_PROTO_NO_BYTERANGE;
        }
        else if (!strcasecmp(arg, "proxy=no")) {
            flags |= CLHY_FILTER_PROTO_NO_PROXY;
        }
        else if (!strcasecmp(arg, "proxy=transform")) {
            flags |= CLHY_FILTER_PROTO_TRANSFORM;
        }
        else if (!strcasecmp(arg, "cache=no")) {
            flags |= CLHY_FILTER_PROTO_NO_CACHE;
        }
    }

    if (pname) {
        provider->frec->proto_flags = flags;
    }
    else {
        filter->proto_flags = flags;
    }

    return NULL;
}
#endif

static const char *filter_declare(cmd_parms *cmd, void *CFG, const char *fname,
                                  const char *place)
{
    capi_filter_cfg *cfg = (capi_filter_cfg *)CFG;
    clhy_filter_rec_t *filter;

    filter = kuda_pcalloc(cmd->pool, sizeof(clhy_filter_rec_t));
    kuda_hash_set(cfg->live_filters, fname, KUDA_HASH_KEY_STRING, filter);

    filter->name = fname;
    filter->filter_init_func = filter_init;
    filter->filter_func.out_func = filter_harness;
    filter->ftype = CLHY_FTYPE_RESOURCE;
    filter->next = NULL;

    if (place) {
        if (!strcasecmp(place, "CONTENT_SET")) {
            filter->ftype = CLHY_FTYPE_CONTENT_SET;
        }
        else if (!strcasecmp(place, "PROTOCOL")) {
            filter->ftype = CLHY_FTYPE_PROTOCOL;
        }
        else if (!strcasecmp(place, "CONNECTION")) {
            filter->ftype = CLHY_FTYPE_CONNECTION;
        }
        else if (!strcasecmp(place, "NETWORK")) {
            filter->ftype = CLHY_FTYPE_NETWORK;
        }
    }

    return NULL;
}

static const char *add_filter(cmd_parms *cmd, void *CFG,
                              const char *fname, const char *pname,
                              const char *expr, const char **types)
{
    capi_filter_cfg *cfg = CFG;
    clhy_filter_provider_t *provider;
    const char *c;
    clhy_filter_rec_t* frec;
    clhy_filter_rec_t* provider_frec;
    clhy_expr_info_t *node;
    const char *err = NULL;

    /* if provider has been registered, we can look it up */
    provider_frec = clhy_get_output_filter_handle(pname);
    if (!provider_frec) {
        return kuda_psprintf(cmd->pool, "Unknown filter provider %s", pname);
    }

    /* fname has been declared with DeclareFilter, so we can look it up */
    frec = kuda_hash_get(cfg->live_filters, fname, KUDA_HASH_KEY_STRING);

    /* or if provider is capi_filter itself, we can also look it up */
    if (!frec) {
        c = filter_declare(cmd, CFG, fname, NULL);
        if ( c ) {
            return c;
        }
        frec = kuda_hash_get(cfg->live_filters, fname, KUDA_HASH_KEY_STRING);
        frec->ftype = provider_frec->ftype;
    }

    if (!frec) {
        return kuda_psprintf(cmd->pool, "Undeclared smart filter %s", fname);
    }

    provider = kuda_palloc(cmd->pool, sizeof(clhy_filter_provider_t));
    if (expr) {
        node = clhy_expr_parse_cmd(cmd, expr, 0, &err, NULL);
        if (err) {
            return kuda_pstrcat(cmd->pool,
                               "Error parsing FilterProvider expression:", err,
                               NULL);
        }
        provider->expr = node;
        provider->types = NULL;
    }
    else {
        provider->types = types;
        provider->expr = NULL;
    }
    provider->frec = provider_frec;
    provider->next = frec->providers;
    frec->providers = provider;
    return NULL;
}

static const char *filter_provider(cmd_parms *cmd, void *CFG,
                                   const char *fname, const char *pname,
                                   const char *expr)
{
    return add_filter(cmd, CFG, fname, pname, expr, NULL);
}

static const char *filter_chain(cmd_parms *cmd, void *CFG, const char *arg)
{
    capi_filter_chain *p;
    capi_filter_chain *q;
    capi_filter_cfg *cfg = CFG;

    switch (arg[0]) {
    case '+':        /* add to end of chain */
        p = kuda_pcalloc(cmd->pool, sizeof(capi_filter_chain));
        p->fname = arg+1;
        if (cfg->chain) {
            for (q = cfg->chain; q->next; q = q->next);
            q->next = p;
        }
        else {
            cfg->chain = p;
        }
        break;

    case '@':        /* add to start of chain */
        p = kuda_palloc(cmd->pool, sizeof(capi_filter_chain));
        p->fname = arg+1;
        p->next = cfg->chain;
        cfg->chain = p;
        break;

    case '-':        /* remove from chain */
        if (cfg->chain) {
            if (strcasecmp(cfg->chain->fname, arg+1)) {
                for (p = cfg->chain; p->next; p = p->next) {
                    if (!strcasecmp(p->next->fname, arg+1)) {
                        p->next = p->next->next;
                    }
                }
            }
            else {
                cfg->chain = cfg->chain->next;
            }
        }
        break;

    case '!':        /* Empty the chain */
                     /** IG: Add a NULL provider to the beginning so that
                      *  we can ensure that we'll empty everything before
                      *  this when doing config merges later */
        p = kuda_pcalloc(cmd->pool, sizeof(capi_filter_chain));
        p->fname = NULL;
        cfg->chain = p;
        break;

    case '=':        /* initialise chain with this arg */
                     /** IG: Prepend a NULL provider to the beginning as above */
        p = kuda_pcalloc(cmd->pool, sizeof(capi_filter_chain));
        p->fname = NULL;
        p->next = kuda_pcalloc(cmd->pool, sizeof(capi_filter_chain));
        p->next->fname = arg+1;
        cfg->chain = p;
        break;

    default:        /* add to end */
        p = kuda_pcalloc(cmd->pool, sizeof(capi_filter_chain));
        p->fname = arg;
        if (cfg->chain) {
            for (q = cfg->chain; q->next; q = q->next);
            q->next = p;
        }
        else {
            cfg->chain = p;
        }
        break;
    }

    return NULL;
}

static const char *filter_bytype1(cmd_parms *cmd, void *CFG,
                                  const char *pname, const char **types)
{
    const char *rv;
    const char *fname;
    int seen_name = 0;
    capi_filter_cfg *cfg = CFG;

    /* construct fname from name */
    fname = kuda_pstrcat(cmd->pool, "BYTYPE:", pname, NULL);

    /* check whether this is already registered, in which case
     * it's already in the filter chain
     */
    if (kuda_hash_get(cfg->live_filters, fname, KUDA_HASH_KEY_STRING)) {
        seen_name = 1;
    }

    rv = add_filter(cmd, CFG, fname, pname, NULL, types);

    /* If it's the first time through, add to filterchain */
    if (rv == NULL && !seen_name) {
        rv = filter_chain(cmd, CFG, fname);
    }
    return rv;
}

static const char *filter_bytype(cmd_parms *cmd, void *CFG,
                                 int argc, char *const argv[])
{
    /* back compatibility, need to parse multiple components in filter name */
    char *pname;
    char *strtok_state = NULL;
    char *name;
    const char **types;
    const char *rv = NULL;
    if (argc < 2)
        return "AddOutputFilterByType requires at least two arguments";
    name = kuda_pstrdup(cmd->temp_pool, argv[0]);
    types = kuda_palloc(cmd->pool, argc * sizeof(char *));
    memcpy(types, &argv[1], (argc - 1) * sizeof(char *));
    types[argc-1] = NULL;
    for (pname = kuda_strtok(name, ";", &strtok_state);
         pname != NULL && rv == NULL;
         pname = kuda_strtok(NULL, ";", &strtok_state)) {
        rv = filter_bytype1(cmd, CFG, pname, types);
    }
    return rv;
}

static const char *filter_debug(cmd_parms *cmd, void *CFG, const char *fname,
                                const char *level)
{
    capi_filter_cfg *cfg = CFG;
    clhy_filter_rec_t *frec = kuda_hash_get(cfg->live_filters, fname,
                                         KUDA_HASH_KEY_STRING);
    if (!frec) {
        return kuda_psprintf(cmd->pool, "Undeclared smart filter %s", fname);
    }
    frec->debug = atoi(level);

    return NULL;
}

static void filter_insert(request_rec *r)
{
    capi_filter_chain *p;
    clhy_filter_rec_t *filter;
    capi_filter_cfg *cfg = clhy_get_capi_config(r->per_dir_config,
                                               &filter_capi);
#ifndef NO_PROTOCOL
    int ranges = 1;
    capi_filter_ctx *ctx = kuda_pcalloc(r->pool, sizeof(capi_filter_ctx));
    clhy_set_capi_config(r->request_config, &filter_capi, ctx);
#endif

    /** IG: Now that we've merged to the final config, go one last time
     *  through the chain, and prune out the NULL filters */

    for (p = cfg->chain; p; p = p->next) {
        if (p->fname == NULL)
            cfg->chain = p->next;
    }

    for (p = cfg->chain; p; p = p->next) {
        filter = kuda_hash_get(cfg->live_filters, p->fname, KUDA_HASH_KEY_STRING);
        if (filter == NULL) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01380)
                          "Unknown filter %s not added", p->fname);
            continue;
        }
        clhy_add_output_filter_handle(filter, NULL, r, r->connection);
#ifndef NO_PROTOCOL
        if (ranges && (filter->proto_flags
                       & (CLHY_FILTER_PROTO_NO_BYTERANGE
                          | CLHY_FILTER_PROTO_CHANGE_LENGTH))) {
            ctx->range = kuda_table_get(r->headers_in, "Range");
            kuda_table_unset(r->headers_in, "Range");
            ranges = 0;
        }
#endif
    }
}

static void filter_hooks(kuda_pool_t *pool)
{
    clhy_hook_insert_filter(filter_insert, NULL, NULL, KUDA_HOOK_MIDDLE);
}

static void *filter_config(kuda_pool_t *pool, char *x)
{
    capi_filter_cfg *cfg = kuda_palloc(pool, sizeof(capi_filter_cfg));
    cfg->live_filters = kuda_hash_make(pool);
    cfg->chain = NULL;
    return cfg;
}

static void *filter_merge(kuda_pool_t *pool, void *BASE, void *ADD)
{
    capi_filter_cfg *base = BASE;
    capi_filter_cfg *add = ADD;
    capi_filter_chain *savelink = 0;
    capi_filter_chain *newlink;
    capi_filter_chain *p;
    capi_filter_cfg *conf = kuda_palloc(pool, sizeof(capi_filter_cfg));

    conf->live_filters = kuda_hash_overlay(pool, add->live_filters,
                                          base->live_filters);
    if (base->chain && add->chain) {
        for (p = base->chain; p; p = p->next) {
            newlink = kuda_pmemdup(pool, p, sizeof(capi_filter_chain));
            if (newlink->fname == NULL) {
                conf->chain = savelink = newlink;
            }
            else if (savelink) {
                savelink->next = newlink;
                savelink = newlink;
            }
            else {
                conf->chain = savelink = newlink;
            }
        }

        for (p = add->chain; p; p = p->next) {
            newlink = kuda_pmemdup(pool, p, sizeof(capi_filter_chain));
            /** Filter out merged chain resets */
            if (newlink->fname == NULL) {
                conf->chain = savelink = newlink;
            }
            else if (savelink) {
                savelink->next = newlink;
                savelink = newlink;
            }
            else {
                conf->chain = savelink = newlink;
            }
        }
    }
    else if (add->chain) {
        conf->chain = add->chain;
    }
    else {
        conf->chain = base->chain;
    }

    return conf;
}

static const command_rec filter_cmds[] = {
    CLHY_INIT_TAKE12("FilterDeclare", filter_declare, NULL, OR_OPTIONS,
        "filter-name [filter-type]"),
    CLHY_INIT_TAKE3("FilterProvider", filter_provider, NULL, OR_OPTIONS,
        "filter-name provider-name match-expression"),
    CLHY_INIT_ITERATE("FilterChain", filter_chain, NULL, OR_OPTIONS,
        "list of filter names with optional [+-=!@]"),
    CLHY_INIT_TAKE2("FilterTrace", filter_debug, NULL, RSRC_CONF | ACCESS_CONF,
        "filter-name debug-level"),
    CLHY_INIT_TAKE_ARGV("AddOutputFilterByType", filter_bytype, NULL, OR_FILEINFO,
        "output filter name followed by one or more content-types"),
#ifndef NO_PROTOCOL
    CLHY_INIT_TAKE23("FilterProtocol", filter_protocol, NULL, OR_OPTIONS,
        "filter-name [provider-name] protocol-args"),
#endif
    { NULL }
};

CLHY_DECLARE_CAPI(filter) = {
    STANDARD16_CAPI_STUFF,
    filter_config,
    filter_merge,
    NULL,
    NULL,
    filter_cmds,
    filter_hooks
};

