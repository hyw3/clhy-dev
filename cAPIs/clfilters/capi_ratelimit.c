/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "util_filter.h"

#include "capi_ratelimit.h"

#define RATE_LIMIT_FILTER_NAME "RATE_LIMIT"
#define RATE_INTERVAL_MS (200)

typedef enum rl_state_e
{
    RATE_LIMIT,
    RATE_FULLSPEED
} rl_state_e;

typedef struct rl_ctx_t
{
    int speed;
    int chunk_size;
    int burst;
    int do_sleep;
    rl_state_e state;
    kuda_bucket_brigade *tmpbb;
    kuda_bucket_brigade *holdingbb;
} rl_ctx_t;

#if defined(RLFDEBUG)
static void brigade_dump(request_rec *r, kuda_bucket_brigade *bb)
{
    kuda_bucket *e;
    int i = 0;

    for (e = KUDA_BRIGADE_FIRST(bb);
         e != KUDA_BRIGADE_SENTINEL(bb); e = KUDA_BUCKET_NEXT(e), i++) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(03193)
                      "brigade: [%d] %s", i, e->type->name);

    }
}
#endif /* RLFDEBUG */

static kuda_status_t
rate_limit_filter(clhy_filter_t *f, kuda_bucket_brigade *bb)
{
    kuda_status_t rv = KUDA_SUCCESS;
    rl_ctx_t *ctx = f->ctx;
    kuda_bucket_alloc_t *ba = f->r->connection->bucket_alloc;

    /* Set up our rl_ctx_t on first use */
    if (ctx == NULL) {
        const char *rl = NULL;
        int ratelimit;
        int burst = 0;

        /* no subrequests. */
        if (f->r->main != NULL) {
            clhy_remove_output_filter(f);
            return clhy_pass_brigade(f->next, bb);
        }

        /* Configuration: rate limit */
        rl = kuda_table_get(f->r->subprocess_env, "rate-limit");

        if (rl == NULL) {
            clhy_remove_output_filter(f);
            return clhy_pass_brigade(f->next, bb);
        }
        
        /* rl is in kilo bytes / second  */
        ratelimit = atoi(rl) * 1024;
        if (ratelimit <= 0) {
            /* remove ourselves */
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, f->r,
                          CLHYLOGNO(03488) "rl: disabling: rate-limit = %s (too high?)", rl);
            clhy_remove_output_filter(f);
            return clhy_pass_brigade(f->next, bb);
        }

        /* Configuration: optional initial burst */
        rl = kuda_table_get(f->r->subprocess_env, "rate-initial-burst");
        if (rl != NULL) {
            burst = atoi(rl) * 1024;
            if (burst <= 0) {
               clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, f->r,
                             CLHYLOGNO(03489) "rl: disabling burst: rate-initial-burst = %s (too high?)", rl);
               burst = 0;
            }
        }

        /* Set up our context */
        ctx = kuda_palloc(f->r->pool, sizeof(rl_ctx_t));
        f->ctx = ctx;
        ctx->state = RATE_LIMIT;
        ctx->speed = ratelimit;
        ctx->burst = burst;
        ctx->do_sleep = 0;

        /* calculate how many bytes / interval we want to send */
        /* speed is bytes / second, so, how many  (speed / 1000 % interval) */
        ctx->chunk_size = (ctx->speed / (1000 / RATE_INTERVAL_MS));
        ctx->tmpbb = kuda_brigade_create(f->r->pool, ba);
        ctx->holdingbb = kuda_brigade_create(f->r->pool, ba);
    }
    else {
        KUDA_BRIGADE_PREPEND(bb, ctx->holdingbb);
    }

    while (!KUDA_BRIGADE_EMPTY(bb)) {
        kuda_bucket *e;

        if (ctx->state == RATE_FULLSPEED) {
            /* Find where we 'stop' going full speed. */
            for (e = KUDA_BRIGADE_FIRST(bb);
                 e != KUDA_BRIGADE_SENTINEL(bb); e = KUDA_BUCKET_NEXT(e)) {
                if (CLHY_RL_BUCKET_IS_END(e)) {
                    kuda_brigade_split_ex(bb, e, ctx->holdingbb);
                    ctx->state = RATE_LIMIT;
                    break;
                }
            }

            e = kuda_bucket_flush_create(ba);
            KUDA_BRIGADE_INSERT_TAIL(bb, e);
            rv = clhy_pass_brigade(f->next, bb);
            kuda_brigade_cleanup(bb);

            if (rv != KUDA_SUCCESS) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, rv, f->r, CLHYLOGNO(01455)
                              "rl: full speed brigade pass failed.");
                return rv;
            }
        }
        else {
            for (e = KUDA_BRIGADE_FIRST(bb);
                 e != KUDA_BRIGADE_SENTINEL(bb); e = KUDA_BUCKET_NEXT(e)) {
                if (CLHY_RL_BUCKET_IS_START(e)) {
                    kuda_brigade_split_ex(bb, e, ctx->holdingbb);
                    ctx->state = RATE_FULLSPEED;
                    break;
                }
            }

            while (!KUDA_BRIGADE_EMPTY(bb)) {
                kuda_off_t len = ctx->chunk_size + ctx->burst;

                KUDA_BRIGADE_CONCAT(ctx->tmpbb, bb);

                /*
                 * Pull next chunk of data; the initial amount is our
                 * burst allotment (if any) plus a chunk.  All subsequent
                 * iterations are just chunks with whatever remaining
                 * burst amounts we have left (in case not done in the
                 * first bucket).
                 */
                rv = kuda_brigade_partition(ctx->tmpbb, len, &e);
                if (rv != KUDA_SUCCESS && rv != KUDA_INCOMPLETE) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, f->r, CLHYLOGNO(01456)
                                  "rl: partition failed.");
                    return rv;
                }
                /* Send next metadata now if any */
                while (e != KUDA_BRIGADE_SENTINEL(ctx->tmpbb)
                       && KUDA_BUCKET_IS_METADATA(e)) {
                    e = KUDA_BUCKET_NEXT(e);
                }
                if (e != KUDA_BRIGADE_SENTINEL(ctx->tmpbb)) {
                    kuda_brigade_split_ex(ctx->tmpbb, e, bb);
                }
                else {
                    kuda_brigade_length(ctx->tmpbb, 1, &len);
                }

                /*
                 * Adjust the burst amount depending on how much
                 * we've done up to now.
                 */
                if (ctx->burst) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, f->r,
                        CLHYLOGNO(03485) "rl: burst %d; len %"KUDA_OFF_T_FMT, ctx->burst, len);
                    if (len < ctx->burst) {
                        ctx->burst -= len;
                    }
                    else {
                        ctx->burst = 0;
                    }
                }

                e = KUDA_BRIGADE_LAST(ctx->tmpbb);
                if (KUDA_BUCKET_IS_EOS(e)) {
                    clhy_remove_output_filter(f);
                }
                else if (!KUDA_BUCKET_IS_FLUSH(e)) {
                    if (KUDA_BRIGADE_EMPTY(bb)) {
                        /* Wait for more (or next call) */
                        break;
                    }
                    e = kuda_bucket_flush_create(ba);
                    KUDA_BRIGADE_INSERT_TAIL(ctx->tmpbb, e);
                }

#if defined(RLFDEBUG)
                brigade_dump(f->r, ctx->tmpbb);
                brigade_dump(f->r, bb);
#endif /* RLFDEBUG */

                if (ctx->do_sleep) {
                    kuda_sleep(RATE_INTERVAL_MS * 1000);
                }
                else {
                    ctx->do_sleep = 1;
                }

                rv = clhy_pass_brigade(f->next, ctx->tmpbb);
                kuda_brigade_cleanup(ctx->tmpbb);

                if (rv != KUDA_SUCCESS) {
                    /* Most often, user disconnects from stream */
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, rv, f->r, CLHYLOGNO(01457)
                                  "rl: brigade pass failed.");
                    return rv;
                }
            }
        }

        if (!KUDA_BRIGADE_EMPTY(ctx->holdingbb)) {
            /* Any rate-limited data in tmpbb is sent unlimited along
             * with the rest.
             */
            KUDA_BRIGADE_CONCAT(bb, ctx->tmpbb);
            KUDA_BRIGADE_CONCAT(bb, ctx->holdingbb);
        }
    }

#if defined(RLFDEBUG)
    brigade_dump(f->r, ctx->tmpbb);
#endif /* RLFDEBUG */

    /* Save remaining tmpbb with the correct lifetime for the next call */
    return clhy_save_brigade(f, &ctx->holdingbb, &ctx->tmpbb, f->r->pool);
}

static kuda_status_t
rl_bucket_read(kuda_bucket *b, const char **str,
               kuda_size_t *len, kuda_read_type_e block)
{
    *str = NULL;
    *len = 0;
    return KUDA_SUCCESS;
}

CLHY_RL_DECLARE(kuda_bucket *)
    clhy_rl_end_create(kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    b->length = 0;
    b->start = 0;
    b->data = NULL;
    b->type = &clhy_rl_bucket_type_end;

    return b;
}

CLHY_RL_DECLARE(kuda_bucket *)
    clhy_rl_start_create(kuda_bucket_alloc_t *list)
{
    kuda_bucket *b = kuda_bucket_alloc(sizeof(*b), list);

    KUDA_BUCKET_INIT(b);
    b->free = kuda_bucket_free;
    b->list = list;
    b->length = 0;
    b->start = 0;
    b->data = NULL;
    b->type = &clhy_rl_bucket_type_start;

    return b;
}

CLHY_RL_DECLARE_DATA const kuda_bucket_type_t clhy_rl_bucket_type_end = {
    "RL_END", 5, KUDA_BUCKET_METADATA,
    kuda_bucket_destroy_noop,
    rl_bucket_read,
    kuda_bucket_setaside_noop,
    kuda_bucket_split_notimpl,
    kuda_bucket_simple_copy
};

CLHY_RL_DECLARE_DATA const kuda_bucket_type_t clhy_rl_bucket_type_start = {
    "RL_START", 5, KUDA_BUCKET_METADATA,
    kuda_bucket_destroy_noop,
    rl_bucket_read,
    kuda_bucket_setaside_noop,
    kuda_bucket_split_notimpl,
    kuda_bucket_simple_copy
};

static void register_hooks(kuda_pool_t *p)
{
    /* run after capi_deflate etc etc, but not at connection level, ie, capi_ssl. */
    clhy_register_output_filter(RATE_LIMIT_FILTER_NAME, rate_limit_filter,
                              NULL, CLHY_FTYPE_CONNECTION - 1);
}

CLHY_DECLARE_CAPI(ratelimit) = {
    STANDARD16_CAPI_STUFF,
    NULL,                       /* create per-directory config structure */
    NULL,                       /* merge per-directory config structures */
    NULL,                       /* create per-server config structure */
    NULL,                       /* merge per-server config structures */
    NULL,                       /* command kuda_table_t */
    register_hooks
};

