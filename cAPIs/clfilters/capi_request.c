/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * capi_request.c --- HTTP routines to set aside or process request bodies.
 */

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_buckets.h"
#include "kuda_lib.h"

#include "clhy_config.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_protocol.h"
#include "http_log.h"           /* For errors detected in basic auth common
                                 * support code... */
#include "http_request.h"

#include "capi_request.h"

/* Handles for core filters */
static clhy_filter_rec_t *keep_body_input_filter_handle;
static clhy_filter_rec_t *kept_body_input_filter_handle;

static kuda_status_t bail_out_on_error(kuda_bucket_brigade *bb,
                                      clhy_filter_t *f,
                                      int http_error)
{
    kuda_bucket *e;

    kuda_brigade_cleanup(bb);
    e = clhy_bucket_error_create(http_error,
                               NULL, f->r->pool,
                               f->c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bb, e);
    e = kuda_bucket_eos_create(f->c->bucket_alloc);
    KUDA_BRIGADE_INSERT_TAIL(bb, e);
    return clhy_pass_brigade(f->r->output_filters, bb);
}

typedef struct keep_body_filter_ctx {
    kuda_off_t remaining;
    kuda_off_t keep_body;
} keep_body_ctx_t;

/**
 * This is the KEEP_BODY_INPUT filter for HTTP requests, for times when the
 * body should be set aside for future use by other cAPIs.
 */
static kuda_status_t keep_body_filter(clhy_filter_t *f, kuda_bucket_brigade *b,
                                     clhy_input_mode_t mode,
                                     kuda_read_type_e block,
                                     kuda_off_t readbytes)
{
    kuda_bucket *e;
    keep_body_ctx_t *ctx = f->ctx;
    kuda_status_t rv;
    kuda_bucket *bucket;
    kuda_off_t len = 0;


    if (!ctx) {
        const char *lenp;
        char *endstr = NULL;
        request_dir_conf *dconf = clhy_get_capi_config(f->r->per_dir_config,
                                                       &request_capi);

        /* must we step out of the way? */
        if (!dconf->keep_body || f->r->kept_body) {
            clhy_remove_input_filter(f);
            return clhy_get_brigade(f->next, b, mode, block, readbytes);
        }

        f->ctx = ctx = kuda_pcalloc(f->r->pool, sizeof(*ctx));

        /* fail fast if the content length exceeds keep body */
        lenp = kuda_table_get(f->r->headers_in, "Content-Length");
        if (lenp) {

            /* Protects against over/underflow, non-digit chars in the
             * string (excluding leading space) (the endstr checks)
             * and a negative number. */
            if (kuda_strtoff(&ctx->remaining, lenp, &endstr, 10)
                || endstr == lenp || *endstr || ctx->remaining < 0) {

                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, f->r, CLHYLOGNO(01411)
                              "Invalid Content-Length");

                clhy_remove_input_filter(f);
                return bail_out_on_error(b, f, HTTP_REQUEST_ENTITY_TOO_LARGE);
            }

            /* If we have a limit in effect and we know the C-L ahead of
             * time, stop it here if it is invalid.
             */
            if (dconf->keep_body < ctx->remaining) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, f->r, CLHYLOGNO(01412)
                          "Requested content-length of %" KUDA_OFF_T_FMT
                          " is larger than the configured limit"
                          " of %" KUDA_OFF_T_FMT, ctx->remaining, dconf->keep_body);
                clhy_remove_input_filter(f);
                return bail_out_on_error(b, f, HTTP_REQUEST_ENTITY_TOO_LARGE);
            }

        }

        f->r->kept_body = kuda_brigade_create(f->r->pool, f->r->connection->bucket_alloc);
        ctx->remaining = dconf->keep_body;

    }

    /* get the brigade from upstream, and read it in to get its length */
    rv = clhy_get_brigade(f->next, b, mode, block, readbytes);
    if (rv == KUDA_SUCCESS) {
        rv = kuda_brigade_length(b, 1, &len);
    }

    /* does the length take us over the limit? */
    if (KUDA_SUCCESS == rv && len > ctx->remaining) {
        if (f->r->kept_body) {
            kuda_brigade_cleanup(f->r->kept_body);
            f->r->kept_body = NULL;
        }
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, f->r, CLHYLOGNO(01413)
                      "Requested content-length of %" KUDA_OFF_T_FMT
                      " is larger than the configured limit"
                      " of %" KUDA_OFF_T_FMT, len, ctx->keep_body);
        return bail_out_on_error(b, f, HTTP_REQUEST_ENTITY_TOO_LARGE);
    }
    ctx->remaining -= len;

    /* pass any errors downstream */
    if (rv != KUDA_SUCCESS) {
        if (f->r->kept_body) {
            kuda_brigade_cleanup(f->r->kept_body);
            f->r->kept_body = NULL;
        }
        return rv;
    }

    /* all is well, set aside the buckets */
    for (bucket = KUDA_BRIGADE_FIRST(b);
         bucket != KUDA_BRIGADE_SENTINEL(b);
         bucket = KUDA_BUCKET_NEXT(bucket))
    {
        kuda_bucket_copy(bucket, &e);
        KUDA_BRIGADE_INSERT_TAIL(f->r->kept_body, e);
    }

    return KUDA_SUCCESS;
}


typedef struct kept_body_filter_ctx {
    kuda_off_t offset;
    kuda_off_t remaining;
} kept_body_ctx_t;

/**
 * Initialisation of filter to handle a kept body on subrequests.
 *
 * If a body is to be reinserted into a subrequest, any chunking will have
 * been removed from the body during storage. We need to change the request
 * from Transfer-Encoding: chunked to an explicit Content-Length.
 */
static int kept_body_filter_init(clhy_filter_t *f)
{
    kuda_off_t length = 0;
    request_rec *r = f->r;
    kuda_bucket_brigade *kept_body = r->kept_body;

    if (kept_body) {
        kuda_table_unset(r->headers_in, "Transfer-Encoding");
        kuda_brigade_length(kept_body, 1, &length);
        kuda_table_setn(r->headers_in, "Content-Length", kuda_off_t_toa(r->pool, length));
    }

    return OK;
}

/**
 * Filter to handle a kept body on subrequests.
 *
 * If a body has been previously kept by the request, and if a subrequest wants
 * to re-insert the body into the request, this input filter makes it happen.
 */
static kuda_status_t kept_body_filter(clhy_filter_t *f, kuda_bucket_brigade *b,
                                     clhy_input_mode_t mode,
                                     kuda_read_type_e block,
                                     kuda_off_t readbytes)
{
    request_rec *r = f->r;
    kuda_bucket_brigade *kept_body = r->kept_body;
    kept_body_ctx_t *ctx = f->ctx;
    kuda_bucket *ec, *e2;
    kuda_status_t rv;

    /* just get out of the way of things we don't want. */
    if (!kept_body || (mode != CLHY_MODE_READBYTES && mode != CLHY_MODE_GETLINE)) {
        return clhy_get_brigade(f->next, b, mode, block, readbytes);
    }

    /* set up the context if it does not already exist */
    if (!ctx) {
        f->ctx = ctx = kuda_palloc(f->r->pool, sizeof(*ctx));
        ctx->offset = 0;
        kuda_brigade_length(kept_body, 1, &ctx->remaining);
    }

    /* kept_body is finished, send next filter */
    if (ctx->remaining <= 0) {
        return clhy_get_brigade(f->next, b, mode, block, readbytes);
    }

    /* send all of the kept_body, but no more */
    if (readbytes > ctx->remaining) {
        readbytes = ctx->remaining;
    }

    /* send part of the kept_body */
    if ((rv = kuda_brigade_partition(kept_body, ctx->offset, &ec)) != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01414)
                      "kuda_brigade_partition() failed on kept_body at %" KUDA_OFF_T_FMT, ctx->offset);
        return rv;
    }
    if ((rv = kuda_brigade_partition(kept_body, ctx->offset + readbytes, &e2)) != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01415)
                      "kuda_brigade_partition() failed on kept_body at %" KUDA_OFF_T_FMT, ctx->offset + readbytes);
        return rv;
    }

    do {
        kuda_bucket *foo;
        const char *str;
        kuda_size_t len;

        if (kuda_bucket_copy(ec, &foo) != KUDA_SUCCESS) {
            /* As above; this should not fail since the bucket has
             * a known length, but just to be sure, this takes
             * care of uncopyable buckets that do somehow manage
             * to slip through.  */
            /* XXX: check for failure? */
            kuda_bucket_read(ec, &str, &len, KUDA_BLOCK_READ);
            kuda_bucket_copy(ec, &foo);
        }
        KUDA_BRIGADE_INSERT_TAIL(b, foo);
        ec = KUDA_BUCKET_NEXT(ec);
    } while (ec != e2);

    ctx->remaining -= readbytes;
    ctx->offset += readbytes;
    return KUDA_SUCCESS;

}

/**
 * Check whether this filter is not already present.
 */
static int request_is_filter_present(request_rec * r, clhy_filter_rec_t *fn)
{
    clhy_filter_t * f = r->input_filters;
    while (f) {
        if (f->frec == fn) {
            return 1;
        }
        f = f->next;
    }
    return 0;
}

/**
 * Insert filter hook.
 *
 * Add the KEEP_BODY filter to the request, if the admin wants to keep
 * the body using the KeptBodySize directive.
 *
 * As a precaution, any pre-existing instances of either the kept_body or
 * keep_body filters will be removed before the filter is added.
 *
 * @param r The request
 */
static void clhy_request_insert_filter(request_rec * r)
{
    request_dir_conf *conf = clhy_get_capi_config(r->per_dir_config,
                                                  &request_capi);

    if (r->kept_body) {
        if (!request_is_filter_present(r, kept_body_input_filter_handle)) {
            clhy_add_input_filter_handle(kept_body_input_filter_handle,
                                       NULL, r, r->connection);
        }
    }
    else if (conf->keep_body) {
        if (!request_is_filter_present(r, kept_body_input_filter_handle)) {
            clhy_add_input_filter_handle(keep_body_input_filter_handle,
                                       NULL, r, r->connection);
        }
    }

}

/**
 * Remove the kept_body and keep body filters from this specific request.
 */
static void clhy_request_remove_filter(request_rec * r)
{
    clhy_filter_t * f = r->input_filters;
    while (f) {
        if (f->frec->filter_func.in_func == kept_body_filter ||
                f->frec->filter_func.in_func == keep_body_filter) {
            clhy_remove_input_filter(f);
        }
        f = f->next;
    }
}

static void *create_request_dir_config(kuda_pool_t *p, char *dummy)
{
    request_dir_conf *new =
        (request_dir_conf *) kuda_pcalloc(p, sizeof(request_dir_conf));

    new->keep_body_set = 0; /* unset */
    new->keep_body = 0; /* don't by default */

    return (void *) new;
}

static void *merge_request_dir_config(kuda_pool_t *p, void *basev, void *addv)
{
    request_dir_conf *new = (request_dir_conf *) kuda_pcalloc(p, sizeof(request_dir_conf));
    request_dir_conf *add = (request_dir_conf *) addv;
    request_dir_conf *base = (request_dir_conf *) basev;

    new->keep_body = (add->keep_body_set == 0) ? base->keep_body : add->keep_body;
    new->keep_body_set = add->keep_body_set || base->keep_body_set;

    return new;
}

static const char *set_kept_body_size(cmd_parms *cmd, void *dconf,
                                      const char *arg)
{
    request_dir_conf *conf = dconf;
    char *end = NULL;

    if (KUDA_SUCCESS != kuda_strtoff(&(conf->keep_body), arg, &end, 10)
            || conf->keep_body < 0 || *end) {
        return "KeptBodySize must be a valid size in bytes, or zero.";
    }
    conf->keep_body_set = 1;

    return NULL;
}

static const command_rec request_cmds[] = {
    CLHY_INIT_TAKE1("KeptBodySize", set_kept_body_size, NULL, ACCESS_CONF,
                  "Maximum size of request bodies kept aside for use by filters"),
    { NULL }
};

static void register_hooks(kuda_pool_t *p)
{
    keep_body_input_filter_handle =
        clhy_register_input_filter(KEEP_BODY_FILTER, keep_body_filter,
                                 NULL, CLHY_FTYPE_RESOURCE);
    kept_body_input_filter_handle =
        clhy_register_input_filter(KEPT_BODY_FILTER, kept_body_filter,
                                 kept_body_filter_init, CLHY_FTYPE_RESOURCE);
    clhy_hook_insert_filter(clhy_request_insert_filter, NULL, NULL, KUDA_HOOK_LAST);
    KUDA_REGISTER_OPTIONAL_FN(clhy_request_insert_filter);
    KUDA_REGISTER_OPTIONAL_FN(clhy_request_remove_filter);
}

CLHY_DECLARE_CAPI(request) = {
    STANDARD16_CAPI_STUFF,
    create_request_dir_config, /* create per-directory config structure */
    merge_request_dir_config,  /* merge per-directory config structures */
    NULL,                      /* create per-server config structure */
    NULL,                      /* merge per-server config structures */
    request_cmds,              /* command kuda_table_t */
    register_hooks             /* register hooks */
};
