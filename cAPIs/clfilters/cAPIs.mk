capi_buffer.la: capi_buffer.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_buffer.lo $(CAPI_BUFFER_LDADD)
capi_data.la: capi_data.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_data.lo $(CAPI_DATA_LDADD)
capi_ratelimit.la: capi_ratelimit.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_ratelimit.lo $(CAPI_RATELIMIT_LDADD)
capi_reqtimeout.la: capi_reqtimeout.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_reqtimeout.lo $(CAPI_REQTIMEOUT_LDADD)
capi_ext_filter.la: capi_ext_filter.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_ext_filter.lo $(CAPI_EXT_FILTER_LDADD)
capi_request.la: capi_request.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_request.lo $(CAPI_REQUEST_LDADD)
capi_include.la: capi_include.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_include.lo $(CAPI_INCLUDE_LDADD)
capi_filter.la: capi_filter.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_filter.lo $(CAPI_FILTER_LDADD)
capi_reflector.la: capi_reflector.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_reflector.lo $(CAPI_REFLECTOR_LDADD)
capi_substitute.la: capi_substitute.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_substitute.lo $(CAPI_SUBSTITUTE_LDADD)
capi_sed.la: capi_sed.slo sed0.slo sed1.slo regexp.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_sed.lo sed0.lo sed1.lo regexp.lo $(CAPI_SED_LDADD)
capi_deflate.la: capi_deflate.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_deflate.lo $(CAPI_DEFLATE_LDADD)
capi_xml2enc.la: capi_xml2enc.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_xml2enc.lo $(CAPI_XML2ENC_LDADD)
capi_proxy_html.la: capi_proxy_html.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_proxy_html.lo $(CAPI_PROXY_HTML_LDADD)
capi_brotli.la: capi_brotli.slo
	$(SH_LINK) -rpath $(libexecdir) -cAPI -avoid-version  capi_brotli.lo $(CAPI_BROTLI_LDADD)
DISTCLEAN_TARGETS = cAPIs.mk
static = 
shared =  capi_buffer.la capi_data.la capi_ratelimit.la capi_reqtimeout.la capi_ext_filter.la capi_request.la capi_include.la capi_filter.la capi_reflector.la capi_substitute.la capi_sed.la capi_deflate.la capi_xml2enc.la capi_proxy_html.la capi_brotli.la
CAPI_CFLAGS = -I/usr/include/libxml2
