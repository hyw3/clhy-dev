/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * capi_data.c --- Turn the response into an rfc2397 data URL, suitable for
 *                displaying as inline content on a page.
 */

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_buckets.h"
#include "kuda_base64.h"
#include "kuda_lib.h"

#include "clhy_config.h"
#include "util_filter.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "http_request.h"
#include "http_protocol.h"

#define DATA_FILTER "DATA"

cAPI CLHY_CAPI_DECLARE_DATA data_capi;

typedef struct data_ctx
{
    unsigned char overflow[3];
    int count;
    kuda_bucket_brigade *bb;
} data_ctx;

/**
 * Create a data URL as follows:
 *
 * data:[<mime-type>;][charset=<charset>;]base64,<payload>
 *
 * Where:
 *
 * mime-type: The mime type of the original response.
 * charset: The optional character set corresponding to the mime type.
 * payload: A base64 version of the response body.
 *
 * The content type of the response is set to text/plain.
 *
 * The Content-Length header, if present, is updated with the new content
 * length based on the increase in size expected from the base64 conversion.
 * If the Content-Length header is too large to fit into an int, we remove
 * the Content-Length header instead.
 */
static kuda_status_t data_out_filter(clhy_filter_t *f, kuda_bucket_brigade *bb)
{
    kuda_bucket *e, *ee;
    request_rec *r = f->r;
    data_ctx *ctx = f->ctx;
    kuda_status_t rv = KUDA_SUCCESS;

    /* first time in? create a context */
    if (!ctx) {
        char *type;
        char *charset = NULL;
        char *end;
        const char *content_length;

        /* base64-ing won't work on subrequests, it would be nice if
         * it did. Within subrequests, we have no EOS to check for,
         * so we don't know when to flush the tail to the network.
         */
        if (!clhy_is_initial_req(f->r)) {
            clhy_remove_output_filter(f);
            return clhy_pass_brigade(f->next, bb);
        }

        ctx = f->ctx = kuda_pcalloc(r->pool, sizeof(*ctx));
        ctx->bb = kuda_brigade_create(r->pool, f->c->bucket_alloc);

        type = kuda_pstrdup(r->pool, r->content_type);
        if (type) {
            charset = strchr(type, ' ');
            if (charset) {
                *charset++ = 0;
                end = strchr(charset, ' ');
                if (end) {
                    *end++ = 0;
                }
            }
        }

        kuda_brigade_printf(ctx->bb, NULL, NULL, "data:%s%s;base64,",
                type ? type : "", charset ? charset : "");

        content_length = kuda_table_get(r->headers_out, "Content-Length");
        if (content_length) {
            kuda_off_t len, clen;
            kuda_brigade_length(ctx->bb, 1, &len);
            clen = kuda_atoi64(content_length);
            if (clen >= 0 && clen < KUDA_INT32_MAX) {
                clhy_set_content_length(r, len +
                                      kuda_base64_encode_len((int)clen) - 1);
            }
            else {
                kuda_table_unset(r->headers_out, "Content-Length");
            }
        }

        clhy_set_content_type(r, "text/plain");

    }

    /* Do nothing if asked to filter nothing. */
    if (KUDA_BRIGADE_EMPTY(bb)) {
        return clhy_pass_brigade(f->next, bb);
    }

    while (KUDA_SUCCESS == rv && !KUDA_BRIGADE_EMPTY(bb)) {
        const char *data;
        kuda_size_t size;
        kuda_size_t tail;
        kuda_size_t len;
        /* buffer big enough for 8000 encoded bytes (6000 raw bytes) and terminator */
        char buffer[KUDA_BUCKET_BUFF_SIZE + 1];
        char encoded[((sizeof(ctx->overflow)) / 3) * 4 + 1];

        e = KUDA_BRIGADE_FIRST(bb);

        /* EOS means we are done. */
        if (KUDA_BUCKET_IS_EOS(e)) {

            /* write away the tail */
            if (ctx->count) {
                len = kuda_base64_encode_binary(encoded, ctx->overflow,
                        ctx->count);
                kuda_brigade_write(ctx->bb, NULL, NULL, encoded, len - 1);
                ctx->count = 0;
            }

            /* pass the EOS across */
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(ctx->bb, e);

            /* pass what we have down the chain */
            clhy_remove_output_filter(f);
            rv = clhy_pass_brigade(f->next, ctx->bb);

            /* pass any stray buckets after the EOS down the stack */
            if ((KUDA_SUCCESS == rv) && (!KUDA_BRIGADE_EMPTY(bb))) {
               rv = clhy_pass_brigade(f->next, bb);
            }
            continue;
        }

        /* flush what we can, we can't flush the tail until EOS */
        if (KUDA_BUCKET_IS_FLUSH(e)) {

            /* pass the flush bucket across */
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(ctx->bb, e);

            /* pass what we have down the chain */
            rv = clhy_pass_brigade(f->next, ctx->bb);
            continue;
        }

        /* metadata buckets are preserved as is */
        if (KUDA_BUCKET_IS_METADATA(e)) {
            /*
             * Remove meta data bucket from old brigade and insert into the
             * new.
             */
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(ctx->bb, e);
            continue;
        }

        /* make sure we don't read more than 6000 bytes at a time */
        kuda_brigade_partition(bb, (KUDA_BUCKET_BUFF_SIZE / 4 * 3), &ee);

        /* size will never be more than 6000 bytes */
        if (KUDA_SUCCESS == (rv = kuda_bucket_read(e, &data, &size,
                KUDA_BLOCK_READ))) {

            /* fill up and write out our overflow buffer if partially used */
            while (size && ctx->count && ctx->count < sizeof(ctx->overflow)) {
                ctx->overflow[ctx->count++] = *data++;
                size--;
            }
            if (ctx->count == sizeof(ctx->overflow)) {
                len = kuda_base64_encode_binary(encoded, ctx->overflow,
                        sizeof(ctx->overflow));
                kuda_brigade_write(ctx->bb, NULL, NULL, encoded, len - 1);
                ctx->count = 0;
            }

            /* write the main base64 chunk */
            tail = size % sizeof(ctx->overflow);
            size -= tail;
            if (size) {
                len = kuda_base64_encode_binary(buffer,
                        (const unsigned char *) data, size);
                kuda_brigade_write(ctx->bb, NULL, NULL, buffer, len - 1);
            }

            /* save away any tail in the overflow buffer */
            if (tail) {
                memcpy(ctx->overflow, data + size, tail);
                ctx->count += tail;
            }

            kuda_bucket_delete(e);

            /* pass what we have down the chain */
            rv = clhy_pass_brigade(f->next, ctx->bb);
            if (rv) {
                /* should break out of the loop, since our write to the client
                 * failed in some way. */
                continue;
            }

        }

    }

    return rv;

}

static const command_rec data_cmds[] = { { NULL } };

static void register_hooks(kuda_pool_t *p)
{
    clhy_register_output_filter(DATA_FILTER, data_out_filter, NULL,
            CLHY_FTYPE_RESOURCE);
}
CLHY_DECLARE_CAPI(data) = { STANDARD16_CAPI_STUFF,
    NULL,  /* create per-directory config structure */
    NULL, /* merge per-directory config structures */
    NULL, /* create per-server config structure */
    NULL, /* merge per-server config structures */
    data_cmds, /* command kuda_table_t */
    register_hooks /* register hooks */
};
