/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * capi_buffer.c --- Buffer the input and output filter stacks, collapse
 *                  many small buckets into fewer large buckets.
 */

#include "kuda.h"
#include "kuda_strings.h"
#include "kuda_buckets.h"
#include "kuda_lib.h"

#include "clhy_config.h"
#include "util_filter.h"
#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "http_request.h"

static const char bufferFilterName[] = "BUFFER";
cAPI CLHY_CAPI_DECLARE_DATA buffer_capi;

#define DEFAULT_BUFFER_SIZE 128*1024

typedef struct buffer_conf {
    kuda_off_t size; /* size of the buffer */
    int size_set; /* has the size been set */
} buffer_conf;

typedef struct buffer_ctx {
    kuda_bucket_brigade *bb;
    kuda_bucket_brigade *tmp;
    buffer_conf *conf;
    kuda_off_t remaining;
    int seen_eos;
} buffer_ctx;

/**
 * Buffer buckets being written to the output filter stack.
 */
static kuda_status_t buffer_out_filter(clhy_filter_t *f, kuda_bucket_brigade *bb)
{
    kuda_bucket *e;
    request_rec *r = f->r;
    buffer_ctx *ctx = f->ctx;
    kuda_status_t rv = KUDA_SUCCESS;
    int move = 0;

    /* first time in? create a context */
    if (!ctx) {

        /* buffering won't work on subrequests, it would be nice if
         * it did. Within subrequests, we have no EOS to check for,
         * so we don't know when to flush the buffer to the network
         */
        if (f->r->main) {
            clhy_remove_output_filter(f);
            return clhy_pass_brigade(f->next, bb);
        }

        ctx = f->ctx = kuda_pcalloc(r->pool, sizeof(*ctx));
        ctx->bb = kuda_brigade_create(r->pool, f->c->bucket_alloc);
        ctx->conf = clhy_get_capi_config(f->r->per_dir_config, &buffer_capi);
    }

    /* Do nothing if asked to filter nothing. */
    if (KUDA_BRIGADE_EMPTY(bb)) {
        return clhy_pass_brigade(f->next, bb);
    }

    /* Empty buffer means we can potentially optimise below */
    if (KUDA_BRIGADE_EMPTY(ctx->bb)) {
        move = 1;
    }

    while (KUDA_SUCCESS == rv && !KUDA_BRIGADE_EMPTY(bb)) {
        const char *data;
        kuda_off_t len;
        kuda_size_t size;

        e = KUDA_BRIGADE_FIRST(bb);

        /* EOS means we are done. */
        if (KUDA_BUCKET_IS_EOS(e)) {

            /* should we add an etag? */

            /* pass the EOS across */
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(ctx->bb, e);

            /* pass what we have down the chain */
            rv = clhy_pass_brigade(f->next, ctx->bb);
            continue;
        }

        /* A flush takes precedence over buffering */
        if (KUDA_BUCKET_IS_FLUSH(e)) {

            /* pass the flush bucket across */
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(ctx->bb, e);

            /* pass what we have down the chain */
            rv = clhy_pass_brigade(f->next, ctx->bb);
            continue;
        }

        /* metadata buckets are preserved as is */
        if (KUDA_BUCKET_IS_METADATA(e)) {
            /*
             * Remove meta data bucket from old brigade and insert into the
             * new.
             */
            KUDA_BUCKET_REMOVE(e);
            KUDA_BRIGADE_INSERT_TAIL(ctx->bb, e);
            continue;
        }

        /* is our buffer full?
         * If so, send what we have down the filter chain. If the buffer
         * gets full, we can no longer compute a content length.
         */
        kuda_brigade_length(ctx->bb, 1, &len);
        if (len > ctx->conf->size) {

            /* pass what we have down the chain */
            rv = clhy_pass_brigade(f->next, ctx->bb);
            if (rv) {
                /* should break out of the loop, since our write to the client
                 * failed in some way. */
                continue;
            }
        }

        /* at this point we are ready to buffer.
         * Buffering takes advantage of an optimisation in the handling of
         * bucket brigades. Heap buckets are always created at a fixed
         * size, regardless of the size of the data placed into them.
         * The kuda_brigade_write() call will first try and pack the data
         * into any free space in the most recent heap bucket, before
         * allocating a new bucket if necessary.
         */
        if (KUDA_SUCCESS == (rv = kuda_bucket_read(e, &data, &size,
                KUDA_BLOCK_READ))) {

            /* further optimisation: if the buckets are already heap
             * buckets, and the buckets stay exactly KUDA_BUCKET_BUFF_SIZE
             * long (as they would be if we were reading bits of a
             * large bucket), then move the buckets instead of copying
             * them.
             */
            if (move && KUDA_BUCKET_IS_HEAP(e)) {
                KUDA_BUCKET_REMOVE(e);
                KUDA_BRIGADE_INSERT_TAIL(ctx->bb, e);
                if (KUDA_BUCKET_BUFF_SIZE != size) {
                    move = 0;
                }
            } else {
                kuda_brigade_write(ctx->bb, NULL, NULL, data, size);
                kuda_bucket_delete(e);
            }

        }

    }

    return rv;

}

/**
 * Buffer buckets being read from the input filter stack.
 */
static kuda_status_t buffer_in_filter(clhy_filter_t *f, kuda_bucket_brigade *bb,
        clhy_input_mode_t mode, kuda_read_type_e block, kuda_off_t readbytes)
{
    kuda_bucket *e, *after;
    kuda_status_t rv;
    buffer_ctx *ctx = f->ctx;

    /* buffer on main requests only */
    if (!clhy_is_initial_req(f->r)) {
        clhy_remove_input_filter(f);
        return clhy_get_brigade(f->next, bb, mode, block, readbytes);
    }

    /* first time in? create a context */
    if (!ctx) {
        ctx = f->ctx = kuda_pcalloc(f->r->pool, sizeof(*ctx));
        ctx->bb = kuda_brigade_create(f->r->pool, f->c->bucket_alloc);
        ctx->tmp = kuda_brigade_create(f->r->pool, f->c->bucket_alloc);
        ctx->conf = clhy_get_capi_config(f->r->per_dir_config, &buffer_capi);
    }

    /* just get out of the way of things we don't want. */
    if (mode != CLHY_MODE_READBYTES) {
        return clhy_get_brigade(f->next, bb, mode, block, readbytes);
    }

    /* if our buffer is empty, read off the network until the buffer is full */
    if (KUDA_BRIGADE_EMPTY(ctx->bb)) {
        int seen_flush = 0;

        ctx->remaining = ctx->conf->size;

        while (!ctx->seen_eos && !seen_flush && ctx->remaining > 0) {
            const char *data;
            kuda_size_t size = 0;

            if (KUDA_BRIGADE_EMPTY(ctx->tmp)) {
                rv = clhy_get_brigade(f->next, ctx->tmp, mode, block,
                                    ctx->remaining);

                /* if an error was received, bail out now. If the error is
                 * EAGAIN and we have not yet seen an EOS, we will definitely
                 * be called again, at which point we will send our buffered
                 * data. Instead of sending EAGAIN, some filters return an
                 * empty brigade instead when data is not yet available. In
                 * this case, pass through the KUDA_SUCCESS and emulate the
                 * underlying filter.
                 */
                if (rv != KUDA_SUCCESS || KUDA_BRIGADE_EMPTY(ctx->tmp)) {
                    return rv;
                }
            }

            do {
                e = KUDA_BRIGADE_FIRST(ctx->tmp);

                /* if we see an EOS, we are done */
                if (KUDA_BUCKET_IS_EOS(e)) {
                    KUDA_BUCKET_REMOVE(e);
                    KUDA_BRIGADE_INSERT_TAIL(ctx->bb, e);
                    ctx->seen_eos = 1;
                    break;
                }

                /* flush buckets clear the buffer */
                if (KUDA_BUCKET_IS_FLUSH(e)) {
                    KUDA_BUCKET_REMOVE(e);
                    KUDA_BRIGADE_INSERT_TAIL(ctx->bb, e);
                    seen_flush = 1;
                    break;
                }

                /* pass metadata buckets through */
                if (KUDA_BUCKET_IS_METADATA(e)) {
                    KUDA_BUCKET_REMOVE(e);
                    KUDA_BRIGADE_INSERT_TAIL(ctx->bb, e);
                    continue;
                }

                /* read the bucket in, pack it into the buffer */
                if (KUDA_SUCCESS == (rv = kuda_bucket_read(e, &data, &size,
                                                         KUDA_BLOCK_READ))) {
                    kuda_brigade_write(ctx->bb, NULL, NULL, data, size);
                    ctx->remaining -= size;
                    kuda_bucket_delete(e);
                } else {
                    return rv;
                }

            } while (!KUDA_BRIGADE_EMPTY(ctx->tmp));
        }
    }

    /* give the caller the data they asked for from the buffer */
    kuda_brigade_partition(ctx->bb, readbytes, &after);
    e = KUDA_BRIGADE_FIRST(ctx->bb);
    while (e != after) {
        if (KUDA_BUCKET_IS_EOS(e)) {
            /* last bucket read, step out of the way */
            clhy_remove_input_filter(f);
        }
        KUDA_BUCKET_REMOVE(e);
        KUDA_BRIGADE_INSERT_TAIL(bb, e);
        e = KUDA_BRIGADE_FIRST(ctx->bb);
    }

    return KUDA_SUCCESS;
}

static void *create_buffer_config(kuda_pool_t *p, char *dummy)
{
    buffer_conf *new = (buffer_conf *) kuda_pcalloc(p, sizeof(buffer_conf));

    new->size_set = 0; /* unset */
    new->size = DEFAULT_BUFFER_SIZE; /* default size */

    return (void *) new;
}

static void *merge_buffer_config(kuda_pool_t *p, void *basev, void *addv)
{
    buffer_conf *new = (buffer_conf *) kuda_pcalloc(p, sizeof(buffer_conf));
    buffer_conf *add = (buffer_conf *) addv;
    buffer_conf *base = (buffer_conf *) basev;

    new->size = (add->size_set == 0) ? base->size : add->size;
    new->size_set = add->size_set || base->size_set;

    return new;
}

static const char *set_buffer_size(cmd_parms *cmd, void *dconf, const char *arg)
{
    buffer_conf *conf = dconf;

    if (KUDA_SUCCESS != kuda_strtoff(&(conf->size), arg, NULL, 10) || conf->size
            <= 0) {
        return "BufferSize must be a size in bytes, and greater than zero";
    }
    conf->size_set = 1;

    return NULL;
}

static const command_rec buffer_cmds[] = { CLHY_INIT_TAKE1("BufferSize",
        set_buffer_size, NULL, ACCESS_CONF,
        "Maximum size of the buffer used by the buffer filter"), { NULL } };

static void register_hooks(kuda_pool_t *p)
{
    clhy_register_output_filter(bufferFilterName, buffer_out_filter, NULL,
            CLHY_FTYPE_CONTENT_SET);
    clhy_register_input_filter(bufferFilterName, buffer_in_filter, NULL,
            CLHY_FTYPE_CONTENT_SET);
}

CLHY_DECLARE_CAPI(buffer) = {
    STANDARD16_CAPI_STUFF,
    create_buffer_config, /* create per-directory config structure */
    merge_buffer_config, /* merge per-directory config structures */
    NULL, /* create per-server config structure */
    NULL, /* merge per-server config structures */
    buffer_cmds, /* command kuda_table_t */
    register_hooks /* register hooks */
};
