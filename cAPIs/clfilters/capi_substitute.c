/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * capi_substitute.c: Perform content rewriting on the fly
 */

#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "kuda_general.h"
#include "kuda_strings.h"
#include "kuda_strmatch.h"
#include "kuda_lib.h"
#include "util_filter.h"
#include "util_varbuf.h"
#include "kuda_buckets.h"
#include "http_request.h"
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

/*
 * We want to limit the memory usage in a way that is predictable.
 * Therefore we limit the resulting length of the line.
 * This is the default value.
 */
#define CLHY_SUBST_MAX_LINE_LENGTH (1024*1024)

static const char substitute_filter_name[] = "SUBSTITUTE";

cAPI CLHY_CAPI_DECLARE_DATA substitute_capi;

typedef struct subst_pattern_t {
    const kuda_strmatch_pattern *pattern;
    const clhy_regex_t *regexp;
    const char *replacement;
    kuda_size_t replen;
    kuda_size_t patlen;
    int flatten;
    const char *from;
} subst_pattern_t;

typedef struct {
    kuda_array_header_t *patterns;
    kuda_size_t max_line_length;
    int max_line_length_set;
    int inherit_before;
} subst_dir_conf;

typedef struct {
    kuda_bucket_brigade *linebb;
    kuda_bucket_brigade *linesbb;
    kuda_bucket_brigade *passbb;
    kuda_bucket_brigade *pattbb;
    kuda_pool_t *tpool;
} substitute_capi_ctx;

static void *create_substitute_dcfg(kuda_pool_t *p, char *d)
{
    subst_dir_conf *dcfg =
        (subst_dir_conf *) kuda_palloc(p, sizeof(subst_dir_conf));

    dcfg->patterns = kuda_array_make(p, 10, sizeof(subst_pattern_t));
    dcfg->max_line_length = CLHY_SUBST_MAX_LINE_LENGTH;
    dcfg->max_line_length_set = 0;
    dcfg->inherit_before = -1;
    return dcfg;
}

static void *merge_substitute_dcfg(kuda_pool_t *p, void *basev, void *overv)
{
    subst_dir_conf *a =
        (subst_dir_conf *) kuda_palloc(p, sizeof(subst_dir_conf));
    subst_dir_conf *base = (subst_dir_conf *) basev;
    subst_dir_conf *over = (subst_dir_conf *) overv;

    a->inherit_before = (over->inherit_before != -1)
                            ? over->inherit_before
                            : base->inherit_before;
    if (a->inherit_before == 1) {
        a->patterns = kuda_array_append(p, base->patterns,
                                          over->patterns);
    }
    else {
        a->patterns = kuda_array_append(p, over->patterns,
                                          base->patterns);
    }
    a->max_line_length = over->max_line_length_set ?
                             over->max_line_length : base->max_line_length;
    a->max_line_length_set = over->max_line_length_set
                           | base->max_line_length_set;
    return a;
}

#define CLHY_MAX_BUCKETS 1000

#define SEDRMPATBCKT(b, offset, tmp_b, patlen) do {   \
    kuda_bucket_split(b, offset);                     \
    tmp_b = KUDA_BUCKET_NEXT(b);                      \
    kuda_bucket_split(tmp_b, patlen);                 \
    b = KUDA_BUCKET_NEXT(tmp_b);                      \
    kuda_bucket_delete(tmp_b);                        \
} while (0)

#define CAP2LINEMAX(n) ((n) < (kuda_size_t)200 ? (int)(n) : 200)

static kuda_status_t do_pattmatch(clhy_filter_t *f, kuda_bucket *inb,
                                 kuda_bucket_brigade *mybb,
                                 kuda_pool_t *pool)
{
    int i;
    int force_quick = 0;
    clhy_regmatch_t regm[CLHY_MAX_REG_MATCH];
    kuda_size_t bytes;
    kuda_size_t len;
    const char *buff;
    struct clhy_varbuf vb;
    kuda_bucket *b;
    kuda_bucket *tmp_b;

    subst_dir_conf *cfg =
    (subst_dir_conf *) clhy_get_capi_config(f->r->per_dir_config,
                                             &substitute_capi);
    subst_pattern_t *script;

    KUDA_BRIGADE_INSERT_TAIL(mybb, inb);
    clhy_varbuf_init(pool, &vb, 0);

    script = (subst_pattern_t *) cfg->patterns->elts;
    /*
     * Simple optimization. If we only have one pattern, then
     * we can safely avoid the overhead of flattening
     */
    if (cfg->patterns->nelts == 1) {
       force_quick = 1;
    }
    for (i = 0; i < cfg->patterns->nelts; i++) {
        for (b = KUDA_BRIGADE_FIRST(mybb);
             b != KUDA_BRIGADE_SENTINEL(mybb);
             b = KUDA_BUCKET_NEXT(b)) {
            if (KUDA_BUCKET_IS_METADATA(b)) {
                /*
                 * we should NEVER see this, because we should never
                 * be passed any, but "handle" it just in case.
                 */
                continue;
            }
            if (kuda_bucket_read(b, &buff, &bytes, KUDA_BLOCK_READ)
                    == KUDA_SUCCESS) {
                int have_match = 0;

                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE8, 0, f->r,
                              "Line read (%" KUDA_SIZE_T_FMT " bytes): %.*s",
                              bytes, CAP2LINEMAX(bytes), buff);
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE8, 0, f->r,
                              "Replacing %s:'%s' by '%s'",
                              script->pattern ? "string" :
                              script->regexp  ? "regex"  :
                                                "unknown",
                              script->from, script->replacement);

                vb.strlen = 0;
                if (script->pattern) {
                    const char *repl;
                    /*
                     * space_left counts how many bytes we have left until the
                     * line length reaches max_line_length.
                     */
                    kuda_size_t space_left = cfg->max_line_length;
                    kuda_size_t repl_len = strlen(script->replacement);
                    while ((repl = kuda_strmatch(script->pattern, buff, bytes)))
                    {
                        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE8, 0, f->r,
                                      "Matching found, result: '%s'",
                                      script->replacement);
                        have_match = 1;
                        /* get offset into buff for pattern */
                        len = (kuda_size_t) (repl - buff);
                        if (script->flatten && !force_quick) {
                            /*
                             * We are flattening the buckets here, meaning
                             * that we don't do the fast bucket splits.
                             * Instead we copy over what the buckets would
                             * contain and use them. This is slow, since we
                             * are constanting allocing space and copying
                             * strings.
                             */
                            if (vb.strlen + len + repl_len > cfg->max_line_length)
                                return KUDA_ENOMEM;
                            clhy_varbuf_strmemcat(&vb, buff, len);
                            clhy_varbuf_strmemcat(&vb, script->replacement, repl_len);
                        }
                        else {
                            /*
                             * The string before the match but after the
                             * previous match (if any) has length 'len'.
                             * Check if we still have space for this string and
                             * the replacement string.
                             */
                            if (space_left < len + repl_len)
                                return KUDA_ENOMEM;
                            space_left -= len + repl_len;
                            /*
                             * We now split off the string before the match
                             * as its own bucket, then isolate the matched
                             * string and delete it.
                             */
                            SEDRMPATBCKT(b, len, tmp_b, script->patlen);
                            /*
                             * Finally, we create a bucket that contains the
                             * replacement...
                             */
                            tmp_b = kuda_bucket_transient_create(script->replacement,
                                      script->replen,
                                      f->r->connection->bucket_alloc);
                            /* ... and insert it */
                            KUDA_BUCKET_INSERT_BEFORE(b, tmp_b);
                        }
                        /* now we need to adjust buff for all these changes */
                        len += script->patlen;
                        bytes -= len;
                        buff += len;
                    }
                    if (have_match) {
                        if (script->flatten && !force_quick) {
                            /* XXX: we should check for CLHY_MAX_BUCKETS here and
                             * XXX: call clhy_pass_brigade accordingly
                             */
                            char *copy = clhy_varbuf_pdup(pool, &vb, NULL, 0,
                                                        buff, bytes, &len);
                            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE8, 0, f->r,
                                          "New line (%" KUDA_SIZE_T_FMT " bytes): %.*s",
                                          len, CAP2LINEMAX(len), copy);
                            tmp_b = kuda_bucket_pool_create(copy, len, pool,
                                                           f->r->connection->bucket_alloc);
                            KUDA_BUCKET_INSERT_BEFORE(b, tmp_b);
                            kuda_bucket_delete(b);
                            b = tmp_b;
                        }
                        else {
                            /*
                             * We want the behaviour to be predictable.
                             * Therefore we try to always error out if the
                             * line length is larger than the limit,
                             * regardless of the content of the line. So,
                             * let's check if the remaining non-matching
                             * string does not exceed the limit.
                             */
                            if (space_left < b->length)
                                return KUDA_ENOMEM;
                            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE8, 0, f->r,
                                          "New line (%" KUDA_SIZE_T_FMT " bytes): %.*s",
                                          bytes, CAP2LINEMAX(bytes), buff);
                        }
                    }
                }
                else if (script->regexp) {
                    int left = bytes;
                    const char *pos = buff;
                    char *repl;
                    kuda_size_t space_left = cfg->max_line_length;
                    while (!clhy_regexec_len(script->regexp, pos, left,
                                       CLHY_MAX_REG_MATCH, regm, 0)) {
                        kuda_status_t rv;
                        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE8, 0, f->r,
                                      "Matching found");
                        have_match = 1;
                        if (script->flatten && !force_quick) {
                            /* check remaining buffer size */
                            /* Note that the last param in clhy_varbuf_regsub below
                             * must stay positive. If it gets 0, it would mean
                             * unlimited space available. */
                            if (vb.strlen + regm[0].rm_so >= cfg->max_line_length)
                                return KUDA_ENOMEM;
                            /* copy bytes before the match */
                            if (regm[0].rm_so > 0)
                                clhy_varbuf_strmemcat(&vb, pos, regm[0].rm_so);
                            /* add replacement string, last argument is unsigned! */
                            rv = clhy_varbuf_regsub(&vb, script->replacement, pos,
                                                  CLHY_MAX_REG_MATCH, regm,
                                                  cfg->max_line_length - vb.strlen);
                            if (rv != KUDA_SUCCESS)
                                return rv;
                            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE8, 0, f->r,
                                          "Result: '%s'", vb.buf);
                        }
                        else {
                            kuda_size_t repl_len;
                            /* acount for string before the match */
                            if (space_left <= regm[0].rm_so)
                                return KUDA_ENOMEM;
                            space_left -= regm[0].rm_so;
                            rv = clhy_pregsub_ex(pool, &repl,
                                               script->replacement, pos,
                                               CLHY_MAX_REG_MATCH, regm,
                                               space_left);
                            if (rv != KUDA_SUCCESS)
                                return rv;
                            repl_len = strlen(repl);
                            space_left -= repl_len;
                            len = (kuda_size_t) (regm[0].rm_eo - regm[0].rm_so);
                            SEDRMPATBCKT(b, regm[0].rm_so, tmp_b, len);
                            tmp_b = kuda_bucket_transient_create(repl, repl_len,
                                                f->r->connection->bucket_alloc);
                            KUDA_BUCKET_INSERT_BEFORE(b, tmp_b);
                            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE8, 0, f->r,
                                          "Result: '%s'", repl);
                        }
                        /*
                         * reset to past what we just did. pos now maps to b
                         * again
                         */
                        pos += regm[0].rm_eo;
                        left -= regm[0].rm_eo;
                    }
                    if (have_match && script->flatten && !force_quick) {
                        char *copy;
                        /* Copy result plus the part after the last match into
                         * a bucket.
                         */
                        copy = clhy_varbuf_pdup(pool, &vb, NULL, 0, pos, left,
                                              &len);
                        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE8, 0, f->r,
                                      "New line (%" KUDA_SIZE_T_FMT " bytes): %.*s",
                                      len, CAP2LINEMAX(len), copy);
                        tmp_b = kuda_bucket_pool_create(copy, len, pool,
                                           f->r->connection->bucket_alloc);
                        KUDA_BUCKET_INSERT_BEFORE(b, tmp_b);
                        kuda_bucket_delete(b);
                        b = tmp_b;
                    }
                }
                else {
                    clhy_assert(0);
                    continue;
                }
            }
        }
        script++;
    }
    clhy_varbuf_free(&vb);
    return KUDA_SUCCESS;
}

static kuda_status_t substitute_filter(clhy_filter_t *f, kuda_bucket_brigade *bb)
{
    kuda_size_t bytes;
    kuda_size_t len;
    kuda_size_t fbytes;
    const char *buff;
    const char *nl = NULL;
    char *bflat;
    kuda_bucket *b;
    kuda_bucket *tmp_b;
    kuda_bucket_brigade *tmp_bb = NULL;
    kuda_status_t rv;
    subst_dir_conf *cfg =
    (subst_dir_conf *) clhy_get_capi_config(f->r->per_dir_config,
                                             &substitute_capi);

    substitute_capi_ctx *ctx = f->ctx;

    /*
     * First time around? Create the saved bb that we used for each pass
     * through. Note that we can also get here when we explicitly clear ctx,
     * for error handling
     */
    if (!ctx) {
        f->ctx = ctx = kuda_pcalloc(f->r->pool, sizeof(*ctx));
        /*
         * Create all the temporary brigades we need and reuse them to avoid
         * creating them over and over again from r->pool which would cost a
         * lot of memory in some cases.
         */
        ctx->linebb = kuda_brigade_create(f->r->pool, f->c->bucket_alloc);
        ctx->linesbb = kuda_brigade_create(f->r->pool, f->c->bucket_alloc);
        ctx->pattbb = kuda_brigade_create(f->r->pool, f->c->bucket_alloc);
        /*
         * Everything to be passed to the next filter goes in
         * here, our pass brigade.
         */
        ctx->passbb = kuda_brigade_create(f->r->pool, f->c->bucket_alloc);
        /* Create our temporary pool only once */
        kuda_pool_create(&(ctx->tpool), f->r->pool);
        kuda_table_unset(f->r->headers_out, "Content-Length");
    }

    /*
     * Shortcircuit processing
     */
    if (KUDA_BRIGADE_EMPTY(bb))
        return KUDA_SUCCESS;

    /*
     * Here's the concept:
     *  Read in the data and look for newlines. Once we
     *  find a full "line", add it to our working brigade.
     *  If we've finished reading the brigade and we have
     *  any left over data (not a "full" line), store that
     *  for the next pass.
     *
     * Note: anything stored in ctx->linebb for sure does not have
     * a newline char, so we don't concat that bb with the
     * new bb, since we would spending time searching for the newline
     * in data we know it doesn't exist. So instead, we simply scan
     * our current bb and, if we see a newline, prepend ctx->linebb
     * to the front of it. This makes the code much less straight-
     * forward (otherwise we could KUDA_BRIGADE_CONCAT(ctx->linebb, bb)
     * and just scan for newlines and not bother with needing to know
     * when ctx->linebb needs to be reset) but also faster. We'll take
     * the speed.
     *
     * Note: kuda_brigade_split_line would be nice here, but we
     * really can't use it since we need more control and we want
     * to re-use already read bucket data.
     *
     * See capi_include if still confused :)
     */

    while ((b = KUDA_BRIGADE_FIRST(bb)) && (b != KUDA_BRIGADE_SENTINEL(bb))) {
        if (KUDA_BUCKET_IS_EOS(b)) {
            /*
             * if we see the EOS, then we need to pass along everything we
             * have. But if the ctx->linebb isn't empty, then we need to add
             * that to the end of what we'll be passing.
             */
            if (!KUDA_BRIGADE_EMPTY(ctx->linebb)) {
                rv = kuda_brigade_pflatten(ctx->linebb, &bflat,
                                          &fbytes, ctx->tpool);
                if (rv != KUDA_SUCCESS)
                    goto err;
                if (fbytes > cfg->max_line_length) {
                    rv = KUDA_ENOMEM;
                    goto err;
                }
                tmp_b = kuda_bucket_transient_create(bflat, fbytes,
                                                f->r->connection->bucket_alloc);
                rv = do_pattmatch(f, tmp_b, ctx->pattbb, ctx->tpool);
                if (rv != KUDA_SUCCESS)
                    goto err;
                KUDA_BRIGADE_CONCAT(ctx->passbb, ctx->pattbb);
                kuda_brigade_cleanup(ctx->linebb);
            }
            KUDA_BUCKET_REMOVE(b);
            KUDA_BRIGADE_INSERT_TAIL(ctx->passbb, b);
        }
        /*
         * No need to handle FLUSH buckets separately as we call
         * clhy_pass_brigade anyway at the end of the loop.
         */
        else if (KUDA_BUCKET_IS_METADATA(b)) {
            KUDA_BUCKET_REMOVE(b);
            KUDA_BRIGADE_INSERT_TAIL(ctx->passbb, b);
        }
        else {
            /*
             * We have actual "data" so read in as much as we can and start
             * scanning and splitting from our read buffer
             */
            rv = kuda_bucket_read(b, &buff, &bytes, KUDA_BLOCK_READ);
            if (rv != KUDA_SUCCESS || bytes == 0) {
                kuda_bucket_delete(b);
            }
            else {
                int num = 0;
                while (bytes > 0) {
                    nl = memchr(buff, '\n', bytes);
                    if (nl) {
                        len = (kuda_size_t) (nl - buff) + 1;
                        /* split *after* the newline */
                        kuda_bucket_split(b, len);
                        /*
                         * We've likely read more data, so bypass rereading
                         * bucket data and continue scanning through this
                         * buffer
                         */
                        bytes -= len;
                        buff += len;
                        /*
                         * we need b to be updated for future potential
                         * splitting
                         */
                        tmp_b = KUDA_BUCKET_NEXT(b);
                        KUDA_BUCKET_REMOVE(b);
                        /*
                         * Hey, we found a newline! Don't forget the old
                         * stuff that needs to be added to the front. So we
                         * add the split bucket to the end, flatten the whole
                         * bb, morph the whole shebang into a bucket which is
                         * then added to the tail of the newline bb.
                         */
                        if (!KUDA_BRIGADE_EMPTY(ctx->linebb)) {
                            KUDA_BRIGADE_INSERT_TAIL(ctx->linebb, b);
                            rv = kuda_brigade_pflatten(ctx->linebb, &bflat,
                                                      &fbytes, ctx->tpool);
                            if (rv != KUDA_SUCCESS)
                                goto err;
                            if (fbytes > cfg->max_line_length) {
                                /* Avoid pflattening further lines, we will
                                 * abort later on anyway.
                                 */
                                rv = KUDA_ENOMEM;
                                goto err;
                            }
                            b = kuda_bucket_transient_create(bflat, fbytes,
                                            f->r->connection->bucket_alloc);
                            kuda_brigade_cleanup(ctx->linebb);
                        }
                        rv = do_pattmatch(f, b, ctx->pattbb, ctx->tpool);
                        if (rv != KUDA_SUCCESS)
                            goto err;
                        /*
                         * Count how many buckets we have in ctx->passbb
                         * so far. Yes, this is correct we count ctx->passbb
                         * and not ctx->pattbb as we do not reset num on every
                         * iteration.
                         */
                        for (b = KUDA_BRIGADE_FIRST(ctx->pattbb);
                             b != KUDA_BRIGADE_SENTINEL(ctx->pattbb);
                             b = KUDA_BUCKET_NEXT(b)) {
                            num++;
                        }
                        KUDA_BRIGADE_CONCAT(ctx->passbb, ctx->pattbb);
                        /*
                         * If the number of buckets in ctx->passbb reaches an
                         * "insane" level, we consume much memory for all the
                         * buckets as such. So lets flush them down the chain
                         * in this case and thus clear ctx->passbb. This frees
                         * the buckets memory for further processing.
                         * Usually this condition should not become true, but
                         * it is a safety measure for edge cases.
                         */
                        if (num > CLHY_MAX_BUCKETS) {
                            b = kuda_bucket_flush_create(
                                                f->r->connection->bucket_alloc);
                            KUDA_BRIGADE_INSERT_TAIL(ctx->passbb, b);
                            rv = clhy_pass_brigade(f->next, ctx->passbb);
                            kuda_brigade_cleanup(ctx->passbb);
                            num = 0;
                            kuda_pool_clear(ctx->tpool);
                            if (rv != KUDA_SUCCESS)
                                goto err;
                        }
                        b = tmp_b;
                    }
                    else {
                        /*
                         * no newline in whatever is left of this buffer so
                         * tuck data away and get next bucket
                         */
                        KUDA_BUCKET_REMOVE(b);
                        KUDA_BRIGADE_INSERT_TAIL(ctx->linebb, b);
                        bytes = 0;
                    }
                }
            }
        }
        if (!KUDA_BRIGADE_EMPTY(ctx->passbb)) {
            rv = clhy_pass_brigade(f->next, ctx->passbb);
            kuda_brigade_cleanup(ctx->passbb);
            if (rv != KUDA_SUCCESS)
                goto err;
        }
        kuda_pool_clear(ctx->tpool);
    }

    /* Anything left we want to save/setaside for the next go-around */
    if (!KUDA_BRIGADE_EMPTY(ctx->linebb)) {
        /*
         * Provide clhy_save_brigade with an existing empty brigade
         * (ctx->linesbb) to avoid creating a new one.
         */
        clhy_save_brigade(f, &(ctx->linesbb), &(ctx->linebb), f->r->pool);
        tmp_bb = ctx->linebb;
        ctx->linebb = ctx->linesbb;
        ctx->linesbb = tmp_bb;
    }

    return KUDA_SUCCESS;
err:
    if (rv == KUDA_ENOMEM)
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, f->r, CLHYLOGNO(01328) "Line too long, URI %s",
                      f->r->uri);
    kuda_pool_clear(ctx->tpool);
    return rv;
}

static const char *set_pattern(cmd_parms *cmd, void *cfg, const char *line)
{
    char *from = NULL;
    char *to = NULL;
    char *flags = NULL;
    char *ourline;
    char delim;
    subst_pattern_t *nscript;
    int is_pattern = 0;
    int ignore_case = 0;
    int flatten = 1;
    clhy_regex_t *r = NULL;

    if (kuda_tolower(*line) != 's') {
        return "Bad Substitute format, must be an s/// pattern";
    }
    ourline = kuda_pstrdup(cmd->pool, line);
    delim = *++ourline;
    if (delim)
        from = ++ourline;
    if (from) {
        if (*ourline != delim) {
            while (*++ourline && *ourline != delim);
        }
        if (*ourline) {
            *ourline = '\0';
            to = ++ourline;
        }
    }
    if (to) {
        if (*ourline != delim) {
            while (*++ourline && *ourline != delim);
        }
        if (*ourline) {
            *ourline = '\0';
            flags = ++ourline;
        }
    }

    if (!delim || !from || !*from || !to) {
        return "Bad Substitute format, must be a complete s/// pattern";
    }

    if (flags) {
        while (*flags) {
            delim = kuda_tolower(*flags);    /* re-use */
            if (delim == 'i')
                ignore_case = 1;
            else if (delim == 'n')
                is_pattern = 1;
            else if (delim == 'f')
                flatten = 1;
            else if (delim == 'q')
                flatten = 0;
            else
                return "Bad Substitute flag, only s///[infq] are supported";
            flags++;
        }
    }

    /* first see if we can compile the regex */
    if (!is_pattern) {
        r = clhy_pregcomp(cmd->pool, from, CLHY_REG_EXTENDED |
                        (ignore_case ? CLHY_REG_ICASE : 0));
        if (!r)
            return "Substitute could not compile regex";
    }
    nscript = kuda_array_push(((subst_dir_conf *) cfg)->patterns);
    /* init the new entries */
    nscript->pattern = NULL;
    nscript->regexp = NULL;
    nscript->replacement = NULL;
    nscript->patlen = 0;
    nscript->from = from;

    if (is_pattern) {
        nscript->patlen = strlen(from);
        nscript->pattern = kuda_strmatch_precompile(cmd->pool, from,
                                                   !ignore_case);
    }
    else {
        nscript->regexp = r;
    }

    nscript->replacement = to;
    nscript->replen = strlen(to);
    nscript->flatten = flatten;

    return NULL;
}

#define KBYTE         1024
#define MBYTE         1048576
#define GBYTE         1073741824

static const char *set_max_line_length(cmd_parms *cmd, void *cfg, const char *arg)
{
    subst_dir_conf *dcfg = (subst_dir_conf *)cfg;
    kuda_off_t max;
    char *end;
    kuda_status_t rv;

    rv = kuda_strtoff(&max, arg, &end, 10);
    if (rv == KUDA_SUCCESS) {
        if ((*end == 'K' || *end == 'k') && !end[1]) {
            max *= KBYTE;
        }
        else if ((*end == 'M' || *end == 'm') && !end[1]) {
            max *= MBYTE;
        }
        else if ((*end == 'G' || *end == 'g') && !end[1]) {
            max *= GBYTE;
        }
        else if (*end && /* neither empty nor [Bb] */
                 ((*end != 'B' && *end != 'b') || end[1])) {
            rv = KUDA_EGENERAL;
        }
    }

    if (rv != KUDA_SUCCESS || max < 0)
    {
        return "SubstituteMaxLineLength must be a non-negative integer optionally "
               "suffixed with 'b', 'k', 'm' or 'g'.";
    }
    dcfg->max_line_length = (kuda_size_t)max;
    dcfg->max_line_length_set = 1;
    return NULL;
}

#define PROTO_FLAGS CLHY_FILTER_PROTO_CHANGE|CLHY_FILTER_PROTO_CHANGE_LENGTH
static void register_hooks(kuda_pool_t *pool)
{
    clhy_register_output_filter(substitute_filter_name, substitute_filter,
                              NULL, CLHY_FTYPE_RESOURCE);
}

static const command_rec substitute_cmds[] = {
    CLHY_INIT_TAKE1("Substitute", set_pattern, NULL, OR_FILEINFO,
                  "Pattern to filter the response content (s/foo/bar/[inf])"),
    CLHY_INIT_TAKE1("SubstituteMaxLineLength", set_max_line_length, NULL, OR_FILEINFO,
                  "Maximum line length"),
    CLHY_INIT_FLAG("SubstituteInheritBefore", clhy_set_flag_slot,
                 (void *)KUDA_OFFSETOF(subst_dir_conf, inherit_before), OR_FILEINFO,
                 "Apply inherited patterns before those of the current context"),
    {NULL}
};

CLHY_DECLARE_CAPI(substitute) = {
    STANDARD16_CAPI_STUFF,
    create_substitute_dcfg,     /* dir config creater */
    merge_substitute_dcfg,      /* dir merger --- default is to override */
    NULL,                       /* server config */
    NULL,                       /* merge server config */
    substitute_cmds,            /* command table */
    register_hooks              /* register hooks */
};

