/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * capi_ext_filter allows Unix-style filters to filter http content.
 */

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "http_protocol.h"

#include "http_core.h"
#include "kuda_buckets.h"
#include "util_filter.h"
#include "util_script.h"
#include "util_time.h"
#include "kuda_strings.h"
#include "kuda_hash.h"
#include "kuda_lib.h"
#include "kuda_poll.h"
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

typedef struct ef_server_t {
    kuda_pool_t *p;
    kuda_hash_t *h;
} ef_server_t;

typedef struct ef_filter_t {
    const char *name;
    enum {INPUT_FILTER=1, OUTPUT_FILTER} mode;
    clhy_filter_type ftype;
    const char *command;
    const char *enable_env;
    const char *disable_env;
    char **args;
    const char *intype;             /* list of IMTs we process (well, just one for now) */
#define INTYPE_ALL (char *)1
    const char *outtype;            /* IMT of filtered output */
#define OUTTYPE_UNCHANGED (char *)1
    int preserves_content_length;
} ef_filter_t;

typedef struct ef_dir_t {
    int log_stderr;
    int onfail;
} ef_dir_t;

typedef struct ef_ctx_t {
    kuda_pool_t *p;
    kuda_proc_t *proc;
    kuda_procattr_t *procattr;
    ef_dir_t *dc;
    ef_filter_t *filter;
    int noop, hit_eos;
#if KUDA_FILES_AS_SOCKETS
    kuda_pollset_t *pollset;
#endif
} ef_ctx_t;

cAPI CLHY_CAPI_DECLARE_DATA ext_filter_capi;
static const server_rec *main_server;

static kuda_status_t ef_output_filter(clhy_filter_t *, kuda_bucket_brigade *);
static kuda_status_t ef_input_filter(clhy_filter_t *, kuda_bucket_brigade *,
                                     clhy_input_mode_t, kuda_read_type_e,
                                     kuda_off_t);

#define ERRFN_USERDATA_KEY         "EXTFILTCHILDERRFN"

static void *create_ef_dir_conf(kuda_pool_t *p, char *dummy)
{
    ef_dir_t *dc = (ef_dir_t *)kuda_pcalloc(p, sizeof(ef_dir_t));

    dc->log_stderr = -1;
    dc->onfail = -1;

    return dc;
}

static void *create_ef_server_conf(kuda_pool_t *p, server_rec *s)
{
    ef_server_t *conf;

    conf = (ef_server_t *)kuda_pcalloc(p, sizeof(ef_server_t));
    conf->p = p;
    conf->h = kuda_hash_make(conf->p);
    return conf;
}

static void *merge_ef_dir_conf(kuda_pool_t *p, void *basev, void *overridesv)
{
    ef_dir_t *a = (ef_dir_t *)kuda_pcalloc (p, sizeof(ef_dir_t));
    ef_dir_t *base = (ef_dir_t *)basev, *over = (ef_dir_t *)overridesv;

    if (over->log_stderr != -1) {   /* if admin coded something... */
        a->log_stderr = over->log_stderr;
    }
    else {
        a->log_stderr = base->log_stderr;
    }

    if (over->onfail != -1) {   /* if admin coded something... */
        a->onfail = over->onfail;
    }
    else {
        a->onfail = base->onfail;
    }

    return a;
}

static const char *add_options(cmd_parms *cmd, void *in_dc,
                               const char *arg)
{
    ef_dir_t *dc = in_dc;

    if (!strcasecmp(arg, "LogStderr")) {
        dc->log_stderr = 1;
    }
    else if (!strcasecmp(arg, "NoLogStderr")) {
        dc->log_stderr = 0;
    }
    else if (!strcasecmp(arg, "Onfail=remove")) {
        dc->onfail = 1;
    }
    else if (!strcasecmp(arg, "Onfail=abort")) {
        dc->onfail = 0;
    }
    else {
        return kuda_pstrcat(cmd->temp_pool,
                           "Invalid ExtFilterOptions option: ",
                           arg,
                           NULL);
    }

    return NULL;
}

static const char *parse_cmd(kuda_pool_t *p, const char **args, ef_filter_t *filter)
{
    if (**args == '"') {
        const char *start = *args + 1;
        char *parms;
        int escaping = 0;
        kuda_status_t rv;

        ++*args; /* move past leading " */
        /* find true end of args string (accounting for escaped quotes) */
        while (**args && (**args != '"' || (**args == '"' && escaping))) {
            if (escaping) {
                escaping = 0;
            }
            else if (**args == '\\') {
                escaping = 1;
            }
            ++*args;
        }
        if (**args != '"') {
            return "Expected cmd= delimiter";
        }
        /* copy *just* the arg string for parsing, */
        parms = kuda_pstrndup(p, start, *args - start);
        ++*args; /* move past trailing " */

        /* parse and tokenize the args. */
        rv = kuda_tokenize_to_argv(parms, &(filter->args), p);
        if (rv != KUDA_SUCCESS) {
            return "cmd= parse error";
        }
    }
    else
    {
        /* simple path */
        /* Allocate space for two argv pointers and parse the args. */
        filter->args = (char **)kuda_palloc(p, 2 * sizeof(char *));
        filter->args[0] = clhy_getword_white(p, args);
        filter->args[1] = NULL; /* end of args */
    }
    if (!filter->args[0]) {
        return "Invalid cmd= parameter";
    }
    filter->command = filter->args[0];

    return NULL;
}

static const char *define_filter(cmd_parms *cmd, void *dummy, const char *args)
{
    ef_server_t *conf = clhy_get_capi_config(cmd->server->capi_config,
                                             &ext_filter_capi);
    const char *token;
    const char *name;
    char *normalized_name;
    ef_filter_t *filter;

    name = clhy_getword_white(cmd->pool, &args);
    if (!name) {
        return "Filter name not found";
    }

    /* During request processing, we find information about the filter
     * by looking up the filter name provided by core server in our
     * hash table.  But the core server has normalized the filter
     * name by converting it to lower case.  Thus, when adding the
     * filter to our hash table we have to use lower case as well.
     */
    normalized_name = kuda_pstrdup(cmd->pool, name);
    clhy_str_tolower(normalized_name);

    if (kuda_hash_get(conf->h, normalized_name, KUDA_HASH_KEY_STRING)) {
        return kuda_psprintf(cmd->pool, "ExtFilter %s is already defined",
                            name);
    }

    filter = (ef_filter_t *)kuda_pcalloc(conf->p, sizeof(ef_filter_t));
    filter->name = name;
    filter->mode = OUTPUT_FILTER;
    filter->ftype = CLHY_FTYPE_RESOURCE;
    kuda_hash_set(conf->h, normalized_name, KUDA_HASH_KEY_STRING, filter);

    while (*args) {
        while (kuda_isspace(*args)) {
            ++args;
        }

        /* Nasty parsing...  I wish I could simply use clhy_getword_white()
         * here and then look at the token, but clhy_getword_white() doesn't
         * do the right thing when we have cmd="word word word"
         */
        if (!strncasecmp(args, "preservescontentlength", 22)) {
            token = clhy_getword_white(cmd->pool, &args);
            if (!strcasecmp(token, "preservescontentlength")) {
                filter->preserves_content_length = 1;
            }
            else {
                return kuda_psprintf(cmd->pool,
                                    "mangled argument `%s'",
                                    token);
            }
            continue;
        }

        if (!strncasecmp(args, "mode=", 5)) {
            args += 5;
            token = clhy_getword_white(cmd->pool, &args);
            if (!strcasecmp(token, "output")) {
                filter->mode = OUTPUT_FILTER;
            }
            else if (!strcasecmp(token, "input")) {
                filter->mode = INPUT_FILTER;
            }
            else {
                return kuda_psprintf(cmd->pool, "Invalid mode: `%s'",
                                    token);
            }
            continue;
        }

        if (!strncasecmp(args, "ftype=", 6)) {
            args += 6;
            token = clhy_getword_white(cmd->pool, &args);
            filter->ftype = atoi(token);
            continue;
        }

        if (!strncasecmp(args, "enableenv=", 10)) {
            args += 10;
            token = clhy_getword_white(cmd->pool, &args);
            filter->enable_env = token;
            continue;
        }

        if (!strncasecmp(args, "disableenv=", 11)) {
            args += 11;
            token = clhy_getword_white(cmd->pool, &args);
            filter->disable_env = token;
            continue;
        }

        if (!strncasecmp(args, "intype=", 7)) {
            args += 7;
            filter->intype = clhy_getword_white(cmd->pool, &args);
            continue;
        }

        if (!strncasecmp(args, "outtype=", 8)) {
            args += 8;
            filter->outtype = clhy_getword_white(cmd->pool, &args);
            continue;
        }

        if (!strncasecmp(args, "cmd=", 4)) {
            args += 4;
            if ((token = parse_cmd(cmd->pool, &args, filter))) {
                return token;
            }
            continue;
        }

        return kuda_psprintf(cmd->pool, "Unexpected parameter: `%s'",
                            args);
    }

    /* parsing is done...  register the filter
     */
    if (filter->mode == OUTPUT_FILTER) {
        /* XXX need a way to ensure uniqueness among all filters */
        clhy_register_output_filter(filter->name, ef_output_filter, NULL, filter->ftype);
    }
    else if (filter->mode == INPUT_FILTER) {
        /* XXX need a way to ensure uniqueness among all filters */
        clhy_register_input_filter(filter->name, ef_input_filter, NULL, filter->ftype);
    }
    else {
        clhy_assert(1 != 1); /* we set the field wrong somehow */
    }

    return NULL;
}

static const command_rec cmds[] =
{
    CLHY_INIT_ITERATE("ExtFilterOptions",
                    add_options,
                    NULL,
                    ACCESS_CONF, /* same as SetInputFilter/SetOutputFilter */
                    "valid options: LogStderr, NoLogStderr"),
    CLHY_INIT_RAW_ARGS("ExtFilterDefine",
                     define_filter,
                     NULL,
                     RSRC_CONF,
                     "Define an external filter"),
    {NULL}
};

static int ef_init(kuda_pool_t *p, kuda_pool_t *plog, kuda_pool_t *ptemp, server_rec *main_s)
{
    main_server = main_s;
    return OK;
}

static void register_hooks(kuda_pool_t *p)
{
    clhy_hook_post_config(ef_init, NULL, NULL, KUDA_HOOK_MIDDLE);
}

static kuda_status_t set_resource_limits(request_rec *r,
                                        kuda_procattr_t *procattr)
{
#if defined(RLIMIT_CPU)  || defined(RLIMIT_NPROC) || \
    defined(RLIMIT_DATA) || defined(RLIMIT_VMEM) || defined (RLIMIT_AS)
    core_dir_config *conf =
        (core_dir_config *)clhy_get_core_capi_config(r->per_dir_config);
    kuda_status_t rv;

#ifdef RLIMIT_CPU
    rv = kuda_procattr_limit_set(procattr, KUDA_LIMIT_CPU, conf->limit_cpu);
    clhy_assert(rv == KUDA_SUCCESS); /* otherwise, we're out of sync with KUDA */
#endif
#if defined(RLIMIT_DATA) || defined(RLIMIT_VMEM) || defined(RLIMIT_AS)
    rv = kuda_procattr_limit_set(procattr, KUDA_LIMIT_MEM, conf->limit_mem);
    clhy_assert(rv == KUDA_SUCCESS); /* otherwise, we're out of sync with KUDA */
#endif
#ifdef RLIMIT_NPROC
    rv = kuda_procattr_limit_set(procattr, KUDA_LIMIT_NPROC, conf->limit_nproc);
    clhy_assert(rv == KUDA_SUCCESS); /* otherwise, we're out of sync with KUDA */
#endif

#endif /* if at least one limit defined */

    return KUDA_SUCCESS;
}

static kuda_status_t ef_close_file(void *vfile)
{
    return kuda_file_close(vfile);
}

static void child_errfn(kuda_pool_t *pool, kuda_status_t err, const char *description)
{
    request_rec *r;
    void *vr;
    kuda_file_t *stderr_log;
    char time_str[KUDA_CTIME_LEN];

    kuda_pool_userdata_get(&vr, ERRFN_USERDATA_KEY, pool);
    r = vr;
    kuda_file_open_stderr(&stderr_log, pool);
    clhy_recent_ctime(time_str, kuda_time_now());
    kuda_file_printf(stderr_log,
                    "[%s] [client %s] capi_ext_filter (%d)%pm: %s\n",
                    time_str,
                    r->useragent_ip,
                    err,
                    &err,
                    description);
}

/* init_ext_filter_process: get the external filter process going
 * This is per-filter-instance (i.e., per-request) initialization.
 */
static kuda_status_t init_ext_filter_process(clhy_filter_t *f)
{
    ef_ctx_t *ctx = f->ctx;
    kuda_status_t rc;
    ef_dir_t *dc = ctx->dc;
    const char * const *env;

    ctx->proc = kuda_pcalloc(ctx->p, sizeof(*ctx->proc));

    rc = kuda_procattr_create(&ctx->procattr, ctx->p);
    clhy_assert(rc == KUDA_SUCCESS);

    rc = kuda_procattr_io_set(ctx->procattr,
                            KUDA_CHILD_BLOCK,
                            KUDA_CHILD_BLOCK,
                            KUDA_CHILD_BLOCK);
    clhy_assert(rc == KUDA_SUCCESS);

    rc = set_resource_limits(f->r, ctx->procattr);
    clhy_assert(rc == KUDA_SUCCESS);

    if (dc->log_stderr > 0) {
        rc = kuda_procattr_child_err_set(ctx->procattr,
                                      f->r->server->error_log, /* stderr in child */
                                      NULL);
        clhy_assert(rc == KUDA_SUCCESS);
    }

    rc = kuda_procattr_child_errfn_set(ctx->procattr, child_errfn);
    clhy_assert(rc == KUDA_SUCCESS);
    kuda_pool_userdata_set(f->r, ERRFN_USERDATA_KEY, kuda_pool_cleanup_null, ctx->p);

    rc = kuda_procattr_error_check_set(ctx->procattr, 1);
    if (rc != KUDA_SUCCESS) {
        return rc;
    }

    /* add standard CGI variables as well as DOCUMENT_URI, DOCUMENT_PATH_INFO,
     * and QUERY_STRING_UNESCAPED
     */
    clhy_add_cgi_vars(f->r);
    clhy_add_common_vars(f->r);
    kuda_table_setn(f->r->subprocess_env, "DOCUMENT_URI", f->r->uri);
    kuda_table_setn(f->r->subprocess_env, "DOCUMENT_PATH_INFO", f->r->path_info);
    if (f->r->args) {
            /* QUERY_STRING is added by clhy_add_cgi_vars */
        char *arg_copy = kuda_pstrdup(f->r->pool, f->r->args);
        clhy_unescape_url(arg_copy);
        kuda_table_setn(f->r->subprocess_env, "QUERY_STRING_UNESCAPED",
                       clhy_escape_shell_cmd(f->r->pool, arg_copy));
    }

    env = (const char * const *) clhy_create_environment(ctx->p,
                                                       f->r->subprocess_env);

    rc = kuda_proc_create(ctx->proc,
                            ctx->filter->command,
                            (const char * const *)ctx->filter->args,
                            env, /* environment */
                            ctx->procattr,
                            ctx->p);
    if (rc != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rc, f->r, CLHYLOGNO(01458)
                      "couldn't create child process to run `%s'",
                      ctx->filter->command);
        return rc;
    }

    kuda_pool_note_subprocess(ctx->p, ctx->proc, KUDA_KILL_AFTER_TIMEOUT);

    /* We don't want the handle to the child's stdin inherited by any
     * other processes created by wwhy.  Otherwise, when we close our
     * handle, the child won't see EOF because another handle will still
     * be open.
     */

    kuda_pool_cleanup_register(ctx->p, ctx->proc->in,
                         kuda_pool_cleanup_null, /* other mechanism */
                         ef_close_file);

#if KUDA_FILES_AS_SOCKETS
    {
        kuda_pollfd_t pfd = { 0 };

        rc = kuda_pollset_create(&ctx->pollset, 2, ctx->p, 0);
        clhy_assert(rc == KUDA_SUCCESS);

        pfd.p         = ctx->p;
        pfd.desc_type = KUDA_POLL_FILE;
        pfd.reqevents = KUDA_POLLOUT;
        pfd.desc.f    = ctx->proc->in;
        rc = kuda_pollset_add(ctx->pollset, &pfd);
        clhy_assert(rc == KUDA_SUCCESS);

        pfd.reqevents = KUDA_POLLIN;
        pfd.desc.f    = ctx->proc->out;
        rc = kuda_pollset_add(ctx->pollset, &pfd);
        clhy_assert(rc == KUDA_SUCCESS);
    }
#endif

    return KUDA_SUCCESS;
}

static const char *get_cfg_string(ef_dir_t *dc, ef_filter_t *filter, kuda_pool_t *p)
{
    const char *log_stderr_str = dc->log_stderr < 1 ?
        "NoLogStderr" : "LogStderr";
    const char *preserve_content_length_str = filter->preserves_content_length ?
        "PreservesContentLength" : "!PreserveContentLength";
    const char *intype_str = !filter->intype ?
        "*/*" : filter->intype;
    const char *outtype_str = !filter->outtype ?
        "(unchanged)" : filter->outtype;

    return kuda_psprintf(p,
                        "ExtFilterOptions %s %s ExtFilterInType %s "
                        "ExtFilterOuttype %s",
                        log_stderr_str, preserve_content_length_str,
                        intype_str, outtype_str);
}

static ef_filter_t *find_filter_def(const server_rec *s, const char *fname)
{
    ef_server_t *sc;
    ef_filter_t *f;

    sc = clhy_get_capi_config(s->capi_config, &ext_filter_capi);
    f = kuda_hash_get(sc->h, fname, KUDA_HASH_KEY_STRING);
    if (!f && s != main_server) {
        s = main_server;
        sc = clhy_get_capi_config(s->capi_config, &ext_filter_capi);
        f = kuda_hash_get(sc->h, fname, KUDA_HASH_KEY_STRING);
    }
    return f;
}

static kuda_status_t init_filter_instance(clhy_filter_t *f)
{
    ef_ctx_t *ctx;
    ef_dir_t *dc;
    kuda_status_t rv;

    f->ctx = ctx = kuda_pcalloc(f->r->pool, sizeof(ef_ctx_t));
    dc = clhy_get_capi_config(f->r->per_dir_config,
                              &ext_filter_capi);
    ctx->dc = dc;
    /* look for the user-defined filter */
    ctx->filter = find_filter_def(f->r->server, f->frec->name);
    if (!ctx->filter) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, f->r, CLHYLOGNO(01459)
                      "couldn't find definition of filter '%s'",
                      f->frec->name);
        return KUDA_EINVAL;
    }
    ctx->p = f->r->pool;
    if (ctx->filter->intype &&
        ctx->filter->intype != INTYPE_ALL) {
        const char *ctypes;

        if (ctx->filter->mode == INPUT_FILTER) {
            ctypes = kuda_table_get(f->r->headers_in, "Content-Type");
        }
        else {
            ctypes = f->r->content_type;
        }

        if (ctypes) {
            const char *ctype = clhy_getword(f->r->pool, &ctypes, ';');

            if (strcasecmp(ctx->filter->intype, ctype)) {
                /* wrong IMT for us; don't mess with the output */
                ctx->noop = 1;
            }
        }
        else {
            ctx->noop = 1;
        }
    }
    if (ctx->filter->enable_env &&
        !kuda_table_get(f->r->subprocess_env, ctx->filter->enable_env)) {
        /* an environment variable that enables the filter isn't set; bail */
        ctx->noop = 1;
    }
    if (ctx->filter->disable_env &&
        kuda_table_get(f->r->subprocess_env, ctx->filter->disable_env)) {
        /* an environment variable that disables the filter is set; bail */
        ctx->noop = 1;
    }
    if (!ctx->noop) {
        rv = init_ext_filter_process(f);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
        if (ctx->filter->outtype &&
            ctx->filter->outtype != OUTTYPE_UNCHANGED) {
            clhy_set_content_type(f->r, ctx->filter->outtype);
        }
        if (ctx->filter->preserves_content_length != 1) {
            /* nasty, but needed to avoid confusing the browser
             */
            kuda_table_unset(f->r->headers_out, "Content-Length");
        }
    }

    if (CLHYLOGrtrace1(f->r)) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE1, 0, f->r,
                      "%sfiltering `%s' of type `%s' through `%s', cfg %s",
                      ctx->noop ? "NOT " : "",
                      f->r->uri ? f->r->uri : f->r->filename,
                      f->r->content_type ? f->r->content_type : "(unspecified)",
                      ctx->filter->command,
                      get_cfg_string(dc, ctx->filter, f->r->pool));
    }

    return KUDA_SUCCESS;
}

/* drain_available_output():
 *
 * if any data is available from the filter, read it and append it
 * to the bucket brigade
 */
static kuda_status_t drain_available_output(clhy_filter_t *f,
                                           kuda_bucket_brigade *bb)
{
    request_rec *r = f->r;
    conn_rec *c = r->connection;
    ef_ctx_t *ctx = f->ctx;
    kuda_size_t len;
    char buf[4096];
    kuda_status_t rv;
    kuda_bucket *b;

    while (1) {
        int lvl = CLHYLOG_TRACE5;
        len = sizeof(buf);
        rv = kuda_file_read(ctx->proc->out, buf, &len);
        if (rv && !KUDA_STATUS_IS_EAGAIN(rv))
           lvl = CLHYLOG_DEBUG;
        clhy_log_rerror(CLHYLOG_MARK, lvl, rv, r, CLHYLOGNO(01460)
                      "kuda_file_read(child output), len %" KUDA_SIZE_T_FMT,
                      !rv ? len : -1);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
        b = kuda_bucket_heap_create(buf, len, NULL, c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);
        return KUDA_SUCCESS;
    }
    /* we should never get here; if we do, a bogus error message would be
     * the least of our problems
     */
    return KUDA_ANONYMOUS;
}

static kuda_status_t pass_data_to_filter(clhy_filter_t *f, const char *data,
                                        kuda_size_t len, kuda_bucket_brigade *bb)
{
    ef_ctx_t *ctx = f->ctx;
    kuda_status_t rv;
    kuda_size_t bytes_written = 0;
    kuda_size_t tmplen;

    do {
        tmplen = len - bytes_written;
        rv = kuda_file_write_full(ctx->proc->in,
                       (const char *)data + bytes_written,
                       tmplen, &tmplen);
        bytes_written += tmplen;
        if (rv != KUDA_SUCCESS && !KUDA_STATUS_IS_EAGAIN(rv)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, f->r, CLHYLOGNO(01461)
                          "kuda_file_write(child input), len %" KUDA_SIZE_T_FMT,
                          tmplen);
            return rv;
        }
        if (KUDA_STATUS_IS_EAGAIN(rv)) {
            /* XXX handle blocking conditions here...  if we block, we need
             * to read data from the child process and pass it down to the
             * next filter!
             */
            rv = drain_available_output(f, bb);
            if (KUDA_STATUS_IS_EAGAIN(rv)) {
#if KUDA_FILES_AS_SOCKETS
                int num_events;
                const kuda_pollfd_t *pdesc;

                rv = kuda_pollset_poll(ctx->pollset, f->r->server->timeout,
                                      &num_events, &pdesc);
                if (rv != KUDA_SUCCESS && !KUDA_STATUS_IS_EINTR(rv)) {
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, rv, f->r, CLHYLOGNO(01462)
                                  "kuda_pollset_poll()");
                    /* some error such as KUDA_TIMEUP */
                    return rv;
                }
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE6, rv, f->r,
                              "kuda_pollset_poll()");
#else /* KUDA_FILES_AS_SOCKETS */
                /* Yuck... I'd really like to wait until I can read
                 * or write, but instead I have to sleep and try again
                 */
                kuda_sleep(kuda_time_from_msec(100));
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_TRACE6, 0, f->r, "kuda_sleep()");
#endif /* KUDA_FILES_AS_SOCKETS */
            }
            else if (rv != KUDA_SUCCESS) {
                return rv;
            }
        }
    } while (bytes_written < len);
    return rv;
}

/* ef_unified_filter:
 *
 * runs the bucket brigade bb through the filter and puts the result into
 * bb, dropping the previous content of bb (the input)
 */

static int ef_unified_filter(clhy_filter_t *f, kuda_bucket_brigade *bb)
{
    request_rec *r = f->r;
    conn_rec *c = r->connection;
    ef_ctx_t *ctx = f->ctx;
    kuda_bucket *b;
    kuda_size_t len;
    const char *data;
    kuda_status_t rv;
    char buf[4096];
    kuda_bucket *eos = NULL;
    kuda_bucket_brigade *bb_tmp;

    bb_tmp = kuda_brigade_create(r->pool, c->bucket_alloc);

    for (b = KUDA_BRIGADE_FIRST(bb);
         b != KUDA_BRIGADE_SENTINEL(bb);
         b = KUDA_BUCKET_NEXT(b))
    {
        if (KUDA_BUCKET_IS_EOS(b)) {
            eos = b;
            break;
        }
        /* Avoid the error buckets interference issued by other cAPIs */
        if (CLHY_BUCKET_IS_ERROR(b)) {
            kuda_bucket *cpy;
            kuda_bucket_copy(b, &cpy);
            KUDA_BRIGADE_INSERT_TAIL(bb_tmp, cpy);
            break;
        }

        rv = kuda_bucket_read(b, &data, &len, KUDA_BLOCK_READ);
        if (rv != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01463) "kuda_bucket_read()");
            return rv;
        }

        /* Good cast, we just tested len isn't negative */
        if (len > 0 &&
            (rv = pass_data_to_filter(f, data, (kuda_size_t)len, bb_tmp))
                != KUDA_SUCCESS) {
            return rv;
        }
    }

    kuda_brigade_cleanup(bb);
    KUDA_BRIGADE_CONCAT(bb, bb_tmp);
    kuda_brigade_destroy(bb_tmp);

    if (eos) {
        /* close the child's stdin to signal that no more data is coming;
         * that will cause the child to finish generating output
         */
        if ((rv = kuda_file_close(ctx->proc->in)) != KUDA_SUCCESS) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01464)
                          "kuda_file_close(child input)");
            return rv;
        }
        /* since we've seen eos and closed the child's stdin, set the proper pipe
         * timeout; we don't care if we don't return from kuda_file_read() for a while...
         */
        rv = kuda_file_pipe_timeout_set(ctx->proc->out,
                                       r->server->timeout);
        if (rv) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01465)
                          "kuda_file_pipe_timeout_set(child output)");
            return rv;
        }
    }

    do {
        int lvl = CLHYLOG_TRACE6;
        len = sizeof(buf);
        rv = kuda_file_read(ctx->proc->out, buf, &len);
        if (rv && !KUDA_STATUS_IS_EOF(rv) && !KUDA_STATUS_IS_EAGAIN(rv))
            lvl = CLHYLOG_ERR;
        clhy_log_rerror(CLHYLOG_MARK, lvl, rv, r, CLHYLOGNO(01466)
                      "kuda_file_read(child output), len %" KUDA_SIZE_T_FMT,
                      !rv ? len : -1);
        if (KUDA_STATUS_IS_EAGAIN(rv)) {
            if (eos) {
                /* should not occur, because we have an KUDA timeout in place */
                CLHY_DEBUG_ASSERT(1 != 1);
            }
            return KUDA_SUCCESS;
        }

        if (rv == KUDA_SUCCESS) {
            b = kuda_bucket_heap_create(buf, len, NULL, c->bucket_alloc);
            KUDA_BRIGADE_INSERT_TAIL(bb, b);
        }
    } while (rv == KUDA_SUCCESS);

    if (!KUDA_STATUS_IS_EOF(rv)) {
        return rv;
    }

    if (eos) {
        b = kuda_bucket_eos_create(c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, b);
        ctx->hit_eos = 1;
    }

    return KUDA_SUCCESS;
}

static kuda_status_t ef_output_filter(clhy_filter_t *f, kuda_bucket_brigade *bb)
{
    request_rec *r = f->r;
    ef_ctx_t *ctx = f->ctx;
    kuda_status_t rv;

    if (!ctx) {
        if ((rv = init_filter_instance(f)) != KUDA_SUCCESS) {
            ctx = f->ctx;
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01467)
                          "can't initialise output filter %s: %s",
                          f->frec->name,
                          (ctx->dc->onfail == 1) ? "removing" : "aborting");
            clhy_remove_output_filter(f);
            if (ctx->dc->onfail == 1) {
                return clhy_pass_brigade(f->next, bb);
            }
            else {
                kuda_bucket *e;
                f->r->status_line = "500 Internal Server Error";

                kuda_brigade_cleanup(bb);
                e = clhy_bucket_error_create(HTTP_INTERNAL_SERVER_ERROR,
                                           NULL, r->pool,
                                           f->c->bucket_alloc);
                KUDA_BRIGADE_INSERT_TAIL(bb, e);
                e = kuda_bucket_eos_create(f->c->bucket_alloc);
                KUDA_BRIGADE_INSERT_TAIL(bb, e);
                clhy_pass_brigade(f->next, bb);
                return CLHY_FILTER_ERROR;
            }
        }
        ctx = f->ctx;
    }
    if (ctx->noop) {
        clhy_remove_output_filter(f);
        return clhy_pass_brigade(f->next, bb);
    }

    rv = ef_unified_filter(f, bb);
    if (rv != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01468)
                      "ef_unified_filter() failed");
    }

    if ((rv = clhy_pass_brigade(f->next, bb)) != KUDA_SUCCESS) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, r, CLHYLOGNO(01469)
                      "clhy_pass_brigade() failed");
    }
    return rv;
}

static kuda_status_t ef_input_filter(clhy_filter_t *f, kuda_bucket_brigade *bb,
                                    clhy_input_mode_t mode, kuda_read_type_e block,
                                    kuda_off_t readbytes)
{
    ef_ctx_t *ctx = f->ctx;
    kuda_status_t rv;

    /* just get out of the way of things we don't want. */
    if (mode != CLHY_MODE_READBYTES) {
        return clhy_get_brigade(f->next, bb, mode, block, readbytes);
    }

    if (!ctx) {
        if ((rv = init_filter_instance(f)) != KUDA_SUCCESS) {
            ctx = f->ctx;
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, f->r, CLHYLOGNO(01470)
                          "can't initialise input filter %s: %s",
                          f->frec->name,
                          (ctx->dc->onfail == 1) ? "removing" : "aborting");
            clhy_remove_input_filter(f);
            if (ctx->dc->onfail == 1) {
                return clhy_get_brigade(f->next, bb, mode, block, readbytes);
            }
            else {
                f->r->status = HTTP_INTERNAL_SERVER_ERROR;
                return HTTP_INTERNAL_SERVER_ERROR;
            }
        }
        ctx = f->ctx;
    }

    if (ctx->hit_eos) {
        /* Match behaviour of HTTP_IN if filter is re-invoked after
         * hitting EOS: give back another EOS. */
        kuda_bucket *e = kuda_bucket_eos_create(f->c->bucket_alloc);
        KUDA_BRIGADE_INSERT_TAIL(bb, e);
        return KUDA_SUCCESS;
    }

    if (ctx->noop) {
        clhy_remove_input_filter(f);
        return clhy_get_brigade(f->next, bb, mode, block, readbytes);
    }

    rv = clhy_get_brigade(f->next, bb, mode, block, readbytes);
    if (rv != KUDA_SUCCESS) {
        return rv;
    }

    rv = ef_unified_filter(f, bb);
    return rv;
}

CLHY_DECLARE_CAPI(ext_filter) =
{
    STANDARD16_CAPI_STUFF,
    create_ef_dir_conf,
    merge_ef_dir_conf,
    create_ef_server_conf,
    NULL,
    cmds,
    register_hooks
};

