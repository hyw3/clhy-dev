/*      Copyright (c) 2007-11, WebThing Ltd
 *      
 *
 * The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if defined(WIN32)
#define XML2ENC_DECLARE_EXPORT
#endif

#include <ctype.h>

/* libxml2 includes unicode/[...].h files which uses C++ comments */
#if defined(__GNUC__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)
#pragma GCC diagnostic push
#pragma GCC diagnostic warning "-Wcomment"
#endif
#elif defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic warning "-Wcomment"
#endif

/* libxml2 */
#include <libxml/encoding.h>

#if defined(__GNUC__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)
#pragma GCC diagnostic pop
#endif
#elif defined(__clang__)
#pragma clang diagnostic pop
#endif

#include "http_protocol.h"
#include "http_config.h"
#include "http_log.h"
#include "kuda_strings.h"
#include "kuda_xlate.h"

#include "kuda_optional.h"
#include "capi_xml2enc.h"

cAPI CLHY_CAPI_DECLARE_DATA xml2enc_capi;

#define BUFLEN 8192
#define BUF_MIN 4096
#define KUDA_BRIGADE_DO(b,bb) for (b = KUDA_BRIGADE_FIRST(bb); \
                                   b != KUDA_BRIGADE_SENTINEL(bb); \
                                   b = KUDA_BUCKET_NEXT(b))

#define ENC_INITIALISED 0x100
#define ENC_SEEN_EOS 0x200
#define ENC_SKIPTO ENCIO_SKIPTO

#define HAVE_ENCODING(enc) \
        (((enc)!=XML_CHAR_ENCODING_NONE)&&((enc)!=XML_CHAR_ENCODING_ERROR))

/*
 * XXX: Check all those clhy_assert()s ans replace those that should not happen
 * XXX: with CLHY_DEBUG_ASSERT and those that may happen with proper error
 * XXX: handling.
 */
typedef struct {
    xmlCharEncoding xml2enc;
    char* buf;
    kuda_size_t bytes;
    kuda_xlate_t* convset;
    unsigned int flags;
    kuda_off_t bblen;
    kuda_bucket_brigade* bbnext;
    kuda_bucket_brigade* bbsave;
    const char* encoding;
} xml2ctx;

typedef struct {
    const char* default_charset;
    xmlCharEncoding default_encoding;
    kuda_array_header_t* skipto;
} xml2cfg;

typedef struct {
    const char* val;
} tattr;

static clhy_regex_t* seek_meta_ctype;
static clhy_regex_t* seek_charset;

static kuda_status_t xml2enc_filter(request_rec* r, const char* enc,
                                   unsigned int mode)
{
    /* set up a ready-initialised ctx to convert to enc, and insert filter */
    kuda_xlate_t* convset; 
    kuda_status_t rv;
    unsigned int flags = (mode ^ ENCIO);
    if ((mode & ENCIO) == ENCIO_OUTPUT) {
        rv = kuda_xlate_open(&convset, enc, "UTF-8", r->pool);
        flags |= ENC_INITIALISED;
    }
    else if ((mode & ENCIO) == ENCIO_INPUT) {
        rv = kuda_xlate_open(&convset, "UTF-8", enc, r->pool);
        flags |= ENC_INITIALISED;
    }
    else if ((mode & ENCIO) == ENCIO_INPUT_CHECKS) {
        convset = NULL;
        rv = KUDA_SUCCESS; /* we'll initialise later by sniffing */
    }
    else {
        rv = KUDA_EGENERAL;
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01426)
                      "xml2enc: bad mode %x", mode);
    }
    if (rv == KUDA_SUCCESS) {
        xml2ctx* ctx = kuda_pcalloc(r->pool, sizeof(xml2ctx));
        ctx->flags = flags;
        if (flags & ENC_INITIALISED) {
            ctx->convset = convset;
            ctx->bblen = BUFLEN;
            ctx->buf = kuda_palloc(r->pool, (kuda_size_t)ctx->bblen);
        }
        clhy_add_output_filter("xml2enc", ctx, r, r->connection);
    }
    else {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, r, CLHYLOGNO(01427)
                      "xml2enc: Charset %s not supported.", enc) ;
    }
    return rv;
}

/* This needs to operate only when we're using htmlParser */
/* Different cAPIs may apply different rules here.  Ho, hum.  */
static void fix_skipto(request_rec* r, xml2ctx* ctx)
{
    kuda_status_t rv;
    xml2cfg* cfg = clhy_get_capi_config(r->per_dir_config, &xml2enc_capi);
    if ((cfg->skipto != NULL) && (ctx->flags & ENC_SKIPTO)) {
        int found = 0;
        char* p = clhy_strchr(ctx->buf, '<');
        tattr* starts = (tattr*) cfg->skipto->elts;
        while (!found && p && *p) {
            int i;
            for (i = 0; i < cfg->skipto->nelts; ++i) {
                if (!strncasecmp(p+1, starts[i].val, strlen(starts[i].val))) {
                    /* found a starting element. Strip all that comes before. */
                    kuda_bucket* b;
                    kuda_bucket* bstart;
                    rv = kuda_brigade_partition(ctx->bbsave, (p-ctx->buf),
                                               &bstart);
                    clhy_assert(rv == KUDA_SUCCESS);
                    while (b = KUDA_BRIGADE_FIRST(ctx->bbsave), b != bstart) {
                        kuda_bucket_delete(b);
                    }
                    ctx->bytes -= (p-ctx->buf);
                    ctx->buf = p ;
                    found = 1;
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01428)
                                  "Skipped to first <%s> element",
                                  starts[i].val) ;
                    break;
                }
            }
            p = clhy_strchr(p+1, '<');
        }
        if (p == NULL) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01429)
                          "Failed to find start of recognised HTML!");
        }
    }
}
static void sniff_encoding(request_rec* r, xml2ctx* ctx)
{
    xml2cfg* cfg = NULL; /* initialise to shut compiler warnings up */
    char* p ;
    kuda_bucket* cutb;
    kuda_bucket* cute;
    kuda_bucket* b;
    clhy_regmatch_t match[2] ;
    kuda_status_t rv;
    const char* ctype = r->content_type;

    if (ctype) {
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01430)
                      "Content-Type is %s", ctype) ;

        /* If we've got it in the HTTP headers, there's nothing to do */
        if (ctype && (p = clhy_strcasestr(ctype, "charset=") , p != NULL)) {
            p += 8 ;
            if (ctx->encoding = kuda_pstrndup(r->pool, p, strcspn(p, " ;") ),
                ctx->encoding) {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(01431)
                              "Got charset %s from HTTP headers", ctx->encoding) ;
                ctx->xml2enc = xmlParseCharEncoding(ctx->encoding);
            }
        }
    }
  
    /* to sniff, first we look for BOM */
    if (ctx->xml2enc == XML_CHAR_ENCODING_NONE) {
        ctx->xml2enc = xmlDetectCharEncoding((const xmlChar*)ctx->buf,
                                             ctx->bytes); 
        if (HAVE_ENCODING(ctx->xml2enc)) {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(01432)
                          "Got charset from XML rules.") ;
            ctx->encoding = xmlGetCharEncodingName(ctx->xml2enc);
        }
    }

    /* If none of the above, look for a META-thingey */
    /* also we're probably about to invalidate it, so we remove it. */
    if (clhy_regexec(seek_meta_ctype, ctx->buf, 1, match, 0) == 0 ) {
        /* get markers on the start and end of the match */
        rv = kuda_brigade_partition(ctx->bbsave, match[0].rm_eo, &cute);
        clhy_assert(rv == KUDA_SUCCESS);
        rv = kuda_brigade_partition(ctx->bbsave, match[0].rm_so, &cutb);
        clhy_assert(rv == KUDA_SUCCESS);
        /* now set length of useful buf for start-of-data hooks */
        ctx->bytes = match[0].rm_so;
        if (ctx->encoding == NULL) {
            p = kuda_pstrndup(r->pool, ctx->buf + match[0].rm_so,
                             match[0].rm_eo - match[0].rm_so) ;
            if (clhy_regexec(seek_charset, p, 2, match, 0) == 0) {
                if (ctx->encoding = kuda_pstrndup(r->pool, p+match[1].rm_so,
                                               match[1].rm_eo - match[1].rm_so),
                    ctx->encoding) {
                    ctx->xml2enc = xmlParseCharEncoding(ctx->encoding);
                    if (HAVE_ENCODING(ctx->xml2enc))
                        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_INFO, 0, r, CLHYLOGNO(01433)
                                      "Got charset %s from HTML META", ctx->encoding) ;
                }
            }
        }

        /* cut out the <meta> we're invalidating */
        while (cutb != cute) {
            b = KUDA_BUCKET_NEXT(cutb);
            kuda_bucket_delete(cutb);
            cutb = b;
        }
        /* and leave a string */
        ctx->buf[ctx->bytes] = 0;
    }

    /* either it's set to something we found or it's still the default */
    /* Aaargh!  libxml2 has undocumented <META-crap> support.  So this fails
     * if metafix is not active.  Have to make it conditional.
     *
     * No, that means no-metafix breaks things.  Deal immediately with
     * this particular instance of metafix.
     */
    if (!HAVE_ENCODING(ctx->xml2enc)) {
        cfg = clhy_get_capi_config(r->per_dir_config, &xml2enc_capi);
        if (!ctx->encoding) {
            ctx->encoding = cfg->default_charset?cfg->default_charset:"ISO-8859-1";
        }
        /* Unsupported charset. Can we get (iconv) support through kuda_xlate? */
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, r, CLHYLOGNO(01434)
                      "Charset %s not supported by libxml2; trying kuda_xlate",
                      ctx->encoding);
        if (kuda_xlate_open(&ctx->convset, "UTF-8", ctx->encoding, r->pool)
            == KUDA_SUCCESS) {
            ctx->xml2enc = XML_CHAR_ENCODING_UTF8 ;
        } else {
            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, r, CLHYLOGNO(01435)
                          "Charset %s not supported.  Consider aliasing it?",
                          ctx->encoding) ;
        }
    }

    if (!HAVE_ENCODING(ctx->xml2enc)) {
        /* Use configuration default as a last resort */
        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_WARNING, 0, r, CLHYLOGNO(01436)
                  "No usable charset information; using configuration default");
        ctx->xml2enc = (cfg->default_encoding == XML_CHAR_ENCODING_NONE)
                        ? XML_CHAR_ENCODING_8859_1 : cfg->default_encoding ;
    }
    if (ctype && ctx->encoding) {
        if (clhy_regexec(seek_charset, ctype, 2, match, 0)) {
            r->content_type = kuda_pstrcat(r->pool, ctype, ";charset=utf-8",
                                          NULL);
        } else {
            char* str = kuda_palloc(r->pool, strlen(r->content_type) + 13
                                   - (match[0].rm_eo - match[0].rm_so) + 1);
            memcpy(str, r->content_type, match[1].rm_so);
            memcpy(str + match[1].rm_so, "utf-8", 5);
            strcpy(str + match[1].rm_so + 5, r->content_type+match[1].rm_eo);
            r->content_type = str;
        }
    }
}

static kuda_status_t xml2enc_filter_init(clhy_filter_t* f)
{
    xml2ctx* ctx;
    if (!f->ctx) {
        xml2cfg* cfg = clhy_get_capi_config(f->r->per_dir_config,
                                            &xml2enc_capi);
        f->ctx = ctx = kuda_pcalloc(f->r->pool, sizeof(xml2ctx));
        ctx->xml2enc = XML_CHAR_ENCODING_NONE;
        if (cfg->skipto != NULL) {
            ctx->flags |= ENC_SKIPTO;
        }
    }
    return KUDA_SUCCESS;
}
static kuda_status_t xml2enc_ffunc(clhy_filter_t* f, kuda_bucket_brigade* bb)
{
    xml2ctx* ctx = f->ctx;
    kuda_status_t rv;
    kuda_bucket* b;
    kuda_bucket* bstart;
    kuda_size_t insz = 0;
    int pending_meta = 0;
    char *ctype;
    char *p;

    if (!ctx || !f->r->content_type) {
        /* log error about configuring this */
        clhy_remove_output_filter(f);
        return clhy_pass_brigade(f->next, bb) ;
    }

    ctype = kuda_pstrdup(f->r->pool, f->r->content_type);
    for (p = ctype; *p; ++p)
        if (isupper(*p))
            *p = tolower(*p);

    /* only act if starts-with "text/" or contains "xml" */
    if (strncmp(ctype, "text/", 5) && !strstr(ctype, "xml"))  {
        clhy_remove_output_filter(f);
        return clhy_pass_brigade(f->next, bb) ;
    }

    if (ctx->bbsave == NULL) {
        ctx->bbsave = kuda_brigade_create(f->r->pool,
                                         f->r->connection->bucket_alloc);
    }
    /* append to any data left over from last time */
    KUDA_BRIGADE_CONCAT(ctx->bbsave, bb);

    if (!(ctx->flags & ENC_INITIALISED)) {
        /* some kind of initialisation required */
        /* Turn all this off when post-processing */

        /* if we don't have enough data to sniff but more's to come, wait */
        kuda_brigade_length(ctx->bbsave, 0, &ctx->bblen);
        if ((ctx->bblen < BUF_MIN) && (ctx->bblen != -1)) {
            KUDA_BRIGADE_DO(b, ctx->bbsave) {
                if (KUDA_BUCKET_IS_EOS(b)) {
                    ctx->flags |= ENC_SEEN_EOS;
                    break;
                }
            }
            if (!(ctx->flags & ENC_SEEN_EOS)) {
                /* not enough data to sniff.  Wait for more */
                KUDA_BRIGADE_DO(b, ctx->bbsave) {
                    rv = kuda_bucket_setaside(b, f->r->pool);
                    clhy_assert(rv == KUDA_SUCCESS);
                }
                return KUDA_SUCCESS;
            }
        }
        if (ctx->bblen == -1) {
            ctx->bblen = BUFLEN-1;
        }

        /* flatten it into a NULL-terminated string */
        ctx->buf = kuda_palloc(f->r->pool, (kuda_size_t)(ctx->bblen+1));
        ctx->bytes = (kuda_size_t)ctx->bblen;
        rv = kuda_brigade_flatten(ctx->bbsave, ctx->buf, &ctx->bytes);
        clhy_assert(rv == KUDA_SUCCESS);
        ctx->buf[ctx->bytes] = 0;
        sniff_encoding(f->r, ctx);

        /* FIXME: hook here for rewriting start-of-data? */
        /* nah, we only have one action here - call it inline */
        fix_skipto(f->r, ctx);

        /* we might change the Content-Length, so let's force its re-calculation */
        kuda_table_unset(f->r->headers_out, "Content-Length");

        /* consume the data we just sniffed */
        /* we need to omit any <meta> we just invalidated */
        ctx->flags |= ENC_INITIALISED;
        clhy_set_capi_config(f->r->request_config, &xml2enc_capi, ctx);
    }
    if (ctx->bbnext == NULL) {
        ctx->bbnext = kuda_brigade_create(f->r->pool,
                                         f->r->connection->bucket_alloc);
    }

    if (!ctx->convset) {
        rv = clhy_pass_brigade(f->next, ctx->bbsave);
        kuda_brigade_cleanup(ctx->bbsave);
        clhy_remove_output_filter(f);
        return rv;
    }
    /* move the data back to bb */
    KUDA_BRIGADE_CONCAT(bb, ctx->bbsave);

    while (!KUDA_BRIGADE_EMPTY(bb)) {
        b = KUDA_BRIGADE_FIRST(bb);
        ctx->bytes = 0;
        if (KUDA_BUCKET_IS_METADATA(b)) {
            KUDA_BUCKET_REMOVE(b);
            KUDA_BRIGADE_INSERT_TAIL(ctx->bbnext, b);
            /* Besides FLUSH, aggregate meta buckets to send them at
             * once below. This resource filter is over on EOS.
             */
            pending_meta = 1;
            if (KUDA_BUCKET_IS_EOS(b)) {
                clhy_remove_output_filter(f);
                KUDA_BRIGADE_CONCAT(ctx->bbnext, bb);
            }
            else if (!KUDA_BUCKET_IS_FLUSH(b)) {
                continue;
            }
        }
        if (pending_meta) {
            pending_meta = 0;
            /* passing meta bucket down the chain */
            rv = clhy_pass_brigade(f->next, ctx->bbnext);
            kuda_brigade_cleanup(ctx->bbnext);
            if (rv != KUDA_SUCCESS) {
                return rv;
            }
            continue;
        }
        /* data bucket */
        {
            char* buf;
            kuda_size_t bytes = 0;
            char fixbuf[BUFLEN];
            kuda_bucket* bdestroy = NULL;
            if (insz > 0) { /* we have dangling data.  Flatten it. */
                buf = fixbuf;
                bytes = BUFLEN;
                rv = kuda_brigade_flatten(bb, buf, &bytes);
                clhy_assert(rv == KUDA_SUCCESS);
                if (bytes == insz) {
                    /* this is only what we've already tried to convert.
                     * The brigade is exhausted.
                     * Save remaining data for next time round
                     */
          
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, f->r, CLHYLOGNO(01437)
                                  "xml2enc: Setting aside %" KUDA_SIZE_T_FMT
                                  " unconverted bytes", bytes);
                    rv = clhy_fflush(f->next, ctx->bbnext);
                    KUDA_BRIGADE_CONCAT(ctx->bbsave, bb);
                    KUDA_BRIGADE_DO(b, ctx->bbsave) {
                        clhy_assert(kuda_bucket_setaside(b, f->r->pool)
                                  == KUDA_SUCCESS);
                    }
                    return rv;
                }
                /* remove the data we've just read */
                rv = kuda_brigade_partition(bb, bytes, &bstart);
                while (b = KUDA_BRIGADE_FIRST(bb), b != bstart) {
                    kuda_bucket_delete(b);
                }
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, f->r, CLHYLOGNO(01438)
                              "xml2enc: consuming %" KUDA_SIZE_T_FMT
                              " bytes flattened", bytes);
            }
            else {
                rv = kuda_bucket_read(b, (const char**)&buf, &bytes,
                                     KUDA_BLOCK_READ);
                KUDA_BUCKET_REMOVE(b);
                bdestroy = b;  /* can't destroy until finished with the data */
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, f->r, CLHYLOGNO(01439)
                              "xml2enc: consuming %" KUDA_SIZE_T_FMT
                              " bytes from bucket", bytes);
            }
            /* OK, we've got some input we can use in [buf,bytes] */
            if (rv == KUDA_SUCCESS) {
                kuda_size_t consumed;
                xml2enc_run_preprocess(f, &buf, &bytes);
                consumed = insz = bytes;
                while (insz > 0) {
                    kuda_status_t rv2;
                    if (ctx->bytes == ctx->bblen) {
                        /* nothing was converted last time!
                         * break out of this loop! 
                         */
                        b = kuda_bucket_transient_create(buf+(bytes - insz), insz,
                                                        bb->bucket_alloc);
                        KUDA_BRIGADE_INSERT_HEAD(bb, b);
                        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, f->r, CLHYLOGNO(01440)
                                      "xml2enc: reinserting %" KUDA_SIZE_T_FMT
                                      " unconsumed bytes from bucket", insz);
                        break;
                    }
                    ctx->bytes = (kuda_size_t)ctx->bblen;
                    rv = kuda_xlate_conv_buffer(ctx->convset, buf+(bytes - insz),
                                               &insz, ctx->buf, &ctx->bytes);
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, f->r, CLHYLOGNO(01441)
                                  "xml2enc: converted %" KUDA_SIZE_T_FMT
                                  "/%" KUDA_OFF_T_FMT " bytes", consumed - insz,
                                  ctx->bblen - ctx->bytes);
                    consumed = insz;
                    rv2 = clhy_fwrite(f->next, ctx->bbnext, ctx->buf,
                                    (kuda_size_t)ctx->bblen - ctx->bytes);
                    if (rv2 != KUDA_SUCCESS) {
                        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv2, f->r, CLHYLOGNO(01442)
                                      "clhy_fwrite failed");
                        return rv2;
                    }
                    switch (rv) {
                    case KUDA_SUCCESS:
                        continue;
                    case KUDA_EINCOMPLETE:
                        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, 0, f->r, CLHYLOGNO(01443)
                                      "INCOMPLETE");
                        continue;     /* If outbuf too small, go round again.
                                       * If it was inbuf, we'll break out when
                                       * we test ctx->bytes == ctx->bblen
                                       */
                    case KUDA_EINVAL: /* try skipping one bad byte */
                        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, 0, f->r, CLHYLOGNO(01444)
                                   "Skipping invalid byte(s) in input stream!");
                        --insz;
                        continue;
                    default:
                        /* Erk!  What's this?
                         * Bail out, flush, and hope to eat the buf raw
                         */
                        clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, f->r, CLHYLOGNO(01445)
                                      "Failed to convert input; trying it raw") ;
                        ctx->convset = NULL;
                        rv = clhy_fflush(f->next, ctx->bbnext);
                        if (rv != KUDA_SUCCESS)
                            clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, rv, f->r, CLHYLOGNO(01446)
                                          "clhy_fflush failed");
                        kuda_brigade_cleanup(ctx->bbnext);
                    }
                }
            } else {
                clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_ERR, rv, f->r, CLHYLOGNO(01447)
                              "xml2enc: error reading data") ;
            }
            if (bdestroy)
                kuda_bucket_destroy(bdestroy);
            if (rv != KUDA_SUCCESS)
                return rv;
        }
    }
    if (pending_meta) {
        /* passing pending meta bucket down the chain before leaving */
        rv = clhy_pass_brigade(f->next, ctx->bbnext);
        kuda_brigade_cleanup(ctx->bbnext);
        if (rv != KUDA_SUCCESS) {
            return rv;
        }
    }

    return KUDA_SUCCESS;
}

static kuda_status_t xml2enc_charset(request_rec* r, xmlCharEncoding* encp,
                                    const char** encoding)
{
    xml2ctx* ctx = clhy_get_capi_config(r->request_config, &xml2enc_capi);
    if (!ctx || !(ctx->flags & ENC_INITIALISED)) {
        return KUDA_EAGAIN;
    }
    *encp = ctx->xml2enc;
    *encoding = ctx->encoding;
    return HAVE_ENCODING(ctx->xml2enc) ? KUDA_SUCCESS : KUDA_EGENERAL;
}

#define PROTO_FLAGS CLHY_FILTER_PROTO_CHANGE|CLHY_FILTER_PROTO_CHANGE_LENGTH
static void xml2enc_hooks(kuda_pool_t* pool)
{
    clhy_register_output_filter_protocol("xml2enc", xml2enc_ffunc,
                                       xml2enc_filter_init,
                                       CLHY_FTYPE_RESOURCE, PROTO_FLAGS);
    KUDA_REGISTER_OPTIONAL_FN(xml2enc_filter);
    KUDA_REGISTER_OPTIONAL_FN(xml2enc_charset);
    seek_meta_ctype = clhy_pregcomp(pool,
                       "(<meta[^>]*http-equiv[ \t\r\n='\"]*content-type[^>]*>)",
                                  CLHY_REG_EXTENDED|CLHY_REG_ICASE) ;
    seek_charset = clhy_pregcomp(pool, "charset=([A-Za-z0-9_-]+)",
                               CLHY_REG_EXTENDED|CLHY_REG_ICASE) ;
}
static const char* set_alias(cmd_parms* cmd, void* CFG,
                             const char* charset, const char* alias)
{
    const char* errmsg = clhy_check_cmd_context(cmd, GLOBAL_ONLY);
    if (errmsg != NULL)
        return errmsg ;
    else if (xmlAddEncodingAlias(charset, alias) == 0)
        return NULL;
    else
        return "Error setting charset alias";
}

static const char* set_default(cmd_parms* cmd, void* CFG, const char* charset)
{
    xml2cfg* cfg = CFG;
    cfg->default_charset = charset;
    cfg->default_encoding = xmlParseCharEncoding(charset);
    switch(cfg->default_encoding) {
    case XML_CHAR_ENCODING_NONE:
        return "Default charset not found";
    case XML_CHAR_ENCODING_ERROR:
        return "Invalid or unsupported default charset";
    default:
        return NULL;
    }
}
static const char* set_skipto(cmd_parms* cmd, void* CFG, const char* arg)
{
    tattr* attr;
    xml2cfg* cfg = CFG;
    if (cfg->skipto == NULL)
        cfg->skipto = kuda_array_make(cmd->pool, 4, sizeof(tattr));
    attr = kuda_array_push(cfg->skipto) ;
    attr->val = arg;
    return NULL;
}

static const command_rec xml2enc_cmds[] = {
    CLHY_INIT_TAKE1("xml2EncDefault", set_default, NULL, OR_ALL,
                  "Usage: xml2EncDefault charset"),
    CLHY_INIT_ITERATE2("xml2EncAlias", set_alias, NULL, RSRC_CONF,
                     "EncodingAlias charset alias [more aliases]"),
    CLHY_INIT_ITERATE("xml2StartParse", set_skipto, NULL, OR_ALL,
                    "Ignore anything in front of the first of these elements"),
    { NULL }
};
static void* xml2enc_config(kuda_pool_t* pool, char* x)
{
    xml2cfg* ret = kuda_pcalloc(pool, sizeof(xml2cfg));
    ret->default_encoding = XML_CHAR_ENCODING_NONE ;
    return ret;
}

static void* xml2enc_merge(kuda_pool_t* pool, void* BASE, void* ADD)
{
    xml2cfg* base = BASE;
    xml2cfg* add = ADD;
    xml2cfg* ret = kuda_pcalloc(pool, sizeof(xml2cfg));
    ret->default_encoding = (add->default_encoding == XML_CHAR_ENCODING_NONE)
                          ? base->default_encoding : add->default_encoding ;
    ret->default_charset = add->default_charset
                         ? add->default_charset : base->default_charset;
    ret->skipto = add->skipto ? add->skipto : base->skipto;
    return ret;
}

CLHY_DECLARE_CAPI(xml2enc) = {
    STANDARD16_CAPI_STUFF,
    xml2enc_config,
    xml2enc_merge,
    NULL,
    NULL,
    xml2enc_cmds,
    xml2enc_hooks
};

KUDA_IMPLEMENT_OPTIONAL_HOOK_RUN_ALL(xml2enc, XML2ENC, int, preprocess,
                      (clhy_filter_t *f, char** bufp, kuda_size_t* bytesp),
                      (f, bufp, bytesp), OK, DECLINED)

