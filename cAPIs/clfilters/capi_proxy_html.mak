# Microsoft Developer Studio Generated NMAKE File, Based on capi_proxy_html.dsp
!IF "$(CFG)" == ""
CFG=capi_proxy_html - Win32 Release
!MESSAGE No configuration specified. Defaulting to capi_proxy_html - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "capi_proxy_html - Win32 Release" && "$(CFG)" != "capi_proxy_html - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "capi_proxy_html.mak" CFG="capi_proxy_html - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "capi_proxy_html - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "capi_proxy_html - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "capi_proxy_html - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_proxy_html.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Release" "libkudadelman - Win32 Release" "libkuda - Win32 Release" "$(OUTDIR)\capi_proxy_html.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudadelman - Win32 ReleaseCLEAN" "libwwhy - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\wwhy.res"
	-@erase "$(INTDIR)\capi_proxy_html.obj"
	-@erase "$(INTDIR)\capi_proxy_html_src.idb"
	-@erase "$(INTDIR)\capi_proxy_html_src.pdb"
	-@erase "$(OUTDIR)\capi_proxy_html.exp"
	-@erase "$(OUTDIR)\capi_proxy_html.lib"
	-@erase "$(OUTDIR)\capi_proxy_html.pdb"
	-@erase "$(OUTDIR)\capi_proxy_html.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/libxml2/include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_proxy_html_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\wwhy.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../kudelrunsrc/kuda-delman/include" /i "../../kudelrunsrc/libxml2/include" /d "NDEBUG" /d BIN_NAME="capi_proxy_html.so" /d LONG_NAME="proxy_html_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_proxy_html.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib libxml2.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_proxy_html.pdb" /debug /out:"$(OUTDIR)\capi_proxy_html.so" /implib:"$(OUTDIR)\capi_proxy_html.lib" /libpath:"../../kudelrunsrc/libxml2/win32/bin.msvc" /base:@..\..\platforms\win32\BaseAddr.ref,capi_proxy_html.so /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\capi_proxy_html.obj" \
	"$(INTDIR)\wwhy.res" \
	"..\..\kudelrunsrc\kuda\Release\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Release\libkudadelman-1.lib" \
	"..\..\Release\libwwhy.lib"

"$(OUTDIR)\capi_proxy_html.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\capi_proxy_html.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_proxy_html.so"
   if exist .\Release\capi_proxy_html.so.manifest mt.exe -manifest .\Release\capi_proxy_html.so.manifest -outputresource:.\Release\capi_proxy_html.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "capi_proxy_html - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_proxy_html.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Debug" "libkudadelman - Win32 Debug" "libkuda - Win32 Debug" "$(OUTDIR)\capi_proxy_html.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudadelman - Win32 DebugCLEAN" "libwwhy - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\wwhy.res"
	-@erase "$(INTDIR)\capi_proxy_html.obj"
	-@erase "$(INTDIR)\capi_proxy_html_src.idb"
	-@erase "$(INTDIR)\capi_proxy_html_src.pdb"
	-@erase "$(OUTDIR)\capi_proxy_html.exp"
	-@erase "$(OUTDIR)\capi_proxy_html.lib"
	-@erase "$(OUTDIR)\capi_proxy_html.pdb"
	-@erase "$(OUTDIR)\capi_proxy_html.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/libxml2/include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_proxy_html_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\wwhy.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../kudelrunsrc/kuda-delman/include" /i "../../kudelrunsrc/libxml2/include" /d "_DEBUG" /d BIN_NAME="capi_proxy_html.so" /d LONG_NAME="proxy_html_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_proxy_html.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib libxml2.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_proxy_html.pdb" /debug /out:"$(OUTDIR)\capi_proxy_html.so" /implib:"$(OUTDIR)\capi_proxy_html.lib" /libpath:"../../kudelrunsrc/libxml2/win32/bin.msvc" /base:@..\..\platforms\win32\BaseAddr.ref,capi_proxy_html.so 
LINK32_OBJS= \
	"$(INTDIR)\capi_proxy_html.obj" \
	"$(INTDIR)\wwhy.res" \
	"..\..\kudelrunsrc\kuda\Debug\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Debug\libkudadelman-1.lib" \
	"..\..\Debug\libwwhy.lib"

"$(OUTDIR)\capi_proxy_html.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\capi_proxy_html.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_proxy_html.so"
   if exist .\Debug\capi_proxy_html.so.manifest mt.exe -manifest .\Debug\capi_proxy_html.so.manifest -outputresource:.\Debug\capi_proxy_html.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("capi_proxy_html.dep")
!INCLUDE "capi_proxy_html.dep"
!ELSE 
!MESSAGE Warning: cannot find "capi_proxy_html.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "capi_proxy_html - Win32 Release" || "$(CFG)" == "capi_proxy_html - Win32 Debug"
SOURCE=..\..\build\win32\wwhy.rc

!IF  "$(CFG)" == "capi_proxy_html - Win32 Release"


"$(INTDIR)\wwhy.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\wwhy.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../kudelrunsrc/kuda-delman/include" /i "../../kudelrunsrc/libxml2/include" /i "../../build\win32" /d "NDEBUG" /d BIN_NAME="capi_proxy_html.so" /d LONG_NAME="proxy_html_capi for cLHy" $(SOURCE)


!ELSEIF  "$(CFG)" == "capi_proxy_html - Win32 Debug"


"$(INTDIR)\wwhy.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\wwhy.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../kudelrunsrc/kuda-delman/include" /i "../../kudelrunsrc/libxml2/include" /i "../../build\win32" /d "_DEBUG" /d BIN_NAME="capi_proxy_html.so" /d LONG_NAME="proxy_html_capi for cLHy" $(SOURCE)


!ENDIF 

SOURCE=.\capi_proxy_html.c

"$(INTDIR)\capi_proxy_html.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "capi_proxy_html - Win32 Release"

"libkuda - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\..\cAPIs\clfilters"

"libkuda - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clfilters"

!ELSEIF  "$(CFG)" == "capi_proxy_html - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\..\cAPIs\clfilters"

"libkuda - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clfilters"

!ENDIF 

!IF  "$(CFG)" == "capi_proxy_html - Win32 Release"

"libkudadelman - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" 
   cd "..\..\cAPIs\clfilters"

"libkudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clfilters"

!ELSEIF  "$(CFG)" == "capi_proxy_html - Win32 Debug"

"libkudadelman - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" 
   cd "..\..\cAPIs\clfilters"

"libkudadelman - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clfilters"

!ENDIF 

!IF  "$(CFG)" == "capi_proxy_html - Win32 Release"

"libwwhy - Win32 Release" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" 
   cd ".\cAPIs\clfilters"

"libwwhy - Win32 ReleaseCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" RECURSE=1 CLEAN 
   cd ".\cAPIs\clfilters"

!ELSEIF  "$(CFG)" == "capi_proxy_html - Win32 Debug"

"libwwhy - Win32 Debug" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" 
   cd ".\cAPIs\clfilters"

"libwwhy - Win32 DebugCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" RECURSE=1 CLEAN 
   cd ".\cAPIs\clfilters"

!ENDIF 


!ENDIF 

