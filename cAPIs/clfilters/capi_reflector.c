/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "kuda_strings.h"
#include "kuda_tables.h"

#include "wwhy.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_protocol.h"
#include "http_request.h"
#include "capi_core.h"

cAPI CLHY_CAPI_DECLARE_DATA reflector_capi;

typedef struct {
    kuda_table_t *headers;
} reflector_cfg;

static int header_do(void *dummy, const char *key, const char *value)
{
    request_rec *r = (request_rec *) dummy;
    const char *payload;

    payload = kuda_table_get(r->headers_in, key);
    if (payload) {
        kuda_table_setn(r->headers_out, value, payload);
    }

    return 1;
}

static int reflector_handler(request_rec * r)
{
    kuda_bucket_brigade *bbin, *bbout;
    reflector_cfg *conf;
    kuda_status_t status;

    if (strcmp(r->handler, "reflector")) {
        return DECLINED;
    }

    conf = (reflector_cfg *) clhy_get_capi_config(r->per_dir_config,
                                                  &reflector_capi);

    clhy_allow_methods(r, 1, "POST", "OPTIONS", NULL);

    if (r->method_number == M_OPTIONS) {
        return clhy_send_http_options(r);
    }

    else if (r->method_number == M_POST) {
        const char *content_length, *content_type;
        int seen_eos;

        /*
         * Sometimes we'll get in a state where the input handling has
         * detected an error where we want to drop the connection, so if
         * that's the case, don't read the data as that is what we're trying
         * to avoid.
         *
         * This function is also a no-op on a subrequest.
         */
        if (r->main || r->connection->keepalive == CLHY_CONN_CLOSE ||
            clhy_status_drops_connection(r->status)) {
            return OK;
        }

        /* copy headers from in to out if configured */
        kuda_table_do(header_do, r, conf->headers, NULL);

        /* last modified defaults to now, unless otherwise set on the way in */
        if (!kuda_table_get(r->headers_out, "Last-Modified")) {
            clhy_update_mtime(r, kuda_time_now());
            clhy_set_last_modified(r);
        }
        clhy_set_accept_ranges(r);

        /* reflect the content length, if present */
        if ((content_length = kuda_table_get(r->headers_in, "Content-Length"))) {
            kuda_off_t offset;

            kuda_strtoff(&offset, content_length, NULL, 10);
            clhy_set_content_length(r, offset);

        }

        /* reflect the content type, if present */
        if ((content_type = kuda_table_get(r->headers_in, "Content-Type"))) {

            clhy_set_content_type(r, content_type);

        }

        bbin = kuda_brigade_create(r->pool, r->connection->bucket_alloc);
        bbout = kuda_brigade_create(r->pool, r->connection->bucket_alloc);

        seen_eos = 0;
        do {
            kuda_bucket *bucket;

            status = clhy_get_brigade(r->input_filters, bbin, CLHY_MODE_READBYTES,
                                    KUDA_BLOCK_READ, HUGE_STRING_LEN);

            if (status != KUDA_SUCCESS) {
                kuda_brigade_destroy(bbin);
                return clhy_map_http_request_error(status, HTTP_BAD_REQUEST);
            }

            for (bucket = KUDA_BRIGADE_FIRST(bbin);
                 bucket != KUDA_BRIGADE_SENTINEL(bbin);
                 bucket = KUDA_BUCKET_NEXT(bucket)) {
                const char *data;
                kuda_size_t len;

                if (KUDA_BUCKET_IS_EOS(bucket)) {
                    seen_eos = 1;
                    break;
                }

                /* These are metadata buckets. */
                if (bucket->length == 0) {
                    continue;
                }

                /*
                 * We MUST read because in case we have an unknown-length
                 * bucket or one that morphs, we want to exhaust it.
                 */
                status = kuda_bucket_read(bucket, &data, &len, KUDA_BLOCK_READ);
                if (status != KUDA_SUCCESS) {
                    kuda_brigade_destroy(bbin);
                    return HTTP_BAD_REQUEST;
                }

                kuda_brigade_write(bbout, NULL, NULL, data, len);

                status = clhy_pass_brigade(r->output_filters, bbout);
                if (status != KUDA_SUCCESS) {
                    /* no way to know what type of error occurred */
                    clhy_log_rerror(CLHYLOG_MARK, CLHYLOG_DEBUG, status, r, CLHYLOGNO(01410)
                             "reflector_handler: clhy_pass_brigade returned %i",
                                  status);
                    return CLHY_FILTER_ERROR;
                }

            }

            kuda_brigade_cleanup(bbin);

        } while (!seen_eos);

        return OK;

    }

    else {
        return HTTP_METHOD_NOT_ALLOWED;
    }

}

static void *create_reflector_dir_config(kuda_pool_t * p, char *d)
{
    reflector_cfg *conf = kuda_pcalloc(p, sizeof(reflector_cfg));

    conf->headers = kuda_table_make(p, 8);

    return conf;
}

static void *merge_reflector_dir_config(kuda_pool_t * p, void *basev, void *addv)
{
    reflector_cfg *new = (reflector_cfg *) kuda_pcalloc(p,
            sizeof(reflector_cfg));
    reflector_cfg *add = (reflector_cfg *) addv;
    reflector_cfg *base = (reflector_cfg *) basev;

    new->headers = kuda_table_overlay(p, add->headers, base->headers);

    return new;
}

static const char *reflector_header(cmd_parms * cmd, void *dummy, const char *in,
        const char *out)
{
    reflector_cfg *cfg = (reflector_cfg *) dummy;

    kuda_table_addn(cfg->headers, in, out ? out : in);

    return NULL;
}

static void reflector_hooks(kuda_pool_t * p)
{
    clhy_hook_handler(reflector_handler, NULL, NULL, KUDA_HOOK_MIDDLE);
}

static const command_rec reflector_cmds[] = {
    CLHY_INIT_TAKE12("ReflectorHeader", reflector_header, NULL, OR_OPTIONS,
      "Header to reflect back in the response, with an optional new name."),
    {NULL}
};

CLHY_DECLARE_CAPI(reflector) = {
    STANDARD16_CAPI_STUFF,
    create_reflector_dir_config,
    merge_reflector_dir_config,
    NULL,
    NULL,
    reflector_cmds,
    reflector_hooks
};
