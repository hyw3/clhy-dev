/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _CAPI_RATELIMIT_H_
#define _CAPI_RATELIMIT_H_

/* Create a set of CLHY_RL_DECLARE(type), CLHY_RL_DECLARE_NONSTD(type) and
 * CLHY_RL_DECLARE_DATA with appropriate export and import tags for the platform
 */
#if !defined(WIN32)
#define CLHY_RL_DECLARE(type)            type
#define CLHY_RL_DECLARE_NONSTD(type)     type
#define CLHY_RL_DECLARE_DATA
#elif defined(CLHY_RL_DECLARE_STATIC)
#define CLHY_RL_DECLARE(type)            type __stdcall
#define CLHY_RL_DECLARE_NONSTD(type)     type
#define CLHY_RL_DECLARE_DATA
#elif defined(CLHY_RL_DECLARE_EXPORT)
#define CLHY_RL_DECLARE(type)            __declspec(dllexport) type __stdcall
#define CLHY_RL_DECLARE_NONSTD(type)     __declspec(dllexport) type
#define CLHY_RL_DECLARE_DATA             __declspec(dllexport)
#else
#define CLHY_RL_DECLARE(type)            __declspec(dllimport) type __stdcall
#define CLHY_RL_DECLARE_NONSTD(type)     __declspec(dllimport) type
#define CLHY_RL_DECLARE_DATA             __declspec(dllimport)
#endif

CLHY_RL_DECLARE_DATA extern const kuda_bucket_type_t clhy_rl_bucket_type_end;
CLHY_RL_DECLARE_DATA extern const kuda_bucket_type_t clhy_rl_bucket_type_start;

#define CLHY_RL_BUCKET_IS_END(e)         (e->type == &clhy_rl_bucket_type_end)
#define CLHY_RL_BUCKET_IS_START(e)         (e->type == &clhy_rl_bucket_type_start)

/* TODO: Make these Optional Functions, so that cAPI load order doesn't matter. */
CLHY_RL_DECLARE(kuda_bucket*) clhy_rl_end_create(kuda_bucket_alloc_t *list);
CLHY_RL_DECLARE(kuda_bucket*) clhy_rl_start_create(kuda_bucket_alloc_t *list);

#endif
