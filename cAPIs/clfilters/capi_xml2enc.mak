# Microsoft Developer Studio Generated NMAKE File, Based on capi_xml2enc.dsp
!IF "$(CFG)" == ""
CFG=capi_xml2enc - Win32 Release
!MESSAGE No configuration specified. Defaulting to capi_xml2enc - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "capi_xml2enc - Win32 Release" && "$(CFG)" != "capi_xml2enc - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "capi_xml2enc.mak" CFG="capi_xml2enc - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "capi_xml2enc - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "capi_xml2enc - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "capi_xml2enc - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_xml2enc.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Release" "libkudadelman - Win32 Release" "libkuda - Win32 Release" "$(OUTDIR)\capi_xml2enc.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudadelman - Win32 ReleaseCLEAN" "libwwhy - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\wwhy.res"
	-@erase "$(INTDIR)\capi_xml2enc.obj"
	-@erase "$(INTDIR)\capi_xml2enc_src.idb"
	-@erase "$(INTDIR)\capi_xml2enc_src.pdb"
	-@erase "$(OUTDIR)\capi_xml2enc.exp"
	-@erase "$(OUTDIR)\capi_xml2enc.lib"
	-@erase "$(OUTDIR)\capi_xml2enc.pdb"
	-@erase "$(OUTDIR)\capi_xml2enc.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/libxml2/include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_xml2enc_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\wwhy.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../kudelrunsrc/kuda-delman/include" /i "../../kudelrunsrc/libxml2/include" /d "NDEBUG" /d BIN_NAME="capi_xml2enc.so" /d LONG_NAME="xml2enc_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_xml2enc.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib libxml2.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_xml2enc.pdb" /debug /out:"$(OUTDIR)\capi_xml2enc.so" /implib:"$(OUTDIR)\capi_xml2enc.lib" /libpath:"../../kudelrunsrc/libxml2/win32/bin.msvc" /base:@..\..\platforms\win32\BaseAddr.ref,capi_xml2enc.so /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\capi_xml2enc.obj" \
	"$(INTDIR)\wwhy.res" \
	"..\..\kudelrunsrc\kuda\Release\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Release\libkudadelman-1.lib" \
	"..\..\Release\libwwhy.lib"

"$(OUTDIR)\capi_xml2enc.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\capi_xml2enc.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_xml2enc.so"
   if exist .\Release\capi_xml2enc.so.manifest mt.exe -manifest .\Release\capi_xml2enc.so.manifest -outputresource:.\Release\capi_xml2enc.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "capi_xml2enc - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\capi_xml2enc.so" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Debug" "libkudadelman - Win32 Debug" "libkuda - Win32 Debug" "$(OUTDIR)\capi_xml2enc.so" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudadelman - Win32 DebugCLEAN" "libwwhy - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\wwhy.res"
	-@erase "$(INTDIR)\capi_xml2enc.obj"
	-@erase "$(INTDIR)\capi_xml2enc_src.idb"
	-@erase "$(INTDIR)\capi_xml2enc_src.pdb"
	-@erase "$(OUTDIR)\capi_xml2enc.exp"
	-@erase "$(OUTDIR)\capi_xml2enc.lib"
	-@erase "$(OUTDIR)\capi_xml2enc.pdb"
	-@erase "$(OUTDIR)\capi_xml2enc.so"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../../include" /I "../../kudelrunsrc/kuda/include" /I "../../kudelrunsrc/kuda-delman/include" /I "../../kudelrunsrc/libxml2/include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\capi_xml2enc_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\wwhy.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../kudelrunsrc/kuda-delman/include" /i "../../kudelrunsrc/libxml2/include" /d "_DEBUG" /d BIN_NAME="capi_xml2enc.so" /d LONG_NAME="xml2enc_capi for cLHy" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\capi_xml2enc.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib libxml2.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\capi_xml2enc.pdb" /debug /out:"$(OUTDIR)\capi_xml2enc.so" /implib:"$(OUTDIR)\capi_xml2enc.lib" /libpath:"../../kudelrunsrc/libxml2/win32/bin.msvc" /base:@..\..\platforms\win32\BaseAddr.ref,capi_xml2enc.so 
LINK32_OBJS= \
	"$(INTDIR)\capi_xml2enc.obj" \
	"$(INTDIR)\wwhy.res" \
	"..\..\kudelrunsrc\kuda\Debug\libkuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\Debug\libkudadelman-1.lib" \
	"..\..\Debug\libwwhy.lib"

"$(OUTDIR)\capi_xml2enc.so" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\capi_xml2enc.so
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\capi_xml2enc.so"
   if exist .\Debug\capi_xml2enc.so.manifest mt.exe -manifest .\Debug\capi_xml2enc.so.manifest -outputresource:.\Debug\capi_xml2enc.so;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("capi_xml2enc.dep")
!INCLUDE "capi_xml2enc.dep"
!ELSE 
!MESSAGE Warning: cannot find "capi_xml2enc.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "capi_xml2enc - Win32 Release" || "$(CFG)" == "capi_xml2enc - Win32 Debug"
SOURCE=..\..\build\win32\wwhy.rc

!IF  "$(CFG)" == "capi_xml2enc - Win32 Release"


"$(INTDIR)\wwhy.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\wwhy.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../kudelrunsrc/kuda-delman/include" /i "../../kudelrunsrc/libxml2/include" /i "../../build\win32" /d "NDEBUG" /d BIN_NAME="capi_xml2enc.so" /d LONG_NAME="xml2enc_capi for cLHy" $(SOURCE)


!ELSEIF  "$(CFG)" == "capi_xml2enc - Win32 Debug"


"$(INTDIR)\wwhy.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\wwhy.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../kudelrunsrc/kuda-delman/include" /i "../../kudelrunsrc/libxml2/include" /i "../../build\win32" /d "_DEBUG" /d BIN_NAME="capi_xml2enc.so" /d LONG_NAME="xml2enc_capi for cLHy" $(SOURCE)


!ENDIF 

SOURCE=.\capi_xml2enc.c

"$(INTDIR)\capi_xml2enc.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "capi_xml2enc - Win32 Release"

"libkuda - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\..\cAPIs\clfilters"

"libkuda - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clfilters"

!ELSEIF  "$(CFG)" == "capi_xml2enc - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\..\cAPIs\clfilters"

"libkuda - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clfilters"

!ENDIF 

!IF  "$(CFG)" == "capi_xml2enc - Win32 Release"

"libkudadelman - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" 
   cd "..\..\cAPIs\clfilters"

"libkudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clfilters"

!ELSEIF  "$(CFG)" == "capi_xml2enc - Win32 Debug"

"libkudadelman - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" 
   cd "..\..\cAPIs\clfilters"

"libkudadelman - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\cAPIs\clfilters"

!ENDIF 

!IF  "$(CFG)" == "capi_xml2enc - Win32 Release"

"libwwhy - Win32 Release" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" 
   cd ".\cAPIs\clfilters"

"libwwhy - Win32 ReleaseCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" RECURSE=1 CLEAN 
   cd ".\cAPIs\clfilters"

!ELSEIF  "$(CFG)" == "capi_xml2enc - Win32 Debug"

"libwwhy - Win32 Debug" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" 
   cd ".\cAPIs\clfilters"

"libwwhy - Win32 DebugCLEAN" : 
   cd ".\..\.."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" RECURSE=1 CLEAN 
   cd ".\cAPIs\clfilters"

!ENDIF 


!ENDIF 

