# The cLHy README

cLHy is a fast and robust HTTP server from the Hyang Language Foundation (HLF).
cLHy source code is distributed under GNU GPL version 3.

*See [LICENSE](LICENSE) for license information, see [INSTALL](INSTALL) for build/install informations, see [README](README) for other informations.*
