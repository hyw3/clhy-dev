
dnl CLHYKUDEL_HELP_STRING(LHS, RHS)
dnl Autoconf 2.50 can not handle substr correctly.  It does have 
dnl AC_HELP_STRING, so let's try to call it if we can.
dnl Note: this define must be on one line so that it can be properly returned
dnl as the help string.
AC_DEFUN([CLHYKUDEL_HELP_STRING],[ifelse(regexp(AC_ACVERSION, 2\.1), -1, AC_HELP_STRING($1,$2),[  ]$1 substr([                       ],len($1))$2)])dnl

dnl CLHYKUDEL_SUBST(VARIABLE)
dnl Makes VARIABLE available in generated files
dnl (do not use @variable@ in Makefiles, but $(variable))
AC_DEFUN([CLHYKUDEL_SUBST],[
  CLHYKUDEL_VAR_SUBST="$CLHYKUDEL_VAR_SUBST $1"
  AC_SUBST($1)
])

dnl CLHYKUDEL_FAST_OUTPUT(FILENAME)
dnl Perform substitutions on FILENAME (Makefiles only)
AC_DEFUN([CLHYKUDEL_FAST_OUTPUT],[
  CLHYKUDEL_FAST_OUTPUT_FILES="$CLHYKUDEL_FAST_OUTPUT_FILES $1"
])

dnl CLHYKUDEL_GEN_CONFIG_VARS
dnl Creates config_vars.mk
AC_DEFUN([CLHYKUDEL_GEN_CONFIG_VARS],[
  CLHYKUDEL_SUBST(WWHY_VERSION)
  CLHYKUDEL_SUBST(WWHY_CAPIMN)
  CLHYKUDEL_SUBST(abs_srcdir)
  CLHYKUDEL_SUBST(bindir)
  CLHYKUDEL_SUBST(sbindir)
  CLHYKUDEL_SUBST(cgidir)
  CLHYKUDEL_SUBST(logfiledir)
  CLHYKUDEL_SUBST(exec_prefix)
  CLHYKUDEL_SUBST(datadir)
  CLHYKUDEL_SUBST(localstatedir)
  CLHYKUDEL_SUBST(mandir)
  CLHYKUDEL_SUBST(libdir)
  CLHYKUDEL_SUBST(libexecdir)
  CLHYKUDEL_SUBST(htdocsdir)
  CLHYKUDEL_SUBST(manualdir)
  CLHYKUDEL_SUBST(includedir)
  CLHYKUDEL_SUBST(errordir)
  CLHYKUDEL_SUBST(iconsdir)
  CLHYKUDEL_SUBST(sysconfdir)
  CLHYKUDEL_SUBST(installbuilddir)
  CLHYKUDEL_SUBST(runtimedir)
  CLHYKUDEL_SUBST(proxycachedir)
  CLHYKUDEL_SUBST(other_targets)
  CLHYKUDEL_SUBST(progname)
  CLHYKUDEL_SUBST(prefix)
  CLHYKUDEL_SUBST(AWK)
  CLHYKUDEL_SUBST(CC)
  CLHYKUDEL_SUBST(CPP)
  CLHYKUDEL_SUBST(CXX)
  CLHYKUDEL_SUBST(CPPFLAGS)
  CLHYKUDEL_SUBST(CFLAGS)
  CLHYKUDEL_SUBST(CXXFLAGS)
  CLHYKUDEL_SUBST(LTFLAGS)
  CLHYKUDEL_SUBST(LDFLAGS)
  CLHYKUDEL_SUBST(LT_LDFLAGS)
  CLHYKUDEL_SUBST(SH_LDFLAGS)
  CLHYKUDEL_SUBST(WWHY_LDFLAGS)
  CLHYKUDEL_SUBST(UTIL_LDFLAGS)
  CLHYKUDEL_SUBST(LIBS)
  CLHYKUDEL_SUBST(DEFS)
  CLHYKUDEL_SUBST(INCLUDES)
  CLHYKUDEL_SUBST(NOTEST_CPPFLAGS)
  CLHYKUDEL_SUBST(NOTEST_CFLAGS)
  CLHYKUDEL_SUBST(NOTEST_CXXFLAGS)
  CLHYKUDEL_SUBST(NOTEST_LDFLAGS)
  CLHYKUDEL_SUBST(NOTEST_LIBS)
  CLHYKUDEL_SUBST(EXTRA_CPPFLAGS)
  CLHYKUDEL_SUBST(EXTRA_CFLAGS)
  CLHYKUDEL_SUBST(EXTRA_CXXFLAGS)
  CLHYKUDEL_SUBST(EXTRA_LDFLAGS)
  CLHYKUDEL_SUBST(EXTRA_LIBS)
  CLHYKUDEL_SUBST(EXTRA_INCLUDES)
  CLHYKUDEL_SUBST(INTERNAL_CPPFLAGS)
  CLHYKUDEL_SUBST(LIBTOOL)
  CLHYKUDEL_SUBST(SHELL)
  CLHYKUDEL_SUBST(RSYNC)
  CLHYKUDEL_SUBST(CAPI_DIRS)
  CLHYKUDEL_SUBST(CAPI_CLEANDIRS)
  CLHYKUDEL_SUBST(PORT)
  CLHYKUDEL_SUBST(SSLPORT)
  CLHYKUDEL_SUBST(CORE_IMPLIB_FILE)
  CLHYKUDEL_SUBST(CORE_IMPLIB)
  CLHYKUDEL_SUBST(SH_LIBS)
  CLHYKUDEL_SUBST(SH_LIBTOOL)
  CLHYKUDEL_SUBST(MK_IMPLIB)
  CLHYKUDEL_SUBST(MKDEP)
  CLHYKUDEL_SUBST(INSTALL_PROG_FLAGS)
  CLHYKUDEL_SUBST(DSO_CAPIS)
  CLHYKUDEL_SUBST(ENABLED_DSO_CAPIS)
  CLHYKUDEL_SUBST(ACTIVATE_ALL_CAPIS)
  CLHYKUDEL_SUBST(KUDA_BINDIR)
  CLHYKUDEL_SUBST(KUDA_INCLUDEDIR)
  CLHYKUDEL_SUBST(KUDA_VERSION)
  CLHYKUDEL_SUBST(KUDA_CONFIG)
  CLHYKUDEL_SUBST(KUDELMAN_BINDIR)
  CLHYKUDEL_SUBST(KUDELMAN_INCLUDEDIR)
  CLHYKUDEL_SUBST(KUDELMAN_VERSION)
  CLHYKUDEL_SUBST(KUDELMAN_CONFIG)

  abs_srcdir="`(cd $srcdir && pwd)`"

  AC_MSG_NOTICE([creating config_vars.mk])
  test -d build || $mkdir_p build
  > build/config_vars.mk
  for i in $CLHYKUDEL_VAR_SUBST; do
    eval echo "$i = \$$i" >> build/config_vars.mk
  done
])

dnl CLHYKUDEL_GEN_MAKEFILES
dnl Creates Makefiles
AC_DEFUN([CLHYKUDEL_GEN_MAKEFILES],[
  $SHELL $srcdir/build/fastgen.sh $srcdir $ac_cv_mkdir_p $BSD_MAKEFILE $CLHYKUDEL_FAST_OUTPUT_FILES
])

dnl
dnl CLHYKUDEL_TYPE_RLIM_T
dnl
dnl If rlim_t is not defined, define it to int
dnl
AC_DEFUN([CLHYKUDEL_TYPE_RLIM_T], [
  AC_CACHE_CHECK([for rlim_t], ac_cv_type_rlim_t, [
    AC_TRY_COMPILE([
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
], [rlim_t spoon;], [
      ac_cv_type_rlim_t=yes
    ],[ac_cv_type_rlim_t=no
    ])
  ])
  if test "$ac_cv_type_rlim_t" = "no" ; then
      AC_DEFINE(rlim_t, int,
          [Define to 'int' if <sys/resource.h> doesn't define it for us])
  fi
])

dnl the list of build variables which are available for customization on a
dnl per cAPI subdir basis (to be inserted into cAPIs.mk with a "CAPI_"
dnl prefix, i.e. CAPI_CFLAGS etc.). Used in CLHYKUDEL_CAPIPATH_{INIT,FINISH}.
define(capi_buildvars, [CFLAGS CXXFLAGS CPPFLAGS LDFLAGS LIBS INCLUDES])
dnl
dnl CLHYKUDEL_CAPIPATH_INIT(capipath)
AC_DEFUN([CLHYKUDEL_CAPIPATH_INIT],[
  current_dir=$1
  capipath_current=cAPIs/$1
  capipath_static=
  capipath_shared=
  for var in capi_buildvars; do
    eval CAPI_$var=
  done
  test -d $1 || $srcdir/build/mkdir.sh $capipath_current
  > $capipath_current/cAPIs.mk
])dnl
dnl
AC_DEFUN([CLHYKUDEL_CAPIPATH_FINISH],[
  echo "DISTCLEAN_TARGETS = cAPIs.mk" >> $capipath_current/cAPIs.mk
  echo "static = $capipath_static" >> $capipath_current/cAPIs.mk
  echo "shared = $capipath_shared" >> $capipath_current/cAPIs.mk
  for var in capi_buildvars; do
    if eval val=\"\$CAPI_$var\"; test -n "$val"; then
      echo "CAPI_$var = $val" >> $capipath_current/cAPIs.mk
    fi
  done
  if test ! -z "$capipath_static" -o ! -z "$capipath_shared"; then
    CAPI_DIRS="$CAPI_DIRS $current_dir"
  else
    CAPI_CLEANDIRS="$CAPI_CLEANDIRS $current_dir"
  fi
  CLHYKUDEL_FAST_OUTPUT($capipath_current/Makefile)
])dnl
dnl
dnl CLHYKUDEL_CAPIPATH_ADD(name[, shared[, objects [, ldflags[, libs]]]])
AC_DEFUN([CLHYKUDEL_CAPIPATH_ADD],[
  if test -z "$3"; then
    objects="capi_$1.lo"
  else
    objects="$3"
  fi

  if test -z "$capi_standalone"; then
    if test -z "$2"; then
      # The filename of a convenience library must have a "lib" prefix:
      libname="libcapi_$1.la"
      BUILTIN_LIBS="$BUILTIN_LIBS $capipath_current/$libname"
      capipath_static="$capipath_static $libname"
      cat >>$capipath_current/cAPIs.mk<<EOF
$libname: $objects
	\$(CAPI_LINK) $objects $5
EOF
      if test ! -z "$5"; then
        KUDA_ADDTO(CLHY_LIBS, [$5])
      fi
    else
      clhy_need_shared=yes
      libname="capi_$1.la"
      shobjects=`echo $objects | sed 's/\.lo/.slo/g'`
      capipath_shared="$capipath_shared $libname"
      cat >>$capipath_current/cAPIs.mk<<EOF
$libname: $shobjects
	\$(SH_LINK) -rpath \$(libexecdir) -cAPI -avoid-version $4 $objects $5
EOF
    fi
  fi
])dnl

dnl
dnl CLHYKUDEL_CLMP_CAPI(name[, shared[, objects[, config[, path[, libs]]]]])
dnl
dnl Provide information for building the cLMP.  (Enablement is handled using
dnl --with-clmp/--enable-clmp-shared.)
dnl
dnl name     -- name of cLHy core API (cLMP), same as clcore directory name
dnl shared   -- "shared" to indicate shared cAPI build, empty string otherwise
dnl objects  -- one or more .lo files to link into the core cAPI (default: clcorename.lo)
dnl config   -- configuration logic to run if the cLMP is enabled
dnl path     -- relative path to clcore (default: server/clcore/clcorename)
dnl libs     -- libs needed by this cLMP
dnl
AC_DEFUN([CLHYKUDEL_CLMP_CAPI],[
    if clhy_clmp_is_enabled $1; then
        if test -z "$3"; then
            objects="$1.lo"
        else
            objects="$3"
        fi

        if test -z "$5"; then
            clcorepath="server/clcore/$1"
        else
            clcorepath=$5
        fi

        dnl VPATH support
        test -d $clcorepath || $srcdir/build/mkdir.sh $clcorepath

        CLHYKUDEL_FAST_OUTPUT($clcorepath/Makefile)

        if test -z "$2"; then
            KUDA_ADDTO(CLHY_LIBS, [$6])
            libname="lib$1.la"
            cat >$clcorepath/cAPIs.mk<<EOF
$libname: $objects
	\$(CAPI_LINK) $objects
DISTCLEAN_TARGETS = cAPIs.mk
static = $libname
shared =
EOF
        else
            clhy_need_shared=yes
            libname="capi_core_$1.la"
            shobjects=`echo $objects | sed 's/\.lo/.slo/g'`
            cat >$clcorepath/cAPIs.mk<<EOF
$libname: $shobjects
	\$(SH_LINK) -rpath \$(libexecdir) -cAPI -avoid-version $objects $6
DISTCLEAN_TARGETS = cAPIs.mk
static =
shared = $libname
EOF
            DSO_CAPIS="$DSO_CAPIS clmp_$1"
            # add default cLMP to ActivatecAPI list
            if test $1 = $default_clmp; then
                ENABLED_DSO_CAPIS="${ENABLED_DSO_CAPIS},clmp_$1"
            fi
        fi
        $4
    fi
])dnl

dnl
dnl CLHYKUDEL_CAPI(name, helptext[, objects[, structname[, default[, config[, prereq_capi]]]]])
dnl
dnl default is one of:
dnl   yes    -- enabled by default. user must explicitly disable.
dnl   no     -- disabled under default, most, all. user must explicitly enable.
dnl   most   -- disabled by default. enabled explicitly or with most or all.
dnl   static -- enabled as static by default, must be explicitly changed.
dnl   ""     -- disabled under default, most. enabled explicitly or with all.
dnl             XXX: The arg must really be empty here. Passing an empty shell
dnl             XXX: variable doesn't work for some reason. This should be
dnl             XXX: fixed.
dnl
dnl basically: yes/no is a hard setting. "most" means follow the "most"
dnl            setting. otherwise, fall under the "all" setting.
dnl            explicit yes/no always overrides, except if the user selects
dnl            "reallyall".
dnl
dnl prereq_capi is a cAPI (without the "capi_" prefix) that must be enabled
dnl   if the current cAPI is enabled.  If the current cAPI is built
dnl   statically, prereq_capi must be built statically, too.  If these
dnl   conditions are not fulfilled, configure will abort if the current cAPI
dnl   has been enabled explicitly. Otherwise, configure will disable the
dnl   current cAPI.
dnl   prereq_capi's CLHYKUDEL_CAPI() statement must have been processed
dnl   before the current CLHYKUDEL_CAPI() statement.
dnl
AC_DEFUN([CLHYKUDEL_CAPI],[
  AC_MSG_CHECKING(whether to enable capi_$1)
  define([optname],[--]ifelse($5,yes,disable,enable)[-]translit($1,_,-))dnl
  AC_ARG_ENABLE(translit($1,_,-),CLHYKUDEL_HELP_STRING(optname(),$2),force_$1=$enableval,enable_$1=ifelse($5,,maybe-all,$5))
  undefine([optname])dnl
  _clhycapi_extra_msg=""
  dnl If the cAPI was not explicitly requested, allow it to disable itself if
  dnl its pre-reqs fail.
  case "$enable_$1" in
    yes|static|shared)
      _clhycapi_required="yes"
      ;;
    *)
      _clhycapi_required="no"
      ;;
  esac
  if test "$enable_$1" = "static" -o "$enable_$1" = "shared"; then
    :
  elif test "$enable_$1" = "yes"; then
    enable_$1=$capi_default
  elif test "$enable_$1" = "few"; then
    if test "$capi_selection" = "few" -o "$capi_selection" = "most" -o \
            "$capi_selection" = "all" -o "$capi_selection" = "reallyall"
    then
      enable_$1=$capi_default
    else
      enable_$1=no
    fi
    _clhycapi_extra_msg=" ($capi_selection)"
  elif test "$enable_$1" = "most"; then
    if test "$capi_selection" = "most" -o "$capi_selection" = "all" -o \
            "$capi_selection" = "reallyall"
    then
      enable_$1=$capi_default
    else
      enable_$1=no
    fi
    _clhycapi_extra_msg=" ($capi_selection)"
  elif test "$enable_$1" = "all" -o "$enable_$1" = "maybe-all"; then
    if test "$capi_selection" = "all" -o "$capi_selection" = "reallyall"
    then
      enable_$1=$capi_default
      _clhycapi_extra_msg=" ($capi_selection)"
    else
      enable_$1=no
    fi
  elif test "$enable_$1" = "reallyall" -o "$enable_$1" = "no" ; then
    if test "$capi_selection" = "reallyall" -a "$force_$1" != "no" ; then
      enable_$1=$capi_default
      _clhycapi_extra_msg=" ($capi_selection)"
    else
      enable_$1=no
    fi
  else
    enable_$1=no
  fi
  if test "$enable_$1" != "no"; then
    dnl If we plan to enable it, allow the cAPI to run some autoconf magic
    dnl that may disable it because of missing dependencies.
    ifelse([$6$7],,:,
           [AC_MSG_RESULT([checking dependencies])
            ifelse([$7],,:,[m4_foreach([prereq],[$7],
                           [if test "$enable_[]prereq" = "no" ; then
                              enable_$1=no
                              AC_MSG_WARN("capi_[]prereq is disabled but required for capi_$1")
                            elif test "$enable_$1" = "static" && test "$enable_[]prereq" != "static" ; then
                              enable_$1=$enable_[]prereq
                              AC_MSG_WARN("building capi_$1 shared because capi_[]prereq is built shared")
                            el])se])
            ifelse([$6],,:,[  $6])
            ifelse([$7],,:,[fi])
            AC_MSG_CHECKING(whether to enable capi_$1)
            if test "$enable_$1" = "no"; then
              if test "$_clhycapi_required" = "no"; then
                _clhycapi_extra_msg=" (disabled)"
              else
                AC_MSG_ERROR([capi_$1 has been requested but can not be built due to prerequisite failures])
              fi
            fi])
  fi
  AC_MSG_RESULT($enable_$1$_clhycapi_extra_msg)
  if test "$enable_$1" != "no"; then
    case "$enable_$1" in
    static*)
      CAPILIST="$CAPILIST ifelse($4,,$1,$4)"
      if test "$1" = "so"; then
          sharedobjs=yes
      fi
      shared="";;
    *)
      sharedobjs=yes
      shared=yes
      DSO_CAPIS="$DSO_CAPIS $1"
      if test "$5" = "yes" ; then
        ENABLED_DSO_CAPIS="${ENABLED_DSO_CAPIS},$1"
      fi
      ;;
    esac
    define([modprefix], [CAPI_]translit($1, [a-z-], [A-Z_]))
    CLHYKUDEL_CAPIPATH_ADD($1, $shared, $3,, [\$(]modprefix[_LDADD)])
    CLHYKUDEL_SUBST(modprefix[_LDADD])
    undefine([modprefix])
  fi
])dnl

dnl
dnl CLHYKUDEL_ENABLE_CAPIS
dnl
AC_DEFUN([CLHYKUDEL_ENABLE_CAPIS],[
  capi_selection=most
  capi_default=shared

  dnl Check whether we have DSO support.
  dnl If "yes", we build shared cAPIs by default.
  KUDA_CHECK_KUDA_DEFINE(KUDA_HAS_DSO)

  if test $ac_cv_define_KUDA_HAS_DSO = "no"; then
    AC_MSG_WARN([Missing DSO support - building static cAPIs by default.])
    capi_default=static
  fi


  AC_ARG_ENABLE(cAPIs,
  CLHYKUDEL_HELP_STRING(--enable-cAPIs=CAPI-LIST,Space-separated list of cAPIs to enable | "all" | "most" | "few" | "none" | "reallyall"),[
    if test "$enableval" = "none"; then
       capi_default=no
       capi_selection=none
    else
      for i in $enableval; do
        if test "$i" = "all" -o "$i" = "most" -o "$i" = "few" -o "$i" = "reallyall"
        then
          capi_selection=$i
        else
          i=`echo $i | sed 's/-/_/g'`
          eval "enable_$i=shared"
        fi
      done
    fi
  ])
  
  AC_ARG_ENABLE(capis-shared,
  CLHYKUDEL_HELP_STRING(--enable-capis-shared=CAPI-LIST,Space-separated list of shared cAPIs to enable | "all" | "most" | "few" | "reallyall"),[
    for i in $enableval; do
      if test "$i" = "all" -o "$i" = "most" -o "$i" = "few" -o "$i" = "reallyall"
      then
        capi_selection=$i
        capi_default=shared
      else
        i=`echo $i | sed 's/-/_/g'`
    	eval "enable_$i=shared"
      fi
    done
  ])
  
  AC_ARG_ENABLE(capis-static,
  CLHYKUDEL_HELP_STRING(--enable-capis-static=CAPI-LIST,Space-separated list of static cAPIs to enable | "all" | "most" | "few" | "reallyall"),[
    for i in $enableval; do
      if test "$i" = "all" -o "$i" = "most" -o "$i" = "few" -o "$i" = "reallyall"; then
        capi_selection=$i
        capi_default=static
      else
        i=`echo $i | sed 's/-/_/g'`
    	eval "enable_$i=static"
      fi
    done
  ])
])

AC_DEFUN([CLHYKUDEL_REQUIRE_CXX],[
  if test -z "$clhy_cxx_done"; then
    AC_PROG_CXX
    AC_PROG_CXXCPP
    clhy_cxx_done=yes
  fi
])

dnl
dnl CLHYKUDEL_CHECK_OPENSSL
dnl
dnl Configure for OpenSSL, giving preference to
dnl "--with-ssl=<path>" if it was specified.
dnl
AC_DEFUN([CLHYKUDEL_CHECK_OPENSSL],[
  AC_CACHE_CHECK([for OpenSSL], [ac_cv_openssl], [
    dnl initialise the variables we use
    ac_cv_openssl=no
    clhy_openssl_found=""
    clhy_openssl_base=""
    clhy_openssl_libs=""
    clhy_openssl_capi_cflags=""
    clhy_openssl_capi_ldflags=""

    dnl Determine the OpenSSL base directory, if any
    AC_MSG_CHECKING([for user-provided OpenSSL base directory])
    AC_ARG_WITH(ssl, CLHYKUDEL_HELP_STRING(--with-ssl=PATH,OpenSSL installation directory), [
      dnl If --with-ssl specifies a directory, we use that directory
      if test "x$withval" != "xyes" -a "x$withval" != "x"; then
        dnl This ensures $withval is actually a directory and that it is absolute
        clhy_openssl_base="`cd $withval ; pwd`"
      fi
    ])
    if test "x$clhy_openssl_base" = "x"; then
      AC_MSG_RESULT(none)
    else
      AC_MSG_RESULT($clhy_openssl_base)
    fi

    dnl Run header and version checks
    saved_CPPFLAGS="$CPPFLAGS"
    saved_LIBS="$LIBS"
    saved_LDFLAGS="$LDFLAGS"

    dnl Before doing anything else, load in pkg-config variables
    if test -n "$PKGCONFIG"; then
      saved_PKG_CONFIG_PATH="$PKG_CONFIG_PATH"
      if test "x$clhy_openssl_base" != "x" -a \
              -f "${clhy_openssl_base}/lib/pkgconfig/openssl.pc"; then
        dnl Ensure that the given path is used by pkg-config too, otherwise
        dnl the system openssl.pc might be picked up instead.
        PKG_CONFIG_PATH="${clhy_openssl_base}/lib/pkgconfig${PKG_CONFIG_PATH+:}${PKG_CONFIG_PATH}"
        export PKG_CONFIG_PATH
      fi
      AC_ARG_ENABLE(ssl-staticlib-deps,CLHYKUDEL_HELP_STRING(--enable-ssl-staticlib-deps,[link capi_ssl with dependencies of OpenSSL's static libraries (as indicated by "pkg-config --static"). Must be specified in addition to --enable-ssl.]), [
        if test "$enableval" = "yes"; then
          PKGCONFIG_LIBOPTS="--static"
        fi
      ])
      clhy_openssl_libs="`$PKGCONFIG $PKGCONFIG_LIBOPTS --libs-only-l --silence-errors openssl`"
      if test $? -eq 0; then
        clhy_openssl_found="yes"
        pkglookup="`$PKGCONFIG --cflags-only-I openssl`"
        KUDA_ADDTO(CPPFLAGS, [$pkglookup])
        KUDA_ADDTO(CAPI_CFLAGS, [$pkglookup])
        KUDA_ADDTO(clbench_CFLAGS, [$pkglookup])
        pkglookup="`$PKGCONFIG $PKGCONFIG_LIBOPTS --libs-only-L openssl`"
        KUDA_ADDTO(LDFLAGS, [$pkglookup])
        KUDA_ADDTO(CAPI_LDFLAGS, [$pkglookup])
        pkglookup="`$PKGCONFIG $PKGCONFIG_LIBOPTS --libs-only-other openssl`"
        KUDA_ADDTO(LDFLAGS, [$pkglookup])
        KUDA_ADDTO(CAPI_LDFLAGS, [$pkglookup])
      fi
      PKG_CONFIG_PATH="$saved_PKG_CONFIG_PATH"
    fi

    dnl fall back to the user-supplied directory if not found via pkg-config
    if test "x$clhy_openssl_base" != "x" -a "x$clhy_openssl_found" = "x"; then
      KUDA_ADDTO(CPPFLAGS, [-I$clhy_openssl_base/include])
      KUDA_ADDTO(CAPI_CFLAGS, [-I$clhy_openssl_base/include])
      KUDA_ADDTO(clbench_CFLAGS, [-I$clhy_openssl_base/include])
      KUDA_ADDTO(LDFLAGS, [-L$clhy_openssl_base/lib])
      KUDA_ADDTO(CAPI_LDFLAGS, [-L$clhy_openssl_base/lib])
      if test "x$clhy_platform_runtime_link_flag" != "x"; then
        KUDA_ADDTO(LDFLAGS, [$clhy_platform_runtime_link_flag$clhy_openssl_base/lib])
        KUDA_ADDTO(CAPI_LDFLAGS, [$clhy_platform_runtime_link_flag$clhy_openssl_base/lib])
      fi
    fi

    AC_MSG_CHECKING([for OpenSSL version >= 0.9.8a])
    AC_TRY_COMPILE([#include <openssl/opensslv.h>],[
#if !defined(OPENSSL_VERSION_NUMBER)
#error "Missing OpenSSL version"
#endif
#if OPENSSL_VERSION_NUMBER < 0x0090801f
#error "Unsupported OpenSSL version " OPENSSL_VERSION_TEXT
#endif],
      [AC_MSG_RESULT(OK)
       ac_cv_openssl=yes],
      [AC_MSG_RESULT(FAILED)])

    if test "x$ac_cv_openssl" = "xyes"; then
      clhy_openssl_libs="${clhy_openssl_libs:--lssl -lcrypto} `$kuda_config --libs`"
      KUDA_ADDTO(CAPI_LDFLAGS, [$clhy_openssl_libs])
      KUDA_ADDTO(LIBS, [$clhy_openssl_libs])
      KUDA_SETVAR(clbench_LDFLAGS, [$CAPI_LDFLAGS])
      CLHYKUDEL_SUBST(clbench_CFLAGS)
      CLHYKUDEL_SUBST(clbench_LDFLAGS)

      dnl Run library and function checks
      liberrors=""
      AC_CHECK_HEADERS([openssl/engine.h])
      AC_CHECK_FUNCS([SSLeay_version SSL_CTX_new], [], [liberrors="yes"])
      AC_CHECK_FUNCS([ENGINE_init ENGINE_load_builtin_engines RAND_egd])
      if test "x$liberrors" != "x"; then
        AC_MSG_WARN([OpenSSL libraries are unusable])
      fi
    else
      AC_MSG_WARN([OpenSSL version is too old])
    fi

    dnl restore
    CPPFLAGS="$saved_CPPFLAGS"
    LIBS="$saved_LIBS"
    LDFLAGS="$saved_LDFLAGS"

    dnl cache CAPI_LDFLAGS, CAPI_CFLAGS
    clhy_openssl_capi_cflags=$CAPI_CFLAGS
    clhy_openssl_capi_ldflags=$CAPI_LDFLAGS
  ])
  if test "x$ac_cv_openssl" = "xyes"; then
    AC_DEFINE(HAVE_OPENSSL, 1, [Define if OpenSSL is available])
    KUDA_ADDTO(CAPI_LDFLAGS, [$clhy_openssl_capi_ldflags])
    KUDA_ADDTO(CAPI_CFLAGS, [$clhy_openssl_capi_cflags])
  fi
])

dnl
dnl CLHYKUDEL_EXPORT_ARGUMENTS
dnl Export (via CLHYKUDEL_SUBST) the various path-related variables that
dnl clhy will use while generating scripts like autoconf and clhyext and
dnl the default config file.

AC_DEFUN([CLHYKUDEL_SUBST_EXPANDED_ARG],[
  KUDA_EXPAND_VAR(exp_$1, [$]$1)
  CLHYKUDEL_SUBST(exp_$1)
  KUDA_PATH_RELATIVE(rel_$1, [$]exp_$1, ${prefix})
  CLHYKUDEL_SUBST(rel_$1)
])

AC_DEFUN([CLHYKUDEL_EXPORT_ARGUMENTS],[
  CLHYKUDEL_SUBST_EXPANDED_ARG(exec_prefix)
  CLHYKUDEL_SUBST_EXPANDED_ARG(bindir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(sbindir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(libdir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(libexecdir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(mandir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(sysconfdir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(datadir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(installbuilddir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(errordir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(iconsdir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(htdocsdir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(manualdir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(cgidir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(includedir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(localstatedir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(runtimedir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(logfiledir)
  CLHYKUDEL_SUBST_EXPANDED_ARG(proxycachedir)
])

dnl 
dnl CLHYKUDEL_CHECK_CLHYxVER({kuda|kudelman}, major, minor, 
dnl                     [actions-if-ok], [actions-if-not-ok])
dnl
dnl Checks for KUDA or Kuda-Delman of given major/minor version or later; 
dnl if so, runs actions-if-ok; otherwise runs actions-if-not-ok if given.
dnl If the version is not satisfactory and actions-if-not-ok is not
dnl given, then an error is printed and the configure script is aborted.
dnl
dnl The first argument must be [kuda] or [kudelman].
dnl
AC_DEFUN([CLHYKUDEL_CHECK_CLHYxVER], [
define(clhy_ckver_major, translit($1, [kudadelm], [KUDADELM])[_MAJOR_VERSION])
define(clhy_ckver_minor, translit($1, [kudadelm], [KUDADELM])[_MINOR_VERSION])
define(clhy_ckver_cvar, [clhy_cv_$1ver$2$3])
define(clhy_ckver_name, ifelse([$1],[kuda],[KUDA],[Kuda-Delman]))

clhy_ckver_CPPFLAGS="$CPPFLAGS"
CPPFLAGS="$CPPFLAGS `$[$1]_config --includes`"

AC_CACHE_CHECK([for clhy_ckver_name version $2.$3.0 or later], clhy_ckver_cvar, [
AC_EGREP_CPP([good], [
#include <$1_version.h>
#if ]clhy_ckver_major[ > $2 || (]clhy_ckver_major[ == $2 && ]clhy_ckver_minor[ >= $3)
good
#endif
], [clhy_ckver_cvar=yes], [clhy_ckver_cvar=no])])

if test "$clhy_ckver_cvar" = "yes"; then
  ifelse([$4],[],[:],[$4])
else
  ifelse([$5],[],[AC_MSG_ERROR([clhy_ckver_name version $2.$3.0 or later is required])], [$5])
fi

CPPFLAGS="$clhy_ckver_CPPFLAGS"

undefine([clhy_ckver_major])
undefine([clhy_ckver_minor])
undefine([clhy_ckver_cvar])
undefine([clhy_ckver_name])
])

dnl
dnl CLHYKUDEL_CHECK_VOID_PTR_LEN
dnl
dnl Checks if the size of a void pointer is at least as big as a "long" 
dnl integer type.
dnl
AC_DEFUN([CLHYKUDEL_CHECK_VOID_PTR_LEN], [

AC_CACHE_CHECK([for void pointer length], [clhy_cv_void_ptr_lt_long],
[AC_TRY_RUN([
int main(void)
{
    return sizeof(void *) < sizeof(long); 
}], [clhy_cv_void_ptr_lt_long=no], [clhy_cv_void_ptr_lt_long=yes], 
    [clhy_cv_void_ptr_lt_long=yes])])

if test "$clhy_cv_void_ptr_lt_long" = "yes"; then
    AC_MSG_ERROR([Size of "void *" is less than size of "long"])
fi
])

dnl
dnl CLHYKUDEL_CHECK_KUDA_HAS_LDAP
dnl
dnl Check if KUDA_HAS_LDAP is 1
dnl Unfortunately, we can't use KUDA_CHECK_KUDA_DEFINE (because it only includes kuda.h)
dnl or KUDA_CHECK_DEFINE (because it only checks for defined'ness and not for 0/1).
dnl
AC_DEFUN([CLHYKUDEL_CHECK_KUDA_HAS_LDAP], [
  AC_CACHE_CHECK([for ldap support in kuda/kuda-delman],ac_cv_KUDA_HAS_LDAP,[
    clhy_old_cppflags="$CPPFLAGS"
    CPPFLAGS="$CPPFLAGS $INCLUDES"
    AC_EGREP_CPP(YES_IS_DEFINED, [
#include <kuda_ldap.h>
#if KUDA_HAS_LDAP
YES_IS_DEFINED
#endif
    ], ac_cv_KUDA_HAS_LDAP=yes, ac_cv_KUDA_HAS_LDAP=no)
    CPPFLAGS="$clhy_old_cppflags"
  ])
])

dnl
dnl CLHYKUDEL_ADD_GCC_CFLAGS
dnl
dnl Check if compiler is gcc and supports flag. If yes, add to CFLAGS.
dnl
AC_DEFUN([CLHYKUDEL_ADD_GCC_CFLAG], [
  define([clhy_gcc_ckvar], [ac_cv_gcc_]translit($1, [-:.=], [____]))
  if test "$GCC" = "yes"; then
    AC_CACHE_CHECK([whether gcc accepts $1], clhy_gcc_ckvar, [
      save_CFLAGS="$CFLAGS"
      CFLAGS="$CFLAGS $1"
      AC_COMPILE_IFELSE([AC_LANG_PROGRAM()],
        [clhy_gcc_ckvar=yes], [clhy_gcc_ckvar=no])
      CFLAGS="$save_CFLAGS"
    ])
    if test "$]clhy_gcc_ckvar[" = "yes" ; then
       KUDA_ADDTO(CFLAGS,[$1])
    fi
  fi
  undefine([clhy_gcc_ckvar])
])
