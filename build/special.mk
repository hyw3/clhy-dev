# Licensed to the Hyang Language Foundation (HLF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The HLF licenses this file to You under the GNU GPL Version 3 or later
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://clhy.hyang.org/license.hyss
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# The build environment was provided by Sascha Schumann.

all: all-recursive

include $(builddir)/cAPIs.mk

TARGETS = $(static)
SHARED_TARGETS = $(shared)
INSTALL_TARGETS = install-cAPIs-$(INSTALL_DSO)

include $(top_builddir)/build/rules.mk

install-cAPIs-yes: $(SHARED_TARGETS)
	@$(MKINSTALLDIRS) $(DESTDIR)$(libexecdir)
	@list='$(shared)'; for i in $$list; do \
	  $(top_srcdir)/build/instdso.sh SH_LIBTOOL='$(SH_LIBTOOL)' $$i $(DESTDIR)$(libexecdir); \
	done

install-cAPIs-no:

