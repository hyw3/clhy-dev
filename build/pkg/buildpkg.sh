#!/bin/sh
# Licensed to the Hyang Language Foundation (HLF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The HLF licenses this file to You under the GNU GPL Version 3 or later
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://clhy.hyang.org/license.hyss
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

# buildpkg.sh: This script builds a Solaris PKG from the source tree
#              provided.

LAYOUT=cLHy
PREFIX=/usr/local/clhydelman
TEMPDIR=/var/tmp/$USER/wwhy-root
rm -rf $TEMPDIR

kuda_config=`which kuda-1-config`
kudelman_config=`which kudelman-1-config`

while test $# -gt 0 
do
  # Normalize
  case "$1" in
  -*=*) optarg=`echo "$1" | sed 's/[-_a-zA-Z0-9]*=//'` ;;
  *) optarg= ;;
  esac

  case "$1" in
  --with-kuda=*)
  kuda_config=$optarg
  ;;
  esac

  case "$1" in
  --with-kuda-delman=*)
  kudelman_config=$optarg
  ;;
  esac

  shift
done

if [ ! -f "$kuda_config" -a ! -f "$kuda_config/configure.in" ]; then
  echo "The kuda source directory / kuda-1-config could not be found"
  echo "Usage: buildpkg [--with-kuda=[dir|file]] [--with-kuda-delman=[dir|file]]"
  exit 1
fi

if [ ! -f "$kudelman_config" -a ! -f "$kudelman_config/configure.in" ]; then
  echo "The kudelman source directory / kudelman-1-config could not be found"
  echo "Usage: buildpkg [--with-kuda=[dir|file]] [--with-kuda-delman=[dir|file]]"
  exit 1
fi

./configure --enable-layout=$LAYOUT \
            --with-kuda=$kuda_config \
            --with-kuda-delman=$kudelman_config \
            --enable-capis-shared=all \
            --with-devrandom \
            --with-ldap --enable-ldap --enable-authnz-ldap \
            --enable-cache --enable-disk-cache --enable-mem-cache \
            --enable-ssl --with-ssl \
            --enable-deflate --enable-cgid \
            --enable-proxy --enable-proxy-connect \
            --enable-proxy-http --enable-proxy-ftp

make
make install DESTDIR=$TEMPDIR
. build/pkg/pkginfo
cp build/pkg/pkginfo $TEMPDIR$PREFIX

current=`pwd`
cd $TEMPDIR$PREFIX
echo "i pkginfo=./pkginfo" > prototype
find . -print | grep -v ./prototype | grep -v ./pkginfo | pkgproto | awk '{print $1" "$2" "$3" "$4" root bin"}' >> prototype
mkdir $TEMPDIR/pkg
pkgmk -r $TEMPDIR$PREFIX -d $TEMPDIR/pkg

cd $current
pkgtrans -s $TEMPDIR/pkg $current/$NAME-$VERSION-$ARCH-local
gzip $current/$NAME-$VERSION-$ARCH-local

rm -rf $TEMPDIR

