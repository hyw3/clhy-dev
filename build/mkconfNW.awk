# Licensed to the Hyang Language Foundation (HLF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The HLF licenses this file to You under the GNU GPL Version 3 or later
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://clhy.hyang.org/license.hyss
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

BEGIN {
    A["ServerRoot"] = "\${SRVROOT}"
    A["Port"] = PORT
    A["SSLPort"] = SSLPORT
    A["cgidir"] = "cgi-bin"
    A["logfiledir"] = "logs"
    A["htdocsdir"] = "htdocs"
    A["sysconfdir"] = "conf"
    A["iconsdir"] = "icons"
    A["manualdir"] = "manual"
    A["runtimedir"] = "logs"
    A["errordir"] = "error"
    A["proxycachedir"] = "proxy"

    B["htdocsdir"] = A["ServerRoot"]"/"A["htdocsdir"]
    B["iconsdir"] = A["ServerRoot"]"/"A["iconsdir"]
    B["manualdir"] = A["ServerRoot"]"/"A["manualdir"]
    B["errordir"] = A["ServerRoot"]"/"A["errordir"]
    B["proxycachedir"] = A["ServerRoot"]"/"A["proxycachedir"]
    B["cgidir"] = A["ServerRoot"]"/"A["cgidir"]
    B["logfiledir"] = A["logfiledir"]
    B["sysconfdir"] = A["sysconfdir"]
    B["runtimedir"] = A["runtimedir"]
}

/^ServerRoot / {
    print "Define SRVROOT \"SYS:/" BDIR "\""
    print ""
}
/@@ActivatecAPI@@/ {
    print "#ActivatecAPI access_compat_capi cAPIs/accesscompat.nlm"
    print "#ActivatecAPI actions_capi cAPIs/actions.nlm"
    print "#ActivatecAPI allowmethods_capi cAPIs/allowmethods.nlm"
    print "#ActivatecAPI auth_basic_capi cAPIs/authbasc.nlm"
    print "#ActivatecAPI auth_digest_capi cAPIs/authdigt.nlm"
    print "#ActivatecAPI authn_anon_capi cAPIs/authnano.nlm"
    print "#ActivatecAPI authn_dbd_capi cAPIs/authndbd.nlm"
    print "#ActivatecAPI authn_dbm_capi cAPIs/authndbm.nlm"
    print "#ActivatecAPI authn_file_capi cAPIs/authnfil.nlm"
    print "#ActivatecAPI authz_dbd_capi cAPIs/authzdbd.nlm"
    print "#ActivatecAPI authz_dbm_capi cAPIs/authzdbm.nlm"
    print "#ActivatecAPI authz_groupfile_capi cAPIs/authzgrp.nlm"
    print "#ActivatecAPI authz_user_capi cAPIs/authzusr.nlm"
    print "#ActivatecAPI authnz_ldap_capi cAPIs/authnzldap.nlm"
    print "#ActivatecAPI ldap_capi cAPIs/utilldap.nlm"
    print "#ActivatecAPI asis_capi cAPIs/capi_asis.nlm"
    print "ActivatecAPI autoindex_capi cAPIs/autoindex.nlm"
    print "#ActivatecAPI buffer_capi cAPIs/capibuffer.nlm"
    print "#ActivatecAPI cern_meta_capi cAPIs/cernmeta.nlm"
    print "ActivatecAPI cgi_capi cAPIs/capi_cgi.nlm"
    print "#ActivatecAPI data_capi cAPIs/capi_data.nlm"
    print "#ActivatecAPI dav_capi cAPIs/capi_dav.nlm"
    print "#ActivatecAPI dav_fs_capi cAPIs/capidavfs.nlm"
    print "#ActivatecAPI dav_lock_capi cAPIs/capidavlk.nlm"
    print "#ActivatecAPI expires_capi cAPIs/expires.nlm"
    print "#ActivatecAPI filter_capi cAPIs/capi_filter.nlm"
    print "#ActivatecAPI ext_filter_capi cAPIs/extfiltr.nlm"
    print "#ActivatecAPI file_cache_capi cAPIs/filecach.nlm"
    print "#ActivatecAPI headers_capi cAPIs/headers.nlm"
    print "#ActivatecAPI ident_capi cAPIs/capiident.nlm"
    print "#ActivatecAPI imagemap_capi cAPIs/imagemap.nlm"
    print "#ActivatecAPI info_capi cAPIs/info.nlm"
    print "#ActivatecAPI log_forensic_capi cAPIs/forensic.nlm"
    print "#ActivatecAPI logio_capi cAPIs/capilogio.nlm"
    print "#ActivatecAPI mime_magic_capi cAPIs/mimemagi.nlm"
    print "#ActivatecAPI proxy_capi cAPIs/proxy.nlm"
    print "#ActivatecAPI proxy_connect_capi cAPIs/proxycon.nlm"
    print "#ActivatecAPI proxy_http_capi cAPIs/proxyhtp.nlm"
    print "#ActivatecAPI proxy_ftp_capi cAPIs/proxyftp.nlm"
    print "#ActivatecAPI rewrite_capi cAPIs/rewrite.nlm"
    print "#ActivatecAPI speling_capi cAPIs/speling.nlm"
    print "#ActivatecAPI status_capi cAPIs/status.nlm"
    print "#ActivatecAPI unique_id_capi cAPIs/uniqueid.nlm"
    print "#ActivatecAPI usertrack_capi cAPIs/usertrk.nlm"
    print "#ActivatecAPI version_capi cAPIs/capiversion.nlm"
    print "#ActivatecAPI userdir_capi cAPIs/userdir.nlm"
    print "#ActivatecAPI vhost_alias_capi cAPIs/vhost.nlm"
    if (CAPISSL) {
       print "#ActivatecAPI socache_dbm_capi cAPIs/socachedbm.nlm"
       print "#ActivatecAPI socache_shmcb_capi cAPIs/socacheshmcb.nlm"
       print "#ActivatecAPI ssl_capi cAPIs/capi_ssl.nlm"
    }
    print ""
    next
}

match ($0,/^#SSLSessionCache +"dbm:/) {
    sub(/^#/, "")
}

match ($0,/^SSLSessionCache +"shmcb:/) {
    sub(/^SSLSessionCache/, "#SSLSessionCache")
}

match ($0,/^# Mutex +default +file:@rel_runtimedir@/) {
    sub(/file:@rel_runtimedir@/, "default")
}

match ($0,/@@.*@@/) {
    s=substr($0,RSTART+2,RLENGTH-4)
    sub(/@@.*@@/,A[s],$0)
}

match ($0,/@rel_.*@/) {
    s=substr($0,RSTART+5,RLENGTH-6)
    sub(/@rel_.*@/,A[s],$0)
}

match ($0,/@exp_.*@/) {
    s=substr($0,RSTART+5,RLENGTH-6)
    sub(/@exp_.*@/,B[s],$0)
}

match ($0,/@nonssl_.*@/) {
    s=substr($0,RSTART+8,RLENGTH-9)
    sub(/@nonssl_.*@/,B[s],$0)
}

match ($0,/^<IfcAPI cgid_capi>$/) {
    print "#"
    print "# CGIMapExtension: Technique for locating the interpreter for CGI scripts."
    print "# The special interpreter path \"PLATFORM\" can be used for NLM CGIs."
    print "#"
    print "#CGIMapExtension PLATFORM .cgi"
    print "CGIMapExtension SYS:/perl/Perlcgi/perlcgi.nlm .pl"
    print ""
}

{
    print
}

END {
    if ((ARGV[1] ~ /wwhy.conf.in/) && !BSDSKT) {
       print ""
       print "#"
       print "# SecureListen: Allows you to securely bind cLHy to specific IP addresses "
       print "# and/or ports (capi_nwssl)."
       print "#"
       print "# Change this to SecureListen on specific IP addresses as shown below to "
       print "# prevent cLHy from glomming onto all bound IP addresses (0.0.0.0)"
       print "#"
       print "#SecureListen "SSLPORT" \"SSL CertificateDNS\""
    }
    print ""
}
