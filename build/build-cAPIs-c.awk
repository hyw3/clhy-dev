# Licensed to the Hyang Language Foundation (HLF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The HLF licenses this file to You under the GNU GPL Version 3 or later
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://clhy.hyang.org/license.hyss
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
BEGIN {
    RS = " "
    # the core cAPI must come first
    cAPIs[n++] = "core"
    pcAPIs[pn++] = "core"
} 
{
    cAPIs[n] = $1;
    pcAPIs[pn] = $1;
    gsub("\n","",cAPIs[n]);
    gsub("\n","",pcAPIs[pn]);
    ++n;
    ++pn;
} 
END {
    print "/*"
    print " * cAPIs.c --- automatically generated by cLHy"
    print " * configuration script.  DO NOT HAND EDIT!!!!!"
    print " */"
    print ""
    print "#include \"clhy_config.h\""
    print "#include \"wwhy.h\""
    print "#include \"http_config.h\""
    print ""
    for (i = 0; i < pn; ++i) {
        printf ("extern cAPI %s_capi;\n", pcAPIs[i])
    }
    print ""
    print "/*"
    print " *  cAPIs which implicitly form the"
    print " *  list of activated cAPIs on startup,"
    print " *  i.e. these are the cAPIs which are"
    print " *  initially linked into the cLHy processing"
    print " *  [extendable under run-time via AddcAPI]"
    print " */"
    print "cAPI *clhy_prelinked_capis[] = {"
    for (i = 0 ; i < n; ++i) {
        printf "  &%s_capi,\n", cAPIs[i]
    }
    print "  NULL"
    print "};"
    print ""
    print "/*"
    print " *  We need the symbols as strings for <IfcAPI> containers"
    print " */"
    print ""
    print "clhy_capi_symbol_t clhy_prelinked_capi_symbols[] = {"
    for (i = 0; i < n; ++i) {
        printf ("  {\"%s_capi\", &%s_capi},\n", cAPIs[i], cAPIs[i])
    }
    print "  {NULL, NULL}"
    print "};"
    print ""
    print "/*"
    print " *  cAPIs which initially form the"
    print " *  list of available cAPIs on startup,"
    print " *  i.e. these are the cAPIs which are"
    print " *  initially loaded into the cLHy process"
    print " *  [extendable under run-time via ActivatecAPI]"
    print " */"
    print "cAPI *clhy_preactivated_capis[] = {"
    for (i = 0; i < pn; ++i) {
        printf "  &%s_capi,\n", pcAPIs[i]
    }
    print "  NULL"
    print "};"
    print ""
}
