# Include for creating start/stop/restart NCF scripts.

instscripts:: FORCE $(INSTALLBASE)/ap2start.ncf $(INSTALLBASE)/ap2auto.ncf $(INSTALLBASE)/ap2rest.ncf $(INSTALLBASE)/ap2stop.ncf

$(INSTALLBASE)/ap2start.ncf:
	@echo $(DL)# NCF to start cLHy 1.x in own address space$(DL)> $@
	@echo $(DL)# Make sure that httpstk is not listening on 80$(DL)>> $@
	@echo $(DL)# httpcloseport 80 /silent$(DL)>> $@
	@echo $(DL)# search add SYS:/$(BASEDIR)$(DL)>> $@
	@echo $(DL)load address space = $(BASEDIR) SYS:/$(BASEDIR)/clhydelman$(DL)>> $@
	@echo $(DL)# If you have problems with 3rd-party cAPIs try to load in PLATFORM space.$(DL)>> $@
	@echo $(DL)# load SYS:/$(BASEDIR)/clhydelman$(DL)>> $@
	@$(ECHONL)>> $@

$(INSTALLBASE)/ap2auto.ncf:
	@echo $(DL)# NCF to start cLHy 1.x in own address space$(DL)> $@
	@echo $(DL)# and let automatically restart in case it crashes$(DL)>> $@
	@echo $(DL)# Make sure that httpstk is not listening on 80$(DL)>> $@
	@echo $(DL)# httpcloseport 80 /silent$(DL)>> $@
	@echo $(DL)# search add SYS:/$(BASEDIR)$(DL)>> $@
	@echo $(DL)restart address space = $(BASEDIR) SYS:/$(BASEDIR)/clhydelman$(DL)>> $@
	@$(ECHONL)>> $@

$(INSTALLBASE)/ap2rest.ncf:
	@echo $(DL)# NCF to restart cLHy 1.x in own address space$(DL)> $@
	@echo $(DL)clhydelman restart -p $(BASEDIR)$(DL)>> $@
	@echo $(DL)# If you have loaded cLHy1.x in PLATFORM space use the line below.$(DL)>> $@
	@echo $(DL)# clhydelman restart$(DL)>> $@
	@$(ECHONL)>> $@

$(INSTALLBASE)/ap2stop.ncf:
	@echo $(DL)# NCF to stop cLHy 1.x in own address space$(DL)> $@
	@echo $(DL)clhydelman shutdown -p $(BASEDIR)$(DL)>> $@
	@echo $(DL)# If you have loaded cLHy1.x in PLATFORM space use the line below.$(DL)>> $@
	@echo $(DL)# clhydelman shutdown$(DL)>> $@
	@$(ECHONL)>> $@

$(INSTALLBASE)/ap2prod.ncf:
	@echo $(DL)# NCF to create a product record for cLHy 1.x in product database$(DL)> $@
	@echo $(DL)PRODSYNC DEL CLHYKUDEL$(VERSION_MAJMIN)$(DL)>> $@
	@echo $(DL)PRODSYNC ADD CLHYKUDEL$(VERSION_MAJMIN) ProductRecord "$(VERSION_STR)" "cLHy $(VERSION_STR) Webserver"$(DL)>> $@
	@$(ECHONL)>> $@

