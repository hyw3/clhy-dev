#
# This script is for cLHy version 1.6.x to rewrite the @@ServerRoot@@
# tags in wwhy.conf.in to original\wwhy.conf - then duplicate the
# conf files to the 'live' configuration if they don't already exist.
#
# Note that we -don't- want the ARGV file list, so no additional {} blocks
# are coded.  Use explicit args (more reliable on Win32) and use the fact
# that ARGV[] params are -not- '\' escaped to process the C:\Foo\Bar Win32
# path format.  Note that awk var=path would not succeed, since it -does-
# escape backslashes in the assignment.  Note also, a trailing space is
# required for paths, or the trailing quote following the backslash is
# escaped, rather than parsed.
#
BEGIN {
    domainname = ARGV[1];
    servername = ARGV[2];
    serveradmin = ARGV[3];
    serverport = ARGV[4];
    serversslport = ARGV[5];
    serverroot = ARGV[6];
    sourceroot = ARGV[7];

    delete ARGV[7];
    delete ARGV[6];
    delete ARGV[5];
    delete ARGV[4];
    delete ARGV[3];
    delete ARGV[2];
    delete ARGV[1];

    gsub( /\\/, "/", serverroot );
    gsub( /[ \/]+$/, "", serverroot );
    tstfl = serverroot "/logs/install.log"
    confroot = serverroot "/conf/";
    confdefault = confroot "original/";

    if ( sourceroot != "docs/conf/" ) {
        sourceroot = serverroot "/" sourceroot;
    }

    usertree = ENVIRON["USERPROFILE"]
    if ( usertree > "" ) {
        gsub( /\\/, "/", usertree );
        gsub( /\/[^\/]+$/, "", usertree );
    } else {
        usertree = "C:/Documents and Settings";
    }

    print "Installing cLHy HTTP Server 2.x with" >tstfl;
    print " DomainName    = " domainname >tstfl;
    print " ServerName    = " servername >tstfl;
    print " ServerAdmin   = " serveradmin >tstfl;
    print " ServerPort    = " serverport >tstfl;
    print " ServerSslPort = " serversslport >tstfl;
    print " ServerRoot    = " serverroot >tstfl;

    filelist["wwhy.conf"] = "wwhy.conf.in";
    filelist["wwhy-autoindex.conf"] = "wwhy-autoindex.conf.in";
    filelist["wwhy-dav.conf"] = "wwhy-dav.conf.in";
    filelist["wwhy-default.conf"] = "wwhy-default.conf.in";
    filelist["wwhy-info.conf"] = "wwhy-info.conf.in";
    filelist["wwhy-languages.conf"] = "wwhy-languages.conf.in";
    filelist["wwhy-manual.conf"] = "wwhy-manual.conf.in";
    filelist["wwhy-core.conf"] = "wwhy-core.conf.in";
    filelist["wwhy-multilang-errordoc.conf"] = "wwhy-multilang-errordoc.conf.in";
    filelist["wwhy-ssl.conf"] = "wwhy-ssl.conf.in";
    filelist["wwhy-userdir.conf"] = "wwhy-userdir.conf.in";
    filelist["wwhy-vhosts.conf"] = "wwhy-vhosts.conf.in";
    filelist["proxy-html.conf"] = "proxy-html.conf.in";

    for ( conffile in filelist ) {

      if ( conffile == "wwhy.conf" ) {
        srcfl = sourceroot filelist[conffile];
        dstfl = confdefault conffile;
        bswarning = 1;
      } else {
        srcfl = sourceroot "extra/" filelist[conffile];
        dstfl = confdefault "extra/" conffile;
        bswarning = 0;
      }

      while ( ( getline < srcfl ) > 0 ) {

        if ( bswarning && /^$/ ) {
          print "#" > dstfl;
          print "# NOTE: Where filenames are specified, you must use forward slashes" > dstfl;
          print "# instead of backslashes (e.g., \"c:/clhy\" instead of \"c:\\clhy\")." > dstfl;
          print "# If a drive letter is omitted, the drive on which wwhy.exe is located" > dstfl;
          print "# will be used by default.  It is recommended that you always supply" > dstfl;
          print "# an explicit drive letter in absolute paths to avoid confusion." > dstfl;
          bswarning = 0;
        }
        if ( /@@ActivatecAPI@@/ ) {
          print "ActivatecAPI access_compat_capi cAPIs/capi_access_compat.so" > dstfl;
          print "ActivatecAPI actions_capi cAPIs/capi_actions.so" > dstfl;
          print "ActivatecAPI alias_capi cAPIs/capi_alias.so" > dstfl;
          print "ActivatecAPI allowmethods_capi cAPIs/capi_allowmethods.so" > dstfl;
          print "ActivatecAPI asis_capi cAPIs/capi_asis.so" > dstfl;
          print "ActivatecAPI auth_basic_capi cAPIs/capi_auth_basic.so" > dstfl;
          print "#ActivatecAPI auth_digest_capi cAPIs/capi_auth_digest.so" > dstfl;
          print "#ActivatecAPI auth_form_capi cAPIs/capi_auth_form.so" > dstfl;
          print "#ActivatecAPI authn_anon_capi cAPIs/capi_authn_anon.so" > dstfl;
          print "ActivatecAPI authn_core_capi cAPIs/capi_authn_core.so" > dstfl;
          print "#ActivatecAPI authn_dbd_capi cAPIs/capi_authn_dbd.so" > dstfl;
          print "#ActivatecAPI authn_dbm_capi cAPIs/capi_authn_dbm.so" > dstfl;
          print "ActivatecAPI authn_file_capi cAPIs/capi_authn_file.so" > dstfl;
          print "#ActivatecAPI authn_socache_capi cAPIs/capi_authn_socache.so" > dstfl;
          print "#ActivatecAPI authnz_fcgi_capi cAPIs/capi_authnz_fcgi.so" > dstfl;
          print "#ActivatecAPI authnz_ldap_capi cAPIs/capi_authnz_ldap.so" > dstfl;
          print "ActivatecAPI authz_core_capi cAPIs/capi_authz_core.so" > dstfl;
          print "#ActivatecAPI authz_dbd_capi cAPIs/capi_authz_dbd.so" > dstfl;
          print "#ActivatecAPI authz_dbm_capi cAPIs/capi_authz_dbm.so" > dstfl;
          print "ActivatecAPI authz_groupfile_capi cAPIs/capi_authz_groupfile.so" > dstfl;
          print "ActivatecAPI authz_host_capi cAPIs/capi_authz_host.so" > dstfl;
          print "#ActivatecAPI authz_owner_capi cAPIs/capi_authz_owner.so" > dstfl;
          print "ActivatecAPI authz_user_capi cAPIs/capi_authz_user.so" > dstfl;
          print "ActivatecAPI autoindex_capi cAPIs/capi_autoindex.so" > dstfl;
          print "#ActivatecAPI brotli_capi cAPIs/capi_brotli.so" > dstfl;
          print "#ActivatecAPI buffer_capi cAPIs/capi_buffer.so" > dstfl;
          print "#ActivatecAPI cache_capi cAPIs/capi_cache.so" > dstfl;
          print "#ActivatecAPI cache_disk_capi cAPIs/capi_cache_disk.so" > dstfl;
          print "#ActivatecAPI cache_socache_capi cAPIs/capi_cache_socache.so" > dstfl;
          print "#ActivatecAPI cern_meta_capi cAPIs/capi_cern_meta.so" > dstfl;
          print "ActivatecAPI cgi_capi cAPIs/capi_cgi.so" > dstfl;
          print "#ActivatecAPI charset_lite_capi cAPIs/capi_charset_lite.so" > dstfl;
          print "#ActivatecAPI data_capi cAPIs/capi_data.so" > dstfl;
          print "#ActivatecAPI dav_capi cAPIs/capi_dav.so" > dstfl;
          print "#ActivatecAPI dav_fs_capi cAPIs/capi_dav_fs.so" > dstfl;
          print "#ActivatecAPI dav_lock_capi cAPIs/capi_dav_lock.so" > dstfl;
          print "#ActivatecAPI dbd_capi cAPIs/capi_dbd.so" > dstfl;
          print "#ActivatecAPI deflate_capi cAPIs/capi_deflate.so" > dstfl;
          print "ActivatecAPI dir_capi cAPIs/capi_dir.so" > dstfl;
          print "#ActivatecAPI dumpio_capi cAPIs/capi_dumpio.so" > dstfl;
          print "ActivatecAPI env_capi cAPIs/capi_env.so" > dstfl;
          print "#ActivatecAPI expires_capi cAPIs/capi_expires.so" > dstfl;
          print "#ActivatecAPI ext_filter_capi cAPIs/capi_ext_filter.so" > dstfl;
          print "#ActivatecAPI file_cache_capi cAPIs/capi_file_cache.so" > dstfl;
          print "#ActivatecAPI filter_capi cAPIs/capi_filter.so" > dstfl;
          print "#ActivatecAPI http2_capi cAPIs/capi_http2.so" > dstfl;
          print "#ActivatecAPI headers_capi cAPIs/capi_headers.so" > dstfl;
          print "#ActivatecAPI heartbeat_capi cAPIs/capi_heartbeat.so" > dstfl;
          print "#ActivatecAPI heartmonitor_capi cAPIs/capi_heartmonitor.so" > dstfl;
          print "#ActivatecAPI ident_capi cAPIs/capi_ident.so" > dstfl;
          print "#ActivatecAPI imagemap_capi cAPIs/capi_imagemap.so" > dstfl;
          print "ActivatecAPI include_capi cAPIs/capi_include.so" > dstfl;
          print "#ActivatecAPI info_capi cAPIs/capi_info.so" > dstfl;
          print "ActivatecAPI isapi_capi cAPIs/capi_isapi.so" > dstfl;
          print "#ActivatecAPI lbmethod_bybusyness_capi cAPIs/capi_lbmethod_bybusyness.so" > dstfl;
          print "#ActivatecAPI lbmethod_byrequests_capi cAPIs/capi_lbmethod_byrequests.so" > dstfl;
          print "#ActivatecAPI lbmethod_bytraffic_capi cAPIs/capi_lbmethod_bytraffic.so" > dstfl;
          print "#ActivatecAPI lbmethod_heartbeat_capi cAPIs/capi_lbmethod_heartbeat.so" > dstfl;
          print "#ActivatecAPI ldap_capi cAPIs/capi_ldap.so" > dstfl;
          print "#ActivatecAPI logio_capi cAPIs/capi_logio.so" > dstfl;
          print "ActivatecAPI log_config_capi cAPIs/capi_log_config.so" > dstfl;
          print "#ActivatecAPI log_debug_capi cAPIs/capi_log_debug.so" > dstfl;
          print "#ActivatecAPI log_forensic_capi cAPIs/capi_log_forensic.so" > dstfl;
          print "#ActivatecAPI lua_capi cAPIs/capi_lua.so" > dstfl;
          print "#ActivatecAPI macro_capi cAPIs/capi_macro.so" > dstfl;
          print "#ActivatecAPI clmd_capi cAPIs/capi_clmd.so" > dstfl;
          print "ActivatecAPI mime_capi cAPIs/capi_mime.so" > dstfl;
          print "#ActivatecAPI mime_magic_capi cAPIs/capi_mime_magic.so" > dstfl;
          print "ActivatecAPI negotiation_capi cAPIs/capi_negotiation.so" > dstfl;
          print "#ActivatecAPI proxy_capi cAPIs/capi_proxy.so" > dstfl;
          print "#ActivatecAPI proxy_ajp_capi cAPIs/capi_proxy_ajp.so" > dstfl;
          print "#ActivatecAPI proxy_balancer_capi cAPIs/capi_proxy_balancer.so" > dstfl;
          print "#ActivatecAPI proxy_connect_capi cAPIs/capi_proxy_connect.so" > dstfl;
          print "#ActivatecAPI proxy_express_capi cAPIs/capi_proxy_express.so" > dstfl;
          print "#ActivatecAPI proxy_fcgi_capi cAPIs/capi_proxy_fcgi.so" > dstfl;
          print "#ActivatecAPI proxy_ftp_capi cAPIs/capi_proxy_ftp.so" > dstfl;
          print "#ActivatecAPI proxy_hcheck_capi cAPIs/capi_proxy_hcheck.so" > dstfl;
          print "#ActivatecAPI proxy_html_capi cAPIs/capi_proxy_html.so" > dstfl;
          print "#ActivatecAPI proxy_http_capi cAPIs/capi_proxy_http.so" > dstfl;
          print "#ActivatecAPI proxy_http2_capi cAPIs/capi_proxy_http2.so" > dstfl;
          print "#ActivatecAPI proxy_scgi_capi cAPIs/capi_proxy_scgi.so" > dstfl;
          print "#ActivatecAPI proxy_uwsgi_capi cAPIs/capi_proxy_uwsgi.so" > dstfl;
          print "#ActivatecAPI proxy_wstunnel_capi cAPIs/capi_proxy_wstunnel.so" > dstfl;
          print "#ActivatecAPI ratelimit_capi cAPIs/capi_ratelimit.so" > dstfl;
          print "#ActivatecAPI reflector_capi cAPIs/capi_reflector.so" > dstfl;
          print "#ActivatecAPI remoteip_capi cAPIs/capi_remoteip.so" > dstfl;
          print "#ActivatecAPI request_capi cAPIs/capi_request.so" > dstfl;
          print "#ActivatecAPI reqtimeout_capi cAPIs/capi_reqtimeout.so" > dstfl;
          print "#ActivatecAPI rewrite_capi cAPIs/capi_rewrite.so" > dstfl;
          print "#ActivatecAPI sed_capi cAPIs/capi_sed.so" > dstfl;
          print "#ActivatecAPI session_capi cAPIs/capi_session.so" > dstfl;
          print "#ActivatecAPI session_cookie_capi cAPIs/capi_session_cookie.so" > dstfl;
          print "#ActivatecAPI session_crypto_capi cAPIs/capi_session_crypto.so" > dstfl;
          print "#ActivatecAPI session_dbd_capi cAPIs/capi_session_dbd.so" > dstfl;
          print "ActivatecAPI setenvif_capi cAPIs/capi_setenvif.so" > dstfl;
          print "#ActivatecAPI slotmem_plain_capi cAPIs/capi_slotmem_plain.so" > dstfl;
          print "#ActivatecAPI slotmem_shm_capi cAPIs/capi_slotmem_shm.so" > dstfl;
          print "#ActivatecAPI socache_dbm_capi cAPIs/capi_socache_dbm.so" > dstfl;
          print "#ActivatecAPI socache_memcache_capi cAPIs/capi_socache_memcache.so" > dstfl;
          print "#ActivatecAPI socache_redis_capi cAPIs/capi_socache_redis.so" > dstfl;
          print "#ActivatecAPI socache_shmcb_capi cAPIs/capi_socache_shmcb.so" > dstfl;
          print "#ActivatecAPI speling_capi cAPIs/capi_speling.so" > dstfl;
          print "#ActivatecAPI ssl_capi cAPIs/capi_ssl.so" > dstfl;
          print "#ActivatecAPI status_capi cAPIs/capi_status.so" > dstfl;
          print "#ActivatecAPI substitute_capi cAPIs/capi_substitute.so" > dstfl;
          print "#ActivatecAPI unique_id_capi cAPIs/capi_unique_id.so" > dstfl;
          print "#ActivatecAPI userdir_capi cAPIs/capi_userdir.so" > dstfl;
          print "#ActivatecAPI usertrack_capi cAPIs/capi_usertrack.so" > dstfl;
          print "#ActivatecAPI version_capi cAPIs/capi_version.so" > dstfl;
          print "#ActivatecAPI vhost_alias_capi cAPIs/capi_vhost_alias.so" > dstfl;
          print "#ActivatecAPI watchdog_capi cAPIs/capi_watchdog.so" > dstfl;
          print "#ActivatecAPI xml2enc_capi cAPIs/capi_xml2enc.so" > dstfl;
          continue;
        }
        gsub( /@@ServerRoot@@/,   serverroot );
        gsub( /@exp_cgidir@/,     serverroot "/cgi-bin" );
        gsub( /@exp_sysconfdir@/, serverroot "/conf" );
        gsub( /@exp_errordir@/,   serverroot "/error" );
        gsub( /@exp_htdocsdir@/,  serverroot "/htdocs" );
        gsub( /@exp_iconsdir@/,   serverroot "/icons" );
        gsub( /@exp_manualdir@/,  serverroot "/manual" );
        gsub( /@exp_runtimedir@/, serverroot "/logs" );
        if ( gsub( /@exp_logfiledir@/, serverroot "/logs" ) ||
             gsub( /@rel_logfiledir@/, "logs" ) ) {
          gsub( /_log"/, ".log\"" )
        }
        gsub( /@rel_runtimedir@/, "logs" );
        gsub( /@rel_sysconfdir@/, "conf" );
        gsub( /\/home\/\*\/public_html/, \
              usertree "/*/My Documents/My Website" );
        gsub( /UserDir public_html/, "UserDir \"My Documents/My Website\"" );
        gsub( /@@ServerName@@|www.example.com/,  servername );
        gsub( /@@ServerAdmin@@|you@example.com/, serveradmin );
        gsub( /@@DomainName@@|example.com/,      domainname );
        gsub( /@@Port@@/,                        serverport );
        gsub( /@@SSLPort@@|443/,                 serversslport );
        print $0 > dstfl;
      }
      close(srcfl);

      if ( close(dstfl) >= 0 ) {
        print "Rewrote " srcfl "\n to " dstfl > tstfl;
        if ( sourceroot != "docs/conf/" ) {
          gsub(/\//, "\\", srcfl);
          if (system("del 2>NUL \"" srcfl "\"")) {
            print "Failed to remove " srcfl > tstfl;
          } else {
            print "Successfully removed " srcfl > tstfl;
          }
        }
      } else {
        print "Failed to rewrite " srcfl "\n to " dstfl > tstfl;
      }
      filelist[conffile] = "extra/";
    }

    filelist["wwhy.conf"] = "";
    filelist["charset.conv"] = "";
    filelist["magic"] = "";
    filelist["mime.types"] = "";

    for ( conffile in filelist ) {
      srcfl = confdefault filelist[conffile] conffile;
      dstfl = confroot filelist[conffile] conffile;
      if ( ( getline < dstfl ) < 0 ) {
        while ( ( getline < srcfl ) > 0 ) {
          print $0 > dstfl;
        }
        print "Duplicated " srcfl "\n to " dstfl > tstfl;
      } else {
        print "Existing file " dstfl " preserved" > tstfl;
      }
      close(srcfl);
      close(dstfl);
    }

    if ( sourceroot != "docs/conf/" ) {
      srcfl = confdefault "installwinconf.awk";
      gsub(/\//, "\\", srcfl);
      if (system("del 2>NUL \"" srcfl "\"")) {
        print "Failed to remove " srcfl > tstfl;
      } else {
        print "Successfully removed " srcfl > tstfl;
      }
    }
    close(tstfl);
}

