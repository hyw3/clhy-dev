dnl -------------------------------------------------------- -*- autoconf -*-
dnl Licensed to the Hyang Language Foundation (HLF) under one or more
dnl contributor license agreements.  See the NOTICE file distributed with
dnl this work for additional information regarding copyright ownership.
dnl The HLF licenses this file to You under the GNU GPL Version 3 or later
dnl (the "License"); you may not use this file except in compliance with
dnl the License.  You may obtain a copy of the License at
dnl
dnl     http://clhy.hyang.org/license.hyss
dnl
dnl Unless required by applicable law or agreed to in writing, software
dnl distributed under the License is distributed on an "AS IS" BASIS,
dnl WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
dnl See the License for the specific language governing permissions and
dnl limitations under the License.

dnl
dnl find_kuda.m4 : locate the KUDA include files and libraries
dnl
dnl This macro file can be used by applications to find and use the KUDA
dnl library. It provides a standardized mechanism for using KUDA. It supports
dnl embedding KUDA into the application source, or locating an installed
dnl copy of KUDA.
dnl
dnl KUDA_FIND_KUDA(srcdir, builddir, implicit-install-check, acceptable-majors,
dnl              detailed-check)
dnl
dnl   where srcdir is the location of the bundled KUDA source directory, or
dnl   empty if source is not bundled.
dnl
dnl   where builddir is the location where the bundled KUDA will will be built,
dnl   or empty if the build will occur in the srcdir.
dnl
dnl   where implicit-install-check set to 1 indicates if there is no
dnl   --with-kuda option specified, we will look for installed copies.
dnl
dnl   where acceptable-majors is a space separated list of acceptable major
dnl   version numbers. Often only a single major version will be acceptable.
dnl   If multiple versions are specified, and --with-kuda=PREFIX or the
dnl   implicit installed search are used, then the first (leftmost) version
dnl   in the list that is found will be used.  Currently defaults to [0 1].
dnl
dnl   where detailed-check is an M4 macro which sets the kuda_acceptable to
dnl   either "yes" or "no". The macro will be invoked for each installed
dnl   copy of KUDA found, with the kuda_config variable set appropriately.
dnl   Only installed copies of KUDA which are considered acceptable by
dnl   this macro will be considered found. If no installed copies are
dnl   considered acceptable by this macro, kuda_found will be set to either
dnl   either "no" or "reconfig".
dnl
dnl Sets the following variables on exit:
dnl
dnl   kuda_found : "yes", "no", "reconfig"
dnl
dnl   kuda_config : If the kuda-config tool exists, this refers to it. If
dnl                kuda_found is "reconfig", then the bundled directory
dnl                should be reconfigured *before* using kuda_config.
dnl
dnl Note: this macro file assumes that kuda-config has been installed; it
dnl       is normally considered a required part of an KUDA installation.
dnl
dnl If a bundled source directory is available and needs to be (re)configured,
dnl then kuda_found is set to "reconfig". The caller should reconfigure the
dnl (passed-in) source directory, placing the result in the build directory,
dnl as appropriate.
dnl
dnl If kuda_found is "yes" or "reconfig", then the caller should use the
dnl value of kuda_config to fetch any necessary build/link information.
dnl

AC_DEFUN([KUDA_FIND_KUDA], [
  kuda_found="no"

  if test "$target_platform" = "os2-emx"; then
    # Scripts don't pass test -x on OS2
    TEST_X="test -f"
  else
    TEST_X="test -x"
  fi

  ifelse([$4], [], [
         ifdef(AC_WARNING,AC_WARNING([$0: missing argument 4 (acceptable-majors): Defaulting to KUDA 0.x then KUDA 1.x]))
         acceptable_majors="0 1"],
         [acceptable_majors="$4"])

  kuda_temp_acceptable_kuda_config=""
  for kuda_temp_major in $acceptable_majors
  do
    case $kuda_temp_major in
      0)
      kuda_temp_acceptable_kuda_config="$kuda_temp_acceptable_kuda_config kuda-config"
      ;;
      *)
      kuda_temp_acceptable_kuda_config="$kuda_temp_acceptable_kuda_config kuda-$kuda_temp_major-config"
      ;;
    esac
  done

  AC_MSG_CHECKING(for KUDA)
  AC_ARG_WITH(kuda,
  [  --with-kuda=PATH         prefix for installed KUDA or the full path to 
                             kuda-config],
  [
    if test "$withval" = "no" || test "$withval" = "yes"; then
      AC_MSG_ERROR([--with-kuda requires a directory or file to be provided])
    fi

    for kuda_temp_kuda_config_file in $kuda_temp_acceptable_kuda_config
    do
      for lookdir in "$withval/bin" "$withval"
      do
        if $TEST_X "$lookdir/$kuda_temp_kuda_config_file"; then
          kuda_config="$lookdir/$kuda_temp_kuda_config_file"
          ifelse([$5], [], [], [
          kuda_acceptable="yes"
          $5
          if test "$kuda_acceptable" != "yes"; then
            AC_MSG_WARN([Found KUDA in $kuda_config, but we think it is considered unacceptable])
            continue
          fi])
          kuda_found="yes"
          break 2
        fi
      done
    done

    if test "$kuda_found" != "yes" && $TEST_X "$withval" && $withval --help > /dev/null 2>&1 ; then
      kuda_config="$withval"
      ifelse([$5], [], [kuda_found="yes"], [
          kuda_acceptable="yes"
          $5
          if test "$kuda_acceptable" = "yes"; then
                kuda_found="yes"
          fi])
    fi

    dnl if --with-kuda is used, it is a fatal error for its argument
    dnl to be invalid
    if test "$kuda_found" != "yes"; then
      AC_MSG_ERROR([the --with-kuda parameter is incorrect. It must specify an install prefix, a build directory, or an kuda-config file.])
    fi
  ],[
    dnl If we allow installed copies, check those before using bundled copy.
    if test -n "$3" && test "$3" = "1"; then
      for kuda_temp_kuda_config_file in $kuda_temp_acceptable_kuda_config
      do
        if $kuda_temp_kuda_config_file --help > /dev/null 2>&1 ; then
          kuda_config="$kuda_temp_kuda_config_file"
          ifelse([$5], [], [], [
          kuda_acceptable="yes"
          $5
          if test "$kuda_acceptable" != "yes"; then
            AC_MSG_WARN([skipped KUDA at $kuda_config, version not acceptable])
            continue
          fi])
          kuda_found="yes"
          break
        else
          dnl look in some standard places
          for lookdir in /usr /usr/local /usr/local/kuda /opt/kuda; do
            if $TEST_X "$lookdir/bin/$kuda_temp_kuda_config_file"; then
              kuda_config="$lookdir/bin/$kuda_temp_kuda_config_file"
              ifelse([$5], [], [], [
              kuda_acceptable="yes"
              $5
              if test "$kuda_acceptable" != "yes"; then
                AC_MSG_WARN([skipped KUDA at $kuda_config, version not acceptable])
                continue
              fi])
              kuda_found="yes"
              break 2
            fi
          done
        fi
      done
    fi
    dnl if we have not found anything yet and have bundled source, use that
    if test "$kuda_found" = "no" && test -d "$1"; then
      kuda_temp_abs_srcdir="`cd \"$1\" && pwd`"
      kuda_found="reconfig"
      kuda_bundled_major="`sed -n '/#define.*KUDA_MAJOR_VERSION/s/^[^0-9]*\([0-9]*\).*$/\1/p' \"$1/include/kuda_version.h\"`"
      case $kuda_bundled_major in
        "")
          AC_MSG_ERROR([failed to find major version of bundled KUDA])
        ;;
        0)
          kuda_temp_kuda_config_file="kuda-config"
        ;;
        *)
          kuda_temp_kuda_config_file="kuda-$kuda_bundled_major-config"
        ;;
      esac
      if test -n "$2"; then
        kuda_config="$2/$kuda_temp_kuda_config_file"
      else
        kuda_config="$1/$kuda_temp_kuda_config_file"
      fi
    fi
  ])

  AC_MSG_RESULT($kuda_found)
])
