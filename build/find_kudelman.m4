dnl -------------------------------------------------------- -*- autoconf -*-
dnl Licensed to the Hyang Language Foundation (HLF) under one or more
dnl contributor license agreements.  See the NOTICE file distributed with
dnl this work for additional information regarding copyright ownership.
dnl The HLF licenses this file to You under the GNU GPL Version 3 or later
dnl (the "License"); you may not use this file except in compliance with
dnl the License.  You may obtain a copy of the License at
dnl
dnl     http://clhy.hyang.org/license.hyss
dnl
dnl Unless required by applicable law or agreed to in writing, software
dnl distributed under the License is distributed on an "AS IS" BASIS,
dnl WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
dnl See the License for the specific language governing permissions and
dnl limitations under the License.

dnl
dnl find_kudelman.m4 : locate the Kuda-Delman (KUDELMAN) include files and libraries
dnl
dnl This macro file can be used by applications to find and use the KUDELMAN
dnl library. It provides a standardized mechanism for using KUDELMAN. It supports
dnl embedding KUDELMAN into the application source, or locating an installed
dnl copy of KUDELMAN.
dnl
dnl KUDA_FIND_KUDELMAN(srcdir, builddir, implicit-install-check, acceptable-majors,
dnl              detailed-check)
dnl
dnl   where srcdir is the location of the bundled KUDELMAN source directory, or
dnl   empty if source is not bundled.
dnl
dnl   where builddir is the location where the bundled KUDELMAN will be built,
dnl   or empty if the build will occur in the srcdir.
dnl
dnl   where implicit-install-check set to 1 indicates if there is no
dnl   --with-kuda-delman option specified, we will look for installed copies.
dnl
dnl   where acceptable-majors is a space separated list of acceptable major
dnl   version numbers. Often only a single major version will be acceptable.
dnl   If multiple versions are specified, and --with-kuda-delman=PREFIX or the
dnl   implicit installed search are used, then the first (leftmost) version
dnl   in the list that is found will be used.  Currently defaults to [0 1].
dnl
dnl   where detailed-check is an M4 macro which sets the kudelman_acceptable to
dnl   either "yes" or "no". The macro will be invoked for each installed
dnl   copy of KUDELMAN found, with the kudelman_config variable set appropriately.
dnl   Only installed copies of KUDELMAN which are considered acceptable by
dnl   this macro will be considered found. If no installed copies are
dnl   considered acceptable by this macro, kudelman_found will be set to either
dnl   either "no" or "reconfig".
dnl
dnl Sets the following variables on exit:
dnl
dnl   kudelman_found : "yes", "no", "reconfig"
dnl
dnl   kudelman_config : If the kudelman-config tool exists, this refers to it.  If
dnl                kudelman_found is "reconfig", then the bundled directory
dnl                should be reconfigured *before* using kudelman_config.
dnl
dnl Note: this macro file assumes that kuda-config has been installed; it
dnl       is normally considered a required part of an KUDA installation.
dnl
dnl Note: At this time, we cannot find *both* a source dir and a build dir.
dnl       If both are available, the build directory should be passed to
dnl       the --with-kuda-delman switch.
dnl
dnl Note: the installation layout is presumed to follow the standard
dnl       PREFIX/lib and PREFIX/include pattern. If the KUDELMAN config file
dnl       is available (and can be found), then non-standard layouts are
dnl       possible, since it will be described in the config file.
dnl
dnl If a bundled source directory is available and needs to be (re)configured,
dnl then kudelman_found is set to "reconfig". The caller should reconfigure the
dnl (passed-in) source directory, placing the result in the build directory,
dnl as appropriate.
dnl
dnl If kudelman_found is "yes" or "reconfig", then the caller should use the
dnl value of kudelman_config to fetch any necessary build/link information.
dnl

AC_DEFUN([KUDA_FIND_KUDELMAN], [
  kudelman_found="no"

  if test "$target_platform" = "os2-emx"; then
    # Scripts don't pass test -x on OS2
    TEST_X="test -f"
  else
    TEST_X="test -x"
  fi

  ifelse([$4], [],
  [
    ifdef(AC_WARNING,([$0: missing argument 4 (acceptable-majors): Defaulting to KUDELMAN 0.x then KUDELMAN 1.x]))
    acceptable_majors="0 1"
  ], [acceptable_majors="$4"])

  kudelman_temp_acceptable_kudelman_config=""
  for kudelman_temp_major in $acceptable_majors
  do
    case $kudelman_temp_major in
      0)
      kudelman_temp_acceptable_kudelman_config="$kudelman_temp_acceptable_kudelman_config kudelman-config"
      ;;
      *)
      kudelman_temp_acceptable_kudelman_config="$kudelman_temp_acceptable_kudelman_config kudelman-$kudelman_temp_major-config"
      ;;
    esac
  done

  AC_MSG_CHECKING(for Kuda-Delman)
  AC_ARG_WITH(kuda-delman,
  [  --with-kuda-delman=PATH    prefix for installed KUDELMAN or the full path to 
                             kudelman-config],
  [
    if test "$withval" = "no" || test "$withval" = "yes"; then
      AC_MSG_ERROR([--with-kuda-delman requires a directory or file to be provided])
    fi

    for kudelman_temp_kudelman_config_file in $kudelman_temp_acceptable_kudelman_config
    do
      for lookdir in "$withval/bin" "$withval"
      do
        if $TEST_X "$lookdir/$kudelman_temp_kudelman_config_file"; then
          kudelman_config="$lookdir/$kudelman_temp_kudelman_config_file"
          ifelse([$5], [], [], [
          kudelman_acceptable="yes"
          $5
          if test "$kudelman_acceptable" != "yes"; then
            AC_MSG_WARN([Found KUDELMAN in $kudelman_config, but it is considered unacceptable])
            continue
          fi])
          kudelman_found="yes"
          break 2
        fi
      done
    done

    if test "$kudelman_found" != "yes" && $TEST_X "$withval" && $withval --help > /dev/null 2>&1 ; then
      kudelman_config="$withval"
      ifelse([$5], [], [kudelman_found="yes"], [
          kudelman_acceptable="yes"
          $5
          if test "$kudelman_acceptable" = "yes"; then
                kudelman_found="yes"
          fi])
    fi

    dnl if --with-kuda-delman is used, it is a fatal error for its argument
    dnl to be invalid
    if test "$kudelman_found" != "yes"; then
      AC_MSG_ERROR([the --with-kuda-delman parameter is incorrect. It must specify an install prefix, a build directory, or an kudelman-config file.])
    fi
  ],[
    if test -n "$3" && test "$3" = "1"; then
      for kudelman_temp_kudelman_config_file in $kudelman_temp_acceptable_kudelman_config
      do
        if $kudelman_temp_kudelman_config_file --help > /dev/null 2>&1 ; then
          kudelman_config="$kudelman_temp_kudelman_config_file" 
          ifelse([$5], [], [], [
          kudelman_acceptable="yes"
          $5
          if test "$kudelman_acceptable" != "yes"; then
            AC_MSG_WARN([skipped Kuda-Delman at $kudelman_config, version not acceptable])
            continue
          fi])
          kudelman_found="yes"
          break
        else
          dnl look in some standard places (apparently not in builtin/default)
          for lookdir in /usr /usr/local /usr/local/kuda /opt/kuda; do
            if $TEST_X "$lookdir/bin/$kudelman_temp_kudelman_config_file"; then
              kudelman_config="$lookdir/bin/$kudelman_temp_kudelman_config_file"
              ifelse([$5], [], [], [
              kudelman_acceptable="yes"
              $5
              if test "$kudelman_acceptable" != "yes"; then
                AC_MSG_WARN([skipped Kuda-Delman at $kudelman_config, version not acceptable])
                continue
              fi])
              kudelman_found="yes"
              break 2
            fi
          done
        fi
      done
    fi
    dnl if we have not found anything yet and have bundled source, use that
    if test "$kudelman_found" = "no" && test -d "$1"; then
      kudelman_temp_abs_srcdir="`cd \"$1\" && pwd`"
      kudelman_found="reconfig"
      kudelman_bundled_major="`sed -n '/#define.*KUDELMAN_MAJOR_VERSION/s/^[^0-9]*\([0-9]*\).*$/\1/p' \"$1/include/kudelman_version.h\"`"
      case $kudelman_bundled_major in
        "")
          AC_MSG_ERROR([failed to find major version of bundled KUDELMAN])
        ;;
        0)
          kudelman_temp_kudelman_config_file="kudelman-config"
        ;;
        *)
          kudelman_temp_kudelman_config_file="kudelman-$kudelman_bundled_major-config"
        ;;
      esac
      if test -n "$2"; then
        kudelman_config="$2/$kudelman_temp_kudelman_config_file"
      else
        kudelman_config="$1/$kudelman_temp_kudelman_config_file"
      fi
    fi
  ])

  AC_MSG_RESULT($kudelman_found)
])
