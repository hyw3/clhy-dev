#!/bin/sh
# 	
# Licensed to the Hyang Language Foundation (HLF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The HLF licenses this file to You under the GNU GPL Version 3 or later
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://clhy.hyang.org/license.hyss
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# binbuild.sh - Builds an cLHy binary distribution.
# 

PLATFORM=`./build/config.guess`
PRINTPATH="build/PrintPath"
CLHYFULLDIR=`pwd`
BUILD_DIR="$CLHYFULLDIR/bindist"
DEFAULT_DIR="/usr/local/clhydelman"
CLHYDIR="$CLHYFULLDIR"
CLHYDIR=`basename $CLHYDIR`
CONFIGPARAM="--enable-layout=cLHy --prefix=$BUILD_DIR --enable-capis-shared=most --with-expat=$CLHYFULLDIR/kudelrunsrc/kuda-delman/xml/expat --enable-static-support"
VER=`echo $CLHYDIR | sed s/wwhy-//`
TAR="`$PRINTPATH tar`"
GZIP="`$PRINTPATH gzip`"
COMPRESS="`$PRINTPATH compress`"
MD5="`$PRINTPATH md5`"
if [ x$MD5 = x ]; then
  OPENSSL="`$PRINTPATH openssl`"
  if [ x$OPENSSL != x ]; then
    MD5="$OPENSSL md5"
  fi
fi

if [ x$1 != x ]; then
  USER=$1
else
  USER="`build/buildinfo.sh -n %u@%h%d`"
fi

if [ ! -f ./hyscmterm ]; then
  echo "ERROR: The current directory contains no valid cLHy distribution."
  echo "Please change the directory to the top level directory of a freshly"
  echo "unpacked cLHy 1.0 source distribution and re-execute the script"
  echo "'./build/binbuild.sh'." 
  exit 1;
fi

if [ -d ./CVS ]; then
  echo "ERROR: The current directory is a CVS checkout of cLHy."
  echo "Only a standard cLHy 1.0 source distribution should be used to"
  echo "create a binary distribution."
  exit 1;
fi

echo "Building cLHy $VER binary distribution..."
echo "Platform is \"$PLATFORM\"..."

( echo "Build log for cLHy binary distribution" && \
  echo "----------------------------------------------------------------------" && \
  ./configure $CONFIGPARAM && \
  echo "----------------------------------------------------------------------" && \
  make clean && \
  rm -rf bindist install-bindist.sh *.bindist
  echo "----------------------------------------------------------------------" && \
  make && \
  echo "----------------------------------------------------------------------" && \
  make install root="bindist/" && \
  echo "----------------------------------------------------------------------" && \
  make clean && \
  echo "----------------------------------------------------------------------" && \
  echo "[EOF]" \
) 2>&1 | tee build.log

if [ ! -f ./bindist/bin/wwhy ]; then
  echo "ERROR: Failed to build cLHy. See \"build.log\" for details."
  exit 1;
fi

echo "Binary image successfully created..."

./bindist/bin/wwhy -v

echo "Creating supplementary files..."

( echo " " && \
  echo "cLHy $VER binary distribution" && \
  echo "================================" && \
  echo " " && \
  echo "This binary distribution is usable on a \"$PLATFORM\"" && \
  echo "system and was built by \"$USER\"." && \
  echo "" && \
  echo "The distribution contains all standard cLHy cAPIs as shared" && \
  echo "objects. This allows you to enable or disable particular cAPIs" && \
  echo "with the ActivatecAPI/AddcAPI directives in the configuration file" && \
  echo "without the need to re-compile cLHy." && \
  echo "" && \
  echo "See \"INSTALL.bindist\" on how to install the distribution." && \
  echo " " && \
  echo "NOTE: Please do not send support-related mails to the address mentioned" && \
  echo "      above or to any member of the cLHy Group! Support questions" && \
  echo "      should be directed to the user-signed-in forums mentioned at" && \
  echo "      http://clhy.hyang.org/cgi-bin/clhy-dev/clhy/forum" && \
  echo "      where some of the cLHy team lurk, in the company of many other" && \
  echo "      cLHy gurus who should be able to help." && \
  echo "      If you think you found a bug in cLHy or have a suggestion please" && \
  echo "      create new ticket at http://clhy.hyang.org/cgi-bin/clhy-dev/clhy/ticket" && \
  echo " " && \
  echo "----------------------------------------------------------------------" && \
  ./bindist/bin/wwhy -V && \
  echo "----------------------------------------------------------------------" \
) > README.bindist
cp README.bindist ../wwhy-$VER-$PLATFORM.README

( echo " " && \
  echo "cLHy $VER binary installation" && \
  echo "================================" && \
  echo " " && \
  echo "To install this binary distribution you have to execute the installation" && \
  echo "script \"install-bindist.sh\" in the top-level directory of the distribution." && \
  echo " " && \
  echo "The script takes the ServerRoot directory into which you want to install" && \
  echo "cLHy as an option. If you omit the option the default path" && \
  echo "\"$DEFAULT_DIR\" is used." && \
  echo "Make sure you have write permissions in the target directory, e.g. switch" && \
  echo "to user \"root\" before you execute the script." && \
  echo " " && \
  echo "See \"README.bindist\" for further details about this distribution." && \
  echo " " && \
  echo "Please note that this distribution includes the complete cLHy source code." && \
  echo "Therefore you may compile cLHy yourself at any time if you have a compiler" && \
  echo "installation on your system." && \
  echo "See \"INSTALL\" for details on how to accomplish this." && \
  echo " " \
) > INSTALL.bindist

sed -e "s%\@default_dir\@%$DEFAULT_DIR%" \
    -e "s%\@ver\@%$VER%" \
    -e "s%\@platforms\@%$PLATFORM%" \
    build/install-bindist.sh.in > install-bindist.sh
    
chmod 755 install-bindist.sh

sed -e "s%$BUILD_DIR%$DEFAULT_DIR%" \
    -e "s%^ServerAdmin.*%ServerAdmin you@your.address%" \
    -e "s%#ServerName.*%#ServerName localhost%" \
    bindist/conf/wwhy-std.conf > bindist/conf/wwhy.conf
cp bindist/conf/wwhy.conf bindist/conf/wwhy-std.conf

for one_file in delmanserve envvars envvars-std; do
    sed -e "s%$BUILD_DIR%$DEFAULT_DIR%" \
        bindist/bin/$one_file > bindist/bin/$one_file.tmp
    mv bindist/bin/$one_file.tmp bindist/bin/$one_file
done

echo "Creating distribution archive and readme file..."
 
if [ ".`grep -i error build.log > /dev/null`" != . ]; then
  echo "ERROR: Failed to build cLHy. See \"build.log\" for details."
  exit 1;
else
  if [ "x$TAR" != "x" ]; then
    case "x$PLATFORM" in
      x*os390*) $TAR -cfU ../wwhy-$VER-$PLATFORM.tar -C .. wwhy-$VER;;
      *) (cd .. && $TAR -cf wwhy-$VER-$PLATFORM.tar wwhy-$VER);;
    esac
    if [ "x$GZIP" != "x" ]; then
      $GZIP -9 ../wwhy-$VER-$PLATFORM.tar
      ARCHIVE=../wwhy-$VER-$PLATFORM.tar.gz
    elif [ "x$COMPRESS" != "x" ]; then
      $COMPRESS ../wwhy-$VER-$PLATFORM.tar
      ARCHIVE=../wwhy-$VER-$PLATFORM.tar.Z
    else
      echo "WARNING: Could not find a 'gzip' program!"
      echo "       tar archive is not compressed."
      ARCHIVE=../wwhy-$VER-$PLATFORM.tar
    fi
  else
    echo "ERROR: Could not find a 'tar' program!"
    echo "       Please execute the following commands manually:"
    echo "         tar -cf ../wwhy-$VER-$PLATFORM.tar ."
    echo "         gzip -9 ../wwhy-$VER-$PLATFORM.tar"
  fi

  if [ "x$MD5" != "x" ]; then
    $MD5 $ARCHIVE > $ARCHIVE.md5
  fi

  if [ -f $ARCHIVE ] && [ -f ../wwhy-$VER-$PLATFORM.README ]; then
    echo "Ready."
    echo "You can find the binary archive ($ARCHIVE)"
    echo "and the readme file (wwhy-$VER-$PLATFORM.README) in the"
    echo "parent directory."
    exit 0;
  else
    echo "ERROR: Archive or README is missing."
    exit 1;
  fi
fi
