/* Must include clhy_config.h first so that we can redefine
    the standard prototypes macros after it messes with
    them. */
#include "clhy_config.h"

/* Define all of the standard prototype macros as themselves
    so that wwhy.h will not mess with them. This allows 
    them to pass untouched so that the AWK script can pick 
    them out of the preprocessed result file. */
#undef  CLHY_DECLARE
#define CLHY_DECLARE                 CLHY_DECLARE
#undef  CLHY_CORE_DECLARE
#define CLHY_CORE_DECLARE            CLHY_CORE_DECLARE
#undef  CLHY_DECLARE_NONSTD
#define CLHY_DECLARE_NONSTD          CLHY_DECLARE_NONSTD
#undef  CLHY_CORE_DECLARE_NONSTD
#define CLHY_CORE_DECLARE_NONSTD     CLHY_CORE_DECLARE_NONSTD
#undef  CLHY_DECLARE_HOOK
#define CLHY_DECLARE_HOOK            CLHY_DECLARE_HOOK
#undef  CLHY_DECLARE_DATA
#define CLHY_DECLARE_DATA            CLHY_DECLARE_DATA
#undef  KUDA_DECLARE_OPTIONAL_FN
#define KUDA_DECLARE_OPTIONAL_FN    KUDA_DECLARE_OPTIONAL_FN
#undef  KUDA_DECLARE_EXTERNAL_HOOK
#define KUDA_DECLARE_EXTERNAL_HOOK  KUDA_DECLARE_EXTERNAL_HOOK
#undef  CLHYKUDEL_PLATFORM_H

#include "wwhy.h"

/* Preprocess all of the standard WWHY headers. */
#include "clhy_compat.h"
#include "clhy_listen.h"
#include "clhy_capimn.h"
#include "clhy_core.h"
#include "clhy_provider.h"
#include "clhy_release.h"
#include "clhy_expr.h"
#include "http_config.h"
#include "http_connection.h"
#include "http_core.h"
#include "http_log.h"
#include "http_main.h"
#include "http_protocol.h"
#include "http_request.h"
#include "http_vhost.h"
#include "core_common.h"
#include "clhy_regex.h"
#include "scoreboard.h"
#include "util_cfgtree.h"
#include "util_charset.h"
#include "util_cookies.h"
#include "util_ebcdic.h"
#include "util_fcgi.h"
#include "util_filter.h"
/*#include "util_ldap.h"*/
#include "util_md5.h"
#include "util_mutex.h"
#include "util_script.h"
#include "util_time.h"
#include "util_varbuf.h"
#include "util_xml.h"

#include "capi_core.h"
#include "capi_auth.h"
#include "capi_watchdog.h"

