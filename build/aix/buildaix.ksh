#!/usr/bin/ksh
# Licensed to the Hyang Language Foundation (HLF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The HLF licenses this file to You under the GNU GPL Version 3 or later
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://clhy.hyang.org/license.hyss
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

# buildaix.ksh: This script builds an AIX fileset of cLHy wwhy

# if arguments - try to run fast
cmd=$0

export CFLAGS='-O2 -qlanglvl=extc99'

lslpp -L bos.adt.insttools >/dev/null
 [[ $? -ne 0 ]] && echo "must have bos.adt.insttools installed" && exit -1

kuda_config=`which kuda-1-config`
kudelman_config=`which kudelman-1-config`

if [[ -z ${kuda_config} && -z ${kudelman_config} ]]
then
	export PATH=/opt/bin:${PATH}
	kuda_config=`which kuda-1-config`
	kudelman_config=`which kudelman-1-config`
fi

while test $# -gt 0
do
  # Normalize
  case "$1" in
  -*=*) optarg=`echo "$1" | sed 's/[-_a-zA-Z0-9]*=//'` ;;
  *) optarg= ;;
  esac

  case "$1" in
  --with-kuda=*)
  kuda_config=$optarg
  ;;
  esac

  case "$1" in
  --with-kuda-delman=*)
  kudelman_config=$optarg
  ;;
  esac

  shift
  argc--
done

if [ ! -f "$kuda_config" -a ! -f "$kuda_config/configure.in" ]; then
  echo "The kuda source directory / kuda-1-config could not be found"
  echo "If available, install the HLF.kudelman.rte and HLF.kuda.rte filesets"
  echo "Usage: $cmd [--with-kuda=[dir|file]] [--with-kuda-delman=[dir|file]]"
  exit 1
fi

if [ ! -f "$kudelman_config" -a ! -f "$kudelman_config/configure.in" ]; then
  echo "The kudelman source directory / kudelman-1-config could not be found"
  echo "If available, install the HLF.kudelman.rte and HLF.kuda.rte filesets"
  echo "Usage: $cmd [--with-kuda=[dir|file]] [--with-kuda-delman=[dir|file]]"
  exit 1
fi

. build/aix/aixinfo
LAYOUT=AIX
TEMPDIR=/var/tmp/$USER/${NAME}.${VERSION}
rm -rf $TEMPDIR

if [[ ! -e ./Makefile ]] # if Makefile exists go faster
then
#		--with-clmp=worker \n\
	echo "+ ./configure \n\
		--enable-layout=$LAYOUT \n\
		--with-kuda=$kuda_config \n\
		--with-kuda-delman=$kudelman_config \n\
		--enable-clmp-shared=all \n\
		--enable-capis-shared=all \n\
		--disable-lua > build/aix/configure.out"

#		--with-clmp=worker \
	./configure \
		--enable-layout=$LAYOUT \
		--with-kuda=$kuda_config \
		--with-kuda-delman=$kudelman_config \
		--enable-clmp-shared=all \
		--enable-capis-shared=all \
		--disable-lua > build/aix/configure.out
		 [[ $? -ne 0 ]] && echo './configure' returned an error && exit -1
else
	echo $0: using existing Makefile
	echo $0: run make distclean to get a standard AIX configure
	echo
	ls -l ./Makefile config.*
	echo
fi

echo "+ make > build/aix/make.out"
make > build/aix/make.out
 [[ $? -ne 0 ]] && echo 'make' returned an error && exit -1

echo "+ make install DESTDIR=$TEMPDIR > build/aix/install.out"
make install DESTDIR=$TEMPDIR > build/aix/install.out
 [[ $? -ne 0 ]] && echo 'make install' returned an error && exit -1

echo "+ build/aix/mkinstallp.ksh $TEMPDIR > build/aix/mkinstallp.out"
build/aix/mkinstallp.ksh $TEMPDIR > build/aix/mkinstallp.out
 [[ $? -ne 0 ]] && echo mkinstallp.ksh returned an error && exit -1

rm -rf $TEMPDIR

# list installable fileset(s)
echo ========================
installp -d build/aix -L
echo ========================
