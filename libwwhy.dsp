# Microsoft Developer Studio Project File - Name="libwwhy" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=libwwhy - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "libwwhy.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libwwhy.mak" CFG="libwwhy - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libwwhy - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libwwhy - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libwwhy - Win32 Lexical" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libwwhy - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /FD /c
# ADD CPP /nologo /MD /W3 /O2 /Oy- /Zi /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /I "./kudelrunsrc/pcre" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /Fd"Release\libwwhy_cl" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /fo"Release/libwwhy.res" /i "./include" /i "./kudelrunsrc/kuda/include" /d "NDEBUG" /d BIN_NAME="libwwhy.dll" /d LONG_NAME="cLHy HTTP Server Core"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib advapi32.lib ws2_32.lib mswsock.lib /nologo /subsystem:windows /dll
# ADD LINK32 pcre.lib kernel32.lib user32.lib advapi32.lib ws2_32.lib mswsock.lib "Release\buildmark.obj" /nologo /subsystem:windows /dll /debug /libpath:"./kudelrunsrc/pcre" /base:@"platforms\win32\BaseAddr.ref",libwwhy.dll /opt:ref
# Begin Special Build Tool
TargetPath=.\Release\libwwhy.dll
SOURCE="$(InputPath)"
PreLink_Desc=Compiling buildmark
PreLink_Cmds=cl.exe /nologo /MD /W3 /O2 /Oy- /Zi /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /I "./kudelrunsrc/pcre" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /Fd"Release\libwwhy" /FD /c server\buildmark.c /Fo"Release\buildmark.obj"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /EHsc /Zi /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /FD /c
# ADD CPP /nologo /MDd /W3 /EHsc /Zi /Od /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /I "./kudelrunsrc/pcre" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /Fd"Debug\libwwhy_cl" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /fo"Debug/libwwhy.res" /i "./include" /i "./kudelrunsrc/kuda/include" /d "_DEBUG" /d BIN_NAME="libwwhy.dll" /d LONG_NAME="cLHy HTTP Server Core"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib advapi32.lib ws2_32.lib mswsock.lib /nologo /subsystem:windows /dll /incremental:no /debug
# ADD LINK32 pcred.lib kernel32.lib user32.lib advapi32.lib ws2_32.lib mswsock.lib "Debug\buildmark.obj" /nologo /subsystem:windows /dll /incremental:no /debug /libpath:"./kudelrunsrc/pcre" /base:@"platforms\win32\BaseAddr.ref",libwwhy.dll
# Begin Special Build Tool
TargetPath=.\Debug\libwwhy.dll
SOURCE="$(InputPath)"
PreLink_Desc=Compiling buildmark
PreLink_Cmds=cl.exe /nologo /MDd /W3 /EHsc /Zi /Od /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /I "./kudelrunsrc/pcre" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /Fd"Debug\libwwhy" /FD /c server\buildmark.c /Fo"Debug\buildmark.obj"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /FD /c
# ADD CPP /nologo /MD /W3 /O2 /Oy- /Zi /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /I "./kudelrunsrc/pcre" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /Fd"Release\libwwhy_cl" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /fo"Release/libwwhy.res" /i "./include" /i "./kudelrunsrc/kuda/include" /d "NDEBUG" /d BIN_NAME="libwwhy.dll" /d LONG_NAME="cLHy HTTP Server Core"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib advapi32.lib ws2_32.lib mswsock.lib /nologo /subsystem:windows /dll
# ADD LINK32 pcre.lib kernel32.lib user32.lib advapi32.lib ws2_32.lib mswsock.lib "Release\buildmark.obj" /nologo /subsystem:windows /dll /debug /libpath:"./kudelrunsrc/pcre" /base:@"platforms\win32\BaseAddr.ref",libwwhy.dll /opt:ref
# Begin Special Build Tool
TargetPath=.\Release\libwwhy.dll
SOURCE="$(InputPath)"
PreLink_Desc=Compiling buildmark
PreLink_Cmds=cl.exe /nologo /MD /W3 /O2 /Oy- /Zi /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /I "./kudelrunsrc/pcre" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /Fd"Release\libwwhy" /FD /c server\buildmark.c /Fo"Release\buildmark.obj"
PostBuild_Desc=Embed .manifest
PostBuild_Cmds=if exist $(TargetPath).manifest mt.exe -manifest $(TargetPath).manifest -outputresource:$(TargetPath);2
# End Special Build Tool


!ENDIF 

# Begin Target

# Name "libwwhy - Win32 Release"
# Name "libwwhy - Win32 Debug"
# Name "libwwhy - Win32 Lexical"
# Begin Group "headers"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;hpj;bat;for;f90"
# Begin Source File

SOURCE=.\include\clhy_compat.h
# End Source File
# Begin Source File

SOURCE=.\include\clhy_config.h
# End Source File
# Begin Source File

SOURCE=.\include\clhy_expr.h
# End Source File
# Begin Source File

SOURCE=.\include\clhy_capimn.h
# End Source File
# Begin Source File

SOURCE=.\include\clhy_release.h
# End Source File
# Begin Source File

SOURCE=.\include\http_config.h
# End Source File
# Begin Source File

SOURCE=.\include\http_connection.h
# End Source File
# Begin Source File

SOURCE=.\include\http_core.h
# End Source File
# Begin Source File

SOURCE=.\include\http_log.h
# End Source File
# Begin Source File

SOURCE=.\include\http_main.h
# End Source File
# Begin Source File

SOURCE=.\include\http_protocol.h
# End Source File
# Begin Source File

SOURCE=.\include\http_request.h
# End Source File
# Begin Source File

SOURCE=.\include\http_vhost.h
# End Source File
# Begin Source File

SOURCE=.\include\wwhy.h
# End Source File
# Begin Source File

SOURCE=.\platforms\win32\win32_config_layout.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/clhy_config_layout.h
InputPath=.\platforms\win32\win32_config_layout.h

".\include\clhy_config_layout.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\platforms\win32\win32_config_layout.h > .\include\clhy_config_layout.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/clhy_config_layout.h
InputPath=.\platforms\win32\win32_config_layout.h

".\include\clhy_config_layout.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\platforms\win32\win32_config_layout.h > .\include\clhy_config_layout.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\cAPIs\generators\capi_cgi.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/capi_cgi.h
InputPath=.\cAPIs\generators\capi_cgi.h

".\include\capi_cgi.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\cAPIs\generators\capi_cgi.h > .\include\capi_cgi.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/capi_cgi.h
InputPath=.\cAPIs\generators\capi_cgi.h

".\include\capi_cgi.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\cAPIs\generators\capi_cgi.h > .\include\capi_cgi.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\include\capi_core.h
# End Source File
# Begin Source File

SOURCE=.\cAPIs\dav\main\capi_dav.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/capi_dav.h
InputPath=.\cAPIs\dav\main\capi_dav.h

".\include\capi_dav.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\cAPIs\dav\main\capi_dav.h > .\include\capi_dav.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/capi_dav.h
InputPath=.\cAPIs\dav\main\capi_dav.h

".\include\capi_dav.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\cAPIs\dav\main\capi_dav.h > .\include\capi_dav.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\cAPIs\filters\capi_include.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/capi_include.h
InputPath=.\cAPIs\filters\capi_include.h

".\include\capi_include.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\cAPIs\filters\capi_include.h > .\include\capi_include.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/capi_include.h
InputPath=.\cAPIs\filters\capi_include.h

".\include\capi_include.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\cAPIs\filters\capi_include.h > .\include\capi_include.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\cAPIs\proxy\capi_proxy.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/capi_proxy.h
InputPath=.\cAPIs\proxy\capi_proxy.h

".\include\capi_proxy.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\cAPIs\proxy\capi_proxy.h > .\include\capi_proxy.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/capi_proxy.h
InputPath=.\cAPIs\proxy\capi_proxy.h

".\include\capi_proxy.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\cAPIs\proxy\capi_proxy.h > .\include\capi_proxy.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\cAPIs\core\capi_so.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/capi_so.h
InputPath=.\cAPIs\core\capi_so.h

".\include\capi_so.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\cAPIs\core\capi_so.h > .\include\capi_so.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/capi_so.h
InputPath=.\cAPIs\core\capi_so.h

".\include\capi_so.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\cAPIs\core\capi_so.h > .\include\capi_so.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\platforms\win32\platform.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/platform.h
InputPath=.\platforms\win32\platform.h

".\include\platform.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\platforms\win32\platform.h > .\include\platform.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Creating include/platform.h
InputPath=.\platforms\win32\platform.h

".\include\platform.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	type .\platforms\win32\platform.h > .\include\platform.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\server\test_char.h
# End Source File
# End Group
# Begin Group "wwhy"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\server\buildmark.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\cAPIs\http\byterange_filter.c
# End Source File
# Begin Source File

SOURCE=.\cAPIs\http\chunk_filter.c
# End Source File
# Begin Source File

SOURCE=.\server\config.c
# End Source File
# Begin Source File

SOURCE=.\server\connection.c
# End Source File
# Begin Source File

SOURCE=.\server\core.c
# End Source File
# Begin Source File

SOURCE=.\server\core_filters.c
# End Source File
# Begin Source File

SOURCE=.\cAPIs\http\http_core.c
# End Source File
# Begin Source File

SOURCE=.\cAPIs\http\http_etag.c
# End Source File
# Begin Source File

SOURCE=.\cAPIs\http\http_filters.c
# End Source File
# Begin Source File

SOURCE=.\cAPIs\http\http_protocol.c
# End Source File
# Begin Source File

SOURCE=.\cAPIs\http\http_request.c
# End Source File
# Begin Source File

SOURCE=.\server\log.c
# End Source File
# Begin Source File

SOURCE=.\server\protocol.c
# End Source File
# Begin Source File

SOURCE=.\server\request.c
# End Source File
# Begin Source File

SOURCE=.\server\vhost.c
# End Source File
# End Group
# Begin Group "cAPIs"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\cAPIs\core\capi_so.c
# End Source File
# Begin Source File

SOURCE=.\cAPIs\arch\win32\capi_win32.c
# End Source File
# Begin Source File

SOURCE=.\platforms\win32\cAPIs.c
# End Source File
# End Group
# Begin Group "util"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\include\clhy_regex.h
# End Source File
# Begin Source File

SOURCE=.\server\eoc_bucket.c
# End Source File
# Begin Source File

SOURCE=.\server\eor_bucket.c
# End Source File
# Begin Source File

SOURCE=.\server\error_bucket.c
# End Source File
# Begin Source File

SOURCE=.\server\util.c
# End Source File
# Begin Source File

SOURCE=.\server\util_cfgtree.c
# End Source File
# Begin Source File

SOURCE=.\include\util_cfgtree.h
# End Source File
# Begin Source File

SOURCE=.\include\util_charset.h
# End Source File
# Begin Source File

SOURCE=.\server\util_cookies.c
# End Source File
# Begin Source File

SOURCE=.\server\util_cookies.h
# End Source File
# Begin Source File

SOURCE=.\include\util_ebcdic.h
# End Source File
# Begin Source File

SOURCE=.\server\util_expr_private.h
# End Source File
# Begin Source File

SOURCE=.\server\util_expr_eval.c
# End Source File
# Begin Source File

SOURCE=.\server\util_expr_scan.h
# End Source File
# Begin Source File

SOURCE=.\server\util_expr_scan.c
# End Source File
# Begin Source File

SOURCE=.\server\util_expr_parse.h
# End Source File
# Begin Source File

SOURCE=.\server\util_expr_parse.c
# End Source File
# Begin Source File

SOURCE=.\server\util_fcgi.c
# End Source File
# Begin Source File

SOURCE=.\include\util_fcgi.h
# End Source File
# Begin Source File

SOURCE=.\server\util_filter.c
# End Source File
# Begin Source File

SOURCE=.\include\util_filter.h
# End Source File
# Begin Source File

SOURCE=.\server\util_md5.c
# End Source File
# Begin Source File

SOURCE=.\include\util_md5.h
# End Source File
# Begin Source File

SOURCE=.\server\util_mutex.c
# End Source File
# Begin Source File

SOURCE=.\include\util_mutex.h
# End Source File
# Begin Source File

SOURCE=.\server\util_pcre.c
# End Source File
# Begin Source File

SOURCE=.\server\util_regex.c
# End Source File
# Begin Source File

SOURCE=.\server\util_script.c
# End Source File
# Begin Source File

SOURCE=.\include\util_script.h
# End Source File
# Begin Source File

SOURCE=.\server\util_time.c
# End Source File
# Begin Source File

SOURCE=.\include\util_varbuf.h
# End Source File
# Begin Source File

SOURCE=.\platforms\win32\util_win32.c
# End Source File
# Begin Source File

SOURCE=.\server\util_xml.c
# End Source File
# Begin Source File

SOURCE=.\include\util_xml.h
# End Source File
# End Group
# Begin Group "core_winnt"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\include\clhy_listen.h
# End Source File
# Begin Source File

SOURCE=.\platforms\win32\clhy_regkey.c
# End Source File
# Begin Source File

SOURCE=.\include\clhy_regkey.h
# End Source File
# Begin Source File

SOURCE=.\server\clcore\winnt\child.c
# End Source File
# Begin Source File

SOURCE=.\server\listen.c
# End Source File
# Begin Source File

SOURCE=.\server\core_common.c
# End Source File
# Begin Source File

SOURCE=.\include\core_common.h
# End Source File
# Begin Source File

SOURCE=.\server\clcore\winnt\core_default.h

# End Source File
# Begin Source File

SOURCE=.\server\clcore\winnt\core_winnt.c
# End Source File
# Begin Source File

SOURCE=.\server\clcore\winnt\core_winnt.h
# End Source File
# Begin Source File

SOURCE=.\server\clcore\winnt\nt_eventlog.c
# End Source File
# Begin Source File

SOURCE=.\server\provider.c
# End Source File
# Begin Source File

SOURCE=.\server\scoreboard.c
# End Source File
# Begin Source File

SOURCE=.\include\scoreboard.h
# End Source File
# Begin Source File

SOURCE=.\server\clcore\winnt\service.c
# End Source File
# End Group
# Begin Source File

SOURCE=.\server\gen_test_char.exe

!IF  "$(CFG)" == "libwwhy - Win32 Release"

# PROP Ignore_Default_Tool 1
USERDEP__GEN_T=".\include\platform.h"	
# Begin Custom Build - Generating test_char.h from gen_test_char.exe
InputPath=.\server\gen_test_char.exe

".\server\test_char.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	.\server\gen_test_char.exe >.\server\test_char.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

# PROP Ignore_Default_Tool 1
USERDEP__GEN_T=".\include\platform.h"	
# Begin Custom Build - Generating test_char.h from gen_test_char.exe
InputPath=.\server\gen_test_char.exe

".\server\test_char.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	.\server\gen_test_char.exe >.\server\test_char.h

# End Custom Build

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

# PROP Ignore_Default_Tool 1
USERDEP__GEN_T=".\include\platform.h"
# Begin Custom Build - Generating test_char.h from gen_test_char.exe
InputPath=.\server\gen_test_char.exe

".\server\test_char.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	.\server\gen_test_char.exe >.\server\test_char.h

# End Custom Build

!ENDIF 

# End Source File
# Begin Group "Generated Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\server\util_expr_parse.y

!IF  "$(CFG)" == "libwwhy - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Generating util_expr_parse.c/.h from util_expr_parse.y
InputPath=.\server\util_expr_parse.y

BuildCmds= \
	bison -pclhy_expr_yy --defines=.\server\util_expr_parse.h -o .\server\util_expr_parse.c .\server\util_expr_parse.y

".\server\util_expr_parse.c" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)

".\server\util_expr_parse.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ENDIF

# End Source File
# Begin Source File

SOURCE=.\server\util_expr_scan.l

!IF  "$(CFG)" == "libwwhy - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Generating util_expr_scan.c from util_expr_scan.l
InputPath=.\server\util_expr_scan.l

".\server\util_expr_scan.c" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	flex -Pclhy_expr_yy -o .\server\util_expr_scan.c .\server\util_expr_scan.l

# End Custom Build

!ENDIF

# End Source File
# End Group
# Begin Source File

SOURCE=.\build\win32\wwhy.rc
# End Source File
# End Target
# End Project
