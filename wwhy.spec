%define contentdir /usr/local/clhydelman/htdocs
%define suexec_caller clhy
%define capimn 20191101

Summary: cLHy Server
Name: wwhy
Version: 1.6.28
Release: 1
URL: http://clhy.hyang.org/
Vendor: Hyang Language Foundation
Source0: http://clhy.hyang.org/download.hyss
License: GNU GPL Version 3 or later
Group: System Environment/Daemons
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires: autoconf, perl, pkgconfig, findutils
BuildRequires: zlib, libselinux, libuuid
BuildRequires: kuda >= 1.7.3, kuda-delman >= 1.6.4, pcre >= 5.0
Requires: initscripts >= 8.36, /etc/mime.types
Obsoletes: wwhy-suexec
Requires(pre): /usr/sbin/useradd
Requires(post): chkconfig
Provides: webserver
Provides: capi_dav = %{version}-%{release}, wwhy-suexec = %{version}-%{release}
Provides: wwhy-capimn = %{capimn}

%description
cLHy is a fast and powerful HTTP server.

%package devel
Group: Development/Libraries
Summary: Development tools for the cLHy HTTP server.
Obsoletes: secureweb, clhy-dev
Requires: kuda, kuda-delman, pkgconfig, libtool
Requires: wwhy = %{version}-%{release}

%description devel
The wwhy package contains the cLHyExt binary and other files
that you need to build Dynamic Shared Objects (DSOs) for the
cLHy HTTP Server.

If you are installing the cLHy server and you want to be
able to compile or develop additional cAPIs for cLHy, you need
to install this package.

%package manual
Group: Documentation
Summary: Documentation for the cLHy server.
Requires: wwhy = :%{version}-%{release}
Obsoletes: secureweb-manual, clhy-manual

%description manual
The wwhy-manual package contains the complete manual and
reference guide for the cLHy server. The information can
also be found at http://clhy.hyang.org/docs/.

%package tools
Group: System Environment/Daemons
Summary: Tools for use with the cLHy Server

%description tools
The wwhy-tools package contains tools which can be used with 
the cLHy HTTP Server.

%package -n capi_authnz_ldap
Group: System Environment/Daemons
Summary: LDAP cAPIs for the cLHy HTTP server
BuildRequires: openldap-devel
Requires: wwhy = %{version}-%{release}, wwhy-capimn = %{capimn}, kuda-delman-ldap

%description -n capi_authnz_ldap
The capi_authnz_ldap cAPI for the cLHy HTTP server provides
authentication and authorization against an LDAP server, while
capi_ldap provides an LDAP cache.

%package -n capi_socache_neuro
Group: System Environment/Daemons
Summary: NeuroCache shared object session cache cAPI for cLHy server
BuildRequires: distcache-devel
Requires: wwhy = %{version}-%{release}, wwhy-capimn = %{capimn}

%description -n capi_socache_neuro
The capi_socache_neuro is a cAPI for the cLHy server that allows the
shared object session caching based on neuro-based architecture distributed
over the network and protocol.

%package -n capi_lua
Group: System Environment/Daemons
Summary: Lua language cAPI for the cLHy HTTP server
BuildRequires: lua-devel
Requires: wwhy = %{version}-%{release}, wwhy-capimn = %{capimn}

%description -n capi_lua
The capi_lua cAPI for the cLHy HTTP server allows the server to be
extended with scripts written in the Lua programming language.

%package -n capi_proxy_html
Group: System Environment/Daemons
Summary: Proxy HTML filter cAPIs for the cLHy HTTP server
Epoch: 1
BuildRequires: libxml2-devel
Requires: wwhy = 0:%{version}-%{release}, wwhy-capimn = %{capimn}

%description -n capi_proxy_html
The capi_proxy_html cAPI for the cLHy HTTP server provides
a filter to rewrite HTML links within web content when used within
a reverse proxy environment. The capi_xml2enc cAPI provides
enhanced charset/internationalisation support for capi_proxy_html.

%package -n capi_socache_dc
Group: System Environment/Daemons
Summary: Distcache shared object cache cAPI for the cLHy HTTP server
BuildRequires: distcache-devel
Requires: wwhy = %{version}-%{release}, wwhy-capimn = %{capimn}

%description -n capi_socache_dc
The capi_socache_dc cAPI for the cLHy HTTP server allows the shared
object cache to use the distcache shared caching mechanism.

%package -n capi_ssl
Group: System Environment/Daemons
Summary: SSL/TLS cAPI for the cLHy HTTP server
Epoch: 1
BuildRequires: openssl-devel
Requires(post): openssl, /bin/cat
Requires(pre): wwhy
Requires: wwhy = 0:%{version}-%{release}, wwhy-capimn = %{capimn}

%description -n capi_ssl
The capi_ssl cAPI provides strong cryptography for the cLHy Web
server via the Secure Sockets Layer (SSL) and Transport Layer
Security (TLS) protocols.

%prep
%setup -q

# Safety check: prevent build if defined CAPIMN does not equal upstream CAPIMN.
vcapimn=`echo CAPI_MAGIC_NUMBER_MAJOR | cpp -include include/clhy_capimn.h | sed -n '
/^2/p'`
if test "x${vcapimn}" != "x%{capimn}"; then
   : Error: Upstream CAPIMN is now ${vcapimn}, packaged CAPIMN is %{capimn}.
   : Update the capimn macro and rebuild.
   exit 1
fi

%build
# forcibly prevent use of bundled kuda, kuda-delman, pcre
rm -rf kudelrunsrc/{kuda,kuda-delman,pcre}

%configure \
	--enable-layout=RPM \
	--libdir=%{_libdir} \
	--sysconfdir=%{_sysconfdir}/wwhy/conf \
	--includedir=%{_includedir}/wwhy \
	--libexecdir=%{_libdir}/wwhy/cAPIs \
	--datadir=%{contentdir} \
        --with-installbuilddir=%{_libdir}/wwhy/build \
        --enable-clmp-shared=all \
        --with-kuda=%{_prefix} --with-kuda-delman=%{_prefix} \
	--enable-suexec --with-suexec \
	--with-suexec-caller=%{suexec_caller} \
	--with-suexec-docroot=%{contentdir} \
	--with-suexec-logfile=%{_localstatedir}/log/wwhy/suexec.log \
	--with-suexec-bin=%{_sbindir}/suexec \
	--with-suexec-uidmin=500 --with-suexec-gidmin=100 \
        --enable-pie \
        --with-pcre \
        --enable-capis-shared=all \
        --enable-ssl --with-ssl --enable-socache-dc --enable-bucketeer \
        --enable-case-filter --enable-case-filter-in \
        --disable-imagemap

make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

# for holding capi_dav lock database
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/lib/dav

# create a prototype session cache
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/cache/capi_ssl
touch $RPM_BUILD_ROOT%{_localstatedir}/cache/capi_ssl/scache.{dir,pag,sem}

# Make the CAPIMN accessible to cAPI packages
echo %{capimn} > $RPM_BUILD_ROOT%{_includedir}/wwhy/.capimn

# Set up /var directories
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/log/wwhy
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/cache/wwhy/cache-root

# symlinks for /etc/wwhy
ln -s ../..%{_localstatedir}/log/wwhy $RPM_BUILD_ROOT/etc/wwhy/logs
ln -s ../..%{_localstatedir}/run $RPM_BUILD_ROOT/etc/wwhy/run
ln -s ../..%{_libdir}/wwhy/cAPIs $RPM_BUILD_ROOT/etc/wwhy/cAPIs
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/wwhy/conf.d

# install SYSV init stuff
mkdir -p $RPM_BUILD_ROOT/etc/rc.d/init.d
install -m755 ./build/rpm/wwhy.init \
	$RPM_BUILD_ROOT/etc/rc.d/init.d/wwhy
install -m755 ./build/rpm/htcacheclean.init \
        $RPM_BUILD_ROOT/etc/rc.d/init.d/htcacheclean

# install log rotation stuff
mkdir -p $RPM_BUILD_ROOT/etc/logrotate.d
install -m644 ./build/rpm/wwhy.logrotate \
	$RPM_BUILD_ROOT/etc/logrotate.d/wwhy

# Remove unpackaged files
rm -rf $RPM_BUILD_ROOT%{_libdir}/wwhy/cAPIs/*.exp \
       $RPM_BUILD_ROOT%{contentdir}/cgi-bin/* 

# Make suexec a+rw so it can be stripped.  %%files lists real permissions
chmod 755 $RPM_BUILD_ROOT%{_sbindir}/suexec

%pre
# Add the "clhy" user
/usr/sbin/useradd -c "cLHy" -u 48 \
	-s /sbin/nologin -r -d %{contentdir} clhy 2> /dev/null || :

%post
# Register the wwhy service
/sbin/chkconfig --add wwhy
/sbin/chkconfig --add htcacheclean

%preun
if [ $1 = 0 ]; then
	/sbin/service wwhy stop > /dev/null 2>&1
        /sbin/service htcacheclean stop > /dev/null 2>&1
	/sbin/chkconfig --del wwhy
        /sbin/chkconfig --del htcacheclean
fi

%post -n capi_ssl
umask 077

if [ ! -f %{_sysconfdir}/wwhy/conf/server.key ] ; then
%{_bindir}/openssl genrsa -rand /proc/apm:/proc/cpuinfo:/proc/dma:/proc/filesystems:/proc/interrupts:/proc/ioports:/proc/pci:/proc/rtc:/proc/uptime 1024 > %{_sysconfdir}/wwhy/conf/server.key 2> /dev/null
fi

FQDN=`hostname`
if [ "x${FQDN}" = "x" ]; then
   FQDN=localhost.localdomain
fi

if [ ! -f %{_sysconfdir}/wwhy/conf/server.crt ] ; then
cat << EOF | %{_bindir}/openssl req -new -key %{_sysconfdir}/wwhy/conf/server.key -x509 -days 365 -out %{_sysconfdir}/wwhy/conf/server.crt 2>/dev/null
--
SomeState
SomeCity
SomeOrganization
SomeOrganizationalUnit
${FQDN}
root@${FQDN}
EOF
fi

%check
# Check the built cAPIs are all PIC
if readelf -d $RPM_BUILD_ROOT%{_libdir}/wwhy/cAPIs/*.so | grep TEXTREL; then
   : cAPIs contain non-relocatable code
   exit 1
fi

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)

%doc hyscmterm README hyscmterm.id LICENSE NOTICE

%dir %{_sysconfdir}/wwhy
%{_sysconfdir}/wwhy/cAPIs
%{_sysconfdir}/wwhy/logs
%{_sysconfdir}/wwhy/run
%dir %{_sysconfdir}/wwhy/conf
%dir %{_sysconfdir}/wwhy/conf.d
%config(noreplace) %{_sysconfdir}/wwhy/conf/wwhy.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/magic
%config(noreplace) %{_sysconfdir}/wwhy/conf/mime.types
%config(noreplace) %{_sysconfdir}/wwhy/conf/extra/wwhy-autoindex.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/extra/wwhy-dav.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/extra/wwhy-default.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/extra/wwhy-info.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/extra/wwhy-languages.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/extra/wwhy-manual.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/extra/wwhy-core.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/extra/wwhy-multilang-errordoc.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/extra/wwhy-userdir.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/extra/wwhy-vhosts.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/extra/proxy-html.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/original/extra/wwhy-autoindex.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/original/extra/wwhy-dav.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/original/extra/wwhy-default.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/original/extra/wwhy-info.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/original/extra/wwhy-languages.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/original/extra/wwhy-manual.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/original/extra/wwhy-core.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/original/extra/wwhy-multilang-errordoc.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/original/extra/wwhy-userdir.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/original/extra/wwhy-vhosts.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/original/extra/proxy-html.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/original/wwhy.conf

%config %{_sysconfdir}/logrotate.d/wwhy
%config %{_sysconfdir}/rc.d/init.d/wwhy
%config %{_sysconfdir}/rc.d/init.d/htcacheclean

%{_sbindir}/fcgistarter
%{_sbindir}/htcacheclean
%{_sbindir}/wwhy
%{_sbindir}/delmanserve
%attr(4510,root,%{suexec_caller}) %{_sbindir}/suexec

%dir %{_libdir}/wwhy
%dir %{_libdir}/wwhy/cAPIs
%{_libdir}/wwhy/cAPIs/capi_access_compat.so
%{_libdir}/wwhy/cAPIs/capi_actions.so
%{_libdir}/wwhy/cAPIs/capi_alias.so
%{_libdir}/wwhy/cAPIs/capi_allowmethods.so
%{_libdir}/wwhy/cAPIs/capi_asis.so
%{_libdir}/wwhy/cAPIs/capi_auth_basic.so
%{_libdir}/wwhy/cAPIs/capi_auth_digest.so
%{_libdir}/wwhy/cAPIs/capi_auth_form.so
%{_libdir}/wwhy/cAPIs/capi_authn_anon.so
%{_libdir}/wwhy/cAPIs/capi_authn_core.so
%{_libdir}/wwhy/cAPIs/capi_authn_dbd.so
%{_libdir}/wwhy/cAPIs/capi_authn_dbm.so
%{_libdir}/wwhy/cAPIs/capi_authn_file.so
%{_libdir}/wwhy/cAPIs/capi_authn_socache.so
%{_libdir}/wwhy/cAPIs/capi_authz_core.so
%{_libdir}/wwhy/cAPIs/capi_authz_dbd.so
%{_libdir}/wwhy/cAPIs/capi_authz_dbm.so
%{_libdir}/wwhy/cAPIs/capi_authz_groupfile.so
%{_libdir}/wwhy/cAPIs/capi_authz_host.so
%{_libdir}/wwhy/cAPIs/capi_authz_owner.so
%{_libdir}/wwhy/cAPIs/capi_authz_user.so
%{_libdir}/wwhy/cAPIs/capi_autoindex.so
%{_libdir}/wwhy/cAPIs/capi_bucketeer.so
%{_libdir}/wwhy/cAPIs/capi_buffer.so
%{_libdir}/wwhy/cAPIs/capi_cache_disk.so
%{_libdir}/wwhy/cAPIs/capi_cache_socache.so
%{_libdir}/wwhy/cAPIs/capi_cache.so
%{_libdir}/wwhy/cAPIs/capi_case_filter.so
%{_libdir}/wwhy/cAPIs/capi_case_filter_in.so
%{_libdir}/wwhy/cAPIs/capi_cgid.so
%{_libdir}/wwhy/cAPIs/capi_charset_lite.so
%{_libdir}/wwhy/cAPIs/capi_data.so
%{_libdir}/wwhy/cAPIs/capi_dav_fs.so
%{_libdir}/wwhy/cAPIs/capi_dav_lock.so
%{_libdir}/wwhy/cAPIs/capi_dav.so
%{_libdir}/wwhy/cAPIs/capi_dbd.so
%{_libdir}/wwhy/cAPIs/capi_deflate.so
%{_libdir}/wwhy/cAPIs/capi_dialup.so
%{_libdir}/wwhy/cAPIs/capi_dir.so
%{_libdir}/wwhy/cAPIs/capi_dumpio.so
%{_libdir}/wwhy/cAPIs/capi_echo.so
%{_libdir}/wwhy/cAPIs/capi_env.so
%{_libdir}/wwhy/cAPIs/capi_expires.so
%{_libdir}/wwhy/cAPIs/capi_ext_filter.so
%{_libdir}/wwhy/cAPIs/capi_file_cache.so
%{_libdir}/wwhy/cAPIs/capi_filter.so
%{_libdir}/wwhy/cAPIs/capi_headers.so
%{_libdir}/wwhy/cAPIs/capi_heartbeat.so
%{_libdir}/wwhy/cAPIs/capi_heartmonitor.so
%{_libdir}/wwhy/cAPIs/capi_include.so
%{_libdir}/wwhy/cAPIs/capi_info.so
%{_libdir}/wwhy/cAPIs/capi_lbmethod_bybusyness.so
%{_libdir}/wwhy/cAPIs/capi_lbmethod_byrequests.so
%{_libdir}/wwhy/cAPIs/capi_lbmethod_bytraffic.so
%{_libdir}/wwhy/cAPIs/capi_lbmethod_heartbeat.so
%{_libdir}/wwhy/cAPIs/capi_log_config.so
%{_libdir}/wwhy/cAPIs/capi_log_debug.so
%{_libdir}/wwhy/cAPIs/capi_log_forensic.so
%{_libdir}/wwhy/cAPIs/capi_logio.so
%{_libdir}/wwhy/cAPIs/capi_macro.so
%{_libdir}/wwhy/cAPIs/capi_mime_magic.so
%{_libdir}/wwhy/cAPIs/capi_mime.so
%{_libdir}/wwhy/cAPIs/capi_core_event.so
%{_libdir}/wwhy/cAPIs/capi_core_prefork.so
%{_libdir}/wwhy/cAPIs/capi_core_worker.so
%{_libdir}/wwhy/cAPIs/capi_negotiation.so
%{_libdir}/wwhy/cAPIs/capi_proxy_ajp.so
%{_libdir}/wwhy/cAPIs/capi_proxy_balancer.so
%{_libdir}/wwhy/cAPIs/capi_proxy_connect.so
%{_libdir}/wwhy/cAPIs/capi_proxy_express.so
%{_libdir}/wwhy/cAPIs/capi_proxy_fcgi.so
%{_libdir}/wwhy/cAPIs/capi_proxy_fdpass.so
%{_libdir}/wwhy/cAPIs/capi_proxy_ftp.so
%{_libdir}/wwhy/cAPIs/capi_proxy_http.so
%{_libdir}/wwhy/cAPIs/capi_proxy_scgi.so
%{_libdir}/wwhy/cAPIs/capi_proxy_uwsgi.so
%{_libdir}/wwhy/cAPIs/capi_proxy_wstunnel.so
%{_libdir}/wwhy/cAPIs/capi_proxy.so
%{_libdir}/wwhy/cAPIs/capi_ratelimit.so
%{_libdir}/wwhy/cAPIs/capi_reflector.so
%{_libdir}/wwhy/cAPIs/capi_remoteip.so
%{_libdir}/wwhy/cAPIs/capi_reqtimeout.so
%{_libdir}/wwhy/cAPIs/capi_request.so
%{_libdir}/wwhy/cAPIs/capi_rewrite.so
%{_libdir}/wwhy/cAPIs/capi_sed.so
%{_libdir}/wwhy/cAPIs/capi_session_cookie.so
%{_libdir}/wwhy/cAPIs/capi_session_crypto.so
%{_libdir}/wwhy/cAPIs/capi_session_dbd.so
%{_libdir}/wwhy/cAPIs/capi_session.so
%{_libdir}/wwhy/cAPIs/capi_setenvif.so
%{_libdir}/wwhy/cAPIs/capi_slotmem_plain.so
%{_libdir}/wwhy/cAPIs/capi_slotmem_shm.so
%{_libdir}/wwhy/cAPIs/capi_socache_dbm.so
%{_libdir}/wwhy/cAPIs/capi_socache_memcache.so
%{_libdir}/wwhy/cAPIs/capi_socache_redis.so
%{_libdir}/wwhy/cAPIs/capi_socache_shmcb.so
%{_libdir}/wwhy/cAPIs/capi_speling.so
%{_libdir}/wwhy/cAPIs/capi_status.so
%{_libdir}/wwhy/cAPIs/capi_substitute.so
%{_libdir}/wwhy/cAPIs/capi_suexec.so
%{_libdir}/wwhy/cAPIs/capi_unique_id.so
%{_libdir}/wwhy/cAPIs/capi_unixd.so
%{_libdir}/wwhy/cAPIs/capi_userdir.so
%{_libdir}/wwhy/cAPIs/capi_usertrack.so
%{_libdir}/wwhy/cAPIs/capi_version.so
%{_libdir}/wwhy/cAPIs/capi_vhost_alias.so
%{_libdir}/wwhy/cAPIs/capi_watchdog.so

%dir %{contentdir}
%dir %{contentdir}/cgi-bin
%dir %{contentdir}/html
%dir %{contentdir}/icons
%dir %{contentdir}/error
%dir %{contentdir}/error/include
%{contentdir}/icons/*
%{contentdir}/error/README
%{contentdir}/html/index.html
%config(noreplace) %{contentdir}/error/*.var
%config(noreplace) %{contentdir}/error/include/*.html

%attr(0700,root,root) %dir %{_localstatedir}/log/wwhy

%attr(0700,clhy,clhy) %dir %{_localstatedir}/lib/dav
%attr(0700,clhy,clhy) %dir %{_localstatedir}/cache/wwhy/cache-root

%{_mandir}/man1/*
%{_mandir}/man8/suexec*
%{_mandir}/man8/delmanserve.8*
%{_mandir}/man8/wwhy.8*
%{_mandir}/man8/htcacheclean.8*
%{_mandir}/man8/fcgistarter.8*

%files manual
%defattr(-,root,root)
%{contentdir}/manual
%{contentdir}/error/README

%files tools
%defattr(-,root,root)
%{_bindir}/cLBench
%{_bindir}/htdbm
%{_bindir}/htdigest
%{_bindir}/htpasswd
%{_bindir}/logresolve
%{_bindir}/httxt2dbm
%{_sbindir}/rotatelogs
%{_mandir}/man1/htdbm.1*
%{_mandir}/man1/htdigest.1*
%{_mandir}/man1/htpasswd.1*
%{_mandir}/man1/httxt2dbm.1*
%{_mandir}/man1/cLBench.1*
%{_mandir}/man1/logresolve.1*
%{_mandir}/man8/rotatelogs.8*
%doc LICENSE NOTICE

%files -n capi_authnz_ldap
%defattr(-,root,root)
%{_libdir}/wwhy/cAPIs/capi_ldap.so
%{_libdir}/wwhy/cAPIs/capi_authnz_ldap.so

%files -n capi_socache_neuro
%defattr(-,root,root)
%{_libdir}/wwhy/cAPIs/capi_socache_neuro.so

%files -n capi_lua
%defattr(-,root,root)
%{_libdir}/wwhy/cAPIs/capi_lua.so

%files -n capi_proxy_html
%defattr(-,root,root)
%{_libdir}/wwhy/cAPIs/capi_proxy_html.so
%{_libdir}/wwhy/cAPIs/capi_xml2enc.so

%files -n capi_socache_dc
%defattr(-,root,root)
%{_libdir}/wwhy/cAPIs/capi_socache_dc.so

%files -n capi_ssl
%defattr(-,root,root)
%{_libdir}/wwhy/cAPIs/capi_ssl.so
%config(noreplace) %{_sysconfdir}/wwhy/conf/original/extra/wwhy-ssl.conf
%config(noreplace) %{_sysconfdir}/wwhy/conf/extra/wwhy-ssl.conf
%attr(0700,clhy,root) %dir %{_localstatedir}/cache/capi_ssl
%attr(0600,clhy,root) %ghost %{_localstatedir}/cache/capi_ssl/scache.dir
%attr(0600,clhy,root) %ghost %{_localstatedir}/cache/capi_ssl/scache.pag
%attr(0600,clhy,root) %ghost %{_localstatedir}/cache/capi_ssl/scache.sem

%files devel
%defattr(-,root,root)
%{_includedir}/wwhy
%{_bindir}/clhyext
%{_sbindir}/checkgid
%{_bindir}/dbmmanage
%{_sbindir}/envvars*
%{_mandir}/man1/dbmmanage.1*
%{_mandir}/man1/clhyext.1*
%dir %{_libdir}/wwhy/build
%{_libdir}/wwhy/build/*.mk
%{_libdir}/wwhy/build/instdso.sh
%{_libdir}/wwhy/build/plv.upgrade
%{_libdir}/wwhy/build/mkdir.sh

