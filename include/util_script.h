/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  util_script.h
 * @brief cLHy script tools
 *
 * @defgroup CLHYKUDEL_CORE_SCRIPT Script Tools
 * @ingroup  CLHYKUDEL_CORE
 * @{
 */

#ifndef CLHYKUDEL_UTIL_SCRIPT_H
#define CLHYKUDEL_UTIL_SCRIPT_H

#include "kuda_buckets.h"
#include "clhy_config.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef CLHYKUDEL_ARG_MAX
#ifdef _POSIX_ARG_MAX
#define CLHYKUDEL_ARG_MAX _POSIX_ARG_MAX
#else
#define CLHYKUDEL_ARG_MAX 512
#endif
#endif

/**
 * Create an environment variable out of an cLHy table of key-value pairs
 * @param p pool to allocate out of
 * @param t cLHy table of key-value pairs
 * @return An array containing the same key-value pairs suitable for
 *         use with an exec call.
 * @fn char **clhy_create_environment(kuda_pool_t *p, kuda_table_t *t)
 */
CLHY_DECLARE(char **) clhy_create_environment(kuda_pool_t *p, kuda_table_t *t);

/**
 * This "cute" little function comes about because the path info on
 * filenames and URLs aren't always the same. So we take the two,
 * and find as much of the two that match as possible.
 * @param uri The uri we are currently parsing
 * @param path_info The current path info
 * @return The length of the path info
 * @fn int clhy_find_path_info(const char *uri, const char *path_info)
 */
CLHY_DECLARE(int) clhy_find_path_info(const char *uri, const char *path_info);

/**
 * Add CGI environment variables required by HTTP/1.1 to the request's
 * environment table
 * @param r the current request
 * @fn void clhy_add_cgi_vars(request_rec *r)
 */
CLHY_DECLARE(void) clhy_add_cgi_vars(request_rec *r);

/**
 * Add common CGI environment variables to the requests environment table
 * @param r The current request
 * @fn void clhy_add_common_vars(request_rec *r)
 */
CLHY_DECLARE(void) clhy_add_common_vars(request_rec *r);

/**
 * Read headers output from a script, ensuring that the output is valid.  If
 * the output is valid, then the headers are added to the headers out of the
 * current request
 * @param r The current request
 * @param f The file to read from
 * @param buffer Empty when calling the function.  On output, if there was an
 *               error, the string that cause the error is stored here.
 * @return HTTP_OK on success, HTTP_INTERNAL_SERVER_ERROR otherwise
 * @fn int clhy_scan_script_header_err(request_rec *r, kuda_file_t *f, char *buffer)
 */
CLHY_DECLARE(int) clhy_scan_script_header_err(request_rec *r, kuda_file_t *f, char *buffer);

/**
 * Read headers output from a script, ensuring that the output is valid.  If
 * the output is valid, then the headers are added to the headers out of the
 * current request
 * @param r The current request
 * @param f The file to read from
 * @param buffer Empty when calling the function.  On output, if there was an
 *               error, the string that cause the error is stored here.
 * @param capi_index The cAPI index to be used for logging
 * @return HTTP_OK on success, HTTP_INTERNAL_SERVER_ERROR otherwise
 */
CLHY_DECLARE(int) clhy_scan_script_header_err_ex(request_rec *r, kuda_file_t *f,
                                             char *buffer, int capi_index);


/**
 * Read headers output from a script, ensuring that the output is valid.  If
 * the output is valid, then the headers are added to the headers out of the
 * current request
 * @param r The current request
 * @param bb The brigade from which to read
 * @param buffer Empty when calling the function.  On output, if there was an
 *               error, the string that cause the error is stored here.
 * @return HTTP_OK on success, HTTP_INTERNAL_SERVER_ERROR otherwise
 * @fn int clhy_scan_script_header_err_brigade(request_rec *r, kuda_bucket_brigade *bb, char *buffer)
 */
CLHY_DECLARE(int) clhy_scan_script_header_err_brigade(request_rec *r,
                                                  kuda_bucket_brigade *bb,
                                                  char *buffer);

/**
 * Read headers output from a script, ensuring that the output is valid.  If
 * the output is valid, then the headers are added to the headers out of the
 * current request
 * @param r The current request
 * @param bb The brigade from which to read
 * @param buffer Empty when calling the function.  On output, if there was an
 *               error, the string that cause the error is stored here.
 * @param capi_index The cAPI index to be used for logging
 * @return HTTP_OK on success, HTTP_INTERNAL_SERVER_ERROR otherwise
 */
CLHY_DECLARE(int) clhy_scan_script_header_err_brigade_ex(request_rec *r,
                                                     kuda_bucket_brigade *bb,
                                                     char *buffer,
                                                     int capi_index);

/**
 * Read headers strings from a script, ensuring that the output is valid.  If
 * the output is valid, then the headers are added to the headers out of the
 * current request
 * @param r The current request
 * @param buffer Empty when calling the function.  On output, if there was an
 *               error, the string that cause the error is stored here.
 * @param termch Pointer to the last character parsed.
 * @param termarg Pointer to an int to capture the last argument parsed.
 *
 * The varargs are string arguments to parse consecutively for headers,
 * with a NULL argument to terminate the list.
 *
 * @return HTTP_OK on success, HTTP_INTERNAL_SERVER_ERROR otherwise
 */
CLHY_DECLARE_NONSTD(int) clhy_scan_script_header_err_strs(request_rec *r,
                                                      char *buffer,
                                                      const char **termch,
                                                      int *termarg, ...)
                       CLHY_FN_ATTR_SENTINEL;

/**
 * Read headers strings from a script, ensuring that the output is valid.  If
 * the output is valid, then the headers are added to the headers out of the
 * current request
 * @param r The current request
 * @param buffer Empty when calling the function.  On output, if there was an
 *               error, the string that cause the error is stored here.
 * @param capi_index The cAPI index to be used for logging
 * @param termch Pointer to the last character parsed.
 * @param termarg Pointer to an int to capture the last argument parsed.
 *
 * The varargs are string arguments to parse consecutively for headers,
 * with a NULL argument to terminate the list.
 *
 * @return HTTP_OK on success, HTTP_INTERNAL_SERVER_ERROR otherwise
 */
CLHY_DECLARE_NONSTD(int) clhy_scan_script_header_err_strs_ex(request_rec *r,
                                                         char *buffer,
                                                         int capi_index,
                                                         const char **termch,
                                                         int *termarg, ...)
                       CLHY_FN_ATTR_SENTINEL;


/**
 * Read headers output from a script, ensuring that the output is valid.  If
 * the output is valid, then the headers are added to the headers out of the
 * current request
 * @param r The current request
 * @param buffer Empty when calling the function.  On output, if there was an
 *               error, the string that cause the error is stored here.
 * @param getsfunc Function to read the headers from.  This function should
                   act like gets()
 * @param getsfunc_data The place to read from
 * @return HTTP_OK on success, HTTP_INTERNAL_SERVER_ERROR otherwise
 */
CLHY_DECLARE(int) clhy_scan_script_header_err_core(request_rec *r, char *buffer,
                                               int (*getsfunc) (char *, int, void *),
                                               void *getsfunc_data);

/**
 * Read headers output from a script, ensuring that the output is valid.  If
 * the output is valid, then the headers are added to the headers out of the
 * current request
 * @param r The current request
 * @param buffer Empty when calling the function.  On output, if there was an
 *               error, the string that cause the error is stored here.
 * @param getsfunc Function to read the headers from.  This function should
                   act like gets()
 * @param getsfunc_data The place to read from
 * @param capi_index The cAPI index to be used for logging
 * @return HTTP_OK on success, HTTP_INTERNAL_SERVER_ERROR otherwise
 */
CLHY_DECLARE(int) clhy_scan_script_header_err_core_ex(request_rec *r, char *buffer,
                                        int (*getsfunc) (char *, int, void *),
                                        void *getsfunc_data, int capi_index);


/**
 * Parse query args for the request and store in a new table allocated
 * from the request pool.
 * For args with no value, "1" will be used instead.
 * If no query args were specified, the table will be empty.
 * @param r The current request
 * @param table A new table on output.
 */
CLHY_DECLARE(void) clhy_args_to_table(request_rec *r, kuda_table_t **table);

#ifdef __cplusplus
}
#endif

#endif  /* !CLHYKUDEL_UTIL_SCRIPT_H */
/** @} */
