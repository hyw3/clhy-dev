/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file clhy_regkey.h
 * @brief KUDA-style Win32 Registry Manipulation
 */

#ifndef CLHY_REGKEY_H
#define CLHY_REGKEY_H

#if defined(WIN32) || defined(DOXYGEN)

#include "kuda.h"
#include "kuda_pools.h"
#include "clhy_config.h"      /* Just for CLHY_DECLARE */

#ifdef __cplusplus
extern "C" {
#endif

typedef struct clhy_regkey_t clhy_regkey_t;

/* Used to recover CLHY_REGKEY_* constants
 */
CLHY_DECLARE(const clhy_regkey_t *) clhy_regkey_const(int i);

/**
 * Win32 Only: Constants for clhy_regkey_open()
 */
#define CLHY_REGKEY_CLASSES_ROOT         clhy_regkey_const(0)
#define CLHY_REGKEY_CURRENT_CONFIG       clhy_regkey_const(1)
#define CLHY_REGKEY_CURRENT_USER         clhy_regkey_const(2)
#define CLHY_REGKEY_LOCAL_MACHINE        clhy_regkey_const(3)
#define CLHY_REGKEY_USERS                clhy_regkey_const(4)
#define CLHY_REGKEY_PERFORMANCE_DATA     clhy_regkey_const(5)
#define CLHY_REGKEY_DYN_DATA             clhy_regkey_const(6)

/**
 * Win32 Only: Flags for clhy_regkey_value_set()
 */
#define CLHY_REGKEY_EXPAND               0x0001

/**
 * Win32 Only: Open the specified registry key.
 * @param newkey The opened registry key
 * @param parentkey The open registry key of the parent, or one of
 * <PRE>
 *           CLHY_REGKEY_CLASSES_ROOT
 *           CLHY_REGKEY_CURRENT_CONFIG
 *           CLHY_REGKEY_CURRENT_USER
 *           CLHY_REGKEY_LOCAL_MACHINE
 *           CLHY_REGKEY_USERS
 *           CLHY_REGKEY_PERFORMANCE_DATA
 *           CLHY_REGKEY_DYN_DATA
 * </PRE>
 * @param keyname The path of the key relative to the parent key
 * @param flags Or'ed value of:
 * <PRE>
 *           KUDA_READ             open key for reading
 *           KUDA_WRITE            open key for writing
 *           KUDA_CREATE           create the key if it doesn't exist
 *           KUDA_EXCL             return error if KUDA_CREATE and key exists
 * </PRE>
 * @param pool The pool in which newkey is allocated
 */
CLHY_DECLARE(kuda_status_t) clhy_regkey_open(clhy_regkey_t **newkey,
                                        const clhy_regkey_t *parentkey,
                                        const char *keyname,
                                        kuda_int32_t flags,
                                        kuda_pool_t *pool);

/**
 * Win32 Only: Close the registry key opened or created by clhy_regkey_open().
 * @param key The registry key to close
 */
CLHY_DECLARE(kuda_status_t) clhy_regkey_close(clhy_regkey_t *key);

/**
 * Win32 Only: Remove the given registry key.
 * @param parent The open registry key of the parent, or one of
 * <PRE>
 *           CLHY_REGKEY_CLASSES_ROOT
 *           CLHY_REGKEY_CURRENT_CONFIG
 *           CLHY_REGKEY_CURRENT_USER
 *           CLHY_REGKEY_LOCAL_MACHINE
 *           CLHY_REGKEY_USERS
 *           CLHY_REGKEY_PERFORMANCE_DATA
 *           CLHY_REGKEY_DYN_DATA
 * </PRE>
 * @param keyname The path of the key relative to the parent key
 * @param pool The pool used for temp allocations
 * @remark clhy_regkey_remove() is not recursive, although it removes
 * all values within the given keyname, it will not remove a key
 * containing subkeys.
 */
CLHY_DECLARE(kuda_status_t) clhy_regkey_remove(const clhy_regkey_t *parent,
                                          const char *keyname,
                                          kuda_pool_t *pool);

/**
 * Win32 Only: Retrieve a registry value string from an open key.
 * @param result The string value retrieved
 * @param key The registry key to retrieve the value from
 * @param valuename The named value to retrieve (pass "" for the default)
 * @param pool The pool used to store the result
 * @remark There is no toggle to prevent environment variable expansion
 * if the registry value is set with CLHY_REG_EXPAND (REG_EXPAND_SZ), such
 * expansions are always performed.
 */
CLHY_DECLARE(kuda_status_t) clhy_regkey_value_get(char **result,
                                             clhy_regkey_t *key,
                                             const char *valuename,
                                             kuda_pool_t *pool);

/**
 * Win32 Only: Store a registry value string into an open key.
 * @param key The registry key to store the value into
 * @param valuename The named value to store (pass "" for the default)
 * @param value The string to store for the named value
 * @param flags The option CLHY_REGKEY_EXPAND or 0, where CLHY_REGKEY_EXPAND
 * values will find all %foo% variables expanded from the environment.
 * @param pool The pool used for temp allocations
 */
CLHY_DECLARE(kuda_status_t) clhy_regkey_value_set(clhy_regkey_t *key,
                                             const char *valuename,
                                             const char *value,
                                             kuda_int32_t flags,
                                             kuda_pool_t *pool);

/**
 * Win32 Only: Retrieve a raw byte value from an open key.
 * @param result The raw bytes value retrieved
 * @param resultsize Pointer to a variable to store the number raw bytes retrieved
 * @param resulttype Pointer to a variable to store the registry type of the value retrieved
 * @param key The registry key to retrieve the value from
 * @param valuename The named value to retrieve (pass "" for the default)
 * @param pool The pool used to store the result
 */
CLHY_DECLARE(kuda_status_t) clhy_regkey_value_raw_get(void **result,
                                                 kuda_size_t *resultsize,
                                                 kuda_int32_t *resulttype,
                                                 clhy_regkey_t *key,
                                                 const char *valuename,
                                                 kuda_pool_t *pool);

/**
 * Win32 Only: Store a raw bytes value into an open key.
 * @param key The registry key to store the value into
 * @param valuename The named value to store (pass "" for the default)
 * @param value The bytes to store for the named value
 * @param valuesize The number of bytes for value
 * @param valuetype The
 * values will find all %foo% variables expanded from the environment.
 * @param pool The pool used for temp allocations
 */
CLHY_DECLARE(kuda_status_t) clhy_regkey_value_raw_set(clhy_regkey_t *key,
                                                 const char *valuename,
                                                 const void *value,
                                                 kuda_size_t  valuesize,
                                                 kuda_int32_t valuetype,
                                                 kuda_pool_t *pool);

/**
 * Win32 Only: Retrieve a registry value string from an open key.
 * @param result The string elements retrieved from a REG_MULTI_SZ string array
 * @param key The registry key to retrieve the value from
 * @param valuename The named value to retrieve (pass "" for the default)
 * @param pool The pool used to store the result
 */
CLHY_DECLARE(kuda_status_t) clhy_regkey_value_array_get(kuda_array_header_t **result,
                                                   clhy_regkey_t *key,
                                                   const char *valuename,
                                                   kuda_pool_t *pool);

/**
 * Win32 Only: Store a registry value string array into an open key.
 * @param key The registry key to store the value into
 * @param valuename The named value to store (pass "" for the default)
 * @param nelts The string elements to store in a REG_MULTI_SZ string array
 * @param elts The number of elements in the elts string array
 * @param pool The pool used for temp allocations
 */
CLHY_DECLARE(kuda_status_t) clhy_regkey_value_array_set(clhy_regkey_t *key,
                                                   const char *valuename,
                                                   int nelts,
                                                   const char * const * elts,
                                                   kuda_pool_t *pool);

/**
 * Win32 Only: Remove a registry value from an open key.
 * @param key The registry key to remove the value from
 * @param valuename The named value to remove (pass "" for the default)
 * @param pool The pool used for temp allocations
 */
CLHY_DECLARE(kuda_status_t) clhy_regkey_value_remove(const clhy_regkey_t *key,
                                                const char *valuename,
                                                kuda_pool_t *pool);

#ifdef __cplusplus
}
#endif

#endif /* def WIN32 || def DOXYGEN */

#endif /* CLHY_REGKEY_H */
