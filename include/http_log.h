/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  http_log.h
 * @brief cLHy Logging library
 *
 * @defgroup CLHYKUDEL_CORE_LOG Logging library
 * @ingroup  CLHYKUDEL_CORE
 * @{
 */

#ifndef CLHYKUDEL_HTTP_LOG_H
#define CLHYKUDEL_HTTP_LOG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "kuda_thread_proc.h"
#include "http_config.h"

#ifdef HAVE_SYSLOG
#include <syslog.h>

#ifndef LOG_PRIMASK
#define LOG_PRIMASK 7
#endif

#define CLHYLOG_EMERG     LOG_EMERG       /* system is unusable */
#define CLHYLOG_ALERT     LOG_ALERT       /* action must be taken immediately */
#define CLHYLOG_CRIT      LOG_CRIT        /* critical conditions */
#define CLHYLOG_ERR       LOG_ERR         /* error conditions */
#define CLHYLOG_WARNING   LOG_WARNING     /* warning conditions */
#define CLHYLOG_NOTICE    LOG_NOTICE      /* normal but significant condition */
#define CLHYLOG_INFO      LOG_INFO        /* informational */
#define CLHYLOG_DEBUG     LOG_DEBUG       /* debug-level messages */
#define CLHYLOG_TRACE1   (LOG_DEBUG + 1)  /* trace-level 1 messages */
#define CLHYLOG_TRACE2   (LOG_DEBUG + 2)  /* trace-level 2 messages */
#define CLHYLOG_TRACE3   (LOG_DEBUG + 3)  /* trace-level 3 messages */
#define CLHYLOG_TRACE4   (LOG_DEBUG + 4)  /* trace-level 4 messages */
#define CLHYLOG_TRACE5   (LOG_DEBUG + 5)  /* trace-level 5 messages */
#define CLHYLOG_TRACE6   (LOG_DEBUG + 6)  /* trace-level 6 messages */
#define CLHYLOG_TRACE7   (LOG_DEBUG + 7)  /* trace-level 7 messages */
#define CLHYLOG_TRACE8   (LOG_DEBUG + 8)  /* trace-level 8 messages */

#define CLHYLOG_LEVELMASK 15     /* mask off the level value */

#else

#define CLHYLOG_EMERG      0     /* system is unusable */
#define CLHYLOG_ALERT      1     /* action must be taken immediately */
#define CLHYLOG_CRIT       2     /* critical conditions */
#define CLHYLOG_ERR        3     /* error conditions */
#define CLHYLOG_WARNING    4     /* warning conditions */
#define CLHYLOG_NOTICE     5     /* normal but significant condition */
#define CLHYLOG_INFO       6     /* informational */
#define CLHYLOG_DEBUG      7     /* debug-level messages */
#define CLHYLOG_TRACE1     8     /* trace-level 1 messages */
#define CLHYLOG_TRACE2     9     /* trace-level 2 messages */
#define CLHYLOG_TRACE3    10     /* trace-level 3 messages */
#define CLHYLOG_TRACE4    11     /* trace-level 4 messages */
#define CLHYLOG_TRACE5    12     /* trace-level 5 messages */
#define CLHYLOG_TRACE6    13     /* trace-level 6 messages */
#define CLHYLOG_TRACE7    14     /* trace-level 7 messages */
#define CLHYLOG_TRACE8    15     /* trace-level 8 messages */

#define CLHYLOG_LEVELMASK 15     /* mask off the level value */

#endif

/* CLHYLOG_NOERRNO is ignored and should not be used.  It will be
 * removed in a future release of cLHy.
 */
#define CLHYLOG_NOERRNO           (CLHYLOG_LEVELMASK + 1)

/** Use CLHYLOG_TOCLIENT on clhy_log_rerror() to give content
 * handlers the option of including the error text in the
 * ErrorDocument sent back to the client. Setting CLHYLOG_TOCLIENT
 * will cause the error text to be saved in the request_rec->notes
 * table, keyed to the string "error-notes", if and only if:
 * - the severity level of the message is CLHYLOG_WARNING or greater
 * - there are no other "error-notes" set in request_rec->notes
 * Once error-notes is set, it is up to the content handler to
 * determine whether this text should be sent back to the client.
 * Note: Client generated text streams sent back to the client MUST
 * be escaped to prevent CSS attacks.
 */
#define CLHYLOG_TOCLIENT          ((CLHYLOG_LEVELMASK + 1) * 2)

/* normal but significant condition on startup, usually printed to stderr */
#define CLHYLOG_STARTUP           ((CLHYLOG_LEVELMASK + 1) * 4)

#ifndef DEFAULT_LOGLEVEL
#define DEFAULT_LOGLEVEL        CLHYLOG_WARNING
#endif

/**
 * CLHYLOGNO() should be used at the start of the format string passed
 * to clhy_log_error() and friends. The argument must be a 5 digit decimal
 * number. It creates a tag of the form "AH02182: "
 * See docs/log-message-tags/README for details.
 */
#define CLHYLOGNO(n)              "AH" #n ": "

/**
 * CLHYLOG_NO_CAPI may be passed as capi_index to clhy_log_error() and related
 * functions if the cAPI causing the log message is not known. Normally this
 * should not be used directly. Use ::CLHYLOG_MARK or ::CLHYLOG_CAPI_INDEX
 * instead.
 *
 * @see CLHYLOG_MARK
 * @see CLHYLOG_CAPI_INDEX
 * @see clhy_log_error
 */
#define CLHYLOG_NO_CAPI         -1

#ifdef __cplusplus
/**
 * C++ cAPIs must invoke ::CLHYLOG_USE_CAPI or ::CLHY_DECLARE_CAPI in
 * every file which uses clhy_log_* before the first use of ::CLHYLOG_MARK
 * or ::CLHYLOG_CAPI_INDEX.
 * (C cAPIs *should* do that as well, to enable cAPI-specific log
 * levels. C cAPIs need not obey the ordering, though).
 */
#else /* __cplusplus */
/**
 * Constant to store capi_index for the current file.
 * Objects with static storage duration are set to NULL if not
 * initialized explicitly. This means that if clhylog_capi_index
 * is not initialized using the ::CLHYLOG_USE_CAPI or the
 * ::CLHY_DECLARE_CAPI macro, we can safely fall back to
 * use ::CLHYLOG_NO_CAPI. This variable will usually be optimized away.
 */
static int * const clhylog_capi_index;
#endif /* __cplusplus */

/**
 * CLHYLOG_CAPI_INDEX contains the capi_index of the current cAPI if
 * it has been set via the ::CLHYLOG_USE_CAPI or ::CLHY_DECLARE_CAPI macro.
 * Otherwise it contains ::CLHYLOG_NO_CAPI (for example in unmodified wwhy
 * 2.2 cAPIs).
 *
 * If ::CLHYLOG_MARK is used in clhy_log_error() and related functions,
 * ::CLHYLOG_CAPI_INDEX will be passed as capi_index. In cases where
 * ::CLHYLOG_MARK cannot be used, ::CLHYLOG_CAPI_INDEX should normally be passed
 * as capi_index.
 *
 * @see CLHYLOG_MARK
 * @see clhy_log_error
 */
#ifdef __cplusplus
#define CLHYLOG_CAPI_INDEX (*clhylog_capi_index)
#else /* __cplusplus */
#define CLHYLOG_CAPI_INDEX  \
    (clhylog_capi_index ? *clhylog_capi_index : CLHYLOG_NO_CAPI)
#endif /* __cplusplus */

/**
 * CLHYLOG_MAX_LOGLEVEL can be defined to remove logging above some
 * specified level at compile time.
 *
 * This requires a C99 compiler.
 */
#ifdef DOXYGEN
#define CLHYLOG_MAX_LOGLEVEL
#endif
#ifndef CLHYLOG_MAX_LOGLEVEL
#define CLHYLOG_CAPI_IS_LEVEL(s,capi_index,level)              \
          ( (((level)&CLHYLOG_LEVELMASK) <= CLHYLOG_NOTICE) ||       \
            (s == NULL) ||                                       \
            (clhy_get_server_capi_loglevel(s, capi_index)      \
             >= ((level)&CLHYLOG_LEVELMASK) ) )
#define CLHYLOG_C_CAPI_IS_LEVEL(c,capi_index,level)            \
          ( (((level)&CLHYLOG_LEVELMASK) <= CLHYLOG_NOTICE) ||       \
            (clhy_get_conn_capi_loglevel(c, capi_index)        \
             >= ((level)&CLHYLOG_LEVELMASK) ) )
#define CLHYLOG_CS_CAPI_IS_LEVEL(c,s,capi_index,level)            \
          ( (((level)&CLHYLOG_LEVELMASK) <= CLHYLOG_NOTICE) ||          \
            (clhy_get_conn_server_capi_loglevel(c, s, capi_index) \
             >= ((level)&CLHYLOG_LEVELMASK) ) )
#define CLHYLOG_R_CAPI_IS_LEVEL(r,capi_index,level)            \
          ( (((level)&CLHYLOG_LEVELMASK) <= CLHYLOG_NOTICE) ||       \
            (clhy_get_request_capi_loglevel(r, capi_index)     \
             >= ((level)&CLHYLOG_LEVELMASK) ) )
#else
#define CLHYLOG_CAPI_IS_LEVEL(s,capi_index,level)              \
        ( (((level)&CLHYLOG_LEVELMASK) <= CLHYLOG_MAX_LOGLEVEL) &&   \
          ( (((level)&CLHYLOG_LEVELMASK) <= CLHYLOG_NOTICE) ||       \
            (s == NULL) ||                                       \
            (clhy_get_server_capi_loglevel(s, capi_index)      \
             >= ((level)&CLHYLOG_LEVELMASK) ) ) )
#define CLHYLOG_CS_CAPI_IS_LEVEL(c,s,capi_index,level)            \
        ( (((level)&CLHYLOG_LEVELMASK) <= CLHYLOG_MAX_LOGLEVEL) &&      \
          ( (((level)&CLHYLOG_LEVELMASK) <= CLHYLOG_NOTICE) ||          \
            (clhy_get_conn_server_capi_loglevel(c, s, capi_index) \
             >= ((level)&CLHYLOG_LEVELMASK) ) ) )
#define CLHYLOG_C_CAPI_IS_LEVEL(c,capi_index,level)            \
        ( (((level)&CLHYLOG_LEVELMASK) <= CLHYLOG_MAX_LOGLEVEL) &&   \
          ( (((level)&CLHYLOG_LEVELMASK) <= CLHYLOG_NOTICE) ||       \
            (clhy_get_conn_capi_loglevel(c, capi_index)        \
             >= ((level)&CLHYLOG_LEVELMASK) ) ) )
#define CLHYLOG_R_CAPI_IS_LEVEL(r,capi_index,level)            \
        ( (((level)&CLHYLOG_LEVELMASK) <= CLHYLOG_MAX_LOGLEVEL) &&   \
          ( (((level)&CLHYLOG_LEVELMASK) <= CLHYLOG_NOTICE) ||       \
            (clhy_get_request_capi_loglevel(r, capi_index)     \
             >= ((level)&CLHYLOG_LEVELMASK) ) ) )
#endif

#define CLHYLOG_IS_LEVEL(s,level)     \
    CLHYLOG_CAPI_IS_LEVEL(s,CLHYLOG_CAPI_INDEX,level)
#define CLHYLOG_C_IS_LEVEL(c,level)   \
    CLHYLOG_C_CAPI_IS_LEVEL(c,CLHYLOG_CAPI_INDEX,level)
#define CLHYLOG_CS_IS_LEVEL(c,s,level) \
    CLHYLOG_CS_CAPI_IS_LEVEL(c,s,CLHYLOG_CAPI_INDEX,level)
#define CLHYLOG_R_IS_LEVEL(r,level)   \
    CLHYLOG_R_CAPI_IS_LEVEL(r,CLHYLOG_CAPI_INDEX,level)


#define CLHYLOGinfo(s)                CLHYLOG_IS_LEVEL(s,CLHYLOG_INFO)
#define CLHYLOGdebug(s)               CLHYLOG_IS_LEVEL(s,CLHYLOG_DEBUG)
#define CLHYLOGtrace1(s)              CLHYLOG_IS_LEVEL(s,CLHYLOG_TRACE1)
#define CLHYLOGtrace2(s)              CLHYLOG_IS_LEVEL(s,CLHYLOG_TRACE2)
#define CLHYLOGtrace3(s)              CLHYLOG_IS_LEVEL(s,CLHYLOG_TRACE3)
#define CLHYLOGtrace4(s)              CLHYLOG_IS_LEVEL(s,CLHYLOG_TRACE4)
#define CLHYLOGtrace5(s)              CLHYLOG_IS_LEVEL(s,CLHYLOG_TRACE5)
#define CLHYLOGtrace6(s)              CLHYLOG_IS_LEVEL(s,CLHYLOG_TRACE6)
#define CLHYLOGtrace7(s)              CLHYLOG_IS_LEVEL(s,CLHYLOG_TRACE7)
#define CLHYLOGtrace8(s)              CLHYLOG_IS_LEVEL(s,CLHYLOG_TRACE8)

#define CLHYLOGrinfo(r)               CLHYLOG_R_IS_LEVEL(r,CLHYLOG_INFO)
#define CLHYLOGrdebug(r)              CLHYLOG_R_IS_LEVEL(r,CLHYLOG_DEBUG)
#define CLHYLOGrtrace1(r)             CLHYLOG_R_IS_LEVEL(r,CLHYLOG_TRACE1)
#define CLHYLOGrtrace2(r)             CLHYLOG_R_IS_LEVEL(r,CLHYLOG_TRACE2)
#define CLHYLOGrtrace3(r)             CLHYLOG_R_IS_LEVEL(r,CLHYLOG_TRACE3)
#define CLHYLOGrtrace4(r)             CLHYLOG_R_IS_LEVEL(r,CLHYLOG_TRACE4)
#define CLHYLOGrtrace5(r)             CLHYLOG_R_IS_LEVEL(r,CLHYLOG_TRACE5)
#define CLHYLOGrtrace6(r)             CLHYLOG_R_IS_LEVEL(r,CLHYLOG_TRACE6)
#define CLHYLOGrtrace7(r)             CLHYLOG_R_IS_LEVEL(r,CLHYLOG_TRACE7)
#define CLHYLOGrtrace8(r)             CLHYLOG_R_IS_LEVEL(r,CLHYLOG_TRACE8)

#define CLHYLOGcinfo(c)               CLHYLOG_C_IS_LEVEL(c,CLHYLOG_INFO)
#define CLHYLOGcdebug(c)              CLHYLOG_C_IS_LEVEL(c,CLHYLOG_DEBUG)
#define CLHYLOGctrace1(c)             CLHYLOG_C_IS_LEVEL(c,CLHYLOG_TRACE1)
#define CLHYLOGctrace2(c)             CLHYLOG_C_IS_LEVEL(c,CLHYLOG_TRACE2)
#define CLHYLOGctrace3(c)             CLHYLOG_C_IS_LEVEL(c,CLHYLOG_TRACE3)
#define CLHYLOGctrace4(c)             CLHYLOG_C_IS_LEVEL(c,CLHYLOG_TRACE4)
#define CLHYLOGctrace5(c)             CLHYLOG_C_IS_LEVEL(c,CLHYLOG_TRACE5)
#define CLHYLOGctrace6(c)             CLHYLOG_C_IS_LEVEL(c,CLHYLOG_TRACE6)
#define CLHYLOGctrace7(c)             CLHYLOG_C_IS_LEVEL(c,CLHYLOG_TRACE7)
#define CLHYLOGctrace8(c)             CLHYLOG_C_IS_LEVEL(c,CLHYLOG_TRACE8)

CLHY_DECLARE_DATA extern int clhy_default_loglevel;

/**
 * CLHYLOG_MARK is a convenience macro for use as the first three parameters in
 * clhy_log_error() and related functions, i.e. file, line, and capi_index.
 *
 * The capi_index parameter was introduced in version 2.3.6. Before that
 * version, CLHYLOG_MARK only replaced the file and line parameters.
 * This means that CLHYLOG_MARK can be used with clhy_log_*error in all versions
 * of cLHy wwhy.
 *
 * @see CLHYLOG_CAPI_INDEX
 * @see clhy_log_error
 * @see clhy_log_cerror
 * @see clhy_log_rerror
 * @see clhy_log_cserror
 */
#define CLHYLOG_MARK     __FILE__,__LINE__,CLHYLOG_CAPI_INDEX

/**
 * Set up for logging to stderr.
 * @param p The pool to allocate out of
 */
CLHY_DECLARE(void) clhy_open_stderr_log(kuda_pool_t *p);

/**
 * Replace logging to stderr with logging to the given file.
 * @param p The pool to allocate out of
 * @param file Name of the file to log stderr output
 */
CLHY_DECLARE(kuda_status_t) clhy_replace_stderr_log(kuda_pool_t *p,
                                               const char *file);

/**
 * Open the error log and replace stderr with it.
 * @param pconf Not used
 * @param plog  The pool to allocate the logs from
 * @param ptemp Pool used for temporary allocations
 * @param s_main The main server
 * @note clhy_open_logs isn't expected to be used by cAPIs, it is
 * an internal core function
 */
int clhy_open_logs(kuda_pool_t *pconf, kuda_pool_t *plog,
                 kuda_pool_t *ptemp, server_rec *s_main);

/**
 * Perform special processing for piped loggers in cLMP child
 * processes.
 * @param p Not used
 * @param s Not used
 * @note clhy_logs_child_init is not for use by cAPIs; it is an
 * internal core function
 */
void clhy_logs_child_init(kuda_pool_t *p, server_rec *s);

/*
 * The primary logging functions, clhy_log_error, clhy_log_rerror, clhy_log_cerror,
 * and clhy_log_perror use a printf style format string to build the log message.
 * It is VERY IMPORTANT that you not include any raw data from the network,
 * such as the request-URI or request header fields, within the format
 * string.  Doing so makes the server vulnerable to a denial-of-service
 * attack and other messy behavior.  Instead, use a simple format string
 * like "%s", followed by the string containing the untrusted data.
 */

/**
 * clhy_log_error() - log messages which are not related to a particular
 * request or connection.  This uses a printf-like format to log messages
 * to the error_log.
 * @param file The file in which this function is called
 * @param line The line number on which this function is called
 * @param capi_index The capi_index of the cAPI generating this message
 * @param level The level of this error message
 * @param status The status code from the previous command
 * @param s The server on which we are logging
 * @param fmt The format string
 * @param ... The arguments to use to fill out fmt.
 * @note clhy_log_error is implemented as a macro
 * @note Use CLHYLOG_MARK to fill out file and line
 * @note If a request_rec is available, use that with clhy_log_rerror()
 * in preference to calling this function.  Otherwise, if a conn_rec is
 * available, use that with clhy_log_cerror() in preference to calling
 * this function.
 * @warning It is VERY IMPORTANT that you not include any raw data from
 * the network, such as the request-URI or request header fields, within
 * the format string.  Doing so makes the server vulnerable to a
 * denial-of-service attack and other messy behavior.  Instead, use a
 * simple format string like "%s", followed by the string containing the
 * untrusted data.
 */
#ifdef DOXYGEN
CLHY_DECLARE(void) clhy_log_error(const char *file, int line, int capi_index,
                              int level, kuda_status_t status,
                              const server_rec *s, const char *fmt, ...);
#else
#ifdef CLHY_HAVE_C99
/* need additional step to expand CLHYLOG_MARK first */
#define clhy_log_error(...) clhy_log_error__(__VA_ARGS__)
/* need server_rec *sr = ... for the case if s is verbatim NULL */
#define clhy_log_error__(file, line, mi, level, status, s, ...)           \
    do { const server_rec *sr__ = s; if (CLHYLOG_CAPI_IS_LEVEL(sr__, mi, level)) \
             clhy_log_error_(file, line, mi, level, status, sr__, __VA_ARGS__);    \
    } while(0)
#else
#define clhy_log_error clhy_log_error_
#endif
CLHY_DECLARE(void) clhy_log_error_(const char *file, int line, int capi_index,
                               int level, kuda_status_t status,
                               const server_rec *s, const char *fmt, ...)
                              __attribute__((format(printf,7,8)));
#endif

/**
 * clhy_log_perror() - log messages which are not related to a particular
 * request, connection, or virtual server.  This uses a printf-like
 * format to log messages to the error_log.
 * @param file The file in which this function is called
 * @param line The line number on which this function is called
 * @param capi_index ignored dummy value for use by CLHYLOG_MARK
 * @param level The level of this error message
 * @param status The status code from the previous command
 * @param p The pool which we are logging for
 * @param fmt The format string
 * @param ... The arguments to use to fill out fmt.
 * @note clhy_log_perror is implemented as a macro
 * @note Use CLHYLOG_MARK to fill out file, line, and capi_index
 * @warning It is VERY IMPORTANT that you not include any raw data from
 * the network, such as the request-URI or request header fields, within
 * the format string.  Doing so makes the server vulnerable to a
 * denial-of-service attack and other messy behavior.  Instead, use a
 * simple format string like "%s", followed by the string containing the
 * untrusted data.
 */
#ifdef DOXYGEN
CLHY_DECLARE(void) clhy_log_perror(const char *file, int line, int capi_index,
                               int level, kuda_status_t status, kuda_pool_t *p,
                               const char *fmt, ...);
#else
#if defined(CLHY_HAVE_C99) && defined(CLHYLOG_MAX_LOGLEVEL)
/* need additional step to expand CLHYLOG_MARK first */
#define clhy_log_perror(...) clhy_log_perror__(__VA_ARGS__)
#define clhy_log_perror__(file, line, mi, level, status, p, ...)            \
    do { if ((level) <= CLHYLOG_MAX_LOGLEVEL )                              \
             clhy_log_perror_(file, line, mi, level, status, p,             \
                            __VA_ARGS__); } while(0)
#else
#define clhy_log_perror clhy_log_perror_
#endif
CLHY_DECLARE(void) clhy_log_perror_(const char *file, int line, int capi_index,
                                int level, kuda_status_t status, kuda_pool_t *p,
                                const char *fmt, ...)
                               __attribute__((format(printf,7,8)));
#endif

/**
 * clhy_log_rerror() - log messages which are related to a particular
 * request.  This uses a printf-like format to log messages to the
 * error_log.
 * @param file The file in which this function is called
 * @param line The line number on which this function is called
 * @param capi_index The capi_index of the cAPI generating this message
 * @param level The level of this error message
 * @param status The status code from the previous command
 * @param r The request which we are logging for
 * @param fmt The format string
 * @param ... The arguments to use to fill out fmt.
 * @note clhy_log_rerror is implemented as a macro
 * @note Use CLHYLOG_MARK to fill out file, line, and capi_index
 * @warning It is VERY IMPORTANT that you not include any raw data from
 * the network, such as the request-URI or request header fields, within
 * the format string.  Doing so makes the server vulnerable to a
 * denial-of-service attack and other messy behavior.  Instead, use a
 * simple format string like "%s", followed by the string containing the
 * untrusted data.
 */
#ifdef DOXYGEN
CLHY_DECLARE(void) clhy_log_rerror(const char *file, int line, int capi_index,
                               int level, kuda_status_t status,
                               const request_rec *r, const char *fmt, ...);
#else
#ifdef CLHY_HAVE_C99
/* need additional step to expand CLHYLOG_MARK first */
#define clhy_log_rerror(...) clhy_log_rerror__(__VA_ARGS__)
#define clhy_log_rerror__(file, line, mi, level, status, r, ...)              \
    do { if (CLHYLOG_R_CAPI_IS_LEVEL(r, mi, level))                         \
             clhy_log_rerror_(file, line, mi, level, status, r, __VA_ARGS__); \
    } while(0)
#else
#define clhy_log_rerror clhy_log_rerror_
#endif
CLHY_DECLARE(void) clhy_log_rerror_(const char *file, int line, int capi_index,
                                int level, kuda_status_t status,
                                const request_rec *r, const char *fmt, ...)
                                __attribute__((format(printf,7,8)));
#endif

/**
 * clhy_log_cerror() - log messages which are related to a particular
 * connection.  This uses a printf-like format to log messages to the
 * error_log.
 * @param file The file in which this function is called
 * @param line The line number on which this function is called
 * @param level The level of this error message
 * @param capi_index The capi_index of the cAPI generating this message
 * @param status The status code from the previous command
 * @param c The connection which we are logging for
 * @param fmt The format string
 * @param ... The arguments to use to fill out fmt.
 * @note clhy_log_cerror is implemented as a macro
 * @note Use CLHYLOG_MARK to fill out file, line, and capi_index
 * @note If a request_rec is available, use that with clhy_log_rerror()
 * in preference to calling this function.
 * @warning It is VERY IMPORTANT that you not include any raw data from
 * the network, such as the request-URI or request header fields, within
 * the format string.  Doing so makes the server vulnerable to a
 * denial-of-service attack and other messy behavior.  Instead, use a
 * simple format string like "%s", followed by the string containing the
 * untrusted data.
 */
#ifdef DOXYGEN
CLHY_DECLARE(void) clhy_log_cerror(const char *file, int line, int capi_index,
                               int level, kuda_status_t status,
                               const conn_rec *c, const char *fmt, ...);
#else
#ifdef CLHY_HAVE_C99
/* need additional step to expand CLHYLOG_MARK first */
#define clhy_log_cerror(...) clhy_log_cerror__(__VA_ARGS__)
#define clhy_log_cerror__(file, line, mi, level, status, c, ...)              \
    do { if (CLHYLOG_C_CAPI_IS_LEVEL(c, mi, level))                         \
             clhy_log_cerror_(file, line, mi, level, status, c, __VA_ARGS__); \
    } while(0)
#else
#define clhy_log_cerror clhy_log_cerror_
#endif
CLHY_DECLARE(void) clhy_log_cerror_(const char *file, int line, int capi_index,
                                int level, kuda_status_t status,
                                const conn_rec *c, const char *fmt, ...)
                                __attribute__((format(printf,7,8)));
#endif

/**
 * clhy_log_cserror() - log messages which are related to a particular
 * connection and to a vhost other than c->base_server.  This uses a
 * printf-like format to log messages to the error_log.
 * @param file The file in which this function is called
 * @param line The line number on which this function is called
 * @param level The level of this error message
 * @param capi_index The capi_index of the cAPI generating this message
 * @param status The status code from the previous command
 * @param c The connection which we are logging for
 * @param s The server which we are logging for
 * @param fmt The format string
 * @param ... The arguments to use to fill out fmt.
 * @note clhy_log_cserror is implemented as a macro
 * @note Use CLHYLOG_MARK to fill out file, line, and capi_index
 * @note If a request_rec is available, use that with clhy_log_rerror()
 * in preference to calling this function. This function is mainly useful for
 * cAPIs like capi_ssl to use before the request_rec is created.
 * @warning It is VERY IMPORTANT that you not include any raw data from
 * the network, such as the request-URI or request header fields, within
 * the format string.  Doing so makes the server vulnerable to a
 * denial-of-service attack and other messy behavior.  Instead, use a
 * simple format string like "%s", followed by the string containing the
 * untrusted data.
 */
#ifdef DOXYGEN
CLHY_DECLARE(void) clhy_log_cserror(const char *file, int line, int capi_index,
                                int level, kuda_status_t status,
                                const conn_rec *c, const server_rec *s,
                                const char *fmt, ...);
#else
#ifdef CLHY_HAVE_C99
/* need additional step to expand CLHYLOG_MARK first */
#define clhy_log_cserror(...) clhy_log_cserror__(__VA_ARGS__)
#define clhy_log_cserror__(file, line, mi, level, status, c, s, ...)  \
    do { if (CLHYLOG_CS_CAPI_IS_LEVEL(c, s, mi, level))             \
             clhy_log_cserror_(file, line, mi, level, status, c, s,   \
                             __VA_ARGS__);                          \
    } while(0)
#else
#define clhy_log_cserror clhy_log_cserror_
#endif
CLHY_DECLARE(void) clhy_log_cserror_(const char *file, int line, int capi_index,
                                 int level, kuda_status_t status,
                                 const conn_rec *c, const server_rec *s,
                                 const char *fmt, ...)
                             __attribute__((format(printf,8,9)));
#endif

/*
 * The buffer logging functions, clhy_log_data, clhy_log_rdata, clhy_log_cdata,
 * and clhy_log_csdata log a buffer in printable and hex format.  The exact
 * format is controlled by processing flags, described next.
 */

/**
 * Processing flags for clhy_log_data() et al
 *
 * CLHY_LOG_DATA_DEFAULT - default formatting, with printable chars and hex
 * CLHY_LOG_DATA_SHOW_OFFSET - prefix each line with hex offset from the start
 * of the buffer
 */
#define CLHY_LOG_DATA_DEFAULT       0
#define CLHY_LOG_DATA_SHOW_OFFSET   1

/**
 * clhy_log_data() - log buffers which are not related to a particular request
 * or connection.
 * @param file The file in which this function is called
 * @param line The line number on which this function is called
 * @param capi_index The capi_index of the cAPI logging this buffer
 * @param level The log level
 * @param s The server on which we are logging
 * @param label A label for the buffer, to be logged preceding the buffer
 * @param data The buffer to be logged
 * @param len The length of the buffer
 * @param flags Special processing flags like CLHY_LOG_DATA_SHOW_OFFSET
 * @note clhy_log_data is implemented as a macro.
 * @note Use CLHYLOG_MARK to fill out file, line, and capi_index
 * @note If a request_rec is available, use that with clhy_log_rdata()
 * in preference to calling this function.  Otherwise, if a conn_rec is
 * available, use that with clhy_log_cdata() in preference to calling
 * this function.
 */
#ifdef DOXYGEN
CLHY_DECLARE(void) clhy_log_data(const char *file, int line, int capi_index,
                             int level, const server_rec *s, const char *label,
                             const void *data, kuda_size_t len, unsigned int flags);
#else
#ifdef CLHY_HAVE_C99
/* need additional step to expand CLHYLOG_MARK first */
#define clhy_log_data(...) clhy_log_data__(__VA_ARGS__)
/* need server_rec *sr = ... for the case if s is verbatim NULL */
#define clhy_log_data__(file, line, mi, level, s, ...)           \
    do { const server_rec *sr__ = s; if (CLHYLOG_CAPI_IS_LEVEL(sr__, mi, level)) \
             clhy_log_data_(file, line, mi, level, sr__, __VA_ARGS__);    \
    } while(0)
#else
#define clhy_log_data clhy_log_data_
#endif
CLHY_DECLARE(void) clhy_log_data_(const char *file, int line, int capi_index,
                              int level, const server_rec *s, const char *label,
                              const void *data, kuda_size_t len, unsigned int flags);
#endif

/**
 * clhy_log_rdata() - log buffers which are related to a particular request.
 * @param file The file in which this function is called
 * @param line The line number on which this function is called
 * @param capi_index The capi_index of the cAPI logging this buffer
 * @param level The log level
 * @param r The request which we are logging for
 * @param label A label for the buffer, to be logged preceding the buffer
 * @param data The buffer to be logged
 * @param len The length of the buffer
 * @param flags Special processing flags like CLHY_LOG_DATA_SHOW_OFFSET
 * @note clhy_log_rdata is implemented as a macro.
 * @note Use CLHYLOG_MARK to fill out file, line, and capi_index
 * @note If a request_rec is available, use that with clhy_log_rerror()
 * in preference to calling this function.  Otherwise, if a conn_rec is
 * available, use that with clhy_log_cerror() in preference to calling
 * this function.
 */
#ifdef DOXYGEN
CLHY_DECLARE(void) clhy_log_rdata(const char *file, int line, int capi_index,
                              int level, const request_rec *r, const char *label,
                              const void *data, kuda_size_t len, unsigned int flags);
#else
#ifdef CLHY_HAVE_C99
/* need additional step to expand CLHYLOG_MARK first */
#define clhy_log_rdata(...) clhy_log_rdata__(__VA_ARGS__)
#define clhy_log_rdata__(file, line, mi, level, r, ...)           \
    do { if (CLHYLOG_R_CAPI_IS_LEVEL(r, mi, level)) \
             clhy_log_rdata_(file, line, mi, level, r, __VA_ARGS__);    \
    } while(0)
#else
#define clhy_log_rdata clhy_log_rdata_
#endif
CLHY_DECLARE(void) clhy_log_rdata_(const char *file, int line, int capi_index,
                               int level, const request_rec *r, const char *label,
                               const void *data, kuda_size_t len, unsigned int flags);
#endif

/**
 * clhy_log_cdata() - log buffers which are related to a particular connection.
 * @param file The file in which this function is called
 * @param line The line number on which this function is called
 * @param capi_index The capi_index of the cAPI logging this buffer
 * @param level The log level
 * @param c The connection which we are logging for
 * @param label A label for the buffer, to be logged preceding the buffer
 * @param data The buffer to be logged
 * @param len The length of the buffer
 * @param flags Special processing flags like CLHY_LOG_DATA_SHOW_OFFSET
 * @note clhy_log_cdata is implemented as a macro
 * @note Use CLHYLOG_MARK to fill out file, line, and capi_index
 * @note If a request_rec is available, use that with clhy_log_rerror()
 * in preference to calling this function.  Otherwise, if a conn_rec is
 * available, use that with clhy_log_cerror() in preference to calling
 * this function.
 */
#ifdef DOXYGEN
CLHY_DECLARE(void) clhy_log_cdata(const char *file, int line, int capi_index,
                              int level, const conn_rec *c, const char *label,
                              const void *data, kuda_size_t len, unsigned int flags);
#else
#ifdef CLHY_HAVE_C99
/* need additional step to expand CLHYLOG_MARK first */
#define clhy_log_cdata(...) clhy_log_cdata__(__VA_ARGS__)
#define clhy_log_cdata__(file, line, mi, level, c, ...)           \
    do { if (CLHYLOG_C_CAPI_IS_LEVEL(c, mi, level)) \
             clhy_log_cdata_(file, line, mi, level, c, __VA_ARGS__);    \
    } while(0)
#else
#define clhy_log_cdata clhy_log_cdata_
#endif
CLHY_DECLARE(void) clhy_log_cdata_(const char *file, int line, int capi_index,
                               int level, const conn_rec *c, const char *label,
                               const void *data, kuda_size_t len, unsigned int flags);
#endif

/**
 * clhy_log_csdata() - log buffers which are related to a particular connection
 * and to a vhost other than c->base_server.
 * @param file The file in which this function is called
 * @param line The line number on which this function is called
 * @param capi_index The capi_index of the cAPI logging this buffer
 * @param level The log level
 * @param c The connection which we are logging for
 * @param s The server which we are logging for
 * @param label A label for the buffer, to be logged preceding the buffer
 * @param data The buffer to be logged
 * @param len The length of the buffer
 * @param flags Special processing flags like CLHY_LOG_DATA_SHOW_OFFSET
 * @note clhy_log_csdata is implemented as a macro
 * @note Use CLHYLOG_MARK to fill out file, line, and capi_index
 * @note If a request_rec is available, use that with clhy_log_rerror()
 * in preference to calling this function.  Otherwise, if a conn_rec is
 * available, use that with clhy_log_cerror() in preference to calling
 * this function.
 */
#ifdef DOXYGEN
CLHY_DECLARE(void) clhy_log_csdata(const char *file, int line, int capi_index,
                               int level, const conn_rec *c, const server_rec *s,
                               const char *label, const void *data,
                               kuda_size_t len, unsigned int flags);
#else
#ifdef CLHY_HAVE_C99
/* need additional step to expand CLHYLOG_MARK first */
#define clhy_log_csdata(...) clhy_log_csdata__(__VA_ARGS__)
#define clhy_log_csdata__(file, line, mi, level, c, s, ...)              \
    do { if (CLHYLOG_CS_CAPI_IS_LEVEL(c, s, mi, level))                \
             clhy_log_csdata_(file, line, mi, level, c, s, __VA_ARGS__); \
    } while(0)
#else
#define clhy_log_cdata clhy_log_cdata_
#endif
CLHY_DECLARE(void) clhy_log_csdata_(const char *file, int line, int capi_index,
                                int level, const conn_rec *c, const server_rec *s,
                                const char *label, const void *data,
                                kuda_size_t len, unsigned int flags);
#endif

/**
 * Convert stderr to the error log
 * @param s The current server
 */
CLHY_DECLARE(void) clhy_error_log2stderr(server_rec *s);

/**
 * Log the command line used to start the server.
 * @param p The pool to use for logging
 * @param s The server_rec whose process's command line we want to log.
 * The command line is logged to that server's error log.
 */
CLHY_DECLARE(void) clhy_log_command_line(kuda_pool_t *p, server_rec *s);

/**
 * Log common (various) cLMP shared data at startup.
 * @param s The server_rec of the error log we want to log to.
 * Misc commonly logged data is logged to that server's error log.
 */
CLHY_DECLARE(void) clhy_log_core_common(server_rec *s);

/**
 * Log the current pid of the parent process
 * @param p The pool to use for processing
 * @param fname The name of the file to log to.  If the filename is not
 * absolute then it is assumed to be relative to ServerRoot.
 */
CLHY_DECLARE(void) clhy_log_pid(kuda_pool_t *p, const char *fname);

/**
 * Remove the pidfile.
 * @param p The pool to use for processing
 * @param fname The name of the pid file to remove.  If the filename is not
 * absolute then it is assumed to be relative to ServerRoot.
 */
CLHY_DECLARE(void) clhy_remove_pid(kuda_pool_t *p, const char *fname);

/**
 * Retrieve the pid from a pidfile.
 * @param p The pool to use for processing
 * @param filename The name of the file containing the pid.  If the filename is not
 * absolute then it is assumed to be relative to ServerRoot.
 * @param mypid Pointer to pid_t (valid only if return KUDA_SUCCESS)
 */
CLHY_DECLARE(kuda_status_t) clhy_read_pid(kuda_pool_t *p, const char *filename, pid_t *mypid);

/** @see piped_log */
typedef struct piped_log piped_log;

/**
 * Open the piped log process
 * @param p The pool to allocate out of
 * @param program The program to run in the logging process
 * @return The piped log structure
 * @note The log program is invoked as @p KUDA_PROGRAM_ENV,
 *      @see clhy_open_piped_log_ex to modify this behavior
 */
CLHY_DECLARE(piped_log *) clhy_open_piped_log(kuda_pool_t *p, const char *program);

/**
 * Open the piped log process specifying the execution choice for program
 * @param p The pool to allocate out of
 * @param program The program to run in the logging process
 * @param cmdtype How to invoke program, e.g. KUDA_PROGRAM, KUDA_SHELLCMD_ENV, etc
 * @return The piped log structure
 */
CLHY_DECLARE(piped_log *) clhy_open_piped_log_ex(kuda_pool_t *p,
                                             const char *program,
                                             kuda_cmdtype_e cmdtype);

/**
 * Close the piped log and kill the logging process
 * @param pl The piped log structure
 */
CLHY_DECLARE(void) clhy_close_piped_log(piped_log *pl);

/**
 * A function to return the read side of the piped log pipe
 * @param pl The piped log structure
 * @return The native file descriptor
 */
CLHY_DECLARE(kuda_file_t *) clhy_piped_log_read_fd(piped_log *pl);

/**
 * A function to return the write side of the piped log pipe
 * @param pl The piped log structure
 * @return The native file descriptor
 */
CLHY_DECLARE(kuda_file_t *) clhy_piped_log_write_fd(piped_log *pl);

/**
 * hook method to generate unique id for connection or request
 * @ingroup hooks
 * @param c the conn_rec of the connections
 * @param r the request_req (may be NULL)
 * @param id the place where to store the unique id
 * @return OK or DECLINE
 */
CLHY_DECLARE_HOOK(int, generate_log_id,
                (const conn_rec *c, const request_rec *r, const char **id))


#ifdef __cplusplus
}
#endif

#endif  /* !CLHYKUDEL_HTTP_LOG_H */
/** @} */
