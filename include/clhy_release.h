/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file clhy_release.h
 * @brief Version Release defines
 */

#ifndef CLHY_RELEASE_H
#define CLHY_RELEASE_H

#define CLHY_SERVER_COPYRIGHT \
  "Copyright (C) 2019 The Hyang Language Foundation."

/*
 * The below defines the base string of the Server: header. Additional
 * tokens can be added via the clhy_add_version_component() API call.
 *
 * The tokens are listed in order of their significance for identifying the
 * application.
 *
 * "Product tokens should be short and to the point -- use of them for
 * advertizing or other non-essential information is explicitly forbidden."
 *
 * Example: "cLHy/1.1.0 MrWidget/0.1-alpha"
 */
#define CLHY_SERVER_BASEVENDOR "Hyang Language Foundation"
#define CLHY_SERVER_BASEPROJECT "cLHy HTTP Server"
#define CLHY_SERVER_BASEPRODUCT "cLHy"

#define CLHY_SERVER_MAJORVERSION_NUMBER 1
#define CLHY_SERVER_MINORVERSION_NUMBER 6
#define CLHY_SERVER_PATCHLEVEL_NUMBER   28
#define CLHY_SERVER_DEVBUILD_BOOLEAN    0

/* Synchronize the above with docs/manual/style/version.ent */

#if !CLHY_SERVER_DEVBUILD_BOOLEAN
#define CLHY_SERVER_ADD_STRING          ""
#else
#ifndef CLHY_SERVER_ADD_STRING
#define CLHY_SERVER_ADD_STRING          "-dev"
#endif
#endif

/* KUDA_STRINGIFY is defined here, and also in kuda_general.h, so wrap it */
#ifndef KUDA_STRINGIFY
/** Properly quote a value as a string in the C preprocessor */
#define KUDA_STRINGIFY(n) KUDA_STRINGIFY_HELPER(n)
/** Helper macro for KUDA_STRINGIFY */
#define KUDA_STRINGIFY_HELPER(n) #n
#endif

/* keep old macros as well */
#define CLHY_SERVER_MAJORVERSION  KUDA_STRINGIFY(CLHY_SERVER_MAJORVERSION_NUMBER)
#define CLHY_SERVER_MINORVERSION  KUDA_STRINGIFY(CLHY_SERVER_MINORVERSION_NUMBER)
#define CLHY_SERVER_PATCHLEVEL    KUDA_STRINGIFY(CLHY_SERVER_PATCHLEVEL_NUMBER) \
                                CLHY_SERVER_ADD_STRING

#define CLHY_SERVER_MINORREVISION CLHY_SERVER_MAJORVERSION "." CLHY_SERVER_MINORVERSION
#define CLHY_SERVER_BASEREVISION  CLHY_SERVER_MINORREVISION "." CLHY_SERVER_PATCHLEVEL
#define CLHY_SERVER_BASEVERSION   CLHY_SERVER_BASEPRODUCT "/" CLHY_SERVER_BASEREVISION
#define CLHY_SERVER_VERSION       CLHY_SERVER_BASEVERSION

/* macro for Win32 .rc files using numeric csv representation */
#define CLHY_SERVER_PATCHLEVEL_CSV CLHY_SERVER_MAJORVERSION_NUMBER, \
                                 CLHY_SERVER_MINORVERSION_NUMBER, \
                                 CLHY_SERVER_PATCHLEVEL_NUMBER

#endif
