/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  util_charset.h
 * @brief charset conversion
 *
 * @defgroup CLHYKUDEL_CORE_CHARSET Charset Conversion
 * @ingroup  CLHYKUDEL_CORE
 * 
 * These are the translation handles used to translate between the network
 * format of protocol headers and the local machine format.
 *
 * For an EBCDIC machine, these are valid handles which are set up at
 * initialization to translate between ISO-8859-1 and the code page of
 * the source code.\n
 * For an ASCII machine, they are undefined.
 * 
 * @see clhy_init_ebcdic()
 * @{
 */

#ifndef CLHYKUDEL_UTIL_CHARSET_H
#define CLHYKUDEL_UTIL_CHARSET_H

#ifdef __cplusplus
extern "C" {
#endif

#include "kuda.h"

#if KUDA_CHARSET_EBCDIC || defined(DOXYGEN)

#include "kuda_xlate.h"

/**
 * On EBCDIC machine this is a translation handle used to translate the
 * headers from the local machine format to ASCII for network transmission.
 * @note On ASCII system, this variable does <b>not</b> exist.
 * So, its use should be guarded by \#if KUDA_CHARSET_EBCDIC.
 */
extern kuda_xlate_t *clhy_hdrs_to_ascii;

/**
 * On EBCDIC machine this is a translation handle used to translate the
 * headers from ASCII to the local machine format after network transmission.
 * @note On ASCII system, this variable does <b>not</b> exist.
 * So, its use should be guarded by \#if KUDA_CHARSET_EBCDIC.
 */
extern kuda_xlate_t *clhy_hdrs_from_ascii;

#endif  /* KUDA_CHARSET_EBCDIC */

#ifdef __cplusplus
}
#endif

#endif  /* !CLHYKUDEL_UTIL_CHARSET_H */
/** @} */
