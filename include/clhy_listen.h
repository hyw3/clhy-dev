/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  clhy_listen.h
 * @brief cLHy Listeners Library
 *
 * @defgroup CLHYKUDEL_CORE_LISTEN cLHy Listeners Library
 * @ingroup  CLHYKUDEL_CORE
 * @{
 */

#ifndef CLHY_LISTEN_H
#define CLHY_LISTEN_H

#include "kuda_network_io.h"
#include "wwhy.h"
#include "http_config.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct clhy_slave_t clhy_slave_t;
typedef struct clhy_listen_rec clhy_listen_rec;
typedef kuda_status_t (*accept_function)(void **csd, clhy_listen_rec *lr, kuda_pool_t *ptrans);

/**
 * @brief cLHy's listeners record.
 *
 * These are used in the Multi-Processing cAPIs
 * to setup all of the sockets for the cLMP to listen to and accept on.
 */
struct clhy_listen_rec {
    /**
     * The next listener in the list
     */
    clhy_listen_rec *next;
    /**
     * The actual socket
     */
    kuda_socket_t *sd;
    /**
     * The sockaddr the socket should bind to
     */
    kuda_sockaddr_t *bind_addr;
    /**
     * The accept function for this socket
     */
    accept_function accept_func;
    /**
     * Is this socket currently active
     */
    int active;
    /**
     * The default protocol for this listening socket.
     */
    const char* protocol;

    clhy_slave_t *slave;
};

/**
 * The global list of clhy_listen_rec structures
 */
CLHY_DECLARE_DATA extern clhy_listen_rec *clhy_listeners;
CLHY_DECLARE_DATA extern int clhy_num_listen_buckets;
CLHY_DECLARE_DATA extern int clhy_have_so_reuseport;

/**
 * Setup all of the defaults for the listener list
 */
CLHY_DECLARE(void) clhy_listen_pre_config(void);

/**
 * Loop through the global clhy_listen_rec list and create all of the required
 * sockets.  This executes the listen and bind on the sockets.
 * @param s The global server_rec
 * @return The number of open sockets.
 */
CLHY_DECLARE(int) clhy_setup_listeners(server_rec *s);

/**
 * This function duplicates clhy_listeners into multiple buckets when configured
 * to (see ListenCoresBucketsRatio) and the platform supports it (eg. number of
 * online CPU cores and SO_REUSEPORT available).
 * @param p The config pool
 * @param s The global server_rec
 * @param buckets The array of listeners buckets.
 * @param num_buckets The total number of listeners buckets (array size).
 * @remark If the given *num_buckets is 0 (input), it will be computed
 *         according to the platform capacities, otherwise (positive) it
 *         will be preserved. The number of listeners duplicated will
 *         always match *num_buckets, be it computed or given.
 */
CLHY_DECLARE(kuda_status_t) clhy_duplicate_listeners(kuda_pool_t *p, server_rec *s,
                                                clhy_listen_rec ***buckets,
                                                int *num_buckets);

/**
 * Loop through the global clhy_listen_rec list and close each of the sockets.
 */
CLHY_DECLARE_NONSTD(void) clhy_close_listeners(void);

/**
 * Loop through the given clhy_listen_rec list and close each of the sockets.
 * @param listeners The listener to close.
 */
CLHY_DECLARE_NONSTD(void) clhy_close_listeners_ex(clhy_listen_rec *listeners);

/**
 * FIXMEDOC
 */
CLHY_DECLARE_NONSTD(int) clhy_close_selected_listeners(clhy_slave_t *);

/* Although these functions are exported from libmain, they are not really
 * public functions.  These functions are actually called while parsing the
 * config file, when one of the LISTEN_COMMANDS directives is read.  These
 * should not ever be called by external cAPIs.  ALL cLMPs should include
 * LISTEN_COMMANDS in their command_rec table so that these functions are
 * called.
 */
CLHY_DECLARE_NONSTD(const char *) clhy_set_listenbacklog(cmd_parms *cmd, void *dummy, const char *arg);
CLHY_DECLARE_NONSTD(const char *) clhy_set_listencbratio(cmd_parms *cmd, void *dummy, const char *arg);
CLHY_DECLARE_NONSTD(const char *) clhy_set_listener(cmd_parms *cmd, void *dummy,
                                                int argc, char *const argv[]);
CLHY_DECLARE_NONSTD(const char *) clhy_set_send_buffer_size(cmd_parms *cmd, void *dummy,
                                                        const char *arg);
CLHY_DECLARE_NONSTD(const char *) clhy_set_receive_buffer_size(cmd_parms *cmd,
                                                           void *dummy,
                                                           const char *arg);

#define LISTEN_COMMANDS \
CLHY_INIT_TAKE1("ListenBacklog", clhy_set_listenbacklog, NULL, RSRC_CONF, \
  "Maximum length of the queue of pending connections, as used by listen(2)"), \
CLHY_INIT_TAKE1("ListenCoresBucketsRatio", clhy_set_listencbratio, NULL, RSRC_CONF, \
  "Ratio between the number of CPU cores (online) and the number of listeners buckets"), \
CLHY_INIT_TAKE_ARGV("Listen", clhy_set_listener, NULL, RSRC_CONF, \
  "A port number or a numeric IP address and a port number, and an optional protocol"), \
CLHY_INIT_TAKE1("SendBufferSize", clhy_set_send_buffer_size, NULL, RSRC_CONF, \
  "Send buffer size in bytes"), \
CLHY_INIT_TAKE1("ReceiveBufferSize", clhy_set_receive_buffer_size, NULL, \
              RSRC_CONF, "Receive buffer size in bytes")

#ifdef __cplusplus
}
#endif

#endif
/** @} */
