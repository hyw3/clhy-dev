/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  scoreboard.h
 * @brief cLHy scoreboard library
 */

#ifndef CLHYKUDEL_SCOREBOARD_H
#define CLHYKUDEL_SCOREBOARD_H

#ifdef __cplusplus
extern "C" {
#endif

#if KUDA_HAVE_SYS_TIME_H
#include <sys/time.h>
#include <sys/times.h>
#endif

#include "clhy_config.h"
#include "http_config.h"
#include "kuda_thread_proc.h"
#include "kuda_portable.h"
#include "kuda_shm.h"
#include "kuda_optional.h"

/* Scoreboard file, if there is one */
#ifndef DEFAULT_SCOREBOARD
#define DEFAULT_SCOREBOARD "logs/clhy_runtime_status"
#endif

/* Scoreboard info on a process is, for now, kept very brief ---
 * just status value and pid (the latter so that the caretaker process
 * can properly update the scoreboard when a process dies).  We may want
 * to eventually add a separate set of long_score structures which would
 * give, for each process, the number of requests serviced, and info on
 * the current, or most recent, request.
 *
 * Status values:
 */

#define SERVER_DEAD 0
#define SERVER_STARTING 1       /* Server Starting up */
#define SERVER_READY 2          /* Waiting for connection (or accept() lock) */
#define SERVER_BUSY_READ 3      /* Reading a client request */
#define SERVER_BUSY_WRITE 4     /* Processing a client request */
#define SERVER_BUSY_KEEPALIVE 5 /* Waiting for more requests via keepalive */
#define SERVER_BUSY_LOG 6       /* Logging the request */
#define SERVER_BUSY_DNS 7       /* Looking up a hostname */
#define SERVER_CLOSING 8        /* Closing the connection */
#define SERVER_GRACEFUL 9       /* server is gracefully finishing request */
#define SERVER_IDLE_KILL 10     /* Server is cleaning up idle children. */
#define SERVER_NUM_STATUS 11    /* number of status settings */

/* Type used for generation indicies.  Startup and every restart cause a
 * new generation of children to be spawned.  Children within the same
 * generation share the same configuration information -- pointers to stuff
 * created at config time in the parent are valid across children.  However,
 * this can't work effectively with non-forked architectures.  So while the
 * arrays in the scoreboard never change between the parent and forked
 * children, so they do not require shm storage, the contents of the shm
 * may contain no pointers.
 */
typedef int clhy_generation_t;

/* Is the scoreboard shared between processes or not?
 * Set by the cLMP when the scoreboard is created.
 */
typedef enum {
    SB_NOT_SHARED = 1,
    SB_SHARED = 2
} clhy_scoreboard_e;

/* stuff which is worker specific */
typedef struct worker_score worker_score;
struct worker_score {
#if KUDA_HAS_THREADS
    kuda_platform_thread_t tid;
#endif
    int thread_num;
    /* With some cLMPs (e.g., worker), a worker_score can represent
     * a thread in a terminating process which is no longer
     * represented by the corresponding process_score.  These cLMPs
     * should set pid and generation fields in the worker_score.
     */
    pid_t pid;
    clhy_generation_t generation;
    unsigned char status;
    unsigned short conn_count;
    kuda_off_t     conn_bytes;
    unsigned long access_count;
    kuda_off_t     bytes_served;
    unsigned long my_access_count;
    kuda_off_t     my_bytes_served;
    kuda_time_t start_time;
    kuda_time_t stop_time;
    kuda_time_t last_used;
#ifdef HAVE_TIMES
    struct tms times;
#endif
    char client[32];            /* DEPRECATED: Keep 'em small... */
    char request[64];           /* We just want an idea... */
    char vhost[32];             /* What virtual host is being accessed? */
    char protocol[16];          /* What protocol is used on the connection? */
    kuda_time_t duration;
    char client64[64];
};

typedef struct {
    int             server_limit;
    int             thread_limit;
    clhy_generation_t running_generation; /* the generation of children which
                                         * should still be serving requests.
                                         */
    kuda_time_t restart_time;
#ifdef HAVE_TIMES
    struct tms times;
#endif
} global_score;

/* stuff which the parent generally writes and the children rarely read */
typedef struct process_score process_score;
struct process_score {
    pid_t pid;
    clhy_generation_t generation; /* generation of this child */
    char quiescing;         /* the process whose pid is stored above is
                             * going down gracefully
                             */
    char not_accepting;     /* the process is busy and is not accepting more
                             * connections (for async cLMPs)
                             */
    kuda_uint32_t connections;       /* total connections (for async cLMPs) */
    kuda_uint32_t write_completion;  /* async connections doing write completion */
    kuda_uint32_t lingering_close;   /* async connections in lingering close */
    kuda_uint32_t keep_alive;        /* async connections in keep alive */
    kuda_uint32_t suspended;         /* connections suspended by some cAPI */
    int bucket;  /* Listener bucket used by this child; this field is DEPRECATED
                  * and no longer updated by the cLMPs (i.e. always zero).
                  */
};

/* Scoreboard is now in 'local' memory, since it isn't updated once created,
 * even in forked architectures.  Child created-processes (non-fork) will
 * set up these indicies into the (possibly relocated) shmem records.
 */
typedef struct {
    global_score *global;
    process_score *parent;
    worker_score **servers;
} scoreboard;

typedef struct clhy_sb_handle_t clhy_sb_handle_t;

/*
 * Creation and deletion (internal)
 */
int clhy_create_scoreboard(kuda_pool_t *p, clhy_scoreboard_e t);
kuda_status_t clhy_cleanup_scoreboard(void *d);

/*
 * APIs for cLMPs and other cAPIs
 */
CLHY_DECLARE(int) clhy_exists_scoreboard_image(void);
CLHY_DECLARE(void) clhy_increment_counts(clhy_sb_handle_t *sbh, request_rec *r);
CLHY_DECLARE(void) clhy_set_conn_count(clhy_sb_handle_t *sb, request_rec *r, unsigned short conn_count);

CLHY_DECLARE(kuda_status_t) clhy_reopen_scoreboard(kuda_pool_t *p, kuda_shm_t **shm, int detached);
CLHY_DECLARE(void) clhy_init_scoreboard(void *shared_score);
CLHY_DECLARE(int) clhy_calc_scoreboard_size(void);

CLHY_DECLARE(void) clhy_create_sb_handle(clhy_sb_handle_t **new_sbh, kuda_pool_t *p,
                                     int child_num, int thread_num);
CLHY_DECLARE(void) clhy_update_sb_handle(clhy_sb_handle_t *sbh,
                                     int child_num, int thread_num);

CLHY_DECLARE(int) clhy_find_child_by_pid(kuda_proc_t *pid);
CLHY_DECLARE(int) clhy_update_child_status(clhy_sb_handle_t *sbh, int status, request_rec *r);
CLHY_DECLARE(int) clhy_update_child_status_from_indexes(int child_num, int thread_num,
                                                    int status, request_rec *r);
CLHY_DECLARE(int) clhy_update_child_status_from_conn(clhy_sb_handle_t *sbh, int status, conn_rec *c);
CLHY_DECLARE(int) clhy_update_child_status_from_server(clhy_sb_handle_t *sbh, int status, 
                                                   conn_rec *c, server_rec *s);
CLHY_DECLARE(int) clhy_update_child_status_descr(clhy_sb_handle_t *sbh, int status, const char *descr);

CLHY_DECLARE(void) clhy_time_process_request(clhy_sb_handle_t *sbh, int status);

CLHY_DECLARE(int) clhy_update_global_status(void);

CLHY_DECLARE(worker_score *) clhy_get_scoreboard_worker(clhy_sb_handle_t *sbh);

/** Return a pointer to the worker_score for a given child, thread pair.
 * @param child_num The child number.
 * @param thread_num The thread number.
 * @return A pointer to the worker_score structure.
 * @deprecated This function is deprecated, use clhy_copy_scoreboard_worker instead. */
CLHY_DECLARE(worker_score *) clhy_get_scoreboard_worker_from_indexes(int child_num,
                                                                int thread_num);

/** Copy the contents of a worker scoreboard entry.  The contents of
 * the worker_score structure are copied verbatim into the dest
 * structure.
 * @param dest Output parameter.
 * @param child_num The child number.
 * @param thread_num The thread number.
 */
CLHY_DECLARE(void) clhy_copy_scoreboard_worker(worker_score *dest,
                                           int child_num, int thread_num);

CLHY_DECLARE(process_score *) clhy_get_scoreboard_process(int x);
CLHY_DECLARE(global_score *) clhy_get_scoreboard_global(void);

CLHY_DECLARE_DATA extern scoreboard *clhy_scoreboard_image;
CLHY_DECLARE_DATA extern const char *clhy_scoreboard_fname;
CLHY_DECLARE_DATA extern int clhy_extended_status;
CLHY_DECLARE_DATA extern int clhy_capi_status_reqtail;

/*
 * Command handlers [internal]
 */
const char *clhy_set_scoreboard(cmd_parms *cmd, void *dummy, const char *arg);
const char *clhy_set_extended_status(cmd_parms *cmd, void *dummy, int arg);
const char *clhy_set_reqtail(cmd_parms *cmd, void *dummy, int arg);

/* Hooks */
/**
  * Hook for post scoreboard creation, pre cLMP.
  * @param p       cLHy pool to allocate from.
  * @param sb_type
  * @ingroup hooks
  * @return OK or DECLINE on success; anything else is a error
  */
CLHY_DECLARE_HOOK(int, pre_clmp, (kuda_pool_t *p, clhy_scoreboard_e sb_type))

/* for time_process_request() in http_main.c */
#define START_PREQUEST 1
#define STOP_PREQUEST  2

#ifdef __cplusplus
}
#endif

#endif  /* !CLHYKUDEL_SCOREBOARD_H */
