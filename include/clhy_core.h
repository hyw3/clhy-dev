/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  clhy_core.h
 * @brief cLHy Multi-Processing cAPI library
 *
 * @defgroup CLHYKUDEL_CORE_CLMP Multi-Processing cAPI library
 * @ingroup  CLHYKUDEL_CORE
 * @{
 */

#ifndef CLHY_CLMP_H
#define CLHY_CLMP_H

#include "kuda_thread_proc.h"
#include "wwhy.h"
#include "scoreboard.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Pass control to the cLMP for steady-state processing.  It is responsible
 * for controlling the parent and child processes.  It will run until a
 * restart/shutdown is indicated.
 * @param pconf the configuration pool, reset before the config file is read
 * @param plog the log pool, reset after the config file is read
 * @param server_conf the global server config.
 * @return DONE for shutdown OK otherwise.
 */
CLHY_DECLARE_HOOK(int, clcore, (kuda_pool_t *pconf, kuda_pool_t *plog, server_rec *server_conf))

/**
 * Spawn a process with privileges that another cAPI has requested
 * @param r The request_rec of the current request
 * @param newproc The resulting process handle.
 * @param progname The program to run
 * @param args the arguments to pass to the new program.  The first
 *                   one should be the program name.
 * @param env The new environment kuda_table_t for the new process.  This
 *            should be a list of NULL-terminated strings.
 * @param attr the procattr we should use to determine how to create the new
 *         process
 * @param p The pool to use.
 */
CLHY_DECLARE(kuda_status_t) clhy_platform_create_privileged_process(
    const request_rec *r,
    kuda_proc_t *newproc,
    const char *progname,
    const char * const *args,
    const char * const *env,
    kuda_procattr_t *attr,
    kuda_pool_t *p);

/* Subtypes/Values for CLHY_CLMPQ_IS_THREADED and CLHY_CLMPQ_IS_FORKED        */
#define CLHY_CLMPQ_NOT_SUPPORTED      0  /* This value specifies that an */
                                      /* cLMP is not capable of        */
                                      /* threading or forking.        */
#define CLHY_CLMPQ_STATIC             1  /* This value specifies that    */
                                      /* a cLMP is using a static     */
                                      /* number of threads or daemons */
#define CLHY_CLMPQ_DYNAMIC            2  /* This value specifies that    */
                                      /* a cLMP is using a dynamic    */
                                      /* number of threads or daemons */

/* Values returned for CLHY_CLMPQ_CLMP_STATE */
#define CLHY_CLMPQ_STARTING              0
#define CLHY_CLMPQ_RUNNING               1
#define CLHY_CLMPQ_STOPPING              2

#define CLHY_CLMPQ_MAX_DAEMON_USED       1  /* Max # of daemons used so far */
#define CLHY_CLMPQ_IS_THREADED           2  /* cLMP can do threading        */
#define CLHY_CLMPQ_IS_FORKED             3  /* cLMP can do forking          */
#define CLHY_CLMPQ_HARD_LIMIT_DAEMONS    4  /* The compiled max # daemons   */
#define CLHY_CLMPQ_HARD_LIMIT_THREADS    5  /* The compiled max # threads   */
#define CLHY_CLMPQ_MAX_THREADS           6  /* # of threads/child by config */
#define CLHY_CLMPQ_MIN_SPARE_DAEMONS     7  /* Min # of spare daemons       */
#define CLHY_CLMPQ_MIN_SPARE_THREADS     8  /* Min # of spare threads       */
#define CLHY_CLMPQ_MAX_SPARE_DAEMONS     9  /* Max # of spare daemons       */
#define CLHY_CLMPQ_MAX_SPARE_THREADS    10  /* Max # of spare threads       */
#define CLHY_CLMPQ_MAX_REQUESTS_DAEMON  11  /* Max # of requests per daemon */
#define CLHY_CLMPQ_MAX_DAEMONS          12  /* Max # of daemons by config   */
#define CLHY_CLMPQ_CLMP_STATE           13  /* starting, running, stopping  */
#define CLHY_CLMPQ_IS_ASYNC             14  /* cLMP can process async connections  */
#define CLHY_CLMPQ_GENERATION           15  /* cLMP generation */
#define CLHY_CLMPQ_HAS_SERF             16  /* cLMP can drive serf internally  */

/**
 * Query a property of the current cLMP.
 * @param query_code One of APM_CLMPQ_*
 * @param result A location to place the result of the query
 * @return KUDA_EGENERAL if an clmp-query hook has not been registered;
 * KUDA_SUCCESS or KUDA_ENOTIMPL otherwise
 * @remark The cLMP doesn't register the implementing hook until the
 * register_hooks hook is called, so cAPIs cannot use clhy_clmp_query()
 * until after that point.
 * @fn int clhy_clmp_query(int query_code, int *result)
 */
CLHY_DECLARE(kuda_status_t) clhy_clmp_query(int query_code, int *result);


typedef void (clhy_clmp_callback_fn_t)(void *baton);

/* only added support in the Event cLMP....  check for KUDA_ENOTIMPL */
CLHY_DECLARE(kuda_status_t) clhy_clmp_register_timed_callback(kuda_time_t t,
                                                       clhy_clmp_callback_fn_t *cbfn,
                                                       void *baton);

typedef enum clmp_child_status {
    CLMP_CHILD_STARTED,
    CLMP_CHILD_EXITED,
    CLMP_CHILD_LOST_SLOT
} clmp_child_status;

/**
 * Allow a cAPI to remain aware of cLMP child process state changes,
 * along with the generation and scoreboard slot of the process changing
 * state.
 *
 * With some cLMPs (event and worker), an active cLMP child process may lose
 * its scoreboard slot if the child process is exiting and the scoreboard
 * slot is needed by other processes.  When this occurs, the hook will be
 * called with the CLMP_CHILD_LOST_SLOT state.
 *
 * @param s The main server_rec.
 * @param pid The id of the cLMP child process.
 * @param gen The server generation of that child process.
 * @param slot The scoreboard slot number, or -1.  It will be -1 when an
 * cLMP child process exits, and that child had previously lost its
 * scoreboard slot.
 * @param state One of the clmp_child_status values.  cAPIs should ignore
 * unrecognized values.
 */
CLHY_DECLARE_HOOK(void,child_status,(server_rec *s, pid_t pid, clhy_generation_t gen,
                                   int slot, clmp_child_status state))

/**
 * Allow a cAPI to be notified when the last child process of a generation
 * exits.
 *
 * @param s The main server_rec.
 * @param gen The server generation which is now completely finished.
 */
CLHY_DECLARE_HOOK(void,end_generation,(server_rec *s, clhy_generation_t gen))

/* Defining GPROF when compiling uses the moncontrol() function to
 * disable gprof profiling in the parent, and enable it only for
 * request processing in children (or in one_process mode).  It's
 * absolutely required to get useful gprof results under linux
 * because the profile itimers and such are disabled across a
 * fork().  It's probably useful elsewhere as well.
 */
#ifdef GPROF
extern void moncontrol(int);
#define CLHY_MONCONTROL(x) moncontrol(x)
#else
#define CLHY_MONCONTROL(x)
#endif

#ifdef CLHY_ENABLE_EXCEPTION_HOOK
typedef struct clhy_exception_info_t {
    int sig;
    pid_t pid;
} clhy_exception_info_t;

CLHY_DECLARE_HOOK(int,fatal_exception,(clhy_exception_info_t *ei))
#endif /*CLHY_ENABLE_EXCEPTION_HOOK*/

#ifdef __cplusplus
}
#endif

#endif
/** @} */
