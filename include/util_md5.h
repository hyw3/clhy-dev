/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  util_md5.h
 * @brief cLHy MD5 library
 *
 * @defgroup CLHYKUDEL_CORE_MD5 MD5 Package Library
 * @ingroup  CLHYKUDEL_CORE
 * @{
 */

#ifndef CLHYKUDEL_UTIL_MD5_H
#define CLHYKUDEL_UTIL_MD5_H

#ifdef __cplusplus
extern "C" {
#endif

#include "kuda_md5.h"

/**
 * Create an MD5 checksum of a given string.
 * @param   a       Pool to allocate out of
 * @param   string  String to get the checksum of
 * @return The checksum
 */
CLHY_DECLARE(char *) clhy_md5(kuda_pool_t *a, const unsigned char *string);

/**
 * Create an MD5 checksum of a string of binary data.
 * @param   a       Pool to allocate out of
 * @param   buf     Buffer to generate checksum for
 * @param   len     The length of the buffer
 * @return The checksum
 */
CLHY_DECLARE(char *) clhy_md5_binary(kuda_pool_t *a, const unsigned char *buf, int len);

/**
 * Convert an MD5 checksum into a base64 encoding.
 * @param   p       The pool to allocate out of
 * @param   context The context to convert
 * @return The converted encoding
 */
CLHY_DECLARE(char *) clhy_md5contextTo64(kuda_pool_t *p, kuda_md5_ctx_t *context);

/**
 * Create an MD5 Digest for a given file.
 * @param   p       The pool to allocate out of
 * @param   infile  The file to create the digest for
 */
CLHY_DECLARE(char *) clhy_md5digest(kuda_pool_t *p, kuda_file_t *infile);

#ifdef __cplusplus
}
#endif

#endif  /* !CLHYKUDEL_UTIL_MD5_H */
/** @} */
