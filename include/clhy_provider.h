/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  clhy_provider.h
 * @brief cLHy Provider API
 *
 * @defgroup CLHYKUDEL_CORE_PROVIDER Provider API
 * @ingroup  CLHYKUDEL_CORE
 * @{
 */

#ifndef CLHY_PROVIDER_H
#define CLHY_PROVIDER_H

#include "clhy_config.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    const char *provider_name;
} clhy_list_provider_names_t;

typedef struct {
    const char *provider_group;
    const char *provider_version;
} clhy_list_provider_groups_t;

/**
 * This function is used to register a provider with the global
 * provider pool.
 * @param pool The pool to create any storage from
 * @param provider_group The group to store the provider in
 * @param provider_name The name for this provider
 * @param provider_version The version for this provider
 * @param provider Opaque structure for this provider
 * @return KUDA_SUCCESS if all went well
 */
CLHY_DECLARE(kuda_status_t) clhy_register_provider(kuda_pool_t *pool,
                                              const char *provider_group,
                                              const char *provider_name,
                                              const char *provider_version,
                                              const void *provider);

/**
 * This function is used to retrieve a provider from the global
 * provider pool.
 * @param provider_group The group to look for this provider in
 * @param provider_name The name for the provider
 * @param provider_version The version for the provider
 * @return provider pointer to provider if found, NULL otherwise
 */
CLHY_DECLARE(void *) clhy_lookup_provider(const char *provider_group,
                                      const char *provider_name,
                                      const char *provider_version);

/**
 * This function is used to retrieve a list (array) of provider
 * names from the specified group with the specified version.
 * @param pool The pool to create any storage from
 * @param provider_group The group to look for this provider in
 * @param provider_version The version for the provider
 * @return pointer to array of clhy_list_provider_names_t of provider names (could be empty)
 */

CLHY_DECLARE(kuda_array_header_t *) clhy_list_provider_names(kuda_pool_t *pool,
                                              const char *provider_group,
                                              const char *provider_version);

/**
 * This function is used to retrieve a list (array) of provider groups and versions
 * @param pool The pool to create any storage from
 * @return pointer to array of clhy_list_provider_groups_t of provider groups
 *         and versions (could be empty)
 */

CLHY_DECLARE(kuda_array_header_t *) clhy_list_provider_groups(kuda_pool_t *pool);


#ifdef __cplusplus
}
#endif

#endif
/** @} */
