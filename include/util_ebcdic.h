/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  util_ebcdic.h
 * @brief Utilities for EBCDIC conversion
 *
 * @defgroup CLHYKUDEL_CORE_EBCDIC Utilities for EBCDIC conversion
 * @ingroup  CLHYKUDEL_CORE
 * @{
 */

#ifndef CLHYKUDEL_UTIL_EBCDIC_H
#define CLHYKUDEL_UTIL_EBCDIC_H


#ifdef __cplusplus
extern "C" {
#endif

#include "kuda_xlate.h"
#include "wwhy.h"
#include "util_charset.h"

#if KUDA_CHARSET_EBCDIC || defined(DOXYGEN)

/**
 * Setup all of the global translation handlers.
 * @param   pool    The pool to allocate out of.
 * @note On non-EBCDIC system, this function does <b>not</b> exist.
 * So, its use should be guarded by \#if KUDA_CHARSET_EBCDIC.
 */
kuda_status_t clhy_init_ebcdic(kuda_pool_t *pool);

/**
 * Convert protocol data from the implementation character
 * set to ASCII.
 * @param   buffer  Buffer to translate.
 * @param   len     Number of bytes to translate.
 * @note On non-EBCDIC system, this function is replaced by an 
 * empty macro.
 */
void clhy_xlate_proto_to_ascii(char *buffer, kuda_size_t len);

/**
 * Convert protocol data to the implementation character
 * set from ASCII.
 * @param   buffer  Buffer to translate.
 * @param   len     Number of bytes to translate.
 * @note On non-EBCDIC system, this function is replaced by an 
 * empty macro.
 */
void clhy_xlate_proto_from_ascii(char *buffer, kuda_size_t len);

/**
 * Convert protocol data from the implementation character
 * set to ASCII, then send it.
 * @param   r       The current request.
 * @param   ...     The strings to write, followed by a NULL pointer.
 * @note On non-EBCDIC system, this function is replaced by a call to
 * #clhy_rvputs.
 */
int clhy_rvputs_proto_in_ascii(request_rec *r, ...);

#else   /* KUDA_CHARSET_EBCDIC */

#define clhy_xlate_proto_to_ascii(x,y)          /* NOOP */
#define clhy_xlate_proto_from_ascii(x,y)        /* NOOP */

#define clhy_rvputs_proto_in_ascii  clhy_rvputs

#endif  /* KUDA_CHARSET_EBCDIC */

#ifdef __cplusplus
}
#endif

#endif  /* !CLHYKUDEL_UTIL_EBCDIC_H */
/** @} */
