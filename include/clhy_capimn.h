/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 /**
 * @file  clhy_capimn.h
 * @brief cAPI Magic Number
 *
 * @defgroup CLHYKUDEL_CORE_CAPIMN cAPI Magic Number
 * @ingroup  CLHYKUDEL_CORE
 * @{
 */

#ifndef CLHYKUDEL_CLHY_CAPIMN_H
#define CLHYKUDEL_CLHY_CAPIMN_H

/*
 * CAPI_MAGIC_COOKIE is a hexadecimal (base 16), valid characters are 0 through
 * 9 and letters A through F (which may be upper or lower case); A has the value 10,
 * B is 11, up to F which is 15. Hex values are indicated by the prefix "0x".
 * The "UL" means to force the constant into an unsigned long constant.
 */

#define CAPI_MAGIC_COOKIE 0x634c48792d312e36UL /* cLHy-1.6 */

#ifndef CAPI_MAGIC_NUMBER_MAJOR
#define CAPI_MAGIC_NUMBER_MAJOR 20191101
#endif
#define CAPI_MAGIC_NUMBER_MINOR 7

/*
 * History
 * 
 * 20191029.3 (cLHy-1.6.0-dev4):
 *  - CAPI_MAGIC_COOKIE is set to "cLHy-1.6".
 *  - Update ./configure.in and ./include/clhy_config_auto.h.in files.
 *  - Adding CLHY_LOG_SYSLOG, CLHY_SUEXEC_CAPABILITIES, HAVE_ARC4RANDOM_BUF,
 *    HAVE_CURL, HAVE_CURL_CURL_H, HAVE_JANSSON, HAVE_VSYSLOG, 
 *    HAVE_NGHTTP2_SESSION_GET_STREAM_LOCAL_WINDOW_SIZE,
 *    and remove HAVE_SSLEAY_VERSION.
 * 20191101.6 (cLHy-1.6.0-dev6):
 *  - Introducing cLMPs (cLHy Multi Processings) as cLHy core APIs.
 *  - Current cLMP names including: event, netware, os2threaded, prefork, winnt
 *    and worker.
 *  - Define new CLMP16_CAPI_STUFF and STANDARD16_CAPI_STUFF
 *  - Upgrading Kuda Runtime Library to 1.7.2
 *  - Upgrading Kuda Delman Runtime Library to 1.6.3
 * 20191101.7 (cLHy-1.6.0-rc2):
 *  - Adding flag CLHY_GETLINE_NOSPC_EOL for functions group
 *    clhy_getline() to control buffer limit, setting saw_eol
 *    on LF to stop the outer loop appropriately once the buffer
 *    is filled (no LF seen), either be done at that time or continue
 *    to wait for LF if nospc_eol is set.
 *
 */

/**
 * Determine if the server's current CAPI_MAGIC_NUMBER is at least a
 * specified value.
 *
 * Useful for testing for features.
 * For example, suppose you wish to use the kuda_table_overlap
 *    function.  You can do this:
 *
 * \code
 * #if CLHY_CAPI_MAGIC_AT_LEAST(20191029,3)
 *     ... use kuda_table_overlap()
 * #else
 *     ... alternative code which doesn't use kuda_table_overlap()
 * #endif
 * \endcode
 *
 * @param major The major cAPI magic number
 * @param minor The minor cAPI magic number
 * @def CLHY_CAPI_MAGIC_AT_LEAST(int major, int minor)
 */

#define CLHY_CAPI_MAGIC_AT_LEAST(major,minor)         \
    ((major) < CAPI_MAGIC_NUMBER_MAJOR                \
     || ((major) == CAPI_MAGIC_NUMBER_MAJOR           \
         && (minor) <= CAPI_MAGIC_NUMBER_MINOR))

/** @deprecated present for backwards compatibility */
#define CAPI_MAGIC_NUMBER CAPI_MAGIC_NUMBER_MAJOR
#define CAPI_MAGIC_AT_LEAST old_broken_macro_we_hope_you_are_not_using

#endif /* !CLHYKUDEL_CLHY_CAPIMN_H */
/** @} */
