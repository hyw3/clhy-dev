/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  heartbeat.h
 * @brief commun structures for capi_heartmonitor.c and capi_lbmethod_heartbeat.c
 *
 * @defgroup HEARTBEAT heartbeat
 * @ingroup  CLHYKUDEL_CAPIS
 * @{
 */

#ifndef HEARTBEAT_H
#define HEARTBEAT_H

#include "kuda.h"
#include "kuda_time.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Worse Case: IPv4-Mapped IPv6 Address
 * 0000:0000:0000:0000:0000:FFFF:255.255.255.255
 */
#define MAXIPSIZE  46
typedef struct hm_slot_server_t
{
    char ip[MAXIPSIZE];
    int busy;
    int ready;
    kuda_time_t seen;
    int id;
} hm_slot_server_t;

/* default name of heartbeat data file, created in the configured
 * runtime directory when capi_slotmem_shm is not available
 */
#define DEFAULT_HEARTBEAT_STORAGE "hb.dat"

#ifdef __cplusplus
}
#endif

#endif /* HEARTBEAT_H */
/** @} */
