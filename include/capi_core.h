/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * @file  capi_core.h
 * @brief capi_core private header file
 *
 * @defgroup CAPI_CORE capi_core
 * @ingroup  CLHYKUDEL_CAPIS
 * @{
 */

#ifndef CAPI_CORE_H
#define CAPI_CORE_H

#include "kuda.h"
#include "kuda_buckets.h"

#include "wwhy.h"
#include "util_filter.h"


#ifdef __cplusplus
extern "C" {
#endif

/* Handles for core filters */
CLHY_DECLARE_DATA extern clhy_filter_rec_t *clhy_http_input_filter_handle;
CLHY_DECLARE_DATA extern clhy_filter_rec_t *clhy_http_header_filter_handle;
CLHY_DECLARE_DATA extern clhy_filter_rec_t *clhy_chunk_filter_handle;
CLHY_DECLARE_DATA extern clhy_filter_rec_t *clhy_http_outerror_filter_handle;
CLHY_DECLARE_DATA extern clhy_filter_rec_t *clhy_byterange_filter_handle;

/*
 * These (input) filters are internal to the capi_core operation.
 */
kuda_status_t clhy_http_filter(clhy_filter_t *f, kuda_bucket_brigade *b,
                            clhy_input_mode_t mode, kuda_read_type_e block,
                            kuda_off_t readbytes);

/* HTTP/1.1 chunked transfer encoding filter. */
kuda_status_t clhy_http_chunk_filter(clhy_filter_t *f, kuda_bucket_brigade *b);

/* Filter to handle any error buckets on output */
kuda_status_t clhy_http_outerror_filter(clhy_filter_t *f,
                                     kuda_bucket_brigade *b);

char *clhy_response_code_string(request_rec *r, int error_index);

/**
 * Send the minimal part of an HTTP response header.
 * @param r The current request
 * @param bb The brigade to add the header to.
 * @warning cAPIs should be very careful about using this, and should
 *          the default behavior.  Much of the HTTP/1.1 implementation
 *          correctness depends on the full headers.
 * @fn void clhy_basic_http_header(request_rec *r, kuda_bucket_brigade *bb)
 */
CLHY_DECLARE(void) clhy_basic_http_header(request_rec *r, kuda_bucket_brigade *bb);

/**
 * Send an appropriate response to an http TRACE request.
 * @param r The current request
 * @note returns DONE or the HTTP status error if it handles the TRACE,
 * or DECLINED if the request was not for TRACE.
 * request method was not TRACE.
 */
CLHY_DECLARE_NONSTD(int) clhy_send_http_trace(request_rec *r);

/**
 * Send an appropriate response to an http OPTIONS request.
 * @param r The current request
 */
CLHY_DECLARE(int) clhy_send_http_options(request_rec *r);

/* Used for multipart/byteranges boundary string */
CLHY_DECLARE_DATA extern const char *clhy_multipart_boundary;

/* Init RNG at startup */
CLHY_CORE_DECLARE(void) clhy_init_rng(kuda_pool_t *p);
/* Update RNG state in parent after fork */
CLHY_CORE_DECLARE(void) clhy_random_parent_after_fork(void);

#ifdef __cplusplus
}
#endif

#endif  /* !CAPI_CORE_H */
/** @} */
