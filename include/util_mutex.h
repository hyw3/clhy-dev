/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  util_mutex.h
 * @brief cLHy Mutex support library
 *
 * @defgroup CLHYKUDEL_CORE_MUTEX Mutex Library
 * @ingroup  CLHYKUDEL_CORE
 * @{
 */

#ifndef UTIL_MUTEX_H
#define UTIL_MUTEX_H

#include "wwhy.h"
#include "http_config.h"
#include "kuda_global_mutex.h"

#if KUDA_HAS_FLOCK_SERIALIZE
# define CLHY_LIST_FLOCK_SERIALIZE ", 'flock:/path/to/file'"
#else
# define CLHY_LIST_FLOCK_SERIALIZE
#endif
#if KUDA_HAS_FCNTL_SERIALIZE
# define CLHY_LIST_FCNTL_SERIALIZE ", 'fcntl:/path/to/file'"
#else
# define CLHY_LIST_FCNTL_SERIALIZE
#endif
#if KUDA_HAS_SYSVSEM_SERIALIZE
# define CLHY_LIST_SYSVSEM_SERIALIZE ", 'sysvsem'"
#else
# define CLHY_LIST_SYSVSEM_SERIALIZE
#endif
#if KUDA_HAS_POSIXSEM_SERIALIZE
# define CLHY_LIST_POSIXSEM_SERIALIZE ", 'posixsem'"
#else
# define CLHY_LIST_POSIXSEM_SERIALIZE
#endif
#if KUDA_HAS_PROC_PTHREAD_SERIALIZE
# define CLHY_LIST_PTHREAD_SERIALIZE ", 'pthread'"
#else
# define CLHY_LIST_PTHREAD_SERIALIZE
#endif
#if KUDA_HAS_FLOCK_SERIALIZE || KUDA_HAS_FCNTL_SERIALIZE
# define CLHY_LIST_FILE_SERIALIZE ", 'file:/path/to/file'"
#else
# define CLHY_LIST_FILE_SERIALIZE
#endif
#if KUDA_HAS_SYSVSEM_SERIALIZE || KUDA_HAS_POSIXSEM_SERIALIZE
# define CLHY_LIST_SEM_SERIALIZE ", 'sem'"
#else
# define CLHY_LIST_SEM_SERIALIZE
#endif

#define CLHY_ALL_AVAILABLE_MUTEXES_STRING                  \
    "Mutex mechanisms are: 'none', 'default'"            \
    CLHY_LIST_FLOCK_SERIALIZE   CLHY_LIST_FCNTL_SERIALIZE    \
    CLHY_LIST_FILE_SERIALIZE    CLHY_LIST_PTHREAD_SERIALIZE  \
    CLHY_LIST_SYSVSEM_SERIALIZE CLHY_LIST_POSIXSEM_SERIALIZE \
    CLHY_LIST_SEM_SERIALIZE

#define CLHY_AVAILABLE_MUTEXES_STRING                      \
    "Mutex mechanisms are: 'default'"                    \
    CLHY_LIST_FLOCK_SERIALIZE   CLHY_LIST_FCNTL_SERIALIZE    \
    CLHY_LIST_FILE_SERIALIZE    CLHY_LIST_PTHREAD_SERIALIZE  \
    CLHY_LIST_SYSVSEM_SERIALIZE CLHY_LIST_POSIXSEM_SERIALIZE \
    CLHY_LIST_SEM_SERIALIZE

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get Mutex config data and parse it
 * @param arg The mutex config string
 * @param pool The allocation pool
 * @param mutexmech The KUDA mutex locking mechanism
 * @param mutexfile The lockfile to use as required
 * @return KUDA status code
 * @fn kuda_status_t clhy_parse_mutex(const char *arg, kuda_pool_t *pool,
                                        kuda_lockmech_e *mutexmech,
                                        const char **mutexfile)
 */
CLHY_DECLARE(kuda_status_t) clhy_parse_mutex(const char *arg, kuda_pool_t *pool,
                                        kuda_lockmech_e *mutexmech,
                                        const char **mutexfile);

/* private function to process the Mutex directive */
CLHY_DECLARE_NONSTD(const char *) clhy_set_mutex(cmd_parms *cmd, void *dummy,
                                             const char *arg);

/* private function to initialize Mutex infrastructure */
CLHY_DECLARE_NONSTD(void) clhy_mutex_init(kuda_pool_t *p);

/**
 * option flags for clhy_mutex_register(), clhy_global_mutex_create(), and
 * clhy_proc_mutex_create()
 */
#define CLHY_MUTEX_ALLOW_NONE    1 /* allow "none" as mutex implementation;
                                  * respected only on clhy_mutex_register()
                                  */
#define CLHY_MUTEX_DEFAULT_NONE  2 /* default to "none" for this mutex;
                                  * respected only on clhy_mutex_register()
                                  */

/**
 * Register a cAPI's mutex type with core to allow configuration
 * with the Mutex directive.  This must be called in the pre_config
 * hook; otherwise, configuration directives referencing this mutex
 * type will be rejected.
 *
 * The default_dir and default_mech parameters allow a cAPI to set
 * defaults for the lock file directory and mechanism.  These could
 * be based on compile-time settings.  These aren't required except
 * in special circumstances.
 *
 * The order of precedence for the choice of mechanism and lock file
 * directory is:
 *
 *   1. Mutex directive specifically for this mutex
 *      e.g., Mutex core-default flock:/tmp/clcorelocks
 *   2. Mutex directive for global default
 *      e.g., Mutex default flock:/tmp/wwhylocks
 *   3. Defaults for this mutex provided on the clhy_mutex_register()
 *   4. Built-in defaults for all mutexes, which are
 *      KUDA_LOCK_DEFAULT and DEFAULT_REL_RUNTIMEDIR.
 *
 * @param pconf The pconf pool
 * @param type The type name of the mutex, used as the basename of the
 * file associated with the mutex, if any.  This must be unique among
 * all mutex types (mutex creation accommodates multi-instance mutex
 * types); capi_foo might have mutex  types "foo-pipe" and "foo-shm"
 * @param default_dir Default dir for any lock file required for this
 * lock, to override built-in defaults; should be NULL for most
 * cAPIs, to respect built-in defaults
 * @param default_mech Default mechanism for this lock, to override
 * built-in defaults; should be KUDA_LOCK_DEFAULT for most cAPIs, to
 * respect built-in defaults
 * or NULL if there are no defaults for this mutex.
 * @param options combination of CLHY_MUTEX_* constants, or 0 for defaults
 */
CLHY_DECLARE(kuda_status_t) clhy_mutex_register(kuda_pool_t *pconf,
                                           const char *type,
                                           const char *default_dir,
                                           kuda_lockmech_e default_mech,
                                           kuda_int32_t options);

/**
 * Create an KUDA global mutex that has been registered previously with
 * clhy_mutex_register().  Mutex files, permissions, and error logging will
 * be handled internally.
 * @param mutex The memory address where the newly created mutex will be
 * stored.  If this mutex is disabled, mutex will be set to NULL on
 * output.  (That is allowed only if the CLHY_MUTEX_ALLOW_NONE flag is
 * passed to clhy_mutex_register().)
 * @param name The generated filename of the created mutex, or NULL if
 * no file was created.  Pass NULL if this result is not needed.
 * @param type The type name of the mutex, matching the type name passed
 * to clhy_mutex_register().
 * @param instance_id A unique string to be used in the lock filename IFF
 * this mutex type is multi-instance, NULL otherwise.
 * @param server server_rec of main server
 * @param pool pool lifetime of the mutex
 * @param options combination of CLHY_MUTEX_* constants, or 0 for defaults
 * (currently none are defined for this function)
 */
CLHY_DECLARE(kuda_status_t) clhy_global_mutex_create(kuda_global_mutex_t **mutex,
                                                const char **name,
                                                const char *type,
                                                const char *instance_id,
                                                server_rec *server,
                                                kuda_pool_t *pool,
                                                kuda_int32_t options);

/**
 * Create an KUDA proc mutex that has been registered previously with
 * clhy_mutex_register().  Mutex files, permissions, and error logging will
 * be handled internally.
 * @param mutex The memory address where the newly created mutex will be
 * stored.  If this mutex is disabled, mutex will be set to NULL on
 * output.  (That is allowed only if the CLHY_MUTEX_ALLOW_NONE flag is
 * passed to clhy_mutex_register().)
 * @param name The generated filename of the created mutex, or NULL if
 * no file was created.  Pass NULL if this result is not needed.
 * @param type The type name of the mutex, matching the type name passed
 * to clhy_mutex_register().
 * @param instance_id A unique string to be used in the lock filename IFF
 * this mutex type is multi-instance, NULL otherwise.
 * @param server server_rec of main server
 * @param pool pool lifetime of the mutex
 * @param options combination of CLHY_MUTEX_* constants, or 0 for defaults
 * (currently none are defined for this function)
 */
CLHY_DECLARE(kuda_status_t) clhy_proc_mutex_create(kuda_proc_mutex_t **mutex,
                                              const char **name,
                                              const char *type,
                                              const char *instance_id,
                                              server_rec *server,
                                              kuda_pool_t *pool,
                                              kuda_int32_t options);

CLHY_CORE_DECLARE(void) clhy_dump_mutexes(kuda_pool_t *p, server_rec *s, kuda_file_t *out);

#ifdef __cplusplus
}
#endif

#endif /* UTIL_MUTEX_H */
/** @} */
