/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* The purpose of this file is to store the code that MOST cLMP's will need
 * this does not mean a function only goes into this file if every cLMP needs
 * it.  It means that if a function is needed by more than one cLMP, and
 * future maintenance would be served by making the code common, then the
 * function belongs here.
 *
 * This is going in src/main because it is not platform specific, it is
 * specific to multi-process servers, but NOT to Unix.  Which is why it
 * does not belong in src/platforms/unix
 */

/**
 * @file  core_common.h
 * @brief Multi-Processing cAPIs functions
 *
 * @defgroup CLHYKUDEL_CLMP Multi-Processing cAPIs
 * @ingroup  CLHYKUDEL
 * @{
 */

#ifndef CLHYKUDEL_CLMP_COMMON_H
#define CLHYKUDEL_CLMP_COMMON_H

#include "clhy_config.h"
#include "clhy_core.h"
#include "scoreboard.h"

#if KUDA_HAVE_NETINET_TCP_H
#include <netinet/tcp.h>    /* for TCP_NODELAY */
#endif

#include "kuda_proc_mutex.h"

#ifdef __cplusplus
extern "C" {
#endif

/* The maximum length of the queue of pending connections, as defined
 * by listen(2).  Under some systems, it should be increased if you
 * are experiencing a heavy TCP SYN flood attack.
 *
 * It defaults to 511 instead of 512 because some systems store it
 * as an 8-bit datatype; 512 truncated to 8-bits is 0, while 511 is
 * 255 when truncated.
 */
#ifndef DEFAULT_LISTENBACKLOG
#define DEFAULT_LISTENBACKLOG 511
#endif

/* Signal used to gracefully restart */
#define CLHY_SIG_GRACEFUL SIGUSR1

/* Signal used to gracefully restart (without SIG prefix) */
#define CLHY_SIG_GRACEFUL_SHORT USR1

/* Signal used to gracefully restart (as a quoted string) */
#define CLHY_SIG_GRACEFUL_STRING "SIGUSR1"

/* Signal used to gracefully stop */
#define CLHY_SIG_GRACEFUL_STOP SIGWINCH

/* Signal used to gracefully stop (without SIG prefix) */
#define CLHY_SIG_GRACEFUL_STOP_SHORT WINCH

/* Signal used to gracefully stop (as a quoted string) */
#define CLHY_SIG_GRACEFUL_STOP_STRING "SIGWINCH"

/**
 * Callback function used for clhy_reclaim_child_processes() and
 * clhy_relieve_child_processes().  The callback function will be
 * called for each terminated child process.
 */
typedef void clhy_reclaim_callback_fn_t(int childnum, pid_t pid,
                                      clhy_generation_t gen);

#if (!defined(WIN32) && !defined(NETWARE)) || defined(DOXYGEN)
/**
 * Make sure all child processes that have been spawned by the parent process
 * have died.  This includes process registered as "other_children".
 *
 * @param terminate Either 1 or 0.  If 1, send the child processes SIGTERM
 *        each time through the loop.  If 0, give the process time to die
 *        on its own before signalling it.
 * @param clmp_callback Callback invoked for each dead child process
 *
 * @note The cLMP child processes which are reclaimed are those listed
 * in the scoreboard as well as those currently registered via
 * clhy_register_extra_clmp_process().
 */
CLHY_DECLARE(void) clhy_reclaim_child_processes(int terminate,
                                            clhy_reclaim_callback_fn_t *clmp_callback);

/**
 * Catch any child processes that have been spawned by the parent process
 * which have exited. This includes processes registered as "other_children".
 *
 * @param clmp_callback Callback invoked for each dead child process

 * @note The cLMP child processes which are relieved are those listed
 * in the scoreboard as well as those currently registered via
 * clhy_register_extra_clmp_process().
 */
CLHY_DECLARE(void) clhy_relieve_child_processes(clhy_reclaim_callback_fn_t *clmp_callback);

/**
 * Tell clhy_reclaim_child_processes() and clhy_relieve_child_processes() about
 * a cLMP child process which has no entry in the scoreboard.
 * @param pid The process id of a cLMP child process which should be
 * reclaimed when clhy_reclaim_child_processes() is called.
 * @param gen The generation of this cLMP child process.
 *
 * @note If an extra cLMP child process terminates prior to calling
 * clhy_reclaim_child_processes(), remove it from the list of such processes
 * by calling clhy_unregister_extra_clmp_process().
 */
CLHY_DECLARE(void) clhy_register_extra_clmp_process(pid_t pid, clhy_generation_t gen);

/**
 * Unregister a cLMP child process which was previously registered by a
 * call to clhy_register_extra_clmp_process().
 * @param pid The process id of an cLMP child process which no longer needs to
 * be reclaimed.
 * @param old_gen Set to the server generation of the process, if found.
 * @return 1 if the process was found and removed, 0 otherwise
 */
CLHY_DECLARE(int) clhy_unregister_extra_clmp_process(pid_t pid, clhy_generation_t *old_gen);

/**
 * Safely signal an cLMP child process, if the process is in the
 * current process group.  Otherwise fail.
 * @param pid the process id of a child process to signal
 * @param sig the signal number to send
 * @return KUDA_SUCCESS if signal is sent, otherwise an error as per kill(3);
 * KUDA_EINVAL is returned if passed either an invalid (< 1) pid, or if
 * the pid is not in the current process group
 */
CLHY_DECLARE(kuda_status_t) clhy_clmp_safe_kill(pid_t pid, int sig);

/**
 * Log why a child died to the error log, if the child died without the
 * parent signalling it.
 * @param pid The child that has died
 * @param why The return code of the child process
 * @param status The status returned from clhy_wait_or_timeout
 * @return 0 on success, APEXIT_CHILDFATAL if cLMP should terminate
 */
CLHY_DECLARE(int) clhy_process_child_status(kuda_proc_t *pid, kuda_exit_why_e why, int status);

CLHY_DECLARE(kuda_status_t) clhy_fatal_signal_setup(server_rec *s, kuda_pool_t *in_pconf);
CLHY_DECLARE(kuda_status_t) clhy_fatal_signal_child_setup(server_rec *s);

#endif /* (!WIN32 && !NETWARE) || DOXYGEN */

/**
 * Pool cleanup for end-generation hook implementation
 * (core wwhy function)
 */
kuda_status_t clhy_clmp_end_gen_helper(void *unused);

/**
 * Run the monitor hook (once every ten calls), determine if any child
 * process has died and, if none died, sleep one second.
 * @param status The return code if a process has died
 * @param exitcode The returned exit status of the child, if a child process
 *                 dies, or the signal that caused the child to die.
 * @param ret The process id of the process that died
 * @param p The pool to allocate out of
 * @param s The server_rec to pass
 */
CLHY_DECLARE(void) clhy_wait_or_timeout(kuda_exit_why_e *status, int *exitcode,
                                    kuda_proc_t *ret, kuda_pool_t *p, 
                                    server_rec *s);

#if defined(TCP_NODELAY)
/**
 * Turn off the nagle algorithm for the specified socket.  The nagle algorithm
 * says that we should delay sending partial packets in the hopes of getting
 * more data.  There are bad interactions between persistent connections and
 * Nagle's algorithm that have severe performance penalties.
 * @param s The socket to disable nagle for.
 */
void clhy_sock_disable_nagle(kuda_socket_t *s);
#else
#define clhy_sock_disable_nagle(s)        /* NOOP */
#endif

#ifdef HAVE_GETPWNAM
/**
 * Convert a username to a numeric ID
 * @param name The name to convert
 * @return The user id corresponding to a name
 * @fn uid_t clhy_uname2id(const char *name)
 */
CLHY_DECLARE(uid_t) clhy_uname2id(const char *name);
#endif

#ifdef HAVE_GETGRNAM
/**
 * Convert a group name to a numeric ID
 * @param name The name to convert
 * @return The group id corresponding to a name
 * @fn gid_t clhy_gname2id(const char *name)
 */
CLHY_DECLARE(gid_t) clhy_gname2id(const char *name);
#endif

#ifndef HAVE_INITGROUPS
/**
 * The initgroups() function initializes the group access list by reading the
 * group database /etc/group and using all groups of which user is a member.
 * The additional group basegid is also added to the list.
 * @param name The user name - must be non-NULL
 * @param basegid The basegid to add
 * @return returns 0 on success
 * @fn int initgroups(const char *name, gid_t basegid)
 */
int initgroups(const char *name, gid_t basegid);
#endif

#if (!defined(WIN32) && !defined(NETWARE)) || defined(DOXYGEN)

typedef struct clhy_pod_t clhy_pod_t;

struct clhy_pod_t {
    kuda_file_t *pod_in;
    kuda_file_t *pod_out;
    kuda_pool_t *p;
};

/**
 * Open the pipe-of-death.  The pipe of death is used to tell all child
 * processes that it is time to die gracefully.
 * @param p The pool to use for allocating the pipe
 * @param pod the pipe-of-death that is created.
 */
CLHY_DECLARE(kuda_status_t) clhy_clmp_pod_open(kuda_pool_t *p, clhy_pod_t **pod);

/**
 * Check the pipe to determine if the process has been signalled to die.
 */
CLHY_DECLARE(kuda_status_t) clhy_clmp_pod_check(clhy_pod_t *pod);

/**
 * Close the pipe-of-death
 *
 * @param pod the pipe-of-death to close.
 */
CLHY_DECLARE(kuda_status_t) clhy_clmp_pod_close(clhy_pod_t *pod);

/**
 * Write data to the pipe-of-death, signalling that one child process
 * should die.
 * @param pod the pipe-of-death to write to.
 */
CLHY_DECLARE(kuda_status_t) clhy_clmp_pod_signal(clhy_pod_t *pod);

/**
 * Write data to the pipe-of-death, signalling that all child process
 * should die.
 * @param pod The pipe-of-death to write to.
 * @param num The number of child processes to kill
 */
CLHY_DECLARE(void) clhy_clmp_pod_killpg(clhy_pod_t *pod, int num);

#define CLHY_CLMP_PODX_RESTART_CHAR '$'
#define CLHY_CLMP_PODX_GRACEFUL_CHAR '!'

typedef enum { CLHY_CLMP_PODX_NORESTART, CLHY_CLMP_PODX_RESTART, CLHY_CLMP_PODX_GRACEFUL } clhy_podx_restart_t;

/**
 * Open the extended pipe-of-death.
 * @param p The pool to use for allocating the pipe
 * @param pod The pipe-of-death that is created.
 */
CLHY_DECLARE(kuda_status_t) clhy_clmp_podx_open(kuda_pool_t *p, clhy_pod_t **pod);

/**
 * Check the extended pipe to determine if the process has been signalled to die.
 */
CLHY_DECLARE(int) clhy_clmp_podx_check(clhy_pod_t *pod);

/**
 * Close the pipe-of-death
 *
 * @param pod The pipe-of-death to close.
 */
CLHY_DECLARE(kuda_status_t) clhy_clmp_podx_close(clhy_pod_t *pod);

/**
 * Write data to the extended pipe-of-death, signalling that one child process
 * should die.
 * @param pod the pipe-of-death to write to.
 * @param graceful restart-type
 */
CLHY_DECLARE(kuda_status_t) clhy_clmp_podx_signal(clhy_pod_t *pod,
                                            clhy_podx_restart_t graceful);

/**
 * Write data to the extended pipe-of-death, signalling that all child process
 * should die.
 * @param pod The pipe-of-death to write to.
 * @param num The number of child processes to kill
 * @param graceful restart-type
 */
CLHY_DECLARE(void) clhy_clmp_podx_killpg(clhy_pod_t *pod, int num,
                                    clhy_podx_restart_t graceful);

#endif /* (!WIN32 && !NETWARE) || DOXYGEN */

/**
 * Check that exactly one cLMP is loaded
 * Returns NULL if yes, error string if not.
 */
CLHY_DECLARE(const char *) clhy_check_clcore(void);

/*
 * These data members are common to all cLMPs. Each new core
 * should either use the appropriate clhy_clmp_set_* function
 * in their command table or create their own for custom or
 * platform specific needs. These should work for most.
 */

/**
 * The maximum number of requests each child thread or
 * process handles before dying off
 */
CLHY_DECLARE_DATA extern int clhy_max_requests_per_child;
const char *clhy_clmp_set_max_requests(cmd_parms *cmd, void *dummy,
                                    const char *arg);

/**
 * The filename used to store the process id.
 */
CLHY_DECLARE_DATA extern const char *clhy_pid_fname;
const char *clhy_clmp_set_pidfile(cmd_parms *cmd, void *dummy,
                               const char *arg);
void clhy_clmp_dump_pidfile(kuda_pool_t *p, kuda_file_t *out);

/*
 * The directory that the server changes directory to dump core.
 */
CLHY_DECLARE_DATA extern char clhy_coredump_dir[MAX_STRING_LEN];
CLHY_DECLARE_DATA extern int clhy_coredumpdir_configured;
const char *clhy_clmp_set_coredumpdir(cmd_parms *cmd, void *dummy,
                                   const char *arg);

/**
 * Set the timeout period for a graceful shutdown.
 */
CLHY_DECLARE_DATA extern int clhy_graceful_shutdown_timeout;
CLHY_DECLARE(const char *)clhy_clmp_set_graceful_shutdown(cmd_parms *cmd, void *dummy,
                                         const char *arg);
#define CLHY_GRACEFUL_SHUTDOWN_TIMEOUT_COMMAND \
CLHY_INIT_TAKE1("GracefulShutdownTimeout", clhy_clmp_set_graceful_shutdown, NULL, \
              RSRC_CONF, "Maximum time in seconds to wait for child "        \
              "processes to complete transactions during shutdown")


int clhy_signal_server(int *, kuda_pool_t *);
void clhy_clmp_rewrite_args(process_rec *);

CLHY_DECLARE_DATA extern kuda_uint32_t clhy_max_mem_free;
extern const char *clhy_clmp_set_max_mem_free(cmd_parms *cmd, void *dummy,
                                           const char *arg);

CLHY_DECLARE_DATA extern kuda_size_t clhy_thread_stacksize;
extern const char *clhy_clmp_set_thread_stacksize(cmd_parms *cmd, void *dummy,
                                               const char *arg);

/* core's implementation of child_status hook */
extern void clhy_core_child_status(server_rec *s, pid_t pid, clhy_generation_t gen,
                                 int slot, clmp_child_status status);

#if CLHY_ENABLE_EXCEPTION_HOOK
extern const char *clhy_clmp_set_exception_hook(cmd_parms *cmd, void *dummy,
                                             const char *arg);
#endif

CLHY_DECLARE_HOOK(int,monitor,(kuda_pool_t *p, server_rec *s))

/* register cAPIs that undertake to manage system security */
CLHY_DECLARE(int) clhy_sys_privileges_handlers(int inc);
CLHY_DECLARE_HOOK(int, drop_privileges, (kuda_pool_t * pchild, server_rec * s))

/* implement the clhy_clmp_query() function
 * The cLMP should return OK+KUDA_ENOTIMPL for any unimplemented query codes;
 * cAPIs which intercede for specific query codes should DECLINE for others.
 */
CLHY_DECLARE_HOOK(int, clmp_query, (int query_code, int *result, kuda_status_t *rv))

/* register the specified callback */
CLHY_DECLARE_HOOK(kuda_status_t, clmp_register_timed_callback,
                (kuda_time_t t, clhy_clmp_callback_fn_t *cbfn, void *baton))

/* get cLMP name (e.g., "prefork" or "event") */
CLHY_DECLARE_HOOK(const char *,clmp_get_name,(void))

/**
 * Notification that connection handling is suspending (disassociating from the
 * current thread)
 * @param c The current connection
 * @param r The current request, or NULL if there is no active request
 * @ingroup hooks
 * @see clhy_hook_resume_connection
 * @note This hook is not implemented by cLMPs like Prefork and Worker which 
 * handle all processing of a particular connection on the same thread.
 * @note This hook will be called on the thread that was previously
 * processing the connection.
 * @note This hook is not called at the end of connection processing.  This
 * hook only notifies a cAPI when processing of an active connection is
 * suspended.
 * @note Resumption and subsequent suspension of a connection solely to perform
 * I/O by the cLMP, with no execution of non-cLMP code, may not necessarily result
 * in a call to this hook.
 */
CLHY_DECLARE_HOOK(void, suspend_connection,
                (conn_rec *c, request_rec *r))

/**
 * Notification that connection handling is resuming (associating with a thread)
 * @param c The current connection
 * @param r The current request, or NULL if there is no active request
 * @ingroup hooks
 * @see clhy_hook_suspend_connection
 * @note This hook is not implemented by cLMPs like Prefork and Worker which 
 * handle all processing of a particular connection on the same thread.
 * @note This hook will be called on the thread that will resume processing
 * the connection.
 * @note This hook is not called at the beginning of connection processing.
 * This hook only notifies a cAPI when processing resumes for a
 * previously-suspended connection.
 * @note Resumption and subsequent suspension of a connection solely to perform
 * I/O by the cLMP, with no execution of non-cLMP code, may not necessarily result
 * in a call to this hook.
 */
CLHY_DECLARE_HOOK(void, resume_connection,
                (conn_rec *c, request_rec *r))

/* mutex type string for accept mutex, if any; cLMPs should use the
 * same mutex type for ease of configuration
 */
#define CLHY_ACCEPT_MUTEX_TYPE "clmp-accept"

/* internal pre-config logic for cLMP-related settings, callable only from
 * core's pre-config hook
 */
void core_common_pre_config(kuda_pool_t *pconf);

#ifdef __cplusplus
}
#endif

#endif /* !CLHYKUDEL_CLMP_COMMON_H */
/** @} */
