/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef CLHYKUDEL_HTTP_REQUEST_H
#define CLHYKUDEL_HTTP_REQUEST_H

#include "kuda_optional.h"
#include "util_filter.h"

#ifdef __cplusplus
extern "C" {
#endif

#define CLHY_SUBREQ_NO_ARGS 0
#define CLHY_SUBREQ_MERGE_ARGS 1

CLHY_DECLARE(int) clhy_process_request_internal(request_rec *r);

CLHY_DECLARE(request_rec *) clhy_sub_req_lookup_uri(const char *new_uri,
                                                const request_rec *r,
                                                clhy_filter_t *next_filter);

CLHY_DECLARE(request_rec *) clhy_sub_req_lookup_file(const char *new_file,
                                              const request_rec *r,
                                              clhy_filter_t *next_filter);

CLHY_DECLARE(request_rec *) clhy_sub_req_lookup_dirent(const kuda_finfo_t *finfo,
                                                   const request_rec *r,
                                                   int subtype,
                                                   clhy_filter_t *next_filter);

CLHY_DECLARE(request_rec *) clhy_sub_req_method_uri(const char *method,
                                                const char *new_uri,
                                                const request_rec *r,
                                                clhy_filter_t *next_filter);

CLHY_CORE_DECLARE_NONSTD(kuda_status_t) clhy_sub_req_output_filter(clhy_filter_t *f,
                                                        kuda_bucket_brigade *bb);

CLHY_DECLARE(int) clhy_run_sub_req(request_rec *r);

CLHY_DECLARE(void) clhy_destroy_sub_req(request_rec *r);

CLHY_DECLARE(void) clhy_internal_redirect(const char *new_uri, request_rec *r);

CLHY_DECLARE(void) clhy_internal_redirect_handler(const char *new_uri, request_rec *r);

CLHY_DECLARE(void) clhy_internal_fast_redirect(request_rec *sub_req, request_rec *r);


CLHY_DECLARE(int) clhy_some_auth_required(request_rec *r);


#define CLHY_AUTH_INTERNAL_PER_URI  0  /**< Run access control hooks on all
                                          internal requests with URIs
                                          distinct from that of initial
                                          request */
#define CLHY_AUTH_INTERNAL_PER_CONF 1  /**< Run access control hooks only on
                                          internal requests with
                                          configurations distinct from
                                          that of initial request */
#define CLHY_AUTH_INTERNAL_MASK     0x000F  /**< mask to extract internal request
                                               processing mode */

CLHY_DECLARE(void) clhy_clear_auth_internal(void);

CLHY_DECLARE(void) clhy_setup_auth_internal(kuda_pool_t *ptemp);

CLHY_DECLARE(kuda_status_t) clhy_register_auth_provider(kuda_pool_t *pool,
                                                   const char *provider_group,
                                                   const char *provider_name,
                                                   const char *provider_version,
                                                   const void *provider,
                                                   int type);

/** @} */

KUDA_DECLARE_OPTIONAL_FN(kuda_array_header_t *, authn_clhy_list_provider_names,
                        (kuda_pool_t *ptemp));
KUDA_DECLARE_OPTIONAL_FN(kuda_array_header_t *, authz_clhy_list_provider_names,
                        (kuda_pool_t *ptemp));

CLHY_DECLARE(int) clhy_is_initial_req(request_rec *r);

CLHY_DECLARE(void) clhy_update_mtime(request_rec *r, kuda_time_t dependency_mtime);

CLHY_DECLARE(void) clhy_allow_methods(request_rec *r, int reset, ...)
                 CLHY_FN_ATTR_SENTINEL;

CLHY_DECLARE(void) clhy_allow_standard_methods(request_rec *r, int reset, ...);

#define MERGE_ALLOW 0
#define REPLACE_ALLOW 1

CLHY_DECLARE(void) clhy_process_request(request_rec *r);

CLHY_DECLARE(void) clhy_process_request_after_handler(request_rec *r);

void clhy_process_async_request(request_rec *r);

CLHY_DECLARE(void) clhy_die(int type, request_rec *r);

CLHY_DECLARE(kuda_status_t) clhy_check_pipeline(conn_rec *c, kuda_bucket_brigade *bb,
                                           unsigned int max_blank_lines);

/* Hooks */

CLHY_DECLARE_HOOK(int,create_request,(request_rec *r))

CLHY_DECLARE_HOOK(int,translate_name,(request_rec *r))

CLHY_DECLARE_HOOK(int,map_to_storage,(request_rec *r))

CLHY_DECLARE_HOOK(int,check_user_id,(request_rec *r))

CLHY_DECLARE_HOOK(int,fixups,(request_rec *r))

CLHY_DECLARE_HOOK(int,type_checker,(request_rec *r))

CLHY_DECLARE_HOOK(int,access_checker,(request_rec *r))

CLHY_DECLARE_HOOK(int,access_checker_ex,(request_rec *r))

CLHY_DECLARE_HOOK(int,auth_checker,(request_rec *r))

CLHY_DECLARE(void) clhy_hook_check_access(clhy_HOOK_access_checker_t *pf,
                                      const char * const *aszPre,
                                      const char * const *aszSucc,
                                      int nOrder, int type);

CLHY_DECLARE(void) clhy_hook_check_access_ex(clhy_HOOK_access_checker_ex_t *pf,
                                         const char * const *aszPre,
                                         const char * const *aszSucc,
                                         int nOrder, int type);

CLHY_DECLARE(void) clhy_hook_check_authn(clhy_HOOK_check_user_id_t *pf,
                                     const char * const *aszPre,
                                     const char * const *aszSucc,
                                     int nOrder, int type);

CLHY_DECLARE(void) clhy_hook_check_authz(clhy_HOOK_auth_checker_t *pf,
                                     const char * const *aszPre,
                                     const char * const *aszSucc,
                                     int nOrder, int type);

CLHY_DECLARE_HOOK(void,insert_filter,(request_rec *r))

CLHY_DECLARE_HOOK(int,post_perdir_config,(request_rec *r))

CLHY_DECLARE_HOOK(int,force_authn,(request_rec *r))

CLHY_DECLARE_HOOK(kuda_status_t,dirwalk_stat,(kuda_finfo_t *finfo, request_rec *r, kuda_int32_t wanted))

CLHY_DECLARE(int) clhy_location_walk(request_rec *r);
CLHY_DECLARE(int) clhy_directory_walk(request_rec *r);
CLHY_DECLARE(int) clhy_file_walk(request_rec *r);
CLHY_DECLARE(int) clhy_if_walk(request_rec *r);

CLHY_DECLARE_DATA extern const kuda_bucket_type_t clhy_bucket_type_eor;

#define CLHY_BUCKET_IS_EOR(e)         (e->type == &clhy_bucket_type_eor)

CLHY_DECLARE(kuda_bucket *) clhy_bucket_eor_make(kuda_bucket *b, request_rec *r);

CLHY_DECLARE(kuda_bucket *) clhy_bucket_eor_create(kuda_bucket_alloc_t *list,
                                              request_rec *r);

CLHY_DECLARE(int) clhy_some_authn_required(request_rec *r);

#ifdef __cplusplus
}
#endif

#endif  /* !CLHYKUDEL_HTTP_REQUEST_H */
/** @} */
