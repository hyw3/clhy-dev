/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  clhy_compat.h
 * @brief Redefine cLHy 1.3 symbols
 */

#ifndef CLHY_COMPAT_H
#define CLHY_COMPAT_H

/* redefine 1.3.x symbols to the new symbol names */

#define CAPI_VAR_EXPORT    CLHY_CAPI_DECLARE_DATA
#define clhy_send_http_header(r) ;

#endif /* CLHY_COMPAT_H */
