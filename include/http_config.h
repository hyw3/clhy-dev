/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file http_config.h
 * @brief cLHy Configuration
 *
 * @defgroup CLHYKUDEL_CORE_CONFIG Configuration
 * @ingroup  CLHYKUDEL_CORE
 * @{
 */

#ifndef CLHYKUDEL_HTTP_CONFIG_H
#define CLHYKUDEL_HTTP_CONFIG_H

#include "util_cfgtree.h"
#include "clhy_config.h"
#include "kuda_tables.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * The central data structures around here...
 */

/* Command dispatch structures... */

/**
 * How the directives arguments should be parsed.
 * @remark Note that for all of these except RAW_ARGS, the config routine is
 *      passed a freshly allocated string which can be modified or stored
 *      or whatever...
 */
enum cmd_how {
    RAW_ARGS,           /**< cmd_func parses command line itself */
    TAKE1,              /**< one argument only */
    TAKE2,              /**< two arguments only */
    ITERATE,            /**< one argument, occurring multiple times
                         * (e.g., IndexIgnore)
                         */
    ITERATE2,           /**< two arguments, 2nd occurs multiple times
                         * (e.g., AddIcon)
                         */
    FLAG,               /**< One of 'On' or 'Off' */
    NO_ARGS,            /**< No args at all, e.g. &lt;/Directory&gt; */
    TAKE12,             /**< one or two arguments */
    TAKE3,              /**< three arguments only */
    TAKE23,             /**< two or three arguments */
    TAKE123,            /**< one, two or three arguments */
    TAKE13,             /**< one or three arguments */
    TAKE_ARGV           /**< an argc and argv are passed */
};

/**
 * This structure is passed to a command which is being invoked,
 * to carry a large variety of miscellaneous data which is all of
 * use to *somebody*...
 */
typedef struct cmd_parms_struct cmd_parms;

#if defined(CLHY_HAVE_DESIGNATED_INITIALIZER) || defined(DOXYGEN)

/**
 * All the types of functions that can be used in directives
 * @internal
 */
typedef union {
    /** function to call for a no-args */
    const char *(*no_args) (cmd_parms *parms, void *mconfig);
    /** function to call for a raw-args */
    const char *(*raw_args) (cmd_parms *parms, void *mconfig,
                             const char *args);
    /** function to call for a argv/argc */
    const char *(*take_argv) (cmd_parms *parms, void *mconfig,
                             int argc, char *const argv[]);
    /** function to call for a take1 */
    const char *(*take1) (cmd_parms *parms, void *mconfig, const char *w);
    /** function to call for a take2 */
    const char *(*take2) (cmd_parms *parms, void *mconfig, const char *w,
                          const char *w2);
    /** function to call for a take3 */
    const char *(*take3) (cmd_parms *parms, void *mconfig, const char *w,
                          const char *w2, const char *w3);
    /** function to call for a flag */
    const char *(*flag) (cmd_parms *parms, void *mconfig, int on);
} cmd_func;

/** This configuration directive does not take any arguments */
# define CLHY_NO_ARGS     func.no_args
/** This configuration directive will handle its own parsing of arguments*/
# define CLHY_RAW_ARGS    func.raw_args
/** This configuration directive will handle its own parsing of arguments*/
# define CLHY_TAKE_ARGV   func.take_argv
/** This configuration directive takes 1 argument*/
# define CLHY_TAKE1       func.take1
/** This configuration directive takes 2 arguments */
# define CLHY_TAKE2       func.take2
/** This configuration directive takes 3 arguments */
# define CLHY_TAKE3       func.take3
/** This configuration directive takes a flag (on/off) as a argument*/
# define CLHY_FLAG        func.flag

/** mechanism for declaring a directive with no arguments */
# define CLHY_INIT_NO_ARGS(directive, func, mconfig, where, help) \
    { directive, { .no_args=func }, mconfig, where, RAW_ARGS, help }
/** mechanism for declaring a directive with raw argument parsing */
# define CLHY_INIT_RAW_ARGS(directive, func, mconfig, where, help) \
    { directive, { .raw_args=func }, mconfig, where, RAW_ARGS, help }
/** mechanism for declaring a directive with raw argument parsing */
# define CLHY_INIT_TAKE_ARGV(directive, func, mconfig, where, help) \
    { directive, { .take_argv=func }, mconfig, where, TAKE_ARGV, help }
/** mechanism for declaring a directive which takes 1 argument */
# define CLHY_INIT_TAKE1(directive, func, mconfig, where, help) \
    { directive, { .take1=func }, mconfig, where, TAKE1, help }
/** mechanism for declaring a directive which takes multiple arguments */
# define CLHY_INIT_ITERATE(directive, func, mconfig, where, help) \
    { directive, { .take1=func }, mconfig, where, ITERATE, help }
/** mechanism for declaring a directive which takes 2 arguments */
# define CLHY_INIT_TAKE2(directive, func, mconfig, where, help) \
    { directive, { .take2=func }, mconfig, where, TAKE2, help }
/** mechanism for declaring a directive which takes 1 or 2 arguments */
# define CLHY_INIT_TAKE12(directive, func, mconfig, where, help) \
    { directive, { .take2=func }, mconfig, where, TAKE12, help }
/** mechanism for declaring a directive which takes multiple 2 arguments */
# define CLHY_INIT_ITERATE2(directive, func, mconfig, where, help) \
    { directive, { .take2=func }, mconfig, where, ITERATE2, help }
/** mechanism for declaring a directive which takes 1 or 3 arguments */
# define CLHY_INIT_TAKE13(directive, func, mconfig, where, help) \
    { directive, { .take3=func }, mconfig, where, TAKE13, help }
/** mechanism for declaring a directive which takes 2 or 3 arguments */
# define CLHY_INIT_TAKE23(directive, func, mconfig, where, help) \
    { directive, { .take3=func }, mconfig, where, TAKE23, help }
/** mechanism for declaring a directive which takes 1 to 3 arguments */
# define CLHY_INIT_TAKE123(directive, func, mconfig, where, help) \
    { directive, { .take3=func }, mconfig, where, TAKE123, help }
/** mechanism for declaring a directive which takes 3 arguments */
# define CLHY_INIT_TAKE3(directive, func, mconfig, where, help) \
    { directive, { .take3=func }, mconfig, where, TAKE3, help }
/** mechanism for declaring a directive which takes a flag (on/off) argument */
# define CLHY_INIT_FLAG(directive, func, mconfig, where, help) \
    { directive, { .flag=func }, mconfig, where, FLAG, help }

#else /* CLHY_HAVE_DESIGNATED_INITIALIZER */

typedef const char *(*cmd_func) ();

# define CLHY_NO_ARGS  func
# define CLHY_RAW_ARGS func
# define CLHY_TAKE_ARGV func
# define CLHY_TAKE1    func
# define CLHY_TAKE2    func
# define CLHY_TAKE3    func
# define CLHY_FLAG     func

# define CLHY_INIT_NO_ARGS(directive, func, mconfig, where, help) \
    { directive, func, mconfig, where, RAW_ARGS, help }
# define CLHY_INIT_RAW_ARGS(directive, func, mconfig, where, help) \
    { directive, func, mconfig, where, RAW_ARGS, help }
# define CLHY_INIT_TAKE_ARGV(directive, func, mconfig, where, help) \
    { directive, func, mconfig, where, TAKE_ARGV, help }
# define CLHY_INIT_TAKE1(directive, func, mconfig, where, help) \
    { directive, func, mconfig, where, TAKE1, help }
# define CLHY_INIT_ITERATE(directive, func, mconfig, where, help) \
    { directive, func, mconfig, where, ITERATE, help }
# define CLHY_INIT_TAKE2(directive, func, mconfig, where, help) \
    { directive, func, mconfig, where, TAKE2, help }
# define CLHY_INIT_TAKE12(directive, func, mconfig, where, help) \
    { directive, func, mconfig, where, TAKE12, help }
# define CLHY_INIT_ITERATE2(directive, func, mconfig, where, help) \
    { directive, func, mconfig, where, ITERATE2, help }
# define CLHY_INIT_TAKE13(directive, func, mconfig, where, help) \
    { directive, func, mconfig, where, TAKE13, help }
# define CLHY_INIT_TAKE23(directive, func, mconfig, where, help) \
    { directive, func, mconfig, where, TAKE23, help }
# define CLHY_INIT_TAKE123(directive, func, mconfig, where, help) \
    { directive, func, mconfig, where, TAKE123, help }
# define CLHY_INIT_TAKE3(directive, func, mconfig, where, help) \
    { directive, func, mconfig, where, TAKE3, help }
# define CLHY_INIT_FLAG(directive, func, mconfig, where, help) \
    { directive, func, mconfig, where, FLAG, help }

#endif /* CLHY_HAVE_DESIGNATED_INITIALIZER */

/**
 * The command record structure.  cAPIs can define a table of these
 * to define the directives it will implement.
 */
typedef struct command_struct command_rec;
struct command_struct {
    /** Name of this command */
    const char *name;
    /** The function to be called when this directive is parsed */
    cmd_func func;
    /** Extra data, for functions which implement multiple commands... */
    void *cmd_data;
    /** What overrides need to be allowed to enable this command. */
    int req_override;
    /** What the command expects as arguments */
    enum cmd_how args_how;

    /** 'usage' message, in case of syntax errors */
    const char *errmsg;
};

/**
 * @defgroup ConfigDirectives Allowed locations for configuration directives.
 *
 * The allowed locations for a configuration directive are the union of
 * those indicated by each set bit in the req_override mask.
 *
 * @{
 */
#define OR_NONE 0             /**< *.conf is not available anywhere in this override */
#define OR_LIMIT 1           /**< *.conf inside &lt;Directory&gt; or &lt;Location&gt;
                                and .htaccess when AllowOverride Limit */
#define OR_OPTIONS 2         /**< *.conf anywhere
                                and .htaccess when AllowOverride Options */
#define OR_FILEINFO 4        /**< *.conf anywhere
                                and .htaccess when AllowOverride FileInfo */
#define OR_AUTHCFG 8         /**< *.conf inside &lt;Directory&gt; or &lt;Location&gt;
                                and .htaccess when AllowOverride AuthConfig */
#define OR_INDEXES 16        /**< *.conf anywhere
                                and .htaccess when AllowOverride Indexes */
#define OR_UNSET 32          /**< bit to indicate that AllowOverride has not been set */
#define ACCESS_CONF 64       /**< *.conf inside &lt;Directory&gt; or &lt;Location&gt; */
#define RSRC_CONF 128        /**< *.conf outside &lt;Directory&gt; or &lt;Location&gt; */
#define EXEC_ON_READ 256     /**< force directive to execute a command
                which would modify the configuration (like including another
                file, or IFcAPI */
/* Flags to determine whether syntax errors in .htaccess should be
 * treated as nonfatal (log and ignore errors)
 */
#define NONFATAL_OVERRIDE 512    /* Violation of AllowOverride rule */
#define NONFATAL_UNKNOWN 1024    /* Unrecognised directive */
#define NONFATAL_ALL (NONFATAL_OVERRIDE|NONFATAL_UNKNOWN)

#define PROXY_CONF 2048      /**< *.conf inside &lt;Proxy&gt; only */

/** this directive can be placed anywhere */
#define OR_ALL (OR_LIMIT|OR_OPTIONS|OR_FILEINFO|OR_AUTHCFG|OR_INDEXES)

/** @} */

/**
 * This can be returned by a function if they don't wish to handle
 * a command. Make it something not likely someone will actually use
 * as an error code.
 */
#define DECLINE_CMD "\a\b"

/** Common structure for reading of config files / passwd files etc. */
typedef struct clhy_configfile_t clhy_configfile_t;
struct clhy_configfile_t {
    /**< an kuda_file_getc()-like function */
    kuda_status_t (*getch) (char *ch, void *param);
    /**< an kuda_file_gets()-like function */
    kuda_status_t (*getstr) (void *buf, kuda_size_t bufsiz, void *param);
    /**< a close handler function */
    kuda_status_t (*close) (void *param);
    /**< the argument passed to getch/getstr/close */
    void *param;
    /**< the filename / description */
    const char *name;
    /**< current line number, starting at 1 */
    unsigned line_number;
};

/**
 * This structure is passed to a command which is being invoked,
 * to carry a large variety of miscellaneous data which is all of
 * use to *somebody*...
 */
struct cmd_parms_struct {
    /** Argument to command from cmd_table */
    void *info;
    /** Which allow-override bits are set */
    int override;
    /** Which allow-override-opts bits are set */
    int override_opts;
    /** Table of directives allowed per AllowOverrideList */
    kuda_table_t *override_list;
    /** Which methods are &lt;Limit&gt;ed */
    kuda_int64_t limited;
    /** methods which are limited */
    kuda_array_header_t *limited_xmethods;
    /** methods which are xlimited */
    clhy_method_list_t *xlimited;

    /** Config file structure. */
    clhy_configfile_t *config_file;
    /** the directive specifying this command */
    clhy_directive_t *directive;

    /** Pool to allocate new storage in */
    kuda_pool_t *pool;
    /** Pool for scratch memory; persists during configuration, but
     *  wiped before the first request is served...  */
    kuda_pool_t *temp_pool;
    /** Server_rec being configured for */
    server_rec *server;
    /** If configuring for a directory, pathname of that directory.
     *  NOPE!  That's what it meant previous to the existence of &lt;Files&gt;,
     * &lt;Location&gt; and regex matching.  Now the only usefulness that can be
     * derived from this field is whether a command is being called in a
     * server context (path == NULL) or being called in a dir context
     * (path != NULL).  */
    char *path;
    /** configuration command */
    const command_rec *cmd;

    /** per_dir_config vector passed to handle_command */
    struct clhy_conf_vector_t *context;
    /** directive with syntax error */
    const clhy_directive_t *err_directive;

};

/**
 * Flags associated with a cAPI.
 */
#define CLHY_CAPI_FLAG_NONE         (0)
#define CLHY_CAPI_FLAG_ALWAYS_MERGE (1 << 0)

/**
 * cAPI structures.  Just about everything is dispatched through
 * these, directly or indirectly (through the command and handler
 * tables).
 */
typedef struct capi_struct cAPI;
struct capi_struct {
    /** API version, *not* cAPI version; check that cAPI is
     * compatible with this version of the server.
     */
    int version;
    /** API minor version. Provides API feature milestones. Not checked
     *  during cAPI init */
    int minor_version;
    /** Index to this cAPIs structures in config vectors.  */
    int capi_index;

    /** The name of the cAPI's C file */
    const char *name;
    /** The handle for the DSO.  Internal use only */
    void *dynamic_load_handle;

    /** A pointer to the next cAPI in the list
     *  @var capi_struct *next
     */
    struct capi_struct *next;

    /** Magic Cookie to identify a cAPI structure;  It's mainly
     *  important for the DSO facility (see also capi_so).  */
    unsigned long magic;

    /** Function to allow cLMPs to re-write command line arguments.  This
     *  hook is only available to cLMPs.
     *  @param The process that the server is running in.
     */
    void (*rewrite_args) (process_rec *process);
    /** Function to allow all cAPIs to create per directory configuration
     *  structures.
     *  @param p The pool to use for all allocations.
     *  @param dir The directory currently being processed.
     *  @return The per-directory structure created
     */
    void *(*create_dir_config) (kuda_pool_t *p, char *dir);
    /** Function to allow all cAPIs to merge the per directory configuration
     *  structures for two directories.
     *  @param p The pool to use for all allocations.
     *  @param base_conf The directory structure created for the parent directory.
     *  @param new_conf The directory structure currently being processed.
     *  @return The new per-directory structure created
     */
    void *(*merge_dir_config) (kuda_pool_t *p, void *base_conf, void *new_conf);
    /** Function to allow all cAPIs to create per server configuration
     *  structures.
     *  @param p The pool to use for all allocations.
     *  @param s The server currently being processed.
     *  @return The per-server structure created
     */
    void *(*create_server_config) (kuda_pool_t *p, server_rec *s);
    /** Function to allow all cAPIs to merge the per server configuration
     *  structures for two servers.
     *  @param p The pool to use for all allocations.
     *  @param base_conf The directory structure created for the parent directory.
     *  @param new_conf The directory structure currently being processed.
     *  @return The new per-directory structure created
     */
    void *(*merge_server_config) (kuda_pool_t *p, void *base_conf,
                                  void *new_conf);

    /** A command_rec table that describes all of the directives this cAPI
     * defines. */
    const command_rec *cmds;

    /** A hook to allow cAPIs to hook other points in the request processing.
     *  In this function, cAPIs should call the clhy_hook_*() functions to
     *  register an interest in a specific step in processing the current
     *  request.
     *  @param p the pool to use for all allocations
     */
    void (*register_hooks) (kuda_pool_t *p);

    /** A bitmask of CLHY_CAPI_FLAG_* */
    int flags;
};

/**
 * The CLHY_MAYBE_UNUSED macro is used for variable declarations that
 * might potentially exhibit "unused var" warnings on some compilers if
 * left untreated.
 * Since static intializers are not part of the C language (C89), making
 * (void) usage is not possible. However many compiler have proprietary 
 * mechanism to suppress those warnings.  
 */
#ifdef CLHY_MAYBE_UNUSED
#elif defined(__GNUC__)
# define CLHY_MAYBE_UNUSED(x) x __attribute__((unused)) 
#elif defined(__LCLINT__)
# define CLHY_MAYBE_UNUSED(x) /*@unused@*/ x  
#else
# define CLHY_MAYBE_UNUSED(x) x
#endif
    
/**
 * The CLHYLOG_USE_CAPI macro is used choose which cAPI a file belongs to.
 * This is necessary to allow per-cAPI loglevel configuration.
 *
 * CLHYLOG_USE_CAPI indirectly sets CLHYLOG_CAPI_INDEX and CLHYLOG_MARK.
 *
 * If a cAPI should be backward compatible with versions before 1.6,
 * CLHYLOG_USE_CAPI needs to be enclosed in a ifdef CLHYLOG_USE_CAPI block.
 *
 * @param foo name of the cAPI symbol of the current cAPI, without the
 *            trailing "_capi" part
 * @see CLHYLOG_MARK
 */
#define CLHYLOG_USE_CAPI(foo) \
    extern cAPI CLHY_CAPI_DECLARE_DATA foo##_capi;                  \
    CLHY_MAYBE_UNUSED(static int * const clhylog_capi_index) = &(foo##_capi.capi_index)

/**
 * CLHY_DECLARE_CAPI is a convenience macro that combines a call of
 * CLHYLOG_USE_CAPI with the definition of the cAPI symbol.
 *
 * If a cAPI should be backward compatible with versions before 2.3.6,
 * CLHYLOG_USE_CAPI should be used explicitly instead of CLHY_DECLARE_CAPI.
 */
#define CLHY_DECLARE_CAPI(foo) \
    CLHYLOG_USE_CAPI(foo);                         \
    cAPI CLHY_CAPI_DECLARE_DATA foo##_capi

/**
 * @defgroup cAPIInit cAPI structure initializers
 *
 * Initializer for the first few cAPI slots, which are only
 * really set up once we start running.  Note that the first two slots
 * provide a version check; this should allow us to deal with changes to
 * the API. The major number should reflect changes to the API handler table
 * itself or removal of functionality. The minor number should reflect
 * additions of functionality to the existing API. (the server can detect
 * an old-format cAPI, and either handle it back-compatibly, or at least
 * signal an error). See ./include/clhy_capimn.h for CAPIMN version history.
 * @{
 */

/** Some are used in cLHy 1.5, which will deliberately cause an error */
#define STANDARD_CAPI_STUFF   this_capi_needs_to_be_ported_to_clhy_1_6

/** Use this in all standard cAPIs */
#define STANDARD16_CAPI_STUFF CAPI_MAGIC_NUMBER_MAJOR, \
                                CAPI_MAGIC_NUMBER_MINOR, \
                                -1, \
                                __FILE__, \
                                NULL, \
                                NULL, \
                                CAPI_MAGIC_COOKIE, \
                                NULL      /* rewrite args spot */

/** Use this only in cLMPs */
#define CLMP16_CAPI_STUFF       CAPI_MAGIC_NUMBER_MAJOR, \
                                CAPI_MAGIC_NUMBER_MINOR, \
                                -1, \
                                __FILE__, \
                                NULL, \
                                NULL, \
                                CAPI_MAGIC_COOKIE

/** @} */

/* CONFIGURATION VECTOR FUNCTIONS */

/** configuration vector structure */
typedef struct clhy_conf_vector_t clhy_conf_vector_t;

/**
 * Generic accessors for other cAPIs to get at their own cAPI-specific
 * data
 * @param cv The vector in which the cAPIs configuration is stored.
 *        usually r->per_dir_config or s->capi_config
 * @param m The cAPI to get the data for.
 * @return The cAPI-specific data
 */
CLHY_DECLARE(void *) clhy_get_capi_config(const clhy_conf_vector_t *cv,
                                        const cAPI *m);

/**
 * Generic accessors for other cAPIs to set their own cAPI-specific
 * data
 * @param cv The vector in which the cAPIs configuration is stored.
 *        usually r->per_dir_config or s->capi_config
 * @param m The cAPI to set the data for.
 * @param val The cAPI-specific data to set
 */
CLHY_DECLARE(void) clhy_set_capi_config(clhy_conf_vector_t *cv, const cAPI *m,
                                      void *val);

/**
 * When cAPI flags have been introduced, and a way to check this.
 */
#define CLHY_CAPI_FLAGS_CAPIMN_MAJOR 20191101
#define CLHY_CAPI_FLAGS_CAPIMN_MINOR 7
#define CLHY_CAPI_HAS_FLAGS(m) \
        CLHY_CAPI_MAGIC_AT_LEAST(CLHY_CAPI_FLAGS_CAPIMN_MAJOR, \
                                 CLHY_CAPI_FLAGS_CAPIMN_MINOR)
/**
 * Generic accessor for the cAPI's flags
 * @param m The cAPI to get the flags from.
 * @return The cAPI-specific flags
 */
CLHY_DECLARE(int) clhy_get_capi_flags(const cAPI *m);

#if !defined(CLHY_DEBUG)

#define clhy_get_capi_config(v,m)       \
    (((void **)(v))[(m)->capi_index])
#define clhy_set_capi_config(v,m,val)   \
    ((((void **)(v))[(m)->capi_index]) = (val))

#endif /* CLHY_DEBUG */


/**
 * Generic accessor for cAPIs to get the cAPI-specific loglevel
 * @param s The server from which to get the loglevel.
 * @param index The capi_index of the cAPI to get the loglevel for.
 * @return The cAPI-specific loglevel
 */
CLHY_DECLARE(int) clhy_get_server_capi_loglevel(const server_rec *s, int index);

/**
 * Generic accessor for cAPIs the cAPI-specific loglevel
 * @param c The connection from which to get the loglevel.
 * @param index The capi_index of the cAPI to get the loglevel for.
 * @return The cAPI-specific loglevel
 */
CLHY_DECLARE(int) clhy_get_conn_capi_loglevel(const conn_rec *c, int index);

/**
 * Generic accessor for cAPIs the cAPI-specific loglevel
 * @param c The connection from which to get the loglevel.
 * @param s The server from which to get the loglevel if c does not have a
 *          specific loglevel configuration.
 * @param index The capi_index of the cAPI to get the loglevel for.
 * @return The cAPI-specific loglevel
 */
CLHY_DECLARE(int) clhy_get_conn_server_capi_loglevel(const conn_rec *c,
                                                   const server_rec *s,
                                                   int index);

/**
 * Generic accessor for cAPIs to get the cAPI-specific loglevel
 * @param r The request from which to get the loglevel.
 * @param index The capi_index of the cAPI to get the loglevel for.
 * @return The cAPI-specific loglevel
 */
CLHY_DECLARE(int) clhy_get_request_capi_loglevel(const request_rec *r, int index);

/**
 * Accessor to set cAPI-specific loglevel
 * @param p A pool
 * @param l The clhy_logconf struct to modify.
 * @param index The capi_index of the cAPI to set the loglevel for.
 * @param level The new log level
 */
CLHY_DECLARE(void) clhy_set_capi_loglevel(kuda_pool_t *p, struct clhy_logconf *l,
                                        int index, int level);

#if !defined(CLHY_DEBUG)

#define clhy_get_conn_logconf(c)                     \
    ((c)->log             ? (c)->log             : \
     &(c)->base_server->log)

#define clhy_get_conn_server_logconf(c,s)                             \
    ( ( (c)->log != &(c)->base_server->log && (c)->log != NULL )  ? \
      (c)->log                                                    : \
      &(s)->log )

#define clhy_get_request_logconf(r)                  \
    ((r)->log             ? (r)->log             : \
     (r)->connection->log ? (r)->connection->log : \
     &(r)->server->log)

#define clhy_get_capi_loglevel(l,i)                                     \
    (((i) < 0 || (l)->capi_levels == NULL || (l)->capi_levels[i] < 0) ?  \
     (l)->level :                                                         \
     (l)->capi_levels[i])

#define clhy_get_server_capi_loglevel(s,i)  \
    (clhy_get_capi_loglevel(&(s)->log,i))

#define clhy_get_conn_capi_loglevel(c,i)  \
    (clhy_get_capi_loglevel(clhy_get_conn_logconf(c),i))

#define clhy_get_conn_server_capi_loglevel(c,s,i)  \
    (clhy_get_capi_loglevel(clhy_get_conn_server_logconf(c,s),i))

#define clhy_get_request_capi_loglevel(r,i)  \
    (clhy_get_capi_loglevel(clhy_get_request_logconf(r),i))

#endif /* CLHY_DEBUG */

/**
 * Set all cAPI-specific loglevels to val
 * @param l The log config for which to set the loglevels.
 * @param val the value to set all loglevels to
 */
CLHY_DECLARE(void) clhy_reset_capi_loglevels(struct clhy_logconf *l, int val);

/**
 * Generic command handling function for strings
 * @param cmd The command parameters for this directive
 * @param struct_ptr pointer into a given type
 * @param arg The argument to the directive
 * @return An error string or NULL on success
 */
CLHY_DECLARE_NONSTD(const char *) clhy_set_string_slot(cmd_parms *cmd,
                                                   void *struct_ptr,
                                                   const char *arg);

/**
 * Generic command handling function for integers
 * @param cmd The command parameters for this directive
 * @param struct_ptr pointer into a given type
 * @param arg The argument to the directive
 * @return An error string or NULL on success
 */
CLHY_DECLARE_NONSTD(const char *) clhy_set_int_slot(cmd_parms *cmd,
                                                void *struct_ptr,
                                                const char *arg);

/**
 * Parsing function for log level
 * @param str The string to parse
 * @param val The parsed log level
 * @return An error string or NULL on success
 */
CLHY_DECLARE(const char *) clhy_parse_log_level(const char *str, int *val);

/**
 * Return true if the specified method is limited by being listed in
 * a &lt;Limit&gt; container, or by *not* being listed in a &lt;LimitExcept&gt;
 * container.
 *
 * @param   method  Pointer to a string specifying the method to check.
 * @param   cmd     Pointer to the cmd_parms structure passed to the
 *                  directive handler.
 * @return  0 if the method is not limited in the current scope
 */
CLHY_DECLARE(int) clhy_method_is_limited(cmd_parms *cmd, const char *method);

/**
 * Generic command handling function for strings, always sets the value
 * to a lowercase string
 * @param cmd The command parameters for this directive
 * @param struct_ptr pointer into a given type
 * @param arg The argument to the directive
 * @return An error string or NULL on success
 */
CLHY_DECLARE_NONSTD(const char *) clhy_set_string_slot_lower(cmd_parms *cmd,
                                                         void *struct_ptr,
                                                         const char *arg);
/**
 * Generic command handling function for flags stored in an int
 * @param cmd The command parameters for this directive
 * @param struct_ptr pointer into a given type
 * @param arg The argument to the directive (either 1 or 0)
 * @return An error string or NULL on success
 */
CLHY_DECLARE_NONSTD(const char *) clhy_set_flag_slot(cmd_parms *cmd,
                                                 void *struct_ptr,
                                                 int arg);
/**
 * Generic command handling function for flags stored in a char
 * @param cmd The command parameters for this directive
 * @param struct_ptr pointer into a given type
 * @param arg The argument to the directive (either 1 or 0)
 * @return An error string or NULL on success
 */
CLHY_DECLARE_NONSTD(const char *) clhy_set_flag_slot_char(cmd_parms *cmd,
                                                      void *struct_ptr,
                                                      int arg);
/**
 * Generic command handling function for files
 * @param cmd The command parameters for this directive
 * @param struct_ptr pointer into a given type
 * @param arg The argument to the directive
 * @return An error string or NULL on success
 */
CLHY_DECLARE_NONSTD(const char *) clhy_set_file_slot(cmd_parms *cmd,
                                                 void *struct_ptr,
                                                 const char *arg);
/**
 * Generic command handling function to respond with cmd->help as an error
 * @param cmd The command parameters for this directive
 * @param struct_ptr pointer into a given type
 * @param arg The argument to the directive
 * @return The cmd->help value as the error string
 * @note This allows simple declarations such as:
 * @code
 *     CLHY_INIT_RAW_ARGS("Foo", clhy_set_deprecated, NULL, OR_ALL,
 *         "The Foo directive is no longer supported, use Bar"),
 * @endcode
 */
CLHY_DECLARE_NONSTD(const char *) clhy_set_deprecated(cmd_parms *cmd,
                                                  void *struct_ptr,
                                                  const char *arg);
/**
 * For cAPIs which need to read config files, open logs, etc. this returns
 * the canonical form of fname made absolute to clhy_server_root.
 * @param p pool to allocate data from
 * @param fname The file name
 */
CLHY_DECLARE(char *) clhy_server_root_relative(kuda_pool_t *p, const char *fname);

/**
 * Compute the name of a run-time file (e.g., shared memory "file") relative
 * to the appropriate run-time directory.  Absolute paths are returned as-is.
 * The run-time directory is configured via the DefaultRuntimeDir directive or
 * at build time.
 */
CLHY_DECLARE(char *) clhy_runtime_dir_relative(kuda_pool_t *p, const char *fname);

/* Finally, the hook for dynamically loading cAPIs in... */

/**
 * Add a cAPI to the server
 * @param m The cAPI structure of the cAPI to add
 * @param p The pool of the same lifetime as the cAPI
 * @param s The cAPI's symbol name (used for logging)
 */
CLHY_DECLARE(const char *) clhy_add_capi(cAPI *m, kuda_pool_t *p,
                                       const char *s);

/**
 * Remove a cAPI from the server.  There are some caveats:
 * when the cAPI is removed, its slot is lost so all the current
 * per-dir and per-server configurations are invalid. So we should
 * only ever call this function when you are invalidating almost
 * all our current data. I.e. when doing a restart.
 * @param m the cAPI structure of the cAPI to remove
 */
CLHY_DECLARE(void) clhy_remove_capi(cAPI *m);
/**
 * Add a cAPI to the chained cAPIs list and the list of loaded cAPIs
 * @param mod The cAPI structure of the cAPI to add
 * @param p The pool with the same lifetime as the cAPI
 * @param s The cAPI's symbol name (used for logging)
 */
CLHY_DECLARE(const char *) clhy_add_activated_capi(cAPI *mod, kuda_pool_t *p,
                                              const char *s);
/**
 * Remove a cAPI fromthe chained cAPIs list and the list of loaded cAPIs
 * @param mod the cAPI structure of the cAPI to remove
 */
CLHY_DECLARE(void) clhy_remove_activated_capi(cAPI *mod);
/**
 * Find the name of the specified cAPI
 * @param m The cAPI to get the name for
 * @return the name of the cAPI
 */
CLHY_DECLARE(const char *) clhy_find_capi_name(cAPI *m);
/**
 * Find the short name of the cAPI identified by the specified cAPI index
 * @param capi_index The cAPI index to get the name for
 * @return the name of the cAPI, NULL if not found
 */
CLHY_DECLARE(const char *) clhy_find_capi_short_name(int capi_index);
/**
 * Find a cAPI based on the name of the cAPI
 * @param name the name of the cAPI
 * @return the cAPI structure if found, NULL otherwise
 */
CLHY_DECLARE(cAPI *) clhy_find_linked_capi(const char *name);

/**
 * Open a clhy_configfile_t as kuda_file_t
 * @param ret_cfg open clhy_configfile_t struct pointer
 * @param p The pool to allocate the structure from
 * @param name the name of the file to open
 */
CLHY_DECLARE(kuda_status_t) clhy_pcfg_openfile(clhy_configfile_t **ret_cfg,
                                          kuda_pool_t *p, const char *name);

/**
 * Allocate a clhy_configfile_t handle with user defined functions and params
 * @param p The pool to allocate from
 * @param descr The name of the file
 * @param param The argument passed to getch/getstr/close
 * @param getc_func The getch function
 * @param gets_func The getstr function
 * @param close_func The close function
 */
CLHY_DECLARE(clhy_configfile_t *) clhy_pcfg_open_custom(kuda_pool_t *p,
    const char *descr,
    void *param,
    kuda_status_t (*getc_func) (char *ch, void *param),
    kuda_status_t (*gets_func) (void *buf, kuda_size_t bufsiz, void *param),
    kuda_status_t (*close_func) (void *param));

/**
 * Read one line from open clhy_configfile_t, strip leading and trailing
 * whitespace, increase line number
 * @param buf place to store the line read
 * @param bufsize size of the buffer
 * @param cfp File to read from
 * @return error status, KUDA_ENOSPC if bufsize is too small for the line
 */
CLHY_DECLARE(kuda_status_t) clhy_cfg_getline(char *buf, kuda_size_t bufsize, clhy_configfile_t *cfp);

/**
 * Read one char from open configfile_t, increase line number upon LF
 * @param ch place to store the char read
 * @param cfp The file to read from
 * @return error status
 */
CLHY_DECLARE(kuda_status_t) clhy_cfg_getc(char *ch, clhy_configfile_t *cfp);

/**
 * Detach from open clhy_configfile_t, calling the close handler
 * @param cfp The file to close
 * @return 1 on success, 0 on failure
 */
CLHY_DECLARE(int) clhy_cfg_closefile(clhy_configfile_t *cfp);

/**
 * Convert a return value from clhy_cfg_getline or clhy_cfg_getc to a user friendly
 * string.
 * @param p The pool to allocate the string from
 * @param cfp The config file
 * @param rc The return value to convert
 * @return The error string, NULL if rc == KUDA_SUCCESS
 */
CLHY_DECLARE(const char *) clhy_pcfg_strerror(kuda_pool_t *p, clhy_configfile_t *cfp,
                                          kuda_status_t rc);

/**
 * Read all data between the current &lt;foo&gt; and the matching &lt;/foo&gt;.  All
 * of this data is forgotten immediately.
 * @param cmd The cmd_parms to pass to the directives inside the container
 * @param directive The directive name to read until
 * @return Error string on failure, NULL on success
 * @note If cmd->pool == cmd->temp_pool, clhy_soak_end_container() will assume
 *       .htaccess context and use a lower maximum line length.
 */
CLHY_DECLARE(const char *) clhy_soak_end_container(cmd_parms *cmd, char *directive);

/**
 * Read all data between the current &lt;foo&gt; and the matching &lt;/foo&gt; and build
 * a config tree from it
 * @param p pool to allocate from
 * @param temp_pool Temporary pool to allocate from
 * @param parms The cmd_parms to pass to all directives read
 * @param current The current node in the tree
 * @param curr_parent The current parent node
 * @param orig_directive The directive to read until hit.
 * @return Error string on failure, NULL on success
 * @note If p == temp_pool, clhy_build_cont_config() will assume .htaccess
 *       context and use a lower maximum line length.
*/
CLHY_DECLARE(const char *) clhy_build_cont_config(kuda_pool_t *p,
                                              kuda_pool_t *temp_pool,
                                              cmd_parms *parms,
                                              clhy_directive_t **current,
                                              clhy_directive_t **curr_parent,
                                              char *orig_directive);

/**
 * Build a config tree from a config file
 * @param parms The cmd_parms to pass to all of the directives in the file
 * @param conf_pool The pconf pool
 * @param temp_pool The temporary pool
 * @param conftree Place to store the root node of the config tree
 * @return Error string on erro, NULL otherwise
 * @note If conf_pool == temp_pool, clhy_build_config() will assume .htaccess
 *       context and use a lower maximum line length.
 */
CLHY_DECLARE(const char *) clhy_build_config(cmd_parms *parms,
                                         kuda_pool_t *conf_pool,
                                         kuda_pool_t *temp_pool,
                                         clhy_directive_t **conftree);

/**
 * Walk a config tree and setup the server's internal structures
 * @param conftree The config tree to walk
 * @param parms The cmd_parms to pass to all functions
 * @param section_vector The per-section config vector.
 * @return Error string on error, NULL otherwise
 */
CLHY_DECLARE(const char *) clhy_walk_config(clhy_directive_t *conftree,
                                        cmd_parms *parms,
                                        clhy_conf_vector_t *section_vector);

/**
 * Convenience function to create a clhy_dir_match_t structure from a cmd_parms.
 *
 * @param cmd The command.
 * @param flags Flags to indicate whether optional or recursive.
 * @param cb Callback for each file found that matches the wildcard. Return NULL on
 *        success, an error string on error.
 * @param ctx Context for the callback.
 * @return Structure clhy_dir_match_t with fields populated, allocated from the
 *         cmd->temp_pool.
 */
CLHY_DECLARE(clhy_dir_match_t *)clhy_dir_cfgmatch(cmd_parms *cmd, int flags,
        const char *(*cb)(clhy_dir_match_t *w, const char *fname), void *ctx)
        __attribute__((nonnull(1,3)));

/**
 * @defgroup clhy_check_cmd_context Check command context
 * @{
 */
/**
 * Check the context a command is used in.
 * @param cmd The command to check
 * @param forbidden Where the command is forbidden.
 * @return Error string on error, NULL on success
 */
CLHY_DECLARE(const char *) clhy_check_cmd_context(cmd_parms *cmd,
                                              unsigned forbidden);

#define  NOT_IN_VIRTUALHOST     0x01 /**< Forbidden in &lt;VirtualHost&gt; */
#define  NOT_IN_LIMIT           0x02 /**< Forbidden in &lt;Limit&gt; */
#define  NOT_IN_DIRECTORY       0x04 /**< Forbidden in &lt;Directory&gt; */
#define  NOT_IN_LOCATION        0x08 /**< Forbidden in &lt;Location&gt; */
#define  NOT_IN_FILES           0x10 /**< Forbidden in &lt;Files&gt; or &lt;If&gt;*/
#define  NOT_IN_HTACCESS        0x20 /**< Forbidden in .htaccess files */
#define  NOT_IN_PROXY           0x40 /**< Forbidden in &lt;Proxy&gt; */
/** Forbidden in &lt;Directory&gt;/&lt;Location&gt;/&lt;Files&gt;&lt;If&gt;*/
#define  NOT_IN_DIR_LOC_FILE    (NOT_IN_DIRECTORY|NOT_IN_LOCATION|NOT_IN_FILES)
/** Forbidden in &lt;Directory&gt;/&lt;Location&gt;/&lt;Files&gt;&lt;If&gt;&lt;Proxy&gt;*/
#define  NOT_IN_DIR_CONTEXT     (NOT_IN_LIMIT|NOT_IN_DIR_LOC_FILE|NOT_IN_PROXY)
/** Forbidden in &lt;VirtualHost&gt;/&lt;Limit&gt;/&lt;Directory&gt;/&lt;Location&gt;/&lt;Files&gt;/&lt;If&gt;&lt;Proxy&gt;*/
#define  GLOBAL_ONLY            (NOT_IN_VIRTUALHOST|NOT_IN_DIR_CONTEXT)

/** @} */

/**
 * @brief This structure is used to assign symbol names to cAPI pointers
 */
typedef struct {
    const char *name;
    cAPI *capip;
} clhy_capi_symbol_t;

/**
 * The topmost cAPI in the list
 * @var cAPI *clhy_top_capi
 */
CLHY_DECLARE_DATA extern cAPI *clhy_top_capi;

/**
 * Array of all statically linked cAPIs
 * @var cAPI *clhy_prelinked_capis[]
 */
CLHY_DECLARE_DATA extern cAPI *clhy_prelinked_capis[];
/**
 * Array of all statically linked cAPInames (symbols)
 * @var clhy_capi_symbol_t clhy_prelinked_capi_symbols[]
 */
CLHY_DECLARE_DATA extern clhy_capi_symbol_t clhy_prelinked_capi_symbols[];
/**
 * Array of all preloaded cAPIs
 * @var cAPI *clhy_preactivated_capis[]
 */
CLHY_DECLARE_DATA extern cAPI *clhy_preactivated_capis[];
/**
 * Array of all loaded cAPIs
 * @var cAPI **clhy_activated_capis
 */
CLHY_DECLARE_DATA extern cAPI **clhy_activated_capis;

/* For capi_so.c... */
/** Run a single cAPI's two create_config hooks
 *  @param p the pool to allocate from
 *  @param s The server to configure for.
 *  @param m The cAPI to configure
 */
CLHY_DECLARE(void) clhy_single_capi_configure(kuda_pool_t *p, server_rec *s,
                                            cAPI *m);

/* For http_main.c... */
/**
 * Add all of the prelinked cAPIs into the loaded cAPI list
 * @param process The process that is currently running the server
 */
CLHY_DECLARE(const char *) clhy_setup_prelinked_capis(process_rec *process);

/**
 * Show the preloaded configuration directives, the help string explaining
 * the directive arguments, in what cAPI they are handled, and in
 * what parts of the configuration they are allowed.  Used for wwhy -h.
 */
CLHY_DECLARE(void) clhy_show_directives(void);

/**
 * Returns non-zero if a configuration directive of the given name has
 * been registered by a cAPI at the time of calling.
 * @param p Pool for temporary allocations
 * @param name Directive name
 */
CLHY_DECLARE(int) clhy_exists_directive(kuda_pool_t *p, const char *name);

/**
 * Show the preloaded cAPI names.  Used for wwhy -l.
 */
CLHY_DECLARE(void) clhy_show_capis(void);

/**
 * Show the cLMP name.  Used in reporting cAPIs such as capi_info to
 * provide extra information to the user
 */
CLHY_DECLARE(const char *) clhy_show_clmp(void);

/**
 * Read all config files and setup the server
 * @param process The process running the server
 * @param temp_pool A pool to allocate temporary data from.
 * @param config_name The name of the config file
 * @param conftree Place to store the root of the config tree
 * @return The setup server_rec list.
 */
CLHY_DECLARE(server_rec *) clhy_read_config(process_rec *process,
                                        kuda_pool_t *temp_pool,
                                        const char *config_name,
                                        clhy_directive_t **conftree);

/**
 * Run all rewrite args hooks for loaded cAPIs
 * @param process The process currently running the server
 */
CLHY_DECLARE(void) clhy_run_rewrite_args(process_rec *process);

/**
 * Run the register hooks function for a specified cAPI
 * @param m The cAPI to run the register hooks function fo
 * @param p The pool valid for the lifetime of the cAPI
 */
CLHY_DECLARE(void) clhy_register_hooks(cAPI *m, kuda_pool_t *p);

/**
 * Setup all virtual hosts
 * @param p The pool to allocate from
 * @param main_server The head of the server_rec list
 */
CLHY_DECLARE(void) clhy_fixup_virtual_hosts(kuda_pool_t *p,
                                        server_rec *main_server);

/**
 * Reserve some cAPIs slots for cAPIs loaded by other means than
 * EXEC_ON_READ directives.
 * Relevant cAPIs should call this in the pre_config stage.
 * @param count The number of slots to reserve.
 */
CLHY_DECLARE(void) clhy_reserve_capi_slots(int count);

/**
 * Reserve some cAPIs slots for cAPIs loaded by a specific
 * non-EXEC_ON_READ config directive.
 * This counts how often the given directive is used in the config and calls
 * clhy_reserve_capi_slots() accordingly.
 * @param directive The name of the directive
 */
CLHY_DECLARE(void) clhy_reserve_capi_slots_directive(const char *directive);

/* For http_request.c... */

/**
 * Setup the config vector for a request_rec
 * @param p The pool to allocate the config vector from
 * @return The config vector
 */
CLHY_DECLARE(clhy_conf_vector_t*) clhy_create_request_config(kuda_pool_t *p);

/**
 * Setup the config vector for per dir cAPI configs
 * @param p The pool to allocate the config vector from
 * @return The config vector
 */
CLHY_CORE_DECLARE(clhy_conf_vector_t *) clhy_create_per_dir_config(kuda_pool_t *p);

/**
 * Run all of the cAPIs merge per dir config functions
 * @param p The pool to pass to the merge functions
 * @param base The base directory config structure
 * @param new_conf The new directory config structure
 */
CLHY_CORE_DECLARE(clhy_conf_vector_t*) clhy_merge_per_dir_configs(kuda_pool_t *p,
                                           clhy_conf_vector_t *base,
                                           clhy_conf_vector_t *new_conf);

/**
 * Allocate new clhy_logconf and make (deep) copy of old clhy_logconf
 * @param p The pool to alloc from
 * @param old The clhy_logconf to copy (may be NULL)
 * @return The new clhy_logconf struct
 */
CLHY_DECLARE(struct clhy_logconf *) clhy_new_log_config(kuda_pool_t *p,
                                                  const struct clhy_logconf *old);

/**
 * Merge old clhy_logconf into new clhy_logconf.
 * old and new must have the same life time.
 * @param old_conf The clhy_logconf to merge from
 * @param new_conf The clhy_logconf to merge into
 */
CLHY_DECLARE(void) clhy_merge_log_config(const struct clhy_logconf *old_conf,
                                     struct clhy_logconf *new_conf);

/* For http_connection.c... */
/**
 * Setup the config vector for a connection_rec
 * @param p The pool to allocate the config vector from
 * @return The config vector
 */
CLHY_CORE_DECLARE(clhy_conf_vector_t*) clhy_create_conn_config(kuda_pool_t *p);

/* For http_core.c... (&lt;Directory&gt; command and virtual hosts) */

/**
 * parse an htaccess file
 * @param result htaccess_result
 * @param r The request currently being served
 * @param override Which overrides are active
 * @param override_opts Which allow-override-opts bits are set
 * @param override_list Table of directives allowed for override
 * @param path The path to the htaccess file
 * @param access_name The list of possible names for .htaccess files
 * int The status of the current request
 */
CLHY_CORE_DECLARE(int) clhy_parse_htaccess(clhy_conf_vector_t **result,
                                       request_rec *r,
                                       int override,
                                       int override_opts,
                                       kuda_table_t *override_list,
                                       const char *path,
                                       const char *access_name);

/**
 * Setup a virtual host
 * @param p The pool to allocate all memory from
 * @param hostname The hostname of the virtual hsot
 * @param main_server The main server for this cLHy configuration
 * @param ps Place to store the new server_rec
 * return Error string on error, NULL on success
 */
CLHY_CORE_DECLARE(const char *) clhy_init_virtual_host(kuda_pool_t *p,
                                                   const char *hostname,
                                                   server_rec *main_server,
                                                   server_rec **ps);

/**
 * Process a config file for cLHy
 * @param s The server rec to use for the command parms
 * @param fname The name of the config file
 * @param conftree The root node of the created config tree
 * @param p Pool for general allocation
 * @param ptemp Pool for temporary allocation
 */
CLHY_DECLARE(const char *) clhy_process_resource_config(server_rec *s,
                                                    const char *fname,
                                                    clhy_directive_t **conftree,
                                                    kuda_pool_t *p,
                                                    kuda_pool_t *ptemp);

/**
 * Process all matching files as cLHy configs
 * @param s The server rec to use for the command parms
 * @param fname The filename pattern of the config file
 * @param conftree The root node of the created config tree
 * @param p Pool for general allocation
 * @param ptemp Pool for temporary allocation
 * @param optional Whether a no-match wildcard is allowed
 * @see kuda_fnmatch for pattern handling
 */
CLHY_DECLARE(const char *) clhy_process_fnmatch_configs(server_rec *s,
                                                    const char *fname,
                                                    clhy_directive_t **conftree,
                                                    kuda_pool_t *p,
                                                    kuda_pool_t *ptemp,
                                                    int optional);

/**
 * Process all directives in the config tree
 * @param s The server rec to use in the command parms
 * @param conftree The config tree to process
 * @param p The pool for general allocation
 * @param ptemp The pool for temporary allocations
 * @return OK if no problems
 */
CLHY_DECLARE(int) clhy_process_config_tree(server_rec *s,
                                       clhy_directive_t *conftree,
                                       kuda_pool_t *p,
                                       kuda_pool_t *ptemp);

/**
 * Store data which will be retained across unload/load of cAPIs
 * @param key The unique key associated with this cAPI's retained data
 * @param size in bytes of the retained data (to be allocated)
 * @return Address of new retained data structure, initially cleared
 */
CLHY_DECLARE(void *) clhy_retained_data_create(const char *key, kuda_size_t size);

/**
 * Retrieve data which was stored by clhy_retained_data_create()
 * @param key The unique key associated with this cAPI's retained data
 * @return Address of previously retained data structure, or NULL if not yet saved
 */
CLHY_DECLARE(void *) clhy_retained_data_get(const char *key);

/* cAPI-method dispatchers, also for http_request.c */
/**
 * Run the handler phase of each cAPI until a cAPI accepts the
 * responsibility of serving the request
 * @param r The current request
 * @return The status of the current request
 */
CLHY_CORE_DECLARE(int) clhy_invoke_handler(request_rec *r);

/* for capi_perl */

/**
 * Find a given directive in a command_rec table
 * @param name The directive to search for
 * @param cmds The table to search
 * @return The directive definition of the specified directive
 */
CLHY_CORE_DECLARE(const command_rec *) clhy_find_command(const char *name,
                                                     const command_rec *cmds);

/**
 * Find a given directive in a list of cAPIs.
 * @param cmd_name The directive to search for
 * @param mod Pointer to the first cAPI in the linked list; will be set to
 *            the cAPI providing cmd_name
 * @return The directive definition of the specified directive.
 *         *mod will be changed to point to the cAPI containing the
 *         directive.
 */
CLHY_CORE_DECLARE(const command_rec *) clhy_find_command_in_capis(const char *cmd_name,
                                                                cAPI **mod);

/**
 * Ask a cAPI to create per-server and per-section (dir/loc/file) configs
 * (if it hasn't happened already). The results are stored in the server's
 * config, and the specified per-section config vector.
 * @param server The server to operate upon.
 * @param section_vector The per-section config vector.
 * @param section Which section to create a config for.
 * @param mod The cAPI which is defining the config data.
 * @param pconf A pool for all configuration allocations.
 * @return The (new) per-section config data.
 */
CLHY_CORE_DECLARE(void *) clhy_set_config_vectors(server_rec *server,
                                              clhy_conf_vector_t *section_vector,
                                              const char *section,
                                              cAPI *mod, kuda_pool_t *pconf);

  /* Hooks */

/**
 * Run the header parser functions for each cAPI
 * @param r The current request
 * @return OK or DECLINED
 */
CLHY_DECLARE_HOOK(int,header_parser,(request_rec *r))

/**
 * Run the pre_config function for each cAPI
 * @param pconf The config pool
 * @param plog The logging streams pool
 * @param ptemp The temporary pool
 * @return OK or DECLINED on success anything else is a error
 */
CLHY_DECLARE_HOOK(int,pre_config,(kuda_pool_t *pconf,kuda_pool_t *plog,
                                kuda_pool_t *ptemp))

/**
 * Run the check_config function for each cAPI
 * @param pconf The config pool
 * @param plog The logging streams pool
 * @param ptemp The temporary pool
 * @param s the server to operate upon
 * @return OK or DECLINED on success anything else is a error
 */
CLHY_DECLARE_HOOK(int,check_config,(kuda_pool_t *pconf, kuda_pool_t *plog,
                                  kuda_pool_t *ptemp, server_rec *s))

/**
 * Run the test_config function for each cAPI; this hook is run
 * only if the server was invoked to test the configuration syntax.
 * @param pconf The config pool
 * @param s The list of server_recs
 * @note To avoid reordering problems due to different buffering, hook
 *       functions should only kuda_file_*() to print to stdout/stderr and
 *       not simple printf()/fprintf().
 *     
 */
CLHY_DECLARE_HOOK(void,test_config,(kuda_pool_t *pconf, server_rec *s))

/**
 * Run the post_config function for each cAPI
 * @param pconf The config pool
 * @param plog The logging streams pool
 * @param ptemp The temporary pool
 * @param s The list of server_recs
 * @return OK or DECLINED on success anything else is a error
 */
CLHY_DECLARE_HOOK(int,post_config,(kuda_pool_t *pconf,kuda_pool_t *plog,
                                 kuda_pool_t *ptemp,server_rec *s))

/**
 * Run the open_logs functions for each cAPI
 * @param pconf The config pool
 * @param plog The logging streams pool
 * @param ptemp The temporary pool
 * @param s The list of server_recs
 * @return OK or DECLINED on success anything else is a error
 */
CLHY_DECLARE_HOOK(int,open_logs,(kuda_pool_t *pconf,kuda_pool_t *plog,
                               kuda_pool_t *ptemp,server_rec *s))

/**
 * Run the child_init functions for each cAPI
 * @param pchild The child pool
 * @param s The list of server_recs in this server
 */
CLHY_DECLARE_HOOK(void,child_init,(kuda_pool_t *pchild, server_rec *s))

/**
 * Run the handler functions for each cAPI
 * @param r The request_rec
 * @remark non-wildcard handlers should HOOK_MIDDLE, wildcard HOOK_LAST
 */
CLHY_DECLARE_HOOK(int,handler,(request_rec *r))

/**
 * Run the quick handler functions for each cAPI. The quick_handler
 * is run before any other requests hooks are called (location_walk,
 * directory_walk, access checking, et. al.). This hook was added
 * to provide a quick way to serve content from a URI keyed cache.
 *
 * @param r The request_rec
 * @param lookup_uri Controls whether the caller actually wants content or not.
 * lookup is set when the quick_handler is called out of
 * clhy_sub_req_lookup_uri()
 */
CLHY_DECLARE_HOOK(int,quick_handler,(request_rec *r, int lookup_uri))

/**
 * Retrieve the optional functions for each cAPI.
 * This is run immediately before the server starts. Optional functions should
 * be registered during the hook registration phase.
 */
CLHY_DECLARE_HOOK(void,optional_fn_retrieve,(void))

/**
 * Allow cAPIs to open htaccess files or perform operations before doing so
 * @param r The current request
 * @param dir_name The directory for which the htaccess file should be opened
 * @param access_name The filename  for which the htaccess file should be opened
 * @param conffile Where the pointer to the opened clhy_configfile_t must be
 *        stored
 * @param full_name Where the full file name of the htaccess file must be
 *        stored.
 * @return KUDA_SUCCESS on success,
 *         KUDA_ENOENT or KUDA_ENOTDIR if no htaccess file exists,
 *         CLHY_DECLINED to let later cAPIs do the opening,
 *         any other error code on error.
 */
CLHY_DECLARE_HOOK(kuda_status_t,open_htaccess,
                (request_rec *r, const char *dir_name, const char *access_name,
                 clhy_configfile_t **conffile, const char **full_name))

/**
 * Core internal function, use clhy_run_open_htaccess() instead.
 */
kuda_status_t clhy_open_htaccess(request_rec *r, const char *dir_name,
        const char *access_name, clhy_configfile_t **conffile,
        const char **full_name);

/**
 * A generic pool cleanup that will reset a pointer to NULL. For use with
 * kuda_pool_cleanup_register.
 * @param data The address of the pointer
 * @return KUDA_SUCCESS
 */
CLHY_DECLARE_NONSTD(kuda_status_t) clhy_pool_cleanup_set_null(void *data);

#ifdef __cplusplus
}
#endif

#endif  /* !CLHYKUDEL_HTTP_CONFIG_H */
/** @} */
