/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file clhy_hooks.h
 * @brief clhy hook functions and macros
 */

#ifndef CLHY_HOOKS_H
#define CLHY_HOOKS_H

/* Although this file doesn't declare any hooks, declare the hook group here */
/**
 * @defgroup hooks cLHy Hooks
 * @ingroup  CLHYKUDEL_CORE
 */

#if defined(CLHY_HOOK_PROBES_ENABLED) && !defined(KUDA_HOOK_PROBES_ENABLED)
#define KUDA_HOOK_PROBES_ENABLED 1
#endif

#ifdef KUDA_HOOK_PROBES_ENABLED
#include "clhy_hook_probes.h"
#endif

#include "kuda.h"
#include "kuda_hooks.h"
#include "kuda_optional_hooks.h"

#ifdef DOXYGEN
/* define these just so doxygen documents them */

/**
 * CLHY_DECLARE_STATIC is defined when including cLHy's Core headers,
 * to provide static linkage when the dynamic library may be unavailable.
 *
 * @see CLHY_DECLARE_EXPORT
 *
 * CLHY_DECLARE_STATIC and CLHY_DECLARE_EXPORT are left undefined when
 * including cLHy's Core headers, to import and link the symbols from the
 * dynamic cLHy Core library and assure appropriate indirection and calling
 * conventions at compile time.
 */
# define CLHY_DECLARE_STATIC
/**
 * CLHY_DECLARE_EXPORT is defined when building the cLHy Core dynamic
 * library, so that all public symbols are exported.
 *
 * @see CLHY_DECLARE_STATIC
 */
# define CLHY_DECLARE_EXPORT

#endif /* def DOXYGEN */

/**
 * Declare a hook function
 * @param ret The return type of the hook
 * @param name The hook's name (as a literal)
 * @param args The arguments the hook function takes, in brackets.
 */
#define CLHY_DECLARE_HOOK(ret,name,args) \
        KUDA_DECLARE_EXTERNAL_HOOK(clhy,CLHY,ret,name,args)

/** @internal */
#define CLHY_IMPLEMENT_HOOK_BASE(name) \
        KUDA_IMPLEMENT_EXTERNAL_HOOK_BASE(clhy,CLHY,name)

/**
 * Implement an cLHy core hook that has no return code, and
 * therefore runs all of the registered functions. The implementation
 * is called clhy_run_<i>name</i>.
 *
 * @param name The name of the hook
 * @param args_decl The declaration of the arguments for the hook, for example
 * "(int x,void *y)"
 * @param args_use The arguments for the hook as used in a call, for example
 * "(x,y)"
 * @note If IMPLEMENTing a hook that is not linked into the cLHy core,
 * (e.g. within a dso) see KUDA_IMPLEMENT_EXTERNAL_HOOK_VOID.
 */
#define CLHY_IMPLEMENT_HOOK_VOID(name,args_decl,args_use) \
        KUDA_IMPLEMENT_EXTERNAL_HOOK_VOID(clhy,CLHY,name,args_decl,args_use)

/**
 * Implement an cLHy core hook that runs until one of the functions
 * returns something other than ok or decline. That return value is
 * then returned from the hook runner. If the hooks run to completion,
 * then ok is returned. Note that if no hook runs it would probably be
 * more correct to return decline, but this currently does not do
 * so. The implementation is called clhy_run_<i>name</i>.
 *
 * @param ret The return type of the hook (and the hook runner)
 * @param name The name of the hook
 * @param args_decl The declaration of the arguments for the hook, for example
 * "(int x,void *y)"
 * @param args_use The arguments for the hook as used in a call, for example
 * "(x,y)"
 * @param ok The "ok" return value
 * @param decline The "decline" return value
 * @return ok, decline or an error.
 * @note If IMPLEMENTing a hook that is not linked into the cLHy core,
 * (e.g. within a dso) see KUDA_IMPLEMENT_EXTERNAL_HOOK_RUN_ALL.
 */
#define CLHY_IMPLEMENT_HOOK_RUN_ALL(ret,name,args_decl,args_use,ok,decline) \
        KUDA_IMPLEMENT_EXTERNAL_HOOK_RUN_ALL(clhy,CLHY,ret,name,args_decl, \
                                            args_use,ok,decline)

/**
 * Implement a hook that runs until a function returns something other than
 * decline. If all functions return decline, the hook runner returns decline.
 * The implementation is called clhy_run_<i>name</i>.
 *
 * @param ret The return type of the hook (and the hook runner)
 * @param name The name of the hook
 * @param args_decl The declaration of the arguments for the hook, for example
 * "(int x,void *y)"
 * @param args_use The arguments for the hook as used in a call, for example
 * "(x,y)"
 * @param decline The "decline" return value
 * @return decline or an error.
 * @note If IMPLEMENTing a hook that is not linked into the cLHy core
 * (e.g. within a dso) see KUDA_IMPLEMENT_EXTERNAL_HOOK_RUN_FIRST.
 */
#define CLHY_IMPLEMENT_HOOK_RUN_FIRST(ret,name,args_decl,args_use,decline) \
        KUDA_IMPLEMENT_EXTERNAL_HOOK_RUN_FIRST(clhy,CLHY,ret,name,args_decl, \
                                              args_use,decline)

/* Note that the other optional hook implementations are straightforward but
 * have not yet been needed
 */

/**
 * Implement an optional hook. This is exactly the same as a standard hook
 * implementation, except the hook is optional.
 * @see CLHY_IMPLEMENT_HOOK_RUN_ALL
 */
#define CLHY_IMPLEMENT_OPTIONAL_HOOK_RUN_ALL(ret,name,args_decl,args_use,ok, \
                                           decline) \
        KUDA_IMPLEMENT_OPTIONAL_HOOK_RUN_ALL(clhy,CLHY,ret,name,args_decl, \
                                            args_use,ok,decline)

/**
 * Hook an optional hook. Unlike static hooks, this uses a macro instead of a
 * function.
 */
#define CLHY_OPTIONAL_HOOK(name,fn,pre,succ,order) \
        KUDA_OPTIONAL_HOOK(clhy,name,fn,pre,succ,order)

#endif /* CLHY_HOOKS_H */
