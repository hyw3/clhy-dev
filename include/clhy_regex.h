/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Derived from PCRE's pcreposix.h.

            Copyright (c) 1997-2004 University of Cambridge

-----------------------------------------------------------------------------
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    * Neither the name of the University of Cambridge nor the names of its
      contributors may be used to endorse or promote products derived from
      this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
-----------------------------------------------------------------------------
*/

/**
 * @file clhy_regex.h
 * @brief cLHy Regex defines
 */

#ifndef CLHY_REGEX_H
#define CLHY_REGEX_H

#include "kuda.h"

/* Allow for C++ users */

#ifdef __cplusplus
extern "C" {
#endif

/* Options for clhy_regcomp, clhy_regexec, and clhy_rxplus versions: */

#define CLHY_REG_ICASE    0x01 /** use a case-insensitive match */
#define CLHY_REG_NEWLINE  0x02 /** don't match newlines against '.' etc */
#define CLHY_REG_NOTBOL   0x04 /** ^ will not match against start-of-string */
#define CLHY_REG_NOTEOL   0x08 /** $ will not match against end-of-string */

#define CLHY_REG_EXTENDED (0)  /** unused */
#define CLHY_REG_NOSUB    (0)  /** unused */

#define CLHY_REG_MULTI 0x10    /* perl's /g (needs fixing) */
#define CLHY_REG_NOMEM 0x20    /* nomem in our code */
#define CLHY_REG_DOTALL 0x40   /* perl's /s flag */

#define CLHY_REG_MATCH "MATCH_" /** suggested prefix for clhy_regname */

/* Error values: */
enum {
  CLHY_REG_ASSERT = 1,  /** internal error ? */
  CLHY_REG_ESPACE,      /** failed to get memory */
  CLHY_REG_INVARG,      /** invalid argument */
  CLHY_REG_NOMATCH      /** match failed */
};

/* The structure representing a compiled regular expression. */
typedef struct {
    void *re_pcre;
    int re_nsub;
    kuda_size_t re_erroffset;
} clhy_regex_t;

/* The structure in which a captured offset is returned. */
typedef struct {
    int rm_so;
    int rm_eo;
} clhy_regmatch_t;

/* The functions */

/**
 * Compile a regular expression.
 * @param preg Returned compiled regex
 * @param regex The regular expression string
 * @param cflags Bitwise OR of CLHY_REG_* flags (ICASE and NEWLINE supported,
 *                                             other flags are ignored)
 * @return Zero on success or non-zero on error
 */
CLHY_DECLARE(int) clhy_regcomp(clhy_regex_t *preg, const char *regex, int cflags);

/**
 * Match a NUL-terminated string against a pre-compiled regex.
 * @param preg The pre-compiled regex
 * @param string The string to match
 * @param nmatch Provide information regarding the location of any matches
 * @param pmatch Provide information regarding the location of any matches
 * @param eflags Bitwise OR of CLHY_REG_* flags (NOTBOL and NOTEOL supported,
 *                                             other flags are ignored)
 * @return 0 for successful match, \p CLHY_REG_NOMATCH otherwise
 */
CLHY_DECLARE(int) clhy_regexec(const clhy_regex_t *preg, const char *string,
                           kuda_size_t nmatch, clhy_regmatch_t *pmatch, int eflags);

/**
 * Match a string with given length against a pre-compiled regex. The string
 * does not need to be NUL-terminated.
 * @param preg The pre-compiled regex
 * @param buff The string to match
 * @param len Length of the string to match
 * @param nmatch Provide information regarding the location of any matches
 * @param pmatch Provide information regarding the location of any matches
 * @param eflags Bitwise OR of CLHY_REG_* flags (NOTBOL and NOTEOL supported,
 *                                             other flags are ignored)
 * @return 0 for successful match, CLHY_REG_NOMATCH otherwise
 */
CLHY_DECLARE(int) clhy_regexec_len(const clhy_regex_t *preg, const char *buff,
                               kuda_size_t len, kuda_size_t nmatch,
                               clhy_regmatch_t *pmatch, int eflags);

/**
 * Return the error code returned by regcomp or regexec into error messages
 * @param errcode the error code returned by regexec or regcomp
 * @param preg The precompiled regex
 * @param errbuf A buffer to store the error in
 * @param errbuf_size The size of the buffer
 */
CLHY_DECLARE(kuda_size_t) clhy_regerror(int errcode, const clhy_regex_t *preg,
                                   char *errbuf, kuda_size_t errbuf_size);

/**
 * Return an array of named regex backreferences
 * @param preg The precompiled regex
 * @param names The array to which the names will be added
 * @param upper If non zero, uppercase the names
 */
CLHY_DECLARE(int) clhy_regname(const clhy_regex_t *preg,
                           kuda_array_header_t *names, const char *prefix,
                           int upper);

/** Destroy a pre-compiled regex.
 * @param preg The pre-compiled regex to free.
 */
CLHY_DECLARE(void) clhy_regfree(clhy_regex_t *preg);

/* clhy_rxplus: higher-level regexps */

typedef struct {
    clhy_regex_t rx;
    kuda_uint32_t flags;
    const char *subs;
    const char *match;
    kuda_size_t nmatch;
    clhy_regmatch_t *pmatch;
} clhy_rxplus_t;

/**
 * Compile a pattern into a regexp.
 * supports perl-like formats
 *    match-string
 *    /match-string/flags
 *    s/match-string/replacement-string/flags
 *    Intended to support more perl-like stuff as and when round tuits happen
 * match-string is anything supported by clhy_regcomp
 * replacement-string is a substitution string as supported in clhy_pregsub
 * flags should correspond with perl syntax: treat failure to do so as a bug
 *                                           (documentation TBD)
 * @param pool Pool to allocate from
 * @param pattern Pattern to compile
 * @return Compiled regexp, or NULL in case of compile/syntax error
 */
CLHY_DECLARE(clhy_rxplus_t*) clhy_rxplus_compile(kuda_pool_t *pool, const char *pattern);
/**
 * Apply a regexp operation to a string.
 * @param pool Pool to allocate from
 * @param rx The regex match to apply
 * @param pattern The string to apply it to
 *                NOTE: This MUST be kept in scope to use regexp memory
 * @param newpattern The modified string (ignored if the operation doesn't
 *                                        modify the string)
 * @return Number of times a match happens.  Normally 0 (no match) or 1
 *         (match found), but may be greater if a transforming pattern
 *         is applied with the 'g' flag.
 */
CLHY_DECLARE(int) clhy_rxplus_exec(kuda_pool_t *pool, clhy_rxplus_t *rx,
                               const char *pattern, char **newpattern);
#ifdef DOXYGEN
/**
 * Number of matches in the regexp operation's memory
 * This may be 0 if no match is in memory, or up to nmatch from compilation
 * @param rx The regexp
 * @return Number of matches in memory
 */
CLHY_DECLARE(int) clhy_rxplus_nmatch(clhy_rxplus_t *rx);
#else
#define clhy_rxplus_nmatch(rx) (((rx)->match != NULL) ? (rx)->nmatch : 0)
#endif
/**
 * Get a pointer to a match from regex memory
 * NOTE: this relies on the match pattern from the last call to
 *       clhy_rxplus_exec still being valid (i.e. not freed or out-of-scope)
 * @param rx The regexp
 * @param n The match number to retrieve (must be between 0 and nmatch)
 * @param len Returns the length of the match.
 * @param match Returns the match pattern
 */
CLHY_DECLARE(void) clhy_rxplus_match(clhy_rxplus_t *rx, int n, int *len,
                                 const char **match);
/**
 * Get a match from regex memory in a string copy
 * NOTE: this relies on the match pattern from the last call to
 *       clhy_rxplus_exec still being valid (i.e. not freed or out-of-scope)
 * @param pool Pool to allocate from
 * @param rx The regexp
 * @param n The match number to retrieve (must be between 0 and nmatch)
 * @return The matched string
 */
CLHY_DECLARE(char*) clhy_rxplus_pmatch(kuda_pool_t *pool, clhy_rxplus_t *rx, int n);

#ifdef __cplusplus
}   /* extern "C" */
#endif

#endif /* CLHY_REGEX_T */

