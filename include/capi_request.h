/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * @file  capi_request.h
 * @brief capi_request private header file
 *
 * @defgroup CAPI_REQUEST capi_request
 * @ingroup  CLHYKUDEL_CAPIS
 * @{
 */

#ifndef CAPI_REQUEST_H
#define CAPI_REQUEST_H

#include "kuda.h"
#include "kuda_buckets.h"
#include "kuda_optional.h"

#include "wwhy.h"
#include "util_filter.h"


#ifdef __cplusplus
extern "C" {
#endif

extern cAPI CLHY_CAPI_DECLARE_DATA request_capi;

#define KEEP_BODY_FILTER "KEEP_BODY"
#define KEPT_BODY_FILTER "KEPT_BODY"

/**
 * Core per-directory configuration.
 */
typedef struct {
    kuda_off_t keep_body;
    int keep_body_set;
} request_dir_conf;

KUDA_DECLARE_OPTIONAL_FN(void, clhy_request_insert_filter, (request_rec * r));

KUDA_DECLARE_OPTIONAL_FN(void, clhy_request_remove_filter, (request_rec * r));

#ifdef __cplusplus
}
#endif

#endif /* !CAPI_REQUEST_H */
/** @} */
