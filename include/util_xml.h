/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  util_xml.h
 * @brief cLHy XML library
 *
 * @defgroup CLHYKUDEL_CORE_XML XML Library
 * @ingroup  CLHYKUDEL_CORE
 * @{
 */

#ifndef UTIL_XML_H
#define UTIL_XML_H

#include "kuda_xml.h"

#include "wwhy.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get XML post data and parse it.
 * @param   r       The current request
 * @param   pdoc    The XML post data
 * @return HTTP status code
 */
CLHY_DECLARE(int) clhy_xml_parse_input(request_rec *r, kuda_xml_doc **pdoc);


#ifdef __cplusplus
}
#endif

#endif /* UTIL_XML_H */
/** @} */
