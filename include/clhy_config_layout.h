/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  clhy_config_layout.h
 * @brief cLHy Config Layout
 */

#ifndef CLHY_CONFIG_LAYOUT_H
#define CLHY_CONFIG_LAYOUT_H

/* Configured cLHy directory layout */
#define DEFAULT_PREFIX "/usr/local/clhydelman"
#define DEFAULT_EXP_EXEC_PREFIX "/usr/local/clhydelman"
#define DEFAULT_REL_EXEC_PREFIX ""
#define DEFAULT_EXP_BINDIR "/usr/local/clhydelman/bin"
#define DEFAULT_REL_BINDIR "bin"
#define DEFAULT_EXP_SBINDIR "/usr/local/clhydelman/bin"
#define DEFAULT_REL_SBINDIR "bin"
#define DEFAULT_EXP_LIBEXECDIR "/usr/local/clhydelman/cAPIs"
#define DEFAULT_REL_LIBEXECDIR "cAPIs"
#define DEFAULT_EXP_MANDIR "/usr/local/clhydelman/man"
#define DEFAULT_REL_MANDIR "man"
#define DEFAULT_EXP_SYSCONFDIR "/usr/local/clhydelman/conf"
#define DEFAULT_REL_SYSCONFDIR "conf"
#define DEFAULT_EXP_DATADIR "/usr/local/clhydelman"
#define DEFAULT_REL_DATADIR ""
#define DEFAULT_EXP_INSTALLBUILDDIR "/usr/local/clhydelman/build"
#define DEFAULT_REL_INSTALLBUILDDIR "build"
#define DEFAULT_EXP_ERRORDIR "/usr/local/clhydelman/error"
#define DEFAULT_REL_ERRORDIR "error"
#define DEFAULT_EXP_ICONSDIR "/usr/local/clhydelman/icons"
#define DEFAULT_REL_ICONSDIR "icons"
#define DEFAULT_EXP_HTDOCSDIR "/usr/local/clhydelman/htdocs"
#define DEFAULT_REL_HTDOCSDIR "htdocs"
#define DEFAULT_EXP_MANUALDIR "/usr/local/clhydelman/manual"
#define DEFAULT_REL_MANUALDIR "manual"
#define DEFAULT_EXP_CGIDIR "/usr/local/clhydelman/cgi-bin"
#define DEFAULT_REL_CGIDIR "cgi-bin"
#define DEFAULT_EXP_INCLUDEDIR "/usr/local/clhydelman/include"
#define DEFAULT_REL_INCLUDEDIR "include"
#define DEFAULT_EXP_LOCALSTATEDIR "/usr/local/clhydelman"
#define DEFAULT_REL_LOCALSTATEDIR ""
#define DEFAULT_EXP_RUNTIMEDIR "/usr/local/clhydelman/logs"
#define DEFAULT_REL_RUNTIMEDIR "logs"
#define DEFAULT_EXP_LOGFILEDIR "/usr/local/clhydelman/logs"
#define DEFAULT_REL_LOGFILEDIR "logs"
#define DEFAULT_EXP_PROXYCACHEDIR "/usr/local/clhydelman/proxy"
#define DEFAULT_REL_PROXYCACHEDIR "proxy"

#endif /* CLHY_CONFIG_LAYOUT_H */
