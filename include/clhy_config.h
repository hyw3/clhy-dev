/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file clhy_config.h
 * @brief Symbol export macros and hook functions
 */

#ifndef CLHY_CONFIG_H
#define CLHY_CONFIG_H

#include "clhy_hooks.h"

/* Although this file doesn't declare any hooks, declare the exports group here */
/**
 * @defgroup exports cLHy exports
 * @ingroup  CLHYKUDEL_CORE
 */

#ifdef DOXYGEN
/* define these just so doxygen documents them */

/**
 * CLHY_DECLARE_STATIC is defined when including cLHy's Core headers,
 * to provide static linkage when the dynamic library may be unavailable.
 *
 * @see CLHY_DECLARE_EXPORT
 *
 * CLHY_DECLARE_STATIC and CLHY_DECLARE_EXPORT are left undefined when
 * including cLHy's Core headers, to import and link the symbols from the
 * dynamic cLHy Core library and assure appropriate indirection and calling
 * conventions at compile time.
 */
# define CLHY_DECLARE_STATIC
/**
 * CLHY_DECLARE_EXPORT is defined when building the cLHy Core dynamic
 * library, so that all public symbols are exported.
 *
 * @see CLHY_DECLARE_STATIC
 */
# define CLHY_DECLARE_EXPORT

#endif /* def DOXYGEN */

#if !defined(WIN32)
/**
 * cLHy Core dso functions are declared with CLHY_DECLARE(), so they may
 * use the most appropriate calling convention.  Hook functions and other
 * Core functions with variable arguments must use CLHY_DECLARE_NONSTD().
 * @code
 * CLHY_DECLARE(rettype) clhy_func(args)
 * @endcode
 */
#define CLHY_DECLARE(type)            type

/**
 * cLHy Core dso variable argument and hook functions are declared with
 * CLHY_DECLARE_NONSTD(), as they must use the C language calling convention.
 * @see CLHY_DECLARE
 * @code
 * CLHY_DECLARE_NONSTD(rettype) clhy_func(args [...])
 * @endcode
 */
#define CLHY_DECLARE_NONSTD(type)     type

/**
 * cLHy Core dso variables are declared with CLHY_CAPI_DECLARE_DATA.
 * This assures the appropriate indirection is invoked at compile time.
 *
 * @note CLHY_DECLARE_DATA extern type kuda_variable; syntax is required for
 * declarations within headers to properly import the variable.
 * @code
 * CLHY_DECLARE_DATA type kuda_variable
 * @endcode
 */
#define CLHY_DECLARE_DATA

#elif defined(CLHY_DECLARE_STATIC)
#define CLHY_DECLARE(type)            type __stdcall
#define CLHY_DECLARE_NONSTD(type)     type
#define CLHY_DECLARE_DATA
#elif defined(CLHY_DECLARE_EXPORT)
#define CLHY_DECLARE(type)            __declspec(dllexport) type __stdcall
#define CLHY_DECLARE_NONSTD(type)     __declspec(dllexport) type
#define CLHY_DECLARE_DATA             __declspec(dllexport)
#else
#define CLHY_DECLARE(type)            __declspec(dllimport) type __stdcall
#define CLHY_DECLARE_NONSTD(type)     __declspec(dllimport) type
#define CLHY_DECLARE_DATA             __declspec(dllimport)
#endif

#if !defined(WIN32) || defined(CLHY_CAPI_DECLARE_STATIC)
/**
 * Declare a dso cAPI's exported cAPI structure as CLHY_CAPI_DECLARE_DATA.
 *
 * Unless CLHY_CAPI_DECLARE_STATIC is defined at compile time, symbols
 * declared with CLHY_CAPI_DECLARE_DATA are always exported.
 * @code
 * cAPI CLHY_CAPI_DECLARE_DATA capi_tag
 * @endcode
 */
#if defined(WIN32)
#define CLHY_CAPI_DECLARE(type)            type __stdcall
#else
#define CLHY_CAPI_DECLARE(type)            type
#endif
#define CLHY_CAPI_DECLARE_NONSTD(type)     type
#define CLHY_CAPI_DECLARE_DATA
#else
/**
 * CLHY_CAPI_DECLARE_EXPORT is a no-op.  Unless contradicted by the
 * CLHY_CAPI_DECLARE_STATIC compile-time symbol, it is assumed and defined.
 *
 * The old SHARED_CAPI compile-time symbol is now the default behavior,
 * so it is no longer referenced anywhere with cLHy 1.0.
 */
#define CLHY_CAPI_DECLARE_EXPORT
#define CLHY_CAPI_DECLARE(type)          __declspec(dllexport) type __stdcall
#define CLHY_CAPI_DECLARE_NONSTD(type)   __declspec(dllexport) type
#define CLHY_CAPI_DECLARE_DATA           __declspec(dllexport)
#endif

#include "platform.h"
#if (!defined(WIN32) && !defined(NETWARE)) || defined(__MINGW32__)
#include "clhy_config_auto.h"
#endif
#include "clhy_config_layout.h"

/* Where the main/parent process's pid is logged */
#ifndef DEFAULT_PIDLOG
#define DEFAULT_PIDLOG DEFAULT_REL_RUNTIMEDIR "/wwhy.pid"
#endif

#if defined(NETWARE)
#define CLHY_NONBLOCK_WHEN_MULTI_LISTEN 1
#endif

#if defined(CLHY_ENABLE_DTRACE) && HAVE_SYS_SDT_H
#include <sys/sdt.h>
#else
#undef _DTRACE_VERSION
#endif

#ifdef _DTRACE_VERSION
#include "clhy_probes.h"
#else
#include "clhy_noprobes.h"
#endif

/* If KUDA has OTHER_CHILD logic, use reliable piped logs. */
#if KUDA_HAS_OTHER_CHILD
#define CLHY_HAVE_RELIABLE_PIPED_LOGS TRUE
#endif

#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#define CLHY_HAVE_C99
#endif

/* Presume that the compiler supports C99-style designated
 * initializers if using GCC (but not G++), or for any other compiler
 * which claims C99 support. */
#if (defined(__GNUC__) && !defined(__cplusplus)) || defined(CLHY_HAVE_C99)
#define CLHY_HAVE_DESIGNATED_INITIALIZER
#endif

#ifndef __has_attribute         /* check for supported attributes on clang */
#define __has_attribute(x) 0
#endif
#if (defined(__GNUC__) && __GNUC__ >= 4) || __has_attribute(sentinel)
#define CLHY_FN_ATTR_SENTINEL __attribute__((sentinel))
#else
#define CLHY_FN_ATTR_SENTINEL
#endif

#if ( defined(__GNUC__) &&                                        \
      (__GNUC__ >= 4 || ( __GNUC__ == 3 && __GNUC_MINOR__ >= 4))) \
    || __has_attribute(warn_unused_result)
#define CLHY_FN_ATTR_WARN_UNUSED_RESULT   __attribute__((warn_unused_result))
#else
#define CLHY_FN_ATTR_WARN_UNUSED_RESULT
#endif

#if ( defined(__GNUC__) &&                                        \
      (__GNUC__ >= 4 && __GNUC_MINOR__ >= 3))                     \
    || __has_attribute(alloc_size)
#define CLHY_FN_ATTR_ALLOC_SIZE(x)     __attribute__((alloc_size(x)))
#define CLHY_FN_ATTR_ALLOC_SIZE2(x,y)  __attribute__((alloc_size(x,y)))
#else
#define CLHY_FN_ATTR_ALLOC_SIZE(x)
#define CLHY_FN_ATTR_ALLOC_SIZE2(x,y)
#endif

#endif /* CLHY_CONFIG_H */
