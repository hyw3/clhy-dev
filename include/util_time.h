/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  util_time.h
 * @brief cLHy date-time handling functions
 *
 * @defgroup CLHYKUDEL_CORE_TIME Date-time handling functions
 * @ingroup  CLHYKUDEL_CORE
 * @{
 */

#ifndef CLHYKUDEL_UTIL_TIME_H
#define CLHYKUDEL_UTIL_TIME_H

#include "kuda.h"
#include "kuda_time.h"
#include "wwhy.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Maximum delta from the current time, in seconds, for a past time
 * to qualify as "recent" for use in the clhy_explode_recent_*() functions:
 * (Must be a power of two minus one!)
 */
#define CLHY_TIME_RECENT_THRESHOLD 15

/* Options for clhy_recent_ctime_ex */
/* No extension */
#define CLHY_CTIME_OPTION_NONE    0x0
/* Add sub second timestamps with micro second resolution */
#define CLHY_CTIME_OPTION_USEC    0x1
/* Use more compact ISO 8601 format */
#define CLHY_CTIME_OPTION_COMPACT 0x2


/**
 * convert a recent time to its human readable components in local timezone
 * @param tm the exploded time
 * @param t the time to explode: MUST be within the last
 *          CLHY_TIME_RECENT_THRESHOLD seconds
 * @note This is a faster alternative to kuda_time_exp_lt that uses
 *       a cache of pre-exploded time structures.  It is useful for things
 *       that need to explode the current time multiple times per second,
 *       like loggers.
 * @return KUDA_SUCCESS iff successful
 */
CLHY_DECLARE(kuda_status_t) clhy_explode_recent_localtime(kuda_time_exp_t *tm,
                                                     kuda_time_t t);

/**
 * convert a recent time to its human readable components in GMT timezone
 * @param tm the exploded time
 * @param t the time to explode: MUST be within the last
 *          CLHY_TIME_RECENT_THRESHOLD seconds
 * @note This is a faster alternative to kuda_time_exp_gmt that uses
 *       a cache of pre-exploded time structures.  It is useful for things
 *       that need to explode the current time multiple times per second,
 *       like loggers.
 * @return KUDA_SUCCESS iff successful
 */
CLHY_DECLARE(kuda_status_t) clhy_explode_recent_gmt(kuda_time_exp_t *tm,
                                               kuda_time_t t);


/**
 * format a recent timestamp in the ctime() format.
 * @param date_str String to write to.
 * @param t the time to convert
 * @note Consider using clhy_recent_ctime_ex instead.
 * @return KUDA_SUCCESS iff successful
 */
CLHY_DECLARE(kuda_status_t) clhy_recent_ctime(char *date_str, kuda_time_t t);


/**
 * format a recent timestamp in an extended ctime() format.
 * @param date_str String to write to.
 * @param t the time to convert
 * @param option Additional formatting options (CLHY_CTIME_OPTION_*).
 * @param len Pointer to an int containing the length of the provided buffer.
 *        On successful return it contains the number of bytes written to the
 *        buffer.
 * @return KUDA_SUCCESS iff successful, KUDA_ENOMEM if buffer was to short.
 */
CLHY_DECLARE(kuda_status_t) clhy_recent_ctime_ex(char *date_str, kuda_time_t t,
                                            int option, int *len);


/**
 * format a recent timestamp in the RFC822 format
 * @param date_str String to write to (must have length >= KUDA_RFC822_DATE_LEN)
 * @param t the time to convert
 */
CLHY_DECLARE(kuda_status_t) clhy_recent_rfc822_date(char *date_str, kuda_time_t t);

#ifdef __cplusplus
}
#endif

#endif  /* !CLHYKUDEL_UTIL_TIME_H */
/** @} */
