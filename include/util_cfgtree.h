/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  util_cfgtree.h
 * @brief Config Tree Package
 *
 * @defgroup CLHYKUDEL_CORE_CONFIG_TREE Config Tree Package
 * @ingroup  CLHYKUDEL_CORE_CONFIG
 * @{
 */

#ifndef CLHY_CONFTREE_H
#define CLHY_CONFTREE_H

#include "clhy_config.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct clhy_directive_t clhy_directive_t;

/**
 * @brief Structure used to build the config tree.
 *
 * The config tree only stores
 * the directives that will be active in the running server.  Directives
 * that contain other directions, such as &lt;Directory ...&gt; cause a sub-level
 * to be created, where the included directives are stored.  The closing
 * directive (&lt;/Directory&gt;) is not stored in the tree.
 */
struct clhy_directive_t {
    /** The current directive */
    const char *directive;
    /** The arguments for the current directive, stored as a space
     *  separated list */
    const char *args;
    /** The next directive node in the tree */
    struct clhy_directive_t *next;
    /** The first child node of this directive */
    struct clhy_directive_t *first_child;
    /** The parent node of this directive */
    struct clhy_directive_t *parent;

    /** directive's cAPI can store add'l data here */
    void *data;

    /* ### these may go away in the future, but are needed for now */
    /** The name of the file this directive was found in */
    const char *filename;
    /** The line number the directive was on */
    int line_num;

    /** A short-cut towards the last directive node in the tree.
     *  The value may not always be up-to-date but it always points to
     *  somewhere in the tree, nearer to the tail.
     *  This value is only set in the first node
     */
    struct clhy_directive_t *last;
};

/**
 * The root of the configuration tree
 */
CLHY_DECLARE_DATA extern clhy_directive_t *clhy_conftree;

/**
 * Add a node to the configuration tree.
 * @param parent The current parent node.  If the added node is a first_child,
                 then this is changed to the current node
 * @param current The current node
 * @param toadd The node to add to the tree
 * @param child Is the node to add a child node
 * @return the added node
 */
clhy_directive_t *clhy_add_node(clhy_directive_t **parent, clhy_directive_t *current,
                            clhy_directive_t *toadd, int child);

#ifdef __cplusplus
}
#endif

#endif
/** @} */
