/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  http_main.h
 * @brief Command line options
 *
 * @defgroup CLHYKUDEL_CORE_MAIN Command line options
 * @ingroup  CLHYKUDEL_CORE
 * @{
 */

#ifndef CLHYKUDEL_HTTP_MAIN_H
#define CLHYKUDEL_HTTP_MAIN_H

#include "wwhy.h"
#include "kuda_optional.h"

/** CLHY_SERVER_BASEARGS is the command argument list parsed by http_main.c
 * in kuda_getopt() format.  Use this for default'ing args that the cLMP
 * can safely ignore and pass on from its rewrite_args() handler.
 */
#define CLHY_SERVER_BASEARGS "C:c:D:d:E:e:f:vVlLtTSMh?X"

#ifdef __cplusplus
extern "C" {
#endif

/** The name of the cLHy executable */
CLHY_DECLARE_DATA extern const char *clhy_server_argv0;
/** The global server's ServerRoot */
CLHY_DECLARE_DATA extern const char *clhy_server_root;
/** The global server's DefaultRuntimeDir
 * This is not usable directly in the general case; use
 * clhy_runtime_dir_relative() instead.
 */
CLHY_DECLARE_DATA extern const char *clhy_runtime_dir;
/** The global server's server_rec */
CLHY_DECLARE_DATA extern server_rec *clhy_server_conf;
/** global pool, for access prior to creation of server_rec */
CLHY_DECLARE_DATA extern kuda_pool_t *clhy_pglobal;
/** state of the server (startup, exiting, ...) */
CLHY_DECLARE_DATA extern int clhy_main_state;
/** run mode (normal, config test, config dump, ...) */
CLHY_DECLARE_DATA extern int clhy_run_mode;
/** run mode (normal, config test, config dump, ...) */
CLHY_DECLARE_DATA extern int clhy_config_generation;

/* for -C, -c and -D switches */
/** An array of all -C directives.  These are processed before the server's
 *  config file */
CLHY_DECLARE_DATA extern kuda_array_header_t *clhy_server_pre_read_config;
/** An array of all -c directives.  These are processed after the server's
 *  config file */
CLHY_DECLARE_DATA extern kuda_array_header_t *clhy_server_post_read_config;
/** An array of all -D defines on the command line.  This allows people to
 *  effect the server based on command line options */
CLHY_DECLARE_DATA extern kuda_array_header_t *clhy_server_config_defines;
/** Available integer for using the -T switch */
CLHY_DECLARE_DATA extern int clhy_document_root_check;

/**
 * An optional function to send signal to server on presence of '-k'
 * command line argument.
 * @param status The exit status after sending signal
 * @param pool Memory pool to allocate from
 */
KUDA_DECLARE_OPTIONAL_FN(int, clhy_signal_server, (int *status, kuda_pool_t *pool));

#ifdef __cplusplus
}
#endif

#endif  /* !CLHYKUDEL_HTTP_MAIN_H */
/** @} */
