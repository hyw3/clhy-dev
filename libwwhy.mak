# Microsoft Developer Studio Generated NMAKE File, Based on libwwhy.dsp
!IF "$(CFG)" == ""
CFG=libwwhy - Win32 Release
!MESSAGE No configuration specified. Defaulting to libwwhy - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "libwwhy - Win32 Release" && "$(CFG)" != "libwwhy - Win32 Debug" && "$(CFG)" != "libwwhy - Win32 Lexical"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libwwhy.mak" CFG="libwwhy - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libwwhy - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libwwhy - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libwwhy - Win32 Lexical" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "libwwhy - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : ".\include\capi_dav.h" "$(OUTDIR)\libwwhy.dll" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "gen_test_char - Win32 Release" "libkudadelman - Win32 Release" "libkudaiconv - Win32 Release" "libkuda - Win32 Release" ".\include\capi_dav.h" "$(OUTDIR)\libwwhy.dll" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudaiconv - Win32 ReleaseCLEAN" "libkudadelman - Win32 ReleaseCLEAN" "gen_test_char - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\clhy_regkey.obj"
	-@erase "$(INTDIR)\byterange_filter.obj"
	-@erase "$(INTDIR)\child.obj"
	-@erase "$(INTDIR)\chunk_filter.obj"
	-@erase "$(INTDIR)\config.obj"
	-@erase "$(INTDIR)\connection.obj"
	-@erase "$(INTDIR)\core.obj"
	-@erase "$(INTDIR)\core_filters.obj"
	-@erase "$(INTDIR)\eoc_bucket.obj"
	-@erase "$(INTDIR)\eor_bucket.obj"
	-@erase "$(INTDIR)\error_bucket.obj"
	-@erase "$(INTDIR)\http_core.obj"
	-@erase "$(INTDIR)\http_etag.obj"
	-@erase "$(INTDIR)\http_filters.obj"
	-@erase "$(INTDIR)\http_protocol.obj"
	-@erase "$(INTDIR)\http_request.obj"
	-@erase "$(INTDIR)\libwwhy.res"
	-@erase "$(INTDIR)\libwwhy_cl.idb"
	-@erase "$(INTDIR)\libwwhy_cl.pdb"
	-@erase "$(INTDIR)\listen.obj"
	-@erase "$(INTDIR)\log.obj"
	-@erase "$(INTDIR)\capi_so.obj"
	-@erase "$(INTDIR)\capi_win32.obj"
	-@erase "$(INTDIR)\cAPIs.obj"
	-@erase "$(INTDIR)\core_common.obj"
	-@erase "$(INTDIR)\core_winnt.obj"
	-@erase "$(INTDIR)\nt_eventlog.obj"
	-@erase "$(INTDIR)\protocol.obj"
	-@erase "$(INTDIR)\provider.obj"
	-@erase "$(INTDIR)\request.obj"
	-@erase "$(INTDIR)\scoreboard.obj"
	-@erase "$(INTDIR)\service.obj"
	-@erase "$(INTDIR)\util.obj"
	-@erase "$(INTDIR)\util_cfgtree.obj"
	-@erase "$(INTDIR)\util_cookies.obj"
	-@erase "$(INTDIR)\util_expr_eval.obj"
	-@erase "$(INTDIR)\util_expr_parse.obj"
	-@erase "$(INTDIR)\util_expr_scan.obj"
	-@erase "$(INTDIR)\util_fcgi.obj"
	-@erase "$(INTDIR)\util_filter.obj"
	-@erase "$(INTDIR)\util_md5.obj"
	-@erase "$(INTDIR)\util_mutex.obj"
	-@erase "$(INTDIR)\util_pcre.obj"
	-@erase "$(INTDIR)\util_regex.obj"
	-@erase "$(INTDIR)\util_script.obj"
	-@erase "$(INTDIR)\util_time.obj"
	-@erase "$(INTDIR)\util_win32.obj"
	-@erase "$(INTDIR)\util_xml.obj"
	-@erase "$(INTDIR)\vhost.obj"
	-@erase "$(OUTDIR)\libwwhy.dll"
	-@erase "$(OUTDIR)\libwwhy.exp"
	-@erase "$(OUTDIR)\libwwhy.lib"
	-@erase "$(OUTDIR)\libwwhy.pdb"
	-@erase ".\include\capi_dav.h"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /I "./kudelrunsrc/pcre" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\libwwhy_cl" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\libwwhy.res" /i "./include" /i "./kudelrunsrc/kuda/include" /d "NDEBUG" /d BIN_NAME="libwwhy.dll" /d LONG_NAME="cLHy HTTP Server Core" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libwwhy.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=pcre.lib kernel32.lib user32.lib advapi32.lib ws2_32.lib mswsock.lib "Release\buildmark.obj" /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\libwwhy.pdb" /debug /out:"$(OUTDIR)\libwwhy.dll" /implib:"$(OUTDIR)\libwwhy.lib" /libpath:"./kudelrunsrc/pcre" /base:@"platforms\win32\BaseAddr.ref",libwwhy.dll /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\byterange_filter.obj" \
	"$(INTDIR)\chunk_filter.obj" \
	"$(INTDIR)\config.obj" \
	"$(INTDIR)\connection.obj" \
	"$(INTDIR)\core.obj" \
	"$(INTDIR)\core_filters.obj" \
	"$(INTDIR)\http_core.obj" \
	"$(INTDIR)\http_etag.obj" \
	"$(INTDIR)\http_filters.obj" \
	"$(INTDIR)\http_protocol.obj" \
	"$(INTDIR)\http_request.obj" \
	"$(INTDIR)\log.obj" \
	"$(INTDIR)\protocol.obj" \
	"$(INTDIR)\request.obj" \
	"$(INTDIR)\vhost.obj" \
	"$(INTDIR)\capi_so.obj" \
	"$(INTDIR)\capi_win32.obj" \
	"$(INTDIR)\cAPIs.obj" \
	"$(INTDIR)\eoc_bucket.obj" \
	"$(INTDIR)\eor_bucket.obj" \
	"$(INTDIR)\error_bucket.obj" \
	"$(INTDIR)\util.obj" \
	"$(INTDIR)\util_cfgtree.obj" \
	"$(INTDIR)\util_cookies.obj" \
	"$(INTDIR)\util_expr_eval.obj" \
	"$(INTDIR)\util_expr_scan.obj" \
	"$(INTDIR)\util_expr_parse.obj" \
	"$(INTDIR)\util_fcgi.obj" \
	"$(INTDIR)\util_filter.obj" \
	"$(INTDIR)\util_md5.obj" \
	"$(INTDIR)\util_mutex.obj" \
	"$(INTDIR)\util_pcre.obj" \
	"$(INTDIR)\util_regex.obj" \
	"$(INTDIR)\util_script.obj" \
	"$(INTDIR)\util_time.obj" \
	"$(INTDIR)\util_win32.obj" \
	"$(INTDIR)\util_xml.obj" \
	"$(INTDIR)\clhy_regkey.obj" \
	"$(INTDIR)\child.obj" \
	"$(INTDIR)\listen.obj" \
	"$(INTDIR)\core_common.obj" \
	"$(INTDIR)\core_winnt.obj" \
	"$(INTDIR)\nt_eventlog.obj" \
	"$(INTDIR)\provider.obj" \
	"$(INTDIR)\scoreboard.obj" \
	"$(INTDIR)\service.obj" \
	"$(INTDIR)\libwwhy.res" \
	".\kudelrunsrc\kuda\Release\libkuda-1.lib" \
	".\kudelrunsrc\kuda-iconv\Release\libkudaiconv-1.lib" \
	".\kudelrunsrc\kuda-delman\Release\libkudadelman-1.lib"

"$(OUTDIR)\libwwhy.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
   cl.exe /nologo /MD /W3 /O2 /Oy- /Zi /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /I "./kudelrunsrc/pcre" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /Fd"Release\libwwhy" /FD /c server\buildmark.c /Fo"Release\buildmark.obj"
	 $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\libwwhy.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\libwwhy.dll"
   if exist .\Release\libwwhy.dll.manifest mt.exe -manifest .\Release\libwwhy.dll.manifest -outputresource:.\Release\libwwhy.dll;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : ".\server\test_char.h" ".\include\capi_so.h" ".\include\capi_proxy.h" ".\include\capi_include.h" ".\include\capi_dav.h" ".\include\capi_cgi.h" ".\include\clhy_config_layout.h" "$(OUTDIR)\libwwhy.dll" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "gen_test_char - Win32 Debug" "libkudadelman - Win32 Debug" "libkudaiconv - Win32 Debug" "libkuda - Win32 Debug" ".\server\test_char.h" ".\include\capi_so.h" ".\include\capi_proxy.h" ".\include\capi_include.h" ".\include\capi_dav.h" ".\include\capi_cgi.h" ".\include\clhy_config_layout.h" "$(OUTDIR)\libwwhy.dll" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudaiconv - Win32 DebugCLEAN" "libkudadelman - Win32 DebugCLEAN" "gen_test_char - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\clhy_regkey.obj"
	-@erase "$(INTDIR)\byterange_filter.obj"
	-@erase "$(INTDIR)\child.obj"
	-@erase "$(INTDIR)\chunk_filter.obj"
	-@erase "$(INTDIR)\config.obj"
	-@erase "$(INTDIR)\connection.obj"
	-@erase "$(INTDIR)\core.obj"
	-@erase "$(INTDIR)\core_filters.obj"
	-@erase "$(INTDIR)\eoc_bucket.obj"
	-@erase "$(INTDIR)\eor_bucket.obj"
	-@erase "$(INTDIR)\error_bucket.obj"
	-@erase "$(INTDIR)\http_core.obj"
	-@erase "$(INTDIR)\http_etag.obj"
	-@erase "$(INTDIR)\http_filters.obj"
	-@erase "$(INTDIR)\http_protocol.obj"
	-@erase "$(INTDIR)\http_request.obj"
	-@erase "$(INTDIR)\libwwhy.res"
	-@erase "$(INTDIR)\libwwhy_cl.idb"
	-@erase "$(INTDIR)\libwwhy_cl.pdb"
	-@erase "$(INTDIR)\listen.obj"
	-@erase "$(INTDIR)\log.obj"
	-@erase "$(INTDIR)\capi_so.obj"
	-@erase "$(INTDIR)\capi_win32.obj"
	-@erase "$(INTDIR)\cAPIs.obj"
	-@erase "$(INTDIR)\core_common.obj"
	-@erase "$(INTDIR)\core_winnt.obj"
	-@erase "$(INTDIR)\nt_eventlog.obj"
	-@erase "$(INTDIR)\protocol.obj"
	-@erase "$(INTDIR)\provider.obj"
	-@erase "$(INTDIR)\request.obj"
	-@erase "$(INTDIR)\scoreboard.obj"
	-@erase "$(INTDIR)\service.obj"
	-@erase "$(INTDIR)\util.obj"
	-@erase "$(INTDIR)\util_cfgtree.obj"
	-@erase "$(INTDIR)\util_cookies.obj"
	-@erase "$(INTDIR)\util_expr_eval.obj"
	-@erase "$(INTDIR)\util_expr_parse.obj"
	-@erase "$(INTDIR)\util_expr_scan.obj"
	-@erase "$(INTDIR)\util_fcgi.obj"
	-@erase "$(INTDIR)\util_filter.obj"
	-@erase "$(INTDIR)\util_md5.obj"
	-@erase "$(INTDIR)\util_mutex.obj"
	-@erase "$(INTDIR)\util_pcre.obj"
	-@erase "$(INTDIR)\util_regex.obj"
	-@erase "$(INTDIR)\util_script.obj"
	-@erase "$(INTDIR)\util_time.obj"
	-@erase "$(INTDIR)\util_win32.obj"
	-@erase "$(INTDIR)\util_xml.obj"
	-@erase "$(INTDIR)\vhost.obj"
	-@erase "$(OUTDIR)\libwwhy.dll"
	-@erase "$(OUTDIR)\libwwhy.exp"
	-@erase "$(OUTDIR)\libwwhy.lib"
	-@erase "$(OUTDIR)\libwwhy.pdb"
	-@erase ".\include\clhy_config_layout.h"
	-@erase ".\include\capi_cgi.h"
	-@erase ".\include\capi_dav.h"
	-@erase ".\include\capi_include.h"
	-@erase ".\include\capi_proxy.h"
	-@erase ".\include\capi_so.h"
	-@erase ".\server\test_char.h"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /I "./kudelrunsrc/pcre" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\libwwhy_cl" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\libwwhy.res" /i "./include" /i "./kudelrunsrc/kuda/include" /d "_DEBUG" /d BIN_NAME="libwwhy.dll" /d LONG_NAME="cLHy HTTP Server Core" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libwwhy.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=pcred.lib kernel32.lib user32.lib advapi32.lib ws2_32.lib mswsock.lib "Debug\buildmark.obj" /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\libwwhy.pdb" /debug /out:"$(OUTDIR)\libwwhy.dll" /implib:"$(OUTDIR)\libwwhy.lib" /libpath:"./kudelrunsrc/pcre" /base:@"platforms\win32\BaseAddr.ref",libwwhy.dll 
LINK32_OBJS= \
	"$(INTDIR)\byterange_filter.obj" \
	"$(INTDIR)\chunk_filter.obj" \
	"$(INTDIR)\config.obj" \
	"$(INTDIR)\connection.obj" \
	"$(INTDIR)\core.obj" \
	"$(INTDIR)\core_filters.obj" \
	"$(INTDIR)\http_core.obj" \
	"$(INTDIR)\http_etag.obj" \
	"$(INTDIR)\http_filters.obj" \
	"$(INTDIR)\http_protocol.obj" \
	"$(INTDIR)\http_request.obj" \
	"$(INTDIR)\log.obj" \
	"$(INTDIR)\protocol.obj" \
	"$(INTDIR)\request.obj" \
	"$(INTDIR)\vhost.obj" \
	"$(INTDIR)\capi_so.obj" \
	"$(INTDIR)\capi_win32.obj" \
	"$(INTDIR)\cAPIs.obj" \
	"$(INTDIR)\eoc_bucket.obj" \
	"$(INTDIR)\eor_bucket.obj" \
	"$(INTDIR)\error_bucket.obj" \
	"$(INTDIR)\util.obj" \
	"$(INTDIR)\util_cfgtree.obj" \
	"$(INTDIR)\util_cookies.obj" \
	"$(INTDIR)\util_expr_eval.obj" \
	"$(INTDIR)\util_expr_scan.obj" \
	"$(INTDIR)\util_expr_parse.obj" \
	"$(INTDIR)\util_fcgi.obj" \
	"$(INTDIR)\util_filter.obj" \
	"$(INTDIR)\util_md5.obj" \
	"$(INTDIR)\util_mutex.obj" \
	"$(INTDIR)\util_pcre.obj" \
	"$(INTDIR)\util_regex.obj" \
	"$(INTDIR)\util_script.obj" \
	"$(INTDIR)\util_time.obj" \
	"$(INTDIR)\util_win32.obj" \
	"$(INTDIR)\util_xml.obj" \
	"$(INTDIR)\clhy_regkey.obj" \
	"$(INTDIR)\child.obj" \
	"$(INTDIR)\listen.obj" \
	"$(INTDIR)\core_common.obj" \
	"$(INTDIR)\core_winnt.obj" \
	"$(INTDIR)\nt_eventlog.obj" \
	"$(INTDIR)\provider.obj" \
	"$(INTDIR)\scoreboard.obj" \
	"$(INTDIR)\service.obj" \
	"$(INTDIR)\libwwhy.res" \
	".\kudelrunsrc\kuda\Debug\libkuda-1.lib" \
	".\kudelrunsrc\kuda-iconv\Debug\libkudaiconv-1.lib" \
	".\kudelrunsrc\kuda-delman\Debug\libkudadelman-1.lib"

"$(OUTDIR)\libwwhy.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
   cl.exe /nologo /MDd /W3 /EHsc /Zi /Od /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /I "./kudelrunsrc/pcre" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /Fd"Debug\libwwhy" /FD /c server\buildmark.c /Fo"Debug\buildmark.obj"
	 $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\libwwhy.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\libwwhy.dll"
   if exist .\Debug\libwwhy.dll.manifest mt.exe -manifest .\Debug\libwwhy.dll.manifest -outputresource:.\Debug\libwwhy.dll;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : ".\server\util_expr_parse.h" ".\server\util_expr_parse.c" ".\server\test_char.h" "$(OUTDIR)\libwwhy.dll" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : ".\server\util_expr_parse.h" ".\server\util_expr_parse.c" ".\server\test_char.h" "$(OUTDIR)\libwwhy.dll" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\clhy_regkey.obj"
	-@erase "$(INTDIR)\byterange_filter.obj"
	-@erase "$(INTDIR)\child.obj"
	-@erase "$(INTDIR)\chunk_filter.obj"
	-@erase "$(INTDIR)\config.obj"
	-@erase "$(INTDIR)\connection.obj"
	-@erase "$(INTDIR)\core.obj"
	-@erase "$(INTDIR)\core_filters.obj"
	-@erase "$(INTDIR)\eoc_bucket.obj"
	-@erase "$(INTDIR)\eor_bucket.obj"
	-@erase "$(INTDIR)\error_bucket.obj"
	-@erase "$(INTDIR)\http_core.obj"
	-@erase "$(INTDIR)\http_etag.obj"
	-@erase "$(INTDIR)\http_filters.obj"
	-@erase "$(INTDIR)\http_protocol.obj"
	-@erase "$(INTDIR)\http_request.obj"
	-@erase "$(INTDIR)\libwwhy.res"
	-@erase "$(INTDIR)\libwwhy_cl.idb"
	-@erase "$(INTDIR)\libwwhy_cl.pdb"
	-@erase "$(INTDIR)\listen.obj"
	-@erase "$(INTDIR)\log.obj"
	-@erase "$(INTDIR)\capi_so.obj"
	-@erase "$(INTDIR)\capi_win32.obj"
	-@erase "$(INTDIR)\cAPIs.obj"
	-@erase "$(INTDIR)\core_common.obj"
	-@erase "$(INTDIR)\core_winnt.obj"
	-@erase "$(INTDIR)\nt_eventlog.obj"
	-@erase "$(INTDIR)\protocol.obj"
	-@erase "$(INTDIR)\provider.obj"
	-@erase "$(INTDIR)\request.obj"
	-@erase "$(INTDIR)\scoreboard.obj"
	-@erase "$(INTDIR)\service.obj"
	-@erase "$(INTDIR)\util.obj"
	-@erase "$(INTDIR)\util_cfgtree.obj"
	-@erase "$(INTDIR)\util_cookies.obj"
	-@erase "$(INTDIR)\util_expr_eval.obj"
	-@erase "$(INTDIR)\util_expr_parse.obj"
	-@erase "$(INTDIR)\util_expr_scan.obj"
	-@erase "$(INTDIR)\util_fcgi.obj"
	-@erase "$(INTDIR)\util_filter.obj"
	-@erase "$(INTDIR)\util_md5.obj"
	-@erase "$(INTDIR)\util_mutex.obj"
	-@erase "$(INTDIR)\util_pcre.obj"
	-@erase "$(INTDIR)\util_regex.obj"
	-@erase "$(INTDIR)\util_script.obj"
	-@erase "$(INTDIR)\util_time.obj"
	-@erase "$(INTDIR)\util_win32.obj"
	-@erase "$(INTDIR)\util_xml.obj"
	-@erase "$(INTDIR)\vhost.obj"
	-@erase "$(OUTDIR)\libwwhy.dll"
	-@erase "$(OUTDIR)\libwwhy.exp"
	-@erase "$(OUTDIR)\libwwhy.lib"
	-@erase "$(OUTDIR)\libwwhy.pdb"
	-@erase ".\server\test_char.h"
	-@erase ".\server\util_expr_parse.c"
	-@erase ".\server\util_expr_parse.h"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /I "./kudelrunsrc/pcre" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\libwwhy_cl" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\libwwhy.res" /i "./include" /i "./kudelrunsrc/kuda/include" /d "NDEBUG" /d BIN_NAME="libwwhy.dll" /d LONG_NAME="cLHy HTTP Server Core" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\libwwhy.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=pcre.lib kernel32.lib user32.lib advapi32.lib ws2_32.lib mswsock.lib "Release\buildmark.obj" /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\libwwhy.pdb" /debug /out:"$(OUTDIR)\libwwhy.dll" /implib:"$(OUTDIR)\libwwhy.lib" /libpath:"./kudelrunsrc/pcre" /base:@"platforms\win32\BaseAddr.ref",libwwhy.dll /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\byterange_filter.obj" \
	"$(INTDIR)\chunk_filter.obj" \
	"$(INTDIR)\config.obj" \
	"$(INTDIR)\connection.obj" \
	"$(INTDIR)\core.obj" \
	"$(INTDIR)\core_filters.obj" \
	"$(INTDIR)\http_core.obj" \
	"$(INTDIR)\http_etag.obj" \
	"$(INTDIR)\http_filters.obj" \
	"$(INTDIR)\http_protocol.obj" \
	"$(INTDIR)\http_request.obj" \
	"$(INTDIR)\log.obj" \
	"$(INTDIR)\protocol.obj" \
	"$(INTDIR)\request.obj" \
	"$(INTDIR)\vhost.obj" \
	"$(INTDIR)\capi_so.obj" \
	"$(INTDIR)\capi_win32.obj" \
	"$(INTDIR)\cAPIs.obj" \
	"$(INTDIR)\eoc_bucket.obj" \
	"$(INTDIR)\eor_bucket.obj" \
	"$(INTDIR)\error_bucket.obj" \
	"$(INTDIR)\util.obj" \
	"$(INTDIR)\util_cfgtree.obj" \
	"$(INTDIR)\util_cookies.obj" \
	"$(INTDIR)\util_expr_eval.obj" \
	"$(INTDIR)\util_expr_scan.obj" \
	"$(INTDIR)\util_expr_parse.obj" \
	"$(INTDIR)\util_fcgi.obj" \
	"$(INTDIR)\util_filter.obj" \
	"$(INTDIR)\util_md5.obj" \
	"$(INTDIR)\util_mutex.obj" \
	"$(INTDIR)\util_pcre.obj" \
	"$(INTDIR)\util_regex.obj" \
	"$(INTDIR)\util_script.obj" \
	"$(INTDIR)\util_time.obj" \
	"$(INTDIR)\util_win32.obj" \
	"$(INTDIR)\util_xml.obj" \
	"$(INTDIR)\clhy_regkey.obj" \
	"$(INTDIR)\child.obj" \
	"$(INTDIR)\listen.obj" \
	"$(INTDIR)\core_common.obj" \
	"$(INTDIR)\core_winnt.obj" \
	"$(INTDIR)\nt_eventlog.obj" \
	"$(INTDIR)\provider.obj" \
	"$(INTDIR)\scoreboard.obj" \
	"$(INTDIR)\service.obj" \
	"$(INTDIR)\libwwhy.res"

"$(OUTDIR)\libwwhy.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
   cl.exe /nologo /MD /W3 /O2 /Oy- /Zi /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /I "./kudelrunsrc/pcre" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "CLHY_DECLARE_EXPORT" /Fd"Release\libwwhy" /FD /c server\buildmark.c /Fo"Release\buildmark.obj"
	 $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\libwwhy.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\libwwhy.dll"
   if exist .\Release\libwwhy.dll.manifest mt.exe -manifest .\Release\libwwhy.dll.manifest -outputresource:.\Release\libwwhy.dll;2
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("libwwhy.dep")
!INCLUDE "libwwhy.dep"
!ELSE 
!MESSAGE Warning: cannot find "libwwhy.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "libwwhy - Win32 Release" || "$(CFG)" == "libwwhy - Win32 Debug" || "$(CFG)" == "libwwhy - Win32 Lexical"
SOURCE=.\cAPIs\generators\capi_cgi.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

InputPath=.\cAPIs\generators\capi_cgi.h

".\include\capi_cgi.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\cAPIs\generators\capi_cgi.h > .\include\capi_cgi.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

InputPath=.\cAPIs\generators\capi_cgi.h

".\include\capi_cgi.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\cAPIs\generators\capi_cgi.h > .\include\capi_cgi.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

!ENDIF 

SOURCE=.\cAPIs\dav\main\capi_dav.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

InputPath=.\cAPIs\dav\main\capi_dav.h

".\include\capi_dav.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\cAPIs\dav\main\capi_dav.h > .\include\capi_dav.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

InputPath=.\cAPIs\dav\main\capi_dav.h

".\include\capi_dav.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\cAPIs\dav\main\capi_dav.h > .\include\capi_dav.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

!ENDIF 

SOURCE=.\cAPIs\clfilters\capi_include.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

InputPath=.\cAPIs\clfilters\capi_include.h

".\include\capi_include.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\cAPIs\clfilters\capi_include.h > .\include\capi_include.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

InputPath=.\cAPIs\clfilters\capi_include.h

".\include\capi_include.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\cAPIs\clfilters\capi_include.h > .\include\capi_include.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

!ENDIF 

SOURCE=.\cAPIs\proxy\capi_proxy.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

InputPath=.\cAPIs\proxy\capi_proxy.h

".\include\capi_proxy.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\cAPIs\proxy\capi_proxy.h > .\include\capi_proxy.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

InputPath=.\cAPIs\proxy\capi_proxy.h

".\include\capi_proxy.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\cAPIs\proxy\capi_proxy.h > .\include\capi_proxy.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

!ENDIF 

SOURCE=.\cAPIs\core\capi_so.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

InputPath=.\cAPIs\core\capi_so.h

".\include\capi_so.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\cAPIs\core\capi_so.h > .\include\capi_so.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

InputPath=.\cAPIs\core\capi_so.h

".\include\capi_so.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\cAPIs\core\capi_so.h > .\include\capi_so.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

!ENDIF 

SOURCE=.\platforms\win32\platform.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

InputPath=.\platforms\win32\platform.h

".\include\platform.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\platforms\win32\platform.h > .\include\platform.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

InputPath=.\platforms\win32\platform.h

".\include\platform.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\platforms\win32\platform.h > .\include\platform.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

!ENDIF 

SOURCE=.\platforms\win32\win32_config_layout.h

!IF  "$(CFG)" == "libwwhy - Win32 Release"

InputPath=.\platforms\win32\win32_config_layout.h

".\include\clhy_config_layout.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\platforms\win32\win32_config_layout.h > .\include\clhy_config_layout.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

InputPath=.\platforms\win32\win32_config_layout.h

".\include\clhy_config_layout.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	type .\platforms\win32\win32_config_layout.h > .\include\clhy_config_layout.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

!ENDIF 

SOURCE=.\server\buildmark.c
SOURCE=.\cAPIs\http\byterange_filter.c

"$(INTDIR)\byterange_filter.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\cAPIs\http\chunk_filter.c

"$(INTDIR)\chunk_filter.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\config.c

"$(INTDIR)\config.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\connection.c

"$(INTDIR)\connection.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\core.c

"$(INTDIR)\core.obj" : $(SOURCE) "$(INTDIR)" ".\include\capi_proxy.h" ".\include\capi_so.h" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\core_filters.c

"$(INTDIR)\core_filters.obj" : $(SOURCE) "$(INTDIR)" ".\include\capi_so.h" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\cAPIs\http\http_core.c

"$(INTDIR)\http_core.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\cAPIs\http\http_etag.c

"$(INTDIR)\http_etag.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\cAPIs\http\http_filters.c

"$(INTDIR)\http_filters.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\cAPIs\http\http_protocol.c

"$(INTDIR)\http_protocol.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\cAPIs\http\http_request.c

"$(INTDIR)\http_request.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\log.c

"$(INTDIR)\log.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\protocol.c

"$(INTDIR)\protocol.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\request.c

"$(INTDIR)\request.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\vhost.c

"$(INTDIR)\vhost.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\cAPIs\core\capi_so.c

"$(INTDIR)\capi_so.obj" : $(SOURCE) "$(INTDIR)" ".\cAPIs\core\capi_so.h" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\cAPIs\arch\win32\capi_win32.c

"$(INTDIR)\capi_win32.obj" : $(SOURCE) "$(INTDIR)" ".\include\capi_cgi.h" ".\include\platform.h" ".\include\clhy_config_layout.h" ".\include\capi_include.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\platforms\win32\cAPIs.c

"$(INTDIR)\cAPIs.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\eoc_bucket.c

"$(INTDIR)\eoc_bucket.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\eor_bucket.c

"$(INTDIR)\eor_bucket.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\error_bucket.c

"$(INTDIR)\error_bucket.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util.c

"$(INTDIR)\util.obj" : $(SOURCE) "$(INTDIR)" ".\server\test_char.h" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_cfgtree.c

"$(INTDIR)\util_cfgtree.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_cookies.c

"$(INTDIR)\util_cookies.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_expr_eval.c

"$(INTDIR)\util_expr_eval.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_expr_parse.c

"$(INTDIR)\util_expr_parse.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_expr_scan.c

"$(INTDIR)\util_expr_scan.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_fcgi.c

"$(INTDIR)\util_fcgi.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_filter.c

"$(INTDIR)\util_filter.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_md5.c

"$(INTDIR)\util_md5.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_mutex.c

"$(INTDIR)\util_mutex.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_pcre.c

"$(INTDIR)\util_pcre.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_regex.c

"$(INTDIR)\util_regex.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_script.c

"$(INTDIR)\util_script.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_time.c

"$(INTDIR)\util_time.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\platforms\win32\util_win32.c

"$(INTDIR)\util_win32.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_xml.c

"$(INTDIR)\util_xml.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\platforms\win32\clhy_regkey.c

"$(INTDIR)\clhy_regkey.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\clcore\winnt\child.c

"$(INTDIR)\child.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\listen.c

"$(INTDIR)\listen.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\core_common.c

"$(INTDIR)\core_common.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\clcore\winnt\core_winnt.c

"$(INTDIR)\core_winnt.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\clcore\winnt\nt_eventlog.c

"$(INTDIR)\nt_eventlog.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\provider.c

"$(INTDIR)\provider.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\scoreboard.c

"$(INTDIR)\scoreboard.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\clcore\winnt\service.c

"$(INTDIR)\service.obj" : $(SOURCE) "$(INTDIR)" ".\include\platform.h" ".\include\clhy_config_layout.h"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\server\util_expr_parse.y

!IF  "$(CFG)" == "libwwhy - Win32 Release"

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

InputPath=.\server\util_expr_parse.y

".\server\util_expr_parse.c"	".\server\util_expr_parse.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	bison -pclhy_expr_yy --defines=.\server\util_expr_parse.h -o .\server\util_expr_parse.c .\server\util_expr_parse.y
<< 
	

!ENDIF 

SOURCE=.\server\util_expr_scan.l

!IF  "$(CFG)" == "libwwhy - Win32 Release"

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

InputPath=.\server\util_expr_scan.l

".\server\util_expr_scan.c" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	flex -Pclhy_expr_yy -o .\server\util_expr_scan.c .\server\util_expr_scan.l
<< 
	

!ENDIF 

!IF  "$(CFG)" == "libwwhy - Win32 Release"

"libkuda - Win32 Release" : 
   cd ".\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\.."

"libkuda - Win32 ReleaseCLEAN" : 
   cd ".\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\.."

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd ".\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\.."

"libkuda - Win32 DebugCLEAN" : 
   cd ".\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\.."

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

!ENDIF 

!IF  "$(CFG)" == "libwwhy - Win32 Release"

"libkudaiconv - Win32 Release" : 
   cd ".\kudelrunsrc\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudaiconv.mak" CFG="libkudaiconv - Win32 Release" 
   cd "..\.."

"libkudaiconv - Win32 ReleaseCLEAN" : 
   cd ".\kudelrunsrc\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudaiconv.mak" CFG="libkudaiconv - Win32 Release" RECURSE=1 CLEAN 
   cd "..\.."

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

"libkudaiconv - Win32 Debug" : 
   cd ".\kudelrunsrc\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudaiconv.mak" CFG="libkudaiconv - Win32 Debug" 
   cd "..\.."

"libkudaiconv - Win32 DebugCLEAN" : 
   cd ".\kudelrunsrc\kuda-iconv"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudaiconv.mak" CFG="libkudaiconv - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\.."

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

!ENDIF 

!IF  "$(CFG)" == "libwwhy - Win32 Release"

"libkudadelman - Win32 Release" : 
   cd ".\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" 
   cd "..\.."

"libkudadelman - Win32 ReleaseCLEAN" : 
   cd ".\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\.."

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

"libkudadelman - Win32 Debug" : 
   cd ".\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" 
   cd "..\.."

"libkudadelman - Win32 DebugCLEAN" : 
   cd ".\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\.."

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

!ENDIF 

!IF  "$(CFG)" == "libwwhy - Win32 Release"

"gen_test_char - Win32 Release" : 
   cd ".\server"
   $(MAKE) /$(MAKEFLAGS) /F ".\gen_test_char.mak" CFG="gen_test_char - Win32 Release" 
   cd ".."

"gen_test_char - Win32 ReleaseCLEAN" : 
   cd ".\server"
   $(MAKE) /$(MAKEFLAGS) /F ".\gen_test_char.mak" CFG="gen_test_char - Win32 Release" RECURSE=1 CLEAN 
   cd ".."

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

"gen_test_char - Win32 Debug" : 
   cd ".\server"
   $(MAKE) /$(MAKEFLAGS) /F ".\gen_test_char.mak" CFG="gen_test_char - Win32 Debug" 
   cd ".."

"gen_test_char - Win32 DebugCLEAN" : 
   cd ".\server"
   $(MAKE) /$(MAKEFLAGS) /F ".\gen_test_char.mak" CFG="gen_test_char - Win32 Debug" RECURSE=1 CLEAN 
   cd ".."

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

!ENDIF 

SOURCE=.\server\gen_test_char.exe

!IF  "$(CFG)" == "libwwhy - Win32 Release"

InputPath=.\server\gen_test_char.exe
USERDEP__GEN_T=".\include\platform.h"	

".\server\test_char.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)" $(USERDEP__GEN_T)
	<<tempfile.bat 
	@echo off 
	.\server\gen_test_char.exe >.\server\test_char.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"

InputPath=.\server\gen_test_char.exe
USERDEP__GEN_T=".\include\platform.h"	

".\server\test_char.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)" $(USERDEP__GEN_T)
	<<tempfile.bat 
	@echo off 
	.\server\gen_test_char.exe >.\server\test_char.h
<< 
	

!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"

InputPath=.\server\gen_test_char.exe
USERDEP__GEN_T=".\include\platform.h"	

".\server\test_char.h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)" $(USERDEP__GEN_T)
	<<tempfile.bat 
	@echo off 
	.\server\gen_test_char.exe >.\server\test_char.h
<< 
	

!ENDIF 

SOURCE=.\build\win32\wwhy.rc

!IF  "$(CFG)" == "libwwhy - Win32 Release"


"$(INTDIR)\libwwhy.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\libwwhy.res" /i "./include" /i "./kudelrunsrc/kuda/include" /i "build\win32" /d "NDEBUG" /d BIN_NAME="libwwhy.dll" /d LONG_NAME="cLHy HTTP Server Core" $(SOURCE)


!ELSEIF  "$(CFG)" == "libwwhy - Win32 Debug"


"$(INTDIR)\libwwhy.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\libwwhy.res" /i "./include" /i "./kudelrunsrc/kuda/include" /i "build\win32" /d "_DEBUG" /d BIN_NAME="libwwhy.dll" /d LONG_NAME="cLHy HTTP Server Core" $(SOURCE)


!ELSEIF  "$(CFG)" == "libwwhy - Win32 Lexical"


"$(INTDIR)\libwwhy.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\libwwhy.res" /i "./include" /i "./kudelrunsrc/kuda/include" /i "build\win32" /d "NDEBUG" /d BIN_NAME="libwwhy.dll" /d LONG_NAME="cLHy HTTP Server Core" $(SOURCE)


!ENDIF 


!ENDIF 

