# Microsoft Developer Studio Generated NMAKE File, Based on wwhy.dsp
!IF "$(CFG)" == ""
CFG=wwhy - Win32 Release
!MESSAGE No configuration specified. Defaulting to wwhy - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "wwhy - Win32 Release" && "$(CFG)" != "wwhy - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "wwhy.mak" CFG="wwhy - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "wwhy - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "wwhy - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "wwhy - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\wwhy.exe" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Release" "libkudadelman - Win32 Release" "libkuda - Win32 Release" "$(OUTDIR)\wwhy.exe" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 ReleaseCLEAN" "libkudadelman - Win32 ReleaseCLEAN" "libwwhy - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\wwhy.idb"
	-@erase "$(INTDIR)\wwhy.res"
	-@erase "$(INTDIR)\main.obj"
	-@erase "$(OUTDIR)\wwhy.exe"
	-@erase "$(OUTDIR)\wwhy.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\wwhy" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\wwhy.res" /i "./include" /i "./kudelrunsrc/kuda/include" /d "NDEBUG" /d "APP_FILE" /d BIN_NAME="wwhy.exe" /d LONG_NAME="cLHy HTTP Server" /d ICON_FILE="clhy.ico" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\wwhy.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib advapi32.lib ws2_32.lib mswsock.lib /nologo /stack:0x40000 /subsystem:console /incremental:no /pdb:"$(OUTDIR)\wwhy.pdb" /debug /out:"$(OUTDIR)\wwhy.exe" /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\main.obj" \
	"$(INTDIR)\wwhy.res" \
	".\kudelrunsrc\kuda\Release\libkuda-1.lib" \
	".\kudelrunsrc\kuda-delman\Release\libkudadelman-1.lib" \
	"$(OUTDIR)\libwwhy.lib"

"$(OUTDIR)\wwhy.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\wwhy.exe
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\wwhy.exe"
   if exist .\Release\wwhy.exe.manifest mt.exe -manifest .\Release\wwhy.exe.manifest -outputresource:.\Release\wwhy.exe;1
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "wwhy - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\wwhy.exe" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "libwwhy - Win32 Debug" "libkudadelman - Win32 Debug" "libkuda - Win32 Debug" "$(OUTDIR)\wwhy.exe" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"libkuda - Win32 DebugCLEAN" "libkudadelman - Win32 DebugCLEAN" "libwwhy - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\wwhy.idb"
	-@erase "$(INTDIR)\wwhy.res"
	-@erase "$(INTDIR)\main.obj"
	-@erase "$(OUTDIR)\wwhy.exe"
	-@erase "$(OUTDIR)\wwhy.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "./include" /I "./kudelrunsrc/kuda/include" /I "./kudelrunsrc/kuda-delman/include" /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\wwhy" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\wwhy.res" /i "./include" /i "./kudelrunsrc/kuda/include" /d "_DEBUG" /d "APP_FILE" /d BIN_NAME="wwhy.exe" /d LONG_NAME="cLHy HTTP Server" /d ICON_FILE="clhy.ico" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\wwhy.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib advapi32.lib ws2_32.lib mswsock.lib /nologo /stack:0x40000 /subsystem:console /incremental:no /pdb:"$(OUTDIR)\wwhy.pdb" /debug /out:"$(OUTDIR)\wwhy.exe" 
LINK32_OBJS= \
	"$(INTDIR)\main.obj" \
	"$(INTDIR)\wwhy.res" \
	".\kudelrunsrc\kuda\Debug\libkuda-1.lib" \
	".\kudelrunsrc\kuda-delman\Debug\libkudadelman-1.lib" \
	"$(OUTDIR)\libwwhy.lib"

"$(OUTDIR)\wwhy.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\wwhy.exe
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\wwhy.exe"
   if exist .\Debug\wwhy.exe.manifest mt.exe -manifest .\Debug\wwhy.exe.manifest -outputresource:.\Debug\wwhy.exe;1
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("wwhy.dep")
!INCLUDE "wwhy.dep"
!ELSE 
!MESSAGE Warning: cannot find "wwhy.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "wwhy - Win32 Release" || "$(CFG)" == "wwhy - Win32 Debug"

!IF  "$(CFG)" == "wwhy - Win32 Release"

"libkuda - Win32 Release" : 
   cd ".\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" 
   cd "..\.."

"libkuda - Win32 ReleaseCLEAN" : 
   cd ".\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\.."

!ELSEIF  "$(CFG)" == "wwhy - Win32 Debug"

"libkuda - Win32 Debug" : 
   cd ".\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" 
   cd "..\.."

"libkuda - Win32 DebugCLEAN" : 
   cd ".\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkuda.mak" CFG="libkuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\.."

!ENDIF 

!IF  "$(CFG)" == "wwhy - Win32 Release"

"libkudadelman - Win32 Release" : 
   cd ".\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" 
   cd "..\.."

"libkudadelman - Win32 ReleaseCLEAN" : 
   cd ".\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\.."

!ELSEIF  "$(CFG)" == "wwhy - Win32 Debug"

"libkudadelman - Win32 Debug" : 
   cd ".\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" 
   cd "..\.."

"libkudadelman - Win32 DebugCLEAN" : 
   cd ".\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\libkudadelman.mak" CFG="libkudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\.."

!ENDIF 

!IF  "$(CFG)" == "wwhy - Win32 Release"

"libwwhy - Win32 Release" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" 
   cd "."

"libwwhy - Win32 ReleaseCLEAN" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Release" RECURSE=1 CLEAN 
   cd "."

!ELSEIF  "$(CFG)" == "wwhy - Win32 Debug"

"libwwhy - Win32 Debug" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" 
   cd "."

"libwwhy - Win32 DebugCLEAN" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F ".\libwwhy.mak" CFG="libwwhy - Win32 Debug" RECURSE=1 CLEAN 
   cd "."

!ENDIF 

SOURCE=.\build\win32\wwhy.rc

!IF  "$(CFG)" == "wwhy - Win32 Release"


"$(INTDIR)\wwhy.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\wwhy.res" /i "./include" /i "./kudelrunsrc/kuda/include" /i "build\win32" /d "NDEBUG" /d "APP_FILE" /d BIN_NAME="wwhy.exe" /d LONG_NAME="cLHy HTTP Server" /d ICON_FILE="clhy.ico" $(SOURCE)


!ELSEIF  "$(CFG)" == "wwhy - Win32 Debug"


"$(INTDIR)\wwhy.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\wwhy.res" /i "./include" /i "./kudelrunsrc/kuda/include" /i "build\win32" /d "_DEBUG" /d "APP_FILE" /d BIN_NAME="wwhy.exe" /d LONG_NAME="cLHy HTTP Server" /d ICON_FILE="clhy.ico" $(SOURCE)


!ENDIF 

SOURCE=.\server\main.c

"$(INTDIR)\main.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)



!ENDIF 

