/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file unix/platform.h
 * @brief This file in included in all cLHy source code. It contains definitions
 * of facilities available on _this_ operating system (HAVE_* macros),
 * and prototypes of PLATFORM specific functions defined in platform.c or platforms-inline.c
 *
 * @defgroup CLHYKUDEL_PLATFORM_UNIX unix
 * @ingroup  CLHYKUDEL_PLATFORM
 * @{
 */

#ifndef CLHYKUDEL_PLATFORM_H
#define CLHYKUDEL_PLATFORM_H

#include "kuda.h"
#include "clhy_config.h"

#ifndef PLATFORM
#define PLATFORM "Unix"
#endif

/* On platforms where CLHY_NEED_SET_MUTEX_PERMS is defined, cAPIs
 * should call unixd_set_*_mutex_perms on mutexes created in the
 * parent process. */
#define CLHY_NEED_SET_MUTEX_PERMS 1

/* Define command-line rewriting for this platform, handled by core.
 */
#define CLHY_PLATFORM_REWRITE_ARGS_HOOK clhy_clmp_rewrite_args

#ifdef _OSD_POSIX
pid_t platform_fork(const char *user);
#endif

#endif  /* !CLHYKUDEL_PLATFORM_H */
/** @} */
