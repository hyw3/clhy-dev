/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  unixd.h
 * @brief common stuff that unix cLMPs will want
 *
 * @addtogroup CLHYKUDEL_PLATFORM_UNIX
 * @{
 */

#ifndef UNIXD_H
#define UNIXD_H

#include "wwhy.h"
#include "http_config.h"
#include "scoreboard.h"
#include "clhy_listen.h"
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_SYS_RESOURCE_H
#include <sys/resource.h>
#endif
#include "kuda_hooks.h"
#include "kuda_thread_proc.h"
#include "kuda_proc_mutex.h"
#include "kuda_global_mutex.h"

#include <pwd.h>
#include <grp.h>
#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#ifdef HAVE_SYS_IPC_H
#include <sys/ipc.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    uid_t uid;
    gid_t gid;
    int userdir;
} clhy_unix_identity_t;

CLHY_DECLARE_HOOK(clhy_unix_identity_t *, get_suexec_identity,(const request_rec *r))


/* Default user name and group name. These may be specified as numbers by
 * placing a # before a number */

#ifndef DEFAULT_USER
#define DEFAULT_USER "#-1"
#endif
#ifndef DEFAULT_GROUP
#define DEFAULT_GROUP "#-1"
#endif

typedef struct {
    const char *user_name;
    const char *group_name;
    uid_t user_id;
    gid_t group_id;
    int suexec_enabled;
    const char *chroot_dir;
    const char *suexec_disabled_reason; /* suitable msg if !suexec_enabled */
} unixd_config_rec;
CLHY_DECLARE_DATA extern unixd_config_rec clhy_unixd_config;

#if defined(RLIMIT_CPU) || defined(RLIMIT_DATA) || defined(RLIMIT_VMEM) || defined(RLIMIT_NPROC) || defined(RLIMIT_AS)
CLHY_DECLARE(void) clhy_unixd_set_rlimit(cmd_parms *cmd, struct rlimit **plimit,
                                     const char *arg,
                                     const char * arg2, int type);
#endif

/**
 * One of the functions to set mutex permissions should be called in
 * the parent process on platforms that switch identity when the
 * server is started as root.
 * If the child init logic is performed before switching identity
 * (e.g., cLMP setup for an accept mutex), it should only be called
 * for SysV semaphores.  Otherwise, it is safe to call it for all
 * mutex types.
 */
CLHY_DECLARE(kuda_status_t) clhy_unixd_set_proc_mutex_perms(kuda_proc_mutex_t *pmutex);
CLHY_DECLARE(kuda_status_t) clhy_unixd_set_global_mutex_perms(kuda_global_mutex_t *gmutex);
CLHY_DECLARE(kuda_status_t) clhy_unixd_accept(void **accepted, clhy_listen_rec *lr, kuda_pool_t *ptrans);

#ifdef HAVE_KILLPG
#define clhy_unixd_killpg(x, y)   (killpg ((x), (y)))
#define clhy_platform_killpg(x, y)      (killpg ((x), (y)))
#else /* HAVE_KILLPG */
#define clhy_unixd_killpg(x, y)   (kill (-(x), (y)))
#define clhy_platform_killpg(x, y)      (kill (-(x), (y)))
#endif /* HAVE_KILLPG */

typedef struct {
    void            *baton;  /* cLMP's */

    /* volatile because they're updated from signals' handlers */
    int volatile    clmp_state;
    int volatile    shutdown_pending;
    int volatile    restart_pending;
    int volatile    is_ungraceful;

    clhy_generation_t my_generation;
    int             capi_loads;
    int             was_graceful;

    /*
     * Current number of listeners buckets and maximum reached across
     * restarts (to size retained data according to dynamic num_buckets,
     * eg. idle_spawn_rate).
     */
    int num_buckets, max_buckets;
} clhy_unixd_clmp_retained_data;

CLHY_DECLARE(clhy_unixd_clmp_retained_data *) clhy_unixd_clmp_get_retained_data(void);
CLHY_DECLARE(void) clhy_unixd_clmp_set_signals(kuda_pool_t *pconf, int once_process);

#ifdef __cplusplus
}
#endif

#endif
/** @} */
