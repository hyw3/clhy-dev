/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file will include PLATFORM specific functions which are not inlineable.
 * Any inlineable functions should be defined in platforms-inline.c instead.
 */

#ifdef _OSD_POSIX

#include "platform.h"

#include "wwhy.h"
#include "http_config.h"
#include "http_log.h"
#include "kuda_lib.h"

#define USER_LEN 8

CLHYLOG_USE_CAPI(core);

typedef enum
{
    bs2_unknown,     /* not initialized yet. */
    bs2_noFORK,      /* no fork() because -X flag was specified */
    bs2_FORK,        /* only fork() because uid != 0 */
    bs2_UFORK        /* Normally, ufork() is used to switch identities. */
} bs2_ForkType;

static bs2_ForkType forktype = bs2_unknown;

/* Determine the method for forking off a child in such a way as to
 * set both the POSIX and BS2000 user id's to the unprivileged user.
 */
static bs2_ForkType platform_forktype(int one_process)
{
    /* have we checked the PLATFORM version before? If yes return the previous
     * result - the PLATFORM release isn't going to change suddenly!
     */
    if (forktype == bs2_unknown) {
        /* not initialized yet */

        /* No fork if the one_process option was set */
        if (one_process) {
            forktype = bs2_noFORK;
        }
        /* If the user is unprivileged, use the normal fork() only. */
        else if (getuid() != 0) {
            forktype = bs2_FORK;
        }
        else
            forktype = bs2_UFORK;
    }
    return forktype;
}



/* This routine complements the setuid() call: it causes the BS2000 job
 * environment to be switched to the target user's user id.
 * That is important if CGI scripts try to execute native BS2000 commands.
 */
int platform_init_job_environment(server_rec *server, const char *user_name, int one_process)
{
    bs2_ForkType            type = platform_forktype(one_process);

    /* We can be sure that no change to uid==0 is possible because of
     * the checks in http_core.c:set_user()
     */

    if (one_process) {

        type = forktype = bs2_noFORK;

        clhy_log_error(CLHYLOG_MARK, CLHYLOG_ERR, 0, server, CLHYLOGNO(02170)
                     "The debug mode of cLHy should only "
                     "be started by an unprivileged user!");
        return 0;
    }

    return 0;
}

/* BS2000 requires a "special" version of fork() before a setuid() call */
pid_t platform_fork(const char *user)
{
    pid_t pid;
    char  username[USER_LEN+1];

    switch (platform_forktype(0)) {

      case bs2_FORK:
        pid = fork();
        break;

      case bs2_UFORK:
        kuda_cpystrn(username, user, sizeof username);

        /* Make user name all upper case - for some versions of ufork() */
        clhy_str_toupper(username);

        pid = ufork(username);
        if (pid == -1 && errno == EPERM) {
            clhy_log_error(CLHYLOG_MARK, CLHYLOG_EMERG, errno, clhy_server_conf,
                         CLHYLOGNO(02171) "ufork: Possible mis-configuration "
                         "for user %s - Aborting.", user);
            exit(1);
        }
        break;

      default:
        pid = 0;
        break;
    }

    return pid;
}

#else /* _OSD_POSIX */
void bs2000_platform_is_not_here()
{
}
#endif /* _OSD_POSIX */
