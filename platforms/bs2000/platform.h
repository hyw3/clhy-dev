/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file bs2000/platform.h
 * @brief This file in included in all cLHy source code. It contains definitions
 * of facilities available on _this_ operating system (HAVE_* macros),
 * and prototypes of PLATFORM specific functions defined in platform.c or platforms-inline.c
 *
 * @defgroup CLHYKUDEL_PLATFORM_BS2000 bs2000
 * @ingroup  CLHYKUDEL_PLATFORM
 * @{
 */

#ifndef CLHYKUDEL_PLATFORM_BS2000_H
#define CLHYKUDEL_PLATFORM_BS2000_H

#define PLATFORM "BS2000"

#include "../unix/platform.h"

/* Other clhy_platform_ routines not used by this platform */

extern pid_t platform_fork(const char *user);

#endif /* CLHYKUDEL_PLATFORM_BS2000_H */
/** @} */
