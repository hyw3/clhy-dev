/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define INCL_DOS
#define INCL_DOSERRORS
#include <os2.h>
#include "clhy_config.h"
#include "wwhy.h"
#include "http_log.h"
#include "platform.h"
#include <sys/time.h>
#include <sys/signal.h>
#include <ctype.h>
#include <string.h>
#include "kuda_strings.h"


CLHY_DECLARE(kuda_status_t) clhy_platform_create_privileged_process(
    const request_rec *r,
    kuda_proc_t *newproc, const char *progname,
    const char * const *args,
    const char * const *env,
    kuda_procattr_t *attr, kuda_pool_t *p)
{
    return kuda_proc_create(newproc, progname, args, env, attr, p);
}
