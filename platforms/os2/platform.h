/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file os2/platform.h
 * @brief This file in included in all cLHy source code. It contains definitions
 * of facilities available on _this_ operating system (HAVE_* macros),
 * and prototypes of PLATFORM specific functions defined in platform.c or platforms-inline.c
 *
 * @defgroup CLHYKUDEL_PLATFORM_OS2 os2
 * @ingroup  CLHYKUDEL_PLATFORM
 * @{
 */

#ifndef CLHYKUDEL_PLATFORM_H
#define CLHYKUDEL_PLATFORM_H

#define PLATFORM "OS2"

/* going away shortly... */
#define HAVE_DRIVE_LETTERS
#define HAVE_UNC_PATHS
#define CASE_BLIND_FILESYSTEM
#define CLHY_PLATFORM_REWRITE_ARGS_HOOK NULL

#endif   /* ! CLHYKUDEL_PLATFORM_H */
/** @} */
