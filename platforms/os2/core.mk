# Some rules for making a shared core dll on OS2

os2core: wwhy.dll $(CORE_IMPLIB)
	$(LIBTOOL) --mode=link gcc -Zstack 512 $(LDFLAGS) $(EXTRA_LDFLAGS) -o wwhy $(CORE_IMPLIB)

wwhy.dll: $(PROGRAM_DEPENDENCIES) $(CORE_IMPLIB)
	$(LINK) -Zdll $(EXTRA_LDFLAGS) -s -o $@ server/exports.lo cAPIs.lo $(PROGRAM_DEPENDENCIES) $(CLHY_LIBS) server/cLHyCoreOS2.def
