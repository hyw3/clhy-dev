/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "kuda_strings.h"
#include "arch/win32/kuda_arch_file_io.h"
#include "arch/win32/kuda_arch_misc.h"

#include "wwhy.h"
#include "http_log.h"
#include "clhy_core.h"

#include <stdarg.h>
#include <time.h>
#include <stdlib.h>


CLHY_DECLARE(kuda_status_t) clhy_platform_proc_filepath(char **binpath, kuda_pool_t *p)
{
    kuda_wchar_t wbinpath[KUDA_PATH_MAX];

#if KUDA_HAS_UNICODE_FS
    IF_WIN_PLATFORM_IS_UNICODE
    {
        kuda_size_t binlen;
        kuda_size_t wbinlen;
        kuda_status_t rv;
        if (!GetcAPIFileNameW(NULL, wbinpath, sizeof(wbinpath)
                                              / sizeof(kuda_wchar_t))) {
            return kuda_get_platform_error();
        }
        wbinlen = wcslen(wbinpath) + 1;
        binlen = (wbinlen - 1) * 3 + 1;
        *binpath = kuda_palloc(p, binlen);
        rv = kuda_conv_ucs2_to_utf8(wbinpath, &wbinlen, *binpath, &binlen);
        if (rv != KUDA_SUCCESS)
            return rv;
        else if (wbinlen)
            return KUDA_ENAMETOOLONG;
    }
#endif /* KUDA_HAS_UNICODE_FS */
#if KUDA_HAS_ANSI_FS
    ELSE_WIN_PLATFORM_IS_ANSI
    {
        /* share the same scratch buffer */
        char *pathbuf = (char*) wbinpath;
        if (!GetcAPIFileName(NULL, pathbuf, sizeof(wbinpath))) {
            return kuda_get_platform_error();
        }
        *binpath = kuda_pstrdup(p, pathbuf);
    }
#endif
    return KUDA_SUCCESS;
}


CLHY_DECLARE(kuda_status_t) clhy_platform_create_privileged_process(
    const request_rec *r,
    kuda_proc_t *newproc, const char *progname,
    const char * const *args,
    const char * const *env,
    kuda_procattr_t *attr, kuda_pool_t *p)
{
    return kuda_proc_create(newproc, progname, args, env, attr, p);
}


/* This code is stolen from misc/win32/misc.c and kuda_private.h
 * This helper code resolves late bound entry points
 * missing from one or more releases of the Win32 API...
 * but it sure would be nice if we didn't duplicate this code
 * from the KUDA ;-)
 */
static const char* const lateDllName[DLL_defined] = {
    "kernel32", "advapi32", "mswsock",  "ws2_32"  };
static HCAPI lateDllHandle[DLL_defined] = {
    NULL,       NULL,       NULL,       NULL      };


FARPROC clhy_load_dll_func(clhy_dlltoken_e fnLib, char* fnName, int ordinal)
{
    if (!lateDllHandle[fnLib]) {
        lateDllHandle[fnLib] = LoadLibrary(lateDllName[fnLib]);
        if (!lateDllHandle[fnLib])
            return NULL;
    }
    if (ordinal)
        return GetProcAddress(lateDllHandle[fnLib], (char *) ordinal);
    else
        return GetProcAddress(lateDllHandle[fnLib], fnName);
}


/* To share the semaphores with other processes, we need a NULL ACL
 * Code from MS KB Q106387
 */
PSECURITY_ATTRIBUTES GetNullACL(void)
{
    PSECURITY_DESCRIPTOR pSD;
    PSECURITY_ATTRIBUTES sa;

    sa  = (PSECURITY_ATTRIBUTES) LocalAlloc(LPTR, sizeof(SECURITY_ATTRIBUTES));
    sa->nLength = sizeof(SECURITY_ATTRIBUTES);

    pSD = (PSECURITY_DESCRIPTOR) LocalAlloc(LPTR, SECURITY_DESCRIPTOR_MIN_LENGTH);
    sa->lpSecurityDescriptor = pSD;

    if (pSD == NULL || sa == NULL) {
        return NULL;
    }
    kuda_set_platform_error(0);
    if (!InitializeSecurityDescriptor(pSD, SECURITY_DESCRIPTOR_REVISION)
        || kuda_get_platform_error()) {
        LocalFree( pSD );
        LocalFree( sa );
        return NULL;
    }
    if (!SetSecurityDescriptorDacl(pSD, TRUE, (PACL) NULL, FALSE)
        || kuda_get_platform_error()) {
        LocalFree( pSD );
        LocalFree( sa );
        return NULL;
    }

    sa->bInheritHandle = FALSE;
    return sa;
}


void CleanNullACL(void *sa)
{
    if (sa) {
        LocalFree(((PSECURITY_ATTRIBUTES)sa)->lpSecurityDescriptor);
        LocalFree(sa);
    }
}
