/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file win32/platform.h
 * @brief This file in included in all cLHy source code. It contains definitions
 * of facilities available on _this_ operating system (HAVE_* macros),
 * and prototypes of PLATFORM specific functions defined in platform.c or platforms-inline.c
 *
 * @defgroup CLHYKUDEL_PLATFORM_WIN32 win32
 * @ingroup  CLHYKUDEL_PLATFORM
 * @{
 */

#ifdef WIN32

#ifndef CLHY_PLATFORM_H
#define CLHY_PLATFORM_H
/* Delegate windows include to the kuda.h header, if USER or GDI declarations
 * are required (for a window rather than console application), include
 * windows.h prior to any other cLHy header files.
 */
#include "kuda_pools.h"

#include <io.h>
#include <fcntl.h>

#ifdef _WIN64
#define PLATFORM "Win64"
#else
#define PLATFORM "Win32"
#endif

/* Define command-line rewriting for this platform, handled by core.
 * For Windows, this is currently handled inside the WinNT cLMP.
 * XXX To support a choice of cLMPs, extract common platform behavior
 * into a function specified here.
 */
#define CLHY_PLATFORM_REWRITE_ARGS_HOOK NULL

/* going away shortly... */
#define HAVE_DRIVE_LETTERS
#define HAVE_UNC_PATHS
#define CASE_BLIND_FILESYSTEM

#include <stddef.h>
#include <stdlib.h> /* for exit() */

#ifdef __cplusplus
extern "C" {
#endif

/* BIG RED WARNING: exit() is mapped to allow us to capture the exit
 * status.  This header must only be included from cAPIs linked into
 * the cLHyCore.dll - since it's a horrible behavior to exit() from
 * any cAPI outside the main() block, and we -will- assume it's a
 * fatal error.
 */

CLHY_DECLARE_DATA extern int clhy_real_exit_code;

#define exit(status) ((exit)((clhy_real_exit_code==2) \
                                ? (clhy_real_exit_code = (status)) \
                                : ((clhy_real_exit_code = 0), (status))))

#ifdef CLHY_DECLARE_EXPORT

/* Defined in util_win32.c and available only to the core cAPI for
 * win32 cLMP design.
 */

CLHY_DECLARE(kuda_status_t) clhy_platform_proc_filepath(char **binpath, kuda_pool_t *p);

typedef enum {
    CLHY_DLL_WINBASEAPI = 0,    /* kernel32 From WinBase.h      */
    CLHY_DLL_WINADVAPI = 1,     /* advapi32 From WinBase.h      */
    CLHY_DLL_WINSOCKAPI = 2,    /* mswsock  From WinSock.h      */
    CLHY_DLL_WINSOCK2API = 3,   /* ws2_32   From WinSock2.h     */
    CLHY_DLL_defined = 4        /* must define as last idx_ + 1 */
} clhy_dlltoken_e;

FARPROC clhy_load_dll_func(clhy_dlltoken_e fnLib, char* fnName, int ordinal);

PSECURITY_ATTRIBUTES GetNullACL(void);
void CleanNullACL(void *sa);

#define CLHY_DECLARE_LATE_DLL_FUNC(lib, rettype, calltype, fn, ord, args, names) \
    typedef rettype (calltype *clhy_winapi_fpt_##fn) args; \
    static clhy_winapi_fpt_##fn clhy_winapi_pfn_##fn = NULL; \
    static KUDA_INLINE rettype clhy_winapi_##fn args \
    {   if (!clhy_winapi_pfn_##fn) \
            clhy_winapi_pfn_##fn = (clhy_winapi_fpt_##fn) clhy_load_dll_func(lib, #fn, ord); \
        return (*(clhy_winapi_pfn_##fn)) names; }; \

/* Win2K kernel only */
CLHY_DECLARE_LATE_DLL_FUNC(CLHY_DLL_WINADVAPI, BOOL, WINAPI, ChangeServiceConfig2A, 0, (
    SC_HANDLE hService,
    DWORD dwInfoLevel,
    LPVOID lpInfo),
    (hService, dwInfoLevel, lpInfo));
#undef ChangeServiceConfig2
#define ChangeServiceConfig2 clhy_winapi_ChangeServiceConfig2A

/* WinNT kernel only */
CLHY_DECLARE_LATE_DLL_FUNC(CLHY_DLL_WINBASEAPI, BOOL, WINAPI, CancelIo, 0, (
    IN HANDLE hFile),
    (hFile));
#undef CancelIo
#define CancelIo clhy_winapi_CancelIo

/* Win9x kernel only */
CLHY_DECLARE_LATE_DLL_FUNC(CLHY_DLL_WINBASEAPI, DWORD, WINAPI, RegisterServiceProcess, 0, (
    DWORD dwProcessId,
    DWORD dwType),
    (dwProcessId, dwType));
#define RegisterServiceProcess clhy_winapi_RegisterServiceProcess

#endif /* def CLHY_DECLARE_EXPORT */

#ifdef __cplusplus
}
#endif

#endif  /* ndef CLHY_PLATFORM_H */
#endif  /* def WIN32 */
/** @} */
