/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* cAPIs.c --- major cAPIs compiled into cLHy for Win32.
 * Only insert an entry for a cAPI if it must be compiled into
 * the core server
 */

#include "wwhy.h"
#include "http_config.h"

extern cAPI core_capi;
extern cAPI win32_capi;
extern cAPI core_winnt_capi;
extern cAPI http_capi;
extern cAPI so_capi;

CLHY_DECLARE_DATA cAPI *clhy_prelinked_capis[] = {
  &core_capi, /* core must come first */
  &win32_capi,
  &core_winnt_capi,
  &http_capi,
  &so_capi,
  NULL
};

clhy_capi_symbol_t clhy_prelinked_capi_symbols[] = {
  {"core_capi", &core_capi},
  {"win32_capi", &win32_capi},
  {"core_winnt_capi", &core_winnt_capi},
  {"http_capi", &http_capi},
  {"so_capi", &so_capi},
  {NULL, NULL}
};

CLHY_DECLARE_DATA cAPI *clhy_preactivated_capis[] = {
  &core_capi,
  &win32_capi,
  &core_winnt_capi,
  &http_capi,
  &so_capi,
  NULL
};
