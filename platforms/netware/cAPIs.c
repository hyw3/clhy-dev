/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* cAPIs.c --- major cAPIs compiled into cLHy for NetWare.
 * Only insert an entry for a cAPI if it must be compiled into
 * the core server
 */


#include "wwhy.h"
#include "http_config.h"

extern cAPI core_capi;
extern cAPI core_netware_capi;
extern cAPI http_capi;
extern cAPI so_capi;
extern cAPI mime_capi;
extern cAPI authn_core_capi;
extern cAPI authz_core_capi;
extern cAPI authz_host_capi;
extern cAPI negotiation_capi;
extern cAPI include_capi;
extern cAPI dir_capi;
extern cAPI alias_capi;
extern cAPI env_capi;
extern cAPI log_config_capi;
extern cAPI setenvif_capi;
extern cAPI watchdog_capi;
#ifdef USE_WINSOCK
extern cAPI nwssl_capi;
#endif
extern cAPI netware_capi;

cAPI *clhy_prelinked_capis[] = {
  &core_capi, /* core must come first */
  &core_netware_capi,
  &http_capi,
  &so_capi,
  &mime_capi,
  &authn_core_capi,
  &authz_core_capi,
  &authz_host_capi,
  &negotiation_capi,
  &include_capi,
  &dir_capi,
  &alias_capi,
  &env_capi,
  &log_config_capi,
  &setenvif_capi,
  &watchdog_capi,
#ifdef USE_WINSOCK
  &nwssl_capi,
#endif
  &netware_capi,
  NULL
};

clhy_capi_symbol_t clhy_prelinked_capi_symbols[] = {
  {"core_capi", &core_capi},
  {"core_netware_capi", &core_netware_capi},
  {"http_capi", &http_capi},
  {"so_capi", &so_capi},
  {"mime_capi", &mime_capi},
  {"authn_core_capi", &authn_core_capi},
  {"authz_core_capi", &authz_core_capi},
  {"authz_host_capi", &authz_host_capi},
  {"negotiation_capi", &negotiation_capi},
  {"include_capi", &include_capi},
  {"dir_capi", &dir_capi},
  {"alias_capi", &alias_capi},
  {"env_capi", &env_capi},
  {"log_config_capi", &log_config_capi},
  {"setenvif_capi", &setenvif_capi},
  {"watchdog cAPI", &watchdog_capi},
#ifdef USE_WINSOCK
  {"nwssl_capi", &nwssl_capi},
#endif
  {"netware_capi", &netware_capi},
  {NULL, NULL}
};

cAPI *clhy_preactivated_capis[] = {
  &core_capi,
  &core_netware_capi,
  &http_capi,
  &so_capi,
  &mime_capi,
  &authn_core_capi,
  &authz_core_capi,
  &authz_host_capi,
  &negotiation_capi,
  &include_capi,
  &dir_capi,
  &alias_capi,
  &env_capi,
  &log_config_capi,
  &setenvif_capi,
  &watchdog_capi,
#ifdef USE_WINSOCK
  &nwssl_capi,
#endif
  &netware_capi,
  NULL
};
