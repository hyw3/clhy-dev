/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wwhy.h"
#include "http_log.h"
#include "clhy_core.h"

#include <netware.h>
#include <nks/netware.h>
#include <nks/vm.h>

void clhy_down_server_cb(void *, void *);
void clhy_dummy_cb(void *, void *);
void clhy_cb_destroy(void *);

int nlmUnloadSignaled(int wait);
event_handle_t eh;
Warn_t ref;
Report_t dum;

CLHY_DECLARE(kuda_status_t) clhy_platform_create_privileged_process(
    const request_rec *r,
    kuda_proc_t *newproc, const char *progname,
    const char * const *args,
    const char * const *env,
    kuda_procattr_t *attr, kuda_pool_t *p)
{
    return kuda_proc_create(newproc, progname, args, env, attr, p);
}

int _NonAppCheckUnload(void)
{
    return nlmUnloadSignaled(1);
}

/* down server event callback */
void clhy_down_server_cb(void *a, void *b)
{
    nlmUnloadSignaled(0);
    return;
}

/* Required place holder event callback */
void clhy_dummy_cb(void *a, void *b)
{
    return;
}

/* destroy callback resources */
void clhy_cb_destroy(void *a)
{
    /* cleanup down event notification */
    UnRegisterEventNotification(eh);
    NX_UNWRAP_INTERFACE(ref);
    NX_UNWRAP_INTERFACE(dum);
}

int _NonAppStart
(
    void        *NLMHandle,
    void        *errorScreen,
    const char  *cmdLine,
    const char  *loadDirPath,
    size_t      uninitializedDataLength,
    void        *NLMFileHandle,
    int         (*readRoutineP)( int conn, void *fileHandle, size_t offset,
                    size_t nbytes, size_t *bytesRead, void *buffer ),
    size_t      customDataOffset,
    size_t      customDataSize,
    int         messageCount,
    const char  **messages
)
{
#pragma unused(cmdLine)
#pragma unused(loadDirPath)
#pragma unused(uninitializedDataLength)
#pragma unused(NLMFileHandle)
#pragma unused(readRoutineP)
#pragma unused(customDataOffset)
#pragma unused(customDataSize)
#pragma unused(messageCount)
#pragma unused(messages)

    /* register for down server event */
    rtag_t rt = AllocateResourceTag(NLMHandle, "cLHy1 Down Server Callback",
                                    EventSignature);

    NX_WRAP_INTERFACE((void *)clhy_down_server_cb, 2, (void **)&ref);
    NX_WRAP_INTERFACE((void *)clhy_dummy_cb, 2, (void **)&dum);
    eh = RegisterForEventNotification(rt, EVENT_DOWN_SERVER,
                                      EVENT_PRIORITY_APPLICATION,
                                      ref, dum, NULL);

    /* clean-up */
    NXVmRegisterExitHandler(clhy_cb_destroy, NULL);

    return 0;
}

