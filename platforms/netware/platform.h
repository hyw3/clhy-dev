/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file netware/platform.h
 * @brief This file in included in all cLHy source code. It contains definitions
 * of facilities available on _this_ operating system (HAVE_* macros),
 * and prototypes of PLATFORM specific functions defined in platform.c or platforms-inline.c
 *
 * @defgroup CLHYKUDEL_PLATFORM_NETWARE netware
 * @ingroup  CLHYKUDEL_PLATFORM
 * @{
 */

#ifndef CLHYKUDEL_PLATFORM_H
#define CLHYKUDEL_PLATFORM_H

#ifndef PLATFORM
#define PLATFORM "NETWARE"
#endif

/* Define command-line rewriting for this platform, handled by core.
 * For Netware, this is currently handled inside the Netware cLMP.
 * XXX To support a choice of cLMPs, extract common platform behavior
 * into a function specified here.
 */
#define CLHY_PLATFORM_REWRITE_ARGS_HOOK NULL

#include <screen.h>

CLHY_DECLARE_DATA extern int hold_screen_on_exit; /* Indicates whether the screen should be held open on exit*/

#define CASE_BLIND_FILESYSTEM
#define NO_WRITEV

#define getpid NXThreadGetId

/* Hold the screen open if there is an exit code and the hold_screen_on_exit flag >= 0 or the
   hold_screen_on_exit > 0.  If the hold_screen_on_exit flag is < 0 then close the screen no
   matter what the exit code is. */
#define exit(s) {if((s||hold_screen_on_exit)&&(hold_screen_on_exit>=0)){pressanykey();}kuda_terminate();exit(s);}

#endif   /* ! CLHYKUDEL_PLATFORM_H */
/** @} */
