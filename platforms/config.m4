AC_MSG_CHECKING(for target platform)

case $host in
*pc-os2-emx*)
  PLATFORM="os2"
  PLATFORM_DIR=$PLATFORM
  ;;
bs2000*)
  PLATFORM="unix"
  PLATFORM_DIR=$PLATFORM
  ;;
*cygwin*)
  PLATFORM="cygwin"
  PLATFORM_DIR="unix"
  ;;
*mingw32*)
  PLATFORM="win32"
  PLATFORM_DIR=$PLATFORM
  ;;
*)
  PLATFORM="unix"
  PLATFORM_DIR=$PLATFORM;;
esac

AC_MSG_RESULT($PLATFORM)
CLHYKUDEL_FAST_OUTPUT(platforms/${PLATFORM_DIR}/Makefile)
