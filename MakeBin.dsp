# Microsoft Developer Studio Project File - Name="MakeBin" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=MakeBin - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "MakeBin.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "MakeBin.mak" CFG="MakeBin - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "MakeBin - Win32 Release" (based on "Win32 (x86) External Target")
!MESSAGE "MakeBin - Win32 Debug" (based on "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "MakeBin - Win32 Release"

# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /f makefile.win"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "\cLHy1\bin\wwhy.exe"
# PROP BASE Bsc_Name ".\Browse\MakeBin.bsc"
# PROP BASE Target_Dir ""
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /f makefile.win INSTDIR="\cLHy1" LONG=Release _trydb _trylua _tryxml _tryssl _tryzlib _trynghttp2 _trybrotli _tryclmd _dummy"
# PROP Rebuild_Opt ""
# PROP Target_File "\cLHy1\bin\wwhy.exe"
# PROP Bsc_Name ".\Browse\wwhy.bsc"
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "MakeBin - Win32 Debug"

# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ""
# PROP BASE Intermediate_Dir ""
# PROP BASE Cmd_Line "NMAKE /f makefile.win"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "\cLHy1\bin\wwhy.exe"
# PROP BASE Bsc_Name ".\Browse\MakeBin.bsc"
# PROP BASE Target_Dir ""
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ""
# PROP Intermediate_Dir ""
# PROP Cmd_Line "NMAKE /f makefile.win INSTDIR="\cLHy1" LONG=Debug _trydb _trylua _tryxml _tryssl _tryzlib _trynghttp2 _trybrotli _tryclmd _dummy"
# PROP Rebuild_Opt ""
# PROP Target_File "\cLHy1\bin\wwhy.exe"
# PROP Bsc_Name ".\Browse\wwhy.bsc"
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "MakeBin - Win32 Release"
# Name "MakeBin - Win32 Debug"

!IF  "$(CFG)" == "MakeBin - Win32 Release"

!ELSEIF  "$(CFG)" == "MakeBin - Win32 Debug"

!ENDIF 

# Begin Source File

SOURCE=.\platforms\win32\BaseAddr.ref
# End Source File
# Begin Source File

SOURCE=.\hyscmterm.id
# End Source File
# Begin Source File

SOURCE=.\Makefile.win
# End Source File
# Begin Source File

SOURCE=.\hyscmterm
# End Source File
# End Target
# End Project
