/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * httxt2dbm.c: simple program for converting RewriteMap text files to DBM
 * Rewrite databases for the cLHy HTTP server
 *
 */

#include "kuda.h"
#include "kuda_lib.h"
#include "kuda_strings.h"
#include "kuda_file_io.h"
#include "kuda_file_info.h"
#include "kuda_pools.h"
#include "kuda_getopt.h"
#include "kudelman.h"
#include "kuda_dbm.h"

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h> /* for atexit() */
#endif

static const char *input;
static const char *output;
static const char *format;
static const char *shortname;
static kuda_file_t *errfile;
static int verbose;

/* From capi_rewrite.c */
#ifndef REWRITE_MAX_TXT_MAP_LINE
#define REWRITE_MAX_TXT_MAP_LINE 1024
#endif

#define NL KUDA_EOL_STR

#define AVAIL "available"
#define UNAVAIL "unavailable"

static void usage(void)
{
    const char *have_sdbm;
    const char *have_gdbm;
    const char *have_ndbm;
    const char *have_db;

#if KUDELMAN_HAVE_SDBM
    have_sdbm = AVAIL;
#else
    have_sdbm = UNAVAIL;
#endif
#if KUDELMAN_HAVE_GDBM
    have_gdbm = AVAIL;
#else
    have_gdbm = UNAVAIL;
#endif
#if KUDELMAN_HAVE_NDBM
    have_ndbm = AVAIL;
#else
    have_ndbm = UNAVAIL;
#endif
#if KUDELMAN_HAVE_DB
    have_db = AVAIL;
#else
    have_db = UNAVAIL;
#endif

    kuda_file_printf(errfile,
    "%s -- Program to Create DBM Files for use by RewriteMap" NL
    "Usage: %s [-v] [-f format] -i SOURCE_TXT -o OUTPUT_DBM" NL
    NL
    "Options: " NL
    " -v    More verbose output" NL
    NL
    " -i    Source Text File. If '-', use stdin." NL
    NL
    " -o    Output DBM." NL
    NL
    " -f    DBM Format.  If not specified, will use the KUDA Default." NL
    "           GDBM for GDBM files (%s)" NL
    "           SDBM for SDBM files (%s)" NL
    "           DB   for berkeley DB files (%s)" NL
    "           NDBM for NDBM files (%s)" NL
    "           default for the default DBM type" NL
    NL,
    shortname,
    shortname,
    have_gdbm,
    have_sdbm,
    have_db,
    have_ndbm);
}


static kuda_status_t to_dbm(kuda_dbm_t *dbm, kuda_file_t *fp, kuda_pool_t *pool)
{
    kuda_status_t rv = KUDA_SUCCESS;
    char line[REWRITE_MAX_TXT_MAP_LINE + 1]; /* +1 for \0 */
    kuda_datum_t dbmkey;
    kuda_datum_t dbmval;
    kuda_pool_t* p;

    kuda_pool_create(&p, pool);

    while (kuda_file_gets(line, sizeof(line), fp) == KUDA_SUCCESS) {
        char *c, *value;

        if (*line == '#' || kuda_isspace(*line)) {
            continue;
        }

        c = line;

        while (*c && !kuda_isspace(*c)) {
            ++c;
        }

        if (!*c) {
            /* no value. solid line of data. */
            continue;
        }

        dbmkey.dptr = kuda_pstrmemdup(p, line,  c - line);
        dbmkey.dsize = (c - line);

        while (kuda_isspace(*c)) {
            ++c;
        }

        if (!*c) {
            kuda_pool_clear(p);
            continue;
        }

        value = c;

        while (*c && !kuda_isspace(*c)) {
            ++c;
        }

        dbmval.dptr = kuda_pstrmemdup(p, value,  c - value);
        dbmval.dsize = (c - value);

        if (verbose) {
            kuda_file_printf(errfile, "    '%s' -> '%s'" NL,
                            dbmkey.dptr, dbmval.dptr);
        }

        rv = kuda_dbm_store(dbm, dbmkey, dbmval);

        kuda_pool_clear(p);

        if (rv != KUDA_SUCCESS) {
            break;
        }
    }

    return rv;
}

int main(int argc, const char *const argv[])
{
    kuda_pool_t *pool;
    kuda_status_t rv = KUDA_SUCCESS;
    kuda_getopt_t *opt;
    const char *opt_arg;
    char ch;
    kuda_file_t *infile;
    kuda_dbm_t *outdbm;

    kuda_app_initialize(&argc, &argv, NULL);
    atexit(kuda_terminate);

    verbose = 0;
    format = NULL;
    input = NULL;
    output = NULL;

    kuda_pool_create(&pool, NULL);

    if (argc) {
        shortname = kuda_filepath_name_get(argv[0]);
    }
    else {
        shortname = "httxt2dbm";
    }

    kuda_file_open_stderr(&errfile, pool);
    rv = kuda_getopt_init(&opt, pool, argc, argv);

    if (rv != KUDA_SUCCESS) {
        kuda_file_printf(errfile, "Error: kuda_getopt_init failed." NL NL);
        return 1;
    }

    if (argc <= 1) {
        usage();
        return 1;
    }

    while ((rv = kuda_getopt(opt, "vf::i::o::", &ch, &opt_arg)) == KUDA_SUCCESS) {
        switch (ch) {
        case 'v':
            if (verbose) {
                kuda_file_printf(errfile, "Error: -v can only be passed once" NL NL);
                usage();
                return 1;
            }
            verbose = 1;
            break;
        case 'f':
            if (format) {
                kuda_file_printf(errfile, "Error: -f can only be passed once" NL NL);
                usage();
                return 1;
            }
            format = kuda_pstrdup(pool, opt_arg);
            break;
        case 'i':
            if (input) {
                kuda_file_printf(errfile, "Error: -i can only be passed once" NL NL);
                usage();
                return 1;
            }
            input = kuda_pstrdup(pool, opt_arg);
            break;
        case 'o':
            if (output) {
                kuda_file_printf(errfile, "Error: -o can only be passed once" NL NL);
                usage();
                return 1;
            }
            output = kuda_pstrdup(pool, opt_arg);
            break;
        }
    }

    if (rv != KUDA_EOF) {
        kuda_file_printf(errfile, "Error: Parsing Arguments Failed" NL NL);
        usage();
        return 1;
    }

    if (!input) {
        kuda_file_printf(errfile, "Error: No input file specified." NL NL);
        usage();
        return 1;
    }

    if (!output) {
        kuda_file_printf(errfile, "Error: No output DBM specified." NL NL);
        usage();
        return 1;
    }

    if (!format) {
        format = "default";
    }

    if (verbose) {
        kuda_file_printf(errfile, "DBM Format: %s" NL, format);
    }

    if (!strcmp(input, "-")) {
        rv = kuda_file_open_stdin(&infile, pool);
    }
    else {
        rv = kuda_file_open(&infile, input, KUDA_READ|KUDA_BUFFERED,
                           KUDA_PLATFORM_DEFAULT, pool);
    }

    if (rv != KUDA_SUCCESS) {
        kuda_file_printf(errfile,
                        "Error: Cannot open input file '%s': (%d) %pm" NL NL,
                         input, rv, &rv);
        return 1;
    }

    if (verbose) {
        kuda_file_printf(errfile, "Input File: %s" NL, input);
    }

    rv = kuda_dbm_open_ex(&outdbm, format, output, KUDA_DBM_RWCREATE,
                    KUDA_PLATFORM_DEFAULT, pool);

    if (KUDA_STATUS_IS_ENOTIMPL(rv)) {
        kuda_file_printf(errfile,
                        "Error: The requested DBM Format '%s' is not available." NL NL,
                         format);
        return 1;
    }

    if (rv != KUDA_SUCCESS) {
        kuda_file_printf(errfile,
                        "Error: Cannot open output DBM '%s': (%d) %pm" NL NL,
                         output, rv, &rv);
        return 1;
    }

    if (verbose) {
        kuda_file_printf(errfile, "DBM File: %s" NL, output);
    }

    rv = to_dbm(outdbm, infile, pool);

    if (rv != KUDA_SUCCESS) {
        kuda_file_printf(errfile,
                        "Error: Converting to DBM: (%d) %pm" NL NL,
                         rv, &rv);
        return 1;
    }

    kuda_dbm_close(outdbm);

    if (verbose) {
        kuda_file_printf(errfile, "Conversion Complete." NL);
    }

    return 0;
}

