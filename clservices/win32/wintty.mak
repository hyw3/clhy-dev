# Microsoft Developer Studio Generated NMAKE File, Based on wintty.dsp
!IF "$(CFG)" == ""
CFG=wintty - Win32 Debug
!MESSAGE No configuration specified. Defaulting to wintty - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "wintty - Win32 Release" && "$(CFG)" != "wintty - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "wintty.mak" CFG="wintty - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "wintty - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "wintty - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "wintty - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\wintty.exe" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "kudadelman - Win32 Release" "kuda - Win32 Release" "$(OUTDIR)\wintty.exe" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kuda - Win32 ReleaseCLEAN" "kudadelman - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\wintty.obj"
	-@erase "$(INTDIR)\wintty.res"
	-@erase "$(INTDIR)\wintty_src.idb"
	-@erase "$(INTDIR)\wintty_src.pdb"
	-@erase "$(OUTDIR)\wintty.exe"
	-@erase "$(OUTDIR)\wintty.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../kudelrunsrc/kuda/include" /I "../kudelrunsrc/kuda-delman/include" /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "KUDA_DECLARE_STATIC" /D "KUDELMAN_DECLARE_STATIC" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\wintty_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\wintty.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "NDEBUG" /d "APP_FILE" /d BIN_NAME="wintty.exe" /d LONG_NAME="cLHy wintty console pipe" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\wintty.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib advapi32.lib shell32.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\wintty.pdb" /debug /out:"$(OUTDIR)\wintty.exe" /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\wintty.obj" \
	"$(INTDIR)\wintty.res" \
	"..\..\kudelrunsrc\kuda\LibR\kuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\LibR\kudadelman-1.lib"

"$(OUTDIR)\wintty.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\wintty.exe
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\wintty.exe"
   if exist .\Release\wintty.exe.manifest mt.exe -manifest .\Release\wintty.exe.manifest -outputresource:.\Release\wintty.exe;1
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "wintty - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\wintty.exe" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "kudadelman - Win32 Debug" "kuda - Win32 Debug" "$(OUTDIR)\wintty.exe" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kuda - Win32 DebugCLEAN" "kudadelman - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\wintty.obj"
	-@erase "$(INTDIR)\wintty.res"
	-@erase "$(INTDIR)\wintty_src.idb"
	-@erase "$(INTDIR)\wintty_src.pdb"
	-@erase "$(OUTDIR)\wintty.exe"
	-@erase "$(OUTDIR)\wintty.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../kudelrunsrc/kuda/include" /I "../kudelrunsrc/kuda-delman/include" /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "KUDA_DECLARE_STATIC" /D "KUDELMAN_DECLARE_STATIC" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\wintty_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\wintty.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "_DEBUG" /d "APP_FILE" /d BIN_NAME="wintty.exe" /d LONG_NAME="cLHy wintty console pipe" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\wintty.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib advapi32.lib shell32.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\wintty.pdb" /debug /out:"$(OUTDIR)\wintty.exe" 
LINK32_OBJS= \
	"$(INTDIR)\wintty.obj" \
	"$(INTDIR)\wintty.res" \
	"..\..\kudelrunsrc\kuda\LibD\kuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\LibD\kudadelman-1.lib"

"$(OUTDIR)\wintty.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\wintty.exe
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\wintty.exe"
   if exist .\Debug\wintty.exe.manifest mt.exe -manifest .\Debug\wintty.exe.manifest -outputresource:.\Debug\wintty.exe;1
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("wintty.dep")
!INCLUDE "wintty.dep"
!ELSE 
!MESSAGE Warning: cannot find "wintty.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "wintty - Win32 Release" || "$(CFG)" == "wintty - Win32 Debug"

!IF  "$(CFG)" == "wintty - Win32 Release"

"kuda - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Release" 
   cd "..\..\clservices\win32"

"kuda - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\clservices\win32"

!ELSEIF  "$(CFG)" == "wintty - Win32 Debug"

"kuda - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Debug" 
   cd "..\..\clservices\win32"

"kuda - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\clservices\win32"

!ENDIF 

!IF  "$(CFG)" == "wintty - Win32 Release"

"kudadelman - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Release" 
   cd "..\..\clservices\win32"

"kudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\clservices\win32"

!ELSEIF  "$(CFG)" == "wintty - Win32 Debug"

"kudadelman - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Debug" 
   cd "..\..\clservices\win32"

"kudadelman - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\clservices\win32"

!ENDIF 

SOURCE=..\..\build\win32\wwhy.rc

!IF  "$(CFG)" == "wintty - Win32 Release"


"$(INTDIR)\wintty.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\wintty.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../build\win32" /d "NDEBUG" /d "APP_FILE" /d BIN_NAME="wintty.exe" /d LONG_NAME="cLHy wintty console pipe" $(SOURCE)


!ELSEIF  "$(CFG)" == "wintty - Win32 Debug"


"$(INTDIR)\wintty.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\wintty.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /i "../../build\win32" /d "_DEBUG" /d "APP_FILE" /d BIN_NAME="wintty.exe" /d LONG_NAME="cLHy wintty console pipe" $(SOURCE)


!ENDIF 

SOURCE=.\wintty.c

"$(INTDIR)\wintty.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 

