# Microsoft Developer Studio Generated NMAKE File, Based on cLHyMonitor.dsp
!IF "$(CFG)" == ""
CFG=cLHyMonitor - Win32 Debug
!MESSAGE No configuration specified. Defaulting to cLHyMonitor - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "cLHyMonitor - Win32 Release" && "$(CFG)" != "cLHyMonitor - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "cLHyMonitor.mak" CFG="cLHyMonitor - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "cLHyMonitor - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "cLHyMonitor - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "cLHyMonitor - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\cLHyMonitor.exe" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "kudadelman - Win32 Release" "kuda - Win32 Release" "$(OUTDIR)\cLHyMonitor.exe" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kuda - Win32 ReleaseCLEAN" "kudadelman - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\cLHyMonitor.obj"
	-@erase "$(INTDIR)\cLHyMonitor.res"
	-@erase "$(INTDIR)\cLHyMonitor_src.idb"
	-@erase "$(INTDIR)\cLHyMonitor_src.pdb"
	-@erase "$(OUTDIR)\cLHyMonitor.exe"
	-@erase "$(OUTDIR)\cLHyMonitor.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../../include" /I "../../kudelrunsrc/kuda/include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "STRICT" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\cLHyMonitor_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\cLHyMonitor.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "NDEBUG" /d "APP_FILE" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\cLHyMonitor.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib comctl32.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib wtsapi32.lib /nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\cLHyMonitor.pdb" /debug /out:"$(OUTDIR)\cLHyMonitor.exe" /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\cLHyMonitor.obj" \
	"$(INTDIR)\cLHyMonitor.res" \
	"..\..\kudelrunsrc\kuda\LibR\kuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\LibR\kudadelman-1.lib"

"$(OUTDIR)\cLHyMonitor.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\cLHyMonitor.exe
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\cLHyMonitor.exe"
   if exist .\Release\cLHyMonitor.exe.manifest mt.exe -manifest .\Release\cLHyMonitor.exe.manifest -outputresource:.\Release\cLHyMonitor.exe;1
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "cLHyMonitor - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\cLHyMonitor.exe" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "kudadelman - Win32 Debug" "kuda - Win32 Debug" "$(OUTDIR)\cLHyMonitor.exe" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kuda - Win32 DebugCLEAN" "kudadelman - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\cLHyMonitor.obj"
	-@erase "$(INTDIR)\cLHyMonitor.res"
	-@erase "$(INTDIR)\cLHyMonitor_src.idb"
	-@erase "$(INTDIR)\cLHyMonitor_src.pdb"
	-@erase "$(OUTDIR)\cLHyMonitor.exe"
	-@erase "$(OUTDIR)\cLHyMonitor.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Gm /Zi /Od /I "../../include" /I "../../kudelrunsrc/kuda/include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "STRICT" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\cLHyMonitor_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\cLHyMonitor.res" /i "../../include" /i "../../kudelrunsrc/kuda/include" /d "_DEBUG" /d "APP_FILE" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\cLHyMonitor.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib comctl32.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib wtsapi32.lib /nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\cLHyMonitor.pdb" /debug /out:"$(OUTDIR)\cLHyMonitor.exe" 
LINK32_OBJS= \
	"$(INTDIR)\cLHyMonitor.obj" \
	"$(INTDIR)\cLHyMonitor.res" \
	"..\..\kudelrunsrc\kuda\LibD\kuda-1.lib" \
	"..\..\kudelrunsrc\kuda-delman\LibD\kudadelman-1.lib"

"$(OUTDIR)\cLHyMonitor.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\cLHyMonitor.exe
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\cLHyMonitor.exe"
   if exist .\Debug\cLHyMonitor.exe.manifest mt.exe -manifest .\Debug\cLHyMonitor.exe.manifest -outputresource:.\Debug\cLHyMonitor.exe;1
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("cLHyMonitor.dep")
!INCLUDE "cLHyMonitor.dep"
!ELSE 
!MESSAGE Warning: cannot find "cLHyMonitor.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "cLHyMonitor - Win32 Release" || "$(CFG)" == "cLHyMonitor - Win32 Debug"

!IF  "$(CFG)" == "cLHyMonitor - Win32 Release"

"kuda - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Release" 
   cd "..\..\clservices\win32"

"kuda - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\clservices\win32"

!ELSEIF  "$(CFG)" == "cLHyMonitor - Win32 Debug"

"kuda - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Debug" 
   cd "..\..\clservices\win32"

"kuda - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\clservices\win32"

!ENDIF 

!IF  "$(CFG)" == "cLHyMonitor - Win32 Release"

"kudadelman - Win32 Release" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Release" 
   cd "..\..\clservices\win32"

"kudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\clservices\win32"

!ELSEIF  "$(CFG)" == "cLHyMonitor - Win32 Debug"

"kudadelman - Win32 Debug" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Debug" 
   cd "..\..\clservices\win32"

"kudadelman - Win32 DebugCLEAN" : 
   cd ".\..\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\clservices\win32"

!ENDIF 

SOURCE=.\cLHyMonitor.c

"$(INTDIR)\cLHyMonitor.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cLHyMonitor.rc

"$(INTDIR)\cLHyMonitor.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)



!ENDIF 

