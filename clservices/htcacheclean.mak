# Microsoft Developer Studio Generated NMAKE File, Based on htcacheclean.dsp
!IF "$(CFG)" == ""
CFG=htcacheclean - Win32 Debug
!MESSAGE No configuration specified. Defaulting to htcacheclean - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "htcacheclean - Win32 Release" && "$(CFG)" != "htcacheclean - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "htcacheclean.mak" CFG="htcacheclean - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "htcacheclean - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "htcacheclean - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "htcacheclean - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\htcacheclean.exe" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "kudadelman - Win32 Release" "kuda - Win32 Release" "$(OUTDIR)\htcacheclean.exe" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kuda - Win32 ReleaseCLEAN" "kudadelman - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\htcacheclean.obj"
	-@erase "$(INTDIR)\htcacheclean.res"
	-@erase "$(INTDIR)\htcacheclean_src.idb"
	-@erase "$(INTDIR)\htcacheclean_src.pdb"
	-@erase "$(OUTDIR)\htcacheclean.exe"
	-@erase "$(OUTDIR)\htcacheclean.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../kudelrunsrc/kuda/include" /I "../kudelrunsrc/kuda-delman/include" /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "KUDA_DECLARE_STATIC" /D "KUDELMAN_DECLARE_STATIC" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\htcacheclean_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\htcacheclean.res" /i "../include" /i "../kudelrunsrc/kuda/include" /d "NDEBUG" /d "APP_FILE" /d BIN_NAME="htcacheclean.exe" /d LONG_NAME="cLHy htcacheclean command line utility" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\htcacheclean.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib advapi32.lib wsock32.lib ws2_32.lib rpcrt4.lib shell32.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\htcacheclean.pdb" /debug /out:"$(OUTDIR)\htcacheclean.exe" /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\htcacheclean.obj" \
	"$(INTDIR)\htcacheclean.res" \
	"..\kudelrunsrc\kuda\LibR\kuda-1.lib" \
	"..\kudelrunsrc\kuda-delman\LibR\kudadelman-1.lib"

"$(OUTDIR)\htcacheclean.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\htcacheclean.exe
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\htcacheclean.exe"
   if exist .\Release\htcacheclean.exe.manifest mt.exe -manifest .\Release\htcacheclean.exe.manifest -outputresource:.\Release\htcacheclean.exe;1
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "htcacheclean - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\htcacheclean.exe" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "kudadelman - Win32 Debug" "kuda - Win32 Debug" "$(OUTDIR)\htcacheclean.exe" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kuda - Win32 DebugCLEAN" "kudadelman - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\htcacheclean.obj"
	-@erase "$(INTDIR)\htcacheclean.res"
	-@erase "$(INTDIR)\htcacheclean_src.idb"
	-@erase "$(INTDIR)\htcacheclean_src.pdb"
	-@erase "$(OUTDIR)\htcacheclean.exe"
	-@erase "$(OUTDIR)\htcacheclean.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../kudelrunsrc/kuda/include" /I "../kudelrunsrc/kuda-delman/include" /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "KUDA_DECLARE_STATIC" /D "KUDELMAN_DECLARE_STATIC" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\htcacheclean_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\htcacheclean.res" /i "../include" /i "../kudelrunsrc/kuda/include" /d "_DEBUG" /d "APP_FILE" /d BIN_NAME="htcacheclean.exe" /d LONG_NAME="cLHy htcacheclean command line utility" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\htcacheclean.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib advapi32.lib wsock32.lib ws2_32.lib rpcrt4.lib shell32.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\htcacheclean.pdb" /debug /out:"$(OUTDIR)\htcacheclean.exe" 
LINK32_OBJS= \
	"$(INTDIR)\htcacheclean.obj" \
	"$(INTDIR)\htcacheclean.res" \
	"..\kudelrunsrc\kuda\LibD\kuda-1.lib" \
	"..\kudelrunsrc\kuda-delman\LibD\kudadelman-1.lib"

"$(OUTDIR)\htcacheclean.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\htcacheclean.exe
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\htcacheclean.exe"
   if exist .\Debug\htcacheclean.exe.manifest mt.exe -manifest .\Debug\htcacheclean.exe.manifest -outputresource:.\Debug\htcacheclean.exe;1
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("htcacheclean.dep")
!INCLUDE "htcacheclean.dep"
!ELSE 
!MESSAGE Warning: cannot find "htcacheclean.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "htcacheclean - Win32 Release" || "$(CFG)" == "htcacheclean - Win32 Debug"

!IF  "$(CFG)" == "htcacheclean - Win32 Release"

"kuda - Win32 Release" : 
   cd ".\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Release" 
   cd "..\..\clservices"

"kuda - Win32 ReleaseCLEAN" : 
   cd ".\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\clservices"

!ELSEIF  "$(CFG)" == "htcacheclean - Win32 Debug"

"kuda - Win32 Debug" : 
   cd ".\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Debug" 
   cd "..\..\clservices"

"kuda - Win32 DebugCLEAN" : 
   cd ".\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\clservices"

!ENDIF 

!IF  "$(CFG)" == "htcacheclean - Win32 Release"

"kudadelman - Win32 Release" : 
   cd ".\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Release" 
   cd "..\..\clservices"

"kudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\clservices"

!ELSEIF  "$(CFG)" == "htcacheclean - Win32 Debug"

"kudadelman - Win32 Debug" : 
   cd ".\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Debug" 
   cd "..\..\clservices"

"kudadelman - Win32 DebugCLEAN" : 
   cd ".\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\clservices"

!ENDIF 

SOURCE=.\htcacheclean.c

"$(INTDIR)\htcacheclean.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=..\build\win32\wwhy.rc

!IF  "$(CFG)" == "htcacheclean - Win32 Release"


"$(INTDIR)\htcacheclean.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\htcacheclean.res" /i "../include" /i "../kudelrunsrc/kuda/include" /i "../build\win32" /d "NDEBUG" /d "APP_FILE" /d BIN_NAME="htcacheclean.exe" /d LONG_NAME="cLHy htcacheclean command line utility" $(SOURCE)


!ELSEIF  "$(CFG)" == "htcacheclean - Win32 Debug"


"$(INTDIR)\htcacheclean.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\htcacheclean.res" /i "../include" /i "../kudelrunsrc/kuda/include" /i "../build\win32" /d "_DEBUG" /d "APP_FILE" /d BIN_NAME="htcacheclean.exe" /d LONG_NAME="cLHy htcacheclean command line utility" $(SOURCE)


!ENDIF 


!ENDIF 

