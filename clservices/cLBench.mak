# Microsoft Developer Studio Generated NMAKE File, Based on cLBench.dsp
!IF "$(CFG)" == ""
CFG=cLBench - Win32 Debug
!MESSAGE No configuration specified. Defaulting to cLBench - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "cLBench - Win32 Release" && "$(CFG)" != "cLBench - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "cLBench.mak" CFG="cLBench - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "cLBench - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "cLBench - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "cLBench - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\cLBench.exe" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "kudadelman - Win32 Release" "kuda - Win32 Release" "$(OUTDIR)\cLBench.exe" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kuda - Win32 ReleaseCLEAN" "kudadelman - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\cLBench.obj"
	-@erase "$(INTDIR)\cLBench.res"
	-@erase "$(INTDIR)\cLBench_src.idb"
	-@erase "$(INTDIR)\cLBench_src.pdb"
	-@erase "$(OUTDIR)\cLBench.exe"
	-@erase "$(OUTDIR)\cLBench.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../kudelrunsrc/kuda/include" /I "../kudelrunsrc/kuda-delman/include" /I "../include" /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "KUDA_DECLARE_STATIC" /D "KUDELMAN_DECLARE_STATIC" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\cLBench_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\cLBench.res" /i "../include" /i "../kudelrunsrc/kuda/include" /d "NDEBUG" /d "APP_FILE" /d BIN_NAME="cLBench.exe" /d LONG_NAME="cLHyBench command line utility" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\cLBench.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib advapi32.lib wsock32.lib ws2_32.lib rpcrt4.lib shell32.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\cLBench.pdb" /debug /out:"$(OUTDIR)\cLBench.exe" /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\cLBench.obj" \
	"$(INTDIR)\cLBench.res" \
	"..\kudelrunsrc\kuda\LibR\kuda-1.lib" \
	"..\kudelrunsrc\kuda-delman\LibR\kudadelman-1.lib"

"$(OUTDIR)\cLBench.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\cLBench.exe
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\cLBench.exe"
   if exist .\Release\cLBench.exe.manifest mt.exe -manifest .\Release\cLBench.exe.manifest -outputresource:.\Release\cLBench.exe;1
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "cLBench - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\cLBench.exe" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "kudadelman - Win32 Debug" "kuda - Win32 Debug" "$(OUTDIR)\cLBench.exe" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kuda - Win32 DebugCLEAN" "kudadelman - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\cLBench.obj"
	-@erase "$(INTDIR)\cLBench.res"
	-@erase "$(INTDIR)\cLBench_src.idb"
	-@erase "$(INTDIR)\cLBench_src.pdb"
	-@erase "$(OUTDIR)\cLBench.exe"
	-@erase "$(OUTDIR)\cLBench.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../kudelrunsrc/kuda/include" /I "../kudelrunsrc/kuda-delman/include" /I "../include" /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "KUDA_DECLARE_STATIC" /D "KUDELMAN_DECLARE_STATIC" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\cLBench_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\cLBench.res" /i "../include" /i "../kudelrunsrc/kuda/include" /d "_DEBUG" /d "APP_FILE" /d BIN_NAME="cLBench.exe" /d LONG_NAME="cLHyBench command line utility" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\cLBench.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib advapi32.lib wsock32.lib ws2_32.lib rpcrt4.lib shell32.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\cLBench.pdb" /debug /out:"$(OUTDIR)\cLBench.exe" 
LINK32_OBJS= \
	"$(INTDIR)\cLBench.obj" \
	"$(INTDIR)\cLBench.res" \
	"..\kudelrunsrc\kuda\LibD\kuda-1.lib" \
	"..\kudelrunsrc\kuda-delman\LibD\kudadelman-1.lib"

"$(OUTDIR)\cLBench.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\cLBench.exe
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\cLBench.exe"
   if exist .\Debug\cLBench.exe.manifest mt.exe -manifest .\Debug\cLBench.exe.manifest -outputresource:.\Debug\cLBench.exe;1
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("cLBench.dep")
!INCLUDE "cLBench.dep"
!ELSE 
!MESSAGE Warning: cannot find "cLBench.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "cLBench - Win32 Release" || "$(CFG)" == "cLBench - Win32 Debug"

!IF  "$(CFG)" == "cLBench - Win32 Release"

"kuda - Win32 Release" : 
   cd ".\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Release" 
   cd "..\..\clservices"

"kuda - Win32 ReleaseCLEAN" : 
   cd ".\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\clservices"

!ELSEIF  "$(CFG)" == "cLBench - Win32 Debug"

"kuda - Win32 Debug" : 
   cd ".\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Debug" 
   cd "..\..\clservices"

"kuda - Win32 DebugCLEAN" : 
   cd ".\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\clservices"

!ENDIF 

!IF  "$(CFG)" == "cLBench - Win32 Release"

"kudadelman - Win32 Release" : 
   cd ".\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Release" 
   cd "..\..\clservices"

"kudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\clservices"

!ELSEIF  "$(CFG)" == "cLBench - Win32 Debug"

"kudadelman - Win32 Debug" : 
   cd ".\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Debug" 
   cd "..\..\clservices"

"kudadelman - Win32 DebugCLEAN" : 
   cd ".\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\clservices"

!ENDIF 

SOURCE=.\cLBench.c

"$(INTDIR)\cLBench.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=..\build\win32\wwhy.rc

!IF  "$(CFG)" == "cLBench - Win32 Release"


"$(INTDIR)\cLBench.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\cLBench.res" /i "../include" /i "../kudelrunsrc/kuda/include" /i "../build\win32" /d "NDEBUG" /d "APP_FILE" /d BIN_NAME="cLBench.exe" /d LONG_NAME="cLHyBench command line utility" $(SOURCE)


!ELSEIF  "$(CFG)" == "cLBench - Win32 Debug"


"$(INTDIR)\cLBench.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\cLBench.res" /i "../include" /i "../kudelrunsrc/kuda/include" /i "../build\win32" /d "_DEBUG" /d "APP_FILE" /d BIN_NAME="cLBench.exe" /d LONG_NAME="cLHyBench command line utility" $(SOURCE)


!ENDIF 


!ENDIF 


