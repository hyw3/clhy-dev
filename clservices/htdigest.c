/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/******************************************************************************
 ******************************************************************************
 * NOTE! This program is not safe as a setuid executable!  Do not make it
 * setuid!
 ******************************************************************************
 *****************************************************************************/
/*
 * htdigest.c: simple program for manipulating digest passwd file for cLHy
 *
 * Based on the works of Alexei Kosut and Rob McCool
 */

#include "kuda.h"
#include "kuda_file_io.h"
#include "kuda_md5.h"
#include "kuda_lib.h"            /* for kuda_getpass() */
#include "kuda_general.h"
#include "kuda_signal.h"
#include "kuda_strings.h"        /* for kuda_pstrdup() */

#define KUDA_WANT_STDIO
#define KUDA_WANT_STRFUNC
#include "kuda_want.h"

#if KUDA_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef WIN32
#include <conio.h>
#endif


#if KUDA_CHARSET_EBCDIC
#define LF '\n'
#define CR '\r'
#else
#define LF 10
#define CR 13
#endif /* KUDA_CHARSET_EBCDIC */

#define MAX_STRING_LEN 256

kuda_file_t *tfp = NULL;
kuda_file_t *errfile;
kuda_pool_t *cntxt;
#if KUDA_CHARSET_EBCDIC
kuda_xlate_t *to_ascii;
#endif

static void cleanup_tempfile_and_exit(int rc)
{
    if (tfp) {
        kuda_file_close(tfp);
    }
    exit(rc);
}

static void getword(char *word, char *line, char stop)
{
    int x = 0, y;

    for (x = 0; ((line[x]) && (line[x] != stop)); x++)
        word[x] = line[x];

    word[x] = '\0';
    if (line[x])
        ++x;
    y = 0;

    while ((line[y++] = line[x++]));
}

static int get_line(char *s, int n, kuda_file_t *f)
{
    int i = 0;
    char ch;
    kuda_status_t rv = KUDA_EINVAL;

    /* we need 2 remaining bytes in buffer */
    while (i < (n - 2) &&
           ((rv = kuda_file_getc(&ch, f)) == KUDA_SUCCESS) && (ch != '\n')) {
        s[i++] = ch;
    }
    /* First remaining byte potentially used here */
    if (ch == '\n')
        s[i++] = ch;
    /* Second remaining byte used here */
    s[i] = '\0';

    if (rv != KUDA_SUCCESS)
        return 1;

    return 0;
}

static void putline(kuda_file_t *f, char *l)
{
    int x;

    for (x = 0; l[x]; x++)
        kuda_file_putc(l[x], f);
}


static void add_password(const char *user, const char *realm, kuda_file_t *f)
{
    char *pw;
    kuda_md5_ctx_t context;
    unsigned char digest[16];
    char string[3 * MAX_STRING_LEN]; /* this includes room for 2 * ':' + '\0' */
    char pwin[MAX_STRING_LEN];
    char pwv[MAX_STRING_LEN];
    unsigned int i;
    kuda_size_t len = sizeof(pwin);

    if (kuda_password_get("New password: ", pwin, &len) != KUDA_SUCCESS) {
        kuda_file_printf(errfile, "password too long");
        cleanup_tempfile_and_exit(5);
    }
    len = sizeof(pwin);
    kuda_password_get("Re-type new password: ", pwv, &len);
    if (strcmp(pwin, pwv) != 0) {
        kuda_file_printf(errfile, "They don't match, sorry.\n");
        cleanup_tempfile_and_exit(1);
    }
    pw = pwin;
    kuda_file_printf(f, "%s:%s:", user, realm);

    /* Do MD5 stuff */
    kuda_snprintf(string, sizeof(string), "%s:%s:%s", user, realm, pw);

    kuda_md5_init(&context);
#if KUDA_CHARSET_EBCDIC
    kuda_md5_set_xlate(&context, to_ascii);
#endif
    kuda_md5_update(&context, (unsigned char *) string, strlen(string));
    kuda_md5_final(digest, &context);

    for (i = 0; i < 16; i++)
        kuda_file_printf(f, "%02x", digest[i]);

    kuda_file_printf(f, "\n");
}

static void usage(void)
{
    kuda_file_printf(errfile, "Usage: htdigest [-c] passwordfile realm username\n");
    kuda_file_printf(errfile, "The -c flag creates a new file.\n");
    exit(1);
}

static void interrupted(void)
{
    kuda_file_printf(errfile, "Interrupted.\n");
    cleanup_tempfile_and_exit(1);
}

static void terminate(void)
{
    kuda_terminate();
#ifdef NETWARE
    pressanykey();
#endif
}

int main(int argc, const char * const argv[])
{
    kuda_file_t *f;
    kuda_status_t rv;
    char tn[] = "htdigest.tmp.XXXXXX";
    char *dirname;
    char user[MAX_STRING_LEN];
    char realm[MAX_STRING_LEN];
    char line[3 * MAX_STRING_LEN];
    char l[3 * MAX_STRING_LEN];
    char w[MAX_STRING_LEN];
    char x[MAX_STRING_LEN];
    int found;

    kuda_app_initialize(&argc, &argv, NULL);
    atexit(terminate);
    kuda_pool_create(&cntxt, NULL);
    kuda_file_open_stderr(&errfile, cntxt);

#if KUDA_CHARSET_EBCDIC
    rv = kuda_xlate_open(&to_ascii, "ISO-8859-1", KUDA_DEFAULT_CHARSET, cntxt);
    if (rv) {
        kuda_file_printf(errfile, "kuda_xlate_open(): %pm (%d)\n",
                &rv, rv);
        exit(1);
    }
#endif

    kuda_signal(SIGINT, (void (*)(int)) interrupted);
    if (argc == 5) {
        if (strcmp(argv[1], "-c"))
            usage();
        rv = kuda_file_open(&f, argv[2], KUDA_WRITE | KUDA_CREATE,
                           KUDA_PLATFORM_DEFAULT, cntxt);
        if (rv != KUDA_SUCCESS) {
            kuda_file_printf(errfile, "Could not open passwd file %s for writing: %pm\n",
                    argv[2], &rv);
            exit(1);
        }
        kuda_cpystrn(user, argv[4], sizeof(user));
        kuda_cpystrn(realm, argv[3], sizeof(realm));
        kuda_file_printf(errfile, "Adding password for %s in realm %s.\n",
                    user, realm);
        add_password(user, realm, f);
        kuda_file_close(f);
        exit(0);
    }
    else if (argc != 4)
        usage();

    if (kuda_temp_dir_get((const char**)&dirname, cntxt) != KUDA_SUCCESS) {
        kuda_file_printf(errfile, "%s: could not determine temp dir\n",
                        argv[0]);
        exit(1);
    }
    dirname = kuda_psprintf(cntxt, "%s/%s", dirname, tn);

    if (kuda_file_mktemp(&tfp, dirname, 0, cntxt) != KUDA_SUCCESS) {
        kuda_file_printf(errfile, "Could not open temp file %s.\n", dirname);
        exit(1);
    }

    if (kuda_file_open(&f, argv[1], KUDA_READ, KUDA_PLATFORM_DEFAULT, cntxt) != KUDA_SUCCESS) {
        kuda_file_printf(errfile,
                "Could not open passwd file %s for reading.\n", argv[1]);
        kuda_file_printf(errfile, "Use -c option to create new one.\n");
        cleanup_tempfile_and_exit(1);
    }
    kuda_cpystrn(user, argv[3], sizeof(user));
    kuda_cpystrn(realm, argv[2], sizeof(realm));

    found = 0;
    while (!(get_line(line, sizeof(line), f))) {
        if (found || (line[0] == '#') || (!line[0])) {
            putline(tfp, line);
            continue;
        }
        strcpy(l, line);
        getword(w, l, ':');
        getword(x, l, ':');
        if (strcmp(user, w) || strcmp(realm, x)) {
            putline(tfp, line);
            continue;
        }
        else {
            kuda_file_printf(errfile, "Changing password for user %s in realm %s\n",
                    user, realm);
            add_password(user, realm, tfp);
            found = 1;
        }
    }
    if (!found) {
        kuda_file_printf(errfile, "Adding user %s in realm %s\n", user, realm);
        add_password(user, realm, tfp);
    }
    kuda_file_close(f);

    /* The temporary file has all the data, just copy it to the new location.
     */
    if (kuda_file_copy(dirname, argv[1], KUDA_FILE_SOURCE_PERMS, cntxt) !=
                KUDA_SUCCESS) {
        kuda_file_printf(errfile, "%s: unable to update file %s\n",
                        argv[0], argv[1]);
    }
    kuda_file_close(tfp);

    return 0;
}
