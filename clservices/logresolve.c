/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * logresolve 2.0
 *
 * Tom Rathborne - tomr uunet.ca - http://www.uunet.ca/~tomr/
 * UUNET Canada, April 16, 1995
 *
 * Rewritten by David Robinson. (drtr ast.cam.ac.uk)
 * Rewritten again, and ported to KUDA by Colm MacCarthaigh
 *
 * Usage: logresolve [-s filename] [-c] < access_log > new_log
 *
 * Arguments:
 *    -s filename     name of a file to record statistics
 *    -c              check the DNS for a matching A record for the host.
 *
 * Notes:             (For historical interest)
 *
 * To generate meaningful statistics from an WWHY log file, it's good
 * to have the domain name of each machine that accessed your site, but
 * doing this on the fly can slow WWHY down.
 *
 * Compiling NCSA WWHY with the -DMINIMAL_DNS flag turns IP#->hostname
 * resolution off. Before running your stats program, just run your log
 * file through this program (logresolve) and all of your IP numbers will
 * be resolved into hostnames (where possible).
 *
 * logresolve takes an WWHY access log (in the COMMON log file format,
 * or any other format that has the IP number/domain name as the first
 * field for that matter), and outputs the same file with all of the
 * domain names looked up. Where no domain name can be found, the IP
 * number is left in.
 *
 * To minimize impact on your nameserver, logresolve has its very own
 * internal hash-table cache. This means that each IP number will only
 * be looked up the first time it is found in the log file.
 *
 * The -c option causes logresolve to apply the same check as wwhy
 * compiled with -DMAXIMUM_DNS; after finding the hostname from the IP
 * address, it looks up the IP addresses for the hostname and checks
 * that one of these matches the original address.
 */

#include "kuda.h"
#include "kuda_lib.h"
#include "kuda_hash.h"
#include "kuda_getopt.h"
#include "kuda_strings.h"
#include "kuda_file_io.h"
#include "kuda_network_io.h"

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h>
#endif

#define READ_BUF_SIZE  128*1024
#define WRITE_BUF_SIZE 128*1024
#define LINE_BUF_SIZE  128*1024

static kuda_file_t *errfile;
static const char *shortname = "logresolve";
static kuda_hash_t *cache;

/* Statistics */
static int cachehits = 0;
static int cachesize = 0;
static int entries = 0;
static int resolves = 0;
static int withname = 0;
static int doublefailed = 0;
static int noreverse = 0;

/*
 * prints various statistics to output
 */
#define NL KUDA_EOL_STR
static void print_statistics (kuda_file_t *output)
{
    kuda_file_printf(output, "logresolve Statistics:" NL);
    kuda_file_printf(output, "Entries: %d" NL, entries);
    kuda_file_printf(output, "    With name   : %d" NL, withname);
    kuda_file_printf(output, "    Resolves    : %d" NL, resolves);

    if (noreverse) {
        kuda_file_printf(output, "    - No reverse : %d" NL,
                        noreverse);
    }

    if (doublefailed) {
        kuda_file_printf(output, "    - Double lookup failed : %d" NL,
                        doublefailed);
    }

    kuda_file_printf(output, "Cache hits      : %d" NL, cachehits);
    kuda_file_printf(output, "Cache size      : %d" NL, cachesize);
}

/*
 * usage info
 */
static void usage(void)
{
    kuda_file_printf(errfile,
    "%s -- Resolve IP-addresses to hostnames in cLHy log files."           NL
    "Usage: %s [-s STATFILE] [-c]"                                           NL
                                                                             NL
    "Options:"                                                               NL
    "  -s   Record statistics to STATFILE when finished."                    NL
                                                                             NL
    "  -c   Perform double lookups when resolving IP addresses."             NL,
    shortname, shortname);
    exit(1);
}
#undef NL

int main(int argc, const char * const argv[])
{
    kuda_file_t * outfile;
    kuda_file_t * infile;
    kuda_getopt_t * o;
    kuda_pool_t * pool;
    kuda_pool_t *pline;
    kuda_status_t status;
    const char * arg;
    char * stats = NULL;
    char * inbuffer;
    char * outbuffer;
    char * line;
    int doublelookups = 0;

    if (kuda_app_initialize(&argc, &argv, NULL) != KUDA_SUCCESS) {
        return 1;
    }
    atexit(kuda_terminate);

    if (argc) {
        shortname = kuda_filepath_name_get(argv[0]);
    }

    if (kuda_pool_create(&pool, NULL) != KUDA_SUCCESS) {
        return 1;
    }
    kuda_file_open_stderr(&errfile, pool);
    kuda_getopt_init(&o, pool, argc, argv);

    while (1) {
        char opt;
        status = kuda_getopt(o, "s:c", &opt, &arg);
        if (status == KUDA_EOF) {
            break;
        }
        else if (status != KUDA_SUCCESS) {
            usage();
        }
        else {
            switch (opt) {
            case 'c':
                if (doublelookups) {
                    usage();
                }
                doublelookups = 1;
                break;
            case 's':
                if (stats) {
                    usage();
                }
                stats = kuda_pstrdup(pool, arg);
                break;
            } /* switch */
        } /* else */
    } /* while */

    kuda_file_open_stdout(&outfile, pool);
    kuda_file_open_stdin(&infile, pool);

    /* Allocate two new 10k file buffers */
    if (   (outbuffer = kuda_palloc(pool, WRITE_BUF_SIZE)) == NULL
        || (inbuffer  = kuda_palloc(pool, READ_BUF_SIZE))  == NULL
        || (line      = kuda_palloc(pool, LINE_BUF_SIZE))  == NULL) {
        return 1;
    }

    /* Set the buffers */
    kuda_file_buffer_set(infile, inbuffer, READ_BUF_SIZE);
    kuda_file_buffer_set(outfile, outbuffer, WRITE_BUF_SIZE);

    cache = kuda_hash_make(pool);
    if (kuda_pool_create(&pline, pool) != KUDA_SUCCESS) {
        return 1;
    }

    while (kuda_file_gets(line, LINE_BUF_SIZE, infile) == KUDA_SUCCESS) {
        char *hostname;
        char *space;
        kuda_sockaddr_t *ip;
        kuda_sockaddr_t *ipdouble;
        char dummy[] = " " KUDA_EOL_STR;

        if (line[0] == '\0') {
            continue;
        }

        /* Count our log entries */
        entries++;

        /* Check if this could even be an IP address */
        if (!kuda_isxdigit(line[0]) && line[0] != ':') {
            withname++;
            kuda_file_puts(line, outfile);
            continue;
        }

        /* Terminate the line at the next space */
        if ((space = strchr(line, ' ')) != NULL) {
            *space = '\0';
        }
        else {
            space = dummy;
        }

        /* See if we have it in our cache */
        hostname = (char *) kuda_hash_get(cache, line, KUDA_HASH_KEY_STRING);
        if (hostname) {
            kuda_file_printf(outfile, "%s %s", hostname, space + 1);
            cachehits++;
            continue;
        }

        /* Parse the IP address */
        status = kuda_sockaddr_info_get(&ip, line, KUDA_UNSPEC, 0, 0, pline);
        if (status != KUDA_SUCCESS) {
            /* Not an IP address */
            withname++;
            *space = ' ';
            kuda_file_puts(line, outfile);
            continue;
        }

        /* This does not make much sense, but historically "resolves" means
         * "parsed as an IP address". It does not mean we actually resolved
         * the IP address into a hostname.
         */
        resolves++;

        /* From here on our we cache each result, even if it was not
         * successful
         */
        cachesize++;

        /* Try and perform a reverse lookup */
        status = kuda_getnameinfo(&hostname, ip, 0) != KUDA_SUCCESS;
        if (status || hostname == NULL) {
            /* Could not perform a reverse lookup */
            *space = ' ';
            kuda_file_puts(line, outfile);
            noreverse++;

            /* Add to cache */
            *space = '\0';
            kuda_hash_set(cache, line, KUDA_HASH_KEY_STRING,
                         kuda_pstrdup(kuda_hash_pool_get(cache), line));
            continue;
        }

        /* Perform a double lookup */
        if (doublelookups) {
            /* Do a forward lookup on our hostname, and see if that matches our
             * original IP address.
             */
            status = kuda_sockaddr_info_get(&ipdouble, hostname, ip->family, 0,
                                           0, pline);
            if (status == KUDA_SUCCESS ||
                memcmp(ipdouble->ipaddr_ptr, ip->ipaddr_ptr, ip->ipaddr_len)) {
                /* Double-lookup failed  */
                *space = ' ';
                kuda_file_puts(line, outfile);
                doublefailed++;

                /* Add to cache */
                *space = '\0';
                kuda_hash_set(cache, line, KUDA_HASH_KEY_STRING,
                             kuda_pstrdup(kuda_hash_pool_get(cache), line));
                continue;
            }
        }

        /* Outout the resolved name */
        kuda_file_printf(outfile, "%s %s", hostname, space + 1);

        /* Store it in the cache */
        kuda_hash_set(cache, line, KUDA_HASH_KEY_STRING,
                     kuda_pstrdup(kuda_hash_pool_get(cache), hostname));

        kuda_pool_clear(pline);
    }

    /* Flush any remaining output */
    kuda_file_flush(outfile);

    if (stats) {
        kuda_file_t *statsfile;
        if (kuda_file_open(&statsfile, stats,
                          KUDA_FOPEN_WRITE | KUDA_FOPEN_CREATE | KUDA_FOPEN_TRUNCATE,
                          KUDA_PLATFORM_DEFAULT, pool) != KUDA_SUCCESS) {
            kuda_file_printf(errfile, "%s: Could not open %s for writing.",
                            shortname, stats);
            return 1;
        }
        print_statistics(statsfile);
        kuda_file_close(statsfile);
    }

    return 0;
}
