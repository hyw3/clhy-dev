htpasswd_LTFLAGS=""
htdigest_LTFLAGS=""
rotatelogs_LTFLAGS=""
logresolve_LTFLAGS=""
htdbm_LTFLAGS=""
clbench_LTFLAGS=""
checkgid_LTFLAGS=""
htcacheclean_LTFLAGS=""
httxt2dbm_LTFLAGS=""
fcgistarter_LTFLAGS=""

AC_ARG_ENABLE(static-support,CLHYKUDEL_HELP_STRING(--enable-static-support,Build a statically linked version of the support binaries),[
if test "$enableval" = "yes" ; then
  KUDA_ADDTO(htpasswd_LTFLAGS, [-static])
  KUDA_ADDTO(htdigest_LTFLAGS, [-static])
  KUDA_ADDTO(rotatelogs_LTFLAGS, [-static])
  KUDA_ADDTO(logresolve_LTFLAGS, [-static])
  KUDA_ADDTO(htdbm_LTFLAGS, [-static])
  KUDA_ADDTO(clbench_LTFLAGS, [-static])
  KUDA_ADDTO(checkgid_LTFLAGS, [-static])
  KUDA_ADDTO(htcacheclean_LTFLAGS, [-static])
  KUDA_ADDTO(httxt2dbm_LTFLAGS, [-static])
  KUDA_ADDTO(fcgistarter_LTFLAGS, [-static])
fi
])

AC_ARG_ENABLE(static-htpasswd,CLHYKUDEL_HELP_STRING(--enable-static-htpasswd,Build a statically linked version of htpasswd),[
if test "$enableval" = "yes" ; then
  KUDA_ADDTO(htpasswd_LTFLAGS, [-static])
else
  KUDA_REMOVEFROM(htpasswd_LTFLAGS, [-static])
fi
])
CLHYKUDEL_SUBST(htpasswd_LTFLAGS)

AC_ARG_ENABLE(static-htdigest,CLHYKUDEL_HELP_STRING(--enable-static-htdigest,Build a statically linked version of htdigest),[
if test "$enableval" = "yes" ; then
  KUDA_ADDTO(htdigest_LTFLAGS, [-static])
else
  KUDA_REMOVEFROM(htdigest_LTFLAGS, [-static])
fi
])
CLHYKUDEL_SUBST(htdigest_LTFLAGS)

AC_ARG_ENABLE(static-rotatelogs,CLHYKUDEL_HELP_STRING(--enable-static-rotatelogs,Build a statically linked version of rotatelogs),[
if test "$enableval" = "yes" ; then
  KUDA_ADDTO(rotatelogs_LTFLAGS, [-static])
else
  KUDA_REMOVEFROM(rotatelogs_LTFLAGS, [-static])
fi
])
CLHYKUDEL_SUBST(rotatelogs_LTFLAGS)

AC_ARG_ENABLE(static-logresolve,CLHYKUDEL_HELP_STRING(--enable-static-logresolve,Build a statically linked version of logresolve),[
if test "$enableval" = "yes" ; then
  KUDA_ADDTO(logresolve_LTFLAGS, [-static])
else
  KUDA_REMOVEFROM(logresolve_LTFLAGS, [-static])
fi
])
CLHYKUDEL_SUBST(logresolve_LTFLAGS)

AC_ARG_ENABLE(static-htdbm,CLHYKUDEL_HELP_STRING(--enable-static-htdbm,Build a statically linked version of htdbm),[
if test "$enableval" = "yes" ; then
  KUDA_ADDTO(htdbm_LTFLAGS, [-static])
else
  KUDA_REMOVEFROM(htdbm_LTFLAGS, [-static])
fi
])
CLHYKUDEL_SUBST(htdbm_LTFLAGS)

AC_ARG_ENABLE(static-cLBench,CLHYKUDEL_HELP_STRING(--enable-static-cLBench,Build a statically linked version of cLBench),[
if test "$enableval" = "yes" ; then
  KUDA_ADDTO(clbench_LTFLAGS, [-static])
else
  KUDA_REMOVEFROM(clbench_LTFLAGS, [-static])
fi
])
CLHYKUDEL_SUBST(clbench_LTFLAGS)

AC_ARG_ENABLE(static-checkgid,CLHYKUDEL_HELP_STRING(--enable-static-checkgid,Build a statically linked version of checkgid),[
if test "$enableval" = "yes" ; then
  KUDA_ADDTO(checkgid_LTFLAGS, [-static])
else
  KUDA_REMOVEFROM(checkgid_LTFLAGS, [-static])
fi
])
CLHYKUDEL_SUBST(checkgid_LTFLAGS)

AC_ARG_ENABLE(static-htcacheclean,CLHYKUDEL_HELP_STRING(--enable-static-htcacheclean,Build a statically linked version of htcacheclean),[
if test "$enableval" = "yes" ; then
  KUDA_ADDTO(htcacheclean_LTFLAGS, [-static])
else
  KUDA_REMOVEFROM(htcacheclean_LTFLAGS, [-static])
fi
])
CLHYKUDEL_SUBST(htcacheclean_LTFLAGS)

AC_ARG_ENABLE(static-httxt2dbm,CLHYKUDEL_HELP_STRING(--enable-static-httxt2dbm,Build a statically linked version of httxt2dbm),[
if test "$enableval" = "yes" ; then
  KUDA_ADDTO(httxt2dbm_LTFLAGS, [-static])
else
  KUDA_REMOVEFROM(httxt2dbm, [-static])
fi
])
CLHYKUDEL_SUBST(httxt2dbm_LTFLAGS)

AC_ARG_ENABLE(static-fcgistarter,CLHYKUDEL_HELP_STRING(--enable-static-fcgistarter,Build a statically linked version of fcgistarter),[
if test "$enableval" = "yes" ; then
  KUDA_ADDTO(fcgistarter_LTFLAGS, [-static])
else
  KUDA_REMOVEFROM(fcgistarter, [-static])
fi
])
CLHYKUDEL_SUBST(fcgistarter_LTFLAGS)

# Configure or check which of the non-portable support programs can be enabled.

NONPORTABLE_SUPPORT=""
case $host in
    *mingw*)
        ;;
    *)
        NONPORTABLE_SUPPORT="checkgid fcgistarter"
        ;;
esac
CLHYKUDEL_SUBST(NONPORTABLE_SUPPORT)

# Configure the ulimit -n command used by delmanserve.

case $host in
    *aix*)
        # this works in any locale, unlike the default command below, which
        # fails in a non-English locale if the hard limit is unlimited
        # since the display of the limit will translate "unlimited", but
        # ulimit only accepts English "unlimited" on input
        DELMANSERVE_ULIMIT="ulimit -S -n unlimited"
        ;;
    *alpha*-dec-osf*)
        # Tru64: -H is for setting, not retrieving
        DELMANSERVE_ULIMIT="ulimit -S -n \`ulimit -h -n\`"
        ;;
    *)
        if TMP_ULIMIT=`ulimit -H -n` && ulimit -S -n $TMP_ULIMIT >/dev/null 2>&1; then
            DELMANSERVE_ULIMIT="ulimit -S -n \`ulimit -H -n\`"
        else
            DELMANSERVE_ULIMIT=""
        fi
        ;;
esac
CLHYKUDEL_SUBST(DELMANSERVE_ULIMIT)
