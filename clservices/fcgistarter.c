/* The cLHy Server
 * 
 * Copyright (C) 2019 The Hyang Language Foundation (HLF), Jakarta.
 * The HLF licenses this file under the GNU GPL version 3 or later
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://clhy.hyang.org/license.hyss
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <kuda.h>
#include <kuda_pools.h>
#include <kuda_network_io.h>
#include <kuda_thread_proc.h>
#include <kuda_getopt.h>
#include <kuda_portable.h>

#if KUDA_HAVE_STDLIB_H
#include <stdlib.h> /* For EXIT_SUCCESS, EXIT_FAILURE */
#endif

#if KUDA_HAVE_UNISTD_H
#include <unistd.h> /* For execl */
#endif

static const char *usage_message =
    "usage: fcgistarter -c <command> -p <port> [-i <interface> -N <num>]\n"
    "\n"
    "If an interface is not specified, any available will be used.\n";

static void usage(void)
{
    fprintf(stderr, "%s", usage_message);

    exit(EXIT_FAILURE);
}

static void exit_error(kuda_status_t rv, const char *func)
{
    char buffer[1024];

    fprintf(stderr,
            "%s: %s\n",
            func,
            kuda_strerror(rv, buffer, sizeof(buffer)));

    exit(EXIT_FAILURE);
}

int main(int argc, const char * const argv[])
{
    kuda_file_t *infd, *skwrapper;
    kuda_sockaddr_t *skaddr;
    kuda_getopt_t *gopt;
    kuda_socket_t *skt;
    kuda_pool_t *pool;
    kuda_status_t rv;
    kuda_proc_t proc;


    /* Command line arguments */
    int num_to_start = 1, port = 0;
    const char *interface = NULL;
    const char *command = NULL;

    kuda_app_initialize(&argc, &argv, NULL);

    atexit(kuda_terminate);

    kuda_pool_create(&pool, NULL);

    rv = kuda_getopt_init(&gopt, pool, argc, argv);
    if (rv) {
        return EXIT_FAILURE;
    }

    for (;;) {
        const char *arg;
        char opt;

        rv = kuda_getopt(gopt, "c:p:i:N:", &opt, &arg);
        if (KUDA_STATUS_IS_EOF(rv)) {
            break;
        } else if (rv) {
            usage();
        } else {
            switch (opt) {
            case 'c':
                command = arg;
                break;

            case 'p':
                port = atoi(arg);
                if (! port) {
                    usage();
                }
                break;

            case 'i':
                interface = arg;
                break;

            case 'N':
                num_to_start = atoi(arg);
                if (! num_to_start) {
                    usage();
                }
                break;

            default:
                break;
            }
        }
    }

    if (! command || ! port) {
        usage();
    }

    rv = kuda_sockaddr_info_get(&skaddr, interface, KUDA_UNSPEC, port, 0, pool);
    if (rv) {
        exit_error(rv, "kuda_sockaddr_info_get");
    }

    rv = kuda_socket_create(&skt, skaddr->family, SOCK_STREAM, KUDA_PROTO_TCP, pool);
    if (rv) {
        exit_error(rv, "kuda_socket_create");
    }

    rv = kuda_socket_opt_set(skt, KUDA_SO_REUSEADDR, 1);
    if (rv) {
        exit_error(rv, "kuda_socket_opt_set(KUDA_SO_REUSEADDR)");
    }

    rv = kuda_socket_bind(skt, skaddr);
    if (rv) {
        exit_error(rv, "kuda_socket_bind");
    }

    rv = kuda_socket_listen(skt, 1024);
    if (rv) {
        exit_error(rv, "kuda_socket_listen");
    }

    rv = kuda_proc_detach(KUDA_PROC_DETACH_DAEMONIZE);
    if (rv) {
        exit_error(rv, "kuda_proc_detach");
    }

#if defined(WIN32) || defined(OS2) || defined(NETWARE)

#error "Please implement me."

#else

    while (--num_to_start >= 0) {
        rv = kuda_proc_fork(&proc, pool);
        if (rv == KUDA_INCHILD) {
            kuda_platform_file_t oft = 0;
            kuda_platform_sock_t oskt;

            /* Ok, so we need a file that has file descriptor 0 (which
             * FastCGI wants), but points to our socket.  This isn't really
             * possible in KUDA, so we cheat a bit.  I have no idea how to
             * do this on a non-unix platform, so for now this is platform
             * specific.  Ick.
             *
             * Note that this has to happen post-detach, otherwise fd 0
             * gets closed during kuda_proc_detach and it's all for nothing.
             *
             * Unfortunately, doing this post detach means we have no way
             * to let anyone know if there's a problem at this point :( */

            rv = kuda_platform_file_put(&infd, &oft, KUDA_READ | KUDA_WRITE, pool);
            if (rv) {
                exit(EXIT_FAILURE);
            }

            rv = kuda_platform_sock_get(&oskt, skt);
            if (rv) {
                exit(EXIT_FAILURE);
            }

            rv = kuda_platform_file_put(&skwrapper, &oskt, KUDA_READ | KUDA_WRITE,
                                 pool);
            if (rv) {
                exit(EXIT_FAILURE);
            }

            rv = kuda_file_dup2(infd, skwrapper, pool);
            if (rv) {
                exit(EXIT_FAILURE);
            }

            /* XXX Can't use kuda_proc_create because there's no way to get
             *     infd into the procattr without going through another dup2,
             *     which means by the time it gets to the fastcgi process it
             *     is no longer fd 0, so it doesn't work.  Sigh. */

            execl(command, command, NULL);

        } else if (rv == KUDA_INPARENT) {
            if (num_to_start == 0) {
                kuda_socket_close(skt);
            }
        } else {
            exit_error(rv, "kuda_proc_fork");
        }
    }

#endif

    return EXIT_SUCCESS;
}
