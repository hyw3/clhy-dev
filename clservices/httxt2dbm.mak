# Microsoft Developer Studio Generated NMAKE File, Based on httxt2dbm.dsp
!IF "$(CFG)" == ""
CFG=httxt2dbm - Win32 Debug
!MESSAGE No configuration specified. Defaulting to httxt2dbm - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "httxt2dbm - Win32 Release" && "$(CFG)" != "httxt2dbm - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "httxt2dbm.mak" CFG="httxt2dbm - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "httxt2dbm - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "httxt2dbm - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(PLATFORM)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "httxt2dbm - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\httxt2dbm.exe" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "kudadelman - Win32 Release" "kuda - Win32 Release" "$(OUTDIR)\httxt2dbm.exe" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kuda - Win32 ReleaseCLEAN" "kudadelman - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\httxt2dbm.obj"
	-@erase "$(INTDIR)\httxt2dbm.res"
	-@erase "$(INTDIR)\httxt2dbm_src.idb"
	-@erase "$(INTDIR)\httxt2dbm_src.pdb"
	-@erase "$(OUTDIR)\httxt2dbm.exe"
	-@erase "$(OUTDIR)\httxt2dbm.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /Zi /O2 /Oy- /I "../kudelrunsrc/kuda/include" /I "../kudelrunsrc/kuda-delman/include" /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "KUDA_DECLARE_STATIC" /D "KUDELMAN_DECLARE_STATIC" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\httxt2dbm_src" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\httxt2dbm.res" /i "../include" /i "../kudelrunsrc/kuda/include" /d "NDEBUG" /d "APP_FILE" /d BIN_NAME="httxt2dbm.exe" /d LONG_NAME="cLHy httxt2dbm command line utility" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\httxt2dbm.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib advapi32.lib wsock32.lib ws2_32.lib rpcrt4.lib shell32.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\httxt2dbm.pdb" /debug /out:"$(OUTDIR)\httxt2dbm.exe" /opt:ref 
LINK32_OBJS= \
	"$(INTDIR)\httxt2dbm.obj" \
	"$(INTDIR)\httxt2dbm.res" \
	"..\kudelrunsrc\kuda\LibR\kuda-1.lib" \
	"..\kudelrunsrc\kuda-delman\LibR\kudadelman-1.lib"

"$(OUTDIR)\httxt2dbm.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Release\httxt2dbm.exe
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\httxt2dbm.exe"
   if exist .\Release\httxt2dbm.exe.manifest mt.exe -manifest .\Release\httxt2dbm.exe.manifest -outputresource:.\Release\httxt2dbm.exe;1
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "httxt2dbm - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\httxt2dbm.exe" "$(DS_POSTBUILD_DEP)"

!ELSE 

ALL : "kudadelman - Win32 Debug" "kuda - Win32 Debug" "$(OUTDIR)\httxt2dbm.exe" "$(DS_POSTBUILD_DEP)"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"kuda - Win32 DebugCLEAN" "kudadelman - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\httxt2dbm.obj"
	-@erase "$(INTDIR)\httxt2dbm.res"
	-@erase "$(INTDIR)\httxt2dbm_src.idb"
	-@erase "$(INTDIR)\httxt2dbm_src.pdb"
	-@erase "$(OUTDIR)\httxt2dbm.exe"
	-@erase "$(OUTDIR)\httxt2dbm.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Zi /Od /I "../kudelrunsrc/kuda/include" /I "../kudelrunsrc/kuda-delman/include" /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "KUDA_DECLARE_STATIC" /D "KUDELMAN_DECLARE_STATIC" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\httxt2dbm_src" /FD /EHsc /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\httxt2dbm.res" /i "../include" /i "../kudelrunsrc/kuda/include" /d "_DEBUG" /d "APP_FILE" /d BIN_NAME="httxt2dbm.exe" /d LONG_NAME="cLHy httxt2dbm command line utility" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\httxt2dbm.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib advapi32.lib wsock32.lib ws2_32.lib rpcrt4.lib shell32.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\httxt2dbm.pdb" /debug /out:"$(OUTDIR)\httxt2dbm.exe" 
LINK32_OBJS= \
	"$(INTDIR)\httxt2dbm.obj" \
	"$(INTDIR)\httxt2dbm.res" \
	"..\kudelrunsrc\kuda\LibD\kuda-1.lib" \
	"..\kudelrunsrc\kuda-delman\LibD\kudadelman-1.lib"

"$(OUTDIR)\httxt2dbm.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

TargetPath=.\Debug\httxt2dbm.exe
SOURCE="$(InputPath)"
PostBuild_Desc=Embed .manifest
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

"$(DS_POSTBUILD_DEP)" : "$(OUTDIR)\httxt2dbm.exe"
   if exist .\Debug\httxt2dbm.exe.manifest mt.exe -manifest .\Debug\httxt2dbm.exe.manifest -outputresource:.\Debug\httxt2dbm.exe;1
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("httxt2dbm.dep")
!INCLUDE "httxt2dbm.dep"
!ELSE 
!MESSAGE Warning: cannot find "httxt2dbm.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "httxt2dbm - Win32 Release" || "$(CFG)" == "httxt2dbm - Win32 Debug"

!IF  "$(CFG)" == "httxt2dbm - Win32 Release"

"kuda - Win32 Release" : 
   cd ".\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Release" 
   cd "..\..\clservices"

"kuda - Win32 ReleaseCLEAN" : 
   cd ".\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\clservices"

!ELSEIF  "$(CFG)" == "httxt2dbm - Win32 Debug"

"kuda - Win32 Debug" : 
   cd ".\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Debug" 
   cd "..\..\clservices"

"kuda - Win32 DebugCLEAN" : 
   cd ".\..\kudelrunsrc\kuda"
   $(MAKE) /$(MAKEFLAGS) /F ".\kuda.mak" CFG="kuda - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\clservices"

!ENDIF 

!IF  "$(CFG)" == "httxt2dbm - Win32 Release"

"kudadelman - Win32 Release" : 
   cd ".\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Release" 
   cd "..\..\clservices"

"kudadelman - Win32 ReleaseCLEAN" : 
   cd ".\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Release" RECURSE=1 CLEAN 
   cd "..\..\clservices"

!ELSEIF  "$(CFG)" == "httxt2dbm - Win32 Debug"

"kudadelman - Win32 Debug" : 
   cd ".\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Debug" 
   cd "..\..\clservices"

"kudadelman - Win32 DebugCLEAN" : 
   cd ".\..\kudelrunsrc\kuda-delman"
   $(MAKE) /$(MAKEFLAGS) /F ".\kudadelman.mak" CFG="kudadelman - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\..\clservices"

!ENDIF 

SOURCE=..\build\win32\wwhy.rc

!IF  "$(CFG)" == "httxt2dbm - Win32 Release"


"$(INTDIR)\httxt2dbm.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\httxt2dbm.res" /i "../include" /i "../kudelrunsrc/kuda/include" /i "../build\win32" /d "NDEBUG" /d "APP_FILE" /d BIN_NAME="httxt2dbm.exe" /d LONG_NAME="cLHy httxt2dbm command line utility" $(SOURCE)


!ELSEIF  "$(CFG)" == "httxt2dbm - Win32 Debug"


"$(INTDIR)\httxt2dbm.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x409 /fo"$(INTDIR)\httxt2dbm.res" /i "../include" /i "../kudelrunsrc/kuda/include" /i "../build\win32" /d "_DEBUG" /d "APP_FILE" /d BIN_NAME="httxt2dbm.exe" /d LONG_NAME="cLHy httxt2dbm command line utility" $(SOURCE)


!ENDIF 

SOURCE=.\httxt2dbm.c

"$(INTDIR)\httxt2dbm.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 

